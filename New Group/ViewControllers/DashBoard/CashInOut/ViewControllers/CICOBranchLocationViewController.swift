//
//  CICOBranchLocationViewController.swift
//  OK
//
//  Created by Kethan on 8/23/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import GoogleMaps

class CICOBranchLocationViewController: CashInOutBaseViewController {
    
    //MARK: - Outlets
    @IBOutlet private weak var mapCenterPinImage: UIImageView!
    @IBOutlet private weak var pinImageVerticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapView : GMSMapView!
    
    //MARK: - Properties
    private let locationManager = CLLocationManager()
    private let dataProvider = GoogleDataProvider()
    private let searchRadius: Double = 1000
    var currentBranchDetails : AvailableAgentsList? = nil
    var branchLat = 0.0
    var branchLong = 0.0
    
    //MARK: - Views Method
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addCustomBackButton(navTitle: currentBranchDetails?.firstName ?? "Branch Location")
        self.loadMapwithCurrentLocation()
        self.branchLat = currentBranchDetails?.currentLocation?.latitude ?? 0.0
        self.branchLong = currentBranchDetails?.currentLocation?.longitude ?? 0.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        showRoute(origin: Double( geoLocManager.currentLatitude ?? "0.0") ?? 0.0,
                  destination: Double(geoLocManager.currentLongitude ?? "0.0") ?? 0.0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Methods
    
    private func showMarkerImage() {
        DispatchQueue.main.async {
//            let startPosition = CLLocationCoordinate2D(latitude: Double(geoLocManager.currentLatitude ?? "0.0") ?? 0.0, longitude: Double(geoLocManager.currentLongitude ?? "0.0") ?? 0.0)
//            let startPos = GMSMarker(position: startPosition)
//            startPos.groundAnchor = CGPoint(x: 0.5, y: 0.5)
//            startPos.icon = #imageLiteral(resourceName: "cicoLocation")
//            startPos.map = self.mapView
            
            let endPosition = CLLocationCoordinate2D(latitude: self.branchLat, longitude: self.branchLong)
            let endPos = GMSMarker(position: endPosition)
            //endPos.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            endPos.icon = UIImage(named: "ic_location_pin")
            endPos.map = self.mapView
        }
    }
    
    func loadMapwithCurrentLocation() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        mapView.delegate = self
        
    }
    
    func showRoute(origin: Double, destination: Double) {
        
        let urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(String(origin)),\(String(destination))&destination=\(String(branchLat)),\(String(branchLong))&mode=driving&sensor=true&key=\(googleAPIKey)"
        guard let url = URL(string: urlString) else { return }
        self.showMarkerImage()
        URLSession.shared.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if (error != nil) {
                println_debug("error")
            } else {
                do {
                    guard let dataRaw = data else { return }
                    let json = try JSONSerialization.jsonObject(with: dataRaw, options:.allowFragments) as! [String : AnyObject]
                    let routes = json["routes"] as! NSArray
                    self.mapView.clear()
                    OperationQueue.main.addOperation({
                        for route in routes {
                            let routeOverviewPolyline = (route as! NSDictionary).value(forKey: "overview_polyline") as! NSDictionary
                            let points = routeOverviewPolyline.object(forKey: "points")
                            let path = GMSPath.init(fromEncodedPath: points! as! String)
                            let polyline = GMSPolyline.init(path: path)
                            polyline.strokeWidth = 3
                            polyline.strokeColor = UIColor.black
                            let bounds = GMSCoordinateBounds(path: path!)
                            self.mapView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 30.0))
                            polyline.map = self.mapView
                        }
                        self.showMarkerImage()
                    })
                } catch let error as NSError {
                    println_debug("error:\(error)")
                }
            }
        }).resume()
    }
    
    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            if #available(iOS 11.0, *) {
                self.mapView.padding = UIEdgeInsets(top: self.view.safeAreaInsets.top, left: 0,
                                                    bottom: self.view.safeAreaInsets.bottom, right: 0)
            } else {
                // Fallback on earlier versions
            }
            UIView.animate(withDuration: 0.25) {
                if #available(iOS 11.0, *) {
                    self.pinImageVerticalConstraint.constant = self.view.safeAreaInsets.top * 0.5
                } else {
                    // Fallback on earlier versions
                }
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func fetchNearbyPlaces(coordinate: CLLocationCoordinate2D) {
        mapView.clear()
        //
        //        dataProvider.fetchPlacesNearCoordinate(coordinate, radius:searchRadius, types: searchedTypes) { places in
        //            places.forEach {
        //                let marker = PlaceMarker(place: $0)
        //                marker.map = self.mapView
        //            }
        //        }
    }
}

//MARK: - CLLocationManagerDelegate
extension CICOBranchLocationViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
        locationManager.startUpdatingLocation()
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 17.0, bearing: 0, viewingAngle: 0)
        locationManager.stopUpdatingLocation()
        fetchNearbyPlaces(coordinate: location.coordinate)
    }
}

// MARK: - GMSMapViewDelegate
extension CICOBranchLocationViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        reverseGeocodeCoordinate(position.target)
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {        
        if (gesture) {
            mapCenterPinImage.fadeIn(0.25)
            mapView.selectedMarker = nil
        }
    }
    
    //    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
    //        guard let placeMarker = marker as? PlaceMarker else {
    //            return nil
    //        }
    //        guard let infoView = UIView.viewFromNibName("MarkerInfoView") as? MarkerInfoView else {
    //            return nil
    //        }
    //
    //        infoView.nameLabel.text = placeMarker.place.name
    //        if let photo = placeMarker.place.photo {
    //            infoView.placePhoto.image = photo
    //        } else {
    //            infoView.placePhoto.image = UIImage(named: "generic")
    //        }
    //
    //        return infoView
    //    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        mapCenterPinImage.fadeOut(0.25)
        return false
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        mapCenterPinImage.fadeIn(0.25)
        mapView.selectedMarker = nil
        return false
    }
}
