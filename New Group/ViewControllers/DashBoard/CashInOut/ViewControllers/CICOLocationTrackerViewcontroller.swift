//
//  CICOLocationTrackerViewcontroller.swift
//  OK
//
//  Created by Kethan on 8/16/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import GoogleMaps


class CICOLocationTrackerViewcontroller: CashInOutBaseViewController {
    //MARK: - Outlets
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet private weak var mapCenterPinImage: UIImageView!
    @IBOutlet private weak var pinImageVerticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapView : GMSMapView!
    @IBOutlet weak var acceptCancelView : UIView! {
        didSet {
            self.acceptCancelView.isHidden = true
        }
    }
    @IBOutlet weak var cancelRequestButton : UIButton! {
        didSet {
            self.cancelRequestButton.setTitle(self.cancelRequestButton.titleLabel?.text, for : .normal)
        }
    }
    @IBOutlet weak var acceptRequestButton : UIButton! {
        didSet {
            self.acceptRequestButton.setTitle(self.acceptRequestButton.titleLabel?.text, for : .normal)
        }
    }
    @IBOutlet weak var proceedAndPayButton : UIButton! {
        didSet {
            self.proceedAndPayButton.isHidden = false
            self.proceedAndPayButton.setTitle(self.proceedAndPayButton.titleLabel?.text, for : .normal)
        }
    }

    //MARK: - Properties
    private let locationManager = CLLocationManager()
    private let dataProvider = GoogleDataProvider()
    private let searchRadius: Double = 1000
    var locations : [(x: Double, y: Double)] = []
    var index = 0
    weak var timer: Timer?
    var timerDispatchSourceTimer : DispatchSourceTimer?
    private lazy var modelCashInCashOut: CashInCashOutModel = {
        return CashInCashOutModel()
    }()
    
    
    //MARK: - Views life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addCustomBackButton(navTitle: "Location")
        self.loadMapwithCurrentLocation()
        //GMSPlacesClient.provideAPIKey(googleAPIKey)
        self.addLocations()
        showRoute(origin: Double(geoLocManager.currentLatitude ?? "0.0")!, destination: Double(geoLocManager.currentLongitude ?? "0.0")!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        startTimer()
    }
    
    //MARK: - Methods
    func startTimer() {
        if #available(iOS 10.0, *) {
            timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { [weak self] _ in
                self?.showRoute(origin: (self?.locations[(self?.index)!].x)!, destination: (self?.locations[(self?.index)!].y)!)
                self?.index = (self?.index)! + 1
                if (self?.index)! > 4 {
                    self?.stopTimer()
                }
            }
        } else {
            // Fallback on earlier versions
            timerDispatchSourceTimer = DispatchSource.makeTimerSource(flags: [], queue: DispatchQueue.main)
            timerDispatchSourceTimer?.scheduleRepeating(deadline: .now(), interval: .seconds(60))
            timerDispatchSourceTimer?.setEventHandler{
                // do something here
            }
            timerDispatchSourceTimer?.resume()
        }
    }
    
    func stopTimer() {
        timer?.invalidate()
        //timerDispatchSourceTimer?.suspend() // if you want to suspend timer
        timerDispatchSourceTimer?.cancel()
    }
    
    // if appropriate, make sure to stop your timer in `deinit`
    deinit {
        stopTimer()
    }
    
    private func addLocations() {
        locations.append((x: Double(geoLocManager.currentLatitude ?? "0.0")!, y: Double(geoLocManager.currentLongitude ?? "0.0")!))
        locations.append((x: 16.7797785, y: 96.1722218))
        locations.append((x: 16.7765001, y: 96.1711311))
        locations.append((x: 16.7764989, y: 96.1699515))
        locations.append((x: 16.778836, y: 96.1700724))
        locations.append((x: 16.7788188, y: 96.17273849999999))
        locations.append((x: 16.7794446, y: 96.17277919999999))
    }
    
    func loadMapwithCurrentLocation() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        mapView.delegate = self
        //locationManager.distanceFilter = searchRadius
        //mapView.mapType = .satellite
    }
    
    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            self.addressLabel.unlock()
            
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
            
            self.addressLabel.text = lines.joined(separator: "\n") + ", " + (address.country ?? "")
            
            let labelHeight = self.addressLabel.intrinsicContentSize.height
            if #available(iOS 11.0, *) {
                self.mapView.padding = UIEdgeInsets(top: self.view.safeAreaInsets.top, left: 0,
                                                    bottom: labelHeight, right: 0)
            } else {
                // Fallback on earlier versions
            }
            
            UIView.animate(withDuration: 0.25) {
                if #available(iOS 11.0, *) {
                    self.pinImageVerticalConstraint.constant = ((labelHeight - self.view.safeAreaInsets.top) * 0.5)
                } else {
                    // Fallback on earlier versions
                }
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func fetchNearbyPlaces(coordinate: CLLocationCoordinate2D) {
        mapView.clear()
        let searchedTypes = ["cafe"]
        let searchRadius: Double = 150
        dataProvider.fetchPlacesNearCoordinate(coordinate, radius:searchRadius, types: searchedTypes) { places in
            places.forEach {
                let marker = PlaceMarker(place: $0)
                marker.map = self.mapView
            }
        }
    }
    
    //MARK: - Target Methods
    @objc func showRoute(origin: Double, destination: Double) {
        let urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(String(origin)),\(String(destination))&destination=16.7797785,96.1722218&mode=driving&sensor=true&key=\(googleAPIKey)"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if (error != nil) {
                println_debug("error")
            } else {
                do {
                    guard let dataRaw = data else { return }
                    let json = try JSONSerialization.jsonObject(with: dataRaw, options:.allowFragments) as! [String : AnyObject]
                    let routes = json["routes"] as! NSArray
                    self.mapView.clear()
                    OperationQueue.main.addOperation({
                        for route in routes
                        {
                            let routeOverviewPolyline:NSDictionary = (route as! NSDictionary).value(forKey: "overview_polyline") as! NSDictionary
                            let points = routeOverviewPolyline.object(forKey: "points")
                            let path = GMSPath.init(fromEncodedPath: points! as! String)
                            let polyline = GMSPolyline.init(path: path)
                            polyline.strokeWidth = 3
                            let bounds = GMSCoordinateBounds(path: path!)
                            self.mapView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 30.0))
                            polyline.map = self.mapView
                        }
                    })
                } catch let error as NSError {
                    println_debug("error:\(error)")
                }
            }
        }).resume()
    }
    
    //MARK: - Button Action Methods
    @IBAction func cancelRequestAction(_ sender : UIButton){
        let req = CancelCashTransferRequest(cancelledReason: "", requestID: "")
        modelCashInCashOut.cancelCashTransfer(request: req)
    }
    
    @IBAction func acceptRequestAction(_ sender : UIButton){
        let loc = self.getLocationDetailsForApi()
        let agentReq = AgentRequest(agentID: "", agentRequestStatus: 0, sentTime: "", pushReceived: true, smsSent: false, createdDate: "", modifiedDate: "", createdBy: "", id: "", transactionID: "")
        let acceptDetails = CashTransferAcceptDetails(agentRequests: [agentReq], requestDate: "", cashType: 0, rechargeDestination: loc, requesterID: "", status: 0, cancelledReason: "", rechargeAmount: 501, commisionPercent: 0, isDeleted: false, createdDate: "", modifiedDate: "", createdBy: "", id: "", transactionID: "")        
        let acceptReq = CashTransferAcceptRequest(cashTransferRequestID: "", cashTransferRequest: acceptDetails, agentID: "", agentLocation: loc, agentType: 0)
        modelCashInCashOut.acceptCashTransferRequest(request: acceptReq)
    }
    
    @IBAction func proceedAndPayRequestAction(_ sender : UIButton){
        if let confirmationvc = self.storyboard?.instantiateViewController(withIdentifier: cashInOutViewControllers.viewControllers.cioConfirmationVC) as? CIOConfimationViewController {
            self.navigationController?.pushViewController(confirmationvc, animated: true)
        }
    }
}


//MARK: - CLLocationManagerDelegate
extension CICOLocationTrackerViewcontroller: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status != .denied, status != .notDetermined, status != .restricted else {
            return
        }
        locationManager.startUpdatingLocation()
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 25.0, bearing: 0, viewingAngle: 0)
        locationManager.stopUpdatingLocation()
        fetchNearbyPlaces(coordinate: location.coordinate)
    }
}

// MARK: - GMSMapViewDelegate
extension CICOLocationTrackerViewcontroller: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        reverseGeocodeCoordinate(position.target)
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        addressLabel.lock()
        if (gesture) {
            mapCenterPinImage.fadeIn(0.25)
            mapView.selectedMarker = nil
        }
    }
    
//    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
//        guard let placeMarker = marker as? PlaceMarker else {
//            return nil
//        }
//        guard let infoView = UIView.viewFromNibName("MarkerInfoView") as? MarkerInfoView else {
//            return nil
//        }
//
//        infoView.nameLabel.text = placeMarker.place.name
//        if let photo = placeMarker.place.photo {
//            infoView.placePhoto.image = photo
//        } else {
//            infoView.placePhoto.image = UIImage(named: "generic")
//        }
//
//        return infoView
//    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        mapCenterPinImage.fadeOut(0.25)
        return false
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        mapCenterPinImage.fadeIn(0.25)
        mapView.selectedMarker = nil
        return false
    }
}

extension UIView {
    func lock() {
        if let _ = viewWithTag(10) {
            //View is already locked
        } else {
            let lockView = UIView(frame: bounds)
            lockView.backgroundColor = UIColor(white: 0.0, alpha: 0.75)
            lockView.tag = 10
            lockView.alpha = 0.0
            let activity = UIActivityIndicatorView(style: .white)
            activity.hidesWhenStopped = true
            activity.center = lockView.center
            lockView.addSubview(activity)
            activity.startAnimating()
            addSubview(lockView)
            
            UIView.animate(withDuration: 0.2, animations: {
                lockView.alpha = 1.0
            })
        }
    }
    
    func unlock() {
        if let lockView = viewWithTag(10) {
            UIView.animate(withDuration: 0.2, animations: {
                lockView.alpha = 0.0
            }, completion: { finished in
                lockView.removeFromSuperview()
            })
        }
    }
    
    func fadeOut(_ duration: TimeInterval) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        })
    }
    
    func fadeIn(_ duration: TimeInterval) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1.0
        })
    }
    
    class func viewFromNibName(_ name: String) -> UIView? {
        let views = Bundle.main.loadNibNamed(name, owner: nil, options: nil)
        return views?.first as? UIView
    }
}
