//
//  CIOConfimationViewController.swift
//  OK
//
//  Created by Kethan on 8/8/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
protocol CICOPopViewController : class {
    func popViewController()
}

class CIOConfimationViewController: CashInOutBaseViewController {
    //MARK: - Outlets
    @IBOutlet weak var accountNumberLabel: UILabel! {
        didSet {
            self.accountNumberLabel.text = self.accountNumberLabel.text?.localized
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            self.nameLabel.text = self.nameLabel.text?.localized
        }
    }
    @IBOutlet weak var payToNumberLabel: UILabel! {
        didSet {
            self.payToNumberLabel.text = self.payToNumberLabel.text?.localized
        }
    }
    @IBOutlet weak var agentNameLabel: UILabel! {
        didSet {
            self.agentNameLabel.text = self.agentNameLabel.text?.localized
        }
    }
    @IBOutlet weak var cashInAmountLabel: UILabel! {
        didSet {
            self.cashInAmountLabel.text = self.cashInAmountLabel.text?.localized
        }
    }
    @IBOutlet weak var serviceFeeLabel: UILabel! {
        didSet {
            self.serviceFeeLabel.text = self.serviceFeeLabel.text?.localized
        }
    }
    @IBOutlet weak var totalAmountLabel: UILabel! {
        didSet {
            self.totalAmountLabel.text = self.totalAmountLabel.text?.localized
        }
    }
    @IBOutlet weak var payButton: UIButton! {
        didSet {
            self.payButton.setTitle(self.payButton.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var accountNumberValue: UILabel!
    @IBOutlet weak var nameValue: UILabel!
    @IBOutlet weak var payToNumberValue: UILabel!
    @IBOutlet weak var agentNameValue: UILabel!
    @IBOutlet weak var cashInAmountValue: UILabel!
    @IBOutlet weak var serviceFeeValue: UILabel!
    @IBOutlet weak var totalAmountValue: UILabel!

    //MARK: - Properties
    private lazy var modelCashInCashOut: CashInCashOutModel = {
        return CashInCashOutModel()
    }()
    var currentSelectedCashDetails: CashTransferRequestStatus?
    var isPresented: Bool?
    weak var delegate: CICOPopViewController?
    //MARK: - Views life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addBackButton(navTitle: "Confirmation")
        modelCashInCashOut.cashInCashOutDelegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateView()
    }
    
    //MARK: - Methods
    func addBackButton(navTitle: String) {
        let backButton = UIButton(type: .custom)
        backButton.setBackgroundImage(#imageLiteral(resourceName: "tabBarBack"), for: .normal)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        backButton.addTarget(self, action: #selector(backActionCurrent), for: .touchUpInside)
        navigationItem.title = navTitle.localized
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white ]
        self.navigationController?.navigationBar.barTintColor = kYellowColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    @objc func backActionCurrent() {
        NotificationCenter.default.post(name: Notification.Name("ReloadCICOAllScreens"), object: nil, userInfo: nil)
        if let ispresent = isPresented, ispresent == true {
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: Notification.Name("CICOConfirmPop"), object: nil, userInfo: nil)
            }
        } else {
//            NotificationCenter.default.post(name: Notification.Name("ReloadCICOAllScreens"), object: nil, userInfo: nil)
//            if let navControllers = self.navigationController {
//                let viewControllers: [UIViewController] = navControllers.viewControllers
//                for aViewController in viewControllers {
//                    if aViewController is CIOTransactionHomeViewController {
//                        navControllers.popToViewController(aViewController, animated: true)
//                    }
//                }
//            }
            if let nav  = self.navigationController {
                nav.popViewController(animated: true)
                self.delegate?.popViewController()
            }
        }
    }
    
    fileprivate func updateView() {
        if currentSelectedCashDetails?.cashTransferMaster?.cashType ?? 0 == 0 {
            self.cashInAmountLabel.text = "Cash In Amount".localized
            self.accountNumberValue.text = currentSelectedCashDetails?.agent?.firstName ?? ""
            self.nameValue.text = wrapFormattedNumber(UserModel.shared.mobileNo)
            self.payToNumberValue.text = currentSelectedCashDetails?.customer?.firstName ?? ""
            self.agentNameValue.text = wrapFormattedNumber(currentSelectedCashDetails?.customer?.phoneNumber ?? "")
            self.cashInAmountValue.text = String(currentSelectedCashDetails?.cashTransferMaster?.cashWithOutCommission ?? 0)
            let serviceFee = Int(currentSelectedCashDetails?.cashTransferMaster?.commission ?? 0)
            self.serviceFeeValue.text = String(serviceFee)
            let totalAmount = Int(currentSelectedCashDetails?.cashTransferMaster?.totalAmount ?? 0)
            self.totalAmountValue.text = String(totalAmount)
        } else {
            self.cashInAmountLabel.text = "Cash Out Amount".localized
            self.accountNumberValue.text = currentSelectedCashDetails?.customer?.firstName ?? ""
            self.nameValue.text = wrapFormattedNumber(UserModel.shared.mobileNo)
            self.payToNumberValue.text = currentSelectedCashDetails?.agent?.firstName ?? ""
            self.agentNameValue.text = wrapFormattedNumber(currentSelectedCashDetails?.agent?.phoneNumber ?? "")
            self.cashInAmountValue.text = String(currentSelectedCashDetails?.cashTransferMaster?.cashWithOutCommission ?? 0)
            let serviceFee = Int(currentSelectedCashDetails?.cashTransferMaster?.commission ?? 0)
            self.serviceFeeValue.text = String(serviceFee)
            let totalAmount = Int(currentSelectedCashDetails?.cashTransferMaster?.totalAmount ?? 0)
            self.totalAmountValue.text = String(totalAmount)
        }
    }
    
    fileprivate func makeGenericPayment() {
        
        if let cashType = currentSelectedCashDetails?.cashTransferMaster?.cashType, cashType == 0 {
            guard let amountDouble = currentSelectedCashDetails?.cashTransferMaster?.cashWithOutCommission else { return }
            let trans = LTransactions(businessName: "", destinationNumberWalletBalance: "",
                                      isPromotionApplicable: "0", kickBackMsisdn: "",
                                      merchantName: UserModel.shared.name, promoCodeID: "",
                                      discountPayTo: 0.0, resultCode: 0.0, resultDescription: "",
                                      transactionID: currentSelectedCashDetails?.cashTransferMaster?.transactionID ?? "", transactionTime: "121", accounType: "",
                                      senderBusinessName: currentSelectedCashDetails?.customer?.businessName ?? "",
                                      userName: currentSelectedCashDetails?.customer?.firstName,
                                      amount: String(amountDouble), balance: "0.0", bonusPoint: 0.0, cashBackFlag: 0.0,
                                      comments: "Cash In", //"cash_in_out_\(currentSelectedCashDetails?.cashTransferMaster?.id ?? "")"
                destination: currentSelectedCashDetails?.customer?.phoneNumber,
                kickBack: "00", localTransactionType: "Payto", mobileNumber: UserModel.shared.mobileNo,
                password: ok_password, secureToken: UserLogin.shared.token, transactionType: "PAYTO")
            
            let genericReq = GenericPaymentRequest(lExternalReference: LExternalReference(),
                                                   lGeoLocation: LGeoLocation(),
                                                   lProximity: LProximity(), lTransactions: trans)
            modelCashInCashOut.genericPaymentApi(request: genericReq)
        } else {
            guard let amountDouble = currentSelectedCashDetails?.cashTransferMaster?.totalAmount else { return }
            let trans = LTransactions(businessName: "", destinationNumberWalletBalance: "",
                                      isPromotionApplicable: "0", kickBackMsisdn: "",
                                      merchantName: currentSelectedCashDetails?.agent?.firstName, promoCodeID: "",
                                      discountPayTo: 0.0, resultCode: 0.0, resultDescription: "",
                                      transactionID: currentSelectedCashDetails?.cashTransferMaster?.transactionID ?? "", transactionTime: "121", accounType: "",
                                      senderBusinessName: UserModel.shared.businessName,
                                      userName: UserModel.shared.name,
                                      amount: String(amountDouble), balance: "0.0", bonusPoint: 0.0, cashBackFlag: 0.0,
                                      comments: "Cash Out", //"cash_in_out_\(currentSelectedCashDetails?.cashTransferMaster?.id ?? "")"
                destination: currentSelectedCashDetails?.agent?.phoneNumber,
                kickBack: "00", localTransactionType: "Payto", mobileNumber: UserModel.shared.mobileNo,
                password: ok_password, secureToken: UserLogin.shared.token, transactionType: "PAYTO")
            
            let genericReq = GenericPaymentRequest(lExternalReference: LExternalReference(),
                                                   lGeoLocation: LGeoLocation(),
                                                   lProximity: LProximity(), lTransactions: trans)
            modelCashInCashOut.genericPaymentApi(request: genericReq)
        }
    }
    
    fileprivate func showReceiptScreen() {
        DispatchQueue.main.async {
            if let vc = UIStoryboard(name: "CICOMapAndPay", bundle: nil).instantiateViewController(withIdentifier: "CICOReceiptViewController_ID") as? CICOReceiptViewController {
                vc.currentSelectedCashDetails = self.currentSelectedCashDetails
                vc.genericPaymentResponse = self.modelCashInCashOut.genericPaymentResponse
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    private func callCompletePayment() {
        guard let modelGenResData = modelCashInCashOut.genericPaymentResponse else { return }
        guard let ctMasterID = currentSelectedCashDetails?.cashTransferMaster?.id else { return }
        let complPaymReq = CompletePaymentRequest(cashTransferMasterID: ctMasterID, cashWithOutCommission: 0.0,
                                                  okDollarTransactionID: String(modelGenResData.transId ?? 0) ,
                                                  rechargeAmount: currentSelectedCashDetails?.cashTransferMaster?.totalAmount ?? 0.0)
        modelCashInCashOut.completePaymentRequest(request: complPaymReq)
    }
    
    //MARK: - Button Action Methods
    @IBAction func payButtonAction(_ sender: UIButton) {
        OKPayment.main.authenticate(screenName: "PayToCICO", delegate: self)
    }
}

extension CIOConfimationViewController: BioMetricLoginDelegate, PTWebResponseDelegate{
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        println_debug("Authorised")
        if(isSuccessful) {
            if screen == "PayToCICO" {
                if appDelegate.checkNetworkAvail() {
                    self.makeGenericPayment()
                }
            }
        }
    }
    
    func webFailureResult(screen: String) {
        
    }
    
    func webSuccessResult(data: Any, screen: String) {
     
    }
}

// MARK: - CashInOutModel Delegate
extension CIOConfimationViewController: CashInOutModelDelegate {
    func apiResult(message: String?, statusCode: String?, type: CashInCashOutModel.CashInCashOutApiType) {
        switch type {
        case .genericPayment:
            self.callCompletePayment()
        case .completePayment:
            self.showReceiptScreen()
        default:
            break
        }
    }
    
    func handleCustomError(error: CustomError, type: CashInCashOutModel.CashInCashOutApiType) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}
