//
//  CashInCashOutModel.swift
//  OK
//
//  Created by E J ANTONY on 11/08/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation

/// This protocol is to implement the api response action
protocol CashInOutModelDelegate: class {
    func apiResult(message: String?, statusCode: String?, type: CashInCashOutModel.CashInCashOutApiType)
    func handleCustomError(error: CustomError, type: CashInCashOutModel.CashInCashOutApiType)
}

class CashInCashOutModel {
    // MARK: - Properties
    var cashInCashOutDelegate: CashInOutModelDelegate?
    var agentsList: [AvailableAgentsList] = []
    var acceptedList: [AcceptedCashTransferList] = []
    var pendingList: [PendingCashTransferResponse] = []
    var successList: [AcceptedCashTransferList] = []
    var allCashList: [AcceptedCashTransferList] = []
    var agentUserId: String?
    var bookCashTransferId: String?
    var cashTransferReqStatusCode: Int?
    var cashTransferReqStatus: Int?
    var cashTransferRequestDetail: CashTransferRequestDetail?
    var masterDetail: CashTransferRequestStatus?
    var genericPaymentResponse: CICOPaymentResponse?
    var userLocBeforeStartRide: CICOLocation?
    var userLocAfterStartRide: CICOLocation?
    var getTimeAndDistance: GetDistTimeBtwnLOCResponse?
    
    enum CashInCashOutApiType: String {
        case getAuthToken           = "Get Auth Token", getAgentList = "Get Agent List", updateUserLoc = "Update User Location"
        case acceptedCashTransfer   = "Accepted Cash Transfer List"
        case pendingTransferRequest = "Pending Transfer Request"
        case successCashTransfer    = "Success Cash Transfer"
        case allCashTransfer        = "All Cash Transfer"
        case updateUser             = "Update User"
        case bookCashTransfer       = "Book Cash Transfer"
        case cashTransferReqStatus  = "Cash Transfer Request Status"
        case pushReceived           = "Push Received"
        case getRequestDetails      = "Get Cash Transfer Request Details"
        case acceptCashTransferReq  = "Accept Cash Transfer"
        case rejectCashTransferReq  = "Reject Cash Transfer"
        case getMasterDetails       = "Get Cash Transfer Master Details"
        case updateUserLocTravel    = "Update User Location While Travel"
        case cancelCashTransfer     = "Cancel Cash Transfer"
        case startTrip              = "Start Trip"
        case endTrip                = "End Trip"
        case genericPayment         = "Generic Payment"
        case rateCusOrAgent         = "Rate Customer or Agent"
        case completePayment        = "Complete Payment"
        case createUser             = "Create User"
        case getDistTimeBtwnLoc     = "Distance Time between two location"
    }
    
    //MARK: Update authToken
    func updateAuthToken(fcmToken : String) {
        guard let _ = UserDefaults.standard.string(forKey: "CICOAUTHTOKEN") else  {
            let req = AuthTokenCICORequest(deviceID: fcmToken, userName: UserModel.shared.mobileNo)
            self.getAuthtokenForCICO(request: req)
            return
        }
        println_debug("****CICO Authtoken: \(UserDefaults.standard.string(forKey: "CICOAUTHTOKEN") ?? "")")
    }
    
    //MARK: - Get authtoken
    func getAuthtokenForCICO(request: AuthTokenCICORequest) {
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegateWithAllResponse = self
            
            let urlStrSuffix   = Url.authServiceLogin
            let getAuthTokenUrl = getUrl(urlStr: urlStrSuffix, serverType: .cashInCashOutUrl)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(getAuthTokenUrl)")
            
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: getAuthTokenUrl, param: params , httpMethod: "POST", mScreen: CashInCashOutApiType.getAuthToken.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            UserDefaults.standard.removeObject(forKey: "CICOAUTHTOKEN")
            cashInCashOutDelegate?.handleCustomError(error: CustomError.noInternet, type: .getAuthToken)
        }
    }
    
    private func handleAuthTokenResponse(data: Data) {
        do {
            let decoder = JSONDecoder()
            let agentList = try decoder.decode(AuthTokenCICOResponse.self, from: data)
            PTLoader.shared.hide()
            if agentList.statusCode == 200 {
                DispatchQueue.main.async {
                    self.cashInCashOutDelegate?.apiResult(message: "", statusCode: nil, type: .getAuthToken)
                }
                
            }
        } catch let jsonError {
            println_debug(jsonError)
            cashInCashOutDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed, type: .getAuthToken)
            PTLoader.shared.hide()
        }
    }
    
    //MARK: - Near by merchant agents
    func getNearByMerchants(request: AvailableAgenListRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.agentsList
            let getAgentListUrl = getUrl(urlStr: urlStrSuffix, serverType: .cashInCashOutUrl)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(getAgentListUrl)")
            
            let encoder = JSONEncoder()
            do {
                guard let authToken = UserDefaults.standard.string(forKey: "CICOAUTHTOKEN") else {
                    PTLoader.shared.hide()
                    return
                }
                let jsonData = try encoder.encode(request)
                web.genericClass(url: getAgentListUrl, param: params , httpMethod: "POST", mScreen: CashInCashOutApiType.getAgentList.rawValue, hbData: jsonData, authToken: authToken, authTokenKey: "authToken")
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.noInternet, type: .getAgentList)
        }
    }
    
    private func handleAgentListResponse(data: AnyObject, type: CashInCashOutApiType) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let agentList = try decoder.decode(AvailableAgentsListDTO.self, from: castedData)
            if let responseList = agentList.content {
                agentsList = responseList
                
                
                DispatchQueue.main.async {
                    self.cashInCashOutDelegate?.apiResult(message: "", statusCode: nil, type: type)
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch _ {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed, type: type)
            PTLoader.shared.hide()
        }
    }
    
    //MARK: - Update user location
    func updateUserLocation(request: UpdateUserLocReq) {
        if appDelegate.checkNetworkAvail() {
            //PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.updateUserLocation
            let updateUserLocUrl = getUrl(urlStr: urlStrSuffix, serverType: .cashInCashOutUrl)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(updateUserLocUrl)")
            
            let encoder = JSONEncoder()
            do {
                guard let authToken = UserDefaults.standard.string(forKey: "CICOAUTHTOKEN") else {
                    //PTLoader.shared.hide()
                    return
                }
                let jsonData = try encoder.encode(request)
                web.genericClass(url: updateUserLocUrl, param: params , httpMethod: "POST", mScreen: CashInCashOutApiType.updateUserLoc.rawValue, hbData: jsonData, authToken: authToken, authTokenKey: "authToken")
            } catch {
                //PTLoader.shared.hide()
            }
        } else {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.noInternet, type: .updateUserLoc)
        }
    }
    
    private func handleUpdateUserLocResponse(data: AnyObject, type: CashInCashOutApiType) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let agentList = try decoder.decode(UpdateUserLocResponse.self, from: castedData)
            println_debug("Agent Location Update")
            println_debug(agentList)
            if let userLocData = agentList.userLocationData {
                userLocBeforeStartRide = userLocData.locationInfo
                DispatchQueue.main.async {
                    PTLoader.shared.hide()
                    self.cashInCashOutDelegate?.apiResult(message: "", statusCode: nil, type: type)
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch _ {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed, type: type)
            PTLoader.shared.hide()
        }
    }
    
    //MARK: - Accepeted Cash Transfer List
    func getAcceptedCashTransferList() {
        self.transactionList(type: .acceptedCashTransfer)
    }
    
    //MARK: - Pending Cash Transfer List
    func getPendingCashTransferList() {
        self.transactionList(type: .pendingTransferRequest)
    }
    
    //MARK: - Success Cash Transfer List
    func getSuccessCashTransferList() {
        self.transactionList(type: .successCashTransfer)
    }
    
    //MARK: - All Cash Transfer List
    func getAllCashTransferList() {
        self.transactionList(type: .allCashTransfer)
    }
    
    private func transactionList(type: CashInCashOutApiType) {
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self
            guard let urlRequest = getTransactionUrl(for: type) else { return }
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(urlRequest)")
            guard let authToken = UserDefaults.standard.string(forKey: "CICOAUTHTOKEN") else {
                PTLoader.shared.hide()
                return }
            web.genericClass(url: urlRequest, param: params , httpMethod: "GET", mScreen: type.rawValue, authToken: authToken, authTokenKey: "authToken")
        } else {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.noInternet, type: type)
        }
    }
    
    private func getTransactionUrl(for type: CashInCashOutApiType) -> URL? {
        var urlStrSuffix = ""
        switch type {
        case .acceptedCashTransfer:
            urlStrSuffix   = Url.acceptedCashTransferList
        case .pendingTransferRequest:
            urlStrSuffix   = Url.pendingCashTransferList
        case .successCashTransfer:
            urlStrSuffix   = Url.successCashTransferList
        case .allCashTransfer:
            urlStrSuffix   = Url.allCashTransferList
        default:
            return nil
        }
        return getUrl(urlStr: urlStrSuffix, serverType: .cashInCashOutUrl)
    }
    
    private func handleCashTransferResponse(data: AnyObject, type: CashInCashOutApiType) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let transferList = try decoder.decode(AcceptedCashTransferResponse.self, from: castedData)
            if let responseList = transferList.content {
                switch type {
                case .acceptedCashTransfer:
                    //println_debug(responseList)
                acceptedList = responseList
                case .successCashTransfer:
                    //println_debug(responseList)
                successList = responseList
                case .allCashTransfer:
                    //println_debug(responseList)
                allCashList = responseList
                default:
                    break
                }
                DispatchQueue.main.async {
                    self.cashInCashOutDelegate?.apiResult(message: "", statusCode: nil, type: type)
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch let decodeError {
            println_debug(decodeError.localizedDescription)
            cashInCashOutDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed, type: type)
            PTLoader.shared.hide()
        }
    }
    
    private func handlePendingCashTransferResponse(data: AnyObject, type: CashInCashOutApiType) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let transferList = try decoder.decode(PendingResponseDTO.self, from: castedData)
            if let responseList = transferList.content {
                switch type {
                case .pendingTransferRequest:
                    //println_debug(responseList)
                    pendingList = responseList
                default:
                    break
                }
                DispatchQueue.main.async {
                    PTLoader.shared.hide()
                    self.cashInCashOutDelegate?.apiResult(message: "", statusCode: nil, type: type)
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch let decodeError {
            println_debug(decodeError.localizedDescription)
            cashInCashOutDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed, type: type)
            PTLoader.shared.hide()
        }
    }
    
    //MARK: - Update User
    func updateUser(request: UpdateUserRequest, showIndicator: Bool = true) {
        if appDelegate.checkNetworkAvail() {
            if showIndicator {
                PTLoader.shared.show()
            }
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.updateUser
            let updateUserUrl = getUrl(urlStr: urlStrSuffix, serverType: .cashInCashOutUrl)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(updateUserUrl)")
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: updateUserUrl, param: params , httpMethod: "POST", mScreen: CashInCashOutApiType.updateUser.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.noInternet, type: .updateUser)
        }
    }
    
    private func handleUpdateUserResponse(data: AnyObject, type: CashInCashOutApiType) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let updateUserResponse = try decoder.decode(AuthTokenCICOResponse.self, from: castedData)
            PTLoader.shared.hide()
            if updateUserResponse.statusCode == 200 {
                //if let updatedUserData = updateUserResponse.content {
                //}
                UserDefaults.standard.set(updateUserResponse.content?.id, forKey: "CICOUserID")
                DispatchQueue.main.async {
                    self.cashInCashOutDelegate?.apiResult(message: "", statusCode: nil, type: type)
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch _ {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed, type: type)
            PTLoader.shared.hide()
        }
    }
    
    //MARK: - Book Cash Transfer
    func bookCashTransfer(request: BookCashTransferRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.bookCashTransfer
            let bookCashTransferUrl = getUrl(urlStr: urlStrSuffix, serverType: .cashInCashOutUrl)
            let params = [String: String]() as AnyObject
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                guard let authToken = UserDefaults.standard.string(forKey: "CICOAUTHTOKEN") else {
                    PTLoader.shared.hide()
                    return }
                println_debug("REQUEST URL: \(bookCashTransferUrl)")
                println_debug("REQUEST - \(request)")
                println_debug("AuthToken - \(authToken)")
                web.genericClass(url: bookCashTransferUrl, param: params , httpMethod: "POST", mScreen: CashInCashOutApiType.bookCashTransfer.rawValue, hbData: jsonData, authToken: authToken, authTokenKey: "authToken")
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.noInternet, type: .bookCashTransfer)
        }
    }
    
    private func handleBookCashTransferResponse(data: AnyObject, type: CashInCashOutApiType) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let response = try decoder.decode(BookCashTransferResponse.self, from: castedData)
            PTLoader.shared.hide()
            println_debug(response.content?.id)
            self.agentUserId = response.content?.id
            self.bookCashTransferId = response.content?.id
            println_debug(type.rawValue)
            println_debug(response)
            DispatchQueue.main.async {
                self.cashInCashOutDelegate?.apiResult(message: "", statusCode: String(response.statusCode ?? 0), type: type)
            }
        } catch _ {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed, type: type)
            PTLoader.shared.hide()
        }
    }
    
    //MARK: - Cash Transfer Request Status
    func cashTransferRequestStatus(id: String) {
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.cashTransferReqStatus
            let cashTransferReqStatusUrl = getUrl(urlStr: urlStrSuffix, serverType: .cashInCashOutUrl)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(cashTransferReqStatusUrl)")
            guard let authToken = UserDefaults.standard.string(forKey: "CICOAUTHTOKEN") else {
                //PTLoader.shared.hide()
                return
            }
            let idStr = "\"\(id)\""
            if let bodyData = idStr.data(using: .utf8, allowLossyConversion: true) {
                web.genericClass(url: cashTransferReqStatusUrl, param: params , httpMethod: "POST", mScreen: CashInCashOutApiType.cashTransferReqStatus.rawValue, hbData: bodyData, authToken: authToken, authTokenKey: "authToken")
            }
        } else {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.noInternet, type: .cashTransferReqStatus)
        }
    }
    
    private func handleCashTransferReqStatus(data: AnyObject, type: CashInCashOutApiType) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let response = try decoder.decode(CashTransferRequestStatusResponse.self, from: castedData)
            PTLoader.shared.hide()
            println_debug(response)
            cashTransferReqStatusCode = response.statusCode ?? -2
            cashTransferReqStatus = response.status ?? -2
            println_debug("Request status api")
            println_debug(response)
            DispatchQueue.main.async {
                self.cashInCashOutDelegate?.apiResult(message: "", statusCode: nil, type: type)
            }
        } catch _ {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed, type: type)
            PTLoader.shared.hide()
        }
    }
    
    //MARK: - Push Received Acknowledge
    func acknowledgePushReceived(request: PushReceivedRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.pushReceived
            let pushReceivedUrl = getUrl(urlStr: urlStrSuffix, serverType: .cashInCashOutUrl)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(pushReceivedUrl)")
            guard let authToken = UserDefaults.standard.string(forKey: "CICOAUTHTOKEN") else {
                PTLoader.shared.hide()
                return
            }
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: pushReceivedUrl, param: params , httpMethod: "POST", mScreen: CashInCashOutApiType.pushReceived.rawValue, hbData: jsonData, authToken: authToken, authTokenKey: "authToken")
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.noInternet, type: .pushReceived)
        }
    }
    
    private func handlePushReceived(data: AnyObject, type: CashInCashOutApiType) {
        PTLoader.shared.hide()
        guard let castedData = data as? Data else {
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let response = try decoder.decode(PushReceivedResponse.self, from: castedData)
            PTLoader.shared.hide()
            println_debug(response)
            DispatchQueue.main.async {
                self.cashInCashOutDelegate?.apiResult(message: "", statusCode: nil, type: type)
            }
        } catch _ {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed, type: type)
            PTLoader.shared.hide()
        }
    }
    
    //MARK: - Get Cash Transfer Request Detail
    func getRequestDetail(id: String) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.getCashTransferRequestDetails
            let getRequestDetailsUrl = getUrl(urlStr: urlStrSuffix, serverType: .cashInCashOutUrl)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(getRequestDetailsUrl)")
            guard let authToken = UserDefaults.standard.string(forKey: "CICOAUTHTOKEN") else {
                PTLoader.shared.hide()
                return
            }
            println_debug("ID - \(id)")
            println_debug("AuthToken - \(authToken)")
            let bodyData = Data(id.utf8)
            web.genericClass(url: getRequestDetailsUrl, param: params , httpMethod: "POST", mScreen: CashInCashOutApiType.getRequestDetails.rawValue, hbData: bodyData, authToken: authToken, authTokenKey: "authToken")
        } else {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.noInternet, type: .getRequestDetails)
        }
    }
    
    private func handleRequestDetails(data: AnyObject, type: CashInCashOutApiType) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let response = try decoder.decode(CashTransferRequestDetailResponse.self, from: castedData)
            PTLoader.shared.hide()
            self.cashTransferRequestDetail = response.content
            self.cashInCashOutDelegate?.apiResult(message: "", statusCode: nil, type: type)
        } catch _ {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed, type: type)
            PTLoader.shared.hide()
        }
    }
    
    //MARK: - Accept Cash Transfer
    func acceptCashTransferRequest(request: CashTransferAcceptRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.cashTrasferAccept
            let acceptCashTransferReqUrl = getUrl(urlStr: urlStrSuffix, serverType: .cashInCashOutUrl)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(acceptCashTransferReqUrl)")
            println_debug("REQUEST: \(request)")
            guard let authToken = UserDefaults.standard.string(forKey: "CICOAUTHTOKEN") else {
                PTLoader.shared.hide()
                return
            }
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: acceptCashTransferReqUrl, param: params , httpMethod: "POST", mScreen: CashInCashOutApiType.acceptCashTransferReq.rawValue, hbData: jsonData, authToken: authToken, authTokenKey: "authToken")
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.noInternet, type: .acceptCashTransferReq)
        }
    }
    
    private func handleAcceptCashTransferRes(data: AnyObject, type: CashInCashOutApiType) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let response = try decoder.decode(CashTransferAcceptResponse.self, from: castedData)
            PTLoader.shared.hide()
            println_debug(response)
            self.cashInCashOutDelegate?.apiResult(message: "", statusCode: nil, type: type)
        } catch let error {
            println_debug(error.localizedDescription)
            cashInCashOutDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed, type: type)
            PTLoader.shared.hide()
        }
    }
    
    //MARK: - Reject Cash Transfer
    func rejectCashTransferRequest(request: CashTransferRejectRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.cashTransferReject
            let rejectCashTransferReqUrl = getUrl(urlStr: urlStrSuffix, serverType: .cashInCashOutUrl)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(rejectCashTransferReqUrl)")
            guard let authToken = UserDefaults.standard.string(forKey: "CICOAUTHTOKEN") else {
                PTLoader.shared.hide()
                return
            }
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: rejectCashTransferReqUrl, param: params , httpMethod: "POST", mScreen: CashInCashOutApiType.rejectCashTransferReq.rawValue, hbData: jsonData, authToken: authToken, authTokenKey: "authToken")
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.noInternet, type: .rejectCashTransferReq)
        }
    }
    
    private func handleRejectCashTransferRes(data: AnyObject, type: CashInCashOutApiType) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let response = try decoder.decode(CashTransferRejectResponse.self, from: castedData)
            PTLoader.shared.hide()
            println_debug(response)
            self.cashInCashOutDelegate?.apiResult(message: "", statusCode: nil, type: type)
        } catch let error {
            println_debug(error.localizedDescription)
            cashInCashOutDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed, type: type)
            PTLoader.shared.hide()
        }
    }
    
    //MARK: - Get Master Details
    func getMasterDetailRequest(id: String) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.getMasterDetail
            let getMasterDetailUrl = getUrl(urlStr: urlStrSuffix, serverType: .cashInCashOutUrl)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(getMasterDetailUrl)")
            guard let authToken = UserDefaults.standard.string(forKey: "CICOAUTHTOKEN") else {
                PTLoader.shared.hide()
                return
            }
            let bodyData = Data(id.utf8)
            web.genericClass(url: getMasterDetailUrl, param: params , httpMethod: "POST", mScreen: CashInCashOutApiType.getMasterDetails.rawValue, hbData: bodyData, authToken: authToken, authTokenKey: "authToken")
        } else {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.noInternet, type: .getMasterDetails)
        }
    }
    
    private func handleMasterDetailRes(data: AnyObject, type: CashInCashOutApiType) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let response = try decoder.decode(CashTransferRequestStatusResponse.self, from: castedData)
            PTLoader.shared.hide()
            masterDetail = response.content
            self.cashInCashOutDelegate?.apiResult(message: "", statusCode: nil, type: type)
        } catch _ {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed, type: type)
            PTLoader.shared.hide()
        }
    }
    
    //MARK: - Update Location While Travel
    func updateLocationWhileTravel(request: UpdateLOCWhileTravelRequest) {
        if appDelegate.checkNetworkAvail() {
            //PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.updateUserLocWhileTravel
            let updateUserLocTravelUrl = getUrl(urlStr: urlStrSuffix, serverType: .cashInCashOutUrl)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(updateUserLocTravelUrl)")
            guard let authToken = UserDefaults.standard.string(forKey: "CICOAUTHTOKEN") else {
                //PTLoader.shared.hide()
                return
            }
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: updateUserLocTravelUrl, param: params , httpMethod: "POST", mScreen: CashInCashOutApiType.updateUserLocTravel.rawValue, hbData: jsonData, authToken: authToken, authTokenKey: "authToken")
            } catch {
                //PTLoader.shared.hide()
            }
        } else {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.noInternet, type: .updateUserLocTravel)
        }
    }
    
    private func handleUpdateLocTravel(data: AnyObject, type: CashInCashOutApiType) {
        guard let castedData = data as? Data else {
            //PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let response = try decoder.decode(AuthTokenCICOResponse.self, from: castedData)
            //PTLoader.shared.hide()
            userLocAfterStartRide = response.content?.currentLocation
            self.cashInCashOutDelegate?.apiResult(message: "", statusCode: nil, type: type)
        } catch _ {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed, type: type)
            //PTLoader.shared.hide()
        }
    }
    
    //MARK: - CancelCashTransfer
    func cancelCashTransfer(request: CancelCashTransferRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.cancelCashTransfer
            let cancelCashTransferUrl = getUrl(urlStr: urlStrSuffix, serverType: .cashInCashOutUrl)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(cancelCashTransferUrl)")
            guard let authToken = UserDefaults.standard.string(forKey: "CICOAUTHTOKEN") else {
                PTLoader.shared.hide()
                return
            }
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: cancelCashTransferUrl, param: params , httpMethod: "POST", mScreen: CashInCashOutApiType.cancelCashTransfer.rawValue, hbData: jsonData, authToken: authToken, authTokenKey: "authToken")
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.noInternet, type: .cancelCashTransfer)
        }
    }
    
    private func handleCancelCashTransfer(data: AnyObject, type: CashInCashOutApiType) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let response = try decoder.decode(CancelCashTransferResponse.self, from: castedData)
            PTLoader.shared.hide()
            println_debug(response)
            self.cashInCashOutDelegate?.apiResult(message: "", statusCode: nil, type: type)
        } catch _ {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed, type: type)
            PTLoader.shared.hide()
        }
    }
    
    //MARK: - Start Trip
    func startTripRequest(request: StartTripRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.startTrip
            let startTripUrl = getUrl(urlStr: urlStrSuffix, serverType: .cashInCashOutUrl)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(startTripUrl)")
            guard let authToken = UserDefaults.standard.string(forKey: "CICOAUTHTOKEN") else {
                PTLoader.shared.hide()
                return
            }
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: startTripUrl, param: params , httpMethod: "POST", mScreen: CashInCashOutApiType.startTrip.rawValue, hbData: jsonData, authToken: authToken, authTokenKey: "authToken")
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.noInternet, type: .startTrip)
        }
    }
    
    private func handleStartEndTripResponse(data: AnyObject, type: CashInCashOutApiType) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let response = try decoder.decode(StartEndTripResponse.self, from: castedData)
            PTLoader.shared.hide()
            println_debug(response)
            self.cashInCashOutDelegate?.apiResult(message: "", statusCode: nil, type: type)
        } catch _ {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed, type: type)
            PTLoader.shared.hide()
        }
    }
    
    //MARK: - End Trip
    func endTripRequest(request: EndTripRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.endTrip
            let endTripUrl = getUrl(urlStr: urlStrSuffix, serverType: .cashInCashOutUrl)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(endTripUrl)")
            guard let authToken = UserDefaults.standard.string(forKey: "CICOAUTHTOKEN") else {
                PTLoader.shared.hide()
                return
            }
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: endTripUrl, param: params , httpMethod: "POST", mScreen: CashInCashOutApiType.endTrip.rawValue, hbData: jsonData, authToken: authToken, authTokenKey: "authToken")
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.noInternet, type: .endTrip)
        }
    }
    
    //MARK: - Generic Payment
    func genericPaymentApi(request: GenericPaymentRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.genericPaymentCashinout
            let genericPaymentUrl = getUrl(urlStr: urlStrSuffix, serverType: .serverApp)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(genericPaymentUrl)")
            println_debug("REQUEST URL: \(request)")
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: genericPaymentUrl, param: params , httpMethod: "POST", mScreen: CashInCashOutApiType.genericPayment.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.noInternet, type: .genericPayment)
        }
    }
    
    private func handleGenericPaymentResponse(data: AnyObject, type: CashInCashOutApiType) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let response = try decoder.decode(GenericPaymentResponse.self, from: castedData)
            println_debug(response)
            guard let xmlStr = response.data else {
                PTLoader.shared.hide()
                return }
            let xmlIndexer = SWXMLHash.parse(xmlStr)
            do {
                let finalResponse = try GenericPaymentXMLResponse.deserialize(xmlIndexer)
                println_debug(finalResponse.estel?.response)
                genericPaymentResponse = finalResponse.estel?.response
                cashInCashOutDelegate?.apiResult(message: response.msg, statusCode: String(response.code ?? 0), type: type)
            } catch {
                cashInCashOutDelegate?.handleCustomError(error: CustomError.xmlSerializeFailed, type: type)
            }
        } catch _ {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed, type: type)
            PTLoader.shared.hide()
        }
    }
    
    //MARK: - Rating Customer or Agent
    func rateCustomerOrAgentRequest(request: RateCustomerOrAgentRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.rateCustomerOrAgent
            let rateCustomerOrAgentUrl = getUrl(urlStr: urlStrSuffix, serverType: .cashInCashOutUrl)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(rateCustomerOrAgentUrl)")
            guard let authToken = UserDefaults.standard.string(forKey: "CICOAUTHTOKEN") else {
                PTLoader.shared.hide()
                return
            }
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: rateCustomerOrAgentUrl, param: params , httpMethod: "POST", mScreen: CashInCashOutApiType.rateCusOrAgent.rawValue, hbData: jsonData, authToken: authToken, authTokenKey: "authToken")
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.noInternet, type: .rateCusOrAgent)
        }
    }
    
    private func handleRatingResponse(data: AnyObject, type: CashInCashOutApiType) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let response = try decoder.decode(RateCustomerOrAgentResponse.self, from: castedData)
            PTLoader.shared.hide()
            println_debug(response)
            self.cashInCashOutDelegate?.apiResult(message: "", statusCode: nil, type: type)
        } catch _ {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed, type: type)
            PTLoader.shared.hide()
        }
    }
    
    //MARK: - Complete Payment
    func completePaymentRequest(request: CompletePaymentRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.completePayment
            let completePaymentUrl = getUrl(urlStr: urlStrSuffix, serverType: .cashInCashOutUrl)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(completePaymentUrl)")
            guard let authToken = UserDefaults.standard.string(forKey: "CICOAUTHTOKEN") else {
                PTLoader.shared.hide()
                return
            }
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: completePaymentUrl, param: params , httpMethod: "POST", mScreen: CashInCashOutApiType.completePayment.rawValue, hbData: jsonData, authToken: authToken, authTokenKey: "authToken")
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.noInternet, type: .completePayment)
        }
    }
    
    private func handleCompletePaymentResponse(data: AnyObject, type: CashInCashOutApiType) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let response = try decoder.decode(CompletePaymentResponse.self, from: castedData)
            PTLoader.shared.hide()
            println_debug(response)
            self.cashInCashOutDelegate?.apiResult(message: "", statusCode: nil, type: type)
        } catch _ {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed, type: type)
            PTLoader.shared.hide()
        }
    }
    
    //MARK: - Create User
    func createUserRequest(request: CreateUserRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.createUser
            let createUserUrl = getUrl(urlStr: urlStrSuffix, serverType: .cashInCashOutUrl)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(createUserUrl)")
            
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: createUserUrl, param: params , httpMethod: "POST", mScreen: CashInCashOutApiType.createUser.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.noInternet, type: .createUser)
        }
    }
    
    private func handleCreateUserResponse(data: AnyObject, type: CashInCashOutApiType) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let response = try decoder.decode(CreateUserResponse.self, from: castedData)
            PTLoader.shared.hide()
            println_debug(response)
            self.cashInCashOutDelegate?.apiResult(message: "", statusCode: nil, type: type)
            let request = AuthTokenCICORequest(deviceID: (UserDefaults.standard.string(forKey: "FCMToken") ?? ""), userName: UserModel.shared.mobileNo)
            self.getAuthtokenForCICO(request: request)
        } catch _ {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed, type: type)
            PTLoader.shared.hide()
        }
    }
    
    func getDistanceAndTime(startLat: Double, startLong: Double, endLat: Double, endLong: Double) {
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self
            let urlStr = "https://maps.googleapis.com/maps/api/directions/json?origin=\(startLat),\(startLong)&destination=\(endLat),\(endLong)&sensor=true&key=\(googleAPIKey)"
            guard let urlReq   = URL(string: urlStr) else { return }
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(urlReq)")
            web.genericClass(url: urlReq, param: params , httpMethod: "GET", mScreen: CashInCashOutApiType.getDistTimeBtwnLoc.rawValue)
        } else {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.noInternet, type: .getDistTimeBtwnLoc)
        }
    }
    
    private func handleDistanceTimeResponse(data: AnyObject, type: CashInCashOutApiType) {
        guard let castedData = data as? Data else {
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let response = try decoder.decode(GetDistTimeBtwnLOCResponse.self, from: castedData)
            getTimeAndDistance = response
            println_debug(response)
            self.cashInCashOutDelegate?.apiResult(message: "", statusCode: nil, type: type)
        } catch _ {
            cashInCashOutDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed, type: type)
        }
    }
}

//MARK: - WebService Response1 Delegate
extension CashInCashOutModel: WebServiceResponseDelegate {
    func webResponse(withJson json: AnyObject, screen: String) {
        guard let screen = CashInCashOutApiType(rawValue: screen) else { return }
        switch screen {
        case .getAgentList:
            handleAgentListResponse(data: json, type: screen)
        case .updateUserLoc:
            handleUpdateUserLocResponse(data: json, type: screen)
        case .acceptedCashTransfer, .successCashTransfer, .allCashTransfer:
            handleCashTransferResponse(data: json, type: screen)
        case .pendingTransferRequest:
            handlePendingCashTransferResponse(data: json, type: screen)
        case .updateUser:
            handleUpdateUserResponse(data: json, type: screen)
            println_debug("Executed")
        case .bookCashTransfer:
            handleBookCashTransferResponse(data: json, type: screen)
        case .cashTransferReqStatus:
            handleCashTransferReqStatus(data: json, type: screen)
        case .pushReceived:
            handlePushReceived(data: json, type: screen)
        case .getRequestDetails:
            handleRequestDetails(data: json, type: screen)
        case .acceptCashTransferReq:
            handleAcceptCashTransferRes(data: json, type: screen)
        case .rejectCashTransferReq:
            handleRejectCashTransferRes(data: json, type: screen)
        case .getMasterDetails:
            handleMasterDetailRes(data: json, type: screen)
        case .updateUserLocTravel:
            handleUpdateLocTravel(data: json, type: screen)
        case .cancelCashTransfer:
            handleCancelCashTransfer(data: json, type: screen)
        case .startTrip, .endTrip:
            handleStartEndTripResponse(data: json, type: screen)
        case .genericPayment:
            handleGenericPaymentResponse(data: json, type: screen)
        case .rateCusOrAgent:
            handleRatingResponse(data: json, type: screen)
        case .completePayment:
            handleCompletePaymentResponse(data: json, type: screen)
        case .createUser:
            handleCreateUserResponse(data: json, type: screen)
        case .getDistTimeBtwnLoc:
            handleDistanceTimeResponse(data: json, type: screen)
        default:
            break
        }
    }
    
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        PTLoader.shared.hide()
        guard let screen = CashInCashOutApiType(rawValue: screenName) else { return }
        switch withErrorCode {
        case 404:
            cashInCashOutDelegate?.handleCustomError(error: .serverNotReachable, type: screen)
        default:
            println_debug("Error \(withErrorCode)")
            break
        }
    }
}

//MARK: - WebService Response2 Delegate
extension CashInCashOutModel: WebServiceDataWithResponseDelegate {
    func webResponse(withJson json: Data?, response: URLResponse?, error: Error?, screen: String) {
        if let errorData = error {
            println_debug(errorData.localizedDescription)
        } else if let jsonDataValue = json {
            if let res = response as? HTTPURLResponse {
                if let authToken = res.allHeaderFields["AuthToken"] {
                    UserDefaults.standard.set(authToken, forKey: "CICOAUTHTOKEN")
                    let aToken = UserDefaults.standard.string(forKey: "CICOAUTHTOKEN")
                    println_debug("****CICO Authtoken: \(aToken ?? "Auth Failed")")
                } else {
                    if let jsonDcit = try? JSONSerialization.jsonObject(with: jsonDataValue, options: []) as? Dictionary<String, Any> {
                        println_debug(jsonDcit)
                        if (jsonDcit?.safeValueForKey("Content") as? String ?? "") == "InvalidUserName",  (jsonDcit?.safeValueForKey("StatusCode") as? Int ?? 0) == 401 {
                            formRequestForCreateUser()
                        }
                    }
                }
                handleAuthTokenResponse(data: jsonDataValue)
            }
        }
    }
    
    func formRequestForCreateUser() {
        let addressArray = currentAddressGlobal?.components(separatedBy: ",")
        var add = ""
        if (addressArray?.count)! > 0 {
            add = addressArray![0]
        }
        let loc = CICOLocation(type: "POINT", coordinates: [Double(geoLocManager.currentLongitude ?? "0.0")!,Double(geoLocManager.currentLatitude ?? "0.0")!], angle: nil, time: nil, createdTime: nil, accuracy: 0, name: add, source: currentAddressGlobal ?? "", latitude: Double(geoLocManager.currentLatitude ?? "0.0"), longitude: Double(geoLocManager.currentLongitude ?? "0.0")!)
        let createUserRequest = CreateUserRequest(firstName: UserModel.shared.name, lastName: "", countryCode: (UserModel.shared.countryCode).replacingOccurrences(of: "+", with: ""), phoneNumber: UserModel.shared.mobileNo, email: UserModel.shared.email, address: UserModel.shared.address1, type: 0, gender: (UserModel.shared.gender == "Male") ? 0 : 1, currentLocation: loc, businessLocation: loc, homeLocation: loc, userName: UserModel.shared.mobileNo, password: "", cityName: UserModel.shared.street, businessName: UserModel.shared.businessName, country: UserModel.shared.country, divisionName: UserModel.shared.township, line1: UserModel.shared.address1, line2: UserModel.shared.address2, state: UserModel.shared.state)
        self.createUserRequest(request: createUserRequest)        
    }
}
