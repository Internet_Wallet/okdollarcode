//
//  CashInCashOutDTOA.swift
//  OK
//
//  Created by E J ANTONY on 11/08/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation
//MARK: - AuthTokenCICO Request
struct AuthTokenCICORequest: Codable {
    let deviceID: String
    let deviceType: String = "iOS"
    let password: String = ""
    let timeZoneKey: String = ""
    let userName: String
    
    enum CodingKeys: String, CodingKey {
        case deviceID = "DeviceId"
        case deviceType = "DeviceType"
        case password = "Password"
        case timeZoneKey = "TimeZoneKey"
        case userName = "Username"
    }
}

//MARK: - AuthTokenCICO Response
struct AuthTokenCICOResponse: Codable {
    let content: AvailableAgentsList?
    let statusCode, status: Int
    let comment: String?
    
    enum CodingKeys: String, CodingKey {
        case content = "Content"
        case statusCode = "StatusCode"
        case status = "Status"
        case comment = "Comment"
    }
}

//MARK: - AvailableAgentsListDTO Request
struct AvailableAgenListRequest: Codable {
    let amount, cashType, commission: Int
    let location: CICOLocation
    let filter: CICOFilterUserAgent?
    
    enum CodingKeys: String, CodingKey {
        case amount = "Amount"
        case cashType = "CashType"
        case commission = "Commission"
        case location = "Location"
        case filter = "Filter"
    }
    
    init (amount: Int, cashType: Int, commission: Int, location: CICOLocation, filter: CICOFilterUserAgent? = nil) {
        self.amount = amount
        self.cashType = cashType
        self.commission = commission
        self.location = location
        self.filter = filter
    }
}

struct CICOFilterUserAgent: Codable {
    let isMobileAgent, isStationaryAgent, isFullDay, isOpen, isClosing: Bool
    
    enum CodingKeys: String, CodingKey {
        case isMobileAgent = "IncludeMobileAgent"
        case isStationaryAgent = "IncludeStationaryAgent"
        case isFullDay = "FullDayWorking"
        case isOpen = "Opened"
        case isClosing = "RecentlyClosing"
        
    }
}

//MARK: - AvailableAgentsListDTO Response
struct AvailableAgentsListDTO: Codable {
    let content: [AvailableAgentsList]?
    let statusCode, status: Int
    let comment: String?
    
    enum CodingKeys: String, CodingKey {
        case content = "Content"
        case statusCode = "StatusCode"
        case status = "Status"
        case comment = "Comment"
    }
}

struct AvailableAgentsList: Codable {
    let firstName: String?
    let lastName: String?
    let countryCode, phoneNumber, email: String?
    let address: String?
    let type, gender, agentType: Int?
    let isActive: Bool?
    let okDollarBalance: Double?
    let cashIn, cashOut, isOpen, isOpenAlways: Bool?
    let openingTime, closingTime: String?
    let currentLocation: CICOLocation?
    let homeLocation: CICOLocation?
    let townShipName, cityName: String?
    let businessName: String?
    let country: String?
    let divisionName: String?
    let line1, line2, cellID: String?
    let state: String?
    let userName, password: String?
    let rating: Double?
    let totalRateCount: Int?
    let appVersion, messageRequestRejectedTime: String?
    let category, subCategory: String?
    let isDeleted: Bool?
    let createdDate, modifiedDate, createdBy, id: String?
    let transactionID: String?
    var isSelected: Bool = false
    
    enum CodingKeys: String, CodingKey {
        case firstName = "FirstName"
        case lastName = "LastName"
        case countryCode = "CountryCode"
        case phoneNumber = "PhoneNumber"
        case email = "Email"
        case address = "Address"
        case type = "Type"
        case gender = "Gender"
        case agentType = "AgentType"
        case isActive = "IsActive"
        case okDollarBalance = "OkDollarBalance"
        case cashIn = "CashIn"
        case cashOut = "CashOut"
        case isOpen = "IsOpen"
        case isOpenAlways = "IsOpenAlways"
        case openingTime = "OpeningTime"
        case closingTime = "ClosingTime"
        case currentLocation = "CurrentLocation"
        case homeLocation = "HomeLocation"
        case townShipName = "TownShipName"
        case cityName = "CityName"
        case businessName = "BusinessName"
        case country = "Country"
        case divisionName = "DivisionName"
        case line1 = "Line1"
        case line2 = "Line2"
        case cellID = "CellId"
        case state = "State"
        case userName = "UserName"
        case password = "Password"
        case rating = "Rating"
        case totalRateCount = "TotalRateCount"
        case appVersion = "AppVersion"
        case messageRequestRejectedTime = "MessageRequestRejectedTime"
        case category = "Category"
        case subCategory = "SubCategory"
        case isDeleted = "IsDeleted"
        case createdDate = "CreatedDate"
        case modifiedDate = "ModifiedDate"
        case createdBy = "CreatedBy"
        case id = "Id"
        case transactionID = "TransactionId"
    }
}

struct CICOLocation: Codable {
    let type: String?
    let coordinates: [Double]?
    let angle: Double?
    let time, createdTime: String?
    let accuracy: Int?
    let name: String?
    let source: String?
    let latitude, longitude: Double?
    
    enum CodingKeys: String, CodingKey {
        case type, coordinates
        case angle = "Angle"
        case time = "Time"
        case createdTime = "CreatedTime"
        case accuracy = "Accuracy"
        case name = "Name"
        case source = "Source"
        case latitude = "Latitude"
        case longitude = "Longitude"
    }
}

//MARK: - UpdateUserLoc Request
struct UpdateUserLocReq: Codable {
    var userID: String?
    var userType: Int?
    var location: CICOLocation?
}

//MARK: - Update UserLoc Response
struct UpdateUserLocResponse: Codable {
    let userLocationData: UpdateUserLocation?
    let statusCode, status: Int
    let comment: String?
    
    enum CodingKeys: String, CodingKey {
        case userLocationData = "Content"
        case statusCode = "StatusCode"
        case status = "Status"
        case comment = "Comment"
    }
}

struct UpdateUserLocation: Codable {
    let userID: String?
    let userType: Int?
    let locationInfo: CICOLocation?
    let createdDate, modifiedDate, createdBy, id: String?
    let transactionID: String?
    
    enum CodingKeys: String, CodingKey {
        case userID = "UserId"
        case userType = "UserType"
        case locationInfo = "LocationInfo"
        case createdDate = "CreatedDate"
        case modifiedDate = "ModifiedDate"
        case createdBy = "CreatedBy"
        case id = "Id"
        case transactionID = "TransactionId"
    }
}

//MARK: - Pending Cash Transfer Response
struct PendingResponseDTO: Codable {
    let content: [PendingCashTransferResponse]?
    let statusCode, status: Int?
    let comment: String?
    
    enum CodingKeys: String, CodingKey {
        case content = "Content"
        case statusCode = "StatusCode"
        case status = "Status"
        case comment = "Comment"
    }
}

struct PendingCashTransferResponse: Codable {
    let agentRequests: [AgentRequest]?
    let requestDate: String?
    let cashType: Int?
    let rechargeDestination: CICOLocation?
    let requesterID: String?
    let status: Int?
    let cancelledReason: String?
    let rechargeAmount, commisionPercent: Int?
    let isDeleted: Bool?
    let createdDate, modifiedDate, createdBy, id: String?
    let transactionID: String?
    
    enum CodingKeys: String, CodingKey {
        case agentRequests = "AgentRequests"
        case requestDate = "RequestDate"
        case cashType = "CashType"
        case rechargeDestination = "RechargeDestination"
        case requesterID = "RequesterId"
        case status = "Status"
        case cancelledReason = "CancelledReason"
        case rechargeAmount = "RechargeAmount"
        case commisionPercent = "CommisionPercent"
        case isDeleted = "IsDeleted"
        case createdDate = "CreatedDate"
        case modifiedDate = "ModifiedDate"
        case createdBy = "CreatedBy"
        case id = "Id"
        case transactionID = "TransactionId"
    }
}

//MARK: - Cash Transfer Response
struct AcceptedCashTransferResponse: Codable {
    let content: [AcceptedCashTransferList]?
    let statusCode, status: Int?
    let comment: String?
    
    enum CodingKeys: String, CodingKey {
        case content = "Content"
        case statusCode = "StatusCode"
        case status = "Status"
        case comment = "Comment"
    }
}

struct AcceptedCashTransferList: Codable {
    let cashTransferMaster: CashTransferMaster?
    let customer: AvailableAgentsList?
    
    enum CodingKeys: String, CodingKey {
        case cashTransferMaster = "CashTransferMaster"
        case customer = "Customer"
    }
}

struct CashTransferMaster: Codable {
    let agentType, status, cashType: Int?
    let receiverID, requesterID: String?
    let receiverType, requesterType: Int?
    let travelStartTime, travelEndTime: String?
    let amount, commissionPercent, commission, totalAmount: Double?
    let cashWithOutCommission: Int?
    let startLocation, endLocation: CICOLocation?
    let rechargeDestination: CICOLocation?
    let requestAcceptedTime, requestID: String?
    let cancelledReason: String?
    let ratingCompletedByReceiver, ratingCompletedByRequester: Bool?
    let createdDate, modifiedDate, createdBy, id: String?
    let transactionID: String?
    
    enum CodingKeys: String, CodingKey {
        case agentType = "AgentType"
        case status = "Status"
        case cashType = "CashType"
        case receiverID = "ReceiverId"
        case requesterID = "RequesterId"
        case receiverType = "ReceiverType"
        case requesterType = "RequesterType"
        case travelStartTime = "TravelStartTime"
        case travelEndTime = "TravelEndTime"
        case amount = "Amount"
        case commissionPercent = "CommissionPercent"
        case commission = "Commission"
        case totalAmount = "TotalAmount"
        case cashWithOutCommission = "CashWithOutCommission"
        case startLocation = "StartLocation"
        case endLocation = "EndLocation"
        case rechargeDestination = "RechargeDestination"
        case requestAcceptedTime = "RequestAcceptedTime"
        case requestID = "RequestId"
        case cancelledReason = "CancelledReason"
        case ratingCompletedByReceiver = "RatingCompletedByReceiver"
        case ratingCompletedByRequester = "RatingCompletedByRequester"
        case createdDate = "CreatedDate"
        case modifiedDate = "ModifiedDate"
        case createdBy = "CreatedBy"
        case id = "Id"
        case transactionID = "TransactionId"
    }
}

//MARK: - Update User Request
struct UpdateUserRequest: Codable {
    let countryCode, mobileNumber, openingTime, closingTime: String?
    let okDollarBalance: Int?
    let latitude, longitude, cellID: String?
    let cashIn, cashOut: Bool?
    let agentType: String?
    let type: Int?
    let isOpenAlways: Bool?
    let appVersion, category, subCategory: String?
    
    enum CodingKeys: String, CodingKey {
        case countryCode = "CountryCode"
        case mobileNumber = "MobileNumber"
        case openingTime = "OpeningTime"
        case closingTime = "ClosingTime"
        case okDollarBalance = "OkDollarBalance"
        case latitude = "Latitude"
        case longitude = "Longitude"
        case cellID = "CellId"
        case cashIn = "CashIn"
        case cashOut = "CashOut"
        case agentType = "AgentType"
        case type = "Type"
        case isOpenAlways = "IsOpenAlways"
        case appVersion = "AppVersion"
        case category = "Category"
        case subCategory = "SubCategory"
    }
}

//MARK: - Book Cash Transfer Request
struct BookCashTransferRequest: Codable {
    let agentRequests: [AgentRequestIDs]?
    let cashType, commisionPercent: Int?
    let isDeleted: Bool?
    let rechargeAmount: Int?
    let rechargeDestination: CICOLocation?
    
    enum CodingKeys: String, CodingKey {
        case agentRequests = "AgentRequests"
        case cashType = "CashType"
        case commisionPercent = "CommisionPercent"
        case isDeleted = "IsDeleted"
        case rechargeAmount = "RechargeAmount"
        case rechargeDestination = "RechargeDestination"
    }
}

struct AgentRequestIDs: Codable {
    let agentID: String
    
    enum CodingKeys: String, CodingKey {
        case agentID = "AgentId"
    }
}

//MARK: - Book Cash Transfer Response
struct BookCashTransferResponse: Codable {
    let content: BookCashTransferContent?
    let statusCode, status: Int?
    let comment: String?
    
    enum CodingKeys: String, CodingKey {
        case content = "Content"
        case statusCode = "StatusCode"
        case status = "Status"
        case comment = "Comment"
    }
}

struct BookCashTransferContent: Codable {
    let agentRequests: [AgentRequest]?
    let requestDate: String?
    let cashType: Int?
    let rechargeDestination: CICOLocation?
    let requesterID: String?
    let status: Int?
    let cancelledReason: String?
    let rechargeAmount, commisionPercent: Int?
    let isDeleted: Bool?
    let createdDate, modifiedDate, createdBy, id: String?
    let transactionID: String?
    
    enum CodingKeys: String, CodingKey {
        case agentRequests = "AgentRequests"
        case requestDate = "RequestDate"
        case cashType = "CashType"
        case rechargeDestination = "RechargeDestination"
        case requesterID = "RequesterId"
        case status = "Status"
        case cancelledReason = "CancelledReason"
        case rechargeAmount = "RechargeAmount"
        case commisionPercent = "CommisionPercent"
        case isDeleted = "IsDeleted"
        case createdDate = "CreatedDate"
        case modifiedDate = "ModifiedDate"
        case createdBy = "CreatedBy"
        case id = "Id"
        case transactionID = "TransactionId"
    }
}

struct AgentRequest: Codable {
    let agentID: String?
    let agentRequestStatus: Int?
    let sentTime: String?
    let pushReceived, smsSent: Bool?
    let createdDate, modifiedDate, createdBy, id: String?
    let transactionID: String?
    
    enum CodingKeys: String, CodingKey {
        case agentID = "AgentId"
        case agentRequestStatus = "AgentRequestStatus"
        case sentTime = "SentTime"
        case pushReceived = "PushReceived"
        case smsSent = "SMSSent"
        case createdDate = "CreatedDate"
        case modifiedDate = "ModifiedDate"
        case createdBy = "CreatedBy"
        case id = "Id"
        case transactionID = "TransactionId"
    }
}

//MARK: - Cash Transfer Request Status Response
struct CashTransferRequestStatusResponse: Codable {
    let content: CashTransferRequestStatus?
    let statusCode, status: Int?
    let comment: String?
    
    enum CodingKeys: String, CodingKey {
        case content = "Content"
        case statusCode = "StatusCode"
        case status = "Status"
        case comment = "Comment"
    }
}

struct CashTransferRequestStatus: Codable {
    let cashTransferMaster: CashTransferMaster?
    let customer: AvailableAgentsList?
    let agent: AvailableAgentsList?
    
    enum CodingKeys: String, CodingKey {
        case cashTransferMaster = "CashTransferMaster"
        case customer = "Customer"
        case agent = "Agent"
    }
}

//MARK: - Push Received Request
struct PushReceivedRequest: Codable {
    let pushNotificationKey, tripRequestId, agentId: String
    
    enum CodingKeys: String, CodingKey {
        case pushNotificationKey = "PushNotificationKey"
        case tripRequestId = "TripRequestId"
        case agentId = "AgentId"
    }
}

//MARK: - Push Received Response
struct PushReceivedResponse: Codable {
    let statusCode, status: Int
    let comment: String?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "StatusCode"
        case status = "Status"
        case comment = "Comment"
    }
}

//MARK: - Get Cash Transfer Request Detail Response
struct CashTransferRequestDetailResponse: Codable {
    let content: CashTransferRequestDetail?
    let statusCode, status: Int
    let comment: String?
    
    enum CodingKeys: String, CodingKey {
        case content = "Content"
        case statusCode = "StatusCode"
        case status = "Status"
        case comment = "Comment"
    }
}

struct CashTransferRequestDetail: Codable {
    let cashTransferRequest: CashTransferAcceptDetails?
    let customer: AvailableAgentsList?
    
    enum CodingKeys: String, CodingKey {
        case cashTransferRequest = "CashTransferRequest"
        case customer = "Customer"
    }
}

//MARK: - Accept Cash Transfer Request
struct CashTransferAcceptRequest: Codable {
    let cashTransferRequestID: String?
    let cashTransferRequest: CashTransferAcceptDetails?
    let agentID: String?
    let agentLocation: CICOLocation?
    let agentType: Int?
    
    enum CodingKeys: String, CodingKey {
        case cashTransferRequestID = "CashTransferRequestId"
        case cashTransferRequest = "CashTransferRequest"
        case agentID = "AgentId"
        case agentLocation = "AgentLocation"
        case agentType = "AgentType"
    }
}

struct CashTransferAcceptDetails: Codable {
    let agentRequests: [AgentRequest]?
    let requestDate: String?
    let cashType: Int?
    let rechargeDestination: CICOLocation?
    let requesterID: String?
    let status: Int?
    let cancelledReason: String?
    let rechargeAmount, commisionPercent: Double?
    let isDeleted: Bool?
    let createdDate, modifiedDate, createdBy, id: String?
    let transactionID: String?
    
    enum CodingKeys: String, CodingKey {
        case agentRequests = "AgentRequests"
        case requestDate = "RequestDate"
        case cashType = "CashType"
        case rechargeDestination = "RechargeDestination"
        case requesterID = "RequesterId"
        case status = "Status"
        case cancelledReason = "CancelledReason"
        case rechargeAmount = "RechargeAmount"
        case commisionPercent = "CommisionPercent"
        case isDeleted = "IsDeleted"
        case createdDate = "CreatedDate"
        case modifiedDate = "ModifiedDate"
        case createdBy = "CreatedBy"
        case id = "Id"
        case transactionID = "TransactionId"
    }
}

//MARK: - Reject Cash Transfer Request
struct CashTransferRejectRequest: Codable {
    let cashTransferRequestID: String
    
    enum CodingKeys: String, CodingKey {
        case cashTransferRequestID = "CashTransferRequestId"
    }
}

//MARK: - Accept Cash Transfer Response
struct CashTransferAcceptResponse: Codable {
    let content: CashTransferRequestStatus?
    let statusCode, status: Int?
    let comment: String?
    
    enum CodingKeys: String, CodingKey {
        case content = "Content"
        case statusCode = "StatusCode"
        case status = "Status"
        case comment = "Comment"
    }
}

//MARK: - Reject Cash Transfer Response
struct CashTransferRejectResponse: Codable {
    let content: String?
    let statusCode, status: Int
    let comment: String?
    
    enum CodingKeys: String, CodingKey {
        case content = "Content"
        case statusCode = "StatusCode"
        case status = "Status"
        case comment = "Comment"
    }
}

//MARK: - Update Location While Travel Request
struct UpdateLOCWhileTravelRequest: Codable {
    let cashTransferMasterID, customerID: String?
    let userLocation: UpdateUserLocation?
    
    enum CodingKeys: String, CodingKey {
        case cashTransferMasterID = "CashTransferMasterId"
        case customerID = "CustomerId"
        case userLocation = "UserLocation"
    }
}

//MARK: - Cancel Cash Transfer Request
struct CancelCashTransferRequest: Codable {
    let cancelledReason, requestID: String?
    
    enum CodingKeys: String, CodingKey {
        case cancelledReason = "CancelledReason"
        case requestID = "RequestId"
    }
}

//MARK: - Cancel Cash Transfer Response
struct CancelCashTransferResponse: Codable {
    let content: CancelCashTransfer?
    let statusCode, status: Int?
    let comment: String?
    
    enum CodingKeys: String, CodingKey {
        case content = "Content"
        case statusCode = "StatusCode"
        case status = "Status"
        case comment = "Comment"
    }
}

struct CancelCashTransfer: Codable {
    let customerID, agentID, requestID: String?
    let senderIDS: [CICOSenderID]?
    let request: CashTransferAcceptDetails?
    
    enum CodingKeys: String, CodingKey {
        case customerID = "CustomerId"
        case agentID = "AgentId"
        case requestID = "RequestId"
        case senderIDS = "SenderIds"
        case request = "Request"
    }
}

struct CICOSenderID: Codable {
    let agentID: String?
    let agentRequestStatus: Int?
    let sentTime: String?
    let pushReceived, smsSent: Bool?
    let createdDate, modifiedDate: String?
    let createdBy, id, transactionID: String?
    
    enum CodingKeys: String, CodingKey {
        case agentID = "AgentId"
        case agentRequestStatus = "AgentRequestStatus"
        case sentTime = "SentTime"
        case pushReceived = "PushReceived"
        case smsSent = "SMSSent"
        case createdDate = "CreatedDate"
        case modifiedDate = "ModifiedDate"
        case createdBy = "CreatedBy"
        case id = "Id"
        case transactionID = "TransactionId"
    }
}

//MARK: - Start Trip Request
struct StartTripRequest: Codable {
    let cashTransferMasterID: String?
    let startLocation: CICOLocation?
    
    enum CodingKeys: String, CodingKey {
        case cashTransferMasterID = "CashTransferMasterId"
        case startLocation = "StartLocation"
    }
}

//MARK: - Start End Trip Response
struct StartEndTripResponse: Codable {
    let content: CashTransferMaster?
    let statusCode, status: Int?
    let comment: String?
    
    enum CodingKeys: String, CodingKey {
        case content = "Content"
        case statusCode = "StatusCode"
        case status = "Status"
        case comment = "Comment"
    }
}

//MARK: - End Trip Request
struct EndTripRequest: Codable {
    let cashMasterID: String?
    let endLocation: CICOLocation?
    
    enum CodingKeys: String, CodingKey {
        case cashMasterID = "CashMasterId"
        case endLocation = "EndLocation"
    }
}

//MARK: - Generic Payment Request
struct GenericPaymentRequest: Codable {
    let destinationNumberWalletBalance = ""
    let lExternalReference: LExternalReference
    let lGeoLocation: LGeoLocation
    let lProximity: LProximity
    let lTransactions: LTransactions
    
    enum CodingKeys: String, CodingKey {
        case destinationNumberWalletBalance = "DestinationNumberWalletBalance"
        case lExternalReference, lGeoLocation, lProximity, lTransactions
    }
}

struct LExternalReference: Codable {
    let direction = true
    let endStation = "", startStation = "", vendorID = ""
    
    enum CodingKeys: String, CodingKey {
        case direction = "Direction"
        case endStation = "EndStation"
        case startStation = "StartStation"
        case vendorID = "VendorID"
    }
}

struct LGeoLocation: Codable {
    let cellID = ""
    var latitude = "0.0"
    var longitude = "0.0"
    
    enum CodingKeys: String, CodingKey {
        case cellID = "CellID"
        case latitude = "Latitude"
        case longitude = "Longitude"
    }
}

struct LProximity: Codable {
    let blueToothUsers = [String]()
    let selectionMode = false
    let wifiUsers = [String]()
    
    enum CodingKeys: String, CodingKey {
        case blueToothUsers = "BlueToothUsers"
        case selectionMode = "SelectionMode"
        case wifiUsers = "WifiUsers"
    }
}

struct LTransactions: Codable {
    let businessName: String?
    let destinationNumberWalletBalance: String?
    let isPromotionApplicable: String?
    let kickBackMsisdn: String?
    let merchantName, promoCodeID: String?
    let mode = true
    let discountPayTo, resultCode: Double?
    let resultDescription, transactionID: String?
    let transactionTime, accounType, senderBusinessName, userName: String?
    let amount, balance: String?
    let bonusPoint, cashBackFlag: Double?
    let comments, destination: String?
    let isMectelTopUp = false
    let kickBack, localTransactionType: String?
    let mobileNumber, password, secureToken, transactionType: String?
    
    enum CodingKeys: String, CodingKey {
        case businessName = "BusinessName"
        case destinationNumberWalletBalance = "DestinationNumberWalletBalance"
        case isPromotionApplicable = "IsPromotionApplicable"
        case kickBackMsisdn = "KickBackMsisdn"
        case merchantName = "MerchantName"
        case promoCodeID = "PromoCodeId"
        case mode = "Mode"
        case discountPayTo = "DiscountPayTo"
        case resultCode = "ResultCode"
        case resultDescription = "ResultDescription"
        case transactionID = "TransactionID"
        case transactionTime = "TransactionTime"
        case accounType, senderBusinessName, userName
        case amount = "Amount"
        case balance = "Balance"
        case bonusPoint = "BonusPoint"
        case cashBackFlag = "CashBackFlag"
        case comments = "Comments"
        case destination = "Destination"
        case isMectelTopUp = "IsMectelTopUp"
        case kickBack = "KickBack"
        case localTransactionType = "LocalTransactionType"
        case mobileNumber = "MobileNumber"
        case password = "Password"
        case secureToken = "SecureToken"
        case transactionType = "TransactionType"
    }
}

//MARK: - Generic Payment Response
struct GenericPaymentResponse: Codable {
    let code: Int?
    let data, msg: String?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case msg = "Msg"
    }
}

struct GenericPaymentXMLResponse: XMLIndexerDeserializable {
    var estel: CICOPaymentEstel?
    static func deserialize(_ node: XMLIndexer) throws -> GenericPaymentXMLResponse {
        return try GenericPaymentXMLResponse(
            estel: CICOPaymentEstel.deserialize(node["estel"])
        )
    }
}

struct CICOPaymentEstel: XMLIndexerDeserializable {
    var header: CICOPaymentHeader?
    var response: CICOPaymentResponse?
    static func deserialize(_ node: XMLIndexer) throws -> CICOPaymentEstel {
        return try CICOPaymentEstel(
            header: CICOPaymentHeader.deserialize(node["header"]),
            response: CICOPaymentResponse.deserialize(node["response"])
        )
    }
}

struct CICOPaymentHeader: XMLIndexerDeserializable {
    var responseType: String?
    static func deserialize(_ node: XMLIndexer) throws -> CICOPaymentHeader {
        return try CICOPaymentHeader(
            responseType: node["responsetype"].value()
        )
    }
}

struct CICOPaymentResponse: XMLIndexerDeserializable {
    var resultCode: Int?
    var resultDescription: String?
    var source: Int?
    var agentCode: Int?
    var amount: Double?
    var transId: Int?
    var requestcts: String?
    var responsects: String?
    var responsevalue: Int?
    var destination: Int?
    var vendorCode: String?
    var walletBalance: Double?
    var preWalletBalance: Double?
    var clientType: String?
    var comments: String?
    var agentName: String?
    var preOprWallet: Double?
    var oprWallet: Double?
    var maxWalletValue: Double?
    var minWalletValue: Double?
    var walletAlertValue: Double?
    var isRegisterAgent: String?
    var secureToken: String?
    var clientIP: String?
    var clientOS: String?
    var unRegiPreBalance: Double?
    var unRegiPostBalance: Double?
    var preKickWallet: Double?
    var kickWallet: Double?
    var destLevel: String?
    var isKickBackMSISDNRegister: String
    var referredLang: String?
    var kickBackEnable: String?
    
    static func deserialize(_ node: XMLIndexer) throws -> CICOPaymentResponse {
        
        return try CICOPaymentResponse(
            resultCode: node["resultcode"].value(),
            resultDescription: node["resultdescription"].value(),
            source: node["source"].value(),
            agentCode: node["agentcode"].value(),
            amount: node["amount"].value(),
            transId: node["transid"].value(),
            requestcts: node["requestcts"].value(),
            responsects: node["responsects"].value(),
            responsevalue: node["responsevalue"].value(),
            destination: node["destination"].value(),
            vendorCode: node["vendorcode"].value(),
            walletBalance: node["walletbalance"].value(),
            preWalletBalance: node["prewalletbalance"].value(),
            clientType: node["clienttype"].value(),
            comments: node["comments"].value(),
            agentName: node["agentname"].value(),
            preOprWallet: node["preoprwallet"].value(),
            oprWallet: node["oprwallet"].value(),
            maxWalletValue: node["maxwalletvalue"].value(),
            minWalletValue: node["minwalletvalue"].value(),
            walletAlertValue: node["walletalertvalue"].value(),
            isRegisterAgent: node["isregisteragent"].value(),
            secureToken: node["securetoken"].value(),
            clientIP: node["clientip"].value(),
            clientOS: node["clientos"].value(),
            unRegiPreBalance: node["unregiprebalance"].value(),
            unRegiPostBalance: node["unregipostbalance"].value(),
            preKickWallet: node["prekickwallet"].value(),
            kickWallet: node["kickwallet"].value(),
            destLevel: node["destlevel"].value(),
            isKickBackMSISDNRegister: node["iskickbackmsisdnregister"].value(),
            referredLang: node["referredlang"].value(),
            kickBackEnable: node["kickbackenable"].value())
    }
}

//MARK: - Rate Customer Or Agent Request
struct RateCustomerOrAgentRequest: Codable {
    let cashTransferMasterID, ratedBy, ratedTo: String?
    let ratedValue, ratingTypeBy: Int?
    
    enum CodingKeys: String, CodingKey {
        case cashTransferMasterID = "CashTransferMasterId"
        case ratedBy = "RatedBy"
        case ratedTo = "RatedTo"
        case ratedValue = "RatedValue"
        case ratingTypeBy = "RatingTypeBy"
    }
}

//MARK: - Rate Customer Or Agent Response
struct RateCustomerOrAgentResponse: Codable {
    let content: RateCustomerOrAgentContent?
    let statusCode, status: Int?
    let comment: String?
    
    enum CodingKeys: String, CodingKey {
        case content = "Content"
        case statusCode = "StatusCode"
        case status = "Status"
        case comment = "Comment"
    }
}

struct RateCustomerOrAgentContent: Codable {
    let ratedValue: Int?
    let ratedBy, ratedTo: String?
    let ratingTypeBy: Int?
    let cashTransferMasterID: String?
    let feedBacks: String?
    let createdDate, modifiedDate, createdBy, id: String?
    let transactionID: String?
    
    enum CodingKeys: String, CodingKey {
        case ratedValue = "RatedValue"
        case ratedBy = "RatedBy"
        case ratedTo = "RatedTo"
        case ratingTypeBy = "RatingTypeBy"
        case cashTransferMasterID = "CashTransferMasterId"
        case feedBacks = "FeedBacks"
        case createdDate = "CreatedDate"
        case modifiedDate = "ModifiedDate"
        case createdBy = "CreatedBy"
        case id = "Id"
        case transactionID = "TransactionId"
    }
}

//MARK: - Complete Payment Request
struct CompletePaymentRequest: Codable {
    let cashTransferMasterID: String?
    let cashWithOutCommission: Double?
    let okDollarTransactionID: String?
    let rechargeAmount: Double?
    
    enum CodingKeys: String, CodingKey {
        case cashTransferMasterID = "CashTransferMasterId"
        case cashWithOutCommission = "CashWithOutCommission"
        case okDollarTransactionID = "OkDollarTransactionId"
        case rechargeAmount = "RechargeAmount"
    }
}

//MARK: - Complete Payment Response
struct CompletePaymentResponse: Codable {
    let content: CashTransferMaster?
    let statusCode, status: Int?
    let comment: String?
    
    enum CodingKeys: String, CodingKey {
        case content = "Content"
        case statusCode = "StatusCode"
        case status = "Status"
        case comment = "Comment"
    }
}

//MARK: - Create User Request
struct CreateUserRequest: Codable {
    let firstName, lastName, countryCode, phoneNumber: String?
    let email, address: String?
    let type, gender: Int?
    let currentLocation, businessLocation, homeLocation: CICOLocation?
    let userName, password: String?
    let cityName, businessName, country, divisionName, line1, line2, state: String?
    
    enum CodingKeys: String, CodingKey {
        case firstName = "FirstName"
        case lastName = "LastName"
        case countryCode = "CountryCode"
        case phoneNumber = "PhoneNumber"
        case email = "Email"
        case address = "Address"
        case type = "Type"
        case gender = "Gender"
        case currentLocation = "CurrentLocation"
        case businessLocation = "BusinessLocation"
        case homeLocation = "HomeLocation"
        case userName = "UserName"
        case password = "Password"
        case cityName = "CityName"
        case businessName = "BusinessName"
        case country = "Country"
        case divisionName = "DivisionName"
        case line1 = "Line1"
        case line2 = "Line2"
        case state = "State"
    }
}

//MARK: - Create User Response
struct CreateUserResponse: Codable {
    let content: CreateUserResponseContent?
    let statusCode, status: Int?
    let comment: String?
    
    enum CodingKeys: String, CodingKey {
        case content = "Content"
        case statusCode = "StatusCode"
        case status = "Status"
        case comment = "Comment"
    }
}

struct CreateUserResponseContent: Codable {
    let firstName, lastName, countryCode, phoneNumber: String?
    let email, address: String?
    let type, gender, agentType: Int?
    let isActive: Bool?
    let okDollarBalance: Int?
    let cashIn, cashOut, isOpen, isOpenAlways: Bool?
    let openingTime, closingTime: String?
    let currentLocation, homeLocation: CICOLocation??
    let townShipName, cityName, businessName, country: String?
    let divisionName, line1, line2, cellID: String?
    let state: String?
    let userName, password: String?
    let rating, totalRateCount: Int?
    let appVersion: String?
    let messageRequestRejectedTime: String?
    let category, subCategory: String?
    let isDeleted: Bool?
    let createdDate, modifiedDate, createdBy, id: String?
    let transactionID: String?
    
    enum CodingKeys: String, CodingKey {
        case firstName = "FirstName"
        case lastName = "LastName"
        case countryCode = "CountryCode"
        case phoneNumber = "PhoneNumber"
        case email = "Email"
        case address = "Address"
        case type = "Type"
        case gender = "Gender"
        case agentType = "AgentType"
        case isActive = "IsActive"
        case okDollarBalance = "OkDollarBalance"
        case cashIn = "CashIn"
        case cashOut = "CashOut"
        case isOpen = "IsOpen"
        case isOpenAlways = "IsOpenAlways"
        case openingTime = "OpeningTime"
        case closingTime = "ClosingTime"
        case currentLocation = "CurrentLocation"
        case homeLocation = "HomeLocation"
        case townShipName = "TownShipName"
        case cityName = "CityName"
        case businessName = "BusinessName"
        case country = "Country"
        case divisionName = "DivisionName"
        case line1 = "Line1"
        case line2 = "Line2"
        case cellID = "CellId"
        case state = "State"
        case userName = "UserName"
        case password = "Password"
        case rating = "Rating"
        case totalRateCount = "TotalRateCount"
        case appVersion = "AppVersion"
        case messageRequestRejectedTime = "MessageRequestRejectedTime"
        case category = "Category"
        case subCategory = "SubCategory"
        case isDeleted = "IsDeleted"
        case createdDate = "CreatedDate"
        case modifiedDate = "ModifiedDate"
        case createdBy = "CreatedBy"
        case id = "Id"
        case transactionID = "TransactionId"
    }
}

//MARK: - Generic Response for failure case
struct GenericFailureResponse: Codable {
    let content: String?
    let statusCode: Int?
    let status: Int?
    let exception: String?
    let comment: String?
    
    enum CodingKeys: String, CodingKey {
        case content = "Content"
        case statusCode = "StatusCode"
        case status = "Status"
        case exception = "Exception"
        case comment = "Comment"
    }
}

//MARK: - Get distance time between two location response
struct GetDistTimeBtwnLOCResponse: Codable {
    let routes: [CICORoute]?
    let status: String?
    
    enum CodingKeys: String, CodingKey {
        case routes, status
    }
}

struct CICORoute: Codable {
    let bounds: CICOBounds?
    let legs: [Leg]
    let summary: String?
    
    enum CodingKeys: String, CodingKey {
        case bounds, legs
        case summary
    }
}

struct CICOBounds: Codable {
    let northeast, southwest: CICOBoundsCoord?
}

struct CICOBoundsCoord: Codable {
    let lat, lng: Double
}

struct Leg: Codable {
    let distance, duration: CICODistance?
    let endAddress: String?
    let endLocation: CICOBoundsCoord?
    let startAddress: String?
    let startLocation: CICOBoundsCoord?
    let steps: [CICOSteps]?
    
    enum CodingKeys: String, CodingKey {
        case distance, duration
        case endAddress = "end_address"
        case endLocation = "end_location"
        case startAddress = "start_address"
        case startLocation = "start_location"
        case steps
    }
}

struct CICODistance: Codable {
    let text: String?
    let value: Double?
}

struct CICOSteps: Codable {
    let distance, duration: CICODistance?
    let endLocation: CICOBoundsCoord?
    let htmlInstructions: String?
    let startLocation: CICOBoundsCoord?
    let travelMode: String?
    
    enum CodingKeys: String, CodingKey {
        case distance, duration
        case endLocation = "end_location"
        case htmlInstructions = "html_instructions"
        case startLocation = "start_location"
        case travelMode = "travel_mode"
    }
}
