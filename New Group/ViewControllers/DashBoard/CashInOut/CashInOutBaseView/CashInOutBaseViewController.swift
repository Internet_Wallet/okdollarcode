//
//  CashInOutBaseViewController.swift
//  OK
//
//  Created by Kethan on 8/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit



enum CICONotificationCase : String {
    case KEY_NEW_CASH_TRANSACTION_REQUEST = "NewCashTransferRequest", KEY_CASH_REQUEST_ACCEPTED = "CashRequestAccepted",
    KEY_CANCEL_TRANSACTION = "CancelCashTransfer", KEY_START_TRIP = "StartTrip", KEY_END_TRIP = "EndTrip", KEY_PAYMENT_COLLECTED = "CashTransferPaymentCollected", KEY_AGENT_LOCATION = "AgentLocationUpdated", KEY_PROCESS_PENDING_REQUEST = "ProcessRequestPending", KEY_REJECTED_REQUEST = "RejectCashTransfer", KEY_ACCEPTED_BY_OTHERS = "CashRequestAcceptedByOthers"
}

struct cashInOutViewControllers {
    static let cashinoutStoryBoard = "CashInOutStoryboard"
    
    struct viewControllers {
        static let cioHomeVC = "CIOHomeViewController_ID"
        static let ciocomPerVC = "CIOComsnPercentViewController_ID"
        static let cioAgentListVC = "CIOAgentListViewController_ID"
        //Status Controllers
        static let cioTransHomeVC = "CIOTransactionHomeViewController_ID"
        static let cioMyrequestVC = "CIOMyRequestViewController_ID"
        static let cioPendingrequestVC = "CIOPendingRequestViewController_ID"
        static let cioSuccessrequestVC = "CIOSuccessRequestViewController_ID"
        static let cioAllrequestVC = "CIOAllRequestViewController_ID"
        static let cioLocationTrackerVC = "CICOLocationTrackerViewcontroller_ID"
        static let cioConfirmationVC = "CIOConfimationViewController_ID"
        static let cioRatingVC = "CIORatingViewController_ID"
    }
    
    struct tableViewCells {
        
    }
}

var currentAddressGlobal : String? = ""

class CashInOutBaseViewController: OKBaseController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        appDel.floatingButtonControl?.window.button?.isHidden = true
        appDel.floatingButtonControl?.closeButton.isHidden = true
    }
    
    func addCustomBackButton(navTitle: String) {
        let backButton = UIButton(type: .custom)
        backButton.setBackgroundImage(#imageLiteral(resourceName: "tabBarBack"), for: .normal)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        navigationItem.title = navTitle.localized
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white ]
        self.navigationController?.navigationBar.barTintColor = kYellowColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    func getCurrentAddress(_completionHandler : @escaping(String) -> Void) {
        if let lat = geoLocManager.currentLatitude, let long = geoLocManager.currentLongitude {
            geoLocManager.getLocationNameByLatLong(lattitude: lat, longitude: long, isForCashinCashout : true) { (isSuccess, currentAddress) in
                guard isSuccess else {
                    println_debug("something went wrong when get current address from google api")
                    PTLoader.shared.hide()
                    //need to show alert
                    return
                }
                if let curAddress = currentAddress as? String, curAddress.count > 0 {
                    currentAddressGlobal = curAddress
                    progressViewObj.removeProgressView()
                    _completionHandler(curAddress)
                } else {
                    //need to show alert
                     _completionHandler("")
                }
            }
        } else {
             _completionHandler("")
            //need to show alert
        }
    }
    
    func getLocationDetailsForApi() -> CICOLocation {
        let addressArray = currentAddressGlobal?.components(separatedBy: ",")
        var add = ""
        if (addressArray?.count)! > 0 {
            add = addressArray![0]
        }
        let loc = CICOLocation(type: "POINT", coordinates: [Double(geoLocManager.currentLongitude ?? "0.0")!,Double(geoLocManager.currentLatitude ?? "0.0")!], angle: nil, time: nil, createdTime: nil, accuracy: 0, name: add, source: currentAddressGlobal ?? "", latitude: Double(geoLocManager.currentLatitude ?? "0.0"), longitude: Double(geoLocManager.currentLongitude ?? "0.0")!)
        return loc
    }
    
    func getAgentType() -> Int {
        var agentType = 6
        switch UserModel.shared.agentType {
        case .user:
            agentType = 6
        case .agent:
            agentType = 1
        case .advancemerchant:
            agentType = 4
        case .dummymerchant:
            agentType = 5
        case .merchant:
            agentType = 2
        case .oneStopMart:
            agentType = 8
        }
        
        return agentType
    }
    
}
