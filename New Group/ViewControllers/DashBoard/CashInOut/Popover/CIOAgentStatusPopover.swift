//
//  CIOAgentStatusPopover.swift
//  OK
//
//  Created by Kethan on 8/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol AgentStatusChangeProtocol : class {
    func cashInOutStatusAction(cashIn : Bool, cashout : Bool, stationaryMobileStatus : Bool, typeAndStatus: (CICOTypeAndStatus, Bool))
}

enum CICOTypeAndStatus {
    case cashIn
    case cashOut
    case mobileStationary
}

class CIOAgentStatusPopover: UIViewController {
    
    var cashInEnable = true
    var cashOutEnable = true
    var mobileStationaryEnable = true
    
    @IBOutlet weak var successImageView: UIImageView!
    @IBOutlet weak var cashInLabel: UILabel! {
        didSet {
            self.cashInLabel.font = UIFont(name: appFont, size: appFontSize)
            self.cashInLabel.text = self.cashInLabel.text?.localized
        }
    }
    @IBOutlet weak var cashOutLabel: UILabel! {
        didSet {
            self.cashOutLabel.font = UIFont(name: appFont, size: appFontSize)
            self.cashOutLabel.text = self.cashOutLabel.text?.localized
        }
    }
    
    weak var delegate : AgentStatusChangeProtocol?
    @IBOutlet weak var cashInRadioBtn : UISwitch!
    @IBOutlet weak var cashOutRadioBtn : UISwitch!
    @IBOutlet weak var stationaryMobileBtn : UIButton!{
        didSet{
            stationaryMobileBtn.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        successImageView.image = UIImage(named: "info_success.png")!.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        successImageView.tintColor = UIColor.yellow
        cashInRadioBtn.setOn(UserDefaults.standard.bool(forKey: "MerchantCashInStatus"), animated: true)
        cashOutRadioBtn.setOn(UserDefaults.standard.bool(forKey: "MerchantCashOutStatus"), animated: true)
        if UserDefaults.standard.bool(forKey: "MerchantCashMobStationayStatus") {
            mobileStationaryEnable = true
            stationaryMobileBtn.setTitle("Delivery service enabled".localized, for: .normal)
        } else {
            stationaryMobileBtn.setTitle("Delivery service disabled".localized, for: .normal)
            mobileStationaryEnable = false
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButtonAction(_ sender : UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cashInButtonAction(_ sender : UISwitch) {
        if appDel.checkNetworkAvail() {
            cashInEnable = sender.isOn
            UserDefaults.standard.set(cashInEnable, forKey: "MerchantCashInStatus")
            self.delegate?.cashInOutStatusAction(cashIn: cashInEnable, cashout: cashOutEnable, stationaryMobileStatus: mobileStationaryEnable, typeAndStatus: (.cashIn, cashInEnable))
        } else {
            sender.isOn = !sender.isOn
            self.showErrorAlert(errMessage: "No Internet Connection".localized)
        }
    }
    
    @IBAction func cashOutButtonAction(_ sender : UISwitch) {
        if appDel.checkNetworkAvail() {
            cashOutEnable = sender.isOn
            UserDefaults.standard.set(cashOutEnable, forKey: "MerchantCashOutStatus")
            self.delegate?.cashInOutStatusAction(cashIn: cashInEnable, cashout: cashOutEnable, stationaryMobileStatus: mobileStationaryEnable, typeAndStatus: (.cashOut, cashOutEnable))
        } else {
            sender.isOn = !sender.isOn
        }
    }
    
    @IBAction func stationaryMobileButtonAction(_ sender : UIButton) {
        if appDel.checkNetworkAvail() {
            if !mobileStationaryEnable {
                mobileStationaryEnable = true
                stationaryMobileBtn.setTitle("Delivery service enabled".localized, for: .normal)
                
            } else {
                stationaryMobileBtn.setTitle("Delivery service disabled".localized, for: .normal)
                mobileStationaryEnable = false
            }
            self.delegate?.cashInOutStatusAction(cashIn: cashInEnable, cashout: cashOutEnable, stationaryMobileStatus: mobileStationaryEnable, typeAndStatus: (.mobileStationary, mobileStationaryEnable))
            UserDefaults.standard.set(mobileStationaryEnable, forKey: "MerchantCashMobStationayStatus")
        } else {
            //self.showErrorAlert(errMessage: "No Internet Connection".localized)
        }
    }
    
}
