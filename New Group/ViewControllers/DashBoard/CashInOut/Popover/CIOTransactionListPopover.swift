//
//  CIOTransactionListViewController.swift
//  OK
//
//  Created by Kethan on 8/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol ViewAllTransactionProtocol: class {
    func showViewAllTransactionVC()
}

class CIOTransactionListPopover: UIViewController {
    //MARK:- Outlets
    @IBOutlet weak var viewAllTransactionBtn: UIButton! {
        didSet {
            self.viewAllTransactionBtn.titleLabel?.font = UIFont(name: appFont, size: appTitleButton)
            self.viewAllTransactionBtn.setTitle(viewAllTransactionBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    //MARK:- Properties
    weak var delegate: ViewAllTransactionProtocol?
    
    //MARK:- Methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func showTransactionViewController(_ sender : UIButton) {
        sender.removeTarget(nil, action: nil, for: .touchUpInside)
        self.delegate?.showViewAllTransactionVC()
        self.dismiss(animated: true, completion: nil)
    }
}
