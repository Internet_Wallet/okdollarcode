//
//  OffersSuccessPageViewController.swift
//  OK
//
//  Created by Ashish on 11/16/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit

protocol OfferSuccessPageDelegate : class {
    func backFromSucessPage()
}

class OffersSuccessPageViewController: OKBaseController {
    
    @IBOutlet weak var header : UILabel! {
        didSet {
            header.text = "Congrats!".localized
        }
    }
    
    @IBOutlet weak var subHeading : UILabel! {
        didSet {
            subHeading.text = "Promo Code redeem is successful".localized
        }
    }
    
    @IBOutlet weak var imageView : UIImageView!
    
    @IBOutlet weak var doneBtn : UIButton! {
        didSet {
            DispatchQueue.main.async {
                self.doneBtn.setTitle("Done".localized, for: .normal)
            }
        }
    }
    
    weak var delegate : OfferSuccessPageDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func doneBtnAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        delegate?.backFromSucessPage()
    }
    
}
