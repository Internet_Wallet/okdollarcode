//
//  OffersMoreViewController.swift
//  OK
//
//  Created by Ashish on 1/9/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol OffersMoreAction : class {
    func openShare()
    func openInvoice()
    func redirectOffersListing()
}

class OffersMoreViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var touchResistantView: CardDesignView!
    
    @IBOutlet weak var invoiceBtn: UIButton! {
        didSet {
            self.invoiceBtn.setTitle("Invoice".localized, for: .normal)
        }
    }
    
    @IBOutlet weak var shareBtn: UIButton! {
        didSet {
            self.shareBtn.setTitle("Share".localized, for: .normal)
        }
    }
  
  @IBOutlet weak var moreOffersBtn: UIButton! {
    didSet {
      self.moreOffersBtn.setTitle("More Offers".localized, for: .normal)
    }
  }
    weak var delegate : OffersMoreAction?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideWhenTappedAround()
        // Do any additional setup after loading the view.
    }
    
    func hideWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(dismissSelf))
        tap.delegate = self
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissSelf() {
        println_debug("removed called")
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- UIGestureRecognizerDelegate
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        let point = touch.location(in: self.view)
        if self.touchResistantView.frame.contains(point) {
            println_debug(self.touchResistantView.frame.contains(point))
            return false
        }
        return true
    }

  @IBAction func moreOffersAction(_ sender: UIButton) {
    self.dismiss(animated: false, completion: nil)
    self.delegate?.redirectOffersListing()
  }
    
    @IBAction func invoiceAction(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
        self.delegate?.openInvoice()
    }
    
    @IBAction func shareAction(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
        self.delegate?.openShare()
    }
    
}
