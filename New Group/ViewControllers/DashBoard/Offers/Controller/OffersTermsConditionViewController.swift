//
//  OffersTermsConditionViewController.swift
//  OK
//
//  Created by Ashish on 7/17/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol OffersAlertDelegate : class{
    func navigateToPayment(id: OffersListModel?)
}

class OffersTermsConditionViewController: OKBaseController {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var header: UILabel!
    @IBOutlet weak var close: UIButton!
    @IBOutlet weak var textViewtnc: UITextView!
    @IBOutlet weak var okBtn: UIButton!
      {
      didSet
      {
        okBtn.setTitle("OK".localized, for: .normal)
      }
  }
    @IBOutlet weak var cancelBtn: UIButton!
        {
        didSet
        {
            cancelBtn.setTitle("Cancel".localized, for: .normal)
        }
    }
    @IBOutlet weak var viewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var heightContraint: NSLayoutConstraint!
    var conditions : String?
    
    var offerID : String?
    
    var model: OffersListModel?

    weak var delegate : OffersAlertDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.header.text = "Terms and Conditions".localized
        
        self.textViewtnc.text = ""

        if device.type == .iPhoneXS || device.type == .iPhoneXR || device.type == .iPhoneXSMax  {
          
            heightContraint.constant = 560
        }
        else
        {
             heightContraint.constant = 527
        }
        
      
        if appDel.currentLanguage == "my" {
            if let imageString = model?.promotionBImg, let imageURL = URL(string: imageString) {
                self.imgView.setImage(url: imageURL, contentMode: .scaleAspectFit, placeholderImage: nil)
            }
        }
        else if appDel.currentLanguage == "th" {
            if let imageString = model?.promotionTImg, let imageURL = URL(string: imageString) {
                self.imgView.setImage(url: imageURL, contentMode: .scaleAspectFit, placeholderImage: nil)
            }
        }
        else if appDel.currentLanguage == "zh-Hans" {
            if let imageString = model?.promotionCImg, let imageURL = URL(string: imageString) {
                self.imgView.setImage(url: imageURL, contentMode: .scaleAspectFit, placeholderImage: nil)
            }
        }
        else {
            if let imageString = model?.promotionImg, let imageURL = URL(string: imageString) {
                self.imgView.setImage(url: imageURL, contentMode: .scaleAspectFit, placeholderImage: nil)
            }
        }
        
      if appDel.currentLanguage == "my" {
        self.textViewtnc.text = model?.bTermsAndCondition
      }
      else if appDel.currentLanguage == "zh-Hans" {
        self.textViewtnc.text = model?.cTermsAndCondition
      }
      else {
        self.textViewtnc.text = model?.termsAndCondition
      }
      
    }

    @IBAction func dismissAction(_ sender: UIButton) {
        self.dismiss(animated: true) {
            if sender != self.cancelBtn {
                self.delegate?.navigateToPayment(id: self.model)
            }
        }
    }
    
}
