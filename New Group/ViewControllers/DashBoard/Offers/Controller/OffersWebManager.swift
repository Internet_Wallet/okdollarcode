//
//  OffersWebManager.swift
//  OK
//
//  Created by Ashish on 11/1/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class OffersManager {

    class func getConditionalMappingByPromotinId(amount:String, model: OffersListModel, agentNo: String,  completion: @escaping (_ response: OffersListModel?) -> Void) {
        
        let offersHeaderURL = Url.offersConditionalMapping
        
        guard let offersURL = offersHeaderURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        
        let url = getUrl(urlStr: offersURL, serverType: .serverApp)
        
        var param = Dictionary<String,Any>()
        param["PromotionId"] = model.promotionID
        param["AgentMobileNumber"] = agentNo
        
        TopupWeb.genericClass(url: url, param: param as AnyObject, httpMethod: "POST") { (response, success) in
            DispatchQueue.main.async {
                if !success { return }
                var finalModel : OffersListModel?
                if let valueDictionary = response as? Dictionary<String, Any> {
                    finalModel = nil
                    if valueDictionary.safeValueForKey("Code") as? Int == 200 {
                         finalModel = model
                        if let stringValue = valueDictionary.safeValueForKey("Data") as? String {
                            let collection = OKBaseController.convertToArrDictionary(text: stringValue)
                                
                            let jsonString = OKBaseController().JSONStringFromAnyObject(value: collection![0] as AnyObject)
                                if let dataJson = jsonString.data(using: .utf8) {
                                    do {
                                        let categoryModel = try JSONDecoder().decode(OffersListModel.self, from: dataJson)
                                        finalModel?.value = categoryModel.value
                                        
                                        let  dataSaleFree =   CodeSnippets.getDiscountSaleFreeQty(discountAmount: Int(finalModel?.value ?? 0) ,totalAmount: Int(amount) ?? 0, productValue: Int(finalModel?.productValue ?? 0) , saleQty: finalModel?.buyingQty ?? 0, freeQty:  finalModel?.freeQty ?? 0)
                                        finalModel?.value = dataSaleFree.discountAmount
//                                        finalModel?.buyingQty = dataSaleFree.saleQuantity
//                                        finalModel?.freeQty = dataSaleFree.freeQuantity
                                        
                                    } catch {
                                        println_debug(error.localizedDescription)
                                    }
                                }
                                completion(finalModel)
                                finalModel = nil
                            
                        }
                    } else {
                        completion(finalModel)
                    }
                }
            }
        }
    }
    
    class func getHeadersTitle(_ completion: @escaping (_ response: [OffersCategoryModel]?) -> Void) {
        
        let offersHeaderURL = Url.offersCategory + "Simid=\(simid)&MobileNumber=\(UserModel.shared.mobileNo)&MSID=\(getMsid())&OSType=1&OffSet=0&OTP=\(uuid)&Limit=100"
        
        guard let offersURL = offersHeaderURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        
        let url = getUrl(urlStr: offersURL, serverType: .serverApp)
        
        let param = Dictionary<String,Any>()
        TopupWeb.genericClass(url: url, param: param as AnyObject, httpMethod: "GET") { (response, success) in
            DispatchQueue.main.async {
                if !success { return }
                var collections : [OffersCategoryModel]?
                if let valueDictionary = response as? Dictionary<String, Any> {
                    if valueDictionary.safeValueForKey("Code") as? Int == 200 {
                        if let stringValue = valueDictionary.safeValueForKey("Data") as? String {
                            if let collection = OKBaseController.convertToArrDictionary(text: stringValue) as? [Dictionary<String,Any>] {
                                
                                collections = nil
                                collections = [OffersCategoryModel]()

                                for item in collection {
                                    let jsonString = OKBaseController().JSONStringFromAnyObject(value: item as AnyObject)
                                    if let dataJson = jsonString.data(using: .utf8) {
                                        do {
                                            let categoryModel = try JSONDecoder().decode(OffersCategoryModel.self, from: dataJson)
                                            collections?.append(categoryModel)
                                        } catch {
                                            println_debug(error.localizedDescription)
                                        }
                                    }
                                }
                                completion(collections)
                                collections = nil
                            }
                        }
                    }
                }
            }
        }
    }
  
    
    class func getAllOffers(_ completion: @escaping (_ response: [OffersListModel]?) -> Void) {
      
      //Dynamic Url
      let  offerDetails = "/RestService.svc/GetAllPromotionCodes?Limit=100&MobileNumber=\(UserModel.shared.mobileNo)&BalanceAmount=\(UserLogin.shared.walletBal)&OffSet=0&OSType=1&OTP=0&MSID=\(getMsid())&Simid=\(simid)&AndroidbuildNumber=60&Lac=123&Mnc=\(mccStatus.mnc)&Mac=123&Lat=\(geoLocManager.currentLatitude ?? "0.0")&Long=\(geoLocManager.currentLongitude ?? "0.0")&GDivCode=\(geoLocManager.currentDivCode ?? "YANGDIVI")&GTownshipCode=\(geoLocManager.currentTownshipCode ?? "KAMARYOU")&GcityName=\(geoLocManager.currentCityName ?? "123")&CellId=123"
      let url = getUrl(urlStr: offerDetails, serverType: .serverApp)

        println_debug(url)
        
        let param = Dictionary<String,Any>()
        
        TopupWeb.genericClass(url: url, param: param as AnyObject, httpMethod: "GET") { (response, success) in
            DispatchQueue.main.async {
                
                if !success { return }
                
                var offersData : [OffersListModel]?
                
                offersData = [OffersListModel]()
                
                if let valueDictionary = response as? Dictionary<String, Any> {
                    if valueDictionary.safeValueForKey("Code") as? Int == 200 {
                        if let stringCollections = valueDictionary.safeValueForKey("Data") as? String {
                            if let collections = OKBaseController.convertToArrDictionary(text: stringCollections) as? [Dictionary<String,Any>] {
                                for element in collections {
                                    let jsonString = OKBaseController().JSONStringFromAnyObject(value: element as AnyObject)
                                    if let dataJson = jsonString.data(using: .utf8) {
                                        do {
                                            let categoryModel = try JSONDecoder().decode(OffersListModel.self, from: dataJson)
                                            offersData?.append(categoryModel)
                                        } catch {
                                            println_debug(error.localizedDescription)
                                        }
                                    }
                                }
                                completion(offersData)
                                offersData?.removeAll()
                                offersData = nil
                            }
                        }
                    }
                }
            }
        }
    }
    
    class func generateDynamicControllers(records: [OffersCategoryModel]?,collections: [OffersListModel], _ completion: @escaping (_ controllers: [UIViewController]?) -> Void) {
        
        if records == nil { completion(nil) ; return }
        
        var viewControllers : [UIViewController]?
        
        viewControllers = [UIViewController]()
        
        for element in records! {
            
            guard let firstController = UIStoryboard.init(name: "Offers", bundle: nil).instantiateViewController(withIdentifier: "OffersSelectionViewController") as? OffersSelectionViewController else {
                completion(nil)
                return
            }
            
            firstController.list = collections.filter({$0.categoryName.safelyWrappingString().uppercased() == element.categoryName.uppercased()})
            
            if element.categoryName.uppercased() == "ALL OFFERS".uppercased() {
                firstController.list = collections
            }
          
          if appDel.currentLanguage == "my" {
            firstController.title = element.categoryBName
          }
          else {
            firstController.title = element.categoryName
          }
          
            viewControllers?.append(firstController)
            
        }
        
        completion(viewControllers)
        
        viewControllers = nil
        
    }
}
