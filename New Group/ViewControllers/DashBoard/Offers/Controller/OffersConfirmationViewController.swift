//
//  OffersConfirmationViewController.swift
//  OK
//
//  Created by Ashish on 1/3/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class OffersConfirmationViewController : OKBaseController, UITableViewDelegate, UITableViewDataSource, BioMetricLoginDelegate {
    
    @IBOutlet weak var offerTable : UITableView!
    @IBOutlet weak var payBtn     : UIButton!
    {
      didSet
      {
        payBtn.setTitle("Pay".localized, for: .normal)
      }
    }
    
    var offerConfirmModel : OfferConfirmationScreen?
    var timerView =  TimerView.updateView()
    var seconds        = 60
    
    var timer          = Timer()
    
    var isTimerRunning = false
    var isConditional = false

    //MARK:- View life cycle
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        timerView.removeFromSuperview()
        timer.invalidate()
        println_debug("Timer Invalidated")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Confirmation".localized
        
        timerView.wrapTimerView(timer: "0:60")
        self.offerTable.tableFooterView = UIView()

        self.timerPay()
        self.offerTable.reloadData()
        
        self.loadBackButton()
        DispatchQueue.global(qos: .background).async {
            guard let modelData = self.offerConfirmModel else { return }
            self.callingGenericPaymentApi(modelData)//To check promo code and amount is related with same offer or not
        }
    }
  
    func updateTimerViewAccordingtoLayout() {
        
        timerView.changeFrameForMulti()
        
        if let keyWindow = UIApplication.shared.keyWindow {
            keyWindow.makeKeyAndVisible()
            keyWindow.addSubview(timerView)
        }
        
    }
    
    func timerPay() {
         timerView.secondstrailingcontraint.isActive = false
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(OffersConfirmationViewController.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        
        seconds -= 1
        
        let timerString = (seconds <= 9) ? "0:" + "0" + "\(seconds)" :  "0:" + "\(seconds)"
        
        timerView.wrapTimerView(timer: timerString)
        if seconds == 0 {
            if timer.isValid {
                timer.invalidate()
                alertViewObj.wrapAlert(title: nil, body: "Session expired".localized, img:#imageLiteral(resourceName: "logout_st"))
                alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
                    self.navigationController?.popViewController(animated: true)
                })
                alertViewObj.showAlert(controller: self)
            }
        } else if seconds <= 10 {
            timerView.wrapTimerViewUpdate(color: UIColor.red, alpha: 1.00)
            UIView.animate(withDuration: 0.2, animations: {
                self.timerView.wrapTimerViewUpdate(color: UIColor.red, alpha: 0.00)
            }) { (bool) in
                self.timerView.wrapTimerViewUpdate(color: UIColor.red, alpha: 1.00)
            }
        }
    }
    
  //MARK:- Table View Delegate
  //paymentType = 0 ==Direct & 1 = InDirect
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    // for timer addition in cell
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 1{
        return timerView
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 1{
        return 80.00
        }
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
          if offerConfirmModel?.offers?.paymentType == "1" {//InDirect
            if offerConfirmModel?.offers?.amountType == "0" || offerConfirmModel?.offers?.amountType == "1"
            {
            if offerConfirmModel?.remarks?.count ?? 0 > 0 {
              return 11
            }
            return 10
            } else if offerConfirmModel?.offers?.amountType == "3" || offerConfirmModel?.offers?.amountType == "4"//FreeWAP
            {
                if offerConfirmModel?.remarks?.count ?? 0 > 0 {
                    return 15
                }
                return 14
            } else if offerConfirmModel?.offers?.amountType == "2" {
                if offerConfirmModel?.remarks?.count ?? 0 > 0 {//Free
                    return 13
                }
                return 12
            } else if offerConfirmModel?.offers?.amountType == "6" || offerConfirmModel?.offers?.amountType == "7"//Product WAP
            {
                if offerConfirmModel?.remarks?.count ?? 0 > 0 {
                    return 13
                }
                return 12
            } else   {//Product
                if offerConfirmModel?.remarks?.count ?? 0 > 0 {
                    return 11
                }
                return 10
            }
            
          }
          else//Direct
          {
            if offerConfirmModel?.offers?.amountType == "0" || offerConfirmModel?.offers?.amountType == "1"
            {
                if offerConfirmModel?.remarks?.count ?? 0 > 0 {
                    return 7
                }
                return 6
            } else if offerConfirmModel?.offers?.amountType == "3" || offerConfirmModel?.offers?.amountType == "4"//FreeWAP
            {
                if offerConfirmModel?.remarks?.count ?? 0 > 0 {
                    return 11
                }
                return 10
            } else if offerConfirmModel?.offers?.amountType == "2" {
                if offerConfirmModel?.remarks?.count ?? 0 > 0 {//Free
                    return 8
                }
                return 7
            } else if offerConfirmModel?.offers?.amountType == "6" || offerConfirmModel?.offers?.amountType == "7"//Product WAP
            {
                if offerConfirmModel?.remarks?.count ?? 0 > 0 {
                    return 9
                }
                return 8
            } else   {//Product
                if offerConfirmModel?.remarks?.count ?? 0 > 0 {
                    return 7
                }
                return 6
            }
            
            }
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: OfferConfirmationTableViewCell.identifier, for: indexPath) as? OfferConfirmationTableViewCell
            cell?.wrapLabel(offer: offerConfirmModel, withIndex: indexPath.row)
            return cell!
        } else {
            
        }
        
        return UITableViewCell()
    }
    
    //MARK: BioMetric Verification
    
    @IBAction func payAction(_ sender: UIButton) {
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "offerPay", delegate: self)
        } else {
            if offerConfirmModel?.offers?.promotionTransactionCount == 2 {
                self.genericPayment()
            } else {
                self.oneTranscationPayment()
            }
        }
    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if screen == "offerPay" {
        if isSuccessful {
            if offerConfirmModel?.offers?.promotionTransactionCount == 2 {
                self.genericPayment()
            } else {
                self.oneTranscationPayment()
            }
        }
        }
    }
    
    //MARK: API Methods
    
    func oneTranscationPayment() {
        
        let location = OfferRequestLGeoLocation.init(cellId: "", latitude: geoLocManager.currentLatitude, longitude: geoLocManager.currentLongitude)
        let externalRef = OfferRequestLExternalReference.init(direction: true, endStation: "", startStation: "", vendorId: "")
        
        var wifiName = ""
        var ssid = ""
        var mac = ""
        if UitilityClass.isConnectedToWifi(){
            let wifiInfo = UitilityClass.getNetworkInfos()[0].components(separatedBy: ",")
            
            wifiName = wifiInfo[0]
            ssid = wifiInfo[0]
            mac = wifiInfo[1]
        }

        let cellTower = OfferRequestTransactionsCellTower.init(lac: "", mac: mac, mcc: mccStatus.mcc, mnc: mccStatus.mnc, signalStrength: "", ssid: ssid, connectedwifiName: wifiName, networkType: UitilityClass.getNetworkType(), networkSpeed: UitilityClass.getSignalStrength())
        let prox      = OfferRequestLProximity.init(blueToothUsers: [], selectionMode: false, wifiUsers: [], mBltDevice: "", mWifiDevice: "")
        //Gender Display
        var strGender = ""
        if UserModel.shared.gender == "1" {
            strGender = "M"
        } else {
            strGender = "F"
        }
        
        //Company name
        var companyName = ""
        if appDel.currentLanguage == "my" {
            companyName = offerConfirmModel?.offers?.companyBName ?? ""
        }
        else if appDel.currentLanguage == "zh-Hans" {
            companyName = offerConfirmModel?.offers?.companyCName ?? ""
        }
        else {
            companyName = offerConfirmModel?.offers?.companyName ?? ""
        }
        //Product name
        var productName = ""
        if offerConfirmModel?.offers?.amountType == "2" || offerConfirmModel?.offers?.amountType == "3" || offerConfirmModel?.offers?.amountType == "4" {
            if appDel.currentLanguage == "my" {
                productName = offerConfirmModel?.offers?.productBName ?? ""
            }
            else if appDel.currentLanguage == "zh-Hans" {
                productName = offerConfirmModel?.offers?.productCName ?? ""
            }
            else {
                productName = offerConfirmModel?.offers?.productName ?? ""
            }
        }
        
        let number = offerConfirmModel?.mobileNumber?.replacingOccurrences(of: "0095", with: "0") ?? ""
        
        //Merchant Mobile Number
        let destinationNumber = self.getDestinationNum(number, withCountryCode: "+95")//offerConfirmModel?.mobileNumber
        var promotionAgentMobileNumber = ""
        
        //Merchant Number
        //let destinationNumber = self.getDestinationNum(offerConfirmModel?.mobileNumber ?? "",withCountryCode: "+95")//offerConfirmModel?.mobileNumber
        //var promotionAgentMobileNumber = ""
        if isConditional{
            promotionAgentMobileNumber = destinationNumber
        }
        
        //Old Comment
//        let firstPart_comments = String(format: "%@[#OK-PMC,%@,%@,%@,%d,%@,%@,%@,%d,%d,%d,%@,%@,%@,%@,%@,%@,%@,##%@##%@##]",offerConfirmModel?.remarks ?? "", offerConfirmModel?.offers?.promotionCode ?? "",offerConfirmModel?.offers?.amountType ?? "",offerConfirmModel?.offers?.paymentType ?? "",offerConfirmModel?.offers?.value ?? 0,offerConfirmModel?.amount ?? 0,  offerConfirmModel?.offers?.promotionType ?? "",
//                                        companyName,offerConfirmModel?.offers?.buyingQty ?? 0,offerConfirmModel?.offers?.freeQty ?? 0,offerConfirmModel?.offers?.productValidationRuleValue ?? 0,productName,geoLocManager.currentLatitude, geoLocManager.currentLongitude, "", strGender, UserModel.shared.ageNow, UserModel.shared.address2, offerConfirmModel?.merchant ?? 0, offerConfirmModel?.businessName ?? 0)

        let firstPart_comments = String(format: "%@[#OK-PMC,%@,%@,%@,%d,%@,%@,%@,%d,%d,%d,%@,%d,%@,%@,%@,%@,%@,%@,%@,%@,%@,##%@##%@##%@##]",offerConfirmModel?.remarks ?? "", offerConfirmModel?.offers?.promotionCode ?? "",offerConfirmModel?.offers?.amountType ?? "",offerConfirmModel?.offers?.paymentType ?? "",offerConfirmModel?.offers?.value ?? 0,offerConfirmModel?.amount ?? 0,  offerConfirmModel?.offers?.promotionType ?? "",
                                        companyName,offerConfirmModel?.offers?.buyingQty ?? 0,offerConfirmModel?.offers?.freeQty ?? 0,offerConfirmModel?.offers?.productValidationRuleValue ?? 0,productName,offerConfirmModel?.offers?.promotionTransactionCount ?? 0,offerConfirmModel?.offers?.freeProductName ?? "",UserModel.shared.mobileNo,destinationNumber,geoLocManager.currentLatitude, geoLocManager.currentLongitude, "", strGender, UserModel.shared.ageNow, UserModel.shared.address2, offerConfirmModel?.merchant ?? "", offerConfirmModel?.businessName ?? "",UserModel.shared.businessName)
        
        let secondPart_comments = String(format: "-OKNAME-%@", UserModel.shared.name)
        
        let comments = firstPart_comments + secondPart_comments
        
        var promoAppliedAmount = 0
        //Net Amount
        let offersModel = offerConfirmModel?.offers
        let amount = Int(offerConfirmModel?.amount ?? "0") ?? 0
        let value = Int(offersModel?.value ?? 0)
        
        if offersModel?.amountType == "0" || offersModel?.amountType == "3"//Fixed=Percentage calculate
        {
            promoAppliedAmount = amount - value
        } else if offersModel?.amountType == "1" || offersModel?.amountType == "4"{ //Fixed data calculate
            
            let percentageValue = Int((value * amount) / 100)
            promoAppliedAmount = amount - percentageValue
        } else {
            promoAppliedAmount = Int(offerConfirmModel?.amount ?? "0") ?? 0
        }
        
        let transaction = OfferRequestOneTransactions.init(amount: "\(promoAppliedAmount)", balance: "", bonusPoint: 0, businessName: offerConfirmModel?.businessName, cashBackFlag: 1, comments: comments, destination: destinationNumber, destinationNumberWalletBalance: "", discountPayTo: 1, isMectelTopUp: false, isPromotionApplicable: "0", kickBack: "0.0", kickBackMsisdn: "", localTransactionType: "PAYTO", merchantName: "", mobileNumber: UserModel.shared.mobileNo, mode: true, password: ok_password ?? "", productCode: offerConfirmModel?.offers?.promotionCode, promoCodeId: offerConfirmModel?.offers?.promotionCode, resultCode: 0, resultDescription: "", secureToken: "", transactionId: "", transactionTime: "", transactionType: "PAYTO", accounType: offerConfirmModel?.accountType, senderBusinessName: offerConfirmModel?.businessName, userName: "", promoActualAmount: offerConfirmModel?.amount, promotionAgentMobileNumber: promotionAgentMobileNumber, promotionTransactionCount: offerConfirmModel?.offers?.promotionTransactionCount)
        
        let requestModel = OfferOneTRequest(transactionsCellTower: cellTower, lExternalReference: externalRef, lGeoLocation: location, lProximity: prox, lTransactions: transaction)
        
        let url = getUrl(urlStr: Url.offerOneTransactionPayment, serverType: .serverApp)
        
        do {
            let encode = try JSONEncoder().encode(requestModel)
            TopupWeb.genericApiWithHeader(url: url, param: Dictionary<String,Any>() as AnyObject, httpMethod: "POST", header: nil, data: encode) { (response, success) in
                if success {
                    if let dictionary = response as? Dictionary<String,Any> {
                        if let code = dictionary.safeValueForKey("Code") as? Int, code == 200, let rawDataString = dictionary.safeValueForKey("Data") as? String {
                            
                            var xmlDict = Dictionary<String,Any>()
                            let xml = SWXMLHash.parse(rawDataString)
                            func enumerateMore(indexer: XMLIndexer) {
                                for child in indexer.children {
                                    xmlDict[child.element!.name] = child.element!.text
                                    enumerateMore(indexer: child)
                                }
                            }
                            
                            enumerateMore(indexer: xml)
                            
                            guard let rawData = xmlDict.jsonString().data(using: .utf8) else { return }
                            
                            DispatchQueue.main.async {
                                guard  let offerSucccess = self.storyboard?.instantiateViewController(withIdentifier: "OffersRecieptViewController") as? OffersRecieptViewController else { return }
                                do {
                                    let encode = try JSONDecoder().decode(OfferRecieptModel.self, from: rawData)
                                    offerSucccess.recieptModel = encode
                                    offerSucccess.offerModel   = self.offerConfirmModel
                                    self.navigationController?.pushViewController(offerSucccess, animated: true)
                                } catch {
                                    println_debug(error)
                                }
                            }
                        } else {
                            //Error msg
                            DispatchQueue.main.async(){
                                
                                alertViewObj.wrapAlert(title: nil, body: dictionary.safeValueForKey("Msg") as? String ?? "", img: #imageLiteral(resourceName: "NoOffersFound"))
                                
                                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                    
                                }
                                
                                alertViewObj.showAlert()
                            }
                        }
                    }
                }
            }
        } catch {
            println_debug(error)
        }
    }
    
    func genericPayment() {
        
        let location = OfferRequestLGeoLocation.init(cellId: "", latitude: geoLocManager.currentLatitude, longitude: geoLocManager.currentLongitude)
        let externalRef = OfferRequestLExternalReference.init(direction: true, endStation: "", startStation: "", vendorId: "")
        
        var wifiName = ""
        var ssid = ""
        var mac = ""
        if UitilityClass.isConnectedToWifi(){
            let wifiInfo = UitilityClass.getNetworkInfos()[0].components(separatedBy: ",")
            
            wifiName = wifiInfo[0]
            ssid = wifiInfo[0]
            mac = wifiInfo[1]
        }
        
        let cellTower = OfferRequestTransactionsCellTower.init(lac: "", mac: mac, mcc: mccStatus.mcc, mnc: mccStatus.mnc, signalStrength: "", ssid: ssid, connectedwifiName: wifiName, networkType: UitilityClass.getNetworkType(), networkSpeed: UitilityClass.getSignalStrength())
        
        let prox      = OfferRequestLProximity.init(blueToothUsers: [], selectionMode: false, wifiUsers: [], mBltDevice: "", mWifiDevice: "")
        //Gender Display
        var strGender = ""
        if UserModel.shared.gender == "1" {
            strGender = "M"
        } else {
            strGender = "F"
        }
 
        //Company name
        var companyName = ""
        if appDel.currentLanguage == "my" {
            companyName = offerConfirmModel?.offers?.companyBName ?? ""
        }
        else if appDel.currentLanguage == "zh-Hans" {
            companyName = offerConfirmModel?.offers?.companyCName ?? ""
        }
        else {
            companyName = offerConfirmModel?.offers?.companyName ?? ""
        }
        //Product name
        var productName = ""
        if offerConfirmModel?.offers?.amountType == "2" || offerConfirmModel?.offers?.amountType == "3" || offerConfirmModel?.offers?.amountType == "4" {
        if appDel.currentLanguage == "my" {
            productName = offerConfirmModel?.offers?.productBName ?? ""
        }
        else if appDel.currentLanguage == "zh-Hans" {
            productName = offerConfirmModel?.offers?.productCName ?? ""
        }
        else {
            productName = offerConfirmModel?.offers?.productName ?? ""
        }
        }
        
        let number = offerConfirmModel?.mobileNumber?.replacingOccurrences(of: "0095", with: "0") ?? ""

        //Merchant Mobile Number
        let destinationNumber = self.getDestinationNum(number, withCountryCode: "+95")//offerConfirmModel?.mobileNumber
        var promotionAgentMobileNumber = ""
        if isConditional{
            promotionAgentMobileNumber = destinationNumber
        }

//        let firstPart_comments = String(format: "%@[#OK-PMC,%@,%@,%@,%d,%@,%@,%@,%d,%d,%d,%@,%@,%@,%@,%@,%@,%@,##%@##%@##]",offerConfirmModel?.remarks ?? "", offerConfirmModel?.offers?.promotionCode ?? "",offerConfirmModel?.offers?.amountType ?? "",offerConfirmModel?.offers?.paymentType ?? "",offerConfirmModel?.offers?.value ?? 0,offerConfirmModel?.amount ?? 0,  offerConfirmModel?.offers?.promotionType ?? "",
//    companyName,offerConfirmModel?.offers?.buyingQty ?? 0,offerConfirmModel?.offers?.freeQty ?? 0,offerConfirmModel?.offers?.productValidationRuleValue ?? 0,productName,geoLocManager.currentLatitude, geoLocManager.currentLongitude, "", strGender, UserModel.shared.ageNow, UserModel.shared.address2, offerConfirmModel?.merchant ?? 0, offerConfirmModel?.businessName ?? 0)
 
        let firstPart_comments = String(format: "%@[#OK-PMC,%@,%@,%@,%d,%@,%@,%@,%d,%d,%d,%@,%d,%@,%@,%@,%@,%@,%@,%@,%@,%@,##%@##%@##%@##]",offerConfirmModel?.remarks ?? "", offerConfirmModel?.offers?.promotionCode ?? "",offerConfirmModel?.offers?.amountType ?? "",offerConfirmModel?.offers?.paymentType ?? "",offerConfirmModel?.offers?.value ?? 0,offerConfirmModel?.amount ?? 0,  offerConfirmModel?.offers?.promotionType ?? "",
                                        companyName,offerConfirmModel?.offers?.buyingQty ?? 0,offerConfirmModel?.offers?.freeQty ?? 0,offerConfirmModel?.offers?.productValidationRuleValue ?? 0,productName,offerConfirmModel?.offers?.promotionTransactionCount ?? 0,offerConfirmModel?.offers?.freeProductName ?? "",UserModel.shared.mobileNo,destinationNumber,geoLocManager.currentLatitude, geoLocManager.currentLongitude,"", strGender, UserModel.shared.ageNow, UserModel.shared.address2, offerConfirmModel?.merchant ?? "", offerConfirmModel?.businessName ?? "",UserModel.shared.businessName)
        
        let secondPart_comments = String(format: "-OKNAME-%@", UserModel.shared.name)
        
        let comments = firstPart_comments + secondPart_comments
        
        var promoAppliedAmount = 0
        //Net Amount
        let offersModel = offerConfirmModel?.offers
        let amount = Int(offerConfirmModel?.amount ?? "0") ?? 0
        let value = Int(offersModel?.value ?? 0)
        
        if offersModel?.amountType == "0" || offersModel?.amountType == "3"//Fixed=Percentage calculate
        {
           promoAppliedAmount = amount - value
        } else if offersModel?.amountType == "1" || offersModel?.amountType == "4"{ //Fixed data calculate
            
            let percentageValue = Int((value * amount) / 100)
            promoAppliedAmount = amount - percentageValue
        } else {
            promoAppliedAmount = Int(offerConfirmModel?.amount ?? "0") ?? 0
        }
 
        let transaction = OfferRequestLTransactions.init(amount: "\(promoAppliedAmount)", balance: "", bonusPoint: 0, businessName: offerConfirmModel?.businessName, cashBackFlag: 1, comments: comments, destination: destinationNumber, destinationNumberWalletBalance: "", discountPayTo: 1, isMectelTopUp: false, isPromotionApplicable: "0", kickBack: "0.0", kickBackMsisdn: "", localTransactionType: "PAYTO", merchantName: "", mobileNumber: UserModel.shared.mobileNo, mode: true, password: ok_password ?? "", productCode: offerConfirmModel?.offers?.promotionCode, promoCodeId: offerConfirmModel?.offers?.promotionCode, resultCode: 0, resultDescription: "", secureToken: "", transactionId: "", transactionTime: "", transactionType: "PAYTO", accounType: offerConfirmModel?.accountType, senderBusinessName: offerConfirmModel?.businessName, userName: "", promoActualAmount: offerConfirmModel?.amount, promotionAgentMobileNumber: promotionAgentMobileNumber,DestinationNumberAccountType: "")
        
        let requestModel = OfferRequest(transactionsCellTower: cellTower, lExternalReference: externalRef, lGeoLocation: location, lProximity: prox, lTransactions: transaction)
        
        let url = getUrl(urlStr: Url.genericPayment, serverType: .serverApp) 
        
        do {
            let encode = try JSONEncoder().encode(requestModel)
            TopupWeb.genericApiWithHeader(url: url, param: Dictionary<String,Any>() as AnyObject, httpMethod: "POST", header: nil, data: encode) { (response, success) in
                if success {
                    if let dictionary = response as? Dictionary<String,Any> {
                        if let code = dictionary.safeValueForKey("Code") as? Int, code == 200, let rawDataString = dictionary.safeValueForKey("Data") as? String {
                            
                            var xmlDict = Dictionary<String,Any>()
                            let xml = SWXMLHash.parse(rawDataString)
                            func enumerateMore(indexer: XMLIndexer) {
                                for child in indexer.children {
                                    xmlDict[child.element!.name] = child.element!.text
                                    enumerateMore(indexer: child)
                                }
                            }
                            
                            enumerateMore(indexer: xml)
                            
                            guard let rawData = xmlDict.jsonString().data(using: .utf8) else { return }
                            
                            DispatchQueue.main.async {
                                guard  let offerSucccess = self.storyboard?.instantiateViewController(withIdentifier: "OffersRecieptViewController") as? OffersRecieptViewController else { return }
                                do {
                                    let encode = try JSONDecoder().decode(OfferRecieptModel.self, from: rawData)
                                    offerSucccess.recieptModel = encode
                                    offerSucccess.offerModel   = self.offerConfirmModel
                                    self.navigationController?.pushViewController(offerSucccess, animated: true)
                                } catch {
                                    println_debug(error)
                                }
                            }
                        } else {
                            //Error msg
                            DispatchQueue.main.async(){

                            alertViewObj.wrapAlert(title: nil, body: dictionary.safeValueForKey("Msg") as? String ?? "", img: #imageLiteral(resourceName: "NoOffersFound"))
                            
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                               
                            }
                            
                            alertViewObj.showAlert()
                            }
                        }
                    }
                }
            }
        } catch {
            println_debug(error)
        }
    }
    
    func callingGenericPaymentApi(_ model: OfferConfirmationScreen) {
        guard let promoID =  offerConfirmModel?.offers?.promotionID else { return }
        guard let amount  =  offerConfirmModel?.amount else { return }
        guard let lat     = geoLocManager.currentLatitude else { return }
        guard let long    = geoLocManager.currentLongitude else { return }
        
        //Dynamic Url
        let  stringURL =  String(format: Url.verifyCustomerByPromoID, simid, UserModel.shared.mobileNo, promoID, UserLogin.shared.walletBal, msid, otp, amount, long, lat)
        let url = getUrl(urlStr: stringURL, serverType: .serverApp)
        
        println_debug(url)
        
        TopupWeb.genericClass(url: url, param: Dictionary<String,Any>() as AnyObject, httpMethod: "GET") { [weak self](response, success) in
            if success {
                if let dictionary = response as? Dictionary<String,Any> {
                    if let code = dictionary.safeValueForKey("Code") as? Int, code == 200 {
                        if var data = dictionary.safeValueForKey("Data") as? String {
                            data = data.replacingOccurrences(of: "\"", with: "")
                            if data.lowercased() == "true".lowercased() {
                                println_debug(response)
                                self?.isConditional = true
                            } else {
                                alertViewObj.wrapAlert(title: nil, body: "This promo code offer not valid for your entered amount.".localized, img: #imageLiteral(resourceName: "NoOffersFound"))
                                
                                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                    self?.navigationController?.popViewController(animated: true)
                                }
                                
                                alertViewObj.showAlert()
                            }
                        }
                    }
                }
            }
        }
        
    }
    
}
