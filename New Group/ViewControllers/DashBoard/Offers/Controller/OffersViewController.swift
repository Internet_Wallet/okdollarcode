//
//  OffersViewController.swift
//  OK
//
//  Created by Ashish on 7/13/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol OffersDashboardSyncDelegate : class {
    func presentController(controller: UIViewController)
}

class OffersViewController: OKBaseController {
    
    //MARK: - Outlet
    @IBOutlet weak var lblNoOffers: UILabel! {
        didSet {
            lblNoOffers.font = UIFont(name: appFont, size: 18)
            lblNoOffers.text = "No Offers Found !!".localized
        }
    }
    @IBOutlet weak var nooffericon: UIImageView!
    
    //MARK: - Properties
    var navStatus : String = ""
    fileprivate var offersMenu : PaytoScroller?
    var headerTitleModel : [OffersCategoryModel]?
    var delegate : OffersDashboardSyncDelegate?
    var selectedList : OffersListModel?
    var backOption : OfferSuccessPageDelegate?
    private lazy var searchBar = UISearchBar()
    //First
    var oneTimeLoad = 0
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
       // if navStatus == "InstaPay" {
            self.setupNav()
        //}
    }
    
    private func setupNav() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let label = UILabel()
        label.text = "Offers".localized
        label.font = UIFont.init(name: appFont, size: 17)
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.frame = CGRect(x: 0, y: ypos, width: titleViewSize.width-30, height: 30)
        navTitleView.addSubview(label)

        let locButton = UIButton(type: .custom)
        locButton.layer.borderColor = UIColor.white.cgColor
        locButton.setImage(UIImage(named: "tabBarBack"), for: .normal)
        locButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        locButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        navTitleView.addSubview(locButton)
       
        self.navigationItem.titleView = navTitleView
    }
    
    @objc func backButtonAction() {
        //self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        nooffericon.isHidden = true
         lblNoOffers.text = ""
        GeoLocationManager.shared.getAddressFrom(lattitude: geoLocManager.currentLatitude, longitude:  geoLocManager.currentLongitude,language: ok_default_language) { (isSuccess, dic) in
                self.initialSetup()
            
            }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        oneTimeLoad = 0
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        GeoLocationManager.shared.stopUpdateLocation()
        self.offersMenu?.controllerArray.removeAll()
        self.offersMenu = nil
    }
    
    //MARK:- Methods
    private func initialSetup() {
        if oneTimeLoad == 0 {
            oneTimeLoad = 1
            OffersManager.getHeadersTitle ({ [weak self](collections) in
                if collections == nil {
                    
                    self?.nooffericon.isHidden = false
                    self?.lblNoOffers.text = "No Offers Found !!".localized
                    return
                    
                }
                if collections!.count <= 0 {
                    self?.nooffericon.isHidden = false
                    self?.lblNoOffers.text = "No Offers Found !!".localized
                    
                    return
                    
                }
                OffersManager.getAllOffers({ [weak self](objects) in
                    if objects == nil {
                        self?.nooffericon.isHidden = false
                        self?.lblNoOffers.text = "No Offers Found !!".localized
                        return
                        
                    }
                    if objects!.count <= 0 {
                        self?.nooffericon.isHidden = false
                        self?.lblNoOffers.text = "No Offers Found !!".localized
                        return
                        
                    }
                    DispatchQueue.main.async {
                        
                        guard let secondView = self?.storyboard?.instantiateViewController(withIdentifier: "OffersSelectionViewController") as? OffersSelectionViewController else { return }
                    
                        secondView.categoryList = collections
                        secondView.list = objects
                        secondView.title = "Offers".localized
                        secondView.delegate = self
                        self?.addChild(secondView)
                        var childFrame = secondView.view.frame
                        if let frameSize = self?.view.frame {
                            childFrame = frameSize
                        }
                        secondView.view.frame = childFrame
                        self?.view.addSubview(secondView.view)
                        secondView.didMove(toParent: self)
                        
                    }
                })
            })
        }
    }
    
    func customDeinit() {
        self.offersMenu?.controllerArray.removeAll()
        self.offersMenu?.view.removeFromSuperview()
        self.offersMenu = nil
    }
    
    func callingGenericPaymentApi(_ model: OffersListModel) {
        guard let promoID = model.promotionID else { return }
        guard let amount  = model.value else { return }
        guard let lat     = geoLocManager.currentLatitude else { return }
        guard let long    = geoLocManager.currentLongitude else { return }

        let amountString = String.init(format: "%d", amount)

        //Dynamic Url
        let  stringURL =  String(format: Url.verifyCustomerByPromoID, simid, UserModel.shared.mobileNo, promoID, UserLogin.shared.walletBal, msid, otp, amountString, long, lat)
        let url = getUrl(urlStr: stringURL, serverType: .serverApp)
        
        println_debug(url)
        
        TopupWeb.genericClass(url: url, param: Dictionary<String,Any>() as AnyObject, httpMethod: "GET") { [weak self](response, success) in
            if success {
                if let dictionary = response as? Dictionary<String,Any> {
                    if let code = dictionary.safeValueForKey("Code") as? Int, code == 200 {
                        if var data = dictionary.safeValueForKey("Data") as? String {
                            data = data.replacingOccurrences(of: "\"", with: "")
                            if data.lowercased() == "true".lowercased() {
                                self?.callDirectRedeemOffersApi(model)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func callDirectRedeemOffersApi(_ model: OffersListModel) {
        guard let request = getJsonRequestData(model) else { return }
        let  stringURL =  String(format: Url.directPromoRedeem)
        let url = getUrl(urlStr: stringURL, serverType: .serverApp)
        println_debug(url)
        TopupWeb.genericApiWithHeader(url: url, param: Dictionary<String,Any>() as AnyObject, httpMethod: "POST", header: nil, data: request) { (response, success) in
            if success {
                if let dictionary = response as? Dictionary<String,Any> {
                    if let code = dictionary.safeValueForKey("Code") as? Int, code == 200 {
                        let offerSucccess = self.storyboard?.instantiateViewController(withIdentifier: "OffersSuccessPageViewController") as? OffersSuccessPageViewController
                        offerSucccess?.delegate = self.backOption
                        guard let presentVC = offerSucccess else { return }
                        self.delegate?.presentController(controller: presentVC)
                    }
                }
            }
        }
    }
    
    private func getJsonRequestData(_ model: OffersListModel) -> Data? {
        // cell tower request
        
        var wifiName = ""
        var ssid = ""
        var mac = ""
        if UitilityClass.isConnectedToWifi(){
            let wifiInfo = UitilityClass.getNetworkInfos()[0].components(separatedBy: ",")
            
            wifiName = wifiInfo[0]
            ssid = wifiInfo[0]
            mac = wifiInfo[1]
        }
        
        let cellTower = OfferDirectPayTransactionsCellTower(lac: "", mac: mac, mcc: mccStatus.mcc, mnc: mccStatus.mnc, signalStrength: "", ssid: ssid, connectedwifiName: wifiName, networkType: UitilityClass.getNetworkType(), networkSpeed: UitilityClass.getSignalStrength() )
        
        let lexternalRef = OfferDirectPayLExternalReference.init(direction: true, endStation: "", startStation: "", vendorId: "")
        
        // locaton model
        let lat = geoLocManager.currentLatitude ?? "0.0"
        let long = geoLocManager.currentLongitude ?? "0.0"
        
        let geoLocation = OfferDirectPayLGeoLocation.init(cellId: "", latitude: lat, longitude: long)
        
        // proximity
        let proximity = OfferDirectPayLProximity.init(blueToothUsers: [], selectionMode: false, wifiUsers: [], mBltDevice: "", mWifiDevice: "")
        
        // transaction model
        
        let amount = ((model.value ?? 0) as NSNumber).stringValue
        let destination = (model.mobilenumber ?? "")
        
        let promoCodeID = model.promotionCode
        
        let promotionType = model.promotionType.safelyWrappingString()
        
        let amountType = model.amountType.safelyWrappingString()
        
        let promoName = model.promotionName.safelyWrappingString()
        
        let firsPartComments = String(format: "[#OK-PMC,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@]", promoCodeID.safelyWrappingString(), promotionType, amountType, promoName, lat, long, "", UserModel.shared.gender, UserModel.shared.ageNow, "")
        
        let stringSecondPart = String.init(format: "-OKNAME-%@", UserModel.shared.name)
        
        
        let comments = firsPartComments + stringSecondPart

        let transaction = OfferDirectPayLTransactions.init(amount: amount, balance: "", bonusPoint: 0, businessName: "", cashBackFlag: 1, comments: comments, destination: destination, destinationNumberWalletBalance: "", discountPayTo: 0, isMectelTopUp: false, isPromotionApplicable: "1", kickBack: "0.0", kickBackMsisdn: "", localTransactionType: "PAYTO", merchantName: "", mobileNumber: UserModel.shared.mobileNo, mode: true, password: ok_password, promoActualAmount: "", promoCodeId: promoCodeID, resultCode: 0, resultDescription: "", secureToken: UserLogin.shared.token, transactionId: "", transactionTime: "", transactionType: "PAYTO", accounType: "", senderBusinessName: "", userName: "Unknown")

        let requestModel = OfferDirectPayRequest(destinationNumberWalletBalance: "", transactionsCellTower: cellTower, lExternalReference: lexternalRef, lGeoLocation: geoLocation, lProximity: proximity, lTransactions: transaction)
        do {
            let data = try JSONEncoder().encode(requestModel)
            return data
        } catch {
            println_debug(error)
        }
        
        return nil
    }
}

//MARK:- BioMetricLoginDelegate
extension OffersViewController: BioMetricLoginDelegate {
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if screen == "offers" {
            if isSuccessful {
                guard let model = self.selectedList else { return }
                self.callingGenericPaymentApi(model)
            }
            else{
                nooffericon.isHidden = false
                lblNoOffers.text = "No Offers Found !!".localized
            }
        }
    }
}

//MARK:- OfferPresenter
extension OffersViewController: OfferPresenter {
    func presentController(vc: OffersTermsConditionViewController) {
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func directPaymentNavigation(_ model: OffersListModel) {
        if UserLogin.shared.loginSessionExpired {
            self.callingGenericPaymentApi(model)
        } else {
            selectedList = model
            OKPayment.main.authenticate(screenName: "offers", delegate: self)
        }
    }
    
    func paytoPaymentNavigation(model: OffersListModel,list:[OffersListModel]) {
        let payto = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PayToNavigation")
        payto.modalPresentationStyle = .fullScreen
        isPayToDashboard = false
        if let navigation = payto as? UINavigationController {
            if let firstVC = navigation.viewControllers.first {
                if let paytoVC = firstVC as? PayToViewController {
                    paytoVC.isOfferEnabled = true
                    paytoVC.offerModel     = model
                    paytoVC.offerListModel = list
                }
            }
        }
        if let delegate = self.delegate {
            delegate.presentController(controller: payto)
        } else {
            self.present(payto, animated: true, completion: nil)
        }
    }
}
