//
//  OfferConfirmationTableViewCell.swift
//  OK
//
//  Created by Ashish on 1/3/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class OfferConfirmationTableViewCell: UITableViewCell {
    
    static let identifier = String(describing: OfferConfirmationTableViewCell.self)
    
    @IBOutlet weak var keyLabel : UILabel!
    @IBOutlet weak var valueLabel : UILabel!
    
    func wrapLabel(offer: OfferConfirmationScreen?, withIndex index: Int) {
      
      if offer?.offers?.paymentType == "1"//InDirect
      {
        let offersModel = offer?.offers
        if offersModel?.amountType == "0" || offersModel?.amountType == "1"{
            switch index {
            case 0:
                let receiverNo = offer?.mobileNumber
                self.keyValueMapper(key: "Receiver Number", value: receiverNo)
            case 1:
                let receiverName = offer?.merchant ?? "Unknown"
                self.keyValueMapper(key: "Receiver Name", value: receiverName)
            case 2:
                let receiverName = offer?.accountType //self.agentTypeString()
                self.keyValueMapper(key: "Account Type", value: receiverName)
            case 3:
                let businessName = offer?.businessName ?? "Unknown"
                if businessName.count > 0 {
                    self.keyValueMapper(key: "Business Name", value: businessName)
                } else {
                    self.keyValueMapper(key: "Business Name", value: "Unknown")
                }
            case 4:
                self.keyValueMapper(key: "Promo Code", value: offersModel?.promotionCode)
            case 5:
                if appDel.currentLanguage == "my" {
                    self.keyValueMapper(key: "Company Name", value: offersModel?.categoryBName)
                }
                else if appDel.currentLanguage == "zh-Hans" {
                    self.keyValueMapper(key: "Company Name", value: offersModel?.categoryCName)
                }else {
                    self.keyValueMapper(key: "Company Name", value: offersModel?.categoryName)
                }
            case 6:
                //FIXEDAmount - 0 , Percentage - 1 , Free - 2 , FreeWithFixedAmount - 3 , FreeWithPercentage - 4
                if offersModel?.amountType == "0"//Fixed=Percentage calculate
                {
                    let amount = Float(offer?.amount ?? "0") ?? 0.0
                    let percentageAmount = Float(offersModel?.value ?? Int(0.0))
                    let percentageValue = Float((percentageAmount * 100) / amount)
                    self.keyValueMapper(key: "Discount Percentage", value:                     CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(percentageValue)") + " %")
                    
                } else if offersModel?.amountType == "1" { //Fixed value display
                    let value = Float(offersModel?.value ?? Int(0.0))
                    self.keyValueMapper(key: "Discount Percentage", value: self.getDigitDisplay(digit : value) + " %")
                }
            case 7:
                if offersModel?.amountType == "0" //Fixed=Percentage calculate
                {
                    let value = Float(offersModel?.value ?? Int(0.0))
                    let amont = wrapAmountWithCommaDecimal(key: "\(value)")
                    self.keyValueOfferMapper(key: "Discount Amount", value: wrapAmountWithOfferMMK(bal: amont))
                } else if offersModel?.amountType == "1" { //Fixed data calculate
                    let amount = Float(offer?.amount ?? "0") ?? 0.0
                    let percentageAmount = Float(offersModel?.value ?? Int(0.0))
                    let percentageValue = Float((percentageAmount * amount) / 100)
                    let amont = wrapAmountWithCommaDecimal(key: "\(percentageValue)")
                    self.keyValueOfferMapper(key: "Discount Amount", value: wrapAmountWithOfferMMK(bal: amont))
                }
            case 8:
                let amount = Float(offer?.amount ?? "0") ?? 0.0
                let amont = wrapAmountWithCommaDecimal(key: "\(amount)")
                self.keyValueOfferMapper(key: "Bill Amount", value: wrapAmountWithOfferMMK(bal: amont))
            case 9:
                let amount = Float(offer?.amount ?? "0") ?? 0.0
                let value = Float(offersModel?.value ?? Int(0.0))
                
                if offersModel?.amountType == "0"//Fixed=Percentage calculate
                {
                    let result = amount - value
                    let amont = wrapAmountWithCommaDecimal(key: "\(result)")
                    self.keyValueOfferMapper(key: "Net Payable Amount", value: wrapAmountWithOfferMMK(bal: amont))
                } else if offersModel?.amountType == "1" { //Fixed data calculate
                    
                    let percentageValue = Float((value * amount) / 100)
                    let result = amount - percentageValue
                    
                    let amont = wrapAmountWithCommaDecimal(key: "\(result)")
                    self.keyValueOfferMapper(key: "Net Payable Amount", value: wrapAmountWithOfferMMK(bal: amont))
                }
            case 10:
                let remarks = offer?.remarks ?? ""
                self.keyValueMapper(key: "Remarks", value: remarks)
            default:
                break
            }
        } else if offersModel?.amountType == "2" {//Free
            switch index {
                
            case 0:
                let receiverNo = offer?.mobileNumber
                self.keyValueMapper(key: "Receiver Number", value: receiverNo)
            case 1:
                let receiverName = offer?.merchant ?? "Unknown"
                self.keyValueMapper(key: "Receiver Name", value: receiverName)
            case 2:
                let receiverName = offer?.accountType //self.agentTypeString()
                self.keyValueMapper(key: "Account Type", value: receiverName)
            case 3:
                let businessName = offer?.businessName ?? "Unknown"
                if businessName.count > 0 {
                    self.keyValueMapper(key: "Business Name", value: businessName)
                } else {
                    self.keyValueMapper(key: "Business Name", value: "Unknown")
                }
            case 4:
                self.keyValueMapper(key: "Promo Code", value: offersModel?.promotionCode)
            case 5:
                self.keyValueMapper(key: "Company Name", value: offersModel?.companyName)
            case 6:
                self.keyValueMapper(key: "Product Name", value: offersModel?.productName)
            case 7:
                self.keyValueMapper(key: "Free Product Name", value: offersModel?.freeProductName)
            case 8:
              let buying = String(offersModel?.buyingQty ?? 0)
                 let amont = wrapAmountWithCommaDecimal(key: "\(buying)")
                self.keyValueMapper(key: "Buying Quantity", value: "\(amont)" )
            case 9:
                let freeQty = String(offersModel?.freeQty ?? 0)
                let amont = wrapAmountWithCommaDecimal(key: "\(freeQty)")
                self.keyValueMapper(key: "Free Quantity", value: "\(amont)")
            case 10:
                let amount = Float(offer?.amount ?? "0") ?? 0.0
                let amont = wrapAmountWithCommaDecimal(key: "\(amount)")
                self.keyValueOfferMapper(key: "Bill Amount", value: wrapAmountWithOfferMMK(bal: amont))
            case 11:
                let amount = Float(offer?.amount ?? "0") ?? 0.0
                let value = Float(offersModel?.value ?? Int(0.0))
                
                let result = amount - value
                let amont = wrapAmountWithCommaDecimal(key: "\(result)")
                self.keyValueOfferMapper(key: "Net Payable Amount", value: wrapAmountWithOfferMMK(bal: amont))
            case 12:
                let remarks = offer?.remarks ?? ""
                self.keyValueMapper(key: "Remarks", value: remarks)
            default:
                break
            }
        } else if offersModel?.amountType == "3" || offersModel?.amountType == "4"{//Free With Fixed Amount Percentage
            switch index {
            case 0:
                let receiverNo = offer?.mobileNumber
                self.keyValueMapper(key: "Receiver Number", value: receiverNo)
            case 1:
                let receiverName = offer?.merchant ?? "Unknown"
                self.keyValueMapper(key: "Receiver Name", value: receiverName)
            case 2:
                let receiverName = offer?.accountType //self.agentTypeString()
                self.keyValueMapper(key: "Account Type", value: receiverName)
            case 3:
                let businessName = offer?.businessName ?? "Unknown"
                if businessName.count > 0 {
                    self.keyValueMapper(key: "Business Name", value: businessName)
                } else {
                    self.keyValueMapper(key: "Business Name", value: "Unknown")
                }
            case 4:
                self.keyValueMapper(key: "Promo Code", value: offersModel?.promotionCode)
            case 5:
                self.keyValueMapper(key: "Company Name", value: offersModel?.categoryName)
            case 6:
                self.keyValueMapper(key: "Product Name", value: offersModel?.productName)
            case 7:
                self.keyValueMapper(key: "Free Product Name", value: offersModel?.freeProductName)
            case 8:
                let buying = String(offersModel?.buyingQty ?? 0)
                let amont = wrapAmountWithCommaDecimal(key: "\(buying)")
                self.keyValueMapper(key: "Buying Quantity", value: "\(amont)" )
            case 9:
                let freeQty = String(offersModel?.freeQty ?? 0)
                let amont = wrapAmountWithCommaDecimal(key: "\(freeQty)")
                self.keyValueMapper(key: "Free Quantity", value: "\(amont)")
            case 10:
                //FIXEDAmount - 0 , Percentage - 1 , Free - 2 , FreeWithFixedAmount - 3 , FreeWithPercentage - 4
                if offersModel?.amountType == "3"//Fixed=Percentage calculate
                {
                    let amount = Float(offer?.amount ?? "0") ?? 0.0
                    let percentageAmount = Float(offersModel?.value ?? Int(0.0))
                    let percentageValue = Float((percentageAmount * 100) / amount)
                    self.keyValueMapper(key: "Discount Percentage", value:                     CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(percentageValue)") + " %")
                } else if  offersModel?.amountType == "4"{ //Fixed value display
                    let value = Float(offersModel?.value ?? Int(0.0))
                    self.keyValueMapper(key: "Discount Percentage", value: self.getDigitDisplay(digit : value) + " %")
                }
            case 11:
                if offersModel?.amountType == "3"//Fixed=Percentage calculate
                {
                    let value = Float(offersModel?.value ?? Int(0.0))
                    let amont = wrapAmountWithCommaDecimal(key: "\(value)")
                    self.keyValueOfferMapper(key: "Discount Amount", value: wrapAmountWithOfferMMK(bal: amont))
                } else if offersModel?.amountType == "4"{ //Fixed data calculate
                    let amount = Float(offer?.amount ?? "0") ?? 0.0
                    let percentageAmount = Float(offersModel?.value ?? Int(0.0))
                    let percentageValue = Float((percentageAmount * amount) / 100)
                    let amont = wrapAmountWithCommaDecimal(key: "\(percentageValue)")
                    self.keyValueOfferMapper(key: "Discount Amount", value: wrapAmountWithOfferMMK(bal: amont))
                }
            case 12:
                let amount = Float(offer?.amount ?? "0") ?? 0.0
                let amont = wrapAmountWithCommaDecimal(key: "\(amount)")
                self.keyValueOfferMapper(key: "Bill Amount", value: wrapAmountWithOfferMMK(bal: amont))
            case 13:
                let amount = Float(offer?.amount ?? "0") ?? 0.0
                let value = Float(offersModel?.value ?? Int(0.0))
                
                if offersModel?.amountType == "0" || offersModel?.amountType == "3"//Fixed=Percentage calculate
                {
                    let result = amount - value
                    let amont = wrapAmountWithCommaDecimal(key: "\(result)")
                    self.keyValueOfferMapper(key: "Net Payable Amount", value: wrapAmountWithOfferMMK(bal: amont))
                } else if offersModel?.amountType == "1" || offersModel?.amountType == "4"{ //Fixed data calculate
                    
                    let percentageValue = Float((value * amount) / 100)
                    let result = amount - percentageValue
                    
                    let amont = wrapAmountWithCommaDecimal(key: "\(result)")
                    self.keyValueOfferMapper(key: "Net Payable Amount", value: wrapAmountWithOfferMMK(bal: amont))
                }
            case 14:
                let remarks = offer?.remarks ?? ""
                self.keyValueMapper(key: "Remarks", value: remarks)
            default:
                break
            }
        } else if offersModel?.amountType == "5" {//Product
            switch index {
            case 0:
                let receiverNo = offer?.mobileNumber
                self.keyValueMapper(key: "Receiver Number", value: receiverNo)
            case 1:
                let receiverName = offer?.merchant ?? "Unknown"
                self.keyValueMapper(key: "Receiver Name", value: receiverName)
            case 2:
                let receiverName = offer?.accountType //self.agentTypeString()
                self.keyValueMapper(key: "Account Type", value: receiverName)
            case 3:
                let businessName = offer?.businessName ?? "Unknown"
                if businessName.count > 0 {
                    self.keyValueMapper(key: "Business Name", value: businessName)
                } else {
                    self.keyValueMapper(key: "Business Name", value: "Unknown")
                }
            case 4:
                self.keyValueMapper(key: "Promo Code", value: offersModel?.promotionCode)
            case 5:
                self.keyValueMapper(key: "Company Name", value: offersModel?.companyName)
            case 6:
                self.keyValueMapper(key: "Product Name", value: offersModel?.productName)
            case 7:
                let buying = String(offersModel?.buyingQty ?? 0)
                let amont = wrapAmountWithCommaDecimal(key: "\(buying)")
                self.keyValueMapper(key: "Buying Quantity", value: "\(amont)" )
            case 8:
                let amount = Float(offer?.amount ?? "0") ?? 0.0
                let amont = wrapAmountWithCommaDecimal(key: "\(amount)")
                self.keyValueOfferMapper(key: "Bill Amount", value: wrapAmountWithOfferMMK(bal: amont))
            case 9:
                let amount = Float(offer?.amount ?? "0") ?? 0.0
                let value = Float(offersModel?.value ?? Int(0.0))
                
                let result = amount - value
                let amont = wrapAmountWithCommaDecimal(key: "\(result)")
                self.keyValueOfferMapper(key: "Net Payable Amount", value: wrapAmountWithOfferMMK(bal: amont))
            case 10:
                let remarks = offer?.remarks ?? ""
                self.keyValueMapper(key: "Remarks", value: remarks)
            default:
                break
            }
        } else if offersModel?.amountType == "6" || offersModel?.amountType == "7"{//Product With Fixed Amount & Percentage
            switch index {
            case 0:
                let receiverNo = offer?.mobileNumber
                self.keyValueMapper(key: "Receiver Number", value: receiverNo)
            case 1:
                let receiverName = offer?.merchant ?? "Unknown"
                self.keyValueMapper(key: "Receiver Name", value: receiverName)
            case 2:
                let receiverName = offer?.accountType //self.agentTypeString()
                self.keyValueMapper(key: "Account Type", value: receiverName)
            case 3:
                let businessName = offer?.businessName ?? "Unknown"
                if businessName.count > 0 {
                    self.keyValueMapper(key: "Business Name", value: businessName)
                } else {
                    self.keyValueMapper(key: "Business Name", value: "Unknown")
                }
            case 4:
                self.keyValueMapper(key: "Promo Code", value: offersModel?.promotionCode)
            case 5:
                self.keyValueMapper(key: "Company Name", value: offersModel?.categoryName)
            case 6:
                self.keyValueMapper(key: "Product Name", value: offersModel?.productName)
            case 7:
                let buying = String(offersModel?.buyingQty ?? 0)
                let amont = wrapAmountWithCommaDecimal(key: "\(buying)")
                self.keyValueMapper(key: "Buying Quantity", value: "\(amont)" )
            case 8:
                //FIXEDAmount - 0 , Percentage - 1 , Free - 2 , FreeWithFixedAmount - 3 , FreeWithPercentage - 4
                if offersModel?.amountType == "6"//Fixed=Percentage calculate
                {
                    let amount = Float(offer?.amount ?? "0") ?? 0.0
                    let percentageAmount = Float(offersModel?.value ?? Int(0.0))
                    let percentageValue = Float((percentageAmount * 100) / amount)
                    self.keyValueMapper(key: "Discount Percentage", value:                     CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(percentageValue)") + " %")
                } else if  offersModel?.amountType == "7"{ //Fixed value display
                    let value = Float(offersModel?.value ?? Int(0.0))
                    self.keyValueMapper(key: "Discount Percentage", value: self.getDigitDisplay(digit : value) + " %")
                }
            case 9:
                if offersModel?.amountType == "6"//Fixed=Percentage calculate
                {
                    let value = Float(offersModel?.value ?? Int(0.0))
                    let amont = wrapAmountWithCommaDecimal(key: "\(value)")
                    self.keyValueOfferMapper(key: "Discount Amount", value: wrapAmountWithOfferMMK(bal: amont))
                } else if offersModel?.amountType == "7"{ //Fixed data calculate
                    let amount = Float(offer?.amount ?? "0") ?? 0.0
                    let percentageAmount = Float(offersModel?.value ?? Int(0.0))
                    let percentageValue = Float((percentageAmount * amount) / 100)
                    let amont = wrapAmountWithCommaDecimal(key: "\(percentageValue)")
                    self.keyValueOfferMapper(key: "Discount Amount", value: wrapAmountWithOfferMMK(bal: amont))
                }
            case 10:
                let amount = Float(offer?.amount ?? "0") ?? 0.0
                let amont = wrapAmountWithCommaDecimal(key: "\(amount)")
                self.keyValueOfferMapper(key: "Bill Amount", value: wrapAmountWithOfferMMK(bal: amont))
            case 11:
                let amount = Float(offer?.amount ?? "0") ?? 0.0
                let value = Float(offersModel?.value ?? Int(0.0))
                
                if offersModel?.amountType == "6"//Fixed=Percentage calculate
                {
                    let result = amount - value
                    let amont = wrapAmountWithCommaDecimal(key: "\(result)")
                    self.keyValueOfferMapper(key: "Net Payable Amount", value: wrapAmountWithOfferMMK(bal: amont))
                } else if offersModel?.amountType == "7" { //Fixed data calculate
                    
                    let percentageValue = Float((value * amount) / 100)
                    let result = amount - percentageValue
                    
                    let amont = wrapAmountWithCommaDecimal(key: "\(result)")
                    self.keyValueOfferMapper(key: "Net Payable Amount", value: wrapAmountWithOfferMMK(bal: amont))
                }
            case 12:
                let remarks = offer?.remarks ?? ""
                self.keyValueMapper(key: "Remarks", value: remarks)
            default:
                break
            }
        }
        
      } else {//Direct Data
        let offersModel = offer?.offers
        if offersModel?.amountType == "0" || offersModel?.amountType == "1"{
            switch index {
            
            case 0:
                self.keyValueMapper(key: "Promo Code", value: offersModel?.promotionCode)
            case 1:
                if appDel.currentLanguage == "my" {
                    self.keyValueMapper(key: "Company Name", value: offersModel?.categoryBName)
                }
                else if appDel.currentLanguage == "zh-Hans" {
                    self.keyValueMapper(key: "Company Name", value: offersModel?.categoryCName)
                }else {
                    self.keyValueMapper(key: "Company Name", value: offersModel?.categoryName)
                }
            case 2:
                //FIXEDAmount - 0 , Percentage - 1 , Free - 2 , FreeWithFixedAmount - 3 , FreeWithPercentage - 4
                if offersModel?.amountType == "0"//Fixed=Percentage calculate
                {
                    let amount = Float(offer?.amount ?? "0") ?? 0.0
                    let percentageAmount = Float(offersModel?.value ?? Int(0.0))
                    let percentageValue = Float((percentageAmount * 100) / amount)
                    self.keyValueMapper(key: "Discount Percentage", value:                     CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(percentageValue)") + " %")
                    
                } else if offersModel?.amountType == "1" { //Fixed value display
                    let value = Float(offersModel?.value ?? Int(0.0))
                    self.keyValueMapper(key: "Discount Percentage", value: self.getDigitDisplay(digit : value) + " %")
                }
            case 3:
                if offersModel?.amountType == "0" //Fixed=Percentage calculate
                {
                    let value = Float(offersModel?.value ?? Int(0.0))
                    let amont = wrapAmountWithCommaDecimal(key: "\(value)")
                    self.keyValueOfferMapper(key: "Discount Amount", value: wrapAmountWithOfferMMK(bal: amont))
                } else if offersModel?.amountType == "1" { //Fixed data calculate
                    let amount = Float(offer?.amount ?? "0") ?? 0.0
                    let percentageAmount = Float(offersModel?.value ?? Int(0.0))
                    let percentageValue = Float((percentageAmount * amount) / 100)
                    let amont = wrapAmountWithCommaDecimal(key: "\(percentageValue)")
                    self.keyValueOfferMapper(key: "Discount Amount", value: wrapAmountWithOfferMMK(bal: amont))
                }
            case 4:
                let amount = Float(offer?.amount ?? "0") ?? 0.0
                let amont = wrapAmountWithCommaDecimal(key: "\(amount)")
                self.keyValueOfferMapper(key: "Bill Amount", value: wrapAmountWithOfferMMK(bal: amont))
            case 5:
                let amount = Float(offer?.amount ?? "0") ?? 0.0
                let value = Float(offersModel?.value ?? Int(0.0))
                
                if offersModel?.amountType == "0"//Fixed=Percentage calculate
                {
                    let result = amount - value
                    let amont = wrapAmountWithCommaDecimal(key: "\(result)")
                    self.keyValueOfferMapper(key: "Net Payable Amount", value: wrapAmountWithOfferMMK(bal: amont))
                } else if offersModel?.amountType == "1" { //Fixed data calculate
                    
                    let percentageValue = Float((value * amount) / 100)
                    let result = amount - percentageValue
                    
                    let amont = wrapAmountWithCommaDecimal(key: "\(result)")
                    self.keyValueOfferMapper(key: "Net Payable Amount", value: wrapAmountWithOfferMMK(bal: amont))
                }
            case 6:
                let remarks = offer?.remarks ?? ""
                self.keyValueMapper(key: "Remarks", value: remarks)
            default:
                break
            }
        } else if offersModel?.amountType == "2" {//Free
            switch index {
                
            case 0:
                self.keyValueMapper(key: "Promo Code", value: offersModel?.promotionCode)
            case 1:
                self.keyValueMapper(key: "Company Name", value: offersModel?.companyName)
            case 2:
                self.keyValueMapper(key: "Product Name", value: offersModel?.productName)
            case 3:
                self.keyValueMapper(key: "Free Product Name", value: offersModel?.freeProductName)
            case 4:
                let buying = String(offersModel?.buyingQty ?? 0)
                let amont = wrapAmountWithCommaDecimal(key: "\(buying)")
                self.keyValueMapper(key: "Buying Quantity", value: "\(amont)" )
            case 5:
                let freeQty = String(offersModel?.freeQty ?? 0)
                let amont = wrapAmountWithCommaDecimal(key: "\(freeQty)")
                self.keyValueMapper(key: "Free Quantity", value: "\(amont)")
            case 6:
                let amount = Float(offer?.amount ?? "0") ?? 0.0
                let amont = wrapAmountWithCommaDecimal(key: "\(amount)")
                self.keyValueOfferMapper(key: "Bill Amount", value: wrapAmountWithOfferMMK(bal: amont))
            case 7:
                let amount = Float(offer?.amount ?? "0") ?? 0.0
                let value = Float(offersModel?.value ?? Int(0.0))
                
                let result = amount - value
                let amont = wrapAmountWithCommaDecimal(key: "\(result)")
                self.keyValueOfferMapper(key: "Net Payable Amount", value: wrapAmountWithOfferMMK(bal: amont))
            case 8:
                let remarks = offer?.remarks ?? ""
                self.keyValueMapper(key: "Remarks", value: remarks)
            default:
                break
            }
        } else if offersModel?.amountType == "3" || offersModel?.amountType == "4"{//Free With Fixed Amount Percentage
            switch index {
        
            case 0:
                self.keyValueMapper(key: "Promo Code", value: offersModel?.promotionCode)
            case 1:
                self.keyValueMapper(key: "Company Name", value: offersModel?.categoryName)
            case 2:
                self.keyValueMapper(key: "Product Name", value: offersModel?.productName)
            case 3:
                self.keyValueMapper(key: "Free Product Name", value: offersModel?.freeProductName)
            case 4:
                let buying = String(offersModel?.buyingQty ?? 0)
                let amont = wrapAmountWithCommaDecimal(key: "\(buying)")
                self.keyValueMapper(key: "Buying Quantity", value: "\(amont)" )
            case 5:
                let freeQty = String(offersModel?.freeQty ?? 0)
                let amont = wrapAmountWithCommaDecimal(key: "\(freeQty)")
                self.keyValueMapper(key: "Free Quantity", value: "\(amont)")
            case 6:
                //FIXEDAmount - 0 , Percentage - 1 , Free - 2 , FreeWithFixedAmount - 3 , FreeWithPercentage - 4
                if offersModel?.amountType == "3"//Fixed=Percentage calculate
                {
                    let amount = Float(offer?.amount ?? "0") ?? 0.0
                    let percentageAmount = Float(offersModel?.value ?? Int(0.0))
                    let percentageValue = Float((percentageAmount * 100) / amount)
                    self.keyValueMapper(key: "Discount Percentage", value:                     CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(percentageValue)") + " %")
                } else if  offersModel?.amountType == "4"{ //Fixed value display
                    let value = Float(offersModel?.value ?? Int(0.0))
                    self.keyValueMapper(key: "Discount Percentage", value: self.getDigitDisplay(digit : value) + " %")
                }
            case 7:
                if offersModel?.amountType == "3"//Fixed=Percentage calculate
                {
                    let value = Float(offersModel?.value ?? Int(0.0))
                    let amont = wrapAmountWithCommaDecimal(key: "\(value)")
                    self.keyValueOfferMapper(key: "Discount Amount", value: wrapAmountWithOfferMMK(bal: amont))
                } else if offersModel?.amountType == "4"{ //Fixed data calculate
                    let amount = Float(offer?.amount ?? "0") ?? 0.0
                    let percentageAmount = Float(offersModel?.value ?? Int(0.0))
                    let percentageValue = Float((percentageAmount * amount) / 100)
                    let amont = wrapAmountWithCommaDecimal(key: "\(percentageValue)")
                    self.keyValueOfferMapper(key: "Discount Amount", value: wrapAmountWithOfferMMK(bal: amont))
                }
            case 8:
                let amount = Float(offer?.amount ?? "0") ?? 0.0
                let amont = wrapAmountWithCommaDecimal(key: "\(amount)")
                self.keyValueOfferMapper(key: "Bill Amount", value: wrapAmountWithOfferMMK(bal: amont))
            case 9:
                let amount = Float(offer?.amount ?? "0") ?? 0.0
                let value = Float(offersModel?.value ?? Int(0.0))
                
                if offersModel?.amountType == "0" || offersModel?.amountType == "3"//Fixed=Percentage calculate
                {
                    let result = amount - value
                    let amont = wrapAmountWithCommaDecimal(key: "\(result)")
                    self.keyValueOfferMapper(key: "Net Payable Amount", value: wrapAmountWithOfferMMK(bal: amont))
                } else if offersModel?.amountType == "1" || offersModel?.amountType == "4"{ //Fixed data calculate
                    
                    let percentageValue = Float((value * amount) / 100)
                    let result = amount - percentageValue
                    
                    let amont = wrapAmountWithCommaDecimal(key: "\(result)")
                    self.keyValueOfferMapper(key: "Net Payable Amount", value: wrapAmountWithOfferMMK(bal: amont))
                }
            case 10:
                let remarks = offer?.remarks ?? ""
                self.keyValueMapper(key: "Remarks", value: remarks)
            default:
                break
            }
        } else if offersModel?.amountType == "5" {//Product
            switch index {
           
            case 0:
                self.keyValueMapper(key: "Promo Code", value: offersModel?.promotionCode)
            case 1:
                self.keyValueMapper(key: "Company Name", value: offersModel?.companyName)
            case 2:
                self.keyValueMapper(key: "Product Name", value: offersModel?.productName)
            case 3:
                let buying = String(offersModel?.buyingQty ?? 0)
                let amont = wrapAmountWithCommaDecimal(key: "\(buying)")
                self.keyValueMapper(key: "Buying Quantity", value: "\(amont)" )
            case 4:
                let amount = Float(offer?.amount ?? "0") ?? 0.0
                let amont = wrapAmountWithCommaDecimal(key: "\(amount)")
                self.keyValueOfferMapper(key: "Bill Amount", value: wrapAmountWithOfferMMK(bal: amont))
            case 5:
                let amount = Float(offer?.amount ?? "0") ?? 0.0
                let value = Float(offersModel?.value ?? Int(0.0))
                
                let result = amount - value
                let amont = wrapAmountWithCommaDecimal(key: "\(result)")
                self.keyValueOfferMapper(key: "Net Payable Amount", value: wrapAmountWithOfferMMK(bal: amont))
            case 6:
                let remarks = offer?.remarks ?? ""
                self.keyValueMapper(key: "Remarks", value: remarks)
            default:
                break
            }
        } else if offersModel?.amountType == "6" || offersModel?.amountType == "7"{//Product With Fixed Amount & Percentage
            switch index {
            
            case 0:
                self.keyValueMapper(key: "Promo Code", value: offersModel?.promotionCode)
            case 1:
                self.keyValueMapper(key: "Company Name", value: offersModel?.categoryName)
            case 2:
                self.keyValueMapper(key: "Product Name", value: offersModel?.productName)
            case 3:
                let buying = String(offersModel?.buyingQty ?? 0)
                let amont = wrapAmountWithCommaDecimal(key: "\(buying)")
                self.keyValueMapper(key: "Buying Quantity", value: "\(amont)" )
            case 4:
                //FIXEDAmount - 0 , Percentage - 1 , Free - 2 , FreeWithFixedAmount - 3 , FreeWithPercentage - 4
                if offersModel?.amountType == "6"//Fixed=Percentage calculate
                {
                    let amount = Float(offer?.amount ?? "0") ?? 0.0
                    let percentageAmount = Float(offersModel?.value ?? Int(0.0))
                    let percentageValue = Float((percentageAmount * 100) / amount)
                    self.keyValueMapper(key: "Discount Percentage", value:                     CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(percentageValue)") + " %")
                } else if  offersModel?.amountType == "7"{ //Fixed value display
                    let value = Float(offersModel?.value ?? Int(0.0))
                    self.keyValueMapper(key: "Discount Percentage", value: self.getDigitDisplay(digit : value) + " %")
                }
            case 5:
                if offersModel?.amountType == "6"//Fixed=Percentage calculate
                {
                    let value = Float(offersModel?.value ?? Int(0.0))
                    let amont = wrapAmountWithCommaDecimal(key: "\(value)")
                    self.keyValueOfferMapper(key: "Discount Amount", value: wrapAmountWithOfferMMK(bal: amont))
                } else if offersModel?.amountType == "7"{ //Fixed data calculate
                    let amount = Float(offer?.amount ?? "0") ?? 0.0
                    let percentageAmount = Float(offersModel?.value ?? Int(0.0))
                    let percentageValue = Float((percentageAmount * amount) / 100)
                    let amont = wrapAmountWithCommaDecimal(key: "\(percentageValue)")
                    self.keyValueOfferMapper(key: "Discount Amount", value: wrapAmountWithOfferMMK(bal: amont))
                }
            case 6:
                let amount = Float(offer?.amount ?? "0") ?? 0.0
                let amont = wrapAmountWithCommaDecimal(key: "\(amount)")
                self.keyValueOfferMapper(key: "Bill Amount", value: wrapAmountWithOfferMMK(bal: amont))
            case 7:
                let amount = Float(offer?.amount ?? "0") ?? 0.0
                let value = Float(offersModel?.value ?? Int(0.0))
                
                if offersModel?.amountType == "6"//Fixed=Percentage calculate
                {
                    let result = amount - value
                    let amont = wrapAmountWithCommaDecimal(key: "\(result)")
                    self.keyValueOfferMapper(key: "Net Payable Amount", value: wrapAmountWithOfferMMK(bal: amont))
                } else if offersModel?.amountType == "7" { //Fixed data calculate
                    
                    let percentageValue = Float((value * amount) / 100)
                    let result = amount - percentageValue
                    
                    let amont = wrapAmountWithCommaDecimal(key: "\(result)")
                    self.keyValueOfferMapper(key: "Net Payable Amount", value: wrapAmountWithOfferMMK(bal: amont))
                }
            case 8:
                let remarks = offer?.remarks ?? ""
                self.keyValueMapper(key: "Remarks", value: remarks)
            default:
                break
            }
        }
        
        }
    }
    
    func wrapAmountWithOfferMMK(bal: String) -> NSAttributedString {
        let denomAttr = [NSAttributedString.Key.font: UIFont(name: appFont, size: 17.0) ?? UIFont.systemFont(ofSize: 17)]
        let amntAttr = [NSAttributedString.Key.font: UIFont(name: appFont, size: 11.0) ?? UIFont.systemFont(ofSize: 11)]
        let denomStr = NSMutableAttributedString(string: bal, attributes: denomAttr)
        let amountStr    = NSMutableAttributedString.init(string: " MMK", attributes: amntAttr)
        
        let amountAttr = NSMutableAttributedString()
        amountAttr.append(denomStr)
        amountAttr.append(amountStr)
        return amountAttr
    }

    // MARK: Get Agent Type
    func agentTypeString() -> String {
        let type = UserModel.shared.agentType
        switch type {
        case .user:
            return "Personal".localized
        case .merchant:
            return "Merchant".localized
        case .agent:
            return "Agent".localized
        case .advancemerchant:
            return "Advance Merchant".localized
        case .dummymerchant:
            return "Safety Cashier".localized
        case .oneStopMart:
            return "One Stop Mart".localized
        }
    }
    
    func getDigitDisplay(digit : Float) -> String {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        return formatter.string(from: digit as NSNumber)!
    }
    
    func keyValueMapper(key: String, value: String?) {
        self.keyLabel.text = key.localized
        self.valueLabel.text = value
    }

    func keyValueOfferMapper(key: String, value: NSAttributedString?) {
        self.valueLabel.attributedText = value
        self.keyLabel.text = key.localized
        //self.valueLabel.text = value
    }
}
