//
//  OfferDirectPayRequest.swift
//  OK
//
//  Created by Ashish on 11/15/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit

struct OfferDirectPayRequest: Codable {
    let destinationNumberWalletBalance: String?
    let transactionsCellTower: OfferDirectPayTransactionsCellTower?
    let lExternalReference: OfferDirectPayLExternalReference?
    let lGeoLocation: OfferDirectPayLGeoLocation?
    let lProximity: OfferDirectPayLProximity?
    let lTransactions: OfferDirectPayLTransactions?
    
    enum CodingKeys: String, CodingKey {
        case destinationNumberWalletBalance = "DestinationNumberWalletBalance"
        case transactionsCellTower = "TransactionsCellTower"
        case lExternalReference, lGeoLocation, lProximity, lTransactions
    }
}

struct OfferDirectPayLExternalReference: Codable {
    let direction: Bool?
    let endStation, startStation, vendorId: String?
    
    enum CodingKeys: String, CodingKey {
        case direction = "Direction"
        case endStation = "EndStation"
        case startStation = "StartStation"
        case vendorId = "VendorID"
    }
}

struct OfferDirectPayLGeoLocation: Codable {
    let cellId, latitude, longitude: String?
    
    enum CodingKeys: String, CodingKey {
        case cellId = "CellID"
        case latitude = "Latitude"
        case longitude = "Longitude"
    }
}

struct OfferDirectPayLProximity: Codable {
    let blueToothUsers: [String]?
    let selectionMode: Bool?
    let wifiUsers: [String]?
    let mBltDevice, mWifiDevice: String?
    
    enum CodingKeys: String, CodingKey {
        case blueToothUsers = "BlueToothUsers"
        case selectionMode = "SelectionMode"
        case wifiUsers = "WifiUsers"
        case mBltDevice, mWifiDevice
    }
}

struct OfferDirectPayLTransactions: Codable {
    let amount, balance: String?
    let bonusPoint: Int?
    let businessName: String?
    let cashBackFlag: Int?
    let comments, destination, destinationNumberWalletBalance: String?
    let discountPayTo: Int?
    let isMectelTopUp: Bool?
    let isPromotionApplicable, kickBack, kickBackMsisdn, localTransactionType: String?
    let merchantName, mobileNumber: String?
    let mode: Bool?
    let password, promoActualAmount, promoCodeId: String?
    let resultCode: Int?
    let resultDescription, secureToken, transactionId, transactionTime: String?
    let transactionType, accounType, senderBusinessName, userName: String?
    
    enum CodingKeys: String, CodingKey {
        case amount = "Amount"
        case balance = "Balance"
        case bonusPoint = "BonusPoint"
        case businessName = "BusinessName"
        case cashBackFlag = "CashBackFlag"
        case comments = "Comments"
        case destination = "Destination"
        case destinationNumberWalletBalance = "DestinationNumberWalletBalance"
        case discountPayTo = "DiscountPayTo"
        case isMectelTopUp = "IsMectelTopUp"
        case isPromotionApplicable = "IsPromotionApplicable"
        case kickBack = "KickBack"
        case kickBackMsisdn = "KickBackMsisdn"
        case localTransactionType = "LocalTransactionType"
        case merchantName = "MerchantName"
        case mobileNumber = "MobileNumber"
        case mode = "Mode"
        case password = "Password"
        case promoActualAmount = "PromoActualAmount"
        case promoCodeId = "PromoCodeId"
        case resultCode = "ResultCode"
        case resultDescription = "ResultDescription"
        case secureToken = "SecureToken"
        case transactionId = "TransactionID"
        case transactionTime = "TransactionTime"
        case transactionType = "TransactionType"
        case accounType, senderBusinessName, userName
    }
}

struct OfferDirectPayTransactionsCellTower: Codable {
    let lac, mac, mcc, mnc: String?
    let signalStrength, ssid, connectedwifiName, networkType, networkSpeed : String?
    
    enum CodingKeys: String, CodingKey {
        case lac = "Lac"
        case mac = "Mac"
        case mcc = "Mcc"
        case mnc = "Mnc"
        case signalStrength = "SignalStrength"
        case ssid = "Ssid"
        case connectedwifiName = "ConnectedwifiName"
        case networkType = "NetworkType"
        case networkSpeed = "NetworkSpeed"
    }
}
