//
//  OfferReceiptTableViewCell.swift
//  OK
//
//  Created by Ashish on 1/5/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

//MARK:- OfferReceiptCellHeader
class OfferReceiptCellHeader: UITableViewCell {
    
    static let identifier = String.init(describing: OfferReceiptCellHeader.self)
    
    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var businessName: UILabel!
    @IBOutlet var businessHeightConstraint: NSLayoutConstraint!
   let myString = NSAttributedString()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
      
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func wrapContent(model: OfferConfirmationScreen?, number: String) {
        
        let offersModel = model?.offers
        
        let amount = Int(model?.amount ?? "0") ?? 0
        let value = offersModel?.value ?? 0
        var result = 0
        
        if offersModel?.amountType == "0" || offersModel?.amountType == "3" || offersModel?.amountType == "6"//Fixed=Percentage calculate
        {
            result = Int(amount - value)
        } else if offersModel?.amountType == "1" || offersModel?.amountType == "4" || offersModel?.amountType == "7" { //Fixed data calculate
            
            let percentageValue = Int((value * amount) / 100)
            result = amount - percentageValue
        } else {//Free
             result = amount - value
        }
        
      if model?.offers?.paymentType == "0" {//Direct
        let last4 = String(number.suffix(4))
        self.number.text = "XXXXXX" + last4
        
        self.amount.text = self.getDigitDisplay(digit : Float(result))
        businessHeightConstraint.constant = 0
      } else {
        
        let business = model?.businessName ?? "Unknown"
        if business.count > 0 {
          self.businessName.text = business
        } else {
          self.businessName.text = "Unknown"
        }
        self.number.text = number
        self.amount.text = self.getDigitDisplay(digit : Float(result))
        businessHeightConstraint.constant = 40
      }
    }
    
    func getDigitDisplay(digit : Float) -> String {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        return formatter.string(from: digit as NSNumber)!
    }
    
    fileprivate func wrapAmount(key: String) -> String {
        let number = NSDecimalNumber(string: key)
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        if let num = formatter.string(from: number) {
            return (num == "NaN") ? "" : (num)
        }
        return ""
    }
}



//MARK:- OfferReceiptCellContent
class OfferReceiptCellContent: UITableViewCell {
    
    static let identifier = String.init(describing: OfferReceiptCellContent.self)
    
    @IBOutlet weak var keyLabel: UILabel!
    
    @IBOutlet weak var valueLabel: UILabel!
    
    //Helpers
    func append09Format(_ dest: String) -> String {
        if dest.hasPrefix("00") {
            var number = dest.deletingPrefix("00")
            number = "+" + number
            let country = identifyCountry(withPhoneNumber: number)
            let final = number.replacingOccurrences(of: country.countryCode, with: "0")
            return final
        }
        return dest
    }
    
    func wrapData(key: String, model: OfferConfirmationScreen?, destDetails: OfferRecieptModel?) {
        
        self.keyLabel.text = key.localized
        //self.keyLabel.numberOfLines = 2
        var valueString : String? = ""
        let offersModel = model?.offers

        switch key.lowercased() {
        case "Promo Receiver Number".lowercased():
            valueString = self.append09Format(destDetails?.destination ?? "0095")
              self.valueLabel.text = valueString
        case "Receiver Account Number".lowercased():
            valueString = model?.mobileNumber
              self.valueLabel.text = valueString
        case "Company Name".lowercased():
            if appDel.currentLanguage == "my" {
                valueString = offersModel?.categoryBName
                  self.valueLabel.text = valueString
            }
            else if appDel.currentLanguage == "zh-Hans" {
                valueString = offersModel?.categoryCName
                  self.valueLabel.text = valueString
            }else {
                valueString = offersModel?.categoryName
                  self.valueLabel.text = valueString
            }
        case "Product Name".lowercased():
            if appDel.currentLanguage == "my" {
                valueString = offersModel?.productBName
                  self.valueLabel.text = valueString
            }
            else if appDel.currentLanguage == "zh-Hans" {
                valueString = offersModel?.productCName
                  self.valueLabel.text = valueString
            }else {
                valueString = offersModel?.productName
                  self.valueLabel.text = valueString
            }
        case "Free Product Name".lowercased():
            valueString = offersModel?.freeProductName
              self.valueLabel.text = valueString
        case "Buying Quantity".lowercased():
            valueString = "\(offersModel?.buyingQty ?? 0)"
            let amont = wrapAmountWithCommaDecimal(key: "\(valueString ?? "")")
              self.valueLabel.text = amont
        case "Free Quantity".lowercased():
            valueString = "\(offersModel?.freeQty ?? 0)"
           let amont = wrapAmountWithCommaDecimal(key: "\(valueString ?? "")")
              self.valueLabel.text = amont
        case "Merchant".lowercased():
            valueString = model?.merchant
              self.valueLabel.text = valueString
        case "Category".lowercased():
            valueString = "Offers"
              self.valueLabel.text = valueString
        case "Promocode".lowercased():
            valueString = model?.offers?.promotionCode
              self.valueLabel.text = valueString
        case "Discount Percentage".lowercased():

          if offersModel?.amountType == "0" || offersModel?.amountType == "3" || offersModel?.amountType == "6"//Fixed=Percentage calculate
          {
            let amount = Float(model?.amount ?? "0") ?? 0.0
            let percentageAmount = Float(offersModel?.value ?? Int(0.0))
            
            let percentageValue = Float((percentageAmount * 100) / amount)
            valueString = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(percentageValue)") + " %"
              self.valueLabel.text = valueString
          } else if offersModel?.amountType == "1" || offersModel?.amountType == "4" || offersModel?.amountType == "7"{ //Fixed value display
            let value = Float(offersModel?.value ?? Int(0.0))
            valueString = self.getDigitDisplay(digit: value) + "%"
              self.valueLabel.text = valueString
          }else {//Free
            let value = Float(offersModel?.value ?? Int(0.0))
            valueString = self.getDigitDisplay(digit: value) + "%"
              self.valueLabel.text = valueString
          }
        case "Discount Amount".lowercased():
          
          let offersModel = model?.offers
          
          if offersModel?.amountType == "0" || offersModel?.amountType == "3" || offersModel?.amountType == "6"//Fixed=Percentage calculate
          {
            let value = Float(offersModel?.value ?? Int(0.0))
        //    valueString = self.getDigitDisplay(digit: value) + "MMK"
            let amont = wrapAmountWithCommaDecimal(key: "\(value)")
            self.valueLabel.attributedText = self.wrapAmountWithReceiptMMK(bal: amont )
          } else if offersModel?.amountType == "1" || offersModel?.amountType == "4" || offersModel?.amountType == "7" { //Fixed data calculate
            
            let amount = Float(model?.amount ?? "0") ?? 0.0
            let percentageAmount = Float(offersModel?.value ?? Int(0.0))
            let percentageValue = Float((percentageAmount * amount) / 100)
        //    valueString = self.getDigitDisplay(digit: percentageValue) + "MMK"
            let amont = wrapAmountWithCommaDecimal(key: "\(percentageValue)")
            self.valueLabel.attributedText = self.wrapAmountWithReceiptMMK(bal: amont )
            
          }else {//Free
            let value = Float(offersModel?.value ?? Int(0.0))
         //   valueString = self.getDigitDisplay(digit: value) + "MMK"
            let amont = wrapAmountWithCommaDecimal(key: "\(value)")
            self.valueLabel.attributedText = self.wrapAmountWithReceiptMMK(bal: amont )

          }
        case "Bill Amount".lowercased():
            let amount = Float(model?.amount ?? "0") ?? 0.0
        //    valueString = self.getDigitDisplay(digit: amount) + "MMK"
            let amont = wrapAmountWithCommaDecimal(key: "\(amount)")
            self.valueLabel.attributedText = self.wrapAmountWithReceiptMMK(bal: amont )

        case "Net Paid Amount".lowercased():
        
            let offersModel = model?.offers
            
            let amount = Float(model?.amount ?? "0") ?? 0.0
            let value = Float(offersModel?.value ?? Int(0.0))
            
            if offersModel?.amountType == "0" || offersModel?.amountType == "3" || offersModel?.amountType == "6"//Fixed=Percentage calculate
            {
              let result = amount - value
            //    valueString = self.getDigitDisplay(digit: result) + "MMK"
                let amont = wrapAmountWithCommaDecimal(key: "\(result)")
                self.valueLabel.attributedText = self.wrapAmountWithReceiptMMK(bal: amont )
            } else if offersModel?.amountType == "1" || offersModel?.amountType == "4" || offersModel?.amountType == "7" { //Fixed data calculate
              
              let percentageValue = Float((value * amount) / 100)
              let result = amount - percentageValue
             //   valueString = self.getDigitDisplay(digit: result) + "MMK"
                let amont = wrapAmountWithCommaDecimal(key: "\(result)")
                self.valueLabel.attributedText = self.wrapAmountWithReceiptMMK(bal: amont )
            } else {//Free
                let result = amount - value
                //valueString = self.getDigitDisplay(digit: result) + "MMK"
                let amont = wrapAmountWithCommaDecimal(key: "\(result)")
                self.valueLabel.attributedText = self.wrapAmountWithReceiptMMK(bal: amont )
            }
        case "Transaction ID".lowercased():
            valueString = destDetails?.transid
             self.valueLabel.text = valueString
        case "Transaction Type".lowercased():
            valueString = "PAYTO"
             self.valueLabel.text = valueString
        case "Remarks".lowercased():
          valueString = model?.remarks
          self.valueLabel.text = valueString
        default:
            valueString = ""
            self.valueLabel.text = valueString
        }
        
    }
    
    func getDigitDisplay(digit : Float) -> String {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        return formatter.string(from: digit as NSNumber)!
    }

    func wrapAmountWithReceiptMMK(bal: String) -> NSAttributedString {
        let denomAttr = [NSAttributedString.Key.font: UIFont(name: appFont, size: 17.0) ?? UIFont.systemFont(ofSize: 17)]
        let amntAttr = [NSAttributedString.Key.font: UIFont(name: appFont, size: 11.0) ?? UIFont.systemFont(ofSize: 11)]
        let denomStr = NSMutableAttributedString(string: bal, attributes: denomAttr)
        let amountStr    = NSMutableAttributedString.init(string: " MMK", attributes: amntAttr)
        
        let amountAttr = NSMutableAttributedString()
        amountAttr.append(denomStr)
        amountAttr.append(amountStr)
        return amountAttr
    }
    
}

//MARK:- OfferReceiptCellFooter
class OfferReceiptCellFooter: UITableViewCell {
    
    static let identifier = String.init(describing: OfferReceiptCellFooter.self)
    
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var rateMyService: UILabel!

    func wrapContent(model: OfferConfirmationScreen?, date: String,time: String ) {
        
        self.date.text = date
        self.time.text = time
    }

    @IBAction func rateAction(_ sender: UIButton) {
        
    }
    
}

class OfferTopOKCell: UITableViewCell {
    
    static let identifier = String.init(describing: OfferTopOKCell.self)
    

}
