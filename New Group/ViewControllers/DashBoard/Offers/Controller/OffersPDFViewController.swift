//
//  OffersPDFViewController.swift
//  OK
//
//  Created by OK$ on 10/2/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation

class CodeSnippetsOffers {

    //MARK: - Properties
    private static let pdfSize = CGSize(width: 595, height: 842)
    private static let pdfMargin: CGFloat = 20
    private static let pdfLineSpacing: CGFloat = 10
    private static let pdfLineSpaceBtwnTitles: CGFloat = 15
    private static let pdfHeaderFooterMargin: CGFloat = 30
    private static let pdfCenterSeparatorWidth: CGFloat = 6
    
    class func createInvoicePDF(dictValues: [String: Any], pdfFileName: String = "Invoice") -> URL? {
        var currentYPosition: CGFloat = 0
        let mainViewFrameSize = CGSize(width: 380, height: 400)
        let viewMain = UIView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: mainViewFrameSize))
        //Adding logo
        if let logoImageName = dictValues["logoName"] as? String, let logoImage = UIImage(named: logoImageName) {
            let logoSize = CGSize(width: 70, height: 70)
            let xPos = (mainViewFrameSize.width - logoSize.width) / 2
            let imageViewLogo = UIImageView(frame: CGRect(origin: CGPoint(x: xPos, y: 50), size: logoSize))
            imageViewLogo.image = logoImage
            imageViewLogo.contentMode = .scaleAspectFit
            currentYPosition = imageViewLogo.frame.maxY
            viewMain.addSubview(imageViewLogo)
        }
        //Adding Invoice Title
        if let invoiceTitle = dictValues["invoiceTitle"] as? String {
            let marginPadding: CGFloat = 20
            let labelSize = CGSize(width: mainViewFrameSize.width - (marginPadding * 2), height: 30)
            let labelTitle = UILabel(frame: CGRect(origin: CGPoint(x: marginPadding, y: currentYPosition + 20), size: labelSize))
            labelTitle.adjustsFontSizeToFitWidth = true
            labelTitle.textAlignment = .center
            labelTitle.text = invoiceTitle
            if let myFont = UIFont(name: appFont, size: 17) {
                labelTitle.font = myFont
            }
            labelTitle.textColor = UIColor.blue
            if let titleColor = dictValues["invoiceTitleColor"] as? UIColor {
                labelTitle.textColor = titleColor
            }
            currentYPosition = labelTitle.frame.maxY
            viewMain.addSubview(labelTitle)
        }
        func addRecord(title: String, attributed: NSAttributedString? = nil, value: String, yPadding: CGFloat) {
            //Title
            let marginPadding: CGFloat = 20
            let labelSize = CGSize(width: (mainViewFrameSize.width / 2) - marginPadding, height: 25)
            let labelTitle = UILabel(frame: CGRect(origin: CGPoint(x: marginPadding, y: currentYPosition + yPadding), size: labelSize))
            labelTitle.adjustsFontSizeToFitWidth = true
            labelTitle.textAlignment = .left
            
            labelTitle.text = title
            
            if let myFont = UIFont(name: appFont, size: 12) {
                labelTitle.font = myFont
            }
            labelTitle.textColor = UIColor.black
            //Value
            let labelValue = UILabel(frame: CGRect(origin: CGPoint(x: labelTitle.frame.maxX, y: currentYPosition + yPadding), size: labelSize))
            labelValue.adjustsFontSizeToFitWidth = true
            labelValue.textAlignment = .left
            if let atrValue = attributed {
                labelValue.attributedText = atrValue.length == 0 ? NSMutableAttributedString.init(string: ":    -") : atrValue
            } else {
                labelValue.text = value.count == 0 ? ":    -" : ":    \(value)"
            }
            if let myFont = UIFont(name: appFont, size: 12) {
                labelValue.font = myFont
            }
            labelValue.textColor = UIColor.black
            currentYPosition = labelValue.frame.maxY
            viewMain.addSubview(labelTitle)
            viewMain.addSubview(labelValue)
        }
        var yPad: CGFloat = 0
        //Adding business name title and value
        if let businessName = dictValues["businessName"] as? String {
            yPad = 40
            addRecord(title: "Business Name".localized, value: businessName, yPadding: yPad)
        }
        //Adding sender account name title and value
        if let senderAccName = dictValues["senderAccName"] as? String {
            addRecord(title: "Sender Account Name".localized, value: senderAccName, yPadding: yPad == 0 ? 40 : 10)
        }
        //Adding sender account number title and value
        if let senderAccNumber = dictValues["senderAccNo"] as? String {
            addRecord(title: "Sender Account Number".localized, value: senderAccNumber, yPadding: 10)
        }
        //Adding promo receiver number
        if let promoReceiverNumber = dictValues["promoReceiverNumber"] as? String {
            addRecord(title: "Promo Receiver Number".localized, value: promoReceiverNumber, yPadding: 10)
        }
        //Adding receiver account name title and value
        if let receiverAccName = dictValues["receiverAccName"] as? String {
            addRecord(title: "Receiver Account Name".localized, value: receiverAccName, yPadding: 10)
        }
        //Adding receiver account number title and value
        if let receiverAccNumber = dictValues["receiverAccNo"] as? String {
            addRecord(title: "Receiver Account Number".localized, value: receiverAccNumber, yPadding: 10)
        }
        
        if let agent = dictValues["Payment Type"] as? String {
                addRecord(title: "Payment Type".localized, value: agent, yPadding: 10)
        }
        
        // extra parameter for Mobile Number field for topup // changes by Ashish
        if let receiverAccNumber = dictValues["mobileNumber"] as? String {
            addRecord(title: "Mobile Number".localized, value: receiverAccNumber, yPadding: 10)
        }
        
        //changing name of mobile Number // Ashish Added
        if let receiverAccNumber = dictValues["mobileNumberTopup"] as? String {
            addRecord(title: "Receiver Mobile Number".localized, value: receiverAccNumber, yPadding: 10)
        }
        if let operatorName = dictValues["operatorName"] as? String {
            addRecord(title: "Operator Name".localized, value: operatorName, yPadding: 10)
        }
        if let isForPromocode = dictValues["isForPromocode"] as? Bool, isForPromocode == true {
            if let promocode = dictValues["promocode"] as? String, promocode.count > 0  {
                addRecord(title: "Promo Code".localized, value: promocode, yPadding: 10)
            }
            if let cName = dictValues["companyName"] as? String, cName.count > 0  {
                addRecord(title: "Company Name".localized, value: cName, yPadding: 10)
            }
            if let productName = dictValues["productName"] as? String, productName.count > 0  {
                addRecord(title: "Product Name".localized, value: productName, yPadding: 10)
            }
            if let productName = dictValues["freeProductName"] as? String, productName.count > 0  {
                addRecord(title: "Free Product Name".localized, value: productName, yPadding: 10)
            }
            if let buyingQuantity = dictValues["buyingQuantity"] as? String, buyingQuantity.count > 0  {
                let amont = wrapAmountWithCommaDecimal(key: "\(buyingQuantity)")
                addRecord(title: "Buying Quantity".localized, value: amont, yPadding: 10)
            }
            if let freeQuantity = dictValues["freeQuantity"] as? String, freeQuantity.count > 0  {
                let amont = wrapAmountWithCommaDecimal(key: "\(freeQuantity)")
                addRecord(title: "Free Quantity".localized, value: amont, yPadding: 10)
            }
            if let discountPercent = dictValues["discountPercent"] as? String, discountPercent.count > 0 {
                addRecord(title: "Discount Percentage".localized, value: discountPercent, yPadding: 10)
            }
            if let discountAmount = dictValues["discountAmount"] as? String, discountAmount.count > 0  {
                let amont = wrapAmountWithCommaDecimal(key: "\(discountAmount)")
                addRecord(title: "Discount Amount".localized, value: amont, yPadding: 10)
            }
            if let billAmount = dictValues["billAmount"] as? String, billAmount.count > 0 {
                addRecord(title: "Bill Amount".localized, value: billAmount, yPadding: 10)
            }
        }
        //Adding transaction id
        if let transactionID = dictValues["transactionID"] as? String {
            addRecord(title: "Transaction ID".localized, value: transactionID, yPadding: 10)
        }
        //Adding transaction type
        if let transactionType = dictValues["transactionType"] as? String {
            addRecord(title: "Transaction Type".localized, value: transactionType, yPadding: 10)
        }
        //Adding Payment Type
        if let paymentType = dictValues["paymentType"] as? String {
            addRecord(title: "Payment Type".localized, value: paymentType, yPadding: 10)
        }
       
        if let totalAmount = dictValues["totalAmount"] as? String {
                addRecord(title: "Net Receive Amount".localized, value: totalAmount + " MMK", yPadding: 10)
        }
        
        //adding offers values
        if let transactionOffer = dictValues["OfferObject"] as? OfferConfirmationScreen, let transID = dictValues["OfferObjectTransID"] as? String {
            
            //Adding sender account name title and value
            if let senderAccName = dictValues["senderAccNameOffer"] as? String {
                addRecord(title: "Sender Account Name".localized, value: senderAccName, yPadding: 5)
            }
            //Adding sender account number title and value
            if let senderAccNumber = dictValues["senderAccNoOffer"] as? String {
                addRecord(title: "Sender Account Number".localized, value: senderAccNumber, yPadding: 5)
            }
            
            //Adding promo receiver number
            if let promoReceiverNumber = dictValues["promoReceiverNumber"] as? String {
                addRecord(title: "Promo Receiver Number".localized, value: promoReceiverNumber, yPadding: 5)
            }
            
            if let offer = transactionOffer.offers, offer.paymentType == "0" {
                //Adding receiver account number title and value
                if let receiverAccNumber = dictValues["receiverAccNoOffer"] as? String {
                    addRecord(title: "Receiver Account Number".localized, value: "XXXXXX" + String(receiverAccNumber.suffix(4)), yPadding: 5)
                }
            } else {
                //Adding receiver account name title and value
                if let receiverAccName = dictValues["receiverAccNameOffer"] as? String {
                    addRecord(title: "Receiver Account Name".localized, value: receiverAccName, yPadding: 5)
                }
                //Adding receiver account number title and value
                if let receiverAccNumber = dictValues["receiverAccNoOffer"] as? String {
                    addRecord(title: "Receiver Account Number".localized, value: receiverAccNumber, yPadding: 5)
                }
            }
            addRecord(title: "Category".localized, value: "Offers", yPadding: 5)
            addRecord(title: "Promo Code".localized, value: transactionOffer.offers?.promotionCode ?? "", yPadding: 5)
            if appDel.currentLanguage == "my" {
                addRecord(title: "Company Name".localized, value: transactionOffer.offers?.categoryBName ?? "", yPadding: 5)
            }
            else if appDel.currentLanguage == "zh-Hans" {
                addRecord(title: "Company Name".localized, value: transactionOffer.offers?.categoryCName ?? "", yPadding: 5)
            }
            else {
                addRecord(title: "Company Name".localized, value: transactionOffer.offers?.categoryName ?? "", yPadding: 5)
            }
            
            let offersModel = transactionOffer.offers
            let freeQty = String(transactionOffer.offers?.freeQty ?? 0)
            let freeamont = wrapAmountWithCommaDecimal(key: "\(freeQty)")
            let buyQty = String(transactionOffer.offers?.buyingQty ?? 0)
            let buyamont = wrapAmountWithCommaDecimal(key: "\(buyQty)")
            
            if offersModel?.amountType == "2" || offersModel?.amountType == "3" || offersModel?.amountType == "4" {
                if appDel.currentLanguage == "my" {
                    addRecord(title: "Product Name".localized, value: transactionOffer.offers?.productBName ?? "", yPadding: 5)
                }
                else if appDel.currentLanguage == "zh-Hans" {
                    addRecord(title: "Product Name".localized, value: transactionOffer.offers?.productCName ?? "", yPadding: 5)
                }
                else {
                    addRecord(title: "Product Name".localized, value: transactionOffer.offers?.productName ?? "", yPadding: 5)
                }
                addRecord(title: "Free Product Name".localized, value: transactionOffer.offers?.freeProductName ?? "", yPadding: 5)
                addRecord(title: "Buying Quantity".localized, value: "\(buyamont)", yPadding: 5)
                addRecord(title: "Free Quantity".localized, value: "\(freeamont)", yPadding: 5)
            }
            if offersModel?.amountType == "5" || offersModel?.amountType == "6" || offersModel?.amountType == "7" {
                if appDel.currentLanguage == "my" {
                    addRecord(title: "Product Name".localized, value: transactionOffer.offers?.productBName ?? "", yPadding: 5)
                }
                else if appDel.currentLanguage == "zh-Hans" {
                    addRecord(title: "Product Name".localized, value: transactionOffer.offers?.productCName ?? "", yPadding: 5)
                }
                else {
                    addRecord(title: "Product Name".localized, value: transactionOffer.offers?.productName ?? "", yPadding: 5)
                }
                addRecord(title: "Buying Quantity".localized, value: "\(buyamont)", yPadding: 5)
            }
            
            if offersModel?.amountType == "0" || offersModel?.amountType == "3" || offersModel?.amountType == "6"//Fixed=Percentage calculate
            {
                let amount = Float(transactionOffer.amount ?? "0") ?? 0.0
                let percentageAmount = Float(offersModel?.value ?? Int(0.0))
                let percentageValue = Float((percentageAmount * 100) / amount)
                addRecord(title: "Discount Percentage".localized, value: CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(percentageValue)") + "%", yPadding: 5)
                addRecord(title: "Discount Amount".localized, value: self.getDigitDisplay(digit: percentageAmount)  + " MMK", yPadding: 5)
            } else if offersModel?.amountType == "1" || offersModel?.amountType == "4" || offersModel?.amountType == "7" { //Fixed value display
                let value = Float(offersModel?.value ?? Int(0.0))
                addRecord(title: "Discount Percentage".localized, value: self.getDigitDisplay(digit: value) + "%", yPadding: 5)
                let amount = Float(transactionOffer.amount ?? "0") ?? 0.0
                let percentageAmount = Float(offersModel?.value ?? Int(0.0))
                let percentageValue = Float((percentageAmount * amount) / 100)
                addRecord(title: "Discount Amount".localized, value: self.getDigitDisplay(digit: percentageValue)  + " MMK", yPadding: 5)
            }
            let amount = Float(transactionOffer.amount ?? "0") ?? 0.0
            
            addRecord(title: "Bill Amount".localized, value: self.getDigitDisplay(digit: amount) + " MMK", yPadding: 5)
            
            addRecord(title: "Transaction ID".localized, value: transID, yPadding: 5)
            addRecord(title: "Transaction Type".localized, value: "PAYTO", yPadding: 5)
            //Adding date and time
            if let transactionDate = dictValues["transactionDateOffer"] as? String {
                addRecord(title: "Date & Time".localized, value: transactionDate, yPadding: 5)
            }
            
            if transactionOffer.remarks?.count ?? 0 > 0 {
                addRecord(title: "Remarks".localized, value: transactionOffer.remarks ?? "", yPadding: 5)
            }
        }
        //Adding date and time
        if let transactionDate = dictValues["transactionDate"] as? String {
            addRecord(title: "Date & Time".localized, value: transactionDate, yPadding: 10)
        }
        
        let paddingRemarksAmount: CGFloat = 30
        let sizeRemarksAmount = CGSize(width: mainViewFrameSize.width - (paddingRemarksAmount * 2), height: 0)
        
        let viewRemarksAmount = UIView(frame: CGRect(origin: CGPoint(x: paddingRemarksAmount, y: currentYPosition + 30), size: sizeRemarksAmount))
        
        //Adding Remarks
        if let remarks = dictValues["remarks"] as? String {
            let paddinglabelRemark: CGFloat = 15
            let labelSize = CGSize(width: viewRemarksAmount.frame.size.width - (paddinglabelRemark * 2), height: 42)
            let labelRemarks = UILabel(frame: CGRect(origin: CGPoint(x: paddinglabelRemark, y: 0), size: labelSize))
            labelRemarks.adjustsFontSizeToFitWidth = true
            labelRemarks.numberOfLines = 2
            labelRemarks.textAlignment = .left
            labelRemarks.text = remarks
            if let myFont = UIFont(name: appFont, size: 12) {
                labelRemarks.font = myFont
            }
            labelRemarks.textColor = UIColor.black
            if ((viewRemarksAmount.frame.origin.y <= 842) && ((viewRemarksAmount.frame.origin.y + labelRemarks.frame.size.height)) > 842) {
                viewRemarksAmount.frame.origin.y = 842 + 50
            }
            viewRemarksAmount.frame = CGRect(x: viewRemarksAmount.frame.origin.x, y: viewRemarksAmount.frame.origin.y, width: viewRemarksAmount.frame.size.width, height: viewRemarksAmount.frame.size.height + labelRemarks.frame.size.height)
            viewRemarksAmount.addSubview(labelRemarks)
        }
        
        //Adding Amount
        if let amount = dictValues["amount"] as? NSMutableAttributedString {
            let paddinglabelAmount: CGFloat = 15
            let equalWidth: CGFloat = (viewRemarksAmount.frame.size.width - (paddinglabelAmount * 2)) / 2
            let labelSize = CGSize(width: equalWidth, height: 42)
            var yPos = viewRemarksAmount.frame.size.height
            if viewRemarksAmount.frame.size.height > 0 {
                let separator = UIView(frame: CGRect(x: 0, y: yPos, width: viewRemarksAmount.frame.size.width, height: 1))
                separator.backgroundColor = UIColor.gray
                viewRemarksAmount.addSubview(separator)
                yPos = yPos + 1
            }
            
            let labelAmountTitle = UILabel(frame: CGRect(origin: CGPoint(x: paddinglabelAmount, y: yPos), size: labelSize))
            labelAmountTitle.adjustsFontSizeToFitWidth = true
            labelAmountTitle.textAlignment = .left
            if let str = dictValues["paymentType"] as? String, (str == "Cash in" || str == "Cash Out") {
                labelAmountTitle.text =  (str == "Cash In") ? "Net Paid Digital Amount".localized : "Paid Digital Amount".localized
            } else if let agent = dictValues["Payment Type"] as? String, (agent == "Cash In" || agent == "Cash Out") {
                labelAmountTitle.text =  (agent == "Cash In") ? "Net Paid Digital Amount".localized : "Paid Digital Amount".localized
            } else {
                labelAmountTitle.text = "Amount Paid".localized
            }
            if let myFont = UIFont(name: appFont, size: 12) {
                labelAmountTitle.font = myFont
            }
            labelAmountTitle.textColor = UIColor.black
            
            let labelAmountValue = UILabel(frame: CGRect(origin: CGPoint(x: labelAmountTitle.frame.maxX, y: yPos), size: labelSize))
            labelAmountValue.adjustsFontSizeToFitWidth = true
            labelAmountValue.textAlignment = .right
            labelAmountValue.attributedText = amount
            //            if let myFont = UIFont(name: "Zawgyi-One", size: 12) {
            //                labelAmountValue.font = myFont
            //            }
            labelAmountValue.textColor = UIColor.black
            viewRemarksAmount.frame = CGRect(x: viewRemarksAmount.frame.origin.x, y: viewRemarksAmount.frame.origin.y, width: viewRemarksAmount.frame.size.width, height: labelAmountTitle.frame.maxY)
            viewRemarksAmount.addSubview(labelAmountTitle)
            viewRemarksAmount.addSubview(labelAmountValue)
        } else if var amount = dictValues["amount"] as? String {
            let paddinglabelAmount: CGFloat = 15
            let equalWidth: CGFloat = (viewRemarksAmount.frame.size.width - (paddinglabelAmount * 2)) / 2
            let labelSize = CGSize(width: equalWidth, height: 42)
            var yPos = viewRemarksAmount.frame.size.height
            if viewRemarksAmount.frame.size.height > 0 {
                let separator = UIView(frame: CGRect(x: 0, y: yPos, width: viewRemarksAmount.frame.size.width, height: 1))
                separator.backgroundColor = UIColor.gray
                viewRemarksAmount.addSubview(separator)
                yPos = yPos + 1
            }
            
            let labelAmountTitle = UILabel(frame: CGRect(origin: CGPoint(x: paddinglabelAmount, y: yPos), size: labelSize))
            labelAmountTitle.adjustsFontSizeToFitWidth = true
            labelAmountTitle.textAlignment = .left
          
            labelAmountTitle.text = "Amount Paid".localized
            
            if let myFont = UIFont(name: appFont, size: 12) {
                labelAmountTitle.font = myFont
            }
            labelAmountTitle.textColor = UIColor.black
            
            let labelAmountValue = UILabel(frame: CGRect(origin: CGPoint(x: labelAmountTitle.frame.maxX, y: yPos), size: labelSize))
            labelAmountValue.adjustsFontSizeToFitWidth = true
            labelAmountValue.textAlignment = .right
            if amount.lowercased().contains(find: "points") {
                labelAmountValue.text = amount
                if let myFont = UIFont(name: appFont, size: 12) {
                    labelAmountValue.font = myFont
                }
            } else {
                amount = amount.replacingOccurrences(of: "MMK", with: "")
                labelAmountValue.attributedText = wrapAmountWithMMK(bal: amount)
            }
            labelAmountValue.textColor = UIColor.black
            viewRemarksAmount.frame = CGRect(x: viewRemarksAmount.frame.origin.x, y: viewRemarksAmount.frame.origin.y, width: viewRemarksAmount.frame.size.width, height: labelAmountTitle.frame.maxY)
            if ((viewRemarksAmount.frame.minY <= 842) && (viewRemarksAmount.frame.maxY > 842)) {
                viewRemarksAmount.frame.origin.y = 842 + 50
            }
            viewRemarksAmount.addSubview(labelAmountTitle)
            viewRemarksAmount.addSubview(labelAmountValue)
        } else if let amount = dictValues["amountOffers"] as? String {
            let paddinglabelAmount: CGFloat = 15
            let equalWidth: CGFloat = (viewRemarksAmount.frame.size.width - (paddinglabelAmount * 2)) / 2
            let labelSize = CGSize(width: equalWidth, height: 42)
            var yPos = viewRemarksAmount.frame.size.height
            if viewRemarksAmount.frame.size.height > 0 {
                let separator = UIView(frame: CGRect(x: 0, y: yPos, width: viewRemarksAmount.frame.size.width, height: 1))
                separator.backgroundColor = UIColor.gray
                viewRemarksAmount.addSubview(separator)
                yPos = yPos + 1
            }
            
            let labelAmountTitle = UILabel(frame: CGRect(origin: CGPoint(x: paddinglabelAmount, y: yPos), size: labelSize))
            labelAmountTitle.adjustsFontSizeToFitWidth = true
            labelAmountTitle.textAlignment = .left
            labelAmountTitle.text = "Net Paid Amount".localized
            if let myFont = UIFont(name: appFont, size: 12) {
                labelAmountTitle.font = myFont
            }
            labelAmountTitle.textColor = UIColor.black
            
            let labelAmountValue = UILabel(frame: CGRect(origin: CGPoint(x: labelAmountTitle.frame.maxX, y: yPos), size: labelSize))
            labelAmountValue.adjustsFontSizeToFitWidth = true
            labelAmountValue.textAlignment = .right
            labelAmountValue.text = amount
            if let myFont = UIFont(name: appFont, size: 12) {
                labelAmountValue.font = myFont
            }
            labelAmountValue.textColor = UIColor.black
            viewRemarksAmount.frame = CGRect(x: viewRemarksAmount.frame.origin.x, y: viewRemarksAmount.frame.origin.y, width: viewRemarksAmount.frame.size.width, height: labelAmountTitle.frame.maxY)
            viewRemarksAmount.addSubview(labelAmountTitle)
            viewRemarksAmount.addSubview(labelAmountValue)
        }
        
        viewRemarksAmount.layer.borderWidth = 1
        viewRemarksAmount.layer.borderColor = UIColor.gray.cgColor
        viewMain.addSubview(viewRemarksAmount)
        currentYPosition = viewRemarksAmount.frame.maxY
        //Adding qr image
        if let qrImage = dictValues["qrImage"] as? UIImage {
            let sizeQRImage = CGSize(width: 100, height: 100)
            let xPos = (mainViewFrameSize.width - sizeQRImage.width) / 2
            if (((currentYPosition + 50) <= 842) && (currentYPosition + 150) > 842) {
                currentYPosition = 842
            }
            let imageViewQR = UIImageView(frame: CGRect(origin: CGPoint(x: xPos, y: currentYPosition + 50), size: sizeQRImage))
            imageViewQR.image = qrImage
            imageViewQR.contentMode = .scaleAspectFit
            currentYPosition = imageViewQR.frame.maxY
            viewMain.addSubview(imageViewQR)
        }
        viewMain.frame = CGRect(x: viewMain.frame.origin.x, y: viewMain.frame.origin.y, width: viewMain.frame.size.width, height: currentYPosition)
        //Generate pdf
        return generatePDF(viewForPDF: viewMain, pdfFileName: pdfFileName)
    }
    
    class func generatePDF(viewForPDF: UIView, pdfFileName: String = "Invoice") -> URL? {
        
        let render = UIPrintPageRenderer()
        
        // 3. Assign paperRect and printableRect
        let page = CGRect(x: 0, y: 0, width: 595, height: 842) // A4, 72 dpi
        let printable = page.insetBy(dx: 0, dy: 0)
        
        // render.addPrintFormatter(aView.viewPrintFormatter(), startingAtPageAt: 0)
        
        render.setValue(NSValue(cgRect: page), forKey: "paperRect")
        render.setValue(NSValue(cgRect: printable), forKey: "printableRect")
        
        // 4. Create PDF context and draw
        
        let priorBounds = viewForPDF.bounds
        let fittedSize = viewForPDF.sizeThatFits(CGSize(width:priorBounds.size.width, height:viewForPDF.bounds.size.height))
        viewForPDF.bounds = CGRect(x:0, y:0, width:fittedSize.width, height:fittedSize.height)
        let pdfPageBounds = CGRect(x:0, y:0, width:viewForPDF.frame.width, height: page.size.height)
        
        
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds, nil)
        var pageOriginY: CGFloat = 0
        while pageOriginY < fittedSize.height {
            UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
            UIGraphicsGetCurrentContext()!.saveGState()
            UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: -pageOriginY)
            viewForPDF.layer.render(in: UIGraphicsGetCurrentContext()!)
            UIGraphicsGetCurrentContext()!.restoreGState()
            pageOriginY += pdfPageBounds.size.height
        }
        UIGraphicsEndPDFContext()
        viewForPDF.bounds = priorBounds
        
        //get file path
        guard let documentDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else  { return nil}
        let properPDFFileName = pdfFileName.replacingOccurrences(of: ".pdf", with: "") + ".pdf"
        let pdfUrl = documentDirectories + "/" + properPDFFileName
        //Delete file if alreay exists
        do {
            try FileManager.default.removeItem(atPath: pdfUrl)
        } catch let error as NSError {
            println_debug("Error: \(error.domain)")
        }
        //Write file
        if pdfData.write(toFile: pdfUrl, atomically: true) {
            println_debug("Successfully wrote to \(pdfUrl)")
            
            let pathUrl = URL(fileURLWithPath: documentDirectories)
            let fileUrl = pathUrl.appendingPathComponent(properPDFFileName)
            return fileUrl
        }
        return nil
    }
    
    class func getDigitDisplay(digit : Float) -> String {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        return formatter.string(from: digit as NSNumber)!
    }
}
