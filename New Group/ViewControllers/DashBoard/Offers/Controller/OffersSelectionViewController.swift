//
//  OffersSelectionViewController.swift
//  OK
//
//  Created by Ashish on 7/17/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Foundation


protocol OfferPresenter : class {
    func presentController(vc: OffersTermsConditionViewController)
    func directPaymentNavigation(_ model: OffersListModel)
    func paytoPaymentNavigation(model: OffersListModel,list:[OffersListModel])
}

class OffersSelectionViewController: OKBaseController, UITableViewDelegate, UITableViewDataSource, OfferDisplayDataDelegate, OffersAlertDelegate , UICollectionViewDataSource,UICollectionViewDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var OfferTableView : UITableView!
    
    var list : [OffersListModel]?
    var newList : [OffersListModel]?

    weak var delegate : OfferPresenter?
    var txtSearch = ""
     let button = UIButton(type: .custom)
    //prabu
    var categoryList : [OffersCategoryModel]?
    var offercategoryList : [OffersListModel]?
    
    @IBOutlet weak var searchBarView: UIView!
    //AllOffers Button prabu
    @IBOutlet weak var searchTextfield : UITextField!
    @IBOutlet weak var searchborderView: UIView!{
        
        didSet {
            
            self.searchborderView.layer.borderColor = UIColor.black.cgColor
            self.searchborderView.layer.borderWidth = 1.0
        }
        
    }
    
    @IBOutlet weak var allOffersBtn: UIButton!{
        didSet {
            self.allOffersBtn.setTitle("All Offers".localized, for: .normal)
            self.allOffersBtn.layer.cornerRadius = 25.0
            self.allOffersBtn.layer.masksToBounds = true
            self.allOffersBtn.layer.borderColor = UIColor(red: 246/255, green: 198/255, blue: 0/225, alpha: 1).cgColor
            self.allOffersBtn.layer.borderWidth = 1.0
        }
    }
    
    //Category Button prabu
    @IBOutlet weak var categoriesBtn: UIButton!{
        didSet {
            self.categoriesBtn.setTitle("Categories".localized, for: .normal)
            self.categoriesBtn.layer.cornerRadius = 25.0
            self.categoriesBtn.layer.masksToBounds = true
            self.categoriesBtn.layer.borderColor = UIColor(red: 246/255, green: 198/255, blue: 0/225, alpha: 1).cgColor
            self.categoriesBtn.layer.borderWidth = 1.0
        }
    }
    
    //CollectionView prabu
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    
    var selectedIndexPath: IndexPath = []{
        didSet{
            categoryCollectionView.reloadData()
        }
    }
    
    var selectedCollectionsection = 0
    
    @IBOutlet weak var pageControl: UIPageControl!
    let collectionMargin = CGFloat(0)
    let itemSpacing = CGFloat(0)
    let itemHeight = CGFloat(150)
    var itemWidth = CGFloat(125)
    var currentItem = 0
    var refreshControl = UIRefreshControl()
    let noDataLabel = UILabel()
    
    //Constrain Outlet prabu
    @IBOutlet weak var collectionViewHeightOutlet: NSLayoutConstraint!
    @IBOutlet weak var searchViewHeightOutlet: NSLayoutConstraint!
    
    @IBOutlet weak var allcategorybuttonwidthConstrain: NSLayoutConstraint!
    @IBOutlet weak var allofferbuttonwidthContrain: NSLayoutConstraint!
    //MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
          collectionViewHeightOutlet.constant = 0
        self.allOffersBtn.backgroundColor = UIColor.colorWithRedValue(redValue: 246, greenValue: 198, blueValue: 0, alpha: 1)
        self.allOffersBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
        
     //   let bounds = UIScreen.main.bounds
        // iphone SE has 320 width
//        if bounds.width > 320 {
//            allofferbuttonwidthContrain.constant = 180
//            allcategorybuttonwidthConstrain.constant = 180
//        } else {
//            allofferbuttonwidthContrain.constant = 135
//            allcategorybuttonwidthConstrain.constant = 135
//        }
        
        searchTextfield.delegate = self
        
   //     allofferBtn(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //GeoLocationManager.shared.geteCurrentLocationDetails()

        noDataLabel.isHidden = true
        
        searchTextfield.placeholder = "Search".localized

        // Do any additional setup after loading the view.
        let className = String(describing: OffersTableViewCell.self)
        self.OfferTableView.register(UINib.init(nibName: className, bundle: Bundle.main), forCellReuseIdentifier: className)
        self.OfferTableView.delegate = self
        
        self.OfferTableView.tableFooterView = UIView()
        newList = list
        guard let items = list else {
            self.OfferTableView.isHidden = true
            return
        }
        
        if items.count > 0 {
            self.OfferTableView.isHidden = false
        } else {
            self.OfferTableView.isHidden = true
        }
       
        ////close-button-gray.png
                button.setImage(UIImage(named: "close_cancel.png")!.withRenderingMode(.alwaysOriginal), for: .normal)
                button.imageEdgeInsets = UIEdgeInsets(top: 0, left:0, bottom: 0, right: 0)
                button.frame = CGRect(x: CGFloat(searchTextfield.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
                button.addTarget(self, action: #selector(self.close), for: .touchUpInside)
                searchTextfield.rightView = button
                searchTextfield.rightViewMode = .always

        button.isHidden = true
        
        //Top Button
        self.allOffersBtn.tag = 1
        self.categoriesBtn.tag = 0
        
        self.OfferTableView.refreshControl = self.refreshControl
        self.refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        OfferTableView.addSubview(refreshControl)
    }
    
    //MARK:- Methods
     func initialSetupNew() {
       
            OffersManager.getHeadersTitle ({ [weak self](collections) in
                if collections == nil {
                    return
                }
                if collections!.count <= 0 {
                    return
                }
                OffersManager.getAllOffers({ [weak self](objects) in
                    if objects == nil {
                        return
                    }
                    if objects!.count <= 0 {
                        return
                    }
                    DispatchQueue.main.async {
                        
                        self!.list = objects
                        print("list tag----\(self!.list!.count)")

                        // Updating your data here...
                        print("Button tag----\(self!.allOffersBtn.tag)")
                        if self!.allOffersBtn.tag == 1 {
                            self!.searchTextfield.text = ""
                            self!.newList?.removeAll()
                            self!.offercategoryList?.removeAll()
                            self!.newList = objects
                            // print("offer tag----\(objects!.count)")
                           // print("offer tag----\(self!.newList!.count)")
                            self!.OfferTableView.reloadData()
                            
                        }
                        else
                        {
                            self!.searchTextfield.text = ""
                            self!.newList?.removeAll()
                            self!.categoryList?.removeAll()
                            self!.offercategoryList?.removeAll()
                            
                            self!.categoryList = collections
                            
                            if self!.selectedIndexPath.count != 0 {
                            self!.offercategoryList = self!.list!.filter({$0.categoryName.safelyWrappingString().uppercased() == self!.categoryList![self!.selectedIndexPath.section].categoryName.uppercased()})
                            } else {
                                
                                self!.offercategoryList = self!.list!.filter({$0.categoryName.safelyWrappingString().uppercased() == self!.categoryList![0].categoryName.uppercased()})
                            }
                            
                            self!.newList = self!.offercategoryList
                           // print("caegory tag----\(self!.offercategoryList!.count)")
                           // print("category tag----\(self!.newList!.count)")
                            self!.OfferTableView.reloadData()
                        }
                        self!.refreshControl.endRefreshing()
                        
                      
                    }
                })
            })
        
    }
    
   
    
    @objc func refresh(sender:AnyObject)
    {
        
        self.initialSetupNew()
    }
    
    @objc func close (sender:Any)
    {
        if self.allOffersBtn.tag == 1 {
            searchTextfield.text = ""
            newList?.removeAll()
            offercategoryList?.removeAll()
            newList = list
        }
        else
        {
            searchTextfield.text = ""
            newList?.removeAll()
            newList = offercategoryList
        }
         self.OfferTableView.reloadData()
        searchTextfield.text = ""
        button.isHidden = true
    }
    
    //MARK: - Method
    func searchBarCancel(notification:Notification) -> Void {
        newList = list
        OfferTableView.reloadData()
    }
    
    func displaySearchBar(str:String) -> Void {
        println_debug(list?.count)
        let strSearch = str
        self.applySearch(with: strSearch)
        txtSearch = strSearch
        
    }
    
    func applySearch(with searchText: String) {
        
        if self.allOffersBtn.tag == 1 {
            
            if searchText.isEmpty {
                
                newList?.removeAll()
                newList = list
                
                println_debug(newList?.count)
                OfferTableView.reloadData()
                
            } else {
                
                newList?.removeAll()
                
                newList = list?.filter {
                    if let promCode = $0.promotionCode {
                        if promCode.lowercased().range(of: searchText.lowercased()) != nil{
                            return true
                        }
                    }

                    if let promCode = $0.promotionName {
                        if promCode.lowercased().range(of: searchText.lowercased()) != nil{
                            return true
                        }
                    }
                    
                    return false
                }
                println_debug(newList?.count)
                OfferTableView.reloadData()
            }
            
            
        } else {
            
            if searchText.isEmpty {
                
                newList?.removeAll()
                newList = offercategoryList
                
                println_debug(newList?.count)
                OfferTableView.reloadData()
                
            } else {
        
            newList?.removeAll()
            newList = offercategoryList?.filter {
                if let promCode = $0.promotionCode {
                    if promCode.lowercased().range(of: searchText.lowercased()) != nil{
                        return true
                    }
                }
                return false
            }
            println_debug(newList?.count)
            OfferTableView.reloadData()
            }
        }
    }
    
    
    @objc func backButton() {
        
        self.navigationController!.popViewController(animated: true)
        
    }
    
    // UITextField Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let amount = searchTextfield.text {
            if amount == "" {
                button.isHidden = true

            }
            else
            {

                button.isHidden = false

            }
    }
}
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let amount = searchTextfield.text {
            if amount == "" {
                  button.isHidden = true
               
            }
            else
            {
                button.isHidden = false
            }
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == searchTextfield{
            
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            
            if searchTextfield.text != nil {
                
                 if !(updatedText == updatedText.components(separatedBy: NSCharacterSet(charactersIn: SEARCHCHARSETOffer).inverted).joined(separator: "")) { return false }
                
                if updatedText.count > 50 {
                    return false
                }
                if updatedText == " " || updatedText == "  "{
                    return false
                }
                if (updatedText.contains("  ")){
                    return false
                }
                
                let containsEmoji: Bool = updatedText.containsEmoji
                if (containsEmoji){
                    return false
                }
                
                DispatchQueue.main.async {
                    self.displaySearchBar(str: updatedText)
                }
                
            }
                if updatedText == "" {
                     button.isHidden = true
                }
                else
                {
                    button.isHidden = false
                }
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        if textField == searchTextfield{
            
            searchTextfield.text = ""
            
            if self.allOffersBtn.tag == 1 {
                
                newList = list
                OfferTableView.reloadData()
                
            } else {
                
                offercategoryList = list!.filter({$0.categoryName.safelyWrappingString().uppercased() == categoryList![selectedCollectionsection].categoryName.uppercased()})
                
                newList = offercategoryList
                OfferTableView.reloadData()
            }
            
        }
        textField.resignFirstResponder()
        return true
    }
    
    
    
    //MARK:- Search Button
    @IBAction func allsearchBtn(_ sender: Any) {
        
        if searchTextfield.text != nil {
            DispatchQueue.main.async {
                self.displaySearchBar(str: self.searchTextfield.text!)
            }
            
        }
    }
    
    //MARK:- UITableViewDataSource & UItableViewDelegates
    func numberOfSections(in tableView: UITableView) -> Int {
          noDataLabel.frame = CGRect(x: 0, y: tableView.frame.origin.y + 15, width: screenWidth, height: 40)
        if self.newList?.count == 0 {
            noDataLabel.isHidden = false
            noDataLabel.text = "No Offers Found !!".localized
            noDataLabel.textAlignment = .center
            noDataLabel.font = UIFont(name: appFont, size: 18)
           self.view .addSubview(noDataLabel)
        } else {
                noDataLabel.isHidden = true
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: OffersTableViewCell.self)) as! OffersTableViewCell
        cell.delegate = self
        if let model = self.newList {
            if let dataModel = model[safe: indexPath.row] {
                cell.wrapCellData(dataModel)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (newList == nil) ? 0 : newList!.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.didSelectListObject(model: newList![indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 215.00
    }
    
    //MARK:- Cell Delegate
    func didSelectListObject(model: OffersListModel) {
        
        // terms and condition direct
        isPayToDashboard = false
        guard let tnc = self.storyboard?.instantiateViewController(withIdentifier: String(describing:OffersTermsConditionViewController.self)) as? OffersTermsConditionViewController else { return }
        tnc.modalPresentationStyle = .overCurrentContext
        tnc.delegate = self
        tnc.model = model
        tnc.conditions = model.termsAndCondition
        self.delegate?.presentController(vc: tnc)
    }
    
    func didTappedTnC(withModel model: OffersListModel) {
//        guard let tnc = self.storyboard?.instantiateViewController(withIdentifier: String(describing:OffersTermsConditionViewController.self)) as? OffersTermsConditionViewController else { return }
//        tnc.modalPresentationStyle = .overCurrentContext
//        tnc.conditions = model.termsAndCondition
//        self.delegate?.presentController(vc: tnc)
    }
   
    //MARK:- TNC Delegates
    func navigateToPayment(id: OffersListModel?) {

        guard let offerID = id else { return }
        
        guard let modelArray = list else { return }
        
        if let filteredModel = modelArray.first(where: {$0.promotionCode.safelyWrappingString() == offerID.promotionCode.safelyWrappingString()}) {
            //self.delegate?.paytoPaymentNavigation(filteredModel)
            self.delegate?.paytoPaymentNavigation(model: filteredModel,list:list ?? [filteredModel])
        }
    }

    //MARK:- Category Button
    //prabu
    @IBAction func allofferBtn(_ sender: Any) {
        
        txtSearch = ""
        searchTextfield.text = ""
        
        self.close(sender: self)

        
        newList?.removeAll()
        offercategoryList?.removeAll()
        
        self.allOffersBtn.tag = 1
        self.categoriesBtn.tag = 0
        
        self.allOffersBtn.backgroundColor = UIColor.colorWithRedValue(redValue: 246, greenValue: 198, blueValue: 0, alpha: 1)
        self.allOffersBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
        
        self.categoriesBtn.backgroundColor = UIColor.white
        self.categoriesBtn.setTitleColor(UIColor.black, for: UIControl.State.normal)
        
        collectionViewHeightOutlet.constant = 0
        searchViewHeightOutlet.constant = 60
        
        newList = list
        OfferTableView.reloadData()
        
    }
    
    
    @IBAction func allcategoriesBtn(_ sender: Any) {
    
        
        txtSearch = ""
        searchTextfield.text = ""
        
        self.close(sender: self)
        
        newList?.removeAll()
        offercategoryList?.removeAll()
        
        self.categoriesBtn.tag = 1
        self.allOffersBtn.tag = 0
        
        self.categoriesBtn.backgroundColor = UIColor.colorWithRedValue(redValue: 246, greenValue: 198, blueValue: 0, alpha: 1)
        self.categoriesBtn.setTitleColor(UIColor.white, for:UIControl.State.normal)
        
        self.allOffersBtn.backgroundColor = UIColor.white
        self.allOffersBtn.setTitleColor(UIColor.black, for: UIControl.State.normal)
        
        collectionViewHeightOutlet.constant = 150
        searchViewHeightOutlet.constant = 60
        
        selectedIndexPath = []
        
        self.categoryCollectionView.dataSource = self
        self.categoryCollectionView.delegate = self
        categoryCollectionView.allowsMultipleSelection = false
        self.categoryCollectionView.reloadData()
        
        
        let indexPath = self.categoryCollectionView.indexPathsForSelectedItems?.last ?? IndexPath(item: 0, section: 0)
       self.categoryCollectionView.selectItem(at: indexPath, animated: false, scrollPosition: UICollectionView.ScrollPosition.centeredHorizontally)
        
       
        setupPagination()
        
        offercategoryList = list!.filter({$0.categoryName.safelyWrappingString().uppercased() == categoryList![0].categoryName.uppercased()})
        
        newList = offercategoryList
        OfferTableView.reloadData()
        
       
    }
    
    
    
    //MARK:- COLLECTION VIEW
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return categoryList!.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == categoryCollectionView {
            return  1
        } else {
            return 10
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "offercollectioncell", for: indexPath as IndexPath) as! categoryCustomCell
        
        cell.categoryimageView.layer.borderWidth = 1.0
        cell.categoryimageView.layer.masksToBounds = false
        cell.categoryimageView.layer.borderColor = UIColor(red: 220/255, green: 220/255, blue: 220/225, alpha: 0.2).cgColor
        cell.categoryimageView.layer.cornerRadius = cell.categoryimageView.frame.size.width / 2
        cell.categoryimageView.clipsToBounds = true
        
        cell.categorytopView.layer.borderWidth = 2.0
        cell.categorytopView.layer.masksToBounds = false
        cell.categorytopView.layer.borderColor = UIColor(red: 220/255, green: 220/255, blue: 220/225, alpha: 0.2).cgColor
        cell.categorytopView.layer.cornerRadius = cell.categorytopView.frame.size.width / 2
        cell.categorytopView.clipsToBounds = true
        
        let imageUrl = categoryList![indexPath.section].categoryImage
        
        let finalimgUrl = imageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        self.loadCell(imgUrl:finalimgUrl!, imageView: cell.categoryimageView)
        
        if indexPath == selectedIndexPath {
            
            cell.categorytopView.layer.borderColor = UIColor(red: 246/255, green: 198/255, blue: 0/225, alpha: 1).cgColor
            cell.categorytopView.backgroundColor = UIColor.white
            //Name And Image
            cell.categorynameLbl.text = categoryList![indexPath.section].categoryName
            cell.categorynameLbl.textColor = kYellowColor
            
        }else{
            
                if cell.isSelected {
                    
                    cell.categorytopView.layer.borderColor = UIColor(red: 246/255, green: 198/255, blue: 0/225, alpha: 1).cgColor
                       cell.categorytopView.backgroundColor = UIColor.white
                    //Name And Image
                    cell.categorynameLbl.text = categoryList![indexPath.section].categoryName
                    cell.categorynameLbl.textColor = kYellowColor
                    
                } else {

                    cell.categorytopView.layer.borderColor = UIColor(red: 220/255, green: 220/255, blue: 220/225, alpha: 0.2).cgColor
                   cell.categorytopView.backgroundColor = UIColor.clear
                    //Name And Image
                    cell.categorynameLbl.text = categoryList![indexPath.section].categoryName
                    cell.categorynameLbl.textColor = UIColor.black
            }

            
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if selectedCollectionsection == indexPath.section {
            println_debug("don't reload")
        } else {
            
            txtSearch = ""
            searchTextfield.text = ""
            
            self.close(sender: self)

            
        selectedIndexPath = indexPath
        selectedCollectionsection = indexPath.section
        
            //Reload at center align
            categoryCollectionView.scrollToItem(at: selectedIndexPath, at: .centeredHorizontally, animated: true)
            
        //Category manual select reload
        offercategoryList = list!.filter({$0.categoryName.safelyWrappingString().uppercased() == categoryList![indexPath.section].categoryName.uppercased()})
        
        newList = offercategoryList
        
        if newList!.count > 0 {
            searchViewHeightOutlet.constant = 60
            
            OfferTableView.reloadData()
            
                let indexPath = IndexPath(row: 0, section: 0)
                self.OfferTableView.scrollToRow(at: indexPath, at: .top, animated: true)
           
            
        } else {
            searchViewHeightOutlet.constant = 0
            
            OfferTableView.reloadData()

        }
            
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "offercollectioncell", for: indexPath as IndexPath) as! categoryCustomCell
        
             cell.categorytopView.layer.borderColor = UIColor(red: 220/255, green: 220/255, blue: 220/225, alpha: 0.2).cgColor
           cell.categorytopView.backgroundColor = UIColor.clear
        //Name And Image
        cell.categorynameLbl.text = categoryList![indexPath.section].categoryName
        cell.categorynameLbl.textColor = UIColor.black
        
    }
    
   /* func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        let item = collectionView.cellForItem(at: indexPath)
        if item?.isSelected ?? false {
            collectionView.deselectItem(at: indexPath, animated: true)
        } else {
            collectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
        }
        
        return false
    } */
    
    
    //MARK: IMAGE CACHE
    let imageCache = NSCache<NSString, UIImage>()
    func loadCell(imgUrl : String , imageView:UIImageView)
    {
        if let cachedImage = imageCache.object(forKey: imgUrl as NSString) {
            DispatchQueue.main.async(execute: { () -> Void in
                imageView.image = cachedImage
            })
        } else {
            URLSession.shared.dataTask(with: NSURL(string: imgUrl)! as URL, completionHandler: { (data, response, error) -> Void in
                if error != nil {
                    //                    println_debug(error ?? "No Error")
                    return
                }
                else
                {
                    if let data = data, let image = UIImage(data: data) {
                        self.imageCache.setObject(image, forKey: imgUrl as NSString)
                        DispatchQueue.main.async(execute: { () -> Void in
                            let image = UIImage(data: data)
                            imageView.image = image
                        })
                    }
                }
            }).resume()
        }
    }
    
    //MARK: PAGINATION SETUP
    func setupPagination() {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        
        //itemWidth =  UIScreen.main.bounds.width - collectionMargin * 2.0
        
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
        layout.headerReferenceSize = CGSize(width: collectionMargin, height: 0)
        layout.footerReferenceSize = CGSize(width: collectionMargin, height: 0)
        
        layout.minimumLineSpacing = itemSpacing
        layout.scrollDirection = .horizontal
        categoryCollectionView!.collectionViewLayout = layout
        categoryCollectionView?.decelerationRate = UIScrollView.DecelerationRate.fast
        
        
    }
    
    
}

class categoryCustomCell: UICollectionViewCell {
    
    @IBOutlet var categorynameLbl: UILabel!
    @IBOutlet var categoryimageView: UIImageView!
    @IBOutlet weak var categorytopView: UIView!
    
    
}
