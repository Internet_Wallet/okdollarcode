//
//  OffersRecieptViewController.swift
//  OK
//
//  Created by Ashish on 1/5/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

class OffersRecieptViewController: OKBaseController, UITableViewDelegate, UITableViewDataSource, TabOptionProtocol, OffersMoreAction {
    
    var contactAdded: Bool = false
     var isInFavArray = [Bool]()
   var favoriteAddedCheck: Bool = false
    @IBOutlet weak var offerTable : UITableView!
    @IBOutlet weak var containerFooterView: UIView!
    
    @IBOutlet weak var cardView: CardDesignView!
    var viewOptions: TabOptionsOnReceipt?
    //Two Transaction Receipt
    var contents: [String] = ["Category", "Promocode", "Discount Percentage", "Discount Amount", "Bill Amount", "Net Paid Amount", "Transaction ID", "Transaction Type", "Remarks"]//Direct offer
    var inDirectContents: [String] = ["Merchant","Category", "Promocode", "Company Name","Discount Percentage", "Discount Amount", "Bill Amount", "Net Paid Amount", "Transaction ID", "Transaction Type", "Remarks"]
    var inDirectContentsFree: [String] = ["Merchant","Category", "Promocode", "Company Name","Product Name","Free Product Name","Buying Quantity","Free Quantity","Bill Amount", "Net Paid Amount", "Transaction ID", "Transaction Type", "Remarks"]
     var inDirectContentsFreeWAP: [String] = ["Merchant","Category", "Promocode", "Company Name","Product Name","Free Product Name","Buying Quantity","Free Quantity","Discount Percentage", "Discount Amount", "Bill Amount", "Net Paid Amount", "Transaction ID", "Transaction Type", "Remarks"]
    //Product Buy Qty
    var inDirectContentsFreeProduct: [String] = ["Merchant","Category", "Promocode", "Company Name","Product Name","Buying Quantity","Bill Amount", "Net Paid Amount", "Transaction ID", "Transaction Type", "Remarks"]
    var inDirectContentsFreeWAPProduct: [String] = ["Merchant","Category", "Promocode", "Company Name","Product Name","Buying Quantity","Discount Percentage", "Discount Amount", "Bill Amount", "Net Paid Amount", "Transaction ID", "Transaction Type", "Remarks"]
    //One Transaction Array
    var oneTxContents: [String] = ["Promo Receiver Number","Receiver Account Number","Merchant","Category", "Promocode", "Company Name","Discount Percentage", "Discount Amount", "Bill Amount", "Net Paid Amount", "Transaction ID", "Transaction Type", "Remarks"]
    var oneTxContentsFree: [String] = ["Promo Receiver Number","Receiver Account Number","Merchant","Category", "Promocode", "Company Name","Product Name","Free Product Name","Buying Quantity","Free Quantity","Bill Amount", "Net Paid Amount", "Transaction ID", "Transaction Type", "Remarks"]
    var oneTxContentsFreeWAP: [String] = ["Promo Receiver Number","Receiver Account Number","Merchant","Category", "Promocode", "Company Name","Product Name","Free Product Name","Buying Quantity","Free Quantity","Discount Percentage", "Discount Amount", "Bill Amount", "Net Paid Amount", "Transaction ID", "Transaction Type", "Remarks"]
    var oneTxContentsFreeProduct: [String] = ["Promo Receiver Number","Receiver Account Number","Merchant","Category", "Promocode", "Company Name","Product Name","Buying Quantity","Bill Amount", "Net Paid Amount", "Transaction ID", "Transaction Type", "Remarks"]
    var oneTxContentsFreeWAPProduct: [String] = ["Promo Receiver Number","Receiver Account Number","Merchant","Category", "Promocode", "Company Name","Product Name","Buying Quantity","Discount Percentage", "Discount Amount", "Bill Amount", "Net Paid Amount", "Transaction ID", "Transaction Type", "Remarks"]
    
    var offerModel : OfferConfirmationScreen?

    var recieptModel : OfferRecieptModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            self.changeCancelImplementation()
        }
        self.offerTable.tableFooterView = UIView()
        self.navigationItem.leftBarButtonItem  = nil
        self.title = "Receipt".localized
        guard let bView = Bundle.main.loadNibNamed("TabOptionsOnReceipt", owner: self, options: nil)?.first as? TabOptionsOnReceipt else { return }
        viewOptions = bView
        viewOptions?.delegate = self
        viewOptions?.frame = CGRect(x: 0, y: 0, width: containerFooterView.frame.size.width, height: containerFooterView.frame.size.height)
        if let options = viewOptions {
            containerFooterView.addSubview(options)
            viewOptions!.lblHome.text = "HomePagePT".localized
            viewOptions!.lblMore.text = "More".localized
            viewOptions!.lblAddContact.text = "TabbarAddContact".localized
            viewOptions!.lblAddFav.text = "Add Favorite".localized
            
            let dest = self.append09Format(offerModel?.mobileNumber ?? "0095")
            let isInFav = checkForFavorites(dest, type: "PAYTO").isInFavorite
            if isInFav {
                favoriteAddedCheck = true
            }
            
            if favoriteAddedCheck {
                addFavUI(isInFav: true)
            }
            
            let (isadd, _) = checkForContact(dest)
            if isadd {
             contactAdded = true
            }
            
            if contactAdded {
            addConUI(isInCon: true)
            }
            
            if let offer = offerModel?.offers, offer.paymentType == "0" {
                viewOptions?.viewAddFav.isHidden = true
                viewOptions?.viewAddContact.isHidden = true
            }

            viewOptions!.layoutIfNeeded()
        }
        
        self.loadBackButtonOffer()
    }
    
    func loadBackButtonOffer() {
        let backBtn = UIBarButtonItem(image: UIImage(), style: .plain, target: self, action: #selector(backActionOffer))
        self.navigationItem.leftBarButtonItem  = backBtn
    }
    
    @objc func backActionOffer() {
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if contactAdded {
            addConUI(isInCon: true)
        }
    }
    
    //MARK:- DELEGATES & DATASOURCE
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 2 {
          if let offer = offerModel?.offers, offer.paymentType == "0" {//Direct Offer
            if offerModel?.remarks?.count ?? 0 > 0 {
              return contents.count
            }
            return contents.count-1
          } else {
            if offerModel?.offers?.promotionTransactionCount == 1 {//One Transaction
                if offerModel?.remarks?.count ?? 0 > 0 {
                    if offerModel?.offers?.amountType == "0" || offerModel?.offers?.amountType == "1"
                    {
                        return oneTxContents.count
                    } else if offerModel?.offers?.amountType == "3" || offerModel?.offers?.amountType == "4"
                    {
                        return oneTxContentsFreeWAP.count
                    } else if offerModel?.offers?.amountType == "2"{
                        return oneTxContentsFree.count
                    } else if offerModel?.offers?.amountType == "6" || offerModel?.offers?.amountType == "7" {
                        return oneTxContentsFreeWAPProduct.count
                    } else {//Product
                        return oneTxContentsFreeProduct.count
                    }
                } else {
                    if offerModel?.offers?.amountType == "0" || offerModel?.offers?.amountType == "1" {
                        return oneTxContents.count - 1
                    } else if offerModel?.offers?.amountType == "3" || offerModel?.offers?.amountType == "4" {
                        return oneTxContentsFreeWAP.count - 1
                    } else if offerModel?.offers?.amountType == "2"{
                        return oneTxContentsFree.count - 1
                    } else if offerModel?.offers?.amountType == "6" || offerModel?.offers?.amountType == "7" {
                        return oneTxContentsFreeWAPProduct.count - 1
                    } else {//Product
                        return oneTxContentsFreeProduct.count - 1
                    }
                }
            } else {//Two Transaction
            if offerModel?.remarks?.count ?? 0 > 0 {
                if offerModel?.offers?.amountType == "0" || offerModel?.offers?.amountType == "1"
                {
                    return inDirectContents.count
                } else if offerModel?.offers?.amountType == "3" || offerModel?.offers?.amountType == "4"
                {
                    return inDirectContentsFreeWAP.count
                } else if offerModel?.offers?.amountType == "2" {
                    return inDirectContentsFree.count
                } else if offerModel?.offers?.amountType == "6" || offerModel?.offers?.amountType == "7"
                {
                    return inDirectContentsFreeWAPProduct.count
                } else {
                    return inDirectContentsFreeProduct.count
                }
            } else {
                if offerModel?.offers?.amountType == "0" || offerModel?.offers?.amountType == "1" {
                    return inDirectContents.count - 1
                } else if offerModel?.offers?.amountType == "3" || offerModel?.offers?.amountType == "4" {
                    return inDirectContentsFreeWAP.count - 1
                } else if offerModel?.offers?.amountType == "2" {
                    return inDirectContentsFree.count - 1
                } else if offerModel?.offers?.amountType == "6" || offerModel?.offers?.amountType == "7"
                {
                    return inDirectContentsFreeWAPProduct.count - 1
                } else {
                    return inDirectContentsFreeProduct.count - 1
                }
            }
            }
          }
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: OfferTopOKCell.identifier, for: indexPath) as? OfferTopOKCell
            return cell!
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: OfferReceiptCellHeader.identifier, for: indexPath) as? OfferReceiptCellHeader
             let dest = self.append09Format(recieptModel?.destination ?? "0095")
            cell?.wrapContent(model: self.offerModel, number: dest)
            return cell!
        } else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: OfferReceiptCellContent.identifier, for: indexPath) as? OfferReceiptCellContent
          if let offer = offerModel?.offers, offer.paymentType == "0" {//Direct
            cell?.wrapData(key: contents[indexPath.row],model: offerModel, destDetails: recieptModel)
          } else {
            if offerModel?.offers?.promotionTransactionCount == 1 {//One Transaction
                if offerModel?.offers?.amountType == "0" || offerModel?.offers?.amountType == "1" {//Fixed Amount & Percentage
                    cell?.wrapData(key: oneTxContents[indexPath.row],model: offerModel, destDetails: recieptModel)
                } else if offerModel?.offers?.amountType == "3" || offerModel?.offers?.amountType == "4" {//Fixed Amount & Percentage with product
                    cell?.wrapData(key: oneTxContentsFreeWAP[indexPath.row],model: offerModel, destDetails: recieptModel)
                } else if offerModel?.offers?.amountType == "2" {//Free offer
                    cell?.wrapData(key: oneTxContentsFree[indexPath.row],model: offerModel, destDetails: recieptModel)
                } else if offerModel?.offers?.amountType == "6" || offerModel?.offers?.amountType == "7" {//Buy Amount & Percentage with product
                    cell?.wrapData(key: oneTxContentsFreeWAPProduct[indexPath.row],model: offerModel, destDetails: recieptModel)
                } else {//Buy offer
                    cell?.wrapData(key: oneTxContentsFreeProduct[indexPath.row],model: offerModel, destDetails: recieptModel)
                }
            } else {//Two Transaction
            if offerModel?.offers?.amountType == "0" || offerModel?.offers?.amountType == "1" {//Fixed Amount & Percentage
                cell?.wrapData(key: inDirectContents[indexPath.row],model: offerModel, destDetails: recieptModel)
            } else if offerModel?.offers?.amountType == "3" || offerModel?.offers?.amountType == "4" {//Fixed Amount & Percentage with product
                cell?.wrapData(key: inDirectContentsFreeWAP[indexPath.row],model: offerModel, destDetails: recieptModel)
            } else if offerModel?.offers?.amountType == "2" {//Free offer
                cell?.wrapData(key: inDirectContentsFree[indexPath.row],model: offerModel, destDetails: recieptModel)
            } else if offerModel?.offers?.amountType == "6" || offerModel?.offers?.amountType == "7" {//Fixed Amount & Percentage with product
                cell?.wrapData(key: inDirectContentsFreeWAPProduct[indexPath.row],model: offerModel, destDetails: recieptModel)
            } else {//Free offer
                cell?.wrapData(key: inDirectContentsFreeProduct[indexPath.row],model: offerModel, destDetails: recieptModel)
                }
            }
          }
            return cell!
        } else if indexPath.section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: OfferReceiptCellFooter.identifier, for: indexPath) as? OfferReceiptCellFooter
            
            if let date = self.recieptModel?.responsects {
                let dateFormat = date.components(separatedBy: " ").first?.dateValue(dateFormatIs: "dd-MMM-yyyy")
                let dateKey = (dateFormat?.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy"))!
                //let dateKey = date.components(separatedBy: " ").first ?? ""
                let timeKey = date.components(separatedBy: " ").last ?? ""
                cell?.wrapContent(model: self.offerModel, date: dateKey, time: timeKey)
            } else {
                cell?.wrapContent(model: self.offerModel, date: "", time: "")
            }
            return cell!
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return 50.00
        }
        else if indexPath.section == 1 {
          if let offer = offerModel?.offers, offer.paymentType == "0" {
            return 100.00
          } else {
            return 140.00
          }
          
        } else if indexPath.section == 2 {
            return 40.00
        } else {
            return 40.00
        }
    }
    
    //Helpers
    func append09Format(_ dest: String) -> String {
        if dest.hasPrefix("00") {
            var number = dest.deletingPrefix("00")
            number = "+" + number
            let country = identifyCountry(withPhoneNumber: number)
            let final = number.replacingOccurrences(of: country.countryCode, with: "0")
            return final
        }
        return dest
    }
    
    //MARK:- TAB BAR ACTION
    func addFavorite() {
        if favoriteAddedCheck {
            println_debug("already exist")
            self.showErrorAlert(errMessage: "This number already added as Favourite.".localized)
        } else {
        let phone =  self.recieptModel?.destination ?? ""
        let name  = self.recieptModel?.merchantname ?? "Unknown"
        let vc = self.addFavoriteController(withName: name, favNum: phone, type: "PAYTO", amount: self.recieptModel?.amount ?? "")
        if let vcs = vc {
            vcs.delegate = self
            self.navigationController?.present(vcs, animated: true, completion: nil)
        }
        }
    }
    
    func addFavUI(isInFav: Bool) {
        if let _ = viewOptions {
            viewOptions?.imgAddFav.image = viewOptions?.imgAddFav.image?.withRenderingMode(.alwaysTemplate)
            if isInFav {
                viewOptions?.imgAddFav.tintColor = kYellowColor
                viewOptions!.lblAddFav.textColor = kYellowColor
            } else {
                viewOptions?.imgAddFav.tintColor = UIColor.gray
             //   viewOptions!.lblAddFav.textColor = optionTextDefaultColor
            }
            viewOptions!.layoutIfNeeded()
        }
    }
    func addConUI(isInCon: Bool) {
        if let _ = viewOptions {
            viewOptions?.imgAddContact.image = viewOptions?.imgAddContact.image?.withRenderingMode(.alwaysTemplate)
            if isInCon {
                viewOptions?.imgAddContact.tintColor = kYellowColor
                viewOptions!.lblAddContact.textColor = kYellowColor
            } else {
                viewOptions?.imgAddContact.tintColor = UIColor.gray
                //   viewOptions!.lblAddFav.textColor = optionTextDefaultColor
            }
            viewOptions!.layoutIfNeeded()
        }
    }
    
    func addContact() {
        
        if contactAdded {
            self.showErrorAlert(errMessage: "Contact Already Added".localized)
        }
        
        if let _ = viewOptions {
            if viewOptions!.lblAddContact.textColor != kYellowColor {
                if #available(iOS 9.0, *) {
                    let store     = CNContactStore()
                    let contact   = CNMutableContact()
                    let phone =  self.recieptModel?.destination ?? ""
                    let name  = self.recieptModel?.merchantname ?? "Unknown"
                    let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue: wrapFormattedNumber(phone)))
                    contact.phoneNumbers = [homePhone]
                    contact.givenName = name
                    let controller = CNContactViewController.init(forNewContact: contact)
                    controller.contactStore = store
                    controller.delegate     = self
                    controller.title        = "Add Contact".localized
                    self.navigationController?.setNavigationBarHidden(false, animated: true)
                    self.navigationController?.navigationBar.isHidden = false
                    self.navigationController?.navigationBar.backgroundColor = UIColor.init(hex: "F3C632")
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            }
        }
        
    }
    
    func goHome() {
     //   self.navigationController?.dismiss(animated: true, completion: nil)
        UserDefaults.standard.set(true, forKey: "isLogOut")
        UserLogin.shared.loginSessionExpired = true
        performSegue(withIdentifier: "unwindSegueToVC1", sender: self)
    }
    
    func showMore() {
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "OffersMoreViewController") as? OffersMoreViewController else { return }
        vc.delegate = self
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func openShare() {
        
        let tableviewframe = offerTable.frame
        
        offerTable.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(offerTable.frame.size.width), height: CGFloat(730))
        
        UIGraphicsBeginImageContextWithOptions(
            offerTable.frame.size, false, 0.0)
        
        // Draw view in that context
        offerTable.drawHierarchy(in: offerTable.frame, afterScreenUpdates: true)
        
        // And finally, get image
        let imageShare = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        //let imageShare = cardView.offersnapshotOfCustomeView
        let vc = UIActivityViewController(activityItems: [imageShare as Any], applicationActivities: [])
        DispatchQueue.main.async {self.present(vc, animated: true)}
        
        offerTable.frame = tableviewframe
    }
    
  func redirectOffersListing()
  {
    self.navigationController?.dismiss(animated: false, completion: nil)
  }
    
    func openInvoice() {
        
        func formattedDateFromString(dateString: String, withFormat format: String) -> String? {
            
            let inputFormatter = DateFormatter.init()
            inputFormatter.dateFormat = "d-MMM-yyyy HH:mm:ss"
            
            if let date = inputFormatter.date(from: dateString) {
                
                let outputFormatter = DateFormatter.init()
                outputFormatter.dateFormat = format
                
                return outputFormatter.string(from: date)
            }
            
            return nil
        }

        var dict = Dictionary<String,Any>()
    
        let offersModel = offerModel?.offers
        let amount = Float(offerModel?.amount ?? "0") ?? 0.0
        let value = Float(offersModel?.value ?? Int(0.0))
        //var valueString = CodeSnippets.getDigitDisplay(digit: amount) + " MMK"
        var pdfAmount = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: offerModel?.amount ?? "0") + " MMK"
        if offersModel?.amountType == "0" || offersModel?.amountType == "3" || offersModel?.amountType == "6"//Fixed=Percentage calculate
        {
            let result = amount - value
            //valueString = CodeSnippets.getDigitDisplay(digit: result) + " MMK"
            pdfAmount = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: String(result)) + " MMK"
        } else if offersModel?.amountType == "1" || offersModel?.amountType == "4"  || offersModel?.amountType == "7" { //Fixed data calculate
            
            let percentageValue = Float((value * amount) / 100)
            let result = amount - percentageValue
            //valueString = CodeSnippets.getDigitDisplay(digit: result) + " MMK"
            pdfAmount = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: String(result)) + " MMK"
        } else {//if offersModel?.amountType == "2" {//Free
            let result = amount - value
            //valueString = CodeSnippets.getDigitDisplay(digit: result) + " MMK"
            pdfAmount = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: String(result)) + " MMK"
        }
        //
        let senderObject = PTManagerClass.decodeMobileNumber(phoneNumber: UserModel.shared.mobileNo)
        let senderFormatted = String.init(format: "(%@) %@", senderObject.country.dialCode, UserModel.shared.formattedNumber)
        dict["senderAccNameOffer"]        = UserModel.shared.name
        dict["senderAccNoOffer"]         = senderFormatted
        
        let dest = self.append09Format(offerModel?.mobileNumber ?? "0095")
        dict["receiverAccNameOffer"] = offerModel?.merchant
        dict["receiverAccNoOffer"] = String.init(format: "(%@) %@", senderObject.country.dialCode, dest)//dest
        dict["amountOffers"] = pdfAmount
        //dict["amountOffers"] = NSMutableAttributedString.init(string: valueString)
        //Display Promo Receiver Number
        if offerModel?.offers?.promotionTransactionCount == 1{
            let destPromo = self.append09Format(recieptModel?.destination ?? "0095")
            dict["promoReceiverNumber"] = String.init(format: "(%@) %@", senderObject.country.dialCode, destPromo)//dest
        }
        dict["OfferObject"] = offerModel
        dict["OfferObjectTransID"] = recieptModel?.transid
        dict["transactionDateOffer"] = formattedDateFromString(dateString: recieptModel?.responsects ?? "", withFormat: "EEE, d-MMM-yyyy HH:mm:ss") ?? ""
        dict["invoiceTitle"]       = "Transaction Receipt".localized
        dict["logoName"]           = "appIcon_Ok"
        
        DispatchQueue.main.async {
            dict["qrImage"] = self.getQRCodeImage()

            guard let pdfUrl = CodeSnippets.createInvoicePDF(dictValues: dict, pdfFile: "OK$ offer Receipt") else {
                println_debug("Error - pdf not generated")
                return
            }
            let story = UIStoryboard.init(name: "Topup", bundle: nil)
            guard let vc = story.instantiateViewController(withIdentifier: "SharePDFViewController") as? SharePDFViewController else { return }
            vc.url = pdfUrl
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func formattedString(dateString: String, withFormat format: String) -> String? {
        
        let inputFormatter = DateFormatter.init()
        inputFormatter.dateFormat = "d-MMM-yyyy HH:mm:ss"
        
        if let date = inputFormatter.date(from: dateString) {
            
            let outputFormatter = DateFormatter.init()
            outputFormatter.dateFormat = format
            
            return outputFormatter.string(from: date)
        }
        
        return nil
    }
    
    //Generate QR Code
    func getQRCodeImage() -> UIImage? {
       
        let senderName = UserModel.shared.name
        let senderNumber = UserModel.shared.mobileNo
        let receiverName = offerModel?.merchant ?? ""
        let receiverBusinessName = offerModel?.businessName ?? ""
        let receiverNumber = recieptModel?.destination ?? ""
        
        let transDateStr = formattedString(dateString: recieptModel?.responsects ?? "", withFormat: "dd/MMM/yyyy HH:mm:ss") ?? ""
       
        let firstPartToEncrypt = "\(transDateStr)----\(receiverNumber)"
        
        let transID = recieptModel?.transid ?? ""

        let trasnstype = "PAYTO"
        
        //Gender Display
        var gender = ""
        if UserModel.shared.gender == "1" {
            gender = "M"
        } else {
            gender = "F"
        }
        
        let age = UserModel.shared.ageNow

        let lattitude  = geoLocManager.currentLatitude ?? ""
        let longitude = geoLocManager.currentLongitude ?? ""
        
        let firstPartEncrypted = AESCrypt.encrypt(firstPartToEncrypt, password: "m2n1shlko@$p##d")
        
        let balance = UserLogin.shared.walletBal
        
        let cashBackAmount = "0.00"
       
        let offersModel = offerModel?.offers
        let amount = Float(offerModel?.amount ?? "0") ?? 0.0
        let value = Float(offersModel?.value ?? Int(0.0))
        var valueString = CodeSnippets.getDigitDisplay(digit: amount)
        
        if offersModel?.amountType == "0" || offersModel?.amountType == "3"//Fixed=Percentage calculate
        {
            let result = amount - value
            valueString = CodeSnippets.getDigitDisplay(digit: result)
        } else if offersModel?.amountType == "1" || offersModel?.amountType == "4" { //Fixed data calculate
            
            let percentageValue = Float((value * amount) / 100)
            let result = amount - percentageValue
            valueString = CodeSnippets.getDigitDisplay(digit: result)
        } else if offersModel?.amountType == "2" {//Free
            let result = amount - value
            valueString = CodeSnippets.getDigitDisplay(digit: result)
        }
        
        let bonus = "0.00"
        
        let state = UserModel.shared.state
        
        let str2 = "OK-\(senderName)-\(valueString)-\(transDateStr)-\(transID)-\(senderNumber)-\(balance)-des-\(receiverNumber)-\(trasnstype)-\(cashBackAmount)-\(bonus)-\(lattitude),\(longitude)-0-\(gender)-\(age)-\(state)-\(receiverName)-\(receiverBusinessName)"
        
        let secondEncrypted = AESCrypt.encrypt(str2, password: firstPartToEncrypt)
        
        var firstPartEncryptedSafeValue = ""
        var secondEncryptedSafeValue = ""
        if let firstEncrypted = firstPartEncrypted {
            firstPartEncryptedSafeValue = firstEncrypted
        }
        if let secondEncrypt = secondEncrypted {
            secondEncryptedSafeValue = secondEncrypt
        }
        let finalEncyptFormation = "\(firstPartEncryptedSafeValue)---\(secondEncryptedSafeValue)"
        
        let qrImageObject = PTQRGenerator()
        guard let qrImage =  qrImageObject.getQRImage(stringQR: finalEncyptFormation, withSize: 10) else { return nil }
        
        return qrImage
    }
    
}

//MARK: - CNContactViewControllerDelegate
extension OffersRecieptViewController: CNContactViewControllerDelegate {
    
    func changeCancelImplementation() {
        let originalSelector = Selector(("editCancel:"))
        let swizzledSelector = #selector(CNContactViewController.cancelHack)
        if let originalMethod = class_getInstanceMethod(object_getClass(CNContactViewController()), originalSelector),
           let swizzledMethod = class_getInstanceMethod(object_getClass(CNContactViewController()), swizzledSelector) {
            method_exchangeImplementations(originalMethod, swizzledMethod)
        }
    }
    
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
        if let _ = contact {
            contactAdded = true
        }
        viewController.delegate = nil
        self.navigationController?.popViewController(animated: true)
    }
}
extension OffersRecieptViewController: PTFavoriteActionDelegate {
    func favoriteAdded() {
        //isInFavArray[currentIndex] = true
        favoriteAddedCheck = true
        addFavUI(isInFav: true)
    }
}
