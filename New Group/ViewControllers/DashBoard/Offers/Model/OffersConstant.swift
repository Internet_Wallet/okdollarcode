//
//  OffersConstant.swift
//  OK
//
//  Created by gauri OK$ on 6/7/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class OffersConstant: NSObject {

    static var offer_type_fixed_discount_min_max_purchase = "Fixed Discount Amount %@ MMK. Purchase Minimum Amount %@ MMK to Maximum Amount %@ MMK."
    static var offer_type_fixed_discount_min_purchase = "Fixed Discount Amount %@ MMK. Purchase Minimum Amount %@ MMK."
    static var offer_type_fixed_discount_fixed_purchase = "Fixed Discount Amount %@ MMK. Purchase Fixed Amount %@ MMK."
    static var offer_type_fixed_discount_unlimited_purchase = "Fixed Discount Amount %@ MMK. Purchase Unlimited Amount."
    static var offer_type_fixed_discount_per_min_max_purchase = "Fixed Discount %@ %@. Purchase Minimum Amount %@ MMK to Maximum Amount %@ MMK."
    static var offer_type_fixed_discount_per_max_purchase = "Fixed Discount %@ %@. Purchase Maximum Amount %@ MMK."
    static var offer_type_fixed_discount_per_min_purchase = "Fixed Discount %@ %@. Purchase Minimum Amount %@ MMK."
    static var offer_type_fixed_discount_per_fixed_purchase = "Fixed Discount %@ %@. Purchase Fixed Amount %@ MMK."
    static var offer_type_fixed_discount_per_unlimited_purchase = "Fixed Discount %@ %@. Purchase Unlimited Amount."
    static var offer_free_min_max_purchase = "Buy %@ Quantity Get %@ Quantity. Purchase Minimum Amount %@ MMK to Maximum Amount %@ MMK."
    static var offer_free_fixed_purchase = "Buy %@ Quantity Get %@ Quantity. Purchase Fixed Amount %@ MMK."
    static var offer_free_fixed_purchase_accumulated_quantity_amount = "Buy %@ Quantity Get %@ Quantity. Purchase Fixed Amount %@ MMK. Automatically equal accumulated Quantity and Amount."
    static var offer_free_min_max_purchase_discont_amount = "Buy %@ Quantity Get %@ Quantity + Fixed Discount Amount %@. Purchase Minimum Amount %@ MMK to Maximum Amount %@ MMK."
    static var offer_free_fixed_purchase_discont_amount = "Buy %@ Quantity Get %@ Quantity + Fixed Discount Amount %@ MMK. Purchase Fixed Amount %@ MMK."
    static var offer_free_fixed_purchase_accumulated_quantity_amount_discont_amount = "Buy %@ Quantity Get %@ Quantity + Fixed Discount Amount %@ MMK. Purchase Fixed Amount %@ MMK. Automatically equal accumulated Quantity and Amount."
    
    static var offer_free_min_max_purchase_discont_per = "Buy %@ Quantity Get %@ Quantity + Fixed Discount %@ %@. Purchase Minimum Amount %@ MMK to Maximum Amount %@ MMK."
    static var offer_free_fixed_purchase_discont_per = "Buy %@ Quantity Get %@ Quantity + Fixed Discount %@ %@. Purchase Fixed Amount %@ MMK."
    static var offer_free_fixed_purchase_accumulated_quantity_amount_discont_per = "Buy %@ Quantity Get %@ Quantity + Fixed Discount %@ %@. Purchase Fixed Amount %@ MMK. Automatically equal accumulated Quantity and Amount."
    
    static var offer_buy_min_max_purchase = "Buy %@ Quantity. Purchase Minimum Amount %@ MMK to Maximum Amount %@ MMK."
    static var offer_buy_fixed_purchase = "Buy %@ Quantity. Purchase Fixed Amount %@ MMK."
    static var offer_buy_fixed_purchase_accumulated_quantity_amount = "Buy %@ Quantity. Purchase Fixed Amount %@ MMK. Automatically equal accumulated Quantity and Amount."
    
    static var offer_buy_min_max_purchase_discont_amount = "Buy %@ Quantity + Fixed Discount Amount %@ MMK. Purchase Minimum Amount %@ MMK to Maximum Amount %@ MMK."
    static var offer_buy_fixed_purchase_discont_amount = "Buy %@ Quantity + Fixed Discount Amount %@ MMK. Purchase Fixed Amount %@ MMK."
    static var offer_buy_fixed_purchase_accumulated_quantity_amount_discont_amount = "Buy %@ Quantity + Fixed Discount Amount %@ MMK. Purchase Fixed Amount %@ MMK. Automatically equal accumulated Quantity and Amount."
    
    static var offer_buy_min_max_purchase_discont_per = "Buy %@ Quantity + Fixed Discount %@ %@. Purchase Minimum Amount %@ MMK to Maximum Amount %@ MMK."
    static var offer_buy_fixed_purchase_discont_per = "Buy %@ Quantity + Fixed Discount %@ %@. Purchase Fixed Amount %@ MMK."
    static var offer_buy_fixed_purchase_accumulated_quantity_amount_discont_per = "Buy %@ Quantity + Fixed Discount %@ %@. Purchase Fixed Amount %@ MMK. Automatically equal accumulated Quantity and Amount."
}
