//
//  OffersModelData.swift
//  OK
//
//  Created by Ashish on 7/13/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

// offers list model
struct OffersCategoryModel: Codable {
    let categoryID, categoryName, categoryDescription, categoryBName , categoryImage: String
    let isActive: Bool
    
    enum CodingKeys: String, CodingKey {
        case categoryID = "categoryId"
        case categoryName, categoryDescription, categoryBName, categoryImage, isActive
    }
}

// offers object model
struct OffersListModel: Codable {
    let categoryID, categoryName, categoryDescription, categoryBName, categoryCName , categoryImage: String?
    let categoryBDescription, mobilenumber, promotionName, promotionDescr: String?
    var promotionCode, promotionStartDate, promotionExpiryDate: String?
    let promotionBImg,promotionTImg,promotionCImg,promotionImg: String?
    let promotionID, transactionTypeName, paymentType, amountType, termsAndCondition, bTermsAndCondition,cTermsAndCondition: String?
    var value: Int?
    let promotionType, predefinedMobileNumebrType, customErrorMessage, isLimit: String?
    let minimumBalnceAmount: Int?
    let allowPayWithoutID: Bool?
    let isPayableLimit: Bool?
    let cashBackFlag: Int?
    let discountPayTo: Float?
    let minimumPayableAmount,maximumPayableAmount: Int?
    let companyId,companyName,companyBName,companyCName: String?
    let productId,productName,productBName,productCName,productNameInEngList,productNameInBurZawgyiList,productNameInBurUnicodeList,productNameInChineseList,productNameInThaiList: String?
    var productValue,buyingQty,freeQty,productValidationRuleValue: Int?
    let promotionConditionalValue: Int?
    let promotionTransactionCount: Int?
    let freeProductName: String?
    
    enum CodingKeys: String, CodingKey {
        case categoryID = "categoryId"
        case termsAndCondition = "termsAndCondition"
        case bTermsAndCondition = "bTermsAndCondition"
        case cTermsAndCondition = "cTermsAndCondition"
        case categoryName, categoryDescription, categoryBName,categoryCName,categoryImage, categoryBDescription, mobilenumber, promotionName, promotionDescr, promotionCode, promotionStartDate, promotionExpiryDate, promotionImg,promotionBImg,promotionTImg,promotionCImg
        case promotionID = "promotionId"
        case transactionTypeName, paymentType, amountType, value, promotionType, predefinedMobileNumebrType, customErrorMessage, isLimit, minimumBalnceAmount,minimumPayableAmount,maximumPayableAmount
        case allowPayWithoutID = "allowPayWithoutId"
        
        case discountPayTo, cashBackFlag, isPayableLimit
        case companyId,companyName,companyBName,companyCName
        case productId,productName,productBName,productCName,productNameInEngList,productNameInBurZawgyiList,productNameInBurUnicodeList,productNameInChineseList,productNameInThaiList
        case productValue,buyingQty,freeQty,productValidationRuleValue
        case promotionConditionalValue
        case promotionTransactionCount
        case freeProductName
    }
}

// offers list model
struct OffersDetailResponse: Codable {
  let code: Int
  let data: String?
  let message: String?
  
  enum CodingKeys: String, CodingKey {
    case code = "Code"
    case data = "Data"
    case message = "Msg"
  }
}


//MARK:- Offers Confirmation Model

struct OfferConfirmationScreen {
    var mobileNumber : String?
    var category     : String?
    var merchant     : String?
    var businessName : String?
    var amount       : String?
    var remarks      : String?
    var accountType  : String?
    var offers       : OffersListModel?
    
}

//MARK:- Reciept Model
struct OfferRecieptModel : Codable {
    
    let agentcode : String?
    let agentname : String?
    let amount : String?
    let clientip : String?
    let clientos : String?
    let clienttype : String?
    let comments : String?
    let destination : String?
    let destlevel : String?
    let fee : String?
    let greeting : String?
    let iskickbackmsisdnregister : String?
    let isregisteragent : String?
    let kickbackenable : String?
    let kickvalue : String?
    let kickwallet : String?
    let loyaltypoints : String?
    let maxwalletvalue : String?
    let merchantname : String?
    let merchantnamesms : String?
    let minwalletvalue : String?
    let operators : String?
    let oprwallet : String?
    let prekickwallet : String?
    let preoprwallet : String?
    let prewalletbalance : String?
    let referredlang : String?
    let requestcts : String?
    let responsects : String?
    let responsevalue : String?
    let resultcode : String?
    let resultdescription : String?
    let rtranstype : String?
    let securetoken : String?
    let source : String?
    let transid : String?
    let unregipostbalance : String?
    let unregiprebalance : String?
    let vendorcode : String?
    let walletalertvalue : String?
    let walletbalance : String?
    
    
    enum CodingKeys: String, CodingKey {
        case agentcode = "agentcode"
        case agentname = "agentname"
        case amount = "amount"
        case clientip = "clientip"
        case clientos = "clientos"
        case clienttype = "clienttype"
        case comments = "comments"
        case destination = "destination"
        case destlevel = "destlevel"
        case fee = "fee"
        case greeting = "greeting"
        case iskickbackmsisdnregister = "iskickbackmsisdnregister"
        case isregisteragent = "isregisteragent"
        case kickbackenable = "kickbackenable"
        case kickvalue = "kickvalue"
        case kickwallet = "kickwallet"
        case loyaltypoints = "loyaltypoints"
        case maxwalletvalue = "maxwalletvalue"
        case merchantname = "merchantname"
        case merchantnamesms = "merchantnamesms"
        case minwalletvalue = "minwalletvalue"
        case operators = "operator"
        case oprwallet = "oprwallet"
        case prekickwallet = "prekickwallet"
        case preoprwallet = "preoprwallet"
        case prewalletbalance = "prewalletbalance"
        case referredlang = "referredlang"
        case requestcts = "requestcts"
        case responsects = "responsects"
        case responsevalue = "responsevalue"
        case resultcode = "resultcode"
        case resultdescription = "resultdescription"
        case rtranstype = "rtranstype"
        case securetoken = "securetoken"
        case source = "source"
        case transid = "transid"
        case unregipostbalance = "unregipostbalance"
        case unregiprebalance = "unregiprebalance"
        case vendorcode = "vendorcode"
        case walletalertvalue = "walletalertvalue"
        case walletbalance = "walletbalance"
    }
    
}


//MARK: Offer Request Model
struct OfferOneTRequest: Codable {
    let destinationNumberWalletBalance: String? = ""
    let transactionsCellTower: OfferRequestTransactionsCellTower?
    let lExternalReference: OfferRequestLExternalReference?
    let lGeoLocation: OfferRequestLGeoLocation?
    let lProximity: OfferRequestLProximity?
    let lTransactions: OfferRequestOneTransactions?
    
    enum CodingKeys: String, CodingKey {
        case destinationNumberWalletBalance = "DestinationNumberWalletBalance"
        case transactionsCellTower = "TransactionsCellTower"
        case lExternalReference, lGeoLocation, lProximity, lTransactions
    }
}

struct OfferRequest: Codable {
    let destinationNumberWalletBalance: String? = ""
    let transactionsCellTower: OfferRequestTransactionsCellTower?
    let lExternalReference: OfferRequestLExternalReference?
    let lGeoLocation: OfferRequestLGeoLocation?
    let lProximity: OfferRequestLProximity?
    let lTransactions: OfferRequestLTransactions?
    
    enum CodingKeys: String, CodingKey {
        case destinationNumberWalletBalance = "DestinationNumberWalletBalance"
        case transactionsCellTower = "TransactionsCellTower"
        case lExternalReference, lGeoLocation, lProximity, lTransactions
    }
}

struct OfferRequestLExternalReference: Codable {
    let direction: Bool?
    let endStation, startStation, vendorId: String?
    
    enum CodingKeys: String, CodingKey {
        case direction = "Direction"
        case endStation = "EndStation"
        case startStation = "StartStation"
        case vendorId = "VendorID"
    }
}

struct OfferRequestLGeoLocation: Codable {
    let cellId, latitude, longitude: String?
    
    enum CodingKeys: String, CodingKey {
        case cellId = "CellID"
        case latitude = "Latitude"
        case longitude = "Longitude"
    }
}

struct OfferRequestLProximity: Codable {
    let blueToothUsers: [String]?
    let selectionMode: Bool?
    let wifiUsers: [String]?
    let mBltDevice, mWifiDevice: String?
    
    enum CodingKeys: String, CodingKey {
        case blueToothUsers = "BlueToothUsers"
        case selectionMode = "SelectionMode"
        case wifiUsers = "WifiUsers"
        case mBltDevice, mWifiDevice
    }
}


struct OfferRequestOneTransactions: Codable {
    let amount, balance: String?
    let bonusPoint: Int?
    let businessName: String?
    let cashBackFlag: Int?
    let comments, destination, destinationNumberWalletBalance: String?
    let discountPayTo: Int?
    let isMectelTopUp: Bool?
    let isPromotionApplicable, kickBack, kickBackMsisdn, localTransactionType: String?
    let merchantName, mobileNumber: String?
    let mode: Bool?
    let password, productCode, promoCodeId: String?
    let resultCode: Int?
    let resultDescription, secureToken, transactionId, transactionTime: String?
    let transactionType, accounType, senderBusinessName, userName: String?
    let promoActualAmount: String?
    let promotionAgentMobileNumber: String?
    let promotionTransactionCount: Int?
    
    enum CodingKeys: String, CodingKey {
        case promotionAgentMobileNumber = "PromotionAgentMobileNumber"
        case amount = "Amount"
        case balance = "Balance"
        case bonusPoint = "BonusPoint"
        case businessName = "BusinessName"
        case cashBackFlag = "CashBackFlag"
        case comments = "Comments"
        case destination = "Destination"
        case destinationNumberWalletBalance = "DestinationNumberWalletBalance"
        case discountPayTo = "DiscountPayTo"
        case isMectelTopUp = "IsMectelTopUp"
        case isPromotionApplicable = "IsPromotionApplicable"
        case kickBack = "KickBack"
        case kickBackMsisdn = "KickBackMsisdn"
        case localTransactionType = "LocalTransactionType"
        case merchantName = "MerchantName"
        case mobileNumber = "MobileNumber"
        case mode = "Mode"
        case password = "Password"
        case productCode = "ProductCode"
        case promoCodeId = "PromoCodeId"
        case resultCode = "ResultCode"
        case resultDescription = "ResultDescription"
        case secureToken = "SecureToken"
        case transactionId = "TransactionID"
        case transactionTime = "TransactionTime"
        case transactionType = "TransactionType"
        case accounType, senderBusinessName, userName
        case promoActualAmount = "PromoActualAmount"
        case promotionTransactionCount = "PromotionTransactionCount"
    }
}

struct OfferRequestLTransactions: Codable {
    let amount, balance: String?
    let bonusPoint: Int?
    let businessName: String?
    let cashBackFlag: Int?
    let comments, destination, destinationNumberWalletBalance: String?
    let discountPayTo: Int?
    let isMectelTopUp: Bool?
    let isPromotionApplicable, kickBack, kickBackMsisdn, localTransactionType: String?
    let merchantName, mobileNumber: String?
    let mode: Bool?
    let password, productCode, promoCodeId: String?
    let resultCode: Int?
    let resultDescription, secureToken, transactionId, transactionTime: String?
    let transactionType, accounType, senderBusinessName, userName: String?
    let promoActualAmount: String?
    let promotionAgentMobileNumber,DestinationNumberAccountType: String?

    enum CodingKeys: String, CodingKey {
        case promotionAgentMobileNumber = "PromotionAgentMobileNumber"
        case amount = "Amount"
        case balance = "Balance"
        case bonusPoint = "BonusPoint"
        case businessName = "BusinessName"
        case cashBackFlag = "CashBackFlag"
        case comments = "Comments"
        case destination = "Destination"
        case destinationNumberWalletBalance = "DestinationNumberWalletBalance"
        case discountPayTo = "DiscountPayTo"
        case isMectelTopUp = "IsMectelTopUp"
        case isPromotionApplicable = "IsPromotionApplicable"
        case kickBack = "KickBack"
        case kickBackMsisdn = "KickBackMsisdn"
        case localTransactionType = "LocalTransactionType"
        case merchantName = "MerchantName"
        case mobileNumber = "MobileNumber"
        case mode = "Mode"
        case password = "Password"
        case productCode = "ProductCode"
        case promoCodeId = "PromoCodeId"
        case resultCode = "ResultCode"
        case resultDescription = "ResultDescription"
        case secureToken = "SecureToken"
        case transactionId = "TransactionID"
        case transactionTime = "TransactionTime"
        case transactionType = "TransactionType"
        case accounType, senderBusinessName, userName,DestinationNumberAccountType
        case promoActualAmount = "PromoActualAmount"
    }
}

struct OfferRequestTransactionsCellTower: Codable {
    let lac, mac, mcc, mnc: String?
    let signalStrength, ssid, connectedwifiName, networkType, networkSpeed: String?
    
    enum CodingKeys: String, CodingKey {
        case lac = "Lac"
        case mac = "Mac"
        case mcc = "Mcc"
        case mnc = "Mnc"
        case signalStrength = "SignalStrength"
        case ssid = "Ssid"
        case connectedwifiName = "ConnectedwifiName"
        case networkType = "NetworkType"
        case networkSpeed = "NetworkSpeed"
    }
}

class OfferCommentHelper {
    var commentByUser: String?
    var promoCode: String?
    var discountPercent: String?
    var discountAmount: String?
    var billAmount: String?
    var companyName: String?
    var productName: String?
    var buyingQuantity: String?
    var freeQuantity: String?
    var netAmount: String?
    var amountType: String?
    var promoReceiverNumber: String?
    var senderCreditNumber: String?
    var promotionTransactionCount: Int?//One tx = 1 or Two Tx = 2
    var paymentType: String? //1 = indirect payment or 0 = direct
    var freeProductName: String?
    
    init(comment: String? = nil) {
        self.loadData(from: comment)
    }
    
    func loadData(from comment: String?) {
        guard let _ = comment else { return }
        
        var cArray = comment!.components(separatedBy: "[")
        self.commentByUser = cArray[0]
        if cArray.count > 1 {
            cArray = cArray[1].components(separatedBy: ",")
            self.promoCode = cArray.count > 1 ? cArray[1] : nil
            self.amountType = cArray.count > 2 ? cArray[2] : nil
            self.promotionTransactionCount = cArray.count > 12 ? Int(cArray[12]) : nil
            self.promoReceiverNumber = cArray.count > 14 ? cArray[14].replacingOccurrences(of: "0095", with: "(+95)0") : nil
            self.senderCreditNumber = cArray.count > 13 ? cArray[13].replacingOccurrences(of: "0095", with: "(+95)0") : nil
            self.paymentType = cArray.count > 3 ? cArray[3] : nil
            let otherDetails = self.getCompProdQuan(comArray: cArray)
            self.companyName = otherDetails.companyName
            self.productName = otherDetails.productName
            self.buyingQuantity = wrapAmountWithCommaDecimal(key: "\(otherDetails.buyingQuantity ?? "")")
            self.freeQuantity = wrapAmountWithCommaDecimal(key: "\(otherDetails.freeQuantity ?? "")")
            self.discountPercent = self.getPromoDiscountPercent(from: cArray)
            self.discountAmount = self.getPromoDiscountAmount(from: cArray)
            if cArray.count > 5 {
                self.billAmount = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: cArray[5])
            }
            if var bAmntStr = self.billAmount, var dAmntStr = self.discountAmount {
                bAmntStr = bAmntStr.replacingOccurrences(of: ",", with: "")
                dAmntStr = dAmntStr.replacingOccurrences(of: ",", with: "")
                if let bAmnt = Double(bAmntStr), let dAmnt = Double(dAmntStr) {
                    self.netAmount = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: String(bAmnt - dAmnt))
                }
            }
        }
    }
    
    private func getCompProdQuan(comArray: [String]) -> (companyName: String?, productName: String?, buyingQuantity: String?, freeQuantity: String?) {
        if comArray.count > 11 {
            if comArray[2] == "2" || comArray[2] == "3" || comArray[2] == "4" {
                
                return (comArray[7], comArray[11], comArray[8], comArray[9])
            }
        }
        return (nil, nil, nil, nil)
    }
    
    private func getPromoDiscountPercent(from arrayString: [String]) -> String? {
        if arrayString.count > 5 {
            if arrayString[2] == "0" || arrayString[2] == "3" {
                let amount = Float(arrayString[5]) ?? 0.0
                let percentageAmount = Float(arrayString[4]) ?? 0.0
                let percentageValue = Float((percentageAmount * 100) / amount)
                let displayPercentage = String.init(format: "%.2f", percentageValue)
                return displayPercentage
            } else if arrayString[2] == "1" || arrayString[2] == "4"{
                let value = Float(arrayString[4]) ?? 0.0
                let displayPercentage = String.init(format: "%.2f", value)
                return displayPercentage
            }
        }
        return nil
    }
    
    private func getPromoDiscountAmount(from arrayString: [String]) -> String? {
        if arrayString.count > 5 {
            if arrayString[2] == "0" || arrayString[2] == "3" {
                //let value = Float(arrayString[4]) ?? 0.0
                let displayAmount = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: arrayString[4]) //String.init(format: "%.2f", value)
                return displayAmount
            } else if arrayString[2] == "1" || arrayString[2] == "4"{
                let amount = Float(arrayString[5]) ?? 0.0
                let percentageAmount = Float(arrayString[4]) ?? 0.0
                let percentageValue = Float((percentageAmount * amount) / 100)
                let displayAmount = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(percentageValue)")//String.init(format: "%.2f", percentageValue)
                return displayAmount
            }
        }
         return nil
    }
}
