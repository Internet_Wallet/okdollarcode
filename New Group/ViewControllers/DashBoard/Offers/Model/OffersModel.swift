//
//  OffersModel.swift
//  OK
//
//  Created by SHUBH on 7/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class OffersModel: NSObject {

    var firstName:String? = ""

    var categoryId:String? = ""

    var categoryName:String? = ""

    var categoryDescription:String? = ""

    var mobilenumber:String? = ""

    var promotionName:String? = ""

    var promotionCode:String? = ""

    var promotionStartDate:String? = ""

    var promotionExpiryDate:String? = ""

    var promotionDescr:String? = ""

    var promotionImg:String? = ""

    var promotionId:String? = ""
    
    var categoryBName:String? = ""
    
    var categoryBDescription:String? = ""
    
    var promotionBName:String? = ""
    
    var promotionBDescription:String? = ""
    
    var promotionCName:String? = ""
    
    var promotionCDescription:String? = ""
    
    var transactionTypeName:String? = ""
    
    var paymentType:String? = ""
    
    var categoryCName:String? = ""
    
    var categoryCDescription:String? = ""
    
    var amountType:String? = ""
    
    var value:String? = ""
    
    var promotionType:String? = ""
    
    var predefinedMobileNumebrType:String? = ""
    
    var customErrorMessage:String? = ""
    
    var isLimit:String? = ""
    
    var minimumBalnceAmount:String? = ""
    
    var paytoEnable:Bool? = false
    
    var agentMobileNumber:String? = ""
    
    var latitude:String? = ""
    
    var longitude:String? = ""
    
    var discountPayTo:String? = ""
    
    var CashBackFlag:String? = ""
    
    var termsConditionB:String? = ""
    
    var termsCondition:String? = ""
    
    init(dictionary: Dictionary<String, Any>) {
        super.init()
        if let title = dictionary["categoryId"] as? String {
            self.categoryId = title
        }
        if let title = dictionary["categoryName"] as? String {
            self.categoryName = title
        }
        if let title = dictionary["categoryDescription"] as? String {
            self.categoryDescription = title
        }
        if let title = dictionary["mobilenumber"] as? String {
            self.mobilenumber = title
        }
        if let title = dictionary["promotionName"] as? String {
            self.promotionName = title
        }
        if let title = dictionary["promotionCode"] as? String {
            self.promotionCode = title
        }
        if let title = dictionary["promotionStartDate"] as? String {
            self.promotionStartDate = title
        }
        if let title = dictionary["promotionExpiryDate"] as? String {
            let dateFormatter = DateFormatter()
            dateFormatter.calendar = Calendar(identifier: .gregorian)
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            //dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            let time = dateFormatter.date(from: title)//timeStr
            dateFormatter.dateFormat = "d-MMM-yyyy"
            self.promotionExpiryDate = dateFormatter.string(from: time!)
            
        }
        if let title = dictionary["promotionDescr"] as? String {
            self.promotionDescr = title
        }
        if let title = dictionary["promotionImg"] as? String {
            self.promotionImg = title
        }
        if let title = dictionary["promotionId"] as? String {
            self.promotionId = title
        }
        if let title = dictionary["categoryBName"] as? String {
            self.categoryBName = title
        }
        if let title = dictionary["categoryBDescription"] as? String {
            self.categoryBDescription = title
        }
        if let title = dictionary["promotionBName"] as? String {
            self.promotionBName = title
        }
        if let title = dictionary["promotionBDescription"] as? String {
            self.promotionBDescription = title
        }
        if let title = dictionary["promotionCName"] as? String {
            self.promotionCName = title
        }
        if let title = dictionary["promotionCDescription"] as? String {
            self.promotionCDescription = title
        }
        if let title = dictionary["transactionTypeName"] as? String {
            self.transactionTypeName = title
        }
        if let title = dictionary["paymentType"] as? String {
            self.paymentType = title
        }
        if let title = dictionary["categoryCName"] as? String {
            self.categoryCName = title
        }
        if let title = dictionary["categoryCDescription"] as? String {
            self.categoryCDescription = title
        }
        if let title = dictionary["amountType"] as? String {
            self.amountType = title
        }
        if let title = dictionary["value"] as? String {
            self.value = title
        }
        if let title = dictionary["promotionType"] as? String {
            self.promotionType = title
        }
        if let title = dictionary["predefinedMobileNumebrType"] as? String {
            self.predefinedMobileNumebrType = title
        }
        if let title = dictionary["customErrorMessage"] as? String {
            self.customErrorMessage = title
        }
        if let title = dictionary["isLimit"] as? String {
            self.isLimit = title
        }
        if let title = dictionary["minimumBalnceAmount"] as? String {
            self.minimumBalnceAmount = title
        }
        if let title = dictionary["allowPayWithoutId"] as? Bool {
            self.paytoEnable = title
        }
        if let title = dictionary["agentMobilenumber"] as? String {
            self.agentMobileNumber = title
        }
        if let title = dictionary["latitude"] as? String {
            self.latitude = title
        }
        if let title = dictionary["longitude"] as? String {
            self.longitude = title
        }
        if let title = dictionary["discountPayTo"] as? String {
            self.discountPayTo = title
        }
        if let title = dictionary["cashBackFlag"] as? String {
            self.CashBackFlag = title
        }
        if let title = dictionary["bTermsAndCondition"] as? String {
            self.termsConditionB = title
        }
        if let title = dictionary["termsAndCondition"] as? String {
            self.termsCondition = title
        }
    }
    
}
