//
//  OffersConstantBurmish.swift
//  OK
//
//  Created by gauri OK$ on 6/7/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class OffersConstantBurmish: NSObject {

    static var offer_type_fixed_discount_min_max_purchase = " ေစ်းေလွ်ာ့ေငြပမာဏ %@ က်ပ္ ၊ ဝယ္ယူရမည့္ အနည္းဆံုးေငြပမာဏ %@ က်ပ္ မွ အမ်ားဆံုးေငြပမာဏ %@ က်ပ္ ထိ၊"
    static var offer_type_fixed_discount_min_purchase = " ေစ်းေလွ်ာ့ေငြပမာဏ %@ က်ပ္ ၊ ဝယ္ယူရမည့္ အနည္းဆံုးေငြပမာဏ %@ က်ပ္ ၊"
    static var offer_type_fixed_discount_fixed_purchase = " ေစ်းေလွ်ာ့ေငြပမာဏ %@ က်ပ္ ၊ ဝယ္ယူရမည့္ ေငြပမာဏ %@ က်ပ္ ၊"
    static var offer_type_fixed_discount_unlimited_purchase = "ဝယ္ယူမည့္ ေငြပမာဏအေပၚ ေစ်းေလွ်ာ့ေငြပမာဏ %@"
    
    static var offer_type_fixed_discount_per_min_max_purchase = " ေစ်းေလွ်ာ့ေငြ %@ ရာခိုင္ႏႈန္း ၊ ဝယ္ယူရမည့္ အနည္းဆံုးေငြပမာဏ %@ က်ပ္ မွ အမ်ားဆံုးေငြပမာဏ %@ က်ပ္ အထိ၊"
    
    static var offer_type_fixed_discount_per_max_purchase = " ေစ်းေလွ်ာ့ေငြ %@ ရာခိုင္ႏႈန္း ၊ ဝယ္ယူရမည့္ အမ်ားဆံုးေငြပမာဏ %@ က်ပ္ ၊"
    
    static var offer_type_fixed_discount_per_min_purchase = " ေစ်းေလွ်ာ့ေငြ %@ ရာခိုင္ႏႈန္း ၊ ဝယ္ယူရမည့္ အနည္းဆံုးေငြပမာဏ %@ က်ပ္ ၊"
    
    static var offer_type_fixed_discount_per_fixed_purchase = " ေစ်းေလွ်ာ့ေငြ %@ ရာခိုင္ႏႈန္း ၊ ဝယ္ယူရမည့္ ေငြပမာဏ %@ က်ပ္ ၊"
    
    static var offer_type_fixed_discount_per_unlimited_purchase = "ဝယ္ယူမည့္ ေငြပမာဏအေပၚေစ်းေလွ်ာ့ေငြ %@ ရာခိုင္ႏႈန္း"
    
    static var offer_free_min_max_purchase = "%@ အေရအတြက္ဝယ္လွ်င္ လက္ေဆာင္အေရအတြက္ %@ ရရွိမည္၊ ဝယ္ယူရမည့္ အနည္းဆံုးေငြပမာဏ %@ က်ပ္ မွ အမ်ားဆံုးေငြပမာဏ %@ က်ပ္ အထိ၊ "
    
    static var offer_free_fixed_purchase = "%@ အေရအတြက္ဝယ္လွ်င္ လက္ေဆာင္အေရအတြက္ %@ ရရွိမည္၊ ဝယ္ယူရမည့္ ေငြပမာဏ %@ က်ပ္"
    
    static var offer_free_fixed_purchase_accumulated_quantity_amount = "%@ အေရအတြက္ဝယ္လွ်င္ လက္ေဆာင္အေရအတြက္ %@ ရရွိမည္၊ ဝယ္ယူရမည့္ ေငြပမာဏ %@ က်ပ္ ၊ အလိုအေလွ်ာက္ဆတူ အေရအတြက္ႏွင့္ ေငြပမာဏကိုတိုးသည္။ "
    
    static var offer_free_min_max_purchase_discont_amount = "%@ အေရအတြက္ဝယ္လွ်င္ လက္ေဆာင္အေရအတြက္ %@ ရရွိမည္ + ေစ်းေလွ်ာ့ေငြပမာဏ %@ က်ပ္၊ ဝယ္ယူရမည့္ အနည္းဆံုးေငြပမာဏ %@ က်ပ္ မွ အမ်ားဆံုးေငြပမာဏ %@ က်ပ္ အထိ၊ "
    
    static var offer_free_fixed_purchase_discont_amount = "%@ အေရအတြက္ဝယ္လွ်င္ လက္ေဆာင္အေရအတြက္ %@ ရရွိမည္ + ေစ်းေလွ်ာ့ေငြပမာဏ %@ က်ပ္ ၊ ဝယ္ယူရမည့္ ေငြပမာဏ %@ က်ပ္ ၊ "
    
    static var offer_free_fixed_purchase_accumulated_quantity_amount_discont_amount = "%@ အေရအတြက္ဝယ္လွ်င္ လက္ေဆာင္အေရအတြက္ %@ ရရွိမည္ + ေစ်းေလွ်ာ့ေငြပမာဏ %@ က်ပ္ ၊ ဝယ္ယူရမည့္ ေငြပမာဏ %@ က်ပ္ ၊ အလိုအေလွ်ာက္ဆတူ အေရအတြက္ ႏွင့္ ေငြပမာဏကိုတိုးသည္။ "
    
    static var offer_free_min_max_purchase_discont_per = "%@ အေရအတြက္ဝယ္လွ်င္ လက္ေဆာင္အေရအတြက္ %@ ရရွိမည္ + ေစ်းေလွ်ာ့ေငြ %@ ရာခိုင္နႈန္း ၊ ဝယ္ယူရမည့္အနည္းဆံုးေငြပမာဏ %@ က်ပ္ မွ အမ်ားဆံုးေငြပမာဏ %@ က်ပ္ အထိ၊ "
    
    static var offer_free_fixed_purchase_discont_per = "%@ အေရအတြက္ဝယ္လွ်င္ လက္ေဆာင္အေရအတြက္ %@ ရရွိမည္ + ေစ်းေလွ်ာ့ေငြ %@ ရာခိုင္နႈန္း ၊ ဝယ္ယူရမည့္ ေငြပမာဏ %@ က်ပ္ ၊ "
    
    static var offer_free_fixed_purchase_accumulated_quantity_amount_discont_per = "%@ အေရအတြက္ဝယ္လွ်င္ လက္ေဆာင္အေရအတြက္ %@ ရရွိမည္ + ေစ်းေလွ်ာ့ေငြ %@ ရာခိုင္နႈန္း ၊ ဝယ္ယူရမည့္ ေငြပမာဏ %@ က်ပ္ ၊ အလိုအေလွ်ာက္ဆတူ အေရအတြက္ႏွင့္ ေငြပမာဏကိုတိုးသည္။ "
    
    static var offer_buy_min_max_purchase = "ဝယ္ယူမည့္ အေရအတြက္ %@ ၊ ဝယ္ယူရမည့္ အနည္းဆံုးေငြပမာဏ %@ က်ပ္ မွ အမ်ားဆံုးေငြပမာဏ %@ က်ပ္ အထိ၊ "
    
    static var offer_buy_fixed_purchase = "ဝယ္ယူမည့္ အေရအတြက္ %@ ၊ ဝယ္ယူရမည့္ ေငြပမာဏ %@ က်ပ္ "
    
    static var offer_buy_fixed_purchase_accumulated_quantity_amount = "ဝယ္ယူမည့္ အေရအတြက္ %@ ဝယ္ယူရမည့္ ေငြပမာဏ %@ က်ပ္ ၊ အလိုအေလွ်ာက္ဆတူ အေရအတြက္ႏွင့္ ေငြပမာဏကိုတိုးသည္။ "
    
    static var offer_buy_min_max_purchase_discont_amount = "ဝယ္ယူမည့္ အေရအတြက္ %@ + ေစ်းေလွ်ာ့ေငြပမာဏ %@ က်ပ္ ၊ ဝယ္ယူရမည့္ အနည္းဆံုးေငြပမာဏ %@ က်ပ္ မွ အမ်ားဆံုးေငြပမာဏ %@ က်ပ္ အထိ၊ "
    
    static var offer_buy_fixed_purchase_discont_amount = "ဝယ္ယူမည့္ အေရအတြက္ %@ + ေစ်းေလွ်ာ့ေငြပမာဏ %@ က်ပ္ ၊ ဝယ္ယူရမည့္ ေငြပမာဏ %@ က်ပ္ ၊ "
    
    static var offer_buy_fixed_purchase_accumulated_quantity_amount_discont_amount = "ဝယ္ယူမည့္ အေရအတြက္ %@ + ေစ်းေလွ်ာ့ေငြပမာဏ %@ က်ပ္ ၊ ဝယ္ယူရမည့္ ေငြပမာဏ %@ က်ပ္ ၊ အလိုအေလွ်ာက္ဆတူ အေရအတြက္ႏွင့္ ေငြပမာဏကိုတိုးသည္။ "
    
    static var offer_buy_min_max_purchase_discont_per = "ဝယ္ယူမည့္ အေရအတြက္ %@ + ေစ်းေလွ်ာ့ေငြ %@ ရာခိုင္ႏႈန္း ၊ ဝယ္ယူရမည့္ အနည္းဆံုးေငြပမာဏ %@ က်ပ္ မွ အမ်ားဆံုးေငြပမာဏ %@ က်ပ္ အထိ၊ "
    
    static var offer_buy_fixed_purchase_discont_per = "ဝယ္ယူမည့္ အေရအတြက္ %@ + ေစ်းေလွ်ာ့ေငြ %@ ရာခိုင္ႏႈန္း ၊ ဝယ္ယူရမည့္ေငြပမာဏ %@ က်ပ္ "
    
    static var offer_buy_fixed_purchase_accumulated_quantity_amount_discont_per = "ဝယ္ယူမည့္ အေရအတြက္ %@ + ေစ်းေလွ်ာ့ေငြ %@ ရာခိုင္ႏႈန္း ၊ ဝယ္ယူရမည့္ေငြပမာဏ %@ က်ပ္ ၊ အလိုအေလွ်ာက္ဆတူ အေရအတြက္ႏွင့္ ေငြပမာဏကိုတိုးသည္။ "
}
