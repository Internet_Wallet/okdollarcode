//
//  OffersCategoryModel.swift
//  OK
//
//  Created by SHUBH on 7/7/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class OffersCategoryModel: NSObject {

    var categoryId:String? = ""
    var categoryName:String? = ""
    var categoryDescription:String? = ""
    var categoryBName:String? = ""
    var isActive:Bool? = false

    init(dictionary: Dictionary<String, Any>) {
        super.init()
        if let title = dictionary["categoryId"] as? String {
            self.categoryId = title
        }
        if let title = dictionary["categoryName"] as? String {
            self.categoryName = title
        }
        if let title = dictionary["categoryDescription"] as? String {
            self.categoryDescription = title
        }
        if let title = dictionary["categoryBName"] as? String {
            self.categoryBName = title
        }
        if let title = dictionary["isActive"] as? Bool {
            self.isActive = title
        }
    }
    
}
