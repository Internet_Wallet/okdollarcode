//
//  OffersConstantUnicode.swift
//  OK
//
//  Created by OK$ on 10/23/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class OffersConstantUnicode: NSObject {
    
    static var offer_type_fixed_discount_min_max_purchase = "ဈေးလျှော့ငွေပမာဏ %@ ကျပ် ၊ ဝယ်ယူရမည့် အနည်းဆုံးငွေပမာဏ %@ ကျပ် မှ အများဆုံးငွေပမာဏ %@ ကျပ် ထိ၊"
    static var offer_type_fixed_discount_min_purchase = "ဈေးလျှော့ငွေပမာဏ %@ ကျပ် ၊ ဝယ်ယူရမည့် အနည်းဆုံးငွေပမာဏ %@ ကျပ် ၊"
    static var offer_type_fixed_discount_fixed_purchase = "ဈေးလျှော့ငွေပမာဏ %@ ကျပ် ၊ ဝယ်ယူရမည့် ငွေပမာဏ %@ ကျပ် ၊"
    static var offer_type_fixed_discount_unlimited_purchase = "ဝယ္ယူမည့္ ေငြပမာဏအေပၚ ေစ်းေလွ်ာ့ေငြပမာဏ %@"
    
    static var offer_type_fixed_discount_per_min_max_purchase = "ဈေးလျှော့ငွေ %@ ရာခိုင်နှုန်း ၊ ဝယ်ယူရမည့် အနည်းဆုံးငွေပမာဏ %@ ကျပ် မှ အများဆုံးငွေပမာဏ %@ ကျပ် အထိ၊"
    
    static var offer_type_fixed_discount_per_max_purchase = "ဈေးလျှော့ငွေ %@ ရာခိုင်နှုန်း ၊ ဝယ်ယူရမည့် အများဆုံးငွေပမာဏ %@ ကျပ် ၊"
    
    static var offer_type_fixed_discount_per_min_purchase = "ဈေးလျှော့ငွေ %@ ရာခိုင်နှုန်း ၊ ဝယ်ယူရမည့် အနည်းဆုံးငွေပမာဏ %@ ကျပ် ၊"
    
    static var offer_type_fixed_discount_per_fixed_purchase = "ဈေးလျှော့ငွေ %@ ရာခိုင်နှုန်း ၊ ဝယ်ယူရမည့် ငွေပမာဏ %@ ကျပ် ၊"
    
    static var offer_type_fixed_discount_per_unlimited_purchase = "ဝယ်ယူမည့် ငွေပမာဏအပေါ်ဈေးလျှော့ငွေ %@ ရာခိုင်နှုန်း"
    
    static var offer_free_min_max_purchase = "%@ အရေအတွက်ဝယ်လျှင် လက်ဆောင်အရေအတွက် %@ ရရှိမည်၊ ဝယ်ယူရမည့် အနည်းဆုံးငွေပမာဏ %@ ကျပ် မှ အများဆုံးငွေပမာဏ %@ ကျပ် အထိ၊"
    
    static var offer_free_fixed_purchase = "%@ အရေအတွက်ဝယ်လျှင် လက်ဆောင်အရေအတွက် %@ ရရှိမည်၊ ဝယ်ယူရမည့် ငွေပမာဏ %@ ကျပ်"
    
    static var offer_free_fixed_purchase_accumulated_quantity_amount = "%@ အရေအတွက်ဝယ်လျှင် လက်ဆောင်အရေအတွက် %@ ရရှိမည်၊ ဝယ်ယူရမည့် ငွေပမာဏ %@ ကျပ် ၊ အလိုအလျှောက်ဆတူ အရေအတွက်နှင့် ငွေပမာဏကိုတိုးသည်။"
    
    static var offer_free_min_max_purchase_discont_amount = "%@ အရေအတွက်ဝယ်လျှင် လက်ဆောင်အရေအတွက် %@ ရရှိမည် + ဈေးလျှော့ငွေပမာဏ %@ ကျပ်၊ ဝယ်ယူရမည့် အနည်းဆုံးငွေပမာဏ %@ ကျပ် မှ အများဆုံးငွေပမာဏ %@ ကျပ် အထိ၊"
    
    static var offer_free_fixed_purchase_discont_amount = "%@ အရေအတွက်ဝယ်လျှင် လက်ဆောင်အရေအတွက် %@ ရရှိမည် + ဈေးလျှော့ငွေပမာဏ %@ ကျပ် ၊ ဝယ်ယူရမည့် ငွေပမာဏ %@ ကျပ် ၊"
    
    static var offer_free_fixed_purchase_accumulated_quantity_amount_discont_amount = "%@ အရေအတွက်ဝယ်လျှင် လက်ဆောင်အရေအတွက် %@ ရရှိမည် + ဈေးလျှော့ငွေပမာဏ %@ ကျပ် ၊ ဝယ်ယူရမည့် ငွေပမာဏ %@ ကျပ် ၊ အလိုအလျှောက်ဆတူ အရေအတွက် နှင့် ငွေပမာဏကိုတိုးသည်။"
    
    static var offer_free_min_max_purchase_discont_per = "%@ အရေအတွက်ဝယ်လျှင် လက်ဆောင်အရေအတွက် %@ ရရှိမည် + ဈေးလျှော့ငွေ %@ ရာခိုင်နှုန်း ၊ ဝယ်ယူရမည့်အနည်းဆုံးငွေပမာဏ %@ ကျပ် မှ အများဆုံးငွေပမာဏ %@ ကျပ် အထိ၊"
    
    static var offer_free_fixed_purchase_discont_per = "%@ အရေအတွက်ဝယ်လျှင် လက်ဆောင်အရေအတွက် %@ ရရှိမည် + ဈေးလျှော့ငွေ %@ ရာခိုင်နှုန်း ၊ ဝယ်ယူရမည့် ငွေပမာဏ %@ ကျပ် ၊"
    
    static var offer_free_fixed_purchase_accumulated_quantity_amount_discont_per = "%@ အရေအတွက်ဝယ်လျှင် လက်ဆောင်အရေအတွက် %@ ရရှိမည် + ဈေးလျှော့ငွေ %@ ရာခိုင်နှုန်း ၊ ဝယ်ယူရမည့် ငွေပမာဏ %@ ကျပ် ၊ အလိုအလျှောက်ဆတူ အရေအတွက်နှင့် ငွေပမာဏကိုတိုးသည်။"
    
    static var offer_buy_min_max_purchase = "ဝယ်ယူမည့် အရေအတွက် %@ ၊ ဝယ်ယူရမည့် အနည်းဆုံးငွေပမာဏ %@ ကျပ် မှ အများဆုံးငွေပမာဏ %@ ကျပ် အထိ၊"
    
    static var offer_buy_fixed_purchase = "ဝယ်ယူမည့် အရေအတွက် %@ ၊ ဝယ်ယူရမည့် ငွေပမာဏ %@ ကျပ်"
    
    static var offer_buy_fixed_purchase_accumulated_quantity_amount = "ဝယ်ယူမည့် အရေအတွက် %@ ဝယ်ယူရမည့် ငွေပမာဏ %@ ကျပ် ၊ အလိုအလျှောက်ဆတူ အရေအတွက်နှင့် ငွေပမာဏကိုတိုးသည်။"
    
    static var offer_buy_min_max_purchase_discont_amount = "ဝယ်ယူမည့် အရေအတွက် %@ + ဈေးလျှော့ငွေပမာဏ %@ ကျပ် ၊ ဝယ်ယူရမည့် အနည်းဆုံးငွေပမာဏ %@ ကျပ် မှ အများဆုံးငွေပမာဏ %@ ကျပ် အထိ၊"
    
    static var offer_buy_fixed_purchase_discont_amount = "ဝယ်ယူမည့် အရေအတွက် %@ + ဈေးလျှော့ငွေပမာဏ %@ ကျပ် ၊ ဝယ်ယူရမည့် ငွေပမာဏ %@ ကျပ် ၊"
    
    static var offer_buy_fixed_purchase_accumulated_quantity_amount_discont_amount = "ဝယ်ယူမည့် အရေအတွက် %@ %@ + ဈေးလျှော့ငွေပမာဏ %@ ကျပ် ၊ ဝယ်ယူရမည့် ငွေပမာဏ %@ ကျပ် ၊ အလိုအလျှောက်ဆတူ အရေအတွက်နှင့် ငွေပမာဏကိုတိုးသည်။"
    
    static var offer_buy_min_max_purchase_discont_per = "ဝယ်ယူမည့် အရေအတွက် %@ + ဈေးလျှော့ငွေ %@ ရာခိုင်နှုန်း ၊ ဝယ်ယူရမည့် အနည်းဆုံးငွေပမာဏ %@ ကျပ် မှ အများဆုံးငွေပမာဏ %@ ကျပ် အထိ၊"
    
    static var offer_buy_fixed_purchase_discont_per = "ဝယ်ယူမည့် အရေအတွက် %@ + ဈေးလျှော့ငွေ %@ ရာခိုင်နှုန်း ၊ ဝယ်ယူရမည့်ငွေပမာဏ %@ ကျပ်"
    
    static var offer_buy_fixed_purchase_accumulated_quantity_amount_discont_per = "ဝယ်ယူမည့် အရေအတွက် %@ + ဈေးလျှော့ငွေ %@ ရာခိုင်နှုန်း ၊ ဝယ်ယူရမည့်ငွေပမာဏ %@ ကျပ် ၊ အလိုအလျှောက်ဆတူ အရေအတွက်နှင့် ငွေပမာဏကိုတိုးသည်။"
}
