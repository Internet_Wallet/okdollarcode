//
//  OffersTableViewCell.swift
//  OK
//
//  Created by SHUBH on 7/7/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class OffersTableViewCell: UITableViewCell {
    
    @IBOutlet weak var promoImage: UIImageView!
    @IBOutlet weak var promoCodeValue: UILabel!
    @IBOutlet weak var expireDate: UILabel!

    weak var delegate : OfferDisplayDataDelegate?
    
    var model: OffersListModel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func wrapCellData(_ model: OffersListModel) {
       self.model = model
        //print("wrapCellData-------")
        
        let promoCode = "Promo Code".localized + " "
        if let pmCode = model.promotionCode {
            let amtAttr = [NSAttributedString.Key.font: UIFont(name: appFont, size: 11.0) ?? UIFont.systemFont(ofSize: 11),
                           NSAttributedString.Key.backgroundColor: UIColor.white,
                           NSAttributedString.Key.foregroundColor: UIColor.red]
            let mmkAttr = [NSAttributedString.Key.font: UIFont(name: appFont, size: 15.0) ?? UIFont.systemFont(ofSize: 15),
                           NSAttributedString.Key.backgroundColor: UIColor.white,
                           NSAttributedString.Key.foregroundColor: UIColor.black]
            let amountStr    = NSMutableAttributedString.init(string: promoCode, attributes: amtAttr)
            let mmkString = NSMutableAttributedString(string: pmCode, attributes: mmkAttr)
            
            let stringAttributed = NSMutableAttributedString()
            stringAttributed.append(amountStr)
            stringAttributed.append(mmkString)
            self.promoCodeValue.attributedText =  stringAttributed
            println_debug(model.promotionExpiryDate ?? "")
            let intDays = self.getExpireDays(date: model.promotionExpiryDate ?? "")
            if intDays.count < 0 {
                self.expireDate?.text = ""
            } else {
                let strExpire = " Expires in ".localized
                let strDays = " " + " days".localized
                
                let expAttr = [NSAttributedString.Key.font: UIFont(name: appFont, size: 11.0) ?? UIFont.systemFont(ofSize: 11),
                               NSAttributedString.Key.backgroundColor: UIColor.white,
                               NSAttributedString.Key.foregroundColor: UIColor.red]
                let dayAttr = [NSAttributedString.Key.font: UIFont(name: appFont, size: 15.0) ?? UIFont.systemFont(ofSize: 15),
                               NSAttributedString.Key.backgroundColor: UIColor.white,
                               NSAttributedString.Key.foregroundColor: UIColor.black]
                let attExpire    = NSMutableAttributedString.init(string: strExpire, attributes: expAttr)
                let attDay    = NSMutableAttributedString.init(string: "\(intDays)", attributes: dayAttr)
                let attDays = NSMutableAttributedString(string: strDays, attributes: expAttr)
                
                let stringAttributed = NSMutableAttributedString()
                if appDel.currentLanguage == "my" {
                    stringAttributed.append(attDay)
                    stringAttributed.append(attExpire)
                    stringAttributed.append(attDays)
                } else {
                    stringAttributed.append(attExpire)
                    stringAttributed.append(attDay)
                    stringAttributed.append(attDays)
                }
               
                self.expireDate.attributedText = stringAttributed
            }
        }
            
      if appDel.currentLanguage == "my" {
        if let imageString = model.promotionBImg, let imageURL = URL(string: imageString) {
          //self.promoImage.setImage(url: imageURL, contentMode: .scaleAspectFit, placeholderImage: nil)
            self.promoImage.sd_setImage(with: imageURL, placeholderImage: nil)

        }
      }
      else if appDel.currentLanguage == "th" {
        if let imageString = model.promotionTImg, let imageURL = URL(string: imageString) {
          //self.promoImage.setImage(url: imageURL, contentMode: .scaleAspectFit, placeholderImage: nil)
            self.promoImage.sd_setImage(with: imageURL, placeholderImage: nil)

        }
      }
      else if appDel.currentLanguage == "zh-Hans" {
        if let imageString = model.promotionCImg, let imageURL = URL(string: imageString) {
          //self.promoImage.setImage(url: imageURL, contentMode: .scaleAspectFit, placeholderImage: nil)
            self.promoImage.sd_setImage(with: imageURL, placeholderImage: nil)
        }
      }
      else {
        if let imageString = model.promotionImg, let imageURL = URL(string: imageString) {
          //self.promoImage.setImage(url: imageURL, contentMode: .scaleAspectFit, placeholderImage: nil)
            
            self.promoImage.sd_setImage(with: imageURL, placeholderImage: nil)
        }
      }
    }
    
    func getExpireDays(date: String) -> String
    {
        println_debug(date)

        //Convert current date
        let currentDateFormatter = DateFormatter()
        currentDateFormatter.calendar = Calendar(identifier: .gregorian)
        currentDateFormatter.dateFormat = "dd-MMM-yyyy"
        let currentDateStr = currentDateFormatter.string(from:Date())
        let currentDate = currentDateFormatter.date(from: currentDateStr)
        println_debug(currentDate)
        
        //calculate duration
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss"
        let yourDate = formatter.date(from: date)
        //let expDate = currentDateFormatter.date(from: yourDate)
        //println_debug(expDate!)
        
        let form = DateComponentsFormatter()
        form.maximumUnitCount = 2
        form.unitsStyle = .full
        form.allowedUnits = [.day]
        
        let yearValue = form.string(from: currentDate!, to: yourDate!)
        //println_debug(yearValue as Any)
        let fullNameArr = yearValue?.components(separatedBy: " ")
        let firstName = (fullNameArr![0] as NSString)
        
        return firstName as String
    }
}

