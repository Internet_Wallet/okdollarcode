//
//  OfferDataDisplayCollectionViewCell.swift
//  OK
//
//  Created by Ashish on 7/16/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol OfferDisplayDataDelegate : class {
    
    func didSelectListObject(model: OffersListModel)
    
    func didTappedTnC(withModel model: OffersListModel)
}

class OfferDataDisplayCollectionViewCell: UICollectionViewCell, UITableViewDelegate, UITableViewDataSource, OfferDisplayDataDelegate {
    
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var labelMsg: UILabel!
    {
        didSet
        {
            labelMsg.font = UIFont(name: appFont, size: appFontSize)
            labelMsg.text = "No Offers Found !!".localized
        }
    }
    @IBOutlet weak var stackCollection : UIStackView!
    
    weak var delegate : OfferDisplayDataDelegate?
    
    var model : [OffersListModel]?
    
    var searchCopyModel : [OffersListModel]?
    
    override func awakeFromNib() {
        let className = String(describing: OffersTableViewCell.self)
        self.dataTableView.register(UINib.init(nibName: className, bundle: Bundle.main), forCellReuseIdentifier: className)
        NotificationCenter.default.addObserver(self, selector: #selector(self.searchUpdate(_:)), name: Notification.Name(rawValue: "OfferSearchingDashboard"), object: nil)
    }
    
    // handle notification
    @objc func searchUpdate(_ notification: NSNotification) {
        if let notificationData = notification.userInfo {
            if let searchtext = notificationData.safeValueForKey("searchText") as? String, searchtext.count > 0 {
                self.model = self.searchCopyModel?.filter({
                  guard let promName = $0.promotionName, let promID = $0.promotionID else { return false}
                    return (promName.contains(find: searchtext) || promID.contains(find: searchtext))
                })
            } else {
                self.model = self.searchCopyModel
            }
            self.dataTableView.reloadData()
        }
    }
    
    func didUpdateData(_ dataModel: [OffersListModel]) {
        if dataModel.count > 0 {
            self.stackCollection.isHidden = true
            self.dataTableView.isHidden = false
            self.model = dataModel
            self.searchCopyModel = dataModel
            self.dataTableView.reloadData()
        } else {
            self.stackCollection.isHidden = false
            self.dataTableView.isHidden = true
            self.model = dataModel
            self.searchCopyModel = dataModel
            self.dataTableView.reloadData()
        }
    }
    
    func didSelectListObject(model: OffersListModel) {
        if let delegate = self.delegate {
            delegate.didSelectListObject(model: model)
        }
    }
    
    func didTappedTnC(withModel model: OffersListModel) {
        if let delegate = self.delegate {
            delegate.didTappedTnC(withModel: model)
        }
    }
    
    //MARK:- UITableView Delegate & Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.model == nil) ? 0 : self.model!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: OffersTableViewCell.self)) as! OffersTableViewCell
        cell.delegate = self
        if let model = self.model {
            if let dataModel = model[safe: indexPath.row] {
                cell.wrapCellData(dataModel)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let delegate = self.delegate {
            guard let fetchedObjects = model  else {
                return
            }
            delegate.didSelectListObject(model: fetchedObjects[indexPath.row])
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170.00
    }
    
    
}
