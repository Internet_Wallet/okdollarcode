//
//  PMConfirmationViewController.swift
//  OK
//
//  Created by Admin on 12/29/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PMConfirmationViewController: OKBaseController {
    
    var DictToSend = Dictionary<String, Any>()
    var valueArray : [String] = []
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        print(DictToSend)
        print(valueArray)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    @IBAction func payClick(_ sender: UIButton){
        
        let payVCObj = LLBStrings.FileNames.PMStoryBoardObj.instantiateViewController(withIdentifier: "PMReceiptViewController") as! PMReceiptViewController
        self.navigationController?.pushViewController(payVCObj, animated: true)
        
    }
    
    
    
    @IBAction func btnBackClick(_ sender: UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
}
