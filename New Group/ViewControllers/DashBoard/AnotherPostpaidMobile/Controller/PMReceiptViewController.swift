//
//  PMReceiptViewController.swift
//  OK
//
//  Created by Admin on 12/29/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PMReceiptViewController: OKBaseController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnBackClick(_ sender: UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
    }

}
