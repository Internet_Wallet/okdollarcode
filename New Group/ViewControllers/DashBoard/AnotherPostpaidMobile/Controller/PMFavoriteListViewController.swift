
//
//  PMFavoriteListVC.swift
//  OK
//
//  Created by Admin on 12/26/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PMFavoriteListViewController: OKBaseController{
    
    var tblLabelArr = [String]()
    var tblIconArr  = [UIImage]()
    var privilageUser = String()
    var getPaymentOtpDict = Dictionary<String, Any>()
    var dict = Dictionary<String,String>()
    
    fileprivate let cellId = "PMFavoriteCell"
    
    @IBOutlet weak var FavTbl: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Favorite List"
        
    
        
        self.initalizeArray()
        // Do any additional setup after loading the view.
        
    }
    
    
    func initalizeArray(){
        
        
            
            tblLabelArr = [
                "Security Lock Type",
                "Add & View CashBack",
                "SaleNLock User List",
                "Approve SaleNLock User",
                "Transfer OK $ Main Balance to Cashback",
                "Add 5 Other OK $ Mobile Numbers",
                "Add & View Promotion",
                "Get OTP for Web Admin",
                "Get Payment Gateway OTP",
                "Generate Agent Code",
                "Cash in|Out rate & Amount"
            ]
            
            tblIconArr  =  [
                #imageLiteral(resourceName: "security_lock"),
                #imageLiteral(resourceName: "add_viewcashback_st"),
                #imageLiteral(resourceName: "safety_cashier_st"),
                #imageLiteral(resourceName: "approved_safety_cashier_st"),
                #imageLiteral(resourceName: "mainbalance_cashback_st"),
                #imageLiteral(resourceName: "add_number_st"),
                #imageLiteral(resourceName: "advertisement_st"),
                #imageLiteral(resourceName: "get_otp_st"),
                #imageLiteral(resourceName: "payment_gateway_st"),
                #imageLiteral(resourceName: "otp"),
                #imageLiteral(resourceName: "cash_in_out")
            ]
            
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func switchtoViewController(index : Int){
        
        
        
            
           
        
        
        
        
    }
    
    
    @IBAction func closeBtn(_ sender: UIBarButtonItem) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    func customAlert(msg:String,title:String){
        
        alertViewObj.addTextField(title: "", body: msg, img: #imageLiteral(resourceName: "alert-icon"))
        //alertViewObj.addAction(title: "NO", style: .cancel){}
        alertViewObj.addAction(title: "Done", style: .target){
            
            
        }
        alertViewObj.showAlert(controller: self)
    }
    
    
    
    
    func moveToController(identifier: String){
        
        let VC = self.storyboard?.instantiateViewController(withIdentifier: identifier)
        self.navigationController?.pushViewController(VC!, animated: true)
        
    }
    
    
    
}

extension PMFavoriteListViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SettingTableViewCell
        
        let icon  = tblIconArr[indexPath.row]
        let name  = tblLabelArr[indexPath.row]
        cell.wrapData(img: icon, name: name)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tblLabelArr.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.switchtoViewController(index: indexPath.row)
    }
    
}



class PMFavoriteCell: UITableViewCell{
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var icon: UIImageView!
    
    override func awakeFromNib(){
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool){
        super.setSelected(selected, animated: animated)
    }
    
    func wrapData(img: UIImage, name: String){
        icon.image    = img
        lblName.text = name
    }
    
}
