//
//  SunKingPaymentViewController.swift
//  OK
//
//  Created by iMac on 2/1/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class SunKingPaymentViewController: OKBaseController {

    @IBOutlet weak var webView: UIWebView!
    
    var fromScreen: String? = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Remove all cache
        URLCache.shared.removeAllCachedResponses()
        
        // Delete any associated cookies
        if let cookies = HTTPCookieStorage.shared.cookies {
            for cookie in cookies {
                HTTPCookieStorage.shared.deleteCookie(cookie)
            }
        }
        
        PTLoader.shared.show()
        if let frmScreen = fromScreen, frmScreen == "SunKing" {
            self.title = "Sun King Payment".localized
        } else {
           
            self.title = "Insurance".localized
        }
        self.addBackButton()
        self.navigationController?.isNavigationBarHidden = true
        self.view.backgroundColor = kYellowColor
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 21) ?? UIFont.systemFont(ofSize: 21), NSAttributedString.Key.foregroundColor: UIColor.white]
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let frmScreen = fromScreen, frmScreen != "SunKing" {
            self.navigationController?.isNavigationBarHidden = true
        }
        self.loadUrl()
        webView.scrollView.bouncesZoom = false
        webView.scrollView.bounces = false
        webView.scrollView.contentSize = CGSize.init(width: screenWidth, height: webView.scrollView.contentSize.height)
    }
    
    private func addBackButton() {
        let btn = UIButton(frame: CGRect(x: 12.0, y: 0.0, width: 80.0, height: 64.0))
        btn.addTargetClosure { (btn) in
            self.dismiss(animated: true, completion: nil)
        }
        self.view.addSubview(btn)
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        if webView.canGoBack {
            webView.goBack()
        } else {
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    func sunKingRequestParameters() -> NSDictionary {
        
        var language = "en-GB"
        var languageType = ""
        
        if appDel.currentLanguage == "my"{
            language = "my-MM"
            languageType = ""
        } else if appDel.currentLanguage == "uni"{
            language = "my-MM"
            languageType = "1"
        }else {
            language = "en-GB"
            languageType = ""
        }
        
        let paramDict: NSDictionary  = ["UtilityJsonRequest":
            ["MobileNumber":UserModel.shared.mobileNo,
             "Pwd":ok_password ?? "",
             "Appid":UserModel.shared.appID,
             "Simid":uuid,
             "Msid":msid,
             "Lat":GeoLocationManager.shared.currentLatitude,
             "Long":GeoLocationManager.shared.currentLongitude,
             "DeviceID":UserModel.shared.deviceId,
             "Language":language,
             "LanguageType": languageType,
             "AppVersion":buildVersion,
             "IsNativeApp": false,
             "Balance":self.walletAmount(),
             "TransactionsCellTower": self.transactionsCellTowerDictionary(),
             "lGeoLocation" : self.lGeoLocationDictionary()]]
        return paramDict
    }
    
    func sunKingPayment(urlString: String) -> URLRequest {
        let url = URL.init(string: urlString)
        var request = URLRequest.init(url: url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        let param = self.sunKingRequestParameters()
        let postData: Data? = try? JSONSerialization.data(withJSONObject: param, options: [])
        if postData != nil {    request.httpBody = postData
            
        }
        return request
    }
    
    func insuranceRequestParameters() -> NSDictionary {
        
        var language = "en-GB"
        var languageType = ""
        
        if appDel.currentLanguage == "my"{
            language = "my-MM"
            languageType = ""
        } else if appDel.currentLanguage == "uni"{
            language = "my-MM"
            languageType = "1"
        }else {
            language = "en-GB"
            languageType = ""
        }
        
        let paramDict: NSDictionary  = ["UtilityJsonRequest":
            ["MobileNumber":UserModel.shared.mobileNo,
             "Pwd":ok_password ?? "",
             "Appid":UserModel.shared.appID,
             "Simid":uuid,
             "Msid":msid,
             "Lat":GeoLocationManager.shared.currentLatitude,
             "Long":GeoLocationManager.shared.currentLongitude,
             "DeviceID":UserModel.shared.deviceId,
             "Language":language,
             "LanguageType": languageType,
             "AppVersion":buildVersion,
             "IsNativeApp":false,
             "TransactionsCellTower": self.transactionsCellTowerDictionary(),
             "lGeoLocation" : self.lGeoLocationDictionary()]]
        return paramDict
    }
        
    func insurancePaymentRequest(urlString: String) -> URLRequest {
        let url = URL.init(string: urlString)
        var request = URLRequest.init(url: url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        let param = self.insuranceRequestParameters()
        let postData: Data? = try? JSONSerialization.data(withJSONObject: param, options: [])
        if postData != nil {    request.httpBody = postData
            
        }
        return request
    }
    
    func loadUrl() {
        if let frmScreen = fromScreen, frmScreen == "SunKing" {
           // let urls = "https://angaza.okdollar.org/"
            var urls = "https://angaza.okdollar.org/"
            if serverUrl == .testUrl {
                 urls = "http://test.angaza.okdollar.org/"
            } else {
                urls = "https://angaza.okdollar.org/"
            }
            let request = self.sunKingPayment(urlString: urls)
            println_debug(request.debugDescription)
            println_debug(urls.debugDescription)
            webView.loadRequest(request)
        } else {
            var urls = "https://insurance.okdollar.org/home/index"
            if serverUrl == .testUrl {
                urls = "http://test.insurance.okdollar.org/home/index"
            } else {
                 urls = "https://insurance.okdollar.org/home/index"
            }
            let request = self.insurancePaymentRequest(urlString: urls)
            println_debug(request.debugDescription)
            println_debug(urls.debugDescription)
            webView.loadRequest(request)
        }
        
    }
    
}

//MARK:- Webview Delegates
extension SunKingPaymentViewController: UIWebViewDelegate {
    func webViewDidStartLoad(_ webView: UIWebView) {
        PTLoader.shared.show()
        println_debug("Started")
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        PTLoader.shared.hide()
        println_debug("Finished")

    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        PTLoader.shared.hide()
        println_debug(error)
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        return true
    }
}
