//
//  ShareDocumentViewController.swift
//  OK
//
//  Created by Ashish on 10/31/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import WebKit
class ShareDocumentViewController: UIViewController, UIWebViewDelegate, UIDocumentInteractionControllerDelegate,WKNavigationDelegate,WKUIDelegate {
    
   // @IBOutlet weak var displayWebView : UIWebView!
    
    @IBOutlet var displayWebView: WKWebView!
    
    @IBOutlet weak var closeBtn: UIBarButtonItem!
    
    let documentInteractionController = UIDocumentInteractionController()
    
    var pathURL : URL?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        displayWebView.navigationDelegate = self
        displayWebView.uiDelegate = self
        
        documentInteractionController.delegate = self
        
       guard let loadRequestURL = pathURL else { return }
        
        let request = URLRequest.init(url: loadRequestURL)
        
        self.displayWebView.load(request)
        
    }
    
    //MARK:- IBActions marking
    
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- UIWebViewDelegate Methods
    func webViewDidStartLoad(_ webView: UIWebView) {
        PTLoader.shared.show()
        println_debug("Started")
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        PTLoader.shared.hide()
        println_debug("Finished")
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        PTLoader.shared.hide()
        println_debug(error)
    }

    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        guard let navVC = self.navigationController else {
            return self
        }
        return navVC
    }
}

extension URL {
    var typeIdentifier: String? {
        return (try? resourceValues(forKeys: [.typeIdentifierKey]))?.typeIdentifier
    }
    var localizedName: String? {
        return (try? resourceValues(forKeys: [.localizedNameKey]))?.localizedName
    }
}
