//
//  EcommerceWebViewController.swift
//  OK
//
//  Created by Vinod's MacBookPro on 9/22/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import WebKit
class EcommerceWebViewController: OKBaseController,WKNavigationDelegate,WKUIDelegate {
    
   // @IBOutlet weak var webView: UIWebView!

    @IBOutlet var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
      //  PTLoader.shared.show()
        self.title = "ZAYOK".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
        
        webView.navigationDelegate = self
        webView.uiDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.loadUrl()
        webView.scrollView.bouncesZoom = false
        webView.scrollView.bounces = false
        webView.scrollView.contentSize = CGSize.init(width: screenWidth, height: webView.scrollView.contentSize.height)
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        if webView.canGoBack {
            webView.goBack()
        } else {
            self.dismiss(animated: true, completion: nil)
        }

    }
    
    func eCommerceRequestParameters() -> NSDictionary {
        
        let userName = UserModel.shared.mobileNo.dropFirst(4)
        var DateOfBirthDay = ""
        var DateofBirthMonth = ""
        var DateOfBirthYear = ""
        if let dob = self.getDOBDateFormatforEcommerce(UserModel.shared.dob) as? String{
            if let fullNameArr = dob.components(separatedBy: "-") as? [String]{
                DateOfBirthDay = fullNameArr[0]
                DateofBirthMonth = fullNameArr[1]
                DateOfBirthYear = fullNameArr[2]
            }else{
                DateOfBirthDay = ""
                DateofBirthMonth = ""
                DateOfBirthYear = ""
            }
        }
       
        
        let gender = UserModel.shared.gender
        var gen = ""
        if gender == "0"{
            gen = "F"
        }
        else {
            gen = "M"
        }
        
        let paramDict: NSDictionary  = ["UtilityJsonRequest":
            ["Firstname":UserModel.shared.name,
             "Lastname":"",
             "Email":UserModel.shared.email,
             "Username": userName,
             "Gender":gen,
             "DateOfBirthDay":DateOfBirthDay,
             "DateofBirthMonth":DateofBirthMonth,
             "DateOfBirthYear":DateOfBirthYear,
             "Address1":UserModel.shared.address1,
             "Address2":UserModel.shared.address2,
             "City":UserModel.shared.township,
             "State":UserModel.shared.state,
             "Country":UserModel.shared.country,
             "OtherEmail":"",
             "MobileNumber":UserModel.shared.mobileNo,
             "DeviceID":UserModel.shared.deviceId,
             "Simid":uuid,
             "Pwd":ok_password ?? ""]]
        
        return paramDict
    }

    func giftCardLoader(urlString: String) -> URLRequest {
        let url = URL.init(string: urlString)
        var request = URLRequest.init(url: url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        let param = self.eCommerceRequestParameters()
        let postData: Data? = try? JSONSerialization.data(withJSONObject: param, options: [])
        if postData != nil {
            request.httpBody = postData
        }
        
        return request
    }
    
    func loadUrl() {
        
        let urls = "https://zayok.com"
        let request = self.giftCardLoader(urlString: urls)
        println_debug(request.debugDescription)
        println_debug(urls.debugDescription)
        if let url = URL(string: urls) {
            let request = URLRequest(url: url)
            webView.load(request)
        }
        
    }
    
}

//MARK:- Webview Delegates
//extension EcommerceWebViewController: UIWebViewDelegate {
//    func webViewDidStartLoad(_ webView: UIWebView) {
//        PTLoader.shared.show()
//        println_debug("Started")
//    }
//
//    func webViewDidFinishLoad(_ webView: UIWebView) {
//        PTLoader.shared.hide()
//        println_debug("Finished")
//}
//
//    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
//        PTLoader.shared.hide()
//        println_debug(error)
//    }
//
//    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
//        return true
//    }
//}

