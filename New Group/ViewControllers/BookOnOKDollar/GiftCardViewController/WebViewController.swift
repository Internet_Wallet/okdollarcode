//
//  GiftCardViewController.swift
//  OK
//
//  Created by Vinod's MacBookPro on 10/22/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import WebKit

enum WebType {
    case giftCard
    case merchantPayment
    case bankDeposit
    case dth
    case eCommerce
    case donation
    case pay123
}

class WebViewController: OKBaseController, UIDocumentInteractionControllerDelegate,WKNavigationDelegate,WKUIDelegate{
    
    //MARK: - Outlets
    private var statusBarView: UIView?

    @IBOutlet weak var webView: UIWebView!
//    @IBOutlet var titleText: UILabel!
    
 //   @IBOutlet var webView: WKWebView!
    @IBOutlet var aView: UIView!
    
    @IBOutlet weak var backButton: UIButton!
    //MARK: - Properties
    var type: WebType?
    let documentInteractionController = UIDocumentInteractionController()
    
    //MARK: - Views Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
    //    webView.navigationDelegate = self
    //    webView.uiDelegate = self
        webView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)

        webView.delegate = self
        if let typeLoc = type, typeLoc == .dth || typeLoc == .giftCard || typeLoc == .donation || typeLoc == .merchantPayment || typeLoc == .pay123 {
            self.backButton.setImage(UIImage(named: ""), for: .normal)
        }
        documentInteractionController.delegate = self
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        appDel.floatingButtonControl?.window.isHidden = true
        view.bringSubviewToFront(aView)
        self.loadUrl(urlType: type!)
                
        if #available(iOS 13.0, *) {
            let tag = 38482458385
            if let statusBar = UIApplication.shared.keyWindow?.viewWithTag(tag) {
                statusBarView = statusBar
            } else {
                let statusBar = UIView(frame: UIApplication.shared.statusBarFrame)
                statusBar.tag = tag
                UIApplication.shared.keyWindow?.addSubview(statusBar)
                statusBarView = statusBar
            }
        } else {
            statusBarView = UIApplication.shared.value(forKey: "statusBar") as? UIView
        }
        
        if statusBarView?.responds(to: #selector(setter: UIView.backgroundColor)) ?? false {
            statusBarView?.backgroundColor = kYellowColor//UIColor.init(hex: "F3C632")
        }
        
//        webView.scrollView.bouncesZoom = false
//        webView.scrollView.bounces = false
//        webView.scrollView.contentSize = CGSize.init(width: screenWidth, height: webView.scrollView.contentSize.height)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        if #available(iOS 13.0, *) {
            let tag = 38482458385
            if let statusBar = UIApplication.shared.keyWindow?.viewWithTag(tag) {
                statusBarView = statusBar
            } else {
                let statusBar = UIView(frame: UIApplication.shared.statusBarFrame)
                statusBar.tag = tag
                UIApplication.shared.keyWindow?.addSubview(statusBar)
                statusBarView = statusBar
            }
        } else {
            statusBarView = UIApplication.shared.value(forKey: "statusBar") as? UIView
        }
        
        if statusBarView?.responds(to: #selector(setter: UIView.backgroundColor)) ?? false {
            statusBarView?.backgroundColor = .clear//UIColor.init(hex: "F3C632")
        }
        
    }
    
    //MARK: - Methods
    //MARK: Load URL - common for all type
    func loadUrl(urlType: WebType) {
        var urlS = ""
        switch urlType {
        case .dth:
            let urlString = getUrl(urlStr: "", serverType: .dth)
            urlS = String.init(format: "%@", urlString.absoluteString)
            //urlS = Url.dth
            let request = self.giftCardLoader(urlString: urlS)
            println_debug(request.debugDescription)
            println_debug(urlS.debugDescription)
            self.webView.loadRequest(request)
            
        case .bankDeposit:
            urlS = Url.bankDeposit
            let request = self.giftCardLoader(urlString: urlS)
            println_debug(request.debugDescription)
            println_debug(urlS.debugDescription)
            self.webView.loadRequest(request)
            
        case .giftCard:
            let urlString = getUrl(urlStr: "", serverType: .giftCard)
            urlS = String.init(format: "%@", urlString.absoluteString)
            //urlS = Url.giftCard
            let request = self.giftCardLoader(urlString: urlS)
            println_debug(request.debugDescription)
            println_debug(urlS.debugDescription)
            self.webView.loadRequest(request)
            
        case .pay123: // Gauri Added
            
            let urlString = getUrl(urlStr: "", serverType: .pay123)
            urlS = String.init(format: "%@", urlString.absoluteString)
            let request = self.giftCardLoader(urlString: urlS)
            self.webView.loadRequest(request)
            
        case .merchantPayment: // shobhit modified
            urlS = Url.merchantPayment
            
            urlS = "https://merchantpayment.okdollar.org/MerchantPayments/Home/Index"
            if serverUrl == .testUrl {
                urlS = "http://test.mobileview.okdollar.org/MerchantPayments/Home/Index"
            } else {
                urlS = "https://mobileview.okdollar.org/MerchantPayments/Home/Index"
            }
            
            let request = self.giftCardLoader(urlString: urlS)
            println_debug(request.debugDescription)
            println_debug(urlS.debugDescription)
            self.webView.loadRequest(request)
            
        case .eCommerce:
            let urlString = getUrl(urlStr: "", serverType: .eCommerce)
            urlS = String.init(format: "%@", urlString.absoluteString)
            let request = self.giftCardLoader(urlString: urlS)
            println_debug(request.debugDescription)
            println_debug(urlS.debugDescription)
            self.webView.loadRequest(request)
        case .donation:
            let urlString = getUrl(urlStr: Url.donation, serverType: .donationUrl)
            urlS = String.init(format: "%@", urlString.absoluteString)
            let request = self.giftCardLoader(urlString: urlS)
            self.webView.loadRequest(request)
        }
    }
    
    //MARK: Gift Card Loader
    func giftCardLoader(urlString: String) -> URLRequest {
        
        // Remove all cache
        URLCache.shared.removeAllCachedResponses()
        
        // Delete any associated cookies
        if let cookies = HTTPCookieStorage.shared.cookies {
            for cookie in cookies {
                HTTPCookieStorage.shared.deleteCookie(cookie)
            }
        }
        
        let url = URL.init(string: urlString)
        var request = URLRequest.init(url: url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60)
                    request.addValue("application/json;charset=UTF-8", forHTTPHeaderField: "Content-Type")
                    request.addValue("application/json", forHTTPHeaderField: "Accept")
                    //request.addValue("ISO-8859-1,utf-8;q=0.7,*;q=0.3", forHTTPHeaderField: "Accept-Charset")

            request.httpMethod = "POST"
        if type == WebType.merchantPayment {
            let param = self.getParameter()
            let postData: Data? = try? JSONSerialization.data(withJSONObject: param, options: [])
            if postData != nil {
                request.httpBody = postData
            }

        } else if type == WebType.bankDeposit {
            let param = self.bankDepositParameters()
            let postData: Data? = try? JSONSerialization.data(withJSONObject: param, options: [])
            if postData != nil {
                request.httpBody = postData
            }
        } else if type == WebType.eCommerce {
            let param = self.eCommerceRequestParameters()
            let postData: Data? = try? JSONSerialization.data(withJSONObject: param, options: [])
            if postData != nil {
                request.httpBody = postData
            }
        } else { // for gift cards
            let param = self.getParamsGiftCard()
            let postData: Data? = try? JSONSerialization.data(withJSONObject: param, options: [])
            if postData != nil {
                request.httpBody = postData
            }
        }
        return request
    }
    
    //MARK: Parameters according to enum
    func getParamsGiftCard() -> Dictionary<String,Any> {

        var language = "en-GB"
        var languageType = "0"
        
        if appDel.currentLanguage == "my"{
            language = "my-MM"
            languageType = ""
        } else if appDel.currentLanguage == "uni"{
            language = "my-MM"
            languageType = "1"
        }else {
            language = "en-GB"
            languageType = "0"
        }
        
        let childDicts = [
            "Pwd": ok_password ?? "",
            "Appid": LoginParams.setUniqueIDLogin(),
            "MobileNumber": UserModel.shared.mobileNo,
            "Simid": uuid,
            "Msid": UserModel.shared.msid,
            "Lat": UserModel.shared.lat,
            "Long": UserModel.shared.long,
            "DeviceID": uuid,
            "Language": language,
            "Balance": UserLogin.shared.walletBal,
            "AppVersion": buildVersion,
            "IsNativeApp": false,
            "TransactionsCellTower": self.transactionsCellTowerDictionary(),
            "lGeoLocation" : self.lGeoLocationDictionary(),
            "LanguageType": languageType
            ] as [String: Any]
        
        let final = [
            "UtilityJsonRequest": childDicts
        ]
        return final
    }

    func getParameter() -> NSDictionary {
        let lat  = geoLocManager.currentLatitude ?? ""
        let long = geoLocManager.currentLongitude ?? ""
        
        //let currentStr = UserDefaults.standard.string(forKey: "currentLanguage")
        var language = "en-GB"
        var languageType = ""

        if appDel.currentLanguage == "my"{
            language = "my-MM"
            languageType = ""
        } else if appDel.currentLanguage == "uni"{
            language = "my-MM"
            languageType = "1"
        }else {
            language = "en-GB"
            languageType = ""
        }
        var appID = LoginParams.setUniqueIDLogin()
        if appID.hasPrefix("OK_ID,"){
            appID = String(appID.dropFirst(6))
        }
        let paramDict: NSDictionary = ["Data": [["Lat": lat,
                                                 "Long": long,
                                                 "Msid": UserModel.shared.msid,
                                                 "Language": language,
                                                 "LanguageType": languageType,
                                                 "Pwd": ok_password ?? "",
                                                 "Appid": appID,
                                                 "Simid": uuid,
                                                 "OkAccountNumber": UserModel.shared.mobileNo,
                                                 "AppVersion": buildVersion,
                                                 "Balance": UserLogin.shared.walletBal,
                                                 "IsNativeApp": false,
                                                 "MobileNumber": UserModel.shared.mobileNo,
                                                 "DeviceID": uuid,
                                                 "TransactionsCellTower": self.transactionsCellTowerDictionary(),
                                                 "lGeoLocation" : self.lGeoLocationDictionary()
            ]]]
        return paramDict
    }
    
    func bankDepositParameters() -> NSDictionary {
        
        var language = "en-GB"
        var languageType = ""

        if appDel.currentLanguage == "my"{
            language = "my-MM"
            languageType = ""
        } else if appDel.currentLanguage == "uni"{
            language = "my-MM"
            languageType = "1"
        }else {
            language = "en-GB"
            languageType = ""
        }
        
        let paramDict: NSDictionary = ["BankVerificationJsonRequest": ["MobileNumber": UserModel.shared.mobileNo,
                "Pwd": ok_password ?? "",
                "Appid": LoginParams.setUniqueIDLogin(),
                "Simid": uuid,
                "Msid": UserModel.shared.msid,
                "Lat": UserModel.shared.lat,
                "Long": UserModel.shared.long,
                "DeviceID": uuid,
                "Language": language,
                "LanguageType": languageType,
                "AppVersion": buildVersion,
                "IsNativeApp": false,
                "Balance": UserLogin.shared.walletBal]]
        return paramDict
    }
    
    func eCommerceRequestParameters() -> NSDictionary {
        let userName = UserModel.shared.mobileNo.dropFirst(4)
        let dob = self.getDOBDateFormatforEcommerce(UserModel.shared.dob)
        let fullNameArr = dob.components(separatedBy: "-")
        let gender = UserModel.shared.gender
        var gen = ""
        if gender == "0" {
            gen = "F"
        } else {
            gen = "M"
        }
        
        let paramDict: NSDictionary  = ["UtilityJsonRequest":
            ["Firstname": UserModel.shared.name,
            "Lastname": "","Email": UserModel.shared.email,
            "Username": userName,
            "Gender": gen,
            "DateOfBirthDay": fullNameArr[0],
            "DateofBirthMonth": fullNameArr[1],
            "DateOfBirthYear": fullNameArr[2],
            "Address1": UserModel.shared.address1,
            "Address2": UserModel.shared.address2,
            "City": UserModel.shared.township,
            "State": UserModel.shared.state,
            "Country": UserModel.shared.country,
            "OtherEmail": "",
            "MobileNumber": UserModel.shared.mobileNo,
            "DeviceID": UserModel.shared.deviceId,
            "Simid": uuid,
            "Pwd": ok_password ?? "",
            "TransactionsCellTower": self.transactionsCellTowerDictionary(),
            "lGeoLocation" : self.lGeoLocationDictionary()]]
        return paramDict
    }
  
    //MARK: - Button action methods
    @IBAction func backAction(_ sender: UIButton) {
        if webView.canGoBack {
            webView.goBack()
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
//    private func webView(webView: WKWebView!, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError!) {
//        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
//        present(alert, animated: true, completion: nil)
//    }
//    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
//        //   println_debug("Started")
//
//        print("load")
//        PTLoader.shared.show()
//    }
//    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//        //   println_debug("Finished")
//        print("finish")
//        PTLoader.shared.hide()
//}
//    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
//        if let url = navigationAction.request.url, let param = url.params {
//             if param.safeValueForKey("d") as? String == "download" || param.safeValueForKey("d") as? String == "view" {
//            FileSystemDownloadManager.downloadPDF(url: url) { [weak self](path, success) in
//                if success {
//                    guard let weakSelf = self else { return }
//                    if param.safeValueForKey("d") as? String == "download" {
//                     DispatchQueue.main.async {
//                      weakSelf.documentInteractionController.url  = path
//                      weakSelf.documentInteractionController.uti  = path.typeIdentifier ?? "public.data, public.content"
//                       weakSelf.documentInteractionController.name = "Download & Share".localized
//                       weakSelf.documentInteractionController.presentPreview(animated: true)
//                      }
//                }
//            }
//        }
//
//        }
//    }
 
//  }

}

//MARK: - Webview Delegates
extension WebViewController: UIWebViewDelegate {
    
    func shareWebURL() {
        
        let webViewFrame = webView.frame
        
        webView.frame = CGRect(x: webViewFrame.origin.x, y: webViewFrame.origin.y, width: webView.scrollView.contentSize.width, height: webView.scrollView.contentSize.height)
        UIGraphicsBeginImageContextWithOptions(webView.scrollView.contentSize, false, 0);
        self.webView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image:UIImage = UIGraphicsGetImageFromCurrentImageContext()!;
        UIImageWriteToSavedPhotosAlbum(image, self, nil, nil)
        
        var activityItems = [Any]()
        activityItems.append(image)
        let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToVimeo]
        
        DispatchQueue.main.async {
            self.present(activityViewController, animated: true, completion: {
            })
        }
        
        //this code will work when i will press the cross button of activity view controller
        activityViewController.completionWithItemsHandler = { (activityType, completed:Bool, returnedItems:[Any]?, error: Error?) in
            DispatchQueue.main.async {
                self.webView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        PTLoader.shared.show()
        println_debug("Started")
        if let text = webView.request?.url?.absoluteString{
            println_debug(text)
           
        }
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        PTLoader.shared.hide()
        println_debug("Finished")
        if let text = webView.request?.url?.absoluteString{
            println_debug(text)
            if text.contains("k=sharefullview"){
                println_debug("screenfullview")
                shareWebURL()
            }
        }
    }

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        PTLoader.shared.hide()
        println_debug(error)
    }

    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        PTLoader.shared.show()
        if let url = request.url, let param = url.params {
            if param.safeValueForKey("d") as? String == "download" || param.safeValueForKey("d") as? String == "view" {
                PTLoader.shared.show()
                FileSystemDownloadManager.downloadPDF(url: url) { [weak self](path, success) in
                    PTLoader.shared.hide()
                    if success {
                        guard let weakSelf = self else { return }
                        if param.safeValueForKey("d") as? String == "download" {
                            DispatchQueue.main.async {
                                weakSelf.documentInteractionController.url  = path
                                weakSelf.documentInteractionController.uti  = path.typeIdentifier ?? "public.data, public.content"
                                weakSelf.documentInteractionController.name = "Download & Share".localized
                                weakSelf.documentInteractionController.presentPreview(animated: true)
                            }
                        } else if param.safeValueForKey("d") as? String == "view" {

                            DispatchQueue.main.async {
                            let displayVC = weakSelf.storyboard?.instantiateViewController(withIdentifier: "documentShareNavigation")
                            if let nav = displayVC as? UINavigationController {
                                if let viewController = nav.viewControllers.first {
                                    if let shareDoc = viewController as? ShareDocumentViewController {
                                        shareDoc.pathURL = path
                                    }
                                }
                            }
                            if let vc = displayVC {

                                    weakSelf.present(vc, animated: true, completion: nil)
                                }
                            }
                        }
                    }
                }
                return false
            }
        } else {
            PTLoader.shared.hide()
        }
        return true
    }
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        guard let navVC = self.navigationController else {
            return self
        }
        return navVC
    }
    
    func documentInteractionControllerDidDismissOptionsMenu(_ controller: UIDocumentInteractionController) {
        self.dismissScreen()
    }
}

extension URL {
    var params: [String: Any]? {
        if let urlComponents = URLComponents(url: self, resolvingAgainstBaseURL: true) {
            if let queryItems = urlComponents.queryItems {
                var params = [String: Any]()
                queryItems.forEach{
                    params[$0.name] = $0.value
                }
                return params
            }
        }
        return nil
    }
}
