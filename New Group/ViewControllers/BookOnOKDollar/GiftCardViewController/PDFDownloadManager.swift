//
//  PDFDownloadManager.swift
//  OK
//
//  Created by Ashish on 10/31/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class FileSystemDownloadManager {
    
    class func downloadPDF(url: URL, completion: @escaping (_ path: URL, _ success: Bool) -> Void) {
        
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 120
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "GET"

        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if((error) != nil) {
                completion(url, false)
                println_debug(error.debugDescription)
            }else {
                guard let forcedData = data else {
                    completion(url, false)
                    return
                }
                
                guard var docUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last else {
                    completion(url, false)
                    return
                }
                
                docUrl = docUrl.appendingPathComponent(url.lastPathComponent)
                
                do {
                    try forcedData.write(to: docUrl, options: Data.WritingOptions.atomic)
                    
                    completion(docUrl, true)

                } catch {
                    println_debug(error.localizedDescription)
                    completion(docUrl, false)
                }

            }
        }
        dataTask.resume()

    }
    
}
