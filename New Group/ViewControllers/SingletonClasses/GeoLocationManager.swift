
//
//  GeoLocationManager.swift
//  OK
//
//  Created by Uma Rajendran on 11/6/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import GoogleMaps

class GeoLocationManager : NSObject, CLLocationManagerDelegate {
    static let shared = GeoLocationManager()
  
    var currentLocation: CLLocation!
    var locationManager: CLLocationManager!
    var geoCoder: CLGeocoder!
    var placeMark: CLPlacemark!
    var currentLatitude: String! = "0.0"
    var currentLongitude: String! = "0.0"
    var currentDivCode: String! = "YANGDIVI"
    var currentTownshipCode: String! = "KAMARYOU"
    var currentCityName: String! = "City"
    var adress : CurrentAddress?
    var adressB : CurrentAddressB?
    var allLocationsList = [LocationDetail]()
    var allDivisionList = [LocationDetail]()
    var allStateList = [LocationDetail]()
    var selectedDivState: LocationDetail?
    var state : LocationDetail?
    var township : TownShipDetailForAddress?
  
    override init() {
        super.init()
        
        self.setUpLocationManager()
    }
    
    func setUpLocationManager() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() == .notDetermined {
                locationManager.requestAlwaysAuthorization()
                println_debug("Not deternibes")
            } else if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied {
                self.showLocationAlert()
                println_debug("Restricted")
            } else if CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
                startUpdateLocation()
                geteCurrentLocationDetails()
                println_debug("Authorized")
            }
        } else {
            println_debug("Not enabled")
        }
        
    }
    
    func showLocationAlert() {
        alertViewObj.wrapAlert(title: "", body: "Please allow OK$ to use your location to offer location specific deals. It's completely private and secure.".localized, img: nil)
        alertViewObj.addAction(title: "Setting".localized, style: .target , action: {
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    println_debug("Settings opened: \(success)")
                })
            }
        })
        alertViewObj.addAction(title: "Cancel".localized, style: .cancel , action: {
        })
        alertViewObj.showAlert(controller: UIViewController())
    }
    
    func startUpdateLocation() {
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() == .notDetermined {
                locationManager.requestAlwaysAuthorization()
                println_debug("Not deternibes")
            } else if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied {
                self.showLocationAlert()
                println_debug("Restricted")
            } else if CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
                self.locationManager.startUpdatingLocation()
                println_debug("Authorized")
            }
        } else {
            self.showLocationAlert()
        }
    }
    
    func stopUpdateLocation() {
        self.locationManager.stopUpdatingLocation()
    }
  
  func stringBeforeSpace(str : String) -> String{
    let delimiter = " "
    let token = str.components(separatedBy: delimiter)
    return token[0]
  }
  
    func geteCurrentLocationDetails() {
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse || CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways {
            currentLocation = locationManager.location
        }
        //lGeoLocation
        if let lat = currentLocation?.coordinate.latitude, let long = currentLocation?.coordinate.longitude {
            currentLatitude = String(lat)
            currentLongitude = String(long)
            let setLanguage = UserDefaults.standard.value(forKey: "currentLanguage") as! String
            ok_default_language = setLanguage
            
            allLocationsList = TownshipManager.allDivisionList

          self.getAddressFrom(lattitude: currentLatitude, longitude:  currentLongitude,language: ok_default_language) { (isSuccess, dic) in
          
            if let dict = dic as? Dictionary<String,String>{
                if ok_default_language == "my" {
                  self.adressB = CurrentAddressB.init(dic: dict)
                  
                }else {
                  self.adress = CurrentAddress.init(dic: dict)
                  
                  for item in self.allLocationsList {
                    let state_name : LocationDetail = item
                    if self.stringBeforeSpace(str: self.adress!.division) == self.stringBeforeSpace(str: state_name.stateOrDivitionNameEn) {
                      self.state = item
                      break
                    }
                  }
                    if let state = self.state?.townshipArryForAddress {
                        for item in state {
                            let townshipDetails: TownShipDetailForAddress = item
                            if self.adress!.township == townshipDetails.townShipNameEN{
                                self.township = item
                                if ok_default_language == "my" {
                                    self.currentDivCode = self.state?.stateOrDivitionCode
                                    self.currentTownshipCode = self.township?.townShipCode
                                    self.currentCityName = self.adressB?.cityB
                                    break
                                }else {
                                    self.currentDivCode = self.state?.stateOrDivitionCode
                                    self.currentTownshipCode = self.township?.townShipCode
                                    self.currentCityName = self.adress?.city
                                    break
                                }
                            }
                        }
                    }
                }
            }
          }

        }
    }
    
    
    func ReturnCurrentLocationDetails() ->  CLLocation{
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse || CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways {
            currentLocation = locationManager.location
        }
        
        //lGeoLocation
        if let lat = currentLocation?.coordinate.latitude, let long = currentLocation?.coordinate.longitude {
            currentLatitude = String(lat)
            currentLongitude = String(long)
        }
        
        return currentLocation
    }
    
    
    func ReturnDistanceDetails() ->  Bool{
        
        
        self.setUpLocationManager()
        
//        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse || CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways {  currentLocation = locationManager.location  }
        
        let dictValue = UserDefaults.standard.object(forKey: "BussinessDoneCurrentLocation") as? [String: Any]
        let latitude =  dictValue?["lat"]
        let longitude = dictValue?["long"]
        let coordinate:CLLocation = CLLocation(latitude: (latitude as AnyObject).doubleValue ?? 0.0, longitude: (longitude as AnyObject).doubleValue ?? 0.0)
        let distanceInMeters = coordinate.distance(from: currentLocation)
        if(distanceInMeters <= 160){
            return true
        }
        else{
            return false
        }
    }
    
    func getLatLongByName(cityname: String, completionHandler: @escaping (Bool, String?, String?) -> Void) {
        let googleUrlStr = "https://maps.google.com/maps/api/geocode/json?address=\(cityname)&sensor=false&key=\(googleAPIKey)"
        let escapedUrlStr = googleUrlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        if let url = URL.init(string: escapedUrlStr!) {
            getGoogleApiResponse(apiUrl: url, compilationHandler: { (isSuccess, response) in
                guard isSuccess else {
                    if let errorMsg = response {
                        println_debug("error message from google api ::::: \(errorMsg)")
                    }else{
                        println_debug("error :: something went wrong on google api :::::")
                    }
                    completionHandler(false, nil, nil)
                    return
                }
                if let results = response as? Array<Any> {
                    for dict in results {
                        if let dic = dict as? Dictionary<String, Any> {
                            if let geometry = dic["geometry"] as? Dictionary<String, Any> {
                                if let location = geometry["location"] as? Dictionary<String, Any> {
                                    if let locLat = location["lat"] as? Double, let locLong = location["lng"] as? Double {
                                        completionHandler(true, String(locLat), String(locLong) )
                                        return
                                    }
                                }
                            }
                        }
                    }
                }
                completionHandler(false, nil, nil)
            })
        }
    }
    
    func getLocationNameByLatLong(lattitude: String, longitude: String, isForCashinCashout : Bool = false, completionHandler: @escaping (Bool, Any?) -> Void) {
        
        var googleUrlStr = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lattitude),\(longitude)&key=\(googleAPIKey)"
        if appDel.currentLanguage == "my" {
            googleUrlStr = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lattitude),\(longitude)&language=my&key=\(googleAPIKey)"
        }else {
            googleUrlStr = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lattitude),\(longitude)&key=\(googleAPIKey)"
        }
        let url = URL.init(string: googleUrlStr)
        getGoogleApiResponse(apiUrl: url!) { (isSuccess, response) in
            guard isSuccess else {
                if let errorMsg = response {
                    println_debug("error message from google api ::::: \(errorMsg)")
                }else{
                    println_debug("error :: something went wrong on google api :::::")
                }
                completionHandler(false, nil)
                return
            }
            if let results = response as? Array<Any> {
                for dict in results {
                    if let dic = dict as? Dictionary<String, Any> {
                        if isForCashinCashout {
                            completionHandler(true, dic["formatted_address"])
                            return
                        }
                        if let typesArr = dic["types"] as? [String] {
                            let itemExists = typesArr.contains(where: {
                                $0.range(of: "neighborhood", options: .caseInsensitive) != nil
                            })
                            if itemExists == true {
                                completionHandler(true, dic["formatted_address"])
                                return
                                /*
                                 if let geometryDic = dic["geometry"] as? Dictionary<String, Any> {
                                 if let locType = geometryDic["location_type"] as? String {
                                 if locType.lowercased() == "geometric_center" {
                                 completionHandler(true, dic["formatted_address"])
                                 return
                                 }
                                 }
                                 }
                                 */
                            }
                            
                        }
                    }
                }
            }
            completionHandler(false, nil)
        }
    }
    
    //language=hi
    func getAddressFrom(lattitude: String, longitude: String,language : String , completionHandler: @escaping (Bool, Any?) -> Void) {
        
        //   language = "my"
        var googleUrlStr = "https://maps.googleapis.com/maps/api/geocode/json?latlng=16.816790,96.131840&key=\(googleAPIKey)"
        if language == "my" {
            googleUrlStr = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lattitude),\(longitude)&language=my&key=\(googleAPIKey)"
        }else if language == "en"{
            googleUrlStr = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lattitude),\(longitude)&key=\(googleAPIKey)"
        }else {
             googleUrlStr = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lattitude),\(longitude)&language=my&key=\(googleAPIKey)"
        }
        println_debug("GoogleURL : \(googleUrlStr)")
        let url = URL.init(string: googleUrlStr)
        getGoogleApiResponse(apiUrl: url!) { (isSuccess, response) in
            guard isSuccess else {
                if let errorMsg = response {
                    println_debug("error message from google api ::::: \(errorMsg)")
                }else{
                    println_debug("error :: something went wrong on google api :::::")
                }
                completionHandler(false, nil)
                return
            }
            
            var finalDict = Dictionary<String,String>()
            
            if let results = response as? Array<Any> {
                for dict in results {
                    if let dic = dict as? Dictionary<String,Any> {
                        if let arr = dic["address_components"] as? Array<NSDictionary> {
                            for item in arr {
                                if let arrString = item.object(forKey: "types") as? Array<String> {
                                    for element in arrString {
                                        print(element)
                                        if element == "route" {
                                            finalDict["street"] = item.object(forKey: "long_name") as? String
                                        }
                                        
                                        if (finalDict.safeValueForKey("street") ?? "").lowercased() == "Unnamed Road".lowercased(), element.lowercased() == "neighborhood".lowercased() {
                                            finalDict["street"] = item.object(forKey: "long_name") as? String
                                        }

                                        if element == "administrative_area_level_3" {
                                            finalDict["township"] = item.object(forKey: "long_name") as? String
                                        }
                                        
                                        if element == "administrative_area_level_1" {
                                            finalDict["region"] = item.object(forKey: "long_name") as? String
                                        }
                                        
                                        if element == "locality" {
                                            finalDict["city"] = item.object(forKey: "long_name") as? String
                                        }
                                        
                                        if element == "country" {
                                            finalDict["Country"] = item.object(forKey: "short_name") as? String
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                completionHandler(true,finalDict)
            }
            completionHandler(false, nil)
        }
    }
    
    //Get Duration
    
    func getDurationData(sourceLatitude: String, sourceLongitude: String, destinationLatitude: String, destinationLongitude: String, completionHandler: @escaping (Bool, Any?) -> Void) {
        
        let googleUrlStr = "http://maps.googleapis.com/maps/api/directions/json?origin=\(sourceLatitude),\(sourceLongitude)&destination=\(destinationLatitude),\(destinationLongitude)&mode=driving&sensor=false"
        
        println_debug("GoogleURL : \(googleUrlStr)")
        let url = URL.init(string: googleUrlStr)
        getGoogleTimeApiResponse(apiUrl: url!) { (isSuccess, response) in
            guard isSuccess else {
                if let errorMsg = response {
                    println_debug("error message from google api ::::: \(errorMsg)")
                }else{
                    println_debug("error :: something went wrong on google api :::::")
                }
                completionHandler(false, nil)
                return
            }
            
            var strDuration = String()
            
            if let results = response as? Array<Any> {
                for dict in results {
                    if let dic = dict as? Dictionary<String,Any> {
                        if let arr = dic["legs"] as? Array<NSDictionary> {
                            for item in arr {
                                if let durationString = item.object(forKey: "duration") as? Dictionary<String,Any> {
                                    
                                    strDuration = durationString["text"] as? String ?? ""
                                    println_debug(strDuration)
                                }
                            }
                        }
                    }
                }
                
                completionHandler(true,strDuration)
                
            }
            completionHandler(false, nil)
        }
    }
    
    func getGoogleTimeApiResponse(apiUrl: URL , compilationHandler : @escaping  (Bool, Any?) -> Void){
        
        let request = URLRequest.init(url: apiUrl, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60)
        _ = URLSession.shared.configuration
        
        URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            do {
                
                guard data != nil || error == nil else {
                    compilationHandler(false, nil)
                    return }
                let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                // println_debug(json)
                if (json["status"] as? String)?.lowercased() == "ok" {
                    if let resultArr = json["routes"] as? Array<Any> {
                        if resultArr.count > 0 {
                            compilationHandler(true, resultArr)
                        }else {
                            compilationHandler(false, json["error_message"])
                        }
                    }
                    
                }else{
                    
                    compilationHandler(false, json["error_message"])
                }
            }catch {
                compilationHandler(false, nil)
            }
            }.resume()
    }
    
    func getGoogleApiResponse(apiUrl: URL , compilationHandler : @escaping  (Bool, Any?) -> Void){
        
        let request = URLRequest.init(url: apiUrl, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60)
        _ = URLSession.shared.configuration
        
        URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            do {
                
                guard data != nil || error == nil else {
                    compilationHandler(false, nil)
                    return }
                let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                // println_debug(json)
                if (json["status"] as? String)?.lowercased() == "ok" {
                    if let resultArr = json["results"] as? Array<Any> {
                        if resultArr.count > 0 {
                            compilationHandler(true, resultArr)
                        }else {
                            compilationHandler(false, json["error_message"])
                        }
                    }
                    
                }else{
                    
                    compilationHandler(false, json["error_message"])
                }
            }catch {
                compilationHandler(false, nil)
            }
            }.resume()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        println_debug("GeoLocationManager ::: error in get location \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
      
      guard let locValue: CLLocationCoordinate2D = (manager.location?.coordinate) else {
        return
      }
        currentLatitude = String(locValue.latitude)
        currentLongitude = String(locValue.longitude)
    }
    
}


