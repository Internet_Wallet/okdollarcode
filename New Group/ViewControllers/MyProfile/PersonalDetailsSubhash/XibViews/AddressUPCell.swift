//
//  AddressUPCell.swift
//  OK
//
//  Created by Imac on 3/1/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class AddressUPCell: UITableViewCell {
    @IBOutlet weak var lblhouseStatus: UILabel!
    @IBOutlet weak var lblFloorStatus: UILabel!
    @IBOutlet weak var lblRoomStatus: UILabel!
    @IBOutlet weak var tfHouse: UITextField!
    @IBOutlet weak var tfFloor: UITextField!
    @IBOutlet weak var tfRoom: UITextField!
    @IBOutlet weak var lblRequired: UILabel!
    
    override func awakeFromNib() {
        
        lblhouseStatus.font = UIFont(name: appFont, size: 10)
        lblFloorStatus.font = UIFont(name: appFont, size: 10)
        lblRoomStatus.font = UIFont(name: appFont, size: 10)
        
        var font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
            font = UIFont.systemFont(ofSize: 18)
        }
        tfHouse.font = font
        tfFloor.font = font
        tfRoom.font = font
        
        
        if ok_default_language == "my" {
            tfHouse.font = UIFont(name: appFont, size: 18)
            tfFloor.font = UIFont(name: appFont, size: 18)
            tfRoom.font = UIFont(name: appFont, size: 18)
        }else if ok_default_language == "en"{
            tfHouse.font = UIFont.systemFont(ofSize: 18)
            tfFloor.font = UIFont.systemFont(ofSize: 18)
            tfRoom.font = UIFont.systemFont(ofSize: 18)
        }else {
            
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfHouse.attributedPlaceholder = NSAttributedString(string: "House No.", attributes:attributes as [NSAttributedString.Key : Any])
                
                let attributes1 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfFloor.attributedPlaceholder = NSAttributedString(string: "Floor No.", attributes:attributes1 as [NSAttributedString.Key : Any])
                let attributes2 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                   .font : UIFont(name: appFont, size: 18)]
                tfRoom.attributedPlaceholder = NSAttributedString(string: "Room No.", attributes:attributes2 as [NSAttributedString.Key : Any])
                tfHouse.font = UIFont(name: appFont, size: 18)
                tfFloor.font = UIFont(name: appFont, size: 18)
                tfRoom.font = UIFont(name: appFont, size: 18)
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfHouse.attributedPlaceholder = NSAttributedString(string: "House No.", attributes:attributes)
                //tfHouse.font = UIFont.systemFont(ofSize: 18)
                tfHouse.font = UIFont(name: appFont, size: 18)
                
                let attributes1 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfFloor.attributedPlaceholder = NSAttributedString(string: "Floor No.", attributes:attributes1)
                //tfFloor.font = UIFont.systemFont(ofSize: 18)
                tfFloor.font = UIFont(name: appFont, size: 18)
                
                let attributes2 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                   .font : UIFont.systemFont(ofSize: 18)]
                tfRoom.attributedPlaceholder = NSAttributedString(string: "Room No.", attributes:attributes2)
                tfRoom.font = UIFont(name: appFont, size: 18)
            }
            
            
        }

        lblhouseStatus.text = "House No.".localized
        lblFloorStatus.text = "Floor No.".localized
        lblRoomStatus.text = "Room No.".localized
        tfHouse.placeholder = "House No.".localized
        tfFloor.placeholder = "Floor No.".localized
        tfRoom.placeholder = "Room No.".localized
    }
    
    
}
