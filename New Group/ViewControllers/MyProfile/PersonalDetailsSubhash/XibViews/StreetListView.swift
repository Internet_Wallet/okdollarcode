//
//  StreetListView.swift
//  OK
//
//  Created by Imac on 4/1/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import Rabbit_Swift

protocol StreetListViewDelegate {
  func streetNameSelected(street_Name: addressModel)
}

class StreetListView: UIView {
    var delegate: StreetListViewDelegate?
    @IBOutlet weak var tvList: UITableView!
    var arrFilterVillageData : [Dictionary<String,String>] = []
    var isSearch = true
    var addressArray = [addressModel]()
    //This is created by Tushar and this will be used incase we are typing and searching the street so the tableview was hidding little bit in where to screen
    var isComingFromWhereTo = false
    
    //prabu
    let defaults = UserDefaults.standard
    
    override func awakeFromNib() {
        tvList.backgroundColor = UIColor.clear
        tvList.tableFooterView = UIView(frame: .zero)
        tvList.register(UINib(nibName: "StreetCell", bundle: nil), forCellReuseIdentifier: "StreetCell")
    }
    
    
    func getAllAddressDetails(searchTextStr : String, yAsix: CGFloat) {
        var urlString = ""
        if ok_default_language == "my"{
            urlString = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(searchTextStr)&components=country:mm&language=my&key=\(googleAPIKey)"
        } else {
            urlString = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(searchTextStr)&components=country:mm&language=en&key=\(googleAPIKey)"
        }
        let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        let session = URLSession.shared
        if url != nil {
            let task = session.dataTask(with:url!) { (data, response, error) -> Void in
                if let data = data {
                    do {
                        let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                        if let dic = dict as? Dictionary<String,AnyObject> {
                            print("address dic---------\(dic)");
                            if let resultArray = dic["predictions"] as? [Dictionary<String,Any>] {
                                self.addressArray.removeAll()
                                DispatchQueue.main.async {
                                    for dic in resultArray {
                                        print("address dic11---------\(dic)"); self.addressArray.append(addressModel.init(dict: dic))
                                    }
                                    
                                    if self.isComingFromWhereTo{
                                        self.frame = CGRect(x: 10, y: yAsix, width: UIScreen.main.bounds.width - 20, height: 195)
                                        self.tvList.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: 195)
                                    }else{
                                        if self.addressArray.count > 5 {
                                            self.frame = CGRect(x: 10, y: yAsix, width: UIScreen.main.bounds.width - 20, height: 200)
                                            self.tvList.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: 200)
                                        }else {
                                            self.frame = CGRect(x: 10, y: Int(yAsix), width: Int(UIScreen.main.bounds.width - 20), height: Int(self.addressArray.count * 50))
                                            self.tvList.frame = CGRect(x: 0, y: 0, width: Int(self.frame.width), height: Int(self.addressArray.count * 50))
                                        }
                                    }
                                    
                                    if self.addressArray.count > 0 {
                                        DispatchQueue.main.async {
                                            self.tvList.isHidden = false
                                            self.tvList.delegate = self
                                            self.tvList.dataSource = self
                                            self.tvList.reloadData()
                                        }
                                    }else {
                                        self.tvList.isHidden = true
                                    }
                                }
                            }
                        }
                    } catch let error as NSError {
                        println_debug(error.localizedDescription)
                    }
                }else {
                    println_debug("API NOT CALLING")
                }
            }
            task.resume()
        }
    }
}

extension StreetListView: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addressArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : StreetCell = tableView.dequeueReusableCell(withIdentifier: "StreetCell") as! StreetCell
        cell.lblFullAdress.isHidden = false
        cell.lblStreetName.isHidden = false
        cell.lblVillage.isHidden = true
        if let obj = addressArray[safe: indexPath.row] {
            if ok_default_language == "my"{
                cell.lblStreetName.text = Rabbit.uni2zg(obj.shortName ?? "")
                cell.lblFullAdress.text = Rabbit.uni2zg(obj.address ?? "")
//                cell.lblStreetName.text = parabaik.uni(toZawgyi: obj.shortName) ?? ""
//                cell.lblFullAdress.text = parabaik.uni(toZawgyi: obj.address) ?? ""
            }else {
                cell.lblStreetName.text =  obj.shortName ?? ""
                cell.lblFullAdress.text = obj.address ?? ""
            }
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dic = addressArray[safe: indexPath.row]
        var streetName  = ""
        var address = ""
        if ok_default_language == "my"{
            
             streetName = Rabbit.uni2zg(dic?.shortName ?? "")
             address =   Rabbit.uni2zg(dic?.address ?? "")
        }else{
            streetName = dic?.shortName ?? ""
            address = dic?.address ?? "" 
        }
         defaults.set(streetName, forKey: "NBSRecentLocationAddress1")
         defaults.set(address, forKey: "NBSRecentLocationAddress2")
        defaults.set(streetName, forKey: "NBSCurrentLocation")
        
        if let del = delegate {
            var tempModel = addressModel(dict: Dictionary<String, String>())
            tempModel.shortName = streetName
            tempModel.address = address
            locationArr.append(tempModel)
            //del.streetNameSelected(street_Name: streetName)
            del.streetNameSelected(street_Name: tempModel)
        }
    }
}
