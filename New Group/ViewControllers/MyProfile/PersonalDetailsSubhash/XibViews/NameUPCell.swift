//
//  NameUPCell.swift
//  OK
//
//  Created by Imac on 3/1/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class NameUPCell: UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblStaus: UILabel!{
        didSet{
            lblStaus.font = UIFont(name: appFont, size: 10)
        }
    }
    @IBOutlet weak var lblTitle: UILabel!{
        didSet{
            lblTitle.font = UIFont(name: appFont, size: 16)
        }
    }
    @IBOutlet weak var lblRightTitle: UILabel!{
        didSet{
            lblRightTitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblSaperator: UILabel!{
        didSet{
            lblSaperator.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
   override func awakeFromNib() {
    super.awakeFromNib()
    }

    override func layoutSubviews() {
        DispatchQueue.main.async {
            self.needsUpdateConstraints()
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }
}
