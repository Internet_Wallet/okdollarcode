//
//  BusinessNameCellUP.swift
//  OK
//
//  Created by Imac on 4/25/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class BusinessNameCellUP: UITableViewCell {
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: MarqueeLabel!
    @IBOutlet weak var imgRightArrow: UIImageView!
    @IBOutlet weak var lblRequired: UILabel!
     @IBOutlet weak var lblCatImage: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        imgIcon.image = UIImage(named: "categories")
        lblStatus.font = UIFont(name: appFont, size: 10)
        lblTitle.font = UIFont(name: appFont, size: 16)
    }

}
