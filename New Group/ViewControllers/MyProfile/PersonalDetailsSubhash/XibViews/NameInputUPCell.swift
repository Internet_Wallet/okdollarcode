//
//  NameInputUPCell.swift
//  OK
//
//  Created by Imac on 3/1/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class NameInputUPCell: UITableViewCell {
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var tfTitle: UITextField!
    @IBOutlet weak var btnRightArrow: UIButton!
    @IBOutlet weak var lblSaperator: UILabel!
    @IBOutlet weak var lblRequired: UILabel!
    
    override func awakeFromNib() {
        lblStatus.font = UIFont(name: appFont, size: 10)
      //  tfTitle.font = UIFont(name: appFont, size: 18)
        
    }
    
    override func layoutSubviews() {
        DispatchQueue.main.async {
            self.needsUpdateConstraints()
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }
}
