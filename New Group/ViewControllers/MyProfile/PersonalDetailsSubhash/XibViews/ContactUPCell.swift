//
//  ContactUPCell.swift
//  OK
//
//  Created by Imac on 3/7/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class ContactUPCell: UITableViewCell {
    @IBOutlet weak var imgCountry: UIImageView!
    @IBOutlet weak var lblCountrycode: UILabel!
    @IBOutlet weak var tfContactNumber: RestrictedCursorMovement!
    @IBOutlet weak var btnClose: UIButton!{
        didSet{
            self.btnClose.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnContact: UIButton!
    @IBOutlet weak var lblRequired: UILabel!
    @IBOutlet weak var lblStatus: UILabel!{
        didSet{
            lblStatus.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
