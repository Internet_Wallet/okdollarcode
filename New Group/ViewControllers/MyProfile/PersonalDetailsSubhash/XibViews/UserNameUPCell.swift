//
//  UserNameUPCell.swift
//  OK
//
//  Created by Imac on 3/28/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class UserNameUPCell: UITableViewCell {
    @IBOutlet weak var btnMale: UIButton!
     @IBOutlet weak var btnFemale: UIButton!
    
     @IBOutlet weak var btnU: UIButton!
     @IBOutlet weak var btnMg: UIButton!
     @IBOutlet weak var btnMr: UIButton!
     @IBOutlet weak var btnDr: UIButton!
    
     @IBOutlet weak var btnDaw: UIButton!
     @IBOutlet weak var btnMa: UIButton!
     @IBOutlet weak var btnMs: UIButton!
     @IBOutlet weak var btnMrs: UIButton!
     @IBOutlet weak var btnMDr: UIButton!
    
     @IBOutlet weak var btnFU: UIButton!
     @IBOutlet weak var btnFMr: UIButton!
     @IBOutlet weak var btnFDr: UIButton!

    @IBOutlet weak var viewMaleFemale: UIView!
    @IBOutlet weak var viewMaleEn: UIView!
    @IBOutlet weak var viewFemaleEn: UIView!
    @IBOutlet weak var viewMaleFemaleMy: UIView!
    
    @IBOutlet weak var tfUserName: UITextField!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblStutus: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var btnclose: UIButton!
    @IBOutlet weak var lblRequired: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
           // font = UIFont.systemFont(ofSize: 18)
        }
        tfUserName.font = font
        self.lblStutus.font = UIFont(name: appFont, size: 10.0)
        self.btnMale.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        self.btnFemale.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        
        self.btnU.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        self.btnMg.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        self.btnMr.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        self.btnDr.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        
        self.btnDaw.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        self.btnMa.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        self.btnMs.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        self.btnMrs.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        self.btnMDr.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        
        self.btnFU.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        self.btnFMr.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        self.btnFDr.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        
        
        self.lblStutus.text = "User Name".localized
        self.btnMale.setTitle("Male".localized, for: .normal)
        self.btnFemale.setTitle("Female".localized, for: .normal)
        
        self.btnU.setTitle("U".localized, for: .normal)
        self.btnMg.setTitle("Mg".localized, for: .normal)
        self.btnMr.setTitle("Mr".localized, for: .normal)
        self.btnDr.setTitle("Dr".localized, for: .normal)
        
        self.btnDaw.setTitle("Daw".localized, for: .normal)
        self.btnMa.setTitle("Ma".localized, for: .normal)
        self.btnMs.setTitle("Ms".localized, for: .normal)
        self.btnMrs.setTitle("Mrs".localized, for: .normal)
        self.btnMDr.setTitle("Dr".localized, for: .normal)
        
        self.btnFU.setTitle("U".localized, for: .normal)
        self.btnFMr.setTitle("Mr".localized, for: .normal)
        self.btnFDr.setTitle("Dr".localized, for: .normal)
        
        
        self.tfUserName.tag = 75
        
        if ok_default_language == "my" {
          self.tfUserName.font = UIFont(name: appFont, size: 18)
        }else if ok_default_language == "en"{
           self.tfUserName.font = UIFont.systemFont(ofSize: 18)
        }else {
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfUserName.attributedPlaceholder = NSAttributedString(string: "Enter User Name", attributes:attributes as [NSAttributedString.Key : Any])
                tfUserName.font = UIFont(name: appFont, size: 18)
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfUserName.attributedPlaceholder = NSAttributedString(string: "Enter User Name", attributes:attributes)
               // tfUserName.font = UIFont.systemFont(ofSize: 18)
                tfUserName.font = UIFont(name: appFont, size: 18)
            }
        }
        
        self.tfUserName.placeholder = "Enter User Name".localized
        self.tfUserName.autocapitalizationType = .allCharacters
        
        self.viewMaleFemale.isHidden = true
        self.viewMaleEn.isHidden = true
        self.viewFemaleEn.isHidden = true
        self.viewMaleFemaleMy.isHidden = true
    }

}







class FatherNameUpCell: UITableViewCell {
    @IBOutlet weak var viewFatherMy: UIStackView!
    @IBOutlet weak var viewFatherEn: UIView!
    
    @IBOutlet weak var btnU: UIButton!
    @IBOutlet weak var btnMr: UIButton!
    @IBOutlet weak var btnDr: UIButton!
    
    @IBOutlet weak var btnUMy: UIButton!
    @IBOutlet weak var btnMrMy: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tfFatherName: UITextField!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblRequired: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
           // font = UIFont.systemFont(ofSize: 18)
        }
        tfFatherName.font = font
        self.lbltitle.font = UIFont(name: appFont, size: 10)
        
        self.btnUMy.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        self.btnMrMy.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        self.btnU.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        self.btnMr.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        self.btnDr.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        
        self.lbltitle.text = "Father Name".localized
        self.btnUMy.setTitle("U".localized, for: .normal)
        self.btnMrMy.setTitle("Dr".localized, for: .normal)
        self.btnU.setTitle("U".localized, for: .normal)
        self.btnMr.setTitle("Mr".localized, for: .normal)
        self.btnDr.setTitle("Dr".localized, for: .normal)
        self.tfFatherName.tag = 65
        if ok_default_language == "my" {
            self.tfFatherName.font = UIFont(name: appFont, size: 18)
        }else if ok_default_language == "en"{
            self.tfFatherName.font = UIFont.systemFont(ofSize: 18)
        }else {
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfFatherName.attributedPlaceholder = NSAttributedString(string: "Enter Father Name", attributes:attributes as [NSAttributedString.Key : Any])
                tfFatherName.font = UIFont(name: appFont, size: 18)
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfFatherName.attributedPlaceholder = NSAttributedString(string: "Enter Father Name", attributes:attributes)
               //tfFatherName.font = UIFont.systemFont(ofSize: 18)
                tfFatherName.font = UIFont(name: appFont, size: 18)
            }
        }
        
        self.tfFatherName.placeholder = "Enter Father Name".localized
        self.viewFatherMy.isHidden = true
        self.viewFatherEn.isHidden = true
    }

    @IBAction func btnClsoeAction(_ sender: Any) {
    
    }
    
    
}
