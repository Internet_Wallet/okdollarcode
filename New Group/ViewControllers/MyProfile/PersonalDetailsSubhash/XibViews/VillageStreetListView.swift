//
//  VillageStreetListView.swift
//  OK
//
//  Created by Imac on 4/1/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import Rabbit_Swift


protocol VillageStreetListViewDelegate {
    func villageNameSelected(vill_Name: String)
}

class VillageStreetListView: UIView {
    var delegate: VillageStreetListViewDelegate?
    @IBOutlet weak var tvList: UITableView!
    var arrFilterVillageData : [Dictionary<String,String>] = []
    var isSearch = true
    
    
    override func awakeFromNib() {
        tvList.delegate = self
        tvList.dataSource = self
        tvList.backgroundColor = UIColor.clear
        tvList.tableFooterView = UIView(frame: .zero)
    }
    
    func searchString(string: String) {
        let village = VillageManager.shared.allVillageList
        arrFilterVillageData.removeAll()
        if ok_default_language == "my" {
            let predicate : NSPredicate = NSPredicate(format: "BurmeseName CONTAINS[c] %@", string.lowercased())
            arrFilterVillageData = (village.villageArray as NSArray).filtered(using: predicate) as! [Dictionary<String, String>]
        }else if ok_default_language == "en" {
            let predicate : NSPredicate = NSPredicate(format: "self.Name contains[c] %@", string.lowercased())
            arrFilterVillageData = (village.villageArray as NSArray).filtered(using: predicate) as! [Dictionary<String, String>]
        }else {
            let predicate : NSPredicate = NSPredicate(format: "BurmeseNameUnicode CONTAINS[c] %@", string.lowercased())
            arrFilterVillageData = (village.villageArray as NSArray).filtered(using: predicate) as! [Dictionary<String, String>]
        }
        
        if self.arrFilterVillageData.count > 5 {
            self.tvList.frame = CGRect(x: 10, y: 0, width: UIScreen.main.bounds.width - 20, height: 200)
            self.frame = CGRect(x: 10, y: 0, width: UIScreen.main.bounds.width - 20, height: 200)
        }else {
            self.tvList.frame = CGRect(x: 10, y: 0, width: Int(UIScreen.main.bounds.width - 20), height: Int(self.arrFilterVillageData.count * 50))
            self.frame = CGRect(x: 10, y: 0, width: Int(UIScreen.main.bounds.width - 20), height: Int(self.arrFilterVillageData.count * 50))
        }
        
        if arrFilterVillageData.count > 0 {
            tvList.isHidden = false
            isSearch = true
            
            self.tvList.reloadData()
        }else{
            tvList.isHidden = true
            isSearch = false
        }
    }
    
    
}


extension VillageStreetListView: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearch {
            return arrFilterVillageData.count
        }else{
            let village = VillageManager.shared.allVillageList
            return village.villageArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : UITableViewCell!
        cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
      //  if ok_default_language == "my" {
           cell.textLabel?.font =  UIFont(name: appFont, size: 16)
//        }
//        else {
//           cell.textLabel?.font =  UIFont(name: appFont, size: 16)
//        }
        cell.contentView.backgroundColor = UIColor.init(hex: "D2D7D2")
        
        if isSearch {
            let dic = arrFilterVillageData[indexPath.row]
            cell.textLabel?.font =  UIFont(name: appFont, size: 16)
            if ok_default_language == "my" {
                cell.textLabel?.text = dic["BurmeseName"]
            }else if ok_default_language == "en"{
                cell.textLabel?.text = dic["Name"]
            }else {
                cell.textLabel?.text =  dic["BurmeseNameUnicode"]//parabaik.zawgyi(toUni:dic["BurmeseNameUnicode"])
            }
        }else {
            let village = VillageManager.shared.allVillageList
            let dic = village.villageArray[indexPath.row]
            cell.textLabel?.font =  UIFont(name: appFont, size: 16)
            if ok_default_language == "my" {
                cell.textLabel?.text = dic["BurmeseName"]
            }else if ok_default_language == "en"{
                cell.textLabel?.text = dic["Name"]
            }else {
                cell.textLabel?.text =  dic["BurmeseNameUnicode"]//parabaik.zawgyi(toUni:dic["BurmeseNameUnicode"])
            }
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var villageName = ""
        if isSearch {
            let dic = arrFilterVillageData[indexPath.row]
            if ok_default_language == "my" {
                if let vName = dic["BurmeseName"] {
                    villageName = vName
                }
            }else  if ok_default_language == "en" {
                if let vName = dic["Name"] {
                    villageName = vName
                }
            }else {
                if let vName = dic["BurmeseNameUnicode"] {
                    villageName = vName//parabaik.zawgyi(toUni:vName)
                }
            }
            isSearch = false
        }else {
            let street = VillageManager.shared.allVillageList
            let dic = street.villageArray[indexPath.row]
            if ok_default_language == "my" {
                if let vName = dic["BurmeseName"] {
                    villageName = vName
                }
            }else if ok_default_language == "en"{
                if let vName = dic["Name"] {
                    villageName = vName
                }
            }else {
                if let vName = dic["BurmeseNameUnicode"] {
                    villageName = vName//parabaik.zawgyi(toUni:vName)
                }
            }
        }
        if let del = delegate {
            del.villageNameSelected(vill_Name: villageName)
        }
        
    }
    
    
    
}
