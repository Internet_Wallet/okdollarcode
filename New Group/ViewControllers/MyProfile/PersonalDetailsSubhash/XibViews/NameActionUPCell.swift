//
//  NameActionUPCell.swift
//  OK
//
//  Created by Imac on 3/1/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class NameActionUPCell: UITableViewCell {
    @IBOutlet weak var lblStatus: UILabel!{
        didSet{
            lblStatus.font = UIFont(name: appFont, size: 10)
        }
    }
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: MarqueeLabel!{
        didSet{
            lblTitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgRightArrow: UIImageView!
    @IBOutlet weak var lblSaperator: UILabel!
    @IBOutlet weak var lblRequired: UILabel!
    
    
    override func awakeFromNib() {
        lblTitle.isUserInteractionEnabled = true
        lblTitle.text = ""
        lblStatus.font = UIFont(name: appFont, size: 10)
        lblTitle.font = UIFont(name: appFont, size: 16)
    }
    
    override func layoutSubviews() {
        DispatchQueue.main.async {
            self.needsUpdateConstraints()
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }
}
