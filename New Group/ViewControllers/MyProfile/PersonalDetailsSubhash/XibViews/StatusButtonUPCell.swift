//
//  StatusButtonUPCell.swift
//  OK
//
//  Created by Imac on 3/1/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class StatusButtonUPCell: UITableViewCell {
    @IBOutlet weak var btnUpdate: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnUpdate.setTitle("Update".localized, for: .normal)
        btnUpdate.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
    }
}
