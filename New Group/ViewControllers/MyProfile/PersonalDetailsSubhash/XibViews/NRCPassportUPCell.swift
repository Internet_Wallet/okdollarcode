//
//  NRCPassportUPCell.swift
//  OK
//
//  Created by Imac on 3/1/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class NRCPassportUPCell: UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblStatus: UILabel!{
        didSet{
            lblStatus.font = UIFont(name: appFont, size: 10)
        }
    }
    @IBOutlet weak var btnInformation: UIButton!{
        didSet{
            btnInformation.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var imgFront: UIImageView!
    @IBOutlet weak var lblbFront: UILabel!{
        didSet{
            lblStatus.font = UIFont(name: appFont, size: 10)
        }
    }
    @IBOutlet weak var btnFrontAction: UIButton!{
        didSet{
          //  btnInformation.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgRear: UIImageView!
    @IBOutlet weak var lblRear: UILabel!{
        didSet{
            lblRear.font = UIFont(name: appFont, size: 10)
        }
    }
    @IBOutlet weak var btnRearAction: UIButton!{
        didSet{
            btnRearAction.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var tfNrcPassportNumber: MyTextField!{
        didSet{
            tfNrcPassportNumber.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnNCRNumber: UIButton!{
        didSet{
            btnNCRNumber.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnCloseNRC: UIButton!
    {
        didSet{
            btnCloseNRC.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var lblRequired: UILabel!{
        didSet{
            lblRequired.font = UIFont(name: appFont, size: 10)
        }
    }
    @IBOutlet weak var lblRequiredFront: UILabel!{
        didSet{
            lblRequiredFront.font = UIFont(name: appFont, size: 10)
        }
    }
    @IBOutlet weak var lblRequiredBack: UILabel!{
        didSet{
            lblRequiredBack.font = UIFont(name: appFont, size: 10)
        }
    }
    
    override func awakeFromNib() {
//        lblStatus.font = UIFont(name: appFont, size: 10)
//        btnNCRNumber.titleLabel?.font = UIFont(name: appFont, size: 14)
    }
    
    override func layoutSubviews() {
        DispatchQueue.main.async {
            self.needsUpdateConstraints()
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }
}
