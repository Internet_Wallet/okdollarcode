//
//  ProfilePicUPCell.swift
//  OK
//
//  Created by Imac on 3/1/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class ProfilePicUPCell: UITableViewCell {
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!{
        didSet{
            lblName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblAgeMaleStatus: UILabel!{
        didSet{
            lblAgeMaleStatus.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblSaperator: UILabel!{
        didSet{
            lblSaperator.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var imgVerigy: UIImageView!
    @IBOutlet weak var lblRequired: UILabel!{
        didSet{
            lblRequired.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    override func awakeFromNib() {
        imgProfile.layer.cornerRadius = imgProfile.frame.width / 2
        imgProfile?.layer.masksToBounds = true
        imgProfile.layer.borderColor = UIColor.lightGray.cgColor
        imgProfile.layer.borderWidth = 1.0
        imgIcon.isHidden = false
    }
}
