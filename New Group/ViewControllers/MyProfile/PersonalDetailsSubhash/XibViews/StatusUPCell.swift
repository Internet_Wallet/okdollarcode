//
//  StatusUPCell.swift
//  OK
//
//  Created by Imac on 3/1/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class StatusUPCell: UITableViewCell {
    @IBOutlet weak var lblFillStatus: UILabel!
    @IBOutlet weak var lblStatusPercentage: UILabel!
    @IBOutlet weak var progess: UIProgressView!
    @IBOutlet weak var lblSecondLine: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblSecondLine.font = UIFont(name: appFont, size: 11)
    }
}

