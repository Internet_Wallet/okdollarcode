//
//  PersonalProfileModelUP.swift
//  OK
//
//  Created by Imac on 3/8/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class PersonalProfileModelUP: NSObject {

    static var shared = PersonalProfileModelUP()
    
    var ProfileName: String = ""
    var ProfilePicture: String = ""
    var Gender: String = ""
    var IdType: String = ""
    var OkAccNumber: String = ""
    var OkDollarJoiningDate: String = ""
    var Dob: String = ""
    var FatherName: String = ""
    var NrcImage1: String = ""
    var NrcImage2: String = ""
    var NrcNumber: String = ""
    var NrcExpireDate: String = ""
    var CountryOfCitizen: String = ""
    var AccountType: Int = 0
    var ApproveStatus: Int = 0
    var SecurityQuestion: String = ""
    var SecrityAnswer: String = ""
    var AlternatePhonenumber: String = ""
    var Recommended: String = ""
    
    var EmailId: String = ""
    var FacebookId: String = ""
    
    var Division: String = ""
    var Township: String = ""
    var City: String = ""
    var Village: String = ""
    var Street: String = ""
    var HouseNumber: String = ""
    var FloorNumber: String = ""
    var RoomNumber: String = ""
    var HouseBlockNumber: String = ""
    var AddressType: String = ""
    var KycStatus: Int = 0

    func wrapDataModel() -> Data? {

        let addressInfo = AddressInfo1.init(addressType: "Home".localized, city: City, division: Division, floorNumber: FloorNumber, houseBlockNumber: HouseBlockNumber, houseNumber: HouseNumber, roomNumber: RoomNumber, street: Street, township: Township, village: Village)
        
        let loginUserInfo = LoginUserInfo.init(appID: UserModel.shared.appID, limit: 0, mobileNumber: OkAccNumber, msid: msid, offset: 0, ostype: 1, otp: "", simid: uuid)
        let accountType = (AccountType as NSNumber).stringValue
        let keySt = (KycStatus as NSNumber).stringValue
        let personalDetails = PersonalDetails.init(accType: accountType, idType: IdType, gender: Gender, kycStatus: keySt, alternatePhonenumber: AlternatePhonenumber, recommended: Recommended, dob: Dob, emailID: EmailId, facebookID: FacebookId, fatherName: FatherName, registrationStatus: UserModel.shared.registrationStatus ,nrcImage1: NrcImage1, profilePicture: ProfilePicture, nrcImage2: NrcImage2, nrcNumber: NrcNumber, nrcExpireDate: NrcExpireDate, okAccNumber: OkAccNumber, okDollarJoiningDate: OkDollarJoiningDate, profileName: ProfileName, securityQuestion: SecurityQuestion, securityQuestionAnswer: SecrityAnswer, countryOfCitizen: CountryOfCitizen)
        
        let model = UpdateProfileModelUP.init(addressInfo: addressInfo, loginUserInfo: loginUserInfo, personalDetails: personalDetails)
        
        do {
            let data  =  try JSONEncoder().encode(model)
            return data
        } catch {
            println_debug(error)
        }
        return Data.init()
    }

    func clearAllValues() {
        ProfileName = ""
        ProfilePicture = ""
        Gender = ""
        IdType  = ""
        OkAccNumber = ""
        OkDollarJoiningDate = ""
        Dob = ""
        FatherName = ""
        NrcImage1  = ""
        NrcImage2  = ""
        NrcNumber  = ""
        NrcExpireDate = ""
        CountryOfCitizen = ""
        SecurityQuestion  = ""
        SecrityAnswer = ""
        
        EmailId = ""
        FacebookId = ""
        
        Division = ""
        Township = ""
        City = ""
        Village = ""
        Street = ""
        HouseNumber = ""
        FloorNumber = ""
        RoomNumber = ""
        HouseBlockNumber = ""
        AddressType = ""
    }
}





struct UpdateProfileModelUP: Codable {
    let addressInfo: AddressInfo1?
    let loginUserInfo: LoginUserInfo?
    let personalDetails: PersonalDetails?
    
    
    enum CodingKeys: String, CodingKey {
        case addressInfo = "AddressInfo"
        case loginUserInfo = "LoginUserInfo"
        case personalDetails = "PersonalDetails"
    }
}

struct AddressInfo1: Codable {
    let addressType, city, division, floorNumber: String
    let houseBlockNumber, houseNumber, roomNumber, street: String
    let township, village: String
    
    enum CodingKeys: String, CodingKey {
        case addressType = "AddressType"
        case city = "City"
        case division = "Division"
        case floorNumber = "FloorNumber"
        case houseBlockNumber = "HouseBlockNumber"
        case houseNumber = "HouseNumber"
        case roomNumber = "RoomNumber"
        case street = "Street"
        case township = "Township"
        case village = "Village"
    }
}

struct LoginUserInfo: Codable {
    let appID: String
    let limit: Int
    let mobileNumber, msid: String
    let offset, ostype: Int
    let otp, simid: String
    
    enum CodingKeys: String, CodingKey {
        case appID = "AppId"
        case limit = "Limit"
        case mobileNumber = "MobileNumber"
        case msid = "Msid"
        case offset = "Offset"
        case ostype = "Ostype"
        case otp = "Otp"
        case simid = "Simid"
    }
}

struct PersonalDetails: Codable {
    let accType, idType, gender,kycStatus,alternatePhonenumber: String
    let recommended, dob, emailID, facebookID: String
    let fatherName, registrationStatus: String
    let nrcImage1, profilePicture, nrcImage2: String
    let nrcNumber, nrcExpireDate, okAccNumber, okDollarJoiningDate: String
    let profileName, securityQuestion, securityQuestionAnswer, countryOfCitizen: String
    
    enum CodingKeys: String, CodingKey {
        case accType = "AccType"
        case kycStatus = "KycStatus"
        case idType = "IdType"
        case gender = "Gender"
        case alternatePhonenumber = "AlternatePhonenumber"
        case recommended = "Recommended"
        case dob = "Dob"
        case emailID = "EmailId"
        case facebookID = "FacebookId"
        case fatherName = "FatherName"
        case registrationStatus = "RegistrationStatus"
        case nrcImage1 = "NrcImage1"
        case profilePicture = "ProfilePicture"
        case nrcImage2 = "NrcImage2"
        case nrcNumber = "NrcNumber"
        case nrcExpireDate = "NrcExpireDate"
        case okAccNumber = "OkAccNumber"
        case okDollarJoiningDate = "OkDollarJoiningDate"
        case profileName = "ProfileName"
        case securityQuestion = "SecurityQuestion"
        case securityQuestionAnswer = "SecurityQuestionAnswer"
        case countryOfCitizen = "CountryOfCitizen"
    }
}
