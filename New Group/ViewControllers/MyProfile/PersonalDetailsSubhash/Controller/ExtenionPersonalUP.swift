//
//  ExtenionPersonalUP.swift
//  OK
//
//  Created by Imac on 3/7/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

extension PersonalDetailsUP : ContactPickerDelegate {
    func contact(_: ContactPickersPicker, didContactFetchFailed error: NSError){}
    func contact(_: ContactPickersPicker, didCancel error: NSError){}
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker){ decodeContact(contact: contact)}
    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]){}
    func decodeContact(contact: ContactPicker) {
        var countryCode = ""
        var countryFlag = ""
        var tempContryCode = ""
        var tempPhoneNumber = ""
        let phoneNumberCount = contact.phoneNumbers.count
        var phoneNumber = ""
        
        if phoneNumberCount == 1  {
            phoneNumber = "\(contact.phoneNumbers[0].phoneNumber)"
        }
        else if phoneNumberCount > 1 {
            phoneNumber = "\(contact.phoneNumbers[0].phoneNumber)"
        }else {}
        
        if phoneNumber != "" {
            phoneNumber = phoneNumber.replacingOccurrences(of: ")", with: "")
            phoneNumber = phoneNumber.replacingOccurrences(of: "(", with: "")
            phoneNumber = phoneNumber.trimmingCharacters(in: .whitespaces)
        }
        
        let cDetails = identifyCountry(withPhoneNumber: phoneNumber)
        if cDetails.0 == "" || cDetails.1 == "" {
            countryFlag = "myanmar"
            countryCode = "+95"
            tempContryCode = countryCode
        } else {
            countryFlag = cDetails.1
            countryCode = cDetails.0
            tempContryCode = countryCode
        }
        println_debug(tempContryCode)
        let phone = phoneNumber.replacingOccurrences(of: cDetails.0, with: "0")
        if phone.hasPrefix("0") {
            if countryFlag == "myanmar" {
                tempPhoneNumber = phone
            } else {
                tempPhoneNumber = phoneNumber.replacingOccurrences(of: cDetails.0, with: "")
            }
        } else {
            if countryFlag == "myanmar" {
                tempPhoneNumber =   "0" + phone
            } else {
                tempPhoneNumber =  phone
            }
        }
        
        countryCode.remove(at: countryCode.startIndex)
        if tempPhoneNumber.hasPrefix("0"){
            tempPhoneNumber.remove(at: tempPhoneNumber.startIndex)
        }
        let finalMob =  "00" + countryCode + tempPhoneNumber
        let noToCheck = "0" + tempPhoneNumber
        if finalMob.count > 8 {
            if countryCode == "95" {
                let mbLength = (validObj.getNumberRangeValidation(noToCheck))
                if !mbLength.isRejected {
                    if noToCheck.count < mbLength.min || noToCheck.count > mbLength.max {
                        self.clearNumberFromDB()
                        DispatchQueue.main.async {
                        self.showAlert(alertTitle: "", alertBody: "You selected invalid number, Please try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
                        }
                    }else{
                        if self.contactListShownFor == "4" {
                            let  Section = 4
                            let dic = self.AlternetPersonal[0]
                            let countryData = identifyCountryByCode(withPhoneNumber: "+" + countryCode)
                            dic["title"] = finalMob
                            PersonalProfileModelUP.shared.AlternatePhonenumber = finalMob
                            flagCodeAlternate = countryData.countryFlag
                            countryCodeAlternate =  countryData.countryCode
                            dic["icon"] = flagCodeAlternate
                            dic["country_Code"] = countryCodeAlternate
                            self.reloadRows(index: 0, withSeciton: Section)
                        }else if self.contactListShownFor == "5" {
                            let  Section = 5
                            let dic = self.ContactDetails[0]
                            let countryData = identifyCountryByCode(withPhoneNumber: "+" + countryCode)
                            dic["title"] = finalMob
                            PersonalProfileModelUP.shared.Recommended = finalMob
                            flagCodeReferral = countryData.countryFlag
                            countryCodeReferral =  countryData.countryCode
                            dic["icon"] = flagCodeReferral
                            dic["country_Code"] = countryCodeReferral
                            self.reloadRows(index: 0, withSeciton: Section)
                        }
                    }
                }else{
                    self.clearNumberFromDB()
                    DispatchQueue.main.async {
                    self.showAlert(alertTitle: "", alertBody: "You selected invalid number, Please try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
                    }
                }
            }else{
                    if self.contactListShownFor == "4" {
                        let  Section = 4
                        let dic = self.AlternetPersonal[0]
                        let countryData = identifyCountryByCode(withPhoneNumber: "+" + countryCode)
                        dic["title"] = finalMob
                        PersonalProfileModelUP.shared.AlternatePhonenumber = finalMob
                        flagCodeAlternate = countryData.countryFlag
                        countryCodeAlternate =  countryData.countryCode
                        dic["icon"] = flagCodeAlternate
                        dic["country_Code"] = countryCodeAlternate
                        self.reloadRows(index: 0, withSeciton: Section)
                    }else if self.contactListShownFor == "5" {
                        let Section = 5
                        let dic = self.ContactDetails[0]
                        let countryData = identifyCountryByCode(withPhoneNumber: "+" + countryCode)
                        dic["title"] = finalMob
                        PersonalProfileModelUP.shared.Recommended = finalMob
                        flagCodeReferral = countryData.countryFlag
                        countryCodeReferral =  countryData.countryCode
                        dic["icon"] = flagCodeReferral
                        dic["country_Code"] = countryCodeReferral
                        self.reloadRows(index: 0, withSeciton: Section)
                    }
            }
        }else{
            DispatchQueue.main.async {
                self.clearNumberFromDB()
                self.showAlert(alertTitle: "", alertBody: "You selected invalid number, Please try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
            }
        }
    }
    
    func clearNumberFromDB() {
        if self.contactListShownFor == "4" {
            PersonalProfileModelUP.shared.AlternatePhonenumber = ""
        }else if self.contactListShownFor == "5" {
            PersonalProfileModelUP.shared.Recommended = ""
        }
    }
}


extension UIImageView {
    func downloadImageFrom(link:String, contentMode: UIView.ContentMode) {
        URLSession.shared.dataTask( with: NSURL(string:link)! as URL, completionHandler: {
            (data, response, error) -> Void in
            DispatchQueue.main.async {
                self.contentMode =  contentMode
                if let data = data { self.image = UIImage(data: data) }
            }
        }).resume()
    }
}
