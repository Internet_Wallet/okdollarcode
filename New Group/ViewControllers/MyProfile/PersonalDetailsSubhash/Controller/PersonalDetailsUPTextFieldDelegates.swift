//
//  PersonalDetailsUPTextFieldDelegates.swift
//  OK
//
//  Created by Imac on 3/12/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

extension PersonalDetailsUP: UITextFieldDelegate,VillageStreetListViewDelegate,StreetListViewDelegate, MyTextFieldDelegate {
    


    public func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 400 {
            PersonalProfileModelUP.shared.Village = textField.text ?? ""
            let dic = AddressInfo[4]
            dic["title"] = textField.text ?? ""
            self.removeVillageList()
        }else if textField.tag == 500 {
             PersonalProfileModelUP.shared.Street = textField.text ?? ""
            let dic = AddressInfo[5]
            dic["title"] = textField.text ?? ""
            self.removeStreetList()
        }else if textField.tag == 600 {
             let dic = AddressInfo[6]
             dic["house_no"] = textField.text ?? ""
            PersonalProfileModelUP.shared.HouseNumber = textField.text ?? ""
        }else if textField.tag == 700 {
             let dic = AddressInfo[6]
             dic["floor_no"] = textField.text ?? ""
             PersonalProfileModelUP.shared.FloorNumber = textField.text ?? ""
        }else if textField.tag == 800 {
             let dic = AddressInfo[6]
             dic["room_no"] = textField.text ?? ""
             PersonalProfileModelUP.shared.RoomNumber = textField.text ?? ""
        }else if textField.tag == 900 {
             let dic = AddressInfo[7]
             dic["title"] = textField.text ?? ""
            PersonalProfileModelUP.shared.HouseBlockNumber = textField.text ?? ""
        }else if textField.tag == 1000 {
            let dic = securityList[1]
            dic["title"] = textField.text ?? ""
            PersonalProfileModelUP.shared.SecrityAnswer = textField.text ?? ""
            reloadRows(index: 1, withSeciton: 3)
        }else if textField.tag == 1100 {
            let dic = ContactDetails[1]
            dic["title"] = textField.text ?? ""
            let email = textField.text ?? ""
            if email == "" {
            PersonalProfileModelUP.shared.EmailId = ""
            }else if email.isEmail() {
            PersonalProfileModelUP.shared.EmailId = email
            }else {
            showAlert(alertTitle: "", alertBody: "Please enter valid EMail ID".localized, alertImage: UIImage(named: "email")!)
             PersonalProfileModelUP.shared.EmailId = ""
            }
            reloadRows(index: 1, withSeciton: 5)
        }else if textField.tag == 1200 {
            let dic = ContactDetails[2]
            dic["title"] = textField.text ?? ""
            PersonalProfileModelUP.shared.FacebookId = textField.text ?? ""
            reloadRows(index: 2, withSeciton: 5)
        }else if textField.tag == 24 {
            let indexPath = IndexPath(row: 0, section: 4)
            if let cell: ContactUPCell = self.tbUpdateProfile.cellForRow(at: indexPath) as? ContactUPCell {
                var num = cell.tfContactNumber.text ?? ""
                var c_code = countryCodeAlternate
                c_code.removeFirst()
                if flagCodeAlternate == "myanmar" {
                      num.removeFirst()
                    PersonalProfileModelUP.shared.AlternatePhonenumber = "00" + c_code + num
                }else {
                    PersonalProfileModelUP.shared.AlternatePhonenumber = "00" + num
                }
            }
            return
        }else if textField.tag == 25 {
            let indexPath = IndexPath(row: 0, section: 5)
            if let cell: ContactUPCell = self.tbUpdateProfile.cellForRow(at: indexPath) as? ContactUPCell {
                var num = cell.tfContactNumber.text ?? ""
                var c_code = countryCodeReferral
                c_code.removeFirst()
                if flagCodeReferral == "myanmar" {
                    num.removeFirst()
                    PersonalProfileModelUP.shared.Recommended = "00" + c_code + num
                }else {
                    PersonalProfileModelUP.shared.Recommended = "00" + num
                }
            }
            return
        }else if textField.tag == 65 {
//            let cell: FatherNameUpCell?
//            if PersonalProfileModelUP.shared.IdType == "04" {
//                cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 7, section: 1)) as? FatherNameUpCell
//            }else {
//                cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 5, section: 1)) as? FatherNameUpCell
//            }
            let replace = prefixFatherName //cell?.lblTitle.text ?? ""
            let name = textField.text ?? ""
            PersonalProfileModelUP.shared.FatherName = replace + name.trimmingCharacters(in: .whitespacesAndNewlines)
        }else if textField.tag == 75 {
           // let cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 1, section: 0)) as? UserNameUPCell
            let replace = prefixUserName
            let name = textField.text ?? ""
            PersonalProfileModelUP.shared.ProfileName = replace + name.trimmingCharacters(in: .whitespacesAndNewlines)
            textField.resignFirstResponder()
        }else if textField.tag == 69 {
            if PersonalProfileModelUP.shared.IdType == "04" {
                PersonalProfileModelUP.shared.NrcNumber = textField.text!
            }else {
                nrcPostfix = textField.text!
            }
        }else if textField.tag == 72 {
            let indexPath = IndexPath(row: 3, section: 1)
            if let cell = self.tbUpdateProfile.cellForRow(at: indexPath) as? NameInputUPCell {
                if selectedDate == "" {
                    cell.tfTitle.text = PersonalProfileModelUP.shared.Dob
                    selectedDate = PersonalProfileModelUP.shared.Dob
                }else {
                    cell.tfTitle.text = selectedDate
                }
            }
            PersonalProfileModelUP.shared.Dob = selectedDate
        }else if textField.tag == 06 {
            let indexPath = IndexPath(row: 6, section: 1)
            if let cell = self.tbUpdateProfile.cellForRow(at: indexPath) as? NameInputUPCell {
                if expireyDate == "" {
                    cell.tfTitle.text = PersonalProfileModelUP.shared.NrcExpireDate
                }else {
                    cell.tfTitle.text = expireyDate
                }
            }
             PersonalProfileModelUP.shared.NrcExpireDate = expireyDate
        }
//        checkRequiredFields(handler: {(alertMessage, success)in
//            if success {
//               // myProfileStatus = updateProfileStatusCount()
//              //  reloadRows(index: 0, withSeciton: 6)
//            }else {
//                DispatchQueue.main.async {
//                    self.showAlert(alertTitle: "Please Fill All Mandatory Fields".localized, alertBody: alertMessage, alertImage: UIImage(named: "error")!)
//                }
//            }
//        })
        
        myProfileStatus = updateProfileStatusCount()
        reloadRows(index: 0, withSeciton: 6)
    }

    @objc func nrcNumberEdting(_ textField: UITextField) {
        if let cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 4, section: 1)) as? NRCPassportUPCell {
            if textField.text?.count == 0  {
                nrcPostfix = ""
                self.showNRCSuggestion(cell: cell)
            }else if textField.text == "000000" {
               cell.tfNrcPassportNumber.text?.removeLast()
            }else if textField.text!.count == 6 {
                 cell.tfNrcPassportNumber.resignFirstResponder()
            }
        }
    }
    
    func textFieldDidDelete() {
        if PersonalProfileModelUP.shared.IdType == "01" {
            if let cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 4, section: 1)) as? NRCPassportUPCell {
                if cell.tfNrcPassportNumber.text?.count == 0 {
                    self.showNRCSuggestion(cell: cell)
                }
            }
        }
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
//        checkRequiredFields(handler: {(alertMessage, success)in
//            if success {
//               // myProfileStatus = updateProfileStatusCount()
//              //  reloadRows(index: 0, withSeciton: 6)
//            }else {
//                DispatchQueue.main.async {
//                    self.showAlert(alertTitle: "Please Fill All Mandatory Fields".localized, alertBody: alertMessage, alertImage: UIImage(named: "error")!)
//                }
//            }
//        })
      
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.inputAccessoryView = nil
        textField.inputView = nil
        if textField.tag == 65 {
            textField.keyboardType = .alphabet
            let cell: FatherNameUpCell?
            if PersonalProfileModelUP.shared.IdType == "04" {
                cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 7, section: 1)) as? FatherNameUpCell
            }else {
                cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 5, section: 1)) as? FatherNameUpCell
            }
            
            if !isFatherNameKeyboardHidden {
                if ok_default_language == "my" || ok_default_language == "uni" {
                    cell?.viewFatherMy.isHidden = true
                }else {
                    cell?.viewFatherEn.isHidden = true
                }
            }else{
                if ok_default_language == "my" || ok_default_language == "uni" {
                     cell?.viewFatherMy.isHidden = false
                }else {
                    cell?.viewFatherEn.isHidden = false
                }
                return false
            }
        }else if textField.tag == 75 {
            textField.keyboardType = .alphabet
              let cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 1, section: 0)) as? UserNameUPCell
            if !isYourNameKeyboardHidden {
                cell?.viewMaleFemale.isHidden = true
                cell?.imgUser.isHidden = false
            }else{
                cell?.viewMaleFemale.isHidden = false
                cell?.imgUser.isHidden = true
                return false
            }
        }else if textField.tag == 06 || textField.tag == 72 {
            textField.tintColor = UIColor.white
            let screenSize: CGRect = UIScreen.main.bounds
            let viewDOBKeyboard = UIView(frame: CGRect(x: 0, y: 0, width:screenSize.width , height: 40))
            viewDOBKeyboard.backgroundColor = UIColor(red: 244/255, green: 199/255, blue: 22/255, alpha: 1)
            
            let btnCancel  = UIButton(frame: CGRect(x: 10, y: 0, width:100, height: 40))
            btnCancel.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
            btnCancel.setTitle("Cancel".localized, for: UIControl.State.normal)
            btnCancel.addTarget(self, action:#selector(btnCancelDOBAction), for: UIControl.Event.touchUpInside)
            viewDOBKeyboard.addSubview(btnCancel)
            
            let btnDone  = UIButton(frame: CGRect(x: screenSize.width - 110, y: 0, width:100, height: 40))
            btnDone.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
            btnDone.setTitle("Done".localized, for: UIControl.State.normal)
            btnDone.tag = textField.tag
            btnDone.addTarget(self, action:#selector(btnDoneDOBAction(_:)), for: UIControl.Event.touchUpInside)
            viewDOBKeyboard.addSubview(btnDone)
            textField.inputAccessoryView = viewDOBKeyboard
            
            let date = Date()
            let datePicker = UIDatePicker()
            datePicker.tag = textField.tag
            datePicker.datePickerMode = .date
            datePicker.backgroundColor = UIColor.white
            datePicker.calendar = Calendar(identifier: .gregorian)
            if #available(iOS 13.4, *) {
                datePicker.preferredDatePickerStyle = .wheels
            } else {
                // Fallback on earlier versions
            }
            if textField.tag == 06 {
                if PersonalProfileModelUP.shared.NrcExpireDate.count > 0 {
                    datePicker.date = dateF1.date(from: PersonalProfileModelUP.shared.NrcExpireDate)!
                }else {
                    datePicker.date = date
                }
                datePicker.minimumDate = Date(timeInterval: (24 * 60 * 60), since: date)
                let maxDate = Calendar.current.date(byAdding: .year, value: 100, to: Date())
                datePicker.maximumDate = maxDate
                PersonalProfileModelUP.shared.NrcExpireDate = dateF1.string(from: datePicker.date)
            }else {
                let date1 = Calendar.current.date(byAdding: .year, value: -12, to: Date())
                let minDate = Calendar.current.date(byAdding: .year, value: -117, to: Date())
                if PersonalProfileModelUP.shared.Dob.count > 0 {
                    datePicker.date = dateF1.date(from: PersonalProfileModelUP.shared.Dob)!
                }else {
                    datePicker.date = date1 ?? date
                }
                datePicker.minimumDate = minDate
                let maxDate = Calendar.current.date(byAdding: .year, value: -12, to: Date())
                datePicker.maximumDate = maxDate
                PersonalProfileModelUP.shared.Dob = dateF5.string(from: datePicker.date)
            }
            datePicker.addTarget(self, action: #selector(datePickerChanged(_:)), for: .valueChanged)
            textField.inputView = datePicker
            return true
        }else if textField.tag == 69 {
            if PersonalProfileModelUP.shared.IdType == "04"{
                 textField.keyboardType = .default
            }else {
                textField.keyboardType = .numberPad
                if let cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 4, section: 1)) as? NRCPassportUPCell {
                 cell.btnCloseNRC.isHidden = false
                }
            }
            return true
        }else if textField.tag == 400 || textField.tag == 500 || textField.tag == 600 || textField.tag == 700  || textField.tag == 800 || textField.tag == 900 || textField.tag == 1000 {
          textField.keyboardType = .alphabet
  
            return true
        }else if textField.tag == 24 ||  textField.tag == 25{
            textField.keyboardType = .numberPad
            let section = textField.tag - 20
            let indexPath = IndexPath(row: 0, section: section)
            if let cell: ContactUPCell = self.tbUpdateProfile.cellForRow(at: indexPath) as? ContactUPCell {
                if cell.imgCountry.image == UIImage(named: "myanmar") {
                    let newPosition = textField.endOfDocument
                    textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
                }
            }
            return true
        }else if textField.tag == 1100 ||  textField.tag == 1200 {
            textField.keyboardType = .emailAddress
            return true
        }else {
        }

        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if range.location == 0 && string == " " {
            return false
        }
        println_debug(string)
        let chars = textField.text! + string;
        let mbLength = validObj.getNumberRangeValidation(chars)
        
        
        if textField.tag == 65 || textField.tag == 75 {
            //Father Name
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: NAMECHAR).inverted).joined(separator: "")) { return false }
            if string == "" || string == " " {
                if let char = string.cString(using: String.Encoding.utf8) {
                    let isBackSpace = strcmp(char, "\\b")
                    if (isBackSpace == -92) {
                        return true
                    }
                    let _ = TextValidations.trimSuccessiveSpaces(textField: textField, range: range, string: string)
                    return false
                }
            }
            
            return text.count <= 40
        }
        
        if textField.tag == 69 {
            //Passport
            if PersonalProfileModelUP.shared.IdType == "04"{
                if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: PASSPORTCHARSET).inverted).joined(separator: "")) { return false }
                return text.count <= 14
            }else {
              if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "1234567890").inverted).joined(separator: "")) { return false }
                
                if let cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 4, section: 1)) as? NRCPassportUPCell {
                    if text.count == 0 {
                        cell.btnCloseNRC.isHidden  = true
                    }else {
                        cell.btnCloseNRC.isHidden  = false
                    }
                }
                return text.count <= 6
            }
            
        }
        
        
 
        
        if textField.tag == 400 || textField.tag == 500 || textField.tag == 600 || textField.tag == 700 || textField.tag == 800 || textField.tag == 900 || textField.tag == 1000 {
            if ok_default_language.lowercased() == "uni" {
                
            }else {
             if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
            }
      
            if string == "" || string == " " {
                if let char = string.cString(using: String.Encoding.utf8) {
                    let isBackSpace = strcmp(char, "\\b")
                    if (isBackSpace == -92) {
                        return true
                    }
                    let _ = TextValidations.trimSuccessiveSpaces(textField: textField, range: range, string: string)
                    return false
                }
            }
            if textField.tag == 400 || textField.tag == 500  || textField.tag == 1000 || textField.tag == 900 {
                //textField.keyboardType = .alphabet
                return text.count <= 50
            }
            if textField.tag == 600 || textField.tag == 700 || textField.tag == 800  {
                return text.count <= 6
            }
        }
        
        if textField.tag == 1100 || textField.tag == 1200 {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSETEMAIL).inverted).joined(separator: "")) { return false }
        }

        if textField.tag == 24 || textField.tag == 25 {
            let section = textField.tag - 20
            let indexPath = IndexPath(row: 0, section: section)
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "1234567890").inverted).joined(separator: "")) { return false }
           if let cell: ContactUPCell = self.tbUpdateProfile.cellForRow(at: indexPath) as? ContactUPCell {
            if text.count > 3 {
                cell.btnClose.isHidden = false
            }else {
                cell.btnClose.isHidden = true
            }
            if cell.imgCountry.image == UIImage(named: "myanmar") {
                
                if range.location == 0 && range.length > 1 {
                    textField.text = "09"
                    cell.btnClose.isHidden = true
                    return false
                }
                if range.location == 1 {
                    return false
                }
                if mbLength.isRejected {
                    showAlert(alertTitle: "", alertBody: "Rejected Number".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
                    textField.text = "09"
                    cell.btnClose.isHidden = true
                    return false
                }
                if chars.count > mbLength.max {
                    textField.resignFirstResponder()
                    return false
                }
            }else {
                if text.count > 0 {
                    cell.btnClose.isHidden = false
                }else {
                    cell.btnClose.isHidden = true
                }
                if text.count > 13 {
                    return false
                }
            }
            if textField.selectedTextRange?.start != textField.selectedTextRange?.end {
                let endOfDocument = textField.endOfDocument
                textField.selectedTextRange = textField.textRange(from: endOfDocument, to: endOfDocument)
                return false
            }
            }
        }
        if string == "" || string == " " {
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
            let _ = TextValidations.trimSuccessiveSpaces(textField: textField, range: range, string: string)
            return false
            }
        }
        return true
    }
    
//    @objc func userFatherNameEditing(_ textField: UITextField) {
//        if textField.tag == 75 {
//            if let cell = tbUpdateProfile.cellForRow(at: IndexPath(row: 1, section: 0)) as? UserNameUPCell {
//                let title = cell.lbltitle.text ?? ""
//                let name = textField.text ?? ""
//                PersonalProfileModelUP.shared.ProfileName = title + "," + name
//            }
//        }else if textField.tag == 65 {
//            var index = 0
//            if PersonalProfileModelUP.shared.IdType == "01" {
//                index = 5
//            }else {
//                 index = 7
//            }
//            if let cell = tbUpdateProfile.cellForRow(at: IndexPath(row: index, section: 1)) as? FatherNameUpCell {
//                let title = cell.lbltitle.text ?? ""
//                let name = textField.text ?? ""
//                PersonalProfileModelUP.shared.FatherName = title + "," + name
//            }
//        }
//    }
    
    
    @objc func contactNumberEditing(_ textField: UITextField) {
        let mbLength = validObj.getNumberRangeValidation(textField.text!)
      
        if textField.tag == 24 {
          PersonalProfileModelUP.shared.AlternatePhonenumber = ""
            if let cell: ContactUPCell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 0, section: 4)) as? ContactUPCell {
                if cell.imgCountry.image == UIImage(named: "myanmar") {
                    if textField.text?.count == mbLength.max {
                        textField.resignFirstResponder()
                    }
                }else {
                    if textField.text?.count == 13 {
                        textField.resignFirstResponder()
                    }
                }
            }
        }else if textField.tag == 25 {
          PersonalProfileModelUP.shared.Recommended = ""
            if let cell: ContactUPCell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 0, section: 5)) as? ContactUPCell {
                if cell.imgCountry.image == UIImage(named: "myanmar") {
                    if textField.text?.count == mbLength.max {
                        textField.resignFirstResponder()
                    }
                }else {
                    if textField.text?.count == 13 {
                        textField.resignFirstResponder()
                    }
                }
            }
        }
    }

    
    @objc func datePickerChanged(_ sender: UIDatePicker) {
        if sender.tag == 72 { //DOB
            selectedDate = dateF5.string(from: sender.date)
        }else { // ID_Expiry
            expireyDate = dateF5.string(from: sender.date)
        }
    }
    
    @objc func btnCancelDOBAction () {
       self.view.endEditing(true)
    }
    
    @objc func btnDoneDOBAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if sender.tag == 72 {
            let indexPath = IndexPath(row: 3, section: 1)
            if let cell = self.tbUpdateProfile.cellForRow(at: indexPath) as? NameInputUPCell {
                if selectedDate == "" {
                 cell.tfTitle.text = PersonalProfileModelUP.shared.Dob
                }else {
                    cell.tfTitle.text = selectedDate
                }
            }
            PersonalProfileModelUP.shared.Dob = selectedDate
        }else if sender.tag == 06 {
            let dic = personalInfo[6]
            let indexPath = IndexPath(row: 6, section: 1)
            if let cell = self.tbUpdateProfile.cellForRow(at: indexPath) as? NameInputUPCell {
                if expireyDate == "" {
                    cell.tfTitle.text = PersonalProfileModelUP.shared.NrcExpireDate
                }else {
                    cell.tfTitle.text = expireyDate
                }
            }
            PersonalProfileModelUP.shared.NrcExpireDate = expireyDate
            dic["title"] = PersonalProfileModelUP.shared.NrcExpireDate
        }
    }
    
    @objc func villageTextFieldEditing(_ textField: UITextField) {
        if textField.tag == 400 {
            if textField.text?.count ?? 0 > 0 {
                if self.view.contains(tvVillageList) {
                }else {
                    self.setUpVillageList()
                    tvVillageList.tvList.isHidden = false
                }
                tvVillageList.searchString(string: textField.text ?? "")
            }else {
                if self.view.contains(tvVillageList) {
                         self.removeVillageList()
                }
            }
        }
    }
    
    @objc func streetTextFieldEditing(_ textField: UITextField) {
        if textField.tag == 500 {
            if textField.text?.count ?? 0 > 0 {
                if self.view.contains(tvStreetList) {
                }else {
                    self.setUpStreetList()
                    tvStreetList.tvList.isHidden = false
                }
                tvStreetList.getAllAddressDetails(searchTextStr: textField.text ?? "", yAsix: 0)
            }else {
                if self.view.contains(tvStreetList) {
                     self.removeStreetList()
                }
            }
        }
    }
    
    func villageNameSelected(vill_Name: String) {
         let indexPath = IndexPath(row: 4, section: 2)
        if let cell = self.tbUpdateProfile.cellForRow(at: indexPath) as? NameInputUPCell {
            cell.tfTitle.text = vill_Name
            cell.tfTitle.resignFirstResponder()
            PersonalProfileModelUP.shared.Village = vill_Name
            self.removeVillageList()
        }
    }
    
 func streetNameSelected(street_Name: addressModel) {
    let indexPath = IndexPath(row: 5, section: 2)
    if let cell = self.tbUpdateProfile.cellForRow(at: indexPath) as? NameInputUPCell {
        cell.tfTitle.text = street_Name.shortName
        cell.tfTitle.resignFirstResponder()
        PersonalProfileModelUP.shared.Street = street_Name.shortName ?? ""
        self.removeStreetList()
    }
    }
    
 
}
