//
//  CameraVCUP.swift
//  OK
//
//  Created by Imac on 4/6/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import AVFoundation
import Vision
import CoreMedia

protocol  CameraVCUPDelegate {
    func updateProflePic(imageUrl: String, realImage : UIImage)
}

class CameraVCUP: OKBaseController {
    var delegate: CameraVCUPDelegate?
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var previewView: PreviewView!
    @IBOutlet weak var viewCameraContainer: UIView!
    @IBOutlet weak var btncamera: UIButton!
    
    private weak var faceView: FaceView?
    private var cameraIsReadyToUse = false
    private let session = AVCaptureSession()
    private lazy var cameraPosition = AVCaptureDevice.Position.front
    private weak var previewLayer: AVCaptureVideoPreviewLayer?
    private lazy var sequenceHandler = VNSequenceRequestHandler()
    private lazy var dataOutputQueue = DispatchQueue(label: "FaceDetectionService",
                                                     qos: .userInitiated, attributes: [],
                                                     autoreleaseFrequency: .workItem)
    private var preparingCompletionHandler: ((Bool) -> Void)?
    
    var eyeBlinkCount = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        previewView.isHidden = false
        imgView.isHidden = true
        viewCameraContainer.layer.borderColor = #colorLiteral(red: 0.9766252637, green: 0.8063201308, blue: 0, alpha: 1)
        viewCameraContainer.layer.borderWidth = 1
        viewCameraContainer.layer.cornerRadius =   viewCameraContainer.frame.width / 2
        viewCameraContainer.layer.masksToBounds = true
        btncamera.layer.cornerRadius =   btncamera.frame.width / 2
        btncamera.layer.masksToBounds = true
        setMarqueLabelInNavigation()
        viewCameraContainer.isHidden = true
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: "", body: "Please take a clear selfie. We will use this photo for OK$ account security. Please Blink Your eyes at least two time.".localized, img: UIImage(named: "tbnotif_eye_blue"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                self.eyeBlinkCount = 0
                self.viewCameraContainer.isHidden = false
            }
            alertViewObj.showAlert(controller: self)
        }
        
        self.prepare(previewView: previewView, cameraPosition: .front) { [weak self] _ in
            self?.start()
        }
    }
    
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: 16)
        lblMarque.text = "Take Your Selfie".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
    }
    
    @IBAction func onClickBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
    }
    
    private func showPhotoAlert() {
        alertViewObj.wrapAlert(title:"", body:"Please Allow Camera Access".localized, img: UIImage(named: "alert-icon"))
        alertViewObj.addAction(title: "Setting".localized, style: .target , action: {
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionaryUpload([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
            }
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    @IBAction func onClickCameraAction(_ sender: Any) {
        imgView.isHidden = false
        previewView.isHidden = true
        self.stop()
        if let img =  self.imgView?.image {
            self.detectImage(resultImage: img)
        }
    }
    
    
    func prepare(previewView: UIView,
                 cameraPosition: AVCaptureDevice.Position,
                 completion: ((Bool) -> Void)?) {
        self.previewView = (previewView as! PreviewView)
        self.preparingCompletionHandler = completion
        self.cameraPosition = cameraPosition
        checkCameraAccess { allowed in
            if allowed { self.setup() }
            completion?(allowed)
            self.preparingCompletionHandler = nil
        }
    }
    
    private func setup() {
        guard let bounds = previewView?.bounds else { return }
        let faceView = FaceView(frame: bounds)
        previewView?.addSubview(faceView)
        faceView.backgroundColor = .clear
        self.faceView = faceView
        configureCaptureSession()
    }
    func start() {
        if cameraIsReadyToUse {
            session.startRunning()
        }
    }
    func stop() {
        session.stopRunning()
    }
    
    
    private func askUserForCameraPermission(_ completion:  ((Bool) -> Void)?) {
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { (allowedAccess) -> Void in
            DispatchQueue.main.async { completion?(allowedAccess) }
        }
    }
    
    private func checkCameraAccess(completion: ((Bool) -> Void)?) {
        askUserForCameraPermission { [weak self] allowed in
            guard let self = self, let completion = completion else { return }
            self.cameraIsReadyToUse = allowed
            if allowed {
                completion(true)
            } else {
                //camera permissiong
                self.showPhotoAlert()
                return
            }
        }
    }
    
    private func configureCaptureSession() {
        guard let previewView = previewView else { return }
        // Define the capture device we want to use
        
        guard let camera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: cameraPosition) else {
            self.showPhotoAlert()
            return
        }
        
        // Connect the camera to the capture session input
        do {
            
            try camera.lockForConfiguration()
            defer { camera.unlockForConfiguration() }
            
            if camera.isFocusModeSupported(.continuousAutoFocus) {
                camera.focusMode = .continuousAutoFocus
            }
            
            if camera.isExposureModeSupported(.continuousAutoExposure) {
                camera.exposureMode = .continuousAutoExposure
            }
            
            let cameraInput = try AVCaptureDeviceInput(device: camera)
            session.addInput(cameraInput)
            
        } catch {
            self.showPhotoAlert()
            return
        }
        
        // Create the video data output
        let videoOutput = AVCaptureVideoDataOutput()
        videoOutput.setSampleBufferDelegate(self, queue: dataOutputQueue)
        videoOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String: kCVPixelFormatType_32BGRA]
        
        // Add the video output to the capture session
        session.addOutput(videoOutput)
        
        let videoConnection = videoOutput.connection(with: .video)
        videoConnection?.videoOrientation = .portrait
        
        // Configure the preview layer
        let previewLayer = AVCaptureVideoPreviewLayer(session: session)
        previewLayer.videoGravity = .resizeAspectFill
        previewLayer.frame = previewView.bounds
        previewView.layer.insertSublayer(previewLayer, at: 0)
        self.previewLayer = previewLayer
    }
    
    
    func imageUploadOnServer(imageFile: UIImage) {
        showProgressView()
        var mobilNo = UserModel.shared.mobileNo
        if mobilNo == "" {
            mobilNo = RegistrationModel.shared.MobileNumber
        }
        
        DispatchQueue.main.async {
            let  convertedStr = imageFile.base64(format: ImageFormat.jpeg(0.1))
            VerifyUPModel.share.ProfilePic = convertedStr ?? ""
            let paramString : [String:Any] = [
                "MobileNumber":UserModel.shared.mobileNo,
                "Base64String":convertedStr!
            ]
            let web      = WebApiClass()
            web.delegate = self
            // let urlStr   = Url.businessDetailsImageURL
            let ur = URL(string: "https://www.okdollar.co/RestService.svc/GetImageUrlByBase64String")//getUrl(urlStr: urlStr, serverType: .imageUpload)
            println_debug(ur)
            web.genericClass(url: ur!, param: paramString as AnyObject, httpMethod: "POST", mScreen: "mImageUpload")
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
}

extension CameraVCUP: AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }
        let detectFaceRequest = VNDetectFaceLandmarksRequest(completionHandler: detectedFace)
        do {
            try sequenceHandler.perform(
                [detectFaceRequest],
                on: imageBuffer,
                orientation: .leftMirrored)
            if let image = UIImage(sampleBuffer: sampleBuffer) {
                self.detectEyeBlink(resultImage: image)
            }
        }catch {
            DispatchQueue.main.async {
                self.showAlert(alertTitle: "Camera Error", alertBody: "", alertImage: #imageLiteral(resourceName: "info_one"))
            }
        }
    }
    
    private func detectedFace(request: VNRequest, error: Error?) {
        guard   let previewLayer = previewLayer,
            let results = request.results as? [VNFaceObservation],
            let result = results.first
            else { faceView?.clearAndSetNeedsDisplay(); return }
        faceView?.read(result: result, previewLayer: previewLayer)
    }
}

extension CameraVCUP {
    
    
    func detectEyeBlink(resultImage : UIImage) {
        DispatchQueue.main.async {
            self.imgView.image = nil
             self.imgView.image = UIImage(cgImage: resultImage.cgImage! ,
                       scale: 1.0 ,
                       orientation: UIImage.Orientation.up)
        }
        var exifOrientation: Int = 0
        switch resultImage.imageOrientation {
        case .up:
            exifOrientation = 1
        case .down:
            exifOrientation = 3
        case .left:
            exifOrientation = 8
        case .right:
            exifOrientation = 6
        case .upMirrored:
            exifOrientation = 2
        case .downMirrored:
            exifOrientation = 4
        case .leftMirrored:
            exifOrientation = 5
        case .rightMirrored:
            exifOrientation = 7
        default:
            break
        }
        let detectorOptions = [
            CIDetectorAccuracy: CIDetectorAccuracyHigh
        ]
        let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: detectorOptions)
        
        var features: [CIFeature]? = nil
        
        if let CGImage = resultImage.cgImage {
            features = faceDetector?.features(in: CIImage(cgImage: CGImage), options: [
                CIDetectorSmile: NSNumber(value: true),
                CIDetectorEyeBlink: NSNumber(value: true),
                CIDetectorImageOrientation: NSNumber(value: exifOrientation)
            ])
        }
        
        if let faces = features {
            if faces.count > 1 {
                self.eyeBlinkCount = 0
            } else {
                if let face = faces.first as? CIFaceFeature {
                    let hasLeftEyeBlink = face.leftEyeClosed ? "Yes" : "No"
                    let hasRightEyeBlink = face.rightEyeClosed ? "Yes" : "No"
                    if hasLeftEyeBlink == "Yes" || hasRightEyeBlink == "Yes" {
                        self.eyeBlinkCount += 1
                        print("Eye Blinked : \(eyeBlinkCount)")
                    }
                }else {
                    self.eyeBlinkCount = 0
                }
            }
        }
    }
}

// Camera Settings & Orientation
extension CameraVCUP {
    
    func detectImage(resultImage: UIImage) {
        self.stop()
        self.imgView?.image = resultImage
        AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        let imageOptions =  NSDictionary(object: NSNumber(value: 1) as NSNumber, forKey: CIDetectorImageOrientation as NSString)
        let personciImage = CIImage.init(cgImage: resultImage.cgImage!)
        let accuracy = [CIDetectorAccuracy: CIDetectorAccuracyHigh, CIDetectorEyeBlink: true, CIDetectorTypeFace: true] as [String : Any]
        let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: accuracy)
        let faces = faceDetector?.features(in: personciImage, options: imageOptions as? [String : AnyObject])
        if let faces = faces {
            if faces.count > 1 {
                DispatchQueue.main.async {
                   // let alertViewObj1     = AlertView()
                    alertViewObj.wrapAlert(title: "", body: "More Than One face was detected. Please try again".localized, img: #imageLiteral(resourceName: "info_one"))
                    alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                        self.resetProfilePic()
                    }
                    alertViewObj.showAlert(controller: self)
                }
            } else {
                if let face = faces.first as? CIFaceFeature {
                    print(face)
                    if eyeBlinkCount > 1  {
                        self.eyeBlinkCount = 0
                        DispatchQueue.main.async {
                            self.stop()
                            self.imageUploadOnServer(imageFile: resultImage)
                        }
                    }else {
                        DispatchQueue.main.async {
                            alertViewObj.wrapAlert(title: "", body: "Please take real time photo and Blink eyes at least two time".localized, img: #imageLiteral(resourceName: "info_one"))
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                self.resetProfilePic()
                            }
                            alertViewObj.showAlert(controller: self)
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        alertViewObj.wrapAlert(title: "", body: "No face was detected. Please try again".localized, img: #imageLiteral(resourceName: "info_one"))
                        alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                            self.resetProfilePic()
                        }
                        alertViewObj.showAlert(controller: self)
                    }
                }
            }
        }
    }
    private func resetProfilePic() {
        self.eyeBlinkCount = 0
        imgView.isHidden = true
        previewView.isHidden = false
        self.start()
    }
    
}


extension CameraVCUP: WebServiceResponseDelegate {
    func webResponse(withJson json: AnyObject, screen: String) {
        self.removeProgressView()
        do {
            if let data = json as? Data {
                let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let dic = dict as? Dictionary<String,AnyObject> {
                    println_debug(dic)
                    if dic["Code"] as? NSNumber == 200 {
                        let url = dic["Data"] as! String
                        DispatchQueue.main.async {
                            alertViewObj.wrapAlert(title: nil, body: "Image uploaded successfully".localized, img: #imageLiteral(resourceName: "info_one"))
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                if let del = self.delegate, url != "" {
                                    if let img = self.imgView.image {
                                        del.updateProflePic(imageUrl: url.replacingOccurrences(of: " ", with: "%20"), realImage: img)
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                            }
                            alertViewObj.showAlert(controller: self)
                        }
                    }else {
                        DispatchQueue.main.async {
                            alertViewObj.wrapAlert(title: nil, body: "Image uploading failed. Please try again".localized, img: #imageLiteral(resourceName: "info_one"))
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertViewObj.showAlert(controller: self)
                        }
                    }
                }
            }else {
                DispatchQueue.main.async {
                    alertViewObj.wrapAlert(title: nil, body: "Image uploading failed. Please try again".localized, img: #imageLiteral(resourceName: "info_one"))
                    alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                        self.dismiss(animated: true, completion: nil)
                    }
                    alertViewObj.showAlert(controller: self)
                }
            }
        } catch {}
    }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionaryUpload(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
