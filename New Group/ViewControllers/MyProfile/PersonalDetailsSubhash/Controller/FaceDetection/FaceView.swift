//
//  FaceView.swift
//  HomeWork
//
//  Created by Sam on 28/05/2020.
//  Copyright © 2020 Sam. All rights reserved.
//

import UIKit

import UIKit
import Vision
import AVFoundation
import CoreGraphics

struct FaceElement {
    let points: [CGPoint]
    let needToClosePath: Bool

    func draw(in context: CGContext) {
        if points.isEmpty { return }
        context.addLines(between: points)
        if needToClosePath { context.closePath() }
        context.strokePath()
    }
}

class FaceView: UIView {
    private var faceElements = [FaceElement]()
    private var boundingBox = CGRect.zero

    func clearAndSetNeedsDisplay() {
        faceElements = []
        boundingBox = .zero
        DispatchQueue.main.async { [weak self] in self?.setNeedsDisplay() }
    }

    private func drawElement(context: CGContext, points: [CGPoint], needToClosePath: Bool) {
        if !points.isEmpty {
            context.addLines(between: points)
            if needToClosePath { context.closePath() }
            context.strokePath()
        }
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        guard let context = UIGraphicsGetCurrentContext() else { return }
        context.saveGState()
        defer { context.restoreGState()}

        context.addRect(boundingBox)
        UIColor.yellow.setStroke()
        context.strokePath()

        UIColor.clear.setStroke()
        faceElements.forEach { $0.draw(in: context) }
    }

    func read(result: VNFaceObservation, previewLayer: AVCaptureVideoPreviewLayer) {
        defer { DispatchQueue.main.async { [weak self] in self?.setNeedsDisplay() } }

        let rect = result.boundingBox
        let origin = previewLayer.layerPointConverted(fromCaptureDevicePoint: rect.origin)
        let size = previewLayer.layerPointConverted(fromCaptureDevicePoint: rect.size.cgPoint).cgSize
        boundingBox = CGRect(origin: origin, size: size)

        func addFaceElement(from landmark: VNFaceLandmarkRegion2D?, needToClosePath: Bool) {
            guard let normalizedPoints = landmark?.normalizedPoints else { return }
            let points = normalizedPoints.compactMap { point -> CGPoint in
                let absolute = point.absolutePoint(in: result.boundingBox)
                let converted = previewLayer.layerPointConverted(fromCaptureDevicePoint: absolute)
                return converted
            }
            faceElements.append(FaceElement(points: points, needToClosePath: needToClosePath))
        }

        guard let landmarks = result.landmarks else { return }
        faceElements = []
        addFaceElement(from: landmarks.leftEye, needToClosePath: true)
        addFaceElement(from: landmarks.rightEye, needToClosePath: true)
        addFaceElement(from: landmarks.leftEyebrow, needToClosePath: false)
        addFaceElement(from: landmarks.rightEyebrow, needToClosePath: false)
        addFaceElement(from: landmarks.nose, needToClosePath: false)
        addFaceElement(from: landmarks.outerLips, needToClosePath: true)
        addFaceElement(from: landmarks.innerLips, needToClosePath: true)
        addFaceElement(from: landmarks.faceContour, needToClosePath: false)
    }
}


func + (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x + right.x, y: left.y + right.y)
}

extension CGSize {
    var cgPoint: CGPoint { return CGPoint(x: width, y: height) }
}

extension CGPoint {
    var cgSize: CGSize { return CGSize(width: x, height: y) }

    func absolutePoint(in rect: CGRect) -> CGPoint {
        return CGPoint(x: x * rect.size.width, y: y * rect.size.height) + rect.origin
    }
}
