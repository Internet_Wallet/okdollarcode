//
//  UploadImagesUP.swift
//  OK
//
//  Created by Imac on 3/19/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit


protocol UploadImagesUPDelegate {
    func sendImagesToMainController(images: [String])
}

class UploadImagesUP: OKBaseController {
    @IBOutlet weak var btnUpload: UIButton! {
        didSet {
            btnUpload.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
           btnUpload.setTitle("Update".localized, for: .normal)
        }
    }
    
    var delegate: UploadImagesUPDelegate?
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var tbImages: UITableView!
    let imagePicker = UIImagePickerController()
    var imageList: [UIImage]?
    var selectedIndex = 0
    var optionRowHeight = 0
    var buttonName: [String]?
    var buttonTitleName: [String]?
    var checkImageHave = false
    var imagesURL: [String]?
    var imagesChanged = false
    override func viewDidLoad() {
        super.viewDidLoad()
         removeProgressView()
        if imageList?.count == 0 {
            imageList?.append(UIImage(named: "UploadImagebg")!)
        }else if imageList?.count == 1  {
            imageList?.append(UIImage(named: "UploadImagebg")!)
        }
        
        stackView.isHidden = true
    }
    
     func setMarqueLabelInNavigation(title: String) {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: appFontSize)
        lblMarque.text = title
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
    }
  
    @objc func onClickImageAction(_ sender: UIButton) {
        selectedIndex = sender.tag
        optionRowHeight = 200
        self.stackView.isHidden = true
        self.reloadSection(index: 1)
        self.scrollToRow(indexRow: 0, indexSection: 1)
    }
    
    func reloadSection(index: Int) {
       // UIView.setAnimationsEnabled(false)
        let indexSet: IndexSet = [index]
        self.tbImages.beginUpdates()
        self.tbImages.reloadSections(indexSet, with: .automatic)
        self.tbImages.endUpdates()
       // UIView.setAnimationsEnabled(true)
    }
    
    func reloadRows(index: Int, withSeciton sec: Int) {
        UIView.setAnimationsEnabled(false)
        self.tbImages.beginUpdates()
        let indexPosition = IndexPath(row: index, section: sec)
        self.tbImages.reloadRows(at: [indexPosition], with: .none)
        self.tbImages.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
    @IBAction func onClickBackAction(_ sender: Any) {
        if imagesChanged {
            if let del = delegate {
                del.sendImagesToMainController(images: imagesURL ?? [""])
            }
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc func onClickCamera(_ sender: UIButton) {
        DispatchQueue.main.async {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                 let imagePicker = UIImagePickerController()
                imagePicker.allowsEditing = false
                imagePicker.delegate = self
                imagePicker.title = "Camera".localized
                imagePicker.sourceType = UIImagePickerController.SourceType.camera
                imagePicker.cameraCaptureMode = .photo
                imagePicker.modalPresentationStyle = .fullScreen
                self.present(imagePicker,animated: true,completion: nil)
            }
        }
 
    }
    
    @objc func onClickGallery(_ sender: UIButton) {
        DispatchQueue.main.async {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
        imagePicker.title = "Gallery".localized
        imagePicker.sourceType = .photoLibrary
        imagePicker.modalPresentationStyle = .popover
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    @objc func onClickCancel(_ sender: UIButton) {
        if checkImageHave {
             self.stackView.isHidden = false
        }else {
             self.stackView.isHidden = true
        }
      
        optionRowHeight = 0
         self.reloadSection(index: 1)
        scrollToRow(indexRow: 0, indexSection: 0)
        
    }
    func scrollToRow(indexRow: Int, indexSection: Int) {
        let indexPath = IndexPath(row: indexRow, section: indexSection)
        self.tbImages.scrollToRow(at: indexPath, at: .top, animated: true)
    }
}





extension UploadImagesUP: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return imageList?.count ?? 0
        }else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ImageUpCell") as! ImageUpCell
        cell.selectionStyle = .none
        cell.btnTakeImage.tag = indexPath.row
        cell.btnTakeImage.addTarget(self, action: #selector(onClickImageAction(_:)) , for: .touchUpInside)
        cell.lblTop.text = buttonTitleName?[indexPath.row] ?? ""
        if let img = imageList?[indexPath.row], imageList?[indexPath.row] != UIImage(named: "UploadImagebg") {
            cell.imgActual.image = img
            cell.imgActual.layer.cornerRadius = 15
            cell.imgActual.layer.masksToBounds = true
            cell.btnTakeImage.setTitle("", for: .normal)
        }else {
            cell.imgActual.image = nil
            cell.btnTakeImage.setTitle(buttonName?[indexPath.row] ?? "", for: .normal)
        }
        cell.imgBg.image = UIImage(named: "UploadImagebg")
        return cell
        }else {
           let cell = tableView.dequeueReusableCell(withIdentifier: "ImageOptionUPCell") as! ImageOptionUPCell
            if optionRowHeight == 0 {
                cell.isHidden = true
            }else {
              cell.isHidden = false
            }
            cell.selectionStyle = .none
            cell.btnCamera.addTarget(self, action: #selector(onClickCamera(_:)), for: .touchUpInside)
            cell.btnGallery.addTarget(self, action: #selector(onClickGallery(_:)), for: .touchUpInside)
            cell.btnCancel.addTarget(self, action: #selector(onClickCancel(_:)), for: .touchUpInside)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
                return 250
        }else {
            if optionRowHeight == 0 {
                 return 0
            }else {
                return 200
            }
        }
    }
    
}

extension UploadImagesUP: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let info = convertFromUIImagePickerControllerInfoKeyDictionary1(info)
        if let image = info[convertFromUIImagePickerControllerInfoKey1(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            if selectedIndex == 0 {
                self.imageList?[0] = image
                 imageUploadOnServer(imageFile: image)
            }else if selectedIndex == 1 {
                self.imageList?[1] = image
                imageUploadOnServer(imageFile: image)
            }
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[convertFromUIImagePickerControllerInfoKey1(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            if selectedIndex == 0 {
                self.imageList?[0] = pickedImage
                 imageUploadOnServer(imageFile: pickedImage)
            }else if selectedIndex == 1 {
                self.imageList?[1] = pickedImage
                 imageUploadOnServer(imageFile: pickedImage)
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

}

extension UploadImagesUP: WebServiceResponseDelegate {
    func webResponse(withJson json: AnyObject, screen: String) {
        self.removeProgressView()
        do {
            if let data = json as? Data {
                let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let dic = dict as? Dictionary<String,AnyObject> {
                    println_debug(dic)
                    if dic["Code"] as? NSNumber == 200 {
                        let url = dic["Data"] as! String
                        self.imagesURL?[self.selectedIndex] = url
                        DispatchQueue.main.async {
                            alertViewObj.wrapAlert(title: nil, body: "Image uploaded successfully".localized, img: #imageLiteral(resourceName: "info_one"))
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                self.dismiss(animated: true, completion: nil)
                                self.reloadRows(index: self.selectedIndex, withSeciton: 0)
                                self.stackView.isHidden = false
                                self.checkImageHave = true
                                self.optionRowHeight = 0
                                self.reloadSection(index: 1)
                                self.imagesChanged = true
                                if self.imageList?.count == 1 {
                                    self.imageList?.append(UIImage(named: "UploadImagebg")!)
                                    self.reloadSection(index: 0)
                                }
                                
                            }
                            alertViewObj.showAlert(controller: self)
                        }
                    }else {
                        DispatchQueue.main.async {
                            alertViewObj.wrapAlert(title: nil, body: "Image uploading failed. Please try again".localized, img: #imageLiteral(resourceName: "info_one"))
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertViewObj.showAlert(controller: self)
                        }
                    }
                }
            }else {
                DispatchQueue.main.async {
                    alertViewObj.wrapAlert(title: nil, body: "Image uploading failed. Please try again".localized, img: #imageLiteral(resourceName: "info_one"))
                    alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                        self.dismiss(animated: true, completion: nil)
                    }
                    alertViewObj.showAlert(controller: self)
                }
            }
        } catch {}
    }
    
    func imageUploadOnServer(imageFile: UIImage) {
        showProgressView()
        
        var mobilNo = UserModel.shared.mobileNo
        if mobilNo == "" {
            mobilNo = RegistrationModel.shared.MobileNumber
        }
        
        DispatchQueue.main.async {
            let  convertedStr = imageFile.base64(format: ImageFormat.jpeg(0.1))
            let paramString : [String:Any] = [
                "MobileNumber":UserModel.shared.mobileNo,
                "Base64String":convertedStr!
            ]
            let web      = WebApiClass()
            web.delegate = self
            // let urlStr   = Url.businessDetailsImageURL
            let ur = URL(string: "https://www.okdollar.co/RestService.svc/GetImageUrlByBase64String")//getUrl(urlStr: urlStr, serverType: .imageUpload)
            println_debug(ur)
            web.genericClass(url: ur!, param: paramString as AnyObject, httpMethod: "POST", mScreen: "mImageUpload")
    }
}
}




class ImageUpCell: UITableViewCell {
    @IBOutlet weak var lblTop: UILabel!{
        didSet {
            lblTop.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var lblBottom: UILabel!{
        didSet {
            lblBottom.font = UIFont(name: appFont, size: appFontSize)
           lblBottom.text = "Tap to Change".localized
        }
    }
    @IBOutlet weak var btnTakeImage: UIButton!{
        didSet {
            btnTakeImage.titleLabel?.font  = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var imgActual: UIImageView!
    
}


class ImageOptionUPCell: UITableViewCell {
    @IBOutlet weak var btnCamera: UIButton! {
        didSet {
            btnCamera.titleLabel?.font  = UIFont(name: appFont, size: appButtonSize)
         btnCamera.setTitle("Camera".localized, for: .normal)
        }
    }
    @IBOutlet weak var btnCancel: UIButton! {
        didSet {
            btnCancel.titleLabel?.font  = UIFont(name: appFont, size: appButtonSize)
            btnCancel.setTitle("Cancel".localized, for: .normal)
        }
    }
    
    @IBOutlet weak var btnGallery: UIButton! {
        didSet {
            btnGallery.titleLabel?.font  = UIFont(name: appFont, size: appButtonSize)

           btnGallery.setTitle("Gallery".localized, for: .normal)
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary1(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey1(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
