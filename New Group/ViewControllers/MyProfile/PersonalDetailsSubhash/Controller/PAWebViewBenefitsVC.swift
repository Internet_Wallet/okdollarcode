//
//  PAWebViewBenefitsVC.swift
//  OK
//
//  Created by SHUBH on 9/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

//Used in Agent Profile as by Subhash on Oct20,2020


import UIKit

class PAWebViewBenefitsVC: OKBaseController {
    
//    @IBOutlet weak var wv: UIWebView!
//    @IBOutlet weak var indicatorView: UIActivityIndicatorView!

    @IBOutlet weak var lblFirst: UILabel!{
        didSet{
            self.lblFirst.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    var urlStr:String = ""
    var strTitleHeader:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblFirst.layer.cornerRadius = 10
        lblFirst.layer.masksToBounds = true
        let note = UpdateProfileModel.share.Agent
        let main = note?[2]
        
        if ok_default_language == "my" {
            //lblFirst.attributedText = NSAttributedString.init(html: main?["My"] as? String ?? "")
            let htmlString = main?["My"] as? String ?? ""
            if let labelTextFormatted = htmlString.htmlToAttributedString {
                let textAttributes = [
                    NSAttributedString.Key.foregroundColor: UIColor.black,
                    NSAttributedString.Key.font: UIFont(name: appFont, size:15) ?? UIFont.systemFont(ofSize: 15)
                    ] as [NSAttributedString.Key: Any]
                labelTextFormatted.addAttributes(textAttributes, range: NSRange(location: 0, length: labelTextFormatted.length))
                println_debug(NSAttributedString(attributedString: labelTextFormatted))
                lblFirst.attributedText = NSAttributedString(attributedString: labelTextFormatted)
            }
        }else if ok_default_language == "en"{
            lblFirst.attributedText = NSAttributedString.init(html: main?["En"] as? String ?? "")
        }else {
            let htmlString = main?["UniCode"] as? String ?? ""
            if let labelTextFormatted = htmlString.htmlToAttributedString {
                let textAttributes = [
                    NSAttributedString.Key.foregroundColor: UIColor.black,
                    NSAttributedString.Key.font: UIFont(name: appFont, size:15) ?? UIFont.systemFont(ofSize: 15)
                    ] as [NSAttributedString.Key: Any]
                labelTextFormatted.addAttributes(textAttributes, range: NSRange(location: 0, length: labelTextFormatted.length))
                println_debug(NSAttributedString(attributedString: labelTextFormatted))
                lblFirst.attributedText = NSAttributedString(attributedString: labelTextFormatted)
            }
        }
     
        
        if #available(iOS 13, *) {
                UIApplication.statusBarBackgroundColor =  kYellowColor
            } else {
                if let statusbar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                    statusbar.backgroundColor = kYellowColor
                }
            }
        
        //ok_agent_benefit_burmese
        self.title = strTitleHeader
    }

    
    @IBAction func onClickBackBtn(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }


}



extension String {
    
    var htmlToAttributedString: NSMutableAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSMutableAttributedString(data: data,
                                                 options: [.documentType: NSMutableAttributedString.DocumentType.html,
                                                           .characterEncoding: String.Encoding.utf8.rawValue],
                                                 documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }
    
}
