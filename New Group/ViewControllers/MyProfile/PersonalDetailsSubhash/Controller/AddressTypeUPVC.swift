//
//  AddressTypeUPVC.swift
//  OK
//
//  Created by Imac on 4/2/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol AddressTypeUPVCDelegte {
    func addressType(addType: String, vc: UIViewController)
}

class AddressTypeUPVC: OKBaseController {
    var delegate: AddressTypeUPVCDelegte?
    @IBOutlet weak var lblTitle: UILabel!{
        didSet{
            self.lblTitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var tfAddressType: UITextField!{
        didSet{
            self.tfAddressType.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    
    @IBOutlet weak var btnCancel: UIButton!{
        didSet{
            self.btnCancel.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    @IBOutlet weak var btnOK: UIButton!{
        didSet{
            self.btnOK.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var viewAMin: UIView!
    var NAMECHAR = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = "Address Type".localized
        btnCancel.setTitle("Cancel".localized, for: .normal)
        btnOK.setTitle("OK".localized, for: .normal)
        tfAddressType.delegate = self
        tfAddressType.placeholder = "Enter Address Type".localized
        viewAMin.layer.cornerRadius = 10
        viewAMin.layer.masksToBounds = true
        
        if ok_default_language == "my" {
            NAMECHAR = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀/-,:() "
        }else if ok_default_language == "en"{
           NAMECHAR = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789/-,:() "
        }else {
           NAMECHAR = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀/-,:() "
        }
    }

    @IBAction func onClickDone(_ sender: UIButton) {
        if sender.tag == 1 {
            if let del = delegate {
                del.addressType(addType: "", vc: self)
            }
        }else {
            if let del = delegate {
                del.addressType(addType: tfAddressType.text ?? "", vc: self)
            }
        }
    }
    
}

extension AddressTypeUPVC : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if range.location == 0 && string == " " {
            return false
        }
   
        
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: NAMECHAR).inverted).joined(separator: "")) { return false }
        if string == "" || string == " " {
            let _ = TextValidations.trimSuccessiveSpaces(textField: textField, range: range, string: string)
            return false
        }
        return text.count <= 40
    }
}
