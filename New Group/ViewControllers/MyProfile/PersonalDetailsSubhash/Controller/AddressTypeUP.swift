//
//  AddressTypeUP.swift
//  OK
//
//  Created by Imac on 3/6/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

//Subhash on Oct20,2020 - Business Profile-Ownership Type display


import UIKit

protocol AddressTypeUPDelegate {
    func selectedAddress(name: String, number: Int)
}

class AddressTypeUP: UIViewController,UITableViewDataSource,UITableViewDelegate,AddressTypeUPVCDelegte {
 
    
    @IBOutlet weak var btnCancel: UIButton!{
        didSet{
            self.btnCancel.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var tbAddressType: UITableView!
    var delegate: AddressTypeUPDelegate?
    var listArray: [Dictionary<String,Any>]?
    var viewController: String?
    var list: [String]?
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.setMarqueLabelInNavigation(title: "Address Type".localized)
        btnCancel.setTitle("Cancel".localized, for: . normal)
        tbAddressType.tableFooterView = UIView.init(frame: CGRect.zero)

    }
  
    func setMarqueLabelInNavigation(title: String) {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: appFontSize)
        lblMarque.text = title
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.init(hex: "#1C2C99")
        self.navigationItem.titleView = lblMarque
    }
    @IBAction func onClickBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewController == "personal" {
            return list?.count ?? 0
        }else {
            return listArray?.count ?? 0
        }
       
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "addressTypeCellUP") as! addressTypeCellUP
        if viewController == "personal" {
            let name = list?[indexPath.row]
            cell.lblName.text = name?.localized
        }else {
            let dic = listArray?[indexPath.row]
            if ok_default_language == "my" {
                cell.lblName.text = dic?["Bvalue"] as? String ?? ""
            }else if ok_default_language == "en"{
                cell.lblName.text = dic?["Value"] as? String ?? ""
            }else {
                cell.lblName.text = dic?["BvalueUnicode"] as? String ?? ""
            }
        }

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let del = delegate {
            if viewController == "personal" {
                if indexPath.row == 2 {
                    let addtype  = self.storyboard?.instantiateViewController(withIdentifier: "AddressTypeUPVC") as! AddressTypeUPVC
                    if let window = UIApplication.shared.keyWindow {
                        addtype.delegate = self
                        window.rootViewController?.addChild(addtype)
                        window.addSubview(addtype.view)
                        window.makeKeyAndVisible()
                    }
                }else {
                del.selectedAddress(name: list?[indexPath.row] ?? "", number: 0)
                self.navigationController?.popViewController(animated: true)
                }
            }else {
                let dic = listArray?[indexPath.row]
                let number = dic?["No"] as? String ?? ""
                var ownName = ""
                if ok_default_language == "my" {
                    let name = dic?["Bvalue"] as? String ?? ""
                    ownName = name
                }else if ok_default_language == "en"{
                       let name = dic?["Value"] as? String ?? ""
                     ownName = name
                }else {
                    let name = dic?["BvalueUnicode"] as? String ?? ""
                    ownName = name
                }
                del.selectedAddress(name: ownName, number: Int(number) ?? 0)
                self.navigationController?.popViewController(animated: true)
            }
        }
        
    }
    
    func addressType(addType: String, vc: UIViewController) {
        vc.view.removeFromSuperview()

            if addType != "" {
                if let del = delegate {
                  del.selectedAddress(name: addType, number: 0)
                }
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}


class addressTypeCellUP : UITableViewCell {
    @IBOutlet weak var lblName: UILabel!{
        didSet{
            self.lblName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
}
