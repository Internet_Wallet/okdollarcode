//
//  PAImagePreviewVC.swift
//  OK
//
//  Created by SHUBH on 8/17/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SDWebImage

protocol PAImagePreviewVCDelegate{
    func ImagePreviewUrl(imageName : String, imageURL: String)
}

extension PAImagePreviewVCDelegate {
    func ImagePreviewUrl(imageName : String, imageURL: String) {}
}

class PAImagePreviewVC: OKBaseController, UIScrollViewDelegate {
    var imagePreviewVCDelegate : PAImagePreviewVCDelegate?
    @IBOutlet weak var btnUpdateCancel: UIButton!{
        didSet{
            btnUpdateCancel.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnUpdate: UIButton!{
        didSet{
            btnUpdate.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnCamera : UIButton!{
        didSet{
            btnCamera.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnGallery : UIButton!{
        didSet{
            btnGallery.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var lblSubTitle: UILabel!{
        didSet{
            lblSubTitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    //Manage Image Preview
    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var imgViewAttachFile: UIImageView!
    var imageRotated = 0
    var fullURL : String?
    var imageFile :UIImage!
    var imageFileURL = ""
    var strTitle: String?
    var strSubTitle: String?
    let imagePicker = UIImagePickerController()
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 10.0//maximum zoom scale you want
        scrollView.zoomScale = 1.0
        imageRotated = 0
        imagePicker.delegate = self
        lblSubTitle.text = strSubTitle ?? ""
        btnCamera.setTitle("Take Photo".localized, for : .normal)
        btnGallery.setTitle("Gallery".localized, for : .normal)
        btnUpdate.setTitle("Update".localized, for : .normal)
        btnUpdateCancel.setTitle("Cancel".localized, for : .normal)
        setMarqueLabelInNavigation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        if let url = fullURL {
            imgViewAttachFile.sd_setImage(with:URL(string: url), placeholderImage: #imageLiteral(resourceName: "betterthan"))
            imageFile = imgViewAttachFile.image
        }
        btnUpdate.isHidden = true
        //btnUpdateCancel.isHidden = true
    }
    
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: appFontSize)
        lblMarque.text = strTitle ?? ""
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
    }
    @IBAction func imageRotationAction(_ sender: Any) {
        var portraitImage  = UIImage()
        if imageRotated == 0 {
            portraitImage = UIImage(cgImage: imageFile.cgImage! ,
                                    scale: 1.0 ,
                                    orientation: UIImage.Orientation.right)
            imageRotated = 90
        }else if imageRotated == 90 {
            portraitImage = UIImage(cgImage: imageFile.cgImage! ,
                                    scale: 1.0 ,
                                    orientation: UIImage.Orientation.down)
            imageRotated = 180
        }else if imageRotated == 180 {
            portraitImage = UIImage(cgImage: imageFile.cgImage! ,
                                    scale: 1.0 ,
                                    orientation: UIImage.Orientation.left)
            imageRotated = 270
        }else {
            portraitImage = UIImage(cgImage: imageFile.cgImage! ,
                                    scale: 1.0 ,
                                    orientation: UIImage.Orientation.up)
            imageRotated = 0
        }
        imgViewAttachFile.image = portraitImage
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //zoom in/out image for attach file
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imgViewAttachFile
    }
    
}

extension PAImagePreviewVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    @IBAction func updateOnServer() {
        self.imageUploadOnServer()
    }
    
    @IBAction func updateCancel() {
        self.navigationController?.popViewController(animated : true)
    }
    
    @IBAction func cancelButtonAction() {
        self.navigationController?.popViewController(animated : true)
    }
    
    @IBAction func getImageFromCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.allowsEditing = false
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.cameraCaptureMode = .photo
            imagePicker.modalPresentationStyle = .fullScreen
            self.present(imagePicker,animated: true,completion: nil)
        }
    }
    
    @IBAction func getImageFromGallery() {
          let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.modalPresentationStyle = .popover
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            imageFile = image
            imgViewAttachFile.image = image
            btnUpdate.isHidden = false
            btnUpdateCancel.isHidden = false
            dismiss(animated: true, completion: nil)
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            imageFile = pickedImage
            imgViewAttachFile.image = pickedImage
            btnUpdate.isHidden = false
            btnUpdateCancel.isHidden = false
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        btnUpdate.isHidden = true
        //btnUpdateCancel.isHidden = true
        dismiss(animated: true, completion: nil)
    }
    
}

extension PAImagePreviewVC: WebServiceResponseDelegate{
    //Upload image to server
    func imageUploadOnServer() {
        println_debug("image upload on server")
        showProgressView()
        DispatchQueue.main.async {
            let convertedStr = self.imageFile.base64(format: ImageFormat.jpeg(0.4))
            let paramString : [String:Any] = [
                "MobileNumber":UserModel.shared.mobileNo,
                "Base64String":convertedStr!
            ]
            let web      = WebApiClass()
            web.delegate = self
            // let urlStr   = Url.businessDetailsImageURL
            let ur = URL(string: "https://www.okdollar.co/RestService.svc/GetImageUrlByBase64String")//getUrl(urlStr: urlStr, serverType: .imageUpload)
            println_debug(ur)
            web.genericClass(url: ur!, param: paramString as AnyObject, httpMethod: "POST", mScreen: "mImageUpload")
        }
    }
    
    //MARK:- API Response
    func webResponse(withJson json: AnyObject, screen: String) {
        self.removeProgressView()
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug(dic)
                        imageFileURL = dic["Data"] as! String
                        println_debug(imageFileURL)
                        DispatchQueue.main.async {
                            if self.imageFileURL != "" {
                                alertViewObj.wrapAlert(title: nil, body: "Image uploaded successfully".localized, img: UIImage(named: "bank_success")!)
                                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                    self.imagePreviewVCDelegate?.ImagePreviewUrl(imageName : self.strSubTitle ?? "", imageURL: self.imageFileURL)
                                    self.navigationController?.popViewController(animated : true)
                                }
                                alertViewObj.showAlert(controller: self)
                            
                        }else {
                            alertViewObj.wrapAlert(title: nil, body: "Image uploading failed. Please try again".localized, img: #imageLiteral(resourceName: "info_one"))
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                            }
                            alertViewObj.showAlert(controller: self)
                        }
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        alertViewObj.wrapAlert(title: nil, body: "Image uploading failed. Please try again".localized, img: #imageLiteral(resourceName: "info_one"))
                        alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                        }
                        alertViewObj.showAlert(controller: self)
                    }
                }
            } catch {}
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
