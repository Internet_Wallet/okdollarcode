//
//  ZoomImageUP.swift
//  OK
//
//  Created by Imac on 4/27/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class ZoomImageUP: UIViewController,UIScrollViewDelegate {
    @IBOutlet weak var imageView : UIImageView!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var btnCancel: UIButton!{
        didSet {
            btnCancel.titleLabel?.font  = UIFont(name: appFont, size: appButtonSize)
        }
    }
    var myImage: UIImage?
    var headerTitile: String?
    var imageRotated = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollview.delegate = self
        scrollview.minimumZoomScale = 1.0
        scrollview.maximumZoomScale = 10.0//maximum zoom scale you want
        scrollview.zoomScale = 1.0
        scrollview.showsVerticalScrollIndicator = false
        scrollview.showsHorizontalScrollIndicator = false
        imageView.contentMode = .scaleAspectFit
        self.imageView.image = myImage
        setMarqueLabelInNavigation(title: headerTitile ?? "")
        btnCancel.setTitle("Cancel".localized, for: .normal)
    }
    
    private func setMarqueLabelInNavigation(title: String) {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font = UIFont(name: appFont, size: appFontSize)
        lblMarque.text = title
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
    }

    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    @IBAction func onClickRotateImage(_ sender: Any) {
        var portraitImage  = UIImage()
        if imageRotated == 0 {
            portraitImage = UIImage(cgImage: (imageView.image?.cgImage!)! ,
                                    scale: 1.0 ,
                                    orientation: UIImage.Orientation.right)
            imageRotated = 90
        } else if imageRotated == 90 {
            portraitImage = UIImage(cgImage: (imageView.image?.cgImage!)! ,
                                    scale: 1.0 ,
                                    orientation: UIImage.Orientation.down)
            imageRotated = 180
        } else if imageRotated == 180 {
            portraitImage = UIImage(cgImage: (imageView.image?.cgImage!)! ,
                                    scale: 1.0 ,
                                    orientation: UIImage.Orientation.left)
            imageRotated = 270
        } else {
            portraitImage = UIImage(cgImage: (imageView.image?.cgImage!)! ,
                                    scale: 1.0 ,
                                    orientation: UIImage.Orientation.up)
            imageRotated = 0
        }
        imageView.image = portraitImage
    }
}

