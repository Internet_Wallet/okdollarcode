//
//  PersonalDetailsUP.swift
//  OK
//
//  Created by Imac on 3/1/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import Rabbit_Swift

class PersonalDetailsUP: OKBaseController {
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var tbUpdateProfile: UITableView!
    @IBOutlet weak var bottomConstraintsTV: NSLayoutConstraint!
    @IBOutlet weak var btnUpdate: UIButton!
    
    let imagePicker = UIImagePickerController()
    var profileData: Dictionary<String,Any>?
    var sectionCount: Int?
     var imageInfo = [NSMutableDictionary]()
    var personalInfo = [NSMutableDictionary]()
    var AddressInfo = [NSMutableDictionary]()
    var securityList = [NSMutableDictionary]()
    var AlternetPersonal = [NSMutableDictionary]()
    var ContactDetails = [NSMutableDictionary]()
    var navigation : UINavigationController?
    var navItem    : UINavigationItem?
    var isVillageAvaible = true
    var countryListShownFor = ""
    var contactListShownFor = ""
    let validObj = PayToValidations()
    
    var locationDetails: LocationDetail?
    var townShipDetail : TownShipDetail?
    var townshipCode = ""

    var myProfileStatus = 0
    var myProfileStatusInPercent = 0
    var CHARSET = ""
    let CHARSETEMAIL = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890.@_ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာ၁၂၃၄၅၆၇၈၉၀-"
    var PASSPORTCHARSET = "(),_-1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    var NAMECHAR = ""
    var kycProfileImage = false
    var kycUserName = false
    var kycFatherNAme = false
    var kycNrcPassportIds = false
    var kycNrcPassportNumber = false
    var kycPassportExpDate = false
    var kycDOB = false
    var kycPassportCountry = false
    
    
    var isFatherNameKeyboardHidden = false
    var isYourNameKeyboardHidden = false
    var locationDetailsNRC: LocationDetail?
    var nrcPrefix = ""
    var nrcPostfix = ""
    let btnnrc = UIButton()
    
    var tvVillageList = Bundle.main.loadNibNamed("VillageStreetListView", owner: self, options: nil)?[0] as! VillageStreetListView
    var tvStreetList = Bundle.main.loadNibNamed("StreetListView", owner: self, options: nil)?[0] as! StreetListView
    
    
    var flagCodeAlternate = ""
    var countryCodeAlternate = ""
    var flagCodeReferral = ""
    var countryCodeReferral = ""
    var selectedDate = ""
    var expireyDate = ""
    
    
    var imgFirst = UIImage()
    var imgSecond = UIImage()
    
    var prefixUserName = ""
    var prefixFatherName = ""
    var editselect = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //kycUserName = true
        PersonalProfileModelUP.shared.clearAllValues()
        btnUpdate.setTitle("Update".localized, for: .normal)
        btnUpdate.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        bottomConstraintsTV.constant = 0
        self.setMarqueLabelInNavigation()
       
        btnUpdate.setTitle("Update".localized, for: .normal)
        if ok_default_language == "my" {
        CHARSET = STREETNAME_CHAR_SET_My
        NAMECHAR = NAME_CHAR_SET_My
        }else if ok_default_language == "en"{
            CHARSET = STREETNAME_CHAR_SET_En
            NAMECHAR = NAME_CHAR_SET_En
        }else {
            CHARSET = STREETNAME_CHAR_SET_Uni
            NAMECHAR = NAME_CHAR_SET_Uni
        }
        
       
        
        if UserModel.shared.agentType == .user {
            sectionCount = 8
        }else{
            sectionCount = 8
        }
        
        tbUpdateProfile.register(UINib(nibName: "FatherNameUpCell", bundle: nil), forCellReuseIdentifier: "FatherNameUpCell")
        tbUpdateProfile.register(UINib(nibName: "UserNameUPCell", bundle: nil), forCellReuseIdentifier: "UserNameUPCell")
        tbUpdateProfile.register(UINib(nibName: "ProfilePicUPCell", bundle: nil), forCellReuseIdentifier: "ProfilePicUPCell")
        tbUpdateProfile.register(UINib(nibName: "NameUPCell", bundle: nil), forCellReuseIdentifier: "NameUPCell")
        tbUpdateProfile.register(UINib(nibName: "NRCPassportUPCell", bundle: nil), forCellReuseIdentifier: "NRCPassportUPCell")
        tbUpdateProfile.register(UINib(nibName: "NameActionUPCell", bundle: nil), forCellReuseIdentifier: "NameActionUPCell")
        tbUpdateProfile.register(UINib(nibName: "NameInputUPCell", bundle: nil), forCellReuseIdentifier: "NameInputUPCell")
        tbUpdateProfile.register(UINib(nibName: "AddressUPCell", bundle: nil), forCellReuseIdentifier: "AddressUPCell")
        tbUpdateProfile.register(UINib(nibName: "StatusUPCell", bundle: nil), forCellReuseIdentifier: "StatusUPCell")
        tbUpdateProfile.register(UINib(nibName: "StatusButtonUPCell", bundle: nil), forCellReuseIdentifier: "StatusButtonUPCell")
        tbUpdateProfile.register(UINib(nibName: "ContactUPCell", bundle: nil), forCellReuseIdentifier: "ContactUPCell")
        self.getPersonalProfileData()
    }
    
     func initialPopUp() {
        DispatchQueue.main.async {
            
            if PersonalProfileModelUP.shared.IdType == "01"{
                self.showAlert(alertTitle: "", alertBody: "NRC ID is required to verify your account as per rules & Regulations of Central Bank of Myanmar".localized, alertImage: UIImage(named: "infoQR")!)
            }else{
                self.showAlert(alertTitle: "", alertBody: "Passport ID is required to verify your account as per rules & Regulations of Central Bank of Myanmar".localized, alertImage: UIImage(named: "infoQR")!)
            }
            
            
           
        }
    }
    
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        
        var hdTitle = ""
        if UserModel.shared.agentType == .user {
            hdTitle = "Personal Details".localized
        }else {
           hdTitle = "Merchant Details".localized
        }
        
        lblMarque.font =  UIFont(name: appFont, size: 16)
        lblMarque.text = hdTitle
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.init(hex: "#1C2C99")
        self.navigationItem.titleView = lblMarque
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //self.tbUpdateProfile.layoutSubviews()
        editselect = ""
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let url1Exits = PersonalProfileModelUP.shared.NrcImage1
        let url2Exits = PersonalProfileModelUP.shared.NrcImage2
        let image1 = UIImage()
        let image2 = UIImage()
        if url1Exits != "" {
           if let img = image1.convertURLToImage(str: url1Exits) {
            imgFirst = img
            }
        }
        if url2Exits != "" {
            if let img = image2.convertURLToImage(str: url2Exits) {
                imgSecond =  img
            }
            }
    }
    @IBAction func onClickBackAction(_ sender: Any) {
        if btnEdit.isHidden {
            alertViewObj.wrapAlert(title:"", body:"Clicking back without update will clear all entered details, Are you sure to exit?".localized, img: UIImage(named: "alert-icon"))
            alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
            })
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
               self.navigationController?.popViewController(animated: true)
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    
    func setUpVillageList() {
        tvVillageList.frame = CGRect(x: 10, y: 0, width: self.view.frame.width - 20, height: 200)
        tvVillageList.backgroundColor = UIColor.clear
        tvVillageList.delegate = self
        tvVillageList.tvList.isHidden = true
        tvVillageList.isUserInteractionEnabled = true
        self.view.addSubview(tvVillageList)
        self.view.bringSubviewToFront(tvVillageList)
    }
    func removeVillageList() {
      if self.view.contains(tvVillageList) {
            tvVillageList.removeFromSuperview()
        }
    }
    //tvStreetList
    func setUpStreetList() {
        tvStreetList.frame = CGRect(x: 10, y: 0, width: self.view.frame.width - 20, height: 200)
        tvStreetList.backgroundColor = UIColor.clear
        tvStreetList.delegate = self
        tvStreetList.tvList.isHidden = true
        tvStreetList.isUserInteractionEnabled = true
        self.view.addSubview(tvStreetList)
        self.view.bringSubviewToFront(tvStreetList)
    }
    func removeStreetList() {
        if self.view.contains(tvStreetList) {
            tvStreetList.removeFromSuperview()
        }
    }
    
    
    
    
    private func wrapAllLists() {
        
        let states = TownshipManager.allLocationsList
        for state in states {
            if state.stateOrDivitionCode == PersonalProfileModelUP.shared.Division {
                if ok_default_language == "my" {
                    PersonalProfileModelUP.shared.Division = state.stateOrDivitionNameMy
                }else if ok_default_language == "en" {
                     PersonalProfileModelUP.shared.Division = state.stateOrDivitionNameEn
                }else {
                    PersonalProfileModelUP.shared.Division = state.stateOrDivitionNameUni
                }
                locationDetails = state
                let township = state.townshipArryForAddress
                for town in township {
                    if town.townShipCode == PersonalProfileModelUP.shared.Township {
                        if town.GroupName.contains(find: "YCDC") || town.GroupName.contains(find: "MCDC"){
                            isVillageAvaible = false
                        }
                        if town.cityNameEN == "Amarapura" || town.cityNameEN == "Patheingyi" || town.townShipNameEN == "PatheinGyi" {
                            isVillageAvaible = true
                        }
                        if isVillageAvaible {
                            self.callStreetVillageAPI(townshipcode: PersonalProfileModelUP.shared.Township)
                        }
                        if ok_default_language == "my" {
                            PersonalProfileModelUP.shared.Township = town.townShipNameMY
                            if town.GroupName == "" {
                                PersonalProfileModelUP.shared.City = town.cityNameMY
                            }else {
                                if town.isDefaultCity == "1" {
                                    PersonalProfileModelUP.shared.City = town.DefaultCityNameMY
                                }else{
                                    PersonalProfileModelUP.shared.City = town.cityNameMY
                                }
                            }
                        }else if ok_default_language == "en" {
                           PersonalProfileModelUP.shared.Township = town.townShipNameEN
                            if town.GroupName == "" {
                                 PersonalProfileModelUP.shared.City = town.cityNameEN
                            }else {
                                if town.isDefaultCity == "1" {
                                 PersonalProfileModelUP.shared.City = town.DefaultCityNameEN
                                }else {
                                  PersonalProfileModelUP.shared.City = town.cityNameEN
                                }
                            }
                        }else {
                            PersonalProfileModelUP.shared.Township = town.townShipNameUni
                            if town.GroupName == "" {
                                PersonalProfileModelUP.shared.City = town.cityNameUni
                            }else {
                                if town.isDefaultCity == "1" {
                                    PersonalProfileModelUP.shared.City = town.DefaultCityNameUni
                                }else {
                                    PersonalProfileModelUP.shared.City = town.cityNameUni
                                }
                            }
                        }
                        break
                    }
                }
            }
        }
       var accountType = ""
        if UserModel.shared.agentType == .user  {
            accountType = "Personal".localized
        }else if UserModel.shared.agentType == .merchant  {
            accountType = "Merchant".localized
        }else if UserModel.shared.agentType == .agent  {
            accountType = "Agent".localized
        }else if UserModel.shared.agentType == .advancemerchant  {
            accountType = "Advance Merchant"  .localized
        }else if UserModel.shared.agentType == .oneStopMart  {
            accountType = "One Stop Mart".localized
        }
        
        imageInfo = [["icon": PersonalProfileModelUP.shared.ProfilePicture,"Age": PersonalProfileModelUP.shared.Dob,"Gender": PersonalProfileModelUP.shared.Gender,"Name": PersonalProfileModelUP.shared.ProfileName]]
        personalInfo = [["icon": "personal","statusTitle": "OK $ Account Type".localized,"title": accountType],
                        ["icon": "myanmar","statusTitle": "OK $ Account Mobile Number".localized,"title": PersonalProfileModelUP.shared.OkAccNumber],
                        ["icon": "r_user","statusTitle": "Joined On".localized,"title": PersonalProfileModelUP.shared.OkDollarJoiningDate],
                        ["icon": "dob","statusTitle": "Date of Birth".localized,"title": PersonalProfileModelUP.shared.Dob],
                        ["icon": "myanmar","statusTitle": "Nationality".localized,"title": PersonalProfileModelUP.shared.CountryOfCitizen],
                        ["nrcNumber": PersonalProfileModelUP.shared.NrcNumber,"image1": PersonalProfileModelUP.shared.NrcImage1,"image2": PersonalProfileModelUP.shared.NrcImage2,"statusTitle": "NRC Number & Image".localized],
                        ["icon": "date_expire","statusTitle": "Date of Expiry".localized,"title": PersonalProfileModelUP.shared.NrcExpireDate],
                        ["icon": "father","statusTitle": "Father Name".localized,"title": PersonalProfileModelUP.shared.FatherName]]
        
        AddressInfo = [["icon": "addressUp","statusTitle": "Address Type".localized,"title": PersonalProfileModelUP.shared.AddressType],
                       ["icon": "r_division","statusTitle": "State / Division".localized,"title": PersonalProfileModelUP.shared.Division],
                       ["icon": "r_city","statusTitle": "Township".localized,"title": PersonalProfileModelUP.shared.Township],
                       ["icon": "r_township","statusTitle": "City".localized,"title": PersonalProfileModelUP.shared.City],
                       ["icon": "village_track","statusTitle": "Village Tract".localized,"title": PersonalProfileModelUP.shared.Village],
                       ["icon": "r_street","statusTitle": "Street".localized,"title": PersonalProfileModelUP.shared.Street],
                       ["house_no": PersonalProfileModelUP.shared.HouseNumber,"floor_no": PersonalProfileModelUP.shared.FloorNumber,"room_no": PersonalProfileModelUP.shared.RoomNumber],
                       ["icon": "housing","statusTitle": "Housing Zone Name".localized,"title": PersonalProfileModelUP.shared.HouseBlockNumber]]
        
        securityList = [["icon": "security_question","statusTitle": "Select Security Question".localized,"title": "","code": PersonalProfileModelUP.shared.SecurityQuestion],
                        ["icon": "security_answer","statusTitle": "Answer".localized,"title": PersonalProfileModelUP.shared.SecrityAnswer]]
        
        AlternetPersonal = [["icon": "myanmar","statusTitle": "Alternate Number".localized,"title": PersonalProfileModelUP.shared.AlternatePhonenumber,"country_Code": "+95"]]
        ContactDetails = [["icon": "phone_number","statusTitle": "Referral Number".localized,"title": PersonalProfileModelUP.shared.Recommended],
                          ["icon": "r_email","statusTitle": "Email ID".localized,"title": PersonalProfileModelUP.shared.EmailId],
                          ["icon": "r_facebook","statusTitle": "Facebook".localized,"title": PersonalProfileModelUP.shared.FacebookId]]
    }
    
    private func getPersonalProfileData() {
        
        if let persoanlDic = profileData?["PersonalInfo"] as? Dictionary<String,Any> {
            PersonalProfileModelUP.shared.KycStatus = persoanlDic["KycStatus"] as? Int ?? 0
            if ok_default_language == "my"
            {
                PersonalProfileModelUP.shared.ProfileName = parabaik.uni(toZawgyi:persoanlDic["ProfileName"] as? String) ?? ""
                PersonalProfileModelUP.shared.FatherName = parabaik.uni(toZawgyi:persoanlDic["FatherName"] as? String) ?? ""
                PersonalProfileModelUP.shared.NrcNumber = parabaik.uni(toZawgyi:persoanlDic["NrcNumber"] as? String) ?? ""
                PersonalProfileModelUP.shared.SecrityAnswer = parabaik.uni(toZawgyi:persoanlDic["SecrityAnswer"] as? String) ?? ""


            }
            else {
                PersonalProfileModelUP.shared.ProfileName = parabaik.zawgyi(toUni:persoanlDic["ProfileName"] as? String) ?? ""
                PersonalProfileModelUP.shared.FatherName = parabaik.zawgyi(toUni:persoanlDic["FatherName"] as? String) ?? ""
                PersonalProfileModelUP.shared.NrcNumber = persoanlDic["NrcNumber"] as? String ?? ""
                PersonalProfileModelUP.shared.SecrityAnswer = parabaik.zawgyi(toUni:persoanlDic["SecrityAnswer"] as? String) ?? ""


            }
            //PersonalProfileModelUP.shared.ProfileName = persoanlDic["ProfileName"] as? String ?? ""
            let profilePic = persoanlDic["ProfilePicture"] as? String ?? ""
           let pc =  profilePic.replacingOccurrences(of: " \\", with: "")
            PersonalProfileModelUP.shared.ProfilePicture = pc.replacingOccurrences(of: " ", with: "%20")
            PersonalProfileModelUP.shared.Gender = persoanlDic["Gender"] as? String ?? ""
            PersonalProfileModelUP.shared.IdType = persoanlDic["IdType"] as? String ?? ""
            PersonalProfileModelUP.shared.OkAccNumber = persoanlDic["OkAccNumber"] as? String ?? ""
            PersonalProfileModelUP.shared.OkDollarJoiningDate = persoanlDic["OkDollarJoiningDate"] as? String ?? ""
            PersonalProfileModelUP.shared.Dob = persoanlDic["Dob"] as? String ?? ""
           // PersonalProfileModelUP.shared.FatherName = persoanlDic["FatherName"] as? String ?? ""
            let image1 = persoanlDic["NrcImage1"] as? String ?? ""
            let image2 = persoanlDic["NrcImage2"] as? String ?? ""
            let img1 = image1.replacingOccurrences(of: "\\", with: "")
            let img2 = image2.replacingOccurrences(of: "\\", with: "")
            PersonalProfileModelUP.shared.NrcImage1 = img1.replacingOccurrences(of: " ", with: "%20")
            PersonalProfileModelUP.shared.NrcImage2 = img2.replacingOccurrences(of: " ", with: "%20")
            
            //PersonalProfileModelUP.shared.NrcNumber = persoanlDic["NrcNumber"] as? String ?? ""
            if PersonalProfileModelUP.shared.IdType == "01" {
                if PersonalProfileModelUP.shared.NrcNumber.contains("@") {
                    let nrc = PersonalProfileModelUP.shared.NrcNumber.replacingOccurrences(of: "@", with: "/")
                    PersonalProfileModelUP.shared.NrcNumber = nrc
                    if PersonalProfileModelUP.shared.NrcNumber.contains("(") && PersonalProfileModelUP.shared.NrcNumber.contains(")") {
                        let ncsslipt = PersonalProfileModelUP.shared.NrcNumber.components(separatedBy: ")")
                        nrcPrefix = ncsslipt[0] + ")"
                        nrcPostfix = ncsslipt[1]
                    }
                }else {
                    if PersonalProfileModelUP.shared.NrcNumber.contains("(") && PersonalProfileModelUP.shared.NrcNumber.contains(")") {
                        let ncsslipt = PersonalProfileModelUP.shared.NrcNumber.components(separatedBy: ")")
                        nrcPrefix = ncsslipt[0] + ")"
                        nrcPostfix = ncsslipt[1]
                    }
                }
            }
            
            PersonalProfileModelUP.shared.NrcExpireDate = persoanlDic["NrcExpireDate"] as? String ?? ""
            PersonalProfileModelUP.shared.CountryOfCitizen = persoanlDic["CountryOfCitizen"] as? String ?? ""
            PersonalProfileModelUP.shared.AccountType = persoanlDic["AccountType"] as? Int ?? 0
            
            PersonalProfileModelUP.shared.SecurityQuestion = persoanlDic["SecurityQuestion"] as? String ?? ""
            
            PersonalProfileModelUP.shared.AlternatePhonenumber = persoanlDic["AlternatePhonenumber"] as? String ?? ""
            PersonalProfileModelUP.shared.Recommended = persoanlDic["Recommended"] as? String ?? ""
            let email = persoanlDic["EmailId"] as? String ?? ""
            if email.contains(find: ",") {
                let emailArray = email.components(separatedBy: ",")
                PersonalProfileModelUP.shared.EmailId = emailArray[0]
            }else {
              PersonalProfileModelUP.shared.EmailId = persoanlDic["EmailId"] as? String ?? ""
            }
           
            PersonalProfileModelUP.shared.FacebookId = parabaik.zawgyi(toUni:persoanlDic["FacebookId"] as? String) ?? ""
            PersonalProfileModelUP.shared.ApproveStatus = persoanlDic["ApproveStatus"] as? Int ?? 0
            
        }
        if let addressDic = profileData?["AddressInfo"] as? Dictionary<String,Any> {
            
            if ok_default_language == "my" {
                //Need to check Apple Issue
                PersonalProfileModelUP.shared.Village = parabaik.uni(toZawgyi:addressDic["Village"] as? String) ?? ""
                PersonalProfileModelUP.shared.Street = parabaik.uni(toZawgyi:addressDic["Street"] as? String) ?? ""
                PersonalProfileModelUP.shared.HouseNumber = parabaik.uni(toZawgyi:addressDic["HouseNumber"] as? String) ?? ""
                PersonalProfileModelUP.shared.FloorNumber = parabaik.uni(toZawgyi:addressDic["FloorNumber"] as? String) ?? ""
                PersonalProfileModelUP.shared.RoomNumber = parabaik.uni(toZawgyi:addressDic["RoomNumber"] as? String) ?? ""
            }else {
                PersonalProfileModelUP.shared.Village = addressDic["Village"] as? String ?? ""
                PersonalProfileModelUP.shared.Street = addressDic["Street"] as? String ?? ""
                PersonalProfileModelUP.shared.HouseNumber = addressDic["HouseNumber"] as? String ?? ""
                PersonalProfileModelUP.shared.FloorNumber = addressDic["FloorNumber"] as? String ?? ""
                PersonalProfileModelUP.shared.RoomNumber = addressDic["RoomNumber"] as? String ?? ""
                //Apple Issue resolve-Aug04
//                PersonalProfileModelUP.shared.RoomNumber = parabaik.zawgyi(toUni:addressDic["RoomNumber"] as? String) ?? ""
            }
            PersonalProfileModelUP.shared.AddressType = addressDic["AddressType"] as? String ?? ""
            PersonalProfileModelUP.shared.Division = addressDic["Division"] as? String ?? ""
            PersonalProfileModelUP.shared.Township = addressDic["Township"] as? String ?? ""
            PersonalProfileModelUP.shared.City = addressDic["City"] as? String ?? ""
            PersonalProfileModelUP.shared.HouseBlockNumber = addressDic["HouseBlockNumber"] as? String ?? ""
            //Apple Issue resolve-Aug25-Gauri
//            PersonalProfileModelUP.shared.HouseBlockNumber = parabaik.zawgyi(toUni:addressDic["HouseBlockNumber"] as? String) ?? ""
        }
        if PersonalProfileModelUP.shared.Division == "" && UserModel.shared.addressType == "Home".localized {
            
            if ok_default_language == "my" {
                PersonalProfileModelUP.shared.Village = parabaik.uni(toZawgyi:UserModel.shared.villageName)
                PersonalProfileModelUP.shared.Street = parabaik.uni(toZawgyi:UserModel.shared.street)
                PersonalProfileModelUP.shared.HouseNumber = parabaik.uni(toZawgyi:UserModel.shared.houseName)
                PersonalProfileModelUP.shared.FloorNumber = parabaik.uni(toZawgyi:UserModel.shared.floorNumber)
                PersonalProfileModelUP.shared.RoomNumber = parabaik.uni(toZawgyi:UserModel.shared.roomNumber)
            }else {
                PersonalProfileModelUP.shared.Village = UserModel.shared.villageName
                PersonalProfileModelUP.shared.Street = UserModel.shared.street
                PersonalProfileModelUP.shared.HouseNumber = UserModel.shared.houseName
                PersonalProfileModelUP.shared.FloorNumber = UserModel.shared.floorNumber
                PersonalProfileModelUP.shared.RoomNumber = UserModel.shared.roomNumber
            }
            PersonalProfileModelUP.shared.Division = UserModel.shared.state
            PersonalProfileModelUP.shared.Township = UserModel.shared.township
            PersonalProfileModelUP.shared.Street = UserModel.shared.address2
            PersonalProfileModelUP.shared.HouseBlockNumber = UserModel.shared.houseBlockNo
        }
        
        
        if let kyc = profileData?["KYC"] as? [Dictionary<String,Any>] {
            print(kyc)
            for key in kyc {
                if key["Code"] as? String == "UserProfilePic" {
                   kycProfileImage = key["Status"] as? Bool ?? false
                }else if key["Code"] as? String == "UserName" {
                     kycUserName = key["Status"] as? Bool ?? false
                }else if key["Code"] as? String == "UserDOB" {
                    kycDOB = key["Status"] as? Bool ?? false
                }else if key["Code"] as? String == "UserPassportCountry" {
                    kycPassportCountry = key["Status"] as? Bool ?? false
                }else if key["Code"] as? String == "UserIdProofNumber" {
                    kycNrcPassportNumber = key["Status"] as? Bool ?? false
                    //kycNrcPassportNumber = true
                }else if key["Code"] as? String == "UserIdProofImage" {
                    kycNrcPassportIds = key["Status"] as? Bool ?? false
                }else if key["Code"] as? String == "UserPassportExpiryDate" {
                    kycPassportExpDate = key["Status"] as? Bool ?? false
                }else if key["Code"] as? String == "UserFatherName" {
                    kycFatherNAme = key["Status"] as? Bool ?? false
                }
              
            }
            
        }
        
       // kycUserName = true
       // kycFatherNAme = true
      //  kycProfileImage = true
       // kycDOB = true
        //kycPassportExpDate = true
      
        self.wrapAllLists()
        self.getSecurityQuestion()
        self.myProfileStatus = self.updateProfileStatusCount()
        DispatchQueue.main.async {
            self.tbUpdateProfile.delegate = self
            self.tbUpdateProfile.dataSource = self
            self.tbUpdateProfile.reloadData()
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
   func reloadRows(index: Int, withSeciton sec: Int) {
        UIView.setAnimationsEnabled(false)
        self.tbUpdateProfile.beginUpdates()
        let indexPosition = IndexPath(row: index, section: sec)
        self.tbUpdateProfile.reloadRows(at: [indexPosition], with: .none)
        self.tbUpdateProfile.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
 
    
    func getDateFormateSring(date: String)-> String {
        return dateF5.string(from: parseDate(strDate: date))
    }
    
    func containsOnlyLetters(input: String) -> Bool {
        for chr in input {
            if (!(chr >= "a" && chr <= "z") && !(chr >= "A" && chr <= "Z") ) {
                return false
            }
        }
        return true
    }
    
    
    func getDateFormateSringForJoinDate(date: String)-> String {
        return dateF5.string(from: parseDateJaoinDate(strDate: date))
    }
    
    @IBAction func onClickEditAction(_ sender: Any) {
        btnEdit.isHidden = true
        bottomConstraintsTV.constant = +50
        editselect = "usernameShow"
        if kycProfileImage {
            reloadRows(index: 0, withSeciton: 0)
        }
        if kycUserName {
            reloadRows(index: 1, withSeciton: 0)
        }
        if kycDOB {
            reloadRows(index: 3, withSeciton: 1)
        }
        
        if PersonalProfileModelUP.shared.IdType == "04" {
            if kycPassportCountry {
                reloadRows(index: 4, withSeciton: 1)
            }
            
            if kycNrcPassportNumber || kycNrcPassportIds {
                reloadRows(index: 5, withSeciton: 1)
            }
            if kycPassportExpDate {
                reloadRows(index: 6, withSeciton: 1)
            }
            if kycFatherNAme {
                reloadRows(index: 7, withSeciton: 1)
            }
        }else {
            
            if kycNrcPassportNumber || kycNrcPassportIds {
                reloadRows(index: 4, withSeciton: 1)
            }
            if kycFatherNAme {
                reloadRows(index: 5, withSeciton: 1)
            }
        }
        
        
        reloadRows(index: 0, withSeciton: 2)
        reloadRows(index: 1, withSeciton: 2)
        reloadRows(index: 2, withSeciton: 2)
        reloadRows(index: 4, withSeciton: 2)
        reloadRows(index: 5, withSeciton: 2)
        reloadRows(index: 6, withSeciton: 2)
        reloadRows(index: 7, withSeciton: 2)
        reloadRows(index: 0, withSeciton: 3)
        reloadRows(index: 1, withSeciton: 3)
        if UserModel.shared.agentType == .user {
            reloadRows(index: 0, withSeciton: 4)
        }
        reloadRows(index: 0, withSeciton: 5)
        reloadRows(index: 1, withSeciton: 5)
        reloadRows(index: 2, withSeciton: 5)

    }
    
    @IBAction func onClickUpdateAction(_ sender: Any) {
  
        checkRequiredFields(handler: {(alertMessage, success)in
            if success {
                UpdatePersonalProfile()
            }else {
                DispatchQueue.main.async {
                    self.showAlert(alertTitle: "Please Fill All Mandatory Fields".localized, alertBody: alertMessage, alertImage: UIImage(named: "error")!)
                }
            }
        })
    }
}

extension PersonalDetailsUP : UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return (sectionCount ?? 0) - 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if kycUserName {
                return 2
            }else {
                return 1
            }
        }else if section == 1 {
            if PersonalProfileModelUP.shared.IdType == "01" {
                return 6
            }else {
                return 8
            }
        }
        else if section == 2 {
             return 8
        }else if section == 3 {
                return 2
        }else if section == 4 {
            if UserModel.shared.agentType == .user {
                return 1
            }else {
                return 0
            }
        }else if section == 5 {
            return 3
        }else if section == 6 {
              return 1
        }else if section == 7 {
            return 1
        }else {
            return 0
        }
       
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfilePicUPCell") as! ProfilePicUPCell
                cell.selectionStyle = .none
                let dic = imageInfo[indexPath.row]
                let urlFront = URL(string: PersonalProfileModelUP.shared.ProfilePicture)
                cell.imgProfile.sd_setImage(with: urlFront, placeholderImage: nil)
                cell.lblName.text = PersonalProfileModelUP.shared.ProfileName
                let age = getYearDifferaceForHeader(date: PersonalProfileModelUP.shared.Dob)
                let gender = dic["Gender"] as! String
                cell.lblAgeMaleStatus.text = age + ", " + gender
                let tap = UITapGestureRecognizer(target: self, action: #selector(PersonalDetailsUP.onClickUserImage))
                cell.imgProfile.addGestureRecognizer(tap)
                cell.imgProfile.isUserInteractionEnabled = true
                
                if PersonalProfileModelUP.shared.KycStatus == 1 {
                    cell.imgVerigy.isHidden = false
                }else {
                    cell.imgVerigy.isHidden = true
                }
            
                if kycProfileImage && btnEdit.isHidden {
                    //cell.isUserInteractionEnabled = true
                    cell.imgIcon.isHidden = false
                }else {
                    //cell.isUserInteractionEnabled = false
                    cell.imgIcon.isHidden = true
                }
                if btnEdit.isHidden {
                    cell.lblRequired.textColor = UIColor.red
                }else {
                    cell.lblRequired.textColor = UIColor.darkGray
                }
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserNameUPCell") as! UserNameUPCell
                cell.selectionStyle = .none
                if  editselect == "usernameShow"{
                    
                    cell.isHidden = false
                }
                else{
                    cell.isHidden = true
                }
                let title = PersonalProfileModelUP.shared.ProfileName.components(separatedBy: ",")
                if title.count > 1 {
                    cell.lbltitle.text = title[0] + ","
                    prefixUserName = title[0] + ","
                    cell.tfUserName.text = title[1]
                }
                
                if prefixUserName == "Ms," || prefixUserName == "Mrs," || prefixUserName == "Daw," || prefixUserName == "Ma," || prefixUserName == "MDr,"{
                    cell.imgUser.image = UIImage(named: "r_female")
                }else{
                    cell.imgUser.image = UIImage(named: "r_user")
                }

                cell.tfUserName.delegate = self
                
                
                if cell.tfUserName.text == ""{
                    cell.btnclose.isHidden = true
                }
             
                cell.btnMale.addTarget(self, action: #selector(onClickUserNameMale), for: .touchUpInside)
                cell.btnFemale.addTarget(self, action: #selector(onClickUserNameFemale), for: .touchUpInside)
                
                cell.btnU.addTarget(self, action: #selector(onClickUserNameMaleEn), for: .touchUpInside)
                cell.btnMg.addTarget(self, action: #selector(onClickUserNameMaleEn), for: .touchUpInside)
                cell.btnMr.addTarget(self, action: #selector(onClickUserNameMaleEn), for: .touchUpInside)
                cell.btnDr.addTarget(self, action: #selector(onClickUserNameMaleEn), for: .touchUpInside)
                
                cell.btnDaw.addTarget(self, action: #selector(onClickUserNameFemaleEn), for: .touchUpInside)
                cell.btnMa.addTarget(self, action: #selector(onClickUserNameFemaleEn), for: .touchUpInside)
                cell.btnMs.addTarget(self, action: #selector(onClickUserNameFemaleEn), for: .touchUpInside)
                cell.btnMrs.addTarget(self, action: #selector(onClickUserNameFemaleEn), for: .touchUpInside)
                cell.btnMDr.addTarget(self, action: #selector(onClickUserNameFemaleEn), for: .touchUpInside)
                
                cell.btnFU.addTarget(self, action: #selector(onClickUserNameMaleFemaleMy), for: .touchUpInside)
                cell.btnFDr.addTarget(self, action: #selector(onClickUserNameMaleFemaleMy), for: .touchUpInside)
                cell.btnFMr.addTarget(self, action: #selector(onClickUserNameMaleFemaleMy), for: .touchUpInside)
                
                cell.btnclose.addTarget(self, action: #selector(onClickUserNameClose(_:)), for: .touchUpInside)
                
                if btnEdit.isHidden {
                    if kycFatherNAme {
                       // cell.btnclose.isHidden = false
                    }
                    cell.isUserInteractionEnabled = true
                }else {
                    cell.isUserInteractionEnabled = false
                    cell.btnclose.isHidden = true
                }
                if btnEdit.isHidden {
                    cell.lblRequired.textColor = UIColor.red
                }else {
                    cell.lblRequired.textColor = UIColor.darkGray
                }
                
                return cell
               
            }
        }else if indexPath.section == 1 {
            if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NameUPCell", for: indexPath) as! NameUPCell
                let dicData = personalInfo[indexPath.row]
                return nameWrapCell(index: indexPath, cell: cell, dic: dicData as! Dictionary<String, String>)
            }else if indexPath.row == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NameInputUPCell", for: indexPath) as! NameInputUPCell
                cell.selectionStyle = .none
                cell.imgIcon.image = UIImage(named: "dob")
                cell.tfTitle.tag = 72
                cell.tfTitle.delegate = self
                
                var font = UIFont(name: appFont, size: 18.0)
                if #available(iOS 13.0, *) {
                    font = UIFont.systemFont(ofSize: 18)
                }
                cell.tfTitle.font = font
                
                
                if ok_default_language == "my" {
                    cell.tfTitle.font = UIFont(name: appFont, size: 18)
                }else if ok_default_language == "en"{
                    cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                }else {
                    if #available(iOS 13.0, *) {
                        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                          .font : UIFont(name: appFont, size: 18)]
                        cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "Select Date of Birth", attributes:attributes as [NSAttributedString.Key : Any])
                    } else {
                        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                          .font : UIFont.systemFont(ofSize: 18)]
                        cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "Select Date of Birth", attributes:attributes)
                        cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                    }
                     cell.tfTitle.font = UIFont(name: appFont, size: 18)
                }
                cell.tfTitle.placeholder = "Select Date of Birth".localized
                if PersonalProfileModelUP.shared.Dob == "" {
                    cell.tfTitle.text = ""
                }else {
                    cell.tfTitle.text = getDateFormateSring(date: PersonalProfileModelUP.shared.Dob)
                }
                
                cell.lblStatus.text = "Date of Birth".localized
                cell.lblStatus.textColor = .darkGray
                cell.lblRequired.isHidden = false
                cell.btnRightArrow.isHidden = true
                if kycDOB && btnEdit.isHidden {
                   cell.isUserInteractionEnabled = true
                }else {
                    cell.isUserInteractionEnabled = false
                }
                if btnEdit.isHidden {
                    cell.lblRequired.textColor = UIColor.red
                }else {
                    cell.lblRequired.textColor = UIColor.darkGray
                }
                return cell
            }else if indexPath.row == 4 {
                if  PersonalProfileModelUP.shared.IdType == "01" {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "NRCPassportUPCell", for: indexPath) as! NRCPassportUPCell
                    if btnEdit.isHidden {
                        cell.lblRequired.textColor = UIColor.red
                        cell.lblRequiredFront.textColor = UIColor.red
                        cell.lblRequiredBack.textColor = UIColor.red
                    }else {
                        cell.lblRequired.textColor = UIColor.darkGray
                        cell.lblRequiredFront.textColor = UIColor.darkGray
                        cell.lblRequiredBack.textColor = UIColor.darkGray
                    }
                    return NRCPassportWrapCell(index: indexPath, cell: cell)
                }else {
                    //Nationality
                    let cell = tableView.dequeueReusableCell(withIdentifier: "NameActionUPCell", for: indexPath) as! NameActionUPCell
                    let dicData = personalInfo[indexPath.row]
                    cell.selectionStyle = .none
                    cell.lblStatus.text = dicData["statusTitle"] as? String ?? ""
                    cell.lblTitle.isUserInteractionEnabled = true
                    cell.lblRequired.isHidden = false
                    cell.imgRightArrow.isHidden = true
                    
                    if containsOnlyLetters(input: PersonalProfileModelUP.shared.CountryOfCitizen) {
                         cell.lblTitle.text = PersonalProfileModelUP.shared.CountryOfCitizen.capitalizedFirst()
                        cell.imgIcon.image = UIImage(named: PersonalProfileModelUP.shared.CountryOfCitizen.localizedLowercase)
                    }else {
                        let countryData = identifyCountry(withPhoneNumber: PersonalProfileModelUP.shared.CountryOfCitizen)
                        PersonalProfileModelUP.shared.CountryOfCitizen = countryData.countryFlag.capitalizedFirst()
                        cell.lblTitle.text = countryData.countryFlag.capitalizedFirst()
                        if let safeImage = UIImage(named: countryData.countryFlag) {
                            DispatchQueue.main.async {
                                cell.imgIcon.contentMode = .scaleAspectFit
                                cell.imgIcon.image = safeImage
                            }
                        }
                    }

                    let tapAction = UITapGestureRecognizer(target: self, action:#selector(onClickAction(_:)))
                    tapAction.name = "Nationality"
                    cell.lblTitle?.addGestureRecognizer(tapAction)
                    if btnEdit.isHidden && kycPassportCountry {
                        cell.isUserInteractionEnabled = true
                    }else {
                        cell.isUserInteractionEnabled = false
                    }
                    if btnEdit.isHidden {
                        cell.lblRequired.textColor = UIColor.red
                    }else {
                        cell.lblRequired.textColor = UIColor.darkGray
                    }
                    return cell
                }
            }else if indexPath.row == 5 {
                if PersonalProfileModelUP.shared.IdType == "01" {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "FatherNameUpCell", for: indexPath) as! FatherNameUpCell
                    cell.selectionStyle = .none
                    let title = PersonalProfileModelUP.shared.FatherName.components(separatedBy: ",")
                    if title.count > 1 {
                        cell.lblTitle.text = title[0] + ","
                        prefixFatherName = title[0] + ","
                        cell.tfFatherName.text = title[1]
                    }else {
                        cell.tfFatherName.text = PersonalProfileModelUP.shared.FatherName
                    }
                    cell.tfFatherName.delegate = self
                    
                    if cell.tfFatherName.text == "" {
                        cell.lblTitle.text = ""
                        cell.btnClose.isHidden = true
                    }else {
                        cell.btnClose.isHidden = false
                    }
                    
                   // cell.tfFatherName.addTarget(self, action: #selector(userFatherNameEditing(_:)), for: .editingChanged)
                    cell.btnU.addTarget(self, action: #selector(onClickFatherNameEn(_:)), for: .touchUpInside)
                    cell.btnMr.addTarget(self, action: #selector(onClickFatherNameEn(_:)), for: .touchUpInside)
                    cell.btnDr.addTarget(self, action: #selector(onClickFatherNameEn(_:)), for: .touchUpInside)
                    cell.btnUMy.addTarget(self, action: #selector(onClickFatherNameMy(_:)), for: .touchUpInside)
                    cell.btnMrMy.addTarget(self, action: #selector(onClickFatherNameMy(_:)), for: .touchUpInside)
                    cell.btnClose.addTarget(self, action: #selector(onClickFatherNameClose(_:)), for: .touchUpInside)
                    if kycFatherNAme && btnEdit.isHidden {
                        cell.isUserInteractionEnabled = true
                      //  cell.btnClose.isHidden = false
                    }else {
                        cell.isUserInteractionEnabled = false
                        cell.btnClose.isHidden = true
                    }
                    if btnEdit.isHidden {
                        cell.lblRequired.textColor = UIColor.red
                    }else {
                        cell.lblRequired.textColor = UIColor.darkGray
                    }
                    return cell
                }else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "NRCPassportUPCell", for: indexPath) as! NRCPassportUPCell
                    if btnEdit.isHidden {
                        cell.lblRequired.textColor = UIColor.red
                        cell.lblRequiredFront.textColor = UIColor.red
                        cell.lblRequiredBack.textColor = UIColor.red
                    }else {
                        cell.lblRequired.textColor = UIColor.darkGray
                        cell.lblRequiredFront.textColor = UIColor.darkGray
                        cell.lblRequiredBack.textColor = UIColor.darkGray
                    }
                    return NRCPassportWrapCell(index: indexPath, cell: cell)
                }
            }else if indexPath.row == 6 || indexPath.row == 7 {
                if indexPath.row == 6 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "NameInputUPCell", for: indexPath) as! NameInputUPCell
                     cell.selectionStyle = .none
                    let dic = personalInfo[indexPath.row]
                    cell.imgIcon.image = UIImage(named: dic["icon"] as! String)
                    cell.tfTitle.tag = 06
                    cell.tfTitle.delegate = self
                    cell.lblRequired.isHidden = false
                    cell.tfTitle.text = getDateFormateSring(date: dic["title"] as? String ?? "")
                    cell.lblStatus.text = dic["statusTitle"] as? String
                    cell.btnRightArrow.isHidden = true
                    cell.lblStatus.textColor = .darkGray
                    
                    var font = UIFont(name: appFont, size: 18.0)
                    if #available(iOS 13.0, *) {
                        font = UIFont.systemFont(ofSize: 18)
                    }
                    cell.tfTitle.font = font
                    
                    if ok_default_language == "my" {
                        cell.tfTitle.font = UIFont(name: appFont, size: 18)
                    }else if ok_default_language == "en"{
                       cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                    }else {
                        if #available(iOS 13.0, *) {
                            let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                              .font : UIFont(name: appFont, size: 18)]
                            cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "Select Date of Expiry", attributes:attributes as [NSAttributedString.Key : Any])
                        } else {
                            let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                              .font : UIFont.systemFont(ofSize: 18)]
                            cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "Select Date of Expiry", attributes:attributes)
                            cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                        }
                        cell.tfTitle.font = UIFont(name: appFont, size: 18)
                    }
                    
                    cell.tfTitle.placeholder = "Select Date of Expiry".localized
                    if PersonalProfileModelUP.shared.NrcExpireDate == "" {
                        cell.tfTitle.text = ""
                    }else {
                        cell.tfTitle.text = getDateFormateSring(date: PersonalProfileModelUP.shared.NrcExpireDate)
                    }
                    
                    
                    if kycPassportExpDate && btnEdit.isHidden {
                        cell.isUserInteractionEnabled = true
                    }else {
                        cell.isUserInteractionEnabled = false
                    }
                    if btnEdit.isHidden {
                        cell.lblRequired.textColor = UIColor.red
                    }else {
                        cell.lblRequired.textColor = UIColor.darkGray
                    }
                    return cell
                }else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "FatherNameUpCell", for: indexPath) as! FatherNameUpCell
                    cell.selectionStyle = .none
                   
                    let title = PersonalProfileModelUP.shared.FatherName.components(separatedBy: ",")
                    if title.count > 1 {
                    cell.lblTitle.text = title[0] + ","
                    prefixFatherName = title[0] + ","
                    cell.tfFatherName.text = title[1]
                    }else {
                      cell.tfFatherName.text = PersonalProfileModelUP.shared.FatherName
                    }
                    //cell.tfFatherName.addTarget(self, action: #selector(userFatherNameEditing(_:)), for: .editingChanged)
                    cell.tfFatherName.delegate = self
                    
                    if cell.tfFatherName.text == "" {
                        cell.lblTitle.text = ""
                        cell.btnClose.isHidden = true
                    }else {
                        cell.btnClose.isHidden = false
                    }
                    
//                    if kycFatherNAme {
//                      cell.btnClose.isHidden = false
//                    }else {
//                     cell.btnClose.isHidden = true
//                    }
                    cell.btnU.addTarget(self, action: #selector(onClickFatherNameEn(_:)), for: .touchUpInside)
                    cell.btnMr.addTarget(self, action: #selector(onClickFatherNameEn(_:)), for: .touchUpInside)
                    cell.btnDr.addTarget(self, action: #selector(onClickFatherNameEn(_:)), for: .touchUpInside)
                    cell.btnUMy.addTarget(self, action: #selector(onClickFatherNameMy(_:)), for: .touchUpInside)
                    cell.btnMrMy.addTarget(self, action: #selector(onClickFatherNameMy(_:)), for: .touchUpInside)
                    cell.btnClose.addTarget(self, action: #selector(onClickFatherNameClose(_:)), for: .touchUpInside)
                    if kycFatherNAme && btnEdit.isHidden {
                       // cell.btnClose.isHidden = false
                        cell.isUserInteractionEnabled = true
                    }else {
                        cell.isUserInteractionEnabled = false
                        cell.btnClose.isHidden = true
                    }
                    if btnEdit.isHidden {
                        cell.lblRequired.textColor = UIColor.red
                    }else {
                        cell.lblRequired.textColor = UIColor.darkGray
                    }
                    return cell
                }
            }
        }else if indexPath.section == 2 {
                if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "NameActionUPCell", for: indexPath) as! NameActionUPCell
                    if btnEdit.isHidden {
                        cell.lblRequired.textColor = UIColor.red
                    }else {
                        cell.lblRequired.textColor = UIColor.darkGray
                    }
                    let dicData = AddressInfo[indexPath.row]
                    return NameActionWrapCell(index: indexPath, cell: cell, dic: dicData as! Dictionary<String, String>)
                }else if indexPath.row == 3 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "NameUPCell", for: indexPath) as! NameUPCell
                    cell.lblStaus.textColor = .darkGray
                    let dicData = AddressInfo[indexPath.row]
                    return nameWrapCell(index: indexPath, cell: cell, dic: dicData as! Dictionary<String, String>)
                }else if indexPath.row == 4 || indexPath.row == 5 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "NameInputUPCell", for: indexPath) as! NameInputUPCell
                    cell.selectionStyle = .none
               
                    if indexPath.row == 4 {
                        var font = UIFont(name: appFont, size: 18.0)
                        if #available(iOS 13.0, *) {
                            font = UIFont.systemFont(ofSize: 18)
                        }
                        cell.tfTitle.font = font
                        cell.lblStatus.textColor = .darkGray
                        
                       cell.tfTitle.tag = 400
                        if ok_default_language == "my" {
                            cell.tfTitle.font = UIFont(name: appFont, size: 18)
                        }else if ok_default_language == "en"{
                            cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                        }else {
                            if #available(iOS 13.0, *) {
                                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                                  .font : UIFont(name: appFont, size: 18)]
                                cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "Village Tract", attributes:attributes as [NSAttributedString.Key : Any])
                            } else {
                                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                                  .font : UIFont.systemFont(ofSize: 18)]
                                cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "Village Tract", attributes:attributes)
                                cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                            }
                            cell.tfTitle.font = UIFont(name: appFont, size: 18)
                        }
                        cell.tfTitle.placeholder = "Village Tract".localized
                        cell.tfTitle.addTarget(self, action: #selector(villageTextFieldEditing(_:)), for: .editingChanged)
                        cell.lblRequired.isHidden = true
                    }else {
                        var font = UIFont(name: appFont, size: 18.0)
                        if #available(iOS 13.0, *) {
                            font = UIFont.systemFont(ofSize: 18)
                        }
                        cell.tfTitle.font = font
                        cell.lblStatus.textColor = .darkGray
                        
                        cell.tfTitle.tag = 500
                        if ok_default_language == "my" {
                            cell.tfTitle.font = UIFont(name: appFont, size: 18)
                        }else if ok_default_language == "en"{
                            cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                        }else {
                            if #available(iOS 13.0, *) {
                                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                                  .font : UIFont(name: appFont, size: 18)]
                                cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "Enter street", attributes:attributes as [NSAttributedString.Key : Any])
                            } else {
                                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                                  .font : UIFont.systemFont(ofSize: 18)]
                                cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "Enter street", attributes:attributes)
                                cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                            }
                            cell.tfTitle.font = UIFont(name: appFont, size: 18)
                        }
                        cell.tfTitle.placeholder = "Enter street".localized
                        cell.tfTitle.addTarget(self, action: #selector(streetTextFieldEditing(_:)), for: .editingChanged)
                        cell.lblRequired.isHidden = false
                    }

                    let dic = AddressInfo[indexPath.row]
                    cell.imgIcon.image = UIImage(named: dic["icon"] as! String)
                    cell.tfTitle.text = dic["title"] as? String ?? ""
                     cell.tfTitle.delegate = self
                    cell.lblStatus.text = dic["statusTitle"] as? String
                    cell.lblStatus.textColor = .darkGray
                    if indexPath.row == 4 && !isVillageAvaible {
                       cell.isHidden = true
                    }else {
                        cell.isHidden = false
                    }
                     cell.btnRightArrow.isHidden = true
                    if btnEdit.isHidden {
                        cell.isUserInteractionEnabled = true
                    }else {
                        cell.isUserInteractionEnabled = false
                    }
                    if btnEdit.isHidden {
                        cell.lblRequired.textColor = UIColor.red
                    }else {
                        cell.lblRequired.textColor = UIColor.darkGray
                    }
                    return cell
                }else if indexPath.row == 6 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "AddressUPCell", for: indexPath) as! AddressUPCell
                    cell.selectionStyle = .none
                    cell.tfHouse.tag = 600
                    cell.tfHouse.text = PersonalProfileModelUP.shared.HouseNumber
                    cell.tfHouse.delegate = self
                    cell.tfFloor.tag = 700
                    cell.tfFloor.text = PersonalProfileModelUP.shared.FloorNumber
                    cell.tfFloor.delegate = self
                    cell.tfRoom.tag = 800
                    cell.tfRoom.text = PersonalProfileModelUP.shared.RoomNumber
                    cell.tfRoom.delegate = self
                    if btnEdit.isHidden {
                        cell.isUserInteractionEnabled = true
                    }else {
                        cell.isUserInteractionEnabled = false
                    }
                    cell.lblRequired.isHidden = true
                    return cell
                }else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "NameInputUPCell", for: indexPath) as! NameInputUPCell
                    let dic = AddressInfo[indexPath.row]
                    cell.tfTitle.tag = 900
                    cell.imgIcon.image = UIImage(named: dic["icon"] as! String)
                    cell.lblStatus.textColor = .darkGray
                    
                    if ok_default_language == "my" {
                        cell.tfTitle.font = UIFont(name: appFont, size: 18)
                    }else if ok_default_language == "en"{
                        cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                    }else {
                        if #available(iOS 13.0, *) {
                            let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                              .font : UIFont.init(name: appFont, size: 18)]
                            cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "housing/industrial Zone name", attributes:attributes as [NSAttributedString.Key : Any])
                            cell.tfTitle.font =  UIFont.init(name: appFont, size: 18)
                        }else {
                            let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                              .font : UIFont.systemFont(ofSize: 18)]
                            cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "housing/industrial Zone name", attributes:attributes)
                            cell.tfTitle.font =  UIFont.init(name: appFont, size: 18)
                        }
                    }
                    
                    cell.tfTitle.text = dic["title"] as? String ?? ""
                    cell.tfTitle.placeholder = "Housing Zone Name".localized
                    cell.tfTitle.delegate = self
                    cell.lblStatus.text = dic["statusTitle"] as? String
                    cell.btnRightArrow.isHidden = true
                    cell.lblRequired.isHidden = true
                    if btnEdit.isHidden {
                        cell.isUserInteractionEnabled = true
                    }else {
                        cell.isUserInteractionEnabled = false
                    }
                    if btnEdit.isHidden {
                        cell.lblRequired.textColor = UIColor.red
                    }else {
                        cell.lblRequired.textColor = UIColor.darkGray
                    }
                    return cell
                }
        }else if indexPath.section == 3 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NameActionUPCell", for: indexPath) as! NameActionUPCell
                cell.selectionStyle = .none
                let dic = securityList[indexPath.row]
                cell.imgIcon.image = UIImage(named: dic["icon"] as! String)
                cell.lblTitle.text = dic["title"] as? String
               // DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
               //  cell.lblTitle.text = dic["title"] as? String
               // })
                cell.lblStatus.text = dic["statusTitle"] as? String
                cell.lblStatus.textColor = .darkGray
                cell.imgRightArrow.isHidden = true
                cell.lblRequired.isHidden = false
                cell.lblTitle.isUserInteractionEnabled = true
                let tapAction = UITapGestureRecognizer(target: self, action:#selector(onClickSecurityQuestionAction(_:)))
                cell.lblTitle?.addGestureRecognizer(tapAction)
                if btnEdit.isHidden {
                    cell.isUserInteractionEnabled = true
                }else {
                    cell.isUserInteractionEnabled = false
                }
                if btnEdit.isHidden {
                    cell.lblRequired.textColor = UIColor.red
                }else {
                    cell.lblRequired.textColor = UIColor.darkGray
                }
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NameInputUPCell", for: indexPath) as! NameInputUPCell
                cell.selectionStyle = .none
                let dic = securityList[indexPath.row]
                cell.imgIcon.image = UIImage(named: dic["icon"] as! String)
                cell.tfTitle.text = dic["title"] as? String
                cell.tfTitle.delegate = self
                
                if ok_default_language == "my" {
                    cell.tfTitle.font = UIFont(name: appFont, size: 18)
                }else if ok_default_language == "en"{
                    cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                }else {
                    if #available(iOS 13.0, *) {
                        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                          .font : UIFont(name: appFont, size: 18)]
                        cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "Enter Answer", attributes:attributes as [NSAttributedString.Key : Any])
                        cell.tfTitle.font = UIFont(name: appFont, size: 18.0)
                    } else {
                        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                          .font : UIFont.systemFont(ofSize: 18)]
                        cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "Enter Answer", attributes:attributes)
                        cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                    }
                     cell.tfTitle.font = UIFont(name: appFont, size: 18.0)
                }
                
                cell.tfTitle.placeholder = "Enter Answer".localized
                cell.tfTitle.tag = 1000
                cell.lblStatus.text = dic["statusTitle"] as? String
                cell.lblStatus.textColor = .darkGray
                cell.btnRightArrow.isHidden = true
                cell.lblRequired.isHidden = false
                
                if cell.tfTitle.text == ""{
                    cell.lblStatus.isHidden = true
                }else{
                    cell.lblStatus.isHidden = false
                }
                
                if btnEdit.isHidden {
                    cell.isUserInteractionEnabled = true
                }else {
                    cell.isUserInteractionEnabled = false
                }
                if btnEdit.isHidden {
                    cell.lblRequired.textColor = UIColor.red
                }else {
                    cell.lblRequired.textColor = UIColor.darkGray
                }
                return cell
            }
        }else if indexPath.section == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactUPCell", for: indexPath) as! ContactUPCell
            //cell.lblRequired.isHidden = false
            let dicData = AlternetPersonal[indexPath.row]
            if btnEdit.isHidden {
                cell.isUserInteractionEnabled = true
            }else {
                cell.isUserInteractionEnabled = false
            }
            return contactWrapCell(index: indexPath, cell: cell, dic: dicData as! Dictionary<String, String>)
        }else if indexPath.section == 5 {
            let dicData = ContactDetails[indexPath.row]
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ContactUPCell", for: indexPath) as! ContactUPCell
                if btnEdit.isHidden {
                    cell.isUserInteractionEnabled = true
                }else {
                    cell.isUserInteractionEnabled = false
                }
                return contactWrapCell(index: indexPath, cell: cell, dic: dicData as! Dictionary<String, String>)
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NameInputUPCell", for: indexPath) as! NameInputUPCell
                if btnEdit.isHidden {
                    cell.isUserInteractionEnabled = true
                }else {
                    cell.isUserInteractionEnabled = false
                }
                cell.selectionStyle = .none
                cell.imgIcon.image = UIImage(named: dicData["icon"] as! String)
                cell.tfTitle.text = dicData["title"] as? String
                cell.tfTitle.delegate = self
                
//                if cell.tfTitle.text == ""{
//                    cell.lblStatus.isHidden = true
//                }else{
//                    cell.lblStatus.isHidden = false
//                }
//
                if indexPath.row == 1 {
                    var font = UIFont(name: appFont, size: 18.0)
                    if #available(iOS 13.0, *) {
                        font = UIFont.systemFont(ofSize: 18)
                    }
                    cell.tfTitle.font = font
                    
                    cell.tfTitle.tag = 1100
                    if ok_default_language == "my" {
                        cell.tfTitle.font = UIFont(name: appFont, size: 18)
                    }else if ok_default_language == "en"{
                        cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                    }else {
                        if #available(iOS 13.0, *) {
                            let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                              .font : UIFont(name: appFont, size: 18)]
                            cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "Enter Email ID", attributes:attributes as [NSAttributedString.Key : Any])
                        }else {
                            let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                              .font : UIFont.systemFont(ofSize: 18)]
                            cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "Enter Email ID", attributes:attributes)
                            cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                        }
                        cell.tfTitle.font = UIFont(name: appFont, size: 18)
                    }
                    cell.tfTitle.placeholder = "Enter Email ID".localized
                    cell.lblRequired.isHidden = true
                    if PersonalProfileModelUP.shared.EmailId == ""{
                        cell.lblStatus.isHidden = true
                    }
                    else{
                        cell.lblStatus.isHidden = false
                    }
                    
                    
                }else {
                    var font = UIFont(name: appFont, size: 18.0)
                    if #available(iOS 13.0, *) {
                        font = UIFont.systemFont(ofSize: 18)
                    }
                    cell.tfTitle.font = font
                    
                    cell.tfTitle.tag = 1200
                    if ok_default_language == "my" {
                        cell.tfTitle.font = UIFont(name: appFont, size: 18)
                    }else if ok_default_language == "en"{
                        cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                    }else {
                        if #available(iOS 13.0, *) {
                            let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                              .font : UIFont(name: appFont, size: 18)]
                            cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "Enter Facebook ID", attributes:attributes as [NSAttributedString.Key : Any])
                        } else {
                            let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                              .font : UIFont.systemFont(ofSize: 18)]
                            cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "Enter Facebook ID", attributes:attributes)
                            cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                        }
                        cell.tfTitle.font = UIFont(name: appFont, size: 18)
                    }
                    cell.tfTitle.placeholder = "Enter Facebook ID".localized
                    cell.lblRequired.isHidden = true
                }
                cell.lblStatus.text = dicData["statusTitle"] as? String
                cell.lblStatus.textColor = .darkGray
                cell.btnRightArrow.isHidden = true
                
                if PersonalProfileModelUP.shared.FacebookId == ""{
                    cell.lblStatus.isHidden = true
                }
                else{
                    cell.lblStatus.isHidden = false
                }
                
                
                
                if btnEdit.isHidden {
                    cell.lblRequired.textColor = UIColor.red
                }else {
                    cell.lblRequired.textColor = UIColor.darkGray
                }
                return cell
            }
        }else if indexPath.section == 6 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "StatusUPCell", for: indexPath) as! StatusUPCell
            if btnEdit.isHidden {
                cell.isUserInteractionEnabled = true
            }else {
                cell.isUserInteractionEnabled = false
            }
            var totalCount = 0
            if UserModel.shared.agentType == .user {
                if PersonalProfileModelUP.shared.IdType == "01" {
                    totalCount = 13
                }else {
                    totalCount = 15
                }
                let score : Double = Double(myProfileStatus)
                let total : Double = Double(totalCount)
                let percent : Double = Double((score / total) * 100)
                myProfileStatusInPercent = Int(percent)
                cell.lblSecondLine.text = "100 % Personal profile \n update is required to available complete OK$ service".localized

               cell.lblFillStatus.text = String(myProfileStatus) + "/" + String(totalCount) + " " + "( " + "\(String(format: "%.0f%", percent) + "% ")" + ")"
                
                cell.lblStatusPercentage.text = "0%" //String(format: "%.0f%", percent) + "% "
                cell.progess.progress = Float(percent/100)
            }else {
                if PersonalProfileModelUP.shared.IdType == "01" {
                    totalCount = 13
                }else {
                    totalCount = 15
                }
                cell.lblSecondLine.text = "100 % Merchant profile \n update is required to available complete OK$ service".localized
               // cell.lblFillStatus.text = String(myProfileStatus) + "/" + String(totalCount)
                let score : Double = Double(myProfileStatus)
                let total : Double = Double(totalCount)
                let percent : Double = Double((score / total) * 100)
                myProfileStatusInPercent = Int(percent)
                
                cell.lblFillStatus.text = String(myProfileStatus) + "/" + String(totalCount) + " " + "( " + "\(String(format: "%.0f%", percent) + "% ")" + ")"
                 
                cell.lblStatusPercentage.text = "0%"//String(format: "%.0f%", percent) + "% "
                cell.progess.progress = Float(percent/100)
            }
            cell.selectionStyle = .none
            return cell
        }else {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "StatusButtonUPCell", for: indexPath) as! StatusButtonUPCell
//            cell.btnUpdate.setTitle("Update".localized, for: .normal)
//            cell.btnUpdate.addTarget(self, action: #selector(onClickUpdateAction(_:)), for: .touchUpInside)
//            if btnEdit.isHidden {
//               cell.isHidden = false
//            }else{
//               cell.isHidden = true
//            }
            //return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "StatusButtonUPCell", for: indexPath) as! StatusButtonUPCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Indexpath \(indexPath.row)")
    }
    
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                return 100
            }else {
                if  editselect == "usernameShow"{
                    return 70
                }
                else{
                    return 0
                }
               
            }
        }else if indexPath.section == 1 {
            if PersonalProfileModelUP.shared.IdType == "01" {
                if indexPath.row == 4 {
                    return 149
                }else {
                    return 70
                }
            }else {
                if indexPath.row == 5 {
                    return 149
                }else {
                    return 70
                }
            }
        }else if indexPath.section == 2 {
            if indexPath.row == 4 && !isVillageAvaible {
                return 0
            }else {
                if indexPath.row == 0 {
                    return 0
                }
                return 70
            }
        }else if indexPath.section == 3 {
             return 70
        }else if indexPath.section == 4 {
//                        if UserModel.shared.agentType == .user {
//                            return 70
//                        }else {
//                            return 0
//                        }
            return 0
        }else if indexPath.section == 5 {
            if indexPath.row == 0 {
                return 0
            }else {
                return 70
            }
        }else if indexPath.section == 6 {
             return 140
        }else if indexPath.section == 7 {
            if btnEdit.isHidden {
                return 50
            }else{
                return 0
            }
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 || section == 2 || section == 3 || section == 5 || section == 6 {
            return 35
        }else if section == 4 {
//            if UserModel.shared.agentType == .user {
//                return 35
//            }else{
//                return 0
//            }
            return 0
        }else {
          return 0
        }
    }
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            var title = ""
            if UserModel.shared.agentType == .user {
                title = "Personal Account Information".localized
            }else {
               title = "Merchant Account Information".localized
            }
        return createRequiredFieldLabel(headerTitle: title, reqTitle: "Required fields*".localized)
        }else if section == 2 {
            var title = ""
            if UserModel.shared.agentType == .user {
                title = "Home Address Details".localized
            }else {
                title = "Home Address Details".localized
            }
            return createRequiredFieldLabel(headerTitle: title, reqTitle: "")
        }else if section == 3 {
            return createRequiredFieldLabel(headerTitle: "Select Security Question".localized, reqTitle: "".localized)
        }else if section == 4 {
            if UserModel.shared.agentType == .user {
                     return createRequiredFieldLabel(headerTitle: "Alternate Mobile Number".localized, reqTitle: "".localized)
            }else {
                let view = UIView(frame: CGRect(x: 0, y: 0, width:0, height: 0))
                return view
            }
        }else if section == 5 {
            return createRequiredFieldLabel(headerTitle: "My Contact Details".localized, reqTitle: "".localized)
        }else if section == 6 {
            var stTitle  = ""
            if UserModel.shared.agentType == .user {
                stTitle = "Personal Profile Status".localized
            }else {
                stTitle = "Merchant Profile Status".localized
            }

            return createRequiredFieldLabel(headerTitle: stTitle, reqTitle: "".localized)
        }else {
            let view = UIView(frame: CGRect(x: 0, y: 0, width:0, height: 0))
            return view
        }
    }
    
    
    
    private func createRequiredFieldLabel(headerTitle:String, reqTitle: String) ->UIView? {
        let view1 = UIView(frame: CGRect(x: 5, y: 0, width:self.view.frame.width - 10, height: 35))
        view1.backgroundColor = hexStringToUIColor(hex: "#E0E0E0")
        let lblHeader = UILabel(frame: CGRect(x: 10, y: 0, width: (sizeOfString(myString: headerTitle)?.width)!, height:35))
        lblHeader.text = headerTitle
        lblHeader.textAlignment = .left
        lblHeader.font = UIFont(name: appFont, size: 14)
        lblHeader.backgroundColor = UIColor.clear
        lblHeader.textColor = UIColor.black
        
        let reqLabel = UILabel(frame: CGRect(x: self.view.frame.width - (sizeOfString(myString: reqTitle)?.width)! - 10, y: 0, width: (sizeOfString(myString: reqTitle)?.width)!, height:35))
        reqLabel.text = reqTitle
        reqLabel.textAlignment = .right
        reqLabel.font = UIFont(name: appFont, size: 14)
        reqLabel.backgroundColor = UIColor.clear
        reqLabel.textColor = UIColor.red
        view1.addSubview(lblHeader)
        view1.addSubview(reqLabel)
        return view1
    }
    
    private func sizeOfString(myString : String) ->CGSize? {
        let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: appFont, size: 14) ?? UIFont.systemFont(ofSize: 16.0) ])
        return stringSize
    }
    
    
    private func nameWrapCell(index: IndexPath, cell: NameUPCell, dic: Dictionary<String,String>) -> NameUPCell {
        cell.selectionStyle = .none
        cell.imgIcon.image = UIImage(named: dic["icon"] ?? "")
        cell.lblStaus.text = dic["statusTitle"] ?? ""
         cell.lblRightTitle.text = ""
        cell.lblStaus.textColor = .darkGray
        if index.row == 0 && index.section == 1 {
            cell.lblTitle.text = dic["title"]
        }else if index.row == 1 && index.section == 1 {
            let (mobileNumber,CountryCode) = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: PersonalProfileModelUP.shared.OkAccNumber)
            cell.lblTitle.text = "(" + CountryCode + ")" + mobileNumber
            let countryData = identifyCountry(withPhoneNumber: CountryCode)
            if let safeImage = UIImage(named: countryData.countryFlag) {
                DispatchQueue.main.async {
                    cell.imgIcon.contentMode = .scaleAspectFit
                    cell.imgIcon.image = safeImage
                }
            }
        }else if index.row == 2 && index.section == 1 {
            cell.lblTitle.text = getDateFormateSringForJoinDate(date: dic["title"] ?? "")
            if getYearDifferace(date: dic["title"] ?? "") == "0 days" {
                cell.lblRightTitle.text = "Today"
            }else {
                cell.lblRightTitle.text = getYearDifferace(date: dic["title"] ?? "")
            }
        }else {
            cell.lblTitle.text = dic["title"] ?? ""
        }
        return cell
    }
    private func NameActionWrapCell(index: IndexPath, cell: NameActionUPCell, dic: Dictionary<String,String>) -> NameActionUPCell {
        cell.selectionStyle = .none
        cell.lblStatus.text = dic["statusTitle"] ?? ""
        cell.imgIcon.image = UIImage(named: dic["icon"] ?? "")
        cell.lblTitle.text = dic["title"] ?? ""
        cell.lblStatus.textColor = .darkGray
        
        if index.row == 0 || index.row == 1 || index.row == 2 {
                cell.imgRightArrow.isHidden = false
                cell.lblRequired.isHidden = false
            }else {
                cell.imgRightArrow.isHidden = true
                cell.lblRequired.isHidden = true
            }
        if index.row == 0 {
            cell.imgRightArrow.isHidden = true
            cell.isHidden = true
        }
        
        cell.lblTitle.isUserInteractionEnabled = true
        cell.imgRightArrow.isUserInteractionEnabled = true
        let tapAction = UITapGestureRecognizer(target: self, action:#selector(onClickAction(_:)))
        let tapAction1 = UITapGestureRecognizer(target: self, action:#selector(onClickAction(_:)))
        if index.row == 0 {
            tapAction.name = "Address Type"
            tapAction1.name = "Address Type"
        }else if index.row == 1 {
            tapAction.name = "Division / State"
            tapAction1.name = "Division / State"
        }else if index.row == 2 {
           tapAction.name = "Township"
            tapAction1.name = "Township"
        }
        cell.lblTitle?.addGestureRecognizer(tapAction)
        cell.imgRightArrow?.addGestureRecognizer(tapAction1)
        if btnEdit.isHidden {
            cell.isUserInteractionEnabled = true
        }else {
            cell.isUserInteractionEnabled = false
        }
        
        return cell
    }
    
    private func NRCPassportWrapCell(index: IndexPath, cell: NRCPassportUPCell) -> NRCPassportUPCell  {
        cell.selectionStyle = .none
        cell.tfNrcPassportNumber.delegate = self
        cell.tfNrcPassportNumber.myDelegate = self
        cell.tfNrcPassportNumber.tag = 69
        let buttonName = cell.btnNCRNumber.titleLabel?.text
        
        if ok_default_language == "my" {
            cell.tfNrcPassportNumber.font = UIFont(name: appFont, size: 18)
        }else {
           cell.tfNrcPassportNumber.font = UIFont.systemFont(ofSize: 18)
        }
        
        if PersonalProfileModelUP.shared.IdType == "01" {
            cell.imgIcon.image = UIImage(named: "nrc")
            cell.lblStatus.text = "NRC Number and Image".localized
            cell.lblStatus.textColor = .darkGray
            cell.tfNrcPassportNumber.font = UIFont(name: appFont, size: 17)
            
            if nrcPrefix == "" {
                cell.btnCloseNRC.isHidden  = true
                cell.btnNCRNumber.isHidden = false
                cell.btnNCRNumber.setTitle("Enter NRC Number".localized, for: .normal)
                PersonalProfileModelUP.shared.NrcNumber = ""
                nrcPrefix = ""
            }else if buttonName?.contains(find: "/") ?? false {
                
            }else {
                cell.btnNCRNumber.isHidden = true
                let myString = nrcPrefix
                let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18.0) ])
                btnnrc.frame = CGRect(x: 0, y: -2, width:stringSize.width, height: 52)
                btnnrc.setTitleColor(.black, for: .normal)
                btnnrc.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
                btnnrc.addTarget(self, action: #selector(BtnNrcTypeAction(button:)), for: .touchUpInside)
                btnnrc.titleEdgeInsets = UIEdgeInsets(top: 0, left: -10, bottom: -4, right: 0)
                btnnrc.setTitle(myString, for: UIControl.State.normal)
                let btnView = UIView()
                btnView.frame = CGRect(x: 0, y: 0, width: btnnrc.frame.width , height: 52)
                btnView.addSubview(btnnrc)
                cell.tfNrcPassportNumber.addTarget(self, action: #selector(nrcNumberEdting(_:)), for: .editingChanged)
                cell.tfNrcPassportNumber.leftView = btnView
                cell.tfNrcPassportNumber.leftViewMode = UITextField.ViewMode.always
                cell.btnCloseNRC.isHidden  = false
            }
            cell.tfNrcPassportNumber.text = nrcPostfix
            cell.btnCloseNRC.addTargetClosure(closure: {_ in
                cell.tfNrcPassportNumber.text = ""
                cell.tfNrcPassportNumber.leftView = nil
                cell.tfNrcPassportNumber.resignFirstResponder()
                cell.btnCloseNRC.isHidden  = true
                cell.btnNCRNumber.setTitle("Enter NRC Number".localized, for: .normal)
                cell.btnNCRNumber.isHidden = false
                PersonalProfileModelUP.shared.NrcNumber = ""
                self.nrcPrefix = ""
                self.nrcPostfix = ""
            })
            
            if PersonalProfileModelUP.shared.NrcNumber == ""{
                cell.btnCloseNRC.isHidden = true
            }else{
                cell.btnCloseNRC.isHidden = false
            }
         
            if PersonalProfileModelUP.shared.NrcImage1 == "" {
                cell.imgFront.isHidden = true
            }else {
                cell.imgFront.isHidden = false
                let urlFront = URL(string: PersonalProfileModelUP.shared.NrcImage1)
                cell.imgFront.sd_setImage(with: urlFront, placeholderImage: UIImage(named: ""))
            }
            if PersonalProfileModelUP.shared.NrcImage2 == "" {
                cell.imgRear.isHidden = true
            }else {
                cell.imgRear.isHidden = false
                let urlRear = URL(string: PersonalProfileModelUP.shared.NrcImage2)
                cell.imgRear.sd_setImage(with: urlRear, placeholderImage: UIImage(named: ""))
            }
            cell.lblbFront.text = "Front Side".localized
            cell.lblRear.text = "Back Side".localized
        }else {
            cell.tfNrcPassportNumber.text = PersonalProfileModelUP.shared.NrcNumber
            cell.imgIcon.image = UIImage(named: "passport")
            cell.lblStatus.text = "Passport Number & Images".localized
            cell.lblStatus.textColor = .darkGray
            if PersonalProfileModelUP.shared.NrcImage1 == "" {
                cell.imgFront.isHidden = true
            }else {
                cell.imgFront.isHidden = false
                let urlFront = URL(string: PersonalProfileModelUP.shared.NrcImage1)
                cell.imgFront.sd_setImage(with: urlFront, placeholderImage: UIImage(named: ""))
            }
            if PersonalProfileModelUP.shared.NrcImage2 == "" {
                cell.imgRear.isHidden = true
            }else {
                cell.imgRear.isHidden = false
                let urlRear = URL(string: PersonalProfileModelUP.shared.NrcImage2)
                cell.imgRear.sd_setImage(with: urlRear, placeholderImage: UIImage(named: ""))
            }
            cell.lblbFront.text = "Main Page".localized
            cell.lblRear.text = "Validity Page".localized
        }
        
        cell.btnFrontAction.addTarget(self, action: #selector(frontPhoto(_:)), for: .touchUpInside)
        cell.btnRearAction.addTarget(self, action: #selector(rearPhoto(_:)), for: .touchUpInside)
        cell.btnInformation.addTarget(self, action: #selector(nrcPassportInformaiton(_:)), for: .touchUpInside)
        cell.btnNCRNumber.addTarget(self, action: #selector(onClickNRCNumerAction(_:)), for: .touchUpInside)
        
        if btnEdit.isHidden && kycNrcPassportNumber {
            cell.tfNrcPassportNumber.isUserInteractionEnabled = true
            
            if PersonalProfileModelUP.shared.IdType == "01" {
               // cell.btnCloseNRC.isHidden = false
                if nrcPrefix == "" {
                    cell.btnNCRNumber.isHidden = false
                }else {
                    cell.btnNCRNumber.isHidden = true
                }
            }else {
                cell.btnNCRNumber.isHidden = true
            }
        }else {
            cell.btnCloseNRC.isHidden = true
            cell.tfNrcPassportNumber.isUserInteractionEnabled = false
        }
        return cell
    }
    
    private func contactWrapCell(index: IndexPath, cell: ContactUPCell, dic: Dictionary<String,String>) -> ContactUPCell {
         cell.isHidden = true
        cell.selectionStyle = .none
        if index.section == 4 {
        cell.tfContactNumber.placeholder = "Alternate Number".localized
         cell.tfContactNumber.tag = 24
        }else if index.section == 5 {
        cell.tfContactNumber.placeholder = "Referral Number".localized
        cell.tfContactNumber.tag = 25
        }
        cell.btnClose.tag = index.section
        cell.btnContact.tag = index.section
       
        cell.tfContactNumber.delegate = self
        cell.lblStatus.text = dic["statusTitle"]
        cell.lblStatus.textColor = .darkGray
        cell.tfContactNumber.keyboardType = .numberPad
        cell.btnClose.addTarget(self, action: #selector(onClickClearAction(_:)), for: .touchUpInside)
        cell.btnContact.addTarget(self, action: #selector(onClickContactAction(_:)), for: .touchUpInside)
        cell.tfContactNumber.addTarget(self, action: #selector(contactNumberEditing(_:)), for: .editingChanged)
        cell.imgCountry.isUserInteractionEnabled = true
        let tapAction = UITapGestureRecognizer(target: self, action:#selector(onClickCountryAction(_:)))
        tapAction.name = String(index.section)
        cell.imgCountry?.addGestureRecognizer(tapAction)
        
        let title = dic["title"] ?? ""
        if title.count < 4 {
           let code = dic["country_Code"] ?? ""
            cell.lblCountrycode.text = "(" + code + ")"
            cell.tfContactNumber.text = title
            cell.imgCountry.image = UIImage(named: dic["icon"] ?? "myanmar")
             cell.btnClose.isHidden = true
        }else {
            let (mobileNumber,CountryCode) = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: dic["title"] ?? "")
            cell.lblCountrycode.text = "(" + CountryCode + ")"
            cell.tfContactNumber.text = mobileNumber
            let countryData = identifyCountry(withPhoneNumber: CountryCode)
            
            if index.section == 4 {
                countryCodeAlternate = CountryCode
                flagCodeAlternate = countryData.countryFlag
            }else if index.section == 5 {
                countryCodeReferral = CountryCode
                flagCodeReferral = countryData.countryFlag
            }
        
            if let safeImage = UIImage(named: countryData.countryFlag) {
                DispatchQueue.main.async {
                    cell.imgCountry.image  = safeImage
                }
            }else {
                cell.imgCountry.image = UIImage(named: "myanmar")
            }
            cell.btnClose.isHidden = false
        }

        if btnEdit.isHidden {
            cell.lblRequired.textColor = UIColor.red
        }else {
            cell.lblRequired.textColor = UIColor.darkGray
        }
        return cell
    }
    
    private func showCountryList(showFor: String) {
        let sb = UIStoryboard(name: "Country", bundle: nil)
        let countryVC = sb.instantiateViewController(withIdentifier: "CountryViewController") as! CountryViewController
        countryVC.delegate = self
        countryVC.regiCheck = "AllCountry"
        countryVC.modalPresentationStyle = .fullScreen
        self.navigationController?.present(countryVC, animated: true, completion: nil)
        countryListShownFor = showFor
    }
    
}

extension PersonalDetailsUP : CameraVCUPDelegate {
    @objc func onClickUserImage() {
        if kycProfileImage && btnEdit.isHidden {
            let cameraVC = self.storyboard?.instantiateViewController(withIdentifier: "CameraVCUP") as! CameraVCUP
            cameraVC.delegate = self
            self.navigationController?.pushViewController(cameraVC, animated: true)
        }else if btnEdit.isHidden {
             DispatchQueue.main.async {
            let zoomImageUP = self.storyboard?.instantiateViewController(withIdentifier: "ZoomImageUP") as! ZoomImageUP
            let image1 = UIImage()
            let url1Exits = PersonalProfileModelUP.shared.ProfilePicture
            if url1Exits != "" {
                if let img = image1.convertURLToImage(str: url1Exits) {
                    zoomImageUP.myImage = img
                    zoomImageUP.headerTitile = "Profile Picture".localized
                    self.navigationController?.pushViewController(zoomImageUP, animated: true)
                }
            }
            }
        }
        
    }
    func updateProflePic(imageUrl: String, realImage: UIImage) {
        PersonalProfileModelUP.shared.ProfilePicture = imageUrl.replacingOccurrences(of: " ", with: "%20")
        if let cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 0, section: 0)) as? ProfilePicUPCell {
            DispatchQueue.main.async {
                cell.imgProfile.sd_setImage(with: URL(string: PersonalProfileModelUP.shared.ProfilePicture), placeholderImage: UIImage(named: "userTRN"))
            }
        }
    }

    @objc func onClickUserNameMale(_ sender: UIButton) {
    let cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 1, section: 0)) as? UserNameUPCell
        cell?.viewMaleFemale.isHidden = true
        cell?.imgUser.image = UIImage(named: "r_user")
        cell?.imgUser.isHidden = false
        PersonalProfileModelUP.shared.Gender = "Male"
        
        if ok_default_language == "my" || ok_default_language == "uni"{
            cell?.btnFU.setTitle("U".localized, for: .normal)
            cell?.btnFMr.setTitle("Mg".localized, for: .normal)
            cell?.viewMaleFemaleMy.isHidden = false
        }else {
            cell?.viewMaleEn.isHidden = false
        }
    }
    
    @objc func onClickUserNameFemale(_ sender: UIButton) {
        let cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 1, section: 0)) as? UserNameUPCell
        cell?.viewMaleFemale.isHidden = true
        cell?.imgUser.image = UIImage(named: "r_female")
        cell?.imgUser.isHidden = false
        PersonalProfileModelUP.shared.Gender = "Female"
        
        if ok_default_language == "my" || ok_default_language == "uni"{
            cell?.btnFU.setTitle("Daw".localized, for: .normal)
            cell?.btnFMr.setTitle("Ma".localized, for: .normal)
            cell?.viewMaleFemaleMy.isHidden = false
        }else {
            cell?.viewFemaleEn.isHidden = false
        }
    }
    
    
    @objc func onClickUserNameMaleEn(_ sender: UIButton) {
         let cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 1, section: 0)) as? UserNameUPCell
        cell?.viewMaleEn.isHidden = true
        cell?.viewMaleFemale.isHidden = true
        isYourNameKeyboardHidden = false
        cell?.btnclose.isHidden = false
        let title = sender.title(for: .normal) ?? ""
        cell?.lbltitle.text = title + ","
        prefixUserName = title + ","
        cell?.tfUserName.becomeFirstResponder()
    }
    
    @objc func onClickUserNameFemaleEn(_ sender: UIButton) {
         let cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 1, section: 0)) as? UserNameUPCell
        cell?.viewFemaleEn.isHidden = true
        cell?.viewMaleFemale.isHidden = true
        isYourNameKeyboardHidden = false
         cell?.btnclose.isHidden = false
        let title = sender.title(for: .normal) ?? ""
        cell?.lbltitle.text = title + ","
        prefixUserName = title + ","
        cell?.tfUserName.becomeFirstResponder()
    }
    
    @objc func onClickUserNameMaleFemaleMy(_ sender: UIButton) {
         let cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 1, section: 0)) as? UserNameUPCell
        cell?.viewFemaleEn.isHidden = true
        cell?.viewMaleFemale.isHidden = true
        cell?.viewMaleFemaleMy.isHidden = true
        isYourNameKeyboardHidden = false
        cell?.btnclose.isHidden = false
        cell?.btnclose.isHidden = false
        let title = sender.title(for: .normal) ?? ""
        cell?.lbltitle.text = title + ","
        prefixUserName = title + ","
        cell?.tfUserName.becomeFirstResponder()
    }
    
    @objc func onClickUserNameClose(_ sender: UIButton) {
         let cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 1, section: 0)) as? UserNameUPCell
        cell?.tfUserName.text = ""
        cell?.lbltitle.text = ""
        prefixUserName = ""
        cell?.tfUserName.resignFirstResponder()
        cell?.viewMaleFemale.isHidden = true
        cell?.viewMaleEn.isHidden = true
        cell?.viewFemaleEn.isHidden = true
        cell?.viewMaleFemaleMy.isHidden = true
        cell?.btnclose.isHidden = true
        isYourNameKeyboardHidden  = true
        PersonalProfileModelUP.shared.ProfileName = ""
        cell?.imgUser.isHidden = false
        cell?.imgUser.image = UIImage(named: "r_user")
        myProfileStatus = updateProfileStatusCount()
        reloadRows(index: 0, withSeciton: 6)
    }

    @objc func onClickFatherNameEn(_ sender: UIButton) {
        let cell: FatherNameUpCell?
        if PersonalProfileModelUP.shared.IdType == "04" {
            cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 7, section: 1)) as? FatherNameUpCell
        }else {
            cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 5, section: 1)) as? FatherNameUpCell
        }
        cell?.viewFatherEn.isHidden = true
        cell?.viewFatherMy.isHidden = true
        cell?.btnClose.isHidden = false
        isFatherNameKeyboardHidden = false
        let title = sender.title(for: .normal) ?? ""
        cell?.lblTitle.text = title + ","
        prefixFatherName = title + ","
        cell?.tfFatherName.becomeFirstResponder()
        
    }
    
    @objc func onClickFatherNameMy(_ sender: UIButton) {
        
        let cell: FatherNameUpCell?
        if PersonalProfileModelUP.shared.IdType == "04" {
            cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 7, section: 1)) as? FatherNameUpCell
        }else {
            cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 5, section: 1)) as? FatherNameUpCell
        }
        cell?.viewFatherEn.isHidden = true
        cell?.viewFatherMy.isHidden = true
        cell?.btnClose.isHidden = false
        isFatherNameKeyboardHidden = false
        let title = sender.title(for: .normal) ?? ""
        
        cell?.lblTitle.text = title + ","
        prefixFatherName = title + ","
        cell?.tfFatherName.becomeFirstResponder()
    }
    
    @objc func onClickFatherNameClose(_ sender: UIButton) {
        let cell: FatherNameUpCell?
        if PersonalProfileModelUP.shared.IdType == "04" {
            cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 7, section: 1)) as? FatherNameUpCell
        }else {
            cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 5, section: 1)) as? FatherNameUpCell
        }
        cell?.tfFatherName.text = ""
        cell?.lblTitle.text = ""
        prefixFatherName = ""
        cell?.tfFatherName.resignFirstResponder()
        cell?.viewFatherEn.isHidden = true
        cell?.viewFatherMy.isHidden = true
        cell?.btnClose.isHidden = true
        isFatherNameKeyboardHidden  = true
        PersonalProfileModelUP.shared.FatherName = ""
        myProfileStatus = updateProfileStatusCount()
        if PersonalProfileModelUP.shared.IdType == "04" {
            reloadRows(index: 7, withSeciton: 1)
        }else {
            reloadRows(index: 5, withSeciton: 1)
        }
    }
  
}


extension PersonalDetailsUP {
    @objc func onClickAction(_ reconiger: UITapGestureRecognizer) {
        if reconiger.name == "Nationality" {
            self.showCountryList(showFor: "Nationality")
        }else if reconiger.name == "Address Type" {
            let address = self.storyboard?.instantiateViewController(withIdentifier: "AddressTypeUP") as! AddressTypeUP
            address.viewController = "personal"
            address.list = ["Home".localized,"Business".localized,"Other".localized]
            address.setMarqueLabelInNavigation(title: "Address Type")
            address.delegate = self
           // self.navigationController?.pushViewController(address, animated: true)
        }else if reconiger.name == "Division / State" {
            let sb = UIStoryboard.init(name: "Registration", bundle: nil)
            let stateDivisionVC  = sb.instantiateViewController(withIdentifier: "StateDivisionVC") as! StateDivisionVC
            stateDivisionVC.stateDivisonVCDelegate = self
            self.navigationController?.pushViewController(stateDivisionVC, animated: true)
        }else if reconiger.name == "Township" {
            let sb = UIStoryboard.init(name: "Registration", bundle: nil)
            let addressTownshiopVC  = sb.instantiateViewController(withIdentifier: "AddressTownshiopVC") as! AddressTownshiopVC
            addressTownshiopVC.addressTownshiopVCDelegate = self
            addressTownshiopVC.selectedDivState = locationDetails
            self.navigationController?.pushViewController(addressTownshiopVC, animated: true)
        }
    }
    
    @objc func frontPhoto(_ sender: UIButton) {
      
        
        
        if kycNrcPassportIds && btnEdit.isHidden{
               self.openNewUIImages()
        }else if btnEdit.isHidden {
            
            DispatchQueue.main.async {
                let zoomImageUP = self.storyboard?.instantiateViewController(withIdentifier: "ZoomImageUP") as! ZoomImageUP
                let image1 = UIImage()
                let url1Exits = PersonalProfileModelUP.shared.NrcImage1
                if url1Exits != "" {
                    if let img = image1.convertURLToImage(str: url1Exits) {
                        zoomImageUP.myImage = img
                        var title = ""
                        if PersonalProfileModelUP.shared.IdType == "01" {
                            title = "Front Page".localized
                        }else {
                            title = "Front Side".localized
                        }
                        zoomImageUP.headerTitile = title
                        self.navigationController?.pushViewController(zoomImageUP, animated: true)
                    }
                }
            }
        }
    }
    
    @objc func rearPhoto(_ sender: UIButton) {
        if kycNrcPassportIds && btnEdit.isHidden {
            self.openNewUIImages()
        }else if btnEdit.isHidden {
            DispatchQueue.main.async {
                let zoomImageUP = self.storyboard?.instantiateViewController(withIdentifier: "ZoomImageUP") as! ZoomImageUP
                let image1 = UIImage()
                let url1Exits = PersonalProfileModelUP.shared.NrcImage2
                if url1Exits != "" {
                    if let img = image1.convertURLToImage(str: url1Exits) {
                        zoomImageUP.myImage = img
                        var title = ""
                        if PersonalProfileModelUP.shared.IdType == "01" {
                            title = "Back Page".localized
                        }else {
                            title = "Back Side".localized
                        }
                        zoomImageUP.headerTitile = title
                        self.navigationController?.pushViewController(zoomImageUP, animated: true)
                    }
                }
            }
        }
    }
    
    private func openNewUIImages() {
    
        DispatchQueue.main.async {
            let image1 = UIImage()
            let image2 = UIImage()
            var imgList = [UIImage]()
            let url1Exits = PersonalProfileModelUP.shared.NrcImage1
            let url2Exits = PersonalProfileModelUP.shared.NrcImage2
            
            if self.imgFirst != nil {
                imgList.append(self.imgFirst)
            }else {
                if url1Exits != "" {
                    if let img = image1.convertURLToImage(str: url1Exits) {
                        imgList.append(img)
                    }
                }
            }
           
            if self.imgSecond != nil {
                imgList.append(self.imgSecond)
            }else {
                if url2Exits != "" {
                    if let img = image2.convertURLToImage(str: url2Exits) {
                        imgList.append(img)
                    }}
            }
            
            let imageVC = UIStoryboard(name: "MyProfile", bundle: Bundle.main).instantiateViewController(withIdentifier: "UploadImagesUP")  as! UploadImagesUP
            if PersonalProfileModelUP.shared.IdType == "01" {
                imageVC.setMarqueLabelInNavigation(title: "NRC Images".localized)
                imageVC.buttonName = ["Tap to take Photo of your NRC's front side".localized, "Tap to take Photo of your NRC's back side".localized]
                imageVC.buttonTitleName =  ["Front side of your NRC".localized,"Back side of your NRC".localized]
            }else {
                imageVC.setMarqueLabelInNavigation(title: "Passport Images".localized)
                imageVC.buttonName = ["Tap to take Photo of your Passport's front side".localized, "Tap to take Photo of your Passport's back side".localized]
                imageVC.buttonTitleName =  ["Front side of your Passport".localized,"Back side of your Passport".localized]
            }
            imageVC.imageList = imgList
            imageVC.imagesURL = [PersonalProfileModelUP.shared.NrcImage1,PersonalProfileModelUP.shared.NrcImage2]
            imageVC.delegate = self
            self.showProgressView()
            self.navigationController?.pushViewController(imageVC, animated: true)
        }
    }
    
    @objc func nrcPassportInformaiton(_ sender: UIButton) {
        
        if PersonalProfileModelUP.shared.IdType == "01"{
            alertViewObj.wrapAlert(title:"", body: "NRC ID is required to verify your account as per rules & Regulations of Central Bank of Myanmar".localized, img: UIImage(named: "infoQR")!)
        }else{
            alertViewObj.wrapAlert(title:"", body: "Passport ID is required to verify your account as per rules & Regulations of Central Bank of Myanmar".localized, img: UIImage(named: "infoQR")!)
        }
    
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
          
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    @objc func onClickNRCNumerAction(_ sender: UIButton) {
        
        if sender.titleLabel?.text == "Enter NRC Number".localized {
            let sb = UIStoryboard(name: "Registration", bundle: nil)
            let townVC = sb.instantiateViewController(withIdentifier: "TownshipVC") as! TownshipVC
            townVC.townShipDelegate = self
            self.navigationController?.pushViewController(townVC, animated: false)
        }else {
            let sb = UIStoryboard(name: "Registration", bundle: nil)
            let townshipVC2 = sb.instantiateViewController(withIdentifier: "TownshipVC2") as! TownshipVC2
            townshipVC2.selectedDivState = locationDetailsNRC
            townshipVC2.townshipVC2Delegate = self
            self.navigationController?.pushViewController(townshipVC2, animated: true)
        }
        
    }
    
    @objc func onClickSecurityQuestionAction(_ reconiger: UITapGestureRecognizer) {
        self.view.endEditing(true)
//        let sb = UIStoryboard(name: "Registration" , bundle: nil)
//        let securityQuestionR  = sb.instantiateViewController(withIdentifier: "SecurityQuestionR") as! SecurityQuestionR
//        securityQuestionR.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
//        securityQuestionR.securityQuestionRDelegate = self
//        securityQuestionR.modalPresentationStyle = overCurrentContext
//        self.present(securityQuestionR, animated: false, completion: nil)
//
        let sb = UIStoryboard(name: "ReRegistration" , bundle: nil)
        let securityQuestionR  = sb.instantiateViewController(withIdentifier: "SecurityQuestionVCREgi") as! SecurityQuestionVCREgi
        securityQuestionR.delegate = self
        self.navigationController?.pushViewController(securityQuestionR, animated: true)
        
        
    }
    @objc func onClickClearAction(_ sender: UIButton) {
        let indexPath = IndexPath(row: 0, section: sender.tag)
        let cell: ContactUPCell = self.tbUpdateProfile.cellForRow(at: indexPath) as! ContactUPCell
        if cell.imgCountry.image == UIImage(named: "myanmar") {
            cell.tfContactNumber.text = "09"
        }else {
             cell.tfContactNumber.text = ""
        }
        if sender.tag == 4 {
        PersonalProfileModelUP.shared.AlternatePhonenumber = ""
        }else if sender.tag == 5 {
        PersonalProfileModelUP.shared.Recommended = ""
        }
        sender.isHidden = true
    }
    
    @objc func onClickContactAction(_ sender: UIButton) {
        let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
        self.navigationController?.present(nav, animated: true, completion: nil)
        contactListShownFor = String(sender.tag)
    }

    @objc func onClickCountryAction(_ reconiger: UITapGestureRecognizer) {
        self.showCountryList(showFor: reconiger.name ?? "")
    }
}


extension PersonalDetailsUP : TownShipVcDeletage,NRCTypesVCDelegate,TownshipVC2Delegate {
    func NRCValueString(nrcSting: String) {
        if let cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 4, section: 1)) as? NRCPassportUPCell {
             self.setNRCDetails(nrcDetails: nrcSting, cell: cell)
        }
    }
    
 
    
    func setDivisionAndTownship(strDivison_township: String) {
        if let cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 4, section: 1)) as? NRCPassportUPCell {
            self.setNRCDetails(nrcDetails: strDivison_township, cell: cell)
        }
    }
    
    func setDivison(division: LocationDetail) {
        // After Selecting NRC Divison
        locationDetailsNRC = division
        var divStr = ""
        if ok_default_language == "my"{
            divStr = division.nrcCodeNumberMy + "/"
        }else if ok_default_language == "en"{
            divStr = division.nrcCodeNumber + "/"
        }else {
            divStr = division.nrcCodeNumberUni + "/"
        }
        if let cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 4, section: 1)) as? NRCPassportUPCell {
            nrcPrefix = divStr
          //  cell.btnNCRNumber.setTitle(divStr, for: .normal)
            cell.tfNrcPassportNumber.text = ""
        }
    }
    private func setNRCDetails(nrcDetails : String, cell: NRCPassportUPCell) {
        
     //   DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
        let myString = nrcDetails
        
        if myString == ""{
            cell.btnCloseNRC.isHidden  = true
            cell.btnNCRNumber.isHidden = false
            cell.btnNCRNumber.setTitle("Enter NRC Number".localized, for: .normal)
            PersonalProfileModelUP.shared.NrcNumber = ""
            self.nrcPrefix = ""
        }
        else{
            let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18.0) ])
            self.btnnrc.frame = CGRect(x: 0, y: -2, width:stringSize.width, height: 52)
            self.btnnrc.setTitleColor(.black, for: .normal)
            self.btnnrc.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
            self.btnnrc.addTarget(self, action: #selector(self.BtnNrcTypeAction(button:)), for: .touchUpInside)
            self.btnnrc.setTitle(nrcDetails, for: UIControl.State.normal)
            self.nrcPrefix = nrcDetails
            let btnView = UIView()
            btnView.frame = CGRect(x: 0, y: 0, width: self.btnnrc.frame.width , height: 52)
            btnView.addSubview(self.btnnrc)
            cell.tfNrcPassportNumber.leftView = btnView
            cell.tfNrcPassportNumber.leftViewMode = UITextField.ViewMode.always
            cell.btnNCRNumber.isHidden = true
            cell.btnCloseNRC.isHidden = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
            self.showNRCSuggestion(cell: cell)
            })
        }
    //    })
    }
    
    @objc func BtnNrcTypeAction(button : UIButton) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
        if let cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 4, section: 1)) as? NRCPassportUPCell {
             self.showNRCSuggestion(cell: cell)
        }
        })
    }
    
    func setNrcTypeName(nrcType: String) {
        btnnrc.setTitle(nrcType, for: .normal)
        if let cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: 4, section: 1)) as? NRCPassportUPCell {
            if (cell.tfNrcPassportNumber.text?.length)! < 6 {
                cell.tfNrcPassportNumber.text = ""
                cell.tfNrcPassportNumber.becomeFirstResponder()
            }
        }
    }
    
    func showNRCSuggestion(cell : NRCPassportUPCell){
        self.view.endEditing(true)
        let sb = UIStoryboard(name: "Registration" , bundle: nil)
        let nRCTypesVC  = sb.instantiateViewController(withIdentifier: "NRCTypesVC") as! NRCTypesVC
        nRCTypesVC.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        nRCTypesVC.setName(nrcName: (btnnrc.titleLabel?.text)!)
        nRCTypesVC.nRCTypesVCDelegate = self
        nRCTypesVC.modalPresentationStyle = .overCurrentContext
        self.present(nRCTypesVC, animated: false, completion: nil)

    }
   
}

extension PersonalDetailsUP : CountryViewControllerDelegate {
    func countryViewController(_ list: CountryViewController, country: Country) {
        var indexpath: Int = 0
        var Section: Int = 0
        if countryListShownFor == "Nationality" {
            indexpath = 4
            Section = 1
            let dic = personalInfo[indexpath]
            PersonalProfileModelUP.shared.CountryOfCitizen = country.name
            dic["title"] =  country.name
        }else if countryListShownFor == "4" {
            Section = 4
             indexpath = 0
            let dic = AlternetPersonal[0]
            if country.dialCode == "+95" {
                dic["title"] = "09"
            }else {
                dic["title"] = ""
            }
            flagCodeAlternate = country.code
            countryCodeAlternate = country.dialCode
            dic["icon"] = flagCodeAlternate
            dic["country_Code"] = countryCodeAlternate
        }else if countryListShownFor == "5" {
            Section = 5
             indexpath = 0
            let dic = ContactDetails[0]
            if country.dialCode == "+95" {
                dic["title"] = "09"
            }else {
                dic["title"] = ""
            }
            flagCodeReferral = country.code
            countryCodeReferral = country.dialCode
            dic["icon"] = flagCodeReferral
            dic["country_Code"] = countryCodeReferral
        }
        self.reloadRows(index: indexpath, withSeciton: Section)
        list.dismiss(animated: true, completion: nil)
    }

    func countryViewControllerCloseAction(_ list: CountryViewController) {
        list.dismiss(animated: true, completion: nil)
    }
}



extension PersonalDetailsUP : StateDivisionVCDelegate,AddressTownshiopVCDelegate,AddressTypeUPDelegate {

    func setDivisionStateName(SelectedDic: LocationDetail) {
       locationDetails = SelectedDic
        let dic = AddressInfo[1]
        if ok_default_language == "my" {
          dic["title"] = SelectedDic.stateOrDivitionNameMy
        }else  if ok_default_language == "en"{
            dic["title"] = SelectedDic.stateOrDivitionNameEn
        }else {
            dic["title"] = SelectedDic.stateOrDivitionNameUni
        }
        PersonalProfileModelUP.shared.Division = SelectedDic.stateOrDivitionCode
        reloadRows(index: 1, withSeciton: 2)
        
        let dic2 = AddressInfo[2]
        PersonalProfileModelUP.shared.Township = ""
        dic2["title"] = "Select Township".localized
        reloadRows(index: 2, withSeciton: 2)
        
        let dic3 = AddressInfo[3]
        PersonalProfileModelUP.shared.City = ""
        dic3["title"] = "City Name".localized
        reloadRows(index: 3, withSeciton: 2)
        
        myProfileStatus = updateProfileStatusCount()
        reloadRows(index: 0, withSeciton: 6)
    }
    
    func setTownshipCityNameFromDivison(Dic: TownShipDetailForAddress) {
         self.setTownShip(township: Dic)
    }
    
    func setTownshipCityName(cityDic: TownShipDetailForAddress) {
        self.setTownShip(township: cityDic)
    }
    
    private func setTownShip(township: TownShipDetailForAddress) {
        //township
        let dic = AddressInfo[2]
        if ok_default_language == "my" {
          dic["title"] = township.townShipNameMY
        }else if ok_default_language == "en"{
          dic["title"] = township.townShipNameEN
        }else {
            dic["title"] = township.townShipNameUni
        }
        PersonalProfileModelUP.shared.Township = township.townShipCode
        reloadRows(index: 2, withSeciton: 2)
   
        //City
        if ok_default_language == "my" {
            if township.GroupName == "" {
                PersonalProfileModelUP.shared.City = township.cityNameMY
            }else {
                if township.isDefaultCity == "1" {
                    PersonalProfileModelUP.shared.City = township.DefaultCityNameMY
                }else{
                    PersonalProfileModelUP.shared.City = township.cityNameMY
                }
            }
        }else if ok_default_language == "en"{
            if township.GroupName == "" {
               PersonalProfileModelUP.shared.City = township.cityNameEN
            }else {
                if township.isDefaultCity == "1" {
                    PersonalProfileModelUP.shared.City = township.DefaultCityNameEN
                }else{
                    PersonalProfileModelUP.shared.City = township.cityNameEN
                }
            }
        }else {
            if township.GroupName == "" {
                PersonalProfileModelUP.shared.City = township.cityNameUni
            }else {
                if township.isDefaultCity == "1" {
                    PersonalProfileModelUP.shared.City = township.DefaultCityNameUni
                }else{
                    PersonalProfileModelUP.shared.City = township.cityNameUni
                }
            }
        }

        let dic1 = AddressInfo[3]
        dic1["title"] = PersonalProfileModelUP.shared.City
        reloadRows(index: 3, withSeciton: 2)

        isVillageAvaible = true
        if township.GroupName.contains(find: "YCDC") || township.GroupName.contains(find: "MCDC") {
            isVillageAvaible = false
        }
        
        if township.cityNameEN == "Amarapura" || township.cityNameEN == "Patheingyi" || township.townShipNameEN == "PatheinGyi" {
            isVillageAvaible = true
        }
        //village
            let dic2 = AddressInfo[4]
            dic2["title"] = ""
            PersonalProfileModelUP.shared.Village = ""
            reloadRows(index: 4, withSeciton: 2)
        //Street
        let dic3 = AddressInfo[5]
        dic3["title"] = ""
         PersonalProfileModelUP.shared.Street = ""
        reloadRows(index: 4, withSeciton: 2)
        reloadRows(index: 5, withSeciton: 2)
        
        //Show Village list
        self.callStreetVillageAPI(townshipcode: township.townShipCode)
        myProfileStatus = updateProfileStatusCount()
        reloadRows(index: 0, withSeciton: 6)
    }

    func selectedAddress(name: String, number: Int) {
        let dic = AddressInfo[0]
        dic["title"] = name
        PersonalProfileModelUP.shared.AddressType = name
        reloadRows(index: 0, withSeciton: 2)
    }
    
}

extension PersonalDetailsUP : SecurityQuestionRDelegate,SecurityQuestionVCREgiDelegate {
    func SelectedQuestion(questionDic: Dictionary<String, String>) {
        var question = ""
        if ok_default_language == "my" {
            question = questionDic["QuestionBurmese"] ?? ""
        }else if ok_default_language == "en"{
            question = questionDic["Question"] ?? ""
        }else {
            question = questionDic["QuestionBurmeseUniCode"] ?? ""
        }
        PersonalProfileModelUP.shared.SecurityQuestion =  questionDic["QuestionCode"] ?? ""
        PersonalProfileModelUP.shared.SecrityAnswer = ""
        
        if let cell = tbUpdateProfile.cellForRow(at: IndexPath(row: 0, section: 3)) as? NameActionUPCell {
            let dic = securityList[0]
            dic["title"] = question
            cell.lblTitle.text = question
            cell.lblRequired.isHidden = false
        }
        
        if let cell = tbUpdateProfile.cellForRow(at: IndexPath(row: 1, section: 3)) as? NameInputUPCell {
            let dic1 = securityList[1]
            dic1["title"] = ""
            cell.tfTitle.text = ""
            cell.tfTitle.becomeFirstResponder()
        }
//        reloadRows(index: 0, withSeciton: 3)
//        reloadRows(index: 1, withSeciton: 3)
    }
    
    func SelectedQuestion(questionDic: Dictionary<String, AnyObject>) {
        var question = ""
        if ok_default_language == "my" {
            question = questionDic["QuestionBurmese"] as? String ?? ""
        }else if ok_default_language == "en"{
            question = questionDic["Question"] as? String ?? ""
        }else {
           question = questionDic["QuestionBurmeseUniCode"] as? String ?? ""
        }
        PersonalProfileModelUP.shared.SecurityQuestion =  questionDic["QuestionCode"] as? String ?? ""
        PersonalProfileModelUP.shared.SecrityAnswer = ""
        
        if let cell = tbUpdateProfile.cellForRow(at: IndexPath(row: 0, section: 3)) as? NameActionUPCell {
            let dic = securityList[0]
            dic["title"] = question
            cell.lblTitle.text = question
            cell.lblRequired.isHidden = false
        }
        
        if let cell = tbUpdateProfile.cellForRow(at: IndexPath(row: 1, section: 3)) as? NameInputUPCell {
            let dic1 = securityList[1]
            dic1["title"] = ""
            cell.tfTitle.text = ""
            cell.tfTitle.becomeFirstResponder()
        }
//        reloadRows(index: 0, withSeciton: 3)
//        reloadRows(index: 1, withSeciton: 3)
        
    }
}

extension PersonalDetailsUP : UploadImagesUPDelegate {
    func sendImagesToMainController(images: [String]) {
        let url1 = images[0].replacingOccurrences(of: " ", with: "%20")
        PersonalProfileModelUP.shared.NrcImage1 = url1
         let url2 = images[1].replacingOccurrences(of: " ", with: "%20")
         PersonalProfileModelUP.shared.NrcImage2 = url2
   
        
        
        var index = 0
        if PersonalProfileModelUP.shared.IdType == "01" {
            index = 4
        }else {
            index = 5
        }
        if let cell = self.tbUpdateProfile.cellForRow(at: IndexPath(row: index, section: 1)) as? NRCPassportUPCell {
            let urlFront = URL(string: PersonalProfileModelUP.shared.NrcImage1)
            cell.imgFront.isHidden = false
            cell.imgFront.sd_setImage(with: urlFront, placeholderImage: UIImage(named: ""))
            let urlRear = URL(string: PersonalProfileModelUP.shared.NrcImage2)
            cell.imgRear.isHidden = false
            cell.imgRear.sd_setImage(with: urlRear, placeholderImage: UIImage(named: ""))
        }
        myProfileStatus = updateProfileStatusCount()
        self.reloadRows(index: 0, withSeciton: 6)
        
        DispatchQueue.main.async {
            let image1 = UIImage()
            let image2 = UIImage()
            if PersonalProfileModelUP.shared.NrcImage1 != "" {
                if let img = image1.convertURLToImage(str: PersonalProfileModelUP.shared.NrcImage1) {
                    self.imgFirst = img
                }
            }
            
            if PersonalProfileModelUP.shared.NrcImage2 != "" {
                if let img = image2.convertURLToImage(str: PersonalProfileModelUP.shared.NrcImage2) {
                    self.imgSecond = img
                }
            }
        }
   
    }
    
}

