//
//  DataPersonalUP.swift
//  OK
//
//  Created by Imac on 3/7/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

extension PersonalDetailsUP: WebServiceResponseDelegate {
 
    func getSecurityQuestion() {
        var urlString   = Url.URL_ViewSecurityQuestions + "MobileNumber=" + PersonalProfileModelUP.shared.OkAccNumber + "&Simid=" + uuid + "&MSID=" + msid + "&OSType=" + "1" + "&OTP=" + uuid
        urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let urlS = getUrl(urlStr: urlString, serverType: .serverApp)
        println_debug(urlS)
        let param = [String:String]() as AnyObject
        
        TopupWeb.genericClass(url:urlS , param: param, httpMethod: "GET", handle:{ (response , success) in
            if success {
                if let json = response as? Dictionary<String,Any> {
                    if let code = json["Code"] as? NSNumber, code == 200 {
                        if PersonalProfileModelUP.shared.KycStatus != 1 {
                            self.initialPopUp()
                        }
                        
                        if let que = json["Data"] as? String {
                            if let dic = OKBaseController.convertToDictionary(text: que) {
                                var question = ""
                                if ok_default_language == "my"{
                                    question = (dic["QuestionBurmese"] as? String)!
                                }else if ok_default_language == "en"{
                                    question = (dic["Question"] as? String)!
                                }else {
                                    question = (dic["QuestionBurmeseUniCode"] as? String)!
                                }
                                let dic = self.securityList[0]
                                dic["title"] = question
                            }
                        }
                    }else {
                        if let msg = json["Msg"] as? String {
                            self.showErrorAlert(errMessage: msg)
                        }
                    }
                }
            }
        })
    }
    
    func callStreetVillageAPI(townshipcode : String){
        let urlStr   = Url.URL_VillageList + townshipcode
        let url = getUrl(urlStr: urlStr, serverType: .serverApp)
       // let url = URL(string: "https://www.okdollar.co/RestService.svc/GetVillageStreetByTownShipCode?TownshipCode=" + townshipcode)
        let params = Dictionary<String,String>()
        TopupWeb.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", handle: {response, isSuccess in
            if isSuccess {
                if let json = response as? Dictionary<String,Any> {
                    let response = json["Data"] as Any
                    let dic = OKBaseController.convertToDictionary(text: response as! String)
                    if let d = dic {
                        if let arrDic = d["Streets"] as? Array<Dictionary<String,String>> {
                            let streetList = StreetList()
                            streetList.isStreetList = true
                            streetList.streetArray = arrDic
                            VillageManager.shared.allStreetList = streetList
                            
                        }else{
                            let streetList = StreetList()
                            streetList.streetArray.removeAll()
                            streetList.isStreetList = false
                            VillageManager.shared.allStreetList = streetList
                        }
                        if let arrDic1 = d["Villages"] as? Array<Dictionary<String,String>>{
                            DispatchQueue.main.async {
                                let villageList = VillageList()
                                villageList.villageArray = arrDic1
                                VillageManager.shared.allVillageList = villageList
                                villageList.isVillageList = true
                            }
                        }else{
                            DispatchQueue.main.async {
                                let villageList = VillageList()
                                villageList.villageArray.removeAll()
                                VillageManager.shared.allVillageList = villageList
                                villageList.isVillageList = false
                            }
                        }
                        println_debug(VillageManager.shared.allVillageList)
                    }
                }
            }else {
                DispatchQueue.main.async {
                self.showAlert(alertTitle: "", alertBody: "Network error and try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
                }
            }
        })
    }
    
    
    func updateProfileStatusCount() -> Int {
        var countStatus = [Int]()
        if PersonalProfileModelUP.shared.IdType == "01" {
            countStatus = [0,0,0,0,0,0,0,0,0,0,0,0,0] //16-3 = 13   // after remove alternate and email id, referral number
        }else {
            countStatus = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0] //18-3 = 15
        }

       if PersonalProfileModelUP.shared.ProfilePicture != "" {
           countStatus[0] = 1
        }

        if PersonalProfileModelUP.shared.ProfileName.contains(find: ",") {
            let token = PersonalProfileModelUP.shared.ProfileName.components(separatedBy: ",")
            if token[1].count > 2 {
                countStatus[1] = 1
            }
        }else if PersonalProfileModelUP.shared.ProfileName.contains(find: ".") {
            let token = PersonalProfileModelUP.shared.ProfileName.components(separatedBy: ".")
            if token[1].count > 2 {
                countStatus[1] = 1
            }
        }
        
        if PersonalProfileModelUP.shared.Dob != "" {
            countStatus[2] = 1
        }
        if PersonalProfileModelUP.shared.Gender != "" {
            countStatus[3] = 1
        }

        if PersonalProfileModelUP.shared.FatherName.contains(find: ",") {
            let token = PersonalProfileModelUP.shared.FatherName.components(separatedBy: ",")
            if token[1].count > 2 {
                countStatus[4] = 1
            }
        }else if PersonalProfileModelUP.shared.FatherName.contains(find: ".") {
            let token = PersonalProfileModelUP.shared.FatherName.components(separatedBy: ".")
            if token[1].count > 2 {
                countStatus[4] = 1
            }
        }
        
        if PersonalProfileModelUP.shared.NrcImage1 != "" {
            countStatus[5] = 1
        }
        if PersonalProfileModelUP.shared.NrcImage2 != "" {
            countStatus[6] = 1
        }
        if kycNrcPassportNumber {
           if PersonalProfileModelUP.shared.IdType == "04" {
                if PersonalProfileModelUP.shared.NrcNumber != "" {
                    countStatus[7] = 1
                }
           }else {
            if nrcPostfix != "" {
                countStatus[7] = 1
            }
            }
        }else {
            if PersonalProfileModelUP.shared.NrcNumber != "" {
                countStatus[7] = 1
            }
        }
      
        
//        if PersonalProfileModelUP.shared.AddressType != "" {
//            countStatus[8] = 1
//        }
        
        if PersonalProfileModelUP.shared.IdType == "Home".localized {
            if PersonalProfileModelUP.shared.Division != "" {
                countStatus[8] = 1
            }
            if PersonalProfileModelUP.shared.Township != "" {
                countStatus[9] = 1
            }
            if PersonalProfileModelUP.shared.Street != "" {
                countStatus[10] = 1
            }
        }else {
            countStatus[8] = 1
            countStatus[9] = 1
            countStatus[10] = 1
        }

        if PersonalProfileModelUP.shared.SecurityQuestion != "" {
            countStatus[11] = 1
        }
        if PersonalProfileModelUP.shared.SecrityAnswer != "" {
            countStatus[12] = 1
        }

//        if PersonalProfileModelUP.shared.EmailId != "" {
//            countStatus[13] = 1
//        }
        if PersonalProfileModelUP.shared.IdType == "04" {
            if PersonalProfileModelUP.shared.CountryOfCitizen != "" {
                countStatus[13] = 1
            }
            if PersonalProfileModelUP.shared.NrcExpireDate != "" {
                countStatus[14] = 1
            }
        }

        var finalCount = 0
        for item in countStatus {
            if item == 1 {
                finalCount += 1
            }
        }
        return finalCount
    }
    
    
    func checkRequiredFields(handler: (_ message:String, _ isBool: Bool)-> Void) {
        var emptyFields = ""
        
        
        if kycProfileImage {
            if PersonalProfileModelUP.shared.ProfilePicture == "" {
                emptyFields = emptyFields + " " + "Profile Image".localized
            }
        }
        
        if kycUserName {
            if PersonalProfileModelUP.shared.ProfileName == "" {
                emptyFields = "User Name".localized
            }else if PersonalProfileModelUP.shared.ProfileName.contains(find: ",") {
                let token = PersonalProfileModelUP.shared.ProfileName.components(separatedBy: ",")
                if token[1].count < 3 {
                    emptyFields = emptyFields + "\n" + "User Name must be more than 2 characters".localized
                }
            }else if PersonalProfileModelUP.shared.ProfileName.contains(find: ".") {
                let token = PersonalProfileModelUP.shared.ProfileName.components(separatedBy: ".")
                if token[1].count < 3 {
                    emptyFields = emptyFields + "\n" + "User Name must be more than 2 characters".localized
                }
            }
        }
        if kycFatherNAme {
            if PersonalProfileModelUP.shared.FatherName == "" {
                emptyFields = "Father Name".localized
            }else if PersonalProfileModelUP.shared.FatherName.contains(find: ",") {
                let token = PersonalProfileModelUP.shared.FatherName.components(separatedBy: ",")
                if token[1].count < 3 {
                    emptyFields = emptyFields + "\n" + "Father Name must be more than 2 characters".localized
                }
            }else if PersonalProfileModelUP.shared.FatherName.contains(find: ".") {
                let token = PersonalProfileModelUP.shared.FatherName.components(separatedBy: ".")
                if token[1].count < 3 {
                    emptyFields = emptyFields + "\n" + "Father Name must be more than 2 characters".localized
                }
            }
        }
        
      //  if kycDOB {
        if PersonalProfileModelUP.shared.Dob == "" {
            emptyFields = emptyFields + " " + "Date of Birth".localized
        }
      //  }
        
        let dic = AddressInfo[5]
        
        if dic["title"] as? String ?? "" == "" {
            emptyFields = emptyFields + "Please enter Street".localized
        }
        
        if kycNrcPassportNumber && (PersonalProfileModelUP.shared.IdType == "01") {
            let nrc =  nrcPrefix.replacingOccurrences(of: "/", with: "@")
           PersonalProfileModelUP.shared.NrcNumber = nrc + nrcPostfix
        }
        
        if PersonalProfileModelUP.shared.NrcNumber == ""{
            emptyFields = emptyFields + "\n" + "".localized
        }

        if PersonalProfileModelUP.shared.IdType == "01" {
            if PersonalProfileModelUP.shared.NrcNumber == "" {
                emptyFields = emptyFields + "\n" + "NRC number".localized
            }else if PersonalProfileModelUP.shared.NrcNumber.contains(find: "@") {
                let token = PersonalProfileModelUP.shared.NrcNumber.components(separatedBy: ")")
                if token[1].count < 6 {
                    emptyFields = emptyFields + "\n" + "NRC number must be 6 characters".localized
                }
            }else if PersonalProfileModelUP.shared.NrcNumber.contains(find: "/"){
                let token = PersonalProfileModelUP.shared.NrcNumber.components(separatedBy: ")")
                if token[1].count < 6 {
                    emptyFields = emptyFields + "\n" + "NRC number must be 6 characters".localized
                }else {
                  PersonalProfileModelUP.shared.NrcNumber = PersonalProfileModelUP.shared.NrcNumber.replacingOccurrences(of: "/", with: "@")
                }
            }
        }else if PersonalProfileModelUP.shared.IdType == "04" {
            if PersonalProfileModelUP.shared.NrcNumber == "" {
                emptyFields = emptyFields + "\n" + "Passport Number".localized
            }else if PersonalProfileModelUP.shared.NrcNumber.count < 5 {
                emptyFields = emptyFields + "\n" + "Passport Number must be more than 4 characters".localized
            }
            
            if PersonalProfileModelUP.shared.NrcExpireDate == "" {
                emptyFields = emptyFields + " " + "Date of Expiry".localized
            }
        }
        
        
        
        
        if PersonalProfileModelUP.shared.NrcImage1 == "" {
            if PersonalProfileModelUP.shared.IdType == "01" {
                    emptyFields = emptyFields + "\n" + "NRC Front Side".localized
            }else {
                    emptyFields = emptyFields + "\n" + "Passport Front Side".localized
            }
        }
        if PersonalProfileModelUP.shared.NrcImage2 == "" {
            if PersonalProfileModelUP.shared.IdType == "01" {
                emptyFields = emptyFields + "\n" + "NRC Back Side".localized
            }else {
                emptyFields = emptyFields + "\n" + "Passport Back Side".localized
            }
        }
        
        if PersonalProfileModelUP.shared.Township == "" {
           emptyFields = emptyFields + "\n" + "Township Name".localized
        }
        if PersonalProfileModelUP.shared.City == "" {
            emptyFields = emptyFields + "\n" + "City Name".localized
        }
        if PersonalProfileModelUP.shared.Street == "" {
            emptyFields = emptyFields + "\n" + "Street Name".localized
        }
        
        if PersonalProfileModelUP.shared.SecrityAnswer == "" {
            emptyFields = emptyFields + "\n" + "Security Answer".localized
        }

//        if PersonalProfileModelUP.shared.EmailId == "" {
//             emptyFields = emptyFields + "\n" + "Email ID".localized
//        }
        
        if emptyFields == "" {
        handler(emptyFields,true)
        }else {
        handler(emptyFields,false)
        }
     
    }
    
    func removFirstFourDigits(num: String)-> String {
        var number = num
        if number.prefix(2) == "00" {
            number.removeFirst()
            number.removeFirst()
            number.removeFirst()
            number.removeFirst()
            return number
        }else {
            return num
        }
    }
    
    
    
    func UpdatePersonalProfile() {
        
        if appDelegate.checkNetworkAvail() {
            let web = WebApiClass()
            web.delegate = self
            let urlStr =  Url.updatePersonalDetails
            let url = getUrl(urlStr: urlStr, serverType: .updateProfile)
            println_debug(url)
            if PersonalProfileModelUP.shared.KycStatus == -1 || PersonalProfileModelUP.shared.KycStatus == 2 {
                PersonalProfileModelUP.shared.KycStatus = 0
            }
            
            let dic = PersonalProfileModelUP.shared.wrapDataModel()
            let strData = String(data: dic!, encoding: .utf32)
            println_debug("Request params: \(String(describing: strData))")
            web.genericClassReg(url: url, param: Dictionary<String,Any>(), httpMethod: "POST", mScreen: "UpdateProfile", hbData: dic, authToken: nil)
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
        } else {
            alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img:#imageLiteral(resourceName: "r_user"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        DispatchQueue.main.async {
            progressViewObj.removeProgressView()
        }
    if screen == "UpdateProfile" {
        do {
            if let data = json as? Data {
                let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let dic = dict as? Dictionary<String,AnyObject> {
                    println_debug(dic["Msg"] as! String)
                    if dic["Code"] as? NSNumber == 200 {
                        print(dic)
                        // Call addUpdate profile API
                        UpdateProfileModel.share.personal_details = myProfileStatus
                        var urlString   = Url.addUpdateProfileDetails
                        urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                        let urlS = getUrl(urlStr: urlString, serverType: .updateProfile)
                        
                        UpdateProfileModel.share.updateProfileMenuStatus(urlStr: urlS , handle:{(success , response) in
                            if success {
                                profileObj.callLoginApi(aCode: UserModel.shared.mobileNo, withPassword: ok_password!, handler: { (success) in
                                    DispatchQueue.main.async {
                                        profileObj.callUpdateProfileApi(aCode: UserModel.shared.mobileNo, handler: { (success) in
                                            if success {
                                                
                                                //self.setPreviousLanguage()
                                                alertViewObj.wrapAlert(title:"", body:"Your Personal profile updated successfully".localized, img:#imageLiteral(resourceName: "bank_success"))
                                                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                                    self.navigationController?.popViewController(animated: true)
                                                })
                                                DispatchQueue.main.async {
                                                    alertViewObj.showAlert(controller: self)
                                                }
                                            }
                                        })
                                    }
                                })
                            }else {
                                println_debug("Failed")
                            }
                        })
                    }else {
                        DispatchQueue.main.async {
                            self.showAlert(alertTitle: "", alertBody: dic["Msg"] as! String , alertImage: #imageLiteral(resourceName: "r_user"))
                        }
                    }
                }
            }
        } catch {
            DispatchQueue.main.async {
                self.showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))
            }
        }
        }
    }
    
    func setPreviousLanguage() {
        if UserDefaults.standard.bool(forKey: "LanguageStatusChanged"){
            let preLang = UserDefaults.standard.value(forKey: "PreviousLanguage") as? String
            UserDefaults.standard.set(preLang!, forKey: "currentLanguage")
            appDel.setSeletedlocaLizationLanguage(language: preLang!)
            UserDefaults.standard.synchronize()
        }
    }

}

