//
//  OtherServiceUP.swift
//  MyUpdateprofile
//
//  Created by SHUBH on 12/12/17.
//  Copyright © 2017 SHUBH. All rights reserved.

import UIKit

class OtherServiceUP: UIViewController,FeesOtherServicesDelegate {
    
    @IBOutlet weak var UpdateBtnTopConstraints: NSLayoutConstraint!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var tableview: UITableView!
    var otherServiceArray = Array<Dictionary<String,Any>>()
    var otherServiceData: Dictionary<String,Any>?
    
    var menuItems = ["Mobile Top-Up Sales".localized, "View Topup Cash Back Rate".localized,"Online Gift Cards".localized,"Bus Tickets".localized,"DTH Bill".localized,"Electricity Bill".localized,"Post Paid Phone Bill".localized]
    
    var menuItemImage = [#imageLiteral(resourceName: "reportTopUp"),UIImage(named: "cash-back-rate"),UIImage(named: "online-giftcard"),UIImage(named: "bus-ticket"),UIImage(named: "dth-bill"),UIImage(named: "electricity-bill"),#imageLiteral(resourceName: "phone_number")]
    var feesList = [String]()
    var cashBackRatesList = [Dictionary<String,Any>]()
    override func viewDidLoad() {
        super.viewDidLoad()
         self.btnUpdate.setTitle("Update".localized, for: .normal)
        self.tableview.delegate = self
        self.tableview.dataSource = self
        self.setValuesInEmptyArray()
        btnUpdate.isHidden =  true
        UpdateBtnTopConstraints.constant = -50
    }
    
    private func setNavigationTitle () {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: "Zawgyi-One", size: 20)
        lblMarque.textAlignment = .center
        lblMarque.text = "Other Services".localized
        lblMarque.textColor = UIColor.init(hex: "#1C2C99")
        self.navigationItem.titleView = lblMarque
    }
    
    @IBAction func cliclOnBackButton(_ sender: Any) {
     self.navigationController?.popViewController(animated: true)
    }
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setNavigationTitle()
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    private func setValuesInEmptyArray() {
        
        let dic1 = ["Fees":0,"IsActive":false,"ServiceCode":"MobileTopup"] as [String : Any]
        let dic2 = ["Fees":0,"IsActive":false,"ServiceCode":"CashBackRate"] as [String : Any]
        let dic3 = ["Fees":0,"IsActive":false,"ServiceCode":"OnlineGift"] as [String : Any]
        let dic4 = ["Fees":0,"IsActive":false,"ServiceCode":"BusTicket"] as [String : Any]
        let dic5 = ["Fees":0,"IsActive":false,"ServiceCode":"DTHBill"] as [String : Any]
        let dic6 = ["Fees":0,"IsActive":false,"ServiceCode":"EBBill"] as [String : Any]
        let dic7 = ["Fees":0,"IsActive":false,"ServiceCode":"PostPaid"] as [String : Any]
        otherServiceArray.append(dic1)
        otherServiceArray.append(dic2)
        otherServiceArray.append(dic3)
        otherServiceArray.append(dic4)
        otherServiceArray.append(dic5)
        otherServiceArray.append(dic6)
        otherServiceArray.append(dic7)
        
        if let FeesList = otherServiceData?["FeesList"] as? String {
            self.feesList = FeesList.components(separatedBy: ",")
            println_debug("feesList: \(self.feesList)")
        }
        if let ratesList = otherServiceData?["CashBackRatesList"] as? Array<Dictionary<String,Any>> {
            self.cashBackRatesList = ratesList
            println_debug("cashBackRatesList: \(self.cashBackRatesList)")
        }
        
        if let dicServiceList = otherServiceData?["ServiceList"] as? Array<Dictionary<String,Any>> {
            if dicServiceList.count > 0 {
                for item in dicServiceList {
                    if item["ServiceCode"] as! String == "MobileTopup" {
                        self.otherServiceArray[0] = item
                    }else if item["ServiceCode"] as! String == "CashBackRate" {
                        self.otherServiceArray[1] = item
                    }else if item["ServiceCode"] as! String == "OnlineGift" {
                        self.otherServiceArray[2] = item
                    }else if item["ServiceCode"] as! String == "BusTicket" {
                        self.otherServiceArray[3] = item
                    }else if item["ServiceCode"] as! String == "DTHBill" {
                        self.otherServiceArray[4] = item
                    }else if item["ServiceCode"] as! String == "EBBill"{
                        self.otherServiceArray[5] = item
                    }else if item["ServiceCode"] as! String == "PostPaid"{
                        self.otherServiceArray[6] = item
                    }
                }
            }
            DispatchQueue.main.async {
                self.tableview.reloadData()
            }
        }

    }
    
    @IBAction func onClickUpdate (_ sender : UIButton) {
        var urlString   = Url.updateProfileOtherServices
        urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let urlS = getUrl(urlStr: urlString, serverType: .updateProfile)
        
        let param = ["AppId": "","MobileNumber":UserModel.shared.mobileNo,"Msid":UserModel.shared.msid,"Ostype":1,"Otp":"","Simid":UserModel.shared.simID] as Dictionary<String,Any>
        let paramfinal = ["MerLoginInfo" : param,"OtherServiceList":otherServiceArray] as AnyObject
        println_debug(urlS)
        println_debug(paramfinal)
        TopupWeb.genericClass(url:urlS , param: paramfinal, httpMethod: "POST", handle:{ (response , success) in
            if success {
                if let json = response as? Dictionary<String,Any> {
                    if let code = json["Code"] as? NSNumber, code == 200 {
                        var serviceCount = 0
                        for item in self.otherServiceArray {
                            if item["IsActive"] as! Bool == true {
                                serviceCount += 1
                            }
                        }
                        UpdateProfileModel.share.other_services = serviceCount
                        var urlString   = Url.addUpdateProfileDetails
                        urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                        let urlS = getUrl(urlStr: urlString, serverType: .updateProfile)
                        UpdateProfileModel.share.updateProfileMenuStatus(urlStr: urlS , handle:{(success , response) in
                            if success {
                                alertViewObj.wrapAlert(title:"", body:"Your other services has been updated successfully.", img:#imageLiteral(resourceName: "bank_success"))
                                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                    self.navigationController?.popViewController(animated: true)
                                })
                                DispatchQueue.main.async {
                                    alertViewObj.showAlert(controller: self)
                                }
                            }else {
                                DispatchQueue.main.async {
                                    self.showAlert(alertTitle: "", alertBody: response, alertImage: #imageLiteral(resourceName: "alert-icon"))}
                            }
                        })
                    }else {
                        DispatchQueue.main.async {
                            self.showAlert(alertTitle: "", alertBody: "Request failed Please try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))}
                    }
                }
            }else {
                DispatchQueue.main.async {
                    self.showAlert(alertTitle: "", alertBody: "Network error and try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))}
            }
        })
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func selectedFeesAmount(fees: String, withTag: Int) {
        otherServiceArray[withTag]["IsActive"] = true
        let getFee = fees.components(separatedBy: " ")
        otherServiceArray[withTag]["Fees"] = Int(getFee[0])
        let indexSet: IndexSet = [withTag]
          btnUpdate.isHidden =  false
        UpdateBtnTopConstraints.constant = 0
        tableview.beginUpdates()
        self.tableview.reloadSections(indexSet, with: .automatic)
        tableview.endUpdates()
    }
    func dismissController(tag: Int, hideSubView: Bool) {
        if hideSubView {
            otherServiceArray[tag]["IsActive"] = false
            let indexSet: IndexSet = [tag]
            tableview.beginUpdates()
            self.tableview.reloadSections(indexSet, with: .automatic)
            tableview.endUpdates()
        }else {
            
        }
    }
    @objc func onClickViewTopupCashback(_ sender: UIButton) {
//        let cashBackVC = self.storyboard?.instantiateViewController(withIdentifier: "ViewCashBackRateVC") as! ViewCashBackRateVC
//        self.navigationController?.pushViewController(cashBackVC, animated: true)
    }
}

extension OtherServiceUP : UITableViewDelegate,UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 8
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 || section == 2 || section == 3 || section == 4 || section == 5 || section == 6 {
           let dic = otherServiceArray[section]
            if dic["IsActive"] as? Bool == false {
                return 1
            }else {
                return 2
            }
        }else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let menuCell = tableView.dequeueReusableCell(withIdentifier: "MenuCell") as! MenuCell
         let feesCell = tableView.dequeueReusableCell(withIdentifier: "SetFeesCell") as! SetFeesCell
        
        if indexPath.section == 0 || indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 4 || indexPath.section == 5 || indexPath.section == 6 {
            if indexPath.section == 0 {
                  menuCell.btnInfo.isHidden = true
                    menuCell.btnInfo.addTarget(self, action: #selector(btnInfoAction), for: UIControl.Event.touchUpInside)
            }else {
                  menuCell.btnInfo.isHidden = true
            }
            let dic = otherServiceArray[indexPath.section]
            
            if indexPath.row == 0 {
                menuCell.btnSwitch.tag = indexPath.section
                menuCell.btnSwitch.addTarget(self, action: #selector(switchValueDidChange), for: .valueChanged)
                menuCell.lblMenuTitle.text = menuItems[indexPath.section]
                menuCell.imgMenu.image = menuItemImage[indexPath.section]
                if (dic["IsActive"] as? Bool) == true{
                    menuCell.btnSwitch.isOn = true
                }else {
                    menuCell.btnSwitch.isOn = false
                }
                return menuCell
            }else {
                feesCell.btnSetFees.tag = indexPath.section
                feesCell.btnSetFees.addTarget(self, action: #selector(btnSetFeedAction), for: UIControl.Event.touchUpInside)
                let fee = (dic["Fees"] as? Int)!
                if fee == 0 {
                    feesCell.btnSetFees.setTitle("Free".localized, for: .normal)
                }else if fee == -1 {
                    feesCell.btnSetFees.setTitle("Free for my shopper".localized, for: .normal)
                }else {
                    feesCell.btnSetFees.setTitle(String(fee) + " MMK", for: .normal)
                }
              return feesCell
            }
        }else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ViewCashBackCell") as! ViewCashBackCell
            cell.viewTopup.setTitle("View Topup Cash Back Rate".localized, for: .normal)
            cell.viewTopup.addTarget(self, action:#selector(onClickViewTopupCashback(_:)) , for: .touchUpInside)
            return cell
        }else {
            let noteCell = tableView.dequeueReusableCell(withIdentifier: "NoteCell") as! NoteCell
            return noteCell
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 || section == 2 {
            return 35
        }else{
            return 0
        }
        
    }
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 || indexPath.section == 1 || indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 4 || indexPath.section == 5 || indexPath.section == 6 {
            return 55
        }else {
            return 250
        }
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
        return createRequiredFieldLabel(headerTitle: "Cash Back Earn Service".localized)
        }else if section == 2 {
        return createRequiredFieldLabel(headerTitle: "Set Fees for Chargable Services".localized)
        }else {
            let view = UIView(frame: CGRect(x: 0, y: 0, width:0, height: 0))
            return view
        }
    }
    
    private func createRequiredFieldLabel(headerTitle:String) ->UIView? {
        let localview = UIView(frame: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 35))
        localview.backgroundColor = hexStringToUIColor(hex: "#E0E0E0")
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: localview.frame.width - 10, height:35))
        lblHeader.text = headerTitle
        lblHeader.textAlignment = .left
        lblHeader.font = UIFont(name: "Zawgyi-One", size: 14)
        lblHeader.backgroundColor = UIColor.clear
        lblHeader.textColor = UIColor.darkGray
        localview.addSubview(lblHeader)
        return localview
    }
 
    @objc func btnSetFeedAction(_ sender : UIButton) {
        let setFees  = self.storyboard?.instantiateViewController(withIdentifier: "FeesOtherServices") as! FeesOtherServices
        setFees.delegate = self
        setFees.setTagValue(tagNum: sender.tag, hideview: false)
        self.navigationController?.pushViewController(setFees, animated:true)
    }
    
    @objc func btnInfoAction(_ sender : UIButton){
        self.showAlert(alertTitle: "", alertBody: "Coming Soon".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
     //   let infoVC = self.storyboard?.instantiateViewController(withIdentifier: "InfoCashbackPercentagevcOS") as! InfoCashbackPercentagevcOS
       //   self.navigationController?.pushViewController(infoVC, animated:true)
    }
    
    @objc func switchValueDidChange(_ sender : UISwitch!) {
        let dic = otherServiceArray[sender.tag]
       let allow = dic["ComingSoon"] as? Bool ?? false
        sender.isOn = false
        if allow {
            if sender.isOn == true {
                let setFees  = self.storyboard?.instantiateViewController(withIdentifier: "FeesOtherServices") as! FeesOtherServices
                setFees.delegate = self
                setFees.setTagValue(tagNum: sender.tag, hideview: true)
                self.navigationController?.pushViewController(setFees, animated:true)
            }else {
                otherServiceArray[sender.tag]["IsActive"] = false
                otherServiceArray[sender.tag]["Fees"] = 0
                btnUpdate.isHidden = false
                UpdateBtnTopConstraints.constant = 0
                let indexSet: IndexSet = [sender.tag]
                tableview.beginUpdates()
                self.tableview.reloadSections(indexSet, with: .automatic)
                tableview.endUpdates()
            }
        }else {
           sender.isOn = false
            DispatchQueue.main.async {
                self.showAlert(alertTitle: "", alertBody: "Coming Soon".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))}
        }
    }
}

class MenuCell : UITableViewCell {
    @IBOutlet weak var imgMenu : UIImageView!
    @IBOutlet weak var lblMenuTitle : UILabel!
    @IBOutlet weak var btnInfo : UIButton!
    @IBOutlet weak var btnSwitch : UISwitch!
    
    override func awakeFromNib() {
         super.awakeFromNib()
         btnSwitch.layer.cornerRadius = btnSwitch.frame.height / 2
    }
}

class SetFeesCell : UITableViewCell {
    @IBOutlet weak var btnSetFees : UIButton!
   @IBOutlet weak var lblFeeBill : UILabel!
    
    override func awakeFromNib() {
    super.awakeFromNib()
      lblFeeBill.text = "Fee Per Bill".localized
    }
}

class NoteCell : UITableViewCell {
    @IBOutlet weak var lblNote : UILabel!
    @IBOutlet weak var lblFist : UILabel!
    @IBOutlet weak var lblSecond : UILabel!
    @IBOutlet weak var lblThird : UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        lblNote.text = "Note".localized
        lblFist.text = "By enabling this service".localized
        lblSecond.text = "You will earn cashback and fees.".localized
        lblThird.text = "Your Shop / Business will be visited by more customer in ordered to get above services.".localized
    }
}
class ViewCashBackCell: UITableViewCell {
    @IBOutlet weak var viewTopup: UIButton!
}


