//
//  CashStandardRates.swift
//  OK
//
//  Created by gauri on 12/24/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit

class CashStandardRates: UIViewController {
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var tableList: UITableView!
    var listEn: [Dictionary<String,Any>]?
    var listMy: [Dictionary<String,Any>]?
    var listUni: [Dictionary<String,Any>]?
    var selectedRate: String?
    var fromVC: String?
     let download = FileDownload()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if fromVC == "CashIn" {
            if selectedRate == "Standard" {
                listEn = CashInModel.share.StandaredRateEn
                listMy = CashInModel.share.StandaredRateMy
                listUni = CashInModel.share.StandaredRateUni
                setNavigationTitle(str: "Cash-In Standard Rates".localized)
            }else{
                listEn  = CashInModel.share.DiscountRateEn
                listMy  = CashInModel.share.DiscountRateMy
                listUni  = CashInModel.share.DiscountRateUni
                setNavigationTitle(str: "Cash-In Discount Rates".localized)
            }
        }else {
            if selectedRate == "Standard" {
                listEn  = CashOutModel.share.StandaredRateEn
                listMy  = CashOutModel.share.StandaredRateMy
                listUni = CashInModel.share.StandaredRateUni
                setNavigationTitle(str: "Cash-Out Standard Rates".localized)
            }else {
                listEn = CashOutModel.share.DiscountRateEn
                listMy = CashOutModel.share.DiscountRateMy
                listUni  = CashInModel.share.DiscountRateUni
                setNavigationTitle(str: "Cash-Out Discount Rates".localized)
            }
        }
        btnBack.setTitle("Back".localized, for: .normal)
    }
    
    private func setNavigationTitle(str: String) {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: "Zawgyi-One", size: 20)
        lblMarque.textAlignment = .center
        lblMarque.text = str
        lblMarque.textColor = UIColor.init(hex: "#1C2C99")
        self.navigationItem.titleView = lblMarque
    }
    
    @IBAction func onClickBack(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickDownloadAction(_ sender: Any) {
        DispatchQueue.main.async {
            if let window = UIApplication.shared.keyWindow {
                let sb = UIStoryboard(name: "MyProfile", bundle: nil)
                let termsConditionsRVC  = sb.instantiateViewController(withIdentifier: "LanguageSelectAgentForm") as! LanguageSelectAgentForm
                self.addChild(termsConditionsRVC)
                window.rootViewController?.addChild(termsConditionsRVC)
                termsConditionsRVC.view.frame = .init(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
                termsConditionsRVC.delegate = self
                termsConditionsRVC.input = "hi"
                window.addSubview(termsConditionsRVC.view)
                window.makeKeyAndVisible()
            }
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
}

extension CashStandardRates: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
             return (listEn?.count ?? 0) + 1
        }else if section == 1{
            return (listMy?.count ?? 0) + 1
        }else {
             return (listUni?.count ?? 0) + 1
        }
     
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cashStdRatesCell") as! cashStdRatesCell
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                cell.lblNo.text = "No"
                cell.lblAmount.text = "Amount (MMK)"
                cell.lblRate.text = "Rate"
            }else {
                let dic = listEn?[indexPath.row - 1]
                cell.lblNo.text = dic?["No"] as? String ?? ""
                let minAmt = dic?["MinAmount"] as? String ?? ""
                let maxAmt = dic?["MaxAmount"] as? String ?? ""
                cell.lblAmount.text = minAmt + " - " + maxAmt
                let amtType = dic?["AmountType"] as? String ?? ""
                if amtType == "Amount" {
                    let amt = dic?["Rate"] as? String ?? ""
                    cell.lblRate.text = amt + " " + "MMK"
                }else {
                    let amt = dic?["Rate"] as? String ?? ""
                    cell.lblRate.text = amt + " %"
                }
            }
        }else if indexPath.section == 1 {
            if indexPath.row == 0 {
                cell.lblNo.text = "စဥ္"
                cell.lblAmount.text = "ေငြပမာဏ (က်ပ္)"
                cell.lblRate.text = "နႈန္းထား"
            }else {
                let dic = listMy?[indexPath.row - 1]
                cell.lblNo.text = dic?["No"] as? String ?? ""
                let minAmt = dic?["MinAmount"] as? String ?? ""
                let maxAmt = dic?["MaxAmount"] as? String ?? ""
                cell.lblAmount.text = minAmt + " - " + maxAmt
                let amtType = dic?["AmountType"] as? String ?? ""
                if amtType == "Amount" {
                    let amt = dic?["Rate"] as? String ?? ""
                    cell.lblRate.text = amt + " " + "က်ပ္"
                }else {
                    let amt = dic?["Rate"] as? String ?? ""
                    cell.lblRate.text = amt + " %"
                }
            }
        }else {
            if indexPath.row == 0 {
                cell.lblNo.text = "စဉ်"
                cell.lblAmount.text = "ငွေပမာဏ (ကျပ်)"
                cell.lblRate.text = "နှုန်းထား"
            }else {
                let dic = listUni?[indexPath.row - 1]
                cell.lblNo.text = dic?["No"] as? String ?? ""
                let minAmt = dic?["MinAmount"] as? String ?? ""
                let maxAmt = dic?["MaxAmount"] as? String ?? ""
                cell.lblAmount.text = minAmt + " - " + maxAmt
                let amtType = dic?["AmountType"] as? String ?? ""
                if amtType == "Amount" {
                    let amt = dic?["Rate"] as? String ?? ""
                    cell.lblRate.text = amt + " " + "ကျပ်"
                }else {
                    let amt = dic?["Rate"] as? String ?? ""
                    cell.lblRate.text = amt + " %"
                }
            }
        }
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return createRequiredFieldLabel(headerTitle: "English", reqTitle: "".localized)
        }else if section == 1{
            return createRequiredFieldLabel(headerTitle: "Myanmar (ျမန္မာ)", reqTitle: "".localized)
        }else {
            return createRequiredFieldLabel(headerTitle: "Unicode", reqTitle: "".localized)
        }
    }
    
    private func createRequiredFieldLabel(headerTitle:String, reqTitle: String) ->UIView? {
        let view1 = UIView(frame: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 35))
        view1.backgroundColor = hexStringToUIColor(hex: "#E0E0E0")
        let lblHeader = UILabel(frame: CGRect(x: 10, y: 0, width: (sizeOfString(myString: headerTitle)?.width)!, height:35))
        lblHeader.text = headerTitle
        lblHeader.textAlignment = .left
        lblHeader.font = UIFont(name: "Zawgyi-One", size: 14)
        lblHeader.backgroundColor = UIColor.clear
        lblHeader.textColor = UIColor.darkGray
        view1.addSubview(lblHeader)
        return view1
    }
    
    private func sizeOfString(myString : String) ->CGSize? {
        let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: "Zawgyi-One", size: 16) ?? UIFont.systemFont(ofSize: 16.0) ])
        return stringSize
    }
}

extension CashStandardRates: SelectLanguageAgentFromDelegate,FileDownloadDelegate {
    func formUploadedStatus(status: Bool) {
        
    }
    
    func downloadedFileStatus(status: Bool) {
        self.showAlert(alertTitle: "File downloaded successfully".localized, alertBody: "Path: Files -> On My iPhone -> OK$".localized, alertImage: UIImage(named: "info_success")!)
    }
    
    func downloadedFile(byteWritten : Float) {
       // progressDownload.setProgress(byteWritten, animated: true)
       // lblRatioPercent.text =  "\(byteWritten)/100"
    }
    
    
    func onClickSelectingLanguageAction(action: String, vc: UIViewController, language: String, output: String) {
        var url = ""
        if action == "download" {
            if language == "my" {
                if fromVC == "CashIn" {
                    if selectedRate == "Standard" {
                     url = CashInModel.share.StandardRateUrlMy
                    }else {
                      url = CashInModel.share.DiscountRateUrlMy
                    }
                }else {
                    if selectedRate == "Standard" {
                        url = CashOutModel.share.StandardRateUrlMy
                    }else {
                       url = CashOutModel.share.DiscountRateUrlMy
                    }
                }
            }else if language == "en"{
                if fromVC == "CashIn" {
                    if selectedRate == "Standard" {
                     url = CashInModel.share.StandardRateUrlEn
                    }else {
                      url = CashInModel.share.StandardRateUrlEn
                    }
                }else {
                    if selectedRate == "Standard" {
                      url = CashOutModel.share.StandardRateUrlEn
                    }else {
                       url = CashOutModel.share.StandardRateUrlEn
                    }
                }
            }else {
                if fromVC == "CashIn" {
                    if selectedRate == "Standard" {
                        url = CashInModel.share.StandardRateUrlUni
                    }else {
                        url = CashInModel.share.StandardRateUrlUni
                    }
                }else {
                    if selectedRate == "Standard" {
                        url = CashOutModel.share.StandardRateUrlUni
                    }else {
                        url = CashOutModel.share.StandardRateUrlUni
                    }
                }
            }
            let fileURL = URL(string: url)
            println_debug(url)
            download.delegate = self
            download.fileName = (fromVC ?? "") + (selectedRate ?? "") + language
            download.startDownloading(url: fileURL!)
        }else{
            
        }
        println_debug(url)
        vc.view.removeFromSuperview()
    }
}



class cashStdRatesCell: UITableViewCell {
    @IBOutlet weak var lblNo: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblRate: UILabel!
}




extension NSAttributedString {
    internal convenience init?(html: String) {
        guard let data = html.data(using: String.Encoding.utf16, allowLossyConversion: false) else {
            // not sure which is more reliable: String.Encoding.utf16 or String.Encoding.unicode
            return nil
        }
        guard let attributedString = try? NSMutableAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil) else {
            return nil
        }
        self.init(attributedString: attributedString)
    }
}
