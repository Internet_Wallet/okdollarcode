//
//  CashInModel.swift
//  OK
//
//  Created by Subhash Arya on 15/08/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class CashInModel: NSObject {
   static var share = CashInModel()
    
    var Status: Bool = false
    var DeliveryServiceStatus: Bool = false
    var MaxAmount: String = ""
    var DiscountRateUrlEn: String = ""
    var DiscountRateUrlMy: String = ""
    var DiscountRateUrlUni: String = ""
    var StandardRateUrlEn: String = ""
    var StandardRateUrlMy: String = ""
    var StandardRateUrlUni: String = ""
    var StandardRateStatus: Bool = false
    var DiscountRateStatus: Bool = false
    var DiscountStatus: Dictionary<String,Any>?
    var StandardRate: Dictionary<String,Any>?
    var DiscountRateEn: [Dictionary<String,Any>]?
    var DiscountRateMy: [Dictionary<String,Any>]?
    var DiscountRateUni: [Dictionary<String,Any>]?
    var StandaredRateEn: [Dictionary<String,Any>]?
    var StandaredRateMy: [Dictionary<String,Any>]?
    var StandaredRateUni: [Dictionary<String,Any>]?
    var standaredStatus: Bool?
    var discountStatus: Bool?
    var AppId : String = UserModel.shared.appID
    var MobileNumber : String = UserModel.shared.mobileNo
    var Msid : String = UserModel.shared.msid
    var Ostype : Int = 1
    var Otp : String = ""
    var Simid : String = uuid
    
    func wrapDataForUpdateCashIn() -> NSDictionary {
        var mainDic = NSDictionary()
        mainDic = ["LoginInfo": GetMerLoginInfo()]
        return mainDic
    }
    
   
    func GetMerLoginInfo() -> NSDictionary {
        let dic = ["AppId":AppId,"Offset":0.0,"MobileNumber":MobileNumber,"Msid":Msid,"Ostype":Ostype,"Limit":0.0,"Otp":"","Simid":Simid] as [String : Any]
        return dic as NSDictionary
    }
    
    func bindCashInDataForUpdate()-> NSDictionary {
        var mainDic = NSDictionary()
        mainDic = ["LoginInfo": GetMerLoginInfo(),
                   "CashInOut": ["ServiceType": 1,"Status": Status,"DeliveryServiceStatus":DeliveryServiceStatus,"MaxAmount": MaxAmount]
        ]
        return mainDic
    }
    
    func getAgentType() -> Int {
        var agentType = 6
        switch UserModel.shared.agentType {
        case .user:
            agentType = 6
        case .agent:
            agentType = 1
        case .advancemerchant:
            agentType = 4
        case .dummymerchant:
            agentType = 5
        case .merchant:
            agentType = 2
        case .oneStopMart:
            agentType = 8
        }
        
        return agentType
    }
    
    func bindCashInDataForUpdateCashIn()-> NSDictionary {
        let appVersion = String.init(format: "Version %@, Build (\(buildNumber))", Bundle.main.releaseVersionNumber)
        var c_code = UserModel.shared.countryCode
        if c_code.contains(find: "+") {
            c_code.removeFirst()
        }
        
        var mainDic = NSDictionary()
        let type = getAgentType()
        let deliveryStatus = DeliveryServiceStatus ? "1" : "0"
        mainDic = ["CountryCode": c_code,"MobileNumber":UserModel.shared.mobileNo,"OpeningTime":"","ClosingTime":"","OkDollarBalance": UserLogin.shared.walletBal,"Latitude":  UserModel.shared.lat,"Longitude":  UserModel.shared.long,"CellId":"","CashIn": Status,"CashOut": UpdateProfileModel.share.cash_out, "AgentType": deliveryStatus ,"Type": String(type),"IsOpenAlways":true,"AppVersion": buildVersion,"Category": UserModel.shared.businessCate,"SubCategory": UserModel.shared.businessType]
        return mainDic
    }

    func getServiceData(handle :@escaping (_ isSuccess : Bool,_ message : String)-> Void) {
        var urlString   = Url.cashInCashOutGetService
        urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let urlS = getUrl(urlStr: urlString, serverType: .updateProfile)

        let dic = ["MerLoginInfo":self.GetMerLoginInfo(),"ServiceType":1] as [String : Any]
        println_debug(urlS)
        println_debug("Get CashOut Data:\(dic)")
        TopupWeb.genericClass(url: urlS, param: dic as AnyObject, httpMethod: "POST", handle: {(resp, success) in
            if success {
                if let response = resp as? Dictionary<String,AnyObject> {
                    if response["Code"] as? Int == 200 {
                        let dic = OKBaseController.convertToDictionary(text: response["Data"] as! String)
                        if let data = dic as Dictionary<String,AnyObject>? {
                            self.Status = data["Status"] as? Bool ?? false
                            self.DeliveryServiceStatus = data["DeliveryServiceStatus"] as? Bool ?? false
                            self.MaxAmount = data["MaxAmount"] as? String ?? ""
                            if let comRate = data["CommissiionRate"] as? Dictionary<String,Any> {
                                
                                self.DiscountStatus = comRate["DiscountRate"] as? Dictionary<String, Any>
                                
                                self.discountStatus = self.DiscountStatus?["DiscountStatus"] as? Bool ?? false
                                
                                self.DiscountRateUrlEn = self.DiscountStatus?["DiscountRateUrlEn"] as? String ?? ""
                                self.DiscountRateUrlMy = self.DiscountStatus?["DiscountRateUrlMy"] as? String ?? ""
                                self.DiscountRateUrlUni = self.DiscountStatus?["DiscountRateUrlMyUnicode"] as? String ?? ""
                                
                                self.DiscountRateStatus = self.DiscountStatus?["DiscountStatus"] as? Bool ?? false
                                
                                self.DiscountRateEn = self.DiscountStatus?["RateEn"] as? [Dictionary<String, Any>]
                                self.DiscountRateMy = self.DiscountStatus?["RateMy"] as? [Dictionary<String, Any>]
                                self.DiscountRateUni = self.DiscountStatus?["RateMyUnicode"] as? [Dictionary<String, Any>]
                                
                                self.StandardRate = comRate["StandardRate"] as? Dictionary<String, Any>
                                self.standaredStatus = self.StandardRate?["StandardStatus"] as? Bool ?? false
                                
                                self.StandardRateUrlEn = self.StandardRate?["StandardRateUrlEn"] as? String ?? ""
                                self.StandardRateUrlMy = self.StandardRate?["StandardRateUrlMy"] as? String ?? ""
                                self.StandardRateUrlUni = self.StandardRate?["StandardRateUrlMyUnicode"] as? String ?? ""
                                
                                self.StandardRateStatus = self.StandardRate?["StandardStatus"] as? Bool ?? false
                                
                                self.StandaredRateEn = self.StandardRate?["RateEn"] as? [Dictionary<String, Any>]
                                self.StandaredRateMy = self.StandardRate?["RateMy"] as? [Dictionary<String, Any>]
                                self.StandaredRateUni = self.StandardRate?["RateMyUnicode"] as? [Dictionary<String, Any>]
                            }
                            handle(true,response["Msg"] as! String)
                        }
                }else {
                    handle(false,response["Msg"] as! String)
                }
            }else {
                    handle(false,"Technical Failure".localized)
                }
            }
            println_debug(resp)
        })
    }
    
    
}
