//
//  CashbackPercentagevcOS.swift
//  OK
//
//  Created by Subhash Arya on 17/09/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class InfoCashbackPercentagevcOS: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var tableview : UITableView!
    @IBOutlet weak var btnCancel : UIButton!
    
    var menuList = [Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableview.tableFooterView = UIView(frame : .zero)
        self.setNavigationTitle()
        self.callAPI()
    }
    
    private func setNavigationTitle () {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: "Zawgyi-One", size: 20)
        lblMarque.textAlignment = .center
        lblMarque.text = "Cash Back Percentage".localized
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
    }
    
    private func callAPI() {
        var urlString   = Url.getTelcoCashBackPercentage
        urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let urlS = getUrl(urlStr: urlString, serverType: .serverApp)
        let param = ["Simid" : UserModel.shared.simID, "MobileNumber" : UserModel.shared.mobileNo, "AppId" : "com.cgm.OKDollar","Msid" : UserModel.shared.msid,"Ostype":"1","Otp" : ""] as AnyObject
        TopupWeb.genericClass(url:urlS , param: param, httpMethod: "POST", handle:{ (response , success) in
            if success {
                if let json = response as? Dictionary<String,Any> {
                    if let code = json["Code"] as? NSNumber, code == 200 {
                        if let que = json["Data"] as? String {
                            if let list = OKBaseController.convertToArrDictionary(text: que) {
                                self.menuList.removeAll()
                                self.menuList = list
                                DispatchQueue.main.async {
                                    self.tableview.reloadData()
                                }
                            }
                        }
                    }else {
                        DispatchQueue.main.async {
                            self.showAlert(alertTitle: "", alertBody: (json["Msg"] as? String)! , alertImage: #imageLiteral(resourceName: "alert-icon"))}
                    }
                }
            }
        })
    }
    
    @IBAction func onClickCancel(_ sender : Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let menuCell = tableView.dequeueReusableCell(withIdentifier: "InfoCell") as! InfoCell
        let dic = menuList[indexPath.row] as? Dictionary<String,Any>
        let telco =  dic!["TelcoName"]
        let percent = dic!["CashBackPercentage"]
        menuCell.lblMenu.text = (telco as? String ?? "") +  " " + (percent as? String ?? "")
        return menuCell
    }
    
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
}

class InfoCell : UITableViewCell {
    @IBOutlet weak var lblMenu : UILabel!
}

