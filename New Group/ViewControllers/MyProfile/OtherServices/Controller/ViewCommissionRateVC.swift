//
//  ViewCommissionRateVC.swift
//  OK
//
//  Created by gauri on 12/20/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit

class ViewCommissionRateVC: OKBaseController {
    @IBOutlet weak var btnBAck: UIButton!
    @IBOutlet weak var tableList: UITableView!
    @IBOutlet weak var lblCashInNote: UILabel!
    @IBOutlet weak var lblOuter: UILabel!
    
    var list = [Dictionary<String,String>]()
    var viewController: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        lblOuter.layer.cornerRadius = 10
        lblOuter.layer.masksToBounds = true
        let note = UpdateProfileModel.share.CashOutServices
        let main = note?[2]
        if ok_default_language == "my" {
           // lblCashInNote.attributedText = NSAttributedString.init(html: main?["My"] as? String ?? "")
            let htmlString = main?["My"] as? String ?? ""
            if let labelTextFormatted = htmlString.htmlToAttributedString {
                let textAttributes = [
                    NSAttributedString.Key.foregroundColor: UIColor.black,
                    NSAttributedString.Key.font: UIFont(name: appFont, size:15) ?? UIFont.systemFont(ofSize: 15)
                    ] as [NSAttributedString.Key: Any]
                labelTextFormatted.addAttributes(textAttributes, range: NSRange(location: 0, length: labelTextFormatted.length))
                println_debug(NSAttributedString(attributedString: labelTextFormatted))
                lblCashInNote.attributedText = NSAttributedString(attributedString: labelTextFormatted)
            }
        }else if ok_default_language == "en"{
            lblCashInNote.attributedText = NSAttributedString.init(html: main?["En"] as? String ?? "")
        }else {
            let htmlString = main?["UniCode"] as? String ?? ""
            if let labelTextFormatted = htmlString.htmlToAttributedString {
                let textAttributes = [
                    NSAttributedString.Key.foregroundColor: UIColor.black,
                    NSAttributedString.Key.font: UIFont(name: appFont, size:15) ?? UIFont.systemFont(ofSize: 15)
                    ] as [NSAttributedString.Key: Any]
                labelTextFormatted.addAttributes(textAttributes, range: NSRange(location: 0, length: labelTextFormatted.length))
                println_debug(NSAttributedString(attributedString: labelTextFormatted))
                lblCashInNote.attributedText = NSAttributedString(attributedString: labelTextFormatted)
            }
        }
        self.setNavigationTitle()
        list = [["icon_imge":"amount_new","title_Name": "Standard".localized,"button_Image":"verified"],["icon_imge":"amount_new","title_Name": "Special".localized,"button_Image":"infoQR"]]
        btnBAck.setTitle("Back".localized, for: .normal)
    }
    private func setNavigationTitle () {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: "Zawgyi-One", size: 20)
        lblMarque.textAlignment = .center
        lblMarque.text = "Commission Rates".localized
        lblMarque.textColor = UIColor.init(hex: "#1C2C99")
        self.navigationItem.titleView = lblMarque
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func onClickStandardRates(_ sender: UITapGestureRecognizer) {
        let vcStdRate = self.storyboard?.instantiateViewController(withIdentifier: "CashStandardRates") as! CashStandardRates
        if sender.name == "Standard Rates".localized {
            vcStdRate.selectedRate = "Standard"
        }else {
            vcStdRate.selectedRate = "Discount"
        }
        vcStdRate.fromVC = viewController
        self.navigationController?.pushViewController(vcStdRate, animated: true)
    }

    @objc func onClickInfoAction(_ sender: UIButton) {
        if sender.tag == 0 {
            showAlert(alertTitle: "", alertBody: "Standard Rate - Commission Rate will be default set from OK$ to customers".localized, alertImage: UIImage(named: "infoQR")!)
        }else {
            showAlert(alertTitle: "", alertBody: "Discounted Rate - Commission Rate will be set for the merchants on request of OK$ for their valuable customers".localized, alertImage: UIImage(named: "infoQR")!)
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
}

extension ViewCommissionRateVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:RatesCell = tableView.dequeueReusableCell(withIdentifier: "RatesCell") as! RatesCell
        let dic = list[indexPath.row]
        cell.selectionStyle = .none
        cell.imgIcon.image = UIImage(named: dic["icon_imge"]!)
        cell.lblName.text = dic["title_Name"]
        let tap = UITapGestureRecognizer(target: self, action: #selector(ViewCommissionRateVC.onClickStandardRates))
        tap.name = dic["title_Name"]
        cell.lblName.isUserInteractionEnabled = true
        cell.lblName.addGestureRecognizer(tap)
        cell.btnInfo.isHidden = true
        cell.btnCheck.tag  = indexPath.row
        cell.btnCheck.setImage(UIImage(named: "infoQR"), for:.normal)
        cell.btnInfo.setImage(UIImage(named: "verified"), for:.normal)
        cell.btnCheck.addTarget(self, action: #selector(ViewCommissionRateVC.onClickInfoAction(_:)), for: .touchUpInside)
    
        if viewController == "CashIn" {
            if indexPath.row == 0 {
                if CashInModel.share.StandardRateStatus {
                    cell.btnInfo.isHidden = false
                }else {
                    cell.btnInfo.isHidden = true
                }
            }else {
                if CashInModel.share.DiscountRateStatus {
                    cell.btnInfo.isHidden = false
                }else {
                    cell.btnInfo.isHidden = true
                }
            }
        }else {
            if indexPath.row == 0 {
                if CashOutModel.share.StandardRateStatus {
                    cell.btnInfo.isHidden = false
                }else {
                    cell.btnInfo.isHidden = true
                }
            }else {
                if CashOutModel.share.DiscountRateStatus {
                    cell.btnInfo.isHidden = false
                }else {
                    cell.btnInfo.isHidden = true
                }
            }
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

class RatesCell : UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblName: MarqueeLabel!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var btnInfo: UIButton!
    
}
