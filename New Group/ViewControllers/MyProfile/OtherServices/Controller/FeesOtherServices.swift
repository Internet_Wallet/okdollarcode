//
//  FeesOtherServices.swift
//  OK
//
//  Created by Subhash Arya on 12/09/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol FeesOtherServicesDelegate {
    func selectedFeesAmount(fees : String,withTag : Int)
    func dismissController(tag : Int,hideSubView : Bool)
}

class FeesOtherServices: UIViewController,UITableViewDataSource,UITableViewDelegate,setCustomCommissionRateVCDelegate{
    
    @IBOutlet weak var tableview : UITableView!
    @IBOutlet weak var btnCancel : UIButton!
    var delegate : FeesOtherServicesDelegate?
    let feesList = ["Free".localized,"Free for my shopper".localized,"50 MMK","100 MMK","150 MMK","200 MMK","250 MMK","300 MMK","350 MMK","400 MMK","450 MMK","500 MMK","Enter Amount (1 - 500) MMK".localized]

    var tagValue : Int?
    var isHideSubView : Bool?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Set Fees"

        tableview.tableFooterView = UIView.init(frame: CGRect.zero)
    }
    
    @IBAction func cliclOnBackButton(_ sender: Any) {
        if let del = delegate {
            del.dismissController(tag: tagValue!, hideSubView: isHideSubView!)
        }
        self.navigationController?.popViewController(animated: true)
    }
    func setTagValue(tagNum : Int,hideview : Bool){
        tagValue = tagNum
        isHideSubView = hideview
    }
    
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return feesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let feeCell = tableView.dequeueReusableCell(withIdentifier: "chooseFeedCell") as! chooseFeedCell
        feeCell.lblTitle.text = feesList[indexPath.row]
        return feeCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            if let del = delegate {
                del.selectedFeesAmount(fees: "0", withTag: tagValue!)
                self.navigationController?.popViewController(animated: true)
            }
        }else if indexPath.row == 1{
            if let del = delegate {
                del.selectedFeesAmount(fees: "-1", withTag: tagValue!)
                self.navigationController?.popViewController(animated: true)
            }
        }else if indexPath.row == feesList.count - 1 {
            let comRateCom = self.storyboard?.instantiateViewController(withIdentifier: "setCustomCommissionRateVC") as! setCustomCommissionRateVC
            if let window = UIApplication.shared.keyWindow {
                window.rootViewController?.addChild(comRateCom)
                comRateCom.view.frame = .init(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
                comRateCom.setFeeAndPercentMode(mode: "OtherServiceFees")
                comRateCom.delegate = self
                window.addSubview((comRateCom.view))
                window.makeKeyAndVisible()
            }
        }else {
            if let del = delegate {
                del.selectedFeesAmount(fees: feesList[indexPath.row], withTag: tagValue!)
                self.navigationController?.popViewController(animated: true)
            }
        }
        
    }
    
    func getCustomAmountAndPersentage(strValue value: String, strMode forMode: String) {
        if let del = delegate {
            del.selectedFeesAmount(fees: value, withTag: tagValue!)
            self.navigationController?.popViewController(animated: true)
        }
    }
}


class chooseFeedCell : UITableViewCell {
    @IBOutlet weak var lblTitle : UILabel!
}
