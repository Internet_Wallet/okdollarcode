//
//  cashOut_UP.swift
//  OK
//
//  Created by Subhash Arya on 13/08/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class cashOut_UP: OKBaseController,UITextFieldDelegate,WebServiceResponseDelegate {
    
    @IBOutlet weak var btnUpdateButton: UIButton!{
        didSet{
             btnUpdateButton.setTitle(" Submit".localized, for: .normal)
        }
    }
    @IBOutlet weak var viewCashInOff: UIView!
    @IBOutlet weak var tableview: UITableView!
  @IBOutlet weak var lblCashInNote: UILabel!
      @IBOutlet weak var lblOuter: UILabel!
    let placholderColor : UIColor = UIColor.init(red: 28.0/255.0, green: 44.0/255.0, blue: 153.0/255.0, alpha: 1)
    let bGColor : UIColor = UIColor.init(red: 224.0/255.0, green: 224.0/255.0, blue: 244.0/255.0, alpha: 1)
    var arrCashOutTitle = [Array<String>]()
       let AMOUNTCHARSET = "0123456789"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblOuter.layer.cornerRadius = 10
        lblOuter.layer.masksToBounds = true
        let note = UpdateProfileModel.share.CashOutServices
        let main = note?[0]
        if ok_default_language == "my" {
            let htmlString = main?["My"] as? String ?? ""
            if let labelTextFormatted = htmlString.htmlToAttributedString {
                let textAttributes = [
                    NSAttributedString.Key.foregroundColor: UIColor.black,
                    NSAttributedString.Key.font: UIFont(name: appFont, size:15) ?? UIFont.systemFont(ofSize: 15)
                    ] as [NSAttributedString.Key: Any]
                labelTextFormatted.addAttributes(textAttributes, range: NSRange(location: 0, length: labelTextFormatted.length))
                println_debug(NSAttributedString(attributedString: labelTextFormatted))
                lblCashInNote.attributedText = NSAttributedString(attributedString: labelTextFormatted)
            }
        }else if ok_default_language == "en"{
            lblCashInNote.attributedText = NSAttributedString.init(html: main?["En"] as? String ?? "")
        }else {
            lblCashInNote.text = NSAttributedString.init(html: main?["UniCode"] as? String ?? "")?.string //NSAttributedString.init(html: main?["UniCode"] as? String ?? "")
//            let htmlString = main?["UniCode"] as? String ?? ""
//            if let labelTextFormatted = htmlString.htmlToAttributedString {
//                let textAttributes = [
//                    NSAttributedString.Key.foregroundColor: UIColor.black,
//                    NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)
//                    ] as [NSAttributedString.Key: Any]
//                labelTextFormatted.addAttributes(textAttributes, range: NSRange(location: 0, length: labelTextFormatted.length))
//                println_debug(NSAttributedString(attributedString: labelTextFormatted))
//                lblCashInNote.attributedText = NSAttributedString(attributedString: labelTextFormatted)
//            }
        }
        
        
        self.setNavigationTitle()
        
        if UpdateProfileModel.share.cash_out == 1 {
                self.arrCashOutTitle = [["Cash-Out".localized],["Delivery Service".localized,CashOutModel.share.MaxAmount,"Commission Rate".localized]]
                CashOutModel.share.Status = true
                self.showServiceOffView(isShow: false)
        }else {
            CashOutModel.share.StandardRateStatus = true
            arrCashOutTitle = [["Cash-Out Services".localized],["","",""]]
            CashOutModel.share.Status = false
            self.resetModelData()
            self.showServiceOffView(isShow: true)
        }
          self.tableview.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    private func setNavigationTitle() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: "Zawgyi-One", size: 20)
        lblMarque.textAlignment = .center
        lblMarque.text = "Cash-Out".localized
        lblMarque.textColor = UIColor.init(hex: "#1C2C99")
        self.navigationItem.titleView = lblMarque
    }
    
    private func resetModelData() {
        CashOutModel.share.Status = false
        CashOutModel.share.MaxAmount = ""
        CashOutModel.share.DeliveryServiceStatus = false
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
            textField.keyboardType = .numberPad
    }
   
    @objc func textFieldDidChanged(_ textField: UITextField) {
        let cashInMinAmt = textField.text!.replacingOccurrences(of: ",", with: "")
        if (textField.text?.count)! > 0 {
            if Int(cashInMinAmt)! <= 1000000000 {
                textField.text = getDigitDisplay(textField.text!)
            }else{
                textField.text?.removeLast()
                self.showToastlocal(message:"Maximum Cash-Out Amount should be below 10,000,000".localized)
            }
           // textField.rightView = nil
        }else{

        }
        CashOutModel.share.MaxAmount = textField.text!.replacingOccurrences(of: ",", with: "")
        arrCashOutTitle[1] = ["Delivery Sercice".localized, CashOutModel.share.MaxAmount, "View Commission Rate".localized]
    }
    
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
            CashOutModel.share.MaxAmount = textField.text!.replacingOccurrences(of: ",", with: "")
            arrCashOutTitle[1] = ["Delivery Sercice".localized, CashOutModel.share.MaxAmount, "View Commission Rate".localized]
    }
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if range.location == 0 && string == "0" {
            return false
        }
  
         if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: AMOUNTCHARSET).inverted).joined(separator: "")) { return false }
        
        if text.count > 1 {
            let cashInMinAmt = textField.text!.replacingOccurrences(of: ",", with: "")
            if Int(cashInMinAmt)! <= 1000000000 {
                return true
            }else {
                return false
            }
        }
            return true
   
    }
    
    
    func convert24Formate(str : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "HH:mm a"
        let date = dateFormatter.date(from: str)
        dateFormatter.dateFormat = "HH:mm:ss"
        if let dt = date {
            return dateFormatter.string(from: dt)
        }else {
            return ""
        }
    }
    @IBAction func clickOnUpdateAction(_ sender: Any) {
        self.checkAllMadetoryFields(handle: { (isSuccess,response) in
            if isSuccess {
                UpdateProfileModel.share.cash_out = 1
            //   self.UpdateCashInService()
                self.callUpdateProfileService()
            }else {
                DispatchQueue.main.async {
                    self.showAlert(alertTitle: "", alertBody: response , alertImage: #imageLiteral(resourceName: "alert-icon"))}
            }
        })
    }
    
    private func UpdateCashInService() {
        if appDelegate.checkNetworkAvail() {
            let web = WebApiClass()
            web.delegate = self
             var urlString   = Url.updateUser
             urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let urlS = getUrl(urlStr: urlString, serverType: .cashInCashOutUrl)
            let param = CashOutModel.share.bindCashOutDataForUpdateCashOut()
            println_debug(urlS)
            println_debug(param)
            web.genericClass(url: urlS, param: param, httpMethod: "POST", mScreen: "CashOutUpdate")
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
        }else{
            alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img:#imageLiteral(resourceName: "r_user"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    private func callUpdateProfileService() {
        if appDelegate.checkNetworkAvail() {
            let web = WebApiClass()
            web.delegate = self
            var urlString   = Url.cashOUT
            urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let urlS = getUrl(urlStr: urlString, serverType: .updateProfile)
            let param = CashOutModel.share.bindCashOutDataForUpdate()
            println_debug(urlS)
            println_debug("CashOut Params: \(param)")
            web.genericClassReg1(url: urlS, param: param as AnyObject , httpMethod: "POST", mScreen: "cashout")
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
        }else{
            alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img:#imageLiteral(resourceName: "r_user"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        DispatchQueue.main.async {
            progressViewObj.removeProgressView()
        }
        
        if screen == "cashout" {
            do {
            if let data = json as? Data {
                let response = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                let dic = response as? Dictionary<String , Any>
                if dic!["Code"] as? NSInteger == 200 {
                    // Call addUpdate profile API
                    UpdateProfileModel.share.cash_out = 1
                                        var urlString   = Url.addUpdateProfileDetails
                                        urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                                        let urlS = getUrl(urlStr: urlString, serverType: .updateProfile)
                    UpdateProfileModel.share.updateProfileMenuStatus(urlStr: urlS , handle:{(success , response) in
                        if success {
                            println_debug(response)
                           
                            alertViewObj.wrapAlert(title:"", body:"Your Cash-Out Services has been enabled successfully".localized, img:#imageLiteral(resourceName: "bank_success"))
                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                UserDefaults.standard.set(true, forKey: "MerchantCashOutStatus")
                                self.navigationController?.popViewController(animated: true)
                            })
                            DispatchQueue.main.async {
                                alertViewObj.showAlert(controller: self)
                            }
                            
                        }else {
                            println_debug("Failed")
                        }
                    })
                    
                }else {
                    DispatchQueue.main.async {
                        self.showAlert(alertTitle: "", alertBody: dic!["Msg"] as? String ?? "", alertImage: #imageLiteral(resourceName: "alert-icon"))}
                }
            }
        } catch {
            DispatchQueue.main.async {
                self.showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))}
            }
        }else if screen == "CashOutUpdate" {
            do {
                if let data = json as? Data {
                    let response = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    let dic = response as? Dictionary<String , Any>
                    println_debug(dic)
                    if dic!["StatusCode"] as? NSInteger == 200 {
                        if UpdateProfileModel.share.cash_out != 0 {
                            self.callUpdateProfileService()
                        }
                    }else {
                        DispatchQueue.main.async {
                            self.showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))}
                    }
                }
            } catch {
                DispatchQueue.main.async {
                    self.showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))}
            }
        }
        
    }
    
    private func checkAllMadetoryFields(handle: @escaping (_ success : Bool ,_ message : String)->Void ) {
        
        var emptyFields = ""

        if CashOutModel.share.MaxAmount == "" {
            emptyFields = emptyFields + "\n" + "Maximum Amount".localized
        }else if Int(CashOutModel.share.MaxAmount) ?? 0 < 1000 {
            emptyFields = emptyFields + "\n" + "Minimum Amount should be 1000 MMK".localized
        }
        
        
        if emptyFields == "" {
            handle(true,"Success")
        }else {
            handle(false,emptyFields)
        }
        
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
    private func showServiceOffView(isShow : Bool) {
        DispatchQueue.main.async{
            if isShow {
                self.viewCashInOff.isHidden = false
                self.tableview.isScrollEnabled = false
                //self.btnUpdateButton.isHidden = true
                self.btnUpdateButton.backgroundColor = UIColor.init(hex: "#E0E0E0") //1C2C99
                self.btnUpdateButton.setTitleColor(UIColor.init(hex: "#E0E0E0"), for: .normal)
                self.btnUpdateButton.isUserInteractionEnabled = false
            }else{
                self.viewCashInOff.isHidden = true
                self.tableview.isScrollEnabled = true
                //self.btnUpdateButton.isHidden = false
                self.btnUpdateButton.isUserInteractionEnabled = true
                self.btnUpdateButton.setTitleColor(UIColor.init(hex: "#1C2C99"), for: .normal)
                self.btnUpdateButton.backgroundColor = UIColor.init(hex: "#F5CE4E")
            }
        }
    }
    
    func showToastlocal(message : String) {
        let toastLabel = UILabel()
        toastLabel.frame = CGRect(x: (self.view.frame.width/2), y: 100, width: 300, height: 60)
        toastLabel.center.x = self.view.center.x
        toastLabel.text = message
        toastLabel.numberOfLines = 0
        toastLabel.font = UIFont(name: appFont,size: 14.0)
        toastLabel.textAlignment = .center
        toastLabel.numberOfLines = 2
        toastLabel.layer.cornerRadius = 30
        toastLabel.layer.masksToBounds = true
        toastLabel.textColor = .white
        toastLabel.backgroundColor = UIColor.black
        self.view.bringSubviewToFront(toastLabel)
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 5.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    
    @objc func showInformation(_ sender: UIButton) {
        self.showAlert(alertTitle: "".localized, alertBody: "Cash-Out service will deliver upto customer door step".localized, alertImage: UIImage(named: "infoQR")!)
    }
    
    @objc func onClickViewCommRate(_ sender: UIButton) {
        if CashOutModel.share.discountStatus ?? false {
            let viewComRate = self.storyboard?.instantiateViewController(withIdentifier: "ViewCommissionRateVC") as! ViewCommissionRateVC
            viewComRate.viewController = "CashOut"
            self.navigationController?.pushViewController(viewComRate, animated: true)
        }else {
            let vcStdRate = self.storyboard?.instantiateViewController(withIdentifier: "CashStandardRates") as! CashStandardRates
            vcStdRate.selectedRate = "Standard"
            vcStdRate.fromVC = "CashOut"
            self.navigationController?.pushViewController(vcStdRate, animated: true)
        }
    }
    
    @IBAction func onClickBack(_ sender: Any) {
     self.navigationController?.popViewController(animated: true)
    }
}

extension cashOut_UP: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return arrCashOutTitle[0].count
        }else if section == 1{
            return arrCashOutTitle[1].count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell: FirstCellCashOut = tableView.dequeueReusableCell(withIdentifier:"FirstCellCashOut") as! FirstCellCashOut
            if CashOutModel.share.Status {
                cell.btnSwitch.isOn = true
            }else {
                cell.btnSwitch.isOn = false
            }
            cell.btnSwitch.addTarget(self, action:#selector(categorySwitchValueChanged(_:)), for: .valueChanged)
            cell.lblTitle.text = arrCashOutTitle[0][indexPath.row]
            //cell.imgTitle.image = UIImage(named:"money")
            cell.btnInfo.isHidden = true
            return cell
        }else if indexPath.section == 1 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "FirstCellCashOut") as! FirstCellCashOut
                if CashOutModel.share.DeliveryServiceStatus {
                    cell.btnSwitch.isOn = true
                }else {
                    cell.btnSwitch.isOn = false
                }
                cell.btnSwitch.addTarget(self, action:#selector(deliveryServiceSwitch(_:)), for: .valueChanged)
                cell.lblTitle.text = "Delivery Service".localized
                cell.imgTitle.image = UIImage(named:"add_Vech_Bike")
                cell.btnInfo.isHidden = false
                cell.btnInfo.addTarget(self, action: #selector(showInformation(_ :)), for: .touchUpInside)
                return cell
            }else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessCellCashOut") as! BusinessCellCashOut
                let amount = self.arrCashOutTitle[1][1]
                cell.tfTitle.text = getDigitDisplay(amount)
                
                var font = UIFont(name: appFont, size: 18.0)
                if #available(iOS 13.0, *) {
                    font = UIFont.systemFont(ofSize: 18)
                }
                cell.tfTitle.font = font
                
                if ok_default_language == "my" {
                    cell.tfTitle.font = UIFont(name: appFont, size: 18)
                }else if ok_default_language == "en"{
                    cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                }else {

                    if #available(iOS 13.0, *) {
                        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                          .font : UIFont(name: appFont, size: 18)]
                        cell.tfTitle.attributedPlaceholder = NSAttributedString(string:  "Enter Maximum Available Amount", attributes:attributes as [NSAttributedString.Key : Any])
                    } else {
                        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                          .font : UIFont.systemFont(ofSize: 18)]
                        cell.tfTitle.attributedPlaceholder = NSAttributedString(string:  "Enter Maximum Available Amount", attributes:attributes)
                        cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                    }
                }
                
                
                cell.tfTitle.placeholder = "Enter Maximum Available Amount".localized
                cell.tfTitle.title.text = "Maximum Available Amount".localized
                cell.tfTitle.delegate = self
//                cell.tfTitle.rightView = nil
//                cell.tfTitle.rightView = mmkView
//                cell.tfTitle.rightViewMode = .always
                cell.tfTitle.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SecondCellCashOut") as! SecondCellCashOut
                cell.lblTitle.text = "Commission Rate".localized
                if CashOutModel.share.DiscountRateStatus {
                cell.btnComRate.setTitle("Special".localized, for: .normal)
                }else {
                cell.btnComRate.setTitle("Standard".localized, for: .normal)
                }
                cell.btnComRate.addTarget(self, action:#selector(onClickViewCommRate(_:)), for: .touchUpInside)
                return cell
            }
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier:"cell")
            return cell!
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }else{
            return 35
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 57
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var titile = ""
        if section == 1 {
            var str = ""
            titile = ""
            if CashOutModel.share.Status == false {
               str = ""
            }else {
               str = "Required fields*".localized
            titile = "Service Details".localized
            }
            
            return createRequiredFieldLabel(headerTitle: titile, reqTitle: str)
        }else {
            let view = UIView(frame: CGRect(x: 0, y: 0, width:0, height: 0))
            return view
        }
    }
    
    private func createRequiredFieldLabel(headerTitle:String, reqTitle: String) ->UIView? {
        let view1 = UIView(frame: CGRect(x: 5, y: 0, width:self.view.frame.width - 10, height: 35))
        view1.backgroundColor = hexStringToUIColor(hex: "#E0E0E0")
        let lblHeader = UILabel(frame: CGRect(x: 0, y: 0, width: (sizeOfString(myString: headerTitle)?.width)!, height:35))
        lblHeader.text = headerTitle
        lblHeader.textAlignment = .right
        lblHeader.font = UIFont(name: "Zawgyi-One", size: 14)
        lblHeader.backgroundColor = UIColor.clear
        lblHeader.textColor = UIColor.darkGray
        
        let reqLabel = UILabel(frame: CGRect(x: self.view.frame.width - (sizeOfString(myString: reqTitle)?.width)! - 10, y: 0, width: (sizeOfString(myString: reqTitle)?.width)!, height:35))
        reqLabel.text = reqTitle
        reqLabel.textAlignment = .right
        reqLabel.font = UIFont(name: "Zawgyi-One", size: 14)
        reqLabel.backgroundColor = UIColor.clear
        reqLabel.textColor = UIColor.red
        view1.addSubview(lblHeader)
        view1.addSubview(reqLabel)
        return view1
    }
    
    private func sizeOfString(myString : String) ->CGSize? {
        let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: "Zawgyi-One", size: 16) ?? UIFont.systemFont(ofSize: 16.0) ])
        return stringSize
    }
    
    @objc func categorySwitchValueChanged(_ sender : UISwitch!){
        if sender.isOn {
            sender.tintColor = UIColor.init(hex: "3DBA06")
            CashOutModel.share.Status = true
            self.arrCashOutTitle[1][1] = ""
            sender.setOn(true, animated: true)
            self.showServiceOffView(isShow: false)
            DispatchQueue.main.async {
                self.tableview.reloadData()
            }
        } else {
            sender.tintColor = UIColor.init(hex: "9A9A9A")
            CashOutModel.share.Status = false
            sender.setOn(false, animated: true)
            self.showServiceOffView(isShow: true)
            DispatchQueue.main.async {
                self.tableview.reloadData()
            }
            if UpdateProfileModel.share.cash_out != 1 {
                return
            }
              UpdateProfileModel.share.cash_out = 0
            var urlString   = Url.addUpdateProfileDetails
            urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let urlS = getUrl(urlStr: urlString, serverType: .updateProfile)
            UpdateProfileModel.share.updateProfileMenuStatus(urlStr: urlS , handle:{(success , response) in
                if success {
                    self.arrCashOutTitle = [["Cash-Out".localized],["Delivery Service".localized, CashOutModel.share.MaxAmount,"View Commission Rate".localized]]
                    CashOutModel.share.Status = true
                    self.showServiceOffView(isShow: true)
                    alertViewObj.wrapAlert(title:"", body: "Your Cash-Out Services has been disabled successfully".localized, img:#imageLiteral(resourceName: "bank_success"))
                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                        DispatchQueue.main.async {
                           // self.UpdateCashInService()
                           // self.callUpdateProfileService()
                            self.resetModelData()
                            self.tableview.reloadData()
                            UserDefaults.standard.set(false, forKey: "MerchantCashOutStatus")
                        }
                    })
                    DispatchQueue.main.async {
                        alertViewObj.showAlert(controller: self)
                    }
                }else {
                    sender.setOn(true, animated: true)
                    CashOutModel.share.Status = false
                }
            })
            
        }
    }
    
    
    @objc func deliveryServiceSwitch(_ sender : UISwitch!){
        if sender.isOn {
            sender.setOn(true, animated: true)
            CashOutModel.share.DeliveryServiceStatus = true
            sender.tintColor = UIColor.init(hex: "3DBA06")
        }else {
            sender.setOn(false, animated: true)
            CashOutModel.share.DeliveryServiceStatus = true
             sender.tintColor = UIColor.init(hex: "9A9A9A")
        }
    }
}


class FirstCellCashOut : UITableViewCell{
    @IBOutlet weak var btnSwitch: UISwitch!
    @IBOutlet weak var imgTitle: UIImageView!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var btnInfo: UIButton!
    override func awakeFromNib() {
        btnSwitch.layer.cornerRadius = btnSwitch.frame.height / 2
    }
}

class BusinessCellCashOut : UITableViewCell {
    @IBOutlet weak var tfTitle : FloatLabelTextField!{
        didSet
        {
            tfTitle.titleFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
        }
    }
}

class SecondCellCashOut: UITableViewCell {
    @IBOutlet weak var btnComRate: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
}



