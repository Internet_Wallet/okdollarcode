//
//  ViewCashBackRateVC.swift
//  OK
//
//  Created by gauri on 12/20/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit
var listCashBack = [Dictionary<String,String>]()
class ViewCashBackRateVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        listCashBack = [["network":"Telenor","cashBack": "5.5"],["network":"Ooredoo","cashBack": "6.5"],["network":"MPT","cashBack": "7.5"],["network":"MECTEL","cashBack": "8.5"],["network":"Mytel","cashBack": "9.5"]]
    }

    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ViewCashBackRateVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listCashBack.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 5 {
           let cell = tableView.dequeueReusableCell(withIdentifier: "NoteCashBackTopUp") as! NoteCashBackTopUp
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CashBackTopUp") as! CashBackTopUp
            let dic = listCashBack[indexPath.row]
            cell.lblNetwork.text = dic["network"]
            cell.lblCashBack.text = dic["cashBack"]
            return cell
        }
    }

    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 5 {
            return 250
        }else{
            return 57
        }
    }
    
    
}

class CashBackTopUp: UITableViewCell {
    @IBOutlet weak var lblNetwork: UILabel!
    @IBOutlet weak var lblCashBack: UILabel!
}

class NoteCashBackTopUp: UITableViewCell {
    
}
