//
//  setCustomCommissionRateVC.swift
//  OK
//
//  Created by Subhash Arya on 10/08/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol  setCustomCommissionRateVCDelegate{
    func getCustomAmountAndPersentage(strValue : String,strMode : String)
}

class setCustomCommissionRateVC: UIViewController {
    var delegate : setCustomCommissionRateVCDelegate?
    @IBOutlet weak var viewAlert : UIView!
    @IBOutlet weak var viewTextField : UIView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var tfAmount : UITextField!
    
    var modeStr = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTextField.layer.borderColor = UIColor(red:44/255, green:28/255, blue:153/255, alpha: 1).cgColor
        viewTextField.layer.borderWidth = 1
        viewTextField.layer.cornerRadius = 2
        viewAlert.layer.cornerRadius = 10
        viewAlert.layer.masksToBounds = true
        
    }

    func setFeeAndPercentMode(mode: String) {
        modeStr = mode
        if mode == "CashInRate" {
           lblHeader.text = "Enter Rate (1-200) MMK".localized
           tfAmount.placeholder = "Enter Amount".localized
            tfAmount.keyboardType = .numberPad
        }else if mode == "CashInPercentage" {
            lblHeader.text = "Enter Percentage (0-0.3) %".localized
            tfAmount.placeholder = "Enter Percentage".localized
              tfAmount.keyboardType = .decimalPad
        }else if mode == "CashOutRateFirst" || mode == "CashOutRateSecond" || mode == "CashOutRateThird"{
            lblHeader.text = "Enter Rate (1-900) MMK".localized
            tfAmount.placeholder = "Enter Amount"
              tfAmount.keyboardType = .numberPad
        } else if mode == "CashOutPercentage"{
            lblHeader.text = "Enter Percentage (0-1) %".localized
            tfAmount.placeholder = "Enter Percentage"
              tfAmount.keyboardType = .decimalPad
        }else if mode == "OtherServiceFees"{
            lblHeader.text = "Enter Amount (1-500) MMK".localized
            tfAmount.placeholder = "Enter Amount"
            tfAmount.keyboardType = .numberPad
        }
    }
    
    @IBAction func endEditingChanged(_ sender: UITextField) {
        if modeStr == "CashInRate"{
            let amount = Int(sender.text!)
        if amount != nil {
            if !(amount! <= 200 && amount! > 0) {
                sender.text?.removeLast()
                self.showToastlocal(message:"Please Enter the Amount less than or equal to 200".localized)
                    }
            }
        }else if modeStr == "CashInPercentage"{
            let amount = Float(sender.text!)
            if amount != nil {
                if !(amount! <= 0.3 && amount! > 0.0) {
                    sender.text?.removeLast()
                    self.showToastlocal(message:"Please Enter the Percentage less than or equal to 0.3".localized)
                }
            }
        }else if modeStr == "CashOutRateFirst" || modeStr == "CashOutRateSecond" || modeStr == "CashOutRateThird"{
            let amount = Int(sender.text!)
            if amount != nil {
                if !(amount! <= 900 && amount! > 0) {
                    sender.text?.removeLast()
                    self.showToastlocal(message:"Please Enter the Amount less than or equal to 900".localized)
                }
            }
        }else if modeStr == "CashOutPercentage"{
            let amount = Float(sender.text!)
            if amount != nil {
                if !(amount! <= 1 && amount! > 0.0) {
                    sender.text?.removeLast()
                    self.showToastlocal(message:"Please Enter the Percentage less than or equal to 1".localized)
                }
            }
        }else if modeStr == "OtherServiceFees"{
            let amount = Int(sender.text!)
            if amount != nil {
                if !(amount! <= 500 && amount! > 0) {
                    sender.text?.removeLast()
                    self.showToastlocal(message:"Please Enter the Amount less than or equal to 500".localized)
                }
            }
        }
        
    }
    
    @IBAction func clickOnTapGuesture(_ sender : UITapGestureRecognizer) {
        //self.view.removeFromSuperview()
    }
    
    @IBAction func clickOnOK(_ sender : UIButton){
        if let del = delegate{
            if !(tfAmount.text! == "") {
                del.getCustomAmountAndPersentage(strValue: tfAmount.text!,strMode : modeStr)
            }
        }
        self.view.removeFromSuperview()
    }
    
    @IBAction func clickOnCancel(_ sender : UIButton){
          self.view.removeFromSuperview()
    }
    
    
    func showToastlocal(message : String) {
        let toastLabel = UILabel()
        toastLabel.frame = CGRect(x: (self.view.frame.width/2) - 150, y: 100, width: 300, height: 60)
        toastLabel.text = message
        toastLabel.numberOfLines = 0
        toastLabel.font = UIFont(name: "Zawgyi-One",size: 14.0)
        toastLabel.textAlignment = .center
        toastLabel.numberOfLines = 2
        toastLabel.layer.cornerRadius = 30
        toastLabel.layer.masksToBounds = true
        toastLabel.textColor = .white
        toastLabel.backgroundColor = UIColor.black
        self.view.bringSubviewToFront(toastLabel)
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 5.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }

}
