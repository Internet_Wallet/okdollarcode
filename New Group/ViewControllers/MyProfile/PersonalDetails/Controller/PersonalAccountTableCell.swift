//
//  PersonalAccountTableCell.swift
//  OK
//
//  Created by SHUBH on 10/9/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Rabbit_Swift

class PersonalAccountTableCell: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}

//MARK: Table View Extension
extension PersonalAccountVC : UITableViewDataSource,UITableViewDelegate,CountryViewControllerDelegate {
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView.tag != 1 {
            if(UserModel.shared.accountType == "1") {//personal user
                return 6
            }else {
                return 5
            }
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag != 1 {
            return 1
        }
        return self.addressArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView.tag != 1 {
            return 25
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 1 {
            return 60
        }else {
            if(UserModel.shared.accountType == "1"){//Personal user
                if indexPath.section == 0 {
                    return 629
                }else if indexPath.section == 1 {
                    if boolVillageTrack {
                        return 421
                    }
                    return 364
                }else if indexPath.section == 2 {
                    return 100
                }else if indexPath.section == 3 {
                    return 55
                }else if indexPath.section == 4 {
                    return 150
                }else if indexPath.section == 5 {
                    return 110
                }
            }else {
                if indexPath.section == 0 {
                    return 629
                }else if indexPath.section == 1 {
                    if boolVillageTrack {
                        return 421
                    }
                    return 364
                }else if indexPath.section == 2 {
                    return 100
                }else if indexPath.section == 3 {
                    return 150
                }else if indexPath.section == 4 {
                    return 110
                }
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width:200, height: 1))
        view.backgroundColor = .clear
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if(UserModel.shared.accountType == "1") {//personal user
            if section == 0 {
                return self.showHeaderViewWithRequiredTitle(strHeaderTitle: "Personal Account Information")
            }else if section == 1 {
                return self.showHeaderViewWithRequiredTitle(strHeaderTitle: "Home Address Details")
            }else if section == 2 {
                let view = UIView(frame: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 25))
                let label = UILabel(frame: CGRect(x: 15, y: 0, width: self.view.frame.width - 15, height:25))
                label.text = "Security Questions".localized
                label.textAlignment = NSTextAlignment.left
                label.font = UIFont(name: appFont, size: 14)
                label.textColor = UIColor.darkGray
                label.backgroundColor = UIColor.clear
                view.addSubview(label)
                
                let buttonInfo = UIButton(frame: CGRect(x: self.view.frame.width - 150, y: 3, width: 20, height:20))
                buttonInfo.tag = 2
                buttonInfo.addTarget(self, action: #selector(self.infoAction(sender:)), for: .touchUpInside)
                buttonInfo.setBackgroundImage(UIImage(named:"info")!, for: UIControl.State.normal)
                view.addSubview(buttonInfo)
                view.backgroundColor = UIColor.init(red: 223/255, green: 223/255, blue: 223/255, alpha: 1)
                return view
            }else if section == 3 {
                let view = UIView(frame: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 25))
                let imgLeftIcon = UIImageView(frame: CGRect(x: 5, y: 5, width: 15, height:15))
                imgLeftIcon.image = UIImage(named: "alternativeMobile")
                view.addSubview(imgLeftIcon)
                
                let label = UILabel(frame: CGRect(x: 25, y: 0, width: self.view.frame.width - 15, height:25))
                label.text = "Alternative Mobile Number".localized
                label.textAlignment = NSTextAlignment.left
                label.font = UIFont(name: appFont, size: 14)
                label.textColor = UIColor.darkGray
                label.backgroundColor = UIColor.clear
                view.addSubview(label)
                
                let buttonInfo = UIButton(frame: CGRect(x: self.view.frame.width - 150, y: 3, width: 20, height:20))
                buttonInfo.tag = 3
                buttonInfo.addTarget(self, action: #selector(self.infoAction(sender:)), for: .touchUpInside)
                buttonInfo.setBackgroundImage(UIImage(named:"info")!, for: UIControl.State.normal)
                view.addSubview(buttonInfo)
                
                let labelRequired = UILabel(frame: CGRect(x: self.view.frame.width - 50, y: 5, width: 25, height: 25))
                labelRequired.text = "*".localized
                labelRequired.backgroundColor = UIColor.clear
                labelRequired.textAlignment = NSTextAlignment.right
                labelRequired.font = UIFont(name: appFont, size: 14)
                labelRequired.textColor = UIColor.red
                view.addSubview(labelRequired)
                
                view.backgroundColor = UIColor.init(red: 223/255, green: 223/255, blue: 223/255, alpha: 1)
                return view
            }else if section == 4 {
                return self.showHeaderViewWithTitle(strHeaderTitle: "My Contact Details")
            }else if section == 5 {
                return self.showHeaderViewWithTitle(strHeaderTitle: "My Profile Status")
            }else {
                let view = UIView(frame: CGRect(x: 0, y: 0, width:0, height: 1))
                return view
            }
        }else {//Merchant User
            if section == 0 {
                return self.showHeaderViewWithRequiredTitle(strHeaderTitle: "Merchant Account Information")
            }else if section == 1 {
                return self.showHeaderViewWithRequiredTitle(strHeaderTitle: "Address Details")
            }else if section == 2 {
                let view = UIView(frame: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 25))
                let label = UILabel(frame: CGRect(x: 15, y: 0, width: self.view.frame.width - 15, height:25))
                label.text = "Security Questions".localized
                label.textAlignment = NSTextAlignment.left
                label.font = UIFont(name: appFont, size: 14)
                label.textColor = UIColor.darkGray
                label.backgroundColor = UIColor.clear
                view.addSubview(label)
                
                let buttonInfo = UIButton(frame: CGRect(x: self.view.frame.width - 150, y: 3, width: 20, height:20))
                buttonInfo.tag = 2
                buttonInfo.addTarget(self, action: #selector(self.infoAction(sender:)), for: .touchUpInside)
                buttonInfo.setBackgroundImage(UIImage(named:"info")!, for: UIControl.State.normal)
                view.addSubview(buttonInfo)
                view.backgroundColor = UIColor.init(red: 223/255, green: 223/255, blue: 223/255, alpha: 1)
                return view
            }else if section == 3 {
                return self.showHeaderViewWithRequiredTitle(strHeaderTitle: "My Contact Details")
            }else if section == 4 {
                return self.showHeaderViewWithTitle(strHeaderTitle: "Merchant Profile Status")
            }else {
                let view = UIView(frame: CGRect(x: 0, y: 0, width:0, height: 1))
                return view
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 1 {
            let identifier = "StreetCell"
            var cell: StreetCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? StreetCell
            if cell == nil {
                tableView.register(UINib(nibName: "StreetCell", bundle: nil), forCellReuseIdentifier: identifier)
                cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? StreetCell
            }
            cell.backgroundColor = UIColor(red: 236, green: 236, blue: 236, alpha: 1.0)
            cell.lblFullAdress.isHidden = false
            cell.lblStreetName.isHidden = false
            cell.lblVillage.isHidden = true
            
            if let obj = addressArray[safe: indexPath.row] {
                cell.lblStreetName.text = parabaik.uni(toZawgyi: obj.shortName) ?? ""
                cell.lblFullAdress.text = parabaik.uni(toZawgyi: obj.address) ?? ""
            }
            return cell
        }else {
            let cell:PAMyPersonalProfileCell! = tableView.dequeueReusableCell(withIdentifier:"PAMyPersonalProfileCell") as? PAMyPersonalProfileCell
            if indexPath.section == 0 {
                let identifier = "PAPersonalAccountCell"
                var cellView: PAPersonalAccountCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? PAPersonalAccountCell
                if cellView == nil {
                    tableView.register (UINib(nibName: "PAPersonalAccountCell", bundle: nil), forCellReuseIdentifier: identifier)
                    cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? PAPersonalAccountCell
                }
                
                cellView.lblName.text = UserModel.shared.name
                if(UserModel.shared.gender == "1") {
                    cellView.lblYearsGender.text = (self.getDOBYear(UserModel.shared.dob)) + " Years, " + "Male"//"26 Years, Male"
                }else {
                    cellView.lblYearsGender.text = (self.getDOBYear(UserModel.shared.dob)) + " Years, " + "Female"//"26 Years, Male"
                }
                //cellView.imgUser = UserModel.shared.proPic
                cellView.imgUser.layer.cornerRadius =  cellView.imgUser!.frame.size.width/2
                cellView.imgUser.layer.masksToBounds = true
                
                var fullURL = UserModel.shared.proPic
                fullURL = fullURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                let url = URL.init(string: fullURL)
                cellView.imgUser.sd_setImage(with: url, placeholderImage: UIImage(named: "avatar"))
                
                if(UserModel.shared.accountType == "0"){
                    cellView.txtAccountType.text = "Merchant".localized
                }else if (UserModel.shared.accountType == "1") {
                    cellView.txtAccountType.text = "Personal".localized
                }
                
                cellView.txtAccountMobNo.text = self.getActualNum(UserModel.shared.mobileNo, withCountryCode: "+95")
                cellView.txtJoinOn.text = self.getJoiningDate(UserModel.shared.createdDate)
                cellView.txtDOB.text = UserModel.shared.dob
                cellView.lblYearsJoinOn.text = self.getJoiningYear(UserModel.shared.createdDate)
                cellView.lblNetwork.text = getOparatorName(UserModel.shared.mobileNo)
                cellView.btnImageInfo.tag = 1
                cellView.btnImageInfo.addTarget(self, action:#selector(self.infoAction(sender:)), for: .touchUpInside)
                cellView.btnNationality.addTarget(self, action:#selector(self.btnNationalityAction(sender:)), for: .touchUpInside)
                cellView.txtNationality.text = personalInfoModel.nationality//UserModel.shared.country
                cellView.imgViewNationality.image = UIImage.init(named: personalInfoModel.nationalityCode)
                
                if(UserModel.shared.idType == "01") {
                    cellView.imgViewPassport.image = UIImage(named: "nrc")
                    cellView.lblFront.text = "front side".localized
                    cellView.lblBack.text = "back side".localized
                    cellView.txtNRCNumber.text = UserModel.shared.nrc
                    if cellView.txtNRCNumber.text?.count ?? 0 > 0 {
                        cellView.txtNRCNumber.placeholder = "NRC Number & Image".localized
                    }else {
                        cellView.txtNRCNumber.placeholder = "Attach NRC Number & Image".localized
                    }
                    
                    //Front Image
                    cellView.btnFrontNRC.tag = 1
                    cellView.btnBackNRC.tag = 2
                    
                    if arrImageData[0] == "0" {
                        cellView.btnFrontNRC.addTarget(self, action:#selector(self.btnImageSelectionAction(sender:)), for: .touchUpInside)
                    }else {
                        cellView.btnFrontNRC.addTarget(self, action:#selector(self.btnPreviewImageAction(sender:)), for: .touchUpInside)
                    }
                    
                    if arrImageData[1] == "0" {
                        cellView.btnBackNRC.addTarget(self, action:#selector(self.btnImageSelectionAction(sender:)), for: .touchUpInside)
                    }else {
                        cellView.btnBackNRC.addTarget(self, action:#selector(self.btnPreviewImageAction(sender:)), for: .touchUpInside)
                    }
                    cellView.imgViewFront.sd_setImage(with: self.getImageURLFromString(UserModel.shared.idPhoto), placeholderImage: UIImage(named: "nrc"))
                    cellView.imgViewBack.sd_setImage(with: self.getImageURLFromString(UserModel.shared.idPhoto1), placeholderImage: UIImage(named: "nrc"))
                }else {
                    cellView.imgViewPassport.image = UIImage(named: "passport")
                    cellView.txtNRCNumber.text = UserModel.shared.nrc
                    cellView.lblFront.text = "main page".localized
                    cellView.lblBack.text = "validity page".localized
                    cellView.btnFrontNRC.tag = 3
                    cellView.btnFrontNRC.addTarget(self, action:#selector(self.btnImageSelectionAction(sender:)), for: .touchUpInside)
                    cellView.btnBackNRC.tag = 4
                    cellView.btnBackNRC.addTarget(self, action:#selector(self.btnImageSelectionAction(sender:)), for: .touchUpInside)
                    cellView.imgViewFront.sd_setImage(with: self.getImageURLFromString(UserModel.shared.idPhoto), placeholderImage: UIImage(named: "passportGray"))
                    cellView.imgViewBack.sd_setImage(with: self.getImageURLFromString(UserModel.shared.idPhoto1), placeholderImage: UIImage(named: "passportGray"))
                    
                    if cellView.txtNRCNumber.text?.count ?? 0 > 0 {
                        cellView.txtNRCNumber.placeholder = "Passport Number & Image".localized
                    }else {
                        cellView.txtNRCNumber.placeholder = "Attach Passport Number & Image".localized
                    }
                }
                
                cellView.txtDateOfExp.text = UserModel.shared.idExpDate
                cellView.txtFatherName.text = UserModel.shared.fatherName
                cellView.cellWrapper(statusBool : boolEditView)
                cellView.selectionStyle = .none
                return cellView
            }else if indexPath.section == 1 {
                
                let identifier = "PAHomeAddressCell"
                var cell: PAHomeAddressCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? PAHomeAddressCell
                if cell == nil {
                    tableView.register(UINib(nibName: "PAHomeAddressCell", bundle: nil), forCellReuseIdentifier: identifier)
                    cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? PAHomeAddressCell
                }
                
                cell.btnDivision.addTarget(self, action:#selector(self.btnDivisionAction(sender:)), for: .touchUpInside)
                cell.btnTownship.addTarget(self, action:#selector(self.btnTownshipAction(sender:)), for: .touchUpInside)
                cell.txtDivision.text = addressModel.division
                cell.txtTownship.text = addressModel.township
                cell.txtCity.text = addressModel.city
                cell.txtVillTract.tag = 13
                cell.txtVillTract.delegate = self
                cell.txtVillTract.text = addressModel.villgTrack
                if boolVillageTrack {
                    cell.viewVillageTrackConstraintHeight.constant = 57
                }else {
                    cell.viewVillageTrackConstraintHeight.constant = 0
                }
                cell.txtStreet.tag = 14
                cell.txtStreet.delegate = self
                cell.txtStreet.text = addressModel.street
                cell.txtStreet.addTarget(self, action: #selector(PersonalAccountVC.textFieldDidChange(_:)),
                                         for: UIControl.Event.editingChanged)
                cell.txtHouseNo.tag = 15
                cell.txtHouseNo.delegate = self
                cell.txtHouseNo.text = addressModel.houseNo
                cell.txtFloorNo.tag = 16
                cell.txtFloorNo.delegate = self
                cell.txtFloorNo.text = addressModel.floorNo
                cell.txtRoomNo.tag = 17
                cell.txtRoomNo.delegate = self
                cell.txtRoomNo.text = addressModel.roomNo
                cell.txtHousingName.tag = 18
                cell.txtHousingName.delegate = self
                cell.txtHousingName.text = addressModel.housingZoneName
                //User interaction for cell
                cell.cellWrapper(statusBool : boolEditView)
                cell.selectionStyle = .none
                return cell
            }else if indexPath.section == 2 {
                let identifier = "PASecurityQuestionsCell"
                var cell: PASecurityQuestionsCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? PASecurityQuestionsCell
                if cell == nil {
                    tableView.register(UINib(nibName: "PASecurityQuestionsCell", bundle: nil), forCellReuseIdentifier: identifier)
                    cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? PASecurityQuestionsCell
                }
                cell.txtSecurityQuestion.text = securityQuestionsModel.securityQuestion
                cell.txtSecurityAnswer.tag = 21
                cell.txtSecurityAnswer.delegate = self
                cell.txtSecurityAnswer.text = securityQuestionsModel.securityAnswer
                cell.btnSecurityQuestions.addTarget(self, action:#selector(self.btnSecurityQuestionsAction(sender:)), for: .touchUpInside)
                //User interaction for cell
                cell.cellWrapper(boolEditView : boolEditView)
                cell.selectionStyle = .none
                return cell
            }
            if(UserModel.shared.accountType == "1") {//personal user
                if indexPath.section == 3 {
                    let identifier = "PAAlternativeMobileNoCell"
                    var cell: PAAlternativeMobileNoCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? PAAlternativeMobileNoCell
                    if cell == nil {
                        tableView.register(UINib(nibName: "PAAlternativeMobileNoCell", bundle: nil), forCellReuseIdentifier: identifier)
                        cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? PAAlternativeMobileNoCell
                    }
                    cell.txtAltPhoneNumber.delegate = self
                    cell.txtAltPhoneNumber.tag = 29
                    cell.txtAltPhoneNumber.text = myConatctModel.altMobNumber
                    cell.txtAltPhoneNumber.addTarget(self, action: #selector(PersonalAccountVC.textFieldDidChange(_:)),
                                                     for: UIControl.Event.editingChanged)
                    if boolEditView {
                        cell.txtAltPhoneNumber.isUserInteractionEnabled = true
                    }else {
                        cell.txtAltPhoneNumber.isUserInteractionEnabled = false
                    }
                    return cell
                }else if indexPath.section == 4 {
                    let identifier = "PAMyContactDetailsCell"
                    var cell: PAMyContactDetailsCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? PAMyContactDetailsCell
                    if cell == nil {
                        tableView.register(UINib(nibName: "PAMyContactDetailsCell", bundle: nil), forCellReuseIdentifier: identifier)
                        cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? PAMyContactDetailsCell
                    }
                    cell.txtPhoneNumber.delegate = self
                    cell.txtPhoneNumber.tag = 30
                    cell.txtPhoneNumber.text = myConatctModel.phoneNumber //UserModel.shared.phoneNumber
                    cell.txtPhoneNumber.addTarget(self, action: #selector(PersonalAccountVC.textFieldDidChange(_:)),
                                                  for: UIControl.Event.editingChanged)
                    cell.txtEmailAddress.delegate = self
                    cell.txtEmailAddress.tag = 31
                    cell.txtEmailAddress.text = myConatctModel.emailID
                    cell.txtFacebookID.delegate = self
                    cell.txtFacebookID.tag = 32
                    cell.txtFacebookID.text = myConatctModel.facebookID
                    cell.btnContact.addTarget(self, action:#selector(self.btnAddContactAction(sender:)), for: .touchUpInside)
                    if cell.txtPhoneNumber.text?.count == 0 {
                        cell.txtPhoneNumber.placeholder = "Enter Phone Number".localized
                    }else {
                        cell.txtPhoneNumber.placeholder = "Phone Number".localized
                    }
                    if cell.txtEmailAddress.text?.count == 0 {
                        cell.txtEmailAddress.placeholder = "Enter Email Address".localized
                    }else {
                        cell.txtEmailAddress.placeholder = "Email Address".localized
                    }
                    if cell.txtFacebookID.text?.count == 0 {
                        cell.txtFacebookID.placeholder = "Enter Facebook ID".localized
                    }else {
                        cell.txtFacebookID.placeholder = "Facebook ID".localized
                    }
                    //User interaction for cell
                    cell.cellWrapper(boolEditView : boolEditView)
                    cell.selectionStyle = .none
                    return cell
                }else if indexPath.section == 5 {
                    let cell:PAMyPersonalProfileCell! = tableView.dequeueReusableCell(withIdentifier:"PAMyPersonalProfileCell") as? PAMyPersonalProfileCell
                    cell.wrapDataIntoCell(addressModel: addressModel, myConatctModel: myConatctModel, securityQuestionsModel: securityQuestionsModel, personalInfoModel:  personalInfoModel)
                    personalInfoModel.profileStatus = Int(cell.percentageValue)
                    cell.selectionStyle = .none
                    return cell
                }
            }else { //Merchant Users
                if indexPath.section == 3 {
                    let identifier = "PAMyContactDetailsCell"
                    var cell: PAMyContactDetailsCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? PAMyContactDetailsCell
                    if cell == nil {
                        tableView.register(UINib(nibName: "PAMyContactDetailsCell", bundle: nil), forCellReuseIdentifier: identifier)
                        cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? PAMyContactDetailsCell
                    }
                    cell.txtPhoneNumber.delegate = self
                    cell.txtPhoneNumber.tag = 30
                    cell.txtPhoneNumber.text = myConatctModel.phoneNumber //UserModel.shared.phoneNumber
                    cell.txtPhoneNumber.addTarget(self, action: #selector(PersonalAccountVC.textFieldDidChange(_:)),
                                                  for: UIControl.Event.editingChanged)
                    cell.txtEmailAddress.delegate = self
                    cell.txtEmailAddress.tag = 31
                    cell.txtEmailAddress.text = myConatctModel.emailID
                    cell.txtFacebookID.delegate = self
                    cell.txtFacebookID.tag = 32
                    cell.txtFacebookID.text = myConatctModel.facebookID
                    cell.btnContact.addTarget(self, action:#selector(self.btnAddContactAction(sender:)), for: .touchUpInside)
                    
                    if cell.txtPhoneNumber.text?.count == 0 {
                        cell.txtPhoneNumber.placeholder = "Enter Phone Number".localized
                    }else {
                        cell.txtPhoneNumber.placeholder = "Phone Number".localized
                    }
                    if cell.txtEmailAddress.text?.count == 0 {
                        cell.txtEmailAddress.placeholder = "Enter Email Address".localized
                    }else {
                        cell.txtEmailAddress.placeholder = "Email Address".localized
                    }
                    if cell.txtFacebookID.text?.count == 0 {
                        cell.txtFacebookID.placeholder = "Enter Facebook ID".localized
                    }else {
                        cell.txtFacebookID.placeholder = "Facebook ID".localized
                    }
                    //User interaction for cell
                    cell.cellWrapper(boolEditView : boolEditView)
                    cell.selectionStyle = .none
                    return cell
                }else if indexPath.section == 4 {
                    let cell:PAMyPersonalProfileCell! = tableView.dequeueReusableCell(withIdentifier:"PAMyPersonalProfileCell") as? PAMyPersonalProfileCell
                    cell.wrapDataIntoCell(addressModel: addressModel, myConatctModel: myConatctModel, securityQuestionsModel: securityQuestionsModel, personalInfoModel:  personalInfoModel)
                    personalInfoModel.profileStatus = Int(cell.percentageValue)
                    cell.selectionStyle = .none
                    return cell
                }
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if tableView.tag == 1 {
            let dic = addressArray[indexPath.row]
            addressModel.street = Rabbit.uni2zg(dic.shortName ?? "")
            addressArray.removeAll()
            tblStreetList.isHidden = true
            tblMainList.reloadData()
        }
    }
    
    //MARK:- Table Custom Methods
    
    @objc func btnNationalityAction(sender:UIButton) {
        println_debug("btnNationalityAction")
        let sb = UIStoryboard(name: "Country", bundle: nil)
        let countryVC = sb.instantiateViewController(withIdentifier: "CountryViewController") as! CountryViewController
        countryVC.delegate = self
        self.navigationController?.present(countryVC, animated: false, completion: nil)
    }
    
    func countryViewControllerCloseAction(_ list: CountryViewController) {
        list.dismiss(animated: true, completion: nil)
    }
    
    func countryViewController(_ list: CountryViewController, country: Country) {
        personalInfoModel.nationality = country.name
        personalInfoModel.nationalityCode = country.code
        list.dismiss(animated: false, completion: nil)
        self.tblMainList.reloadSections(IndexSet(integer: 0), with: .automatic);
    }
    
    @objc func infoAction(sender:UIButton) {
        println_debug("infoAction")
        if(sender.tag == 1) {
            alertViewObj.wrapAlert(title: nil, body: "This ID Proof Attachment used to verify the account information.".localized, img: #imageLiteral(resourceName: "info_one"))
        }else {
            alertViewObj.wrapAlert(title: nil, body: "This is the Security Question to verify the account details.".localized, img: #imageLiteral(resourceName: "info_one"))
        }
        alertViewObj.addAction(title: "OK".localized, style: .cancel) {}
        alertViewObj.showAlert(controller: self)
    }
    
    @IBAction func btnDivisionAction(sender: UIButton) {
        let stateDivisionVC =  UIStoryboard(name: "Registration", bundle: Bundle.main).instantiateViewController(withIdentifier: "StateDivisionVC")as! StateDivisionVC
        stateDivisionVC.stateDivisonVCDelegate = self
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(stateDivisionVC, animated: true)
            }
        }
    }
    
    @IBAction func btnTownshipAction(sender: UIButton) {
        let addressTownshiopVC =  UIStoryboard(name: "Registration", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddressTownshiopVC")as! AddressTownshiopVC
        addressTownshiopVC.addressTownshiopVCDelegate = self
        addressTownshiopVC.selectedDivState = locationDetails
        self.navigationController?.pushViewController(addressTownshiopVC, animated: true)
    }
    
    @IBAction func btnSecurityQuestionsAction(sender: UIButton) {
        println_debug("btnSecurityQuestionsAction")
        //Load Question View
        let sb = UIStoryboard(name: "MyProfile" , bundle: nil)
        let securityQuestionR  = sb.instantiateViewController(withIdentifier: "PASecurityQuestionVC") as! PASecurityQuestionVC
        addChild(securityQuestionR)
        
        if let window = UIApplication.shared.keyWindow {
            window.rootViewController?.addChild(securityQuestionR)
            securityQuestionR.view.frame = .init(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
            securityQuestionR.securityQuestionRDelegate = self
            window.addSubview(securityQuestionR.view)
            window.makeKeyAndVisible()
        }
    }
    
    func SelectedQuestion(questionDic: Dictionary<String, AnyObject>) {
        securityQuestionsModel.securityQuestion = (questionDic["Question"] as? String)!
        securityQuestionsModel.securityQuestionCode = (questionDic["QuestionCode"] as? String)!
        self.tblMainList.reloadSections(IndexSet(integer: 2), with: .automatic);
    }
    
    @IBAction func btnAddContactAction(sender: UIButton) {
        println_debug("btnAddContactAction")
        let nav = UitilityClass.openContact(multiSelection: false, self)
        self.present(nav, animated: true, completion: nil)
    }
    
    
    
    //New Image View
    @objc func btnPreviewImageAction(sender: UIButton!) {
        println_debug("btnPreviewImageAction")
        let personalDetailVC = UIStoryboard(name: "MyProfile", bundle: Bundle.main).instantiateViewController(withIdentifier: "ImagePreviewViewController")  as! ImagePreviewViewController
        
        if(sender.tag == 1) {
            personalDetailVC.statusData = "NRC Front"
            personalDetailVC.fullURL =  UserModel.shared.idPhoto
        } else if(sender.tag == 2) {
            personalDetailVC.statusData = "NRC Back"
            personalDetailVC.fullURL = UserModel.shared.idPhoto1
        }else if(sender.tag == 3) {
            personalDetailVC.statusData = "Passport Front"
            personalDetailVC.fullURL = UserModel.shared.idPhoto
        }else {
            personalDetailVC.statusData = "Passport Back"
            personalDetailVC.fullURL = UserModel.shared.idPhoto1
        }
        self.navigationController?.pushViewController(personalDetailVC, animated: true)
    }
    
    @objc func btnImageSelectionAction(sender: UIButton!) {
        println_debug("imageSelectionAction")
        let imgEditVC = UIStoryboard(name: "MyProfile", bundle: Bundle.main).instantiateViewController(withIdentifier: "PAImagePreviewVC")  as! PAImagePreviewVC
        imgEditVC.imagePreviewVCDelegate = self
        
        if(sender.tag == 1) {
            imgEditVC.statusData = "NRC Front"
            imgEditVC.fullURL = UserModel.shared.idPhoto
        }else if(sender.tag == 2) {
            imgEditVC.statusData = "NRC Back"
            imgEditVC.fullURL = UserModel.shared.idPhoto1
        }else if(sender.tag == 3) {
            imgEditVC.statusData = "Passport Front"
            imgEditVC.fullURL = UserModel.shared.idPhoto1
        }else {
            imgEditVC.statusData = "Passport Back"
            imgEditVC.fullURL = UserModel.shared.idPhoto1
        }
        self.navigationController?.pushViewController(imgEditVC, animated: true)
    }
    
    // MARK: - Image Display Delegate
    func ImagePreviewUrl(imageName : String, imageURL: String) {
        println_debug(imageName)
        switch imageName {
        case "NRC Front" :
            UserModel.shared.idPhoto = imageURL
            self.personalInfoModel.attachNRCFront = imageURL
            arrImageData[0] = "1"
        case "NRC Back" :
            UserModel.shared.idPhoto1 = imageURL
            self.personalInfoModel.attachNRCBack = imageURL
            arrImageData[1] = "1"
        case "Passport Front" :
            self.personalInfoModel.attachPassport = imageURL
            UserModel.shared.idPhoto = imageURL
            arrImageData[0] = "1"
        case "Passport Back" :
            self.personalInfoModel.attachPassport = imageURL
            UserModel.shared.idPhoto = imageURL
            arrImageData[1] = "1"
        default     : println_debug("Hello!")
        }
        DispatchQueue.main.async {
            self.tblMainList.reloadSections(IndexSet(integer: 0), with: .automatic);
        }
    }
    
    func setDivisionStateName(SelectedDic: LocationDetail) {
        println_debug(SelectedDic.stateOrDivitionNameEn)
        if(appDel.currentLanguage == "my") {
            addressModel.division = SelectedDic.stateOrDivitionNameMy
        }else {
            addressModel.division = SelectedDic.stateOrDivitionNameEn
        }
        addressModel.divisionCode = SelectedDic.stateOrDivitionCode
        locationDetails = SelectedDic
        self.tblMainList.reloadSections(IndexSet(integer: 1), with: .automatic);
        
    }
    
    func setTownshipCityNameFromDivison(Dic: TownShipDetailForAddress) {
        DispatchQueue.main.async {
            VillageManager.getVillageAndStreet(townshipCode:  Dic.townShipCode)
        }
        if(appDel.currentLanguage == "my") {
            addressModel.city = Dic.cityNameMY
            addressModel.township = Dic.townShipNameMY
        }else {
            addressModel.city = Dic.cityNameEN
            addressModel.township = Dic.townShipNameEN
        }
        addressModel.townshipCode = Dic.townShipCode
        self.showHideVillageTrack(Dic: Dic)
        self.tblMainList.reloadSections(IndexSet(integer: 1), with: .automatic);
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(10), execute: {
            self.villageFuntion()
        })
    }
    
    func setTownshipCityName(cityDic : TownShipDetailForAddress) {
        
        DispatchQueue.main.async {
            VillageManager.getVillageAndStreet(townshipCode: cityDic.townShipCode)
        }
        
        if(appDel.currentLanguage == "my") {
            addressModel.city = cityDic.cityNameMY
            addressModel.township = cityDic.townShipNameMY
        }else {
            addressModel.city = cityDic.cityNameEN
            addressModel.township = cityDic.townShipNameEN
        }
        
        self.showHideVillageTrack(Dic: cityDic)
        self.tblMainList.reloadSections(IndexSet(integer: 1), with: .automatic);
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(10), execute: {
            self.villageFuntion()
        })
    }
    
    func showHideVillageTrack(Dic : TownShipDetailForAddress) {
        boolVillageTrack = true
        if Dic.GroupName.contains(find: "YCDC") || Dic.GroupName.contains(find: "MCDC") {
            boolVillageTrack = false
        }
        if Dic.cityNameEN == "Amarapura" || Dic.cityNameEN == "Patheingyi" || Dic.townShipNameEN == "PatheinGyi" {
            boolVillageTrack = true
        }
    }
    
    func villageFuntion() {
        let village = VillageManager.shared.allVillageList
        println_debug(village)
        if village.isVillageList{
            let dic = village.villageArray[0]
            if(appDel.currentLanguage == "my") {
                addressModel.villgTrack = dic["BurmeseName"]!
            }else {
                addressModel.villgTrack = dic["Name"]!
            }
            self.tblMainList.reloadSections(IndexSet(integer: 1), with: .automatic);
        }
    }
}
