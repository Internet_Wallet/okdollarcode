//
//  PAListDataVC.swift
//  OK
//
//  Created by SHUBH on 8/7/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol  PAListDataVCDelegate{
    func SelectListData(selectedName : String,selectedOption: String)
}

class PAListDataVC: OKBaseController {

    //Manage PopOver
    @IBOutlet weak var viewOthers: UIView!
    @IBOutlet weak var viewTransparent: UIView!

    @IBOutlet weak var lblTitleOther: UILabel!
    @IBOutlet weak var txtFieldOther: UITextField!
    @IBOutlet weak var btnListCancel: UIButton!
        {
        didSet
        {
            btnListCancel.setTitle("Cancel".localized, for : .normal)
        }
    }
    @IBOutlet weak var btnCancel: UIButton!
        {
        didSet
        {
            btnCancel.setTitle("Cancel".localized, for : .normal)
        }
    }
    @IBOutlet weak var btnOK: UIButton!
        {
        didSet
        {
            btnOK.setTitle("OK".localized, for : .normal)
        }
    }
    //Main View
    var listDataVCDelegate : PAListDataVCDelegate?

    @IBOutlet weak var tableview: UITableView!

    var arrOwnershipType = [String]()
    
    var statusData = "Ownership"
    
    //MARK: -View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        
        viewOthers.isHidden = true
        viewTransparent.isHidden = true
        lblTitleOther.text = statusData
        txtFieldOther.placeholder = "Enter " + statusData
        
        if statusData == "Ownership" {
            arrOwnershipType = ["Individual".localized,"Partnership".localized,"Private Ltd.".localized,"Public Ltd.".localized,"Government".localized,"NGO".localized,"Other".localized]
        }
        else if statusData == "Address Type"
        {
            arrOwnershipType = ["Business".localized,"Shop House".localized,"Other".localized]
        }
        else if statusData == "Registration Issuing Authority"
        {
            arrOwnershipType = ["CDC","ABC","HDF","JKL","LGA"]
        }
        else if statusData == "License Issuing Authority"
        {
            arrOwnershipType = ["CDC","ABC","HDF","LGA"]
        }
        else if statusData == "License Classes"
        {
            arrOwnershipType = ["A   For All Motorcycles.".localized,"A1   For below engine Power CC 110.".localized,"B   For Private Owned Buses and up to 3 ton Private vehices.".localized,"C   For Machinery.".localized]
        }
        else if statusData == "Blood Groups"
        {
            arrOwnershipType = ["O+".localized,"O-".localized,"A+".localized,"A-".localized,"B+".localized,"B-".localized,"AB+".localized,"AB-".localized]
        }
        else if statusData == "Education Organization"
        {
            arrOwnershipType = ["Government".localized,"Private".localized,"Government + Private".localized,"Others".localized]
        }
        else if statusData == "Type of Education"
        {
            arrOwnershipType = ["School".localized,"College".localized,"University".localized,"Institute".localized,"Others".localized]
        }
        else  if statusData == "Grade / Subject"
        {
            arrOwnershipType = ["Civil Engineering".localized,"Electronics Engineering".localized,"Computer Science Engineering".localized,"Electrical Engineering".localized]
        }
        else  if statusData == "Qualification"
        {
            arrOwnershipType = ["Primary School".localized,"Middle School".localized,"High School".localized,"Diploma".localized,"Bachelor".localized,"Honour".localized,"Master".localized,"Ph.D".localized]
        }
        else  if statusData == "Occupation"
        {
            arrOwnershipType = ["Account".localized,"Advertising".localized,"Government Staff".localized,"Lawyer".localized,"Own Business".localized,"Military Force Service".localized,"Police Force Service".localized,"Criminal Justice".localized,"Airline Pilot".localized,"Teacher".localized,"Actor".localized,"Architecture".localized,"Others".localized]
        }
        tableview.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.updateNavigationTitle()
    }

    func updateNavigationTitle()
    {
        if statusData == "Ownership" {
             self.title = "Select Ownership Type".localized
        }
        else if statusData == "Address Type"
        {
             self.title = "Select Address Type".localized
        }
        else if statusData == "Registration Issuing Authority"
        {
             self.title = "Registration Issuing Authority".localized
        }
        else if statusData == "License Issuing Authority"
        {
             self.title = "License Issuing Authority".localized
        }
        else if statusData == "License Classes"
        {
             self.title = "License Classes".localized
        }
        else if statusData == "Blood Groups"
        {
             self.title = "Blood Groups".localized
        }
        else  if statusData == "Qualification"
        {
             self.title = "Select Qualification".localized
        }
        else  if statusData == "Occupation"
        {
             self.title = "Select Occupation".localized
        }
        else  if statusData == "Education Organization"
        {
             self.title = "Education Organization".localized
        }
        else  if statusData == "Type of Education"
        {
             self.title = "Type of Education".localized
        }
        else  if statusData == "Grade / Subject"
        {
             self.title = "Grade / Subject".localized
        }
        
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: "Zawgyi-One", size: 21)!, NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnOtherCancelAction(sender: UIButton) {
        viewTransparent.isHidden = true
        viewOthers.isHidden = true
    }
    
    @IBAction func btnOtherOKAction(sender: UIButton) {
        if(txtFieldOther.text?.length == 0)
        {
            alertViewObj.wrapAlert(title: nil, body:"Please enter text!".localized, img: #imageLiteral(resourceName: "info"))
            
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                
            }
            alertViewObj.showAlert(controller: self)
            
        }
        else
        {
            self.listDataVCDelegate?.SelectListData(selectedName: txtFieldOther.text!, selectedOption : statusData)
            self.navigationController?.popViewController(animated : true)
        }
    }
}

extension PAListDataVC: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOwnershipType.count
    }   
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:PopOverCell = tableView.dequeueReusableCell(withIdentifier:"PopOverCell") as! PopOverCell
        cell.lblTitle.text = arrOwnershipType[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        println_debug(arrOwnershipType[indexPath.row])
        
        if(arrOwnershipType[indexPath.row] == "Others".localized || arrOwnershipType[indexPath.row] == "Other".localized)
        {
            viewOthers.isHidden = false
            viewTransparent.isHidden = false
        }
        else
        {
            self.listDataVCDelegate?.SelectListData(selectedName: arrOwnershipType[indexPath.row], selectedOption : statusData)
            self.navigationController?.popViewController(animated : true)
        }
    }
    
}
