//
//  PersonalAccountVC.swift
//  OK
//
//  Created by PC on 12/4/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Rabbit_Swift

class PersonalAccountVC: OKBaseController,PASecurityQuestionVCDelegate,UITextFieldDelegate,StateDivisionVCDelegate,AddressTownshiopVCDelegate,PAImagePreviewVCDelegate {
    var navigation : UINavigationController?
    @IBOutlet weak var btnEditPersonal: UIButton!
    @IBOutlet weak var btnUpdatePersonalProfile: UIButton! {
        didSet {
            btnUpdatePersonalProfile.setTitle("Update".localized, for: .normal)
        }
    }
    @IBOutlet weak var tblMainList: UITableView!
    //Manage Image Data
    var imageFile: UIImage!
    var arrImageData = [String]()
    var intStatusContact = 0
    var imageFileURL = ""
    let imagePicker = UIImagePickerController()
    
    var boolEditView = Bool()
    var boolVillageTrack = Bool()//true-display& false-Hide
    
    var locationDetails: LocationDetail?
    var townShipDetail : TownShipDetail?
    var townshipCode = ""
    var statusView = ""
    
    //Data Model
    var personalInfoModel =  PAPersonalAccountModel()
    var myConatctModel =  PAMyContactDetailsModel()
    var securityQuestionsModel =  PASecurityQuestionsModel()
    var addressModel =  BusinessAddressModel()
    var updateProfileModel = UpdateProfileModel()
    
    //Validation for Mobile numbers prefix
    let validObj  = PayToValidations()
    //Manage Street View
    var addressArray = [StreetAddressModel]()
    @IBOutlet weak var tblStreetList: UITableView!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        personalInfoModel.nationality = UserModel.shared.country
        personalInfoModel.nationalityCode = "myanmar"
        
        //Manage Keyboard
        IQKeyboardManager.sharedManager().enable            = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        
        btnUpdatePersonalProfile.isUserInteractionEnabled = false
        btnUpdatePersonalProfile.isHidden = true
        
        tblStreetList.isHidden = true
        boolVillageTrack = false
        boolEditView = false
        arrImageData = ["0","0"]
        if UserModel.shared.idPhoto.length > 0 {
            arrImageData[0] = "1"
        }
        if UserModel.shared.idPhoto1.length > 0 {
            arrImageData[1] = "1"
        }
        //Age Calculation Range
        //let strAge = Int(self.getDOBYear(UserModel.shared.dob)) ?? 0
        self.changesLanguageAccordingToRegistration()
        self.loadViewData()
        self.callGetPersonalAccountAPI()
        self.callGetSecurityQuestionsAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UserModel.shared.accountType = "1"
        if(UserModel.shared.accountType == "1") {//personal user
            self.title = "Personal Details".localized
        }else{
            self.title = "Merchant Details".localized
        }
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: "Zawgyi-One", size: 21)!, NSAttributedString.Key.foregroundColor: UIColor.white]
        
        if statusView == "Personal" {
            self.callAPIToGetMenuStatus()
        }
    }
    
    private func changesLanguageAccordingToRegistration() {
        // Change Language according to resgistered laguage
        if UserModel.shared.language == "my" || UserModel.shared.language == "MY" || UserModel.shared.language == "MYA" || UserModel.shared.language == "mya"{
            ok_default_language  = "my"
        }else if UserModel.shared.language == "en" || UserModel.shared.language == "EN" {
            ok_default_language  = "en"
        }
        
        let setLanguage = UserDefaults.standard.value(forKey: "currentLanguage") as! String
        if ok_default_language != setLanguage {
            UserDefaults.standard.set(true, forKey: "LanguageStatusChanged")
            UserDefaults.standard.set(UserDefaults.standard.value(forKey: "currentLanguage"), forKey: "PreviousLanguage")
            UserDefaults.standard.set(ok_default_language, forKey: "currentLanguage")
            appDel.setSeletedlocaLizationLanguage(language: ok_default_language)
            UserDefaults.standard.synchronize()
        }
    }
    
    //MARK:- API Methods
    func callAPIToGetMenuStatus() {
        if appDelegate.checkNetworkAvail() {
            let paramString : [String:Any] = [
                "AgentId" : UserModel.shared.agentID,
                "Login":self.loginInfoDictionary()
            ]
            let web      = WebApiClass()
            web.delegate = self
            let urlStr   = Url.getUpdateProfileDetails
            let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
            println_debug(ur)
            println_debug(paramString)
            web.genericClass(url: ur, param: paramString as AnyObject, httpMethod: "POST", mScreen: "mGetMenuStatus")
        }
    }
    
    func callUpdateStatusAPI() {
        println_debug("callUpdateStatusAPI")
        if appDelegate.checkNetworkAvail() {
            //Create Array Data
            var arrMenuStatus = Array<Dictionary<String,Any>>()
            
            let dictMerchantProfileStatusList = NSMutableDictionary()
            dictMerchantProfileStatusList["Type"] = "personal_details"
            dictMerchantProfileStatusList["Value"] = personalInfoModel.profileStatus
            arrMenuStatus.append(dictMerchantProfileStatusList as! Dictionary<String, Any>)
            
            let dictBusiness_details = NSMutableDictionary()
            dictBusiness_details["Type"] = "business_details"
            dictBusiness_details["Value"] = updateProfileModel.business_details
            arrMenuStatus.append(dictBusiness_details as! Dictionary<String, Any>)
            
            let dictOther_services = NSMutableDictionary()
            dictOther_services["Type"] = "other_services"
            dictOther_services["Value"] = updateProfileModel.other_services
            arrMenuStatus.append(dictOther_services as! Dictionary<String, Any>)
            
            let dictMerchant_authorization = NSMutableDictionary()
            dictMerchant_authorization["Type"] = "merchant_authorization"
           // dictMerchant_authorization["Value"] = updateProfileModel.merchant_authorization
            arrMenuStatus.append(dictMerchant_authorization as! Dictionary<String, Any>)
            
            let dictOk_agent_services = NSMutableDictionary()
            dictOk_agent_services["Type"] = "ok_agent_services"
            dictOk_agent_services["Value"] = updateProfileModel.ok_agent_services
            arrMenuStatus.append(dictOk_agent_services as! Dictionary<String, Any>)
            
            let dictCash_in = NSMutableDictionary()
            dictCash_in["Type"] = "cash_in"
            dictCash_in["Value"] = updateProfileModel.cash_in
            arrMenuStatus.append(dictCash_in as! Dictionary<String, Any>)
            
            let dictCash_out = NSMutableDictionary()
            dictCash_out["Type"] = "cash_out"
            dictCash_out["Value"] = updateProfileModel.cash_out
            arrMenuStatus.append(dictCash_out as! Dictionary<String, Any>)
            
            let dictBusiness_closed = NSMutableDictionary()
            dictBusiness_closed["Type"] = "business_closed"
            dictBusiness_closed["Value"] = updateProfileModel.business_closed
            arrMenuStatus.append(dictBusiness_closed as! Dictionary<String, Any>)
            
            let paramString : [String:Any] = [
                "AgentId" : UserModel.shared.agentID,
                "Login":self.loginInfoDictionary(),
                "MerchantProfileStatusList":arrMenuStatus
            ]
            
            let web      = WebApiClass()
            web.delegate = self
            let urlStr   = Url.addUpdateProfileDetails
            let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
            println_debug(ur)
            println_debug(paramString)
            web.genericClass(url: ur, param: paramString as AnyObject, httpMethod: "POST", mScreen: "mProfileMenuStatus")
        }
    }
    
    
    //Submit Personal Account Details
    func callPostPersonalAccountAPI() {
        println_debug("callPostPersonalAccountAPI")
        if appDelegate.checkNetworkAvail() {
            showProgressView()
            //PersonalDetails
            let dictPersonalDetails = NSMutableDictionary()
            dictPersonalDetails["AccType"] = personalInfoModel.accountType
            dictPersonalDetails["ContactPhonenumber"] = myConatctModel.phoneNumber
            dictPersonalDetails["DateOfBirth"] = UserModel.shared.dob
            dictPersonalDetails["EmailId"] = myConatctModel.emailID
            dictPersonalDetails["FaceBookId"] = myConatctModel.facebookID
            
            dictPersonalDetails["NrcNumber"] = UserModel.shared.nrc
            
            if(UserModel.shared.idType == "04")
            {
                dictPersonalDetails["NrcImage1"] = personalInfoModel.attachPassport
                dictPersonalDetails["NrcImage2"] = ""
            }
            else
            {
                dictPersonalDetails["NrcImage1"] = personalInfoModel.attachNRCFront
                dictPersonalDetails["NrcImage2"] = personalInfoModel.attachNRCBack
            }
            
            dictPersonalDetails["Occupation"] = personalInfoModel.occupation
            dictPersonalDetails["FatherName"] = UserModel.shared.fatherName
            dictPersonalDetails["OkAccNumber"] = UserModel.shared.mobileNo
            dictPersonalDetails["OkDollarJoiningDate"] = self.getSubmitJoiningDate(UserModel.shared.createdDate)
            dictPersonalDetails["ProfileName"] = UserModel.shared.name
            dictPersonalDetails["Qualification"] = personalInfoModel.qualification
            dictPersonalDetails["RegistrationStatus"] = personalInfoModel.registrationStatus
            dictPersonalDetails["SecurityQuestion"] = securityQuestionsModel.securityQuestionCode
            dictPersonalDetails["SecurityQuestionAnswer"] = securityQuestionsModel.securityAnswer
            
            //Add Address Info
            let dictAddressInfo = NSMutableDictionary()
            dictAddressInfo["AddressType"] = ""
            dictAddressInfo["City"] = addressModel.city
            dictAddressInfo["CountryCode"] = UserModel.shared.countryCode
            dictAddressInfo["CountryName"] = UserModel.shared.country
            dictAddressInfo["Division"] = addressModel.divisionCode
            dictAddressInfo["FloorNumber"] = addressModel.floorNo
            dictAddressInfo["HouseBlockNumber"] = addressModel.houseNo
            dictAddressInfo["HouseNumber"] = addressModel.houseNo
            dictAddressInfo["HousingOrZoneName"] = addressModel.housingZoneName
            dictAddressInfo["Line2"] = ""
            dictAddressInfo["RoomNumber"] = addressModel.roomNo
            dictAddressInfo["Street"] = addressModel.street
            dictAddressInfo["Township"] = addressModel.townshipCode
            dictAddressInfo["Village"] = addressModel.villgTrack
            
            var finalObject =  Dictionary<String,AnyObject>()
            finalObject = [
                "AddressInfo" : dictAddressInfo as AnyObject,
                "DrivingLicenseInfo" : dictAddressInfo as AnyObject,
                "EducationAndOccupationInfo":dictAddressInfo as AnyObject,
                "LoginUserInfo": self.loginInfoDictionary() as AnyObject,
                "PassportInfo" : dictAddressInfo as AnyObject,
                "PersonalDetails":dictPersonalDetails as AnyObject
            ]
            
            let web      = WebApiClass()
            web.delegate = self
            let urlStr   = Url.updatePersonalDetails
            let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
            println_debug(ur)
            println_debug(finalObject)
            web.genericClassReg1(url: ur, param: finalObject as AnyObject, httpMethod: "POST", mScreen: "mPostPersonalAccount")
            
        }
    }
    
    //Get Personal Account details
    func callGetPersonalAccountAPI() {
        println_debug("callGetPersonalAccountAPI")
        if appDelegate.checkNetworkAvail() {
            showProgressView()
            let paramString : [String:Any] = [
                "AppId" : UserModel.shared.appID,
                "Limit" : 0,
                "MobileNumber":UserModel.shared.mobileNo,
                "Msid":msid,
                "Offset":0,
                "Ostype":1,
                "Otp":uuid,
                "SimId":UserModel.shared.simID
            ]
            
            let web      = WebApiClass()
            web.delegate = self
            let urlStr   = Url.getPersonalDetails
            let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
            println_debug(ur)
            println_debug(paramString)
            web.genericClass(url: ur, param: paramString as AnyObject, httpMethod: "POST", mScreen: "mPersonalAccount")
        }
    }
    
    //Get Security QUestion
    func callGetSecurityQuestionsAPI() {
        println_debug("callGetSecurityQuestionsAPI")
        if appDelegate.checkNetworkAvail() {
            showProgressView()
            let paramString : [String:Any] = [:]
            let web      = WebApiClass()
            web.delegate = self
            let urlStr   = Url.getSecurityQuestions
            let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
            println_debug(ur)
            println_debug(paramString)
            web.genericClass(url: ur, param: paramString as AnyObject, httpMethod: "GET", mScreen: "mSecurityQuestions")
        }
    }
    
    func loadViewData() {
        println_debug("load old data")
        //Address Data
        addressModel.addressType = UserModel.shared.addressType
        addressModel.division = UserModel.shared.state
        addressModel.township = UserModel.shared.township
        addressModel.city = UserModel.shared.township
        addressModel.villgTrack = UserModel.shared.villageName
        addressModel.street = UserModel.shared.isNewStreet
        addressModel.houseNo = UserModel.shared.houseBlockNo
        addressModel.floorNo = UserModel.shared.floorNumber
        addressModel.roomNo = UserModel.shared.roomNumber
        addressModel.housingZoneName = UserModel.shared.houseName
        //Contact info
        myConatctModel.phoneNumber = UserModel.shared.phoneNumber
        myConatctModel.emailID = UserModel.shared.email
        myConatctModel.facebookID = UserModel.shared.fbEmailId
        //Security Question
        securityQuestionsModel.securityQuestionCode = UserModel.shared.securityQuestionCode
        securityQuestionsModel.securityAnswer = UserModel.shared.securityAnswer
    }
    
    //MARK:- Text Field Delegate
    func textFieldDidEndEditing(_ textField: UITextField) {
        //Home Address
        if textField.tag == 14 {
            //addressModel.street = textField.text!
        }
        else if textField.tag == 15 {
            addressModel.houseNo = textField.text!
        }else if textField.tag == 16 {
            addressModel.floorNo = textField.text!
        }else if textField.tag == 17 {
            addressModel.roomNo = textField.text!
        }else if textField.tag == 18 {
            addressModel.housingZoneName = textField.text!
        }else if textField.tag == 31 {
            //Section3 row1
            //My Contact details
            if isValidEmail(email : textField.text!){
                myConatctModel.emailID = textField.text!
            }else {
                alertViewObj.wrapAlert(title: nil, body: "Please enter valid Email Address.".localized, img: #imageLiteral(resourceName: "info_one"))
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                }
                alertViewObj.showAlert(controller: self)
            }
        }else if textField.tag == 32 {
            myConatctModel.facebookID = textField.text!
        }else if textField.tag == 21 {
            //Security Question
            securityQuestionsModel.securityAnswer = textField.text!
        }
        tblMainList.reloadSections(IndexSet(integer: 4), with: .automatic);
    }
    
    func isValidEmail(email:String) -> Bool {
        let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let pred = NSPredicate(format:"SELF MATCHES %@", regEx)
        return pred.evaluate(with: email)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField.tag == 30 || textField.tag == 29) {
            textField.text = "09"
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if(textField.tag == 30  || textField.tag == 29) {
            textField.text = "09"
            return false
        }
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if(textField.tag == 30) {
            if((textField.text?.length)! <= 2) {
                textField.text = "09"
            }else {
                myConatctModel.phoneNumber = textField.text!
            }
        }else if textField.tag == 14 {
            getAllAddressDetails(searchTextStr: textField.text!)
        }else if(textField.tag == 29) {
            if((textField.text?.length)! <= 2) {
                textField.text = "09"
            }else {
                myConatctModel.altMobNumber = textField.text!
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField.tag == 30) {
            let strTotalLength = (textField.text! + string)
            guard validObj.getNumberRangeValidation(strTotalLength).isRejected == false else {
                return false
            }
            if validObj.getNumberRangeValidation(strTotalLength).max >= strTotalLength.count {
                return true
            }
            return false
        }
        return true
    }
    
    
    // MARK: - Custom Methods
    
    func getAllAddressDetails(searchTextStr : String) {
        
        var urlString = ""
        if ok_default_language == "my"{
            urlString = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(searchTextStr)&components=country:mm&language=my&key=\(googleAPIKey)"
        } else {
            urlString = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(searchTextStr)&components=country:mm&language=en&key=\(googleAPIKey)"
        }
        let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        let session = URLSession.shared
        if url != nil {
            let task = session.dataTask(with:url!) { (data, response, error) -> Void in
                if let data = data {
                    do {
                        let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                        if let dic = dict as? Dictionary<String,AnyObject> {
                            if let resultArray = dic["predictions"] as? [Dictionary<String,Any>] {
                                self.addressArray.removeAll()
                                //println_debug(resultArray)
                                DispatchQueue.main.async {
                                    for dic in resultArray {
                                        self.addressArray.append(StreetAddressModel.init(dict: dic))
                                    }
                                    
                                    if self.addressArray.count > 0 {
                                        DispatchQueue.main.async {
                                            
                                            self.tblStreetList.isHidden = false
                                            self.tblStreetList.allowsMultipleSelection = false
                                            self.tblStreetList.reloadData()
                                            
                                        }
                                    }
                                }
                            }
                        }
                    } catch let error as NSError {
                        println_debug(error.localizedDescription)
                    }
                }else {
                    println_debug("API NOT CALLING")
                }
            }
            task.resume()
        }
    }
    
    @IBAction func btnUpdatePersonalAction(_ sender: Any) {
        if addressModel.division.count > 0 && addressModel.township.count > 0 && addressModel.city.count > 0 && addressModel.street.count > 0 && securityQuestionsModel.securityAnswer.count > 0 && myConatctModel.emailID.count > 0 && myConatctModel.phoneNumber.count > 0 {
            self.callPostPersonalAccountAPI()
            boolEditView = false
            tblMainList.reloadData()
        }else {
            alertViewObj.wrapAlert(title: nil, body:"Please provide all details.".localized, img: #imageLiteral(resourceName: "passport"))
            alertViewObj.addAction(title: "OK", style: .cancel) {
            }
            alertViewObj.showAlert(controller: self)
        }
    }
    
    @IBAction func btnEditAction(_ sender: Any) {
        btnUpdatePersonalProfile.isUserInteractionEnabled = true
        btnUpdatePersonalProfile.isHidden = false
        btnEditPersonal.isHidden = true
        boolEditView = true
        tblMainList.reloadData()
    }
    
    @IBAction func backAction(_ sender: Any) {
        if(boolEditView) {
            alertViewObj.wrapAlert(title: nil, body: "Clicking back without update will clear all entered details, Are you sure to exit?".localized, img: #imageLiteral(resourceName: "info_one"))
            alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
            }
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                self.popToOtherView()
            }
            alertViewObj.showAlert(controller: self)
        }else {
            self.popToOtherView()
        }
    }
    
    func popToOtherView() {
        if(UserModel.shared.accountType == "1") {
            //personal user
            // Set Laguage after change
            if UserDefaults.standard.bool(forKey: "LanguageStatusChanged"){
                let preLang = UserDefaults.standard.value(forKey: "PreviousLanguage") as? String
                if let language = preLang {
                    UserDefaults.standard.set(language, forKey: "currentLanguage")
                    appDel.setSeletedlocaLizationLanguage(language: language)
                    UserDefaults.standard.set(false, forKey: "LanguageStatusChanged")
                }
                UserDefaults.standard.synchronize()
            }
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    //Handle Navigation Bar
    func navigationbarController(toEnable: Bool) {
        if toEnable {
            self.navigationController?.navigationBar.layer.zPosition = 0
            self.title = "Personal Details".localized
            self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: "Zawgyi-One", size: 21)!, NSAttributedString.Key.foregroundColor: UIColor.white]
        }else {
            self.navigationController?.navigationBar.layer.zPosition = -64
        }
        self.navigationController?.navigationBar.isUserInteractionEnabled = toEnable
    }
    
}

//MARK: Contact List Delegate
extension PersonalAccountVC : ContactPickerDelegate {
    
    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]) {
        for contact in contacts {
            decodeContact(contact: contact, isMulti: true)
        }
    }
    
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) {
        decodeContact(contact: contact, isMulti: false)
    }
    
    func decodeContact(contact: ContactPicker, isMulti: Bool) {
        
        println_debug(contact.firstName)
        println_debug(contact.phoneNumbers[0].phoneNumber)
        
        let phoneNumberCount = contact.phoneNumbers.count
        
        if phoneNumberCount == 1 {
            _ = "\(contact.phoneNumbers[0].phoneNumber)"
            //add it last element
            myConatctModel.phoneNumber = contact.phoneNumbers[0].phoneNumber
            tblMainList.reloadData()
            //            let indexPath = IndexPath(item: 0, section: 3)
            //            self.tblMainList.reloadRows(at: [indexPath], with: .none)
        }else if phoneNumberCount > 1 {
            _ = "\(contact.phoneNumbers[0].phoneNumber) and \(contact.phoneNumbers.count-1) more"
        }else {
            _ = ContactGlobalConstants.Strings.phoneNumberNotAvaialable
        }
    }
}

struct StreetAddressModel {
    var shortName : String?
    var address : String?
    init(dict : Dictionary<String, Any>)  {
        if let addressComponents = dict["structured_formatting"] as? Dictionary<String, Any>
        {
            if addressComponents.count > 0
            {
                self.shortName = addressComponents["main_text"] as? String ?? ""
            } else {
                self.shortName = ""
            }
        }
        self.address = dict["description"] as? String ?? ""
    }
}



