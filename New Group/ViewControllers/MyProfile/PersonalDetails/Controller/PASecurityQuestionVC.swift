//
//  PASecurityQuestionVC.swift
//  OK
//
//  Created by PC on 12/28/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol PASecurityQuestionVCDelegate{
    func SelectedQuestion(questionDic : Dictionary<String,AnyObject>)
}

class PASecurityQuestionVC: OKBaseController,WebServiceResponseDelegate,UITableViewDelegate,UITableViewDataSource {
    
    var securityQuestionRDelegate : PASecurityQuestionVCDelegate?
    
    @IBOutlet weak var tableview: UITableView!
    var Question_arr = Array<Dictionary<String,AnyObject>>()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.delegate = self
        tableview.dataSource = self
        println_debug("check network")
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self
            let urlStr   = Url.getSecurityQuestions
            let url = getUrl(urlStr: urlStr, serverType: .serverApp)
            println_debug(url)
            let param = [String:String]() as AnyObject
            web.genericClass(url: url, param: param, httpMethod: "GET", mScreen: "mLogin")
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
        }else{
            println_debug("network is not avialable")
            alertViewObj.wrapAlert(title: "", body: "Network error and try again", img:#imageLiteral(resourceName: "r_user"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return Question_arr.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:PASecurityQuestionCell = tableView.dequeueReusableCell(withIdentifier:"PASecurityQuestionCell") as! PASecurityQuestionCell
        let dic = Question_arr[indexPath.row]
        cell.setSecurityQuestion(securityQuestion: dic["Question"] as! String)
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableview.deselectRow(at: indexPath, animated: true)
        guard (self.securityQuestionRDelegate?.SelectedQuestion(questionDic: Question_arr[indexPath.row]) != nil) else {
            return
        }
        self.view.removeFromSuperview()
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        
        do {
            if let data = json as? Data {
                let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let dic = dict as? Dictionary<String,AnyObject> {
                    println_debug(dic)
                    println_debug(dic["Msg"] as! String)
                    
                    if dic["Msg"] as? String == "Success" {
                        if let Resopnse = dic["Data"] as? String {
                            let dic = OKBaseController.convertToArrDictionary(text: Resopnse)
                            if let log = dic as? Array<Dictionary<String,AnyObject>> {
                                Question_arr = log
                            }
                           
                            DispatchQueue.main.async {
                                progressViewObj.removeProgressView()
                                self.tableview.reloadData()
                            }
                        }
                    }
                    println_debug(Question_arr)
                }
            }
        } catch {
            alertViewObj.wrapAlert(title: "", body: "Network error and try again", img:#imageLiteral(resourceName: "r_user"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
}
