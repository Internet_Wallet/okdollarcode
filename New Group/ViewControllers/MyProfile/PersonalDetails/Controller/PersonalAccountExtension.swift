//
//  PersonalAccountExtension.swift
//  OK
//
//  Created by SHUBH on 10/9/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Rabbit_Swift

class PersonalAccountExtension: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

}

extension PersonalAccountVC : WebServiceResponseDelegate {
    //MARK:- API Response
    func webResponse(withJson json: AnyObject, screen: String) {
        self.removeProgressView()
        if screen == "mGetMenuStatus" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug(dic)
                        if dic["Code"] as? NSNumber == 200 {
                            if let tempString = dic["Data"] as? String {
                                var dictB = OKBaseController.convertToDictionary(text: tempString)
                                //Inner modules
                                if let dictLicenseDoc = dictB!["LicenseDocument"] as? Dictionary<String,Any>{
                                    let dictPersonalDoc = dictLicenseDoc["PersonalDocInfo"] as? Dictionary<String,Dictionary<String,Any>>
                                    UserDefaults.standard.set(dictPersonalDoc, forKey: "PersonalDocInfo")
                                }
                            }
                        }else {
                            println_debug("Error found")
                            DispatchQueue.main.async {
                                alertViewObj.wrapAlert(title: nil, body:dic["Msg"] as? String ?? "", img: #imageLiteral(resourceName: "info_one"))
                                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                }
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                    }
                }
            }catch {}
        }else if screen == "mPersonalAccount" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug(dic)
                        if let code = dic["Code"] as? NSNumber, code == 200 {
                            println_debug("Success")
                            guard let dictData = dic["Data"] else { return }
                            println_debug(dictData as Any)
                            guard let dictAll = OKBaseController.convertToDictionary(text: dictData as! String) else { return }
                            //PersonalDetails
                            if let dictPersonalDetails = dictAll["PersonalDetails"]  as? Dictionary<String,AnyObject>  {
                                println_debug(dictPersonalDetails as Any)
                                personalInfoModel.registrationStatus = dictPersonalDetails["RegistrationStatus"] as? Int ?? 1
                                personalInfoModel.nrcNo = dictPersonalDetails["NrcNumber"] as? String ?? ""
                                
                                var urlAttachNRCFront = dictPersonalDetails["NrcImage1"] as? String ?? ""
                                urlAttachNRCFront = urlAttachNRCFront.replacingOccurrences(of: "%2520", with: " ")
                                urlAttachNRCFront = urlAttachNRCFront.replacingOccurrences(of: "%20", with: " ")
                                urlAttachNRCFront = urlAttachNRCFront.replacingOccurrences(of: "%25", with: " ")
                                personalInfoModel.attachNRCFront = urlAttachNRCFront
                                UserModel.shared.idPhoto = urlAttachNRCFront
                                
                                var urlAttachNRCBack = dictPersonalDetails["NrcImage2"] as? String ?? ""
                                urlAttachNRCBack = urlAttachNRCBack.replacingOccurrences(of: "%2520", with: " ")
                                urlAttachNRCBack = urlAttachNRCBack.replacingOccurrences(of: "%20", with: " ")
                                urlAttachNRCBack = urlAttachNRCBack.replacingOccurrences(of: "%25", with: " ")
                                personalInfoModel.attachNRCBack = urlAttachNRCBack//UserModel.shared.idPhoto1//
                                UserModel.shared.idPhoto1 = urlAttachNRCBack
                                
                                var urlAttachPassport = dictPersonalDetails["NrcImage1"] as? String ?? ""
                                urlAttachPassport = urlAttachPassport.replacingOccurrences(of: "%2520", with: " ")
                                urlAttachPassport = urlAttachPassport.replacingOccurrences(of: "%20", with: " ")
                                urlAttachPassport = urlAttachPassport.replacingOccurrences(of: "%25", with: " ")
                                personalInfoModel.attachPassport = urlAttachPassport//UserModel.shared.idPhoto//
                                
                                personalInfoModel.qualification = Rabbit.uni2zg(dictPersonalDetails["Qualification"] as? String ?? "")
                                personalInfoModel.occupation = Rabbit.uni2zg(dictPersonalDetails["Occupation"] as? String ?? "")
                                personalInfoModel.accountType = dictPersonalDetails["AccType"] as? String ?? ""
                                
                                securityQuestionsModel.securityQuestionCode = dictPersonalDetails["SecurityQuestion"] as? String ?? ""
                                securityQuestionsModel.securityAnswer = dictPersonalDetails["SecurityQuestionAnswer"] as? String ?? ""
                                
                                myConatctModel.phoneNumber  = dictPersonalDetails["ContactPhonenumber"] as? String ?? ""
                                myConatctModel.emailID  = dictPersonalDetails["EmailId"] as? String ?? ""
                                myConatctModel.facebookID  = dictPersonalDetails["FaceBookId"] as? String ?? ""
                            }
                            
                            //AddressInfo
                            if let dictAddressInfo = dictAll["AddressInfo"]  as? Dictionary<String,AnyObject>  {
                                println_debug(dictAddressInfo as Any)
                                //Dec05
                                let states = TownshipManager.allLocationsList
                                for state in states{
                                    if state.stateOrDivitionCode == dictAddressInfo["Division"] as? String ?? "" {
                                        if ok_default_language == "my"{
                                            addressModel.division = state.stateOrDivitionNameMy
                                        }else {
                                            addressModel.division = state.stateOrDivitionNameEn
                                        }
                                        locationDetails = state
                                        let township = state.townshipArryForAddress
                                        for town in township {
                                            if town.townShipCode == dictAddressInfo["Township"] as? String ?? "" {
                                                if town.GroupName.contains(find: "YCDC") || town.GroupName.contains(find: "MCDC"){
                                                    boolVillageTrack = false
                                                }
                                                if town.cityNameEN == "Amarapura" || town.cityNameEN == "Patheingyi" || town.townShipNameEN == "PatheinGyi" {
                                                    boolVillageTrack = true
                                                }
                                                
                                                if ok_default_language == "my" {
                                                    addressModel.township = town.townShipNameMY
                                                    if town.GroupName == "" {
                                                        addressModel.city = town.cityNameMY
                                                    }else {
                                                        if town.isDefaultCity == "1" {
                                                            addressModel.city = town.DefaultCityNameMY
                                                        }else{
                                                            addressModel.city = town.cityNameMY
                                                        }
                                                    }
                                                }else {
                                                    addressModel.township = town.townShipNameEN
                                                    
                                                    if town.GroupName == "" {
                                                        addressModel.city = town.cityNameEN
                                                    }else {
                                                        if town.isDefaultCity == "1" {
                                                            addressModel.city = town.DefaultCityNameEN
                                                        }else {
                                                            addressModel.city = town.cityNameEN
                                                        }
                                                    }
                                                }
                                                break
                                            }
                                        }
                                    }
                                }
                                ///////Code
                                addressModel.villgTrack = dictAddressInfo["Village"] as? String ?? ""
                                addressModel.street = dictAddressInfo["Street"] as? String ?? ""
                                addressModel.houseNo = dictAddressInfo["HouseNumber"] as? String ?? ""
                                addressModel.floorNo = dictAddressInfo["FloorNumber"] as? String ?? ""
                                addressModel.roomNo = dictAddressInfo["RoomNumber"] as? String ?? ""
                                addressModel.housingZoneName = dictAddressInfo["HousingOrZoneName"] as? String ?? ""
                            }
                            DispatchQueue.main.async {
                                self.tblMainList.reloadData()
                            }
                        }else {
                            println_debug("Error found")
                        }
                    }
                }
            }catch {}
        }else if screen == "mPostPersonalAccount" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug(dic)
                        
                        if(dic["Code"] as? NSInteger == 200) {
                            DispatchQueue.main.async {
                                alertViewObj.wrapAlert(title: nil, body: "Personal Details Updated Successfully.".localized, img: #imageLiteral(resourceName: "user"))
                                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                    self.btnEditPersonal.isHidden = false
                                    self.btnUpdatePersonalProfile.isHidden = true
                                    self.callUpdateStatusAPI()
                                }
                                alertViewObj.showAlert(controller: self)
                            }
                        }else {
                            DispatchQueue.main.async {
                                alertViewObj.wrapAlert(title: nil, body: dic["Msg"] as! String, img: #imageLiteral(resourceName: "user"))
                                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                    self.btnEditPersonal.isHidden = false
                                    self.btnUpdatePersonalProfile.isHidden = true
                                }
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                    }
                }
            }catch {}
        }else if screen == "mSecurityQuestions" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        var arrData = Array<Dictionary<String,AnyObject>>()
                        
                        if dic["Msg"] as? String == "Success" {
                            if let Resopnse = dic["Data"] as? String {
                                let dic = OKBaseController.convertToArrDictionary(text: Resopnse)
                                if let log = dic as? Array<Dictionary<String,AnyObject>> {
                                    arrData = log
                                }
                            }
                        }
                        
                        var k = 0
                        while k < (arrData.count) {
                            let modelObj = arrData[k]
                            let qCode = modelObj["QuestionCode"]
                            let qQuestion = modelObj["Question"]
                            if ((qCode?.localizedCaseInsensitiveContains(securityQuestionsModel.securityQuestionCode)))! {
                                securityQuestionsModel.securityQuestion = qQuestion as! String
                                DispatchQueue.main.async {
                                    let indexPath = IndexPath(item: 0, section: 2)
                                    self.tblMainList.reloadRows(at: [indexPath], with: .none)
                                }
                                break
                            }
                            k = k + 1
                        }
                    }
                }
            }catch {}
        }else if screen == "mProfileMenuStatus" {
            do
            {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        if dic["Msg"] as? String == "Sucess" {
                            println_debug("updated menu")
                            DispatchQueue.main.async {
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                    }
                }
            }catch { }
        }
    }
}
