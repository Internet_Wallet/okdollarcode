//
//  BusinessDetailsVCModel.swift
//  OK
//
//  Created by Subhash Arya on 14/03/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class BusinessDetailsVCModel: NSObject {
    var BusinessName : String = ""
    var BusinessRegistration1 : String = ""
    var BusinessRegistration2 : String = ""
    var BusinessCategory : String = ""
    var BusinessSubCategory : String = ""
    var BusinessOutlet : String = ""
    var OkDollarJoiningDate : String = ""
    var UserMembership : String = ""
    var UserMembershipStatus : Bool = false
    var Email : String = ""
    var Website : String = ""
    var FacebookPage : String = ""
    var Instagram : String = ""
    var OwnershipType : String = ""
    var BusinessApproveStatus: Int = -1
    var LogoApproveStatus: Int = -1
    
    var AddressType : String = ""
     var Division : String = ""
    var Township : String = ""
    var City : String = ""
    var Village : String = ""
    var Street : String = ""
    var HouseNumber : String = ""
    var FloorNumber : String = ""
    var RoomNumber : String = ""
    var HouseBlockNumber : String = ""
    
    var OwrshipTypes: [Dictionary<String,Any>]?
    var WorkingDayAndTime: [Dictionary<String,Any>]?
    var PublicHoliday: [Dictionary<String,Any>]?
    var ContactNumber: [Dictionary<String,Any>]?
    var YouTubeLinks: [Dictionary<String,Any>]?
    var LogoImages: Dictionary<String,Any>?
    var KYC: [Dictionary<String,Any>]?
    
    func wrapDataModel() -> Dictionary<String, Any> {
        var dic = Dictionary<String, Any>()
        let loginInfo = ["AppId": UserModel.shared.appID,"Limit": 0,"MobileNumber": UserModel.shared.mobileNo,"Msid": msid,"Offset": 0,"Ostype": 1,"Otp": "","Simid": uuid] as [String : Any]
        let businessInfo =  ["BusinessName": BusinessName,"BusinessRegistration1": BusinessRegistration1,"BusinessRegistration2":BusinessRegistration2,"BusinessCategory": BusinessCategory,"BusinessSubCategory": BusinessSubCategory,"BusinessOutlet": BusinessOutlet,"Email": Email,"Website": Website,"FacebookPage": FacebookPage,"Instagram": Instagram,"OwnershipType": OwnershipType,"BusinessApproveStatus": BusinessApproveStatus,"LogoApproveStatus": LogoApproveStatus,"OkDollarJoiningDate": OkDollarJoiningDate,"UserMembership": UserMembership, "UserMembershipStatus": UserMembershipStatus] as [String : Any]
        let businessAddressInfo = ["AddressType": AddressType,"Division": Division,"Township": Township,"City": City, "Village": Village,"Street": Street,"HouseNumber": HouseNumber,"FloorNumber": FloorNumber,"RoomNumber": RoomNumber,"HouseBlockNumber": HouseBlockNumber] as [String : Any]
        let logo = ["Image1": LogoImages?["Image1"] as? String ?? "","Image2": LogoImages?["Image2"] as? String ?? "","Image3": LogoImages?["Image3"] as? String ?? "","Image4": LogoImages?["Image4"] as? String ?? ""] as [String : Any]
         dic = [
            "LoginInfo": loginInfo,
             "BusinessInfo": businessInfo,
             "BusinessAddressInfo": businessAddressInfo,
             "LogoImages": logo,
             "WorkingDayAndTime": FinalworkDaytime(),
             "PublicHoliday": FinalHoliday(),
             "ContactNumber": ContactNumber as Any,
             "YouTubeLinks": YouTubeLinks as Any] as [String : Any]
        println_debug("Business Update params : \(dic)")
        return dic
    }
    /*{
     "Code": "H012",
     "IsActive": true
     }
     */
    
    func FinalworkDaytime() -> [Dictionary<String,Any>] {
        var finalWorkDay = [Dictionary<String,Any>]()
        if let workDay = WorkingDayAndTime {
            for day in workDay {
                let code = dayFromDayCode(code: day["DayCode"] as! String)
                let codeB = dayFromDayCodeB(code: day["DayCode"] as! String)
                let dic = ["DayCode": day["DayCode"],"OpenTime": day["OpenTime"],"CloseTime": day["CloseTime"],"BName": codeB,"Name": code]
                finalWorkDay.append(dic as [String : Any])
            }
        }
        return finalWorkDay
    }
    
    func dayFromDayCode(code: String) -> String {
        let name = String(code.prefix(3))
        switch name {
        case "SUN":
            return "Sunday"
        case "MON":
            return "Monday"
        case "TUE":
            return "Tuesday"
        case "WED":
            return "Wednesday"
        case "THU":
            return "Thursday"
        case "FRI":
            return "Friday"
        case "SAT":
            return "Saturday"
        case "SUB":
            return "Sabbarth day"
        case "ALL":
            return "All Days"
        default:
            break
        }
        return ""
    }
    func dayFromDayCodeB(code: String) -> String {
        let name = String(code.prefix(3))
        switch name {
        case "SUN":
            return "တနဂၤေႏြေန႔"
        case "MON":
            return "တနလၤာေန႔"
        case "TUE":
            return "အဂၤါေန႔"
        case "WED":
            return "ဗုဒၶဟူးေန႔"
        case "THU":
            return "ၾကာသပေတးေန႔"
        case "FRI":
            return "ေသာၾကာေန႔"
        case "SAT":
            return "စေနေန႔"
        case "SUB":
            return "ဥပုသ္ေန႔"
        case "ALL":
            return "ပိတ္ရက္မရွိ ဆိုင္ဖြင့္ပါသည္"
        default:
            break
        }
        return ""
    }
    
    
    func FinalHoliday() -> [Dictionary<String,Any>] {
        var finalHoliday = [Dictionary<String,Any>]()
        if let holiday = PublicHoliday {
            for day in holiday {
                println_debug(day)
                if day["Status"] as? Bool == true {
                    let dic = ["HolidayCode": day["HolidayCode"],"Status": day["Status"],]
                    finalHoliday.append(dic as [String : Any])
                }
            }
        }
        return finalHoliday
    }

}
