//
//  BusinessPhoneType.swift
//  OK
//
//  Created by Imac on 3/18/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class BusinessPhoneType: UIViewController {
    @IBOutlet weak var lblHeaderTitle: UILabel!{
        didSet{
            self.lblHeaderTitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imgMobile: UIImageView!
    @IBOutlet weak var imgLandline: UIImageView!
    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var imgFax: UIImageView!
    @IBOutlet weak var lblMobile: UILabel!{
        didSet{
            self.lblMobile.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblLandLine: UILabel!{
        didSet{
            self.lblLandLine.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblHome: UILabel!{
        didSet{
            self.lblHome.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblFax: UILabel!{
        didSet{
            self.lblFax.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnCancel: UIButton!{
        didSet{
            self.btnCancel.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnDone: UIButton!
    {
        didSet{
            self.btnDone.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    var seleted = ""
    var input: String?
    var perviouSeleted = ""
    var delegate: BusinessPhoneTypeDelegate?
    override func viewDidLoad() {
        bgView.layer.cornerRadius  = 10
        bgView.layer.masksToBounds = true
        btnCancel.setTitle("Cancel".localized, for: .normal)
        btnDone.setTitle("Done".localized, for: .normal)
        lblHeaderTitle.text = "Contact Type".localized
        lblMobile.text = "MOBILE".localized
        lblLandLine.text = "LANDLINE".localized
        lblHome.text = "HOME".localized
        lblFax.text = "Fax"
        super.viewDidLoad()
    }
    @IBAction func onClickTypeAction(_ sender: UIButton) {
        if sender.tag == 100 {
            imgMobile.image = UIImage(named: "r_act_radio_btn")
            imgLandline.image = UIImage(named: "radio_btn")
            imgHome.image = UIImage(named: "radio_btn")
            imgFax.image = UIImage(named: "radio_btn")
            seleted = "MOBILE"
        }else if sender.tag == 200 {
            imgMobile.image = UIImage(named: "radio_btn")
            imgLandline.image = UIImage(named: "r_act_radio_btn")
            imgHome.image = UIImage(named: "radio_btn")
            imgFax.image = UIImage(named: "radio_btn")
            seleted = "LANDLINE"
        }else if sender.tag == 300 {
            imgMobile.image = UIImage(named: "radio_btn")
            imgLandline.image = UIImage(named: "radio_btn")
            imgHome.image = UIImage(named: "r_act_radio_btn")
            imgFax.image = UIImage(named: "radio_btn")
            seleted = "HOME"
        }else if sender.tag == 400 {
            imgMobile.image = UIImage(named: "radio_btn")
            imgLandline.image = UIImage(named: "radio_btn")
            imgHome.image = UIImage(named: "radio_btn")
            imgFax.image = UIImage(named: "r_act_radio_btn")
            seleted = "FAX"
        }
    }
    
    func setPerviousValue(value: String) {
        if value == "MOBILE" {
            imgMobile.image = UIImage(named: "r_act_radio_btn")
            imgLandline.image = UIImage(named: "radio_btn")
            imgHome.image = UIImage(named: "radio_btn")
            imgFax.image = UIImage(named: "radio_btn")
        }else if value == "LANDLINE" {
            imgMobile.image = UIImage(named: "radio_btn")
            imgLandline.image = UIImage(named: "r_act_radio_btn")
            imgHome.image = UIImage(named: "radio_btn")
            imgFax.image = UIImage(named: "radio_btn")
        }else if value == "HOME" {
            imgMobile.image = UIImage(named: "radio_btn")
            imgLandline.image = UIImage(named: "radio_btn")
            imgHome.image = UIImage(named: "r_act_radio_btn")
            imgFax.image = UIImage(named: "radio_btn")
        }else if value == "FAX" {
            imgMobile.image = UIImage(named: "radio_btn")
            imgLandline.image = UIImage(named: "radio_btn")
            imgHome.image = UIImage(named: "radio_btn")
            imgFax.image = UIImage(named: "r_act_radio_btn")
        }
        perviouSeleted = value
    }
    
    @IBAction func onCickDoneAction(_ sender: UIButton) {
        if sender.tag == 100 {
            if let del = delegate {
                del.seletedPhoneType(vc: self, selected: "", output: input ?? "")
            }
        }else{
            if let del = delegate {
                if perviouSeleted == seleted {
                    del.seletedPhoneType(vc: self, selected: "", output: input ?? "")
                }else {
                    del.seletedPhoneType(vc: self, selected: seleted, output: input ?? "")
                }
            }
        }
    }

}

protocol BusinessPhoneTypeDelegate {
    func seletedPhoneType(vc: UIViewController, selected: String, output: String)
}
