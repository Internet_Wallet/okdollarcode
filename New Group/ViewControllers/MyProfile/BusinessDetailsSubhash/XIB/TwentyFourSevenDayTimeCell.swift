//
//  TwentyFourSevenDayTimeCell.swift
//  OK
//
//  Created by SHUBH on 12/28/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit

class TwentyFourSevenDayTimeCell: UITableViewCell {

    @IBOutlet weak var lblAllTimeOpen: UILabel!
    @IBOutlet weak var btnSelection: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        lblAllTimeOpen.text = "Open 24/7".localized
        // Initialization code
    }

}
