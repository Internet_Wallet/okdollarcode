//
//  BusinessHeaderCell.swift
//  OK
//
//  Created by Subhash Arya on 14/03/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class BusinessHeaderCell: UITableViewCell {
    @IBOutlet weak var imgBusiness: UIImageView!
    @IBOutlet weak var lblBusinessName: MarqueeLabel!
    @IBOutlet weak var lblOutletName: MarqueeLabel!
    @IBOutlet weak var lblMembership: UILabel!
    @IBOutlet weak var lblMemberSince: UILabel!
    @IBOutlet weak var imgVerified: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
            imgBusiness.layer.cornerRadius = imgBusiness.frame.width / 2
            imgBusiness?.layer.masksToBounds = true
            imgBusiness.layer.borderColor = UIColor.lightGray.cgColor
            imgBusiness.layer.borderWidth = 1.0
        
        lblBusinessName.font = UIFont(name: appFont, size: 16)
        lblOutletName.font = UIFont(name: appFont, size: 11)
    }

}
