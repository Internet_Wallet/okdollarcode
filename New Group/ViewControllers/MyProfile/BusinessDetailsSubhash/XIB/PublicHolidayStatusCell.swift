//
//  PublicHolidayStatusCell.swift
//  OK
//
//  Created by SHUBH on 12/29/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit

class PublicHolidayStatusCell: UITableViewCell {

  @IBOutlet weak var btnCheckBoxOpen: UIButton!
  @IBOutlet weak var btnCheckBoxClose: UIButton!
  @IBOutlet weak var lblTitle: UILabel!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
