//
//  SelectDayTimeCell.swift
//  OK
//
//  Created by SHUBH on 12/28/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit

class SelectDayTimeCell: UITableViewCell {

  @IBOutlet weak var btnSelection: UIButton!
  @IBOutlet weak var lblWeekDays: UILabel!
  @IBOutlet weak var lblTimeOpenClose: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
