//
//  BusinessHolidayUPCell.swift
//  OK
//
//  Created by Imac on 3/14/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class BusinessHolidayUPCell: UITableViewCell {

    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: MarqueeLabel!
    @IBOutlet weak var imgRightArrow: UIImageView!
    @IBOutlet weak var lblSaperator: UILabel!
    @IBOutlet weak var lblRequired: UILabel!
    @IBOutlet weak var lblNotSet: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
