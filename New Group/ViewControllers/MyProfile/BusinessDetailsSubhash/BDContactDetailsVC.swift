//
//  BDContactDetailsVC.swift
//  OK
//
//  Created by SHUBH on 12/27/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit



protocol BDContactDetailsVCDelegate {
    func updateContactDetials(contactNumber: [Dictionary<String,Any>], youtubeLinks: [Dictionary<String,Any>], otherLink: [String])
}

class BDContactDetailsVC: OKBaseController {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnUpdate: UIButton!{
        didSet{
            self.btnUpdate.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnBack: UIButton!{
        didSet{
            self.btnBack.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    var contactNumberList: [Dictionary<String,Any>]?
    var youTubeLinksList: [Dictionary<String,Any>]?
    var otherList: [String]?
    var updateStatus: Bool?
    let validObj  = PayToValidations()
    var delegate: BDContactDetailsVCDelegate?
    let MOBILENUMBER = "0123456789"
    let TEXTCHAT = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789/-_@,:()?."
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        btnUpdate.setTitle("Set".localized, for: .normal)
        btnBack.setTitle("Cancel".localized, for: .normal)
        if contactNumberList?.count == 0 {
            let dic = ["PhoneType": "MOBILE","PhoneNumber":""]
            contactNumberList?.append(dic)
        }
        if youTubeLinksList?.count == 0 {
            let dic = ["Link": "https://www.youtube.com/"]
            youTubeLinksList?.append(dic)
        }
        
        if let emailStr = otherList?[0] {
            otherList?[0] = emailStr.lowercased()
        }
        setMarqueLabelInNavigation()
        tblView.register(UINib(nibName: "NameInputUPCell", bundle: nil), forCellReuseIdentifier: "NameInputUPCell")
    }
    
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: appFontSize)
        lblMarque.text = "Contact Details".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white //init(hex: "#1C2C99")
        self.navigationItem.titleView = lblMarque
    }
    
    @IBAction func onClickBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickUpdate(_ sender: UIButton) {
        
        var tempContact = contactNumberList
        //contactNumberList?.removeAll()
        if let temp = tempContact {
            for number in temp {
                let no = number["PhoneNumber"] as? String ?? ""
                if no != "" {
                   // contactNumberList?.append(number)
                }
            }
        }
        tempContact?.removeAll()
        // Email Validate
        let email = otherList?[0] ?? ""
        var checkValidation = ""
        
        if email.count == 0 {
            // checkValidation = "Email".localized + "\n"
        }else if !email.isEmail() {
            checkValidation = checkValidation + "Invalid Email Format".localized + "\n"
        }
        // Website Validate
        let website = otherList?[1] ?? ""
        
        if website == "" {
            
        }else if !website.validateWebiteUrl() {
            checkValidation = checkValidation + "Invalid Website Format".localized + "\n"
        }
        // Youtube Validate
        tempContact = youTubeLinksList
        youTubeLinksList?.removeAll()
        if let temp = tempContact {
            for link in temp {
                let no = link["Link"] as? String ?? ""
                if no == "" {
                    
                }else if !(no.contains(find: "https://www.youtube.com/")) || no == "https://www.youtube.com/" {
                    checkValidation = checkValidation + "Invalid Youtube Link Format".localized + "\n"
                    youTubeLinksList = tempContact
                }else {
                    youTubeLinksList?.append(link)
                }
            }
        }
        tempContact?.removeAll()
        
        if checkValidation != "" {
            showAlert(alertTitle: "Please Fill All Mandatory Fields".localized, alertBody: checkValidation, alertImage: UIImage(named: "father")!)
            return
        }
        //"Please Fill All Mandatory Fields"
        
        alertViewObj.wrapAlert(title:"", body: "Contact details saved successfully".localized, img:UIImage(named: "b_contact"))
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            if let del = self.delegate {
                del.updateContactDetials(contactNumber: self.contactNumberList! , youtubeLinks: self.youTubeLinksList!, otherLink: self.otherList!)
            }
            self.navigationController?.popViewController(animated: true)
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func reloadRows(index: Int, withSeciton sec: Int) {
        UIView.setAnimationsEnabled(false)
        self.tblView.beginUpdates()
        let indexPosition = IndexPath(row: index, section: sec)
        self.tblView.reloadRows(at: [indexPosition], with: .none)
        self.tblView.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
    func reloadSection(index: Int) {
        UIView.setAnimationsEnabled(false)
        let indexSet: IndexSet = [index]
        self.tblView.beginUpdates()
        self.tblView.reloadSections(indexSet, with: .automatic)
        self.tblView.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func isValidUrl(url: String) -> Bool {
        let urlRegEx = "^(https?://)?(www\\.)?([-a-z0-9]{1,63}\\.)*?[a-z0-9][-a-z0-9]{0,61}[a-z0-9]\\.[a-z]{2,6}(/[-\\w@\\+\\.~#\\?&/=%]*)?$"
        let urlTest = NSPredicate(format:"SELF MATCHES %@", urlRegEx)
        let result = urlTest.evaluate(with: url)
        return result
    }
    
}

extension BDContactDetailsVC: UITextFieldDelegate {
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag >= 1000 && textField.tag < 2000 {
            textField.keyboardType = .numberPad
        }else if textField.tag == 100 || textField.tag == 200 || textField.tag == 300 || textField.tag == 400 {
            textField.autocapitalizationType = .none
            textField.keyboardType = .emailAddress
        }else if (textField.tag > 4000 && textField.tag < 5000) {
            textField.autocapitalizationType = .none
            textField.keyboardType = .default
        }
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 100 {
            otherList?[0] = textField.text ?? ""
        }else if textField.tag == 200 {
            otherList?[1] = textField.text ?? ""
        }else if textField.tag == 300 {
            otherList?[2] = textField.text ?? ""
        }else if textField.tag == 400 {
            otherList?[3] = textField.text ?? ""
        }else if textField.tag >= 4000 && textField.tag < 5000 {
            var dic = youTubeLinksList?[textField.tag - 4000]
            dic?["Link"] = textField.text ?? ""
            youTubeLinksList?[textField.tag - 4000] = dic!
        }else if textField.tag >= 1000 && textField.tag < 1100 {
            if contactNumberList?.count ?? 0 > 0 {
                var dic = contactNumberList?[textField.tag - 1000]
                dic?["PhoneNumber"] = textField.text ?? ""
                contactNumberList?[textField.tag - 1000] = dic!
            }
        }
        
    }
    
    @objc func mobileNumberEdited(_ textField: UITextField) {
        
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if range.location == 0 && string == " " {
            return false
        }
        if (textField.tag >= 1000 && textField.tag < 1100){
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: MOBILENUMBER).inverted).joined(separator: "")) { return false }
            if text.count > 13 {
                return false
            }
        }else if (textField.tag >= 4000) || textField.tag == 100 || textField.tag == 200 || textField.tag == 300 || textField.tag == 400 {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: TEXTCHAT).inverted).joined(separator: "")) { return false }
        }
        
        return true
    }
    
}

extension BDContactDetailsVC: UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if contactNumberList?.count == 0 {
                return 1
            }else {
                return contactNumberList?.count ?? 0
            }
        }else if section == 1 || section == 2 {
            return 2
        }else if section == 3 {
            if youTubeLinksList?.count == 0 {
                return 1
            }else {
                return youTubeLinksList?.count ?? 0
            }
        }else if section == 4 {
            return 1
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessPhoneNumbersCell") as! BusinessPhoneNumbersCell
            if contactNumberList?.count == 0 {
                return cell
            }
            let dic = contactNumberList?[indexPath.row]
            if let number = dic?["PhoneNumber"] as? String {
                cell.tfNumber.text = number
            }
            
            if let phoneType = dic?["PhoneType"] as? String {
                cell.btnPhoneType.setTitle(phoneType.localized, for: .normal)
                if phoneType == "MOBILE" {
                    cell.imgIcon.image = UIImage(named: "enter_mobile")
                    cell.tfNumber.placeholder = "Enter Mobile Number".localized
                    cell.lblStatus.text = "Mobile Number".localized
                }else if phoneType == "LANDLINE" {
                    cell.tfNumber.placeholder = "Enter Landline Number".localized
                    cell.imgIcon.image = UIImage(named: "phone_number")
                    cell.lblStatus.text = "Landline Number".localized
                }else if phoneType == "HOME" {
                    cell.tfNumber.placeholder = "Enter Home Number".localized
                    cell.imgIcon.image = UIImage(named: "housing")
                    cell.lblStatus.text = "Home Number".localized
                }else if phoneType == "FAX" {
                    cell.tfNumber.placeholder = "Enter Fax Number".localized
                    cell.imgIcon.image = UIImage(named: "print_gray")
                    cell.lblStatus.text = "Fax Number".localized
                    
                }
            }
            cell.tfNumber.tag =  1000 + indexPath.row
            cell.tfNumber.addTarget(self, action: #selector(mobileNumberEdited(_:)), for: .editingChanged)
            cell.tfNumber.keyboardType = .numberPad
            cell.tfNumber.delegate = self
            cell.btnPhoneType.addTarget(self, action: #selector(onClickPhoneType(_:)), for: .touchUpInside)
            cell.btnPhoneType.stringTag = String(indexPath.row)
            cell.btnAdd.stringTag = String(indexPath.row)
            cell.btnAdd.addTarget(self, action: #selector(onClickAddAction(_:)), for: .touchUpInside)
            let count = contactNumberList?.count ?? 0
            if indexPath.row == 0 {
                if count == 5 {
                    cell.btnAdd.setImage(UIImage(named: "delet"), for: .normal)
                }else {
                    cell.btnAdd.setImage(UIImage(named: "add"), for: .normal)
                }
            }else {
                cell.btnAdd.setImage(UIImage(named: "delet"), for: .normal)
            }
            if updateStatus ?? false {
                cell.isUserInteractionEnabled = true
            }else {
                cell.isUserInteractionEnabled = false
            }
            return cell
        }else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NameInputUPCell") as! NameInputUPCell
            
            cell.tfTitle.delegate = self
            //cell.lblStatus.isHidden = true
            cell.btnRightArrow.isHidden = true
            
            cell.tfTitle.text = otherList?[indexPath.row]
            if indexPath.row == 0 {
                cell.imgIcon.image = UIImage(named: "email")
                cell.tfTitle.tag = 100
                cell.lblStatus.text = "Email".localized
                cell.tfTitle.placeholder = "Enter Email".localized
                cell.lblRequired.isHidden = true
            }else {
                cell.imgIcon.image = UIImage(named: "b_division")
                cell.tfTitle.tag = 200
                cell.lblStatus.text = "Website".localized
                cell.tfTitle.placeholder = "Enter Website".localized
                cell.lblRequired.isHidden = true
            }
            cell.tfTitle.keyboardType = .emailAddress
            if updateStatus ?? false {
                cell.isUserInteractionEnabled = true
            }else {
                cell.isUserInteractionEnabled = false
            }
            return cell
        }else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NameInputUPCell") as! NameInputUPCell
            cell.tfTitle.delegate = self
            cell.btnRightArrow.isHidden = true
            //cell.lblStatus.isHidden = true
            if indexPath.row == 0 {
                cell.tfTitle.text = otherList?[2]
                cell.lblStatus.text = "Facebook".localized
                cell.imgIcon.image = UIImage(named: "b_facebook")
                cell.tfTitle.tag = 300
                cell.tfTitle.placeholder = "Enter Facebook Link".localized
            }else {
                cell.tfTitle.text = otherList?[3]
                cell.lblStatus.text = "Instagram".localized
                cell.imgIcon.image = UIImage(named: "insta")
                cell.tfTitle.tag = 400
                cell.tfTitle.placeholder = "Enter Instagram Link".localized
            }
            cell.tfTitle.keyboardType = .emailAddress
            if updateStatus ?? false {
                cell.isUserInteractionEnabled = true
            }else {
                cell.isUserInteractionEnabled = false
            }
            return cell
        }else if indexPath.section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessYoutubeCell") as! BusinessYoutubeCell
            if youTubeLinksList?.count == 0 {
                return cell
            }
            let dic = youTubeLinksList?[indexPath.row]
            if let link = dic?["Link"] as? String {
                cell.tfLinkMedia.text = link
            }
            cell.lblStatus.text = "YouTube"
            cell.tfLinkMedia.placeholder = "Enter Youtube link".localized
            cell.tfLinkMedia.keyboardType = .URL
            cell.imgIcon.image = UIImage(named: "youtube")
            cell.tfLinkMedia.tag = 4000 + indexPath.row
            cell.tfLinkMedia.delegate = self
            cell.btnAdd.stringTag = String(indexPath.row)
            cell.btnAdd.addTarget(self, action: #selector(onClickYoutubeAddAction(_:)), for: .touchUpInside)
            
            let count = youTubeLinksList?.count ?? 0
            if indexPath.row == 0 {
                if count == 5 {
                    cell.btnAdd.setImage(UIImage(named: "delet"), for: .normal)
                }else {
                    cell.btnAdd.setImage(UIImage(named: "add"), for: .normal)
                }
            }else {
                cell.btnAdd.setImage(UIImage(named: "delet"), for: .normal)
            }
            if updateStatus ?? false {
                cell.isUserInteractionEnabled = true
            }else {
                cell.isUserInteractionEnabled = false
            }
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "YouTubeLinkCell") as! YouTubeLinkCell
            cell.lblLinkInfo.text = "Media link will be activated\n after verified by OK$".localized
            return cell
        }
        
    }
    
    
    @objc func onClickPhoneType(_ sender: UIButton) {
        self.view.endEditing(true)
        DispatchQueue.main.async {
            if let window = UIApplication.shared.keyWindow {
                let phoneeType  = self.storyboard?.instantiateViewController(withIdentifier: "BusinessPhoneType") as! BusinessPhoneType
                self.addChild(phoneeType)
                window.rootViewController?.addChild(phoneeType)
                phoneeType.view.frame = .init(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
                phoneeType.delegate = self
                phoneeType.input = sender.stringTag
                phoneeType.setPerviousValue(value: sender.titleLabel?.text ?? "")
                window.addSubview(phoneeType.view)
                window.makeKeyAndVisible()
            }
        }
        
    }
    @objc func onClickAddAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let indx = Int(sender.stringTag ?? "0")
        if sender.currentImage == UIImage(named: "add") {
            let dic = ["PhoneNumber": "","PhoneType": "MOBILE"]
            if contactNumberList?.count ?? 0 > 4 {
                return
            }
            contactNumberList?.append(dic)
            
        }else {
            contactNumberList?.remove(at: indx ?? 0)
        }
        reloadSection(index: 0)
    }
    
    @objc func onClickYoutubeAddAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let index = Int(sender.stringTag ?? "0")
        if sender.currentImage == UIImage(named: "add") {
            if youTubeLinksList?.count ?? 0 > 4 {
                return
            }
            youTubeLinksList?.append(["Link": "https://www.youtube.com/"])
        }else {
            youTubeLinksList?.remove(at: index ?? 0)
        }
        reloadSection(index: 3)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 || section == 2 {
            return 35
        }else {
            return 0
        }
    }
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return createRequiredFieldLabel(headerTitle: "Business Contact Details".localized, reqTitle: "")
        }else if section == 2 {
            return createRequiredFieldLabel(headerTitle: "Media Profile".localized, reqTitle: "")
        }else {
            let view = UIView(frame: CGRect(x: 0, y: 0, width:0, height: 0))
            return view
        }
    }
    
    private func createRequiredFieldLabel(headerTitle:String, reqTitle: String) ->UIView? {
        let view1 = UIView(frame: CGRect(x: 5, y: 0, width:self.view.frame.width - 10, height: 35))
        view1.backgroundColor = hexStringToUIColor(hex: "#E0E0E0")
        let lblHeader = UILabel(frame: CGRect(x: 10, y: 0, width: (sizeOfString(myString: headerTitle)?.width)!, height:35))
        lblHeader.text = headerTitle
        lblHeader.textAlignment = .left
        lblHeader.font = UIFont(name: "Zawgyi-One", size: 14)
        lblHeader.backgroundColor = UIColor.clear
        lblHeader.textColor = UIColor.black
        
        let reqLabel = UILabel(frame: CGRect(x: self.view.frame.width - (sizeOfString(myString: reqTitle)?.width)! - 10, y: 0, width: (sizeOfString(myString: reqTitle)?.width)!, height:35))
        reqLabel.text = reqTitle
        reqLabel.textAlignment = .right
        reqLabel.font = UIFont(name: "Zawgyi-One", size: 14)
        reqLabel.backgroundColor = UIColor.clear
        reqLabel.textColor = UIColor.red
        view1.addSubview(lblHeader)
        view1.addSubview(reqLabel)
        return view1
    }
    
    private func sizeOfString(myString : String) ->CGSize? {
        let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: "Zawgyi-One", size: 16) ?? UIFont.systemFont(ofSize: 16.0) ])
        return stringSize
    }
    
}


extension BDContactDetailsVC: BusinessPhoneTypeDelegate {
    
    func seletedPhoneType(vc: UIViewController, selected: String, output: String) {
        vc.view.removeFromSuperview()
        if selected != "" {
            var row = 0
//            if selected == "LANDLINE"{
//               // output = "1"
//                row = Int(1)
//            }
//            else if selected == "Home"{
//                row = Int(2)
//            }
//            else if selected == "Fax"{
//                row = Int(3)
//            }else{
//                row = Int(0)
//            }
            row = Int(output) ?? 0
        //    if contactNumberList!.count >= row {
            var dic = contactNumberList?[row]
            dic?["PhoneType"] = selected
            if selected == "MOBILE" {
                dic?["PhoneNumber"] = ""
            }else if selected == "LANDLINE" {
                dic?["PhoneNumber"] = ""
            }
            else{
                dic?["PhoneNumber"] = ""
            }
            contactNumberList?[row] = dic!
            self.reloadRows(index: row, withSeciton: 0)
         //   }
        }
    }
    
    
}




class BusinessPhoneNumbersCell: UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var btnAdd: UIButton!{
        didSet{
            self.btnAdd.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnPhoneType: UIButton!{
        didSet{
            self.btnPhoneType.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var tfNumber: UITextField!{
        didSet{
            self.tfNumber.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblStatus: UILabel!{
        didSet{
            self.lblStatus.font = UIFont(name: appFont, size: appFontSize)
        }
    }
}

class BusinessYoutubeCell: UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var btnAdd: UIButton!{
        didSet{
            self.btnAdd.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var tfLinkMedia: UITextField!{
        didSet{
            self.tfLinkMedia.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblStatus: UILabel!{
        didSet{
            self.lblStatus.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
}
class YouTubeLinkCell: UITableViewCell {
    @IBOutlet weak var lblLinkInfo: UILabel!{
        didSet{
            self.lblLinkInfo.font = UIFont(name: appFont, size: appFontSize)
        }
    }
}


extension String {
    func validateWebiteUrl () -> Bool {
        let urlRegEx = "((\\w|-)+)(([.]|[/])((\\w|-)+))+"
        return NSPredicate(format: "SELF MATCHES %@", urlRegEx).evaluate(with: self)
    }
}
