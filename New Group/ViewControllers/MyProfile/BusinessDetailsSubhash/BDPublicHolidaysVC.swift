//
//  BDPublicHolidaysVC.swift
//  OK
//
//  Created by SHUBH on 12/27/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit

protocol  BDPublicHolidaysDelegate {
  func SelectHolidays(holidayList : [Dictionary<String,Any>])
}

class BDPublicHolidaysVC: OKBaseController {
  var delegate : BDPublicHolidaysDelegate?

    @IBOutlet weak var lblDays: UILabel!{
        didSet{
            lblDays.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblOpen: UILabel!{
        didSet{
            lblOpen.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblClose: UILabel!{
        didSet{
            lblClose.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnCancel: UIButton!{
        didSet{
            btnCancel.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnSet: UIButton!{
        didSet{
            btnSet.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    @IBOutlet weak var tblView: UITableView!
    var arrOldSelection: [Dictionary<String,Any>]?

    override func viewDidLoad() {
        super.viewDidLoad()
        lblDays.text = "Select Days".localized
        lblOpen.text = " Open".localized
        lblClose.text = "Close".localized
        btnCancel.setTitle("Cancel".localized, for: .normal)
        btnSet.setTitle("Set".localized, for: .normal)
        setMarqueLabelInNavigation()
    }
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: appFontSize)
        lblMarque.text = "Public Holidays Status".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.init(hex: "#1C2C99")
        self.navigationItem.titleView = lblMarque
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tblView.reloadData()
    }
    @IBAction func setButtonClicked(sender: UIButton) {
        
        
        guard (self.delegate?.SelectHolidays(holidayList: arrOldSelection!) != nil) else {
            return
        }
        alertViewObj.wrapAlert(title:"", body: "Holidays status successfully saved".localized, img: UIImage(named: "bank_success")!)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                 self.navigationController?.popViewController(animated: true)
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
   
    }
    
    @IBAction func cancelButtonClicked(sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backButtonClicked(sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func reloadRows(index: Int, withSeciton sec: Int) {
        UIView.setAnimationsEnabled(false)
        self.tblView.beginUpdates()
        let indexPosition = IndexPath(row: index, section: sec)
        self.tblView.reloadRows(at: [indexPosition], with: .none)
        self.tblView.endUpdates()
        UIView.setAnimationsEnabled(true)
    }

}

extension BDPublicHolidaysVC: UITableViewDelegate,UITableViewDataSource {
 
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return arrOldSelection?.count ?? 0
  }
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:PublicHolidayStatusCell = tableView.dequeueReusableCell(withIdentifier:"PublicHolidayStatusCell") as! PublicHolidayStatusCell
        cell.selectionStyle = .none
        let dic = arrOldSelection?[indexPath.row]
        if ok_default_language == "my" {
          cell.lblTitle.text = dic?["BHolidayName"] as? String ?? ""
        }else if ok_default_language == "en"{
            let holidayString = dic?["HolidayName"] as? String ?? ""
            let lines = holidayString.split(maxSplits: .max, omittingEmptySubsequences: true, whereSeparator: { $0 == "\n" })
            let result = lines.joined(separator: "\n")
            cell.lblTitle.text = result
            print(result)
        }else  {
            //cell.lblTitle.font = UIFont.systemFont(ofSize: 15.0)
            cell.lblTitle.text = dic?["BHolidayNameUnicode"] as? String ?? ""
        }
        cell.btnCheckBoxOpen.tag = indexPath.row
        cell.btnCheckBoxOpen.addTarget(self, action:#selector(checkOpenButtonClicked(_:)), for: .touchUpInside)
        cell.btnCheckBoxClose.tag = indexPath.row
        cell.btnCheckBoxClose.addTarget(self, action:#selector(checkCloseButtonClicked(_:)), for: .touchUpInside)
        
        if dic?["Status"] as? Bool ?? false {
            cell.btnCheckBoxOpen.setBackgroundImage(UIImage(named:"bd_green_check")!, for: .normal)
            cell.btnCheckBoxClose.setBackgroundImage(UIImage(named:"bd_red_check")!, for: .normal)
        }else {
            cell.btnCheckBoxOpen.setBackgroundImage(UIImage(named:"bd_red_check")!, for: .normal)
            cell.btnCheckBoxClose.setBackgroundImage(UIImage(named:"bd_green_check")!, for: .normal)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
  
  //MARK: Custom Methods
  @objc func checkOpenButtonClicked(_ sender: UIButton) {
   var dic = arrOldSelection?[sender.tag]
    if dic?["Status"] as? Bool ?? false {
       dic?["Status"] = false
    } else {
        dic?["Status"] = true
    }
    arrOldSelection?[sender.tag] = dic!
    reloadRows(index: sender.tag, withSeciton: 0)
  }
  
  @objc func checkCloseButtonClicked(_ sender: UIButton) {
    var dic = arrOldSelection?[sender.tag]
    if dic?["Status"] as? Bool ?? false {
        dic?["Status"] = false
    } else {
        dic?["Status"] = true
    }
    arrOldSelection?[sender.tag] = dic!
    reloadRows(index: sender.tag, withSeciton: 0)
  }
}
