//
//  DataBusinessUP.swift
//  OK
//
//  Created by Imac on 3/27/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

extension BusinessDetailsVC: WebServiceResponseDelegate {
    
    func checkRequiredFields(handler: (_ message:String, _ isBool: Bool)-> Void) {
        var emptyFields = ""
        
        model.BusinessName = businessName
        if model.BusinessName == ""{
            emptyFields = emptyFields + "\n" + "Business Name".localized
        }else if model.BusinessName.count < 3 {
          emptyFields = emptyFields + "\n" + "Business Name must be more than 2 characters".localized
        }
        if model.OwnershipType == "0" || model.OwnershipType == "" {
            emptyFields = emptyFields + "\n" + "Ownership Type".localized
        }
        if model.BusinessCategory == "" {
            emptyFields = emptyFields + "\n" + "Business Category".localized
        }
        if model.BusinessSubCategory == "" {
            emptyFields = emptyFields + "\n" + "Business Sub-Category".localized
        }
        if model.LogoImages?["Image1"] as? String ?? "" == "" {
            emptyFields = emptyFields + "\n" + "Business Image".localized
        }
        model.BusinessOutlet = businessOutLet
//        if model.BusinessOutlet == ""{
//            emptyFields = emptyFields + "\n" + "Business Outlet".localized
//        }
        if model.BusinessRegistration1 == ""{
            emptyFields = emptyFields + "\n" + "Business Liencese 1".localized
        }
        if model.BusinessRegistration2 == ""{
            emptyFields = emptyFields + "\n" + "Business Liencese 2".localized
        }
        if model.Division == ""{
            emptyFields = emptyFields + "\n" + "State/Division Name".localized
        }
        if model.Township == ""{
            emptyFields = emptyFields + "\n" + "Township Name".localized
        }
        if model.Street == "" {
            emptyFields = emptyFields + "\n" + "Street Name".localized
        }
        
//        if model.HouseNumber == "" &&  model.FloorNumber == "" && model.RoomNumber == "" {
//           emptyFields = emptyFields + "\n" + "House No/Floor No/Room No".localized
//        }
        
        if emptyFields == "" {
            handler(emptyFields,true)
        }else {
            handler(emptyFields,false)
        }
    
    }
    
    func updateProfileStatusCount() -> Int {
        var countStatus = [0,0,0,0,0,0,0,0,0,0]
        
        
        if businessName == "" {
            countStatus[0] = 0
        }else {
            countStatus[0] = 1
        }
        
//        if businessOutLet == "" {
//          countStatus[1] = 0
//        }else {
//          countStatus[1] = 1
//        }
        
        if model.OwnershipType == "0" || model.OwnershipType == "" {
            countStatus[1] = 0
        }else {
            countStatus[1] = 1
        }
        
        if model.BusinessCategory == "" {
           countStatus[2] = 0
        }else {
            countStatus[2] = 1
        }
        if model.BusinessSubCategory == "" {
           countStatus[3] = 0
        }else {
           countStatus[3] = 1
        }
     
        if model.BusinessRegistration1 == "" {
            countStatus[4] = 0
        }else {
              countStatus[4] = 1
        }
        if model.BusinessRegistration2 == "" {
             countStatus[5] = 0
        }else {
              countStatus[5] = 1
        }
        
        if model.LogoImages?["Image1"] as? String ?? "" == "" {
            countStatus[6] = 0
        }else {
            countStatus[6] = 1
        }
        
        if model.Division == "" {
            countStatus[7] = 0
        }else {
            countStatus[7] = 1
        }
        
        if model.Township == "" {
            countStatus[8] = 0
        }else {
            countStatus[8] = 1
        }
        if model.Township == "" {
            countStatus[9] = 0
        }else {
            countStatus[9] = 1
        }
        
        var finalCount = 0
        for item in countStatus {
            if item == 1 {
               finalCount += 1
            }
        }
        return finalCount
    }
    
    func UpdateBusinessProfile() {
        if appDelegate.checkNetworkAvail() {
            let web = WebApiClass()
            web.delegate = self

            let urlStr   = Url.updateBusinessDetails
             let url = getUrl(urlStr: urlStr, serverType: .updateProfile)
             println_debug(url)
            let dic = model.wrapDataModel()
            println_debug("Business Details ----->\(dic)")
            web.genericClassReg(url: url, param: dic, httpMethod: "POST", mScreen: "UpdateProfile", hbData: nil, authToken: nil)
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
        } else {
            alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img:#imageLiteral(resourceName: "r_user"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        DispatchQueue.main.async {
            progressViewObj.removeProgressView()
        }
        do {
            if let data = json as? Data {
                let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let dic = dict as? Dictionary<String,AnyObject> {
                    println_debug(dic["Msg"] as! String)
                    if dic["Code"] as? NSNumber == 200 {
                        print(dic)
                        // Call addUpdate profile API
                        UpdateProfileModel.share.business_details = myProfileStatus
                        var urlString   = Url.addUpdateProfileDetails
                        urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                        let urlS = getUrl(urlStr: urlString, serverType: .updateProfile)
                        UpdateProfileModel.share.updateProfileMenuStatus(urlStr: urlS , handle:{(success , response) in
                            if success {
                                profileObj.callLoginApi(aCode: UserModel.shared.mobileNo, withPassword: ok_password!, handler: { (success) in
                                    DispatchQueue.main.async {
                                        profileObj.callUpdateProfileApi(aCode: UserModel.shared.mobileNo, handler: { (success) in
                                            if success {
                                               //self.setPreviousLanguage()
                                                UserModel.shared.logoImages = self.model.LogoImages?["Image1"] as? String ?? ""
                                                UserModel.shared.ownershipType = self.model.OwnershipType
                                                alertViewObj.wrapAlert(title:"", body:"Your business details updated successfully".localized, img:#imageLiteral(resourceName: "bank_success"))
                                                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                                    self.navigationController?.popViewController(animated: true)
                                                })
                                                DispatchQueue.main.async {
                                                    alertViewObj.showAlert(controller: self)
                                                }

                                            }
                                        })
                                    }
                                })
                                
                            }else {
                                println_debug("Failed")
                            }
                        })
                    }else {
                        DispatchQueue.main.async {
                            self.showAlert(alertTitle: "", alertBody: dic["Msg"] as! String , alertImage: #imageLiteral(resourceName: "r_user"))
                        }
                    }
                }
            }
        } catch {
            DispatchQueue.main.async {
                self.showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))
            }
        }
    }
    
    func setPreviousLanguage() {
        if UserDefaults.standard.bool(forKey: "LanguageStatusChanged"){
            let preLang = UserDefaults.standard.value(forKey: "PreviousLanguage") as? String
            UserDefaults.standard.set(preLang!, forKey: "currentLanguage")
            appDel.setSeletedlocaLizationLanguage(language: preLang!)
            UserDefaults.standard.synchronize()
        }
    }
    
    func callStreetVillageAPI(townshipcode : String){
        let urlStr   = Url.URL_VillageList + townshipcode
        let url = getUrl(urlStr: urlStr, serverType: .serverApp)
        // let url = URL(string: "https://www.okdollar.co/RestService.svc/GetVillageStreetByTownShipCode?TownshipCode=" + townshipcode)
        let params = Dictionary<String,String>()
        TopupWeb.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", handle: {response, isSuccess in
            if isSuccess {
                if let json = response as? Dictionary<String,Any> {
                    let response = json["Data"] as Any
                    let dic = OKBaseController.convertToDictionary(text: response as! String)
                    if let d = dic {
                        if let arrDic = d["Streets"] as? Array<Dictionary<String,String>> {
                            let streetList = StreetList()
                            streetList.isStreetList = true
                            streetList.streetArray = arrDic
                            VillageManager.shared.allStreetList = streetList
                            
                        }else{
                            let streetList = StreetList()
                            streetList.streetArray.removeAll()
                            streetList.isStreetList = false
                            VillageManager.shared.allStreetList = streetList
                        }
                        if let arrDic1 = d["Villages"] as? Array<Dictionary<String,String>>{
                            DispatchQueue.main.async {
                                let villageList = VillageList()
                                villageList.villageArray = arrDic1
                                VillageManager.shared.allVillageList = villageList
                                villageList.isVillageList = true
                            }
                        }else{
                            DispatchQueue.main.async {
                                let villageList = VillageList()
                                villageList.villageArray.removeAll()
                                VillageManager.shared.allVillageList = villageList
                                villageList.isVillageList = false
                            }
                        }
                        println_debug(VillageManager.shared.allVillageList)
                    }
                }
            }else {
                DispatchQueue.main.async {
                self.showAlert(alertTitle: "", alertBody: "Network error and try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
                }
            }
        })
    }
}
