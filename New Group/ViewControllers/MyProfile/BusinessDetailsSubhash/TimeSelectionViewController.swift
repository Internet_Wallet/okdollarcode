//
//  TimeSelectionViewController.swift
//  OK
//
//  Created by PC on 11/27/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol  TimeSelectionViewControllerDelegate{
    func SelectTime(timeList : String, forDys dayIndex: Int)
}

class TimeSelectionViewController: UIViewController {

    var delegate : TimeSelectionViewControllerDelegate?

    @IBOutlet weak var lblCloseAt: UILabel! {
        didSet{
            lblCloseAt.font = UIFont(name: appFont, size: appFontSize)
           lblCloseAt.text = lblCloseAt.text?.localized
        }
    }

    @IBOutlet weak var viewOpenAt: UIView!
    @IBOutlet weak var viewCloseAt: UIView!
    @IBOutlet weak var lblTitle: UILabel! {
        didSet {
            lblTitle.font = UIFont(name: appFont, size: appFontSize)
            lblTitle.text = lblTitle.text?.localized
        }
    }
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblCloseTime: UILabel!
    @IBOutlet weak var dateTimePicker: UIDatePicker!
    @IBOutlet weak var dateClosePicker: UIDatePicker!
    @IBOutlet weak var btnCancel: UIButton! {
        didSet {
            btnCancel.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnCancel.setTitle("Cancel".localized,for : .normal)
        }
    }
    @IBOutlet weak var btnSet: UIButton! {
        didSet{
            btnSet.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnSet.setTitle("Set".localized,for : .normal)
        }
    }
    @IBOutlet weak var btnCloseCancel: UIButton! {
        didSet{
            btnCloseCancel.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnCloseCancel.setTitle("Cancel".localized,for : .normal)
        }
    }
    @IBOutlet weak var btnSetCancel: UIButton! {
        didSet {
            btnSetCancel.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnSetCancel.setTitle("Set".localized,for : .normal)
        }
    }
    @IBOutlet weak var viewTransparent: UIView!
    @IBOutlet weak var datePickerOpenAt: UIDatePicker!
    @IBOutlet weak var datePickerCloseAt: UIDatePicker!
    var seletedIndex: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.title = "Select Open Time".localized
        viewOpenAt.isHidden = false
        viewCloseAt.isHidden = true
        if #available(iOS 13.4, *) {
            datePickerOpenAt.preferredDatePickerStyle = .wheels
            datePickerCloseAt.preferredDatePickerStyle = .wheels
                   } else {
                       // Fallback on earlier versions
                   }
        let date = datePickerOpenAt.date
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.timeStyle = .full
        formatter.dateFormat = "hh:mm a"
        lblTime.text = formatter.string(from:date)
        
        let dateClose = datePickerCloseAt.date
        lblCloseTime.text = formatter.string(from:dateClose)
        setMarqueLabelInNavigation(title: "Select Open Time".localized)
    }
    
    private func setMarqueLabelInNavigation(title: String) {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: appFontSize)
        lblMarque.text = title
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.init(hex: "#1C2C99")
        self.navigationItem.titleView = lblMarque
    }

    @IBAction func openAtDatePickerAction(_ sender: Any) {
        let date = datePickerOpenAt.date
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.timeStyle = .full
        formatter.dateFormat = "hh:mm a"
        lblTime.text = formatter.string(from:date)
    }
    
    @IBAction func closeAtDatePickerAction(_ sender: Any) {
        let date = datePickerCloseAt.date
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.timeStyle = .full
        formatter.dateFormat = "hh:mm a"
        lblCloseTime.text = formatter.string(from:date)
    }
    
    //Open At
    @IBAction func btnCancelAction(_ sender: Any) {
      self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSetAction(_ sender: Any) {
        viewOpenAt.isHidden = true
        viewCloseAt.isHidden = false
        setMarqueLabelInNavigation(title: "Select Close Time".localized)
    }
    
    //Close At
    @IBAction func btnCloseCancelAction(_ sender: Any) {
        viewOpenAt.isHidden = false
        viewCloseAt.isHidden = true
        setMarqueLabelInNavigation(title: "Select Open Time".localized)
    }
    
    @IBAction func btnCloseSetAction(_ sender: Any) {
        guard (self.delegate?.SelectTime(timeList: (lblTime.text!+" - "+lblCloseTime.text!), forDys: seletedIndex!) != nil) else {
            return
        }
      self.navigationController?.popViewController(animated: true)
    }
  
  @IBAction func backButtonClicked(sender: UIButton) {
    if viewCloseAt.isHidden {
        self.navigationController?.popViewController(animated: true)
    }else {
        viewOpenAt.isHidden = false
        viewCloseAt.isHidden = true
        setMarqueLabelInNavigation(title: "Select Open Time".localized)
    }
    
  }

}
