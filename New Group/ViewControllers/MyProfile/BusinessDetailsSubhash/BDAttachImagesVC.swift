//
//  BDAttachImagesVC.swift
//  OK
//
//  Created by SHUBH on 12/27/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit

protocol BDAttachImagesVCDelegte {
    func businesssImagesUploaded(dic: Dictionary<String,Any>, logoStauts: Int)
}

class BDAttachImagesVC: OKBaseController,PAImagePreviewVCDelegate {
    var delegate: BDAttachImagesVCDelegte?
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblLogoImage: UILabel!{
        didSet{
            lblLogoImage.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgImage1: UIImageView!
    @IBOutlet weak var lblImage1: UILabel!{
        didSet{
            lblImage1.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgImage2: UIImageView!
    @IBOutlet weak var lblImage2: UILabel!{
        didSet{
            lblImage2.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgImage3: UIImageView!
    @IBOutlet weak var lblImage3: UILabel!{
        didSet{
            lblImage3.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnUpload: UIButton!{
        didSet{
            btnUpload.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    
    @IBOutlet weak var btnCancel: UIButton!{
        didSet{
            btnCancel.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var lblNote: UILabel!{
        didSet{
            lblNote.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblLogoStatus: UILabel!{
        didSet{
            lblLogoStatus.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    var isEditable: Bool?
    
    var imageDic: Dictionary<String,Any>?
    var logoStatus: Int?
    override func viewDidLoad() {
        btnUpload.setTitle("Update".localized, for: .normal)
        btnCancel.setTitle("Cancel".localized, for: .normal)
        lblLogoImage.text = "Logo Image".localized
        lblImage1.text = "Business Image 1".localized
        lblImage2.text = "Business Image 2".localized
        lblImage3.text = "Business Image 3".localized
        if logoStatus == -1 {
         lblLogoStatus.text = "Incomplete".localized
        }else if logoStatus == 0 {
         lblLogoStatus.text = "Submitted".localized
        }else if logoStatus == 1 {
          lblLogoStatus.text = "Verified".localized
        }else if logoStatus == 2 {
          lblLogoStatus.text = "Rejected".localized
        }
        
        lblNote.text = "Logo & images(s) will appear only after verified by OK$".localized
        showImages(imgName: "Logo Image".localized, strURl: imageDic?["Image1"] as? String ?? "")
        showImages(imgName: "Business Image 1".localized, strURl: imageDic?["Image2"] as? String ?? "")
        showImages(imgName: "Business Image 2".localized, strURl: imageDic?["Image3"] as? String ?? "")
        showImages(imgName: "Business Image 3".localized, strURl: imageDic?["Image4"] as? String ?? "")
        
        setMarqueLabelInNavigation()
        super.viewDidLoad()
    }
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: appFontSize)
        lblMarque.text = "Attach Images".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
    }
    
    @IBAction func cancelButtonClicked(sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickUpdateAction(_ sender: Any) {
        if let del = delegate {
            if let dic = imageDic {
                del.businesssImagesUploaded(dic: dic, logoStauts: logoStatus ?? -1)
            }
        }
         self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnImageSelectionAction(sender: UIButton!) {
        if isEditable ?? false {
            return
        }
        DispatchQueue.main.async {
            let imgEditVC = UIStoryboard(name: "MyProfile", bundle: Bundle.main).instantiateViewController(withIdentifier: "PAImagePreviewVC")  as! PAImagePreviewVC
            imgEditVC.imagePreviewVCDelegate = self
            if(sender.tag == 0) {
                if self.logoStatus == -1 || self.logoStatus == 2 {
                    imgEditVC.strSubTitle = "Logo Image".localized
                    imgEditVC.fullURL = self.imageDic?["Image1"] as? String ?? ""
                    imgEditVC.strTitle = self.lblLogoImage.text
                }else{
                    self.ZoomImage()
                    return
                }
            } else if(sender.tag == 1) {
                imgEditVC.strSubTitle = "Business Image 1".localized
                imgEditVC.fullURL = self.imageDic?["Image2"]  as? String ?? ""
                imgEditVC.strTitle = self.lblImage1.text
            }else if(sender.tag == 2) {
                imgEditVC.strSubTitle = "Business Image 2".localized
                imgEditVC.fullURL = self.imageDic?["Image3"] as? String ?? ""
                imgEditVC.strTitle = self.lblImage2.text
            } else {
                imgEditVC.strSubTitle = "Business Image 3".localized
                imgEditVC.fullURL = self.imageDic?["Image4"] as? String ?? ""
                imgEditVC.strTitle = self.lblImage3.text
            }
            self.navigationController?.pushViewController(imgEditVC, animated: true)
        }

    }
    
    func showImages(imgName: String, strURl: String) {
        let image = strURl.replacingOccurrences(of: " ", with: "%20").replacingOccurrences(of: "\\", with: "")
        switch imgName {
        case "Logo Image".localized :
            if image != "" {
                let urlLogo = URL(string: image)
                imgLogo.sd_setImage(with: urlLogo, placeholderImage: UIImage(named: "r_attachement"))
            }
        case "Business Image 1".localized :
            if image != "" {
                let urlLogo = URL(string: image)
                imgImage1.sd_setImage(with: urlLogo, placeholderImage: UIImage(named: "r_attachement"))
            }
        case "Business Image 2".localized :
            if image != "" {
                let urlLogo = URL(string: image)
                imgImage2.sd_setImage(with: urlLogo, placeholderImage: UIImage(named: "r_attachement"))
            }
        case "Business Image 3".localized :
            if image != "" {
                let urlLogo = URL(string: image)
                imgImage3.sd_setImage(with: urlLogo, placeholderImage: UIImage(named: "r_attachement"))
            }
        default :
            println_debug("Hello!")
        }
    }
    
    // MARK: - Image Display Delegate
    func ImagePreviewUrl(imageName : String, imageURL: String) {
        let image = imageURL.replacingOccurrences(of: " ", with: "%20")
        switch imageName {
        case "Logo Image".localized :
            imageDic?["Image1"] = image
            logoStatus = 0
            lblLogoStatus.text = "Submitted".localized
            showImages(imgName: imageName, strURl: image)
        case "Business Image 1".localized :
            imageDic?["Image2"] = image
            showImages(imgName: imageName, strURl: image)
        case "Business Image 2".localized :
            imageDic?["Image3"] = image
            showImages(imgName: imageName, strURl: image)
        case "Business Image 3".localized :
            imageDic?["Image4"] = image
            showImages(imgName: imageName, strURl: image)
        default :
            println_debug("Hello!")
        }
    }
    
    
    func ZoomImage() {
        DispatchQueue.main.async {
            let zoomImageUP = self.storyboard?.instantiateViewController(withIdentifier: "ZoomImageUP") as! ZoomImageUP
            let image1 = UIImage()
            let url1Exits = self.imageDic?["Image1"] as? String ?? ""
            if url1Exits != "" {
                if let img = image1.convertURLToImage(str: url1Exits) {
                    zoomImageUP.myImage = img
                    zoomImageUP.headerTitile = "Logo Image".localized
                    self.navigationController?.pushViewController(zoomImageUP, animated: true)
                }
            }
        }

    }
}
