//
//  BusinessDetailsVC.swift
//  OK
//
//  Created by Subhash Arya on 13/03/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import Rabbit_Swift

class BusinessDetailsVC: OKBaseController {
    let sectionCount = 7
    @IBOutlet weak var btnEdit: UIButton!{
        didSet{
            btnEdit.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var tbUpdateProfile: UITableView!
    @IBOutlet weak var bottomContraintsTV: NSLayoutConstraint!
    @IBOutlet weak var btnUpdate: UIButton!{
        didSet{
            btnUpdate.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    var businessData: Dictionary<String,Any>?
    var businessInfo = [NSMutableDictionary]()
    var AddressInfo = [NSMutableDictionary]()
    var addtionalInfo = [NSMutableDictionary]()
    var daytimeInfo = [NSMutableDictionary]()
    var holidayInfo = [NSMutableDictionary]()
    var categoryDetail : CategoryDetail?
    var categoryName = ""
    var categoryimage = ""
    var subcategoryName = ""
    var subcategoryimage = ""
    var arrWorkingDays = [Dictionary<String,String>]()
    var arrPublicDays = [Dictionary<String,Any>]()
    var myProfileStatus = 0
    var myProfileStatusInPercent = 0
    var kycBusinessRegistration = false
    var kycBusinessName = false
    var kycBusinessCategory = false
    var kycBusinessSubCategory = false
    
    var isBusinessNameRowHidden = true
    var BUSINESSNAMECHARSET = ""
    var businessName = ""
    var businessOutLet = ""
    var model = BusinessDetailsVCModel()
    var tvVillageList = Bundle.main.loadNibNamed("VillageStreetListView", owner: self, options: nil)?[0] as! VillageStreetListView
    var tvStreetList = Bundle.main.loadNibNamed("StreetListView", owner: self, options: nil)?[0] as! StreetListView
    var isVillageAvaible = true
    var locationDetails: LocationDetail?
    var townShipDetail : TownShipDetail?
    var divisionName = ""
    var townshipName = ""
    var CHARSET = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        bottomContraintsTV.constant = 0
        btnUpdate.setTitle("Update".localized, for: .normal)
        if ok_default_language == "my" {
            BUSINESSNAMECHARSET = BUSINESSNAME_CHAR_SET_My
             CHARSET = STREETNAME_CHAR_SET_My
        }else if ok_default_language == "en"{
            BUSINESSNAMECHARSET = BUSINESSNAME_CHAR_SET_En
            CHARSET = STREETNAME_CHAR_SET_En
        }else {
            BUSINESSNAMECHARSET = BUSINESSNAME_CHAR_SET_Uni
            CHARSET = STREETNAME_CHAR_SET_Uni
        }
        
        
        if let business = businessData?["BusinessInfo"] as? Dictionary<String,Any> {
            
            println_debug(business)
            
            if ok_default_language == "my"{
                model.BusinessName = parabaik.uni(toZawgyi:business["BusinessName"] as? String) ?? ""

            }
            else {
                model.BusinessName = parabaik.zawgyi(toUni:business["BusinessName"] as? String) ?? ""

            }
            //model.BusinessName = business["BusinessName"] as? String ?? ""
            businessName = model.BusinessName
            let img1 = business["BusinessRegistration1"] as? String ?? ""
            model.BusinessRegistration1 = img1.replacingOccurrences(of: "\\", with: "")
            let img2 = business["BusinessRegistration2"] as? String ?? ""
            model.BusinessRegistration2 =  img2.replacingOccurrences(of: "\\", with: "")
            model.BusinessCategory = business["BusinessCategory"] as? String ?? ""
            model.BusinessSubCategory = business["BusinessSubCategory"] as? String ?? ""
            model.BusinessOutlet = business["BusinessOutlet"] as? String ?? ""
            businessOutLet = model.BusinessOutlet
            model.OkDollarJoiningDate = business["OkDollarJoiningDate"] as? String ?? ""
            model.UserMembership = business["UserMembership"] as? String ?? ""
            model.UserMembershipStatus = business["UserMembershipStatus"]  as? Bool ?? false
            model.Email = business["Email"] as? String ?? ""
            model.Website = business["Website"] as? String ?? ""
            model.FacebookPage = business["FacebookPage"] as? String ?? ""
            model.Instagram = business["Instagram"] as? String ?? ""
            model.OwnershipType = business["OwnershipType"] as? String ?? "0"
            model.BusinessApproveStatus = business["BusinessApproveStatus"] as? Int ?? 0
            model.LogoApproveStatus = business["LogoApproveStatus"] as? Int ?? 0
        }
        
        if let business = businessData?["BusinessAddressInfo"] as? Dictionary<String,Any> {
            println_debug(business)
            
            if ok_default_language == "my"{
                
                model.Village = parabaik.uni(toZawgyi:business["Village"] as? String) ?? "Enter Village Tract".localized
                model.Street = parabaik.uni(toZawgyi:business["Street"] as? String) ?? "Enter street".localized
                model.HouseNumber = parabaik.uni(toZawgyi:business["HouseNumber"] as? String) ?? "House No.".localized
                model.FloorNumber = parabaik.uni(toZawgyi:business["FloorNumber"] as? String) ?? "Floor No.".localized
                model.RoomNumber = parabaik.uni(toZawgyi:business["RoomNumber"] as? String) ?? "Room No.".localized

            }else {
                model.Village = business["Village"] as? String ?? "Enter Village Tract".localized
                model.Street = business["Street"] as? String ?? "Enter street".localized
                model.HouseNumber = business["HouseNumber"] as? String ?? "House No.".localized
                model.FloorNumber = business["FloorNumber"] as? String ?? "Floor No.".localized
                model.RoomNumber = business["RoomNumber"] as? String ?? "Room No.".localized
            }
            model.AddressType = business["AddressType"] as? String ?? "Business".localized
            model.Division = business["Division"] as? String ?? ""
            model.Township = business["Township"] as? String ?? ""
            model.City = business["City"] as? String ?? ""
            model.HouseBlockNumber = business["HouseBlockNumber"] as? String ?? ""
        }
        
        if  model.Division == "" && UserModel.shared.addressType == "Business".localized {
            
            if ok_default_language == "my"{
                model.Village = parabaik.uni(toZawgyi:UserModel.shared.villageName)
                model.Street = parabaik.uni(toZawgyi:UserModel.shared.address2)
                model.HouseNumber = parabaik.uni(toZawgyi:UserModel.shared.houseBlockNo)
                model.FloorNumber = parabaik.uni(toZawgyi:UserModel.shared.floorNumber)
                model.RoomNumber = parabaik.uni(toZawgyi:UserModel.shared.roomNumber)
            }else {
                model.Village = UserModel.shared.villageName
                model.Street = UserModel.shared.address2
                model.HouseNumber = UserModel.shared.houseBlockNo
                model.FloorNumber = UserModel.shared.floorNumber
                model.RoomNumber = UserModel.shared.roomNumber
            }

            model.AddressType = UserModel.shared.addressType
            model.Division = UserModel.shared.state
            model.Township = UserModel.shared.township
            model.City = UserModel.shared.address1
            model.HouseBlockNumber = UserModel.shared.houseName
        }
        
        if let owerType = businessData?["OwrshipTypes"] as? [Dictionary<String,Any>] {
            model.OwrshipTypes = owerType
        }
        if let logoImages = businessData?["LogoImages"] as? Dictionary<String,Any> {
            model.LogoImages = logoImages
        }
        
        
        if let dayTime = businessData?["WorkingDayAndTime"] as? [Dictionary<String,Any>] {
            print(dayTime)
            model.WorkingDayAndTime = dayTime
        }
        if let holiday = businessData?["PublicHoliday"] as? [Dictionary<String,Any>] {
            model.PublicHoliday = holiday
            self.getAllPublicHoliday()
        }
        if let contact = businessData?["ContactNumber"] as? [Dictionary<String,Any>] {
            model.ContactNumber = contact
        }
        
        if let youtube = businessData?["YouTubeLinks"] as? [Dictionary<String,Any>] {
            model.YouTubeLinks = youtube
        }
        if let kyc = businessData?["KYC"] as? [Dictionary<String,Any>] {
            model.KYC = kyc
            for key in kyc {
                if key["Code"] as? String == "BusinessRegistration" {
                    kycBusinessRegistration = key["Status"] as? Bool ?? false
                }else if key["Code"] as? String == "BusinessSubCategory" {
                    kycBusinessSubCategory = key["Status"] as? Bool ?? false
                }else if key["Code"] as? String == "BusinessName" {
                    kycBusinessName = key["Status"] as? Bool ?? false
                }else if key["Code"] as? String == "BusinessCategory" {
                    kycBusinessCategory = key["Status"] as? Bool ?? false
                }
            }
        }
        
       // kycBusinessRegistration = true
        
        self.setMarqueLabelInNavigation()
        tbUpdateProfile.register(UINib(nibName: "BusinessHeaderCell", bundle: nil), forCellReuseIdentifier: "BusinessHeaderCell")
        tbUpdateProfile.register(UINib(nibName: "NRCPassportUPCell", bundle: nil), forCellReuseIdentifier: "NRCPassportUPCell")
        tbUpdateProfile.register(UINib(nibName: "NameActionUPCell", bundle: nil), forCellReuseIdentifier: "NameActionUPCell")
        tbUpdateProfile.register(UINib(nibName: "NameInputUPCell", bundle: nil), forCellReuseIdentifier: "NameInputUPCell")
        tbUpdateProfile.register(UINib(nibName: "NameUPCell", bundle: nil), forCellReuseIdentifier: "NameUPCell")
        tbUpdateProfile.register(UINib(nibName: "AddressUPCell", bundle: nil), forCellReuseIdentifier: "AddressUPCell")
        tbUpdateProfile.register(UINib(nibName: "BusinessNameCellUP", bundle: nil), forCellReuseIdentifier: "BusinessNameCellUP")
        tbUpdateProfile.register(UINib(nibName: "StatusUPCell", bundle: nil), forCellReuseIdentifier: "StatusUPCell")
        tbUpdateProfile.register(UINib(nibName: "StatusButtonUPCell", bundle: nil), forCellReuseIdentifier: "StatusButtonUPCell")
        tbUpdateProfile.register(UINib(nibName: "BusinessHolidayUPCell", bundle: nil), forCellReuseIdentifier: "BusinessHolidayUPCell")
        tbUpdateProfile.register(UINib(nibName: "CheckBoxHeaderCell", bundle: nil), forCellReuseIdentifier: "CheckBoxHeaderCell")
        tbUpdateProfile.register(UINib(nibName: "SelectDayTimeCell", bundle: nil), forCellReuseIdentifier: "SelectDayTimeCell")
        
        
        let states = TownshipManager.allLocationsList
        for state in states {
            if state.stateOrDivitionCode == model.Division {
                if ok_default_language == "my" {
                    divisionName = state.stateOrDivitionNameMy
                }else if ok_default_language == "en"{
                    divisionName = state.stateOrDivitionNameEn
                }else {
                    divisionName = state.stateOrDivitionNameUni
                }
                locationDetails = state
                let township = state.townshipArryForAddress
                for town in township {
                    if town.townShipCode == model.Township {
                        if town.GroupName.contains(find: "YCDC") || town.GroupName.contains(find: "MCDC"){
                            isVillageAvaible = false
                        }
                        if town.cityNameEN == "Amarapura" || town.cityNameEN == "Patheingyi" || town.townShipNameEN == "PatheinGyi" {
                            isVillageAvaible = true
                        }
                        if isVillageAvaible {
                            self.callStreetVillageAPI(townshipcode: model.Township)
                        }
                        if ok_default_language == "my" {
                            townshipName = town.townShipNameMY
                            if town.GroupName == "" {
                                model.City = town.cityNameMY
                            }else {
                                if town.isDefaultCity == "1" {
                                    model.City = town.DefaultCityNameMY
                                }else{
                                    model.City = town.cityNameMY
                                }
                            }
                        }else if ok_default_language == "en" {
                            townshipName = town.townShipNameEN
                            if town.GroupName == "" {
                                model.City = town.cityNameEN
                            }else {
                                if town.isDefaultCity == "1" {
                                    model.City = town.DefaultCityNameEN
                                }else {
                                    model.City = town.cityNameEN
                                }
                            }
                        }else {
                            townshipName = town.townShipNameUni
                            if town.GroupName == "" {
                                model.City = town.cityNameUni
                            }else {
                                if town.isDefaultCity == "1" {
                                   model.City = town.DefaultCityNameUni
                                }else {
                                model.City = town.cityNameUni
                                }
                            }
                        }
                        break
                    }
                }
            }
        }
        
        
        let categoriesList = CategoriesManager.categoriesList
        var i = 0
        while i < (categoriesList.count) {
            let category = categoriesList[i]
            if (category.categoryCode.localizedCaseInsensitiveContains(model.BusinessCategory)) {
                categoryDetail = category
                if ok_default_language == "my" {
                    categoryName = category.categoryBurmeseName
                }else if ok_default_language == "en"{
                    categoryName = category.categoryName
                }else {
                    categoryName = category.categoryBurmeseUName
                }
                
                categoryimage = category.category_image
                //fetch subcat
                let subCatList = category.subCategoryList
                var subCat = 0
                while subCat < (subCatList.count) {
                    let subCategory = subCatList[subCat]
                    if (subCategory.subCategoryCode.localizedCaseInsensitiveContains(replaceString(str: model.BusinessSubCategory))) {
                        if ok_default_language == "my" {
                            subcategoryName = subCategory.subCategoryBurmeseName
                        }else if ok_default_language == "en"{
                            subcategoryName = subCategory.subCategoryName
                        }else {
                            subcategoryName = subCategory.subCategoryBurmeseUName
                        }
                        subcategoryimage = subCategory.subCategoryEmoji
                        break
                    }
                    subCat = subCat + 1
                }
                break
            }
            i = i + 1
        }
        
        
        businessInfo = [["icon": "r_security_question","status": "Ownership Type".localized, "title": setOwerShipType(number: model.OwnershipType)],
                        ["icon": "categories","status": "Category".localized, "title":categoryName],
                        ["icon": "categories","status": "Sub-Category".localized, "title":subcategoryName],
                        ["icon": "branch_outlet","status": "Branch Outlet".localized, "title":  businessOutLet]
        ]
        AddressInfo = [["icon": "addressUp","statusTitle": "Address Type".localized,"title": model.AddressType],
                       ["icon": "r_division","statusTitle": "Division / State".localized,"title": divisionName],
                       ["icon": "r_city","statusTitle": "Township".localized,"title": townshipName],
                       ["icon": "r_township","statusTitle": "City".localized,"title": model.City],
                       ["icon": "village_track","statusTitle": "Village Tract".localized,"title": model.Village],
                       ["icon": "r_street","statusTitle": "Street".localized,"title": model.Street],
                       ["house_no": model.HouseNumber,"floor_no": model.FloorNumber,"room_no": model.RoomNumber],
                       ["icon": "housing","statusTitle": "Housing /Industrial  Zone Name".localized,"title": model.HouseBlockNumber]]
        addtionalInfo = [["icon": "b_contact","status": "", "title": "Contact Details".localized],["icon": "attachBig","status": "", "title": "Logo & Image(s) Attached".localized]]
        daytimeInfo = [["icon": "r_opening_ten","status": "", "title": "Select Business Days & Time".localized]]
        holidayInfo = [["icon": "dob","status": "", "title": "Public Holidays Open Status".localized]]
        makeOrderOfdays()

        self.myProfileStatus = self.updateProfileStatusCount()
        tbUpdateProfile.delegate = self
        tbUpdateProfile.dataSource = self
    }
    
    private func makeOrderOfdays() {
        if let workingDays = model.WorkingDayAndTime {
            let dic = Dictionary<String,Any>()
            var tempDic = [dic,dic,dic,dic,dic,dic,dic,dic,dic]
            for dayDic in workingDays {
                let day = dayDic["DayCode"] as? String ?? ""
                if day == "SUN" {
                    tempDic[0] = dayDic
                }else if day == "MON" {
                    tempDic[1] = dayDic
                }else if day == "TUE" {
                    tempDic[2] = dayDic
                }else if day == "WED" {
                    tempDic[3] = dayDic
                }else if day == "THU" {
                    tempDic[4] = dayDic
                }else if day == "FRI" {
                    tempDic[5] = dayDic
                }else if day == "SAT" {
                    tempDic[6] = dayDic
                }else if day == "SUB" {
                    tempDic[7] = dayDic
                }else if day == "ALL" {
                    tempDic[8] = dayDic
                }
            }
             model.WorkingDayAndTime?.removeAll()
            for dic in tempDic {
                if !(dic.isEmpty) {
                 model.WorkingDayAndTime?.append(dic)
                }
            }
        }
        
        
        //let dic = model.WorkingDayAndTime?[indexPath.row]
    }
    
    
    private func getAllPublicHoliday() {
        if let hd = model.PublicHoliday {
            for hldy in hd {
                if let holiday = hldy["Status"] as? Bool  {
                    if holiday {
                        arrPublicDays.append(hldy)
                    }
                }
            }
        }
    }
    
    private func setOwerShipType(number: String) -> String {
        
        if let allOwenerList = model.OwrshipTypes {
            for owerShip in allOwenerList {
                let ownerNumber = owerShip["No"] as? String ?? ""
                if ownerNumber == String(number) {
                    var str = ""
                    if ok_default_language == "my" {
                       str = owerShip["Bvalue"] as? String ?? ""
                        return str
                    }else if ok_default_language == "en"{
                       str = owerShip["Value"] as? String ?? ""
                        return str
                    }else {
                        str = owerShip["BvalueUnicode"] as? String ?? ""
                        return str
                    }
                }
            }
        }else {
            return "Select Ownership type".localized
        }
        return "Select Ownership type".localized
    }
    
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: "Zawgyi-One", size: 16)
        lblMarque.text = "Business Details".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.init(hex: "#1C2C99")
        self.navigationItem.titleView = lblMarque
    }
    
    func reloadRows(index: Int, withSeciton sec: Int) {
        UIView.setAnimationsEnabled(false)
        self.tbUpdateProfile.beginUpdates()
        let indexPosition = IndexPath(row: index, section: sec)
        self.tbUpdateProfile.reloadRows(at: [indexPosition], with: .none)
        self.tbUpdateProfile.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
    func reloadSection(index: Int) {
        UIView.setAnimationsEnabled(false)
        let indexSet: IndexSet = [index]
        self.tbUpdateProfile.beginUpdates()
        self.tbUpdateProfile.reloadSections(indexSet, with: .automatic)
        self.tbUpdateProfile.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
    @IBAction func onClickEditAction(_ sender: Any) {
        btnEdit.isHidden = true
        bottomContraintsTV.constant = +50
        if kycBusinessName {
            isBusinessNameRowHidden = false
           reloadRows(index: 1, withSeciton: 0)
        }
        reloadRows(index: 0, withSeciton: 1)
        if kycBusinessCategory {
                reloadRows(index: 1, withSeciton: 1)
        }
        if kycBusinessCategory {
            reloadRows(index: 2, withSeciton: 1)
        }
        reloadRows(index: 3, withSeciton: 1)
        if kycBusinessRegistration {
          reloadRows(index: 4, withSeciton: 1)
        }
        reloadRows(index: 1, withSeciton: 2)
        reloadRows(index: 2, withSeciton: 2)
        reloadRows(index: 3, withSeciton: 2)
        reloadRows(index: 4, withSeciton: 2)
        reloadRows(index: 5, withSeciton: 2)
        reloadRows(index: 6, withSeciton: 2)
        reloadRows(index: 7, withSeciton: 2)
        reloadRows(index: 0, withSeciton: 3)
        reloadRows(index: 0, withSeciton: 4)
        reloadRows(index: 0, withSeciton: 5)
        reloadRows(index: 1, withSeciton: 5)
    }
    
    func getDateFormateSring(date: String)-> String {
        return dateF5.string(from: parseDate(strDate: date))
    }
    
    @IBAction func onClickBackAction(_ sender: Any) {
        if btnEdit.isHidden {
            alertViewObj.wrapAlert(title:"", body:"Clicking back without update will clear all entered details, Are you sure to exit?".localized, img: UIImage(named: "alert-icon"))
            alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
            })
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                self.navigationController?.popViewController(animated: true)
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    func getDateFormateSringForJoinDate(date: String)-> String {
        return dateF5.string(from: parseDateJaoinDate(strDate: date))
    }
    
    @IBAction func onClickUpdateAction(_ sender: Any) {
        self.checkRequiredFields(handler: {(alertMessage, success)in
            if success {
                self.UpdateBusinessProfile()
            }else {
                DispatchQueue.main.async {
                self.showAlert(alertTitle: "Fields missing".localized, alertBody: alertMessage, alertImage: UIImage(named: "error")!)
                }
            }
        })
    }
}

extension BusinessDetailsVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                return 110
            }else {
                if isBusinessNameRowHidden {
                    return 0
                }else {
                    return 70
                }
            }
        }else if indexPath.section == 1 {
            if indexPath.row == 4 {
                return 149
            }else {
                return 70
            }
        }else if indexPath.section == 2 {
            if indexPath.row == 4 {
                if !isVillageAvaible {
                   return 0
                }else {
                   return 70
                }
            }else if indexPath.row == 0 {
                return 0
            }else {
                 return 70
            }
        }else if indexPath.section == 3 || indexPath.section == 4 {
            if indexPath.row == 0{
                return 70
            }else {
                return 45
            }
        }else if indexPath.section == 6 {
            if indexPath.row == 0 {
                return 140
            }else {
                return 0
            }
        }else {
            return 70
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionCount
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        }else if section == 1 {
            return 5
        }else if section == 2 {
            return 8
        }else if section == 3 {
            if let days = model.WorkingDayAndTime {
                return 1 + days.count
            }else{
                return 1
            }
        }else if section == 4{
            return 1 + arrPublicDays.count
        }else if section == 5 {
            return 2
        }else if section == 6 {
            return 1
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessHeaderCell") as! BusinessHeaderCell
                cell.selectionStyle = .none
                let logo = model.LogoImages?["Image1"] as? String ?? ""
                if logo != "" {
                    let url = URL(string: logo.replacingOccurrences(of: " ", with: "%20").replacingOccurrences(of: "\\", with: ""))
                    cell.imgBusiness.sd_setImage(with: url, placeholderImage: nil)
                }
                cell.lblBusinessName.text = model.BusinessName
                cell.lblBusinessName.tag = 8
                cell.lblOutletName.text = model.BusinessOutlet
                cell.lblMembership.text =  model.UserMembership + " Member"
                cell.lblMemberSince.text = "Since".localized + " " + getDateFormateSringForJoinDate(date:  model.OkDollarJoiningDate)
                if model.BusinessApproveStatus == 1 {
                     cell.imgVerified.isHidden = false
                }else {
                    cell.imgVerified.isHidden = true
                }
                cell.isUserInteractionEnabled = false
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NameInputUPCell") as! NameInputUPCell
                cell.selectionStyle = .none
                cell.imgIcon.image = UIImage(named: "r_business_name")
                cell.lblStatus.text = "Business Name".localized
                cell.tfTitle.text = parabaik.zawgyi(toUni:businessName)
                cell.lblRequired.isHidden = false
                
                var font = UIFont(name: appFont, size: 18.0)
                if #available(iOS 13.0, *) {
                    font = UIFont.systemFont(ofSize: 18)
                }
                cell.tfTitle.font = font
                
                if ok_default_language == "my" {
                    cell.tfTitle.font = UIFont(name: appFont, size: 18)
                }else  if ok_default_language == "en"{
                    cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                }else {
                    if #available(iOS 13.0, *) {
                        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                          .font : UIFont(name: appFont, size: 18)]
                        cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "Enter Business Name", attributes:attributes as [NSAttributedString.Key : Any])
                    } else {
                        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                          .font : UIFont.systemFont(ofSize: 18)]
                        cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "Enter Business Name", attributes:attributes)
                        cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                    }
                }
                
                cell.tfTitle.placeholder = "Enter Business Name".localized
                cell.tfTitle.delegate = self
                cell.tfTitle.tag = 141
                cell.btnRightArrow.addTargetClosure(closure: {_ in
                     cell.tfTitle.text = ""
                     self.businessName = ""
                     cell.btnRightArrow.isHidden = true
                })
                cell.btnRightArrow.setImage(UIImage(named: "close"), for: .normal)
                if businessName.count > 0 && btnEdit.isHidden {
                    cell.btnRightArrow.isHidden = false
                }else {
                    cell.btnRightArrow.isHidden = true
                }
                if kycBusinessName && btnEdit.isHidden {
                    cell.isUserInteractionEnabled = true
                    cell.isHidden = false
                }else {
                    cell.isUserInteractionEnabled = false
                    cell.isHidden = true
                }
                
                if btnEdit.isHidden {
                   cell.lblRequired.textColor = UIColor.red
                }else {
                  cell.lblRequired.textColor = UIColor.darkGray
                }
                return cell
            }
        }else if indexPath.section == 1 {
            if indexPath.row == 0  {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NameActionUPCell") as! NameActionUPCell
                let dic = businessInfo[indexPath.row]
                if btnEdit.isHidden {
                    cell.lblRequired.textColor = UIColor.red
                }else {
                    cell.lblRequired.textColor = UIColor.darkGray
                }

                return NameActioncell(cell: cell, index: indexPath, data: dic as! Dictionary<String, String>)
            }else if indexPath.row == 1 || indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessNameCellUP") as! BusinessNameCellUP
                let dic = businessInfo[indexPath.row]
                if btnEdit.isHidden {
                    cell.lblRequired.textColor = UIColor.red
                }else {
                    cell.lblRequired.textColor = UIColor.darkGray
                }
                return BusinessNameCell(cell: cell, index: indexPath, data: dic as! Dictionary<String, String>)
            }else if indexPath.row == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NameInputUPCell") as! NameInputUPCell
                cell.selectionStyle = .none
                let dic = businessInfo[indexPath.row]
                cell.imgIcon.image = UIImage(named: dic["icon"] as? String ?? "")
                cell.lblStatus.text = dic["status"] as? String ?? ""
                cell.tfTitle.text = businessOutLet
                cell.lblRequired.isHidden = true
                
                if businessOutLet == ""{
                    cell.lblStatus.isHidden = true
                }else{
                    cell.lblStatus.isHidden = false
                }
                
                var font = UIFont(name: appFont, size: 18.0)
                if #available(iOS 13.0, *) {
                    font = UIFont.systemFont(ofSize: 18)
                }
                cell.tfTitle.font = font
                
                if ok_default_language == "my" {
                    cell.tfTitle.font = UIFont(name: appFont, size: 18)
                }else if ok_default_language == "en"{
                    cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                }else {
                    if #available(iOS 13.0, *) {
                        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                          .font : UIFont(name: appFont, size: 18)]
                        cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "Enter Branch Outlet", attributes:attributes as [NSAttributedString.Key : Any])
                    } else {
                        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                          .font : UIFont.systemFont(ofSize: 18)]
                        cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "Enter Branch Outlet", attributes:attributes)
                        cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                    }
                }
                
                cell.tfTitle.placeholder = "Enter Branch Outlet".localized
                cell.tfTitle.delegate = self
                cell.tfTitle.tag = 100
                cell.btnRightArrow.isHidden = true
                if btnEdit.isHidden && model.BusinessApproveStatus != 1 {
                    cell.isUserInteractionEnabled = true
                }else {
                    cell.isUserInteractionEnabled = false
                }
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NRCPassportUPCell") as! NRCPassportUPCell
                cell.selectionStyle = .none
                if btnEdit.isHidden {
                    cell.lblRequired.textColor = UIColor.red
                    cell.lblRequiredFront.textColor = UIColor.red
                     cell.lblRequiredBack.textColor = UIColor.red
                }else {
                    cell.lblRequired.textColor = UIColor.darkGray
                    cell.lblRequiredFront.textColor = UIColor.darkGray
                    cell.lblRequiredBack.textColor = UIColor.darkGray
                }
                return NRCPassportCell(cell: cell)
            }
        }else if indexPath.section == 2 {
            if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NameActionUPCell", for: indexPath) as! NameActionUPCell
                
                let dicData = AddressInfo[indexPath.row]
                if btnEdit.isHidden {
                    cell.lblRequired.textColor = UIColor.red
                }else {
                    cell.lblRequired.textColor = UIColor.darkGray
                }
                return NameActionWrapCell(index: indexPath, cell: cell, dic: dicData as! Dictionary<String, String>)
            }else if indexPath.row == 3 {
              let cell = tableView.dequeueReusableCell(withIdentifier: "NameInputUPCell", for: indexPath) as! NameInputUPCell
                cell.selectionStyle = .none
                let dic = AddressInfo[indexPath.row]
                cell.imgIcon.image = UIImage(named: dic["icon"] as! String)
                cell.tfTitle.delegate = self
                cell.lblStatus.text = dic["statusTitle"] as? String
                cell.btnRightArrow.isHidden = true
                cell.lblRequired.isHidden = true
                cell.isUserInteractionEnabled = false
                cell.lblStatus.textColor = .darkGray
                if model.City == ""{
                    cell.tfTitle.placeholder = "City".localized
                    cell.tfTitle.textColor = .lightGray
                    cell.lblStatus.isHidden = true
                }
                else{
                    cell.tfTitle.text = model.City
                    cell.tfTitle.textColor = .black
                    cell.lblStatus.isHidden = false
                }
                
                
                if btnEdit.isHidden {
                    cell.lblRequired.textColor = UIColor.red
                }else {
                    cell.lblRequired.textColor = UIColor.darkGray
                }
                return cell
            }else if indexPath.row == 4 || indexPath.row == 5 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NameInputUPCell", for: indexPath) as! NameInputUPCell
                
                if indexPath.row == 4 {
                    cell.tfTitle.tag = 400
                    cell.tfTitle.text = model.Village
                    cell.lblStatus.textColor = .darkGray
                    if model.Village == ""{
                        cell.lblStatus.isHidden = true
                    }else{
                        cell.lblStatus.isHidden = false
                    }
                    
                    var font = UIFont(name: appFont, size: 18.0)
                    if #available(iOS 13.0, *) {
                        font = UIFont.systemFont(ofSize: 18)
                    }
                    cell.tfTitle.font = font
                    
                    if ok_default_language == "my" {
                        cell.tfTitle.font = UIFont(name: appFont, size: 18)
                    }else if ok_default_language == "en"{
                        cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                    }else {
                        if #available(iOS 13.0, *) {
                            let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                              .font : UIFont(name: appFont, size: 18)]
                            cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "Enter Village Tract", attributes:attributes as [NSAttributedString.Key : Any])
                        } else {
                            let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                              .font : UIFont.systemFont(ofSize: 18)]
                            cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "Enter Village Tract", attributes:attributes)
                            cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                        }
                    }
                    cell.tfTitle.placeholder = "Enter Village Tract".localized
                    cell.tfTitle.addTarget(self, action: #selector(villageTextFieldEditing(_:)), for: .editingChanged)
                    cell.lblRequired.isHidden = true
                }else {
                    cell.tfTitle.tag = 500
                    cell.tfTitle.text = model.Street
                    cell.lblStatus.textColor = .darkGray
                    if model.Street == ""{
                        cell.lblStatus.isHidden = true
                    }else{
                        cell.lblStatus.isHidden = false
                    }
                    
                    var font = UIFont(name: appFont, size: 18.0)
                    if #available(iOS 13.0, *) {
                        font = UIFont.systemFont(ofSize: 18)
                    }
                    cell.tfTitle.font = font
                    
                    if ok_default_language == "my" {
                        cell.tfTitle.font = UIFont(name: appFont, size: 18)
                    }else if ok_default_language == "en"{
                        cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                    }else {
                        if #available(iOS 13.0, *) {
                            let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                              .font : UIFont(name: appFont, size: 18)]
                            cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "Enter street", attributes:attributes as [NSAttributedString.Key : Any])
                        } else {
                            let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                              .font : UIFont.systemFont(ofSize: 18)]
                            cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "Enter street", attributes:attributes)
                            cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                        }
                    }
                    cell.tfTitle.placeholder = "Enter street".localized
                    cell.tfTitle.addTarget(self, action: #selector(streetTextFieldEditing(_:)), for: .editingChanged)
                    cell.lblRequired.isHidden = false
                }
                cell.selectionStyle = .none
                let dic = AddressInfo[indexPath.row]
                cell.imgIcon.image = UIImage(named: dic["icon"] as! String)
                cell.tfTitle.delegate = self
                cell.lblStatus.text = dic["statusTitle"] as? String
                if indexPath.row == 4 && !isVillageAvaible {
                    cell.isHidden = true
                }else {
                    cell.isHidden = false
                }
                cell.btnRightArrow.isHidden = true
                if btnEdit.isHidden {
                    cell.lblRequired.textColor = UIColor.red
                    cell.isUserInteractionEnabled = true
                }else {
                    cell.lblRequired.textColor = UIColor.darkGray
                    cell.isUserInteractionEnabled = false
                }

                return cell
            }else if indexPath.row == 6 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddressUPCell", for: indexPath) as! AddressUPCell
                cell.selectionStyle = .none
                cell.tfHouse.tag = 600
                cell.tfHouse.text = model.HouseNumber
                cell.tfHouse.delegate = self
                cell.tfFloor.tag = 700
                cell.tfFloor.text = model.FloorNumber
                cell.tfFloor.delegate = self
                cell.tfRoom.tag = 800
                cell.tfRoom.text = model.RoomNumber
                cell.tfRoom.delegate = self
                if btnEdit.isHidden {
                    cell.isUserInteractionEnabled = true
                }else {
                    cell.isUserInteractionEnabled = false
                }
                 cell.lblRequired.isHidden = true
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NameInputUPCell", for: indexPath) as! NameInputUPCell
                cell.selectionStyle = .none
                let dic = AddressInfo[indexPath.row]
                cell.tfTitle.tag = 900
                cell.imgIcon.image = UIImage(named: dic["icon"] as! String)
                cell.tfTitle.text = model.HouseBlockNumber
                cell.lblStatus.textColor = .darkGray
                
                if model.HouseBlockNumber == ""{
                    cell.lblStatus.isHidden = true
                }
                else{
                    cell.lblStatus.isHidden = false
                }
                
                var font = UIFont(name: appFont, size: 18.0)
                if #available(iOS 13.0, *) {
                    font = UIFont.systemFont(ofSize: 18)
                }
                cell.tfTitle.font = font
                
                if ok_default_language == "my" {
                    cell.tfTitle.font = UIFont(name: appFont, size: 18)
                }else if ok_default_language == "en"{
                    cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                }else {
                    if #available(iOS 13.0, *) {
                        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                          .font : UIFont(name: appFont, size: 18)]
                        cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "Housing /Industrial  Zone Name", attributes:attributes as [NSAttributedString.Key : Any])
                    } else {
                        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                          .font : UIFont.systemFont(ofSize: 18)]
                        cell.tfTitle.attributedPlaceholder = NSAttributedString(string: "Housing /Industrial  Zone Name", attributes:attributes)
                        cell.tfTitle.font = UIFont.systemFont(ofSize: 18)
                    }
                    
                }
                
                cell.tfTitle.placeholder = "Housing /Industrial  Zone Name".localized
                cell.tfTitle.delegate = self
                cell.lblStatus.text = dic["statusTitle"] as? String
                cell.btnRightArrow.isHidden = true
                cell.lblRequired.isHidden = true
                if btnEdit.isHidden {
                    cell.isUserInteractionEnabled = true
                }else {
                    cell.isUserInteractionEnabled = false
                }
                return cell
            }
        }else if indexPath.section == 3 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessHolidayUPCell") as! BusinessHolidayUPCell
                let dic = daytimeInfo[indexPath.row]
                return DaysTimeCell(cell: cell, index: indexPath, data: dic as! Dictionary<String, String>)
            }else  {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SelectDayTimeCell") as! SelectDayTimeCell
                cell.btnSelection.isHidden = true
                cell.selectionStyle = .none
                if let dic = model.WorkingDayAndTime?[indexPath.row - 1] {
                    let days = self.dayFromDayCode(code: dic["DayCode"] as? String ?? "")
                    cell.lblWeekDays.text = days
                    var opentime = ""
                    var closeTime = ""
                    
                    opentime = dic["OpenTime"] as? String ?? ""
                    closeTime = dic["CloseTime"] as? String ?? ""
                    cell.lblTimeOpenClose.text = opentime.trimmingCharacters(in: .whitespacesAndNewlines) + " - " + closeTime.trimmingCharacters(in: .whitespacesAndNewlines)
                }
                return cell
            }
        }else if indexPath.section == 4 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessHolidayUPCell") as! BusinessHolidayUPCell
                let dic = holidayInfo[0]
                return HolidayCell(cell: cell, index: indexPath, data: dic as! Dictionary<String, String>)
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CheckBoxHeaderCell") as! CheckBoxHeaderCell
                cell.selectionStyle = .none
                let dic = arrPublicDays[indexPath.row - 1]
                if ok_default_language == "my" {
                      cell.lblSelectedDays.text = dic["BHolidayName"] as? String ?? ""
                }else if ok_default_language == "en"{
                     cell.lblSelectedDays.text = dic["HolidayName"] as? String ?? ""
                }else {
                    //cell.lblSelectedDays.font = UIFont.systemFont(ofSize: 15.0)
                    cell.lblSelectedDays.text = dic["BHolidayNameUnicode"] as? String ?? ""
                }
                return cell
            }
        }else if indexPath.section == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NameActionUPCell") as! NameActionUPCell
            let dic = addtionalInfo[indexPath.row]
            return AdditionalInfoCell(cell: cell, index: indexPath, data: dic as! Dictionary<String, String>)
        }else if indexPath.section == 6 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "StatusUPCell") as! StatusUPCell
                cell.selectionStyle = .none
                
                let score : Double = Double(myProfileStatus)
                let total : Double = Double(10)
                let percent : Double = Double((score / total) * 100)
                myProfileStatusInPercent = Int(percent)
                
                cell.lblSecondLine.text = "100 % Business profile \n update is required to available complete OK$ service".localized
            cell.lblFillStatus.text = String(myProfileStatus) + "/" + "10" + " " + "( " + "\(String(format: "%.0f%", percent) + "% ")" + ")"
                
                cell.lblStatusPercentage.text = "0%" //String(format: "%.0f%", percent) + "% "
                cell.progess.progress = Float(percent/100)
                cell.selectionStyle = .none
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "StatusButtonUPCell") as! StatusButtonUPCell
                if btnEdit.isHidden {
                    cell.isHidden = false
                }else {
                    cell.isHidden = true
                }
                cell.btnUpdate.addTargetClosure(closure: {_ in
                    self.checkRequiredFields(handler: {(alertMessage, success)in
                        if success {
                            self.UpdateBusinessProfile()
                        }else {
                            
                            self.showAlert(alertTitle: "Fields missing".localized, alertBody: alertMessage, alertImage: UIImage(named: "error")!)
                        }
                    })
                })
                return cell
            }
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NRCPassportUPCell") as! NRCPassportUPCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath)
        cell!.contentView.backgroundColor = UIColor.white
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath)
        cell!.contentView.backgroundColor = UIColor.white
    }
    
    private func NameActioncell(cell: NameActionUPCell, index: IndexPath, data : Dictionary<String,String>)-> NameActionUPCell {
        cell.selectionStyle = .none
        cell.imgIcon.image = UIImage(named: data["icon"] ?? "")
        cell.lblStatus.text = data["status"] ?? ""
        cell.lblTitle.text = data["title"] ?? ""
        cell.lblTitle.isUserInteractionEnabled = true
        cell.lblRequired.isHidden = false
        cell.imgRightArrow.isUserInteractionEnabled = true
        cell.lblStatus.textColor = .darkGray
        
        if cell.lblTitle.text == "Select Ownership type"{
            cell.lblStatus.isHidden = true
            cell.lblTitle.textColor = .darkGray
        }else{
            cell.lblStatus.isHidden = false
            cell.lblTitle.textColor = .black
        }
        
        let tapAction = UITapGestureRecognizer(target: self, action:#selector(onClickAction(_:)))
        let tapAction1 = UITapGestureRecognizer(target: self, action:#selector(onClickAction(_:)))
        if index.row == 0 {
            cell.imgRightArrow.isHidden = false
            tapAction.name = "OwnerShip"
            tapAction1.name = "OwnerShip"
            cell.lblTitle?.addGestureRecognizer(tapAction)
            cell.imgRightArrow.addGestureRecognizer(tapAction1)
            if btnEdit.isHidden && model.BusinessApproveStatus != 1 {
                cell.isUserInteractionEnabled = true
            }else {
                cell.isUserInteractionEnabled = false
            }
        }
        return cell
    }
    private func BusinessNameCell(cell: BusinessNameCellUP, index: IndexPath, data : Dictionary<String,String>)-> BusinessNameCellUP {
        cell.selectionStyle = .none
        let tapAction = UITapGestureRecognizer(target: self, action:#selector(onClickAction(_:)))
        let tapAction1 = UITapGestureRecognizer(target: self, action:#selector(onClickAction(_:)))
        cell.lblStatus.text = data["status"] ?? ""
        cell.lblStatus.textColor = .darkGray
        cell.lblTitle.isUserInteractionEnabled = true
        cell.lblRequired.isHidden = false
        if index.row == 1 {
             cell.imgRightArrow.isHidden = false
             cell.imgRightArrow.isUserInteractionEnabled = true
             cell.lblCatImage.isHidden = true
             cell.imgIcon.isHidden = false
            if categoryimage == "" {
                   cell.imgIcon.image = UIImage(named: "categories")
            }else {
                cell.imgIcon.image = UIImage(named: categoryimage)
            }
          
             tapAction.name = "Category"
            tapAction1.name = "Category"
            let cat = data["title"] ?? ""
            if cat == "" {
                cell.lblStatus.isHidden = true
                cell.lblTitle.text = "Select Business Category".localized
                cell.lblTitle.textColor = .darkGray
            }else {
                cell.lblStatus.isHidden = false
                cell.lblTitle.text = cat
                cell.lblTitle.textColor = .black
            }
            cell.lblTitle?.addGestureRecognizer(tapAction)
            cell.imgRightArrow.addGestureRecognizer(tapAction1)
            if kycBusinessCategory && btnEdit.isHidden {
                cell.isUserInteractionEnabled = true
            }else {
                cell.isUserInteractionEnabled = false
            }
        }else {
            if subcategoryimage == "" {
                cell.imgIcon.image =  UIImage(named: "categories")
                cell.imgIcon.isHidden = false
                cell.lblCatImage.isHidden =  true
            }else {
                cell.imgIcon.isHidden = true
                cell.lblCatImage.isHidden = false
                cell.lblCatImage.text = subcategoryimage
            }
            cell.imgRightArrow.isUserInteractionEnabled = true
            cell.imgRightArrow.isHidden = false
            tapAction.name = "SubCategory"
            tapAction1.name = "SubCategory"
            let subCat = data["title"] ?? ""
            if subCat == "" {
                cell.lblStatus.isHidden = true
                cell.lblTitle.text = "Select Business Sub-Category".localized
                cell.lblTitle.textColor = .darkGray
            }else {
                cell.lblStatus.isHidden = false
                cell.lblTitle.text = subCat
                cell.lblTitle.textColor = .black
            }
            cell.imgRightArrow.addGestureRecognizer(tapAction1)
            cell.lblTitle?.addGestureRecognizer(tapAction)
            if kycBusinessSubCategory &&  btnEdit.isHidden {
                cell.isUserInteractionEnabled = true
            }else {
                cell.isUserInteractionEnabled = false
            }
        }
        return cell
    }
    
    
    private func NRCPassportCell(cell: NRCPassportUPCell) -> NRCPassportUPCell  {
        cell.selectionStyle = .none
        let image1 = model.BusinessRegistration1.replacingOccurrences(of: " ", with: "%20").replacingOccurrences(of: "\\", with: "")
        let image2 = model.BusinessRegistration2.replacingOccurrences(of: " ", with: "%20").replacingOccurrences(of: "\\", with: "")
        cell.tfNrcPassportNumber.text = "Attach Business Registration / License".localized
        cell.tfNrcPassportNumber.isUserInteractionEnabled = false
        cell.tfNrcPassportNumber.font = UIFont(name: appFont, size: 13)
        cell.imgIcon.image = UIImage(named: "attachBig")
        cell.lblStatus.text = ""
        cell.lblbFront.text = "Front Side".localized
        cell.lblRear.text = "Back Side".localized
        
        if image1 == "" {
           cell.imgFront.isHidden = true
        }else {
            cell.imgFront.isHidden = false
            let urlFront = URL(string: image1)
            cell.imgFront.sd_setImage(with: urlFront, placeholderImage: nil)
            let urlRear = URL(string: image2)
            cell.imgRear.sd_setImage(with: urlRear, placeholderImage: nil)
        }
        if image2 == "" {
            cell.imgFront.isHidden = true
        }else {
            cell.imgFront.isHidden = false
            let urlFront = URL(string: image1)
            cell.imgFront.sd_setImage(with: urlFront, placeholderImage: nil)
            let urlRear = URL(string: image2)
            cell.imgRear.sd_setImage(with: urlRear, placeholderImage: nil)
        }
        
     
        cell.btnFrontAction.addTarget(self, action: #selector(frontPhoto(_:)), for: .touchUpInside)
        cell.btnRearAction.addTarget(self, action: #selector(rearPhoto(_:)), for: .touchUpInside)
        cell.btnInformation.addTarget(self, action: #selector(nrcPassportInformaiton(_:)), for: .touchUpInside)
        if btnEdit.isHidden && kycBusinessRegistration {
            cell.btnFrontAction.isUserInteractionEnabled = true
            cell.btnRearAction.isUserInteractionEnabled = true
        }else {
            cell.btnFrontAction.isUserInteractionEnabled = false
            cell.btnRearAction.isUserInteractionEnabled = false
        }
        return cell
    }
    
    private func NameActionWrapCell(index: IndexPath, cell: NameActionUPCell, dic: Dictionary<String,String>) -> NameActionUPCell {
        cell.selectionStyle = .none
        cell.lblStatus.text = dic["statusTitle"] ?? ""
        cell.imgIcon.image = UIImage(named: dic["icon"] ?? "")
        cell.lblTitle.text = dic["title"] ?? ""
        cell.lblStatus.textColor = .darkGray
        
        if  index.row == 1 || index.row == 2 {
            cell.imgRightArrow.isHidden = false
            cell.lblRequired.isHidden = false
        }else {
            cell.imgRightArrow.isHidden = true
            cell.lblRequired.isHidden = true
            cell.isHidden = true
        }
        
        if index.row == 0 {
           model.AddressType = "Business".localized
           cell.lblTitle.text = "Business".localized
        }else if index.row == 1 {
            if dic["title"] ?? "" == "" {
                 cell.lblTitle.text = "Select State / Division".localized
                 cell.lblTitle.textColor = .lightGray
                 cell.lblStatus.isHidden = true
            }
            else{
                cell.lblStatus.isHidden = false
                cell.lblTitle.textColor = .black
            }
        }else if index.row == 2 {
            if dic["title"] ?? "" == "" {
                cell.lblTitle.text = "Select Township".localized
                cell.lblTitle.textColor = .lightGray
                cell.lblStatus.isHidden = true
            }else{
                cell.lblStatus.isHidden = false
                cell.lblTitle.textColor = .black
            }
        }
        cell.imgRightArrow.isUserInteractionEnabled = true
        cell.lblTitle.isUserInteractionEnabled = true
        let tapAction = UITapGestureRecognizer(target: self, action:#selector(onClickAction(_:)))
         let tapAction1 = UITapGestureRecognizer(target: self, action:#selector(onClickAction(_:)))
        if index.row == 0 {
            tapAction.name = "Address Type"
            tapAction1.name = "Address Type"
        }else if index.row == 1 {
            tapAction.name = "Division / State"
            tapAction1.name = "Division / State"
        }else if index.row == 2 {
            tapAction.name = "Township"
            tapAction1.name = "Township"
        }
        cell.lblTitle?.addGestureRecognizer(tapAction)
        cell.imgRightArrow.addGestureRecognizer(tapAction1)
        if btnEdit.isHidden {
            cell.isUserInteractionEnabled = true
        }else {
            cell.isUserInteractionEnabled = false
        }
        
        return cell
    }
    private func nameWrapCell(index: IndexPath, cell: NameUPCell, dic: Dictionary<String,String>) -> NameUPCell {
        cell.selectionStyle = .none
        cell.imgIcon.image = UIImage(named: dic["icon"] ?? "")
        cell.lblStaus.text = dic["statusTitle"] ?? ""
        cell.lblRightTitle.text = ""
        if index.row == 0 && index.section == 1 {
            if dic["title"] == "0" {
                cell.lblTitle.text = "Merchant".localized
            }else {
                cell.lblTitle.text = "Personal".localized
            }
        }else if index.row == 1 && index.section == 1 {
            let (mobileNumber,CountryCode) = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: PersonalProfileModelUP.shared.OkAccNumber)
            cell.lblTitle.text = "(" + CountryCode + ")" + mobileNumber
            let countryData = identifyCountry(withPhoneNumber: CountryCode)
            if let safeImage = UIImage(named: countryData.countryFlag) {
                DispatchQueue.main.async {
                    cell.imgIcon.contentMode = .scaleAspectFit
                    cell.imgIcon.image = safeImage
                }
            }
        }else if index.row == 2 && index.section == 1 {
            cell.lblTitle.text = getDateFormateSringForJoinDate(date: dic["title"] ?? "")
            cell.lblRightTitle.text = getYearDifferace(date: dic["title"] ?? "")
        }else {
            cell.lblTitle.text = dic["title"] ?? ""
        }
        return cell
    }
    private func DaysTimeCell(cell: BusinessHolidayUPCell, index: IndexPath, data : Dictionary<String,String>)-> BusinessHolidayUPCell {
        cell.selectionStyle = .none
        cell.imgIcon.image = UIImage(named: data["icon"] ?? "")
        cell.lblStatus.text = data["status"] ?? ""
        cell.lblTitle.text = data["title"] ?? ""
        cell.lblNotSet.text = ""
        cell.lblRequired.isHidden = true
        cell.lblTitle.isUserInteractionEnabled = true
        cell.imgRightArrow.isUserInteractionEnabled = true
        cell.imgRightArrow.isHidden = false
        cell.imgRightArrow.image = UIImage(named: "down_arrow_small")
        let tapAction1 = UITapGestureRecognizer(target: self, action:#selector(onClickAction(_:)))
        tapAction1.name = "DayTime"
        cell.imgRightArrow?.addGestureRecognizer(tapAction1)
        
        let tapAction = UITapGestureRecognizer(target: self, action:#selector(onClickAction(_:)))
        tapAction.name = "DayTime"
        cell.lblTitle?.addGestureRecognizer(tapAction)
        
        if btnEdit.isHidden {
            cell.isUserInteractionEnabled = true
        }else {
            cell.isUserInteractionEnabled = false
        }
        return cell
    }
    
    private func HolidayCell(cell: BusinessHolidayUPCell, index: IndexPath, data : Dictionary<String,String>)-> BusinessHolidayUPCell {
        cell.selectionStyle = .none
        cell.imgIcon.image = UIImage(named: data["icon"] ?? "")
        cell.lblStatus.text = data["status"] ?? ""
        cell.lblTitle.text = data["title"] ?? ""
        if arrPublicDays.count == 0 {
            cell.lblNotSet.text = "(Not Set)".localized
        }else {
            cell.lblNotSet.text = ""
        }
        cell.lblRequired.isHidden = true
        cell.lblTitle.isUserInteractionEnabled = true
        cell.imgRightArrow.isUserInteractionEnabled = true
        cell.imgRightArrow.isHidden = false
        cell.imgRightArrow.image = UIImage(named: "down_arrow_small")
        let tapAction1 = UITapGestureRecognizer(target: self, action:#selector(onClickAction(_:)))
        tapAction1.name = "Holiday"
        cell.imgRightArrow?.addGestureRecognizer(tapAction1)
        
        let tapAction = UITapGestureRecognizer(target: self, action:#selector(onClickAction(_:)))
        tapAction.name = "Holiday"
        cell.lblTitle?.addGestureRecognizer(tapAction)
        
        if btnEdit.isHidden {
            cell.isUserInteractionEnabled = true
        }else {
            cell.isUserInteractionEnabled = false
        }
        return cell
    }
    private func AdditionalInfoCell(cell: NameActionUPCell, index: IndexPath, data : Dictionary<String,String>)-> NameActionUPCell {
        cell.selectionStyle = .none
        cell.imgIcon.image = UIImage(named: data["icon"] ?? "")
        cell.lblStatus.text = data["status"] ?? ""
        cell.lblTitle.text = data["title"] ?? ""
        cell.imgRightArrow.isUserInteractionEnabled = true
        cell.lblTitle.isUserInteractionEnabled = true
        let tapAction = UITapGestureRecognizer(target: self, action:#selector(onClickAction(_:)))
        let tapAction1 = UITapGestureRecognizer(target: self, action:#selector(onClickAction(_:)))
        if index.row == 0 {
            tapAction.name = "ContactDetails"
            tapAction1.name = "ContactDetails"
            cell.lblRequired.isHidden = true
        }else {
            tapAction.name = "BusinessLogo"
            tapAction1.name = "BusinessLogo"
            cell.lblRequired.isHidden = false
        }
        cell.isUserInteractionEnabled = true
        cell.lblTitle?.addGestureRecognizer(tapAction)
        cell.imgRightArrow.addGestureRecognizer(tapAction1)
        if btnEdit.isHidden {
            cell.isUserInteractionEnabled = true
        }else {
            cell.isUserInteractionEnabled = false
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 || section == 2 || section == 3 || section == 4 || section == 5 {
            return 35
        }else {
            return 0
        }
    }
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var title = ""
        if section == 0 {
            if UserModel.shared.agentType == .merchant {
                title = "Merchant Information".localized
            }else if UserModel.shared.agentType == .advancemerchant {
                  title = "Advance Merchant Information".localized
            }else if UserModel.shared.agentType == .agent {
                title = "Agent Information".localized
            }
            
            return createRequiredFieldLabel(headerTitle: title, reqTitle: "Required fields*".localized)
        }else if section == 2 {
            return createRequiredFieldLabel(headerTitle: "Business Address Details".localized, reqTitle: "")
        }else if section == 3 {
            return createRequiredFieldLabel(headerTitle: "Business Working Days & Time".localized, reqTitle: "".localized)
        }else if section == 4 {
            return createRequiredFieldLabel(headerTitle: "Business Holidays".localized, reqTitle: "".localized)
        }else if section == 5 {
            return createRequiredFieldLabel(headerTitle: "Business Additional Information".localized, reqTitle: "".localized)
        }else if section == 6 {
            return createRequiredFieldLabel(headerTitle: "Business Profile Status".localized, reqTitle: "".localized)
        }else {
            let view = UIView(frame: CGRect(x: 0, y: 0, width:0, height: 0))
            return view
        }
    }
    
    private func createRequiredFieldLabel(headerTitle:String, reqTitle: String) ->UIView? {
        let view1 = UIView(frame: CGRect(x: 5, y: 0, width:self.view.frame.width - 10, height: 35))
        view1.backgroundColor = hexStringToUIColor(hex: "#E0E0E0")
        let lblHeader = UILabel(frame: CGRect(x: 10, y: 0, width: (sizeOfString(myString: headerTitle)?.width)!, height:35))
        lblHeader.text = headerTitle
        lblHeader.textAlignment = .left
        lblHeader.font = UIFont(name: "Zawgyi-One", size: 14)
        lblHeader.backgroundColor = UIColor.clear
        lblHeader.textColor = UIColor.black
        
        let reqLabel = UILabel(frame: CGRect(x: self.view.frame.width - (sizeOfString(myString: reqTitle)?.width)! - 10, y: 0, width: (sizeOfString(myString: reqTitle)?.width)!, height:35))
        reqLabel.text = reqTitle
        reqLabel.textAlignment = .right
        reqLabel.font = UIFont(name: "Zawgyi-One", size: 14)
        reqLabel.backgroundColor = UIColor.clear
        reqLabel.textColor = UIColor.red
        view1.addSubview(lblHeader)
        view1.addSubview(reqLabel)
        return view1
    }
    
    private func sizeOfString(myString : String) ->CGSize? {
        let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: "Zawgyi-One", size: 16) ?? UIFont.systemFont(ofSize: 16.0) ])
        return stringSize
    }
    
}

extension BusinessDetailsVC: PAImagePreviewVCDelegate {
    @objc func onClickAction(_ reconiger: UITapGestureRecognizer) {
        if reconiger.name == "OwnerShip" {
            let address = self.storyboard?.instantiateViewController(withIdentifier: "AddressTypeUP") as! AddressTypeUP
            address.listArray = model.OwrshipTypes
            address.viewController = "business"
            address.setMarqueLabelInNavigation(title: "Select Ownership Type".localized)
            address.delegate = self
            self.navigationController?.pushViewController(address, animated: true)
        }else if reconiger.name == "Category" {
            let sb = UIStoryboard(name: "Registration", bundle: nil)
            let businessCategoryVC  = sb.instantiateViewController(withIdentifier: "BusinessCategoryVC") as! BusinessCategoryVC
            businessCategoryVC.bCatDelegate = self
            self.navigationController?.pushViewController(businessCategoryVC, animated: true)
        }else if reconiger.name == "SubCategory" {
            let sb = UIStoryboard(name: "Registration", bundle: nil)
            let businessSubCategoryVC  = sb.instantiateViewController(withIdentifier: "BusinessSubCategoryVC") as! BusinessSubCategoryVC
            businessSubCategoryVC.businessSubCategoryVCDelegate = self
            businessSubCategoryVC.dicName = categoryDetail
            self.navigationController?.pushViewController(businessSubCategoryVC, animated: true)
        }else if reconiger.name == "DayTime" {
            let dayTime = self.storyboard?.instantiateViewController(withIdentifier: "BDServiceDayTime") as! BDServiceDayTime
            dayTime.seletedDays = model.WorkingDayAndTime
            dayTime.delegate = self
            self.navigationController?.pushViewController(dayTime, animated: true)
        }else if reconiger.name == "Holiday" {
            let holidays = self.storyboard?.instantiateViewController(withIdentifier: "BDPublicHolidaysVC") as! BDPublicHolidaysVC
            holidays.arrOldSelection = model.PublicHoliday
            holidays.delegate = self
            self.navigationController?.pushViewController(holidays, animated: true)
        }else if reconiger.name == "ContactDetails" {
            if btnEdit.isHidden {
            let contact = self.storyboard?.instantiateViewController(withIdentifier: "BDContactDetailsVC") as! BDContactDetailsVC
            contact.delegate = self
            contact.contactNumberList = model.ContactNumber
            contact.youTubeLinksList = model.YouTubeLinks
            contact.otherList = [model.Email,model.Website,model.FacebookPage,model.Instagram]
            contact.updateStatus = btnEdit.isHidden
            self.navigationController?.pushViewController(contact, animated: true)
            }
        }else if reconiger.name == "BusinessLogo" {
            if btnEdit.isHidden {
            let logo = self.storyboard?.instantiateViewController(withIdentifier: "BDAttachImagesVC") as! BDAttachImagesVC
            logo.imageDic = model.LogoImages
            logo.logoStatus = model.LogoApproveStatus
            logo.isEditable = !btnEdit.isHidden
            logo.delegate = self
            self.navigationController?.pushViewController(logo, animated: true)
            }
        }else if reconiger.name == "Address Type" {
            let address = self.storyboard?.instantiateViewController(withIdentifier: "AddressTypeUP") as! AddressTypeUP
            address.viewController = "personal"
            address.list = ["Home".localized,"Business".localized,"Other".localized]
            address.setMarqueLabelInNavigation(title: "Address Type")
           // self.navigationController?.pushViewController(address, animated: true)
        }else if reconiger.name == "Division / State" {
            let sb = UIStoryboard.init(name: "Registration", bundle: nil)
            let stateDivisionVC  = sb.instantiateViewController(withIdentifier: "StateDivisionVC") as! StateDivisionVC
            stateDivisionVC.stateDivisonVCDelegate = self
            self.navigationController?.pushViewController(stateDivisionVC, animated: true)
        }else if reconiger.name == "Township" {
            if locationDetails != nil {
                let sb = UIStoryboard.init(name: "Registration", bundle: nil)
                let addressTownshiopVC  = sb.instantiateViewController(withIdentifier: "AddressTownshiopVC") as! AddressTownshiopVC
                addressTownshiopVC.addressTownshiopVCDelegate = self
                addressTownshiopVC.selectedDivState = locationDetails
                self.navigationController?.pushViewController(addressTownshiopVC, animated: true)
            }else {
                showAlert(alertTitle: "", alertBody: "Select State / Division".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
            }
        }
    }
    
    @objc func frontPhoto(_ sender: UIButton) {
        let imgEditVC = UIStoryboard(name: "MyProfile", bundle: Bundle.main).instantiateViewController(withIdentifier: "PAImagePreviewVC")  as! PAImagePreviewVC
        imgEditVC.imagePreviewVCDelegate = self
        imgEditVC.strSubTitle = "Attach Front Registration".localized.localized
        imgEditVC.fullURL = self.model.BusinessRegistration1
        imgEditVC.strTitle = "Attach Business".localized
        self.navigationController?.pushViewController(imgEditVC, animated: true)
    }
    
    @objc func rearPhoto(_ sender: UIButton) {
        let imgEditVC = UIStoryboard(name: "MyProfile", bundle: Bundle.main).instantiateViewController(withIdentifier: "PAImagePreviewVC")  as! PAImagePreviewVC
        imgEditVC.imagePreviewVCDelegate = self
        imgEditVC.strSubTitle = "Attach Back Registration".localized
        imgEditVC.fullURL = self.model.BusinessRegistration2
        imgEditVC.strTitle = "Attach Business".localized
        self.navigationController?.pushViewController(imgEditVC, animated: true)
    }
 
    func ImagePreviewUrl(imageName : String, imageURL: String) {
        let image = imageURL.replacingOccurrences(of: " ", with: "%20")
        switch imageName {
        case "Attach Front Registration".localized :
            self.model.BusinessRegistration1 = image
        case "Attach Back Registration".localized:
           self.model.BusinessRegistration2 = image
        default :
            println_debug("Hello!")
        }
        if let cell = tbUpdateProfile.cellForRow(at: IndexPath(row: 4, section: 1)) as? NRCPassportUPCell {
            DispatchQueue.main.async {
                if self.model.BusinessRegistration1 != "" {
                    cell.imgFront.isHidden = false
                    cell.imgFront.sd_setImage(with: URL(string: self.model.BusinessRegistration1), placeholderImage: nil)
                }
                if self.model.BusinessRegistration2 != "" {
                    cell.imgFront.isHidden = false
                    cell.imgRear.sd_setImage(with: URL(string: self.model.BusinessRegistration2), placeholderImage: nil)
                }
            }
        }
        updateCountStatus()

    }
    
    
    
    @objc func nrcPassportInformaiton(_ sender: UIButton) {
        alertViewObj.wrapAlert(title:"Information".localized, body: "Your Business License is required to verify your account as per rules & Regulations of Central Bank of Myanmar".localized, img: UIImage(named: "infoQR"))
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
}
    
    extension BusinessDetailsVC : StateDivisionVCDelegate,AddressTownshiopVCDelegate {
        
        func setDivisionStateName(SelectedDic: LocationDetail) {
            locationDetails = SelectedDic
            let dic = AddressInfo[1]
            if ok_default_language == "my" {
                dic["title"] = SelectedDic.stateOrDivitionNameMy
            }else if ok_default_language == "en"{
                dic["title"] = SelectedDic.stateOrDivitionNameEn
            }else {
                dic["title"] = SelectedDic.stateOrDivitionNameUni
            }
            model.Division = SelectedDic.stateOrDivitionCode
            reloadRows(index: 1, withSeciton: 2)
            
            let dic2 = AddressInfo[2]
            model.Township = ""
            dic2["title"] = "Select Township".localized
            reloadRows(index: 2, withSeciton: 2)
            
            let dic3 = AddressInfo[3]
            model.City = ""
            dic3["title"] = "City Name".localized
            reloadRows(index: 3, withSeciton: 2)
               myProfileStatus = updateProfileStatusCount()
        }
        
        func setTownshipCityNameFromDivison(Dic: TownShipDetailForAddress) {
            self.setTownShip(township: Dic)
        }
        
        func setTownshipCityName(cityDic: TownShipDetailForAddress) {
            self.setTownShip(township: cityDic)
        }
        
        private func setTownShip(township: TownShipDetailForAddress) {
            //township
            let dic = AddressInfo[2]
            if ok_default_language == "my" {
                dic["title"] = township.townShipNameMY
            }else if ok_default_language == "en"{
                dic["title"] = township.townShipNameEN
            }else {
                dic["title"] = township.townShipNameUni
            }
            model.Township = township.townShipCode
            myProfileStatus = updateProfileStatusCount()
            reloadRows(index: 2, withSeciton: 2)
            
            //City
            if ok_default_language == "my" {
                if township.GroupName == "" {
                    model.City = township.cityNameMY
                }else {
                    if township.isDefaultCity == "1" {
                        model.City = township.DefaultCityNameMY
                    }else{
                        model.City = township.cityNameMY
                    }
                }
            }else if ok_default_language == "en"{
                if township.GroupName == "" {
                    model.City = township.cityNameEN
                }else {
                    if township.isDefaultCity == "1" {
                        model.City = township.DefaultCityNameEN
                    }else{
                        model.City = township.cityNameEN
                    }
                }
            }else {
                if township.GroupName == "" {
                    model.City = township.cityNameUni
                }else {
                    if township.isDefaultCity == "1" {
                        model.City = township.DefaultCityNameUni
                    }else{
                        model.City = township.cityNameUni
                    }
                }
            }
            
            let dic1 = AddressInfo[3]
            dic1["title"] = model.City
            reloadRows(index: 3, withSeciton: 2)
            
            isVillageAvaible = true
            if township.GroupName.contains(find: "YCDC") || township.GroupName.contains(find: "MCDC") {
                isVillageAvaible = false
            }
            
            if township.cityNameEN == "Amarapura" || township.cityNameEN == "Patheingyi" || township.townShipNameEN == "PatheinGyi" {
                isVillageAvaible = true
            }
            //village
            let dic2 = AddressInfo[4]
            dic2["title"] = ""
            model.Village = ""
            reloadRows(index: 4, withSeciton: 2)
            //Street
            let dic3 = AddressInfo[5]
            dic3["title"] = ""
            model.Street = ""
            reloadRows(index: 4, withSeciton: 2)
            reloadRows(index: 5, withSeciton: 2)
            
            //Show Village list
            self.callStreetVillageAPI(townshipcode: township.townShipCode)
            
        }
        
    }


extension BusinessDetailsVC : AddressTypeUPDelegate,BusinessSubCategoryVCDelegate,BusinessCategoryVCDelegate,UploadImagesUPDelegate,BDAttachImagesVCDelegte,VillageStreetListViewDelegate,StreetListViewDelegate {
    
    func selectedAddress(name: String, number: Int) {
        let dic = businessInfo[0]
        dic["title"] = name
        model.OwnershipType = String(number)
        reloadRows(index: 0, withSeciton: 1)
        updateCountStatus()
    }
    
    func backFromBusinessSubCategoryVCWithNameImage(dicSubCate: SubCategoryDetail) {
        model.BusinessSubCategory = replaceSubCat(sbCat: dicSubCate.subCategoryCode)
        let dic = businessInfo[2]
        if ok_default_language  == "my" {
            dic["title"] = dicSubCate.subCategoryBurmeseName
        }else if ok_default_language  == "en" {
            dic["title"] = dicSubCate.subCategoryName
        }else {
            dic["title"] = dicSubCate.subCategoryBurmeseUName
        }
        subcategoryimage = dicSubCate.subCategoryEmoji
        reloadRows(index: 2, withSeciton: 1)
        updateCountStatus()
    }
    
    func setBusinessCategoryNameImage(Dicti: CategoryDetail) {
        model.BusinessCategory = Dicti.categoryCode
        model.BusinessSubCategory = ""
        categoryDetail = Dicti
        let dic = businessInfo[1]
        if ok_default_language  == "my" {
            dic["title"] = Dicti.categoryBurmeseName
        }else if ok_default_language  == "en"{
            dic["title"] = Dicti.categoryName
        }else {
            dic["title"] = Dicti.categoryBurmeseUName
        }
        categoryimage = Dicti.category_image
        subcategoryimage = ""
        reloadRows(index: 1, withSeciton: 1)
        // reset sub category titile & image
        let dic1 = businessInfo[2]
        dic1["title"] = "Select Business Sub-Category".localized
        reloadRows(index: 2, withSeciton: 1)
        updateCountStatus()
    }
    
    func setBusinessSubcategoryNameImage(sub_dictionary: SubCategoryDetail) {
        model.BusinessSubCategory = replaceSubCat(sbCat: sub_dictionary.subCategoryCode)
        let dic = businessInfo[2]
        if ok_default_language  == "my" {
            dic["title"] = sub_dictionary.subCategoryBurmeseName
        }else if ok_default_language  == "en"{
            dic["title"] = sub_dictionary.subCategoryName
        }else {
            dic["title"] = sub_dictionary.subCategoryBurmeseUName
        }
         subcategoryimage = sub_dictionary.subCategoryEmoji
        reloadRows(index: 2, withSeciton: 1)
        updateCountStatus()
    }
    
    func sendImagesToMainController(images: [String]) {
        model.BusinessRegistration1 = images[0].replacingOccurrences(of: " ", with: "%20")
        model.BusinessRegistration2 = images[1].replacingOccurrences(of: " ", with: "%20")
        if let cell = tbUpdateProfile.cellForRow(at: IndexPath(row: 4, section: 1)) as? NRCPassportUPCell {
            DispatchQueue.main.async {
                if self.model.BusinessRegistration1 != "" {
                     cell.imgFront.isHidden = false
                     cell.imgFront.sd_setImage(with: URL(string: self.model.BusinessRegistration1), placeholderImage: nil)
                }
                if self.model.BusinessRegistration2 != "" {
                    cell.imgFront.isHidden = false
                    cell.imgRear.sd_setImage(with: URL(string: self.model.BusinessRegistration2), placeholderImage: nil)
                }
            }
        }
        reloadRows(index: 4, withSeciton: 1)
        
         updateCountStatus()
    }
    func setUpVillageList() {
        tvVillageList.frame = CGRect(x: 10, y: 64, width: self.view.frame.width - 20, height: 200)
        tvVillageList.backgroundColor = UIColor.clear
        tvVillageList.delegate = self
        tvVillageList.tvList.isHidden = true
        tvVillageList.isUserInteractionEnabled = true
        self.view.addSubview(tvVillageList)
        self.view.bringSubviewToFront(tvVillageList)
    }
    func removeVillageList() {
        if self.view.contains(tvVillageList) {
            tvVillageList.removeFromSuperview()
        }
    }
    //tvStreetList
    func setUpStreetList() {
        tvStreetList.frame = CGRect(x: 10, y: 64, width: self.view.frame.width - 20, height: 200)
        tvStreetList.backgroundColor = UIColor.clear
        tvStreetList.delegate = self
        tvStreetList.tvList.isHidden = true
        tvStreetList.isUserInteractionEnabled = true
        self.view.addSubview(tvStreetList)
        self.view.bringSubviewToFront(tvStreetList)
    }
    func removeStreetList() {
        if self.view.contains(tvStreetList) {
            tvStreetList.removeFromSuperview()
        }
    }
    
    @objc func villageTextFieldEditing(_ textField: UITextField) {
        if textField.tag == 400 {
            if textField.text?.count ?? 0 > 0 {
                if self.view.contains(tvVillageList) {
                }else {
                    self.setUpVillageList()
                    tvVillageList.tvList.isHidden = false
                }
                tvVillageList.searchString(string: textField.text ?? "")
            }else {
                if self.view.contains(tvVillageList) {
                    self.removeVillageList()
                }
            }
        }
    }
    
    @objc func streetTextFieldEditing(_ textField: UITextField) {
        if textField.tag == 500 {
            if textField.text?.count ?? 0 > 0 {
                if self.view.contains(tvStreetList) {
                }else {
                    self.setUpStreetList()
                    tvStreetList.tvList.isHidden = false
                }
                tvStreetList.getAllAddressDetails(searchTextStr: textField.text ?? "", yAsix: 0)
            }else {
                if self.view.contains(tvStreetList) {
                    self.removeStreetList()
                }
            }
        }
    }
    
    func villageNameSelected(vill_Name: String) {
        let indexPath = IndexPath(row: 4, section: 2)
        if let cell = self.tbUpdateProfile.cellForRow(at: indexPath) as? NameInputUPCell {
            cell.tfTitle.text = vill_Name
            cell.tfTitle.resignFirstResponder()
            model.Village = vill_Name
            self.removeVillageList()
        }
    }
    
    func streetNameSelected(street_Name: addressModel) {
        let indexPath = IndexPath(row: 5, section: 2)
        if let cell = self.tbUpdateProfile.cellForRow(at: indexPath) as? NameInputUPCell {
            cell.tfTitle.text = street_Name.shortName
            cell.tfTitle.resignFirstResponder()
            model.Street = street_Name.shortName ?? ""
            self.removeStreetList()
        }
    }
    
    func replaceString(str : String) -> String {
        if str.count == 2 {
            let index = str.index(str.startIndex, offsetBy: 0)
            if str == "00" {
                return "0"
            }else {
                if str[index] == "0"{
                    let string = str.replacingOccurrences(of: "0", with: "")
                    return string
                }
            }
        }
        return str
    }
    
    func replaceSubCat(sbCat : String) ->String {
        var st = sbCat
        if st.count == 1 {
            st = "0" + st
        }
        return st
    }
    func businesssImagesUploaded(dic: Dictionary<String,Any>, logoStauts: Int){
        model.LogoImages = dic
        model.LogoApproveStatus = logoStauts
        updateCountStatus()
    }
    
    func updateCountStatus() {
        myProfileStatus = updateProfileStatusCount()
       // reloadRows(index: 0, withSeciton: 5)
        reloadSection(index: 6)
    }
    
}

extension BusinessDetailsVC : BDServiceDayTimeDelegate,BDPublicHolidaysDelegate, BDContactDetailsVCDelegate {
    func SelectHolidays(holidayList: [Dictionary<String, Any>]) {
        model.PublicHoliday = holidayList
        arrPublicDays.removeAll()
        for hldy in holidayList {
            if let holiday = hldy["Status"] as? Bool  {
                if holiday {
                    arrPublicDays.append(hldy)
                }
            }
        }
        reloadSection(index: 4)
    }
    
    func updateContactDetials(contactNumber: [Dictionary<String, Any>], youtubeLinks: [Dictionary<String, Any>], otherLink: [String]) {
        model.ContactNumber = contactNumber
        model.YouTubeLinks = youtubeLinks
        model.Email = otherLink[0]
        model.Website = otherLink[1]
        model.FacebookPage = otherLink[2]
        model.Instagram = otherLink[3]
    }
    
    func selectedDaysWithTime(daysWithTime: [Dictionary<String, String>]) {
        var tempDaysList = [Dictionary<String, String>]()
        println_debug(daysWithTime)
        model.WorkingDayAndTime?.removeAll()
        for dayCode in daysWithTime {
            let name = dayCode["DayCode"]
            switch name {
            case "SUN":
                tempDaysList.append(dayCode)
                break
            case "MON":
                tempDaysList.append(dayCode)
                break
            case "TUE":
                tempDaysList.append(dayCode)
                break
            case "WED":
                tempDaysList.append(dayCode)
                break
            case "THU":
                tempDaysList.append(dayCode)
                break
            case "FRI":
                tempDaysList.append(dayCode)
                break
            case "SAT":
                tempDaysList.append(dayCode)
                break
            case "SUB":
                tempDaysList.append(dayCode)
                break
            case "ALL":
                tempDaysList.append(dayCode)
                break
            default:
                break
            }
        }
        println_debug(tempDaysList)
        model.WorkingDayAndTime = tempDaysList
        reloadSection(index: 3)
        updateCountStatus()
    }
    
    func dayFromDayCode(code: String) -> String {
        let name = String(code.prefix(3))
        switch name {
        case "SUN":
            return "Sunday".localized
        case "MON":
            return "Monday".localized
        case "TUE":
            return "Tuesday".localized
        case "WED":
            return "Wednesday".localized
        case "THU":
            return "Thursday".localized
        case "FRI":
            return "Friday".localized
        case "SAT":
            return "Saturday".localized
        case "SUB":
            return "Sabbarth day".localized
        case "ALL":
            return "All Days".localized
        default:
            break
        }
        return ""
    }
    
}
extension BusinessDetailsVC: UITextFieldDelegate {
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        if  textField.tag == 100 {
            businessOutLet = textField.text ?? ""
            updateCountStatus()
        }else if textField.tag == 141 {
            businessName = textField.text ?? ""
            updateCountStatus()
        }else if textField.tag == 400 {
            //Village
            model.Village = textField.text ?? ""
            self.removeVillageList()
            reloadRows(index: 4, withSeciton: 2)
        }else if textField.tag == 500 {
            // Street
             model.Street = textField.text ?? ""
             self.removeStreetList()
            myProfileStatus = updateProfileStatusCount()
            reloadRows(index: 5, withSeciton: 2)
            
        }else if textField.tag == 600 {
            // house no
             model.HouseNumber = textField.text ?? ""
        }else if textField.tag == 700 {
            // floor no
             model.FloorNumber = textField.text ?? ""
        }else if textField.tag == 800 {
            // Room No
             model.RoomNumber = textField.text ?? ""
        }else if textField.tag == 900 {
            // housing
             model.HouseBlockNumber = textField.text ?? ""
            reloadRows(index: 7, withSeciton: 2)
        }
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 400 || textField.tag == 500 || textField.tag == 600 || textField.tag == 700 || textField.tag == 800 || textField.tag == 900 {
            textField.textColor = .black
            let div = AddressInfo[1]
            let township = AddressInfo[2]
            if div["title"] as? String ?? "" == "" {
                 showAlert(alertTitle: "", alertBody: "Select State / Division".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
                return false
            }else if township["title"] as? String ?? "" == "" {
                 showAlert(alertTitle: "", alertBody: "Select Township".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
                  return false
            }
        }
          return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if range.location == 0 && string == " " {
            return false
        }
        textField.textColor = .black
        
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if text.count == 1 && string == "" {
            if textField.tag == 400 {
                if self.view.contains(tvVillageList) {
                    self.removeVillageList()
                }
            }else if textField.tag == 500 {
                if self.view.contains(tvStreetList) {
                    self.removeStreetList()
                }
            }
        }
        
        if textField.tag == 8 {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: BUSINESSNAMECHARSET).inverted).joined(separator: "")) { return false }
        }else if textField.tag == 400 || textField.tag == 500 || textField.tag == 600 || textField.tag == 700 || textField.tag == 800 || textField.tag == 900 || textField.tag == 1000 {
              if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
        }else {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
        }
       
        if text.count > 50 {
            return false
        }
        if textField.tag == 141 {
            if let cell = tbUpdateProfile.cellForRow(at: IndexPath(row: 1, section: 0)) as? NameInputUPCell {
                if text.count > 0 {
                    cell.btnRightArrow.isHidden = false
                }else {
                     cell.btnRightArrow.isHidden = true
                }
            }
        }

        if textField.tag == 600 || textField.tag == 700 || textField.tag == 800  {
            return text.count <= 6
        }
    
        if string == "" || string == " " {
            let _ = TextValidations.trimSuccessiveSpaces(textField: textField, range: range, string: string)
            return false
        }
        return true
    }
    
}


