//
//  BDServiceDayTime.swift
//  OK
//
//  Created by SHUBH on 12/27/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit

protocol  BDServiceDayTimeDelegate {
//  func SelectDays(daysList : [String])
    func selectedDaysWithTime(daysWithTime: [Dictionary<String,String>])
}

class BDServiceDayTime: OKBaseController {
    var delegate : BDServiceDayTimeDelegate?
    
    @IBOutlet weak var btnSet: UIButton!{
        didSet {
            btnSet.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
           btnSet.setTitle("Set".localized, for: .normal)
        }
    }
    @IBOutlet weak var btnCancel: UIButton!{
        didSet {
            btnCancel.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
          btnCancel.setTitle("Cancel".localized, for: .normal)
        }
    }
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnNext: UIButton!{
        didSet {
            btnNext.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    var arrDays = [String]()
    var arrOpenCloseTime = [String]()
    var seletedDays: [Dictionary<String,Any>]?
    var arrSelection = [String]()
    var arrFinal = [String]()
    var arrOldSelection = [String]()
    var boolOpenAlways = Bool()

    override func viewDidLoad() {
        setMarqueLabelInNavigation()
        super.viewDidLoad()
        arrDays = ["Monday".localized,"Tuesday".localized,"Wednesday".localized,"Thursday".localized,"Friday".localized,"Saturday".localized,"Sunday".localized,"Sabbarth day".localized]
        arrSelection = ["0","0","0","0","0","0","0","0"]
        arrOpenCloseTime = ["09:00 AM - 04:00 PM","09:00 AM - 04:00 PM","09:00 AM - 04:00 PM","09:00 AM - 04:00 PM","09:00 AM - 04:00 PM","09:00 AM - 04:00 PM","09:00 AM - 04:00 PM","09:00 AM - 04:00 PM"]
        
        if let days = seletedDays , seletedDays?.count ?? 0 > 0 {
            for dic in days {
                let dayName = dayFromDayCode(code: dic["DayCode"] as! String)
                if dayName == "All Days".localized {
                    boolOpenAlways = true
                    return
                }
                var index = 0
                for dd in arrDays {
                    if dd == dayName {
                        arrSelection[index] = "1"
                        var opentime = dic["OpenTime"] as? String ?? ""
                        var closeTime = dic["CloseTime"] as? String ?? ""
                        if opentime.first == " " {
                            opentime.removeFirst()
                        }
                        if opentime.last == " " {
                            opentime.removeLast()
                        }
                        if closeTime.first == " " {
                            closeTime.removeFirst()
                        }
                        if closeTime.last == " " {
                            closeTime.removeLast()
                        }
                        arrOpenCloseTime[index] = opentime + " - " + closeTime
                    }
                    index += 1
                }
            }
        }

    }
    
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: appFontSize)
        lblMarque.text = "Business Day & Time".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.init(hex: "#1C2C99")
        self.navigationItem.titleView = lblMarque
    }
    
    override func viewWillAppear(_ animated: Bool) {
        btnNext.isHidden = true
        tblView.reloadData()
    }
 
}

extension BDServiceDayTime: UITableViewDelegate, UITableViewDataSource, TimeSelectionViewControllerDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 60
        }
        else if indexPath.section == 1 {
            return 40
        }
        else {
            return 50
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 || section == 1{
            return 1
        }
        return arrDays.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let identifier = "TwentyFourSevenDayTimeCell"
            var cell: TwentyFourSevenDayTimeCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? TwentyFourSevenDayTimeCell
            if cell == nil {
                tableView.register(UINib(nibName: "TwentyFourSevenDayTimeCell", bundle: nil), forCellReuseIdentifier: identifier)
                cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? TwentyFourSevenDayTimeCell
            }
            cell.btnSelection.addTarget(self, action:#selector(self.checkBoxTwentyFourAction(sender:)), for: .touchUpInside)
            if  boolOpenAlways {
                cell.btnSelection.setImage(UIImage(named:"bd_green_check"), for: .normal)
            } else {
                cell.btnSelection.setImage(UIImage(named:"bd_gray_blank"), for: .normal)
            }
            return cell
        }else if indexPath.section == 1 {
            let identifier = "CheckBoxHeaderCell"
            var cellFile: CheckBoxHeaderCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? CheckBoxHeaderCell
            if cellFile == nil {
                tableView.register(UINib(nibName: "CheckBoxHeaderCell", bundle: nil), forCellReuseIdentifier: identifier)
                cellFile = tableView.dequeueReusableCell(withIdentifier: identifier) as? CheckBoxHeaderCell
                //cellFile.contentView.backgroundColor = UIColor.colorWithRedValue(redValue: 230, greenValue: 230, blueValue: 230, alpha: 1)
            }
            
            let countSelection = arrSelection.reduce(0) { $1 == "1" ? $0 + 1 : $0 }
            if countSelection > 0 {
                if ok_default_language == "my" {
                    cellFile.lblSelectedDays.text = String.init(format: " ေရြးခ်ယ္ထားေသာ အလုပ္ခ်ိန္ " + String(countSelection) + " ရက္" )
                }else if ok_default_language == "en" {
                    cellFile.lblSelectedDays.text = "Selected " + String(countSelection) + " Working Days"
                }else {
                    cellFile.lblSelectedDays.text = String.init(format: "ရွေးထားသော" + String(countSelection) + " ရက္" )
                }
            }else {
                cellFile.lblSelectedDays.text = "Select Working Days".localized
            }
            
            if boolOpenAlways {
                cellFile.lblSelectedDays.text = "Selected Open 24/7 Working Days".localized
            }
            return cellFile
        }else {
            let identifier = "SelectDayTimeCell"
            var cell: SelectDayTimeCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? SelectDayTimeCell
            if cell == nil {
                tableView.register(UINib(nibName: "SelectDayTimeCell", bundle: nil), forCellReuseIdentifier: identifier)
                cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? SelectDayTimeCell
            }
            cell.btnSelection.tag = indexPath.row
            cell.btnSelection.addTarget(self, action:#selector(self.checkBoxDaysAction(_:)), for: .touchUpInside)
            
            if !boolOpenAlways {
                if arrSelection[indexPath.row] == "1" {
                    cell.btnSelection.setImage(UIImage(named:"bd_green_check"), for: .normal)
                }else {
                    cell.btnSelection.setImage(UIImage(named:"bd_red_check"), for: .normal)
                }
                cell.lblTimeOpenClose.text = arrOpenCloseTime[indexPath.row]
            }else {
                cell.btnSelection.setImage(UIImage(named:"bd_gray_check"), for: .normal)
                cell.lblTimeOpenClose.text = "00:00 AM - 11:59 PM"
            }
            cell.lblWeekDays.text = arrDays[indexPath.row]
            cell.lblTimeOpenClose.isUserInteractionEnabled = true
            let tapAction = UITapGestureRecognizer(target: self, action:#selector(onClickSelectTimeAction(_:)))
            tapAction.name = String(indexPath.row)
            cell.lblTimeOpenClose?.addGestureRecognizer(tapAction)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        println_debug(arrDays[indexPath.row])
    }

    @IBAction func setButtonClicked(sender: UIButton) {
        var tempArray = [Dictionary<String,String>]()
        if boolOpenAlways  {
            let dic = ["DayCode": "ALL","OpenTime": "00:00 AM","CloseTime": "11:59 PM"]
            tempArray.append(dic)
        }else {
            var index = 0
            for day in arrSelection {
                if day == "1" {
                    let str = arrOpenCloseTime[index]
                    let arr = str.components(separatedBy: "-")
                    let dic = ["DayCode": getDayThrough(index: index),"OpenTime": arr[0],"CloseTime": arr[1]]
                    tempArray.append(dic)
                }
                index += 1
            }
            
            if tempArray.count == 0 {
                alertViewObj.wrapAlert(title: nil, body: "No day selected. Please select day".localized, img: UIImage(named: "check_sucess_bill"))
                alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
                }
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                    if let del = self.delegate {
                        del.selectedDaysWithTime(daysWithTime: tempArray)
                    }
                    tempArray.removeAll()
                    self.navigationController?.popViewController(animated: true)
                }
                
                alertViewObj.showAlert(controller: self)
                
//            self.showAlert(alertTitle: "", alertBody: "No day selected. Please select day".localized, alertImage: UIImage(named: "error")!)
                return
            }
      
        }
        
        alertViewObj.wrapAlert(title: nil, body: "Business Working Days Successfully saved".localized, img: UIImage(named: "check_sucess_bill"))
        alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            if let del = self.delegate {
                del.selectedDaysWithTime(daysWithTime: tempArray)
            }
            tempArray.removeAll()
            self.navigationController?.popViewController(animated: true)
        }
        alertViewObj.showAlert(controller: self)
  
    }
    
    private func getDayThrough(index : Int) -> String {
        switch index {
        case 0:
            return "MON"
        case 1:
            return "TUE"
        case 2:
            return "WED"
        case 3:
            return "THU"
        case 4:
            return "FRI"
        case 5:
            return "SAT"
        case 6:
            return "SUN"
        case 7:
            return "SUB"
        default:
            return ""
        }
    }
    
    func dayFromDayCode(code: String) -> String {
        let name = String(code.prefix(3))
        switch name {
        case "SUN":
            return "Sunday".localized
        case "MON":
            return "Monday".localized
        case "TUE":
            return "Tuesday".localized
        case "WED":
            return "Wednesday".localized
        case "THU":
            return "Thursday".localized
        case "FRI":
            return "Friday".localized
        case "SAT":
            return "Saturday".localized
        case "SUB":
            return "Sabbarth day".localized
        case "ALL":
            return "All Days".localized
        default:
            break
        }
        return ""
    }

    
    
    @IBAction func cancelButtonClicked(sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backButtonClicked(sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func checkBoxTwentyFourAction(sender: UIButton) {
        boolOpenAlways = !boolOpenAlways
        arrSelection = ["0","0","0","0","0","0","0","0"]
        arrOpenCloseTime = ["09:00 AM - 04:00 PM","09:00 AM - 04:00 PM","09:00 AM - 04:00 PM","09:00 AM - 04:00 PM","09:00 AM - 04:00 PM","09:00 AM - 04:00 PM","09:00 AM - 04:00 PM","09:00 AM - 04:00 PM"]
        self.tblView.reloadData()
    }
    
    @IBAction func checkBoxDaysAction(_ sender: UIButton) {
        if boolOpenAlways {
            return
        }
        if arrSelection[sender.tag] == "1" {
            sender.setImage(UIImage(named: "bd_red_check"), for: .normal)
            arrSelection[sender.tag] = "0"
        }else {
            sender.setImage(UIImage(named: "bd_green_check"), for: .normal)
            arrSelection[sender.tag] = "1"
        }
        tblView.reloadSections([1], with: .none)
    }
    
    @objc func onClickSelectTimeAction(_ gesture: UITapGestureRecognizer) {
        if boolOpenAlways {
            return
        }
        let index = Int(gesture.name ?? "0")
        if arrSelection[index ?? 0] == "1" {
        let contactDetailsVC = UIStoryboard(name: "MyProfile", bundle: Bundle.main).instantiateViewController(withIdentifier: "TimeSelectionViewController")  as! TimeSelectionViewController
        contactDetailsVC.delegate = self
        contactDetailsVC.seletedIndex = Int(gesture.name ?? "100")
            self.navigationController?.pushViewController(contactDetailsVC, animated: true)
        }else {
               self.showAlert(alertTitle: "", alertBody: "Please select day first".localized, alertImage: UIImage(named: "error")!)
        }
    }

    func SelectTime(timeList: String, forDys dayIndex: Int) {
        if let cell = tblView.cellForRow(at: IndexPath(row: dayIndex, section: 2)) as? SelectDayTimeCell {
            cell.lblTimeOpenClose.text = timeList
             arrOpenCloseTime[dayIndex] = timeList
        }
     
    }
    
    
    @IBAction func nextButtonClicked(sender: UIButton) {
        let contactDetailsVC = UIStoryboard(name: "MyProfile", bundle: Bundle.main).instantiateViewController(withIdentifier: "TimeSelectionViewController")  as! TimeSelectionViewController
        contactDetailsVC.delegate = self
        self.navigationController?.pushViewController(contactDetailsVC, animated: true)
    }
    
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }

}
