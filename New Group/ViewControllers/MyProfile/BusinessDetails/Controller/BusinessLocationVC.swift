//
//  BusinessLocationViewController.swift
//  OK
//
//  Created by PC on 11/25/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import MapKit

class BusinessLocationVC: OKBaseController {
    
    @IBOutlet var mapView: MKMapView?
    @IBOutlet var lblHeader: UILabel!
    {
      didSet
      {
        lblHeader.text = "Location As Per Business Address Filled".localized
      }
  }
    @IBOutlet var btnOK: UIButton!
    {
      didSet
      {
        btnOK.setTitle("OK".localized, for: .normal)
      }
  }

    let locationManager = CLLocationManager()
    let regionRadius: CLLocationDistance = 1000

    override func viewDidLoad() {
        super.viewDidLoad()
      self.title = "Business Location".localized
      self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: "Zawgyi-One", size: 21)!, NSAttributedString.Key.foregroundColor: UIColor.white]
      
        mapView?.delegate = self
        // Do any additional setup after loading the view.
        mapView?.showsUserLocation = true
        let initialLocation = CLLocation(latitude:Double (UserModel.shared.lat)!, longitude: Double (UserModel.shared.long)!)
        //centerMapOnLocation(location: initialLocation)
        
        let regionRadius: CLLocationDistance = 1000
        let coordinateRegion = MKCoordinateRegion.init(center: initialLocation.coordinate,
                                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView?.setRegion(coordinateRegion, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude:Double (UserModel.shared.lat)!, longitude: Double (UserModel.shared.long)!)
        mapView?.addAnnotation(annotation)
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion.init(center: location.coordinate,
                                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView?.setRegion(coordinateRegion, animated: true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func zoomOutButtonClicked(sender: UIButton) {
        
        let span = MKCoordinateSpan(latitudeDelta: (mapView?.region.span.latitudeDelta)!*2, longitudeDelta: (mapView?.region.span.longitudeDelta)!*2)
        let region = MKCoordinateRegion(center: (mapView?.region.center)!, span: span)
        
        if(span.latitudeDelta <= 182.71732403)
        {
            mapView?.setRegion(region, animated: true)
        }
    }
    
    @IBAction func zoomInButtonClicked(sender: UIButton) {
        
        let span = MKCoordinateSpan(latitudeDelta: (mapView?.region.span.latitudeDelta)!/2, longitudeDelta: (mapView?.region.span.longitudeDelta)!/2)
        let region = MKCoordinateRegion(center: (mapView?.region.center)!, span: span)
               
        mapView?.setRegion(region, animated: true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BusinessLocationVC: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
            
        else {
            let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "annotationView") ?? MKAnnotationView()
            annotationView.image = UIImage(named: "map_location")
            return annotationView
        }
    }
}

