//
//  PersonalProfileModelUP.swift
//  OK
//
//  Created by Imac on 3/8/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class PersonalProfileModelUP: NSObject {

    static var shared = PersonalProfileModelUP()
    
    var ProfilePicture: String = ""
    var ProfileName: String = ""
    var Gender: String = ""
    var AccountType: Int = 0
    var OkAccNumber: String = ""
    var OkDollarJoiningDate: String = ""
    var CountryName: String = ""
    var Dob: String = ""
    var NrcImage1: String = ""
    var NrcImage2: String = ""
    var IdType: String = ""
    var NrcNumber: String = ""
    var NrcExpireDate: String = ""
    var FatherName: String = ""
    
    var SecurityQuestion: String = ""
    var SecrityAnswer: String = ""
    
    var ContactPhonenumber: String = ""
    var AlternatePhonenumber: String = ""
    
    var EmailId: String = ""
    var FacebookId: String = ""
    var PhoneNumber: String = ""
    
    var AddressType: String = ""
    var Division: String = ""
    var Township: String = ""
    var City: String = ""
    var Village: String = ""
    var Street: String = ""
    var HouseNumber: String = ""
    var FloorNumber: String = ""
    var RoomNumber: String = ""
    var HousingOrZoneName: String = ""
    var HouseBlockNumber: String = ""
    var Line2: String = ""
    var CountryCode: String = ""
    
    
      var RegistrationStatus: Int = 0
  

    
}
