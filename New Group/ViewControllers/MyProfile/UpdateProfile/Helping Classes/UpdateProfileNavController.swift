//
//  UpdateProfileNavController.swift
//  OK
//
//  Created by PC on 11/22/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class UpdateProfileNavController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.topViewController?.navigationController?.navigationBar.barTintColor = UIColor(red: 0.96, green: 0.77, blue: 0.0, alpha: 1.0)  //UIColor.yellow//
        self.topViewController?.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }

}
