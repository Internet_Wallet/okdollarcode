//
//  BeLuckyCell.swift
//  OK
//
//  Created by PC on 12/5/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class BeLuckyCell: UITableViewCell {

    @IBOutlet weak var viewLucky: UIView!
    @IBOutlet weak var lblTitle: UILabel!{
        didSet {
            lblTitle.font = UIFont(name: appFont, size: 17)
            lblTitle.text = lblTitle.text?.localized
            
        }
    }
    @IBOutlet weak var lblDesc: UILabel!{
        didSet {
            lblDesc.font = UIFont(name: appFont, size: 14)
            lblDesc.text = lblDesc.text?.localized
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
