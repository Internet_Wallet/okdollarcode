//
//  BusinessCloseAlert.swift
//  OK
//
//  Created by PC on 12/9/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol  BusinessCloseAlertDelegate{
    func SelectOKType(ownershipName : String)
}

class BusinessCloseAlert: UIViewController {

    var businessCloseAlertDelegate : BusinessCloseAlertDelegate?

    @IBOutlet weak var viewTransparent: UIView!
    @IBOutlet weak var viewDateBG: UIView!
    
    @IBOutlet weak var imgAlertIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
        {
        didSet
        {
            lblTitle.font = UIFont(name: appFont, size: appFontSize)
            lblTitle.text = lblTitle.text?.localized
        }
    }
    @IBOutlet weak var btnOK: UIButton!
        {
        didSet
        {
            btnOK.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnOK.setTitle("OK".localized, for: .normal)
        }
    }
    
    var statusData = ""
    var imgIconName = "owner_name"

    override func viewWillAppear(_ animated: Bool) {
        self.loadInitialData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //Handled Attach file selection
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.tranparentViewAction(sender:)))
        viewTransparent.addGestureRecognizer(gesture)
        
        viewDateBG.layer.cornerRadius = 10
        viewDateBG.layer.masksToBounds = true
        
        viewDateBG.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
        
        UIView.animate(withDuration: 0.7, animations: { () -> Void in
            
            self.viewDateBG.transform = CGAffineTransform(scaleX: 1,y: 1)
            
        })
        
        self.loadInitialData()

    }
    
    func getData (data:String) {
        println_debug(data)
        statusData = data
        if(statusData == "UP-AuthorizedMer")
        {
            lblTitle.text = "Please update your Personal Details & Business Details to become a Authorized Merchant.".localized
        }
        else if(statusData == "UP-OK$Agent")
        {
            lblTitle.text = "Please update your Personal Details & Business Details to become a OK$ Agent.".localized
        }
    }
    
    func loadInitialData()
    {
        
        imgAlertIcon.image = UIImage(named: imgIconName)
        
        if(statusData == "UpdateProfileInfo")
        {
            lblTitle.text = "If you turn on/off this option, you can open/close your shop temporary. So customer will know easily about your business availablity.".localized
        }
        else if(statusData == "AttachShopInfo")
        {
            lblTitle.text = "This images will be displayed, when user is checking about your shop.".localized
        }
        else if(statusData == "UP-AuthorizedMer")
        {
            lblTitle.text = "Please update your Personal Details & Business Details to become a Authorized Merchant.".localized
        }
        else if(statusData == "UP-OK$Agent")
        {
            lblTitle.text = "Please update your Personal Details & Business Details to become a OK$ Agent.".localized
        }
        else if(statusData == "AuthorizedMerchant")
        {
            lblTitle.text = "To become Authorized Merchant, following Documents are required to submit.\n. Business Registration / License.\n. OK$ account owner NRC / ID.\n. Agent Application Form.".localized
        }
        else if(statusData == "OK$Agent")
        {
            lblTitle.text = "To become OK$ Agent, following Documents are required to submit. .Business Registration / License. . OK$ account owner NRC / ID. .Agent Application Form.".localized
        }
        else
        {
            lblTitle.text = statusData
        }
    }
    
    @objc func tranparentViewAction(sender : UITapGestureRecognizer) {
        // Do what you want
        navigationbarController(toEnable: true)
        
        self.view.removeFromSuperview()
    }
    
    func navigationbarController(toEnable: Bool) {
        if toEnable {
            self.navigationController?.navigationBar.layer.zPosition = 0
        } else {
            self.navigationController?.navigationBar.layer.zPosition = -64
        }
        self.navigationController?.navigationBar.isUserInteractionEnabled = toEnable
    }
    
    @IBAction func btnOKAction(_ sender: Any) {
        
        if(statusData == "UP-AuthorizedMer" || statusData == "UP-OK$Agent")
        {
            guard (self.businessCloseAlertDelegate?.SelectOKType(ownershipName: statusData) != nil) else {
                return
            }
        }
        navigationbarController(toEnable: true)
        self.view.removeFromSuperview()
        
    }
}
