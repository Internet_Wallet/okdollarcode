//
//  AdditionalSettingCell.swift
//  DemoUpdateProfile
//
//  Created by SHUBH on 7/20/17.
//  Copyright © 2017 SHUBH. All rights reserved.
//

import UIKit

class AdditionalSettingCell: UITableViewCell {
    
    @IBOutlet weak var lblNameAS: UILabel!{
        didSet{
            lblNameAS.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var imgIconAS: UIImageView!
    
    @IBOutlet weak var switchBusiness: UISwitch!
    @IBOutlet weak var lblNowOpen: UILabel! {
        didSet{
            lblNowOpen.font = UIFont(name: appFont, size: appFontSize)
            lblNowOpen.text = "Now Open".localized
            
        }
    }
    
    @IBOutlet weak var SwtAS: UIImageView!
    @IBOutlet weak var btnInfo: UIButton!{
        didSet{
            self.btnInfo.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
         lblNameAS.font = UIFont(name: appFont, size: 15)
         lblNowOpen.font = UIFont(name: appFont, size: 12)
         switchBusiness.layer.cornerRadius = switchBusiness.frame.height / 2
    }
    
    func cellWrapperData(boolStatus : Int) {
        if boolStatus == 1 {
            switchBusiness.isOn = true
            lblNowOpen.isHidden = false
            lblNameAS.text =  "To Close Your Business".localized + "\n" + "(" + "Temporary".localized + ")"
            lblNameAS.textColor = UIColor.black
            switchBusiness.tintColor = UIColor.init(hex: "3DBA06")
        }else {
            switchBusiness.isOn = false
            lblNowOpen.isHidden = true
            lblNameAS.text = "Business Closed".localized + "\n" + "(" + "Temporary".localized + ")"
            lblNameAS.textColor = UIColor.red
            switchBusiness.tintColor = UIColor.init(hex: "FF0715")
        }
    }

}
