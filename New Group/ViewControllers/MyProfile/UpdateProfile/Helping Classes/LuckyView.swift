//
//  LuckyView.swift
//  OK
//
//  Created by PC on 12/7/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class LuckyView: UIViewController {
    
    @IBOutlet weak var viewTransparent: UIView!
    @IBOutlet weak var viewDateBG: UIView!
    @IBOutlet weak var lblTitle: UILabel! {
        didSet {
            lblTitle.font = UIFont(name: appFont, size: 17)
            lblTitle.text = lblTitle.text?.localized
            
        }
    }
    @IBOutlet weak var lblParticipate: UILabel! {
        didSet {
            lblParticipate.font = UIFont(name: appFont, size: 16)
            lblParticipate.text = lblParticipate.text?.localized
        }
    }
    @IBOutlet weak var lblSpin: UILabel! {
        didSet {
            lblSpin.font = UIFont(name: appFont, size: 20)
            lblSpin.text = lblSpin.text?.localized
        }
    }
    @IBOutlet weak var lblLucky: UILabel! {
        didSet {
            lblLucky.font = UIFont(name: appFont, size: 16)
            lblLucky.text = lblLucky.text?.localized
        }
    }
    @IBOutlet weak var lblProfile: UILabel! {
        didSet {
            lblProfile.font = UIFont(name: appFont, size: 16)
            lblProfile.text = lblProfile.text?.localized
        }
    }
    @IBOutlet weak var lblMillionaire: UILabel! {
        didSet {
            lblMillionaire.font = UIFont(name: appFont, size: 20)
            lblMillionaire.text = lblMillionaire.text?.localized
        }
    }
    @IBOutlet weak var btnOK: UIButton! {
        didSet {
            btnOK.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnOK.setTitle("OK".localized, for: .normal)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
       
        
        
       
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.tranparentViewAction(sender:)))
        viewTransparent.addGestureRecognizer(gesture)
        viewDateBG.layer.cornerRadius = 10
        viewDateBG.layer.masksToBounds = true
        viewDateBG.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
        UIView.animate(withDuration: 0.7, animations: { () -> Void in
            self.viewDateBG.transform = CGAffineTransform(scaleX: 1,y: 1)
        })
    }
    
    @objc func tranparentViewAction(sender : UITapGestureRecognizer) {
        // Do what you want
        navigationbarController(toEnable: true)
        
        self.view.removeFromSuperview()
    }
    
    func navigationbarController(toEnable: Bool) {
        if toEnable {
            self.navigationController?.navigationBar.layer.zPosition = 0
        } else {
            self.navigationController?.navigationBar.layer.zPosition = -64
        }
        self.navigationController?.navigationBar.isUserInteractionEnabled = toEnable
    }
    
    @IBAction func btnOKAction(_ sender: Any) {
        
        navigationbarController(toEnable: true)
        self.view.removeFromSuperview()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
