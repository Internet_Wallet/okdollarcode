//
//  UpdateProfileModel.swift
//  OK
//
//  Created by SHUBH on 8/3/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class UpdateProfileModel: NSObject {
    
    static var share = UpdateProfileModel()
    var business_closed: Int =  0
    var cash_in: Int = 0
    var ok_agent_services: Int = 0
    var cash_out: Int = 0
    var personal_details: Int = 0
    var other_services: Int =  0
    var business_details: Int = 0
    var CashInServices: [Dictionary<String,Any>]?
    var CashOutServices: [Dictionary<String,Any>]?
    var Agent: [Dictionary<String,Any>]?
    var AgentSince: String = ""
    var AgentValidTo: String = ""
    var AgentStatus: Int = 0
    func updateProfileMenuStatus(urlStr : URL, handle :@escaping (_ isSuccess : Bool,_ message : String) -> Void) {
    
        let dictLoginUserInfo = NSMutableDictionary()
        dictLoginUserInfo["AppId"] = "com.cgm.OKDollar"
        dictLoginUserInfo["Limit"] = 0
        dictLoginUserInfo["MobileNumber"] = UserModel.shared.mobileNo
        dictLoginUserInfo["Msid"] = UserModel.shared.msid
        dictLoginUserInfo["Offset"] = 0
        dictLoginUserInfo["Ostype"] = 1
        dictLoginUserInfo["Simid"] = UserModel.shared.simID
        
        let array = [
            ["Type":"business_closed","Value":business_closed as Any],
            ["Type":"cash_in","Value":cash_in as Any],
            ["Type":"cash_out","Value":cash_out as Any],
            ["Type":"other_services","Value":other_services as Any],
            ["Type":"ok_agent_services","Value":ok_agent_services as Any],
            ["Type":"personal_details","Value":personal_details as Any],
            ["Type":"business_details","Value":business_details as Any]
        ]
        let dic = ["AgentId" : UserModel.shared.agentID,"Login":dictLoginUserInfo,"MerchantProfileStatusList":array] as [String : Any]
        println_debug("ProfileMenuUpdate: \(urlStr)")
        println_debug("ProfileMenuUpdate Params\(dic)")
        TopupWeb.genericClass(url: urlStr, param: dic as AnyObject, httpMethod: "POST", handle: {(resp, success) in
            if success {
                if let response = resp as? Dictionary<String,AnyObject> {
                    if response["Code"] as? Int == 200 {
                        profileObj.callUpdateProfileApi(aCode: UserModel.shared.mobileNo, handler: { (success) in
                            if success {
                                handle(success, "Success")
                            }else {
                                println_debug("callUpdateProfileApi Failed")
                            }
                        })
                    }else {
                        handle(success, "Unable to update, Please try again".localized)
                    }
                }
            }else {
                handle(success, "Unable to update, Please try again".localized)
            }
            })
    }
    
    
    func wrapMenuStatus() -> [Dictionary<String,Any>] {
        var arrMenuStatus = Array<Dictionary<String,Any>>()
        let dictMerchantProfileStatusList = NSMutableDictionary()
        dictMerchantProfileStatusList["Type"] = "personal_details"
        dictMerchantProfileStatusList["Value"] = personal_details
        arrMenuStatus.append(dictMerchantProfileStatusList as! Dictionary<String, Any>)
        
        let dictBusiness_details = NSMutableDictionary()
        dictBusiness_details["Type"] = "business_details"
        dictBusiness_details["Value"] = business_details
        arrMenuStatus.append(dictBusiness_details as! Dictionary<String, Any>)
        
        let dictCash_in = NSMutableDictionary()
        dictCash_in["Type"] = "cash_in"
        dictCash_in["Value"] = cash_in
        arrMenuStatus.append(dictCash_in as! Dictionary<String, Any>)
        
        let dictCash_out = NSMutableDictionary()
        dictCash_out["Type"] = "cash_out"
        dictCash_out["Value"] = cash_out
        arrMenuStatus.append(dictCash_out as! Dictionary<String, Any>)
        
        let dictOther_services = NSMutableDictionary()
        dictOther_services["Type"] = "other_services"
        dictOther_services["Value"] = other_services
        arrMenuStatus.append(dictOther_services as! Dictionary<String, Any>)
        
        let dictOk_agent_services = NSMutableDictionary()
        dictOk_agent_services["Type"] = "ok_agent_services"
        dictOk_agent_services["Value"] = ok_agent_services
        arrMenuStatus.append(dictOk_agent_services as! Dictionary<String, Any>)
        
        let dictBusiness_closed = NSMutableDictionary()
        dictBusiness_closed["Type"] = "business_closed"
        dictBusiness_closed["Value"] = business_closed
        arrMenuStatus.append(dictBusiness_closed as! Dictionary<String, Any>)
        return arrMenuStatus
    }
    
    }
