//
//  ViewController.swift
//  DemoUpdateProfile
//
//  Created by SHUBH on 7/20/17.
//  Copyright © 2017 SHUBH. All rights reserved.
//

import UIKit


class UpdateProfile: OKBaseController, UITableViewDelegate, UITableViewDataSource, WebServiceResponseDelegate {
    //MARK:  CLASS VARIABLES
    var navigation : UINavigationController?
    var navItem    : UINavigationItem?
    var titleList = [NSMutableDictionary]()
    
    var personalData: Dictionary<String,Any>?
    var businessData: Dictionary<String,Any>?
    var otherData: Dictionary<String,Any>?
    
    //MARK:  UIOUTLETS
    @IBOutlet weak var tvUpdateProfile: UITableView!
   // @IBOutlet weak var btnHome: UIButton!
    
    //MARK:  UIVIEWCONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tvUpdateProfile.tableFooterView = UIView()
        self.tvUpdateProfile.delegate = self
        self.tvUpdateProfile.dataSource = self
        self.setMarqueLabelInNavigation()
        self.changesLanguageAccordingToRegistration()
    
        self.tvUpdateProfile.reloadData()
        self.callAllNoteAPI()
        //self.loadViewStart()
    }

    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: appFontSize)
        lblMarque.text = "Update Profile".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.init(hex: "#1C2C99")
        self.navigationItem.titleView = lblMarque
    }

    override func viewWillAppear(_ animated: Bool) {
        
        //self.title = "Update Profile".localized
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
        self.callAPIToGetMenuStatus()
    }

    private func changesLanguageAccordingToRegistration() {
        // Change Language according to resgistered laguage
        println_debug(UserModel.shared.language)
        if UserModel.shared.language == "my" || UserModel.shared.language == "MY" || UserModel.shared.language == "MYA" || UserModel.shared.language == "mya" {
            ok_default_language = "my"
        }else if UserModel.shared.language == "en" || UserModel.shared.language == "EN" {
            ok_default_language = "en"
        }else {
            ok_default_language = "uni"
        }
        let setLanguage = UserDefaults.standard.value(forKey: "currentLanguage") as! String
        println_debug(setLanguage)
        if ok_default_language != setLanguage {
            UserDefaults.standard.set(true, forKey: "LanguageStatusChanged")
            UserDefaults.standard.set(setLanguage, forKey: "PreviousLanguage")
            UserDefaults.standard.set(ok_default_language, forKey: "currentLanguage")
            appDel.setSeletedlocaLizationLanguage(language: ok_default_language)
            UserDefaults.standard.synchronize()
        }
    }
    
    func loadViewStart() {
        navigationbarController(toEnable: false)
        let personalDetailVC = UIStoryboard(name: "MyProfile", bundle: Bundle.main).instantiateViewController(withIdentifier: "LuckyView")  as! LuckyView
        personalDetailVC.view.frame = CGRect.init(x: 0, y:0, width:self.view.frame.width, height: self.view.frame.height)
        addChild(personalDetailVC)
        personalDetailVC.didMove(toParent: self)
        self.view.addSubview((personalDetailVC.view)!)
    }
    
    func navigationbarController(toEnable: Bool) {
        if toEnable {
            self.navigationController?.navigationBar.layer.zPosition = 0
            self.title = "Update Profile".localized
            self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 21) ?? UIFont.systemFont(ofSize: 21), NSAttributedString.Key.foregroundColor: UIColor.white]
        }else {
            self.navigationController?.navigationBar.layer.zPosition = -64
        }
        self.navigationController?.navigationBar.isUserInteractionEnabled = toEnable
    }
    
    @IBAction func backAction(_ sender: Any) {
        //Set Laguage after change
        if UserDefaults.standard.bool(forKey: "LanguageStatusChanged") {
            let preLang = UserDefaults.standard.value(forKey: "PreviousLanguage") as? String
            if let language = preLang {
                UserDefaults.standard.set(language, forKey: "currentLanguage")
                appDel.setSeletedlocaLizationLanguage(language: language)
                UserDefaults.standard.set(false, forKey: "LanguageStatusChanged")
            }
            UserDefaults.standard.synchronize()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func callAllNoteAPI() {
        if appDelegate.checkNetworkAvail() {
            
            let paramString : [String:Any] = ["AgentId": UserModel.shared.agentID,"Login": ["AppId": "","Limit": 0,"MobileNumber": UserModel.shared.mobileNo,"Msid": msid,"Offset": 0,"Ostype": 1,"Simid": uuid]]
            let web = WebApiClass()
            web.delegate = self
            let urlStr = Url.getAllItemInfoDetails
            let ur = getUrl(urlStr: urlStr, serverType: .updateProfile)
            println_debug(paramString)
           // let ur = URL(string: "http://69.160.4.151:8085/UpdateProfile.svc/GetAllItemInfoDetails")
            println_debug(ur)
            println_debug(paramString)
            self.showProgressView()
            web.genericClass(url: ur, param: paramString as AnyObject, httpMethod: "POST", mScreen: "mAllNote")
        }
        
    }
    
    //Post Business closed or Open
    func callAPIToPostBusinessStatus() {
        
        if appDelegate.checkNetworkAvail() {
            let paramString : [String:Any] = [
                "LoginRequestInfo":self.loginInfoDictionary(),
                "ServiceType" : 2,
                "Status" : UpdateProfileModel.share.business_closed
            ]
            let web = WebApiClass()
            web.delegate = self
            let urlStr = Url.updateBusinessRunningStatus
            let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
            println_debug(ur)
            println_debug(paramString)
                    self.showProgressView()
            web.genericClass(url: ur, param: paramString as AnyObject, httpMethod: "POST", mScreen: "mUpdateBusinessStatus")
        }
    }
    
    func callAPIToGetMenuStatus() {
        self.showProgressView()
        if appDelegate.checkNetworkAvail() {
            let paramString : [String:Any] = [
                "AgentId" : UserModel.shared.agentID,
                "Login":self.loginInfoDictionary()
            ]
            let web      = WebApiClass()
            web.delegate = self
            let urlStr = Url.getUpdateProfileDetails
            let ur = getUrl(urlStr: urlStr, serverType: .updateProfile)
            println_debug("GetUpdateProfileMenuStatus : \(String(describing: ur))")
            println_debug("GetUpdateProfileMenuStatus Params :\(paramString)")
            web.genericClass(url: ur , param: paramString as AnyObject, httpMethod: "POST", mScreen: "mGetMenuStatus")
        }
    }
    
    //MARK:- API Response
    
    func webResponse(withJson json: AnyObject, screen: String) {
        self.removeProgressView()
        if screen == "mGetMenuStatus" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                             println_debug("mGetMenuStatus:\(dic)")
                        if dic["Code"] as? Int == 200 {
                            if let Resopnse = dic["Data"] as? String {
                                let dic = OKBaseController.convertToDictionary(text: Resopnse)
                                let dic1 = dic?["UpdateProfileMenuStatus"] as? [Any]
                                if let Items = dic1 as? Array<Dictionary<String,AnyObject>> {
                                    for menuItems in Items {
                                        if menuItems["Type"] as? String == "other_services" {
                                            UpdateProfileModel.share.other_services = menuItems["Value"] as? Int ?? 0
                                        }else if menuItems["Type"] as? String  == "business_details" {
                                            UpdateProfileModel.share.business_details = menuItems["Value"] as? Int ?? 0
                                            UpdateProfileModel.share.business_details = checkPercentOfBusiness()
                                        }else if menuItems["Type"] as? String == "personal_details" {
                                            UpdateProfileModel.share.personal_details = menuItems["Value"] as? Int ?? 0
                                            UpdateProfileModel.share.personal_details = checkPercentOfPersonal()
                                        }else if menuItems["Type"] as? String == "cash_out" {
                                            UpdateProfileModel.share.cash_out = menuItems["Value"] as? Int ?? 0
                                        }else if menuItems["Type"] as? String == "cash_in" {
                                            UpdateProfileModel.share.cash_in = menuItems["Value"] as? Int ?? 0
                                        }else if menuItems["Type"] as? String == "ok_agent_services" {
                                            UpdateProfileModel.share.ok_agent_services = menuItems["Value"] as? Int ?? 0
                                        }else if menuItems["Type"] as? String == "business_closed" {
                                            UpdateProfileModel.share.business_closed = menuItems["Value"] as? Int ?? 0
                                        }
                                    }
                                    if Items.count == 0 {
                                         UpdateProfileModel.share.personal_details = checkPercentOfPersonal()
                                        UpdateProfileModel.share.business_details = checkPercentOfBusiness()
                                    }
                                }
                                if let validTill = dic?["AgentValidTo"] as? String  {
                                    UpdateProfileModel.share.AgentValidTo = validTill
                                }
                                if let agentSince = dic?["AgentSince"] as? String  {
                                    UpdateProfileModel.share.AgentSince = agentSince
                                }
                                if let status = dic?["AgentStatus"] as? Int {
                                    UpdateProfileModel.share.AgentStatus = status
                                }
                                
                                var count = 0
                                if UserModel.shared.agentType == .user {
                                    if UserModel.shared.idType == "01" {
                                        count = 13  //NRC 18
                                    }else {//PASSPORT 20
                                        count = 15
                                    }
                                    if UpdateProfileModel.share.personal_details !=  count {
                                        DispatchQueue.main.async {
                                        self.showAlert(alertTitle: "", alertBody: "Please Update Your Personal Details to avail complete OK$ services".localized, alertImage: UIImage(named: "n_personal")!)
                                        }
                                    }
                                }else {
                                    if UserModel.shared.idType == "01" {
                                        count = 13 // NRC 17
                                    }else {
                                        count = 15 // PAs 19
                                    }
                                }
                                let score : Double = Double(UpdateProfileModel.share.personal_details)
                                let count1 : Double = Double(count)
                                let percent : Double = Double((score / count1) * 100)
                                let personalPercent = String(format: "%.0f%", percent)
                                
                                let scoreBusiness : Double = Double(UpdateProfileModel.share.business_details)
                                let percentBusiness : Double = Double((scoreBusiness / 10) * 100)
                                let businessPercent = String(format: "%.0f%", percentBusiness)
                                
                                if UserModel.shared.agentType == .user {
                                    titleList = [["title": "Personal Details".localized,"image": "personal","status": personalPercent],
                                                 //["title": "More Update".localized,"image": "","status": ""]
                                    ]
                                }else if UserModel.shared.agentType == .merchant {
                                    titleList = [["title": "Personal Details".localized,"image": "personal","status": personalPercent],
                                                 ["title": "Business Details".localized,"image": "r_merchant","status": businessPercent],
                                                 ["title": "Cash-In".localized,"image": "cashIn_new","status": UpdateProfileModel.share.cash_in],
                                                 ["title": "Cash-Out".localized,"image": "cashout_new","status": UpdateProfileModel.share.cash_out],
                                                 ["title": "Other Services".localized,"image": "otherservices","status": UpdateProfileModel.share.other_services],
                                                 ["title": "Apply for OK$ Agent".localized,"image": "agent","status": UpdateProfileModel.share.ok_agent_services],
                                                 //["title": "More Update".localized,"image": "","status": 0]
                                    ]
                                }else if UserModel.shared.agentType == .agent {
                                    titleList = [["title": "Personal Details".localized,"image": "personal","status": personalPercent],
                                                 ["title": "Business Details".localized,"image": "r_merchant","status": businessPercent],
                                                 ["title": "Cash-In".localized,"image": "cashIn_new","status": UpdateProfileModel.share.cash_in],
                                                 ["title": "Cash-Out".localized,"image": "cashout_new","status": UpdateProfileModel.share.cash_out],
                                                 ["title": "Other Services".localized,"image": "otherservices","status": UpdateProfileModel.share.other_services],
                                                 ["title": "Apply for OK$ Agent".localized,"image": "agent","status": UpdateProfileModel.share.ok_agent_services],
                                                 // ["title": "More Update".localized,"image": "","status": 0]
                                    ]
                                }else if UserModel.shared.agentType == .advancemerchant {
                                    titleList = [["title": "Personal Details".localized,"image": "personal","status": personalPercent],
                                                 ["title": "Business Details".localized,"image": "r_merchant","status": businessPercent],
                                                 ["title": "Cash-In".localized,"image": "cashIn_new","status": UpdateProfileModel.share.cash_in],
                                                 ["title": "Cash-Out".localized,"image": "cashout_new","status": UpdateProfileModel.share.cash_out],
                                                 ["title": "Other Services".localized,"image": "otherservices","status": UpdateProfileModel.share.other_services],
                                                 // ["title": "More Update".localized,"image": "","status": 0]
                                    ]
                                }
                                DispatchQueue.main.async {

                                if UserModel.shared.agentType == .user  {
                                    if UpdateProfileModel.share.personal_details != count {
                                        self.showAlert(alertTitle: "".localized, alertBody: "Please Update Your Personal Details to avail complete OK$ services".localized, alertImage: UIImage(named: "alert-icon")!)
                                    }
                                }else {
                                if UpdateProfileModel.share.personal_details != count && UpdateProfileModel.share.business_details != 10 {
                                    self.showAlert(alertTitle: "".localized, alertBody: "Please Update Your Personal & Business Details to avail complete OK$ services".localized, alertImage: UIImage(named: "alert-icon")!)
                                }else if UpdateProfileModel.share.personal_details != count {
                                    self.showAlert(alertTitle: "".localized, alertBody: "Please Update Your Personal Details to avail complete OK$ services".localized, alertImage: UIImage(named: "alert-icon")!)
                                }else if UpdateProfileModel.share.business_details != 10 {
                                        self.showAlert(alertTitle: "".localized, alertBody: "Please Update Your Business Details to avail complete OK$ services".localized, alertImage: UIImage(named: "alert-icon")!)
                                    }
                                }
                                }
                            }
                            DispatchQueue.main.async {
                                self.tvUpdateProfile.reloadData()
                            }
                        }
                    }else {
                        print("First time data nil")
                    }
                }
                }catch {}
    }else if screen == "GetUserBusinessInfo" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    
                    if let dic = dict as? Dictionary<String,Any> {
                         println_debug("GetUserBusinessInfo:\(dic)")
                        if dic["Code"] as? Int == 200 {
                             if let data = dic["Data"]  {
                            let dic = OKBaseController.convertToDictionary(text: data as! String)
                                self.businessData = dic
                                self.navigatetoBusinessDetails()
                            }
                        }else {
                            DispatchQueue.main.async {
                                self.showAlert(alertTitle: "Error".localized, alertBody: "Request Failed".localized, alertImage: UIImage(named: "error")!)
                            }
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        self.showAlert(alertTitle: "Error".localized, alertBody: "Request Failed".localized, alertImage: UIImage(named: "error")!)
                    }
                }
            }catch {}
        }else if screen == "GetUserProfilePersonalInfo" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,Any> {
                          println_debug("GetUserProfilePersonalInfo:\(dic)")
                        if dic["Code"] as? Int == 200 {
                            if let data = dic["Data"]  {
                                let dic = OKBaseController.convertToDictionary(text: data as! String)
                                self.personalData = dic
                                self.navigatetoPersonalDetails()
                            }
                        }else {
                            DispatchQueue.main.async {
                                self.showAlert(alertTitle: "Error".localized, alertBody: "Request Failed".localized, alertImage: UIImage(named: "error")!)
                            }
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        self.showAlert(alertTitle: "Error".localized, alertBody: "Request Failed".localized, alertImage: UIImage(named: "error")!)
                    }
                }
            }catch {}
        }else if screen == "mUpdateBusinessStatus" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,Any> {
                        println_debug("mUpdateBusinessStatus:\(dic)")
                        if dic["Code"] as? Int == 200 {
                            DispatchQueue.main.async {
                                if UpdateProfileModel.share.business_closed == 0 {
                                    self.showAlert(alertTitle: "".localized, alertBody: "Your Business Closed request has been enabled successfully".localized, alertImage: UIImage(named: "r_business_name")!)
                                }else { //"Your Business Closed request has been disabled successfully"
                                    self.showAlert(alertTitle: "".localized, alertBody: "Your Business Opened request has been enabled successfully".localized, alertImage: UIImage(named: "r_business_name")!)
                                }
                            }
                            var urlString   = Url.addUpdateProfileDetails
                            urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                            let urlS = getUrl(urlStr: urlString, serverType: .updateProfile)
                            //  let urlS  = URL(string: "http://69.160.4.151:8085/UpdateProfile.svc/AddUpdateProfileMenuStatus")
                            UpdateProfileModel.share.updateProfileMenuStatus(urlStr: urlS , handle:{(success , response) in
                                if success {
                                    DispatchQueue.main.async {
                                        let indexPath = IndexPath(item: 0, section: 2)
                                        self.tvUpdateProfile.reloadRows(at: [indexPath], with: .none)
                                    }
                                }else {
                                }
                            })
                        }else {
                            if UpdateProfileModel.share.business_closed == 0 {
                               UpdateProfileModel.share.business_closed = 1
                            }else {
                              UpdateProfileModel.share.business_closed = 0
                            }
                            DispatchQueue.main.async {
                                let indexPath = IndexPath(item: 0, section: 2)
                                self.tvUpdateProfile.reloadRows(at: [indexPath], with: .none)
                            
                                self.showAlert(alertTitle: "Error".localized, alertBody: "Request Failed".localized, alertImage: UIImage(named: "error")!)
                            }
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        self.showAlert(alertTitle: "Error".localized, alertBody: "Request Failed".localized, alertImage: UIImage(named: "error")!)
                    }
                }
            }catch {}
        }else if screen == "mAllNote" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,Any> {
                        println_debug("mAllNote:\(dic)")
                        if dic["Code"] as? Int == 200 {
                            if let data = dic["Data"]  {
                                let note = OKBaseController.convertToDictionary(text: data as! String)
                                if let cashInNote = note?["CashInServices"] as? [Dictionary<String,Any>] {
                                 UpdateProfileModel.share.CashInServices = cashInNote
                                }
                                if let cashOutNote = note?["CashOutServices"] as? [Dictionary<String,Any>] {
                                    UpdateProfileModel.share.CashOutServices = cashOutNote
                                }
                                
                                if let agent = note?["Agent"] as? [Dictionary<String,Any>] {
                                    UpdateProfileModel.share.Agent = agent
                                }
                            }
                        }else {
                            DispatchQueue.main.async {
                                self.showAlert(alertTitle: "Error".localized, alertBody: "Request Failed".localized, alertImage: UIImage(named: "error")!)
                            }
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        self.showAlert(alertTitle: "Error".localized, alertBody: "Request Failed".localized, alertImage: UIImage(named: "error")!)
                    }
                }
            }catch {}
        }
    }
    
    //MARK: UITABLEVIEW DATA SOURCE & DELEGATE
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else if section == 1 {
            return titleList.count
        }else if section == 2 {
            if UserModel.shared.agentType == .user {
                return 0
            }else {
               return 1
            }
        }else {
            return 1
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section ==  3 {
            return 0
        }else {
        return 60
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 35
        }else if section == 1 {
            if UpdateProfileModel.share.personal_details == 100 && UpdateProfileModel.share.business_details == 100 {
               return 0
            }else {
                return 80
            }
        }else if section == 2 {
            if UserModel.shared.agentType == .user {
                return 0
            }else {
                return 35
            }
        }else {
            return 0
        }
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            var title = ""
            if UserModel.shared.agentType == .user {
                title = "Personal Profile Status".localized
            }else if UserModel.shared.agentType == .merchant {
                title = "Merchant Profile Status".localized
            }else if UserModel.shared.agentType == .advancemerchant {
                title = "Advance Merchant Profile Status".localized
            }else if UserModel.shared.agentType == .agent {
                title = "Agent Profile Status".localized
            }else {
               title = "Merchant Profile Status".localized
            }
            
           return self.createRequiredFieldLabel(headerTitle: title, txtAlignment: .left, gcRect: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 35))
        }else if section == 1 {
            return self.createRequiredFieldLabel(headerTitle: "100 % Profile update \nis required to avail complete OK$ services and be a \n Millionaire".localized, txtAlignment: .center, gcRect: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 80))
        }else if section == 2 {
            return self.createRequiredFieldLabel(headerTitle: "Additional Setting".localized, txtAlignment: .left, gcRect: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 35))
        }else {
            let view = UIView(frame: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 0))
            view.backgroundColor = hexStringToUIColor(hex: "#E0E0E0")
            return view
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpdateProfileProgressCell") as! UpdateProfileProgressCell
            if UserModel.shared.agentType == .user {
            cell.updateProgress(total: 1, scored: UpdateProfileModel.share.personal_details)
            }else {
            cell.updateProgress(total: 2, scored: UpdateProfileModel.share.personal_details + UpdateProfileModel.share.business_details)
            }
            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 1 {
            let cell: PersonalProfileCell = tableView.dequeueReusableCell(withIdentifier: "PersonalProfileCell") as! PersonalProfileCell
            let dic = titleList[indexPath.row]
            
            if indexPath.row == 0 || indexPath.row == 1 {
            let index = dic["status"] as? String ?? ""
            cell.initCellWithData(iconImage: dic["image"] as? String ?? "", title: dic["title"] as? String ?? "", status: index + " %", isStatusHidden: false)
                if Int(index) ?? 0 > 99  {
                 cell.lblStatus.textColor = #colorLiteral(red: 0.1099196896, green: 0.6209999919, blue: 0.004164368846, alpha: 1)
                }else if Int(index) ?? 0 < 100 && Int(index) ?? 0 > 29 {
                 cell.lblStatus.textColor = #colorLiteral(red: 0.1456509531, green: 0.2563888431, blue: 0.6651189923, alpha: 1)
                }else {
                 cell.lblStatus.textColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
                }
            }else if indexPath.row == 2 || indexPath.row == 3 {
                let index = dic["status"] as? Int ?? 0
                var strStatus = ""
                if index == 1 {
                    strStatus = "Enabled".localized
                }else {
                   strStatus = "Disabled".localized
                }
                if indexPath.row == 2 {
                    cell.imgIcon.contentMode = .scaleAspectFill
                }
            cell.initCellWithData(iconImage: dic["image"] as? String ?? "", title: dic["title"] as? String ?? "", status: strStatus, isStatusHidden: false)
            }else if indexPath.row == 5 {
                var agetTitle = ""
                if UpdateProfileModel.share.ok_agent_services == 0 {
                    agetTitle = ">".localized
                }else if UpdateProfileModel.share.ok_agent_services == -1{
                    agetTitle = "".localized
                }else if UpdateProfileModel.share.AgentStatus == 1{
                    let date = getDateFormateSringForJoinDate(date:  UpdateProfileModel.share.AgentValidTo)
                    agetTitle = "Valid Till".localized + " " + date
                }else if UpdateProfileModel.share.AgentStatus == 2{
                    agetTitle = "OK$ Agent Rejected".localized
                }
            cell.initCellWithData(iconImage: dic["image"] as? String ?? "", title: dic["title"] as? String ?? "", status: agetTitle, isStatusHidden: false)
            }else {
                var str = ""
                let index = dic["status"] as? Int ?? 0
                if index == 0 {
                    str = "Disabled".localized
                }else {
                    str = String(index) + " " + "Enabled".localized
                }
                cell.initCellWithData(iconImage: dic["image"] as? String ?? "", title: dic["title"] as? String ?? "", status: str, isStatusHidden: false)
            }

            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 2 {
            let cell:AdditionalSettingCell = tableView.dequeueReusableCell(withIdentifier: "AdditionalSettingCell") as! AdditionalSettingCell
            cell.imgIconAS.image = UIImage(named:"r_merchant")
            cell.btnInfo.addTarget(self, action:#selector(self.infoAction(sender:)), for: .touchUpInside)
            cell.switchBusiness.addTarget(self, action: #selector(self.switchValueDidChange(sender:)), for: .valueChanged)
            cell.cellWrapperData(boolStatus: UpdateProfileModel.share.business_closed)
            cell.selectionStyle = .none
            return cell
        }else {
            let identifier = "BeLuckyCell"
            var cell: BeLuckyCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BeLuckyCell
            if cell == nil {
                tableView.register(UINib(nibName: "BeLuckyCell", bundle: nil), forCellReuseIdentifier: identifier)
                cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? BeLuckyCell
            }
            cell.viewLucky.layer.cornerRadius = 10
            cell.viewLucky.layer.masksToBounds = true
            cell.selectionStyle = .none
            return cell
        }
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 1 {
            if indexPath.row == 0 {
                self.getPersonalDetailsData()
            }else if indexPath.row == 1 {
                self.getBusinessDetailsData()
            }else if indexPath.row == 2 {
                self.getCashInData()
            }else if indexPath.row == 3 {
                self.getCashOutData()
            }else if indexPath.row == 4 {
                self.getOtherServiceData()
            }else if indexPath.row == 5 {
               var checkCount = 0
                if UserModel.shared.idType == "01" {
                    checkCount = 13
                }else {
                    checkCount = 15
                }
                if (UpdateProfileModel.share.personal_details == checkCount) && (UpdateProfileModel.share.business_details == 10) {
                    let VC =  UIStoryboard(name: "MyProfile", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPOkAgentVC") as! UPOkAgentVC
                    self.navigationController?.pushViewController(VC, animated: true)
                }else {
                    DispatchQueue.main.async {
                        self.showAlert(alertTitle: "".localized, alertBody: "You have to complete Your Personal and Business Details to become OK$ Agent".localized, alertImage: UIImage(named: "infoQR")!)
                    }
                }
            }
        }
    }
    
    func getDateFormateSringForJoinDate(date: String)-> String {
        return dateF5.string(from: parseDateJaoinDate(strDate: date))
    }
    
    
    private func getPersonalDetailsData() {
        if appDelegate.checkNetworkAvail() {
            self.showProgressView()
            let paramString = self.loginInfoDictionaryWithOTP() as! [String : Any]
            let web = WebApiClass()
            web.delegate = self
             let urlStr = Url.getPersonalDetails
            let ur = getUrl(urlStr: urlStr, serverType: .updateProfile)
            println_debug(ur)
            println_debug(paramString)
            web.genericClass(url: ur, param: paramString as AnyObject, httpMethod: "POST", mScreen: "GetUserProfilePersonalInfo")
        }
    }
    
    private func navigatetoPersonalDetails() {
        DispatchQueue.main.async {
            let personalDetailVC = UIStoryboard(name: "MyProfile", bundle: Bundle.main).instantiateViewController(withIdentifier: "PersonalDetailsUP") as! PersonalDetailsUP
            personalDetailVC.profileData = self.personalData
            self.navigationController?.pushViewController(personalDetailVC, animated: true)
        }
    }
    
    private func getBusinessDetailsData() {
        if appDelegate.checkNetworkAvail() {
            self.showProgressView()
            let paramString = self.loginInfoDictionaryWithOTP() as! [String : Any]
            let web      = WebApiClass()
            web.delegate = self
              let urlStr = Url.getBusinessDetails
           // let ur = URL(string: "http://69.160.4.151:8085/UpdateProfile.svc/GetMerchantBusinessInfoAndDocs") //
            let ur = getUrl(urlStr: urlStr, serverType: .updateProfile)
            println_debug(ur)
            println_debug(paramString)
            web.genericClass(url: ur, param: paramString as AnyObject, httpMethod: "POST", mScreen: "GetUserBusinessInfo")
        }
    }
    
    private func navigatetoBusinessDetails() {
        DispatchQueue.main.async {
            let VC =  UIStoryboard(name: "MyProfile", bundle: Bundle.main).instantiateViewController(withIdentifier: "BusinessDetailsVC") as! BusinessDetailsVC
            VC.businessData = self.businessData
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
   
    private func getCashInData() {
        CashInModel.share.getServiceData(handle: {(isSuccess,response) in
            if isSuccess {
                self.navigateToCashIn()
            }else {
                alertViewObj.wrapAlert(title: "", body: response, img:#imageLiteral(resourceName: "r_user"))
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                })
                DispatchQueue.main.async {
                    alertViewObj.showAlert(controller: self)
                }
            }
        })
    }
    
    private func navigateToCashIn() {
        DispatchQueue.main.async {
            let VC =  UIStoryboard(name: "OtherService", bundle: Bundle.main).instantiateViewController(withIdentifier: "CashIn_UP") as! CashIn_UP
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
    
    private func getCashOutData()  {
        CashOutModel.share.getServiceData(handle: {(isSuccess,response) in
            if isSuccess {
                self.navigateToCashOut()
            }else {
                alertViewObj.wrapAlert(title: "", body: response, img:#imageLiteral(resourceName: "r_user"))
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                })
                DispatchQueue.main.async {
                    alertViewObj.showAlert(controller: self)
                }
            }
        })
    }
    
    private func navigateToCashOut() {
        DispatchQueue.main.async {
            let VC =  UIStoryboard(name: "OtherService", bundle: Bundle.main).instantiateViewController(withIdentifier: "cashOut_UP") as! cashOut_UP
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
    
    private func getOtherServiceData()  {
                var urlString   = Url.getMerOtherServices
                urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                let urlS = getUrl(urlStr: urlString, serverType: .updateProfile)
       // let urlS = URL(string: "http://69.160.4.151:8085/UpdateProfile.svc/GetMerOtherServices")
        let param = ["MobileNumber" : UserModel.shared.mobileNo,"AppId" : "","Ostype" : 1,"Otp" : "","Simid": UserModel.shared.simID,"Msid" : UserModel.shared.msid] as AnyObject
        println_debug("OtherService:\(param)")
        TopupWeb.genericClass(url:urlS , param: param, httpMethod: "POST", handle:{ (response , success) in
            if success {
                if let json = response as? Dictionary<String,Any> {
                    if let code = json["Code"] as? NSNumber, code == 200 {
                        if let que = json["Data"] as? String {
                            if let dic = OKBaseController.convertToDictionary(text: que) {
                                println_debug(dic)
                                println_debug("OtherService Data:\(dic)")
                                self.otherData = dic
                                self.navigateToOtherService()
                            }
                        }
                    }else {
                        DispatchQueue.main.async {
                        self.showAlert(alertTitle: "Error".localized, alertBody: "Request Failed".localized, alertImage: UIImage(named: "error")!)
                        }
                    }
                }
            }else {
                DispatchQueue.main.async {
                self.showAlert(alertTitle: "Error".localized, alertBody: "Request Failed".localized, alertImage: UIImage(named: "error")!)
                }
            }
        })
    }
    
    private func navigateToOtherService() {
        DispatchQueue.main.async {
            let VC =  UIStoryboard(name: "OtherService", bundle: Bundle.main).instantiateViewController(withIdentifier: "OtherServiceUP") as! OtherServiceUP
            VC.otherServiceData = self.otherData
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }

    private func createRequiredFieldLabel(headerTitle:String, txtAlignment: NSTextAlignment, gcRect: CGRect) ->UIView? {
        let view1 = UIView(frame: CGRect(x: 0, y: 0, width:self.view.frame.width, height: gcRect.height))
        view1.backgroundColor = hexStringToUIColor(hex: "#E0E0E0")
        let lblHeader = UILabel(frame: CGRect(x: 10, y: 0, width: self.view.frame.width - 20, height: view1.frame.height))
        lblHeader.text = headerTitle
        lblHeader.numberOfLines = 3
        lblHeader.textAlignment = txtAlignment
        lblHeader.font = UIFont(name: appFont, size: 14)
        lblHeader.backgroundColor = UIColor.clear
        lblHeader.textColor = UIColor.darkGray
        view1.addSubview(lblHeader)
        return view1
    }

    //Attach file show and hide on table view cell
    @objc func switchValueDidChange(sender: UISwitch) {
        if sender.isOn {
            UpdateProfileModel.share.business_closed = 1
            sender.tintColor = UIColor.init(hex: "3DBA06")
            self.callAPIToPostBusinessStatus()
        }else {
            sender.tintColor = UIColor.init(hex: "FF0715")
            UpdateProfileModel.share.business_closed = 0
            self.callAPIToPostBusinessStatus()
        }
//        let indexPath = IndexPath(item: 0, section: 2)
//        self.tvUpdateProfile.reloadRows(at: [indexPath], with: .none)
    }
    
    @objc func infoAction(sender:UIButton) {
        println_debug("infoAction")
        alertViewObj.wrapAlert(title: nil, body:  "If you turn on/off this option, you can open/close your business temporary. So customer will know easily about your business availability.".localized, img: UIImage(named: "infoQR"))
        alertViewObj.addAction(title: "OK".localized, style: .cancel) {
        }
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func checkPercentOfPersonal() -> Int {
        var countStatus = [Int]()
     
        if UserModel.shared.idType == "01" {
            countStatus = [0,0,0,0,0,0,0,0,0,0,0,0,0] //NRC 18-2-2 = 13
        }else {
            countStatus = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0] //PASSPORT 20-2-2 = 15
        }
        
        if UserModel.shared.proPic != "" {
            countStatus[0] = 1
        }
        if UserModel.shared.name != "" {
            countStatus[1] = 1
        }
        if UserModel.shared.dob != "" {
            countStatus[2] = 1
        }
        if UserModel.shared.gender != "" {
            countStatus[3] = 1
        }
        if UserModel.shared.fatherName != "" {
            countStatus[4] = 1
        }
        if UserModel.shared.idPhoto != "" {
            countStatus[5] = 1
        }
        if UserModel.shared.idPhoto1 != "" {
            countStatus[6] = 1
        }
        if UserModel.shared.nrc != "" {
            countStatus[7] = 1
        }
        if UserModel.shared.addressType == "Home".localized {
            if UserModel.shared.state != "" {
                countStatus[8] = 1
            }
            if UserModel.shared.township != "" {
                countStatus[9] = 1
            }
            if UserModel.shared.address2 != "" {
                countStatus[10] = 1
            }
        }else {
            countStatus[8] = 1
            countStatus[9] = 1
            countStatus[10] = 1
        }
        
        if UserModel.shared.securityQuestionCode != "" {
            countStatus[11] = 1
        }
        if UserModel.shared.securityAnswer != "" {
            countStatus[12] = 1
        }
 
//        if UserModel.shared.email != "" {
//            countStatus[13] = 1
//        }
        if UserModel.shared.idType == "04" {
            if UserModel.shared.countryOfCitizen != "" {
                countStatus[13] = 1
            }
            if UserModel.shared.idExpDate != "" {
                countStatus[14] = 1
            }
        }
        
        var finalCount = 0
        for item in countStatus {
            if item == 1 {
                finalCount += 1
            }
        }
        return finalCount
    }
    
    func checkPercentOfBusiness() -> Int {
        var countStatus = [0,0,0,0,0,0,0,0,0,0]
        
        if UserModel.shared.businessName != "" {
            countStatus[0] = 1
        }
    
        if UserModel.shared.ownershipType != "" {
            countStatus[1] = 1
        }
        
        if UserModel.shared.logoImages != "" {
            countStatus[2] = 1
        }
        if UserModel.shared.businessType != "" {
            countStatus[3] = 1
        }
        if UserModel.shared.businessCate != "" {
            countStatus[4] = 1
        }
        
        if UserModel.shared.businessIdPhoto1 != "" {
            countStatus[5] = 1
        }
        if UserModel.shared.businessIdPhoto2 != "" {
            countStatus[6] = 1
        }
        
        if UserModel.shared.b_Division == "" {
            countStatus[7] = 1
            countStatus[8] = 1
            countStatus[9] = 1
        }else {
            if UserModel.shared.b_Division != "" {
                countStatus[7] = 1
            }
            
            if UserModel.shared.b_Township != "" {
                countStatus[8] = 1
            }
            
            if UserModel.shared.b_Street != "" {
                countStatus[9] = 1
            }
            
        }
        
        var finalCount = 0
        for item in countStatus {
            if item == 1 {
                finalCount += 1
            }
        }
        return finalCount
    }
    
}


class PersonalProfileCell: UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblName.font = UIFont(name: appFont, size: 15)
        lblStatus.font = UIFont(name: appFont, size: 14)
    }
    
    func initCellWithData(iconImage: String?, title: String?, status: String?, isStatusHidden: Bool?) {
        imgIcon.image = UIImage(named: iconImage ?? "Hi")
        lblName.text = title ?? ""
        lblStatus.text = status ?? ""
        lblStatus.isHidden = isStatusHidden ?? false
    }
}
class UpdateProfileProgressCell: UITableViewCell {
    @IBOutlet weak var progress: UIProgressView?
    @IBOutlet weak var lblstatus: UILabel!
    @IBOutlet weak var lblGetPercentage: UILabel!
    var count = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if UserModel.shared.agentType == .user  {
            //Personal
            if UserModel.shared.idType == "01" {
                count = 13
                //NRC 18
            }else {
                //PASSPORT 20
                count = 15
            }
        }else {
            // Merchant
            if UserModel.shared.idType == "01" {
                count = 13 + 10 // NRC 17
            }else {
                count = 15 + 10 // PAs 19
            }
        }
    }
    func updateProgress(total: Int, scored: Int) {
        let score : Double = Double(scored)
        let count1 : Double = Double(count)
         let percent : Double = Double((score / count1) * 100)
        let finalPercent : Double = Double((score / count1) * 100)
        lblstatus.text = String(scored) + "/" + String(count) + " ( " + String(format: "%.0f%", finalPercent) + "% " + ")"
        progress?.progress = Float(percent/100)
        print("finalPercent = \(finalPercent)")
        if finalPercent > 99.0 {
           progress?.progressTintColor = #colorLiteral(red: 0.1099196896, green: 0.6209999919, blue: 0.004164368846, alpha: 1)
            println_debug("Green Color")
        }else if finalPercent < 100.0 && finalPercent > 29.0 {
            progress?.progressTintColor = #colorLiteral(red: 0.1456509531, green: 0.2563888431, blue: 0.6651189923, alpha: 1)
            println_debug("Blue Color")
        }else {
            progress?.progressTintColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
            println_debug("Red Color")
        }
        print("Integer finalPercent = \(Int(finalPercent))")
    }
    
}
