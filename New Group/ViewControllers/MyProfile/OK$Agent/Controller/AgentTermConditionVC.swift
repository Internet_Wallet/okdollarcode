//
//  AgentTermConditionVC.swift
//  OK
//
//  Created by gauri on 10/4/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol AgentTermConditionVCDelegate : class {
    func TermsAndConditionsAccpeted(vc : UIViewController)
    func TermsAndConditionsRejected(vc : UIViewController)
}


class AgentTermConditionVC: UIViewController {
    weak var delegate : AgentTermConditionVCDelegate?
    @IBOutlet weak var btnReject: UIButton!{
        didSet {
            btnReject.setTitle(" Reject".localized, for: .normal)
        }
    }
    @IBOutlet weak var btnAccept: UIButton!{
        didSet {
            btnAccept.setTitle(" Accept".localized, for: .normal)
        }
    }
    
    @IBOutlet weak var wViewTC : UIWebView! {
        didSet {
            wViewTC.loadRequest(URLRequest(url: URL(string: "www.google.com")!))
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
         do {
         var filename = ""
         if UserModel.shared.language == "my" || UserModel.shared.language == "MY" || UserModel.shared.language == "MYA" || UserModel.shared.language == "mya"{
         filename = "agentTerm_Burmese"
         }else if UserModel.shared.language == "en" {
         filename = "agentTerm_Englsih"
         }else {
           filename = "agentTerm_Burmese_Unicode"
        }
         guard let filePath = Bundle.main.path(forResource: filename, ofType: "html") else { return }
         
         let contents =  try String(contentsOfFile: filePath, encoding: .utf8)
         let baseUrl = URL(fileURLWithPath: filePath)
         self.wViewTC.loadHTMLString(contents as String, baseURL: baseUrl)
         }catch { print ("File HTML error") }
        setMarqueLabelInNavigation()
    }
    
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: "Zawgyi-One", size: 16)
        lblMarque.text = "Terms & Conditions".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
    }
    
    @IBAction func onClickBack(_ sender : Any) {
      self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickReject(_ sender : Any) {
        if let del = delegate {
            del.TermsAndConditionsRejected(vc: self)
        }
    }
    
    @IBAction func onClickAccept(_ sender : Any) {
        if let del = delegate {
            del.TermsAndConditionsAccpeted(vc: self)
        }
    }
    

}
