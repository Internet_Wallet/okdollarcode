//
//  FileDownload.swift
//  OK
//
//  Created by gauri on 10/5/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import MobileCoreServices

protocol FileDownloadDelegate : class {
    func downloadedFile(byteWritten : Float)
    func downloadedFileStatus(status : Bool)
    func formUploadedStatus(status : Bool)
}



class FileDownload: OKBaseController, URLSessionDownloadDelegate {

    weak var delegate : FileDownloadDelegate?
    var url : URL?
    var fileName: String?
    var defaultSession: URLSession!
    var downloadTask: URLSessionDownloadTask!
    var persentOnControll = UIViewController()
    var agentAppFormIMG : UIImage?
    var uploadAgentURL : String?
    
    func startDownloading (url:URL) {
        showProgressView()
        self.url = url
        let backgroundSessionConfiguration = URLSessionConfiguration.background(withIdentifier: "backgroundSession")
        defaultSession = Foundation.URLSession(configuration: backgroundSessionConfiguration, delegate: self, delegateQueue: OperationQueue.main)
        downloadTask = defaultSession.downloadTask(with: url)
        downloadTask.resume()
    }
    
    func downloadingCancel() {
        downloadTask.cancel()
    }
    
    // MARK:- URLSessionDownloadDelegate
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        let urlString = url?.absoluteString
        let ext = urlString?.components(separatedBy: ".")
        let laststring = ext?.last ?? "pdf"
        let nam = fileName ?? "Downloaded_File"
        let f_name = nam + "." + laststring
        print("File Name: \(f_name)")
        let destinationUrl = documentsUrl!.appendingPathComponent(f_name)
        let dataFromURL = NSData(contentsOf: location)
        dataFromURL?.write(to: destinationUrl, atomically: true)
       
        if let del = delegate {
            removeProgressView()
            del.downloadedFileStatus(status: true)
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        if let del = delegate {
            del.downloadedFile(byteWritten: Float(totalBytesWritten)/Float(totalBytesExpectedToWrite))
        }
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        removeProgressView()
        downloadTask = nil
        if (error != nil) {
            if let del = delegate {
                del.downloadedFileStatus(status: false)
            }
        }
    }
}

extension FileDownload {
        
    func imageUpload(imgName : String, fileData : Data) {
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            let url = getUrl(urlStr: "phones/\(UserModel.shared.mobileNo)/devices/\(uuid)/files", serverType: .mediaUploadFilesUrl)
            println_debug(url)
            let request = NSMutableURLRequest(url:url as URL);
            request.httpMethod = "POST";
            let boundary = "Boundary-\(uuid)"
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            request.httpBody = createBodyWithParameters(parameters: nil, filePathKey: imgName, imageDataKey: fileData as Data, boundary: boundary) as Data
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                (data, response, error) in
                progressViewObj.removeProgressView()
                
                if error != nil {
                    if let del = self.delegate {
                        del.formUploadedStatus(status: false)
                    }
                    return
                }
                println_debug(response)
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 201 {
                    println_debug("statusCode should be 200, but is \(httpStatus.statusCode)")
              
                    self.uploadAgentURL = httpStatus.allHeaderFields["Location"] as? String ?? ""
                    println_debug(self.uploadAgentURL)
                    self.agentAppFormIMG = UIImage(data: fileData)
                    if let del = self.delegate {
                        del.formUploadedStatus(status: true)
                    }
                }
            }
            task.resume()
        }
    }
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> NSData {
        var body = Data()
        if parameters != nil {
            for (key, value) in parameters! {
                body.append(Data("--\(boundary)\r\n".utf8))
                body.append(Data("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".utf8))
                body.append(Data("\(value)\r\n".utf8))
            }
        }
        
        var filename = ""
        if filePathKey != "" {
            let fileArray = filePathKey?.components(separatedBy: "/")
            filename = (fileArray?.last)!
        }
        let mimetype = "multipart/form-data"
        body.append(Data("--\(boundary)\r\n".utf8))
        body.append(Data("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n".utf8))
        body.append(Data("Content-Type: \(mimetype)\r\n\r\n".utf8))
        body.append(imageDataKey as Data)
        body.append(Data("\r\n".utf8))
        body.append(Data("--\(boundary)--\r\n".utf8))
        return body as NSData
    }
}

extension UIImage {
    func resize(_ image: UIImage) -> UIImage {
        var actualHeight = Float(image.size.height)
        var actualWidth = Float(image.size.width)
        let maxHeight: Float = 300.0
        let maxWidth: Float = 400.0
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        let compressionQuality: Float = 0.5
        //50 percent compression
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        let rect = CGRect(x: 0.0, y: 0.0, width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = img!.jpegData(compressionQuality: CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        return UIImage(data: imageData!) ?? UIImage()
    }
}
