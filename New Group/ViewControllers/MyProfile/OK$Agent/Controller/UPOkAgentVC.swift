//
//  UPOkAgentVC.swift
//  OK
//
//  Created by PC on 12/9/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

//

import UIKit
import MobileCoreServices
class UPOkAgentVC: OKBaseController,UITextFieldDelegate,WebServiceResponseDelegate,FileDownloadDelegate,AgentTermConditiosCellDelegate {

    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var tblMainList: UITableView!
    @IBOutlet weak var viewDownloading: UIView! {
        didSet {
            viewDownloading.isHidden = true
        }
    }
    
    @IBOutlet weak var bottomConstaints: NSLayoutConstraint!
    @IBOutlet weak var lblDownloadingWait: UILabel! {
        didSet {
            lblDownloadingWait.text = "Downloading, Please Wait!".localized
        }
    }
    @IBOutlet weak var btnDownloadCancel: UIButton! {
        didSet {
            btnDownloadCancel.setTitle("Cancel".localized, for: .normal)
        }
    }
    @IBOutlet weak var lblRatioPercent: UILabel!
    @IBOutlet weak var progressDownload: UIProgressView!
    
    //Manage Image Data
    var imageFile: UIImage!
    var imageFileURL = ""
    var boolEdit:Bool = false
    //Manage Model Data
    var model = UPOkAgentModel()
    let imagePickerGallery = UIImagePickerController()
    let download = FileDownload()
    
    //MARK: - View Life Cycle
    var rowSection = 6
    override func viewDidLoad() {
        super.viewDidLoad()

        tblMainList.register (UINib(nibName: "PAMyStudentSupportCell", bundle: nil), forCellReuseIdentifier: "PAMyStudentSupportCell")
        progressDownload.transform = progressDownload.transform.scaledBy(x: 1, y: 10)
        download.delegate = self
        self.title = "OK$ Agent".localized
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20)!, NSAttributedString.Key.foregroundColor:  UIColor.init(hex: "#1C2C99")]
        
        model.readTC = false
        self.btnUpdate.setTitle("Submit".localized, for: .normal)
        if model.AgentStatus == -1 || model.AgentStatus == 2 {
             self.loadIntialData()
        }
        self.callGetOK$AgentAPI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    func loadIntialData() {
        let note = UpdateProfileModel.share.Agent
        let main = note?[0]
        println_debug(note)
        var str = ""
        if ok_default_language == "my" {
        let notStr = main?["My"] as? String ?? ""
           str =  notStr
            alertViewObj.wrapAlert(title: "Note".localized, Arrtibutedbody: str, img: UIImage(named: "hi"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel) { }
            alertViewObj.showAlert(controller: self)
        }else if ok_default_language == "en"{
            let notStr = main?["En"] as? String ?? ""
            str =  notStr
            alertViewObj.wrapAlert(title: "Note".localized, Arrtibutedbody: str, img: UIImage(named: "hi"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel) { }
            alertViewObj.showAlert(controller: self)
        }else {
                let notStr = main?["UniCode"] as? String ?? ""
                str =  notStr
            alertViewObj.wrapAlert(title: nil, body: str, img: #imageLiteral(resourceName: "info_one"))
            alertViewObj.addAction(title: "Cancel".localized, style: .cancel) { }
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                self.navigationController?.popViewController(animated: true)
            }
            alertViewObj.showAlert(controller: self)
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        if(boolEdit) {
            alertViewObj.wrapAlert(title: nil, body: "Clicking back without update will clear all entered details, Are you sure to exit?".localized, img: #imageLiteral(resourceName: "info_one"))
            alertViewObj.addAction(title: "Cancel".localized, style: .cancel) { }
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                self.navigationController?.popViewController(animated: true)
            }
            alertViewObj.showAlert(controller: self)
        }else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //Handle Navigation Bar
    func navigationbarController(toEnable: Bool) {
        if toEnable {
            self.navigationController?.navigationBar.layer.zPosition = 0
            self.title = "OK$ Agent".localized
            self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
        } else {
            self.navigationController?.navigationBar.layer.zPosition = -64
        }
        self.navigationController?.navigationBar.isUserInteractionEnabled = toEnable
    }
    
    // MARK: - API Methods
    func callGetOK$AgentAPI() {
        if appDelegate.checkNetworkAvail() {
            showProgressView()
            let dictLoginUserInfo = ["AppId": UserModel.shared.appID,"Limit": 0,"MobileNumber": UserModel.shared.mobileNo,"Msid": msid,"Offset": 0,"Ostype": 1,"Otp": uuid,"Simid": UserModel.shared.simID] as [String : Any]
            let paramString : [String:Any] = ["CategoryName": "AGENTAUTHLICINFO","Login": dictLoginUserInfo ]
            let web      = WebApiClass()
            web.delegate = self
            let urlStr   = Url.getOK$AgentDetails
            let ur = getUrl(urlStr: urlStr, serverType: .updateProfile)
            println_debug("GET Aget Details URL: \(ur)")
            web.genericClass(url: ur, param: paramString as AnyObject, httpMethod: "POST", mScreen: "mOK$AgentDetails")
        }
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        self.removeProgressView()
        if screen == "mOK$AgentDetails" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        if dic["Code"] as? NSNumber == 200 {
                            let dictData = dic["Data"]
                            let dic = OKBaseController.convertToDictionary(text: dictData as! String)
                            println_debug(dic)
                            self.model.BusinessName = dic?["BusinessName"] as? String ?? ""
                            self.model.BranchOrOutletName = dic?["BranchOrOutletName"] as? String ?? ""
                            self.model.AgentStatus = dic?["AgentStatus"] as? Int ?? 0
                            self.model.AgentSince = dic?["AgentSince"] as? String ?? ""
                            self.model.AgentType = dic?["AgentType"] as? String ?? ""
                            self.model.UserMembership = dic?["UserMembership"] as? String ?? ""
                            self.model.agentUploadFromURL = dic?["AttachedAgentAppFormImgUrl"] as? String ?? ""
                            self.model.AgentTermsAndConditionEn = dic?["AgentTermsAndConditionEn"] as? String ?? ""
                            self.model.AgentTermsAndConditionMy = dic?["AgentTermsAndConditionMy"] as? String ?? ""
                            self.model.AgentTermsAndConditionUni = dic?["AgentTermsAndConditionMyUnicode"] as? String ?? ""
                            self.model.ApplicationFormUrlEn = dic?["ApplicationFormUrlEn"] as? String ?? ""
                            self.model.ApplicationFormUrlMy = dic?["ApplicationFormUrlMy"] as? String ?? ""
                            self.model.ApplicationFormUrlUni = dic?["ApplicationFormUrlMyUnicode"] as? String ?? ""
                           
                            DispatchQueue.main.async {
                                if self.model.AgentStatus == -1 ||  self.model.AgentStatus == 2 {
                                    self.btnUpdate.isHidden = false
                                    self.bottomConstaints.constant = 0
                                }else {
                                    self.btnUpdate.isHidden = true
                                    self.bottomConstaints.constant = -50
                                    self.model.readTC = true
                                }
                              self.tblMainList.reloadData()
                            }
                        }else {
                            println_debug("Error found")
                        }
                    }
                }
            } catch { }
        }
    }
    
    @IBAction func onClickCancelDownload(_ sender: Any) {
        self.viewDownloading.isHidden = true
    }
    
    @IBAction func onClickUpdate(_ sender: Any) {
        self.checkRequiredFields(handler: {(succes) in
            if succes {
                if appDelegate.checkNetworkAvail() {
                    let params = self.model.agentUpdateDetails()
                    let web      = WebApiClass()
                    web.delegate = self
                    let urlStr   = Url.updateOK$AgentDetails
                    let ur = getUrl(urlStr: urlStr, serverType: .updateProfile)
                    println_debug(ur)
                    println_debug(params)
                    TopupWeb.genericClass(url: ur, param: params as AnyObject, httpMethod: "POST", handle: {(resp, success) in
                        if success {
                            if let response = resp as? Dictionary<String,AnyObject> {
                                println_debug(response)
                                if response["Code"] as? Int == 200 {
                                    
                                    UpdateProfileModel.share.ok_agent_services = self.model.AgentStatus
                                    var urlString   = Url.addUpdateProfileDetails
                                    urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                                    let urlS = getUrl(urlStr: urlString, serverType: .updateProfile)
                                     UpdateProfileModel.share.updateProfileMenuStatus(urlStr: urlS , handle:{(success , response) in
                                        if success {
                                            println_debug(response)
                                            alertViewObj.wrapAlert(title:"", body: "Your OK$ Agent Form Submitted Successfully".localized, img: UIImage(named: "info_success")!)
                                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                                self.navigationController?.popViewController(animated: true)
                                            })
                                            DispatchQueue.main.async {
                                                alertViewObj.showAlert(controller: self)
                                            }
                                        }else {
                                            println_debug("Failed")
                                        }
                                    })
                                    
                                }
                            }
                        }
                    })
                }
            }
        })
    }
    
    
    private func checkRequiredFields(handler : @escaping (_ allFilled : Bool) -> Void) {
        DispatchQueue.main.async {
            if self.model.agentUploadFromURL != "" {
            if self.model.readTC {
                handler(true)
            }else{
                DispatchQueue.main.async {
                    self.showAlert(alertTitle: "", alertBody: "Terms and Conditions not accepted".localized, alertImage: UIImage(named: "error")!)
                    handler(false)}
            }
        }else {
            DispatchQueue.main.async {
                self.showAlert(alertTitle: "", alertBody: "Agent Application form did not uploaded".localized, alertImage: UIImage(named: "error")!)
                handler(false)}
        }
        }
    }
    
    func getDateFormateSring(date: String)-> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd-MMM-yyyy"
        let dt = dateFormatterGet.date(from:date)
        let finalDate = dateFormatterGet.string(from: dt ?? Date())
        return finalDate
    }
    
    func imageStateAgent() -> UIImage {
        let status = String(model.AgentStatus)
        if status == "0" {
           //Need to Verify
            //Yellow
            return UIImage(named: "document")!
        }else if status == "-1"{
            //Incomplete Information
            //Gray
           return UIImage(named: "document")!
        }else if status == "1"{
            //Verified
           return UIImage(named: "verify")!
        }else if status == "2"{
            //Rejected
            return UIImage(named: "rejectAgent")!
        }else if status == "3" {
            //Need to attach document
            //Gray
           return UIImage(named: "document")!
        }else {
           return UIImage(named: "document")!
        }
    }
    
    func getDateFormateSringForJoinDate(date: String)-> String {
        return dateF5.string(from: parseDateJaoinDate(strDate: date))
    }
}

extension UPOkAgentVC : UITableViewDataSource,UITableViewDelegate {
    public func numberOfSections(in tableView: UITableView) -> Int {
        return rowSection
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
          return 1
        }else if section == 1 {
           return 1
        }else if section == 2 {
            return 2
        }else if section == 3 {
             return 0
        }else if section == 4 {
            return 2
        }else if section == 5 {
            return 1
        }else {
            return 0
        }
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            var date = ""
            if model.AgentSince == ""{
                date = ""
            }else {
                date = "Since " + getDateFormateSringForJoinDate(date:  model.AgentSince)
            }
            
            let aBusinessOutletCell = tableView.dequeueReusableCell(withIdentifier: "AgentBusinessOutletCell") as! AgentBusinessOutletCell
            var member = ""
            if model.UserMembership.contains(find: "Member") {
                member = model.UserMembership
            }else {
                member = model.UserMembership + " Member"
            }
            aBusinessOutletCell.wrapData(businessName: model.BusinessName , outletName: model.BranchOrOutletName , sinceTime: date, memberType: member ,businessImg: UIImage(named: "verify")!)
                aBusinessOutletCell.imgBusiness.image = imageStateAgent()
            if model.AgentStatus == 1 {
              aBusinessOutletCell.imgBusiness.isHidden = true
            }else {
            aBusinessOutletCell.imgBusiness.isHidden = true
            }
        
            return aBusinessOutletCell
        }else if indexPath.section == 1 {
            if indexPath.row == 0 {
                let aAppFormCell = tableView.dequeueReusableCell(withIdentifier: "AgentFormCell") as! AgentFormCell
                if model.agentUploadFromURL != "" {
                    let imageURL:URL = URL(string: model.agentUploadFromURL)!
                    let data = NSData(contentsOf: imageURL)
                    let imgeView = UIImageView()
                    imgeView.image = UIImage(data: data! as Data)
                    //imgeView.sd_setImage(with: urlImage, placeholderImage: UIImage(named: "agent_camera"))
                   // aAppFormCell.btnCamera.setImage(imgeView.image!, for: .normal)
                    model.agentFormImage = imgeView.image!
                }
                 aAppFormCell.btnCamera.setImage(model.agentFormImage, for: .normal)
                if model.agentUploadFromURL == "" {
                    aAppFormCell.lblAttachAgentAppForm.text = "Upload Agent Application Form".localized
                }else {
                    aAppFormCell.lblAttachAgentAppForm.text = "Agent Application Form Attached".localized
                }
                aAppFormCell.btnAgentAppForm.addTarget(self, action: #selector(onClickAppFormURL(_:)), for: .touchUpInside)
                aAppFormCell.btnAgentAppForm.stringTag = "AgentForm"
                aAppFormCell.btnCamera.addTarget(self, action: #selector(onClickUploadFrom(_:)), for: .touchUpInside)
                aAppFormCell.btnAttachAgentAppForm.addTarget(self, action: #selector(onClickUploadFrom(_:)), for: .touchUpInside)
                return aAppFormCell
            }
        }else if indexPath.section == 2 {
            if indexPath.row == 0 {
                let agentTCCell = tableView.dequeueReusableCell(withIdentifier: "AgentDownloadTermsConditionCell") as! AgentDownloadTermsConditionCell
                agentTCCell.btnDownloadAgentTC.addTarget(self, action: #selector(onClickAppFormURL(_:)), for: .touchUpInside)
                agentTCCell.btnDownloadAgentTC.stringTag = "TermsConditions"
                return agentTCCell
            }else {
                let aTCCell = tableView.dequeueReusableCell(withIdentifier: "AgentTermConditiosCell") as! AgentTermConditiosCell
                aTCCell.delegate = self
                if model.AgentStatus == -1 ||  model.AgentStatus == 2 {
                aTCCell.btnReadTermsCondtions.isUserInteractionEnabled = true
                }else {
                aTCCell.btnReadTermsCondtions.isUserInteractionEnabled = false
                }
                aTCCell.wrapData(termsConditionStatus: model.readTC)
                return aTCCell
            }
        }
        else if indexPath.section == 4 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HelpSupportUPCell") as! HelpSupportUPCell
                cell.imgIcon.image = UIImage(named: "approved_safety_cashier_st")
                cell.lblTitle.text = "Agent Benefits".localized
                cell.btnAction.tag = indexPath.row
                cell.btnAction.addTarget(self, action: #selector(onClickHelpSupport(_:)), for: .touchUpInside)
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HelpSupportUPCell") as! HelpSupportUPCell
                cell.imgIcon.image = UIImage(named: "help_call_back")
                cell.lblTitle.text = "Help & Support".localized
                cell.btnAction.tag = indexPath.row
                cell.btnAction.addTarget(self, action: #selector(onClickHelpSupport(_:)), for: .touchUpInside)
                return cell
            }
        }else {
            let aNoteCell = tableView.dequeueReusableCell(withIdentifier: "AgentNoteCell") as! AgentNoteCell
            return aNoteCell
        }
        let aNoteCell = tableView.dequeueReusableCell(withIdentifier: "AgentNoteCell") as! AgentNoteCell
        return aNoteCell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 100
        }else if indexPath.section == 1 {
            return 110
        }else if indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 4 {
            return 55
        }else if indexPath.section == 5 {
             return 270
        }else {
           return 0
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 || section == 1{
            return 35
        }else {
            return 0
        }
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            return self.createRequiredFieldLabel(headerTitle: "Apply for OK$ Agent".localized, reqTitle: "".localized)
        }else if section == 0 {
            var title = ""
            if UserModel.shared.agentType == .merchant {
                title = "Merchant Information".localized
            }else if UserModel.shared.agentType == .advancemerchant {
                title = "Advance Merchant Information".localized
            }else if UserModel.shared.agentType == .agent {
                title = "Agent Information".localized
            }
        return self.createRequiredFieldLabel(headerTitle: title, reqTitle: "Required fields*".localized)
        }else {
            let myview = UIView(frame: CGRect(x: 0, y: 0, width:0, height: 0))
            return myview
        }
    }
    
    private func createRequiredFieldLabel(headerTitle:String, reqTitle: String) ->UIView? {
        let view1 = UIView(frame: CGRect(x:0, y: 0, width:self.view.frame.width, height: 35))
        view1.backgroundColor = hexStringToUIColor(hex: "#E0E0E0")
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: (sizeOfString(myString: headerTitle)?.width)!, height:35))
        lblHeader.text = headerTitle
        lblHeader.textAlignment = .right
        lblHeader.font = UIFont(name: "Zawgyi-One", size: 14)
        lblHeader.backgroundColor = UIColor.clear
        lblHeader.textColor = UIColor.darkGray
        
        let reqLabel = UILabel(frame: CGRect(x: self.view.frame.width - (sizeOfString(myString: reqTitle)?.width)! - 10, y: 0, width: (sizeOfString(myString: reqTitle)?.width)!, height:35))
        reqLabel.text = reqTitle
        reqLabel.textAlignment = .right
        reqLabel.font = UIFont(name: "Zawgyi-One", size: 14)
        reqLabel.backgroundColor = UIColor.clear
        reqLabel.textColor = UIColor.red
        view1.addSubview(lblHeader)
        view1.addSubview(reqLabel)
        return view1
    }
    
    private func sizeOfString(myString : String) ->CGSize? {
        let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: "Zawgyi-One", size: 16) ?? UIFont.systemFont(ofSize: 16.0) ])
        return stringSize
    }
    //342583
    func showTermsConditionPopUp() {
        let tcViewControler = self.storyboard?.instantiateViewController(withIdentifier: "AgentTermConditionVC") as! AgentTermConditionVC
        tcViewControler.delegate = self
        self.navigationController?.pushViewController(tcViewControler, animated: true)
    }
    
    //MARK: - Download form Custom Methods
    func downloadAttachFormAction(urlString : String) {
        //viewDownloading.isHidden = false
        progressDownload.setProgress(0.0, animated: true)
        let fileURL = URL(string: urlString)
        download.startDownloading(url: fileURL!)
    }
    
    func downloadedFile(byteWritten : Float) {
        progressDownload.setProgress(byteWritten, animated: true)
        lblRatioPercent.text =  "\(byteWritten)/100"
    }
    
    func downloadedFileStatus(status : Bool) {
        //viewDownloading.isHidden = true
        self.showAlert(alertTitle: "File downloaded successfully".localized, alertBody: "Path: Files -> On My iPhone -> OK$".localized, alertImage: UIImage(named: "info_success")!)
    }
  
    func formUploadedStatus(status: Bool) {
        if status {
            DispatchQueue.main.async {
                self.model.agentFormImage = self.download.agentAppFormIMG!
                if self.download.uploadAgentURL != "" {
                    self.model.agentUploadFromURL = self.download.uploadAgentURL?.replacingOccurrences(of: " ", with: "%20") ?? ""
                }
                self.showAlert(alertTitle: "", alertBody: "Form uploaded successfully".localized, alertImage: UIImage(named: "info_success")!)
                self.tblMainList.beginUpdates()
                let indexset : IndexSet = [1]
                self.tblMainList.reloadSections(indexset, with: .automatic)
                self.tblMainList.endUpdates()
            }
        }else {
            model.agentUploadFromURL = ""
            self.showAlert(alertTitle: "", alertBody: "Form uploading failed, Please try again".localized, alertImage: UIImage(named: "error")!)
        }
    }
    
    @objc func btnBenefitsAction(sender:UIButton) {
        let controller:PAWebViewBenefitsVC =  UIStoryboard(name: "MyProfile", bundle: Bundle.main).instantiateViewController(withIdentifier: "PAWebViewBenefitsVC") as! PAWebViewBenefitsVC
        if appDel.currentLanguage == "my" {
            controller.urlStr = "ok_agent_benefit_burmese"
        }else if appDel.currentLanguage == "en"{
            controller.urlStr = "ok_agent_benefit_english"
        }else {
            controller.urlStr = "ok_agent_benefit_burmese_unicode"
        }
        controller.strTitleHeader = "Agent Benefits".localized
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func btnHelpSupportAction(sender:UIButton) {
        let viewController = UIStoryboard(name: "HelpSupport", bundle: Bundle.main).instantiateViewController(withIdentifier: "HelpNavigation")
        self.present(viewController, animated: true, completion: nil)
    }
    
    @objc func onClickHelpSupport(_ sender: UIButton) {
        if sender.tag == 0 {
            let controller:PAWebViewBenefitsVC =  UIStoryboard(name: "MyProfile", bundle: Bundle.main).instantiateViewController(withIdentifier: "PAWebViewBenefitsVC") as! PAWebViewBenefitsVC
            if appDel.currentLanguage == "my" {
                controller.urlStr = "ok_agent_benefit_burmese"
            }else if appDel.currentLanguage == "en" {
                controller.urlStr = "ok_agent_benefit_english"
            }else {
                controller.urlStr = "ok_agent_benefit_burmese_unicode"
            }
            controller.strTitleHeader = "Agent Benefits".localized
            self.navigationController?.pushViewController(controller, animated: true)
        }else {
            let viewController = UIStoryboard(name: "HelpSupport", bundle: Bundle.main).instantiateViewController(withIdentifier: "HelpNavigation")
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    @objc func onClickAppFormURL(_ sender: UIButton) {
        DispatchQueue.main.async {
            if let window = UIApplication.shared.keyWindow {
                                let termsConditionsRVC  = self.storyboard?.instantiateViewController(withIdentifier: "LanguageSelectAgentForm") as! LanguageSelectAgentForm
                self.addChild(termsConditionsRVC)
                window.rootViewController?.addChild(termsConditionsRVC)
                termsConditionsRVC.view.frame = .init(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
                termsConditionsRVC.delegate = self
                termsConditionsRVC.input = sender.stringTag
                window.addSubview(termsConditionsRVC.view)
                window.makeKeyAndVisible()
            }
        }
    }
    
    @objc func onClickUploadFrom(_ sender: UIButton) {
       if model.AgentStatus == -1 || model.AgentStatus == 2 {
            self.fileUpload()
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
}


class AgentBusinessOutletCell : UITableViewCell {
    @IBOutlet weak var lblBusinessName : UILabel!
    @IBOutlet weak var lblOutletName : UILabel!
    @IBOutlet weak var lblSinceTime : UILabel!
    @IBOutlet weak var lblMemmer: UILabel!
    @IBOutlet weak var imgBusiness : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func wrapData(businessName: String, outletName: String ,sinceTime :String, memberType : String, businessImg : UIImage) {
        lblBusinessName.text = businessName
        lblOutletName.text = outletName
        lblSinceTime.text = sinceTime
        lblMemmer.text = memberType
        imgBusiness.image = businessImg
    }
    
}



class AgentFormCell : UITableViewCell {
    @IBOutlet weak var btnAgentAppForm: UIButton!
    @IBOutlet weak var lblAgentAppForm: MarqueeLabel! {
        didSet {
          lblAgentAppForm.text = "Download Agent Application Form".localized
        }
    }
    @IBOutlet weak var btnAttachAgentAppForm: UIButton!
    @IBOutlet weak var lblAttachAgentAppForm: MarqueeLabel!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var imgAgentForm: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //btnAgentAppForm.isUserInteractionEnabled = false
    }
    
}

class AgentDownloadTermsConditionCell : UITableViewCell {
    @IBOutlet weak var btnDownloadAgentTC : UIButton!
    @IBOutlet weak var lblDownloadAgentTc: MarqueeLabel! {
        didSet {
            lblDownloadAgentTc.text = "Download Agent Terms & Conditions".localized
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}


class AgentTermConditiosCell : UITableViewCell {
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var lblTnC: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var btnReadTermsCondtions: UIButton!
    weak var delegate : AgentTermConditiosCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    @IBAction func onClickReadTnC(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if !sender.isSelected {
            btnCheck.isSelected = false
            lblTnC.text = "Read and Accept Terms & Conditions".localized
            lblTnC.textColor = .black
            bgView.backgroundColor = UIColor.init(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
        }else {
            if let del = delegate {
                del.showTermsConditionPopUp()
            }
        }
    }
    
    func wrapData(termsConditionStatus : Bool) {
        btnCheck.isSelected = termsConditionStatus
        if termsConditionStatus {
            lblTnC.text = "Terms & Conditions Accepted".localized
            lblTnC.textColor = .white
            bgView.backgroundColor = UIColor.init(red: 22.0/255.0, green: 150.0/255.0, blue: 3.0/255.0, alpha: 1)
        }else {
            lblTnC.text = "Read and Accept Terms & Conditions".localized
            lblTnC.textColor = .black
            bgView.backgroundColor = UIColor.init(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
        }
    }
    
}

class AgentStatusCell : UITableViewCell {
    @IBOutlet weak var tfStatus: SkyFloatingLabelTextField!
    @IBOutlet weak var imgStatus: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func wrapData(agentStatus: String) {
        if agentStatus == "0" {
             tfStatus.text = "Need to Verify".localized
             imgStatus.image = UIImage(named: "document")
        }else if agentStatus == "-1"{
            tfStatus.attributedText = textColorChanged(txt: "Incomplete Information".localized)
            imgStatus.image = UIImage(named: "rejectAgent")
        }else if agentStatus == "1"{
             tfStatus.text = "Verified".localized
             imgStatus.image = UIImage(named: "verifyed_green")
        }else if agentStatus == "2"{
             tfStatus.attributedText = textColorChanged(txt: "Rejected".localized)
             imgStatus.image = UIImage(named: "rejectAgent")
        }else if agentStatus == "3" {
            tfStatus.text = "Need to attach document".localized
            imgStatus.image = UIImage(named: "document")
        }
    }
    
    private func textColorChanged(txt: String) -> NSAttributedString {
        let attributedString = NSMutableAttributedString.init(string: txt)
        let range = (txt as NSString).range(of: txt)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: range)
       return attributedString
    }
    
    
}

class AgentVerifyTimeCell : UITableViewCell {
    @IBOutlet weak var tfAgentSince: SkyFloatingLabelTextField! {
        didSet {
            tfAgentSince.title = "Agent Since".localized
        }
    }
    @IBOutlet weak var tfAgentFrom: SkyFloatingLabelTextField! {
        didSet {
            tfAgentFrom.title = "Verify On".localized
            
        }
    }
    @IBOutlet weak var tfAgentTill: SkyFloatingLabelTextField! {
        didSet {
            tfAgentTill.title = "Valid Till".localized
         
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func wrapData(agentSince: String, agentFrom: String, agentTill: String) {
        tfAgentSince.text = agentSince
        tfAgentFrom.text = agentFrom
        tfAgentTill.text = agentTill
    }
    
}

class AgentNoteCell : UITableViewCell {
    @IBOutlet weak var lblNote : UILabel!
    @IBOutlet weak var lblFirst : UILabel!
    @IBOutlet weak var lblSecond : UILabel!
    @IBOutlet weak var lblThird : UILabel!
    @IBOutlet weak var lblFourth : UILabel!
    @IBOutlet weak var lblFive : UILabel!
    @IBOutlet weak var lblSix : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let note = UpdateProfileModel.share.Agent
        let main = note?[1]
        lblNote.text = "Note".localized
        if ok_default_language == "my" {
            let htmlString = main?["My"] as? String ?? ""
            if let labelTextFormatted = htmlString.htmlToAttributedString {
                let textAttributes = [
                    NSAttributedString.Key.foregroundColor: UIColor.black,
                    NSAttributedString.Key.font: UIFont(name: appFont, size:15) ?? UIFont.systemFont(ofSize: 15)
                    ] as [NSAttributedString.Key: Any]
                labelTextFormatted.addAttributes(textAttributes, range: NSRange(location: 0, length: labelTextFormatted.length))
                println_debug(NSAttributedString(attributedString: labelTextFormatted))
                lblFirst.attributedText = NSAttributedString(attributedString: labelTextFormatted)
            }
        }else if ok_default_language == "en"{
            lblFirst.attributedText = NSAttributedString.init(html: main?["En"] as? String ?? "")
        }else {
            lblFirst.text = NSAttributedString.init(html: main?["UniCode"] as? String ?? "")?.string //NSAttributedString.init(html: main?["UniCode"] as? String ?? "")
        }
      
       
        lblSecond.text = "".localized
        lblThird.text = "".localized
        lblFourth.text = "".localized
        lblFive.text = "".localized
        lblSix.text = "".localized
    }
    
}

class HelpSupportUPCell: UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: MarqueeLabel!
    @IBOutlet weak var btnAction: UIButton!
}

protocol AgentTermConditiosCellDelegate : class {
    func showTermsConditionPopUp()
}

extension UPOkAgentVC : AgentTermConditionVCDelegate,SelectLanguageAgentFromDelegate {
    func onClickSelectingLanguageAction(action: String, vc: UIViewController, language: String, output: String) {
        vc.view.removeFromSuperview()
        if action == "download" {
            if language == "my" {
                if output == "AgentForm" {
                    if model.ApplicationFormUrlMy != "" {
                        self.download.fileName = "Application_Form_Burmese"
                       self.downloadAttachFormAction(urlString: model.ApplicationFormUrlMy)
                    }
                }else {
                    if model.AgentTermsAndConditionMy != "" {
                         self.download.fileName = "Agent_Terms_Conditions_Burmese"
                         self.downloadAttachFormAction(urlString: model.AgentTermsAndConditionMy)
                    }
                }
            }else if language == "en" {
                if output == "AgentForm" {
                    if model.ApplicationFormUrlEn != "" {
                    self.download.fileName = "Application_Form_English"
                      self.downloadAttachFormAction(urlString: model.ApplicationFormUrlEn)
                    }
                }else {
                    if model.AgentTermsAndConditionEn != "" {
                     self.download.fileName = "Agent_Terms_Conditions_English"
                     self.downloadAttachFormAction(urlString: model.AgentTermsAndConditionEn)
                    }
                }
            }else {
                if output == "AgentForm" {
                    if model.ApplicationFormUrlUni != "" {
                        self.download.fileName = "Application_Form_Unicode"
                        self.downloadAttachFormAction(urlString: model.ApplicationFormUrlUni)
                    }
                }else {
                    if model.AgentTermsAndConditionUni != "" {
                        self.download.fileName = "Agent_Terms_Conditions_Unicode"
                        self.downloadAttachFormAction(urlString: model.AgentTermsAndConditionUni)
                    }
                }

            }
        }
    }
    
    func TermsAndConditionsAccpeted(vc: UIViewController) {
        self.navigationController?.popViewController(animated: true)
        model.readTC = true
        self.reloadSection(index: 2)
    }
    
    func TermsAndConditionsRejected(vc: UIViewController) {
        self.navigationController?.popViewController(animated: true)
        model.readTC = false
        self.reloadSection(index: 2)
        
    }
    
    private func reloadSection(index : Int) {
        DispatchQueue.main.async {
            self.tblMainList.beginUpdates()
            let indexSet : IndexSet = [index]
            self.tblMainList.reloadSections(indexSet, with: .automatic)
            self.tblMainList.endUpdates()
        }
    }
    
}


extension UPOkAgentVC :  UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func fileUpload() {
        let imgEditVC = UIStoryboard(name: "MyProfile", bundle: Bundle.main).instantiateViewController(withIdentifier: "PAImagePreviewVC")  as! PAImagePreviewVC
        imgEditVC.strTitle = "Attach Document".localized
        imgEditVC.strSubTitle = "Agent".localized
        imgEditVC.fullURL = self.model.agentUploadFromURL
        imgEditVC.imagePreviewVCDelegate = self
        self.navigationController?.pushViewController(imgEditVC, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            let uploadedImg =  image.resize(image)
            let imageData = uploadedImg.pngData() as NSData?
            self.download.imageUpload(imgName : "deviceImg.png", fileData : imageData! as Data)
            picker.dismiss(animated: true, completion: nil)
        }else {
            showAlert(alertTitle: "", alertBody: "Error in image, Please try again".localized, alertImage: UIImage(named: "error")!)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}


extension UPOkAgentVC : PAImagePreviewVCDelegate {
    func ImagePreviewUrl(imageName : String, imageURL: String) {
        if let cell = tblMainList.cellForRow(at: IndexPath(row: 0, section: 1)) as? AgentFormCell {
            if imageURL != "" {
                self.model.agentUploadFromURL = imageURL.replacingOccurrences(of: " ", with: "%20")
                DispatchQueue.main.async {
                        let img = UIImage()
                        if let  image = img.convertURLToImage(str: self.model.agentUploadFromURL) {
                        self.model.agentFormImage = image
                        self.model.AgentStatus = 0
                        cell.btnCamera.setImage(self.model.agentFormImage, for: .normal)
                        cell.lblAttachAgentAppForm.text = "Agent Application Form Attached".localized
                    }
                }
            }
        }
    }
}

extension UPOkAgentVC : UIDocumentPickerDelegate {
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let cico = url as URL
        do {
            let imageData = try Data(contentsOf: cico)
            self.download.imageUpload(imgName : cico.path , fileData : imageData)
        } catch {
            showAlert(alertTitle: "", alertBody: "Document loading error, Please try again".localized, alertImage: UIImage(named: "error")!)
        }
        controller.dismiss(animated: true, completion: nil)
    }

    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}



// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}


extension UIImage {
    func convertURLToImage(str: String) -> UIImage? {
        let strURL = str.replacingOccurrences(of: "\\", with: "")
        let imageURL:URL = URL(string: strURL.replacingOccurrences(of: " ", with: "%20"))!
        let data = NSData(contentsOf: imageURL)
        let imgeView = UIImageView()
        if let dt = data {
            imgeView.image = UIImage(data: dt as Data)
        }
        return imgeView.image
    }
}




