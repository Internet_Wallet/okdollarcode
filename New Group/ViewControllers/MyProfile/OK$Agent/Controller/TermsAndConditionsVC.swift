//
//  TermsAndConditionsVC.swift
//  OK
//
//  Created by PC on 1/9/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol  TermsAndConditionsVCDelegate{
    func OptAcceptReject(selectionType : String)
}

class TermsAndConditionsVC: OKBaseController {

    var termsAndConditionsDelegate : TermsAndConditionsVCDelegate?

    @IBOutlet weak var viewTransparent: UIView!
 
    override func viewDidLoad() {
        super.viewDidLoad()
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.tranparentViewAction(sender:)))
        viewTransparent.addGestureRecognizer(gesture)
       /*
         do {
         var filename = ""
         if ok_default_language == "my"{
         filename = "agentTerm_Burmese"
         }else {
         filename = "agentTerm_Englsih"
         }
         guard let filePath = Bundle.main.path(forResource: filename, ofType: "html") else { return }
         
         let contents =  try String(contentsOfFile: filePath, encoding: .utf8)
         let baseUrl = URL(fileURLWithPath: filePath)
         self.webView.loadHTMLString(contents as String, baseURL: baseUrl)
         }catch { print ("File HTML error") }
         */
    }

    @objc func tranparentViewAction(sender : UITapGestureRecognizer) {
        navigationbarController(toEnable: true)
        self.view.removeFromSuperview()
    }
    
    func navigationbarController(toEnable: Bool) {
        if toEnable {
            self.navigationController?.navigationBar.layer.zPosition = 0
        } else {
            self.navigationController?.navigationBar.layer.zPosition = -64
        }
        self.navigationController?.navigationBar.isUserInteractionEnabled = toEnable
    }
    
    @IBAction func btnRejectAction(_ sender: Any) {
        
        guard (self.termsAndConditionsDelegate?.OptAcceptReject(selectionType:"Reject".localized) != nil) else {
            return
        }
        navigationbarController(toEnable: true)
        self.view.removeFromSuperview()
    }
    
    @IBAction func btnAcceptAction(_ sender: Any) {
        
        guard (self.termsAndConditionsDelegate?.OptAcceptReject(selectionType:"Accept".localized) != nil) else {
            return
        }
        navigationbarController(toEnable: true)
        self.view.removeFromSuperview()
    }


}
