//
//  UPOkAgentFormCell.swift
//  OK
//
//  Created by SHUBH on 8/31/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class UPOkAgentFormCell: UITableViewCell {

    @IBOutlet weak var btnDownloadAttachForm: UIButton!
    {
        didSet
        {
            btnDownloadAttachForm.setTitle("Download Agent Application Form".localized, for: .normal)
        }
    }
    @IBOutlet weak var btnUploadAttachForm: UIButton!
    {
        didSet
        {
            btnUploadAttachForm.setTitle("Attach Agent Application Form".localized, for: .normal)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
