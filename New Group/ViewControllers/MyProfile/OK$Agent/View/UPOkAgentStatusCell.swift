//
//  UPOkAgentStatusCell.swift
//  OK
//
//  Created by PC on 1/8/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class UPOkAgentStatusCell: UITableViewCell {

    @IBOutlet weak var txtAgentSince: SkyFloatingLabelTextField!
    @IBOutlet weak var txtLevel: SkyFloatingLabelTextField!
    @IBOutlet weak var txtVerifyOn: SkyFloatingLabelTextField!
    @IBOutlet weak var txtValidTill: SkyFloatingLabelTextField!
    @IBOutlet weak var txtStatus: SkyFloatingLabelTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
