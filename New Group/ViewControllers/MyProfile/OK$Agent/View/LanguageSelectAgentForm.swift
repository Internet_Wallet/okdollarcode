//
//  LanguageSelectAgentForm.swift
//  OK
//
//  Created by Imac on 3/17/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class LanguageSelectAgentForm: UIViewController {
    var delegate: SelectLanguageAgentFromDelegate?
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var imgBurmese: UIImageView!
    @IBOutlet weak var imgEnglish: UIImageView!
    @IBOutlet weak var imgUnicode: UIImageView!
    @IBOutlet weak var lblBurmese: UILabel!
    @IBOutlet weak var lblEnlish: UILabel!
    @IBOutlet weak var lblUnicode: UILabel!
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnDownload: UIButton!
    
    var input: String?
    
    override func viewDidLoad() {
        lblHeader.text = "Select Language".localized
        lblBurmese.text = "Burmese".localized
        lblEnlish.text = "English".localized
        lblUnicode.text = "Unicode".localized
        btnCancel.setTitle("Cancel".localized, for: .normal)
        btnDownload.setTitle("Download".localized, for: .normal)
        imgBurmese.image = UIImage(named: "r_act_radio_btn")
        imgEnglish.image = UIImage(named: "radio_btn")
        imgUnicode.image = UIImage(named: "radio_btn")
        self.bgView.layer.cornerRadius = 10
        self.bgView.layer.masksToBounds = true
        super.viewDidLoad()
    }
    
    @IBAction func onClickBurmeseAction(_ sender: UIButton) {
        if sender.tag == 100 {
           imgBurmese.image = UIImage(named: "r_act_radio_btn")
           imgEnglish.image = UIImage(named: "radio_btn")
           imgUnicode.image = UIImage(named: "radio_btn")
        }else if sender.tag == 200 {
            imgBurmese.image = UIImage(named: "radio_btn")
            imgEnglish.image = UIImage(named: "r_act_radio_btn")
            imgUnicode.image = UIImage(named: "radio_btn")
        }else {
            imgBurmese.image = UIImage(named: "radio_btn")
            imgEnglish.image = UIImage(named: "radio_btn")
            imgUnicode.image = UIImage(named: "r_act_radio_btn")
        }
    }
    
    @IBAction func onClickCancelDownloadAction(_ sender: UIButton) {
        var lan = ""
        if imgEnglish.image == UIImage(named: "r_act_radio_btn") {
            lan = "en"
        }else if imgBurmese.image == UIImage(named: "r_act_radio_btn"){
            lan = "my"
        }else {
             lan = "uni"
        }
        if sender.tag == 100 {
            if let del = delegate {
                del.onClickSelectingLanguageAction(action: "cancel", vc: self, language: lan, output: input ?? "")
            }
        }else {
            if let del = delegate {
                del.onClickSelectingLanguageAction(action: "download", vc: self, language: lan, output: input ?? "")
            }
        }
    }
    

}

protocol SelectLanguageAgentFromDelegate {
    func onClickSelectingLanguageAction(action: String, vc: UIViewController, language: String, output: String)
}
