//
//  UPOkAgentModel.swift
//  OK
//
//  Created by SHUBH on 8/31/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class UPOkAgentModel: NSObject {
    var agentFormImage: UIImage = UIImage(named: "agent_camera")!
    var readTC : Bool = false
    var BranchOrOutletName : String = ""
    var BusinessName : String = ""
    var AgentStatus: Int = 0
    var AgentSince: String = ""
    var AgentType: String = ""
    var UserMembership: String = ""
    var AttachedAgentAppFormImgUrl : String = ""
    
    var ApplicationFormUrlEn: String = ""
    var ApplicationFormUrlMy: String = ""
    var ApplicationFormUrlUni: String = ""
    
    var AgentTermsAndConditionEn: String = ""
    var AgentTermsAndConditionMy: String = ""
    var AgentTermsAndConditionUni: String = ""
    
    var agentUploadFromURL: String = ""
    
    
    func agentUpdateDetails() -> Dictionary<String, AnyObject> {
        let params = ["AttachedAgentAppFormImgUrl": agentUploadFromURL, "AgentDocument": AgentDocument() as [String : Any],
                      "LoginInfo": LoginInfo()] as [String : Any]
        return params as Dictionary<String, AnyObject>
    }

    
    func LoginInfo() -> Dictionary<String,AnyObject> {
        let loginInfoDic = ["AppId": UserModel.shared.appID,"Offset": 0,"MobileNumber": UserModel.shared.mobileNo,"Msid": msid,"Ostype": 1,"Limit": 0,"Otp": "","Simid": UserModel.shared.simID] as [String : AnyObject]
        return loginInfoDic
    }
    
    func AgentDocument() -> Dictionary<String,AnyObject> {
        let agentData = ["BusinessName": BusinessName,"BranchOrOutletName": BranchOrOutletName,"AgentStatus": AgentStatus] as [String : AnyObject]
        return agentData
    }
}





