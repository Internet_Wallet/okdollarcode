//
//  TransactionDTO.swift
//  OK
//  This file converts the data to object
//  Created by ANTONY on 10/02/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation

struct GetTransReportResponse: XMLIndexerDeserializable {
    var estel: TransEstel?
    static func deserialize(_ node: XMLIndexer) throws -> GetTransReportResponse {
        return try GetTransReportResponse(
            estel: TransEstel.deserialize(node["estel"])
        )
    }
}

struct TransEstel: XMLIndexerDeserializable {
    var header: TransHeader?
    var response: TransResponse?
    static func deserialize(_ node: XMLIndexer) throws -> TransEstel {
        return try TransEstel(
            header: TransHeader.deserialize(node["header"]),
            response: TransResponse.deserialize(node["response"])
        )
    }
}

struct TransHeader: XMLIndexerDeserializable {
    var responseType: String?
    
    static func deserialize(_ node: XMLIndexer) throws -> TransHeader {
        
        return try TransHeader(
            responseType: node["responsetype"].value()
        )
    }
}

struct TransResponse: XMLIndexerDeserializable {
    var resultCode: String?
    var resultDescription: String?
    var agentCode: String?
    var transId: String?
    var source: String?
    var responseDate: String?
    var recordCount: String?
    var comments: String?
    var vendorCode: String?
    var clientType: String?
    var prewalletBalance: String?
    var secureToken: String?
    var startIndex: String?
    var lastIndex: String?
    var records: TransRecord?
    
    static func deserialize(_ node: XMLIndexer) throws -> TransResponse {
        
        return try TransResponse(
            resultCode: node["resultcode"].value(),
            resultDescription: node["resultdescription"].value(),
            agentCode: node["agentcode"].value(),
            transId: node["transid"].value(),
            source: node["source"].value(),
            responseDate: node["responsects"].value(),
            recordCount: node["recordcount"].value(),
            comments: node["comments"].value(),
            vendorCode: node["vendorcode"].value(),
            clientType: node["clienttype"].value(),
            prewalletBalance: node["prewalletbalance"].value(),
            secureToken: node["securetoken"].value(),
            startIndex: node["startindex"].value(),
            lastIndex: node["lastindex"].value(),
            records: TransRecord.deserialize(node["records"])
        )
    }
}

struct TransRecord: XMLIndexerDeserializable {
    var record: [RecordData]?
    
    static func deserialize(_ node: XMLIndexer) throws -> TransRecord {
        var records = [RecordData]()
        for record in node.children {
            let newRecord = try RecordData.deserialize(record)
            records.append(newRecord)
        }
        return TransRecord(
            record: records
        )
    }
}

struct RecordData: XMLIndexerDeserializable {
    var amount: String?
    var destination: String?
    var transactionId: String?
    var transactionType: String?
    var responseDate: String?
    var operatorName: String?
    var walletBalance: String?
    var cashBack: String?
    var comments: String?
    var accTransactionType: String?
    var bonus: String?
    var refTransID: String?
    var fee: String?
    
    static func deserialize(_ node: XMLIndexer) throws -> RecordData {
        return try RecordData(
            amount: node["amount"].value(),
            destination: node["destination"].value(),
            transactionId: node["transid"].value(),
            transactionType: node["transtype"].value(),
            responseDate: node["responsects"].value(),
            operatorName: node["operator"].value(),
            walletBalance: node["walletbalance"].value(),
            cashBack: node["kickback"].value(), //node["fee"].value(),
            comments: node["comments"].value(),
            accTransactionType: node["acctranstype"].value(),
            bonus: node["Loyalty"].value(), //node["FEE"].value(),
            refTransID: node["reftransid"].value(),
            fee: node["FEE"].value()
        )
    }
}


//GetLoyalty
struct GetLoyaltyReportResponse: XMLIndexerDeserializable {
    var estel: LoyartyReportEstel?
    static func deserialize(_ node: XMLIndexer) throws -> GetLoyaltyReportResponse {
        return try GetLoyaltyReportResponse(
            estel: LoyartyReportEstel.deserialize(node["estel"])
        )
    }
}

struct LoyartyReportEstel: XMLIndexerDeserializable {
    var header: LoyaltyHeader?
    var response: LoyaltyResponse?
    static func deserialize(_ node: XMLIndexer) throws -> LoyartyReportEstel {
        return try LoyartyReportEstel(
            header: LoyaltyHeader.deserialize(node["header"]),
            response: LoyaltyResponse.deserialize(node["response"])
        )
    }
}

struct LoyaltyHeader: XMLIndexerDeserializable {
    var responseType: String?
    
    static func deserialize(_ node: XMLIndexer) throws -> LoyaltyHeader {
        
        return try LoyaltyHeader(
            responseType: node["responsetype"].value()
        )
    }
}

struct LoyaltyResponse: XMLIndexerDeserializable {
    var resultCode: String?
    var resultDescription: String?
    var agentCode: String?
    var transId: String?
    var responseDate: String?
    var comments: String?
    var vendorCode: String?
    var clientType: String?
    var records: LoyaltyRecord?
    
    static func deserialize(_ node: XMLIndexer) throws -> LoyaltyResponse {
        
        return try LoyaltyResponse(
            resultCode: node["resultcode"].value(),
            resultDescription: node["resultdescription"].value(),
            agentCode: node["agentcode"].value(),
            transId: node["transid"].value(),
            responseDate: node["responsects"].value(),
            comments: node["comments"].value(),
            vendorCode: node["vendorcode"].value(),
            clientType: node["clienttype"].value(),
            records: LoyaltyRecord.deserialize(node["records"])
        )
    }
}

struct LoyaltyRecord: XMLIndexerDeserializable {
    var record: [LoyaltyData]?
    
    static func deserialize(_ node: XMLIndexer) throws -> LoyaltyRecord {
        var records = [LoyaltyData]()
        for record in node.children {
            let newRecord = try LoyaltyData.deserialize(record)
            records.append(newRecord)
        }
        return LoyaltyRecord(
            record: records
        )
    }
}

struct LoyaltyData: XMLIndexerDeserializable {
    var transId: String?
    var transDate: String?
    var subcriberCode: String?
    var transferPoint: String?
    var validateDate: String?
    var openingPoint: String?
    var closingPoint: String?
    var merchantCode: String?
    var comments: String?
    var merchantName: String?
    var points: String?
    
    
    static func deserialize(_ node: XMLIndexer) throws -> LoyaltyData {
        return try LoyaltyData(
            transId: node["transid"].value(),
            transDate: node["transdate"].value(),
            subcriberCode: node["subcribercode"].value(),
            transferPoint: node["transferpoint"].value(),
            validateDate: node["validatedate"].value(),
            openingPoint: node["openingpoint"].value(),
            closingPoint: node["closingpoint"].value(),
            merchantCode: node["merchantcode"].value(), //node["fee"].value(),
            comments: node["comments"].value(),
            merchantName: node["merchantname"].value(),
            points: node["points"].value()
        )
    }
}

// MARK: - Send Money To Bank Report Request
struct SendMoneyToBankReportRequest: Codable {
    var appID: String
    var limit: String
    var mobileNumber: String
    var msId: String
    var offSet = "0"
    var osType = 1
    var simId: String
    init(appID: String, limit: String, mobileNumber: String, msId: String, simId: String) {
        self.appID = appID
        self.limit = limit
        self.mobileNumber = mobileNumber
        self.msId = msId
        self.simId = simId
    }
    private enum CodingKeys: String, CodingKey {
        case appID = "AppId"
        case limit = "Limit"
        case mobileNumber = "MobileNumber"
        case msId = "Msid"
        case offSet = "Offset"
        case osType = "Ostype"
        case simId = "Simid"
    }
}

// MARK: - Send Money To Bank Report Response
struct SendMoneyToBankReportResponse: Codable {
    var code: Int
    var response: String?
    var message: String?
    private enum CodingKeys: String, CodingKey {
        case code = "Code"
        case response = "Data"
        case message = "Msg"
    }
}

struct SendMoneyToBankReport: Codable {
    var receiverAccName: String?
    var bankName: String?
    var bankBranchName: String?
    var bankAccountNumber: String?
    var bankAccountType: String?
    var transferredAmount: Double?
    var balance: Double?
    var transactionDateTime: String?
    var transactionId: String?
    var comments: String?
    private enum CodingKeys: String, CodingKey {
        case receiverAccName = "ReceiverOkAccName"
        case bankName = "BankName"
        case bankBranchName = "BankBranchName"
        case bankAccountNumber = "BankAccountNumber"
        case bankAccountType = "BankAccountType"
        case transferredAmount = "TransaferedAmount"
        case balance = "Balance"
        case transactionDateTime = "TransactionTime"
        case transactionId = "TransactionId"
        case comments = "Comments"
    }
}

struct ResaleMainBalanceReport: Codable {
    var productId: String?
    var phoneNumber: String?
    var airtimeAmount: Double?
    var destinationNumber: String?
    var status: String?
    var createDate: String?
    var customerId: String?
    var telcoId: String?
    var telcoName: String?
    var sellingAmount: Double?
    var customer: CustomerResaleReport?
    private enum CodingKeys: String, CodingKey {
        case productId = "ProductId"
        case phoneNumber = "PhoneNumber"
        case airtimeAmount = "AirtimeAmount"
        case destinationNumber = "DestinationNumber"
        case status = "Status"
        case createDate = "CreateDate"
        case customerId = "CustomerId"
        case telcoId = "TelcoId"
        case telcoName = "TelcoName"
        case sellingAmount = "SellingAmount"
        case customer = "Customer"
    }
}

struct CustomerResaleReport: Codable {
    var customerId: String?
    var customerPhoneNumber: String?
    var gcmRegistrationId: String?
    
    private enum CodingKeys: String, CodingKey {
        case customerId = "CustomerId"
        case customerPhoneNumber = "CustomerPhoneNumber"
        case gcmRegistrationId = "GcmRegistrationId"
    }
}

struct VotingReportResponseDTO: Codable {
    let statusCode: Int
    let status: Bool
    let message: String
    let result: VotingReportList?
    let results: [VotingReportList]?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "StatusCode"
        case status = "Status"
        case message = "Message"
        case result = "Result"
        case results = "Results"
    }
}

struct VotingReportList: Codable {
    let votingDetailID, userID: String?
    let mainProgrammeName: String?
    let programmeName: String?
    let eventName, eventID, participantName, participantID: String?
    let votingTypeName: String?
    let votingTypeID: String?
    let votes: Int?
    let createdDate, votePackageID: String?
    let voteStatus: String?
    let voteSentDate: String?
    let payment: ReportPayment?
    
    enum CodingKeys: String, CodingKey {
        case votingDetailID = "VotingDetailId"
        case userID = "UserId"
        case mainProgrammeName = "MainProgrammeName"
        case programmeName = "ProgrammeName"
        case eventName = "EventName"
        case eventID = "EventId"
        case participantName = "ParticipantName"
        case participantID = "ParticipantId"
        case votingTypeName = "VotingTypeName"
        case votingTypeID = "VotingTypeId"
        case votes = "Votes"
        case createdDate = "CreatedDate"
        case votePackageID = "VotePackageId"
        case voteStatus = "VoteStatus"
        case voteSentDate = "VoteSentDate"
        case payment = "Payment"
    }
}

struct ReportPayment: Codable {
    let paymentID, transactionID, okAccountNumber, destinationNumber: String?
    let amount: Double?
    let paymentStatus: String?
    let rate: Double?
    let totalVotes: Int?
    let createdDate, votingDetailID, priceTimeLineID: String?
    
    enum CodingKeys: String, CodingKey {
        case paymentID = "PaymentId"
        case transactionID = "TransactionId"
        case okAccountNumber = "OkAccountNumber"
        case destinationNumber = "DestinationNumber"
        case amount = "Amount"
        case paymentStatus = "PaymentStatus"
        case rate = "Rate"
        case totalVotes = "TotalVotes"
        case createdDate = "CreatedDate"
        case votingDetailID = "VotingDetailId"
        case priceTimeLineID = "PriceTimeLineId"
    }
}

struct SolarReportResponseDTO: Codable {
    let statusCode: Int?
    let status: Bool?
    let message: String?
    let result: SolarReport?
    let results: [SolarReport]?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "StatusCode"
        case status = "Status"
        case message = "Message"
        case result = "Result"
        case results = "Results"
    }
}

struct SolarReport: Codable {
    let topupID, customerID: String?
    let amount: Double?
    let destinationNumber, customerOkPaymentID, customerOkPaymentStatus, okPaymentID: String?
    let okPaymentStatus, topupStatus, refundOkTransactionID, createdDate: String?
    let solarHomeAccountNumber, solarHomeStatus, solarHomeMessage: String?
    let serviceCharges: Double?
    let customer: SolareReportCustomer?
    
    enum CodingKeys: String, CodingKey {
        case topupID = "TopupId"
        case customerID = "CustomerId"
        case amount = "Amount"
        case destinationNumber = "DestinationNumber"
        case customerOkPaymentID = "CustomerOkPaymentId"
        case customerOkPaymentStatus = "CustomerOkPaymentStatus"
        case okPaymentID = "OkPaymentId"
        case okPaymentStatus = "OkPaymentStatus"
        case topupStatus = "TopupStatus"
        case refundOkTransactionID = "RefundOkTransactionId"
        case createdDate = "CreatedDate"
        case solarHomeAccountNumber = "SolarHomeAccountNumber"
        case solarHomeStatus = "SolarHomeStatus"
        case solarHomeMessage = "SolarHomeMessage"
        case serviceCharges = "ServiceCharges"
        case customer = "Customer"
    }
}

struct SolareReportCustomer: Codable {
    let customerID, customerName, okAccountNumber, createdDate: String
    
    enum CodingKeys: String, CodingKey {
        case customerID = "CustomerId"
        case customerName = "CustomerName"
        case okAccountNumber = "OkAccountNumber"
        case createdDate = "CreatedDate"
    }
}

//MARK: - Offer Report Response
struct OfferReportResponseDTO: Codable {
    let code: Int
    let data: String?
    let msg: String
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case msg = "Msg"
    }
}

//MARK: - Offer List Date Request
struct OfferReporListCompanyRequest: Codable {
    let mobileNumber, fromDate, toDate, offSet, limit,categoryID: String
    
    enum CodingKeys: String, CodingKey {
        case mobileNumber = "MobileNumber"
        case fromDate = "FromDate"
        case toDate = "ToDate"
        case offSet = "OffSet"
        case limit = "Limit"
        case categoryID = "PromotionCategoryId"
        
    }
}

//MARK: - NonOffer List Date Request
struct NonOfferSummaryRequest: Codable {
    
    let mobileNumber, fromDate, toDate, transactionRequestedType, townShipId, advanceMerchantNumber, dummyMerchantMobileNumber: String
    
    enum CodingKeys: String, CodingKey {
        case mobileNumber = "MobileNumber"
        case transactionRequestedType = "TransactionRequestedType"
        case townShipId = "TownShipId"
        case advanceMerchantNumber = "AdvanceMerchantNumber"
        case dummyMerchantMobileNumber = "DummyMerchantMobileNumber"
        case fromDate = "FromDate"
        case toDate = "ToDate"
    }
}

struct NonOfferAllTransactionRequest: Codable {
    
    let mobileNumber: String
    
    enum CodingKeys: String, CodingKey {
        case mobileNumber = "MobileNumber"
        
    }
}

struct NonOfferAdvanceMerchantRequest: Codable {
    
    let mobileNumber: String
    let townShipId: String

    enum CodingKeys: String, CodingKey {
        case mobileNumber = "MobileNumber"
        case townShipId = "TownShipId"

    }
}

struct NonOfferDummyMerchantRequest: Codable {
    
    let mobileNumber: String
    let townShipId: String
    let advanceMerchantNumber: String

    enum CodingKeys: String, CodingKey {
        case mobileNumber = "MobileNumber"
        case townShipId = "TownShipId"
        case advanceMerchantNumber = "AdvanceMerchantMobileNumber"

    }
}


//MARK: - NonOffer Report Date Request
struct NonOfferReporListDateRequest: Codable {
    
   // let mobileNumber, fromDate, toDate, transactionRequestedType, townShipId, advanceMerchantNumber, dummyMerchantMobileNumber: String
    
    let mobileNumber, fromDate, toDate, transactionRequestedType, townShipId, advanceMerchantNumber: String
    
    let dummyMerchantMobileNumber : String
    
    enum CodingKeys: String, CodingKey {
        case mobileNumber = "MobileNumber"
        case transactionRequestedType = "TransactionRequestedType"
        case townShipId = "TownShipId"
        case advanceMerchantNumber = "AdvanceMerchantNumber"
        case dummyMerchantMobileNumber = "DummyMerchantMobileNumber"
        case fromDate = "FromDate"
        case toDate = "ToDate"
    }
}

//MARK: - NonOffer Report Date Request
struct NonOfferReporListDateRequestNew: Codable {
    
    
    let mobileNumber, fromDate, toDate, transactionRequestedType, advanceMerchantNumber: String
    
    
    enum CodingKeys: String, CodingKey {
        case mobileNumber = "MobileNumber"
        case transactionRequestedType = "TransactionRequestedType"
        case advanceMerchantNumber = "AdvanceMerchantNumber"
        case fromDate = "FromDate"
        case toDate = "ToDate"
    }
}

//MARK: - NonOffer Report Date Request
struct NonOfferReporListAmountRequest: Codable {
    
    // let mobileNumber, fromDate, toDate, transactionRequestedType, townShipId, advanceMerchantNumber, dummyMerchantMobileNumber: String
    
    let mobileNumber, fromDate, toDate, transactionRequestedType : String
    
    
    enum CodingKeys: String, CodingKey {
        case mobileNumber = "MobileNumber"
        case transactionRequestedType = "TransactionRequestedType"
        case fromDate = "FromDate"
        case toDate = "ToDate"
    }
}


//MARK: - Offer List Date Request
struct OfferReporListDateRequest: Codable {
    let mobileNumber, fromDate, toDate, offSet, limit: String
    
    enum CodingKeys: String, CodingKey {
        case mobileNumber = "MobileNumber"
        case offSet = "OffSet"
        case limit = "Limit"
        case fromDate = "FromDate"
        case toDate = "ToDate"
    }
}

//MARK: - Offer List Request
struct OfferReporListRequest: Codable {
    let mobileNumber, offSet, limit: String
    
    enum CodingKeys: String, CodingKey {
        case mobileNumber = "MobileNumber"
        case offSet = "OffSet"
        case limit = "Limit"
    }
}

//MARK: - NonOffer List Response
class NonOfferReportList {
    //New Model
    var accTransType: String?
    var age: Int16?
    var amount: Int?
    var bonus: Int?
    var cashBack: Int?
    var countryCode: String?
    var desc: String?
    var destination: String?
    var gender: String?
    var isFromApi: String?
    var latitude: String?
    var longitude: String?
    var operatorName: String?
    var rating: String?
    var ratingFeedback: String?
    var rawDesc: String?
    var receiverBName: String?
    var receiverName: String?
    var refTransID: String?
    var senderBName: String?
    var senderName: String?
    var transactionDate: String?
    var transID: String?
    var transType: String?
    var walletBalance: Int?
    
    //Old Model
    var townShipName: String?
    var advanceMerchantNumber: String?
    var dummyMerchentNumber: String?
    var source: String?
    var balance: Int?
    var resultCode: String?
    var resultDescription: String?
    var kickBack: Int?
    var comments: String?
    var transactionTypeName: String?
    var accountTransactionType: String?
}

//MARK: - Offer List Response

//prabu Summary Response
struct NonOfferAllTransactionModel: Codable {

    var transactionTypeName: String?
    var transactionTypeValue: String?
    var transactionRequestedTypeNameMyanmar: String?
    var transactionRequestedTypeNameChinese: String?
    var transactionRequestedTypeNameThai: String?
    
    enum CodingKeys: String, CodingKey {
        case transactionTypeName = "transactionRequestedTypeName"
        case transactionTypeValue = "transactionRequestedTypeValue"
        case transactionRequestedTypeNameMyanmar = "transactionRequestedTypeNameMyanmar"
        case transactionRequestedTypeNameChinese = "transactionRequestedTypeNameChinese"
        case transactionRequestedTypeNameThai = "transactionRequestedTypeNameThai"
        
    }
}

struct NonOfferAllLocationModel: Codable {
    
    var locationID: String?
    var locationName: String?
    var locationNameMyanmar: String?
    
    enum CodingKeys: String, CodingKey {
        case locationID = "locationId"
        case locationName = "locationName"
        case locationNameMyanmar = "locationNameMyanmar"
    }
}


struct NonOfferSafetyCashierSummaryModel: Codable {
    
    var totalAvailableAmount: Double?
    var totalDiscountAmount: Double?
    var totalReceivedAmount: Double?
    var totalBuyingQty: Double?
    var totalFreeQty: Double?

    
    enum CodingKeys: String, CodingKey {
        
        case totalAvailableAmount = "advanceMerchantMobileNumber"
        case totalDiscountAmount = "advanceMerchantName"
        case totalReceivedAmount = "dummyMerchantModelList"
        case totalBuyingQty = "advanceMerchantTotalCount"
        case totalFreeQty = "advanceMerchantTotalAmount"
        
    }
}

struct NonOfferAdvanceMerchantModel: Codable {
    
    var advanceMerchantMobileNumber: String?
    var advanceMerchantName: String?
    //var AdvanceMerchantModelList:[NonofferAdvanceMerchantModelList]
    var advanceMerchantTotal: NonOfferMerchantModelTotal
    var advanceMerchantTotalAmount: NonOfferMerchantModelTotalAmount
    
    //var advanceMerchantTotalAmount: String?
    
    enum CodingKeys: String, CodingKey {
        
        case advanceMerchantMobileNumber = "advanceMerchantMobileNumber"
        case advanceMerchantName = "advanceMerchantName"
        //case AdvanceMerchantModelList = "dummyMerchantModelList"
        case advanceMerchantTotal = "advanceMerchantTotalCount"
        case advanceMerchantTotalAmount = "advanceMerchantTotalAmount"
        
        
        //case advanceMerchantTotalAmount = "advanceMerchantTotalAmount"
        
    }
}

struct NonOfferAdvanceMerchantModelnew: Codable {
    
   
    var AdvanceMerchantModelList:[NonofferAdvanceMerchantModelList]
    var advanceMerchantTotal : String?
    var advanceMerchantTotalAmount : String?

    //var advanceMerchantTotalAmount: String?

    enum CodingKeys: String, CodingKey {
        
        case AdvanceMerchantModelList = "dummyMerchantModelList"
        case advanceMerchantTotal = "advanceMerchantTotalCount"
        case advanceMerchantTotalAmount = "advanceMerchantTotalAmount"


        //case advanceMerchantTotalAmount = "advanceMerchantTotalAmount"

    }
}

struct NonofferAdvanceMerchantModelList: Codable {
    
    var advanceMerchantMobileNumber: String?
    var advanceMerchantName: String?
    
    enum CodingKeys: String, CodingKey {
        case advanceMerchantMobileNumber = "advanceMerchantMobileNumber"
        case advanceMerchantName = "advanceMerchantName"
    }
}

struct NonOfferMerchantModelTotal: Codable {
    
    var TotalCount: Int?
    
    enum CodingKeys: String, CodingKey {
        case TotalCount = "advanceMerchantTotalCount"
    }
}

struct NonOfferMerchantModelTotalAmount: Codable {
    
    var TotalAmount: Double?
    
    enum CodingKeys: String, CodingKey {
        case TotalAmount = "advanceMerchantTotalAmount"
    }
}


struct NonOfferDummyMerchantModel: Codable {
    
    var dummyMerchantMobileNumber: String?
    var dummyMerchantName: String?
    //var dummyMerchantTotalAmount: String?

    var isSelected: Bool


    enum CodingKeys: String, CodingKey {
        case dummyMerchantMobileNumber = "dummyMerchantMobileNumber"
        case dummyMerchantName = "dummyMerchantName"
        //case dummyMerchantTotalAmount = "dummyMerchantTotalAmount"

    }
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.isSelected = false
        self.dummyMerchantMobileNumber = try container.decodeIfPresent(String.self, forKey: .dummyMerchantMobileNumber)
        self.dummyMerchantName = try container.decodeIfPresent(String.self, forKey: .dummyMerchantName)
       // self.dummyMerchantTotalAmount = try container.decodeIfPresent(String.self, forKey: .dummyMerchantTotalAmount)
    }
    init(dummyMNo: String, dummyMName: String, dummyMerchantAmount: String) {
        self.dummyMerchantMobileNumber = dummyMNo
        self.dummyMerchantName = dummyMName
       // self.dummyMerchantTotalAmount = dummyMerchantAmount
        self.isSelected = false
    }
}

extension NonOfferDummyMerchantModel: Equatable {
    static func == (lhs: NonOfferDummyMerchantModel, rhs: NonOfferDummyMerchantModel) -> Bool {
        guard let lName = lhs.dummyMerchantName, let rName = rhs.dummyMerchantName,
            let lNo = lhs.dummyMerchantMobileNumber, let rNo = rhs.dummyMerchantMobileNumber
                else { return false }
            //let lAmount = lhs.dummyMerchantTotalAmount, let rAmount = rhs.dummyMerchantTotalAmount
        //return lName == rName && lNo == rNo && lAmount == rAmount
        return lName == rName && lNo == rNo
    }
}

struct NonOfferTransactionSummaryModel: Codable {
    
    var transactionSummary: NonOfferTransactionSummaryModelData
    var transactionSummaryRequestModel:NonOfferTransactionSummaryRequestModel

    enum CodingKeys: String, CodingKey {
        case transactionSummary = "transactionSummarryModels"
        case transactionSummaryRequestModel = "transactionSummaryRequestModel"

        
    }
}

struct NonOfferTransactionSummaryModelData: Codable {
    
    var totalCollectionAmount: Double?
    var totalDiscountAmount: Double?
    var totalSaleQty: Double?
    var totalFreeQty: Double?
    
   // var totalCollectionAmount, totalDiscountAmount, totalSaleQty, totalFreeQty: String?
    
    
    enum CodingKeys: String, CodingKey {
        //case transactionSummary = "transactionSummarryModels"
        case totalCollectionAmount = "totalCollectionAmount"
        case totalDiscountAmount = "totalDiscountAmount"
        case totalSaleQty = "totalSaleQty"
        case totalFreeQty = "totalFreeQty"
       // case transactionSummaryRequestModel = "transactionSummaryRequestModel"
        
    }
}

struct NonOfferTransactionSummaryRequestModel: Codable {
    
    var fromDate: Date?
    var toDate: Date?
    var advanceMerchantNumber: String?
    var dummyMerchantMobileNumber: String?
    var mobileNumber: String?
    var townShipId: String?
    var transactionRequestedType: String?


   // var mobileNumber,fromDate,toDate, transactionRequestedType, townShipId, advanceMerchantNumber, dummyMerchantMobileNumber: String?
    
    enum CodingKeys: String, CodingKey {
        case fromDate = "fromDate"
        case toDate = "toDate"
        case advanceMerchantNumber = "advanceMerchantNumber"
        case dummyMerchantMobileNumber = "dummyMerchantMobileNumber"
        case mobileNumber = "mobileNumber"
        case townShipId = "townShipId"
        case transactionRequestedType = "transactionRequestedType"

    }
}


struct OfferReportList: Codable {
    var promotionId: String?
    var promotionName: String?
    var promotionCode: String?
    var promotionStartDate: String?
    var promotionEndDate: String?
    var promotionImage: String?
    var promotionProceesType: String?
    var fixedAmountValue: String?
    var percentageValue: String?
    var paymentType: String?
    var promotionType: String?
    var isLimit: String?
    var amountType: Int?
}

//MARK: - Offer Filter One Record
struct OfferAllTransactionRequest: Codable {
    var mobileNumber: String
    var promotionId: String
    
    init(mobileNumber: String, promotionId: String) {
        self.mobileNumber = mobileNumber
        self.promotionId = promotionId
    }
    private enum CodingKeys: String, CodingKey {
        case mobileNumber = "MobileNumber"
        case promotionId = "PromotionId"
    }
}

//MARK: - Offer Summary Filter
struct OfferSummaryFilterRequest: Codable {
    var mobileNumber: String
    var promotionId: String
    var toTransactionDate: String?
    var fromTransactionDate: String?
    var townshipID: String?
    var advMerchantNo: String?
    var dummyMerchantNo: [String]?
    init(mobileNumber: String, promotionId: String, toTransactionDate: String?, fromTransactionDate: String?, townshipId: String?, advMerchantNo: String?, dummyMerchantNo: [String]?) {
        self.mobileNumber = mobileNumber
        self.promotionId = promotionId
        self.toTransactionDate = toTransactionDate
        self.fromTransactionDate = fromTransactionDate
        self.townshipID = townshipId
        self.advMerchantNo = advMerchantNo
        self.dummyMerchantNo = dummyMerchantNo
    }
    private enum CodingKeys: String, CodingKey {
        case mobileNumber = "MobileNumber"
        case promotionId = "PromotionId"
        case fromTransactionDate = "TransactionFromDate"
        case toTransactionDate = "TransactionToDate"
        case townshipID = "TownShipId"
        case advMerchantNo = "AdvanceMerchantNumber"
        case dummyMerchantNo = "DummyMerchantNumber"
    }
}

//MARK: - Offer Result Filter
struct OfferResultFilterRequest: Codable {
    var mobileNumber: String
    var promotionId: String
    var toTransactionDate: String?
    var fromTransactionDate: String?
    var townshipID: String?
    var advMerchantNo: String?
    var dummyMerchantNo: [String]?
    init(mobileNumber: String, promotionId: String, toTransactionDate: String?, fromTransactionDate: String?, townshipId: String?, advMerchantNo: String?, dummyMerchantNo: [String]?) {
        self.mobileNumber = mobileNumber
        self.promotionId = promotionId
        self.fromTransactionDate = fromTransactionDate
        self.toTransactionDate = toTransactionDate
        self.townshipID = townshipId
        self.advMerchantNo = advMerchantNo
        self.dummyMerchantNo = dummyMerchantNo
    }
    private enum CodingKeys: String, CodingKey {
        case mobileNumber = "MobileNumber"
        case promotionId = "PromotionId"
        case fromTransactionDate = "TransactionFromDate"
        case toTransactionDate = "TransactionToDate"
        case townshipID = "TownShipId"
        case advMerchantNo = "AdvanceMerchantNumber"
        case dummyMerchantNo = "DummyMerchantNumber"
    }
}

struct OfferListOneRequest: Codable {
    var mobileNumber: String
    var promotionId: String
    var fromDate: String
    var toDate: String

    init(mobileNumber: String, promotionId: String, fromDate: String, toDate: String) {
        self.mobileNumber = mobileNumber
        self.promotionId = promotionId
        self.fromDate = fromDate
        self.toDate = toDate

    }
    private enum CodingKeys: String, CodingKey {
        case mobileNumber = "MobileNumber"
        case promotionId = "PromotionId"
        case fromDate = "FromDate"
        case toDate = "ToDate"
    }
}

//MARK: - Offer Filter Request
struct OfferListFilterRequest: Codable {
    var mobileNumber: String
    var promotionId: String
    var transactionDate: String?
    var townshipID: String?
    var advMerchantNo: String?
    var dummyMerchantNo: [String]?
    var offSet: String?
    var limit: String?
    init(mobileNumber: String, promotionId: String, date: String?, townshipId: String?, advMerchantNo: String?, offSetVal: String?, dummyMerchantNo: [String]?, limitVal: String?) {
        self.mobileNumber = mobileNumber
        self.promotionId = promotionId
        self.transactionDate = date
        self.townshipID = townshipId
        self.advMerchantNo = advMerchantNo
        self.dummyMerchantNo = dummyMerchantNo
        self.offSet = offSetVal
        self.limit = limitVal
    }
    private enum CodingKeys: String, CodingKey {
        case mobileNumber = "MobileNumber"
        case promotionId = "PromotionId"
        case transactionDate = "TransactionDate"
        case townshipID = "TownShipId"
        case advMerchantNo = "AdvanceMerchantNumber"
        case dummyMerchantNo = "DummyMerchantNumber"
        case offSet = "OffSet"
        case limit = "Limit"
    }
}

//MARK: - Offer Summary Filter Response
struct OfferReportFilterSummary: Codable {

    let advanceMerchantCount, totalTransactionCount, totalBillAmount, totalDiscountAmount, totalNetAmount, totalSaleQty, totalFreeQty, totalDiscountPercentage: Double?
    
    enum CodingKeys: String, CodingKey {
        case advanceMerchantCount, totalTransactionCount, totalBillAmount, totalDiscountAmount, totalNetAmount, totalSaleQty, totalFreeQty, totalDiscountPercentage
    }
}

//MARK: - Offer Filter Response
struct OfferReporFilterList: Codable {
    let promotionID, promotionName, promotionCode: String?
    let totalBillAmount, totalDiscountAmount, totalNetAmount, productValue: Double?
    let promotionStartDate, promotionEndDate, advanceMerchantName, advanceMerchantNumber: String?
    let dummyMerchantName, dummyMerchantNumber, promotionUsedMobileNumber, promotionUsedCustomerName: String?
    let promotionDestinationMobileNumber, transactionTime, transactionComments, lattitude: String?
    let longitude, cellID, parentTransactionID, childTransactionID: String?
    let promotionProceesType, fixedAmountValue, percentageValue, paymentType: String?
    let promotionType, isLimit: String?
    let freeQty, buyingQty,productValidationRuleValue: Int?
    let transactionCount: Int?
    
    enum CodingKeys: String, CodingKey {
        case promotionID = "promotionId"
        case promotionName, promotionCode, totalBillAmount, totalDiscountAmount, totalNetAmount, productValue, promotionStartDate, promotionEndDate, advanceMerchantName, advanceMerchantNumber, dummyMerchantName, dummyMerchantNumber, transactionTime, transactionComments, lattitude, longitude
        case promotionUsedMobileNumber = "prmotionUsedMobileNumber"
        case promotionUsedCustomerName = "prmotionUsedCustomerName"
        case promotionDestinationMobileNumber = "prmotionDestinationMobileNumber"
        case cellID = "cellId"
        case parentTransactionID = "parentTransactionId"
        case childTransactionID = "childTransactionId"
        case promotionProceesType, fixedAmountValue, percentageValue, paymentType, promotionType, isLimit
        case freeQty, buyingQty, productValidationRuleValue
        case transactionCount
    }
}

//MARK: - Offer Location Response
struct OfferLocationList: Codable {
    let locationID, locationName: String?
    enum CodingKeys: String, CodingKey {
        case locationID = "promotionLocationId"
        case locationName = "promotionLocationName"
    }
}

//MARK: - Advance Merchant List Response
struct OfferAdvanceMerchantList: Codable {
    let advanceMerchantMobileNumber, advanceMerchantName, advanceMerchantTotalAmount: String?
}

//MARK: - Dummy Merchant List Response
struct OfferDummyMerchantList: Codable {
    var dummyMerchantMobileNumber, dummyMerchantName, dummyMerchantTotalAmount: String?
    var isSelected: Bool
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.isSelected = false
        self.dummyMerchantMobileNumber = try container.decodeIfPresent(String.self, forKey: .dummyMerchantMobileNumber)
        self.dummyMerchantName = try container.decodeIfPresent(String.self, forKey: .dummyMerchantName)
        self.dummyMerchantTotalAmount = try container.decodeIfPresent(String.self, forKey: .dummyMerchantTotalAmount)
    }
    
    init(dummyMNo: String, dummyMName: String, dummyMerchantAmount: String) {
        self.dummyMerchantMobileNumber = dummyMNo
        self.dummyMerchantName = dummyMName
        self.dummyMerchantTotalAmount = dummyMerchantAmount
        self.isSelected = false
    }
}

extension OfferDummyMerchantList: Equatable {
    static func == (lhs: OfferDummyMerchantList, rhs: OfferDummyMerchantList) -> Bool {
        guard let lName = lhs.dummyMerchantName, let rName = rhs.dummyMerchantName,
            let lNo = lhs.dummyMerchantMobileNumber, let rNo = rhs.dummyMerchantMobileNumber,
            let lAmount = lhs.dummyMerchantTotalAmount, let rAmount = rhs.dummyMerchantTotalAmount else { return false }
        return lName == rName && lNo == rNo && lAmount == rAmount
    }
}

//MARK: - Report Cashin CashOut Request
struct ReportCICORequest: Codable {
    var countryCode: String?
    var phoneNumber: String?
    
    private enum CodingKeys: String, CodingKey {
        case countryCode = "CountryCode"
        case phoneNumber = "PhoneNumber"
    }
}

//MARK: - Report Cashin CashOut Response
struct ReportCICOResponse: Codable {
    let content: [ReportCICOList]?
    let statusCode, status: Int
    let comment: String?
    
    enum CodingKeys: String, CodingKey {
        case content = "Content"
        case statusCode = "StatusCode"
        case status = "Status"
        case comment = "Comment"
    }
}

struct LoyaltyAllReports: Codable {
    let type: String
    enum CodingKeys: String, CodingKey {
        case type = "type"
    }
}

struct ReportCICOList: Codable {
    let receiverCountryCode, receiverPhoneNumber, requesterCountryCode, requesterPhoneNumber: String?
    let cashType: Int
    let senderName, receiverName, paymentType: String?
    let commission, commissionPercent: Double?
    let date, convertedDate: String?
    let totalBalance, rechargeAmount, totalAmount, bonusPoints: Double?
    let cashBack: Double?
    let transactionID: String?
    let receiverTotalBalance, requesterTotalBalance: Double?
    let senderOpeningBalance, senderClosingBalance: Double?
    
    enum CodingKeys: String, CodingKey {
        case receiverCountryCode = "ReceiverCountryCode"
        case receiverPhoneNumber = "ReceiverPhoneNumber"
        case requesterCountryCode = "RequesterCountryCode"
        case requesterPhoneNumber = "RequesterPhoneNumber"
        case cashType = "CashType"
        case commission = "Commission"
        case commissionPercent = "CommissionPercent"
        case date = "Dates"
        case senderName = "SenderName"
        case receiverName = "ReceiverName"
        case paymentType = "PaymentType"
        case convertedDate = "ConvertedDate"
        case totalBalance = "TotalBalance"
        case rechargeAmount = "RechargeAmount"
        case totalAmount = "TotalAmount"
        case bonusPoints = "BonusPoints"
        case cashBack = "CashBack"
        case transactionID = "TransactionId"
        case receiverTotalBalance = "ReceiverTotalBalance"
        case requesterTotalBalance = "RequesterTotalBalance"
        case senderOpeningBalance = "SenderOpeningBalance"
        case senderClosingBalance = "SenderClosingBalance"
    }
}

struct AddWithdrawData: Codable {
    let customerName: String?
    let customerPhoneNumber, beneficiaryAccountNumber, beneficiaryName: String?
    let sourceNumber: String?
    let amount: Double?
    let addMoneyType: String?
    let transactionDate: String?
    let bankCharges: Double?
    let adminFee: Double?
    let bankChargesType, adminFeeType: String?
    let totalAmount: Double?
    let comment, bankTransactionID, bankTransactionStatus: String?
    let okTransactionID: String?
    let okTransactionStatus: String?
    let cardNumber: String?
    
    enum CodingKeys: String, CodingKey {
        case customerName = "CustomerName"
        case customerPhoneNumber = "CustomerPhoneNumber"
        case beneficiaryAccountNumber = "BeneficiaryAccountNumber"
        case beneficiaryName = "BeneficiaryName"
        case sourceNumber = "SourceNumber"
        case amount = "Amount"
        case addMoneyType = "AddMoneyType"
        case transactionDate = "TransactionDate"
        case bankCharges = "BankCharges"
        case adminFee = "AdminFee"
        case bankChargesType = "BankChargesType"
        case adminFeeType = "AdminFeeType"
        case totalAmount = "TotalAmount"
        case comment = "Comment"
        case bankTransactionID = "BankTransactionId"
        case bankTransactionStatus = "BankTransactionStatus"
        case okTransactionID = "OkTransactionId"
        case okTransactionStatus = "OkTransactionStatus"
        case cardNumber = "CardNumber"
    }
}
