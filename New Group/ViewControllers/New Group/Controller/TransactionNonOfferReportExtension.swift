//
//  TransactionNonOfferReportExtension.swift
//  OK
//
//  Created by iMac on 3/27/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation

extension TransactionNonOfferReport {
    // MARK: - Methods -
    
    func setInitialValues() {
        if expandedTransactionTypes.count > 0 {
            buttonTransactionType.setTitle(expandedTransactionTypes[0].filterTitle.localized, for: .normal)
        }
        if amountRanges.count > 0 {
            buttonAmountRange.setTitle(amountRanges[0].filterTitle.localized, for: .normal)
        }
        if genderTypes.count > 0 {
            buttonGender.setTitle(genderTypes[0].filterTitle.localized, for: .normal)
        }
        if ageRanges.count > 0 {
            buttonAge.setTitle(ageRanges[0].filterTitle.localized, for: .normal)
        }
        if periodTypes.count > 0 {
            buttonPeriod.setTitle(periodTypes[0].filterTitle.localized, for: .normal)
        }
        buttonUser.setTitle("User".localized, for: .normal)
        buttonTransactionID.setTitle("Transaction ID".localized, for: .normal)
        buttonAmount.setTitle("Amount".localized + "\n" + "(MMK)".localized, for: .normal)
        buttonMobileNumber.setTitle("Mobile Number".localized, for: .normal)
        buttonBonus.setTitle("Bonus Points".localized, for: .normal)
        buttonCashBack.setTitle("Cash Back".localized + "\n" + "(MMK)".localized, for: .normal)
        buttonBalance.setTitle("Balance".localized + "\n" + "(MMK)".localized, for: .normal)
        buttonDate.setTitle("Date".localized, for: .normal)
        
        lblQR.text = "QR Code for Payment Verification".localized
        lblQR.layer.cornerRadius = 5
        lblQR.layer.borderWidth = 0.5
        
    }
    
    func resetFilterButtons() {
        buttonTransactionType.setTitle(expandedTransactionTypes[0].filterTitle.localized, for: .normal)
        buttonAmountRange.setTitle(amountRanges[0].filterTitle.localized, for: .normal)
        buttonGender.setTitle(genderTypes[0].filterTitle.localized, for: .normal)
        buttonAge.setTitle(ageRanges[0].filterTitle.localized, for: .normal)
        buttonPeriod.setTitle(periodTypes[0].filterTitle.localized, for: .normal)
    }
    
    func removeGestureFromShadowView() {
        if let recognizers = viewShadow.gestureRecognizers {
            for recognizer in recognizers {
                if let recognizer = recognizer as? UITapGestureRecognizer {
                    viewShadow.removeGestureRecognizer(recognizer)
                }
            }
        }
    }
    
    func configureFilterCell(cell: FilterTableViewCell, filterType: FilterType) {
        cell.labelFilterName.text = filterType.filterTitle.localized
        if filterType.hasChild {
            if filterType.isSubShowing {
                cell.imgViewDropDown.image = UIImage(named: "reportArrowUpBlue")
            } else {
                cell.imgViewDropDown.image = UIImage(named: "reportArrowDownBlue")
            }
        } else {
            cell.imgViewDropDown.image = nil
        }
    }
    
    func resetSortButtons(sender: UIButton) {
        if sender != buttonUser {
            buttonUser.tag = 0
            buttonUser.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonTransactionID {
            buttonTransactionID.tag = 0
            buttonTransactionID.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonAmount {
            buttonAmount.tag = 0
            buttonAmount.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonMobileNumber {
            buttonMobileNumber.tag = 0
            buttonMobileNumber.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonBonus {
            buttonBonus.tag = 0
            buttonBonus.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonCashBack {
            buttonCashBack.tag = 0
            buttonCashBack.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonBalance {
            buttonBalance.tag = 0
            buttonBalance.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonDate {
            buttonDate.tag = 0
            buttonDate.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
    }
    
 /*   func getQRCodeImage(qrData: ReportTransactionRecord) -> UIImage? {
        let phNumber = qrData.destination ?? "0000000000"
        var transDateStr = ""
        
        let dFormatter = DateFormatter()
        dFormatter.calendar = Calendar(identifier: .gregorian)
        dFormatter.dateFormat = "dd/MMM/yyyy HH:mm:ss"
        if let trDate = qrData.transactionDate {
            transDateStr = dFormatter.string(from: trDate)
        }
        var senderName = ""
        var senderNumber = ""
        var receiverName = ""
        var receiverBusinessName = ""
        var receiverNumber = ""
        if let sName = qrData.senderName {
            senderName = sName
            if senderName.count == 0 {
                senderName = UserModel.shared.name
            }
        }
        if let rName = qrData.receiverName{
            receiverName = rName
        }
        if let businessName = qrData.receiverBName, businessName.count > 0 {
            receiverBusinessName = businessName
        }
        if let accTransType = qrData.accTransType {
            if accTransType == "Cr" {
                if let dest = qrData.destination {
                    senderNumber = dest
                }
                receiverNumber = UserModel.shared.mobileNo
                if let transType = qrData.transType {
                    if transType == "PAYWITHOUT ID" {
                        senderNumber = "XXXXXXXXXX"
                    }
                }
            } else {
                if let dest = qrData.destination {
                    receiverNumber = dest
                }
                senderNumber = UserModel.shared.mobileNo
            }
        }
        let firstPartToEncrypt = "\(transDateStr)----\(phNumber)"
        let transID = qrData.transID ?? ""
        //let operatorName = qrData.operatorName ?? ""
        let trasnstype = qrData.transType ?? ""
        var gender = ""
        if let safeGenger = qrData.gender {
            gender = safeGenger == "1" ? "M" : "F"
        }
        let age = qrData.age > 0 ? "\(qrData.age)" : ""
        //let description = qrData.desc ?? ""
        let lattitude = qrData.latitude ?? ""
        let longitude = qrData.longitude ?? ""
        
        let firstPartEncrypted = AESCrypt.encrypt(firstPartToEncrypt, password: "m2n1shlko@$p##d")
        
        var balance = ""
        if let balanceData = qrData.walletBalance {
            balance = balanceData.toString()
        }
        var cashBackAmount = ""
        if let cashBack = qrData.cashBack {
            cashBackAmount = cashBack.toString()
        }
        var amount = ""
        if let amountData = qrData.amount {
            amount = amountData.toString()
        }
        var bonus = ""
        if let bonusData = qrData.bonus {
            bonus = bonusData.toString()
        }
        let state = UserModel.shared.state
        
        let str2 = "OK-\(senderName)-\(amount)-\(transDateStr)-\(transID)-\(senderNumber)-\(balance)-des-\(receiverNumber)-\(trasnstype)-\(cashBackAmount)-\(bonus)-\(lattitude),\(longitude)-0-\(gender)-\(age)-\(state)-\(receiverName)-\(receiverBusinessName)"
        
        let secondEncrypted = AESCrypt.encrypt(str2, password: firstPartToEncrypt)
        
        var firstPartEncryptedSafeValue = ""
        var secondEncryptedSafeValue = ""
        if let firstEncrypted = firstPartEncrypted {
            firstPartEncryptedSafeValue = firstEncrypted
        }
        if let secondEncrypt = secondEncrypted {
            secondEncryptedSafeValue = secondEncrypt
        }
        let finalEncyptFormation = "\(firstPartEncryptedSafeValue)---\(secondEncryptedSafeValue)"
        
        let qrImageObject = PTQRGenerator()
        guard let qrImage =  qrImageObject.getQRImage(stringQR: finalEncyptFormation, withSize: 10) else { return nil }
        
        return qrImage
    } */
    
    
    func getQRCodeImage(qrData: NonOfferReportList) -> UIImage? {
        let phNumber = qrData.destination ?? "0000000000"
        var transDateStr = ""
        
        let dFormatter = DateFormatter()
        dFormatter.calendar = Calendar(identifier: .gregorian)
        dFormatter.dateFormat = "dd/MMM/yyyy HH:mm:ss"
        if let trDate = qrData.transactionDate {
            //transDateStr = dFormatter.string(from: trDate)
            transDateStr = trDate
        }
        var senderName = ""
        var senderNumber = ""
        var receiverName = ""
        var receiverBusinessName = ""
        var receiverNumber = ""
        if let sName = qrData.senderName {
            senderName = sName
            if senderName.count == 0 {
                senderName = UserModel.shared.name
            }
        }
        if let rName = qrData.receiverName{
            receiverName = rName
        }
        if let businessName = qrData.receiverBName, businessName.count > 0 {
            receiverBusinessName = businessName
        }
        if let accTransType = qrData.accTransType {
            if accTransType == "Cr" {
                if let dest = qrData.destination {
                    senderNumber = dest
                }
                receiverNumber = UserModel.shared.mobileNo
                if let transType = qrData.transType {
                    if transType == "PAYWITHOUT ID" {
                        senderNumber = "XXXXXXXXXX"
                    }
                }
            } else {
                if let dest = qrData.destination {
                    receiverNumber = dest
                }
                senderNumber = UserModel.shared.mobileNo
            }
        }
        let firstPartToEncrypt = "\(transDateStr)----\(phNumber)"
        let transID = qrData.transID ?? ""
        //let operatorName = qrData.operatorName ?? ""
        let trasnstype = qrData.transType ?? ""
        var gender = ""
        if let safeGenger = qrData.gender {
            
            if safeGenger == "1" {
                gender = safeGenger == "1" ? "M" : "F"
            } else {
            gender = safeGenger
            }
        }
        //let age = qrData.age > 0 ? "\(qrData.age!)" : ""
        let age = qrData.age ?? 0
        //let description = qrData.desc ?? ""
        let lattitude = qrData.latitude ?? ""
        let longitude = qrData.longitude ?? ""
        
        let firstPartEncrypted = AESCrypt.encrypt(firstPartToEncrypt, password: "m2n1shlko@$p##d")
        
        var balance = ""
        if let balanceData = qrData.walletBalance {
            //balance = balanceData.toString()
            balance = String(describing: balanceData)
        }
        var cashBackAmount = ""
        if let cashBack = qrData.cashBack {
            //cashBackAmount = cashBack.toString()
            cashBackAmount = String(describing: cashBack)

        }
        var amount = ""
        if let amountData = qrData.amount {
            //amount = amountData.toString()
            amount = String(describing: amountData)

        }
        var bonus = ""
        if let bonusData = qrData.bonus {
           // bonus = bonusData.toString()
            bonus = String(describing: bonusData)

        }
        let state = UserModel.shared.state
        
        let str2 = "OK-\(senderName)-\(amount)-\(transDateStr)-\(transID)-\(senderNumber)-\(balance)-des-\(receiverNumber)-\(trasnstype)-\(cashBackAmount)-\(bonus)-\(lattitude),\(longitude)-0-\(gender)-\(age)-\(state)-\(receiverName)-\(receiverBusinessName)"
        
        print("lattitude all--------\(lattitude)")
        print("longitude all--------\(longitude)")
        print("gender all--------\(gender)")
        print("age all--------\(age)")

        
       //  let str2 = "OK-\(senderName)-\(amount)-\(transDateStr)-\(transID)-\(senderNumber)-\(balance)-des-\(receiverNumber)-\(trasnstype)-\(state)-\(receiverName)-\(receiverBusinessName)"
        
        let secondEncrypted = AESCrypt.encrypt(str2, password: firstPartToEncrypt)
        
        var firstPartEncryptedSafeValue = ""
        var secondEncryptedSafeValue = ""
        if let firstEncrypted = firstPartEncrypted {
            firstPartEncryptedSafeValue = firstEncrypted
        }
        if let secondEncrypt = secondEncrypted {
            secondEncryptedSafeValue = secondEncrypt
        }
        let finalEncyptFormation = "\(firstPartEncryptedSafeValue)---\(secondEncryptedSafeValue)"
        
        print("string all--------\(str2)")
        
        let qrImageObject = PTQRGenerator()
        guard let qrImage =  qrImageObject.getQRImage(stringQR: finalEncyptFormation, withSize: 10) else { return nil }
        
        return qrImage
    }
    
    func updateFilterTableHeight() {
        constraintFilterTableHeight.constant = filterArray.count > 4 ? CGFloat(5*44) : CGFloat(filterArray.count * 44)
        self.view.layoutIfNeeded()
    }
    
    // MARK: - Expand Filters
    func expandRefundOptions(selectedFilter: FilterType) {
        if selectedFilter.isSubShowing {
            filterArray = expandedTransactionTypes.filter { $0.parentType == .main }
            var refundRow = selectedFilter
            refundRow.isSubShowing = false
            filterArray[5] = refundRow
        } else {
            var parentTypes = expandedTransactionTypes.filter { $0.parentType == .main }
            let childTypes = expandedTransactionTypes.filter { $0.parentType == .refunds }
            let index = 6
            if 0 ... parentTypes.count ~= index {
                parentTypes[index..<index] = childTypes[0..<childTypes.count]
                filterArray = parentTypes
            }
            var refundRow = selectedFilter
            refundRow.isSubShowing = true
            filterArray[5] = refundRow
        }
        tableViewFilter.reloadData()
    }
    
    func expandPaymentOptions(selectedFilter: FilterType) {
        if selectedFilter.isSubShowing {
            filterArray = expandedTransactionTypes.filter { $0.parentType == .main }
            var refundRow = selectedFilter
            refundRow.isSubShowing = false
            filterArray[6] = refundRow
        } else {
            var parentTypes = expandedTransactionTypes.filter { $0.parentType == .main }
            let childTypes = expandedTransactionTypes.filter { $0.parentType == .payment }
            let index = 7
            if 0 ... parentTypes.count ~= index {
                parentTypes[index..<index] = childTypes[0..<childTypes.count]
                filterArray = parentTypes
            }
            var refundRow = selectedFilter
            refundRow.isSubShowing = true
            filterArray[6] = refundRow
        }
        tableViewFilter.reloadData()
    }
    
    func expandTransportOptions(selectedFilter: FilterType) {
        if selectedFilter.isSubShowing {
            filterArray = expandedTransactionTypes.filter { $0.parentType == .main }
            var refundRow = selectedFilter
            refundRow.isSubShowing = false
            filterArray[7] = refundRow
        } else {
            var parentTypes = expandedTransactionTypes.filter { $0.parentType == .main }
            let childTypes = expandedTransactionTypes.filter { $0.parentType == .transportation }
            let index = 8
            if 0 ... parentTypes.count ~= index {
                parentTypes[index..<index] = childTypes[0..<childTypes.count]
                filterArray = parentTypes
            }
            var refundRow = selectedFilter
            refundRow.isSubShowing = true
            filterArray[7] = refundRow
        }
        tableViewFilter.reloadData()
    }
    
    func expandRequestMoneyOptions(selectedFilter: FilterType) {
        if selectedFilter.isSubShowing {
            filterArray = expandedTransactionTypes.filter { $0.parentType == .main }
            var refundRow = selectedFilter
            refundRow.isSubShowing = false
            filterArray[8] = refundRow
        } else {
            var parentTypes = expandedTransactionTypes.filter { $0.parentType == .main }
            let childTypes = expandedTransactionTypes.filter { $0.parentType == .requestMoney }
            let index = 9
            if 0 ... parentTypes.count ~= index {
                parentTypes[index..<index] = childTypes[0..<childTypes.count]
                filterArray = parentTypes
            }
            var refundRow = selectedFilter
            refundRow.isSubShowing = true
            filterArray[8] = refundRow
        }
        tableViewFilter.reloadData()
    }
    
    // MARK: - Filter Actions
    
    func filterProcess(forTransactionType arrayToFilter: [NonOfferReportList]) -> [NonOfferReportList] {
        if let transFilter = buttonTransactionType.currentTitle {
            switch transFilter {
            case "Transaction".localized, "All".localized:
                return arrayToFilter
             
            case "Data Plan".localized:
                return arrayToFilter.filter {
                    guard let descr = $0.desc else { return false }
                    return descr.contains("Data Plan")
                }
            case "Top-Up Plan".localized:
                return arrayToFilter.filter {
                    guard let descr = $0.desc else { return false }
                    return descr.contains("Top-Up Plan")
                }
            case "Special Offer".localized:
                return arrayToFilter.filter {
                    guard let descr = $0.desc else { return false }
                    return descr.contains("Special Offer")
                }
            case "Remarks".localized:
                return arrayToFilter.filter {
                    guard let descr = $0.desc else { return false }
                    return descr.contains("Remarks: ")
                }
            case "Received Amount".localized:
                return arrayToFilter.filter {
                    guard let acTransType = $0.accTransType else { return false }
                    return acTransType == "Cr"
                }
            case "Pay / Send".localized:
                return arrayToFilter.filter {
                    guard let acTransType = $0.accTransType else { return false }
                    return acTransType == "Dr"
                }
            case "Hide My Number to pay".localized:
                return arrayToFilter.filter {
                    guard let acTransType = $0.accTransType, let transType = $0.transType else { return false }
                    return acTransType == "Dr" && transType == "PAYWITHOUT ID"
                }
            case "Received Amount no number".localized:
                return arrayToFilter.filter {
                    guard let acTransType = $0.accTransType, let transType = $0.transType else { return false }
                    return acTransType == "Cr" && transType == "PAYWITHOUT ID"
                }
            case "All Ticket Transactions".localized:
                return arrayToFilter.filter {
                    guard let transType = $0.transType else { return false }
                    return transType == "TICKET"
                }
            case "All Toll Parking Entrance Transportation".localized:
                return arrayToFilter.filter {
                    guard let transType = $0.transType else { return false }
                    return transType == "TOLL"
                }
            case "Offers".localized:
                return arrayToFilter.filter {
                    guard let descr = $0.desc else { return false }
                    return descr.contains("OK-PMC")
                }
            case "Sent Request Money".localized, "Received Request Money".localized:
                return []
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcess(forAmountType arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        func filterFromDecimalNumbers(greaterThan: NSDecimalNumber, lesserThan: NSDecimalNumber)-> [ReportTransactionRecord] {
            return arrayToFilter.filter
                {
                    guard let amount = $0.amount else { return false }
                    return (amount.compare(greaterThan) == .orderedDescending && amount.compare(lesserThan) == .orderedAscending)
            }
        }
        if let transFilter = buttonAmountRange.currentTitle {
            switch transFilter {
            case "Amount".localized, "All".localized:
                return arrayToFilter
            case "0 - 1,000".localized:
                return filterFromDecimalNumbers(greaterThan: -0.01, lesserThan: 1001)
            case "1,001 - 10,000".localized:
                return filterFromDecimalNumbers(greaterThan: 1000.99, lesserThan: 10001)
            case "10,001 - 50,000".localized:
                return filterFromDecimalNumbers(greaterThan: 10000.99, lesserThan: 50001)
            case "50,001 - 1,00,000".localized:
                return filterFromDecimalNumbers(greaterThan: 50000.99, lesserThan: 100001)
            case "1,00,001 - 2,00,000".localized:
                return filterFromDecimalNumbers(greaterThan: 100000.99, lesserThan: 200001)
            case "2,00,001 - 5,00,000".localized:
                return filterFromDecimalNumbers(greaterThan: 200000.99, lesserThan: 500001)
            case "5,00,001 - Above".localized:
                return arrayToFilter.filter {
                    guard let amount = $0.amount else { return false }
                    return (amount.compare(500000.99) == .orderedDescending)
                }
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcess(forGenderType arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        func filterString(filterString: String) -> [ReportTransactionRecord] {
            return arrayToFilter.filter {
                guard let gender = $0.gender else { return false }
                return gender == filterString
            }
        }
        if let transFilter = buttonGender.currentTitle {
            switch transFilter {
            case "Gender".localized, "All".localized:
                return arrayToFilter
            case "Male".localized:
                return filterString(filterString: "1")
            case "Female".localized:
                return filterString(filterString: "0")
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcess(forAge arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        func filterIntValue(greaterThan: Int16, lesserThan: Int16) -> [ReportTransactionRecord] {
            return arrayToFilter.filter { ($0.age > greaterThan && $0.age < lesserThan) }
        }
        if let transFilter = buttonAge.currentTitle {
            switch transFilter {
            case "Age".localized, "All".localized:
                return arrayToFilter
            case "0 - 19".localized:
                return filterIntValue(greaterThan: -1, lesserThan: 20)
            case "20 - 34".localized:
                return filterIntValue(greaterThan: 19, lesserThan: 35)
            case "35 - 50".localized:
                return filterIntValue(greaterThan: 34, lesserThan: 51)
            case "51 - 64".localized:
                return filterIntValue(greaterThan: 50, lesserThan: 65)
            case "65 - Above".localized:
                return arrayToFilter.filter { $0.age > 64 }
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcess(forPeriod arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        if let periodFilter = buttonPeriod.currentTitle {
            switch periodFilter {
            case "Period".localized, "All".localized:
                return arrayToFilter
            case "Date".localized:
                return arrayToFilter.filter {
                    guard let transDate = $0.transactionDate else { return false }
                    let dFormatter = DateFormatter()
                    dFormatter.calendar = Calendar(identifier: .gregorian)
                    dFormatter.dateFormat = "dd MMM yyyy"
                    let transDateStr = dFormatter.string(from: transDate)
                    guard let transactionDate = dFormatter.date(from: transDateStr) else { return false }
                    guard let dateStartStr = buttonDateOne.currentTitle else { return false }
                    guard let dateEndStr = buttonDateTwo.currentTitle else { return false }
                    guard let exactDateStart = dFormatter.date(from: dateStartStr) else { return false }
                    guard let dateStart = Calendar.current.date(byAdding: Calendar.Component.day, value: -1, to: exactDateStart) else { return false }
                    guard let exactDateEnd = dFormatter.date(from: dateEndStr) else { return false }
                    guard let dateEnd = Calendar.current.date(byAdding: Calendar.Component.day, value: 1, to: exactDateEnd) else { return false}
                    return (dateStart.compare(transactionDate) == .orderedAscending && transactionDate.compare(dateEnd) == .orderedAscending)
                }
            case "Month".localized:
                return arrayToFilter.filter {
                    guard let transDate = $0.transactionDate else { return false }
                    let dFormatter = DateFormatter()
                    dFormatter.calendar = Calendar(identifier: .gregorian)
                    dFormatter.dateFormat = "MMM yyyy"
                    let transDateStr = dFormatter.string(from: transDate)
                    guard let transactionDate = dFormatter.date(from: transDateStr) else { return false }
                    guard let dateStr = buttonDateTwo.currentTitle else { return false }
                    guard let monthToFilter = dFormatter.date(from: dateStr) else { return false }
                    return transactionDate.compare(monthToFilter) == .orderedSame
                }
            default:
                return []
            }
        }
        return []
    }
    
    // MARK: - DatePicker methods
    func showDateOptions(selectedOption: String) {
        switch selectedOption {
        case "Period".localized, "All".localized:
            buttonPeriod.setTitle(selectedOption, for: .normal)
            //applyFilter()
            isDateViewHidden = true
        case "Date".localized:
            dateFormatter.dateFormat = "dd MMM yyyy"
            buttonDateOne.setTitle(dateFormatter.string(from: Date()), for: .normal)
            buttonDateTwo.setTitle(dateFormatter.string(from: Date()), for: .normal)
            labelDateOne.isHidden = false
            buttonDateOne.isHidden = false
            labelDateTwo.text = "To Date".localized
            configureDatePicker(tag: 0)
            isDateViewHidden = false
        case "Month".localized:
            dateFormatter.dateFormat = "MMM yyyy"
            buttonDateOne.setTitle(nil, for: .normal)
            buttonDateTwo.setTitle(dateFormatter.string(from: Date()), for: .normal)
            labelDateOne.isHidden = true
            buttonDateOne.isHidden = true
            labelDateTwo.text = "Month".localized
            configureDatePicker(tag: 2)
            isDateViewHidden = false
        default:
            break
        }
    }
    
    func configureDatePicker(tag: Int) {
        switch tag {
        case 0:
            buttonDateOne.setTitleColor(ConstantsColor.navigationHeaderTransaction, for: .normal)
            buttonDateTwo.setTitleColor(UIColor(red: 30/255.0, green: 30/255, blue: 30/255, alpha: 1.0), for: .normal)
            datePicker.datePickerMode = .date
            datePicker.removeTarget(nil, action: nil, for: .allEvents)
            datePicker.addTarget(self, action: #selector(fromDatePickerChanged), for: .valueChanged)
            datePicker.minimumDate = nil
            if let endDateStr = buttonDateTwo.title(for: .normal), let endDate = dateFormatter.date(from: endDateStr) {
                datePicker.maximumDate = endDate
            } else {
                datePicker.maximumDate = Date()
            }
            if let currentDateStr = buttonDateOne.title(for: .normal), let currentDate = dateFormatter.date(from: currentDateStr) {
                datePicker.date = currentDate
            } else {
                buttonDateOne.setTitle(dateFormatter.string(from: Date()), for: .normal)
            }
        case 1:
            buttonDateOne.setTitleColor(UIColor(red: 30/255.0, green: 30/255, blue: 30/255, alpha: 1.0), for: .normal)
            buttonDateTwo.setTitleColor(ConstantsColor.navigationHeaderTransaction, for: .normal)
            datePicker.datePickerMode = .date
            datePicker.maximumDate = Date()
            datePicker.removeTarget(nil, action: nil, for: .allEvents)
            datePicker.addTarget(self, action: #selector(toDatePickerChanged), for: .valueChanged)
           // datePicker.minimumDate = nil
            if let endDateStr = buttonDateOne.title(for: .normal), let endDate = dateFormatter.date(from: endDateStr) {
                datePicker.minimumDate = endDate
            } else {
                datePicker.minimumDate = nil
            }

            if let currentDateStr = buttonDateTwo.title(for: .normal), let currentDate = dateFormatter.date(from: currentDateStr) {
                datePicker.date = currentDate
            } else {
                buttonDateTwo.setTitle(dateFormatter.string(from: Date()), for: .normal)
            }
        case 2:
            buttonDateTwo.setTitleColor(ConstantsColor.navigationHeaderTransaction, for: .normal)
            datePicker.datePickerMode = .date
            datePicker.maximumDate = Date()
            datePicker.date = Date()
            datePicker.removeTarget(nil, action: nil, for: .allEvents)
            datePicker.addTarget(self, action: #selector(monthPickerChanged), for: .valueChanged)
            datePicker.minimumDate = nil
            if let currentDateStr = buttonDateTwo.title(for: .normal), let currentDate = currentDateStr.dateValue(dateFormatIs: "MMM yyyy") {
                datePicker.date = currentDate
            } else {
                buttonDateTwo.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "MMM yyyy"), for: .normal)
            }
        default:
            break
        }
    }
    
    // MARK: - Button Action Methods
    // MARK: - Show Filter Options
    // It filters and show the detail by transaction type
    @IBAction func showTransactionFilterOptions(_ sender: UIButton) {
        self.view.endEditing(true)
        func showListAtTransaction() {
            filterArray = expandedTransactionTypes.filter { $0.parentType == .main }
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 5
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
        }
        if tableViewFilter.isHidden {
            showListAtTransaction()
        } else {
            switch lastSelectedFilter {
            case .transactionType :
                tableViewFilter.isHidden = true
            default:
                showListAtTransaction()
            }
        }
        lastSelectedFilter = .transactionType
    }
    
    // It filters and show the detail by amount range
    @IBAction func showAmountFilterOptions(_ sender: UIButton) {
        self.view.endEditing(true)
        func showListAtAmount() {
            filterArray = amountRanges
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 205
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
        }
        if tableViewFilter.isHidden {
            showListAtAmount()
        } else {
            switch lastSelectedFilter {
            case .amount :
                tableViewFilter.isHidden = true
            default:
                showListAtAmount()
            }
        }
        lastSelectedFilter = .amount
    }
    
    // It filters and show the detail by gender
    @IBAction func showGenderFilterOptions(_ sender: UIButton) {
        self.view.endEditing(true)
        func showListAtGender() {
            filterArray = genderTypes
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 395
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
        }
        if tableViewFilter.isHidden {
            showListAtGender()
        } else {
            switch lastSelectedFilter {
            case .gender :
                tableViewFilter.isHidden = true
            default:
                showListAtGender()
            }
        }
        lastSelectedFilter = .gender
    }
    
    // It filters and show the detail by age type
    @IBAction func showAgeFilterOptions(_ sender: UIButton) {
        self.view.endEditing(true)
        func showListAtAge() {
            filterArray = ageRanges
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 525
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
        }
        if tableViewFilter.isHidden {
            showListAtAge()
        } else {
            switch lastSelectedFilter {
            case .age :
                tableViewFilter.isHidden = true
            default:
                showListAtAge()
            }
        }
        lastSelectedFilter = .age
    }
    
    // It filters and show the detail by period type
    @IBAction func showPeriodFilterOptions(_ sender: UIButton) {
        self.view.endEditing(true)
        func showListAtPeriod() {
            filterArray = periodTypes
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 655
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
        }
        if tableViewFilter.isHidden {
            showListAtPeriod()
        } else {
            switch lastSelectedFilter {
            case .period :
                tableViewFilter.isHidden = true
            default:
                showListAtPeriod()
            }
        }
        lastSelectedFilter = .period
    }
    
    // MARK: - Sort Actions
    @IBAction func sortByUser(_ sender: UIButton) {
    }
    
    @IBAction func sortByTransactionID(_ sender: UIButton) {
        func sortTransactionID(orderType: ComparisonResult) -> [NonOfferReportList]{
            return transactionRecords.sorted {
                guard let transID1 = $0.transID, let transID2 = $1.transID else { return false }
                return transID1.localizedStandardCompare(transID2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            transactionRecords = sortTransactionID(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            transactionRecords = sortTransactionID(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            //applyFilter()
        default:
            break
        }
        resetSortButtons(sender: sender)
        tableViewTransaction.reloadData()
    }
    
    @IBAction func sortByAmount(_ sender: UIButton) {
        func sortAmount(orderType: ComparisonResult) -> [NonOfferReportList]{
            return transactionRecords.sorted {
                guard let amount1 = $0.amount, let amount2 = $1.amount else { return false }
                return true //amount1.compare(amount2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            transactionRecords = sortAmount(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            transactionRecords = sortAmount(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            //applyFilter()
        default:
            break
        }
        resetSortButtons(sender: sender)
        tableViewTransaction.reloadData()
    }
    
    @IBAction func sortByMobileNumber(_ sender: UIButton) {
        func sortMobileNumber(orderType: ComparisonResult) -> [NonOfferReportList]{
            return transactionRecords.sorted {
                guard let phNumber1 = $0.destination, let phNumber2 = $1.destination else { return false }
                return phNumber1.compare(phNumber2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            transactionRecords = sortMobileNumber(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            transactionRecords = sortMobileNumber(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            //applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewTransaction.reloadData()
    }
    
    @IBAction func sortByBonus(_ sender: UIButton) {
        func sortBonus(orderType: ComparisonResult) -> [NonOfferReportList]{
            return transactionRecords.sorted {
                guard let bonus1 = $0.bonus, let bonus2 = $1.bonus else { return false }
                return true //bonus1.compare(bonus2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            transactionRecords = sortBonus(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            transactionRecords = sortBonus(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            //applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewTransaction.reloadData()
    }
    
    @IBAction func sortByCashBack(_ sender: UIButton) {
        func sortCashBack(orderType: ComparisonResult) -> [NonOfferReportList]{
            return transactionRecords.sorted {
                guard let cashBack1 = $0.cashBack, let cashBack2 = $1.cashBack else { return false }
                return true //cashBack1.compare(cashBack2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            transactionRecords = sortCashBack(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            transactionRecords = sortCashBack(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            //applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewTransaction.reloadData()
    }
    
    @IBAction func sortByBalance(_ sender: UIButton) {
        func sortBalance(orderType: ComparisonResult) -> [NonOfferReportList]{
            return transactionRecords.sorted {
                guard let balance1 = $0.walletBalance, let balance2 = $1.walletBalance else { return false }
                return true //balance1.compare(balance2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            transactionRecords = sortBalance(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            transactionRecords = sortBalance(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            //applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewTransaction.reloadData()
    }
    
    @IBAction func sortByDate(_ sender: UIButton) {
        func sortDates(orderType: ComparisonResult) -> [NonOfferReportList]{
            return transactionRecords.sorted {
                guard let date1 = $0.transactionDate, let date2 = $1.transactionDate else { return false }
                return date1.compare(date2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            transactionRecords = sortDates(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            transactionRecords = sortDates(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            //applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewTransaction.reloadData()
    }
    
    @IBAction func configureDatePickerOne(_ sender: UIButton) {
        configureDatePicker(tag: 0)
    }
    
    @IBAction func configureDatePickerTwo(_ sender: UIButton) {
        if buttonDateOne.isHidden {
            configureDatePicker(tag: 2)
        } else {
            configureDatePicker(tag: 1)
        }
    }
}
