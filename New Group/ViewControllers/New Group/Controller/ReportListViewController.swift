//
//  ReportListViewController.swift
//  OK
//  This controller used to list all the report categories
//  Created by ANTONY on 26/01/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportListViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var tableViewReport: UITableView!
    
    // MARK: - Properties
    var cellIdentifier = "ReportItemTableViewCell"
    private enum ReportType: String {
        case allTransaction = "AllTransaction", statistics = "Statistics", topup
        case receipt, billPayments, sendMoneyToBank, resale, offer , nonOffer
        case cashInOut, addWithdraw, surveyDetails, votingSystem, solar
        case loyalty, cardlessCash
    }
    private var modelReport: ReportModel!
    private var reportItems = [(itemName: String, imageName: String, reportType: ReportType)]()
    
    // MARK: - View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        modelReport = ReportModel()
        modelReport.reportModelDelegate = self
     //  modelReport.getTransactionInfo()
        
      //  modelReport.getALLTransactionInfo(first: "0", last: "5")
        if UserModel.shared.agentType == .advancemerchant {
            reportItems = [("All Transaction Details", "reportAllTransaction", .allTransaction),
                           ("Transaction Statistics", "reportStatistics", .statistics),
                           ("Transaction Receipt", "reportReceipt", .receipt),
                           ("Send Money To Bank", "reportSendMoney", .sendMoneyToBank),
                           ("Add Money Report", "reportAddMoney", .addWithdraw),
                           ("Merchant Survey Details", "reportSurvey", .surveyDetails),
                           ("Loyalty Report", "loyalty_report", .loyalty)]
            if isOfferShow {
                reportItems.insert(("Offer Report", "reportOffer", .offer), at: 5)
            }
        } else if UserModel.shared.agentType ==  .user {
            reportItems = [("All Transaction Details", "reportAllTransaction", .allTransaction),
                           ("Transaction Statistics", "reportStatistics", .statistics),
                           ("Top-Up & Recharge", "reportTopUp", .topup),
                           ("Transaction Receipt", "reportReceipt", .receipt),
                           ("Bill Payments", "reportPayments", .billPayments),
                           ("Send Money To Bank", "reportSendMoney", .sendMoneyToBank),
                           ("Re-Sale Main Balance", "reportResale", .resale),
                           ("Add Money Report", "reportAddMoney", .addWithdraw),
                           ("Solar Home Report", "reportSolar", .solar),
                           ("Loyalty Report", "loyalty_report", .loyalty)]
            
            if isOfferShow {
                reportItems.insert(("Offer Report", "reportOffer", .offer), at: 8)
            }
            if isVotingshow {
                reportItems.insert(("Voting System", "reportVoting", .votingSystem), at: 8)
            }
        } else if UserModel.shared.agentType ==  .agent {
            reportItems = [("All Transaction Details", "reportAllTransaction", .allTransaction),
                           ("Transaction Statistics", "reportStatistics", .statistics),
                           ("Top-Up & Recharge", "reportTopUp", .topup),
                           ("Transaction Receipt", "reportReceipt", .receipt),
                           ("Bill Payments", "reportPayments", .billPayments),
                           ("Send Money To Bank", "reportSendMoney", .sendMoneyToBank),
                           ("Re-Sale Main Balance", "reportResale", .resale),
                           ("Add Money Report", "reportAddMoney", .addWithdraw),
                           ("Sent & Received Digital Money Report", "reportCashInOut", .cashInOut),
                           ("Solar Home Report", "reportSolar", .solar),
                           ("Loyalty Report", "loyalty_report", .loyalty)]
            if isOfferShow {
                reportItems.insert(("Offer Report", "reportOffer", .offer), at: 8)
            }
            if isVotingshow {
                reportItems.insert(("Voting System", "reportVoting", .votingSystem), at: 10)
            }
            self.checkIsAgent()
        }else {
            reportItems = [("All Transaction Details", "reportAllTransaction", .allTransaction),
                           ("Transaction Statistics", "reportStatistics", .statistics),
                           ("Top-Up & Recharge", "reportTopUp", .topup),
                           ("Transaction Receipt", "reportReceipt", .receipt),
                           ("Bill Payments", "reportPayments", .billPayments),
                           ("Send Money To Bank", "reportSendMoney", .sendMoneyToBank),
                           ("Re-Sale Main Balance", "reportResale", .resale),
                           ("Add Money Report", "reportAddMoney", .addWithdraw),
                           ("Sent & Received Digital Money Report", "reportCashInOut", .cashInOut),
                           ("Merchant Survey Details", "reportSurvey", .surveyDetails),
                           ("Solar Home Report", "reportSolar", .solar),
                           ("Loyalty Report", "loyalty_report", .loyalty)]
            
            if isOfferShow {
                reportItems.insert(("Offer Report", "reportOffer", .offer), at: 8)
            }
            if isVotingshow {
                reportItems.insert(("Voting System", "reportVoting", .votingSystem), at: 11)
            }
            self.checkIsAgent()
        }
        tableViewReport.tableFooterView = UIView()
        setUpNavigation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        self.navigationController?.navigationBar.barTintColor        = ConstantsColor.navigationHeaderTransaction
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Methods
    private func navigateToTransactionDetail() {
        guard let transactionDetailViewController = UIStoryboard(name: "Reports", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.transactionDetailViewController.nameAndID) as? TransactionDetailViewController else { return }
        self.navigationController?.pushViewController(transactionDetailViewController, animated: true)
    }
    
    private func navigateToStatisticsDetail() {
        guard let statisticsTransactionController = UIStoryboard(name: "Reports", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.statisticsTransactionController.nameAndID) as? StatisticsTransactionAndBillController else { return }
        statisticsTransactionController.screenType = .statistics
        self.navigationController?.pushViewController(statisticsTransactionController, animated: true)
    }
    
    private func navigateToReceiptList() {
        guard let receiptViewController = UIStoryboard(name: "Reports", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportReceiptListViewController.nameAndID) as? ReportReceiptListViewController else { return }
        self.navigationController?.pushViewController(receiptViewController, animated: true)
    }
    
    private func navigateToReportTopUp() {
        guard let reportTopUpViewController = UIStoryboard(name: "Reports", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportTopUpViewController.nameAndID) as? ReportTopUpViewController else { return }
        self.navigationController?.pushViewController(reportTopUpViewController, animated: true)
    }
    
    private func navigateToSendMoneyToBank() {
        guard let reportSendMoneyToBankController = UIStoryboard(name: "Reports", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportSendMoneyToBankController.nameAndID) as? ReportSendMoneyToBankController else { return }

        self.navigationController?.pushViewController(reportSendMoneyToBankController, animated: true)
    }
    
    private func navigateToReportBillPayments() {
        guard let reportBillPaymentsController = UIStoryboard(name: "Reports", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportBillPaymentsViewController.nameAndID) as? ReportBillPaymentsViewController else { return }
        self.navigationController?.pushViewController(reportBillPaymentsController, animated: true)
    }
    
    private func navigateToOfferReport() {
        guard let reportOfferViewController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportOfferViewController.nameAndID) as? ReportOfferViewController else { return }
        self.navigationController?.pushViewController(reportOfferViewController, animated: true)
    }
    
    private func navigateToNonOfferReport() {
        guard let reportNonOfferViewController = UIStoryboard(name: "ReportsB", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportNonOfferViewController.nameAndID) as? ReportNonOfferViewController else { return }
        self.navigationController?.pushViewController(reportNonOfferViewController, animated: true)
    }
    
    private func navigateToReSaleMainBalance() {
        guard let resaleMainBalanceController = UIStoryboard(name: "Reports", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportResaleMainBalanceController.nameAndID) as? ReportResaleMainBalanceController else { return }
        self.navigationController?.pushViewController(resaleMainBalanceController, animated: true)
    }
    
    private func navigateToSurveyList() {
        guard let surveyListController = UIStoryboard(name: "Reports", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportSurveyListController.nameAndID) as? ReportSurveyListController else { return }
        self.navigationController?.pushViewController(surveyListController, animated: true)
    }
    
    private func navigateToCashInOut() {
        guard let reportCashInOutViewController = UIStoryboard(name: "Reports", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportCashInOutViewController.nameAndID) as? ReportCashInOutViewController else { return }
        self.navigationController?.pushViewController(reportCashInOutViewController, animated: true)
    }
    
    private func navigateToAddWithdraw() {
        guard let addWithdrawViewController = UIStoryboard(name: "Reports", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportAddWithdrawViewController.nameAndID) as? ReportAddWithdrawViewController else { return }
        self.navigationController?.pushViewController(addWithdrawViewController, animated: true)
    }
    
    private func navigateToCardlessCash() {
        guard let addWithdrawViewController = UIStoryboard(name: "Reports", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportCardlessCashViewController.nameAndID) as? ReportCardlessCashViewController else { return }
        self.navigationController?.pushViewController(addWithdrawViewController, animated: true)
    }
    
    private func navigateToVoting() {
        guard let reportVotingListViewController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportVotingListViewController.nameAndID) as? ReportVotingListViewController else { return }
        self.navigationController?.pushViewController(reportVotingListViewController, animated: true)
    }
    
    private func navigateToSolar() {
        guard let reportSolarListViewController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportSolarListViewController.nameAndID) as? ReportSolarListViewController else { return }
        self.navigationController?.pushViewController(reportSolarListViewController, animated: true)
    }
    
    private func navigateToLoyalty() {
//        guard let reportLoyaltytViewController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportLoyaltyViewController.nameAndID) as? ReportLoyaltyViewController else { return }
//        self.navigationController?.pushViewController(reportLoyaltytViewController, animated: true)
        
        guard let reportLoyaltyoptionsViewController = UIStoryboard(name: "ReportsB", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportLoyaltyOptionsViewController.nameAndID) as? LoyaltyReportOptionsViewController else { return }
        self.navigationController?.pushViewController(reportLoyaltyoptionsViewController, animated: true)
    }
}

extension ReportListViewController {
    func checkIsAgent() {
        guard appDel.checkNetworkAvail() else {
            PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.noInternet.localized)
            return
        }
        let url = getUrl(urlStr: "/UserService/FetchUserContact", serverType: .cashInCashOutUrl)
        let paramsDict = ["MobileNumber" : UserModel.shared.mobileNo] //UserModel.shared.mobileNo //00959975301583

        TopupWeb.genericClass(url: url, param: paramsDict as AnyObject, httpMethod: "POST", isCicoPayTo: true, handle: { [weak self](resp, success) in
            if success {
                DispatchQueue.main.async {
                    self?.parseMultiPaymentResult(data: resp)
                }
            } else {
                DispatchQueue.main.async {
                    self?.reportItems.remove(at: 9)
                    self?.tableViewReport.reloadData()
                }
            }
        })
    }
    
    private func parseMultiPaymentResult(data: Any) {
        if let dict = data as? Dictionary<String,Any> {
            guard let success = dict.safeValueForKey("StatusCode") as? Int, success == 200 else {
                //self.reportItems.remove(at: 9)
                self.tableViewReport.reloadData()
                return
            }
            if let successDict = dict.safeValueForKey("Content") as? Dictionary<String, Any> {
                if let type = successDict["Type"] as? Int, type == 7 || type == 8 || type == 9 || type == 10 || type == 11 {
                    
                } else {
                    //self.reportItems.remove(at: 9)
                    self.tableViewReport.reloadData()
                }
            } else {
                //self.reportItems.remove(at: 9)
                self.tableViewReport.reloadData()
            }
        }
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ReportListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reportItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reportItemCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ReportItemTableViewCell
        reportItemCell!.configureCell(itemName: reportItems[indexPath.row].itemName, imageName: reportItems[indexPath.row].imageName)
        return reportItemCell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        guard !UserLogin.shared.loginSessionExpired else {
//            OKPayment.main.authenticate(screenName: reportItems[indexPath.row].reportType.rawValue, delegate: self)
//            return
//        }
        switch reportItems[indexPath.row].reportType {
        case .allTransaction:
            navigateToTransactionDetail()
        case .statistics:
            navigateToStatisticsDetail()
        case .topup:
            navigateToReportTopUp()
        case .receipt:
            navigateToReceiptList()
        case .billPayments:
            navigateToReportBillPayments()
        case .sendMoneyToBank:
            navigateToSendMoneyToBank()
        case .resale:
            navigateToReSaleMainBalance()
        case .addWithdraw:
            navigateToAddWithdraw()
        case .offer:
            navigateToOfferReport()
        case .nonOffer:
            navigateToNonOfferReport()
        case .cashInOut:
            navigateToCashInOut()
        case .surveyDetails:
            navigateToSurveyList()
        case .votingSystem:
            navigateToVoting()
        case .solar:
            navigateToSolar()
        case .loyalty:
            navigateToLoyalty()
        case .cardlessCash:
            navigateToCardlessCash()
        }
    }
}

extension ReportListViewController: NavigationSetUpProtocol {
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = ConstantsColor.navigationHeaderTransaction
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text = ConstantsController.reportListViewController.screenTitle.localized + " "
        if let navFont = UIFont(name: appFont, size: 23) {
            label.font = navFont
        }
        label.textColor = UIColor.white
        label.center = navTitleView.center
        label.textAlignment = .center
        
        let dismissBtn = UIButton(type: .custom)
        dismissBtn.setImage(#imageLiteral(resourceName: "close_icon").withRenderingMode(.alwaysOriginal), for: .normal)
        dismissBtn.addTarget(self, action: #selector(dismissScreen), for: .touchUpInside)
        
        dismissBtn.frame = CGRect(x: 0, y: 5, width: 30, height: 30)
        
        navTitleView.addSubview(dismissBtn)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
}

extension ReportListViewController: BioMetricLoginDelegate {
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful {
            switch screen {
            case ReportType.allTransaction.rawValue:
                navigateToTransactionDetail()
            case ReportType.statistics.rawValue:
                navigateToStatisticsDetail()
            case ReportType.topup.rawValue:
                navigateToReportTopUp()
            case ReportType.receipt.rawValue:
                navigateToReceiptList()
            case ReportType.billPayments.rawValue:
                navigateToReportBillPayments()
            case ReportType.sendMoneyToBank.rawValue:
                navigateToSendMoneyToBank()
            case ReportType.resale.rawValue:
                navigateToReSaleMainBalance()
            case ReportType.addWithdraw.rawValue:
                navigateToAddWithdraw()
            case ReportType.offer.rawValue:
                navigateToOfferReport()
            case ReportType.nonOffer.rawValue:
                navigateToNonOfferReport()
            case ReportType.cashInOut.rawValue:
                navigateToCashInOut()
            case ReportType.surveyDetails.rawValue:
                navigateToSurveyList()
            case ReportType.votingSystem.rawValue:
                navigateToVoting()
            case ReportType.solar.rawValue:
                navigateToSolar()
            case ReportType.cardlessCash.rawValue:
                navigateToCardlessCash()
            default:
                break
            }
        }
    }
}

extension ReportListViewController: ReportModelDelegate {
    func apiResult(message: String?, statusCode: String?) {
        //use data from model
    }
    func handleCustomError(error: CustomError) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}

//Phone Number Validations
protocol PhValidationProtocol {
    func getDataFromJSONFile() -> [PhNumValidationList]?
}

extension PhValidationProtocol {
    func getDataFromJSONFile() -> [PhNumValidationList]? {
        if let path = Bundle.main.path(forResource: "MobNumValidation", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let decoder = JSONDecoder()
                let mobNumDataResponse = try decoder.decode(ValidationListResponse.self, from: data)
                return mobNumDataResponse.validations
            } catch {
                return nil
            }
        }
        return nil
    }
    
    func myanmarValidation(_ prefix: String) -> (min: Int, max: Int, operator: String, isRejected: Bool, color: String) {
        var validationList = [PhNumValidationList]()
        let validObject =  (11,11,"MPT",false, "#013d84")
        //if the server fails to provide data we are using local json
        if phNumValidationsApi.count > 0 {
            //Api data
            validationList = phNumValidationsApi
        } else {
            //Json file data
            if phNumValidationsFile.count > 0 {
                validationList = phNumValidationsFile
            } else {
                return validObject
            }
        }
        validationList = validationList.filter {
            guard let cCode = $0.countryCode else { return false }
            return cCode == "95"
        }
       // print("validation====\(validationList)")

        //Myanmar, India, China... But filter applied, so now Myanmar only
        for item in validationList {

            if let phOperators = item.phOperators, phOperators.count > 0 {
               // print("phOperators====\(phOperators)")

                var tempObject: (Int, Int, String, Bool, String, String)?
                var finalObject: (Int, Int, String, Bool, String, String)?
                //Mpt, Telenor...
                for operatorItem in phOperators {
                   // print("operatorItem====\(operatorItem)")

                    //if let isActive = operatorItem.isActive, isActive == true {
                        if let nSeries = operatorItem.numberSeries, nSeries.count > 0 {
                          //  print("nSeries====\(nSeries)")

                            //928, 978, 995
                            for series in nSeries {
                               // print("series====\(series)")

                                if prefix.hasPrefix(series) {
                                    if let isActive = operatorItem.isActive, isActive == true {
                                        if let fObj = finalObject, fObj.5.count > series.count {
                                            continue
                                        } else {
                                            if let maxL = operatorItem.maxLenth, let minL = operatorItem.minLength, let operName = operatorItem.operatorName {
                                                tempObject = (minL, maxL, operName, false, operatorItem.operatorColor ?? "#013d84", series)
                                             //   print("insidetempObject====\(tempObject)")

                                            } else {
                                                break
                                            }
                                        }
                                    } else {
                                        finalObject = (operatorItem.minLength ?? 0, operatorItem.maxLenth ?? 0,  operatorItem.operatorName ?? "MPT", true, operatorItem.operatorColor ?? "#013d84", series)
                                       // print("insidefinalObject====\(finalObject)")

                                        break
                                    }
                                }
                            }
                            //////
                            if let safeTempObj = tempObject {
                                if let safeFinalObj = finalObject {
                                    let prefixCount = prefix.count
                                    if safeFinalObj.0 < safeTempObj.0 {
                                        if prefixCount <= safeFinalObj.0 {
                                            finalObject = safeFinalObj
                                        } else {
                                            finalObject = safeTempObj
                                        }
                                    } else {
                                        if prefixCount <= safeTempObj.0 {
                                            finalObject = safeTempObj
                                        } else {
                                            finalObject = safeFinalObj
                                        }
                                    }
                                } else {
                                    finalObject = safeTempObj
                                }
                            } else {
                                continue
                            }
                           // print("outsidefinalObject====\(finalObject)")

                        }
                    //}
                }
                ////////
                if let rtnObje = finalObject {
                    let opNa = rtnObje.2.lowercased().contains(find: "mpt") ? "Mpt" : rtnObje.2
                   // print("opNa====\(opNa)")

                    return (rtnObje.0, rtnObje.1, opNa, rtnObje.3, rtnObje.4)
                } else {
                    return validObject
                }
            } else {
                return validObject
            }
        }
        ///////
        return validObject
    }
}
