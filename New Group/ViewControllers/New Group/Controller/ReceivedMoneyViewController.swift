//
//  ReceivedMoneyViewController.swift
//  OK
//  It shows the list of transaction of received money
//  Created by ANTONY on 09/03/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReceivedMoneyViewController: UIViewController, UIGestureRecognizerDelegate {

    // MARK: - Outlets
    @IBOutlet weak var tableViewReceivedMoney: UITableView!
    @IBOutlet weak var tableViewFilter: UITableView!
    @IBOutlet weak var constraintFilterTableLeading: NSLayoutConstraint!
    @IBOutlet weak var constraintFilterTableHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintScrollContentHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintScrollTop: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var viewDatePicker: UIView!
    @IBOutlet weak var buttonDateSubmit: UIButton!{
            didSet {
                buttonDateSubmit.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
            }
        }
    @IBOutlet weak var buttonDateCancel: UIButton!{
            didSet {
                buttonDateCancel.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
            }
        }
    @IBOutlet weak var labelDateOne: UILabel!{
        didSet {
            labelDateOne.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }

    @IBOutlet weak var labelDateTwo: UILabel!{
        didSet {
            labelDateTwo.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }

    @IBOutlet weak var buttonDateOne: UIButton!{
        didSet {
            buttonDateOne.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateTwo: UIButton!{
        didSet {
            buttonDateTwo.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var imageViewQR: UIImageView!
    @IBOutlet weak var viewToShareAllRecords: UIView!
    
    //For search functions
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var textFieldSearch: UITextField!{
        didSet {
            textFieldSearch.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonCancel: UIButton!{
        didSet {
            buttonCancel.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonClear: UIButton!{
        didSet {
            buttonClear.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    //For filter functions
    @IBOutlet weak var buttonTransactionType: ButtonWithImage!{
            didSet {
                buttonTransactionType.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
            }
        }
    @IBOutlet weak var buttonAmountRange: ButtonWithImage!{
        didSet {
            buttonAmountRange.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonPeriod: ButtonWithImage!{
        didSet {
            buttonPeriod.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    //For sorting functions
    @IBOutlet weak var buttonUser: ButtonWithImage!{
        didSet {
            buttonUser.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonTransactionID: ButtonWithImage!{
        didSet {
            buttonTransactionID.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonAmount: ButtonWithImage!{
        didSet {
            buttonAmount.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonMobileNumber: ButtonWithImage!{
        didSet {
            buttonMobileNumber.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonBalance: ButtonWithImage!{
        didSet {
            buttonBalance.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDate: ButtonWithImage!{
        didSet {
            buttonDate.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonAccountType: ButtonWithImage!{
        didSet {
            buttonAccountType.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonBusinessName: ButtonWithImage!{
        didSet {
            buttonBusinessName.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }

    // MARK: - Properties
    private var modelReport: ReportModel!
    var initialRecords = [ReportTransactionRecord]()
    var transactionRecords = [ReportTransactionRecord]()
    let dateFormatter = DateFormatter()
    var filterArray = [String]()
    let receivedMoneyCellId = "ReceivedMoneyCell"
    let transactionTypes = ["Transaction", "All", "Received Money", "Received Money No Number", "Refund"]
    let amountRanges = ["Amount", "All", "0 - 1,000", "1,001 - 10,000", "10,001 - 50,000", "50,001 - 1,00,000",
                        "1,00,001 - 2,00,000", "2,00,001 - 5,00,000", "5,00,001 - Above"]
    
    let periodTypes = ["Period", "All", "Date", "Month"]
    enum FilterType {
        case transactionType, amountRange, period
    }
    
    let countValue = UserDefaults.standard.integer(forKey: "ReceivedCount")
    var lastSelectedFilter = FilterType.transactionType
    var currentBalance: NSDecimalNumber = 0
    var isToPopToGoBack = false
    var isSearchViewShow: Bool {
        get {
            return !viewSearch.isHidden
        }
        set(newValue) {
            if newValue {
                viewSearch.isHidden = false
                textFieldSearch.becomeFirstResponder()
                constraintScrollTop.constant = 0
                self.view.layoutIfNeeded()
            } else {
                textFieldSearch.text = ""
                viewSearch.isHidden = true
                constraintScrollTop.constant = -40
                self.view.layoutIfNeeded()
                self.view.endEditing(true)
            }
        }
    }
    var isDateViewHidden: Bool {
        get {
            return viewDatePicker.isHidden
        }
        set(newValue) {
            if newValue {
                viewShadow.isHidden = true
                viewDatePicker.isHidden = true
            } else {
                viewShadow.isHidden = false
                viewDatePicker.isHidden = false
            }
        }
    }
    private let refreshControl = UIRefreshControl()
    
    // MARK: - View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatter.calendar = Calendar.current
        dateFormatter.dateFormat = "dd MMM yyyy"
        println_debug("ReceivedMoneyViewController")
        AppDelegate.AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)
//        setUpNavigation()
        tableViewReceivedMoney.tag = 0
        tableViewFilter.tag = 1
        tableViewReceivedMoney.tableFooterView = UIView()
        tableViewFilter.tableFooterView = UIView()
        tableViewReceivedMoney.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshTableData(_:)), for: .valueChanged)
        buttonCancel.layer.cornerRadius = 15
        buttonClear.layer.cornerRadius = 15
        buttonCancel.addTarget(self, action: #selector(hideSearchView), for: .touchUpInside)
        buttonClear.addTarget(self, action: #selector(clearSerchTextField), for: .touchUpInside)
        
        buttonDateSubmit.layer.cornerRadius = 15
        buttonDateCancel.layer.cornerRadius = 15
        buttonDateSubmit.addTarget(self, action: #selector(filterSelectedDate), for: .touchUpInside)
        buttonDateCancel.addTarget(self, action: #selector(hideDateSelectionView), for: .touchUpInside)
        
        buttonCancel.setTitle("Cancel".localized, for: .normal)
        buttonClear.setTitle("Clear".localized, for: .normal)
        labelDateOne.text = "From Date".localized
        labelDateTwo.text = "To Date".localized
        buttonDateSubmit.setTitle("Submit".localized, for: .normal)
        buttonDateCancel.setTitle("Cancel".localized, for: .normal)
        textFieldSearch.placeholder = "Search By Transaction ID or Amount or Mobile Number or Remarks".localized
        resetFilters()
        setLocalizedForSortButtons()
        let cellFilterNibName = UINib(nibName: ConstantsXibCell.filterTableViewCell, bundle: nil)
        tableViewFilter.register(cellFilterNibName, forCellReuseIdentifier: ConstantsXibCell.filterTableViewCell)
        datePicker.calendar = Calendar(identifier: .gregorian)
        if #available(iOS 13.4, *) {
                       datePicker.preferredDatePickerStyle = .wheels
                   } else {
                       // Fallback on earlier versions
                   }
        dateFormatter.dateFormat = "dd MMM yyyy"
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        modelReport = ReportModel()
        modelReport.reportModelDelegate = self
        modelReport.getTransactionInfo()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(closeFilterView))
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
        dateFormatter.calendar = Calendar.current
        dateFormatter.dateFormat = "dd MMM yyyy"
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        setUpNavigation()
    }
    
    override func viewDidLayoutSubviews() {
        let navBarHeight = navigationController?.navigationBar.frame.size.height ?? 44
        let heightToReduce = navBarHeight == 32 ? 56 : 44
        constraintScrollContentHeight.constant = scrollView.frame.size.height - CGFloat(heightToReduce) + 44
        self.view.layoutIfNeeded()
    }
    
    // MARK: - Target Methods -
    @objc private func refreshTableData(_ sender: Any) {
//        if viewSearch.isHidden {
        textFieldSearch.text = ""
        modelReport.getTransactionInfo()
//        } else {
//            self.refreshControl.endRefreshing()
//        }
    }
    
    @objc func closeFilterView() {
        tableViewFilter.isHidden = true
    }
    
    @objc func showSearchView() {
        buttonUser.tag = 0
        buttonUser.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        buttonTransactionID.tag = 0
        buttonTransactionID.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        buttonAmount.tag = 0
        buttonAmount.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        buttonMobileNumber.tag = 0
        buttonMobileNumber.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        buttonBalance.tag = 0
        buttonBalance.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        buttonDate.tag = 0
        buttonDate.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        applyFilter()
        
        tableViewFilter.isHidden = true
        if !isSearchViewShow {
            removeGestureFromShadowView()
            viewShadow.isHidden = true
            /// For qr
            imageViewQR.isHidden = true
            imageViewQR.image = nil
            /// For share all transaction
            viewToShareAllRecords.isHidden = true
            if transactionRecords.count > 0 {
                isSearchViewShow = true
                textFieldSearch.becomeFirstResponder()
            } else {
                noRecordAlertActions()
            }
        } else {
            hideSearchView()
        }
    }
    
    
    // UIGestureRecognizerDelegate method
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: self.tableViewFilter) == true {
            return false
        }
        return true
    }
    
    
    @objc func showShareOption() {
        if viewToShareAllRecords.isHidden {
            if transactionRecords.count > 0 {
                let tapGestToRemoveView = UITapGestureRecognizer(target: self, action: #selector(shadowIsTapped))
                viewShadow.addGestureRecognizer(tapGestToRemoveView)
                viewShadow.isHidden = false
                viewToShareAllRecords.isHidden = false
            } else {
                viewShadow.isHidden = true
                noRecordAlertActions()
            }
        }
    }
    
    @objc func showGraph() {
        if transactionRecords.count == 0 {
            noRecordAlertActions()
            return
        }
        guard let statisticsTransactionController = UIStoryboard(name: "Reports", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.statisticsTransactionController.nameAndID) as? StatisticsTransactionAndBillController else { return }
        statisticsTransactionController.screenType = .receivedMoney
        statisticsTransactionController.initialRecords = initialRecords
        self.navigationController?.pushViewController(statisticsTransactionController, animated: true)
    }
    
    @objc func hideSearchView() {
        self.view.endEditing(true)
        textFieldSearch.text = ""
        isSearchViewShow = false
        buttonUser.tag = 0
        buttonUser.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        buttonTransactionID.tag = 0
        buttonTransactionID.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        buttonAmount.tag = 0
        buttonAmount.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        buttonMobileNumber.tag = 0
        buttonMobileNumber.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        buttonBalance.tag = 0
        buttonBalance.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        buttonDate.tag = 0
        buttonDate.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        applyFilter()
    }
    
    @objc func clearSerchTextField() {
        textFieldSearch.text = ""
        applyFilter()
    }
    
    @objc func filterSelectedDate() {
        if let _ = buttonDateOne.title(for: .normal) {
            buttonPeriod.setTitle("Date".localized, for: .normal)
        } else {
            buttonPeriod.setTitle("Month".localized, for: .normal)
        }
        applyFilter()
        isDateViewHidden = true
    }
    
    @objc func hideDateSelectionView() {
        isDateViewHidden = true
    }
    
    @objc func fromDatePickerChanged(datePicker:UIDatePicker) {
        buttonDateOne.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
    }
    
    @objc func toDatePickerChanged(datePicker:UIDatePicker) {
        if let startDateStr = buttonDateOne.title(for: .normal), let startDate = startDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
            switch datePicker.date.compare(startDate) {
            case .orderedAscending:
                if let endDateStr = buttonDateTwo.title(for: .normal), let endDate = endDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                    datePicker.date = endDate
                }
            case .orderedDescending, .orderedSame:
                buttonDateTwo.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            }
        } else {
            buttonDateTwo.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
        }
    }
    
    @objc func monthPickerChanged(datePicker:UIDatePicker) {
        buttonDateTwo.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "MMM yyyy"), for: .normal)
    }
    
    @objc func shadowIsTapped(sender: UITapGestureRecognizer) {
        removeGestureFromShadowView()
        viewShadow.isHidden = true
        /// For qr
        imageViewQR.isHidden = true
        imageViewQR.image = nil
        /// For share all transaction
        viewToShareAllRecords.isHidden = true
    }
    
    func setLocalizedForSortButtons() {
        buttonUser.setTitle("User".localized, for: .normal)
        buttonTransactionID.setTitle("Transaction ID".localized, for: .normal)
        buttonAmount.setTitle("Amount".localized , for: .normal)
        buttonMobileNumber.setTitle("Mobile Number".localized, for: .normal)
        buttonBalance.setTitle("Balance".localized, for: .normal)
        buttonDate.setTitle("Date & Time".localized, for: .normal)
        buttonAccountType.setTitle("Account Type".localized, for: .normal)
        buttonBusinessName.setTitle("Business Name".localized, for: .normal)
    }
    
    func noRecordAlertActions() {
        alertViewObj.wrapAlert(title: "Alert".localized, body: "No Records".localized, img: UIImage(named : "AppIcon"))
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
        })
        alertViewObj.showAlert(controller: self)
    }
    
    // MARK: - Button Action Methods
    @IBAction func shareRecordInPdf(_ sender: UIButton) {
        guard let transactionCell = sender.superview?.superview?.superview as? ReceivedMoneyCell else { return }
        guard let indexPath = tableViewReceivedMoney.indexPath(for: transactionCell) else { return }
        var pdfDictionary = [String: Any]()
        pdfDictionary["logoName"] = "appIcon_Ok"
        pdfDictionary["invoiceTitle"] = "Received Money Statement".localized
        let senderNameArray = transactionRecords[indexPath.row].senderName?.components(separatedBy: "-")
        
        let value = senderNameArray?.first ?? ""
        if value.contains("@sender"){
            let newValue = value.components(separatedBy: "@sender")
            if newValue.indices.contains(1){
                let name = newValue[1].components(separatedBy: "@businessname")
                if name.indices.contains(0){
                   pdfDictionary["senderAccName"] = name[0]
                }
            }
        }else{
           pdfDictionary["senderAccName"] = senderNameArray?.first ?? ""
        }
        
       // pdfDictionary["senderAccName"] = senderNameArray?.first ?? ""
        
        pdfDictionary["receiverAccName"] = (UserModel.shared.businessName.count != 0) ? UserModel.shared.businessName : UserModel.shared.name
    
        if let accTransType = transactionRecords[indexPath.row].accTransType {
            if accTransType == "Cr" {
                var sNumber = ""
                var rNumber = UserModel.shared.mobileNo
                if let des = transactionRecords[indexPath.row].destination {
                    sNumber = des
                    if des.hasPrefix("0095") {
                        sNumber = "(+95)0\(des.substring(from: 4))"
                    }
                }
                if rNumber.hasPrefix("0095") {
                    rNumber = "(+95)0\(rNumber.substring(from: 4))"
                }
                pdfDictionary["senderAccNo"] = sNumber
                pdfDictionary["receiverAccNo"] = rNumber
                if let transType = transactionRecords[indexPath.row].transType {
                    if transType == "PAYWITHOUT ID" {
                        pdfDictionary["senderAccNo"] = "XXXXXXXXXX"
                    }
                }
                if let businessName = transactionRecords[indexPath.row].senderBName, businessName.count > 0 {
                    pdfDictionary["businessName"] = businessName
                }
                if let desc = transactionRecords[indexPath.row].rawDesc, desc.contains(find: "Bank To Wallet") {
                    pdfDictionary["senderAccNo"] = "XXXXXXXXXX"
              }
            } else {
                var sNumber = UserModel.shared.mobileNo
                var rNumber = ""
                if let des = transactionRecords[indexPath.row].destination {
                    rNumber = des
                    if des.hasPrefix("0095") {
                        rNumber = "(+95)0\(des.substring(from: 4))"
                    }
                }
                if sNumber.hasPrefix("0095") {
                    sNumber = "(+95)0\(sNumber.substring(from: 4))"
                }
                pdfDictionary["receiverAccNo"] = rNumber
                pdfDictionary["senderAccNo"] = sNumber
                if let businessName = transactionRecords[indexPath.row].receiverBName, businessName.count > 0 {
                    pdfDictionary["businessName"] = businessName
                }
            }
        }
        var tType = ""
        var pType: String? = nil
        if let desc = transactionRecords[indexPath.row].rawDesc {
            if desc.contains("#OK") {
                if desc.contains("Top-Up") || desc.contains("TopUp") || desc.contains("Bonus Point Top Up") || desc.contains("TopUpPlan"){
                    tType = "Top-Up"
                } else if desc.contains("Data Plan") || desc.contains("DataPlan"){
                    tType = "Data Plan"
                } else {
                    tType = "Special Offers"
                }
            } else {
                if let transType = transactionRecords[indexPath.row].transType {
                    tType = transType
                    if transType == "PAYWITHOUT ID" {
                        if let accTransType = transactionRecords[indexPath.row].accTransType {
//                            if accTransType == "Cr" {
//                                tType = "XXXXXX"
//                            } else {
                                tType = "Hide My Number"
                            //}
                        }
                    } else if transType == "TOLL" {
                        tType = "TOLL"
                    } else if transType == "TICKET" {
                        tType = "TICKET"
                    } else {
                        tType = "PAYTO"
                        if desc.contains(find: "-cashin") {
                            pType = "Cash In"
                        } else if desc.contains(find: "-cashout") {
                            pType = "Cash Out"
                        } else if desc.contains(find: "-transferto") {
                            pType = "Transfer To"
                        }
                    }
                }
            }
        }
        pdfDictionary["transactionType"] = tType
        pdfDictionary["paymentType"] = pType
        pdfDictionary["transactionID"] = transactionRecords[indexPath.row].transID
        if let transDate = transactionRecords[indexPath.row].transactionDate {
            pdfDictionary["transactionDate"] = transDate.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss")
        }
        if let descriptionStr = transactionRecords[indexPath.row].desc, descriptionStr.count > 0 {
            pdfDictionary["remarks"] = descriptionStr
        }
        if let transAmount = transactionRecords[indexPath.row].amount {
            pdfDictionary["amount"] = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: transAmount.toString()) + " MMK"
        }
        if let transType = transactionRecords[indexPath.row].accTransType, transType == "Dr" {
            pdfDictionary["qrImage"] = getQRCodeImage(qrData: transactionRecords[indexPath.row])
        }
        if let promoRawDesc = transactionRecords[indexPath.row].rawDesc, promoRawDesc.contains("#OK-PMC") {
            pdfDictionary["isForPromocode"] = true
            var helper: OfferCommentHelper! = OfferCommentHelper(comment: promoRawDesc.removingPercentEncoding)
            
            if helper.promotionTransactionCount == 1 && helper.paymentType == "1" {
                pdfDictionary["promoReceiverNumber"] = transactionRecords[indexPath.row].destination?.replacingOccurrences(of: "0095", with: "(+95)0")
                if let transType = transactionRecords[indexPath.row].accTransType, transType == "Dr" {
                    //Show tx done from user login
                    pdfDictionary["senderAccNo"] = UserModel.shared.mobileNo.replacingOccurrences(of: "0095", with: "(+95)0")
                    pdfDictionary["receiverAccNo"] = helper.promoReceiverNumber
                } else {
                    pdfDictionary["senderAccNo"] = helper.senderCreditNumber
                    pdfDictionary["receiverAccNo"] = UserModel.shared.mobileNo.replacingOccurrences(of: "0095", with: "(+95)0")
                }
            }
            
            pdfDictionary["promocode"] = helper.promoCode
            if let discPercent = helper.discountPercent {
                pdfDictionary["discountPercent"] = discPercent + " %"
            }
            if let discAmount = helper.discountAmount {
                pdfDictionary["discountAmount"] = discAmount + " MMK"
            }
            if let billAmount = helper.billAmount {
                pdfDictionary["billAmount"] = billAmount + " MMK"
            }
            pdfDictionary["companyName"] = helper.companyName
            pdfDictionary["productName"] = helper.productName
            pdfDictionary["buyingQuantity"] = helper.buyingQuantity
            pdfDictionary["freeQuantity"] = helper.freeQuantity
            helper = nil
        }
        guard let pdfUrl = CodeSnippets.createInvoicePDF(dictValues: pdfDictionary, pdfFile: "OK$ Received Money Receiept") else {
            println_debug("Error - pdf not generated")
            return
        }
        let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
    
    @IBAction func shareRecordByQRCode(_ sender: UIButton) {
        self.view.endEditing(true)
        guard let transactionCell = sender.superview?.superview?.superview as? ReceivedMoneyCell else { return }
        guard let indexPath = tableViewReceivedMoney.indexPath(for: transactionCell) else { return }
        if let qrImage = getQRCodeImage(qrData: transactionRecords[indexPath.row]) {
            let tapGestToRemoveView = UITapGestureRecognizer(target: self, action: #selector(shadowIsTapped))
            viewShadow.addGestureRecognizer(tapGestToRemoveView)
            imageViewQR.image = qrImage
            viewShadow.isHidden = false
            imageViewQR.isHidden = false
        }
    }
    
    @IBAction func shareAllTransactionsInPdf(_ sender: UIButton) {
        viewShadow.isHidden = true
        viewToShareAllRecords.isHidden = true
        removeGestureFromShadowView()
        if transactionRecords.count == 0 {
            return
        }
        let pdfView = UIView()
        if let pdfHeaderView = Bundle.main.loadNibNamed("AllTransactionPdfHeader", owner: self, options: nil)?.first as? AllTransactionPdfHeader {
            pdfHeaderView.fillReceivedDetails(list: transactionRecords, dateOne: buttonDateOne.currentTitle ?? "", dateTwo: buttonDateTwo.currentTitle ?? "",
                                              currentBalance: CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: String(walletAmount())),
                                              amountRange: buttonAmountRange.currentTitle ?? "",
                                              transactionTypeFilter: buttonTransactionType.currentTitle ?? "", operatorFilter: "", typeFilter: buttonAmountRange.currentTitle ?? "",
                                              periodStr: buttonPeriod.currentTitle ?? "")
            pdfHeaderView.creditLblAmount.text = "Total Cashback Amount"
            let newHeight = pdfHeaderView.viewHeaderforReceivedMoney.frame.maxY
            pdfHeaderView.frame = CGRect(x: 0, y: 0, width: pdfHeaderView.frame.size.width, height: newHeight)
            pdfView.addSubview(pdfHeaderView)
            pdfView.frame = pdfHeaderView.bounds
            let xPos: CGFloat = pdfHeaderView.viewHeaderforReceivedMoney.frame.minX
            var yPos: CGFloat = pdfView.frame.maxY
            var index = 0
            for record in transactionRecords {
                if let pdfBodyView = Bundle.main.loadNibNamed("ReceivedMoneyPdfBody", owner: self, options: nil)?.first as? ReceivedMoneyPdfBody {
                    pdfBodyView.fillDetails(record: record)
                    let recordHeight = pdfBodyView.frame.size.height
                    let yPosInPage = Int(yPos) % 842
                    if (yPosInPage + Int(recordHeight)) > 842 {
                        yPos = yPos + CGFloat((842 - yPosInPage) + 30 + 30)
                    }
                    pdfBodyView.frame = CGRect(x: xPos, y: yPos, width: pdfBodyView.frame.size.width, height: recordHeight)
                    yPos = yPos + recordHeight + 2
                    pdfView.addSubview(pdfBodyView)
                }
                if index == 49 {
                    break
                }
                index += 1
            }
            pdfView.frame = CGRect(x: 0, y: 0, width: pdfView.frame.size.width, height: yPos + 30)
            guard let pdfUrl = CodeSnippets.generatePDF(viewForPDF: pdfView, pdfFile: "OK$ Received Money Report") else {
                println_debug("Error - pdf not generated")
                return
            }
            let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
            DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
        }
    }
    
    @IBAction func shareAllTransactionInExcel(_ sender: UIButton) {
        viewShadow.isHidden = true
        viewToShareAllRecords.isHidden = true
        removeGestureFromShadowView()
        
        if transactionRecords.count == 0 {
            return
        }
        var excelTxt = "Transaction ID,Account Holder Number,Source Number,Business Name, Account Type,Opening Balance (MMK), Cash Back (MMK), Received Amount (MMK), Closing Balance (MMK), Date & Time, Remarks\n"
        var totalAmount: NSDecimalNumber = 0
        var bonusTransAmount: NSDecimalNumber = 0
        var cashBackAmt: NSDecimalNumber = 0
        for record in transactionRecords {
            var recText = record.transID ?? ""
            var phNo = ""
            if let mobileNo = record.destination {
                var phNumber = mobileNo.replaceFirstTwoChar(withStr: "+")
                phNumber.insert(")", at: phNumber.index(phNumber.startIndex, offsetBy: 3))
                phNumber.insert("(", at: phNumber.startIndex)
                phNo = phNumber
            }
            if let tranAmount = record.amount {
                totalAmount = totalAmount.adding(tranAmount)
            }
            if let bonAmount = record.bonus {
                bonusTransAmount = bonusTransAmount.adding(bonAmount)
            }
            if let cashBAmount = record.cashBack {
                cashBackAmt = cashBackAmt.adding(cashBAmount)
            }
            var openingBal: Double = 0.0
            if let bal = record.walletBalance, let amt = record.amount {
                if let trType = record.accTransType, trType == "Cr" {
                    openingBal = Double(truncating: bal) - Double(truncating: amt)
                }
            }
            var dest = ""
            
            if record.destination == nil, let cashBack = record.cashBack, cashBack > 0 {
                dest = "XXXXXXXXXX"
                 openingBal = 0.0
            } else {
                if record.transType == "PAYWITHOUT ID" || record.transType == "TICKET" || record.transType == "TOLL" {
                    dest = "XXXXXXXXXX"
                } else {
                    dest = phNo
                }
            }
            
            if record.rawDesc?.contains(find: "MPU") ?? false || record.rawDesc?.contains(find: "Visa") ?? false {
                dest = "XXXXXXXXXX"
            } else if record.rawDesc?.contains(find: "meter refund") ?? false {
                dest = "XXXXXXXXXX"
            } else if let desc = record.rawDesc, desc.contains(find: "Bonus Point Exchange Payment") {
                dest = "XXXXXXXXXX"
            }  else if let desc = record.rawDesc, desc.contains(find: "Bank To Wallet") {
                dest = "XXXXXXXXXX"
            }
        
            let behavior = NSDecimalNumberHandler(roundingMode: NSDecimalNumber.RoundingMode.plain,
                                                  scale: 2,
                                                  raiseOnExactness: false,
                                                  raiseOnOverflow: false,
                                                  raiseOnUnderflow: false,
                                                  raiseOnDivideByZero: false)
            let calcDN = NSDecimalNumber.init(value: openingBal).rounding(accordingToBehavior: behavior)
            let userNo = CodeSnippets.getMobileNumWithBrackets(countryCode: UserModel.shared.countryCode, number: UserModel.shared.formattedNumber)
            
            var descWithNoComa = ""
            if let newDesc = record.desc {
                descWithNoComa = newDesc.replacingOccurrences(of: ",", with: "").replacingOccurrences(of: "1#", with: "").replacingOccurrences(of: "2#", with: "")
            }
            
//            let senderNameArray = record.senderName?.components(separatedBy: "-")
//            let type = (((senderNameArray?.count ?? 0) > 1) ? senderNameArray?.last ?? "" : "").components(separatedBy: ":")
            let accountType = ((record.senderBName?.count ?? 0) > 0) ? "Merchant" : "Personal"
            let dateStr = record.transactionDate?.stringValue().replacingOccurrences(of: ",", with: "")
            recText = recText + ",\(userNo),\(dest),\(record.senderBName ?? "-"),\(accountType),\(calcDN.toString()),\(record.cashBack?.toString() ?? ""),\(record.amount?.toString() ?? ""),\(record.walletBalance?.toString() ?? ""),\(dateStr ?? ""),\(descWithNoComa)\n"
            excelTxt.append(recText)
        }
        excelTxt.append("\nTotal,,,,,,\(cashBackAmt.toString()),\(totalAmount.toString())")
        guard let excelUrl = CodeSnippets.generateExcel(excelDataWithFormat: excelTxt, excelFileName: "OK$ Received Money Report") else {
            println_debug("Error - excel not generated")
            return
        }
        let activityViewController = UIActivityViewController(activityItems: [excelUrl], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
    
    // MARK: - Methods -
    func resetFilters() {
        buttonTransactionType.setTitle(transactionTypes[0].localized, for: .normal)
        buttonAmountRange.setTitle(amountRanges[0].localized, for: .normal)
        buttonPeriod.setTitle(periodTypes[0].localized, for: .normal)
    }
    
    func removeGestureFromShadowView() {
        if let recognizers = viewShadow.gestureRecognizers {
            for recognizer in recognizers {
                if let recognizer = recognizer as? UITapGestureRecognizer {
                    viewShadow.removeGestureRecognizer(recognizer)
                }
            }
        }
    }
    
    func configureFilterCell(cell: FilterTableViewCell, filterName: String) {
        cell.labelFilterName.text = filterName.localized + " "
        cell.imgViewDropDown.image = nil
        cell.labelFilterName.resetLabel()
        cell.labelFilterName.restartLabel()
    }
    
    func resetSortButtons(sender: UIButton) {
        resetFilters()
        if sender != buttonUser {
            buttonUser.tag = 0
            buttonUser.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonTransactionID {
            buttonTransactionID.tag = 0
            buttonTransactionID.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonAmount {
            buttonAmount.tag = 0
            buttonAmount.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonMobileNumber {
            buttonMobileNumber.tag = 0
            buttonMobileNumber.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonBalance {
            buttonBalance.tag = 0
            buttonBalance.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonAccountType {
            buttonAccountType.tag = 0
            buttonAccountType.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonBusinessName {
            buttonBusinessName.tag = 0
            buttonBusinessName.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonDate {
            buttonDate.tag = 0
            buttonDate.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        tableViewFilter.isHidden = true
    }
    
    func getQRCodeImage(qrData: ReportTransactionRecord) -> UIImage? {
        let phNumber = qrData.destination ?? "0000000000"
        var transDateStr = ""
        
        let dFormatter = DateFormatter()
        dFormatter.calendar = Calendar(identifier: .gregorian)
        dFormatter.dateFormat = "dd/MMM/yyyy HH:mm:ss"
        if let trDate = qrData.transactionDate {
            transDateStr = dFormatter.string(from: trDate)
        }
        var senderName = ""
        var senderNumber = ""
        var receiverName = ""
        var receiverBusinessName = ""
        var receiverNumber = ""
        if let sName = qrData.senderName {
            senderName = sName
            if senderName.count == 0 {
                senderName = UserModel.shared.name
            }
        }
        if let rName = qrData.receiverName {
            receiverName = rName
        }
        if let businessName = qrData.receiverBName, businessName.count > 0 {
            receiverBusinessName = businessName
        }
        if let accTransType = qrData.accTransType {
            if accTransType == "Cr" {
                if let dest = qrData.destination {
                    senderNumber = dest
                }
                var rNo = UserModel.shared.mobileNo
                if rNo.hasPrefix("0095") {
                   rNo = "(+95)0\(rNo.substring(from: 4))"
                }
                receiverNumber = rNo
                if let transType = qrData.transType {
                    if transType == "PAYWITHOUT ID" {
                        senderNumber = "XXXXXXXXXX"
                    }
                }
            } else {
                if let dest = qrData.destination {
                    receiverNumber = dest
                }
                senderNumber = UserModel.shared.mobileNo
            }
        }
        let firstPartToEncrypt = "\(transDateStr)----\(phNumber)"
        let transID = qrData.transID ?? ""
        //let operatorName = qrData.operatorName ?? ""
        let trasnstype = qrData.transType ?? ""
        var gender = ""
        if let safeGender = qrData.gender {
            gender = safeGender == "1" ? "M" : "F"
        }
        let age = qrData.age > 0 ? "\(qrData.age)" : ""
        //let description = qrData.desc ?? ""
        let lattitude = qrData.latitude ?? ""
        let longitude = qrData.longitude ?? ""
        
        let firstPartEncrypted = AESCrypt.encrypt(firstPartToEncrypt, password: "m2n1shlko@$p##d")
        
        var balance = ""
        if let balanceData = qrData.walletBalance {
            balance = balanceData.toString()
        }
        var cashBackAmount = ""
        if let cashBack = qrData.cashBack {
            cashBackAmount = cashBack.toString()
        }
        var amount = ""
        if let amountData = qrData.amount {
            amount = amountData.toString()
        }
        var bonus = ""
        if let bonusData = qrData.bonus {
            bonus = bonusData.toString()
        }
        let state = UserModel.shared.state
        let str2 = "OK-\(senderName)-\(amount)-\(transDateStr)-\(transID)-\(senderNumber)-\(balance)-des-\(receiverNumber)-\(trasnstype)-\(cashBackAmount)-\(bonus)-\(lattitude),\(longitude)-0-\(gender)-\(age)-\(state)-\(receiverName)-\(receiverBusinessName)"
        
        let secondEncrypted = AESCrypt.encrypt(str2, password: firstPartToEncrypt)
        
        var firstPartEncryptedSafeValue = ""
        var secondEncryptedSafeValue = ""
        if let firstEncrypted = firstPartEncrypted {
            firstPartEncryptedSafeValue = firstEncrypted
        }
        if let secondEncrypt = secondEncrypted {
            secondEncryptedSafeValue = secondEncrypt
        }
        let finalEncyptFormation = "\(firstPartEncryptedSafeValue)---\(secondEncryptedSafeValue)"
        
        let qrImageObject = PTQRGenerator()
        guard let qrImage =  qrImageObject.getQRImage(stringQR: finalEncyptFormation, withSize: 10) else { return nil }
        
        return qrImage
    }
    
    func updateFilterTableHeight() {
        constraintFilterTableHeight.constant = filterArray.count > 4 ? CGFloat(5*44) : CGFloat(filterArray.count * 44)
        self.view.layoutIfNeeded()
    }
    
    // MARK: - Filter Actions
    func applyFilter(searchText: String = "") {
        var filteredArray = initialRecords
        var searchTxt = searchText
        if isSearchViewShow && searchTxt.count == 0 {
            searchTxt = textFieldSearch.text ?? ""
            filteredArray = filteredArray.sorted {
                guard let date1 = $0.transactionDate, let date2 = $1.transactionDate else { return false }
                return date1.compare(date2) == .orderedDescending
            }
        }
        filteredArray = filterProcess(forTransactionType: filteredArray)
        filteredArray = filterProcess(forAmountType: filteredArray)
        filteredArray = filterProcess(forPeriod: filteredArray)
        if isSearchViewShow && searchTxt.count > 0 {
            filteredArray = filteredArray.filter {
                if let desc = $0.desc {
                    if desc.lowercased().range(of:searchTxt.lowercased()) != nil {
                        return true
                    }
                }
                if let transID = $0.transID {
                    if transID.lowercased().range(of: searchTxt.lowercased()) != nil {
                        return true
                    }
                }
                if let mobileNumber = $0.destination {
                    if mobileNumber.range(of: searchTxt.lowercased()) != nil {
                        return true
                    }
                }
                if let amount = $0.amount {
                    if amount.toString().range(of: searchTxt.lowercased()) != nil {
                        return true
                    }
                }
                return false
            }
        }
        transactionRecords = filteredArray
        tableViewReceivedMoney.reloadData()
        println_debug(tableViewReceivedMoney.contentInset)
        println_debug(tableViewReceivedMoney.contentOffset)
    }
    
    func filterProcess(forTransactionType arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        if let transFilter = buttonTransactionType.currentTitle {
            switch transFilter {
            case "Transaction".localized, "All".localized:
                return arrayToFilter
            case "Received Money".localized:
                return arrayToFilter.filter {
                    guard let acTransType = $0.accTransType else { return false }
                    guard let transType = $0.transType else { return false }
                    if acTransType != "Cr" {
                        return false
                    }
                    if transType == "PAYWITHOUT ID" {
                        return false
                    }
                    return true
                }
            case "Received Money No Number".localized:
                return arrayToFilter.filter {
                    guard let acTransType = $0.accTransType else { return false }
                    guard let transType = $0.transType else { return false }
                    if acTransType != "Cr" {
                        return false
                    }
                    if transType != "PAYWITHOUT ID" {
                        return false
                    }
                    return true
                }
            case "Refund".localized:
                return arrayToFilter.filter {
                    guard let descr = $0.desc else { return false }
                    return descr.contains("Refund")
                }
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcess(forAmountType arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        func filterFromDecimalNumbers(greaterThan: NSDecimalNumber, lesserThan: NSDecimalNumber)-> [ReportTransactionRecord] {
            return arrayToFilter.filter
                {
                    guard let amount = $0.amount else { return false }
                    return (amount.compare(greaterThan) == .orderedDescending && amount.compare(lesserThan) == .orderedAscending)
            }
        }
        if let transFilter = buttonAmountRange.currentTitle {
            switch transFilter {
            case "Amount".localized, "All".localized:
                return arrayToFilter
            case "0 - 1,000".localized:
                return filterFromDecimalNumbers(greaterThan: -0.01, lesserThan: 1001)
            case "1,001 - 10,000".localized:
                return filterFromDecimalNumbers(greaterThan: 1000.99, lesserThan: 10001)
            case "10,001 - 50,000".localized:
                return filterFromDecimalNumbers(greaterThan: 10000.99, lesserThan: 50001)
            case "50,001 - 1,00,000".localized:
                return filterFromDecimalNumbers(greaterThan: 50000.99, lesserThan: 100001)
            case "1,00,001 - 2,00,000".localized:
                return filterFromDecimalNumbers(greaterThan: 100000.99, lesserThan: 200001)
            case "2,00,001 - 5,00,000".localized:
                return filterFromDecimalNumbers(greaterThan: 200000.99, lesserThan: 500001)
            case "5,00,001 - Above".localized:
                return arrayToFilter.filter {
                    guard let amount = $0.amount else { return false }
                    return (amount.compare(500000.99) == .orderedDescending)
                }
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcess(forPeriod arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        if let periodFilter = buttonPeriod.currentTitle {
            switch periodFilter {
            case "Period".localized, "All".localized:
                return arrayToFilter
            case "Date".localized:
                return arrayToFilter.filter {
                    guard let transDate = $0.transactionDate else { return false }
                    let dFormatter = DateFormatter()
                    dFormatter.calendar = Calendar(identifier: .gregorian)
                    dFormatter.dateFormat = "dd MMM yyyy"
                    let transDateStr = dFormatter.string(from: transDate)
                    guard let transactionDate = dFormatter.date(from: transDateStr) else { return false }
                    guard let dateStartStr = buttonDateOne.currentTitle else { return false }
                    guard let dateEndStr = buttonDateTwo.currentTitle else { return false }
                    guard let exactDateStart = dFormatter.date(from: dateStartStr) else { return false }
                    guard let dateStart = Calendar.current.date(byAdding: Calendar.Component.day, value: -1, to: exactDateStart) else { return false }
                    guard let exactDateEnd = dFormatter.date(from: dateEndStr) else { return false }
                    guard let dateEnd = Calendar.current.date(byAdding: Calendar.Component.day, value: 1, to: exactDateEnd) else { return false}
                    return (dateStart.compare(transactionDate) == .orderedAscending && transactionDate.compare(dateEnd) == .orderedAscending)
                }
            case "Month".localized:
                return arrayToFilter.filter {
                    guard let transDate = $0.transactionDate else { return false }
                    let dFormatter = DateFormatter()
                    dFormatter.calendar = Calendar(identifier: .gregorian)
                    dFormatter.dateFormat = "MMM yyyy"
                    let transDateStr = dFormatter.string(from: transDate)
                    guard let transactionDate = dFormatter.date(from: transDateStr) else { return false }
                    guard let dateStr = buttonDateTwo.currentTitle else { return false }
                    guard let monthToFilter = dFormatter.date(from: dateStr) else { return false }
                    return transactionDate.compare(monthToFilter) == .orderedSame
                }
            default:
                return []
            }
        }
        return []
    }
    
    func loadData() {
        initialRecords = ReportCoreDataManager.sharedInstance.fetchTransactionRecords()
        initialRecords = initialRecords.filter {
            guard let transType = $0.accTransType else { return false }
            return transType == "Cr"
        }
        if initialRecords.count > 0 {
            currentBalance = initialRecords[0].walletBalance ?? 0
        }
        transactionRecords = initialRecords
        
        DispatchQueue.main.async {
            self.tableViewReceivedMoney.reloadData()
        }
    }
    
    // MARK: - DatePicker methods
    func showDateOptions(selectedOption: String) {
        switch selectedOption {
        case "Period".localized, "All".localized:
            buttonPeriod.setTitle(selectedOption, for: .normal)
            applyFilter()
            isDateViewHidden = true
        case "Date".localized:
            dateFormatter.dateFormat = "dd MMM yyyy"
            buttonDateOne.setTitle(dateFormatter.string(from: Date()), for: .normal)
            buttonDateTwo.setTitle(dateFormatter.string(from: Date()), for: .normal)
            labelDateOne.isHidden = false
            buttonDateOne.isHidden = false
            labelDateTwo.text = "To Date".localized
            configureDatePicker(tag: 0)
            isDateViewHidden = false
        case "Month".localized:
            dateFormatter.dateFormat = "MMM yyyy"
            buttonDateOne.setTitle(nil, for: .normal)
            buttonDateTwo.setTitle(dateFormatter.string(from: Date()), for: .normal)
            labelDateOne.isHidden = true
            buttonDateOne.isHidden = true
            labelDateTwo.text = "Month".localized
            configureDatePicker(tag: 2)
            isDateViewHidden = false
        default:
            break
        }
    }
    
    func configureDatePicker(tag: Int) {
        switch tag {
        case 0:
            buttonDateOne.setTitleColor(ConstantsColor.navigationHeaderTransaction, for: .normal)
            buttonDateTwo.setTitleColor(UIColor(red: 30/255.0, green: 30/255, blue: 30/255, alpha: 1.0), for: .normal)
            datePicker.datePickerMode = .date
            datePicker.removeTarget(nil, action: nil, for: .allEvents)
            datePicker.addTarget(self, action: #selector(fromDatePickerChanged), for: .valueChanged)
            datePicker.minimumDate = nil
            if let endDateStr = buttonDateTwo.title(for: .normal), let endDate = dateFormatter.date(from: endDateStr) {
                datePicker.maximumDate = endDate
            } else {
                datePicker.maximumDate = Date()
            }
            if let currentDateStr = buttonDateOne.title(for: .normal), let currentDate = dateFormatter.date(from: currentDateStr) {
                datePicker.date = currentDate
            } else {
                buttonDateOne.setTitle(dateFormatter.string(from: Date()), for: .normal)
            }
        case 1:
            buttonDateOne.setTitleColor(UIColor(red: 30/255.0, green: 30/255, blue: 30/255, alpha: 1.0), for: .normal)
            buttonDateTwo.setTitleColor(ConstantsColor.navigationHeaderTransaction, for: .normal)
            datePicker.datePickerMode = .date
            datePicker.maximumDate = Date()
            datePicker.removeTarget(nil, action: nil, for: .allEvents)
            datePicker.addTarget(self, action: #selector(toDatePickerChanged), for: .valueChanged)
          //  datePicker.minimumDate = nil
            if let endDateStr = buttonDateOne.title(for: .normal), let endDate = dateFormatter.date(from: endDateStr) {
                datePicker.minimumDate = endDate
            } else {
                datePicker.minimumDate = nil
            }

            if let currentDateStr = buttonDateTwo.title(for: .normal), let currentDate = dateFormatter.date(from: currentDateStr) {
                datePicker.date = currentDate
            } else {
                buttonDateTwo.setTitle(dateFormatter.string(from: Date()), for: .normal)
            }
        case 2:
            buttonDateTwo.setTitleColor(ConstantsColor.navigationHeaderTransaction, for: .normal)
            datePicker.datePickerMode = .date
            datePicker.maximumDate = Date()
            datePicker.date = Date()
            datePicker.removeTarget(nil, action: nil, for: .allEvents)
            datePicker.addTarget(self, action: #selector(monthPickerChanged), for: .valueChanged)
            datePicker.minimumDate = nil
            if let currentDateStr = buttonDateTwo.title(for: .normal), let currentDate = currentDateStr.dateValue(dateFormatIs: "MMM yyyy") {
                datePicker.date = currentDate
            } else {
                buttonDateTwo.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "MMM yyyy"), for: .normal)
            }
        default:
            break
        }
    }
    
    // MARK: - Button Action Methods
    // MARK: - Show Filter Options
    // It filters and show the detail by transaction type
    @IBAction func showTransactionFilterOptions(_ sender: UIButton) {
        self.view.endEditing(true)
        func showListAtTransaction() {
            filterArray = transactionTypes
            tableViewFilter.isHidden = false
            tableViewFilter.reloadData()
            constraintFilterTableLeading.constant = 70
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
        }
        if tableViewFilter.isHidden {
            showListAtTransaction()
        } else {
            switch lastSelectedFilter {
            case .transactionType :
                tableViewFilter.isHidden = true
            default:
                showListAtTransaction()
            }
        }
        lastSelectedFilter = .transactionType
    }
    
    // It filters and show the detail by amount range
    @IBAction func showAmountFilterOptions(_ sender: UIButton) {
        self.view.endEditing(true)
        func showListAtAmount() {
            filterArray = amountRanges
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 350
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
        }
        if tableViewFilter.isHidden {
            showListAtAmount()
        } else {
            switch lastSelectedFilter {
            case .amountRange :
                tableViewFilter.isHidden = true
            default:
                showListAtAmount()
            }
        }
        lastSelectedFilter = .amountRange
    }
    
    // It filters and show the detail by period type
    @IBAction func showPeriodFilterOptions(_ sender: UIButton) {
        self.view.endEditing(true)
        func showListAtPeriod() {
            filterArray = periodTypes
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 635
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
        }
        if tableViewFilter.isHidden {
            showListAtPeriod()
        } else {
            switch lastSelectedFilter {
            case .period :
                tableViewFilter.isHidden = true
            default:
                showListAtPeriod()
            }
        }
        lastSelectedFilter = .period
    }
    
    // MARK: - Sort Actions
    @IBAction func sortByUser(_ sender: UIButton) {
    }
    
    @IBAction func sortByTransactionID(_ sender: UIButton) {
        func sortTransactionID(orderType: ComparisonResult) -> [ReportTransactionRecord]{
            return transactionRecords.sorted {
                guard let transID1 = $0.transID, let transID2 = $1.transID else { return false }
                return transID1.localizedStandardCompare(transID2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            transactionRecords = sortTransactionID(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            transactionRecords = sortTransactionID(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewReceivedMoney.reloadData()
    }
    
    @IBAction func sortByAccType(_ sender: UIButton) {
        println_debug("sortByAccType")
        func sortAccType(orderType: ComparisonResult) -> [ReportTransactionRecord] {
            return transactionRecords.sorted {
                println_debug($0.senderName)
                println_debug($1.senderName)
                let accType1 = UitilityClass.getAccountType(accountName: $0.senderName,businessName: ($0.accTransType == "Dr") ? $0.receiverBName?.lowercased() : $0.senderBName?.lowercased(),txType: $0.accTransType,rawDesc: $0.transType)
                println_debug(accType1)
                let accType2 = UitilityClass.getAccountType(accountName: $1.senderName,businessName: ($1.accTransType == "Dr") ? $1.receiverBName?.lowercased() : $1.senderBName?.lowercased(),txType: $1.accTransType,rawDesc: $1.transType)
                println_debug(accType2)
                return accType1.localizedStandardCompare(accType2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            transactionRecords = sortAccType(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            transactionRecords = sortAccType(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewReceivedMoney.reloadData()
    }
    
    @IBAction func sortByAmount(_ sender: UIButton) {
        func sortAmount(orderType: ComparisonResult) -> [ReportTransactionRecord]{
            return transactionRecords.sorted {
                guard let amount1 = $0.amount, let amount2 = $1.amount else { return false }
                return amount1.compare(amount2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            transactionRecords = sortAmount(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            transactionRecords = sortAmount(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewReceivedMoney.reloadData()
    }
    
    @IBAction func sortByMobileNumber(_ sender: UIButton) {
        func sortMobileNumber(orderType: ComparisonResult) -> [ReportTransactionRecord]{
            return transactionRecords.sorted {
                guard let phNumber1 = $0.destination, let phNumber2 = $1.destination else { return false }
                return phNumber1.compare(phNumber2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            transactionRecords = sortMobileNumber(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            transactionRecords = sortMobileNumber(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewReceivedMoney.reloadData()
    }
    
    @IBAction func sortByBalance(_ sender: UIButton) {
        func sortBalance(orderType: ComparisonResult) -> [ReportTransactionRecord]{
            return transactionRecords.sorted {
                guard let balance1 = $0.walletBalance, let balance2 = $1.walletBalance else { return false }
                return balance1.compare(balance2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            transactionRecords = sortBalance(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            transactionRecords = sortBalance(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewReceivedMoney.reloadData()
    }
    
    @IBAction func sortByDate(_ sender: UIButton) {
        func sortDates(orderType: ComparisonResult) -> [ReportTransactionRecord]{
            return transactionRecords.sorted {
                guard let date1 = $0.transactionDate, let date2 = $1.transactionDate else { return false }
                return date1.compare(date2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            transactionRecords = sortDates(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            transactionRecords = sortDates(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewReceivedMoney.reloadData()
    }
    
    @IBAction func sortByBusinessName(_ sender: UIButton) {
           func sortBusinessName(orderType: ComparisonResult) -> [ReportTransactionRecord]{
               return transactionRecords.sorted {
                   let bName1 = ($0.accTransType == "Dr") ? $0.receiverBName?.lowercased() ?? "A" : $0.senderBName?.lowercased() ?? "A"
                   println_debug(bName1)
                   let bName2 = ($1.accTransType == "Dr") ? $1.receiverBName?.lowercased() ?? "A": $1.senderBName?.lowercased() ?? "A"
                   println_debug(bName2)
                   return bName1.localizedCaseInsensitiveCompare(bName2) == orderType
               }
           }
           switch sender.tag {
           case 0:
               sender.tag = 1
               sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
               transactionRecords = sortBusinessName(orderType: .orderedAscending)
           case 1:
               sender.tag = 2
               sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
               transactionRecords = sortBusinessName(orderType: .orderedDescending)
           case 2:
               sender.tag = 0
               sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
               applyFilter(searchText: textFieldSearch.text ?? "")
           default:
               sender.tag = 0
           }
           resetSortButtons(sender: sender)
           tableViewReceivedMoney.reloadData()
       }
    
    @IBAction func configureDatePickerOne(_ sender: UIButton) {
        configureDatePicker(tag: 0)
    }
    
    @IBAction func configureDatePickerTwo(_ sender: UIButton) {
        if buttonDateOne.isHidden {
            configureDatePicker(tag: 2)
        } else {
            configureDatePicker(tag: 1)
        }
    }
}

// MARK: - UITextFieldDelegate
extension ReceivedMoneyViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tableViewFilter.isHidden = true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        tableViewReceivedMoney.contentOffset = CGPoint(x: 0, y: 0)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
            if range.location == 0 && string == " " { return false}
            if let text = textField.text, let textRange = Range(range, in: text) {
                let updatedText = text.replacingCharacters(in: textRange, with: string)
                if text.last == " " && string == " " { return false }
                if updatedText.count > 50 || (updatedText == "" && text == "") {
                    return false
                }
                if string != "\n" {
                    applyFilter(searchText: updatedText)
                }
            }
            return true
        
        /*
        if textField == textFieldSearch{
            
            if range.location == 0 && string == " " {
                   return false
            }
            
            if textField.text?.characters.last == " "  && string == " "{
                // If consecutive spaces entered by user
                return false
            }
            
            // If no consecutive space entered by user
            
            return true
        }
        
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            if updatedText.count > 50 || (updatedText == "" && text == "") {
                return false
            }
            if string != "\n" {
                applyFilter(searchText: updatedText)
            }
        }
        return true
        */
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ReceivedMoneyViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        DispatchQueue.main.async {
            switch tableView.tag {
            case 0:
                if self.transactionRecords.count == 0 {
                    let noDataLabel = UILabel(frame: tableView.bounds)
                  if let navFont = UIFont(name: appFont, size: 23) {
                    noDataLabel.font = navFont
                  }
                    noDataLabel.text = "No Records".localized
                    noDataLabel.textAlignment = .center
                    tableView.backgroundView = noDataLabel
                } else {
                    tableView.backgroundView = nil
                }
            default:
                break
            }
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView.tag {
        case 0:
            return transactionRecords.count
        case 1:
            return filterArray.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView.tag {
        case 0:
            let receivedMoneyCell = tableView.dequeueReusableCell(withIdentifier: receivedMoneyCellId, for: indexPath) as? ReceivedMoneyCell
            receivedMoneyCell!.configureReceivedMoneyCell(transRecord: transactionRecords[indexPath.row])
            if countValue != 0, indexPath.row <= countValue {
                receivedMoneyCell?.labelAmount.textColor = .red
                receivedMoneyCell?.labelTransaction.font = .boldSystemFont(ofSize: 13)
                receivedMoneyCell?.labelAccountType.font = .boldSystemFont(ofSize: 13)
                receivedMoneyCell?.labelBalance.font = .boldSystemFont(ofSize: 13)
                receivedMoneyCell?.labelDate.font = .boldSystemFont(ofSize: 13)
                receivedMoneyCell?.labelMobileNo.font = .boldSystemFont(ofSize: 13)
            }
          
            return receivedMoneyCell!
        case 1:
            let filterCell = tableView.dequeueReusableCell(withIdentifier: ConstantsXibCell.filterTableViewCell, for: indexPath) as? FilterTableViewCell
            configureFilterCell(cell: filterCell!, filterName: filterArray[indexPath.row])
            return filterCell!
        default:
            let receivedMoneyCell = tableView.dequeueReusableCell(withIdentifier: receivedMoneyCellId, for: indexPath) as? ReceivedMoneyCell
            return receivedMoneyCell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView.tag {
        case 0:
            break
        case 1:
            let selectedFilter = filterArray[indexPath.row]
            switch lastSelectedFilter {
            case .transactionType:
                buttonTransactionType.setTitle(selectedFilter.localized, for: .normal)
                tableViewFilter.isHidden = true
                applyFilter()
            case .amountRange:
                buttonAmountRange.setTitle(selectedFilter.localized, for: .normal)
                tableViewFilter.isHidden = true
                applyFilter()
            case .period:
                tableViewFilter.isHidden = true
                showDateOptions(selectedOption: selectedFilter.localized)
            }
        default:
            break
        }
    }
}

// MARK: - Additional Methods
extension ReceivedMoneyViewController: NavigationSetUpProtocol {
    /// It set up the navigation bar
    func setUpNavigation() {
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationController?.navigationBar.barTintColor        = ConstantsColor.navigationHeaderTransaction
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: titleViewWidth, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        
        let searchButton = UIButton(type: .custom)
        searchButton.setImage(#imageLiteral(resourceName: "reportSearch").withRenderingMode(.alwaysOriginal), for: .normal)
        searchButton.addTarget(self, action: #selector(showSearchView), for: .touchUpInside)
        
        let shareButton = UIButton(type: .custom)
        shareButton.setImage(#imageLiteral(resourceName: "reportShare").withRenderingMode(.alwaysOriginal), for: .normal)
        shareButton.addTarget(self, action: #selector(showShareOption), for: .touchUpInside)
        
        let graphButton = UIButton(type: .custom)
        graphButton.setImage(#imageLiteral(resourceName: "reportGraph").withRenderingMode(.alwaysOriginal), for: .normal)
        graphButton.addTarget(self, action: #selector(showGraph), for: .touchUpInside)
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "tabBarBack").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        let ypos = (titleViewSize.height - 30 ) / 2
        if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 180, y: ypos, width: 30, height: 30)
            shareButton.frame = CGRect(x: navTitleView.frame.size.width - 140, y: ypos, width: 30, height: 30)
            graphButton.frame = CGRect(x: navTitleView.frame.size.width - 100, y: ypos, width: 30, height: 30)
            backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        }
        else{
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 110, y: ypos, width: 30, height: 30)
            shareButton.frame = CGRect(x: navTitleView.frame.size.width - 70, y: ypos, width: 30, height: 30)
            graphButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
            backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        }
       
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 240, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text = ConstantsController.receivedMoneyViewController.screenTitle.localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 23) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        navTitleView.addSubview(searchButton)
        navTitleView.addSubview(shareButton)
        navTitleView.addSubview(graphButton)
        self.navigationItem.titleView = navTitleView
    }
    
    /// It shows the home screen
    @objc func backAction() {
        if isToPopToGoBack {
            if isSearchViewShow {
                hideSearchView()
            } else {
                AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
                self.navigationController?.popViewController(animated: true)
            }
        } else {
            if isSearchViewShow {
                hideSearchView()
            } else {
                AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}

extension ReceivedMoneyViewController: UIScrollViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        targetContentOffset.pointee.y = max(targetContentOffset.pointee.y - 1, 1)
    }
}

extension ReceivedMoneyViewController: ReportModelDelegate {
    func apiResult(message: String?, statusCode: String?) {
        DispatchQueue.main.async {
            if self.refreshControl.isRefreshing {
                self.loadData()
                self.tableViewReceivedMoney.reloadData()
                self.resetFilters()
                self.resetSortButtons(sender: UIButton())
                self.refreshControl.endRefreshing()
            } else {
                self.loadData()
            }
            CodeSnippets.performReceivedCount(isToAdd: false)
            println_debug("/n/nRecieved Count is 0 /n/n")
        }
    }
    func handleCustomError(error: CustomError) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}
