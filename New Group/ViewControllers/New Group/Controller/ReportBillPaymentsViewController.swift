//
//  ReportBillPaymentsViewController.swift
//  OK
//  This controller shows the list of bill payment records
//  Created by ANTONY on 02/02/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportBillPaymentsViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var tableViewBillPayments: UITableView!
    @IBOutlet weak var tableViewFilter: UITableView!
    @IBOutlet weak var constraintFilterTableLeading: NSLayoutConstraint!
    @IBOutlet weak var constraintFilterTableHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintScrollContentHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var constraintScrollTop: NSLayoutConstraint!
    
    //For search functions
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var textFieldSearch: UITextField!{
        didSet {
            textFieldSearch.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonCancel: UIButton!{
        didSet {
            buttonCancel.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonClear: UIButton!{
        didSet {
            buttonClear.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    //For filter functions
    @IBOutlet weak var buttonBillType: ButtonWithImage!{
        didSet {
            buttonBillType.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonAmountRange: ButtonWithImage!{
        didSet {
            buttonAmountRange.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonServiceProviderType: ButtonWithImage!{
        didSet {
            buttonServiceProviderType.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonPeriod: ButtonWithImage!{
        didSet {
            buttonPeriod.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    //For sorting functions
    @IBOutlet weak var buttonTransactionID: ButtonWithImage!{
        didSet {
            buttonTransactionID.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonAmount: ButtonWithImage!{
        didSet {
            buttonAmount.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonMobileNumber: ButtonWithImage!{
        didSet {
            buttonMobileNumber.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonBonus: ButtonWithImage!{
        didSet {
            buttonBonus.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonCashBack: ButtonWithImage!{
        didSet {
            buttonCashBack.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonBalance: ButtonWithImage!{
        didSet {
            buttonBalance.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateTime: ButtonWithImage!{
        didSet {
            buttonDateTime.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonBillMonth: ButtonWithImage!{
        didSet {
            buttonBillMonth.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonBillDuedate: ButtonWithImage!{
        didSet {
            buttonBillDuedate.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var buttonDateSubmit: UIButton!{
        didSet {
            buttonDateSubmit.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateCancel: UIButton!{
        didSet {
            buttonDateCancel.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDateOne: UILabel!{
        didSet {
            labelDateOne.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDateTwo: UILabel!{
        didSet {
            labelDateTwo.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateOne: UIButton!{
        didSet {
            buttonDateOne.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateTwo: UIButton!{
        didSet {
            buttonDateTwo.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var imageViewQR: UIImageView!
    @IBOutlet weak var viewToShareAllRecords: UIView!
    @IBOutlet weak var viewDatePicker: UIView!
    
    // MARK: - Properties
    var initialRecords = [ReportTransactionRecord]()
    var billRecords = [ReportTransactionRecord]()
    let cellReportBillPaymentId = "ReportBillPaymentCell"
    let expandedServiceProviderTypes = [FilterType(rootFilter: .serviceProviderType, filterTitle: "Type"),
                                        FilterType(rootFilter: .serviceProviderType, filterTitle: "All"),
                                                FilterType(rootFilter: .serviceProviderType, filterTitle: "Mobile Postpaid", hasChild: true),
                                                FilterType(rootFilter: .serviceProviderType, filterTitle: "Mpt", parentType: .mobilePostpaid),
                                                FilterType(rootFilter: .serviceProviderType, filterTitle: "Telenor", parentType: .mobilePostpaid),
                                                FilterType(rootFilter: .serviceProviderType, filterTitle: "Ooredoo", parentType: .mobilePostpaid),
                                                FilterType(rootFilter: .serviceProviderType, filterTitle: "Mectel", parentType: .mobilePostpaid),
                                                FilterType(rootFilter: .serviceProviderType, filterTitle: "Mytel", parentType: .mobilePostpaid),
                                                FilterType(rootFilter: .serviceProviderType, filterTitle: "Gift Card", hasChild: true),
                                                FilterType(rootFilter: .serviceProviderType, filterTitle: "iTunes Gift Card", parentType: .giftCard),
                                                FilterType(rootFilter: .serviceProviderType, filterTitle: "Google Play Gift Card", parentType: .giftCard),
                                                FilterType(rootFilter: .serviceProviderType, filterTitle: "Skype Wallet Gift Card", parentType: .giftCard),
                                                FilterType(rootFilter: .serviceProviderType, filterTitle: "Steam Wallet Gift Card", parentType: .giftCard),
                                                FilterType(rootFilter: .serviceProviderType, filterTitle: "Facebook Gift Card", parentType: .giftCard),
                                                FilterType(rootFilter: .serviceProviderType, filterTitle: "Xbox Gift Card", parentType: .giftCard),
                                                FilterType(rootFilter: .serviceProviderType, filterTitle: "Windows Store Gift Card", parentType: .giftCard),
                                                FilterType(rootFilter: .serviceProviderType, filterTitle: "Viber Out Credits", parentType: .giftCard),
                                                FilterType(rootFilter: .serviceProviderType, filterTitle: "Electricity", hasChild: false)]
//                                                FilterType(rootFilter: .serviceProviderType, filterTitle: "My Meter", parentType: .electricity),
//                                                FilterType(rootFilter: .serviceProviderType, filterTitle: "Other Meter", parentType: .electricity)]
    let amountRanges = [FilterType(rootFilter: .amount, filterTitle: "Amount"), FilterType(rootFilter: .amount, filterTitle: "All"), FilterType(rootFilter: .amount, filterTitle: "0 - 1,000"),
                                FilterType(rootFilter: .amount, filterTitle: "1,001 - 10,000"), FilterType(rootFilter: .amount, filterTitle: "10,001 - 50,000"),
                                FilterType(rootFilter: .amount, filterTitle: "50,001 - 1,00,000"), FilterType(rootFilter: .amount, filterTitle: "1,00,001 - 2,00,000"),
                                FilterType(rootFilter: .amount, filterTitle: "2,00,001 - 5,00,000"), FilterType(rootFilter: .amount, filterTitle: "5,00,001 - Above")]
    let billTypes = [FilterType(rootFilter: .billType, filterTitle: "Transaction"), FilterType(rootFilter: .billType, filterTitle: "All"), FilterType(rootFilter: .billType, filterTitle: "Mobile Postpaid"), FilterType(rootFilter: .billType, filterTitle: "Landlines"),
                             FilterType(rootFilter: .billType, filterTitle: "Electricity"), FilterType(rootFilter: .billType, filterTitle: "YCDC"),
                             FilterType(rootFilter: .billType, filterTitle: "Gift Card"), FilterType(rootFilter: .billType, filterTitle: "DTH")]
    let periodTypes = [FilterType(rootFilter: .period, filterTitle: "Period"), FilterType(rootFilter: .period, filterTitle: "All"), FilterType(rootFilter: .period, filterTitle: "Date"), FilterType(rootFilter: .period, filterTitle: "Month")]
    var filterArray = [FilterType]()
    var currentBalance: NSDecimalNumber = 0
    var lastSelectedFilter = RootFilter.billType
    var isSearchViewShow: Bool {
        get {
            return !viewSearch.isHidden
        }
        set(newValue) {
            if newValue {
                viewSearch.isHidden = false
                textFieldSearch.becomeFirstResponder()
                constraintScrollTop.constant = 0
                self.view.layoutIfNeeded()
            } else {
                textFieldSearch.text = ""
                viewSearch.isHidden = true
                constraintScrollTop.constant = -40
                self.view.layoutIfNeeded()
                self.view.endEditing(true)
            }
        }
    }
    var isDateViewHidden: Bool {
        get {
            return viewDatePicker.isHidden
        }
        set(newValue) {
            if newValue {
                viewShadow.isHidden = true
                viewDatePicker.isHidden = true
            } else {
                viewShadow.isHidden = false
                viewDatePicker.isHidden = false
            }
        }
    }
    enum ParentType: String {
        case main = "Main", mobilePostpaid = "Mobile Postpaid", giftCard = "Gift Card", electricity = "Electricity"
    }
    enum RootFilter {
        case billType, amount, serviceProviderType, period
    }
    struct FilterType {
        var rootFilter: RootFilter
        var filterTitle: String
        var parentType: ParentType
        var isSubShowing: Bool
        var hasChild: Bool
        init(rootFilter: RootFilter, filterTitle: String, parentType: ParentType = .main, isSubShowing: Bool = false, hasChild: Bool = false) {
            self.rootFilter = rootFilter
            self.filterTitle = filterTitle
            self.parentType = parentType
            self.isSubShowing = isSubShowing
            self.hasChild = hasChild
        }
    }
    private let refreshControl = UIRefreshControl()
    var modelReport: ReportModel!
    let dateFormatter = DateFormatter()
    // MARK: - View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewBillPayments.tag = 0
        tableViewFilter.tag = 1
        tableViewBillPayments.tableFooterView = UIView()
        tableViewFilter.tableFooterView = UIView()
        tableViewBillPayments.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshTableData), for: .valueChanged)
        buttonCancel.layer.cornerRadius = 15
        buttonClear.layer.cornerRadius = 15
        buttonCancel.addTarget(self, action: #selector(hideSearchView), for: .touchUpInside)
        buttonClear.addTarget(self, action: #selector(clearSerchTextField), for: .touchUpInside)
        textFieldSearch.placeholder = "Search By Transaction ID or Amount or Remarks".localized
        buttonDateSubmit.layer.cornerRadius = 15
        buttonDateCancel.layer.cornerRadius = 15
        buttonDateSubmit.addTarget(self, action: #selector(filterSelectedDate), for: .touchUpInside)
        buttonDateCancel.addTarget(self, action: #selector(hideDateSelectionView), for: .touchUpInside)
        datePicker.calendar = Calendar(identifier: .gregorian)
        if #available(iOS 13.4, *) {
                       datePicker.preferredDatePickerStyle = .wheels
                   } else {
                       // Fallback on earlier versions
                   }
        let cellFilterNibName = UINib(nibName: ConstantsXibCell.filterTableViewCell, bundle: nil)
        tableViewFilter.register(cellFilterNibName, forCellReuseIdentifier: ConstantsXibCell.filterTableViewCell)
        fetchRecordsFromDB()
        buttonCancel.setTitle("Cancel".localized, for: .normal)
        buttonClear.setTitle("Clear".localized, for: .normal)
        labelDateOne.text = "From Date".localized
        labelDateTwo.text = "To Date".localized
        buttonDateSubmit.setTitle("Submit".localized, for: .normal)
        buttonDateCancel.setTitle("Cancel".localized, for: .normal)
        resetFilterButtons()
        setSortLocalizedStrings()
        AppDelegate.AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)
        setUpNavigation()
        modelReport = ReportModel()
        modelReport.reportModelDelegate = self
        modelReport.getTransactionInfo()
    }
    
    override func viewDidLayoutSubviews() {
        let navBarHeight = navigationController?.navigationBar.frame.size.height ?? 44
        let heightToReduce = navBarHeight == 32 ? 56 : 44
        constraintScrollContentHeight.constant = scrollView.frame.size.height - CGFloat(heightToReduce) + 44
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    // MARK: - Target Methods
    @objc func showSearchView() {
        tableViewFilter.isHidden = true
        if !isSearchViewShow {
            removeGestureFromShadowView()
            viewShadow.isHidden = true
            /// For qr
            imageViewQR.isHidden = true
            imageViewQR.image = nil
            /// For share all transaction
            viewToShareAllRecords.isHidden = true
            if billRecords.count > 0 {
                isSearchViewShow = true
                textFieldSearch.becomeFirstResponder()
            } else {
                noRecordAlertActions()
            }
        } else {
            self.hideSearchView()
        }
    }
    
    @objc func showShareOption() {
        if viewToShareAllRecords.isHidden {
            if billRecords.count > 0 {
                let tapGestToRemoveView = UITapGestureRecognizer(target: self, action: #selector(shadowIsTapped))
                viewShadow.addGestureRecognizer(tapGestToRemoveView)
                viewShadow.isHidden = false
                viewToShareAllRecords.isHidden = false
            } else {
                viewShadow.isHidden = true
                noRecordAlertActions()
            }
        }
    }
    
    @objc func showGraph() {
        if billRecords.count == 0 {
            noRecordAlertActions()
            return
        }
        guard let statisticsTransactionController = UIStoryboard(name: "Reports", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.statisticsTransactionController.nameAndID) as? StatisticsTransactionAndBillController else { return }
        statisticsTransactionController.screenType = .billPayments
        statisticsTransactionController.initialRecords = initialRecords
        self.navigationController?.pushViewController(statisticsTransactionController, animated: true)
    }
    
    @objc func hideSearchView() {
        self.view.endEditing(true)
        isSearchViewShow = false
        applyFilter()
    }
    
    @objc func clearSerchTextField() {
        textFieldSearch.text = ""
        applyFilter()
    }
    
    @objc func filterSelectedDate() {
        if let _ = buttonDateOne.title(for: .normal) {
            buttonPeriod.setTitle("Date".localized, for: .normal)
        } else {
            buttonPeriod.setTitle("Month".localized, for: .normal)
        }
        applyFilter()
        isDateViewHidden = true
    }
    
    @objc func hideDateSelectionView() {
        isDateViewHidden = true
    }
    
    @objc func fromDatePickerChanged(datePicker:UIDatePicker) {
        buttonDateOne.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
    }
    
    @objc func toDatePickerChanged(datePicker:UIDatePicker) {
        if let startDateStr = buttonDateOne.title(for: .normal), let startDate = startDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
            switch datePicker.date.compare(startDate) {
            case .orderedAscending:
                if let endDateStr = buttonDateTwo.title(for: .normal), let endDate = endDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                    datePicker.date = endDate
                }
            case .orderedDescending, .orderedSame:
                buttonDateTwo.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            }
        } else {
            buttonDateTwo.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
        }
    }
    
    @objc func monthPickerChanged(datePicker:UIDatePicker) {
        buttonDateTwo.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "MMM yyyy"), for: .normal)
    }
    
    @objc func shadowIsTapped(sender: UITapGestureRecognizer) {
        removeGestureFromShadowView()
        viewShadow.isHidden = true
        /// For qr
        imageViewQR.isHidden = true
        imageViewQR.image = nil
        /// For share all transaction
        viewToShareAllRecords.isHidden = true
    }
    
    @objc private func refreshTableData(_ sender: Any) {
//        if viewSearch.isHidden {
            self.hideSearchView()
            modelReport.getTransactionInfo()
//        } else {
//            self.refreshControl.endRefreshing()
//        }
    }
    
    // MARK: - Filter Actions
    func fetchRecordsFromDB() {
        initialRecords = ReportCoreDataManager.sharedInstance.fetchTransactionRecords()
        if initialRecords.count > 0 {
            currentBalance = initialRecords[initialRecords.count - 1].walletBalance ?? 0
        }
        initialRecords = initialRecords.filter {
            guard let desc = $0.desc else { return false }
            return (desc.contains("#OKBILLPAYMENT") || desc.contains("Post-paid") || desc.contains("Post paid") || desc.contains("landline") || desc.contains("Landline")) //("#OK-00-")//
        }
        billRecords = initialRecords
        DispatchQueue.main.async {
            self.tableViewBillPayments.reloadData()
        }
    }
    
    func noRecordAlertActions() {
        
        alertViewObj.wrapAlert(title: "Alert".localized, body: "No Records".localized, img: UIImage(named : "AppIcon"))
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
        })
        alertViewObj.showAlert(controller: self)
    }
    
    func resetFilterButtons() {
        buttonBillType.setTitle(billTypes[0].filterTitle.localized, for: .normal)
        buttonAmountRange.setTitle(amountRanges[0].filterTitle.localized, for: .normal)
        buttonServiceProviderType.setTitle(expandedServiceProviderTypes[0].filterTitle.localized, for: .normal)
        buttonPeriod.setTitle(periodTypes[0].filterTitle.localized, for: .normal)
    }
    
    func setSortLocalizedStrings() {
        buttonTransactionID.setTitle("Transaction ID".localized, for: .normal)
        buttonAmount.setTitle("Amount".localized, for: .normal)
        buttonMobileNumber.setTitle("Bill Type".localized, for: .normal)
        buttonBonus.setTitle("Bonus Points".localized, for: .normal)
        buttonCashBack.setTitle("Cash Back".localized, for: .normal)
        buttonBalance.setTitle("Balance".localized, for: .normal)
        buttonDateTime.setTitle("Date & Time".localized, for: .normal)
        buttonBillMonth.setTitle("Bill Month".localized, for: .normal)
        buttonBillDuedate.setTitle("Bill Due Date".localized, for: .normal)
    }
    
    func applyFilter(searchText: String = "") {
        var searchTxt = searchText
        var filteredArray = initialRecords
        if isSearchViewShow && searchTxt.count == 0 {
            searchTxt = textFieldSearch.text ?? ""
            filteredArray = filteredArray.sorted {
                guard let date1 = $0.transactionDate, let date2 = $1.transactionDate else { return false }
                return date1.compare(date2) == .orderedDescending
            }
        }
        filteredArray = filterProcess(forBillType: filteredArray)
        filteredArray = filterProcess(forAmountType: filteredArray)
        filteredArray = filterProcess(forServiceProviderType: filteredArray)
        filteredArray = filterProcess(forPeriod: filteredArray)
        if isSearchViewShow && searchTxt.count > 0 {
            filteredArray = filteredArray.filter {
                if let desc = $0.desc {
                    if desc.lowercased().range(of:searchTxt.lowercased()) != nil {
                        return true
                    }
                }
                if let transID = $0.transID {
                    if transID.lowercased().range(of: searchTxt.lowercased()) != nil {
                        return true
                    }
                }
                if let mobileNumber = $0.destination {
                    if mobileNumber.range(of: searchTxt.lowercased()) != nil {
                        return true
                    }
                }
                if let amount = $0.amount {
                    if amount.toString().range(of: searchTxt.lowercased()) != nil {
                        return true
                    }
                }
                return false
            }
        }
        billRecords = filteredArray
        tableViewBillPayments.reloadData()
    }
    
    func filterProcess(forServiceProviderType arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        func filterBy(string: String) -> [ReportTransactionRecord] {
            return arrayToFilter.filter {
                guard let descr = $0.desc else { return false }
                return descr.lowercased().contains(string.lowercased())
            }
        }
        if let transFilter = buttonServiceProviderType.currentTitle {
            switch transFilter {
            case "Type".localized, "All".localized:
                return arrayToFilter
            case "Mpt".localized:
                return filterBy(string: "Mpt")
            case "Telenor".localized:
                return filterBy(string: "Telenor")
            case "Ooredoo".localized:
                return filterBy(string: "Ooredoo")
            case "Mectel".localized:
                return filterBy(string: "Mectel")
            case "Mytel".localized:
                return filterBy(string: "Mytel")
            case "iTunes Gift Card".localized:
                return filterBy(string: "iTunesGift")
            case "Google Play Gift Card".localized:
                return filterBy(string: "GooglePlay")
            case "Skype Wallet Gift Card".localized:
                return filterBy(string: "SkypeWallet")
            case "Steam Wallet Gift Card".localized:
                return filterBy(string: "SteamGift")
            case "Facebook Gift Card".localized:
                return filterBy(string: "FacebookGift")
            case "Xbox Gift Card".localized:
                return filterBy(string: "XboxGift")
            case "Windows Store Gift Card".localized:
                return filterBy(string: "WindowsGift")
            case "Viber Out Credits".localized:
                return filterBy(string: "Viber")
            case "Other Meter".localized:
                return filterBy(string: "ELECTRICITY")
            case "My Meter".localized:
                return filterBy(string: "ELECTRICITY")
            case "Electricity".localized:
                return filterBy(string: "ELECTRICITY")
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcess(forBillType arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        func filterBy(string: String) -> [ReportTransactionRecord] {
            return arrayToFilter.filter {
                guard let descr = $0.desc else { return false }
                return descr.contains(string)
            }
        }
        if let transFilter = buttonBillType.currentTitle {
            switch transFilter {
            case "Transaction".localized, "All".localized:
                return arrayToFilter
            case "Landlines".localized:
                return filterBy(string: "Landline")
            case "Mobile Postpaid".localized:
                return filterBy(string: "Post paid")
            case "Electricity".localized:
                return filterBy(string: "ELECTRICITY")
            case "YCDC".localized:
                return filterBy(string: "YCDC")
            case "Gift Card".localized:
                return filterBy(string: "GiftCard")
            case "DTH".localized:
                return filterBy(string: "DTH")
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcess(forAmountType arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        func filterFromDecimalNumbers(greaterThan: NSDecimalNumber, lesserThan: NSDecimalNumber)-> [ReportTransactionRecord] {
            return arrayToFilter.filter
                {
                    guard let amount = $0.amount else { return false }
                    return (amount.compare(greaterThan) == .orderedDescending && amount.compare(lesserThan) == .orderedAscending)
            }
        }
        if let transFilter = buttonAmountRange.currentTitle {
            switch transFilter {
            case "Amount".localized, "All".localized:
                return arrayToFilter
            case "0 - 1,000".localized:
                return filterFromDecimalNumbers(greaterThan: -0.01, lesserThan: 1001)
            case "1,001 - 10,000".localized:
                return filterFromDecimalNumbers(greaterThan: 1000.99, lesserThan: 10001)
            case "10,001 - 50,000".localized:
                return filterFromDecimalNumbers(greaterThan: 10000.99, lesserThan: 50001)
            case "50,001 - 1,00,000".localized:
                return filterFromDecimalNumbers(greaterThan: 50000.99, lesserThan: 100001)
            case "1,00,001 - 2,00,000".localized:
                return filterFromDecimalNumbers(greaterThan: 100000.99, lesserThan: 200001)
            case "2,00,001 - 5,00,000".localized:
                return filterFromDecimalNumbers(greaterThan: 200000.99, lesserThan: 500001)
            case "5,00,001 - Above".localized:
                return arrayToFilter.filter {
                    guard let amount = $0.amount else { return false }
                    return (amount.compare(500000.99) == .orderedDescending)
                }
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcess(forPeriod arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        if let periodFilter = buttonPeriod.currentTitle {
            switch periodFilter {
            case "Period".localized, "All".localized:
                return arrayToFilter
            case "Date".localized:
                return arrayToFilter.filter {
                    guard let transDate = $0.transactionDate else { return false }
                    let dFormatter = DateFormatter()
                    dFormatter.calendar = Calendar(identifier: .gregorian)
                    dFormatter.dateFormat = "dd MMM yyyy"
                    let transDateStr = dFormatter.string(from: transDate)
                    guard let transactionDate = dFormatter.date(from: transDateStr) else { return false }
                    guard let dateStartStr = buttonDateOne.currentTitle else { return false }
                    guard let dateEndStr = buttonDateTwo.currentTitle else { return false }
                    guard let exactDateStart = dFormatter.date(from: dateStartStr) else { return false }
                    guard let dateStart = Calendar.current.date(byAdding: Calendar.Component.day, value: -1, to: exactDateStart) else { return false }
                    guard let exactDateEnd = dFormatter.date(from: dateEndStr) else { return false }
                    guard let dateEnd = Calendar.current.date(byAdding: Calendar.Component.day, value: 1, to: exactDateEnd) else { return false}
                    return (dateStart.compare(transactionDate) == .orderedAscending && transactionDate.compare(dateEnd) == .orderedAscending)
                }
            case "Month".localized:
                return arrayToFilter.filter {
                    guard let transDate = $0.transactionDate else { return false }
                    let dFormatter = DateFormatter()
                    dFormatter.calendar = Calendar(identifier: .gregorian)
                    dFormatter.dateFormat = "MMM yyyy"
                    let transDateStr = dFormatter.string(from: transDate)
                    guard let transactionDate = dFormatter.date(from: transDateStr) else { return false }
                    guard let dateStr = buttonDateTwo.currentTitle else { return false }
                    guard let monthToFilter = dFormatter.date(from: dateStr) else { return false }
                    return transactionDate.compare(monthToFilter) == .orderedSame
                }
            default:
                return []
            }
        }
        return []
    }
    
    func showDateOptions(selectedOption: String) {
        switch selectedOption {
        case "Period".localized, "All".localized:
            buttonPeriod.setTitle(selectedOption, for: .normal)
            applyFilter()
            isDateViewHidden = true
        case "Date".localized:
            buttonDateOne.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            buttonDateTwo.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            labelDateOne.isHidden = false
            buttonDateOne.isHidden = false
            labelDateTwo.text = "To Date".localized
            configureDatePicker(tag: 0)
            isDateViewHidden = false
        case "Month".localized:
            buttonDateOne.setTitle(nil, for: .normal)
            buttonDateTwo.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "MMM yyyy"), for: .normal)
            labelDateOne.isHidden = true
            buttonDateOne.isHidden = true
            labelDateTwo.text = "Month".localized
            configureDatePicker(tag: 2)
            isDateViewHidden = false
        default:
            break
        }
    }
    
    func configureDatePicker(tag: Int) {
        switch tag {
        case 0:
            buttonDateOne.setTitleColor(ConstantsColor.navigationHeaderBillPayment, for: .normal)
            buttonDateTwo.setTitleColor(UIColor(red: 30/255.0, green: 30/255, blue: 30/255, alpha: 1.0), for: .normal)
            datePicker.datePickerMode = .date
            datePicker.removeTarget(nil, action: nil, for: .allEvents)
            datePicker.addTarget(self, action: #selector(fromDatePickerChanged), for: .valueChanged)
            datePicker.minimumDate = nil
            if let endDateStr = buttonDateTwo.title(for: .normal), let endDate = endDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                datePicker.maximumDate = endDate
            } else {
                datePicker.maximumDate = Date()
            }
            if let currentDateStr = buttonDateOne.title(for: .normal), let currentDate = currentDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                datePicker.date = currentDate
            } else {
                buttonDateOne.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            }
        case 1:
            buttonDateOne.setTitleColor(UIColor(red: 30/255.0, green: 30/255, blue: 30/255, alpha: 1.0), for: .normal)
            buttonDateTwo.setTitleColor(ConstantsColor.navigationHeaderBillPayment, for: .normal)
            datePicker.datePickerMode = .date
            datePicker.maximumDate = Date()
            datePicker.removeTarget(nil, action: nil, for: .allEvents)
            datePicker.addTarget(self, action: #selector(toDatePickerChanged), for: .valueChanged)
            //datePicker.minimumDate = nil
            if let endDateStr = buttonDateOne.title(for: .normal), let endDate = dateFormatter.date(from: endDateStr) {
                datePicker.minimumDate = endDate
            } else {
                datePicker.minimumDate = nil
            }

            if let currentDateStr = buttonDateTwo.title(for: .normal), let currentDate = currentDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                datePicker.date = currentDate
            } else {
                buttonDateTwo.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            }
        case 2:
            buttonDateTwo.setTitleColor(ConstantsColor.navigationHeaderBillPayment, for: .normal)
            datePicker.datePickerMode = .date
            datePicker.maximumDate = Date()
            datePicker.date = Date()
            datePicker.removeTarget(nil, action: nil, for: .allEvents)
            datePicker.addTarget(self, action: #selector(monthPickerChanged), for: .valueChanged)
            datePicker.minimumDate = nil
            if let currentDateStr = buttonDateTwo.title(for: .normal), let currentDate = currentDateStr.dateValue(dateFormatIs: "MMM yyyy") {
                datePicker.date = currentDate
            } else {
                buttonDateTwo.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "MMM yyyy"), for: .normal)
            }
        default:
            break
        }
    }
    
    func removeGestureFromShadowView() {
        if let recognizers = viewShadow.gestureRecognizers {
            for recognizer in recognizers {
                if let recognizer = recognizer as? UITapGestureRecognizer {
                    viewShadow.removeGestureRecognizer(recognizer)
                }
            }
        }
    }
    
    func resetSortButtons(sender: UIButton) {
        if sender != buttonTransactionID {
            buttonTransactionID.tag = 0
            buttonTransactionID.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonAmount {
            buttonAmount.tag = 0
            buttonAmount.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonMobileNumber {
            buttonMobileNumber.tag = 0
            buttonMobileNumber.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonBonus {
            buttonBonus.tag = 0
            buttonBonus.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonCashBack {
            buttonCashBack.tag = 0
            buttonCashBack.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonBalance {
            buttonBalance.tag = 0
            buttonBalance.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonDateTime {
            buttonDateTime.tag = 0
            buttonDateTime.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
    }
    
    func configureFilterCell(cell: FilterTableViewCell, filterType: FilterType) {
        cell.labelFilterName.text = filterType.filterTitle.localized
        if filterType.hasChild {
            if filterType.isSubShowing {
                cell.imgViewDropDown.image = UIImage(named: "reportArrowUpBlue")
            } else {
                cell.imgViewDropDown.image = UIImage(named: "reportArrowDownBlue")
            }
        } else {
            cell.imgViewDropDown.image = nil
        }
    }
    
    func expandPostpaidSelection(selectedFilter: FilterType) {
        if selectedFilter.isSubShowing {
            filterArray = expandedServiceProviderTypes.filter { $0.parentType == .main }
            var selectedFilterType = selectedFilter
            selectedFilterType.isSubShowing = false
            filterArray[2] = selectedFilterType
        } else {
            var parentTypes = expandedServiceProviderTypes.filter { $0.parentType == .main }
            let childTypes = expandedServiceProviderTypes.filter { $0.parentType == .mobilePostpaid }
            let index = 3
            if 0 ... parentTypes.count ~= index {
                parentTypes[index..<index] = childTypes[0..<childTypes.count]
                filterArray = parentTypes
            }
            var selectedFilterType = selectedFilter
            selectedFilterType.isSubShowing = true
            filterArray[2] = selectedFilterType
        }
        tableViewFilter.reloadData()
    }
    
    func expandGiftCardSelection(selectedFilter: FilterType) {
        if selectedFilter.isSubShowing {
            filterArray = expandedServiceProviderTypes.filter { $0.parentType == .main }
            var selectedFilterType = selectedFilter
            selectedFilterType.isSubShowing = false
            filterArray[3] = selectedFilterType
        } else {
            var parentTypes = expandedServiceProviderTypes.filter { $0.parentType == .main }
            let childTypes = expandedServiceProviderTypes.filter { $0.parentType == .giftCard }
            let index = 4
            if 0 ... parentTypes.count ~= index {
                parentTypes[index..<index] = childTypes[0..<childTypes.count]
                filterArray = parentTypes
            }
            var selectedFilterType = selectedFilter
            selectedFilterType.isSubShowing = true
            filterArray[3] = selectedFilterType
        }
        tableViewFilter.reloadData()
    }
    
    func expandElectricitySelection(selectedFilter: FilterType) {
        if selectedFilter.isSubShowing {
            filterArray = expandedServiceProviderTypes.filter { $0.parentType == .main }
            var selectedFilterType = selectedFilter
            selectedFilterType.isSubShowing = false
            filterArray[4] = selectedFilterType
        } else {
            var parentTypes = expandedServiceProviderTypes.filter { $0.parentType == .main }
            let childTypes = expandedServiceProviderTypes.filter { $0.parentType == .electricity }
            let index = 5
            if 0 ... parentTypes.count ~= index {
                parentTypes[index..<index] = childTypes[0..<childTypes.count]
                filterArray = parentTypes
            }
            var selectedFilterType = selectedFilter
            selectedFilterType.isSubShowing = true
            filterArray[4] = selectedFilterType
        }
        tableViewFilter.reloadData()
    }
    
    func getQRCodeImage(qrData: ReportTransactionRecord) -> UIImage? {
        let phNumber = qrData.destination ?? "0000000000"
        var transDateStr = ""
        
        let dFormatter = DateFormatter()
        dFormatter.calendar = Calendar(identifier: .gregorian)
        dFormatter.dateFormat = "dd/MMM/yyyy HH:mm:ss"
        if let trDate = qrData.transactionDate {
            transDateStr = dFormatter.string(from: trDate)
        }
        var senderName = ""
        var senderNumber = ""
        var receiverName = ""
        var receiverBusinessName = ""
        var receiverNumber = ""
        if let sName = qrData.senderName {
            senderName = sName
            if senderName.count == 0 {
                senderName = UserModel.shared.name
            }
        }
        if let rName = qrData.receiverName{
            receiverName = rName
        }
        if let businessName = qrData.receiverBName, businessName.count > 0 {
            receiverBusinessName = businessName
        }
        if let accTransType = qrData.accTransType {
            if accTransType == "Cr" {
                if let dest = qrData.destination {
                    senderNumber = dest
                }
                receiverNumber = UserModel.shared.mobileNo
                if let transType = qrData.transType {
                    if transType == "PAYWITHOUT ID" {
                        senderNumber = "XXXXXXXXXX"
                    }
                }
            } else {
                if let dest = qrData.destination {
                    receiverNumber = dest
                }
                senderNumber = UserModel.shared.mobileNo
            }
        }
        let firstPartToEncrypt = "\(transDateStr)----\(phNumber)"
        let transID = qrData.transID ?? ""
        //let operatorName = qrData.operatorName ?? ""
        let trasnstype = qrData.transType ?? ""
        var gender = ""
        if let safeGender = qrData.gender {
            gender = safeGender == "1" ? "M": "F"
        }
        let age = qrData.age > 0 ? "\(qrData.age)" : ""
        //let description = qrData.desc ?? ""
        let lattitude = qrData.latitude ?? ""
        let longitude = qrData.longitude ?? ""
        
        let firstPartEncrypted = AESCrypt.encrypt(firstPartToEncrypt, password: "m2n1shlko@$p##d")
        
        var balance = ""
        if let balanceData = qrData.walletBalance {
            balance = balanceData.toString()
        }
        var cashBackAmount = ""
        if let cashBack = qrData.cashBack {
            cashBackAmount = cashBack.toString()
        }
        var amount = ""
        if let amountData = qrData.amount {
            amount = amountData.toString()
        }
        var bonus = ""
        if let bonusData = qrData.bonus {
            bonus = bonusData.toString()
        }
        let state = UserModel.shared.state
        let str2 = "OK-\(senderName)-\(amount)-\(transDateStr)-\(transID)-\(senderNumber)-\(balance)-des-\(receiverNumber)-\(trasnstype)-\(cashBackAmount)-\(bonus)-\(lattitude),\(longitude)-0-\(gender)-\(age)-\(state)-\(receiverName)-\(receiverBusinessName)"
        
        //OK-agentName-amount-date-transID-senderNumber-balance-“des”-operator-transtype-cashback-bonus-lat-long-“0”-gender-age-city
        
        let secondEncrypted = AESCrypt.encrypt(str2, password: firstPartToEncrypt)
        
        var firstPartEncryptedSafeValue = ""
        var secondEncryptedSafeValue = ""
        if let firstEncrypted = firstPartEncrypted {
            firstPartEncryptedSafeValue = firstEncrypted
        }
        if let secondEncrypt = secondEncrypted {
            secondEncryptedSafeValue = secondEncrypt
        }
        let finalEncyptFormation = "\(firstPartEncryptedSafeValue)---\(secondEncryptedSafeValue)"
        
        let qrImageObject = PTQRGenerator()
        guard let qrImage =  qrImageObject.getQRImage(stringQR: finalEncyptFormation, withSize: 10) else { return nil }
        
        return qrImage
    }
    
    func updateFilterTableHeight() {
        constraintFilterTableHeight.constant = filterArray.count > 4 ? CGFloat(5*44) : CGFloat(filterArray.count * 44)
        self.view.layoutIfNeeded()
    }
    
    // MARK: - Button Actions
    // It filters and show the detail by transaction type
    @IBAction func filterServiceProviderType(_ sender: UIButton) {
        self.view.endEditing(true)
        func showListAtServiceProvider() {
            filterArray = expandedServiceProviderTypes.filter { $0.parentType == .main }
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 545
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
        }
        if tableViewFilter.isHidden {
            showListAtServiceProvider()
        } else {
            switch lastSelectedFilter {
            case .serviceProviderType :
                tableViewFilter.isHidden = true
            default:
                showListAtServiceProvider()
            }
        }
        lastSelectedFilter = .serviceProviderType
    }
    
    // It filters and show the detail by transaction type
    @IBAction func filterByBillType(_ sender: UIButton) {
        self.view.endEditing(true)
        func showListAtBillType() {
            filterArray = billTypes
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 40
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
        }
        if tableViewFilter.isHidden {
            showListAtBillType()
        } else {
            switch lastSelectedFilter {
            case .billType :
                tableViewFilter.isHidden = true
            default:
                showListAtBillType()
            }
        }
        lastSelectedFilter = .billType
    }
    
    // It filters and show the detail by amount range
    @IBAction func filterByAmount(_ sender: UIButton) {
        self.view.endEditing(true)
        func showListAtAmount() {
            filterArray = amountRanges
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 295
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
        }
        if tableViewFilter.isHidden {
            showListAtAmount()
        } else {
            switch lastSelectedFilter {
            case .amount :
                tableViewFilter.isHidden = true
            default:
                showListAtAmount()
            }
        }
        lastSelectedFilter = .amount
    }
    
    // It filters and show the detail by period type
    @IBAction func filterByPeriod(_ sender: UIButton) {
        self.view.endEditing(true)
        func showListAtPeriod() {
            filterArray = periodTypes
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 545
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
        }
        if tableViewFilter.isHidden {
            showListAtPeriod()
        } else {
            switch lastSelectedFilter {
            case .period :
                tableViewFilter.isHidden = true
            default:
                showListAtPeriod()
            }
        }
        lastSelectedFilter = .period
    }
    
    @IBAction func sortByTransactionID(_ sender: UIButton) {
        func sortTransactionID(orderType: ComparisonResult) -> [ReportTransactionRecord]{
            return billRecords.sorted {
                guard let transID1 = $0.transID, let transID2 = $1.transID else { return false }
                return transID1.localizedStandardCompare(transID2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            billRecords = sortTransactionID(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            billRecords = sortTransactionID(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewBillPayments.reloadData()
    }
    
    @IBAction func sortByAmount(_ sender: UIButton) {
        func sortAmount(orderType: ComparisonResult) -> [ReportTransactionRecord]{
            return billRecords.sorted {
                guard let amount1 = $0.amount, let amount2 = $1.amount else { return false }
                return amount1.compare(amount2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            billRecords = sortAmount(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            billRecords = sortAmount(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewBillPayments.reloadData()
    }
    
    @IBAction func sortByMobileNumber(_ sender: UIButton) {
        func sortMobileNumber(orderType: ComparisonResult) -> [ReportTransactionRecord]{
            return billRecords.sorted {
                guard let phNumber1 = $0.destination, let phNumber2 = $1.destination else { return false }
                return phNumber1.compare(phNumber2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            billRecords = sortMobileNumber(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            billRecords = sortMobileNumber(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewBillPayments.reloadData()
    }
    
    @IBAction func sortByBonus(_ sender: UIButton) {
        func sortBonus(orderType: ComparisonResult) -> [ReportTransactionRecord]{
            return billRecords.sorted {
                guard let bonus1 = $0.bonus, let bonus2 = $1.bonus else { return false }
                return bonus1.compare(bonus2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            billRecords = sortBonus(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            billRecords = sortBonus(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewBillPayments.reloadData()
    }
    
    @IBAction func sortByCashBack(_ sender: UIButton) {
        func sortCashBack(orderType: ComparisonResult) -> [ReportTransactionRecord] {
            return billRecords.sorted {
                guard let cashBack1 = $0.cashBack, let cashBack2 = $1.cashBack else { return false }
                return cashBack1.compare(cashBack2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            billRecords = sortCashBack(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            billRecords = sortCashBack(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewBillPayments.reloadData()
    }
    
    @IBAction func sortByBalance(_ sender: UIButton) {
        func sortBalance(orderType: ComparisonResult) -> [ReportTransactionRecord]{
            return billRecords.sorted {
                guard let balance1 = $0.walletBalance, let balance2 = $1.walletBalance else { return false }
                return balance1.compare(balance2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            billRecords = sortBalance(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            billRecords = sortBalance(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewBillPayments.reloadData()
    }
    
    @IBAction func sortByDateTime(_ sender: UIButton) {
        func sortDates(orderType: ComparisonResult) -> [ReportTransactionRecord]{
            return billRecords.sorted {
                guard let date1 = $0.transactionDate, let date2 = $1.transactionDate else { return false }
                return date1.compare(date2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            billRecords = sortDates(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            billRecords = sortDates(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewBillPayments.reloadData()
    }
    
    @IBAction func configureDatePickerOne(_ sender: UIButton) {
        configureDatePicker(tag: 0)
    }
    
    @IBAction func configureDatePickerTwo(_ sender: UIButton) {
        if buttonDateOne.isHidden {
            configureDatePicker(tag: 2)
        } else {
            configureDatePicker(tag: 1)
        } 
    }
    
    @IBAction func shareRecordInPdf(_ sender: UIButton) {
        guard let billCell = sender.superview?.superview?.superview as? ReportBillPaymentCell else { return }
        guard let indexPath = tableViewBillPayments.indexPath(for: billCell) else { return }
        var pdfDictionary = [String: Any]()
        pdfDictionary["logoName"] = "appIcon_Ok"
        pdfDictionary["invoiceTitle"] = "Bill Payments Statement"
        pdfDictionary["invoiceTitleColor"] = ConstantsColor.navigationHeaderBillPayment
        pdfDictionary["senderAccName"] = UserModel.shared.name
        pdfDictionary["receiverAccName"] = billRecords[indexPath.row].receiverName
        pdfDictionary["transactionType"] = billRecords[indexPath.row].transType
        if let accTransType = billRecords[indexPath.row].accTransType {
            if accTransType == "Cr" {
                var sNumber = ""
                var rNumber = UserModel.shared.mobileNo
                if let des = billRecords[indexPath.row].destination {
                    sNumber = des
                    if des.hasPrefix("0095") {
                        sNumber = "(+95)0\(des.substring(from: 4))"
                    }
                }
                if rNumber.hasPrefix("0095") {
                    rNumber = "(+95)0\(rNumber.substring(from: 4))"
                }
                pdfDictionary["senderAccNo"] = sNumber
                pdfDictionary["receiverAccNo"] = "XXXXXXXXXX"
                if let transType = billRecords[indexPath.row].transType {
                    if transType == "PAYWITHOUT ID" {
                        pdfDictionary["senderAccNo"] = "XXXXXXXXXX"
                        pdfDictionary["transactionType"] = "XXXXXX"
                    }
                }
                if let businessName = billRecords[indexPath.row].senderBName, businessName.count > 0 {
                    pdfDictionary["businessName"] = businessName
                }
            } else {
                if let transType = billRecords[indexPath.row].transType, transType == "PAYWITHOUT ID" {
                    pdfDictionary["transactionType"] = "Hide My Number"
                }
                var sNumber = UserModel.shared.mobileNo
//                var rNumber = ""
//                if let des = billRecords[indexPath.row].destination {
//                    rNumber = des
//                    if des.hasPrefix("0095") {
//                        rNumber = "(+95)0\(des.substring(from: 4))"
//                    }
//                }
                if sNumber.hasPrefix("0095") {
                    sNumber = "(+95)0\(sNumber.substring(from: 4))"
                }
                pdfDictionary["receiverAccNo"] = "XXXXXXXXXX"
                pdfDictionary["senderAccNo"] = sNumber
            }
            if let businessName = billRecords[indexPath.row].receiverBName, businessName.count > 0 {
                pdfDictionary["businessName"] = businessName
            }
        }
        pdfDictionary["transactionID"] = billRecords[indexPath.row].transID
        if let transDate = billRecords[indexPath.row].transactionDate {
            pdfDictionary["transactionDate"] = transDate.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss")
        }
        let desc = billRecords[indexPath.row].rawDesc?.removingPercentEncoding?.replacingOccurrences(of: "+", with: " ").components(separatedBy: "-")
        if billRecords[indexPath.row].rawDesc?.contains(find: "ELECTRICITY") ?? false {
            let dateFormat = self.formatDate(dateString: desc?.last ?? "")
            var name: [String]?
            if desc?[9].contains(find: "Name :") ?? false {
                name = desc?[9].components(separatedBy: "Name :")
            } else if desc?[10].contains(find: "Name :") ?? false {
                name = desc?[10].components(separatedBy: "Name :")
            }
            var diviLoc = ""
            if desc?[7].contains(find: "Division") ?? false {
                let division = desc?[7].components(separatedBy: "Division :")
                if let divLocal = division?[1] {
                    let arr = divLocal.components(separatedBy: "Division")
                    diviLoc = arr[0]
                }
            } else if desc?[8].contains(find: "Division") ?? false {
                let division = desc?[8].components(separatedBy: "Division :")
                if let divLocal = division?[1] {
                    let arr = divLocal.components(separatedBy: "Division")
                    diviLoc = arr[0]
                }
            }
            
            var township: [String]?
            if desc?[8].contains(find: "Township") ?? false {
                township = desc?[8].components(separatedBy: "Township")
            } else if desc?[9].contains(find: "Township") ?? false {
                township = desc?[9].components(separatedBy: "Township")
            }
            var meterNo = ""
            var payFor:[String]?
            if desc?[6].contains(find: "PayFor:") ?? false {
                meterNo = desc?[2] ?? ""
                payFor = desc?[6].components(separatedBy: "PayFor:")
            } else if desc?[7].contains(find: "PayFor:") ?? false {
                meterNo = "\(desc?[2] ?? "")-\(desc?[3] ?? "")"
                payFor = desc?[7].components(separatedBy: "PayFor:")
            }
            let fullDesc = "\(meterNo) \(name?[1] ?? "") \(township?[1] ?? "") Township \(diviLoc) \(diviLoc) Division Bill Month: \(dateFormat.0) Due Date \(dateFormat.1) \(payFor?[1] ?? "")"
            pdfDictionary["remarks"] = fullDesc.replacingOccurrences(of: ",", with: "")
           
                let displayNumber = UitilityClass.showMobileNumberFromDesc(desc: billRecords[indexPath.row].desc)
                if displayNumber.sourceNumber == false {
                    pdfDictionary["senderAccNo"] = "XXXXXXXXXX"
                }
                if displayNumber.destinationNumber == false {
                    pdfDictionary["receiverAccNo"] = "XXXXXXXXXX"
                }
        } else if billRecords[indexPath.row].rawDesc?.contains(find: "DTH") ?? false {
            pdfDictionary["remarks"] = billRecords[indexPath.row].rawDesc?.components(separatedBy: "Destination").first ?? ""
        } else {
            if let descriptionStr = billRecords[indexPath.row].desc, descriptionStr.count > 0 {
                pdfDictionary["remarks"] = descriptionStr
            }
        }
        if let transAmount = billRecords[indexPath.row].amount {
            pdfDictionary["amount"] = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: transAmount.toString()) + " MMK"
        }
        if let transType = billRecords[indexPath.row].accTransType, transType == "Dr" {
            pdfDictionary["qrImage"] = getQRCodeImage(qrData: billRecords[indexPath.row])
        }
        guard let pdfUrl = CodeSnippets.createInvoicePDF(dictValues: pdfDictionary, pdfFile: "OK$ Bill Payments Report") else {
            println_debug("Error - pdf not generated")
            return
        }
        let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
    
    @IBAction func shareRecordByQRCode(_ sender: UIButton) {
        self.view.endEditing(true)
        guard let billCell = sender.superview?.superview?.superview as? ReportBillPaymentCell else { return }
        guard let indexPath = tableViewBillPayments.indexPath(for: billCell) else { return }
        if let qrImage = getQRCodeImage(qrData: billRecords[indexPath.row]) {
            let tapGestToRemoveView = UITapGestureRecognizer(target: self, action: #selector(shadowIsTapped))
            viewShadow.addGestureRecognizer(tapGestToRemoveView)
            imageViewQR.image = qrImage
            viewShadow.isHidden = false
            imageViewQR.isHidden = false
        }
    }
    
    @IBAction func shareAllTransactionsInPdf(_ sender: UIButton) {
        viewShadow.isHidden = true
        viewToShareAllRecords.isHidden = true
        removeGestureFromShadowView()
        if billRecords.count == 0 {
            return
        }
        let pdfView = UIView()
        if let pdfHeaderView = Bundle.main.loadNibNamed("AllTransactionPdfHeader", owner: self, options: nil)?.first as? AllTransactionPdfHeader {
            pdfHeaderView.fillBillDetails(list: billRecords, dateOne: buttonDateOne.currentTitle ?? "", dateTwo: buttonDateTwo.currentTitle ?? "",
                                          currentBalance: CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: String(walletAmount())), amountRange: buttonAmountRange.currentTitle ?? "",
                                          billType: buttonBillType.currentTitle ?? "", serviceProviderType: buttonServiceProviderType.currentTitle ?? "",
                                          periodStr: buttonPeriod.currentTitle ?? "")
            pdfHeaderView.creditLblAmount.text = "Total CashBack Amount"
            let newHeight = pdfHeaderView.viewHeaderForBillPayments.frame.maxY
            pdfHeaderView.frame = CGRect(x: 0, y: 0, width: pdfHeaderView.frame.size.width, height: newHeight)
            pdfView.addSubview(pdfHeaderView)
            pdfView.frame = pdfHeaderView.bounds
            let xPos: CGFloat = pdfHeaderView.viewHeaderForBillPayments.frame.minX
            var yPos: CGFloat = pdfView.frame.maxY
            var index = 0
            for record in billRecords {
                if let pdfBodyView = Bundle.main.loadNibNamed("BillPaymentsPdfBody", owner: self, options: nil)?.first as? BillPaymentsPdfBody {
                    pdfBodyView.fillDetails(record: record)
                    let recordHeight = pdfBodyView.frame.size.height
                    let yPosInPage = Int(yPos) % 842
                    if (yPosInPage + Int(recordHeight)) > 842 {
                        yPos = yPos + CGFloat((842 - yPosInPage) + 30 + 30)
                    }
                    pdfBodyView.frame = CGRect(x: xPos, y: yPos, width: pdfBodyView.frame.size.width, height: recordHeight)
                    yPos = yPos + recordHeight + 2
                    pdfView.addSubview(pdfBodyView)
                }
                if index == 49 {
                    break
                }
                index += 1
            }
            pdfView.frame = CGRect(x: 0, y: 0, width: pdfView.frame.size.width, height: yPos + 30)
            guard let pdfUrl = CodeSnippets.generatePDF(viewForPDF: pdfView, pdfFile: "OK$ Bill_Payments Report") else {
                println_debug("Error - pdf not generated")
                return
            }
            let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
            DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
        }
    }
    
    
    
    @IBAction func shareAllTransactionInExcel(_ sender: UIButton) {
        viewShadow.isHidden = true
        viewToShareAllRecords.isHidden = true
        removeGestureFromShadowView()
        
        if billRecords.count == 0 {
            return
        }
        var excelTxt = "Transaction ID,Account Holder Number,Destination Number,Bill Type,Bill Month, Bill Due Date, Bill Amount (MMK),Opening Balance (MMK), Bonus / Loyalty Points, Cash Back Amount (MMK), Debit Amount (MMK), Closing Balance (MMK), Date & Time, Remarks\n"
        var totalAmount: NSDecimalNumber = 0
        var bonusTransAmount: NSDecimalNumber = 0
        var cashBackAmt: NSDecimalNumber = 0
        var feesAmt: NSDecimalNumber = 0
        for record in billRecords {
            var recText = record.transID ?? ""
            let phNo = "XXXXXXXXXX"
//            if let mobileNo = record.destination {
//                var phNumber = mobileNo.replaceFirstTwoChar(withStr: "+")
//                phNumber.insert(")", at: phNumber.index(phNumber.startIndex, offsetBy: 3))
//                phNumber.insert("(", at: phNumber.startIndex)
//                phNo = phNumber
//            }
            let userNo = CodeSnippets.getMobileNumWithBrackets(countryCode: UserModel.shared.countryCode, number: UserModel.shared.formattedNumber)
            var billtype = ""
            if record.desc?.contains(find: "ELECTRICITY") ?? false {
                billtype = "ELECTRICITY"
            } else if record.desc?.contains(find: "POSTPAID") ?? false {
                billtype = "POSTPAID"
            } else if record.desc?.contains(find: "LANDLINE") ?? false {
                billtype = "LANDLINE"
            } else if record.desc?.contains(find: "SUNKING") ?? false {
                billtype = "SUNKING"
            } else if record.desc?.contains(find: "INSURANCE") ?? false {
                billtype = "INSURANCE"
            } else if record.desc?.contains(find: "MERCHANTPAY") ?? false {
                billtype = "MERCHANTPAY"
            } else if record.desc?.contains(find: "DTH") ?? false {
                billtype = "DTH"
            } else if record.desc?.contains(find: "GiftCard") ?? false {
                billtype = "GIFTCARD"
            } else {
                billtype = ""
            }
            if let tranAmount = record.amount {
                totalAmount = totalAmount.adding(tranAmount)
            }
            if let bonAmount = record.bonus {
                bonusTransAmount = bonusTransAmount.adding(bonAmount)
            }
            if let cashBAmount = record.cashBack {
                cashBackAmt = cashBackAmt.adding(cashBAmount)
            }
            if let fees = record.fee {
                feesAmt = feesAmt.adding(fees)
            }
            let desc = record.rawDesc?.removingPercentEncoding?.replacingOccurrences(of: "+", with: " ").components(separatedBy: "-")
            var description = record.desc
            if record.rawDesc?.contains(find: "ELECTRICITY") ?? false {
                let dateFormat = self.formatDate(dateString: desc?.last ?? "")
                var name: [String]?
                if desc?[9].contains(find: "Name :") ?? false {
                    name = desc?[9].components(separatedBy: "Name :")
                } else if desc?[10].contains(find: "Name :") ?? false {
                    name = desc?[10].components(separatedBy: "Name :")
                }
                var diviLoc = ""
                if desc?[7].contains(find: "Division") ?? false {
                    let division = desc?[7].components(separatedBy: "Division :")
                    if let divLocal = division?[1] {
                        let arr = divLocal.components(separatedBy: "Division")
                        diviLoc = arr[0]
                    }
                } else if desc?[8].contains(find: "Division") ?? false {
                    let division = desc?[8].components(separatedBy: "Division :")
                    if let divLocal = division?[1] {
                        let arr = divLocal.components(separatedBy: "Division")
                        diviLoc = arr[0]
                    }
                }
                
                var township: [String]?
                if desc?[8].contains(find: "Township") ?? false {
                    township = desc?[8].components(separatedBy: "Township")
                } else if desc?[9].contains(find: "Township") ?? false {
                    township = desc?[9].components(separatedBy: "Township")
                }
                var meterNo = ""
                var payFor:[String]?
                if desc?[6].contains(find: "PayFor:") ?? false {
                    meterNo = desc?[2] ?? ""
                    payFor = desc?[6].components(separatedBy: "PayFor:")
                } else if desc?[7].contains(find: "PayFor:") ?? false {
                    meterNo = "\(desc?[2] ?? "")-\(desc?[3] ?? "")"
                    payFor = desc?[7].components(separatedBy: "PayFor:")
                }
                let fullDesc = "\(meterNo) \(name?[1] ?? "") \(township?[1] ?? "") Township \(diviLoc) \(diviLoc) Division Bill Month: \(dateFormat.0) Due Date \(dateFormat.1) \(payFor?[1] ?? "")"
                description = fullDesc.replacingOccurrences(of: ",", with: "")
                
            } else if record.rawDesc?.contains(find: "DTH") ?? false {
                description = record.rawDesc?.components(separatedBy: "Destination").first ?? ""
            } else {
                description = record.desc?.replacingOccurrences(of: ",", with: "")
            }
            var openingBal: Double = 0.0
            if let bal = record.walletBalance, let amt = record.amount {
                openingBal = Double(truncating: bal) + Double(truncating: amt)
                if openingBal < 1 {
                    openingBal = walletAmount()
                }
            }
            
            let behavior = NSDecimalNumberHandler(roundingMode: NSDecimalNumber.RoundingMode.plain,
                                                  scale: 2,
                                                  raiseOnExactness: false,
                                                  raiseOnOverflow: false,
                                                  raiseOnUnderflow: false,
                                                  raiseOnDivideByZero: false)
            let calcDN = NSDecimalNumber.init(value: openingBal).rounding(accordingToBehavior: behavior)
            var dateFormat = ("", "")
            if record.rawDesc?.contains(find: "ELECTRICITY") ?? false {
                dateFormat = self.formatDate(dateString: desc?.last ?? "")
            } else {
                
            }
            let date = (record.transactionDate?.stringValue() ?? "").replacingOccurrences(of: ",", with: "")
            recText = recText + ",\(userNo),\(phNo),\(billtype),\(dateFormat.0),\(dateFormat.1.replacingOccurrences(of: ",", with: "")),\(record.amount?.toString() ?? ""),\(calcDN.toString()),\(record.bonus?.toString() ?? ""),\(record.cashBack?.toString() ?? ""),\(record.amount?.toString() ?? ""),\(record.walletBalance?.toString() ?? ""),\(date),\(description ?? "")\n"
            excelTxt.append(recText)
        }
        excelTxt.append("\nTotal,,,,,,\(totalAmount.toString()),,\(bonusTransAmount.toString()),\(cashBackAmt.toString()),\(totalAmount.toString())")
        guard let excelUrl = CodeSnippets.generateExcel(excelDataWithFormat: excelTxt, excelFileName: "OK$ Bill_Payments Report") else {
            println_debug("Error - excel not generated")
            return
        }
        let activityViewController = UIActivityViewController(activityItems: [excelUrl], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
    
    func formatDate(dateString: String) -> (String, String) {
        let dateStr = dateString.components(separatedBy: "Due Date :")
        if dateStr.count > 0 {
            let currentDate = dateStr[1].replacingOccurrences(of: "#", with: "")
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy hh:mm:ss a"
            let date = formatter.date(from: currentDate)
            let previousMonth = Calendar.current.date(byAdding: .month, value: -1, to: date ?? Date())
            formatter.dateFormat = "EEE, dd-MMM-yyyy"
            let strDate = formatter.string(from: date ?? Date())
            formatter.dateFormat = "MMMM"
            let prevMonth = formatter.string(from: previousMonth ?? Date())
            return(prevMonth, strDate)
        }
        return ("", "")
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ReportBillPaymentsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        switch tableView.tag {
        case 0:
            if billRecords.count == 0 {
                let noDataLabel = UILabel(frame: tableView.bounds)
                noDataLabel.text = "No Records".localized
                noDataLabel.font = UIFont(name: appFont, size: 18)
                noDataLabel.textAlignment = .center
                tableView.backgroundView = noDataLabel
            } else {
                tableView.backgroundView = nil
            }
        default:
            break
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView.tag {
        case 0:
            return billRecords.count
        case 1:
            return filterArray.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView.tag {
        case 0:
            let reportBillPaymentCell = tableView.dequeueReusableCell(withIdentifier: cellReportBillPaymentId, for: indexPath) as? ReportBillPaymentCell
            reportBillPaymentCell!.configureReportBillItemCell(billRecord: billRecords[indexPath.row])
            return reportBillPaymentCell!
        case 1:
            let filterCell = tableView.dequeueReusableCell(withIdentifier: ConstantsXibCell.filterTableViewCell, for: indexPath) as? FilterTableViewCell
            configureFilterCell(cell: filterCell!, filterType: filterArray[indexPath.row])
            return filterCell!
        default:
            let reportBillPaymentCell = tableView.dequeueReusableCell(withIdentifier: cellReportBillPaymentId, for: indexPath) as? ReportBillPaymentCell
            return reportBillPaymentCell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView.tag {
        case 0:
            break
        case 1:
            let selectedFilter = filterArray[indexPath.row]
            switch selectedFilter.rootFilter {
            case .serviceProviderType:
                if selectedFilter.hasChild {
                    switch selectedFilter.filterTitle {
                    case ParentType.mobilePostpaid.rawValue:
                        expandPostpaidSelection(selectedFilter: selectedFilter)
                    case ParentType.giftCard.rawValue:
                        expandGiftCardSelection(selectedFilter: selectedFilter)
                    case ParentType.electricity.rawValue:
                        expandElectricitySelection(selectedFilter: selectedFilter)
                    default:
                        buttonServiceProviderType.setTitle(selectedFilter.filterTitle.localized, for: .normal)
                        tableViewFilter.isHidden = true
                    }
                    updateFilterTableHeight()
                } else {
                    buttonServiceProviderType.setTitle(selectedFilter.filterTitle.localized, for: .normal)
                    tableViewFilter.isHidden = true
                    applyFilter()
                }
            case .billType:
                buttonBillType.setTitle(selectedFilter.filterTitle.localized, for: .normal)
                tableViewFilter.isHidden = true
                applyFilter()
            case .amount:
                buttonAmountRange.setTitle(selectedFilter.filterTitle.localized, for: .normal)
                tableViewFilter.isHidden = true
                applyFilter()
            case .period:
                tableViewFilter.isHidden = true
                showDateOptions(selectedOption: selectedFilter.filterTitle.localized)
            }
        default:
            break
        }
    }
}

// MARK: - UITextFieldDelegate
extension ReportBillPaymentsViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tableViewFilter.isHidden = true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        tableViewBillPayments.contentOffset = CGPoint(x: 0, y: 0)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            if updatedText.count > 50 || (updatedText == "" && text == "") {
                return false
            }
            if string != "\n" {
                applyFilter(searchText: updatedText)
            }
        }
        return true
    }
}

extension ReportBillPaymentsViewController: UIScrollViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        targetContentOffset.pointee.y = max(targetContentOffset.pointee.y - 1, 1)
    }
}

// MARK: - Additional Methods
extension ReportBillPaymentsViewController: NavigationSetUpProtocol {
    /// It set up the navigation bar
    func setUpNavigation() {
        //self.navigationItem.titleView = getTitleView(screenTitle: ).titleView
        self.navigationController?.navigationBar.barTintColor        = ConstantsColor.navigationHeaderBillPayment
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: titleViewWidth, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        
        let searchButton = UIButton(type: .custom)
        searchButton.setImage(#imageLiteral(resourceName: "reportSearch").withRenderingMode(.alwaysOriginal), for: .normal)
        searchButton.addTarget(self, action: #selector(showSearchView), for: .touchUpInside)
        
        let shareButton = UIButton(type: .custom)
        shareButton.setImage(#imageLiteral(resourceName: "reportShare").withRenderingMode(.alwaysOriginal), for: .normal)
        shareButton.addTarget(self, action: #selector(showShareOption), for: .touchUpInside)
        
        let graphButton = UIButton(type: .custom)
        graphButton.setImage(#imageLiteral(resourceName: "reportGraph").withRenderingMode(.alwaysOriginal), for: .normal)
        graphButton.addTarget(self, action: #selector(showGraph), for: .touchUpInside)
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        let ypos = (titleViewSize.height - 30 ) / 2
        
        if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 180, y: ypos, width: 30, height: 30)
            shareButton.frame = CGRect(x: navTitleView.frame.size.width - 140, y: ypos, width: 30, height: 30)
            graphButton.frame = CGRect(x: navTitleView.frame.size.width - 100, y: ypos, width: 30, height: 30)
            backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        }
        else
        {
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 110, y: ypos, width: 30, height: 30)
            shareButton.frame = CGRect(x: navTitleView.frame.size.width - 70, y: ypos, width: 30, height: 30)
            graphButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
            backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        }
        
       
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 240, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text = ConstantsController.reportBillPaymentsViewController.screenTitle.localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 23) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        navTitleView.addSubview(searchButton)
        navTitleView.addSubview(shareButton)
        navTitleView.addSubview(graphButton)
        self.navigationItem.titleView = navTitleView
    }
    
    /// It shows the home screen
    @objc func backAction() {
        //        AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        //        self.navigationController?.popViewController(animated: true)
        if isSearchViewShow {
            hideSearchView()
        } else {
            AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension ReportBillPaymentsViewController: ReportModelDelegate {
    func apiResult(message: String?, statusCode: String?) {
        DispatchQueue.main.async {
            self.fetchRecordsFromDB()
            self.tableViewBillPayments.reloadData()
            self.resetFilterButtons()
            self.resetSortButtons(sender: UIButton())
            self.refreshControl.endRefreshing()
        }
    }
    func handleCustomError(error: CustomError) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}
