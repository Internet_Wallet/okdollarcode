//
//  ReceiptListViewController.swift
//  OK
//  It shows the receipt list
//  Created by ANTONY on 26/01/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReceiptListViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var tableViewReceipt: UITableView!
    
    // MARK: - Properties
    private let cellIdSort = "CellIdSort"
    private let cellIdentifier = "ReceiptTableViewCell"
    private var initialRecords = [ReportTransactionRecord]()
    private var transactionRecords = [ReportTransactionRecord]()
    private var headersKeys = [Date]()
    private var dictValues = [Date: [ReportTransactionRecord]]()
    private lazy var searchBar = UISearchBar()
    private var currentNavTitle = ""
    private var defaultNavTitle = ConstantsController.receiptListViewController.screenTitle.localized
    private var isSearchNeed = true
    var multiButton : ActionButton?
    var sortAmountAscending = false
    var sortNameAscending = false
    var sortCashBackAscending = false
    var sortBonusAscending = false
    var sortDateAscending = true
    var sortedRecent = false
    private let refreshControl = UIRefreshControl()
    var modelReport: ReportModel!
    // MARK: - View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewReceipt.tableFooterView = UIView()
        tableViewReceipt.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshTableData), for: .valueChanged)
        currentNavTitle = defaultNavTitle
        fetchRecordsFromDB()
        buildingDataSet()
        setUpNavigation()
        searchBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40)
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        searchBar.tintColor = UIColor.white
        searchBar.placeholder = "Search".localized
        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: "Zawgyi-One", size: 18) {
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                searchTextField.font = myFont
                searchTextField.font = myFont
                searchTextField.keyboardType = .asciiCapable
            }
        }
        let view = self.searchBar.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                subView.tintColor = UIColor.gray
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        loadFloatButtons()
        modelReport = ReportModel()
        modelReport.reportModelDelegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Methods
    @objc func refreshTableData() {
        guard let isHidden = multiButton?.floatButton.isHidden, !isHidden else {
            self.refreshControl.endRefreshing()
            return
        }
        modelReport.getTransactionInfo()
    }
    
    private func fetchRecordsFromDB() {
        if let unwrappedList = ReportCoreDataManager.sharedInstance.fetchTransactionRecords()?.transRecords {
            initialRecords = unwrappedList.allObjects as? [ReportTransactionRecord] ?? []
//            initialRecords = initialRecords.filter {
//                guard let acTransType = $0.accTransType else { return false }
//                return acTransType == "Cr"
//            }
            initialRecords = initialRecords.sorted {
                guard let date1 = $0.transactionDate, let date2 = $1.transactionDate else { return false }
                return date1.compare(date2) == .orderedDescending
            }
            transactionRecords = initialRecords
        }
    }
    
    private func buildingDataSet() {
        dictValues = [:]
        headersKeys = []
        formDictionaryForSort()
    }
    
    private func formDictionaryForSort() {
        for checkRecord in transactionRecords {
            var rowRecords = [ReportTransactionRecord]()
            if let chckDate = checkRecord.transactionDate {
                let chckDateStr = chckDate.stringValue(dateFormatIs: "dd MMM yyyy")
                if let chckFormattedDate = chckDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                    if Array(dictValues.keys).contains(chckFormattedDate) {
                        continue
                    }
                }
            }
            for record in transactionRecords {
                if let transDate = record.transactionDate, let checkDate = checkRecord.transactionDate {
                    if Calendar.current.isDate(checkDate, inSameDayAs: transDate) {
                        rowRecords.append(record)
                        let keyStr = checkDate.stringValue(dateFormatIs: "dd MMM yyyy")
                        if let keyDate = keyStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                            if !headersKeys.contains(keyDate) {
                                headersKeys.append(keyDate)
                            }
                            dictValues[keyDate] = rowRecords
                        }
                    }
                }
            }
        }
        headersKeys = headersKeys.sorted(by: >)
    }
    
    private func sortByAmount() {
        headersKeys = []
        dictValues = [:]
        var orderType = ComparisonResult.orderedAscending
        if sortAmountAscending {
            orderType = .orderedDescending
            sortAmountAscending = false
        } else {
            orderType = .orderedAscending
            sortAmountAscending = true
        }
        transactionRecords = transactionRecords.sorted(by: {
            guard let amount1 = $0.amount , let amount2 = $1.amount else { return false }
            return amount1.compare(amount2) == orderType
        })
        formDictionaryForSort()
    }
    
    private func sortByName() {
        headersKeys = []
        dictValues = [:]
        if sortNameAscending {
            transactionRecords = transactionRecords.sorted(by: {
                guard let name1 = $0.senderName , let name2 = $1.senderName else { return false }
                return name1 > name2
            })
            sortNameAscending = false
        } else {
            transactionRecords = transactionRecords.sorted(by: {
                guard let name1 = $0.senderName , let name2 = $1.senderName else { return false }
                return name1 < name2
            })
            sortNameAscending = true
        }
        formDictionaryForSort()
    }
    
    private func sortByCashBack() {
        headersKeys = []
        dictValues = [:]
        if sortCashBackAscending {
            transactionRecords = transactionRecords.sorted(by: {
                guard let cashBack1 = $0.cashBack , let cashBack2 = $1.cashBack else { return false }
                return cashBack1.compare(cashBack2) == .orderedDescending
            })
            sortCashBackAscending = false
        } else {
            transactionRecords = transactionRecords.sorted(by: {
                guard let cashBack1 = $0.cashBack , let cashBack2 = $1.cashBack else { return false }
                return cashBack1.compare(cashBack2) == .orderedAscending
            })
            sortCashBackAscending = true
        }
        formDictionaryForSort()
    }
    
    private func sortByBonus() {
        headersKeys = []
        dictValues = [:]
        if sortBonusAscending {
            transactionRecords = transactionRecords.sorted(by: {
                guard let bonus1 = $0.bonus , let bonus2 = $1.bonus else { return false }
                return bonus1.compare(bonus2) == .orderedDescending
            })
            sortBonusAscending = false
        } else {
            transactionRecords = transactionRecords.sorted(by: {
                guard let bonus1 = $0.bonus , let bonus2 = $1.bonus else { return false }
                return bonus1.compare(bonus2) == .orderedAscending
            })
            sortBonusAscending = true
        }
        formDictionaryForSort()
    }
    
    private func sortByDate() {
        if sortBonusAscending {
            headersKeys = headersKeys.sorted(by: >)
            sortBonusAscending = false
        } else {
            headersKeys = headersKeys.sorted(by: <)
            sortBonusAscending = true
        }
    }
    
    private func sortByRecent() {
        if sortedRecent {
            headersKeys = headersKeys.sorted(by: <)
            sortedRecent = false
        } else {
            headersKeys = headersKeys.sorted(by: >)
            sortedRecent = true
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let contentInset = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
            self.tableViewReceipt.contentInset = contentInset
            self.tableViewReceipt.scrollIndicatorInsets = contentInset
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.tableViewReceipt.contentInset = UIEdgeInsets.zero
        self.tableViewReceipt.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    @objc private func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func showSearchOption() {
        searchBar.placeholder = "Search".localized
        searchBar.text = ""
        self.navigationItem.titleView = searchBar
        searchBar.becomeFirstResponder()
        multiButton?.hide()
    }
    
    private func applySearch(with searchText: String) {
        var tempRecords = initialRecords
        if searchText.count > 0 {
            tempRecords = tempRecords.filter {
                
                if let senderName = $0.senderName {
                    if senderName.lowercased().range(of:searchText.lowercased()) != nil {
                        return true
                    }
                }
                if let mobileNumber = $0.destination {
                    if mobileNumber.range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                if let amount = $0.amount {
                    if amount.toString().range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                return false
            }
        }
        transactionRecords = tempRecords
        buildingDataSet()
        tableViewReceipt.reloadData()
    }
    
    func loadFloatButtons() {
        let sortByDate = ActionButtonItem(title: "Sort By Date".localized, image: #imageLiteral(resourceName: "receiptSortDate"))
        sortByDate.action = { item in
            self.sortByDate()
            self.currentNavTitle = "Sort By Date".localized
            self.isSearchNeed = true
            self.setUpNavigation()
            self.tableViewReceipt.reloadData()
            self.multiButton?.toggleMenu()
        }
        let cashBack = ActionButtonItem(title: "Cash Back".localized, image: #imageLiteral(resourceName: "receiptCashBack"))
        cashBack.action = { item in
            self.sortByCashBack()
            self.currentNavTitle = "Sort By Cash Back".localized
            self.isSearchNeed = true
            self.setUpNavigation()
            self.tableViewReceipt.reloadData()
            self.multiButton?.toggleMenu()
        }
        let bonus = ActionButtonItem(title: "Bonus".localized, image: #imageLiteral(resourceName: "receiptBonus"))
        bonus.action = { item in
            self.sortByBonus()
            self.currentNavTitle = "Sort By Bonus".localized
            self.isSearchNeed = true
            self.setUpNavigation()
            self.tableViewReceipt.reloadData()
            self.multiButton?.toggleMenu()
        }
        let recent = ActionButtonItem(title: "Recent".localized, image: #imageLiteral(resourceName: "receiptRecent"))
        recent.action = { item in
            self.sortByRecent()
            self.currentNavTitle = "Sort By Recent".localized
            self.isSearchNeed = true
            self.setUpNavigation()
            self.tableViewReceipt.reloadData()
            self.multiButton?.toggleMenu()
        }
        let sortByName = ActionButtonItem(title: "Sort By Name".localized, image: #imageLiteral(resourceName: "receiptSortName"))
        sortByName.action = { item in
            self.sortByName()
            self.currentNavTitle = "Sort By Name".localized
            self.isSearchNeed = true
            self.setUpNavigation()
            self.tableViewReceipt.reloadData()
            self.multiButton?.toggleMenu()
        }
        let sortByAmount = ActionButtonItem(title: "Sort By Amount".localized, image: #imageLiteral(resourceName: "receiptSortAmount"))
        sortByAmount.action = { item in
            self.sortByAmount()
            self.currentNavTitle = "Sort By Amount".localized
            self.isSearchNeed = true
            self.setUpNavigation()
            self.tableViewReceipt.reloadData()
            self.multiButton?.toggleMenu()
        }
        multiButton = ActionButton(attachedToView: self.view, items: [sortByDate, cashBack, bonus, recent, sortByName, sortByAmount])
        multiButton?.notifyBlurDelegate = self
        multiButton?.action = {
            button in button.toggleMenu()
            if let isExpanded = self.multiButton?.active, isExpanded == true {
                self.isSearchNeed = false
                self.setUpNavigation()
            } else {
                self.isSearchNeed = true
                self.setUpNavigation()
            }
            
        }
        multiButton?.setTitle("+", forState: UIControlState())
        multiButton?.backgroundColor = kYellowColor
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.multiButton = nil
    }
}

// MARK: -UISearchBarDelegate
extension ReceiptListViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            uiButton.setTitle("Cancel".localized, for:.normal)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            transactionRecords = initialRecords
            buildingDataSet()
            tableViewReceipt.reloadData()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
            let updatedText = searchText.replacingCharacters(in: textRange, with: text)
            if updatedText.count > 50 {
                return false
            }
            if updatedText != "" && text != "\n" {
                applySearch(with: updatedText)
            }
        }
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearchNeed = true
        setUpNavigation()
        transactionRecords = initialRecords
        buildingDataSet()
        tableViewReceipt.reloadData()
        multiButton?.show()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ReceiptListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if headersKeys.count == 0 {
            let noDataLabel = UILabel(frame: tableView.bounds)
            noDataLabel.text = "No records found!".localized
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
        } else {
            tableView.backgroundView = nil
        }
        return headersKeys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let dicts = dictValues[headersKeys[section]] else { return 0 }
        return dicts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let receiptCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        guard let transactionReceiptCell =  receiptCell as? ReceiptTableViewCell else {
            return receiptCell
        }
        if let dicts = dictValues[headersKeys[indexPath.section]] {
            transactionReceiptCell.configureReceiptCell(record: dicts[indexPath.row])
        }
        return transactionReceiptCell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return headersKeys[section].stringValue(dateFormatIs: "dd MMM yyyy")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let receiptDetailListViewController = UIStoryboard(name: "Reports", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.receiptDetailListViewController.nameAndID) as? ReceiptDetailListViewController else { return }
        if let dicts = dictValues[headersKeys[indexPath.section]] {
            receiptDetailListViewController.transactionDetail = dicts[indexPath.row]
        }
        self.navigationController?.pushViewController(receiptDetailListViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.textLabel?.textColor = UIColor.black
        header.textLabel?.font = UIFont(name: "Zawgyi-One", size: 17)
        header.textLabel?.textAlignment = .center
    }
}

// MARK: - Additional Methods
extension ReceiptListViewController: NavigationSetUpProtocol {
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        if isSearchNeed {
            let searchButton = UIButton(type: .custom)
            searchButton.setImage(#imageLiteral(resourceName: "reportSearch").withRenderingMode(.alwaysOriginal), for: .normal)
            searchButton.addTarget(self, action: #selector(showSearchOption), for: .touchUpInside)
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
            navTitleView.addSubview(searchButton)
        }
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  currentNavTitle + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: "Zawgyi-One", size: 23) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
}

extension ReceiptListViewController: NotifyBlurDelegate {
    func blurRemoveDelegate() {
        isSearchNeed = true
        setUpNavigation()
    }
}

extension ReceiptListViewController: ReportModelDelegate {
    func apiResult(message: String?, statusCode: String?) {
        DispatchQueue.main.async {
            self.fetchRecordsFromDB()
            self.buildingDataSet()
            self.tableViewReceipt.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
    
    func handleCustomError(error: CustomError) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}
