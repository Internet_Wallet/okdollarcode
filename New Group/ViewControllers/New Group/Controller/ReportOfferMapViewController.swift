//
//  ReportOfferMapViewController.swift
//  OK
//
//  Created by E J ANTONY on 14/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import MapKit

class ReportOfferMapViewController: UIViewController {

    //MARK: - Outlet
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tblSummaryRecord: UITableView!
    @IBOutlet weak var constraintTableHeight: NSLayoutConstraint!
    @IBOutlet weak var imgViewPanItem: UIImageView!
  @IBOutlet weak var btnViewPDF: UIButton!
  {
    didSet
    {
      btnViewPDF.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
      btnViewPDF.setTitle("View Receipt".localized, for: .normal)
    }
  }
  
    //MARK: - Properties
    private let regionRadius: CLLocationDistance = 1000
    private var dataArray = [OfferReporSummary]()
    private var heightToShowFullData: CGFloat = 0
    var sourceDataArray = [OfferReporFilterList]()
    var screenType = ScreenType.totalSummary
    enum ScreenType {
        case totalSummary, recordSummary
    }
    var amountType = 0

    //MARK: - Views life cycle method
    override func viewDidLoad() {
        super.viewDidLoad()
        tblSummaryRecord.tableFooterView = UIView()
        let initialLocation = CLLocation(latitude: 21.282778, longitude: -157.829444)
        centerMapOnLocation(location: initialLocation)
        setUpNavigation()
        loadData()
        imgViewPanItem.layer.cornerRadius = imgViewPanItem.frame.size.height / 2
        setTapGesture()
        imgViewPanItem.tag = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Methods
    private func setTapGesture() {
        let tapGest = UITapGestureRecognizer(target: self, action: #selector(toggleListViewingSize))
        imgViewPanItem.addGestureRecognizer(tapGest)
    }
    
    @objc private func toggleListViewingSize() {
        switch imgViewPanItem.tag {
        case 0:
            UIView.animate(withDuration: 1.2, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.7, options: [.curveEaseOut, .allowUserInteraction], animations: {
                self.constraintTableHeight.constant = 60
                self.imgViewPanItem.transform = CGAffineTransform.identity.rotated(by: CGFloat(Double.pi))
                self.view.layoutIfNeeded()
                self.imgViewPanItem.tag = 1
            })
        case 1:
            UIView.animate(withDuration: 1.2, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.7, options: [.curveEaseOut, .allowUserInteraction], animations: {
                self.imgViewPanItem.transform = CGAffineTransform.identity
                self.showFullList()
                self.imgViewPanItem.tag = 0
            })
        default:
            break
        }
        tblSummaryRecord.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }
    
    private func showFullList() {
        let fullHeight = (UIScreen.main.bounds.size.height / 2) + 50
        constraintTableHeight.constant = fullHeight >= heightToShowFullData ?  heightToShowFullData : fullHeight
        self.view.layoutIfNeeded()
    }
    
    private func showMinimizedList() {
        constraintTableHeight.constant = 50
        self.view.layoutIfNeeded()
    }
    
    private func populateRecordData() {
        for data in sourceDataArray {
            if let cName = data.promotionUsedCustomerName {
                dataArray.append(OfferReporSummary(title: "Account Name", value: cName))
            }
            if var mNumber = data.promotionUsedMobileNumber {
                if mNumber.hasPrefix("0095") {
                    mNumber = "0" + String(mNumber.substring(from: 4))
                }
                dataArray.append(OfferReporSummary(title: "Account Number", value: mNumber))
            }
            if let nAmount = data.totalNetAmount {
                dataArray.append(OfferReporSummary(title: "Bill Amount", value: "\(nAmount)",
                    valueType: OfferReporSummary.ValueType.attributed))
            }
            
            if amountType != 2 {
            if let discAmount = data.totalDiscountAmount {
                   let amont = wrapAmountWithCommaDecimal(key: "\(discountAmount)")
                dataArray.append(OfferReporSummary(title: "Discount Amount", value: "\(amont)",
                    valueType: OfferReporSummary.ValueType.attributed))
                
                let amount = Float(data.totalNetAmount ?? 0)
                let percentageValue = Float((Float(discAmount) * 100) / amount)
                dataArray.append(OfferReporSummary(title: "Discount Percentage".localized, value: "\(CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(percentageValue)")) %"))
            }
            }
            
            if amountType == 2 || amountType == 3 || amountType == 4 {
                
                if let bAmount = data.buyingQty {
                    dataArray.append(OfferReporSummary(title: "Sale Quantity", value: "\(bAmount)"))
                }
                if let bAmount = data.freeQty {
                    dataArray.append(OfferReporSummary(title: "Free Quantity", value: "\(bAmount)"))
                }
                
            }
           
            if amountType == 5 || amountType == 6 || amountType == 7 {
                    if let bAmount = data.buyingQty {
                        dataArray.append(OfferReporSummary(title: "Sale Quantity", value: "\(bAmount)"))
                    }
            }
            
            if let bAmount = data.totalBillAmount {
                dataArray.append(OfferReporSummary(title: "Net Amount", value: "\(bAmount)",
                    valueType: OfferReporSummary.ValueType.attributed))
            }
            if let pName = data.promotionName {
                dataArray.append(OfferReporSummary(title: "Promotion Name", value: "\(pName)"))
            }
            if let pCode = data.promotionCode {
                dataArray.append(OfferReporSummary(title: "Promotion Code", value: "\(pCode)"))
            }
            let dateStr = data.transactionTime
            if let dateVal = dateStr?.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") {
                let transDate = dateVal.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy\nHH:mm:ss")
                dataArray.append(OfferReporSummary(title: "Date & Time", value: "\(transDate)"))
            }
        }
        heightToShowFullData = CGFloat(50 * dataArray.count)
        showFullList()
    }
    
    private func showDataInMap() {
        var latitud: Double?
        var longitud: Double?
        for (index,item) in self.sourceDataArray.enumerated() {
            guard let latitu = item.lattitude, let itemLat = Double(latitu), let longitu = item.longitude, let itemLon = Double(longitu) else { return }
            if index == 0 {
                latitud = itemLat
                longitud = itemLon
            }
            var anotTitle: String?
            var anotSubtitle: String?
            anotTitle = item.promotionUsedMobileNumber ?? ""
            if let amount = item.totalBillAmount {
                anotSubtitle = "\(amount) " + "MMK"
            }
            let annotation = MKPointAnnotation()
            annotation.title = anotTitle
            annotation.subtitle = anotSubtitle
            annotation.coordinate = CLLocationCoordinate2D(latitude: itemLat, longitude: itemLon)
            mapView.addAnnotation(annotation)
        }
        guard let latVal = latitud, let longVal = longitud else { return }
        let location = CLLocationCoordinate2D(latitude: latVal,
                                              longitude: longVal)
        let span = MKCoordinateSpan.init(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
    }
    
    private func populateSummaryRecord() {
        let totalTransaction = sourceDataArray.count
        var totalBillAmount: Double = 0.0
        var totalDiscAmount: Double = 0.0
        var totalNetRecvdAmount: Double = 0.0
        var totalBuyingQty: Int = 0
        var totalFreeQty: Int = 0

        var promotionName = ""
        var promoCode = ""
        var transDate = ""
        if sourceDataArray.count > 0 {
            if let pName = sourceDataArray[0].promotionName {
                promotionName = pName
            }
            if let pCode = sourceDataArray[0].promotionCode {
                promoCode = pCode
            }
        }
        transDate = Date().stringValueWithOutUTC(dateFormatIs: "EEE, dd-MMM-yyyy\nHH:mm:ss")
        var aMerc: Set<String> = []
        var dMerc: Set<String> = []
        for data in sourceDataArray {
            if let mNum = data.advanceMerchantName {
                aMerc.insert(mNum)
            }
            if let dNum = data.dummyMerchantName {
                dMerc.insert(dNum)
            }
            if let nAmount = data.totalBillAmount {
                totalNetRecvdAmount += nAmount
            }
            if let discAmount = data.totalDiscountAmount {
                totalDiscAmount += discAmount
            }
            if let bAmount = data.totalNetAmount {
                totalBillAmount += bAmount
            }
            if let buyingQty = data.buyingQty {
                totalBuyingQty += buyingQty
            }
            if let freeQty = data.freeQty {
                totalFreeQty += freeQty
            }
        }
        dataArray.append(OfferReporSummary(title: "Advance Merchant", value: "\(aMerc.count)"))
        dataArray.append(OfferReporSummary(title: "Dummy Merchant", value: "\(dMerc.count)"))
        dataArray.append(OfferReporSummary(title: "Transaction", value: "\(totalTransaction)"))
        dataArray.append(OfferReporSummary(title: "Bill Amount", value: "\(totalBillAmount)", valueType: OfferReporSummary.ValueType.attributed))
        if amountType != 2 {
              let amont = wrapAmountWithCommaDecimal(key: "\(totalDiscAmount)")
            dataArray.append(OfferReporSummary(title: "Discount Amount".localized, value: "\(amont)",
                valueType: OfferReporSummary.ValueType.attributed))
            
            let percentageValue = Float((Float(totalDiscAmount) * 100) / Float(totalBillAmount))
            dataArray.append(OfferReporSummary(title: "Discount Percentage".localized, value: "\(CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(percentageValue)")) %"))
        }
        
        if amountType == 2 || amountType == 3 || amountType == 4 {
                dataArray.append(OfferReporSummary(title: "Sale Quantity", value: "\(totalBuyingQty)"))
                dataArray.append(OfferReporSummary(title: "Free Quantity", value: "\(totalFreeQty)"))
        }
        if amountType == 5 || amountType == 6 || amountType == 7 {
            dataArray.append(OfferReporSummary(title: "Sale Quantity", value: "\(totalBuyingQty)"))
        }
        dataArray.append(OfferReporSummary(title: "Net Amount", value: "\(totalNetRecvdAmount)", valueType: OfferReporSummary.ValueType.attributed))
        dataArray.append(OfferReporSummary(title: "Promotion Name", value: promotionName))
        dataArray.append(OfferReporSummary(title: "Promotion Code", value: promoCode))
        dataArray.append(OfferReporSummary(title: "Date & Time", value: transDate))
        heightToShowFullData = CGFloat(50 * dataArray.count)
        showFullList()
    }
    
    private func loadData() {
        dataArray = []
        switch screenType {
        case .recordSummary:
            populateRecordData()
        case .totalSummary:
            populateSummaryRecord()
        }
        showDataInMap()
    }
    
    private func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion.init(center: location.coordinate,
                                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    //Button Action Methods
    @IBAction func toggleListSize(_ sender: UIButton) {
        self.toggleListViewingSize()
    }
    
    @IBAction func showPdfScreen(_ sender: UIButton) {
        guard let reportOfferPdfScreenController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportOfferPdfScreenController.nameAndID) as? ReportOfferPdfScreenController else { return }
        reportOfferPdfScreenController.sourceDataArray = sourceDataArray
        switch screenType {
        case .recordSummary:
            reportOfferPdfScreenController.screenType = ReportOfferPdfScreenController.ScreenType.recordSummary
            
        case .totalSummary:
            reportOfferPdfScreenController.screenType = ReportOfferPdfScreenController.ScreenType.totalSummary
        }
        reportOfferPdfScreenController.amountType = amountType
        self.navigationController?.pushViewController(reportOfferPdfScreenController, animated: true)
    }
}

//MARK: - Outlets
extension ReportOfferMapViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let offerMapSummaryCell = tableView.dequeueReusableCell(withIdentifier: "OfferMapCell", for: indexPath) as? OfferMapCell else { return OfferMapCell() }
            let summaryData = dataArray[indexPath.row]
            offerMapSummaryCell.configureCell(data: summaryData)
            return offerMapSummaryCell
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        targetContentOffset.pointee.y = max(targetContentOffset.pointee.y - 1, 1)
    }
}

// MARK: - MKMapViewDelegate
extension ReportOfferMapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }
        
        let annotationIdentifier = "AnnotationIdentifier"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
        
        if let viewAnnotation = annotationView {
            viewAnnotation.annotation = annotation
        }
        else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView!.canShowCallout = true
        }
        
        let pinImage = UIImage(named: "offer_report_map_annotation")
        annotationView!.image = pinImage
        return annotationView
    }
}

// MARK: - Additional Methods
extension ReportOfferMapViewController: NavigationSetUpProtocol {
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  "Report Detail".localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        navTitleView.addSubview(label)
    
        navTitleView.addSubview(backButton)
        self.navigationItem.titleView = navTitleView
    }
}

class OfferMapCell: UITableViewCell {
    
    //MARK: - Outlet
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(data: OfferReporSummary) {
        self.lblTitle.text = data.title.localized
        switch data.valueType {
        case .attributed:
            self.lblValue.attributedText = CodeSnippets.getCurrencyInAttributed(withString: data.value, amountSize: 15, amountColor: .black, currencyDenomSize: 12, currencyDenomColor: .black)
        case .normal:
            self.lblValue.text = data.value
        }
    }
}

struct OfferReporSummary {
    enum ValueType {
        case attributed, normal
    }
    var title = ""
    var value = ""
    var valueType = ValueType.normal
    init(title: String, value: String, valueType: ValueType = ValueType.normal) {
        self.title = title
        self.value = value
        self.valueType = valueType
    }
}
