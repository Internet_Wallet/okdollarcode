//
//  ReportOfferFilterMainVC.swift
//  OK
//
//  Created by gauri OK$ on 5/3/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class ReportOfferFilterMainVC: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var lblTitleDateTime: UILabel!{
        didSet{
            lblTitleDateTime.font = UIFont(name : appFont, size : appFontSizeReport)
            lblTitleDateTime.text = "Date".localized
        }
    }
    @IBOutlet weak var lblTitlePromoCode: UILabel!{
        didSet{
            lblTitlePromoCode.font = UIFont(name : appFont, size : appFontSizeReport)
            lblTitlePromoCode.text = "Promotion Code".localized
        }
    }
    @IBOutlet weak var lblTitleTxCount: UILabel!{
        didSet{
            lblTitleTxCount.font = UIFont(name : appFont, size : appFontSizeReport)
            lblTitleTxCount.text = "Transaction Type".localized
        }
    }
    @IBOutlet weak var lblTitleRedeemCount: UILabel!{
        didSet{
            lblTitleRedeemCount.font = UIFont(name : appFont, size : appFontSizeReport)
            lblTitleRedeemCount.text = "Total Promo Redeem Count".localized
        }
    }
    @IBOutlet weak var lblTitleRecvdAmnt: UILabel!{
        didSet{
            lblTitleRecvdAmnt.font = UIFont(name : appFont, size : appFontSizeReport)
            lblTitleRecvdAmnt.text = "Total Received Amount".localized
        }
    }
   
    @IBOutlet weak var lblTitleDiscAmnt: UILabel!{
        didSet{
            lblTitleDiscAmnt.font = UIFont(name : appFont, size : appFontSizeReport)
            lblTitleDiscAmnt.text = "Total Discount Amount".localized
        }
    }
    @IBOutlet weak var lblTitleDiscPecentage: UILabel!{
        didSet{
            lblTitleDiscPecentage.font = UIFont(name : appFont, size : appFontSizeReport)
            lblTitleDiscPecentage.text = "Total Discount Percentage".localized
        }
    }
    @IBOutlet weak var lblTitleSellingQty: UILabel!{
        didSet{
            lblTitleSellingQty.font = UIFont(name : appFont, size : appFontSizeReport)
            lblTitleSellingQty.text = "Total Sale Quantity".localized
        }
    }
    @IBOutlet weak var lblTitleFreeQty: UILabel!{
        didSet{
            lblTitleFreeQty.font = UIFont(name : appFont, size : appFontSizeReport)
            lblTitleFreeQty.text = "Total Free Quantity".localized
        }
    }
    @IBOutlet weak var lblValueSellingQty: UILabel!{
        didSet{
            lblValueSellingQty.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblValueFreeQty: UILabel!{
        didSet{
            lblValueFreeQty.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblValueDateTime: UILabel!{
        didSet{
            lblValueDateTime.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblValueDiscPecentage: UILabel!{
        didSet{
            lblValueDiscPecentage.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    @IBOutlet weak var lblValuePromoCode: UILabel!{
        didSet{
            lblValuePromoCode.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblValueRedeemCount: UILabel!{
        didSet{
            lblValueRedeemCount.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblValuePromotionType: UILabel!
    @IBOutlet weak var lblValueTxType: UILabel!{
        didSet{
            lblValueTxType.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblValueRecvdAmnt: UILabel!{
        didSet{
            lblValueRecvdAmnt.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblValueDiscAmnt: UILabel!{
        didSet{
            lblValueDiscAmnt.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var btnViewDetails: UIButton!{
        didSet{
            btnViewDetails.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
            btnViewDetails.setTitle("View Details".localized, for: .normal)
        }
    }
    @IBOutlet weak var constraintBTable: NSLayoutConstraint!
    @IBOutlet weak var promoCodeView: UIView!
    @IBOutlet weak var txTypeView: UIView!
    @IBOutlet weak var redeemCountView: UIView!
    @IBOutlet weak var discountView: UIView!
    @IBOutlet weak var percentageView: UIView!
    @IBOutlet weak var sellingQtyView: UIView!
    @IBOutlet weak var freeQtyView: UIView!
    @IBOutlet weak var constraintHStackView: NSLayoutConstraint!
    
    //MARK: - Properties
    private var multiButton : ActionButton?
    private lazy var modelAReport: ReportAModel = {
        return ReportAModel()
    }()
    private var isDataRequested = false
    var promoCode = ""
    var promotionID = ""
    var promotionType = "2"
    var amountType = 0
    var offerFilterResult = [OfferReportList]()
    var dateStringToFilter = Date().stringValueWithOutUTC(dateFormatIs: "yyyy-MM-dd")
    var fromDateString = ""
    var toDateString = ""
    var townshipId: String?
    var advMerchantNo: String?
    var dummyMerchantNumber: [String]?
    var offerReportFilterSummary : OfferReportFilterSummary?
    
    
    //MARK: - Views Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        if amountType == 0 || amountType == 1 {//For fixed amount & percentage
            discountView.isHidden = false
            percentageView.isHidden = false
            sellingQtyView.isHidden = true
            freeQtyView.isHidden = true
        } else if amountType == 2 || amountType == 3 || amountType == 4 {
            discountView.isHidden = true
            percentageView.isHidden = true
            sellingQtyView.isHidden = false
            freeQtyView.isHidden = false
        } else if amountType == 5 || amountType == 6 || amountType == 7 {//Product
            discountView.isHidden = true
            percentageView.isHidden = true
            sellingQtyView.isHidden = false
            freeQtyView.isHidden = true
        }
        constraintHStackView.constant = 400//300
        btnViewDetails.isHidden = true
        setUpNavigation()
        self.loadFloatButtons()
        setRecvdAndDiscount()
        setSellingFreeQty()
        modelAReport.reportAModelDelegate  = self
        initialViewSetup()
        
        getSummaryData()
    }
    
    func initialViewSetup(){
        
        //Display date range
        //Convert string to date format
        var fromDate: String
        var toDate: String
        
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateFormat = "yyyy-MM-dd"
        
        let fromDateTemp = formatter.date(from: fromDateString)
        let formatterNew = DateFormatter()
        formatterNew.calendar = Calendar(identifier: .gregorian)
        formatterNew.dateFormat = "E,dd-MMM-yyyy"
        fromDate = formatterNew.string(from: fromDateTemp!)
        
        let toDateTemp = formatter.date(from: toDateString)
        toDate = formatterNew.string(from: toDateTemp!)
        
        if fromDateString == toDateString {
            lblValueDateTime.text = fromDate
        } else {
            lblValueDateTime.text = fromDate + " - " + toDate
        }
        //Set Promotion code ,Tx type,redeem count
        lblValuePromoCode.text = promoCode
        lblValueTxType.text = promotionType
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - API Methods
    private func getSummaryData() {
        let request = OfferSummaryFilterRequest(mobileNumber: UserModel.shared.mobileNo, promotionId: promotionID, toTransactionDate: toDateString, fromTransactionDate: fromDateString, townshipId: townshipId, advMerchantNo: advMerchantNo, dummyMerchantNo: dummyMerchantNumber)
        modelAReport.getOfferSummaryFilterRequest(request: request)
    }
    
    //To display all records
    func setSellingFreeQty(withSale: Int = 0, withFree: Int = 0) {
        lblValueSellingQty.text = "\(withSale)"
        lblValueFreeQty.text = "\(withFree)"
    }
    
    func setRecvdAndDiscount(withBillAmount: Int = 0, withRcvd: Int = 0, withDiscount: Int = 0, withPercentageDiscount: Double = 0.0) {
        lblValueRecvdAmnt.attributedText = CodeSnippets.getCurrencyInAttributed(withString: "\(withRcvd)")
        lblValueDiscAmnt.attributedText = CodeSnippets.getCurrencyInAttributed(withString: "\(withDiscount)")
        lblValueDiscPecentage.attributedText = CodeSnippets.getCurrencyPercentageInAttributed(withString: "\(withPercentageDiscount)")
    }
    
    // MARK: - Custom Methods
    func loadFloatButtons() {
        var buttonItems = [ActionButtonItem]()
        let date = ActionButtonItem(title: "Date".localized, image: #imageLiteral(resourceName: "receiptSortDate"))
        date.action = { item in
            self.multiButton?.toggleMenu()
            guard let reportDatePickerController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportDatePickerController.nameAndID) as? ReportDatePickerController else { return }
            reportDatePickerController.dateMode = .fromToDate
            reportDatePickerController.delegate = self
            reportDatePickerController.viewOrientation = .portrait
            reportDatePickerController.modalPresentationStyle = .overCurrentContext
            self.present(reportDatePickerController, animated: false, completion: nil)
        }
        
        let filter = ActionButtonItem(title: "Filter".localized, image: #imageLiteral(resourceName: "offer_report_menu_icon_filter"))
        filter.action = { item in
            self.multiButton?.toggleMenu()
            guard let reportOfferFilterController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportOfferFilterController.nameAndID) as? ReportOfferFilterController else { return }
            reportOfferFilterController.offerFilterResult = self.offerFilterResult
            reportOfferFilterController.sourcePromoCode = self.promoCode
            reportOfferFilterController.sourcePromoId = self.promotionID
            reportOfferFilterController.offerAllFilterDelegate = self
            self.navigationController?.pushViewController(reportOfferFilterController, animated: true)
        }
        
        buttonItems = [filter, date]
        
        multiButton = ActionButton(attachedToView: self.view, items: buttonItems)
        multiButton?.notifyBlurDelegate = self
        multiButton?.action = {
            button in button.toggleMenu()
        }
        multiButton?.setImage(#imageLiteral(resourceName: "menu_white_payto"), forState: .normal)
        multiButton?.backgroundColor = kYellowColor
    }

    @IBAction func viewDetailAction() {
    guard let reportOfferFilterResultController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportOfferFilterResultController.nameAndID) as? ReportOfferFilterResultController else { return }
        reportOfferFilterResultController.amountType = amountType
        reportOfferFilterResultController.promotionID = promotionID
        reportOfferFilterResultController.fromDateString = fromDateString
        reportOfferFilterResultController.toDateString = toDateString
        reportOfferFilterResultController.townshipId = townshipId
        reportOfferFilterResultController.advMerchantNo = advMerchantNo
        reportOfferFilterResultController.dummyMerchantNumber = dummyMerchantNumber
        self.navigationController?.pushViewController(reportOfferFilterResultController, animated: true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.constraintBTable.constant = keyboardSize.height
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.constraintBTable.constant = 0
        self.view.layoutIfNeeded()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}

extension Double {
    var stringWithoutZeroFraction: String {
        return truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}

// MARK: - ReportDatePickerDelegate
extension ReportOfferFilterMainVC: ReportDatePickerDelegate {
    func processWithDate(fromDate: Date?, toDate: Date?, dateMode: DateMode) {
        var selectedDate = ""
        switch dateMode {
        case .fromToDate:
            if let safeFDate = fromDate, let safeTDate = toDate {
                
                fromDateString = safeFDate.stringValue(dateFormatIs: "yyyy-MM-dd")
                toDateString = safeTDate.stringValue(dateFormatIs: "yyyy-MM-dd")
                
                if fromDateString == toDateString {
                    selectedDate = safeFDate.stringValue(dateFormatIs: "E,dd-MMM-yyyy")
                } else {
                    selectedDate = safeFDate.stringValue(dateFormatIs: "E,dd-MMM-yyyy") + " - " + safeTDate.stringValue(dateFormatIs: "E,dd-MMM-yyyy")
                }
                lblValueDateTime.text = selectedDate
                getSummaryData()
            }
        default:
            break
        }
    }
}

// MARK: - OfferAllFilterDelegate
extension ReportOfferFilterMainVC: OfferAllFilterDelegate {
    func applyAllFilter(promotionID: String, townshipID: String, advanceMerchantNo: String, dummyMerchantNo: [String]) {
        townshipId = townshipID
        advMerchantNo = advanceMerchantNo
        dummyMerchantNumber = dummyMerchantNo
        getSummaryData()
    }
}

//MARK: - NotifyBlurDelegate
extension ReportOfferFilterMainVC: NotifyBlurDelegate {
    func blurRemoveDelegate() {
    }
}

// MARK: - ReportModelDelegate
extension ReportOfferFilterMainVC: ReportModelDelegate {
    
    func updateSummaryView() {
        lblValueRedeemCount.text = offerReportFilterSummary?.totalTransactionCount?.stringWithoutZeroFraction
        lblValueRecvdAmnt.attributedText = CodeSnippets.getCurrencyInAttributed(withString: offerReportFilterSummary?.totalBillAmount?.stringWithoutZeroFraction ?? "")
        lblValueDiscAmnt.attributedText = CodeSnippets.getCurrencyInAttributed(withString: offerReportFilterSummary?.totalDiscountAmount?.stringWithoutZeroFraction ?? "")
        lblValueDiscPecentage.attributedText = CodeSnippets.getCurrencyPercentageInAttributed(withString: offerReportFilterSummary?.totalDiscountPercentage?.stringWithoutZeroFraction ?? "")
//        lblValueSellingQty.text = "\(offerReportFilterSummary?.totalSaleQty?. ?? 0.0)"
//        lblValueFreeQty.text = "\(offerReportFilterSummary?.totalFreeQty?.rounded() ?? 0.0)"
        lblValueSellingQty.text = offerReportFilterSummary?.totalSaleQty?.stringWithoutZeroFraction
        lblValueFreeQty.text = offerReportFilterSummary?.totalFreeQty?.stringWithoutZeroFraction
    }
    
    func apiResult(message: String?, statusCode: String?) {
        DispatchQueue.main.async {
            //Summary model
            self.isDataRequested = true
            self.offerReportFilterSummary = self.modelAReport.offerReportFilterSummary
            self.loadFloatButtons()
            self.updateSummaryView()
            self.btnViewDetails.isHidden = false
        }
    }
    
    func handleCustomError(error: CustomError) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}


// MARK: - Additional Methods
extension ReportOfferFilterMainVC: NavigationSetUpProtocol {
    func setUpNavigation() {
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        
        let ypos = (titleViewSize.height - 30 ) / 2
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label: MarqueeLabel!
        label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text = ConstantsController.reportOfferFilterResultController.screenTitle.localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
}
