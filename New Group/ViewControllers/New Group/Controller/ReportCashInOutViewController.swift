//
//  ReportCashInOutViewController.swift
//  OK
//  This show the list of cash in out records
//  Created by ANTONY on 05/02/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportCashInOutViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var tableViewCashInOut: UITableView!
    @IBOutlet weak var tableViewFilter: UITableView!
    @IBOutlet weak var tableViewOption: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var constraintScrollContentHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintFilterTableLeading: NSLayoutConstraint!
    @IBOutlet weak var constraintFilterTableHeight: NSLayoutConstraint!
    
    //For search functions
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var buttonCancel: UIButton!{
        didSet {
            buttonCancel.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonClear: UIButton!{
        didSet {
            buttonClear.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    //For filter functions
    @IBOutlet weak var buttonCashInOutFilter: ButtonWithImage!{
        didSet {
            buttonCashInOutFilter.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonPeriodFilter: ButtonWithImage!{
        didSet {
            buttonPeriodFilter.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    //For sorting functions
    @IBOutlet weak var buttonAgentSort: ButtonWithImage!{
        didSet {
            buttonAgentSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonTransactionSort: ButtonWithImage!{
        didSet {
            buttonTransactionSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonMobileNumberSort: ButtonWithImage!{
        didSet {
            buttonMobileNumberSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonCashInSort: ButtonWithImage!{
        didSet {
            buttonCashInSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonCashOutSort: ButtonWithImage!{
        didSet {
            buttonCashOutSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonServiceFeeSort: ButtonWithImage!{
        didSet {
            buttonServiceFeeSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonBalanceSort: ButtonWithImage!{
        didSet {
            buttonBalanceSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonInitiateAmountSort: ButtonWithImage!{
        didSet {
            buttonInitiateAmountSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateSort: ButtonWithImage!{
        didSet {
            buttonDateSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var constraintScrollTop: NSLayoutConstraint!
    
    @IBOutlet weak var viewCircle: UIView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var imgViewInCircle: UIImageView!
    @IBOutlet weak var labelInCircle: MarqueeLabel!{
        didSet {
            labelInCircle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var amountInCircle: UILabel!{
        didSet {
            amountInCircle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    // MARK: - Properties
    private let reportCashInCellId = "ReportCashInCell"
    private lazy var modelReport: ReportModel = {
        return ReportModel()
    }()
    private let cashInOutType = ["Filter", "All", "Sent Digital Money", "Received Digital Money"]
    private let periodFilters = ["Period", "All", "Date", "Month"]
    private let moreOptionsArray = ["Total Amount", "Total Commission"]
    private var filterArray = [String]()
    private enum FilterType {
        case cashInOut, period
    }
    private var circleViewTimer: Timer!
    private var cicoList = [ReportCICOList]()
    private var lastSelectedFilter = FilterType.cashInOut
    private var totalAmount: Double = 0
    private var totalCommissionAmount: Double = 0
    var fromDate: Date?
    var toDate: Date?
    private let refreshControl = UIRefreshControl()
    var isSearchViewShow: Bool {
        get {
            return !viewSearch.isHidden
        }
        set(newValue) {
            if newValue {
                viewSearch.isHidden = false
                textFieldSearch.becomeFirstResponder()
                constraintScrollTop.constant = 0
                self.view.layoutIfNeeded()
            } else {
                textFieldSearch.text = ""
                viewSearch.isHidden = true
                constraintScrollTop.constant = -40
                self.view.layoutIfNeeded()
                self.view.endEditing(true)
            }
        }
    }
    
    // MARK: - View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewCashInOut.tag = 0
        tableViewFilter.tag = 1
        tableViewOption.tag = 2
        tableViewCashInOut.tableFooterView = UIView()
        tableViewFilter.tableFooterView = UIView()
        tableViewOption.tableFooterView = UIView()
        tableViewCashInOut.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshTableData), for: .valueChanged)
        buttonCancel.layer.cornerRadius = 15
        buttonClear.layer.cornerRadius = 15
        buttonCancel.addTarget(self, action: #selector(hideSearchView), for: .touchUpInside)
        buttonClear.addTarget(self, action: #selector(clearSerchTextField), for: .touchUpInside)
        
        viewCircle.layer.cornerRadius = viewCircle.frame.size.height / 2
        buttonCancel.setTitle("Cancel".localized, for: .normal)
        buttonClear.setTitle("Clear".localized, for: .normal)
        textFieldSearch.placeholder = "Search By Transaction ID or Amount or Mobile Number".localized
        setLocalizedTitles()
        let cellFilterNibName = UINib(nibName: ConstantsXibCell.filterTableViewCell, bundle: nil)
        tableViewFilter.register(cellFilterNibName, forCellReuseIdentifier: ConstantsXibCell.filterTableViewCell)
        tableViewOption.register(cellFilterNibName, forCellReuseIdentifier: ConstantsXibCell.filterTableViewCell)
        AppDelegate.AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)
        setUpNavigation()
        modelReport.reportModelDelegate  = self
        getCICOReportList()
        resetFilterButtons()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        let navBarHeight = navigationController?.navigationBar.frame.size.height ?? 44
        let heightToReduce = navBarHeight == 32 ? 56 : 44
        constraintScrollContentHeight.constant = scrollView.frame.size.height - CGFloat(heightToReduce) + 44
        self.view.layoutIfNeeded()
    }
    
    // MARK: - Methods
    private func resetFilterButtons() {
        buttonCashInOutFilter.setTitle(cashInOutType[0].localized, for: .normal)
        buttonPeriodFilter.setTitle(periodFilters[0].localized, for: .normal)
    }
    
    private func getCICOReportList() {
        var cCode = "95"
        cCode = UserModel.shared.countryCode
        if cCode.hasPrefix("+") {
            cCode = "\(cCode.dropFirst())"
        }
        modelReport.getCashInCashOutReport(request: ReportCICORequest(countryCode: cCode, phoneNumber: UserModel.shared.mobileNo))
        //modelReport.getCashInCashOutReport(request: ReportCICORequest(countryCode: cCode, phoneNumber: "00959798148173"))
    }
    
    private func setLocalizedTitles() {
        buttonAgentSort.setTitle("User".localized, for: .normal)
        buttonTransactionSort.setTitle("Transaction ID".localized, for: .normal)
        buttonMobileNumberSort.setTitle("Mobile Number".localized, for: .normal)
        buttonCashInSort.setTitle("CICO Sent Digital Money".localized  + " (MMK)".localized, for: .normal)
        buttonCashOutSort.setTitle("CICO Received Digital Money".localized  + " (MMK)".localized, for: .normal)
        buttonServiceFeeSort.setTitle("Commission".localized  + " (MMK)".localized, for: .normal)
        buttonBalanceSort.setTitle("Balance".localized  + " (MMK)".localized, for: .normal)
        buttonInitiateAmountSort.setTitle("Initiate Amount".localized + " (MMK)".localized, for: .normal)
        buttonDateSort.setTitle("Date & Time".localized, for: .normal)
    }
    
    private func noRecordAlertAction() {
        alertViewObj.wrapAlert(title: "Alert".localized, body: "No Records".localized, img: UIImage(named : "AppIcon"))
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
        })
        alertViewObj.showAlert(controller: self)
    }
    
    private func showDateOptions(selectedOption: String) {
        switch selectedOption {
        case "Period".localized, "All".localized:
            self.fromDate = nil
            self.toDate = nil
            buttonPeriodFilter.setTitle(selectedOption, for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        case "Date".localized:
            presentDatePicker(mode: DateMode.fromToDate)
        case "Month".localized:
            presentDatePicker(mode: DateMode.month)
        default:
            break
        }
    }
    
    private func presentDatePicker(mode: DateMode) {
        guard let reportDatePickerController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportDatePickerController.nameAndID) as? ReportDatePickerController else { return }
        reportDatePickerController.dateMode = mode
        reportDatePickerController.delegate = self
        reportDatePickerController.modalPresentationStyle = .overCurrentContext
        self.present(reportDatePickerController, animated: false, completion: nil)
    }
    
    private func reloadTableWithScrollToTop() {
        self.tableViewCashInOut.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            if self.cicoList.count > 0 {
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableViewCashInOut.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
    }
    
    private func resetSortButtons(sender: UIButton) {
        if sender != buttonAgentSort {
            buttonAgentSort.tag = 0
            buttonAgentSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonTransactionSort {
            buttonTransactionSort.tag = 0
            buttonTransactionSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonMobileNumberSort {
            buttonMobileNumberSort.tag = 0
            buttonMobileNumberSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonCashInSort {
            buttonCashInSort.tag = 0
            buttonCashInSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonCashOutSort {
            buttonCashOutSort.tag = 0
            buttonCashOutSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonServiceFeeSort {
            buttonServiceFeeSort.tag = 0
            buttonServiceFeeSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonBalanceSort {
            buttonBalanceSort.tag = 0
            buttonBalanceSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonInitiateAmountSort {
            buttonInitiateAmountSort.tag = 0
            buttonInitiateAmountSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonDateSort {
            buttonDateSort.tag = 0
            buttonDateSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
    }
    
    private func applyFilter(searchText: String = "") {
        var filteredArray = modelReport.cicoReports
        var searchTxt = searchText
        if isSearchViewShow && searchTxt.count == 0 {
            searchTxt = textFieldSearch.text ?? ""
            filteredArray = filteredArray.sorted {
                guard let dateStr1 = $0.date, let date1 = dateStr1.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS")  else { return false }
                guard let dateStr2 = $1.date, let date2 = dateStr2.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") else { return false }
                return date1.compare(date2) == .orderedDescending
            }
        }
        filteredArray = filterProcess(forCICOType: filteredArray)
        filteredArray = filterProcess(forPeriod: filteredArray)
        if isSearchViewShow && searchTxt.count > 0 {
            filteredArray = filteredArray.filter {
                if let tID = $0.transactionID {
                    if tID.lowercased().range(of: searchTxt.lowercased()) != nil {
                        return true
                    }
                }
                if let amount = $0.rechargeAmount {
                    let rechargeAmount = "\(amount)"
                    if rechargeAmount.range(of: searchTxt.lowercased()) != nil {
                        return true
                    }
                }
                if let phNo = $0.receiverPhoneNumber {
                    if phNo.range(of: searchTxt.lowercased()) != nil {
                        return true
                    }
                }
                return false
            }
        }
        cicoList = filteredArray
        getTotalValues()
        reloadTableWithScrollToTop()
        resetSortButtons(sender: UIButton())
    }
    
    private func filterProcess(forCICOType arrayToFilter: [ReportCICOList]) -> [ReportCICOList] {
        if let transFilter = buttonCashInOutFilter.currentTitle {
            switch transFilter {
            case "Filter".localized, "All".localized:
                return arrayToFilter
            case "Sent Digital Money".localized:
                return arrayToFilter.filter {
                    return $0.cashType == 0
                }
            case "Received Digital Money".localized:
                return arrayToFilter.filter {
                    return $0.cashType == 1
                }
            default:
                return []
            }
        }
        return []
    }
    
    private func filterProcess(forPeriod arrayToFilter: [ReportCICOList]) -> [ReportCICOList] {
        if let periodFilter = buttonPeriodFilter.currentTitle {
            switch periodFilter {
            case "Period".localized, "All".localized:
                return arrayToFilter
            default:
                break
            }
            if let exactDateStart = self.fromDate {
                return arrayToFilter.filter {
                    guard let cDate = $0.date else { return false }
                    guard let createdDate = cDate.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") else { return false }
                    guard let exactDateEnd = self.toDate else { return false }
                    guard let dateEnd = Calendar.current.date(byAdding: Calendar.Component.day, value: 1, to: exactDateEnd) else { return false}
                    return (exactDateStart.compare(createdDate) == .orderedAscending && createdDate.compare(dateEnd) == .orderedAscending)
                }
            } else {
                return arrayToFilter.filter {
                    guard let cDate = $0.date else { return false }
                    guard let createdDate = cDate.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") else { return false }
                    let crDateStr = createdDate.stringValue(dateFormatIs: "yyyy-MM")
                    guard let createdMonth = crDateStr.dateValue(dateFormatIs: "yyyy-MM") else { return false }
                    guard let monthToFilter = self.toDate else { return false }
                    return createdMonth.compare(monthToFilter) == .orderedSame
                }
            }
        }
        return []
    }
    
    private func updateFilterTableHeight() {
        constraintFilterTableHeight.constant = filterArray.count > 4 ? CGFloat(5*44) : CGFloat(filterArray.count * 44)
        self.view.layoutIfNeeded()
    }
    
    private func shareRecordsByPdf() {
        let pdfView = UIView()
        if let pdfHeaderView = Bundle.main.loadNibNamed("ReportCICOPdfHeader", owner: self, options: nil)?.first as? ReportCICOPdfHeader {
            var filterPaymentType = "All"
            var filterPeriod = "All"
            if let fPaymentType = buttonCashInOutFilter.currentTitle {
                filterPaymentType = fPaymentType
                if fPaymentType == "Filter" {
                    filterPaymentType = "All"
                }
            }
            if let fPeriod = buttonPeriodFilter.currentTitle {
                filterPeriod = fPeriod
                if fPeriod == "Period" {
                    filterPeriod = "All"
                }
            }
            
            let filters = ["PaymentTypeFilter": filterPaymentType, "DateFilter": filterPeriod]
            
            pdfHeaderView.configureWithDatas(reportList: cicoList, filters: filters, currentBalance: CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: String(walletAmount())))

            let newHeight = pdfHeaderView.viewForItemHeader.frame.maxY
            pdfHeaderView.frame = CGRect(x: 0, y: 0, width: pdfHeaderView.frame.size.width, height: newHeight)
            pdfView.addSubview(pdfHeaderView)
            pdfView.frame = pdfHeaderView.bounds
            let xPos: CGFloat = pdfHeaderView.viewForItemHeader.frame.minX
            var yPos: CGFloat = pdfView.frame.maxY
            var index = 0
            for record in cicoList {
                if let pdfBodyView = Bundle.main.loadNibNamed("ReportCICOPdfBody", owner: self, options: nil)?.first as? ReportCICOPdfBody {
                    pdfBodyView.configureData(data: record)
                    let recordHeight = pdfBodyView.frame.size.height
                    let yPosInPage = Int(yPos) % 842
                    if (yPosInPage + Int(recordHeight)) > 842 {
                        yPos = yPos + CGFloat((842 - yPosInPage) + 30 + 30)
                    }
                    pdfBodyView.frame = CGRect(x: xPos, y: yPos, width: pdfBodyView.frame.size.width, height: recordHeight)
                    yPos = yPos + recordHeight + 2
                    pdfView.addSubview(pdfBodyView)
                    if index == 49 {
                        break
                    }
                    index += 1
                }
            }
            pdfView.frame = CGRect(x: 0, y: 0, width: pdfView.frame.size.width, height: yPos + 30)
            guard let pdfUrl = CodeSnippets.generatePDF(viewForPDF: pdfView, pdfFile: "OK$ Sent And Receive Digital Money Detail Statement") else {
                println_debug("Error - pdf not generated")
                return
            }
            let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
            if self.presentedViewController != nil {
                self.dismiss(animated: false, completion: {
                    [unowned self] in
                    DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
                    })
            }else{
                DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
            }
        }
    }
    
    private func shareRecordsByExcel() {
        var excelTxt = "Transaction ID,Mobile Number,Payment Type,Sent Digital Money (MMK), Receive Digital Money (MMK), Commission Amount (MMK), Initiate Amount (MMK), Balance (MMK),Date & Time\n"
        var sentTotAmount = 0.0
        var recivedTotAmount = 0.0
        var commissionTotAmount = 0.0
        var initiateTotAmount = 0.0
        for data in cicoList {
            var paymentType = ""
            if data.cashType == 0 {
                paymentType = "Cash In"
            } else {
                paymentType = "Cash Out"
            }
            var transID = ""
            if let tID = data.transactionID {
                transID = tID
            }
            var phNumber = ""
            var mNumber = ""
            if (data.cashType == 0 && data.requesterPhoneNumber ?? "" == UserModel.shared.mobileNo) || (data.cashType == 1 && data.requesterPhoneNumber ?? "" == UserModel.shared.mobileNo) {
                mNumber = data.receiverPhoneNumber ?? ""
            } else {
                if let payType = data.paymentType, (payType == "PAYTOID" || payType == "PAYWITHOUT ID") {
                    mNumber = "XXXXXXXXXX"
                } else {
                    mNumber = data.requesterPhoneNumber ?? ""
                }
            }
            phNumber = mNumber.replaceFirstTwoChar(withStr: "+")
            phNumber.insert(")", at: phNumber.index(phNumber.startIndex, offsetBy: 3))
            phNumber.insert("(", at: phNumber.startIndex)
            if mNumber.hasPrefix("0095") {
                phNumber.insert("0", at: phNumber.index(phNumber.startIndex, offsetBy: 5))
            }
            var inAmount = 0.0
            var outAmount = 0.0
            var initiateAmt = 0.0

            
            if (data.cashType == 0 && ((data.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo)) ||  (data.cashType == 1 && ((data.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo)) {
                
                if data.cashType == 0 && ((data.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo) {
                    if let commisson = data.commissionPercent, let totAmount = data.rechargeAmount {
                        let initiateAt = totAmount + commisson
                        sentTotAmount = sentTotAmount + initiateAt
                        inAmount = totAmount
                        initiateAmt = initiateAt
                        initiateTotAmount = initiateTotAmount + initiateAt
                    }
                } else {
                    if let amountStr = data.totalAmount {
                        sentTotAmount = sentTotAmount + amountStr
                        inAmount = amountStr
                        initiateAmt = amountStr
                        initiateTotAmount = initiateTotAmount + amountStr
                    }
                }
            }  else {
                if data.cashType == 0 && ((data.receiverPhoneNumber ?? "") == UserModel.shared.mobileNo) {
                    if let commisson = data.commissionPercent, let totAmount = data.rechargeAmount {
                        let initiateAt = totAmount + commisson
                        recivedTotAmount = recivedTotAmount + initiateAt
                        outAmount = totAmount
                        initiateAmt = initiateAt
                        initiateTotAmount = initiateTotAmount + initiateAt
                    }
                } else {
                    if let amountStr = data.totalAmount {
                        recivedTotAmount = recivedTotAmount + amountStr
                        outAmount = amountStr
                        initiateAmt = amountStr
                        initiateTotAmount = initiateTotAmount + amountStr
                    }
                }
            }
            
//            if let tAmount = data.totalAmount, let commission = data.commissionPercent {
//                 if data.cashType == 0 && ((data.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo) {
//                    inAmount = tAmount + commission
//                    sentTotAmount = sentTotAmount + tAmount + commission
//                } else {
//                    if data.cashType == 0 && ((data.receiverPhoneNumber ?? "") == UserModel.shared.mobileNo) {
//                        recivedTotAmount = recivedTotAmount + tAmount - commission
//                        inAmount = tAmount - commission
//                    } else {
//                        recivedTotAmount = recivedTotAmount + tAmount
//                        outAmount = tAmount
//                    }
//                }
//            }
            
            var cAmount = "0.0"
            if let sAmount = data.commissionPercent {
                cAmount = "\(sAmount)"
                commissionTotAmount = commissionTotAmount + sAmount
            }
//            if let totAmt = data.totalAmount {
//                    initiateTotAmount = initiateTotAmount + totAmt
//                    initiateAmt = "\(totAmt)"
//            }
            var tBalance = "0.0"
             if let mNumber = data.receiverPhoneNumber, mNumber == UserModel.shared.mobileNo {
                tBalance = "\(UserLogin.shared.walletBal)"
            } else if let balan = data.senderClosingBalance {
                tBalance = "\(balan)"
            }
            var dateTime = ""
            if let dateVal = data.convertedDate?.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") {
                let dateStr = dateVal.stringValue(dateFormatIs: "EEE dd-MMM-yyyy HH:mm:ss")
                dateTime = dateStr
            }
            let recText = "\(transID),\(phNumber),\(paymentType),\(inAmount),\(outAmount),\(cAmount),\(initiateAmt),\(tBalance),\(dateTime)\n"
            excelTxt.append(recText)
        }
        excelTxt.append("\nTotal,,,\(sentTotAmount),\(recivedTotAmount),\(commissionTotAmount),\(initiateTotAmount)")
//        guard let excelUrl = CodeSnippets.generateExcel(excelDataWithFormat: excelTxt, excelFileName: "SentReceivedDigitalAmountExcel") else {
//            println_debug("Error - excel not generated")
//            return
//        }
//        let activityViewController = UIActivityViewController(activityItems: [excelUrl], applicationActivities: nil)
//        self.present(activityViewController, animated: true, completion: nil)
        
        
        guard let excelUrl = CodeSnippets.generateExcel(excelDataWithFormat: excelTxt, excelFileName: "OK$ Sent And Receive Digital Money Detail Statement") else {
            println_debug("Error - excel not generated")
            return
        }
        let activityViewController = UIActivityViewController(activityItems: [excelUrl], applicationActivities: nil)
        if self.presentedViewController != nil {
            self.dismiss(animated: false, completion: {
                [unowned self] in
                DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
                })
        }else{
            DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
            
        }
//        self.present(activityViewController, animated: true, completion: nil)
    }
    
    private func noRecordAlertActions() {
        alertViewObj.wrapAlert(title: "Alert".localized, body: "No Records".localized, img: UIImage(named : "AppIcon"))
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
        })
        alertViewObj.showAlert(controller: self)
    }
    
    private func removeGestureFromShadowView() {
        if let recognizers = viewShadow.gestureRecognizers {
            for recognizer in recognizers {
                if let recognizer = recognizer as? UITapGestureRecognizer {
                    viewShadow.removeGestureRecognizer(recognizer)
                }
            }
        }
    }
    
    func getTotalValues() {
        totalAmount = 0
        totalCommissionAmount = 0
        cicoList.forEach { (record) in
            if let amount = record.totalAmount {
                totalAmount += amount
            }
            if let cAmount = record.commissionPercent {
                totalCommissionAmount += cAmount
            }
        }
    }
    
    //MARK: - Target Methods
    @objc func refreshTableData() {
        self.hideSearchView()
        if viewSearch.isHidden {
           getCICOReportList()
        } else {
            self.refreshControl.endRefreshing()
        }
    }
    
    @objc private func hideSearchView() {
        self.view.endEditing(true)
        textFieldSearch.text = ""
        viewSearch.isHidden = true
        constraintScrollTop.constant = -40
        self.view.layoutIfNeeded()
        self.view.endEditing(true)
        applyFilter()
    }
    
    @objc private func clearSerchTextField() {
        textFieldSearch.text = ""
        applyFilter()
    }
    
    @objc private func showSearchView() {
        circleViewTimer?.invalidate()
        tableViewFilter.isHidden = true
        tableViewOption.isHidden = true
        viewCircle.isHidden = true
        viewShadow.isHidden = true
        removeGestureFromShadowView()
        if !isSearchViewShow {
            if cicoList.count > 0 {
                isSearchViewShow = true
                textFieldSearch.becomeFirstResponder()
            } else {
                noRecordAlertAction()
            }
        } else {
            //textFieldSearch.becomeFirstResponder()
            hideSearchView()
        }
    }
    
    @objc private func showShareOption() {
        circleViewTimer?.invalidate()
        tableViewFilter.isHidden = true
        tableViewOption.isHidden = true
        viewCircle.isHidden = true
        viewShadow.isHidden = true
        removeGestureFromShadowView()
        if cicoList.count < 1 {
            noRecordAlertAction()
        } else {
            guard let reportSharingViewController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportSharingViewController.nameAndID) as? ReportSharingViewController else { return }
            reportSharingViewController.delegate = self
            reportSharingViewController.pdfImage = UIImage(named: "topUpPdf")
            reportSharingViewController.excelImage = UIImage(named: "topUpExcel")
            reportSharingViewController.modalPresentationStyle = .overCurrentContext
            self.present(reportSharingViewController, animated: false, completion: nil)
        }
    }
    
    @objc func runTimedCode() {
        self.viewCircle.isHidden = true
        self.viewShadow.isHidden = true
        circleViewTimer?.invalidate()
    }
    
    @objc private func showMoreOption() {
        circleViewTimer?.invalidate()
        if cicoList.count == 0 {
            noRecordAlertActions()
            return
        }
        if tableViewOption.isHidden {
            viewCircle.isHidden = true
            viewShadow.isHidden = true
            tableViewFilter.isHidden = true
            self.view.endEditing(true)
            tableViewOption.isHidden = false
            let tapGestToRemoveView = UITapGestureRecognizer(target: self, action: #selector(shadowIsTapped))
            viewShadow.addGestureRecognizer(tapGestToRemoveView)
        } else {
            tableViewOption.isHidden = true
        }
    }
    
    @objc func shadowIsTapped(sender: UITapGestureRecognizer) {
        removeGestureFromShadowView()
        viewShadow.isHidden = true
        viewCircle.isHidden = true
    }
    
    // MARK: - Button Actions
    @IBAction func shareRecordInPdf(_ sender: UIButton) {
        guard let cashInOutCell = sender.superview?.superview?.superview as? ReportCashInCell else { return }
        guard let indexPath = tableViewCashInOut.indexPath(for: cashInOutCell) else { return }
        guard cicoList.count > 0 else { return }
        let record = cicoList[indexPath.row]
        var pdfDictionary = [String: Any]()
        pdfDictionary["logoName"] = "appIcon_Ok"
        var header = record.cashType == 0 ? "Sent Digital Money Transaction Receipt" : "Received Digital Money Transaction Receipt"
        pdfDictionary["Payment Type"] = record.cashType == 0 ? "Cash In" : "Cash Out"
        pdfDictionary["transactionID"] = record.transactionID ?? ""
        pdfDictionary["Received"] = "false"
        if var phNo = record.receiverPhoneNumber {
            if phNo == UserModel.shared.mobileNo {
                phNo = record.requesterPhoneNumber ?? ""
                header = "Received Digital Amount Transaction Receipt"
                pdfDictionary["Payment Type"] = "Cash Out"
                pdfDictionary["Received"] = "true"
                if phNo.hasPrefix("0095") {
                    phNo = "(+95)0\(phNo.substring(from: 4))"
                }
            } else {
                pdfDictionary["Payment Type"] = "Cash In"
                header = "Sent Digital Amount Transaction Receipt"
                if let payType = record.paymentType, (payType == "PAYTOID" || payType == "PAYWITHOUT ID") {
                    phNo = "XXXXXXXXXX"
                } else {
                    if phNo.hasPrefix("0095") {
                        phNo = "(+95)0\(phNo.substring(from: 4))"
                    }
                }
            }
            pdfDictionary["mobileNumber"] = phNo
        }
        pdfDictionary["invoiceTitle"] = header
        if let amountStr = record.commissionPercent {
            pdfDictionary["cicoServiceFee"] = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(amountStr)")
        }
        if let dateStr = record.convertedDate {
            if let dateInDF = dateStr.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") {
                pdfDictionary["transactionDate"] = dateInDF.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss")
            }
        }
//        if let amountValue = record.totalAmount, let cAmountValue = record.commissionPercent {
//            if record.cashType == 0 {
//                let amountWithoutCommission = amountValue + cAmountValue
//                pdfDictionary["amountCICO"] = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(amountWithoutCommission)")
//            } else {
//                let amountWithoutCommission = amountValue - cAmountValue
//                pdfDictionary["amountCICO"] = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(amountWithoutCommission)")
//            }
//        }
//        
//        if let tAmount = record.totalAmount, let commission = record.commissionPercent {
//            if record.cashType == 0 && ((record.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo) {
//                let amountStr = tAmount + commission
//                pdfDictionary["amount"] = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(amountStr)") + " MMK"
//            } else {
//                if record.cashType == 0 && ((record.receiverPhoneNumber ?? "") == UserModel.shared.mobileNo) {
//                    let amountStr = tAmount - commission
//                    pdfDictionary["amount"] = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(amountStr)") + " MMK"
//                } else {
//                    pdfDictionary["amount"] = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(tAmount)") + " MMK"
//                }
//            }
//        }
        
        if (record.cashType == 0 && ((record.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo)) ||  (record.cashType == 1 && ((record.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo)) {
            if record.cashType == 0 && ((record.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo) {
                if let commisson = record.commissionPercent, let totAmount = record.rechargeAmount {
                    let initiateAt = totAmount + commisson
                    pdfDictionary["amount"] = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(totAmount)")
                    pdfDictionary["amountCICO"] = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(initiateAt)")
                }
            } else {
                if let amountStr = record.totalAmount {
                    pdfDictionary["amount"] = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(amountStr)")
                    pdfDictionary["amountCICO"] = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(amountStr)")
                }
            }
        }  else {
            if record.cashType == 0 && ((record.receiverPhoneNumber ?? "") == UserModel.shared.mobileNo) {
                if let commisson = record.commissionPercent, let totAmount = record.rechargeAmount {
                    let initiateAt = totAmount + commisson
                    pdfDictionary["amount"] = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(totAmount)")
                    pdfDictionary["amountCICO"] = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(initiateAt)")
                }
            } else {
                if let commisson = record.commissionPercent, let totAmount = record.totalAmount {
                    let initiateAt = totAmount - commisson
                    pdfDictionary["amount"] = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(totAmount)")
                    pdfDictionary["amountCICO"] = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(initiateAt)")
                }
            }
        }
        
       let titleNa = header
        guard let pdfUrl = CodeSnippets.createInvoicePDF(dictValues: pdfDictionary, pdfFile: titleNa) else {
            println_debug("Error - pdf not generated")
            return
        }
        let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
    
    // It filters and show the detail by transaction type
    @IBAction func filterCashInOutType(_ sender: UIButton) {
        self.view.endEditing(true)
        tableViewOption.isHidden = true
        func showFilterAtCashInOut() {
            filterArray = cashInOutType
            tableViewFilter.reloadData()
            constraintFilterTableLeading.constant = 110
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
            tableViewFilter.isHidden = false
        }
        if tableViewFilter.isHidden {
            showFilterAtCashInOut()
        } else {
            switch lastSelectedFilter {
            case .cashInOut:
                tableViewFilter.isHidden = true
            default:
                showFilterAtCashInOut()
            }
        }
        lastSelectedFilter = .cashInOut
    }
    
    // It filters and show the detail by period
    @IBAction func filterByPeriod(_ sender: UIButton) {
        self.view.endEditing(true)
        tableViewOption.isHidden = true
        func showFilterAtPeriod() {
            filterArray = periodFilters
            tableViewFilter.reloadData()
            constraintFilterTableLeading.constant = 510
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
            tableViewFilter.isHidden = false
        }
        if tableViewFilter.isHidden {
            showFilterAtPeriod()
        } else {
            switch lastSelectedFilter {
            case .period:
                tableViewFilter.isHidden = true
            default:
                showFilterAtPeriod()
            }
        }
        lastSelectedFilter = .period
    }
    
    @IBAction func sortByAgentID() {
        
    }
    
    @IBAction func sortByTransactionID(_ sender: UIButton) {
        func sortTransactionID(orderType: ComparisonResult) -> [ReportCICOList]{
            return cicoList.sorted {
                guard let transID1 = $0.transactionID, let transID2 = $1.transactionID else { return false }
                return transID1.localizedStandardCompare(transID2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            cicoList = sortTransactionID(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            cicoList = sortTransactionID(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewCashInOut.reloadData()
    }
    
    @IBAction func sortByMobileNumber(_ sender: UIButton) {
        func sortMobileNumber(orderType: ComparisonResult) -> [ReportCICOList]{
            return cicoList.sorted {
                let transectionTypeA = $0.cashType
                let transectionTypeB = $1.cashType
                var phNumber1 = ""
                var phNumber2 = ""
                
                println_debug("0= Cash type >> " + "\($0.cashType)")
                println_debug("phNumber1= receiverPhoneNumber >> " + $0.receiverPhoneNumber!)
                println_debug("phNumber1= requesterPhoneNumber >> " + $0.requesterPhoneNumber!)
                println_debug("1= Cash type >> " + "\($1.cashType)")
                println_debug("phNumber2= receiverPhoneNumber >> " + $1.receiverPhoneNumber!)
                println_debug("phNumber2= requesterPhoneNumber >> " + $1.requesterPhoneNumber!)
                
                if let phNo = $0.receiverPhoneNumber {
                    if phNo == UserModel.shared.mobileNo {
                        if let rqstPhNo = $0.requesterPhoneNumber {
                            if rqstPhNo.hasPrefix("0095") {
                                phNumber1 = "0\(rqstPhNo.substring(from: 4))"
                            }
                        }
                    } else {
                        if phNo.hasPrefix("0095") {
                            phNumber1 = "0\(phNo.substring(from: 4))"
                        }
                    }
                }
                
                if let phNo = $1.receiverPhoneNumber {
                    if phNo == UserModel.shared.mobileNo {
                        if let rqstPhNo = $1.requesterPhoneNumber {
                            if rqstPhNo.hasPrefix("0095") {
                                phNumber2 = "0\(rqstPhNo.substring(from: 4))"
                            }
                        }
                    } else {
                        if phNo.hasPrefix("0095") {
                            phNumber2 = "0\(phNo.substring(from: 4))"
                        }
                    }
                }
                
                /*
                if transectionTypeA == 0 {
                    guard let phNumberA = $0.requesterPhoneNumber else { return false }
                    phNumber1 = phNumberA
                    
                }
                else {
                    guard let phNumberB = $0.receiverPhoneNumber else { return false }
                    phNumber1 = phNumberB
                }
                if transectionTypeB == 0 {
                    guard let phNumberC = $1.receiverPhoneNumber else { return false }
                    phNumber2 = phNumberC
                }
                else {
                    guard let phNumberD = $1.requesterPhoneNumber else { return false }
                    phNumber2 = phNumberD
                }
                */
                print("Phomne 1 : \(phNumber1) And Phone 2 : \(phNumber2)")
                return phNumber1.compare(phNumber2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            cicoList = sortMobileNumber(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            cicoList = sortMobileNumber(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewCashInOut.reloadData()
    }
    
    @IBAction func sortByCashIn(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            cicoList = cicoList.sorted {
                //                guard let amount1 = $0.totalAmount, let amount2 = $1.totalAmount else { return false }
                //                guard $0.cashType == 1 else { return false }
                //                return amount1 < amount2
                
                var amount1 = $0.totalAmount ?? 0.0
                var amount2 = $1.totalAmount ?? 0.0
                if ($0.cashType == 0 && (($0.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo)) ||  ($0.cashType == 1 && (($0.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo)) {
                    
                    if $0.cashType == 0 && (($0.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo) {
                        amount1 = $0.rechargeAmount ?? 0.0
                    } else {
                        amount1 = $0.totalAmount ?? 0.0
                    }
                }  else {
                    amount1 = 0.0
                }
                
                if ($1.cashType == 0 && (($1.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo)) ||  ($1.cashType == 1 && (($1.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo)) {
                    
                    if $1.cashType == 0 && (($1.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo) {
                        amount2 = $1.rechargeAmount ?? 0.0
                    } else {
                        amount2 = $1.totalAmount ?? 0.0
                    }
                }  else {
                    amount2 = 0.0
                }
                
                return amount1 < amount2
            }
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            cicoList = cicoList.sorted {
                //                guard let amount1 = $0.totalAmount, let amount2 = $1.totalAmount else { return false }
                //                guard $0.cashType == 1 else { return false }
                var amount1 = $0.totalAmount ?? 0.0
                var amount2 = $1.totalAmount ?? 0.0
                if ($0.cashType == 0 && (($0.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo)) ||  ($0.cashType == 1 && (($0.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo)) {
                    
                    if $0.cashType == 0 && (($0.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo) {
                        amount1 = $0.rechargeAmount ?? 0.0
                    } else {
                        amount1 = $0.totalAmount ?? 0.0
                    }
                }  else {
                    amount1 = 0.0
                }
                
                if ($1.cashType == 0 && (($1.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo)) ||  ($1.cashType == 1 && (($1.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo)) {
                    
                    if $1.cashType == 0 && (($1.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo) {
                        amount2 = $1.rechargeAmount ?? 0.0
                    } else {
                        amount2 = $1.totalAmount ?? 0.0
                    }
                }  else {
                    amount2 = 0.0
                }
                return amount1 > amount2
            }
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            break
        }
        resetSortButtons(sender: sender)
        tableViewCashInOut.reloadData()
    }
    
    @IBAction func sortByCashOut(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            cicoList = cicoList.sorted {
                //                guard let amount1 = $0.totalAmount, let amount2 = $1.totalAmount else { return false }
                //                guard $0.cashType == 1 else { return false }
                //                return amount1 < amount2
                
                var amount1 = $0.totalAmount ?? 0.0
                var amount2 = $1.totalAmount ?? 0.0
                if ($0.cashType == 0 && (($0.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo)) ||  ($0.cashType == 1 && (($0.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo)) {
                    amount1 = 0.0
                    
                }  else {
                    if $0.cashType == 0 && (($0.receiverPhoneNumber ?? "") == UserModel.shared.mobileNo) {
                        amount1 = $0.rechargeAmount ?? 0.0
                    } else {
                        amount1 = $0.totalAmount ?? 0.0
                    }
                }
                
                if ($1.cashType == 0 && (($1.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo)) ||  ($1.cashType == 1 && (($1.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo)) {
                    amount2 = 0.0
                    
                }  else {
                    if $1.cashType == 0 && (($1.receiverPhoneNumber ?? "") == UserModel.shared.mobileNo) {
                        amount2 = $1.rechargeAmount ?? 0.0
                    } else {
                        amount2 = $1.totalAmount ?? 0.0
                    }
                }
                
                return amount1 < amount2
                
            }
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            cicoList = cicoList.sorted {
                //                guard let amount1 = $0.totalAmount, let amount2 = $1.totalAmount else { return false }
                //                guard $0.cashType == 1 else { return false }
                var amount1 = $0.totalAmount ?? 0.0
                var amount2 = $1.totalAmount ?? 0.0
                if ($0.cashType == 0 && (($0.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo)) ||  ($0.cashType == 1 && (($0.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo)) {
                    amount1 = 0.0
                }  else {
                    if $0.cashType == 0 && (($0.receiverPhoneNumber ?? "") == UserModel.shared.mobileNo) {
                        amount1 = $0.rechargeAmount ?? 0.0
                    } else {
                        amount1 = $0.totalAmount ?? 0.0
                    }
                }
                
                if ($1.cashType == 0 && (($1.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo)) ||  ($1.cashType == 1 && (($1.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo)) {
                    amount2 = 0.0
                }  else {
                    if $1.cashType == 0 && (($1.receiverPhoneNumber ?? "") == UserModel.shared.mobileNo) {
                        amount2 = $1.rechargeAmount ?? 0.0
                    } else {
                        amount2 = $1.totalAmount ?? 0.0
                    }
                }
                return amount1 > amount2
            }
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            break
        }
        resetSortButtons(sender: sender)
        tableViewCashInOut.reloadData()
    }
    
    @IBAction func sortByServiceFee(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            cicoList = cicoList.sorted {
                guard let amount1 = $0.commissionPercent, let amount2 = $1.commissionPercent else { return false }
                return amount1 < amount2
            }
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            cicoList = cicoList.sorted {
                guard let amount1 = $0.commissionPercent, let amount2 = $1.commissionPercent else { return false }
                return amount1 > amount2
            }
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            break
        }
        resetSortButtons(sender: sender)
        tableViewCashInOut.reloadData()
    }
    
    @IBAction func sortByBalance(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            cicoList = cicoList.sorted {
                var amount1 = $0.senderClosingBalance ?? 0.0
                var amount2 = $1.senderClosingBalance ?? 0.0
                
                if let phNo = $0.receiverPhoneNumber, phNo == UserModel.shared.mobileNo {
                    amount1 = Double(UserLogin.shared.walletBal) ?? 0
                }
                if let phNo = $1.receiverPhoneNumber, phNo == UserModel.shared.mobileNo {
                    amount2 = Double(UserLogin.shared.walletBal) ?? 0
                }
                
                return amount1 < amount2

//                guard let amount1 = $0.senderClosingBalance, let amount2 = $1.senderClosingBalance else { return false }
//                  guard $0.cashType == 0, $0.requesterPhoneNumber == UserModel.shared.mobileNo else { return false }
            }
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            cicoList = cicoList.sorted {
                //                guard let amount1 = $0.senderClosingBalance, let amount2 = $1.senderClosingBalance else { return false }
                //                  guard $0.cashType == 0, $0.requesterPhoneNumber == UserModel.shared.mobileNo else { return false }
                
                var amount1 = $0.senderClosingBalance ?? 0.0
                var amount2 = $1.senderClosingBalance ?? 0.0
                
                if let phNo = $0.receiverPhoneNumber, phNo == UserModel.shared.mobileNo {
                    amount1 = Double(UserLogin.shared.walletBal) ?? 0
                }
                if let phNo = $1.receiverPhoneNumber, phNo == UserModel.shared.mobileNo {
                    amount2 = Double(UserLogin.shared.walletBal) ?? 0
                }
                
                return amount1 > amount2
            }
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            break
        }
        resetSortButtons(sender: sender)
        tableViewCashInOut.reloadData()
    }
    
    @IBAction func sortByInitiateAmount(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            cicoList = cicoList.sorted {
                guard let amount1 = $0.totalAmount, let amount2 = $1.totalAmount else { return false }
                return amount1 < amount2
            }
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            cicoList = cicoList.sorted {
                guard let amount1 = $0.totalAmount, let amount2 = $1.totalAmount else { return false }
                return amount1 > amount2
            }
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            break
        }
        resetSortButtons(sender: sender)
        tableViewCashInOut.reloadData()
    }
    
    @IBAction func sortByDate(_ sender: UIButton) {
        func sortDates(orderType: ComparisonResult) -> [ReportCICOList]{
            return cicoList.sorted {
                guard let dateStr1 = $0.date, let date1 = dateStr1.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS")  else { return false }
                guard let dateStr2 = $1.date, let date2 = dateStr2.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") else { return false }
                return date1.compare(date2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            cicoList = sortDates(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            cicoList = sortDates(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewCashInOut.reloadData()
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ReportCashInOutViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        switch tableView.tag {
        case 0:
            if cicoList.count == 0 {
                let noDataLabel = UILabel(frame: tableView.bounds)
                noDataLabel.text = "No Records".localized
                noDataLabel.font = UIFont(name: appFont, size: 18)
                noDataLabel.textAlignment = .center
                tableView.backgroundView = noDataLabel
            } else {
                tableView.backgroundView = nil
            }
            return 1
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView.tag {
        case 0:
            return cicoList.count
        case 1:
            return filterArray.count
        case 2:
            return moreOptionsArray.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView.tag {
        case 0:
            let reportCashInCell = tableView.dequeueReusableCell(withIdentifier: reportCashInCellId, for: indexPath) as? ReportCashInCell
            let reportData = cicoList[indexPath.row]
            reportCashInCell?.configureCell(record: reportData)
            return reportCashInCell!
        case 1:
            let filterCell = tableView.dequeueReusableCell(withIdentifier: ConstantsXibCell.filterTableViewCell, for: indexPath) as? FilterTableViewCell
            filterCell?.configureTopUpFilter(filterName: filterArray[indexPath.row])
            return filterCell!
        case 2:
            let filterCell = tableView.dequeueReusableCell(withIdentifier: ConstantsXibCell.filterTableViewCell, for: indexPath) as? FilterTableViewCell
            filterCell?.configureMoreOptionInCICO(filterName: moreOptionsArray[indexPath.row])
            return filterCell!
        default:
            let reportCashInCell = tableView.dequeueReusableCell(withIdentifier: reportCashInCellId, for: indexPath) as? ReportCashInCell
            return reportCashInCell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView.tag {
        case 0:
            break
        case 1:
            switch lastSelectedFilter {
            case .cashInOut:
                buttonCashInOutFilter.setTitle(filterArray[indexPath.row].localized, for: .normal)
                tableViewFilter.isHidden = true
                applyFilter(searchText: textFieldSearch.text ?? "")
            case .period:
                tableViewFilter.isHidden = true
                showDateOptions(selectedOption: filterArray[indexPath.row].localized)
            }
        case 2:
            switch indexPath.row {
            case 0:
                if let image = UIImage(named: "newpayamount") {
                    imgViewInCircle.image = image
                }
                labelInCircle.text = "Total Amount".localized
                amountInCircle.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(totalAmount)")
            case 1:
                if let image = UIImage(named: "newpayamount") {
                    imgViewInCircle.image = image
                }
                labelInCircle.text = "Total Commission Amount".localized + " " + "(MMK)".localized
                amountInCircle.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(totalCommissionAmount)")
            default:
                break
            }
            viewCircle.isHidden = false
            viewShadow.isHidden = false
            tableViewOption.isHidden = true
            circleViewTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: false)
        default:
            break
        }
    }
}

// MARK: - Additional Methods
extension ReportCashInOutViewController: NavigationSetUpProtocol {
    /// It set up the navigation bar
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = ConstantsColor.navigationHeaderTopUpRecharge
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: titleViewWidth, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        
        let searchButton = UIButton(type: .custom)
        searchButton.setImage(#imageLiteral(resourceName: "reportSearch").withRenderingMode(.alwaysOriginal), for: .normal)
        searchButton.addTarget(self, action: #selector(showSearchView), for: .touchUpInside)
        
        let shareButton = UIButton(type: .custom)
        shareButton.setImage(#imageLiteral(resourceName: "reportShare").withRenderingMode(.alwaysOriginal), for: .normal)
        shareButton.addTarget(self, action: #selector(showShareOption), for: .touchUpInside)
        
        let moreButton = UIButton(type: .custom)
        moreButton.setImage(#imageLiteral(resourceName: "topUpMore").withRenderingMode(.alwaysOriginal), for: .normal)
        moreButton.addTarget(self, action: #selector(showMoreOption), for: .touchUpInside)
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        let ypos = (titleViewSize.height - 30 ) / 2
        if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 180, y: ypos, width: 30, height: 30)
            shareButton.frame = CGRect(x: navTitleView.frame.size.width - 140, y: ypos, width: 30, height: 30)
            moreButton.frame = CGRect(x: navTitleView.frame.size.width - 100, y: ypos, width: 30, height: 30)
            backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        }
        else{
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 110, y: ypos, width: 30, height: 30)
            shareButton.frame = CGRect(x: navTitleView.frame.size.width - 70, y: ypos, width: 30, height: 30)
            moreButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
            backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        }
       
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 160, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text = ConstantsController.reportCashInOutViewController.screenTitle.localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 23) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        navTitleView.addSubview(searchButton)
        navTitleView.addSubview(shareButton)
        navTitleView.addSubview(moreButton)
        self.navigationItem.titleView = navTitleView
    }
    
    /// It shows the home screen
    @objc func backAction() {
        if !isSearchViewShow {
            AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
            self.navigationController?.popViewController(animated: true)
        } else {
            //textFieldSearch.becomeFirstResponder()
            hideSearchView()
        }
    }
}

// MARK: - UITextFieldDelegate
extension ReportCashInOutViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tableViewFilter.isHidden = true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        tableViewCashInOut.contentOffset = CGPoint(x: 0, y: 0)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            if updatedText.count > 50 || (updatedText == "" && text == "") {
                return false
            }
            if string != "\n" {
                applyFilter(searchText: updatedText)
            }
        }
        return true
    }
}

// MARK: - ReportModelDelegate
extension ReportCashInOutViewController: ReportModelDelegate {
    func apiResult(message: String?, statusCode: String?) {
        DispatchQueue.main.async {
            self.cicoList = self.modelReport.cicoReports
            self.getTotalValues()
            self.tableViewCashInOut.reloadData()
            self.resetSortButtons(sender: UIButton())
            self.resetFilterButtons()
            self.reloadTableWithScrollToTop()
            self.refreshControl.endRefreshing()
        }
    }
    
    func handleCustomError(error: CustomError) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}

// MARK: - ReportDatePickerDelegate
extension ReportCashInOutViewController: ReportDatePickerDelegate {
    func processWithDate(fromDate: Date?, toDate: Date?, dateMode: DateMode) {
        self.fromDate = fromDate
        self.toDate = toDate
        var selectedDate = ""
        switch dateMode {
        case .fromToDate:
            if let safeFDate = fromDate, let safeTDate = toDate {
                let fDateInString = safeFDate.stringValue(dateFormatIs: "dd MMM yyyy")
                let tDateInString = safeTDate.stringValue(dateFormatIs: "dd MMM yyyy")
                selectedDate = fDateInString + " to\n" + tDateInString
            }
        case .month:
            if let safeDate = toDate {
                let monthInString = safeDate.stringValue(dateFormatIs: "MMM yyyy")
                selectedDate = monthInString
            }
        default:
            break
        }
        buttonPeriodFilter.setTitle(selectedDate, for: .normal)
        applyFilter(searchText: textFieldSearch.text ?? "")
    }
}

//MARK: - ReportSharingOptionDelegte
extension ReportCashInOutViewController: ReportSharingOptionDelegte {
    func share(by: ReportSharingViewController.ReportShareOption) {
        switch by {
        case .pdf:
            shareRecordsByPdf()
        case .excel:
            shareRecordsByExcel()
        }
    }
}
