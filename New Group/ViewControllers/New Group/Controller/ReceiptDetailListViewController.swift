//
//  ReceiptDetailListViewController.swift
//  OK
//  This shows the receipt detail in list
//  Created by ANTONY on 01/02/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReceiptDetailListViewController: OKBaseController {

    // MARK: - Outlet
    @IBOutlet weak var tableViewDetail: UITableView!
    
    // MARK: - Properties
    private struct Detail {
        var title: String
        var value: String
    }
    private var detailArray = [Detail]()
    var transactionDetail: ReportTransactionRecord?
    var strSenderName = ""
    
    // MARK: - View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigation()
        loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Target Methods PDF
    @objc private func showShareOption() {
       var pdfDictionary = Dictionary<String,Any>()
        pdfDictionary["logoName"] = "appIcon_Ok"
        pdfDictionary["invoiceTitle"] = "Transaction Receipt".localized
        pdfDictionary["senderAccName"] = ""
        if let name = transactionDetail?.senderName, name.count > 0 {
            var value = name
            value = transactionDetail?.senderName?.replacingOccurrences(of: "-cashin", with: "").replacingOccurrences(of: "-cashout", with: "") ?? ""

            if value.contains("@sender"){
                let newValue = value.components(separatedBy: "@sender")
                if newValue.indices.contains(1){
                    let nameNew = newValue[1].components(separatedBy: "@businessname")
                    if nameNew.indices.contains(0){
                        pdfDictionary["senderAccName"] = nameNew[0]
                    }
                }
            }else{
                pdfDictionary["senderAccName"] = transactionDetail?.senderName?.replacingOccurrences(of: "-cashin", with: "").replacingOccurrences(of: "-cashout", with: "") ?? "Unknown"
//name
            }
            
        } else {
            if let comnts = transactionDetail?.desc, comnts.lowercased().contains(find: "solar") {
                pdfDictionary["senderAccName"] = UserModel.shared.name
            }
        }
        if transactionDetail?.accTransType == "Cr", transactionDetail?.destination == nil {
            pdfDictionary["senderAccName"] = "-"
            pdfDictionary["receiverAccName"] = UserModel.shared.name 
        } else {
            pdfDictionary["receiverAccName"] = transactionDetail?.receiverName
        }
        if let trType = transactionDetail?.accTransType {
            if trType == "Dr" {
                if let businessName = transactionDetail?.receiverBName, businessName.count > 0 {
                    pdfDictionary["businessName"] = businessName
                }
            } else {
                if let businessName = transactionDetail?.senderBName, businessName.count > 0 {
                    pdfDictionary["businessName"] = businessName
                }
            }
        }
        var tType = ""
        var pType: String? = nil
        if let desc = transactionDetail?.rawDesc {
            if desc.contains("#OK") || desc.contains(find: "OOK") {
                if desc.contains("Top-Up") || desc.contains("TopUp") || desc.contains("Bonus Point Top Up") || desc.contains("TopUpPlan"){
                    tType = "Top-Up"
                } else if desc.contains("Data Plan") || desc.contains("DataPlan"){
                    tType = "Data Plan"
                } else if desc.contains("MPU Card") {
                    tType = "MPU"
                } else if desc.contains("VISA") {
                    tType = "VISA"
                } else if desc.contains(find: "PAYMENTTYPE:GiftCard") {
                    tType = "Gift Card"
                } else if transactionDetail?.rawDesc?.contains(find: "ELECTRICITY") ?? false {
                    tType = "ELECTRICITY"
                }  else if transactionDetail?.rawDesc?.contains(find: "POSTPAID") ?? false {
                    tType = "POSTPAID"
                } else if  transactionDetail?.rawDesc?.contains("MERCHANTPAY") ?? false {
                    tType = "MERCHANTPAY"
                } else if  transactionDetail?.rawDesc?.contains("LANDLINE") ?? false {
                    tType = "LANDLINE"
                } else if  transactionDetail?.rawDesc?.contains("INSURANCE") ?? false {
                    tType = "INSURANCE"
                } else if  transactionDetail?.rawDesc?.contains("SUNKING") ?? false {
                    tType = "SUNKING"
                } else if let desc = transactionDetail?.rawDesc, desc.contains(find: "DTH") {
                    tType = "DTH"
                } else {
                    tType = "Special Offers"
                }
            } else {
                if let transType = transactionDetail?.transType {
                    tType = transType
                    if transType == "PAYWITHOUT ID" {
                        tType = "Hide Number to Pay"
                        if transactionDetail?.rawDesc?.contains(find: "#Tax Code") ?? false {
                            tType = "Tax Payment"
                        }
                    } else if transType == "TOLL" {
                        tType = "TOLL"
                    } else if transType == "TICKET" {
                        tType = "TICKET"
                    } else {
                        tType = "PAYTO"
                        if transactionDetail?.rawDesc?.contains(find: "MPU") ?? false {
                            tType = "MPU"
                        } else if transactionDetail?.rawDesc?.contains(find: "REFUND") ?? false {
                            tType = "Refund"
                        } else if transactionDetail?.destination == nil, let cashback = transactionDetail?.cashBack, cashback > 0 {
                            tType = "Cash Back"
                        } else if let desc = transactionDetail?.rawDesc, desc.contains(find: "SEND MONEY TO BANK:") {
                            tType = "SEND MONEY TO BANK"
                        }  else if desc.contains(find: "-cashin") {
                            pType = "Cash In"
                        } else if desc.contains(find: "-cashout") {
                            pType = "Cash Out"
                        } else if desc.contains(find: "-transferto") {
                            pType = "Transfer To"
                        }
                    }
                }
            }
        }
        
        pdfDictionary["transactionType"] = tType
        pdfDictionary["paymentType"] = pType
        if let accTransType = transactionDetail?.accTransType {
            if accTransType == "Cr" {
                var sNumber = ""
                var rNumber = UserModel.shared.mobileNo
                if let des = transactionDetail?.destination {
                    sNumber = des
                    if des.hasPrefix("0095") {
                        sNumber = "(+95)0\(des.substring(from: 4))"
                    }
                }
                if rNumber.hasPrefix("0095") {
                    rNumber = "(+95)0\(rNumber.substring(from: 4))"
                }
                if  transactionDetail?.rawDesc?.contains(find: "OK Wallet Topup") ?? false {
                    sNumber = "XXXXXXXXXX"
                }  else if transactionDetail?.destination == nil, let cashback = transactionDetail?.cashBack, cashback > 0 {
                    sNumber = "XXXXXXXXXX"
                }
                
                pdfDictionary["senderAccNo"] = sNumber
                pdfDictionary["receiverAccNo"] = rNumber
                if let transType = transactionDetail?.transType {
                    if transType == "PAYWITHOUT ID" {
                        pdfDictionary["senderAccNo"] = "XXXXXXXXXX"
                    }
                }
              
                if  transactionDetail?.rawDesc?.contains(find: "Bank To Wallet") ?? false {
                   sNumber = "XXXXXXXXXX"
                    pdfDictionary["senderAccNo"] = "XXXXXXXXXX"
               }
            } else {
                var sNumber = UserModel.shared.mobileNo
                var rNumber = ""
                if let des = transactionDetail?.operatorName, des.hasPrefix("0095") {
                    rNumber = des
                    if des.hasPrefix("0095") {
                        //rNumber = "(+95)0\(des.substring(from: 4))"
                        rNumber = CodeSnippets.getMobileNumber(transRecord: transactionDetail)

                    } else {
                        if let cCode = transactionDetail?.countryCode, cCode.count > 0 {
                            rNumber = "(\(cCode))\(rNumber)"
                        }
                    }
                } else if let des = transactionDetail?.destination {
                    rNumber = des
                    if des.hasPrefix("0095") {
                        rNumber = "(+95)0\(des.substring(from: 4))"
                    } else {
                        if let cCode = transactionDetail?.countryCode, cCode.count > 0 {
                            rNumber = "(\(cCode))\(rNumber)"
                        }
                    }
                }
                if transactionDetail?.rawDesc?.contains(find: "ELECTRICITY") ?? false {
                    rNumber = "XXXXXXXXXX"
                } else if transactionDetail?.rawDesc?.contains(find: "MPU") ?? false {
                    rNumber = "XXXXXXXXXX"
                } else if transactionDetail?.rawDesc?.contains(find: "REFUND") ?? false {
                    rNumber = "XXXXXXXXXX"
                } else if transactionDetail?.rawDesc?.contains(find: "POSTPAID") ?? false || transactionDetail?.rawDesc?.contains(find: "MERCHANTPAY") ?? false || transactionDetail?.rawDesc?.contains(find: "LANDLINE") ?? false || transactionDetail?.rawDesc?.contains(find: "SUNKING") ?? false || transactionDetail?.rawDesc?.contains(find: "INSURANCE") ?? false {
                    rNumber = "XXXXXXXXXX"
                }  else if transactionDetail?.destination == nil, let cashback = transactionDetail?.cashBack, cashback > 0 {
                    rNumber = "XXXXXXXXXX"
                } else if let desc = transactionDetail?.rawDesc, desc.contains(find: "SEND MONEY TO BANK:") {
                    rNumber = "XXXXXXXXXX"
                } else if let desc = transactionDetail?.rawDesc, desc.contains(find: "DTH") {
                    rNumber = "XXXXXXXXXX"
                } else if  transactionDetail?.rawDesc?.contains(find: "PAYMENTTYPE:GiftCard") ?? false {
                    rNumber = "XXXXXXXXXX"
                } else if  transactionDetail?.rawDesc?.contains(find: "ProjectId") ?? false {
                    var mobileNo = "XXXXXXXXXX"
                    let value = transactionDetail?.rawDesc ?? ""
                    if value.contains(find: "ProjectId"){
                        let newValue = value.components(separatedBy: " Wallet Number :")
                        if newValue.count > 2 {
                            if newValue.indices.contains(1){
                                let name = newValue[2].components(separatedBy: "ProjectId")
                                if name.indices.contains(0){
                                    mobileNo = "(+95)".appending(name[0])
                                }
                            }
                        }  else {
                            let arrVal = value.components(separatedBy: "Account Number :")
                            if arrVal.indices.contains(1){
                                let name = arrVal[1].components(separatedBy: "ProjectId")
                                if name.indices.contains(0){
                                    mobileNo = "(+95)".appending(name[0])
                                }
                            }
                        }
                    }
                    rNumber = mobileNo
                } else if  transactionDetail?.rawDesc?.contains(find: "#Tax Code") ?? false {
                    rNumber = "XXXXXXXXXX"
                } else if  transactionDetail?.rawDesc?.contains(find: "Bonus Point Exchange Payment") ?? false {
                    rNumber = "XXXXXXXXXX"
                }
                
                if sNumber.hasPrefix("0095") {
                    sNumber = "(+95)0\(sNumber.substring(from: 4))"
                }
                pdfDictionary["receiverAccNo"] = rNumber
                pdfDictionary["senderAccNo"] = sNumber
                if let sName = pdfDictionary["senderAccName"] as? String, sName.count > 0 {
                    pdfDictionary["senderAccName"] = sName
                } else {
                    pdfDictionary["senderAccName"] = UserModel.shared.name
                }
                if let des = transactionDetail?.rawDesc {
                    if !des.contains(find: "#point-redeempoints") {
                        if !des.contains(find: "#OK-PMC") {
                            let desArr = des.components(separatedBy: "-")
                            if desArr.count > 2 {
                                if des.contains("Top-Up") || des.contains("TopUp") || des.contains("Bonus Point Top Up") || des.contains("TopUpPlan") ||  des.contains("Data Plan") || des.contains("DataPlan") || des.contains("Special Offers") || des.contains("Special Offers") {
                                    pdfDictionary["operatorName"] = desArr[2]
                                } else if des.contains(find: "Overseas") {
                                    pdfDictionary["operatorName"] = desArr[2]
                                }
                            }
                        }
                    }
                }
            }
        }
    
        pdfDictionary["receiverAccName"] = strSenderName

        pdfDictionary["transactionID"] = transactionDetail?.transID
        if let nsBonus = transactionDetail?.bonus {
            pdfDictionary["bonusPoint"] = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: nsBonus.toString()) + " PTS"
        }
        if let transDate = transactionDetail?.transactionDate {
            pdfDictionary["transactionDate"] = transDate.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss")
        }
        if let comment = getSuccessAndComments(comment: true), comment.count > 0 {
            pdfDictionary["remarks"] = comment
        }
        
        if let transAmount = transactionDetail?.amount {
            pdfDictionary["amount"] = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: transAmount.toString()) + " MMK"
        }
        if let comnt = transactionDetail?.rawDesc, comnt.contains("#point-redeempoints-") {
            pdfDictionary["qrImage"] = nil
        } else {
            if let transType = transactionDetail?.accTransType, transType == "Dr", let detail = transactionDetail {
                pdfDictionary["qrImage"] = getQRCodeImage(qrData: detail)
            }
        }
        if let promoRawDesc = transactionDetail?.rawDesc, promoRawDesc.contains("#OK-PMC") {
            pdfDictionary["isForPromocode"] = true
            var helper: OfferCommentHelper! = OfferCommentHelper(comment: promoRawDesc.removingPercentEncoding)
            if helper.promotionTransactionCount == 1 && helper.paymentType == "1" {
                pdfDictionary["promoReceiverNumber"] = transactionDetail?.destination?.replacingOccurrences(of: "0095", with: "(+95)0")
                if let transType = transactionDetail?.accTransType, transType == "Dr" {
                    //Show tx done from user login
                    pdfDictionary["senderAccNo"] = UserModel.shared.mobileNo.replacingOccurrences(of: "0095", with: "(+95)0")
                    pdfDictionary["receiverAccNo"] = helper.promoReceiverNumber
                } else {
                    pdfDictionary["senderAccNo"] = helper.senderCreditNumber
                    pdfDictionary["receiverAccNo"] = UserModel.shared.mobileNo.replacingOccurrences(of: "0095", with: "(+95)0")
                }
            }
            pdfDictionary["promocode"] = helper.promoCode
            if let discPercent = helper.discountPercent {
                pdfDictionary["discountPercent"] = discPercent + " %"
            }
            if let discAmount = helper.discountAmount {
             pdfDictionary["discountAmount"] = discAmount + " MMK"
            }
            if let billAmount = helper.billAmount {
                pdfDictionary["billAmount"] = billAmount + " MMK"
            }
            pdfDictionary["companyName"] = helper.companyName
            pdfDictionary["productName"] = helper.productName
            pdfDictionary["buyingQuantity"] = helper.buyingQuantity
            pdfDictionary["freeQuantity"] = helper.freeQuantity
            helper = nil
        }
        
        guard let pdfUrl = CodeSnippets.createInvoicePDF(dictValues: pdfDictionary, pdfFile: "OK$ Transaction receipt") else {
            println_debug("Error - pdf not generated")
            return
        }
        let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
    
    //MARK: - Methods
    private func loadData() {
        getName()
        if let com = transactionDetail?.rawDesc, com.contains(find: "#OK-PMC") {
            getSenderReceiverNo(comment: com)
        } else {
            getPhNumber()
        }
        getTypes()
        getOperator()
        if let com = transactionDetail?.rawDesc, com.contains(find: "#OK-PMC") {
            getPromocodeDetails(comment: com)
        } else {
            getAmount()
            getCashBack()
            getBonus()
        }
        getBalance()
        getTransactionID()
        getDate()
        //getTime()
        let _ = getSuccessAndComments()
        self.detailArray.append(Detail(title: "Status", value: "Success"))
        self.tableViewDetail.reloadData()
    }
    
    private func getSenderReceiverNo(comment: String) {
        let helper: OfferCommentHelper! = OfferCommentHelper(comment: comment.removingPercentEncoding)

        if helper.promotionTransactionCount == 1 && helper.paymentType == "1" {
            
            if let transType = transactionDetail?.accTransType, transType == "Dr" {
                //Show tx done from user login
               
                let receiverNo = Detail(title: "Receiver Account Number", value: helper.promoReceiverNumber ?? "")
                self.detailArray.append(receiverNo)
                
                let mobileNo = Detail(title: "Mobile Number", value: CodeSnippets.getMobileNumber(transRecord: transactionDetail))
                self.detailArray.append(mobileNo)
            } else {
                let receiverNo = Detail(title: "Sender Account Number", value: helper.senderCreditNumber ?? "")
                self.detailArray.append(receiverNo)
                
                let mobileNo = Detail(title: "Mobile Number", value: CodeSnippets.getMobileNumber(transRecord: transactionDetail))
                self.detailArray.append(mobileNo)
            }
        } else {
            getPhNumber()
        }
    }
    
    private func getName() {
        /*
        var valueName = "Unknown"
        if let transType = transactionDetail?.accTransType {
            var nameFromType = "Unknown"

            if transType == "Dr" {
                nameFromType = transactionDetail?.receiverName?.replacingOccurrences(of: "-cashin", with: "").replacingOccurrences(of: "-cashout", with: "") ?? ""
                nameFromType = transactionDetail?.receiverName ?? "Unknown"
            } else {
                var value = transactionDetail?.senderName ?? "Unknown"
                value = transactionDetail?.senderName?.replacingOccurrences(of: "-cashin", with: "").replacingOccurrences(of: "-cashout", with: "") ?? ""
                if value.contains("@sender"){
                    let newValue = value.components(separatedBy: "@sender")
                    if newValue.indices.contains(1){
                        let name = newValue[1].components(separatedBy: "@businessname")
                        if name.indices.contains(0){
                            nameFromType = name[0]
                        }
                    }
                }else{
                    nameFromType = transactionDetail?.senderName?.replacingOccurrences(of: "-cashin", with: "").replacingOccurrences(of: "-cashout", with: "") ?? "Unknown"
                }
                
            }
            valueName = nameFromType == "" ? "Unknown" : nameFromType
        }
        */
        let name = Detail(title: "Name", value: strSenderName)
        var businessNameData =  ""
        
        if let trType = transactionDetail?.accTransType {
            if trType == "Dr" {
                if let businessName = transactionDetail?.receiverBName, businessName.count > 0 {
                    businessNameData = businessName
                }
            } else {
                if let businessName = transactionDetail?.senderBName, businessName.count > 0 {
                    businessNameData = businessName
                }
            }
        }
        let dataBName =  Detail(title: "Business Name", value: businessNameData)
        if businessNameData.count > 1 {
        self.detailArray.append(name)
        self.detailArray.append(dataBName)
        } else {
            self.detailArray.append(name)
        }
    }
    
    private func getPhNumber() {
        var phNumber = "-"
        if let transType = transactionDetail?.transType {
            if let acTransType = transactionDetail?.accTransType, acTransType == "Dr" {
                phNumber = CodeSnippets.getMobileNumber(transRecord: transactionDetail)
            } else {
                if transType == "PAYWITHOUT ID" {
                    phNumber = "XXXXXXXXXX"
                } else {
                    phNumber = CodeSnippets.getMobileNumber(transRecord: transactionDetail)
                }
            }
        }
        if transactionDetail?.rawDesc?.contains(find: "ELECTRICITY") ?? false || transactionDetail?.rawDesc?.contains(find: "MPU") ?? false || transactionDetail?.rawDesc?.contains(find: "REFUND") ?? false || transactionDetail?.rawDesc?.contains(find: "POSTPAID") ?? false || transactionDetail?.rawDesc?.contains(find: "MERCHANTPAY") ?? false || transactionDetail?.rawDesc?.contains(find: "LANDLINE") ?? false || transactionDetail?.rawDesc?.contains(find: "SUNKING") ?? false || transactionDetail?.rawDesc?.contains(find: "INSURANCE") ?? false || transactionDetail?.rawDesc?.contains(find: "DTH") ?? false || transactionDetail?.rawDesc?.contains(find: "SEND MONEY TO BANK:") ?? false || transactionDetail?.rawDesc?.contains(find: "Bonus Point Exchange Payment") ?? false || transactionDetail?.rawDesc?.contains(find: "OK Wallet Topup") ?? false {
            phNumber = "XXXXXXXXXX"
        } else if transactionDetail?.destination == nil, let cashback = transactionDetail?.cashBack, cashback > 0 {
            phNumber = "XXXXXXXXXX"
        } else if  transactionDetail?.rawDesc?.contains("#OK") ?? false {
            if  transactionDetail?.rawDesc?.contains(find: "PAYMENTTYPE:GiftCard") ?? false {
                phNumber = "XXXXXXXXXX"
            } else {
                let number = Detail(title: "Mobile Number", value: phNumber)
                self.detailArray.append(number)
            }
        } else if  transactionDetail?.rawDesc?.contains("Bank To Wallet") ?? false {
            phNumber = "XXXXXXXXXX"
        } else if  transactionDetail?.rawDesc?.contains("ProjectId") ?? false {
            if  transactionDetail?.rawDesc?.contains(find: "ProjectId") ?? false {
                //phNumber = "XXXXXXXXXX"
                
                    var mobileNo = "XXXXXXXXXX"
                    let value = transactionDetail?.rawDesc ?? ""
                    if value.contains(find: "ProjectId"){
                        let newValue = value.components(separatedBy: " Wallet Number :")
                        if newValue.count > 2 {
                            if newValue.indices.contains(1){
                                let name = newValue[2].components(separatedBy: "ProjectId")
                                if name.indices.contains(0){
                                    mobileNo = "(+95)".appending(name[0])
                                }
                            }
                        }  else {
                            let arrVal = value.components(separatedBy: "Account Number :")
                            if arrVal.indices.contains(1){
                                let name = arrVal[1].components(separatedBy: "ProjectId")
                                if name.indices.contains(0){
                                    mobileNo = "(+95)".appending(name[0])
                                }
                            }
                        }
                    }
                let number = Detail(title: "Mobile Number", value: mobileNo)
                self.detailArray.append(number)
            }
        } else if  transactionDetail?.rawDesc?.contains("#Tax Code") ?? false {
            if  transactionDetail?.rawDesc?.contains(find: "#Tax Code") ?? false {
                phNumber = "XXXXXXXXXX"
            }
        }
        else {
            if (transactionDetail?.transType ?? "") == "PAYWITHOUT ID" && (transactionDetail?.accTransType ?? "") == "Cr"{
                phNumber = "XXXXXXXXXX"
            }
            let number = Detail(title: "Mobile Number", value: phNumber)
            self.detailArray.append(number)
        }
        
    }
    
    private func getTypes() {
        var transType = ""
        var paymentType = ""
        if let desc = transactionDetail?.rawDesc {
            if desc.contains("#OK") || desc.contains(find: "OOK") {
                if desc.contains("Top-Up") || desc.contains("TopUp") || desc.contains("Bonus Point Top Up") || desc.contains("TopUpPlan"){
                    transType = "Top-Up"
                } else if desc.contains("Data Plan") || desc.contains("DataPlan"){
                    transType = "Data Plan"
                } else if desc.contains("MPU Card") {
                    transType = "MPU"
                } else if desc.contains("VISA") {
                    transType = "VISA"
                } else if desc.contains(find: "PAYMENTTYPE:GiftCard") {
                    transType = "Gift Card"
                } else if transactionDetail?.rawDesc?.contains(find: "ELECTRICITY") ?? false {
                    transType = "ELECTRICITY"
                }  else if transactionDetail?.rawDesc?.contains(find: "POSTPAID") ?? false {
                    transType = "POSTPAID"
                } else if  transactionDetail?.rawDesc?.contains("MERCHANTPAY") ?? false {
                    transType = "MERCHANTPAY"
                } else if  transactionDetail?.rawDesc?.contains("LANDLINE") ?? false {
                    transType = "LANDLINE"
                } else if  transactionDetail?.rawDesc?.contains("INSURANCE") ?? false {
                    transType = "INSURANCE"
                } else if  transactionDetail?.rawDesc?.contains("SUNKING") ?? false {
                    transType = "SUNKING"
                } else if let desc = transactionDetail?.rawDesc, desc.contains(find: "DTH") {
                    transType = "DTH"
                } else {
                    transType = "Special Offers"
                }
            } else {
                if let transTyp = transactionDetail?.transType {
                    transType = transTyp
                    if transType == "PAYWITHOUT ID" {
                        transType = "Hide Number to Pay"
                        if transactionDetail?.rawDesc?.contains(find: "#Tax Code") ?? false {
                            transType = "Tax Payment"
                        }
                    } else if transType == "TOLL" {
                        transType = "TOLL"
                    } else if transType == "TICKET" {
                        transType = "TICKET"
                    } else {
                        transType = "PAYTO"
                        if transactionDetail?.rawDesc?.contains(find: "REFUND") ?? false {
                            transType = "Refund"
                        } else if transactionDetail?.destination == nil, let cashback = transactionDetail?.cashBack, cashback > 0 {
                            transType = "Cash Back"
                        } else if let desc = transactionDetail?.rawDesc, desc.contains(find: "SEND MONEY TO BANK:") {
                            transType = "SEND MONEY TO BANK"
                        } else if let desc = transactionDetail?.rawDesc, desc.contains(find: "DTH") {
                            transType = "DTH"
                        } else if desc.contains(find: "-cashin") {
                            paymentType = "Cash In"
                        } else if desc.contains(find: "-cashout") {
                            paymentType = "Cash Out"
                        } else if desc.contains(find: "-transferto") {
                            paymentType = "Transfer To"
                        }
                    }
                }
            }
        }
        if paymentType.count > 0 {
            self.detailArray.append(Detail(title: "Payment Type", value: paymentType))
        }
        if transType.count > 0 {
            self.detailArray.append(Detail(title: "Transaction Type", value: transType))
        }
        
    }
    
    private func getOperator() {
        if let desc = transactionDetail?.rawDesc {
            if desc.contains("#OK") || desc.contains(find: "OOK") {
                if desc.contains("Top-Up") || desc.contains("TopUp") || desc.contains("Bonus Point Top Up") || desc.contains("TopUpPlan") ||  desc.contains("Data Plan") || desc.contains("DataPlan") || desc.contains("Special Offers") || desc.contains("Special Offers") {
                    var operatorName = ""
                    if let mobileNumber = transactionDetail?.destination {
                        if let rawDes = transactionDetail?.rawDesc, rawDes.contains(find: "Overseas") {
                            let desArray = rawDes.components(separatedBy: "-")
                            if desArray.count > 2 {
                                operatorName = desArray[2]
                            }
                        } else {
                            if mobileNumber.hasPrefix("0095") {
                                let validations = PayToValidations().getNumberRangeValidation("0" + mobileNumber.dropFirst(4))
                                operatorName = validations.operator
                            } else {
                                let validations = PayToValidations().getNumberRangeValidation(mobileNumber)
                                operatorName = validations.operator
                            }
                        }
                        if operatorName.count > 0 {
                            self.detailArray.append(Detail(title: "Operator", value: operatorName))
                        }
                    }
                }
            }
        }
    }
    
    private func getAmount() {
        if let amount = transactionDetail?.amount {
            let amount = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: amount.toString()) + " MMK"
            if amount.count > 0 {
                self.detailArray.append(Detail(title: "Amount", value: amount))
            }
        }
    }
    
    private func getCashBack() {
        if let cb = transactionDetail?.cashBack, let bal = transactionDetail?.walletBalance, bal != 0 {
            let cashBack = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: cb.toString()) + " MMK"
            if cashBack.count > 0 {
                self.detailArray.append(Detail(title: "Cash Back", value: cashBack))
            }
        }
    }
    
    private func getBonus() {
        if let bonus = transactionDetail?.bonus {
            let bonus = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: bonus.toString())
            if bonus.count > 0 {
                self.detailArray.append(Detail(title: "Bonus Points", value: bonus))
            }
        }
    }
    
    private func getBalance() {
        if let wBalance = transactionDetail?.walletBalance, wBalance != 0 {
            let balance = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: wBalance.toString()) + " MMK"
            if balance.count > 0 {
                self.detailArray.append(Detail(title: "Balance", value: balance))
            }
        } else {
            let transAmt = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: UserLogin.shared.walletBal) + " MMK"
            self.detailArray.append(Detail(title: "Amount", value: transAmt))
        }
    }
    
    private func getTransactionID() {
        if let transID = transactionDetail?.transID {
            self.detailArray.append(Detail(title: "Transaction ID", value: transID))
        }
    }
    
    private func getDate() {
        if let date = transactionDetail?.transactionDate {
            let dateStr = date.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss")
            self.detailArray.append(Detail(title: "Date & Time", value: dateStr))
        }
    }
    
    private func getTime() {
        if let date = transactionDetail?.transactionDate {
            let timeStr = date.stringValue(dateFormatIs: "h:mm a")
            self.detailArray.append(Detail(title: "Time", value: timeStr))
        }
    }
    
    private func getSuccessAndComments(comment: Bool = false) -> String? {
//        self.detailArray.append(Detail(title: "Status", value: "Success"))

        var descWithNoComa = ""
        
        let desc = transactionDetail?.rawDesc?.removingPercentEncoding?.replacingOccurrences(of: "+", with: " ").components(separatedBy: "-")
        if transactionDetail?.rawDesc?.contains(find: "ELECTRICITY") ?? false {
//            var payType = "ELECTRICITY"
            let dateFormat = self.formatDate(dateString: desc?.last ?? "")
            var name: [String]?
            if desc?[9].contains(find: "Name :") ?? false {
                name = desc?[9].components(separatedBy: "Name :")
            } else if desc?[10].contains(find: "Name :") ?? false {
                name = desc?[10].components(separatedBy: "Name :")
            }
            var diviLoc = ""
            if desc?[7].contains(find: "Division") ?? false {
                let division = desc?[7].components(separatedBy: "Division :")
                if let divLocal = division?[1] {
                    let arr = divLocal.components(separatedBy: "Division")
                    diviLoc = arr[0]
                }
            } else if desc?[8].contains(find: "Division") ?? false {
                let division = desc?[8].components(separatedBy: "Division :")
                if let divLocal = division?[1] {
                    let arr = divLocal.components(separatedBy: "Division")
                    diviLoc = arr[0]
                }
            }
            
            var township: [String]?
            var meterNo = ""
            if desc?[8].contains(find: "Township") ?? false {
                township = desc?[8].components(separatedBy: "Township")
            } else if desc?[9].contains(find: "Township") ?? false {
                township = desc?[9].components(separatedBy: "Township")
            }
            var payFor: [String]?
            if desc?[6].contains(find: "PayFor:") ?? false {
                meterNo = desc?[2] ?? ""
                payFor = desc?[6].components(separatedBy: "PayFor:")
            } else if desc?[7].contains(find: "PayFor:") ?? false {
                meterNo = "\(desc?[2] ?? "")-\(desc?[3] ?? "")"
                payFor = desc?[7].components(separatedBy: "PayFor:")
            }
            let fullDesc = "\(meterNo) \(name?[1] ?? "") \(township?[1] ?? "") Township \(diviLoc) \(diviLoc) Division Bill Month: \(dateFormat.0) Due Date \(dateFormat.1) \(payFor?[1] ?? "")"
            descWithNoComa = fullDesc.replacingOccurrences(of: ",", with: "")
            self.detailArray.append(Detail(title: "Bill Month", value: dateFormat.0))
            self.detailArray.append(Detail(title: "Due Date", value: dateFormat.1))
        } else if let desc = transactionDetail?.rawDesc, desc.contains(find: "SEND MONEY TO BANK:") {
            if let array = transactionDetail?.rawDesc?.components(separatedBy: "SEND MONEY TO BANK:"), array.count > 0 {
                descWithNoComa = "SEND MONEY TO BANK: \(array[1])"
            }
        } else if let desc = transactionDetail?.rawDesc, desc.contains(find: "ProjectId"){
            descWithNoComa = transactionDetail?.rawDesc?.components(separatedBy: "ProjectId").first ?? ""
        } else if let desc = transactionDetail?.rawDesc, desc.contains(find: "#Tax Code"){
            let tmpDesc = transactionDetail?.rawDesc?.replacingOccurrences(of: "#", with: "") ?? ""
            descWithNoComa = tmpDesc.replacingOccurrences(of: "Tax Code :", with: "Tax Payment - IRD Transaction ID :")
        } else if let desc = transactionDetail?.rawDesc, desc.contains(find: "Overseas"){
            //descWithNoComa = transactionDetail?.rawDesc?.components(separatedBy: "ProjectId").first ?? ""
            let arrData = desc.components(separatedBy: "-")
            if arrData.count > 9 {
            if desc.contains(find: "TopUpPlan") {
                descWithNoComa = "Topup : " + arrData[5] + " " + arrData[8]
            } else {
                descWithNoComa = "Data Plan : " + arrData[5] + " " + arrData[8]
            }
            } else {
                if desc.contains(find: "TopUpPlan") {
                    descWithNoComa = "TopUp : " + arrData[5]
                } else {
                    descWithNoComa = "Data Plan : " + arrData[5]
                }
            }
        }
        else {
            if let com = transactionDetail?.rawDesc, !com.contains(find: "#OK-PMC") {
                if let commentsToShow = transactionDetail?.desc {
                    if commentsToShow.hasPrefix("Remarks: ") {
                        descWithNoComa = "\(commentsToShow.substring(from: 9))"
                    } else {
                        descWithNoComa = commentsToShow
                    }
                }
            }
//            if remarks.count > 0 {
//                self.detailArray.append(Detail(title: "Remarks", value: remarks))
//            }
//            descWithNoComa = transactionDetail?.desc?.replacingOccurrences(of: ",", with: "") ?? ""
        }
        if comment {
            return descWithNoComa
        } else {
            if descWithNoComa.count > 0 {
                self.detailArray.append(Detail(title: "Remarks", value: descWithNoComa))
            }
            return ""
        }
    }
    
    func formatDate(dateString: String) -> (String, String) {
        let dateStr = dateString.components(separatedBy: "Due Date :")
        if dateStr.count > 0 {
            let currentDate = dateStr[1].replacingOccurrences(of: "#", with: "")
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy hh:mm:ss a"
            let date = formatter.date(from: currentDate)
            let previousMonth = Calendar.current.date(byAdding: .month, value: -1, to: date ?? Date())
            formatter.dateFormat = "EEE, dd-MMM-yyyy"
            let strDate = formatter.string(from: date ?? Date())
            formatter.dateFormat = "MMMM"
            let prevMonth = formatter.string(from: previousMonth ?? Date())
            return(prevMonth, strDate)
        }
        return ("", "")
    }
    
    private func getPromocodeDetails(comment: String) {
        let offerComments = OfferCommentHelper(comment: comment)
        if let billAmount = offerComments.billAmount, billAmount.count > 0 {
            self.detailArray.append(Detail(title: "Bill Amount", value: billAmount))
        }
        if let discAmount = offerComments.discountAmount, discAmount.count > 0 {
              let amont = wrapAmountWithCommaDecimal(key: "\(discAmount)")
            self.detailArray.append(Detail(title: "Discount Amount", value: amont))
        }
        if let discountPercent = offerComments.discountPercent, discountPercent.count > 0 {
            self.detailArray.append(Detail(title: "Discount Percentage", value: discountPercent))
        }
        if let netAmount = offerComments.netAmount, netAmount.count > 0 {
            self.detailArray.append(Detail(title: "Net Amount", value: netAmount))
        }
        if let saleQ = offerComments.buyingQuantity, saleQ.count > 0 {
            self.detailArray.append(Detail(title: "Sale Quantity", value: saleQ))
        }
        if let freeQ = offerComments.freeQuantity, freeQ.count > 0 {
            self.detailArray.append(Detail(title: "Free Quantity", value: freeQ))
        }
        if let promoCode = offerComments.promoCode, promoCode.count > 0 {
            self.detailArray.append(Detail(title: "Promotion Code", value: promoCode))
        }
    }
    
    func getQRCodeImage(qrData: ReportTransactionRecord) -> UIImage? {
        let phNumber = qrData.destination ?? "0000000000"
        var transDateStr = ""
        
        let dFormatter = DateFormatter()
        dFormatter.calendar = Calendar(identifier: .gregorian)
        dFormatter.dateFormat = "dd/MMM/yyyy HH:mm:ss"
        if let trDate = qrData.transactionDate {
            transDateStr = dFormatter.string(from: trDate)
        }
        var senderName = ""
        var senderNumber = ""
        var receiverName = ""
        var receiverBusinessName = ""
        var receiverNumber = ""
        if let sName = qrData.senderName {
            senderName = sName
            if senderName.count == 0 {
                senderName = UserModel.shared.name
            }
        }
        if let rName = qrData.receiverName{
            receiverName = rName
        }
        if let businessName = qrData.receiverBName, businessName.count > 0 {
            receiverBusinessName = businessName
        }
        if let accTransType = qrData.accTransType {
            if accTransType == "Cr" {
                if let dest = qrData.destination {
                    senderNumber = dest
                }
                receiverNumber = UserModel.shared.mobileNo
                if let transType = qrData.transType {
                    if transType == "PAYWITHOUT ID" {
                        senderNumber = "XXXXXXXXXX"
                    }
                }
            } else {
                if let dest = qrData.destination {
                    receiverNumber = dest
                }
                senderNumber = UserModel.shared.mobileNo
            }
        }
        let firstPartToEncrypt = "\(transDateStr)----\(phNumber)"
        let transID = qrData.transID ?? ""
        //let operatorName = qrData.operatorName ?? ""
        let trasnstype = qrData.transType ?? ""
        var gender = ""
        if let safeGenger = qrData.gender {
            gender = safeGenger == "1" ? "M" : "F"
        }
        let age = qrData.age > 0 ? "\(qrData.age)" : ""
        //let description = qrData.desc ?? ""
        let lattitude = qrData.latitude ?? ""
        let longitude = qrData.longitude ?? ""
        
        let firstPartEncrypted = AESCrypt.encrypt(firstPartToEncrypt, password: "m2n1shlko@$p##d")
        
        var balance = ""
        if let balanceData = qrData.walletBalance {
            balance = balanceData.toString()
        }
        var cashBackAmount = ""
        if let cashBack = qrData.cashBack {
            cashBackAmount = cashBack.toString()
        }
        var amount = ""
        if let amountData = qrData.amount {
            amount = amountData.toString()
        }
        var bonus = ""
        if let bonusData = qrData.bonus {
            bonus = bonusData.toString()
        }
        let state = UserModel.shared.state
        
        let str2 = "OK-\(senderName)-\(amount)-\(transDateStr)-\(transID)-\(senderNumber)-\(balance)-des-\(receiverNumber)-\(trasnstype)-\(cashBackAmount)-\(bonus)-\(lattitude),\(longitude)-0-\(gender)-\(age)-\(state)-\(receiverName)-\(receiverBusinessName)"
        
        let secondEncrypted = AESCrypt.encrypt(str2, password: firstPartToEncrypt)
        
        var firstPartEncryptedSafeValue = ""
        var secondEncryptedSafeValue = ""
        if let firstEncrypted = firstPartEncrypted {
            firstPartEncryptedSafeValue = firstEncrypted
        }
        if let secondEncrypt = secondEncrypted {
            secondEncryptedSafeValue = secondEncrypt
        }
        let finalEncyptFormation = "\(firstPartEncryptedSafeValue)---\(secondEncryptedSafeValue)"
        
        let qrImageObject = PTQRGenerator()
        guard let qrImage =  qrImageObject.getQRImage(stringQR: finalEncyptFormation, withSize: 10) else { return nil }
        
        return qrImage
    }
}

// MARK: - Additional Methods
extension ReceiptDetailListViewController: NavigationSetUpProtocol {
    /// It set up the navigation bar
    func setUpNavigation() {
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        
        let shareButton = UIButton(type: .custom)
        shareButton.setImage(#imageLiteral(resourceName: "reportShare").withRenderingMode(.alwaysOriginal), for: .normal)
        shareButton.addTarget(self, action: #selector(showShareOption), for: .touchUpInside)
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "tabBarBack").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        
        let ypos = (titleViewSize.height - 30 ) / 2
        if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
            shareButton.frame = CGRect(x: navTitleView.frame.size.width - 100, y: ypos, width: 30, height: 30)
        }else{
           shareButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
        }
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text = ConstantsController.receiptDetailListViewController.screenTitle.localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 23) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        navTitleView.addSubview(shareButton)
        self.navigationItem.titleView = navTitleView
    }
}

//MARK: - UITableViewDataSource, UITableViewDelegate
extension ReceiptDetailListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detailArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiptDetailCell", for: indexPath) as? ReceiptDetailCell else { return ReceiptDetailCell() }
        let detail = detailArray[indexPath.row]
        cell.configure(title: detail.title, value: detail.value)
        return cell
    }
}

//MARK: - UITableViewCell
class ReceiptDetailCell: UITableViewCell {
    //MARK: - Outlets
    @IBOutlet weak var lblTitle: UILabel!{
        didSet{
            lblTitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblValue: UILabel!{
        didSet{
            lblValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    //MARK: - Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(title: String, value: String) {
        self.lblTitle.text = title.localized
        self.lblValue.text = value
    }
}
