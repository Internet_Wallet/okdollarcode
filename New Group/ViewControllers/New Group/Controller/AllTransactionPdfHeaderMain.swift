//
//  AllTransactionPdfHeader.swift
//  OK
//
//  Created by ANTONY on 24/02/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class AllTransactionPdfHeaderMain: UIView {
    // MARK: - Outlets
    
    @IBOutlet weak var businessNameHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var accName: UILabel!{
        didSet{
            self.accName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var accNumber: UILabel!{
        didSet{
            self.accNumber.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var email: UILabel!{
        didSet{
            self.email.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var address: UILabel!{
        didSet{
            self.address.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var period: UILabel!{
        didSet{
            self.period.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var creditAmount: UILabel!{
        didSet{
            self.creditAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var debitAmountTextLbl: UILabel!{
        didSet{
            self.debitAmountTextLbl.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var debitAmount: UILabel!{
        didSet{
            self.debitAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var totalTransaction: UILabel!{
        didSet{
            self.totalTransaction.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var balance: UILabel!{
        didSet{
            self.balance.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var dateTime: UILabel!{
        didSet{
            self.dateTime.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var totalTransactionAmount: UILabel!{
        didSet{
            self.totalTransactionAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterType: UILabel!{
        didSet{
            self.filterType.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterAmount: UILabel!{
        didSet{
            self.filterAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterGender: UILabel!{
        didSet{
            self.filterGender.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterAge: UILabel!{
        didSet{
            self.filterAge.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterPeriod: UILabel!{
        didSet{
            self.filterPeriod.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterTypeBillPayment: UILabel!{
        didSet{
            self.filterTypeBillPayment.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterAmountBillPayment: UILabel!{
        didSet{
            self.filterAmountBillPayment.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterBillTypeBillPayment: UILabel!{
        didSet{
            self.filterBillTypeBillPayment.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterPeriodBillPayment: UILabel!{
        didSet{
            self.filterPeriodBillPayment.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterTypeTopUp: UILabel!{
        didSet{
            self.filterTypeTopUp.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterAmountTopUp: UILabel!{
        didSet{
            self.filterAmountTopUp.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterOperatorTopUp: UILabel!{
        didSet{
            self.filterOperatorTopUp.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterPlanTypeTopUp: UILabel!{
        didSet{
            self.filterPlanTypeTopUp.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterPeriodTopUp: UILabel!{
        didSet{
            self.filterPeriodTopUp.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterTypeReceivedMoney: UILabel!{
        didSet{
            self.filterTypeReceivedMoney.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterAmountReceivedMoney: UILabel!{
        didSet{
            self.filterAmountReceivedMoney.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterPeriodReceivedMoney: UILabel!{
        didSet{
            self.filterPeriodReceivedMoney.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var pdfHeaderLabel: UILabel!{
        didSet{
            self.pdfHeaderLabel.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var viewHeaderForTransaction: UIView!

    @IBOutlet weak var sideHeader: UILabel!{
        didSet{
            self.sideHeader.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var creditLblAmount: UILabel!{
        didSet{
            self.creditLblAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    @IBOutlet weak var cashBackLblAmount: UILabel! {
        didSet{
            self.cashBackLblAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var cashBackLbl: UILabel!{
        didSet{
            self.cashBackLbl.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var cashBackLblColon: UILabel!{
        didSet{
            self.cashBackLblColon.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    @IBOutlet weak var businessNameLbl: UILabel!{
        didSet{
            self.businessNameLbl.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var businessNameVal: UILabel!{
        didSet{
            self.businessNameVal.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var accountTypeLbl: UILabel!{
        didSet{
            self.accountTypeLbl.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var accountTypeVal: UILabel!{
        didSet{
            self.accountTypeVal.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var addressHeight: NSLayoutConstraint!
    var enableTransactionMode: Bool {
        get {
            return !viewHeaderForTransaction.isHidden
        }
        set(newValue) {
            if newValue {
                viewHeaderForTransaction.isHidden = false
            } else {
                viewHeaderForTransaction.isHidden = true
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    func fillTransactionDetails(list: [ReportTransactionRecord], dateOne: String, dateTwo: String, currentBalance: String, transType: String, amountRange: String,
                     gender: String, age: String, periodStr: String) {
       
        self.enableTransactionMode = true
        self.getCreditAndDebit(list: list)
        self.getGeneralDetails()
        self.getPeriodInFormat(periodStr: periodStr, dateOne: dateOne, dateTwo: dateTwo)
        
        let balance : Double = (UserLogin.shared.walletBal as NSString).doubleValue
        let doubleStrBalance = String(format: "%.2f", balance)
        self.balance.text = wrapAmountWithCommaDecimal(key: doubleStrBalance) + " MMK"
        var titleStr = ""
        if transType == "Transaction".localized {
            titleStr = "All"
        } else {
            titleStr = transType
        }
        self.filterType.text = titleStr
        if amountRange == "Amount".localized {
            titleStr = "All"
        } else {
            titleStr = amountRange
        }
        self.filterAmount.text = titleStr
        if gender == "Gender".localized {
            titleStr = "All"
        } else {
            titleStr = gender
        }
        self.filterGender.text = titleStr
        if age == "Age".localized {
            titleStr = "All"
        } else {
            titleStr = age
        }
        self.filterAge.text = titleStr
        if periodStr == "Period".localized {
            titleStr = "All"
        } else {
            titleStr = periodStr
        }
        self.filterPeriod.text = titleStr
    }
    
    
    func getCreditAndDebit(list: [ReportTransactionRecord], fromBill: Bool = false) {
        var creditAmount: NSDecimalNumber = 0
        var debitAmount: NSDecimalNumber = 0
        var cashBackAmount: NSDecimalNumber = 0
        var index = 0
        for record in list {
            if fromBill {
                if record.accTransType == "Dr" {
                    if let transAmount = record.amount {
                        debitAmount = debitAmount.adding(transAmount)
                    }
                    if let cashback = record.cashBack {
                        creditAmount = creditAmount.adding(cashback)
                    }
                } else {
                    if let transAmount = record.amount {
                        debitAmount = debitAmount.adding(transAmount)
                    }
                    if let cashback = record.cashBack {
                        creditAmount = creditAmount.adding(cashback)
                    }
                }
                index += 1
            } else {
                if record.accTransType == "Cr" {
                    if let transAmount = record.amount {
                        creditAmount = creditAmount.adding(transAmount)
                    }
                } else if record.accTransType == "Dr" {
                    if let transAmount = record.amount {
                        debitAmount = debitAmount.adding(transAmount)
                    }
                }
                index += 1
            }
            if index == 49 {
                break
            }
            
            if let cashBack = record.cashBack {
                cashBackAmount = cashBackAmount.adding(cashBack)
            }
        }
        self.totalTransaction.text = "\((list.count))"
        self.creditAmount.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(creditAmount)") + " MMK".localized
        self.debitAmount.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(debitAmount)") + " MMK".localized
        let totalAmount = creditAmount.adding(debitAmount)
        self.totalTransactionAmount.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(totalAmount)") + " MMK".localized
        self.cashBackLblAmount.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(cashBackAmount)") + " MMK".localized
    }
    
    func getPeriodInFormat(periodStr: String, dateOne: String, dateTwo: String) {
        var period = ""
        switch periodStr {
        case "Period".localized, "All".localized:
            period = "All".localized
        case "Date".localized:
            if let fromDate = dateOne.dateValue(dateFormatIs: "dd MMM yyyy") {
                let fDate = fromDate.stringValue(dateFormatIs: "dd-MMM-yyyy")
                if let toDate = dateTwo.dateValue(dateFormatIs: "dd MMM yyyy") {
                    let tDate = toDate.stringValue(dateFormatIs: "dd-MMM-yyyy")
                    period = "\(fDate) to \(tDate)"
                }
            }
        case "Month".localized:
            if let month = dateTwo.dateValue(dateFormatIs: "MMM yyyy") {
                let mon = month.stringValue(dateFormatIs: "MMM-yyyy")
                period = "\(mon)"
            }
        default:
            break
        }
        self.period.text = period
    }
    
    private func getDivision() -> String {
        return  getDivisionAndTownShipForReport(divisionCode: UserModel.shared.state, townshipCode: UserModel.shared.township)
    }
    
    func getGeneralDetails() {
        self.accName.text = "\(UserModel.shared.name)"
        if UserModel.shared.agentType == .user {
            businessNameHeightConstraint.constant = 0
        } else {
            businessNameHeightConstraint.constant = 21
            self.businessNameVal.text = (UserModel.shared.businessName != "") ? UserModel.shared.businessName : "-"
        }
        let accountType = self.agentServiceTypeString(type: UserModel.shared.agentType)
        var phNumber = UserModel.shared.mobileNo.replaceFirstTwoChar(withStr: "+")
        phNumber.insert(")", at: phNumber.index(phNumber.startIndex, offsetBy: 3))
        phNumber.insert("(", at: phNumber.startIndex)
        if UserModel.shared.mobileNo.hasPrefix("0095") {
           phNumber.insert("0", at: phNumber.index(phNumber.startIndex, offsetBy: 5))
        }
        self.accNumber.text = "\(phNumber)"
        self.email.text = "\(UserModel.shared.email)".components(separatedBy: ",").first ?? ""
        let division = self.getDivision().components(separatedBy: ",")
        var div = ""
        if division.count > 0 {
            div = division.last?.components(separatedBy: "Division").first ?? ""
        }
        let townShip = "Township"
        if UserModel.shared.language != "en" {
            self.accountTypeVal.text = getBurmeseAgentType()
            self.address.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? ""), \(division.last ?? "")".replacingOccurrences(of: ", ,", with: ",")
        } else {
            self.accountTypeVal.text = accountType
            self.address.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? "") \(townShip), \(div), \(division.last ?? "")".replacingOccurrences(of: ", ,", with: ",")
            self.addressHeight.constant = 65.0
            self.layoutIfNeeded()
        }
//        self.address.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? "") \(townShip), \(div), \(division.last ?? ""), \(UserModel.shared.country)".replacingOccurrences(of: ", ,", with: ",")
        self.dateTime.text = Date().stringValueWithOutUTC(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss")
    }
    
    private func agentServiceTypeString(type: AgentType) -> String {
        switch type {
        case .user:
            return "Personal"
        case .merchant:
            return "Merchant"
        case .agent:
            return "Agent"
        case .advancemerchant:
            return "Advance Merchant"
        case .dummymerchant:
            return "Safety Cashier"
        case .oneStopMart:
            return "One Stop Mart"
        }
    }
}
