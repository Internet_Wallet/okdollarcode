//
//  ReportCompanyViewController.swift
//  OK
//
//  Created by gauri OK$ on 2/2/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol ReportCompanyDelegate {
    func processWithCategoryID(categoryID : String , categoryName : String )
}

class ReportCompanyViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var tblCompanyList: UITableView!
    @IBOutlet weak var viewShadow: UIView!
    let defaults = UserDefaults.standard

    var companyArray = [OffersCategoryModel]()
    var delegate: ReportCompanyDelegate?
    enum ViewMode {
        case landscape, portrait
    }
    var viewOrientation = ViewMode.landscape

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tblCompanyList.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initialSetup()
        self.setUpNavigation()
    }

    //MARK:- InitialSetup
    private func initialSetup() {
        OffersManager.getHeadersTitle { [weak self](collections) in
            if collections == nil { return }
            if collections!.count <= 0 { return }
            println_debug(collections)
            
            if collections!.count > 0
            {
//                let categoryModel = OffersCategoryModel.init(categoryID: "", categoryName: "All".localized, categoryDescription: "", categoryBName: "", categoryImage: "", isActive: true)
//                self?.companyArray.append(categoryModel)
                self?.companyArray = collections!
                self?.tblCompanyList.reloadData()
            }
        }
    }
    
    @IBAction func cancelController() {
        self.navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ReportCompanyViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if companyArray.count == 0 {
            let noDataLabel = UILabel(frame: tableView.bounds)
            noDataLabel.text = "No Records".localized
            noDataLabel.textAlignment = .center
            noDataLabel.font = UIFont(name: appFont, size: 18)
            tableView.backgroundView = noDataLabel
        } else {
            tableView.backgroundView = nil
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return companyArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "filterSubCell", for: indexPath) as? filterByNeartableViewnewCell
        
        if indexPath.row == 0 {
            if defaults.value(forKey: "SelectedCategory") != nil {
                
                let str = defaults.value(forKey: "SelectedCategory") as! String
                if  str == "All" {
                    cell?.cellSubTitle.text = "All".localized
                    cell?.cellSubImageView.image = UIImage(named: "success")
                    
                } else {
                    cell?.cellSubTitle.text = "All".localized
                    cell?.cellSubImageView.image = UIImage(named: "in_active")
                    
                }
            } else {
                cell?.cellSubTitle.text = "All".localized
                cell?.cellSubImageView.image = UIImage(named: "success")
                
            }
        } else {
            
            let modelData = companyArray[indexPath.row - 1]
            
            if defaults.value(forKey: "SelectedCategory") != nil {
                
                let str = defaults.value(forKey: "SelectedCategory") as! String
                if  str == modelData.categoryName {
                    cell?.cellSubTitle.text = modelData.categoryName
                    cell?.cellSubImageView.image = UIImage(named: "success")
                    
                } else {
                    cell?.cellSubTitle.text = modelData.categoryName
                    cell?.cellSubImageView.image = UIImage(named: "in_active")
                }
                
            } else {
                
                cell?.cellSubTitle.text = modelData.categoryName
                cell?.cellSubImageView.image = UIImage(named: "in_active")
            }
            
            
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            delegate?.processWithCategoryID(categoryID : "All" , categoryName : "All".localized)
            UserDefaults.standard.set("All", forKey: "SelectedCategory")
            self.navigationController?.popViewController(animated: true)
        } else {
            let modelData = companyArray[indexPath.row - 1]
            delegate?.processWithCategoryID(categoryID : modelData.categoryID , categoryName : modelData.categoryName)
            defaults.set(modelData.categoryName, forKey: "SelectedCategory")
            self.navigationController?.popViewController(animated: true)
        }
    }
}



// MARK: - Additional Methods
extension ReportCompanyViewController: NavigationSetUpProtocol {
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  "Select Company".localized
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
}

class filterByNeartableViewnewCell: UITableViewCell {
    @IBOutlet weak var cellSubTitle: UILabel!
    @IBOutlet weak var cellSubImageView: UIImageView!
    /* func wrapData(titleStr: String, imageName: String) {
     self.cellSubTitle.text = titleStr
     self.cellSubImageView.image = UIImage(named: imageName)
     } */
}
