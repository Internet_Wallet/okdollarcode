//
//  ReportPromotionLocationFilterController.swift
//  OK
//
//  Created by E J ANTONY on 12/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
protocol PromotionLocationFilterDelegate {
    func assignPromoLocoFilter(filterType: ReportOfferFilterOptions, value: String)
}
class ReportPromotionLocationFilterController: UIViewController  {
    //MARK: - Outlet
    @IBOutlet weak var tableFilter: UITableView!
    @IBOutlet weak var constraintBTable: NSLayoutConstraint!
    
    // MARK: - Properties
    enum ScreenType: String {
        case promotionCode = "Promotion Code", location = "Location" , Nonofferlocation = "Select Location"
    }
    private lazy var modelAReport: ReportAModel = {
        return ReportAModel()
    }()
    
    private lazy var modelBReport: ReportBModel = {
        return ReportBModel()
    }()
    
    private var isDataRequested = false
    private var cellIdentifier = "ReportPromotionLocationFilterCell"
    private lazy var searchBar = UISearchBar()
    private let refreshControl = UIRefreshControl()
    var promoID = ""
    var screenName = ScreenType.promotionCode
    var delegate: PromotionLocationFilterDelegate?
    var offerFilterInitialResult = [OfferReportList]()
    var offerList = [OfferReportList]()
    var initialLocationList = [OfferLocationList]()
    var locationList = [OfferLocationList]()
    var initialNonofferLocationList = [NonOfferAllLocationModel]()
    var NonofferlocationList = [NonOfferAllLocationModel]()
    struct FilterPromotionData {
        var code: String
        var date: String
    }
    
    //MARK: - Views Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        tableFilter.tableFooterView = UIView()
        tableFilter.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshTableData), for: .valueChanged)
        registerKeyboardNotification()
        modelAReport.reportAModelDelegate = self
        offerList = offerFilterInitialResult
        setUpSearchBar()
        setUpNavigation()
        loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Methods
    private func registerKeyboardNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func loadData() {
        switch screenName {
        case .location:
            let request = OfferListFilterRequest(mobileNumber: UserModel.shared.mobileNo, promotionId: promoID, date: nil, townshipId: nil, advMerchantNo: nil, offSetVal: "0", dummyMerchantNo: nil, limitVal: "50")
            modelAReport.getOfferListLocation(request: request)
           
        case .Nonofferlocation:
            let request = NonOfferAllTransactionRequest(mobileNumber: UserModel.shared.mobileNo)
            modelBReport.reportBAllTransactionDelegate = self as ReportBModelAllTransaction
            modelBReport.getNonOfferPromotionAllLocationAPICalling(request: request)

        default:
            break
        }
    }
    
    private func setUpSearchBar() {
        searchBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 44)
        if #available(iOS 11.0, *) {
            searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        searchBar.tintColor = UIColor.white
        searchBar.placeholder = "Search".localized
        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
            searchTextField.keyboardType = .asciiCapable
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
                searchTextField.keyboardType = .asciiCapable
            }
        }
        let view = self.searchBar.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                subView.tintColor = UIColor.gray
            }
        }
    }
    
    private func resetScreen() {
        switch screenName {
        case .promotionCode:
            offerList = offerFilterInitialResult
        case .location:
            locationList = initialLocationList
        case .Nonofferlocation:
            NonofferlocationList = initialNonofferLocationList

        }
        setUpNavigation()
        tableFilter.reloadData()
    }
    
    private func applySearch(with searchText: String) {
        switch screenName {
        case .promotionCode:
            offerList = offerFilterInitialResult.filter {
                if let promName = $0.promotionName {
                    if promName.lowercased().range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                return false
            }
        case .location:
            locationList = initialLocationList.filter {
                if let locName = $0.locationName {
                    if locName.lowercased().range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                return false
            }
        case .Nonofferlocation:
            NonofferlocationList = initialNonofferLocationList.filter {
                if let locName = $0.locationName {
                    if locName.lowercased().range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                return false
            }
        }
        tableFilter.reloadData()
    }
    
    //MARK: - Target Methods
    @objc func searchAction() {
        searchBar.placeholder = "Search".localized
        searchBar.text = ""
        self.navigationItem.titleView = searchBar
        searchBar.becomeFirstResponder()
    }
    
    @objc func refreshTableData() {
        self.refreshControl.endRefreshing()
        switch screenName {
        case .promotionCode:
            let request = OfferReporListRequest(mobileNumber: UserModel.shared.mobileNo, offSet: "0", limit: "50")
            modelAReport.getOfferListForReport(request: request)
        case .location:
            loadData()
        case .Nonofferlocation:
            loadData()

        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.constraintBTable.constant = keyboardSize.height
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.constraintBTable.constant = 0
        self.view.layoutIfNeeded()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ReportPromotionLocationFilterController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        switch screenName {
        case .promotionCode:
            if offerList.count == 0 {
                let noDataLabel = UILabel(frame: tableView.bounds)
                if isDataRequested {
                    noDataLabel.text = "No Records".localized
                }
                noDataLabel.font = UIFont(name: appFont, size: 18)
                noDataLabel.textAlignment = .center
                tableView.backgroundView = noDataLabel
            } else {
                tableView.backgroundView = nil
            }
        case .location:
            if locationList.count == 0 {
                let noDataLabel = UILabel(frame: tableView.bounds)
                if isDataRequested {
                    noDataLabel.text = "No Records".localized
                }
                noDataLabel.textAlignment = .center
                noDataLabel.font = UIFont(name: appFont, size: 18)
                tableView.backgroundView = noDataLabel
            } else {
                tableView.backgroundView = nil
            }
        case .Nonofferlocation:
            if NonofferlocationList.count == 0 {
                let noDataLabel = UILabel(frame: tableView.bounds)
                if isDataRequested {
                    noDataLabel.text = "No Records".localized
                }
                noDataLabel.textAlignment = .center
                noDataLabel.font = UIFont(name: appFont, size: 18)
                tableView.backgroundView = noDataLabel
            } else {
                tableView.backgroundView = nil
            }
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch screenName {
        case .promotionCode:
            return offerList.count
        case .location:
            return locationList.count
        case .Nonofferlocation:
            return NonofferlocationList.count

        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let reportOfferFilterCell =  tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ReportPromotionLocationFilterCell else {
            return ReportOfferFilterCell()
        }
        switch screenName {
        case .promotionCode:
            let data = offerList[indexPath.row]
            let promCode = data.promotionName ?? ""
            var dateValue = ""
            if let sDate = data.promotionStartDate {
                if let dateInDate = sDate.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss") {
                    dateValue = dateInDate.stringValue(dateFormatIs: "dd-MMM-yyyy")
                    if let eDate = data.promotionEndDate {
                        if let eDateInDate = eDate.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss") {
                            dateValue = dateValue + " to " + eDateInDate.stringValue(dateFormatIs: "dd-MMM-yyyy")
                        }
                    }
                }
                
            }
            reportOfferFilterCell.configureCell(primeData: promCode, secondData: dateValue)
        case .location:
            let data = locationList[indexPath.row].locationName ?? ""
            reportOfferFilterCell.configureCell(primeData: data, secondData: "")
        case .Nonofferlocation:
            if appDelegate.getSelectedLanguage() == "my" {
                
                let data = NonofferlocationList[indexPath.row].locationNameMyanmar ?? ""
                reportOfferFilterCell.configureCell(primeData: data, secondData: "")
                
            } else {
                
                let data = NonofferlocationList[indexPath.row].locationName ?? ""
                reportOfferFilterCell.configureCell(primeData: data, secondData: "")
            }

            
            
        }
        return reportOfferFilterCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch screenName {
        case .promotionCode:
            let data = offerList[indexPath.row]
            let pc = (data.promotionCode ?? "") + "@@" + (data.promotionId ?? "")
            self.navigationController?.popViewController(animated: true)
            delegate?.assignPromoLocoFilter(filterType: .promotion, value: pc)
        case .location:
            let data = locationList[indexPath.row]
            let locationDetail = (data.locationID ?? "") + "@@" + (data.locationName ?? "")
            self.navigationController?.popViewController(animated: true)
            delegate?.assignPromoLocoFilter(filterType: .location, value: locationDetail)
        case .Nonofferlocation:
            let data = NonofferlocationList[indexPath.row]
            let locationDetail = (data.locationID ?? "") + "@@" + (data.locationName ?? "")
            self.navigationController?.popViewController(animated: true)
            delegate?.assignPromoLocoFilter(filterType: .location, value: locationDetail)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight = 250
        tableView.rowHeight          = UITableView.automaticDimension
        return tableView.rowHeight
    }
}

// MARK: - UISearchBarDelegate
extension ReportPromotionLocationFilterController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            uiButton.setTitle("Cancel".localized, for:.normal)

            if let navFont = UIFont(name: appFont, size: 18) {
                uiButton.titleLabel?.font = navFont
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            switch screenName {
            case .promotionCode:
                offerList = offerFilterInitialResult
            case .location:
                locationList = initialLocationList
            case .Nonofferlocation:
                NonofferlocationList = initialNonofferLocationList
            }
            tableFilter.reloadData()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
            let updatedText = searchText.replacingCharacters(in: textRange, with: text)
            if updatedText.count > 50 {
                return false
            }
            if updatedText != "" && text != "\n" {
                self.applySearch(with: updatedText)
            }
        }
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        resetScreen()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}

// MARK: - ReportModelDelegate
extension ReportPromotionLocationFilterController: ReportModelDelegate {
    func apiResult(message: String?, statusCode: String?) {
        self.isDataRequested = true
        switch screenName {
        case .promotionCode:
            DispatchQueue.main.async {
                self.offerFilterInitialResult = self.modelAReport.offerInitialReports
                self.offerList = self.offerFilterInitialResult
                self.tableFilter.reloadData()
                self.setUpNavigation()
            }
        case .location:
            DispatchQueue.main.async {
                self.initialLocationList = self.modelAReport.offerInitialLocation
                self.locationList = self.initialLocationList
                self.tableFilter.reloadData()
                self.setUpNavigation()
            }
        default:
            return
        }
    }
    
   /* func handleCustomError(error: CustomError) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }*/
}

// MARK: - Additional Methods
extension ReportPromotionLocationFilterController: NavigationSetUpProtocol {
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        var count = 0
        switch screenName {
        case .promotionCode:
            count = offerFilterInitialResult.count
        case .location:
            count = initialLocationList.count
        case .Nonofferlocation:
            count = initialNonofferLocationList.count
        }
        if count > 1 {
            let searchButton = UIButton(type: .custom)
            searchButton.setImage(#imageLiteral(resourceName: "search_white").withRenderingMode(.alwaysOriginal), for: .normal)
            searchButton.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
            if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
                searchButton.frame = CGRect(x: navTitleView.frame.size.width - 80, y: ypos, width: 30, height: 30)
            }
            else{
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
            }
            navTitleView.addSubview(searchButton)
        }
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  screenName.rawValue.localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
}


extension ReportPromotionLocationFilterController: ReportBModelAllTransaction {
    
    func handleCustomError(error: CustomError) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
    
    func apiResultnonofferSummary(message: String?, statusCode: String?){
        
        //print("apiResultnonofferAllTransaction called-----\(message.count)")
        DispatchQueue.main.async {
            self.initialNonofferLocationList = self.modelBReport.nonofferAllLocationList
            self.NonofferlocationList = self.initialNonofferLocationList
            self.tableFilter.reloadData()
            self.setUpNavigation()
        }
        
    }
}
