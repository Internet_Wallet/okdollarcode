//
//  ReportSolarPdfBody.swift
//  OK
//
//  Created by E J ANTONY on 28/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportSolarPdfBody: UIView {
    //MARK: - Outlet
    @IBOutlet weak var lblName: UILabel!{
        didSet{
            lblName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblTransactionID: UILabel!{
        didSet{
            lblTransactionID.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblAmount: UILabel!{
        didSet{
            lblAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblServiceFee: UILabel!{
        didSet{
            lblServiceFee.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblAccountNo: UILabel!{
        didSet{
            lblAccountNo.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblStatus: UILabel!{
        didSet{
            lblStatus.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblDate: UILabel!{
        didSet{
            lblDate.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    //MARK: - Methods
    func configureData(report: SolarReport) {
        lblName.text = report.customer?.customerName ?? ""
        lblTransactionID.text = report.customerOkPaymentID ?? ""
        lblAmount.text = ""
        if let amnt = report.amount {
            lblAmount.text = "\(amnt)"
        }
        lblServiceFee.text = ""
        if let sCharge = report.serviceCharges {
            lblServiceFee.text = "\(sCharge)"
        }
        lblAccountNo.text = report.solarHomeAccountNumber ?? ""
        lblStatus.text = report.topupStatus ?? ""
        lblDate.text = ""
        if let createdDateStr = report.createdDate {
            let dateValue = createdDateStr.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS")
            let dateString = dateValue?.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy\nHH:mm:ss")
            self.lblDate.text = dateString
        }
    }
}
