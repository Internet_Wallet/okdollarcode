//
//  ReportAdvanceDummyFilterController.swift
//  OK
//
//  Created by E J ANTONY on 13/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
protocol AdvanceFilterDelegate {
    func assignAdvanceFilter(filterType: ReportOfferFilterOptions, name: String, no: String, amount: String)
}
protocol DummyFilterDelegate {
    func assignDummyFilter(filterType: ReportOfferFilterOptions, selectedItems: [OfferDummyMerchantList])
}
class ReportAdvanceDummyFilterController: UIViewController {

    enum ScreenType: String {
        case advanceMerchant = "Advance Merchant", dummyMerchant = "Dummy Merchant"
    }
    
    //MARK: - Outlet
    @IBOutlet weak var tableFilter: UITableView!
    @IBOutlet weak var btnNext: UIButton!{
        didSet {
            btnNext.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var constraintHNextButton: NSLayoutConstraint!
    @IBOutlet weak var constraintBTable: NSLayoutConstraint!
    
    // MARK: - Properties
    private lazy var modelAReport: ReportAModel = {
        return ReportAModel()
    }()
    private var isDataRequested = false
    private var cellIdentifier = "ReportAdvanceDummyFilterCell"
    private var merchantList = [OfferAdvanceMerchantList]()
    private var initialMerchantList = [OfferAdvanceMerchantList]()
    private var dummyMerchantList = [OfferDummyMerchantList]()
    private var initialDummyMerchantList = [OfferDummyMerchantList]()
    private var isSelectedAll = false
    private lazy var searchBar = UISearchBar()
    private let refreshControl = UIRefreshControl()
    private var isSearchActive = false
    var advanceFilterDelegate: AdvanceFilterDelegate?
    var dummyFilterDelegate: DummyFilterDelegate?
    
    var townshipID = ""
    var promotionID = ""
    var advanceMerchantNo = ""
    var screenName = ScreenType.advanceMerchant
    struct FilterAdvanceDummyMerchant {
        var name: String
        var mobileNumber: String
        var amount: String
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableFilter.tableFooterView = UIView()
        tableFilter.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshTableData), for: .valueChanged)
        setUpSearchBar()
        modelAReport.reportAModelDelegate = self
        getData()
        setUpNavigation()
        registerKeyboardNotification()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Methods
    private func registerKeyboardNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func getData() {
        switch screenName {
        case .advanceMerchant:
            let request = OfferListFilterRequest(mobileNumber: UserModel.shared.mobileNo, promotionId: promotionID, date: nil, townshipId: townshipID, advMerchantNo: nil, offSetVal: nil, dummyMerchantNo: nil, limitVal: nil)
            modelAReport.getOfferAdvanceMerchant(request: request)
        case .dummyMerchant:
            let request = OfferListFilterRequest(mobileNumber: UserModel.shared.mobileNo, promotionId: promotionID, date: nil, townshipId: townshipID, advMerchantNo: advanceMerchantNo, offSetVal: nil, dummyMerchantNo: nil, limitVal: nil)
            modelAReport.getOfferDummyMerchant(request: request)
        }
    }
    
    private func setUpSearchBar() {
        searchBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 44)
        if #available(iOS 11.0, *) {
            searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        searchBar.tintColor = UIColor.white
        searchBar.placeholder = "Search".localized
        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
            searchTextField.keyboardType = .asciiCapable
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
                searchTextField.keyboardType = .asciiCapable
            }
        }
        let view = self.searchBar.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                subView.tintColor = UIColor.gray
            }
        }
    }
    
    private func resetScreen() {
        switch screenName {
        case .advanceMerchant:
            merchantList = initialMerchantList
        case .dummyMerchant:
            dummyMerchantList = initialDummyMerchantList
        }
        setUpNavigation()
        tableFilter.reloadData()
    }
    
    private func applySearch(with searchText: String) {
        switch screenName {
        case .advanceMerchant:
            merchantList = initialMerchantList.filter {
                if let mName = $0.advanceMerchantName {
                    if mName.lowercased().range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                return false
            }
        case .dummyMerchant:
            dummyMerchantList = initialDummyMerchantList.filter {
                if let dmName = $0.dummyMerchantName {
                    if dmName.lowercased().range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                return false
            }
        }
        tableFilter.reloadData()
    }
    
    func updateSelectedItemInSource(itemIndex: Int) {
        let dataToBeUpdated = dummyMerchantList[itemIndex]
        var isAtleastOneItemSelected = false
        for (index,item) in initialDummyMerchantList.enumerated() {
            if item == dataToBeUpdated {
                initialDummyMerchantList[index].isSelected = !dataToBeUpdated.isSelected
            }
            if initialDummyMerchantList[index].isSelected {
                isAtleastOneItemSelected = true
            }
        }
        if isAtleastOneItemSelected {
            self.constraintHNextButton.constant = 50
            self.btnNext.isHidden = false
        } else {
            self.constraintHNextButton.constant = 0
            self.btnNext.isHidden = true
        }
        self.view.layoutIfNeeded()
    }
    
    //MARK: - Button Action Methods
    @IBAction func sendSelectedDummyMerchNo(_ sender: UIButton) {
        let selectedItems = initialDummyMerchantList.filter {
            return $0.isSelected
        }
        self.navigationController?.popViewController(animated: true)
        dummyFilterDelegate?.assignDummyFilter(filterType: .dummyMerchantNo, selectedItems: selectedItems)
    }
    
    //MARK: - Target Methods
    @objc func searchAction() {
        searchBar.placeholder = "Search".localized
        searchBar.text = ""
        self.navigationItem.titleView = searchBar
        searchBar.becomeFirstResponder()
    }
    
    @objc func refreshTableData() {
        self.refreshControl.endRefreshing()
        self.getData()
    }
    
    @objc func selectAllAction() {
        var tempArr = [OfferDummyMerchantList]()
        if !isSelectedAll {
            for item in initialDummyMerchantList {
                var newItem = item
                newItem.isSelected = true
                tempArr.append(newItem)
            }
            isSelectedAll = true
            btnNext.isHidden = false
            constraintHNextButton.constant = 50
        } else {
            for item in initialDummyMerchantList {
                var newItem = item
                newItem.isSelected = false
                tempArr.append(newItem)
            }
            isSelectedAll = false
            btnNext.isHidden = true
            constraintHNextButton.constant = 0
        }
        self.view.layoutIfNeeded()
        initialDummyMerchantList = tempArr
        dummyMerchantList = initialDummyMerchantList
        tableFilter.reloadData()
        setUpNavigation()
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.constraintBTable.constant = keyboardSize.height
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.constraintBTable.constant = 0
        self.view.layoutIfNeeded()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ReportAdvanceDummyFilterController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        switch screenName {
        case .advanceMerchant:
            if merchantList.count == 0 {
                let noDataLabel = UILabel(frame: tableView.bounds)
                if isDataRequested {
                    noDataLabel.text = "No Records".localized
                }
                noDataLabel.font = UIFont(name: appFont, size: 18)
                noDataLabel.textAlignment = .center
                tableView.backgroundView = noDataLabel
            } else {
                tableView.backgroundView = nil
            }
        case .dummyMerchant:
            if dummyMerchantList.count == 0 {
                let noDataLabel = UILabel(frame: tableView.bounds)
                if isDataRequested {
                    noDataLabel.text = "No Records".localized
                }
                noDataLabel.font = UIFont(name: appFont, size: 18)
                noDataLabel.textAlignment = .center
                tableView.backgroundView = noDataLabel
            } else {
                tableView.backgroundView = nil
            }
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch screenName {
        case .advanceMerchant:
            return merchantList.count
        case .dummyMerchant:
            return dummyMerchantList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let reportOfferFilterCell =  tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ReportAdvanceDummyFilterCell else {
            return ReportAdvanceDummyFilterCell()
        }
        switch screenName {
        case .advanceMerchant:
            let data = merchantList[indexPath.row]
            let name = data.advanceMerchantName ?? ""
            let no = data.advanceMerchantMobileNumber ?? ""
            let amount = data.advanceMerchantTotalAmount ?? ""
            reportOfferFilterCell.configureCell(name: name, no: no, amount: amount, isSelected: false)
        case .dummyMerchant:
            let data = dummyMerchantList[indexPath.row]
            let name = data.dummyMerchantName ?? ""
            let no = data.dummyMerchantMobileNumber ?? ""
            let amount = data.dummyMerchantTotalAmount ?? ""
            reportOfferFilterCell.configureCell(name: name, no: no, amount: amount, isSelected: data.isSelected)
        }
        return reportOfferFilterCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch screenName {
        case .advanceMerchant:
            let data = merchantList[indexPath.row]
            let name = data.advanceMerchantName ?? ""
            let no = data.advanceMerchantMobileNumber ?? ""
            let amount = data.advanceMerchantTotalAmount ?? ""
            self.navigationController?.popViewController(animated: true)
            advanceFilterDelegate?.assignAdvanceFilter(filterType: .advanceMerchantNo, name: name, no: no, amount: amount)
        case .dummyMerchant:
            if initialDummyMerchantList.count == 1 {
                self.navigationController?.popViewController(animated: true)
                dummyFilterDelegate?.assignDummyFilter(filterType: .dummyMerchantNo, selectedItems: dummyMerchantList)
            } else {
                if isSelectedAll {
                    isSelectedAll = false
                    if !isSearchActive {
                        setUpNavigation()
                    }
                }
                updateSelectedItemInSource(itemIndex: indexPath.row)
                var data = dummyMerchantList[indexPath.row]
                data.isSelected = !data.isSelected
                dummyMerchantList[indexPath.row] = data
                tableView.reloadRows(at: [indexPath], with: .none)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight = 250
        tableView.rowHeight          = UITableView.automaticDimension
        return tableView.rowHeight
    }
}

// MARK: - UISearchBarDelegate
extension ReportAdvanceDummyFilterController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearchActive = true
        if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            uiButton.setTitle("Cancel".localized, for:.normal)
            if let navFont = UIFont(name: appFont, size: 18) {
                uiButton.titleLabel?.font = navFont
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            switch screenName {
            case .advanceMerchant:
                merchantList = initialMerchantList
            case .dummyMerchant:
                dummyMerchantList = initialDummyMerchantList
            }
            tableFilter.reloadData()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
            let updatedText = searchText.replacingCharacters(in: textRange, with: text)
            if updatedText.count > 50 {
                return false
            }
            if updatedText != "" && text != "\n" {
                self.applySearch(with: updatedText)
            }
        }
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearchActive = false
        resetScreen()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}

// MARK: - ReportModelDelegate
extension ReportAdvanceDummyFilterController: ReportModelDelegate {
    func apiResult(message: String?, statusCode: String?) {
        self.isDataRequested = true
        switch screenName {
        case .advanceMerchant:
            DispatchQueue.main.async {
                self.initialMerchantList = self.modelAReport.initialAdvanceMerchantList
                self.merchantList = self.initialMerchantList
                self.tableFilter.reloadData()
                self.setUpNavigation()
            }
        case .dummyMerchant:
            DispatchQueue.main.async {
                self.initialDummyMerchantList = self.modelAReport.initialDummyMerchantList
                self.dummyMerchantList = self.initialDummyMerchantList
                self.tableFilter.reloadData()
                self.isSelectedAll = false
                self.setUpNavigation()
            }
        }
    }
    
    func handleCustomError(error: CustomError) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}

// MARK: - Additional Methods
extension ReportAdvanceDummyFilterController: NavigationSetUpProtocol {
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        func addSearchButton() {
            let searchButton = UIButton(type: .custom)
            searchButton.setImage(#imageLiteral(resourceName: "search_white").withRenderingMode(.alwaysOriginal), for: .normal)
            searchButton.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
            if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
                searchButton.frame = CGRect(x: navTitleView.frame.size.width - 100, y: ypos, width: 30, height: 30)
            }
            else{
                 searchButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
            }
            navTitleView.addSubview(searchButton)
        }
        
        func addTickButton(forXpos: CGFloat) {
            let tickButton = UIButton(type: .custom)
            if isSelectedAll {
                tickButton.setImage(#imageLiteral(resourceName: "reportSelectAll").withRenderingMode(.alwaysOriginal), for: .normal)
            } else {
                tickButton.setImage(#imageLiteral(resourceName: "reportUnselectAll").withRenderingMode(.alwaysOriginal), for: .normal)
            }
            tickButton.addTarget(self, action: #selector(selectAllAction), for: .touchUpInside)
            tickButton.frame = CGRect(x: navTitleView.frame.size.width - forXpos, y: ypos, width: 30, height: 30)
            navTitleView.addSubview(tickButton)
        }
        var forWidthValue: CGFloat = 20
        switch screenName {
        case .advanceMerchant:
            let count = initialMerchantList.count
            if count > 1 {
                addSearchButton()
                forWidthValue = 80
            }
        case .dummyMerchant:
            let count = initialDummyMerchantList.count
            if count > 1 {
                addSearchButton()
                addTickButton(forXpos: 70)
                forWidthValue = 160
            }
        }
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - forWidthValue, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  screenName.rawValue.localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
}
