//
//  ReportSurveyListController.swift
//  OK
//  This show the list of survey detail
//  Created by ANTONY on 05/02/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportSurveyListController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var tableViewSurvey: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var constraintScrollContentHeight: NSLayoutConstraint!
    @IBOutlet weak var buttonMobileNumber: ButtonWithImage!{
            didSet {
                buttonMobileNumber.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
            }
        }
    @IBOutlet weak var buttonRating: ButtonWithImage!{
        didSet {
            buttonRating.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonFeedback: ButtonWithImage!{
        didSet {
            buttonFeedback.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var constraintContentWidth: NSLayoutConstraint!
    
    // MARK: - Properties
    let reportSurveyCellId = "ReportSurveyCell"
    private var initialRecords = [ReportTransactionRecord]()
    private var transactionRecords = [ReportTransactionRecord]()
    private let refreshControl = UIRefreshControl()
    var modelReport: ReportModel!
    var animatedLabel = MarqueeLabel()
    
    // MARK: - View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewSurvey.tag = 0
        tableViewSurvey.tableFooterView = UIView()
        
        initialRecords = ReportCoreDataManager.sharedInstance.fetchTransactionRecords()
        if initialRecords.count == 0 {
                   modelReport = ReportModel()
                   modelReport.reportModelDelegate = self
                   modelReport.getTransactionInfo(showLoader: true)
         }
        else {
            initialRecords = initialRecords.filter {
                       guard let accTransType = $0.accTransType else { return false }
                       return accTransType == "Cr"
                   }
                   initialRecords = initialRecords.filter {
                       guard let rating = $0.rating  else { return false }
                       let rate = (rating as NSString).intValue
                       return rate > 0
                   }
        }
       
        
        scrollView.alwaysBounceVertical = true
        scrollView.bounces  = true
        refreshControl.addTarget(self, action: #selector(refreshTableData), for: .valueChanged)
        self.scrollView.addSubview(refreshControl)
        
       // tableViewSurvey.refreshControl = refreshControl
        
      //  refreshControl.addTarget(self, action: #selector(refreshTableData), for: .allTouchEvents)
        

        transactionRecords = initialRecords
        buttonMobileNumber.setTitle("Mobile Number".localized, for: .normal)
        buttonRating.setTitle("Rating".localized, for: .normal)
        buttonFeedback.setTitle("Feedback".localized, for: .normal)
        AppDelegate.AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)
        setUpNavigation()
    }
    
    @objc private func refreshTableData(_ sender: Any) {
        modelReport = ReportModel()
        modelReport.reportModelDelegate = self
        modelReport.getTransactionInfo(showLoader: false)
    }
    
    override func viewDidLayoutSubviews() {
      
        let navBarHeight = navigationController?.navigationBar.frame.size.height ?? 44
        let heightToReduce = navBarHeight == 32 ? 56 : 44
        constraintScrollContentHeight.constant = scrollView.frame.size.height - CGFloat(heightToReduce) + 44
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        if UIScreen.main.bounds.size.width > 736 {
           constraintContentWidth.constant = UIScreen.main.bounds.size.width
            self.view.layoutIfNeeded()
        }
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ReportSurveyListController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if transactionRecords.count == 0 {
            let noDataLabel = UILabel(frame: tableView.bounds)
            noDataLabel.text = "No Records".localized
            noDataLabel.font = UIFont(name: appFont, size: 18)
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
        } else {
            tableView.backgroundView = nil
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactionRecords.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reportSurveyCell = tableView.dequeueReusableCell(withIdentifier: reportSurveyCellId, for: indexPath) as? ReportSurveyCell
        reportSurveyCell?.configureResaleReportCell(transRecord: transactionRecords[indexPath.row])
        return reportSurveyCell!
    }
}

// MARK: - Additional Methods
extension ReportSurveyListController: NavigationSetUpProtocol {
    /// It set up the navigation bar
    func setUpNavigation() {
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationController?.navigationBar.barTintColor        = ConstantsColor.navigationSurveyDetails
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: titleViewWidth, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        let ypos = (titleViewSize.height - 30 ) / 2
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text = ConstantsController.reportSurveyListController.screenTitle.localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 23) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
    
    /// It shows the home screen
    @objc func backAction() {
        AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        self.navigationController?.popViewController(animated: true)
    }
}


extension ReportSurveyListController: ReportModelDelegate {
    func apiResult(message: String?, statusCode: String?) {
        DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 1.5) {
            self.initialRecords = ReportCoreDataManager.sharedInstance.fetchTransactionRecords()
            self.initialRecords = self.initialRecords.filter {
                guard let accTransType = $0.accTransType else { return false }
                return accTransType == "Cr"
            }
            
            self.initialRecords = self.initialRecords.filter {
                guard let rating = $0.rating  else { return false }
                let rate = (rating as NSString).intValue
                return rate > 0
            }
            self.transactionRecords = self.initialRecords
            DispatchQueue.main.async {
                self.setUpNavigation()
                self.tableViewSurvey.reloadData()
                self.refreshControl.endRefreshing()
            }
            
        }
    }
    func handleCustomError(error: CustomError) {
        DispatchQueue.main.async {
            self.refreshControl.endRefreshing()
        }
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}
