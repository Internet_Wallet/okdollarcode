//
//  ReportAddWithdrawPdfHeader.swift
//  OK
//
//  Created by E J ANTONY on 01/12/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit

class ReportAddWithdrawPdfHeader: UIView {
    
    @IBOutlet weak var businessNameHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelReportHeader: UILabel!{
        didSet {
            labelReportHeader.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelReportSideHeader: UILabel!{
        didSet {
            labelReportSideHeader.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAccNameTitle: UILabel!{
        didSet {
            labelAccNameTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAccNameValue: UILabel!{
        didSet {
            labelAccNameValue.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAccNumberTitle: UILabel!{
        didSet {
            labelAccNumberTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAccNumberValue: UILabel!{
        didSet {
            labelAccNumberValue.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelEmailTitle: UILabel!{
        didSet {
            labelEmailTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelEmailValue: UILabel!{
        didSet {
            labelEmailValue.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDTTitle: UILabel!{
        didSet {
            labelDTTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDTValue: UILabel!{
        didSet {
            labelDTValue.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelTotalTransAmountTitle: UILabel!{
        didSet {
            labelTotalTransAmountTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelTotalTransAmountValue: UILabel!{
        didSet {
            labelTotalTransAmountValue.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAddressTitle: UILabel!{
        didSet {
            labelAddressTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAddressValue: UILabel!{
        didSet {
            labelAddressValue.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAccountFilterTitle: UILabel!{
        didSet {
            labelAccountFilterTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAccountFilterValue: UILabel!{
        didSet {
            labelAccountFilterValue.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAmountFilterTitle: UILabel!{
        didSet {
            labelAmountFilterTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAmountFilterValue: UILabel!{
        didSet {
            labelAmountFilterValue.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelPeriodFilterTitle: UILabel!{
        didSet {
            labelPeriodFilterTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelPeriodFilterValue: UILabel!{
        didSet {
            labelPeriodFilterValue.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelReceiverNo: UILabel!{
        didSet {
            labelReceiverNo.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelCardType: UILabel!{
        didSet {
            labelCardType.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelCardNumber: UILabel!{
        didSet {
            labelCardNumber.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAmount: UILabel!{
        didSet {
            labelAmount.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelFees: UILabel!{
        didSet {
            labelFees.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelTransID: UILabel!{
        didSet {
            labelTransID.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelBankTransID: UILabel!{
        didSet {
            labelBankTransID.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDateTime: UILabel!{
        didSet {
            labelDateTime.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var viewForFilter: UIView!
    
    @IBOutlet weak var labelClosingBal: UILabel!{
        didSet {
            labelClosingBal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelClosingBalVal: UILabel!{
        didSet {
            labelClosingBalVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelPeriod: UILabel!{
        didSet {
            labelPeriod.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelPeriodVal: UILabel!{
        didSet {
            labelPeriodVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelTransCount: UILabel!{
        didSet {
            labelTransCount.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelTransCountVal: UILabel!{
        didSet {
            labelTransCountVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    @IBOutlet weak var labelTotalFeeVal: UILabel!{
        didSet {
            labelTotalFeeVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelTotalFeeLbl: UILabel!{
        didSet {
            labelTotalFeeLbl.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    @IBOutlet weak var businessNameLbl: UILabel!{
        didSet {
            businessNameLbl.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var businessNameVal: UILabel!{
        didSet {
            businessNameVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var accountTypeLbl: UILabel!{
        didSet {
            accountTypeLbl.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var accountTypeVal: UILabel!{
        didSet {
            accountTypeVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var addressHeight: NSLayoutConstraint!
    
    func setLocalizationTitles() {
        labelReportHeader.text = "Add Money Report Statement"
        labelReportSideHeader.text = "Account Details"
        labelAccNameTitle.text = "Account Name"
        labelAccNumberTitle.text = "Account Number"
        labelEmailTitle.text = "Email"
        labelDTTitle.text = "Date & Time"
        labelTotalTransAmountTitle.text = "Total Transaction Amount"
        labelAddressTitle.text = "Address"
        labelAccountFilterTitle.text = "Account Type"
        labelAmountFilterTitle.text = "Amount"
        labelPeriodFilterTitle.text = "Period"
        labelReceiverNo.text = "Receiver OK$ Number"
        labelCardType.text = "Card Type"
        labelCardNumber.text = "Card Number"
        labelAmount.text = "Amount" + "\n" + "(MMk)"
        labelFees.text = "Fees" + "\n" + "(MMk)"
        labelTransID.text = "OK$ Transaction ID"
        labelBankTransID.text = "Bank Transaction ID"
        labelDateTime.text = "Date & Time"
        businessNameLbl.text = "Business Name"
        accountTypeLbl.text = "Account Type"
    }
    
    func fillDetails(awReports: [AddWithdrawData], filterAccType: String, filterAmount: String, filterPeriod: String, walletBal: String) {
        //self.setLocalizationTitles()
        var totalTransAmount: Double = 0
        var totalFees: Double = 0
        for record in awReports {
            if let amount = record.amount {
                totalTransAmount += amount
            }
            if let fee = record.adminFee {
                totalFees = totalFees + fee
            }
        }
        //self.businessNameVal.text = (UserModel.shared.businessName != "") ? UserModel.shared.businessName : "-"
        if UserModel.shared.agentType == .user {
            businessNameHeightConstraint.constant = 0
        } else {
            businessNameHeightConstraint.constant = 21
            self.businessNameVal.text = (UserModel.shared.businessName != "") ? UserModel.shared.businessName : "-"
        }
        let accountType = self.agentServiceTypeString(type: UserModel.shared.agentType)
        
        self.labelTransCountVal.text = "\(awReports.count)"
        self.labelPeriodVal.text = "All"
        self.labelTotalFeeVal.text = wrapAmountWithCommaDecimal(key: String(totalFees)) + " MMK"
        self.labelTotalTransAmountValue.text = wrapAmountWithCommaDecimal(key: String(totalTransAmount)) + " MMK"
        self.labelAccNameValue.text = "\(UserModel.shared.name)"
        var phNumber = UserModel.shared.mobileNo.replaceFirstTwoChar(withStr: "+")
        phNumber.insert(")", at: phNumber.index(phNumber.startIndex, offsetBy: 3))
        phNumber.insert("(", at: phNumber.startIndex)
        if UserModel.shared.mobileNo.hasPrefix("0095") {
            phNumber.insert("0", at: phNumber.index(phNumber.startIndex, offsetBy: 5))
        }
        self.labelAccNumberValue.text = "\(phNumber)"
        self.labelEmailValue.text = "\(UserModel.shared.email)".components(separatedBy: ",").first ?? ""
        let division = self.getDivision().components(separatedBy: ",")
        let townShip = "Township"
        var div = ""
        if division.count > 0 {
            div = division.last?.components(separatedBy: "Division").first ?? ""
        }
        if UserModel.shared.language != "en" {
            self.accountTypeVal.text = getBurmeseAgentType()
            self.labelAddressValue.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? ""), \(division.last ?? "")".replacingOccurrences(of: ", ,", with: ",")
        } else {
            self.accountTypeVal.text = accountType
            self.labelAddressValue.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? "") \(townShip), \(div), \(division.last ?? "")".replacingOccurrences(of: ", ,", with: ",")
            self.addressHeight.constant = 65.0
            self.layoutIfNeeded()
        }
//        self.labelAddressValue.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? "") \(townShip), \(div), \(division.last ?? ""), \(UserModel.shared.country)".replacingOccurrences(of: ", ,", with: ",")
        self.labelDTValue.text = Date().stringValueWithOutUTC(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss")
        let walBal = wrapAmountWithCommaDecimal(key: walletBal) + " MMK"
        self.labelClosingBalVal.text = walBal
        var titleStr = ""
        if filterAccType == "Account Type".localized {
            titleStr = "All"
        } else {
            titleStr = filterAccType
        }
        self.labelAccountFilterValue.text = titleStr
        if filterAmount == "Amount".localized {
            titleStr = "All"
        } else {
            titleStr = filterAmount
        }
        self.labelAmountFilterValue.text = titleStr
        if filterPeriod == "Period".localized {
            titleStr = "All"
        } else {
            titleStr = filterPeriod
        }
        titleStr = titleStr.replacingOccurrences(of: "\n", with: " ")
        self.labelPeriodFilterValue.text = titleStr
        self.layoutIfNeeded()
    }
    
    private func getDivision() -> String {
        return  getDivisionAndTownShipForReport(divisionCode: UserModel.shared.state, townshipCode: UserModel.shared.township)
    }
    
    private func agentServiceTypeString(type: AgentType) -> String {
        switch type {
        case .user:
            return "Personal"
        case .merchant:
            return "Merchant"
        case .agent:
            return "Agent"
        case .advancemerchant:
            return "Advance Merchant"
        case .dummymerchant:
            return "Safety Cashier"
        case .oneStopMart:
            return "One Stop Mart"
        }
    }
    
}
