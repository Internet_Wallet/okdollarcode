//
//  ReportResalePdfHeader.swift
//  OK
//
//  Created by ANTONY on 16/03/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportResalePdfHeader: UIView {
    
    @IBOutlet weak var businessNameHeightConstraint: NSLayoutConstraint!
    // MARK :- Outlets
    @IBOutlet weak var accName: UILabel!{
        didSet{
            self.accName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var accNumber: UILabel!{
        didSet{
            self.accNumber.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var email: UILabel!{
        didSet{
            self.email.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var address: UILabel!{
        didSet{
            self.address.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var period: UILabel!{
        didSet{
            self.period.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var dateTime: UILabel!{
        didSet{
            self.dateTime.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var totalSellingAmount: UILabel!{
        didSet{
            self.totalSellingAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var totalSellingPrice: UILabel!{
        didSet{
            self.totalSellingPrice.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var viewForResaleReportRecHeader: UIView!
    @IBOutlet weak var sellingAmount: UILabel!{
        didSet{
            self.sellingAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var sellingPrice: UILabel!{
        didSet{
            self.sellingPrice.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var operatorName: UILabel!{
        didSet{
            self.operatorName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var status: UILabel!{
        didSet{
            self.status.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var periodFilter: UILabel!{
        didSet{
            self.periodFilter.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    @IBOutlet weak var businessNameLbl: UILabel!{
        didSet{
            self.businessNameLbl.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var businessNameVal: UILabel!{
        didSet{
            self.businessNameVal.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var accountTypeLbl: UILabel!{
        didSet{
            self.accountTypeLbl.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var accountTypeVal: UILabel!{
        didSet{
            self.accountTypeVal.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    @IBOutlet weak var closingBalLbl: UILabel!{
        didSet{
            self.closingBalLbl.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var closingBalVal: UILabel!{
        didSet{
            self.closingBalVal.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var addressHeight: NSLayoutConstraint!
    
    func fillDetails(periodFilter: String, dateOne: String, dateTwo: String, sellAmountFilter: String, sellPriceFilter: String,
                     operatorFilter: String, statusFilter: String, record: [ReportResaleMainBalance], currentBalance: String) {
        self.closingBalVal.text = currentBalance + " MMK"
        self.accName.text = "\(UserModel.shared.name)"
        //self.businessNameVal.text = (UserModel.shared.businessName != "") ? UserModel.shared.businessName : "-"
        if UserModel.shared.agentType == .user {
            businessNameHeightConstraint.constant = 0
        } else {
            businessNameHeightConstraint.constant = 21
            self.businessNameVal.text = (UserModel.shared.businessName != "") ? UserModel.shared.businessName : "-"
        }
        let accountType = self.agentServiceTypeString(type: UserModel.shared.agentType)
        var phNumber = UserModel.shared.mobileNo.replaceFirstTwoChar(withStr: "+")
        phNumber.insert(")", at: phNumber.index(phNumber.startIndex, offsetBy: 3))
        phNumber.insert("(", at: phNumber.startIndex)
        if UserModel.shared.mobileNo.hasPrefix("0095") {
            phNumber.insert("0", at: phNumber.index(phNumber.startIndex, offsetBy: 5))
        }
        self.accNumber.text = "\(phNumber)"
        self.email.text = "\(UserModel.shared.email)".components(separatedBy: ",").first ?? ""
        let division = self.getDivision().components(separatedBy: ",")
        let townShip = "Township"
        var div = ""
        if division.count > 0 {
            div = division.last?.components(separatedBy: "Division").first ?? ""
        }
        if UserModel.shared.language != "en" {
            self.accountTypeVal.text = getBurmeseAgentType()
            self.address.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? ""), \(division.last ?? "")".replacingOccurrences(of: ", ,", with: ",")
        } else {
            self.accountTypeVal.text = accountType
            self.address.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? "") \(townShip), \(div), \(division.last ?? "")".replacingOccurrences(of: ", ,", with: ",")
            self.addressHeight.constant = 65.0
            self.layoutIfNeeded()
        }
//        self.address.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? "") \(townShip), \(div), \(division.last ?? ""), \(UserModel.shared.country)".replacingOccurrences(of: ", ,", with: ",")
        var period = ""
        switch periodFilter {
        case "Period".localized, "All".localized:
            period = "All"
        case "Date".localized:
            if let fromDate = dateOne.dateValue(dateFormatIs: "dd MMM yyyy") {
                let fDate = fromDate.stringValue(dateFormatIs: "dd-MMM-yyyy")
                if let toDate = dateTwo.dateValue(dateFormatIs: "dd MMM yyyy") {
                    let tDate = toDate.stringValue(dateFormatIs: "dd-MMM-yyyy")
                    period = "\(fDate) to \(tDate)"
                }
            }
        case "Month".localized:
            if let month = dateTwo.dateValue(dateFormatIs: "MMM yyyy") {
                let mon = month.stringValue(dateFormatIs: "MMM-yyyy")
                period = "\(mon)"
            }
        default:
            break
        }
        var resaleAmt: NSDecimalNumber = 0
        var sellingAmt: NSDecimalNumber = 0
        for list in record{
            if let sellingAmount = list.airtimeAmount {
                resaleAmt = resaleAmt.adding(sellingAmount)
            }
            if let sellingPrice = list.sellingAmount {
                sellingAmt = sellingAmt.adding(sellingPrice)
            }
        }
        self.period.text = period
        self.dateTime.text = "\(Date().stringValueWithOutUTC(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss"))"
        self.totalSellingAmount.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(resaleAmt)") + " MMK".localized
        self.totalSellingPrice.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(sellingAmt)") + " MMK".localized
        var titleStr = ""
        if sellAmountFilter == "Selling Amount".localized {
            titleStr = "All"
        } else {
            titleStr = sellAmountFilter
        }
        self.sellingAmount.text = titleStr
        if sellPriceFilter == "Selling Price".localized {
            titleStr = "All"
        } else {
            titleStr = sellPriceFilter
        }
        self.sellingPrice.text = titleStr
        if operatorFilter == "Operator".localized {
            titleStr = "All"
        } else {
            titleStr = operatorFilter
        }
        self.operatorName.text = titleStr
        if statusFilter == "Status".localized {
            titleStr = "All"
        } else {
            titleStr = statusFilter
        }
        self.status.text = titleStr
        if periodFilter == "Period".localized {
            titleStr = "All"
        } else {
            titleStr = periodFilter
        }
        self.periodFilter.text = titleStr
    }
    
    private func agentServiceTypeString(type: AgentType) -> String {
        switch type {
        case .user:
            return "Personal"
        case .merchant:
            return "Merchant"
        case .agent:
            return "Agent"
        case .advancemerchant:
            return "Advance Merchant"
        case .dummymerchant:
            return "Safety Cashier"
        case .oneStopMart:
            return "One Stop Mart"
        }
    }
    
    private func getDivision() -> String {
        return getDivisionAndTownShipForReport(divisionCode: UserModel.shared.state, townshipCode: UserModel.shared.township)
    }
}
