//
//  ReportCICOPdfHeader.swift
//  OK
//
//  Created by E J ANTONY on 04/10/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportCICOPdfHeader: UIView {

    //MARK: - Outlets
    
    @IBOutlet weak var businessNameHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblAccNameTitle: UILabel!{
        didSet {
            lblAccNameTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblAccNameValue: UILabel!{
        didSet {
            lblAccNameValue.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblAccNumberTitle: UILabel!{
        didSet {
            lblAccNumberTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblAccNumberValue: UILabel!{
        didSet {
            lblAccNumberValue.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblEmailTitle: UILabel!{
        didSet {
            lblEmailTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblEmailValue: UILabel!{
        didSet {
            lblEmailValue.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblAddressTitle: UILabel!{
        didSet {
            lblAddressTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblAddressValue: UILabel!{
        didSet {
            lblAddressValue.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblPeriodTitle: UILabel!{
        didSet {
            lblPeriodTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblPeriodValue: UILabel!{
        didSet {
            lblPeriodValue.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblDateTimeTitle: UILabel!{
        didSet {
            lblDateTimeTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblDateTimeValue: UILabel!{
        didSet {
            lblDateTimeValue.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblTotalTransTitle: UILabel!{
        didSet {
            lblTotalTransTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblTotalTransValue: UILabel!{
        didSet {
            lblTotalTransValue.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblAgentH: UILabel!{
        didSet {
            lblAgentH.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblTransIDH: UILabel!{
        didSet {
            lblTransIDH.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblMobileNumberH: UILabel!{
        didSet {
            lblMobileNumberH.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblDateTimeH: UILabel!{
        didSet {
            lblDateTimeH.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var viewForItemHeader: UIView!
    @IBOutlet weak var lblFilterH: UILabel!{
        didSet {
            lblFilterH.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblPeriodH: UILabel!{
        didSet {
            lblPeriodH.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    @IBOutlet weak var businessNameLbl: UILabel!{
        didSet {
            businessNameLbl.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var businessNameVal: UILabel!{
        didSet {
            businessNameVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var accountTypeLbl: UILabel!{
        didSet {
            accountTypeLbl.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var accountTypeVal: UILabel!{
        didSet {
            accountTypeVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    @IBOutlet weak var sentAmtLbl: UILabel!{
        didSet {
            sentAmtLbl.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var sentAmtVal: UILabel!{
        didSet {
            sentAmtVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var recAmtLbl: UILabel!{
        didSet {
            recAmtLbl.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var recAmtVal: UILabel!{
        didSet {
            recAmtVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var commissionLbl: UILabel!{
        didSet {
            commissionLbl.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var commissionVal: UILabel!{
        didSet {
            commissionVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var closingBalLbl: UILabel!{
        didSet {
            closingBalLbl.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var closingBalVal: UILabel!{
        didSet {
            closingBalVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
   @IBOutlet weak var addressHeight: NSLayoutConstraint!
    
    
    func setLocalizationTitles() {
        self.lblAccNameTitle.text = "Account Name"
        self.lblAccNumberTitle.text = "Account Number"
        self.lblEmailTitle.text = "Email"
        self.lblAddressTitle.text = "Address"
        self.lblPeriodTitle.text = "Period"
        self.lblDateTimeTitle.text = "Date & Time"
        self.lblTotalTransTitle.text = "Total Transactions"
        self.lblAgentH.text = "Payment Type"
        self.lblTransIDH.text = "Transaction ID"
        self.lblMobileNumberH.text = "Mobile Number"
        self.lblDateTimeH.text = "Date & Time"
        self.sentAmtLbl.text = "Total Sent Amount"
        self.recAmtLbl.text = "Total Received Amount"
        self.commissionLbl.text = "Total Commission"
        self.closingBalLbl.text = "Closing Balance"
    }
    
    func configureWithDatas(reportList: [ReportCICOList], filters: [String: String], currentBalance: String) {
        setLocalizationTitles()
        if let paymentType = filters["PaymentTypeFilter"] {
            self.lblFilterH.text = paymentType
        }
        if let period = filters["DateFilter"] {
            self.lblPeriodH.text = period
        }
        var totalCommission: Double = 0.0
        var sentAmt: Double = 0.0
        var recAmt: Double = 0.0
        for record in reportList {
            if let amount = record.commissionPercent {
                totalCommission += amount
            }
            if let amount = record.totalAmount {
                if (record.cashType == 0 && record.requesterPhoneNumber ?? "" == UserModel.shared.mobileNo) || (record.cashType == 1 && record.requesterPhoneNumber ?? "" == UserModel.shared.mobileNo) {
                   recAmt = recAmt + amount
                } else {
                   sentAmt = sentAmt + amount
                }
            }
        }
        
        //self.businessNameVal.text = (UserModel.shared.businessName != "") ? UserModel.shared.businessName : "-"
        if UserModel.shared.agentType == .user {
            businessNameHeightConstraint.constant = 0
        } else {
            businessNameHeightConstraint.constant = 21
            self.businessNameVal.text = (UserModel.shared.businessName != "") ? UserModel.shared.businessName : "-"
        }
        let accountType = self.agentServiceTypeString(type: UserModel.shared.agentType)        
        self.lblAccNameValue.text = "\(UserModel.shared.name)"
        self.lblAccNumberValue.text = "(\(UserModel.shared.countryCode))\(UserModel.shared.formattedNumber)"
        self.lblEmailValue.text = "\(UserModel.shared.email)".components(separatedBy: ",").first ?? ""
        let division = self.getDivision().components(separatedBy: ",")
        let townShip = "Township"
        var div = ""
        if division.count > 0 {
            div = division.last?.components(separatedBy: "Division").first ?? ""
        }
        if UserModel.shared.language != "en" {
            self.accountTypeVal.text = getBurmeseAgentType()
            self.lblAddressValue.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? ""), \(division.last ?? "")".replacingOccurrences(of: ", ,", with: ",")
        } else {
            self.accountTypeVal.text = accountType
            self.lblAddressValue.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? "") \(townShip), \(div), \(division.last ?? "")".replacingOccurrences(of: ", ,", with: ",")
            self.addressHeight.constant = 65.0
            self.layoutIfNeeded()
        }
//        self.lblAddressValue.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? "") \(townShip), \(div), \(division.last ?? ""), \(UserModel.shared.country)".replacingOccurrences(of: ", ,", with: ",")
        if reportList.count > 0 {
            let sDateStr = reportList[reportList.count - 1].date
            let eDateStr = reportList[0].date
            if let sDate = sDateStr?.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS"),
                let eDate = eDateStr?.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") {
                let fromDate = sDate.stringValue(dateFormatIs: "dd-MMM-yyyy")
                let toDate = eDate.stringValue(dateFormatIs: "dd-MMM-yyyy")
                if fromDate.compare(toDate) == .orderedDescending {
                    self.lblPeriodValue.text = fromDate + " To " + toDate
                } else {
                    self.lblPeriodValue.text = toDate + " To " + fromDate
                }
            }
        }
        let sentAmtStr = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(sentAmt)")
        self.sentAmtVal.text = "\(sentAmtStr)" + " MMK"
        let recAmtStr = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(recAmt)")
        self.recAmtVal.text = "\(recAmtStr)" + " MMK"
        let commissionAmt = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(totalCommission)")
        self.commissionVal.text = "\(commissionAmt)" + " MMK"
        self.closingBalVal.text = "\(currentBalance)" + " MMK"
        self.lblDateTimeValue.text = "\(Date().stringValueWithOutUTC(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss"))"
        self.lblTotalTransValue.text = "\(reportList.count)"
        self.layoutIfNeeded()
    }
    
    private func agentServiceTypeString(type: AgentType) -> String {
        switch type {
        case .user:
            return "Personal"
        case .merchant:
            return "Merchant"
        case .agent:
            return "Agent"
        case .advancemerchant:
            return "Advance Merchant"
        case .dummymerchant:
            return "Safety Cashier"
        case .oneStopMart:
            return "One Stop Mart"
        }
    }
    
    private func getDivision() -> String {
        return getDivisionAndTownShipForReport(divisionCode: UserModel.shared.state, townshipCode: UserModel.shared.township)
    }
}
