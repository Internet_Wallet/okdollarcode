//
//  ReportLoyaltyPdfHeader.swift
//  OK
//
//  Created by E J ANTONY on 11/10/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportLoyaltyPdfHeader: UIView {
    
    @IBOutlet weak var businessNameHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!{
        didSet {
            lblTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    @IBOutlet weak var lblAccNameTitle: UILabel!{
        didSet {
            lblAccNameTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblAccNameValue: UILabel!{
        didSet {
            lblAccNameValue.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblAccNoTitle: UILabel!{
        didSet {
            lblAccNoTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblAccNoValue: UILabel!{
        didSet {
            lblAccNoValue.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblEmailTitle: UILabel!{
        didSet {
            lblEmailTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblEmailValue: UILabel!{
        didSet {
            lblEmailValue.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblAddressTitle: UILabel!{
        didSet {
            lblAddressTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblAddressValue: UILabel!{
        didSet {
            lblAddressValue.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblPeriodTitle: UILabel!{
        didSet {
            lblPeriodTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblPeriodValue: UILabel!{
        didSet {
            lblPeriodValue.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblDateTimeTitle: UILabel!{
        didSet {
            lblDateTimeTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblDateTimeValue: UILabel!{
        didSet {
            lblDateTimeValue.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblTotTransTitle: UILabel!{
        didSet {
            lblTotTransTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblTotTransValue: UILabel!{
        didSet {
            lblTotTransValue.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    @IBOutlet weak var lblTransID: UILabel!{
        didSet {
            lblTransID.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblMerchantNo: UILabel!{
        didSet {
            lblMerchantNo.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblShopName: UILabel!{
        didSet {
            lblShopName.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblPoints: UILabel!{
        didSet {
            lblPoints.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblValidTill: UILabel!{
        didSet {
            lblValidTill.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblBalanceBonusPoint: UILabel!{
        didSet {
            lblBalanceBonusPoint.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblDateTime: UILabel!{
        didSet {
            lblDateTime.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    @IBOutlet weak var viewForTitles: UIView!
    @IBOutlet weak var busineNameValue: UILabel!{
        didSet {
            busineNameValue.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var accountTypeValue: UILabel!{
        didSet {
            accountTypeValue.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var addressHeight: NSLayoutConstraint!
    
    func setLocalizationTitles(type: String) {
        lblTitle.text = "Bonus Point Bill Detail Statement"
        lblAccNameTitle.text = "Account Name"
        lblAccNoTitle.text = "Account Number"
        lblEmailTitle.text = "Email"
        lblAddressTitle.text = "Address"
        lblPeriodTitle.text = "Period"
        lblDateTimeTitle.text = "Date & Time"
        lblTotTransTitle.text = "Total Transactions"
        
        lblTransID.text = "Transaction ID"
        lblMerchantNo.text = "Merchant Number"
        lblShopName.text = "Shop Name"
        lblPoints.text = type
        lblValidTill.text = "Valid Till"
        lblBalanceBonusPoint.text = "Balance Bonus Points"
        lblDateTime.text = "Date & Time"
    }
    
    func fillDetails(totalCount: Int, filterPeriod: String, type: String) {
        setLocalizationTitles(type: type)
        self.lblAccNameValue.text = "\(UserModel.shared.name)"
        //self.busineNameValue.text = (UserModel.shared.businessName != "") ? UserModel.shared.businessName : "-"
        if UserModel.shared.agentType == .user {
                   businessNameHeightConstraint.constant = 0
               } else {
                   businessNameHeightConstraint.constant = 21
                   self.busineNameValue.text = (UserModel.shared.businessName != "") ? UserModel.shared.businessName : "-"
               }
        let accountType = self.agentServiceTypeString(type: UserModel.shared.agentType)
        self.lblAccNoValue.text = "(\(UserModel.shared.countryCode))\(UserModel.shared.formattedNumber)"
        self.lblEmailValue.text = "\(UserModel.shared.email)".components(separatedBy: ",").first ?? ""
        let division = self.getDivision().components(separatedBy: ",")
        let townShip = "Township"
        var div = ""
        if division.count > 0 {
            div = division.last?.components(separatedBy: "Division").first ?? ""
        }
        if UserModel.shared.language != "en" {
            self.accountTypeValue.text = getBurmeseAgentType()
            self.lblAddressValue.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? ""), \(division.last ?? "")".replacingOccurrences(of: ", ,", with: ",")
        } else {
            self.accountTypeValue.text = accountType
            self.lblAddressValue.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? "") \(townShip), \(div), \(division.last ?? "")".replacingOccurrences(of: ", ,", with: ",")
            self.addressHeight.constant = 65.0
            self.layoutIfNeeded()
        }
//        self.lblAddressValue.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? "") \(townShip), \(div), \(division.last ?? ""), \(UserModel.shared.country)".replacingOccurrences(of: ", ,", with: ",")
        self.lblTotTransValue.text = "\(totalCount)"
        self.lblDateTimeValue.text = "\(Date().stringValueWithOutUTC(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss"))"
        var titleStr = ""
        if filterPeriod == "Period".localized {
            titleStr = "All".localized
        } else {
            titleStr = filterPeriod
        }
        titleStr = titleStr.replacingOccurrences(of: "\n", with: " ")
        self.lblPeriodValue.text = titleStr
        self.layoutIfNeeded()
    }
    
    private func agentServiceTypeString(type: AgentType) -> String {
        switch type {
        case .user:
            return "Personal"
        case .merchant:
            return "Merchant"
        case .agent:
            return "Agent"
        case .advancemerchant:
            return "Advance Merchant"
        case .dummymerchant:
            return "Safety Cashier"
        case .oneStopMart:
            return "One Stop Mart"
        }
    }
    
    private func getDivision() -> String {
        return getDivisionAndTownShipForReport(divisionCode: UserModel.shared.state, townshipCode: UserModel.shared.township)
    }
}
