//
//  ReportOfferViewController.swift
//  OK
//  This shows the offer list
//  Created by ANTONY on 03/02/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportOfferViewController: UIViewController {
    // MARK: - Outlet
    @IBOutlet weak var reportOfferTableView: UITableView!
    
    // MARK: - Properties
    private var cellIdentifier = "ReportOfferCell"
    private var multiButton : ActionButton?
    var dateStringToFilter = Date().stringValueWithOutUTC(dateFormatIs: "yyyy-MM-dd")
    var fromDateString = Date().stringValueWithOutUTC(dateFormatIs: "yyyy-MM-dd")
    var toDateString = Date().stringValueWithOutUTC(dateFormatIs: "yyyy-MM-dd")
    private lazy var modelAReport: ReportAModel = {
        return ReportAModel()
    }()
    private var offerList = [OfferReportList]()
    private lazy var searchBar = UISearchBar()
    private var isDataRequested = false
    
    @IBOutlet weak var lblTitleDateTime: UILabel!{
        didSet{
            lblTitleDateTime.font = UIFont(name : appFont, size : appFontSizeReport)
            lblTitleDateTime.text = "Date".localized
            
        }
    }
    @IBOutlet weak var lblValueDateTime: UILabel!{
        didSet{
            lblValueDateTime.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblTitleCName: UILabel!{
        didSet{
            lblTitleCName.font = UIFont(name : appFont, size : appFontSizeReport)
            lblTitleCName.text = "Company Name".localized
            
        }
    }
    @IBOutlet weak var lblValueCName: UILabel!{
        didSet{
            lblValueCName.font = UIFont(name : appFont, size : appFontSizeReport)
            lblValueCName.text = "All".localized
           
        }
    }
    // MARK: - View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigation()
        setUpInitial()
        self.loadFloatButtons()
        getDataWithDate()
    }
    // MARK: - API Custom Methods
    private func getDataWithCompany(categoryID: String){
        let request = OfferReporListCompanyRequest(mobileNumber: UserModel.shared.mobileNo, fromDate: fromDateString, toDate: toDateString, offSet: "0", limit: "100", categoryID: categoryID)
        modelAReport.reportAModelDelegate  = self
        modelAReport.getOfferListCompanyForReport(request: request)
    }
    
    private func getDataWithDate(){
        let request = OfferReporListDateRequest(mobileNumber: UserModel.shared.mobileNo, fromDate: fromDateString, toDate: toDateString, offSet: "0", limit: "100")
        modelAReport.reportAModelDelegate  = self
        modelAReport.getOfferListDateForReport(request: request)
    }

    private func getData() {
        let request = OfferReporListRequest(mobileNumber: UserModel.shared.mobileNo, offSet: "0", limit: "100")
        modelAReport.reportAModelDelegate  = self
        modelAReport.getOfferListForReport(request: request)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadFloatButtons() {
        var buttonItems = [ActionButtonItem]()
        let date = ActionButtonItem(title: "Date".localized, image: #imageLiteral(resourceName: "receiptSortDate"))
        date.action = { item in
            self.multiButton?.toggleMenu()
            guard let reportDatePickerController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportDatePickerController.nameAndID) as? ReportDatePickerController else { return }
            reportDatePickerController.dateMode = .fromToDate
            reportDatePickerController.isResetNeeded = true
            reportDatePickerController.delegate = self
            reportDatePickerController.viewOrientation = .portrait
            reportDatePickerController.modalPresentationStyle = .overCurrentContext
            self.present(reportDatePickerController, animated: false, completion: nil)
        }
        let company = ActionButtonItem(title: "Company Name".localized, image: #imageLiteral(resourceName: "receiptSortDate"))
        company.action = { item in
            self.multiButton?.toggleMenu()
            guard let reportDatePickerController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportCompanyController.nameAndID) as? ReportCompanyViewController else { return }
            reportDatePickerController.delegate = self
            self.navigationController?.pushViewController(reportDatePickerController, animated: true)
        }

        buttonItems = [company , date]
        
        multiButton = ActionButton(attachedToView: self.view, items: buttonItems)
        multiButton?.notifyBlurDelegate = self
        multiButton?.action = {
            button in button.toggleMenu()
        }
        multiButton?.setImage(#imageLiteral(resourceName: "menu_white_payto"), forState: .normal)
        multiButton?.backgroundColor = kYellowColor
    }

    // MARK: - Target Methods
    @objc func showSearchOption() {
        searchBar.placeholder = "Search".localized
        searchBar.text = ""
        self.navigationItem.titleView = searchBar
        searchBar.becomeFirstResponder()
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let contentInset = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: keyboardSize.height, right: 0.0)
            self.reportOfferTableView.contentInset = contentInset
            self.reportOfferTableView.scrollIndicatorInsets = contentInset
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.reportOfferTableView.contentInset = UIEdgeInsets.zero
        self.reportOfferTableView.scrollIndicatorInsets = UIEdgeInsets.zero
        self.view.layoutIfNeeded()
    }
    
    //MARK: - Methods
    func setUpInitial() {
        
        lblValueDateTime.text =  Date().stringValueWithOutUTC(dateFormatIs: "E,dd-MMM-yyyy")
        
        //Search Bar Setup
        searchBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40)
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        searchBar.tintColor = UIColor.white
        searchBar.placeholder = "Search".localized
        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 16) {
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
                searchTextField.keyboardType = .asciiCapable
            }
        }
        let view = self.searchBar.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                subView.tintColor = UIColor.gray
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func applySearch(with searchText: String) {
        offerList = modelAReport.offerInitialReports.filter {
            if let promName = $0.promotionName, let promCode = $0.promotionCode {
                if promName.lowercased().range(of: searchText.lowercased()) != nil || promCode.lowercased().range(of: searchText.lowercased()) != nil{
                    return true
                }
            }
            return false
        }
        reportOfferTableView.reloadData()
        
        let indexPath = IndexPath(row: 0, section: 0)
        self.reportOfferTableView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}

//MARK: - NotifyBlurDelegate
extension ReportOfferViewController: NotifyBlurDelegate {
    func blurRemoveDelegate() {
    }
}

// MARK: - UISearchBarDelegate
extension ReportOfferViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            uiButton.setTitle("Cancel".localized, for:.normal)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            offerList = modelAReport.offerInitialReports
            reportOfferTableView.reloadData()
            let indexPath = IndexPath(row: 0, section: 0)
            self.reportOfferTableView.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
            let updatedText = searchText.replacingCharacters(in: textRange, with: text)
            if !(updatedText == updatedText.components(separatedBy: NSCharacterSet(charactersIn: SEARCHCHARSET).inverted).joined(separator: "")) { return false }
            
            if updatedText == " " || updatedText == "  "{
                return false
            }
            if (updatedText.contains("  ")){
                return false
            }
            if updatedText.count > 50 {
                return false
            }
            if updatedText != "" && text != "\n" {
                self.applySearch(with: updatedText)
            }
            
            
        }
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
       
        offerList = modelAReport.offerInitialReports
        reportOfferTableView.reloadData()
        let indexPath = IndexPath(row: 0, section: 0)
        self.reportOfferTableView.scrollToRow(at: indexPath, at: .top, animated: true)
         setUpNavigation()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}

// MARK: - Additional Methods
extension ReportOfferViewController: ReportCompanyDelegate {
    func processWithCategoryID(categoryID : String , categoryName : String ) {
       lblValueCName.text  = categoryName
        if categoryID.count == 0 {
            getData()
        } else {
            if categoryID == "All" {
                getDataWithDate()
            } else {
            getDataWithCompany(categoryID: categoryID)
            }
        }
    }
}

extension ReportOfferViewController: ReportDatePickerDelegate {
    func processWithDate(fromDate: Date?, toDate: Date?, dateMode: DateMode) {
        switch dateMode {
        case .fromToDate:
            var selectedDate = ""
            if let safeFDate = fromDate, let safeTDate = toDate {
                fromDateString = safeFDate.stringValue(dateFormatIs: "yyyy-MM-dd")
                toDateString = safeTDate.stringValue(dateFormatIs: "yyyy-MM-dd")
                
                if fromDateString == toDateString {
                    selectedDate = safeFDate.stringValue(dateFormatIs: "E,dd-MMM-yyyy")
                } else {
                    selectedDate = safeFDate.stringValue(dateFormatIs: "E,dd-MMM-yyyy") + " - " + safeTDate.stringValue(dateFormatIs: "E,dd-MMM-yyyy")
                }
                lblValueDateTime.text = selectedDate
                getDataWithDate()
            }
        default:
            break
        }
    }
    
    func resetDate() {
        lblValueDateTime.text =  Date().stringValueWithOutUTC(dateFormatIs: "E,dd-MMM-yyyy")
        fromDateString = Date().stringValueWithOutUTC(dateFormatIs: "yyyy-MM-dd")
        toDateString = Date().stringValueWithOutUTC(dateFormatIs: "yyyy-MM-dd")
        getDataWithDate()
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ReportOfferViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if offerList.count == 0 {
            let noDataLabel = UILabel(frame: tableView.bounds)
            if isDataRequested {
                noDataLabel.text = "No Offers Found !!".localized
            }
            noDataLabel.textAlignment = .center
            noDataLabel.font = UIFont(name: appFont, size: 18)
            tableView.backgroundView = noDataLabel
        } else {
            tableView.backgroundView = nil
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offerList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let offerCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ReportOfferCell else { return ReportOfferCell() }
        offerCell.configureCell(data: offerList[indexPath.row])
        
        offerCell.selectionStyle = .none
        return offerCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //New
        guard let reportOfferFilterResultController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportOfferFilterMainVC.nameAndID) as? ReportOfferFilterMainVC else { return }
        reportOfferFilterResultController.promoCode = offerList[indexPath.row].promotionCode ?? ""
        reportOfferFilterResultController.promotionID = offerList[indexPath.row].promotionId ?? ""
        reportOfferFilterResultController.amountType = offerList[indexPath.row].amountType ?? 0
        reportOfferFilterResultController.promotionType = offerList[indexPath.row].promotionType ?? ""

        reportOfferFilterResultController.offerFilterResult = modelAReport.offerInitialReports
        reportOfferFilterResultController.fromDateString = fromDateString
        reportOfferFilterResultController.toDateString = toDateString
        
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
        
        if viewControllers.count < 3 {
            self.navigationController?.pushViewController(reportOfferFilterResultController, animated: true)
        }

        //self.navigationController?.pushViewController(reportOfferFilterResultController, animated: true)
    }
    
  
    
}

// MARK: - ReportModelDelegate
extension ReportOfferViewController: ReportModelDelegate {
    func apiResult(message: String?, statusCode: String?) {
        DispatchQueue.main.async {
            self.isDataRequested = true
            self.offerList = self.modelAReport.offerInitialReports
           
            self.reportOfferTableView.reloadData()
             if self.offerList.count > 0 {
            let indexPath = IndexPath(row: 0, section: 0)
            self.reportOfferTableView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
            self.setUpNavigation()
        }
    }
    
    func handleCustomError(error: CustomError) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}

// MARK: - Additional Methods
extension ReportOfferViewController: NavigationSetUpProtocol {
    func setUpNavigation() {
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        if offerList.count > 1 {
            let searchButton = UIButton(type: .custom)
            searchButton.setImage(#imageLiteral(resourceName: "search_white").withRenderingMode(.alwaysOriginal), for: .normal)
            searchButton.addTarget(self, action: #selector(showSearchOption), for: .touchUpInside)
            if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
                searchButton.frame = CGRect(x: navTitleView.frame.size.width - 100, y: ypos, width: 30, height: 30)
            }else{
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
            }
            navTitleView.addSubview(searchButton)
        }
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text = ConstantsController.reportOfferViewController.screenTitle.localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
}
