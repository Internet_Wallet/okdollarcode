//
//  ReportDatePickerController.swift
//  OK
//
//  Created by E J ANTONY on 17/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
protocol ReportDatePickerDelegate {
    func processWithDate(fromDate: Date?, toDate: Date?, dateMode: DateMode)
    func resetDate()
}
extension ReportDatePickerDelegate {
    func resetDate() { }
}
enum DateMode {
    case fromToDate, month, singleDate
}
class ReportDatePickerController: UIViewController {

    //MARK: - Outlet
    @IBOutlet weak var btnSubmit: UIButton!{
        didSet {
            btnSubmit.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var btnCancel: UIButton!{
        didSet {
            btnCancel.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var btnReset: UIButton!{
        didSet {
            btnReset.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblFromDateTitle: UILabel!{
        didSet {
            lblFromDateTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblToDateTitle: UILabel!{
        didSet {
            lblToDateTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var btnFromDateValue: UIButton!{
        didSet {
            btnFromDateValue.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var btnToDateValue: UIButton!{
        didSet {
            btnToDateValue.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var constraintWDateContainer: NSLayoutConstraint!
    @IBOutlet weak var constraintTDatePicker: NSLayoutConstraint!
    @IBOutlet weak var constraintTToLabel: NSLayoutConstraint!
    
    enum ViewMode {
        case landscape, portrait
    }
    //MARK: - Properties
    var dateMode = DateMode.fromToDate
    var delegate: ReportDatePickerDelegate?
    var viewOrientation = ViewMode.landscape
    var isResetNeeded = false
    
     let dateFormatter = DateFormatter()
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatter.calendar = Calendar.current
        if #available(iOS 13.4, *) {
                       datePicker.preferredDatePickerStyle = .wheels
                   } else {
                       // Fallback on earlier versions
                   }
           dateFormatter.dateFormat = "dd MMM yyyy"
        configureButtonAndLabels()
        addGesture()
        setViewForOrientationMode()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Methods
    private func addGesture() {
        let tapGestToRemoveView = UITapGestureRecognizer(target: self, action: #selector(shadowIsTapped))
        viewShadow.addGestureRecognizer(tapGestToRemoveView)
    }
    
    private func setViewForOrientationMode() {
        switch viewOrientation {
        case .portrait:
            constraintWDateContainer.constant = UIScreen.main.bounds.size.width
            constraintTDatePicker.constant = 10
            constraintTToLabel.constant = -20
            switch dateMode {
            case .fromToDate:
                constraintTToLabel.constant = -5
            default:
                break
            }
            self.view.layoutIfNeeded()
        default:
            break
        }
    }
    
    private func configureButtonAndLabels() {
        btnSubmit.layer.cornerRadius = 15
        btnCancel.layer.cornerRadius = 15
        btnReset.layer.cornerRadius = 15
        btnSubmit.addTarget(self, action: #selector(filterSelectedDate), for: .touchUpInside)
        btnCancel.addTarget(self, action: #selector(dismissPickerController), for: .touchUpInside)
        btnReset.addTarget(self, action: #selector(resetDate), for: .touchUpInside)
        
        lblFromDateTitle.text = "From Date".localized
        lblToDateTitle.text = "To Date".localized
        btnSubmit.setTitle("Submit".localized, for: .normal)
        btnCancel.setTitle("Cancel".localized, for: .normal)
        btnReset.setTitle("Reset".localized, for: .normal)
        datePicker.calendar = Calendar(identifier: .gregorian)
        datePicker.datePickerMode = .date
        
        switch dateMode {
        case .fromToDate:
            btnFromDateValue.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            btnToDateValue.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            configureDatePicker(tag: 0)
        case .month:
            btnFromDateValue.setTitle(nil, for: .normal)
            btnFromDateValue.isHidden = true
            lblFromDateTitle.isHidden = true
            lblToDateTitle.text = "Month".localized
            btnToDateValue.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "MMM yyyy"), for: .normal)
            configureMonthPicker()
        case .singleDate:
            btnFromDateValue.setTitle(nil, for: .normal)
            btnFromDateValue.isHidden = true
            lblFromDateTitle.isHidden = true
            lblToDateTitle.text = "Selected Date".localized
            btnToDateValue.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            configureSingleDatePicker()
        }
        
        if isResetNeeded {
            btnReset.isHidden = false
        } else {
            btnReset.isHidden = true
        }
    }
    
    private func configureDatePicker(tag: Int) {
        switch tag {
        case 0:
            btnFromDateValue.setTitleColor(ConstantsColor.navigationHeaderSendMoney, for: .normal)
            btnToDateValue.setTitleColor(UIColor(red: 30/255.0, green: 30/255, blue: 30/255, alpha: 1.0), for: .normal)
            datePicker.removeTarget(nil, action: nil, for: .allEvents)
            datePicker.addTarget(self, action: #selector(fromDatePickerChanged), for: .valueChanged)
            datePicker.minimumDate = nil
            if let endDateStr = btnToDateValue.title(for: .normal), let endDate = endDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                datePicker.maximumDate = endDate
            } else {
                datePicker.maximumDate = Date()
            }
            if let currentDateStr = btnFromDateValue.title(for: .normal), let currentDate = currentDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                datePicker.date = currentDate
            } else {
                btnFromDateValue.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            }
        case 1:
            btnFromDateValue.setTitleColor(UIColor(red: 30/255.0, green: 30/255, blue: 30/255, alpha: 1.0), for: .normal)
            btnToDateValue.setTitleColor(ConstantsColor.navigationHeaderSendMoney, for: .normal)
            datePicker.maximumDate = Date()
            datePicker.removeTarget(nil, action: nil, for: .allEvents)
            datePicker.addTarget(self, action: #selector(toDatePickerChanged), for: .valueChanged)
           // datePicker.minimumDate = nil
            if let endDateStr = btnFromDateValue.title(for: .normal), let endDate = dateFormatter.date(from: endDateStr) {
                datePicker.minimumDate = endDate
            } else {
                datePicker.minimumDate = nil
            }

            if let currentDateStr = btnToDateValue.title(for: .normal), let currentDate = currentDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                datePicker.date = currentDate
            } else {
                btnToDateValue.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            }
        default:
            break
        }
    }
    
    private func configureMonthPicker() {
        btnToDateValue.setTitleColor(ConstantsColor.navigationHeaderSendMoney, for: .normal)
        datePicker.maximumDate = Date()
        datePicker.date = Date()
        datePicker.removeTarget(nil, action: nil, for: .allEvents)
        datePicker.addTarget(self, action: #selector(monthPickerChanged), for: .valueChanged)
        datePicker.minimumDate = nil
        if let currentDateStr = btnToDateValue.title(for: .normal), let currentDate = currentDateStr.dateValue(dateFormatIs: "MMM yyyy") {
            datePicker.date = currentDate
        } else {
            btnToDateValue.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "MMM yyyy"), for: .normal)
        }
    }
    
    private func configureSingleDatePicker() {
        btnToDateValue.setTitleColor(ConstantsColor.navigationHeaderSendMoney, for: .normal)
        datePicker.maximumDate = Date()
        datePicker.date = Date()
        datePicker.removeTarget(nil, action: nil, for: .allEvents)
        datePicker.addTarget(self, action: #selector(dingleDatePickerChanged), for: .valueChanged)
        datePicker.minimumDate = nil
        if let currentDateStr = btnToDateValue.title(for: .normal), let currentDate = currentDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
            datePicker.date = currentDate
        } else {
            btnToDateValue.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
        }
    }
    
    //MARK: - Target Methods
    @objc func filterSelectedDate() {
        var fromDate: Date?
        var toDate: Date?
        switch dateMode {
        case .fromToDate:
            if let dateStartInStr = btnFromDateValue.title(for: .normal) {
                fromDate = dateStartInStr.dateValue(dateFormatIs: "dd MMM yyyy")
            }
            if let dateEndInStr = btnToDateValue.title(for: .normal) {
                toDate = dateEndInStr.dateValue(dateFormatIs: "dd MMM yyyy")
            }
        case .month:
            if let monthInStr = btnToDateValue.title(for: .normal) {
                toDate = monthInStr.dateValue(dateFormatIs: "MMM yyyy")
            }
        case .singleDate:
            if let monthInStr = btnToDateValue.title(for: .normal) {
                toDate = monthInStr.dateValue(dateFormatIs: "dd MMM yyyy")
            }
        }
        delegate?.processWithDate(fromDate: fromDate, toDate: toDate, dateMode: dateMode)
        dismissPickerController()
    }
    
    @objc func dismissPickerController() {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func resetDate() {
        delegate?.resetDate()
        dismissPickerController()
    }
    
    @objc func fromDatePickerChanged(datePicker:UIDatePicker) {
        btnFromDateValue.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
        //datePicker.minimumDate = datePicker.date
    }
    
    @objc func toDatePickerChanged(datePicker: UIDatePicker) {
        if let startDateStr = btnFromDateValue.title(for: .normal), let startDate = startDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
            datePicker.minimumDate = startDate
            switch datePicker.date.compare(startDate) {
            case .orderedAscending:
                if let endDateStr = btnToDateValue.title(for: .normal), let endDate = endDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                    datePicker.date = endDate
                }
            case .orderedDescending, .orderedSame:
                btnToDateValue.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            }
        } else {
            btnToDateValue.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
        }
    }
    
    @objc func monthPickerChanged(datePicker: UIDatePicker) {
        btnToDateValue.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "MMM yyyy"), for: .normal)
    }
    
    @objc func dingleDatePickerChanged(datePicker: UIDatePicker) {
        btnToDateValue.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
    }
    
    @objc func shadowIsTapped(sender: UITapGestureRecognizer) {
        dismissPickerController()
    }
    
    //MARK: - Button Action Methods
    @IBAction func configureDatePickerOne(_ sender: UIButton) {
        configureDatePicker(tag: 0)
    }
    
    @IBAction func configureDatePickerTwo(_ sender: UIButton) {
        if btnFromDateValue.isHidden {
            configureMonthPicker()
        } else {
            configureDatePicker(tag: 1)
        }
    }
    
    deinit {
        if let recognizers = viewShadow.gestureRecognizers {
            for recognizer in recognizers {
                if let recognizer = recognizer as? UITapGestureRecognizer {
                    viewShadow.removeGestureRecognizer(recognizer)
                }
            }
        }
    }
}
