//
//  ReportTopUpViewController.swift
//  OK
//  This is to show the report of top up and recharge viewcontroller
//  Created by ANTONY on 31/01/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportTopUpViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var tableViewTopUpRecharge: UITableView!
    @IBOutlet weak var tableViewFilter: UITableView!
    @IBOutlet weak var tableViewOption: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var constraintScrollContentHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintFilterTableLeading: NSLayoutConstraint!
    
    //For search functions
    @IBOutlet weak var textFieldSearch: UITextField!{
        didSet {
            textFieldSearch.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var buttonCancel: UIButton!{
        didSet {
            buttonCancel.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonClear: UIButton!{
        didSet {
            buttonClear.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    //For filter functions
    @IBOutlet weak var buttonTransactionFilter: ButtonWithImage!{
        didSet {
            buttonTransactionFilter.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonAmountFilter: ButtonWithImage!{
        didSet {
            buttonAmountFilter.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonOperatorFilter: ButtonWithImage!{
        didSet {
            buttonOperatorFilter.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonTypeFilter: ButtonWithImage!{
        didSet {
            buttonTypeFilter.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonPeriodFilter: ButtonWithImage!{
        didSet {
            buttonPeriodFilter.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    @IBOutlet weak var constraintScrollTop: NSLayoutConstraint!
    @IBOutlet weak var constraintFilterTableHeight: NSLayoutConstraint!
    @IBOutlet weak var viewToShareAllRecords: UIView!
    
    //For sorting functions
    @IBOutlet weak var buttonTransactionSort: ButtonWithImage!{
        didSet {
            buttonTransactionSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonAmountSort: ButtonWithImage!{
        didSet {
            buttonAmountSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonBonusPointSort: ButtonWithImage!{
        didSet {
            buttonBonusPointSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonMobileNumberSort: ButtonWithImage!{
        didSet {
            buttonMobileNumberSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonCashBackSort: ButtonWithImage!{
        didSet {
            buttonCashBackSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonBalanceSort: ButtonWithImage!{
        didSet {
            buttonBalanceSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateSort: ButtonWithImage!{
        didSet {
            buttonDateSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    @IBOutlet weak var viewDatePicker: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var buttonDateSubmit: UIButton!{
        didSet {
            buttonDateSubmit.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateCancel: UIButton!{
        didSet {
            buttonDateCancel.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDateOne: UILabel!{
        didSet {
            labelDateOne.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDateTwo: UILabel!{
        didSet {
            labelDateTwo.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateOne: UIButton!{
        didSet {
            buttonDateOne.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateTwo: UIButton!{
        didSet {
            buttonDateTwo.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var viewCircle: UIView!
    @IBOutlet weak var imgViewInCircle: UIImageView!
    @IBOutlet weak var labelInCircle: UILabel!{
        didSet {
            labelInCircle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var amountInCircle: UILabel!{
        didSet {
            amountInCircle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    // MARK: - Properties
    var initialRecords = [ReportTransactionRecord]()
    var topUpRecords = [ReportTransactionRecord]()
    let topUpRechargeCellID = "TopUpRechargeCell"
    let moreOptionsArray = ["Total Amount", "Total Cash Back", "Total Transactions"]
    let transactionFilters = ["Transaction", "All", "Cash Back", "Bonus Points"]
    let amountFilters = ["Amount", "All", "0 - 1,000", "1,001 - 10,000", "10,001 - 50,000", "50,001 - 1,00,000","1,00,001 - 2,00,000", "2,00,001 - 5,00,000", "5,00,001 - Above"]
    let operatorFilters = ["Operator", "All", "Mpt", "Telenor", "Ooredoo", "Mectel", "Mytel","International Top-Up"]
    let typeFilters = ["Type", "All", "Top-Up", "Special Offers", "Data Plan", "Air Time"]
    let periodFilters = ["Period", "All", "Date", "Month"]
    var filterArray = [String]()
    private var circleViewTimer: Timer!
    var currentBalance: NSDecimalNumber = 0
    private var totalCashBackAmount: NSDecimalNumber = 0
    private var totalTransactionAmount: NSDecimalNumber = 0
    enum FilterType {
        case transaction, amount, operatorFilter, type, period
    }
    var lastSelectedFilter = FilterType.transaction
    var isSearchViewShow: Bool {
        get {
            return !viewSearch.isHidden
        }
        set(newValue) {
            if newValue {
                viewSearch.isHidden = false
                textFieldSearch.becomeFirstResponder()
                constraintScrollTop.constant = 0
                self.view.layoutIfNeeded()
            } else {
                textFieldSearch.text = ""
                viewSearch.isHidden = true
                constraintScrollTop.constant = -40
                self.view.layoutIfNeeded()
                self.view.endEditing(true)
            }
        }
    }
    var isDateViewHidden: Bool {
        get {
            return viewDatePicker.isHidden
        }
        set(newValue) {
            if newValue {
                viewShadow.isHidden = true
                viewDatePicker.isHidden = true
            } else {
                viewShadow.isHidden = false
                viewDatePicker.isHidden = false
            }
        }
    }
    private let refreshControl = UIRefreshControl()
    var modelReport: ReportModel!
     let dateFormatter = DateFormatter()
    // MARK: - View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatter.calendar = Calendar.current
           dateFormatter.dateFormat = "dd MMM yyyy"
        tableViewTopUpRecharge.tag = 0
        tableViewFilter.tag = 1
        tableViewOption.tag = 2
        tableViewTopUpRecharge.tableFooterView = UIView()
        tableViewFilter.tableFooterView = UIView()
        tableViewOption.tableFooterView = UIView()
        tableViewTopUpRecharge.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshTableData), for: .valueChanged)
        buttonCancel.layer.cornerRadius = 15
        buttonClear.layer.cornerRadius = 15
        buttonCancel.addTarget(self, action: #selector(hideSearchView), for: .touchUpInside)
        buttonClear.addTarget(self, action: #selector(clearSerchTextField), for: .touchUpInside)
        
        buttonDateSubmit.layer.cornerRadius = 15
        buttonDateCancel.layer.cornerRadius = 15
        buttonDateSubmit.addTarget(self, action: #selector(filterSelectedDate), for: .touchUpInside)
        buttonDateCancel.addTarget(self, action: #selector(hideDateSelectionView), for: .touchUpInside)
        textFieldSearch.placeholder = "Search By Transaction ID or Amount or Mobile Number or Remarks".localized
        let cellFilterNibName = UINib(nibName: ConstantsXibCell.filterTableViewCell, bundle: nil)
        tableViewFilter.register(cellFilterNibName, forCellReuseIdentifier: ConstantsXibCell.filterTableViewCell)
        tableViewOption.register(cellFilterNibName, forCellReuseIdentifier: ConstantsXibCell.filterTableViewCell)
        
        datePicker.calendar = Calendar(identifier: .gregorian)
        if #available(iOS 13.4, *) {
                       datePicker.preferredDatePickerStyle = .wheels
                   } else {
                       // Fallback on earlier versions
                   }
        fetchRecordsFromDB()
        viewCircle.layer.cornerRadius = viewCircle.frame.size.height / 2
        buttonCancel.setTitle("Cancel".localized, for: .normal)
        buttonClear.setTitle("Clear".localized, for: .normal)
        labelDateOne.text = "From Date".localized
        labelDateTwo.text = "To Date".localized
        buttonDateSubmit.setTitle("Submit".localized, for: .normal)
        buttonDateCancel.setTitle("Cancel".localized, for: .normal)
        resetFilterButtons()
        setInitialValuesOfSort()
        AppDelegate.AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)
        setUpNavigation()
        modelReport = ReportModel()
        modelReport.reportModelDelegate = self
        modelReport.getTransactionInfo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
        let navBarHeight = navigationController?.navigationBar.frame.size.height ?? 44
        let heightToReduce = navBarHeight == 32 ? 56 : 44
        constraintScrollContentHeight.constant = scrollView.frame.size.height - CGFloat(heightToReduce) + 44
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    // MARK: - Methods
    @objc func refreshTableData() {
//        if viewSearch.isHidden {
        self.view.endEditing(true)
        isSearchViewShow = false
        textFieldSearch.text = ""
            modelReport.getTransactionInfo()
//        } else {
//            self.refreshControl.endRefreshing()
//        }
    }
    
    @objc func runTimedCode() {
        self.viewCircle.isHidden = true
        self.viewShadow.isHidden = true
        circleViewTimer?.invalidate()
    }
    
    func fetchRecordsFromDB() {
        totalTransactionAmount = 0.0
        totalCashBackAmount = 0.0
        initialRecords = ReportCoreDataManager.sharedInstance.fetchTransactionRecords()
            if initialRecords.count > 0 {
                currentBalance = initialRecords[initialRecords.count - 1].walletBalance ?? 0
            }
            initialRecords = initialRecords.filter {
                if let desc = $0.desc {
                    //if desc.contains("#OK-00-") {
                    if desc.contains(find: "Top-Up") || desc.contains(find: "TopUp Plan") || desc.contains(find: "TopUpPlan") || desc.contains(find: "Data Plan") || desc.contains("DataPlan") || desc.contains(find: "Special Offers") || desc.contains(find: "Special Offer") || desc.contains(find: "Bonus Point Top Up") || desc.contains("SpecialOffers") || desc.contains("SpecialOffer") {
                        if let cbAmount = $0.cashBack {
                            totalCashBackAmount = totalCashBackAmount.adding(cbAmount)
                        }
                        if let amount = $0.amount {
                            totalTransactionAmount = totalTransactionAmount.adding(amount)
                        }
                    }
                }
                guard let desc = $0.desc else { return false }
                return desc.contains(find: "Top-Up") || desc.contains(find: "TopUp Plan") || desc.contains(find: "TopUpPlan") || desc.contains(find: "Data Plan") || desc.contains(find: "DataPlan") || desc.contains(find: "Special Offers") || desc.contains(find: "Special Offer") || desc.contains(find: "SpecialOffers") || desc.contains(find: "Bonus Point Top Up") || desc.contains("SpecialOffers") || desc.contains("SpecialOffer")
                //return desc.contains("#OK-00-")
            }
            topUpRecords = initialRecords
    }
    
    func setInitialValuesOfSort() {
        buttonTransactionSort.setTitle("Transaction ID".localized, for: .normal)
        buttonAmountSort.setTitle("Amount".localized, for: .normal)
        buttonBonusPointSort.setTitle("Bonus Points".localized, for: .normal)
        buttonMobileNumberSort.setTitle("Mobile Number".localized, for: .normal)
        buttonCashBackSort.setTitle("Cash Back".localized, for: .normal)
        buttonBalanceSort.setTitle("Balance".localized, for: .normal)
        buttonDateSort.setTitle("Date & Time".localized, for: .normal)
    }
    
    func resetSortButtons(sender: UIButton) {
        if sender != buttonTransactionSort {
            buttonTransactionSort.tag = 0
            buttonTransactionSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonAmountSort {
            buttonAmountSort.tag = 0
            buttonAmountSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonMobileNumberSort {
            buttonMobileNumberSort.tag = 0
            buttonMobileNumberSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonBonusPointSort {
            buttonBonusPointSort.tag = 0
            buttonBonusPointSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonCashBackSort {
            buttonCashBackSort.tag = 0
            buttonCashBackSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonBalanceSort {
            buttonBalanceSort.tag = 0
            buttonBalanceSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonDateSort {
            buttonDateSort.tag = 0
            buttonDateSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
    }
    
    func configureDatePicker(tag: Int) {
        switch tag {
        case 0:
            buttonDateOne.setTitleColor(ConstantsColor.navigationHeaderTopUpRecharge, for: .normal)
            buttonDateTwo.setTitleColor(UIColor(red: 30/255.0, green: 30/255, blue: 30/255, alpha: 1.0), for: .normal)
            datePicker.datePickerMode = .date
            datePicker.removeTarget(nil, action: nil, for: .allEvents)
            datePicker.addTarget(self, action: #selector(fromDatePickerChanged), for: .valueChanged)
            datePicker.minimumDate = nil
            if let endDateStr = buttonDateTwo.title(for: .normal), let endDate = endDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                datePicker.maximumDate = endDate
            } else {
                datePicker.maximumDate = Date()
            }
            if let currentDateStr = buttonDateOne.title(for: .normal), let currentDate = currentDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                datePicker.date = currentDate
            } else {
                buttonDateOne.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            }
        case 1:
            buttonDateOne.setTitleColor(UIColor(red: 30/255.0, green: 30/255, blue: 30/255, alpha: 1.0), for: .normal)
            buttonDateTwo.setTitleColor(ConstantsColor.navigationHeaderTopUpRecharge, for: .normal)
            datePicker.datePickerMode = .date
            datePicker.maximumDate = Date()
            datePicker.removeTarget(nil, action: nil, for: .allEvents)
            datePicker.addTarget(self, action: #selector(toDatePickerChanged), for: .valueChanged)
            //datePicker.minimumDate = nil
            if let endDateStr = buttonDateOne.title(for: .normal), let endDate = dateFormatter.date(from: endDateStr) {
                datePicker.minimumDate = endDate
            } else {
                datePicker.minimumDate = nil
            }

            if let currentDateStr = buttonDateTwo.title(for: .normal), let currentDate = currentDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                datePicker.date = currentDate
            } else {
                buttonDateTwo.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            }
        case 2:
            buttonDateTwo.setTitleColor(ConstantsColor.navigationHeaderTopUpRecharge, for: .normal)
            datePicker.datePickerMode = .date
            datePicker.maximumDate = Date()
            datePicker.date = Date()
            datePicker.removeTarget(nil, action: nil, for: .allEvents)
            datePicker.addTarget(self, action: #selector(monthPickerChanged), for: .valueChanged)
            datePicker.minimumDate = nil
            if let currentDateStr = buttonDateTwo.title(for: .normal), let currentDate = currentDateStr.dateValue(dateFormatIs: "MMM yyyy") {
                datePicker.date = currentDate
            } else {
                buttonDateTwo.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "MMM yyyy"), for: .normal)
            }
        default:
            break
        }
    }
    
    func updateFilterTableHeight() {
        constraintFilterTableHeight.constant = filterArray.count > 4 ? CGFloat(5*44) : CGFloat(filterArray.count * 44)
        self.view.layoutIfNeeded()
    }
    
    // MARK: - Target Methods
    @objc func fromDatePickerChanged(datePicker:UIDatePicker) {
        buttonDateOne.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
    }
    
    @objc func toDatePickerChanged(datePicker:UIDatePicker) {
        if let startDateStr = buttonDateOne.title(for: .normal), let startDate = startDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
            switch datePicker.date.compare(startDate) {
            case .orderedAscending:
                if let endDateStr = buttonDateTwo.title(for: .normal), let endDate = endDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                    datePicker.date = endDate
                }
            case .orderedDescending, .orderedSame:
                buttonDateTwo.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            }
        } else {
            buttonDateTwo.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
        }
    }
    
    @objc func monthPickerChanged(datePicker:UIDatePicker) {
        buttonDateTwo.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "MMM yyyy"), for: .normal)
    }
    
    @objc private func hideSearchView() {
        self.view.endEditing(true)
        isSearchViewShow = false
        applyFilter()
    }
    
    @objc private func clearSerchTextField() {
        textFieldSearch.text = ""
        applyFilter()
    }
    
    @objc private func showSearchView() {
        circleViewTimer?.invalidate()
        tableViewFilter.isHidden = true
        tableViewOption.isHidden = true
        viewCircle.isHidden = true
        viewShadow.isHidden = true
        if !isSearchViewShow {
            removeGestureFromShadowView()
            /// For share all transaction
            viewToShareAllRecords.isHidden = true
            
            if topUpRecords.count > 0 {
                isSearchViewShow = true
                textFieldSearch.becomeFirstResponder()
            } else {
                noRecordAlertActions()
            }
        } else {
            //textFieldSearch.becomeFirstResponder()
            self.hideSearchView()
        }
    }
    
    @objc private func showShareOption() {
        circleViewTimer?.invalidate()
        if viewToShareAllRecords.isHidden {
            viewCircle.isHidden = true
            if textFieldSearch.text == "" {
                isSearchViewShow = false
            }
            tableViewOption.isHidden = true
            if topUpRecords.count > 0 {
                tableViewTopUpRecharge.reloadData()
                let tapGestToRemoveView = UITapGestureRecognizer(target: self, action: #selector(shadowIsTapped))
                viewShadow.addGestureRecognizer(tapGestToRemoveView)
                viewShadow.isHidden = false
                viewToShareAllRecords.isHidden = false
            } else {
                viewShadow.isHidden = true
                if textFieldSearch.text == "" {
                    topUpRecords = initialRecords
                }
                resetFilterButtons()
                resetSortButtons(sender: UIButton())
                tableViewTopUpRecharge.reloadData()
                noRecordAlertActions()
            }
        }
    }
    
    @objc private func showGraph() {
        circleViewTimer?.invalidate()
        if topUpRecords.count == 0 {
            noRecordAlertActions()
            return
        }
        guard let statisticsTransactionController = UIStoryboard(name: "Reports", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.statisticsTransactionController.nameAndID) as? StatisticsTransactionAndBillController else { return }
        statisticsTransactionController.screenType = .topUp
        statisticsTransactionController.initialRecords = initialRecords
        self.navigationController?.pushViewController(statisticsTransactionController, animated: true)
    }
    
    @objc private func showMoreOption() {
        circleViewTimer?.invalidate()
        if topUpRecords.count == 0 {
            noRecordAlertActions()
            return
        }
        if tableViewOption.isHidden {
            viewCircle.isHidden = true
            viewShadow.isHidden = true
            tableViewFilter.isHidden = true
            viewToShareAllRecords.isHidden = true
            self.view.endEditing(true)
            tableViewOption.isHidden = false
        } else {
            tableViewOption.isHidden = true
        }
    }
    
    @objc func filterSelectedDate() {
        if let _ = buttonDateOne.title(for: .normal) {
            buttonPeriodFilter.setTitle("Date".localized, for: .normal)
        } else {
            buttonPeriodFilter.setTitle("Month".localized, for: .normal)
        }
        applyFilter()
        isDateViewHidden = true
    }
    
    @objc func hideDateSelectionView() {
        isDateViewHidden = true
    }
    
    @objc func shadowIsTapped(sender: UITapGestureRecognizer) {
        removeGestureFromShadowView()
        viewShadow.isHidden = true
        viewToShareAllRecords.isHidden = true
    }
    
    func noRecordAlertActions() {
        alertViewObj.wrapAlert(title: "Alert".localized, body: "No Records".localized, img: UIImage(named : "AppIcon"))
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
        })
        alertViewObj.showAlert(controller: self)
    }
    
    func resetFilterButtons() {
        buttonTransactionFilter.setTitle(transactionFilters[0].localized, for: .normal)
        buttonAmountFilter.setTitle(amountFilters[0].localized, for: .normal)
        buttonOperatorFilter.setTitle(operatorFilters[0].localized, for: .normal)
        buttonTypeFilter.setTitle(typeFilters[0].localized, for: .normal)
        buttonPeriodFilter.setTitle(periodFilters[0].localized, for: .normal)
    }
    
    func removeGestureFromShadowView() {
        if let recognizers = viewShadow.gestureRecognizers {
            for recognizer in recognizers {
                if let recognizer = recognizer as? UITapGestureRecognizer {
                    viewShadow.removeGestureRecognizer(recognizer)
                }
            }
        }
    }
    
    func showDateOptions(selectedOption: String) {
        switch selectedOption {
        case "Period".localized, "All".localized:
            buttonPeriodFilter.setTitle(selectedOption, for: .normal)
            applyFilter()
            isDateViewHidden = true
        case "Date".localized:
            buttonDateOne.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            buttonDateTwo.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            labelDateOne.isHidden = false
            buttonDateOne.isHidden = false
            labelDateTwo.text = "To Date".localized
            configureDatePicker(tag: 0)
            isDateViewHidden = false
        case "Month".localized:
            buttonDateOne.setTitle(nil, for: .normal)
            buttonDateTwo.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "MMM yyyy"), for: .normal)
            labelDateOne.isHidden = true
            buttonDateOne.isHidden = true
            labelDateTwo.text = "Month".localized
            configureDatePicker(tag: 2)
            isDateViewHidden = false
        default:
            break
        }
    }
    
    // MARK: - Filter Actions
    func applyFilter(searchText: String = "") {
        var filteredArray = initialRecords
        var searchTxt = searchText
        if isSearchViewShow && searchTxt.count == 0 {
            searchTxt = textFieldSearch.text ?? ""
            filteredArray = filteredArray.sorted {
                guard let date1 = $0.transactionDate, let date2 = $1.transactionDate else { return false }
                return date1.compare(date2) == .orderedDescending
            }
        }
        filteredArray = filterProcess(forTransactionType: filteredArray)
        filteredArray = filterProcess(forAmountType: filteredArray)
        filteredArray = filterProcess(forOperator: filteredArray)
        filteredArray = filterProcess(forType: filteredArray)
        filteredArray = filterProcess(forPeriod: filteredArray)
        if isSearchViewShow && searchTxt.count > 0 {
            filteredArray = filteredArray.filter {
                if let desc = $0.desc {
                    if desc.lowercased().range(of:searchTxt.lowercased()) != nil {
                        return true
                    }
                }
                if let transID = $0.transID {
                    if transID.lowercased().range(of: searchTxt.lowercased()) != nil {
                        return true
                    }
                }
                if let mobileNumber = $0.destination {
                    if searchTxt.hasPrefix("0") {
                        let searchTt = searchTxt.dropFirst()
                        if mobileNumber.range(of: searchTt.lowercased()) != nil {
                            return true
                        }
                    } else {
                        if mobileNumber.range(of: searchTxt.lowercased()) != nil {
                            return true
                        }
                    }
                }
                if let amount = $0.amount {
                    if amount.toString().range(of: searchTxt.lowercased()) != nil {
                        return true
                    }
                }
                return false
            }
        }
        topUpRecords = filteredArray
        tableViewTopUpRecharge.reloadData()
    }
    
    func filterProcess(forTransactionType arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        if let transFilter = buttonTransactionFilter.currentTitle {
            switch transFilter {
            case "Transaction".localized, "All".localized:
                return arrayToFilter
            case "Cash Back".localized:
                return arrayToFilter.filter {
                    guard let cashBack = $0.cashBack else { return false }
                    return cashBack.compare(0) == .orderedDescending
                }
            case "Bonus Points".localized:
                return arrayToFilter.filter {
                    guard let bonus = $0.bonus else { return false }
                    return bonus.compare(0) == .orderedDescending
                }
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcess(forAmountType arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        func filterFromDecimalNumbers(greaterThan: NSDecimalNumber, lesserThan: NSDecimalNumber)-> [ReportTransactionRecord] {
            return arrayToFilter.filter
                {
                    guard let amount = $0.amount else { return false }
                    return (amount.compare(greaterThan) == .orderedDescending && amount.compare(lesserThan) == .orderedAscending)
            }
        }
        if let transFilter = buttonAmountFilter.currentTitle {
            switch transFilter {
            case "Amount".localized, "All".localized:
                return arrayToFilter
            case "0 - 1,000".localized:
                return filterFromDecimalNumbers(greaterThan: -0.01, lesserThan: 1001)
            case "1,001 - 10,000".localized:
                return filterFromDecimalNumbers(greaterThan: 1000.99, lesserThan: 10001)
            case "10,001 - 50,000".localized:
                return filterFromDecimalNumbers(greaterThan: 10000.99, lesserThan: 50001)
            case "50,001 - 1,00,000".localized:
                return filterFromDecimalNumbers(greaterThan: 50000.99, lesserThan: 100001)
            case "1,00,001 - 2,00,000".localized:
                return filterFromDecimalNumbers(greaterThan: 100000.99, lesserThan: 200001)
            case "2,00,001 - 5,00,000".localized:
                return filterFromDecimalNumbers(greaterThan: 200000.99, lesserThan: 500001)
            case "5,00,001 - Above".localized:
                return arrayToFilter.filter {
                    guard let amount = $0.amount else { return false }
                    return (amount.compare(500000.99) == .orderedDescending)
                }
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcess(forOperator arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        func filterBy(string: String) -> [ReportTransactionRecord] {
            return arrayToFilter.filter {
                guard let descr = $0.rawDesc else { return false }
                return descr.lowercased().contains(string.lowercased())
            }
        }
        if let transFilter = buttonOperatorFilter.currentTitle {
            switch transFilter {
            case "Operator".localized, "All".localized:
                return arrayToFilter
            case "Mpt":
                return filterBy(string: "Mpt")
            case "Telenor":
                return filterBy(string: "Telenor")
            case "Ooredoo":
                return filterBy(string: "Ooredoo")
            case "Mectel":
                return filterBy(string: "Mectel")
            case "Mytel":
                return filterBy(string: "Mytel")
            case "International Top-Up":
                return filterBy(string: "Overseas")
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcess(forType arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        func filterBy(string: String) -> [ReportTransactionRecord] {
            return arrayToFilter.filter {
                guard let descr = $0.desc else { return false }
                return descr.lowercased().contains(string.lowercased()) || descr.lowercased().contains(string.replacingOccurrences(of: " ", with: "").lowercased())
            }
        }
        if let transFilter = buttonTypeFilter.currentTitle {
            switch transFilter {
            case "Type".localized, "All".localized:
                return arrayToFilter
            case "Top-Up".localized:
                return filterBy(string: "TopUp Plan")
            case "Special Offers".localized, "SpecialOffers".localized:
                return filterBy(string: "Special Offers")
            case "Data Plan".localized, "DataPlan".localized:
                return filterBy(string: "Data Plan")
            case "Air Time".localized:
                return filterBy(string: "Air Time")
            case "International Top-Up".localized:
                return filterBy(string: "Overseas")
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcess(forPeriod arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        if let periodFilter = buttonPeriodFilter.currentTitle {
            switch periodFilter {
            case "Period".localized, "All".localized:
                return arrayToFilter
            case "Date".localized:
                return arrayToFilter.filter {
                    guard let transDate = $0.transactionDate else { return false }
                    let dFormatter = DateFormatter()
                    dFormatter.calendar = Calendar(identifier: .gregorian)
                    dFormatter.dateFormat = "dd MMM yyyy"
                    let transDateStr = dFormatter.string(from: transDate)
                    guard let transactionDate = dFormatter.date(from: transDateStr) else { return false }
                    guard let dateStartStr = buttonDateOne.currentTitle else { return false }
                    guard let dateEndStr = buttonDateTwo.currentTitle else { return false }
                    guard let exactDateStart = dFormatter.date(from: dateStartStr) else { return false }
                    guard let dateStart = Calendar.current.date(byAdding: Calendar.Component.day, value: -1, to: exactDateStart) else { return false }
                    guard let exactDateEnd = dFormatter.date(from: dateEndStr) else { return false }
                    guard let dateEnd = Calendar.current.date(byAdding: Calendar.Component.day, value: 1, to: exactDateEnd) else { return false}
                    return (dateStart.compare(transactionDate) == .orderedAscending && transactionDate.compare(dateEnd) == .orderedAscending)
                }
            case "Month".localized:
                return arrayToFilter.filter {
                    guard let transDate = $0.transactionDate else { return false }
                    let dFormatter = DateFormatter()
                    dFormatter.calendar = Calendar(identifier: .gregorian)
                    dFormatter.dateFormat = "MMM yyyy"
                    let transDateStr = dFormatter.string(from: transDate)
                    guard let transactionDate = dFormatter.date(from: transDateStr) else { return false }
                    guard let dateStr = buttonDateTwo.currentTitle else { return false }
                    guard let monthToFilter = dFormatter.date(from: dateStr) else { return false }
                    return transactionDate.compare(monthToFilter) == .orderedSame
                }
            default:
                return []
            }
        }
        return []
    }
    
    // MARK: - Button Actions
    // It filters and show the detail by transaction type
    @IBAction func filterTransactionType(_ sender: UIButton) {
        self.view.endEditing(true)
        func showFilterAtTransaction() {
            filterArray = transactionFilters
            lastSelectedFilter = .transaction
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 10
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
        }
        if tableViewFilter.isHidden {
            showFilterAtTransaction()
        } else {
            switch lastSelectedFilter {
            case .transaction:
                tableViewFilter.isHidden = true
            default:
                showFilterAtTransaction()
            }
        }
    }
    
    // It filters and show the detail by amount range
    @IBAction func filterByAmount(_ sender: UIButton) {
        self.view.endEditing(true)
        func showFilterAtAmount() {
            filterArray = amountFilters
            lastSelectedFilter = .amount
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 200
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
        }
        if tableViewFilter.isHidden {
            showFilterAtAmount()
        } else {
            switch lastSelectedFilter {
            case .amount:
                tableViewFilter.isHidden = true
            default:
                showFilterAtAmount()
            }
        }
    }
    
    // It filters and show the detail by operator
    @IBAction func filterByOperator(_ sender: UIButton) {
        self.view.endEditing(true)
        func showFilterAtOperator() {
            filterArray = operatorFilters
            lastSelectedFilter = .operatorFilter
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 380
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
        }
        if tableViewFilter.isHidden {
            showFilterAtOperator()
        } else {
            switch lastSelectedFilter {
            case .operatorFilter:
                tableViewFilter.isHidden = true
            default:
                showFilterAtOperator()
            }
        }
    }
    
    // It filters and show the detail by offers
    @IBAction func filterByTypes(_ sender: UIButton) {
        self.view.endEditing(true)
        func showFilterAtTypes() {
            filterArray = typeFilters
            lastSelectedFilter = .type
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 560
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
        }
        if tableViewFilter.isHidden {
            showFilterAtTypes()
        } else {
            switch lastSelectedFilter {
            case .type:
                tableViewFilter.isHidden = true
            default:
                showFilterAtTypes()
            }
        }
    }
    
    // It filters and show the detail by period
    @IBAction func filterByPeriod(_ sender: UIButton) {
        self.view.endEditing(true)
        func showFilterAtPeriod() {
            filterArray = periodFilters
            lastSelectedFilter = .period
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 760
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
        }
        if tableViewFilter.isHidden {
            showFilterAtPeriod()
        } else {
            switch lastSelectedFilter {
            case .period:
                tableViewFilter.isHidden = true
            default:
                showFilterAtPeriod()
            }
        }
        lastSelectedFilter = .period
    }
    
    @IBAction func sortByTransactionID(_ sender: UIButton) {
        func sortTransactionID(orderType: ComparisonResult) -> [ReportTransactionRecord]{
            return topUpRecords.sorted {
                guard let transID1 = $0.transID, let transID2 = $1.transID else { return false }
                return transID1.localizedStandardCompare(transID2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            topUpRecords = sortTransactionID(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            topUpRecords = sortTransactionID(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewTopUpRecharge.reloadData()
    }
    
    @IBAction func sortByAmount(_ sender: UIButton) {
        func sortAmount(orderType: ComparisonResult) -> [ReportTransactionRecord]{
            return topUpRecords.sorted {
                guard let amount1 = $0.amount, let amount2 = $1.amount else { return false }
                return amount1.compare(amount2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            topUpRecords = sortAmount(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            topUpRecords = sortAmount(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewTopUpRecharge.reloadData()
    }
    
    @IBAction func sortByMobileNumber(_ sender: UIButton) {
        func sortMobileNumber(orderType: ComparisonResult) -> [ReportTransactionRecord]{
            return topUpRecords.sorted {
                guard let phNumber1 = $0.destination, let phNumber2 = $1.destination else { return false }
                return phNumber1.compare(phNumber2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            topUpRecords = sortMobileNumber(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            topUpRecords = sortMobileNumber(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewTopUpRecharge.reloadData()
    }
    
    @IBAction func sortByBonus(_ sender: UIButton) {
        func sortBonus(orderType: ComparisonResult) -> [ReportTransactionRecord]{
            return topUpRecords.sorted {
                guard let bonus1 = $0.bonus, let bonus2 = $1.bonus else { return false }
                return bonus1.compare(bonus2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            topUpRecords = sortBonus(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            topUpRecords = sortBonus(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewTopUpRecharge.reloadData()
    }
    
    @IBAction func sortByCashBack(_ sender: UIButton) {
        func sortCashBack(orderType: ComparisonResult) -> [ReportTransactionRecord] {
            return topUpRecords.sorted {
                guard let cashBack1 = $0.cashBack, let cashBack2 = $1.cashBack else { return false }
                return cashBack1.compare(cashBack2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            topUpRecords = sortCashBack(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            topUpRecords = sortCashBack(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewTopUpRecharge.reloadData()
    }
    
    @IBAction func sortByBalance(_ sender: UIButton) {
        func sortBalance(orderType: ComparisonResult) -> [ReportTransactionRecord]{
            return topUpRecords.sorted {
                guard let balance1 = $0.walletBalance, let balance2 = $1.walletBalance else { return false }
                return balance1.compare(balance2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            topUpRecords = sortBalance(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            topUpRecords = sortBalance(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewTopUpRecharge.reloadData()
    }
    
    @IBAction func sortByDate(_ sender: UIButton) {
        func sortDates(orderType: ComparisonResult) -> [ReportTransactionRecord]{
            return topUpRecords.sorted {
                guard let date1 = $0.transactionDate, let date2 = $1.transactionDate else { return false }
                return date1.compare(date2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            topUpRecords = sortDates(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            topUpRecords = sortDates(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewTopUpRecharge.reloadData()
    }
    
    @IBAction func configureDatePickerOne(_ sender: UIButton) {
        configureDatePicker(tag: 0)
    }
    
    @IBAction func configureDatePickerTwo(_ sender: UIButton) {
        if buttonDateOne.isHidden {
            configureDatePicker(tag: 2)
        } else {
            configureDatePicker(tag: 1)
        }
    }
    
    @IBAction func shareAllTransactionInExcel(_ sender: UIButton) {
        viewShadow.isHidden = true
        viewToShareAllRecords.isHidden = true
        removeGestureFromShadowView()
        
        if topUpRecords.count == 0 {
            return
        }
        var excelTxt = "Transaction ID,Transaction Amount (MMK),Account Holder Number, Destination Number,Bonus Points ,Cash Back (MMK), Balance (MMK), Date & Time, Remarks\n"
        var totalAmount: NSDecimalNumber = 0
        var bonusTransAmount: NSDecimalNumber = 0
        var cashBackAmt: NSDecimalNumber = 0
        for record in topUpRecords {
//            let source = "XXXXXXXXXX"
            var recText = record.transID ?? ""
            var phNo = ""
            if let countryCode = record.countryCode, let destination = record.destination {
                phNo = "(\(countryCode))\(destination)"
            } else {
               phNo = CodeSnippets.getMobileNumber(transRecord: record)
            }
            let userNo = CodeSnippets.getMobileNumWithBrackets(countryCode: UserModel.shared.countryCode, number: UserModel.shared.formattedNumber)

            if let tranAmount = record.amount {
                totalAmount = totalAmount.adding(tranAmount)
            }
            if let bonAmount = record.bonus {
                bonusTransAmount = bonusTransAmount.adding(bonAmount)
            }
            if let cashBAmount = record.cashBack {
                cashBackAmt = cashBackAmt.adding(cashBAmount)
            }
            var descWithNoComa = ""
            if let newDesc = record.desc {
                descWithNoComa = newDesc.replacingOccurrences(of: ",", with: "")
            }
            let date = record.transactionDate?.stringValue().replacingOccurrences(of: ",", with: "")
            recText = recText + ",\(record.amount?.toString() ?? ""),\(userNo),\(phNo),\(record.bonus?.toString() ?? ""),\(record.cashBack?.toString() ?? ""),\(record.walletBalance?.toString() ?? ""),\(date ?? ""),\(descWithNoComa)\n"
            excelTxt.append(recText)
        }
        excelTxt.append("\nTotal,\(totalAmount.toString()),,,\(bonusTransAmount.toString()),\(cashBackAmt.toString())")

        guard let excelUrl = CodeSnippets.generateExcel(excelDataWithFormat: excelTxt, excelFileName: "OK$ Top-up Report") else {
            println_debug("Error - excel not generated")
            return
        }
        let activityViewController = UIActivityViewController(activityItems: [excelUrl], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
    
    @IBAction func shareAllTransactionsInPdf(_ sender: UIButton) {
        viewShadow.isHidden = true
        viewToShareAllRecords.isHidden = true
        removeGestureFromShadowView()
        
        if topUpRecords.count == 0 {
            return
        }
        
        let pdfView = UIView()
        if let pdfHeaderView = Bundle.main.loadNibNamed("AllTransactionPdfHeader", owner: self, options: nil)?.first as? AllTransactionPdfHeader {
            pdfHeaderView.fillTopUpDetails(list: topUpRecords, dateOne: buttonDateOne.currentTitle ?? "", dateTwo: buttonDateTwo.currentTitle ?? "", currentBalance: CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: String(walletAmount())),
                                           amountRange: buttonAmountFilter.currentTitle ?? "", transactionFilter: buttonTransactionFilter.currentTitle ?? "",
                                           operatorFilter: buttonOperatorFilter.currentTitle ?? "", typeFilter: buttonTypeFilter.currentTitle ?? "",
                                           periodStr: buttonPeriodFilter.currentTitle ?? "")
            let newHeight = pdfHeaderView.viewHeaderForTopUpRecharge.frame.maxY
            pdfHeaderView.frame = CGRect(x: 0, y: 0, width: pdfHeaderView.frame.size.width, height: newHeight)
            pdfView.addSubview(pdfHeaderView)
            pdfView.frame = pdfHeaderView.bounds
            let xPos: CGFloat = pdfHeaderView.viewHeaderForTopUpRecharge.frame.minX
            var yPos: CGFloat = pdfView.frame.maxY
            var index = 0
            for record in topUpRecords {
                if let pdfBodyView = Bundle.main.loadNibNamed("TopUpPdfBody", owner: self, options: nil)?.first as? TopUpPdfBody {
                    pdfBodyView.fillDetails(record: record)
                    let recordHeight = pdfBodyView.frame.size.height
                    let yPosInPage = Int(yPos) % 842
                    if (yPosInPage + Int(recordHeight)) > 842 {
                        yPos = yPos + CGFloat((842 - yPosInPage) + 30 + 30)
                    }
                    pdfBodyView.frame = CGRect(x: xPos, y: yPos, width: pdfBodyView.frame.size.width, height: recordHeight)
                    yPos = yPos + recordHeight + 2
                    pdfView.addSubview(pdfBodyView)
                }
                if index == 49 {
                    break
                }
                index += 1
            }
            pdfView.frame = CGRect(x: 0, y: 0, width: pdfView.frame.size.width, height: yPos + 30)
            guard let pdfUrl = CodeSnippets.generatePDF(viewForPDF: pdfView, pdfFile: "OK$ Top-up Report") else {
                println_debug("Error - pdf not generated")
                return
            }
            let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
            DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
        }
    }
}

// MARK: - UITextFieldDelegate
extension ReportTopUpViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tableViewFilter.isHidden = true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        tableViewTopUpRecharge.contentOffset = CGPoint(x: 0, y: 0)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            if updatedText.count > 50 || (updatedText == "" && text == "") {
                return false
            }
            if string != "\n" {
                applyFilter(searchText: updatedText)
            }
        }
        return true
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ReportTopUpViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        switch tableView.tag {
        case 0:
            if topUpRecords.count == 0 {
                let noDataLabel = UILabel(frame: tableView.bounds)
                noDataLabel.text = "No Records".localized
                noDataLabel.font = UIFont(name: appFont, size: 18)
                noDataLabel.textAlignment = .center
                tableView.backgroundView = noDataLabel
            } else {
                tableView.backgroundView = nil
            }
        default:
            break
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView.tag {
        case 0:
            return topUpRecords.count
        case 1:
            return filterArray.count
        case 2:
            return moreOptionsArray.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView.tag {
        case 0:
            let topUpRechargeCell = tableView.dequeueReusableCell(withIdentifier: topUpRechargeCellID, for: indexPath) as? TopUpRechargeCell
            topUpRechargeCell!.configureTopUpRechargeCell(topUpRecord: topUpRecords[indexPath.row])
            return topUpRechargeCell!
        case 1:
            let filterCell = tableView.dequeueReusableCell(withIdentifier: ConstantsXibCell.filterTableViewCell, for: indexPath) as? FilterTableViewCell
            filterCell?.configureTopUpFilter(filterName: filterArray[indexPath.row])
            return filterCell!
        case 2:
            let filterCell = tableView.dequeueReusableCell(withIdentifier: ConstantsXibCell.filterTableViewCell, for: indexPath) as? FilterTableViewCell
            filterCell?.configureMoreOptionInTopUp(filterName: moreOptionsArray[indexPath.row])
            return filterCell!
        default:
            let topUpRechargeCell = tableView.dequeueReusableCell(withIdentifier: topUpRechargeCellID, for: indexPath) as? TopUpRechargeCell
            return topUpRechargeCell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView.tag {
        case 0:
            break
        case 1:
            switch lastSelectedFilter {
            case .transaction:
                buttonTransactionFilter.setTitle(filterArray[indexPath.row].localized, for: .normal)
                tableViewFilter.isHidden = true
                applyFilter()
            case .amount:
                buttonAmountFilter.setTitle(filterArray[indexPath.row].localized, for: .normal)
                tableViewFilter.isHidden = true
                applyFilter()
            case .operatorFilter:
                buttonOperatorFilter.setTitle(filterArray[indexPath.row].localized, for: .normal)
                tableViewFilter.isHidden = true
                applyFilter()
            case .type:
                buttonTypeFilter.setTitle(filterArray[indexPath.row].localized, for: .normal)
                tableViewFilter.isHidden = true
                applyFilter()
            case .period:
                tableViewFilter.isHidden = true
                showDateOptions(selectedOption: filterArray[indexPath.row].localized)
            }
        case 2:
            viewShadow.isHidden = false
            switch indexPath.row {
            case 0:
                if let image = UIImage(named: "topUpCashBack") {
                    imgViewInCircle.image = image
                }
                labelInCircle.text = "Total Amount".localized + " " + "(MMK)".localized
                amountInCircle.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: totalTransactionAmount.toString())
            case 1:
                if let image = UIImage(named: "topUpCashBack") {
                    imgViewInCircle.image = image
                }
                labelInCircle.text = "Total Cash Back".localized + " " + "(MMK)".localized
                amountInCircle.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: totalCashBackAmount.toString())
            case 2:
                if let image = UIImage(named: "totalTransaction") {
                    imgViewInCircle.image = image
                }
                labelInCircle.text = "Total Transactions".localized
                amountInCircle.text = "\(initialRecords.count)"
            default:
                break
            }
            viewCircle.isHidden = false
            tableViewOption.isHidden = true
            circleViewTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: false)
        default:
            break
        }
    }
}

extension ReportTopUpViewController: UIScrollViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        targetContentOffset.pointee.y = max(targetContentOffset.pointee.y - 1, 1)
    }
}

// MARK: - Additional Methods
extension ReportTopUpViewController: NavigationSetUpProtocol {
    /// It set up the navigation bar
    func setUpNavigation() {
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationController?.navigationBar.barTintColor        = ConstantsColor.navigationHeaderTopUpRecharge
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: titleViewWidth, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        
        let searchButton = UIButton(type: .custom)
        searchButton.setImage(#imageLiteral(resourceName: "reportSearch").withRenderingMode(.alwaysOriginal), for: .normal)
        searchButton.addTarget(self, action: #selector(showSearchView), for: .touchUpInside)
        
        let shareButton = UIButton(type: .custom)
        shareButton.setImage(#imageLiteral(resourceName: "reportShare").withRenderingMode(.alwaysOriginal), for: .normal)
        shareButton.addTarget(self, action: #selector(showShareOption), for: .touchUpInside)
        
        let graphButton = UIButton(type: .custom)
        graphButton.setImage(#imageLiteral(resourceName: "reportGraph").withRenderingMode(.alwaysOriginal), for: .normal)
        graphButton.addTarget(self, action: #selector(showGraph), for: .touchUpInside)
        
        let moreButton = UIButton(type: .custom)
        moreButton.setImage(#imageLiteral(resourceName: "topUpMore").withRenderingMode(.alwaysOriginal), for: .normal)
        moreButton.addTarget(self, action: #selector(showMoreOption), for: .touchUpInside)
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "tabBarBack").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        let ypos = (titleViewSize.height - 30 ) / 2
        if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 220, y: ypos, width: 30, height: 30)
            shareButton.frame = CGRect(x: navTitleView.frame.size.width - 180, y: ypos, width: 30, height: 30)
            graphButton.frame = CGRect(x: navTitleView.frame.size.width - 140, y: ypos, width: 30, height: 30)
            moreButton.frame = CGRect(x: navTitleView.frame.size.width - 100, y: ypos, width: 30, height: 30)
            backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        }
        else{
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 150, y: ypos, width: 30, height: 30)
            shareButton.frame = CGRect(x: navTitleView.frame.size.width - 110, y: ypos, width: 30, height: 30)
            graphButton.frame = CGRect(x: navTitleView.frame.size.width - 70, y: ypos, width: 30, height: 30)
            moreButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
            backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
            
        }
        
       
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 320, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text = ConstantsController.reportTopUpViewController.screenTitle.localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 23) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        navTitleView.addSubview(searchButton)
        navTitleView.addSubview(shareButton)
        navTitleView.addSubview(graphButton)
        navTitleView.addSubview(moreButton)
        self.navigationItem.titleView = navTitleView
    }
    
    /// It shows the home screen
    @objc func backAction() {
//        AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
//        self.navigationController?.popViewController(animated: true)
        if isSearchViewShow {
            hideSearchView()
        } else {
            AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension ReportTopUpViewController: ReportModelDelegate {
    func apiResult(message: String?, statusCode: String?) {
        DispatchQueue.main.async {
            self.fetchRecordsFromDB()
            self.tableViewTopUpRecharge.reloadData()
            self.resetFilterButtons()
            self.resetSortButtons(sender: UIButton())
            self.refreshControl.endRefreshing()
        }
    }
    func handleCustomError(error: CustomError) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}
