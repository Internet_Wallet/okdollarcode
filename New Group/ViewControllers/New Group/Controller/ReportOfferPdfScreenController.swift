//
//  ReportOfferPdfScreenController.swift
//  OK
//
//  Created by E J ANTONY on 06/08/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportOfferPdfScreenController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var labelHeader: UILabel!{
        didSet {
            labelHeader.font = UIFont(name : appFont, size : appFontSizeReport)
            labelHeader.text = "Offers Transaction Detail".localized
        }
    }
    @IBOutlet weak var lblOneTit: UILabel!{
        didSet
        {
            lblOneTit.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblTwoTit: UILabel!{
        didSet
        {
            lblTwoTit.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblThreeTit: UILabel!{
        didSet
        {
            lblThreeTit.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblBillAmountTit: UILabel!{
        didSet
        {
            lblBillAmountTit.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblDiscAmountTit: UILabel!{
        didSet
        {
            lblDiscAmountTit.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblSaleQuantityTit: UILabel!
        {
    didSet {
        lblSaleQuantityTit.font = UIFont(name : appFont, size : appFontSizeReport)
        lblSaleQuantityTit.text = "Sale Quantity".localized
    }
    }
    @IBOutlet weak var lblFreeQuantityTit: UILabel!{
        didSet {
            lblFreeQuantityTit.font = UIFont(name : appFont, size : appFontSizeReport)
            lblFreeQuantityTit.text = "Free Quantity".localized
        }
    }
    @IBOutlet weak var lblTotalNetAmountTit: UILabel!{
        didSet
        {
            lblTotalNetAmountTit.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblPromoNameTit: UILabel!{
        didSet
        {
            lblPromoNameTit.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblPromoCodeTit: UILabel!{
        didSet
        {
            lblPromoCodeTit.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblDateTimeTit: UILabel!{
        didSet
        {
            lblDateTimeTit.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblOneVal: UILabel!{
        didSet
        {
            lblOneVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblTwoVal: UILabel!{
        didSet
        {
            lblTwoVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblThreeVal: UILabel!{
        didSet
        {
            lblThreeVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblBillAmountVal: UILabel!{
        didSet
        {
            lblBillAmountVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblDiscAmountVal: UILabel!{
        didSet
        {
            lblDiscAmountVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblSaleQuantityVal: UILabel!{
        didSet
        {
            lblSaleQuantityVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblFreeQuantityVal: UILabel!{
        didSet
        {
            lblFreeQuantityVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblTotalNetAmountVal: UILabel!{
        didSet
        {
            lblTotalNetAmountVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblPromoNameVal: UILabel!{
        didSet
        {
            lblPromoNameVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblPromoCodeVal: UILabel!{
        didSet
        {
            lblPromoCodeVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblDateTimeVal: UILabel!{
        didSet
        {
            lblDateTimeVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblTotalBillAmountTit: UILabel!{
        didSet
        {
            lblTotalBillAmountTit.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblTotalBillAmountVal: UILabel!{
        didSet
        {
            lblTotalBillAmountVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblTotalDiscAmountTit: UILabel!{
        didSet
        {
            lblTotalDiscAmountTit.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblTotalDiscAmountVal: UILabel!{
        didSet
        {
            lblTotalDiscAmountVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblPercentageTit: UILabel!{
        didSet
        {
            lblPercentageTit.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblPercentageVal: UILabel!{
        didSet
        {
            lblPercentageVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblTotalRecvdAmountTit: UILabel!{
        didSet
        {
            lblTotalRecvdAmountTit.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblTotalRecvdAmountVal: UILabel!{
        didSet
        {
            lblTotalRecvdAmountVal.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var imgViewQR: UIImageView!
    @IBOutlet weak var constraintHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintHQuantity: NSLayoutConstraint!
    @IBOutlet weak var constraintHDiscount: NSLayoutConstraint!
    @IBOutlet weak var constraintTopDiscAmount: NSLayoutConstraint!
    @IBOutlet weak var constraintTopNetAmount: NSLayoutConstraint!
    @IBOutlet weak var constraintTopQR: NSLayoutConstraint!
    @IBOutlet weak var contentView: UIView!
    
    //MARK:- Properties
    enum ScreenType {
        case recordSummary, totalSummary
    }
    var sourceDataArray = [OfferReporFilterList]()
    var screenType = ScreenType.recordSummary
    var amountType = 0
    private var isHideDiscAmount: Bool {
        get{
            return constraintHDiscount.constant == 0 ? true : false
        }
        set(newValue) {
            if newValue {
                constraintHDiscount.constant = 0
                self.view.layoutIfNeeded()
            } else {
                constraintHDiscount.constant = 165
                self.view.layoutIfNeeded()
            }
        }
    }
    private var isHideQuantity: Bool {
        get{
            return constraintHQuantity.constant == 0 ? true : false
        }
        set(newValue) {
            if newValue {
                constraintHQuantity.constant = 0
                self.view.layoutIfNeeded()
            } else {
                constraintHQuantity.constant = 110
                self.view.layoutIfNeeded()
            }
        }
    }
    private let quantityH = 110
    private let discH = 50
    
    //MARK:- Views life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigation()
        setTitles()
        loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Methods
    private func initializeTitles() {
        switch screenType {
        case .recordSummary:
            constraintHeight.constant = 0
            self.view.layoutIfNeeded()
            lblOneTit.text = ""
            lblOneVal.text = ""
            lblTwoTit.text = "Account Name".localized
            lblThreeTit.text = "Account Number".localized
            lblDiscAmountTit.text = "Discount Percentage".localized
        case .totalSummary:
            constraintHeight.constant = 50
            self.view.layoutIfNeeded()
            lblOneTit.text = "Advance Merchant".localized
            lblTwoTit.text = "Safety Cashier".localized
            lblThreeTit.text = "Transaction".localized
            lblDiscAmountTit.text = "Discount Amount".localized
        }
    }
    
    private func setTitles() {
        self.initializeTitles()
        lblBillAmountTit.text = "Bill Amount".localized
        lblTotalNetAmountTit.text = "Net Amount".localized
        lblPromoNameTit.text = "Promotion Name".localized
        lblPromoCodeTit.text = "Promotion Code".localized
        lblDateTimeTit.text = "Date & Time".localized
        lblTotalBillAmountTit.text = "Bill Amount".localized
        lblTotalDiscAmountTit.text = "Discount Amount".localized
        lblTotalRecvdAmountTit.text = "Net Amount".localized
        lblPercentageTit.text = "Discount Percentage".localized
    }
    
    private func loadData() {
        switch screenType {
        case .recordSummary:
            populateRecordData()
        case .totalSummary:
            populateSummaryRecord()
        }
    }
    
    private func populateRecordData() {
        
        guard sourceDataArray.count > 0 else {
            return
        }
        var qrString = ""
        var aMerchantNo = ""
        var totalBillAmount = ""
        var totalDiscAmount = ""
        var transDate = ""
        var promoName = ""
        var promoCode = ""
        let data = sourceDataArray[0]
        if let aMNo = data.advanceMerchantNumber {
            aMerchantNo = aMNo
        }
        if let cName = data.promotionUsedCustomerName {
            lblTwoVal.text = cName
        }
        if var mN = data.promotionUsedMobileNumber {
            self.lblThreeVal.text = mN
            if mN.hasPrefix("0095") {
                mN = "0" + String(mN.substring(from: 4))
                self.lblThreeVal.text = mN
            }
        }
        if let nAmount = data.totalNetAmount {
            let attrVal = CodeSnippets.getCurrencyInAttributed(withString: "\(nAmount)", amountSize: 15,
                                                               amountColor: .black, currencyDenomSize: 12,
                                                               currencyDenomColor: .black)
            lblBillAmountVal.attributedText = attrVal
            lblTotalBillAmountVal.attributedText = attrVal
        }
        
        //New Record
        if amountType != 2 {
            isHideDiscAmount = false
            if let discAmount = data.totalDiscountAmount {
                let attrVal = CodeSnippets.getCurrencyInAttributed(withString: "\(discAmount)", amountSize: 15,
                                                                   amountColor: .black, currencyDenomSize: 12,
                                                                   currencyDenomColor: .black)
                lblDiscAmountVal.attributedText = attrVal
                
                let amount = Float(data.totalNetAmount ?? 0)
                let percentageValue = Float((Float(discAmount) * 100) / amount)

                lblPercentageVal.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(percentageValue)") + " %"
                
                lblTotalDiscAmountVal.attributedText = attrVal
                totalDiscAmount = "\(discAmount)"
                lblDiscAmountTit.text = "Discount Amount".localized
            }
        } else {
            isHideDiscAmount = true
            let discAmount = data.totalDiscountAmount ?? 0
            let attrVal = CodeSnippets.getCurrencyInAttributed(withString: "\(discAmount)", amountSize: 15,
                                                               amountColor: .black, currencyDenomSize: 12,
                                                               currencyDenomColor: .black)
            lblTotalDiscAmountVal.attributedText = attrVal
            let amount = Float(data.totalNetAmount ?? 0)
            let percentageValue = Float((Float(discAmount) * 100) / amount)
            lblPercentageVal.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(percentageValue)") + " %"
        }
        
        if amountType == 2 || amountType == 3 || amountType == 4 {
            isHideQuantity = false
                if let bq = data.buyingQty {
                    self.lblSaleQuantityVal.text = String(bq)
                }
                if let fq = data.freeQty {
                    self.lblFreeQuantityVal.text = String(fq)
                }
        } else if amountType == 5 || amountType == 6 || amountType == 7 {
            if let bq = data.buyingQty {
                self.lblSaleQuantityVal.text = String(bq)
            }
        }
        else {
            isHideQuantity = true
        }
        
        if let bAmount = data.totalBillAmount {
            let attrVal = CodeSnippets.getCurrencyInAttributed(withString: "\(bAmount)", amountSize: 15,
                                                               amountColor: .black, currencyDenomSize: 12,
                                                               currencyDenomColor: .black)
            lblTotalNetAmountVal.attributedText = attrVal
            lblTotalRecvdAmountVal.attributedText = attrVal
            totalBillAmount = "\(bAmount)"
        }
        if let pName = data.promotionName {
            lblPromoNameVal.text = "\(pName)"
            promoName = "\(pName)"
        }
        if let pCode = data.promotionCode {
            lblPromoCodeVal.text = "\(pCode)"
            promoCode = "\(pCode)"
        }
        let dateStr = data.transactionTime
        if let dateVal = dateStr?.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") {
            transDate = dateVal.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy\nHH:mm:ss")
            lblDateTimeVal.text = "\(transDate)"
        }
        self.view.layoutIfNeeded()
        if ((lblTotalDiscAmountTit.frame.minY <= 842) && (lblTotalDiscAmountTit.frame.maxY > 842)) {
            constraintTopDiscAmount.constant = 872 - lblTotalDiscAmountTit.frame.minY
            self.view.layoutIfNeeded()
        } else if ((lblTotalRecvdAmountTit.frame.minY <= 842) && (lblTotalRecvdAmountTit.frame.maxY > 842)) {
            constraintTopNetAmount.constant = 872 - lblTotalRecvdAmountTit.frame.minY
            self.view.layoutIfNeeded()
        } else if ((imgViewQR.frame.minY <= 842) && (imgViewQR.frame.maxY > 842)) {
            constraintTopQR.constant = 872 - imgViewQR.frame.minY
            self.view.layoutIfNeeded()
        }
        qrString = "\(aMerchantNo),\(data.promotionUsedMobileNumber ?? ""), \(promoCode),\(promoName),\(totalBillAmount),\(totalDiscAmount),\(transDate)"
      
        
//          let str2 = "OK-\(senderName)-\(valueString)-\(transDateStr)-\(transID)-\(senderNumber)-\(balance)-des-\(receiverNumber)-\(trasnstype)-\(cashBackAmount)-\(bonus)-\(lattitude),\(longitude)-0-\(gender)-\(age)-\(state)-\(receiverName)-\(receiverBusinessName)"
        if let qrImage = generateQRCode(qrStr: qrString) {
            imgViewQR.image = qrImage
        }
    }
    
    private func populateSummaryRecord() {
        let totalTransaction = sourceDataArray.count
        var totalBillAmount: Double = 0.0
        var totalDiscAmount: Double = 0.0
        var totalNetRecvdAmount: Double = 0.0
        var totalBuyingQty: Int = 0
        var totalFreeQty: Int = 0
        
        var promotionName = ""
        var promoCode = ""
        var transDate = ""
        var advanceMerchantNo = ""
        if sourceDataArray.count > 0 {
            if let pName = sourceDataArray[0].promotionName {
                promotionName = pName
            }
            if let pCode = sourceDataArray[0].promotionCode {
                promoCode = pCode
            }
            if let aMNo = sourceDataArray[0].advanceMerchantNumber {
                advanceMerchantNo = aMNo
            }
        }
        transDate = Date().stringValueWithOutUTC(dateFormatIs: "EEE, dd-MMM-yyyy\nHH:mm:ss")
        var aMerc = Set<String>()
        var dMerc = Set<String>()
        for data in sourceDataArray {
            if let mNum = data.advanceMerchantName {
                aMerc.insert(mNum)
            }
            if let dNum = data.dummyMerchantName {
                dMerc.insert(dNum)
            }
            if let nAmount = data.totalBillAmount {
                totalNetRecvdAmount += nAmount
            }
            if let discAmount = data.totalDiscountAmount {
                totalDiscAmount += discAmount
            }
            if let bAmount = data.totalNetAmount {
                totalBillAmount += bAmount
            }
            if let buyingQty = data.buyingQty {
                totalBuyingQty += buyingQty
            }
            if let freeQty = data.freeQty {
                totalFreeQty += freeQty
            }
        }
        lblOneVal.text = "\(aMerc.count)"
        lblTwoVal.text = "\(dMerc.count)"
        lblThreeVal.text = "\(totalTransaction)"
        lblBillAmountVal.attributedText = CodeSnippets.getCurrencyInAttributed(withString: "\(totalBillAmount)", amountSize: 15,
                                                                               amountColor: .black, currencyDenomSize: 12,
                                                                               currencyDenomColor: .black)
       
        if amountType != 2 {
            isHideDiscAmount = false
            lblDiscAmountVal.attributedText = CodeSnippets.getCurrencyInAttributed(withString: "\(totalDiscAmount)", amountSize: 15,
                                                                                   amountColor: .black, currencyDenomSize: 12,
                                                                                   currencyDenomColor: .black)
            
            let percentageValue = Float((Float(totalDiscAmount) * 100) / Float(totalBillAmount))
            
            lblPercentageVal.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(percentageValue)") + " %"
            
        } else {
            isHideDiscAmount = true
        }
        
        if amountType == 2 || amountType == 3 || amountType == 4 {
            isHideQuantity = false
                self.lblSaleQuantityVal.text = String(totalBuyingQty)
                self.lblFreeQuantityVal.text = String(totalFreeQty)
        } else {
            isHideQuantity = true
        }
        
        lblTotalNetAmountVal.attributedText = CodeSnippets.getCurrencyInAttributed(withString: "\(totalNetRecvdAmount)", amountSize: 15,
                                                                                   amountColor: .black, currencyDenomSize: 12,
                                                                                   currencyDenomColor: .black)
        lblTotalBillAmountVal.attributedText = CodeSnippets.getCurrencyInAttributed(withString: "\(totalBillAmount)", amountSize: 15,
                                                                                    amountColor: .black, currencyDenomSize: 12,
                                                                                    currencyDenomColor: .black)
        lblTotalDiscAmountVal.attributedText = CodeSnippets.getCurrencyInAttributed(withString: "\(totalDiscAmount)", amountSize: 15,
                                                                                    amountColor: .black, currencyDenomSize: 12,
                                                                                    currencyDenomColor: .black)
        lblTotalRecvdAmountVal.attributedText = CodeSnippets.getCurrencyInAttributed(withString: "\(totalNetRecvdAmount)", amountSize: 15,
                                                                                     amountColor: .black, currencyDenomSize: 12,
                                                                                     currencyDenomColor: .black)
        lblPromoNameVal.text = promotionName
        lblPromoCodeVal.text = promoCode
        lblDateTimeVal.text = transDate
        self.view.layoutIfNeeded()
        if ((lblTotalDiscAmountTit.frame.minY <= 842) && (lblTotalDiscAmountTit.frame.maxY > 842)) {
            constraintTopDiscAmount.constant = 872 - lblTotalDiscAmountTit.frame.minY
            self.view.layoutIfNeeded()
        } else if ((lblTotalRecvdAmountTit.frame.minY <= 842) && (lblTotalRecvdAmountTit.frame.maxY > 842)) {
            constraintTopNetAmount.constant = 872 - lblTotalRecvdAmountTit.frame.minY
            self.view.layoutIfNeeded()
        } else if ((imgViewQR.frame.minY <= 842) && (imgViewQR.frame.maxY > 842)) {
            constraintTopQR.constant = 872 - imgViewQR.frame.minY
            self.view.layoutIfNeeded()
        }
        let qrString = "\(advanceMerchantNo),\(promoCode),\(promotionName),\(totalNetRecvdAmount),\(totalDiscAmount),\(transDate)"
        if let qrImage = generateQRCode(qrStr: qrString) {
            imgViewQR.image = qrImage
            
        }
    }
    
    private func generateQRCode(qrStr: String) -> UIImage?{
        let qrImageObject = PTQRGenerator()
        guard let qrImage =  qrImageObject.getQRImage(stringQR: qrStr, withSize: 10) else { return nil }
        return qrImage
    }
    
    //MARK: - Target Methods
    @objc private func shareOption() {
        guard let pdfUrl = CodeSnippets.generatePDF(viewForPDF: self.contentView, pdfFile: "OK$ Offer_Report") else {
            println_debug("Error - pdf not generated")
            return
        }
        let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
}

// MARK: - Additional Methods
extension ReportOfferPdfScreenController: NavigationSetUpProtocol {
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let shareButton = UIButton(type: .custom)
        shareButton.setImage(#imageLiteral(resourceName: "reportShareWhite").withRenderingMode(.alwaysOriginal), for: .normal)
        shareButton.addTarget(self, action: #selector(shareOption), for: .touchUpInside)
        if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
            shareButton.frame = CGRect(x: navTitleView.frame.size.width - 100, y: ypos, width: 30, height: 30)
        }
        else{
        shareButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 25, height: 25)
        }
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  ConstantsController.reportOfferPdfScreenController.screenTitle.localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(shareButton)
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
}
