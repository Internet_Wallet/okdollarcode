//
//  BillPaymentsPdfBody.swift
//  OK
//
//  Created by iMac on 5/14/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class BillPaymentsPdfBody: UIView {

    @IBOutlet weak var transactionID: UILabel!{
        didSet{
            self.transactionID.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var debitAmount: UILabel!{
        didSet{
            self.debitAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var billType: UILabel!{
        didSet{
            self.billType.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var bonus: UILabel!{
        didSet{
            self.bonus.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var cashBack: UILabel!{
        didSet{
            self.cashBack.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var openingBalance: UILabel!{
        didSet{
            self.openingBalance.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var balanceWallet: UILabel!{
        didSet{
            self.balanceWallet.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var transactionDate: UILabel!{
        didSet{
            self.transactionDate.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var transDescription: UILabel!{
        didSet{
            self.transDescription.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    
    
    func fillDetails(record: ReportTransactionRecord) {
        self.transactionID.text = record.transID ?? ""
        cashBack.text = "0" //For receiver side, since no cashback for receiver
        if let transAmount = record.amount {
            self.debitAmount.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: transAmount.toString())
        }
        
        var billtype = ""
        if record.desc?.contains(find: "ELECTRICITY") ?? false {
            billtype = "ELECTRICITY"
        } else if record.desc?.contains(find: "POSTPAID") ?? false {
            billtype = "POSTPAID"
        } else if record.desc?.contains(find: "LANDLINE") ?? false {
            billtype = "LANDLINE"
        } else if record.desc?.contains(find: "SUNKING") ?? false {
            billtype = "SUNKING"
        } else if record.desc?.contains(find: "INSURANCE") ?? false {
            billtype = "INSURANCE"
        } else if record.desc?.contains(find: "MERCHANTPAY") ?? false {
            billtype = "MERCHANTPAY"
        } else if record.desc?.contains(find: "DTH") ?? false {
            billtype = "DTH"
        } else if record.desc?.contains(find: "GiftCard") ?? false {
            billtype = "GIFTCARD"
        } else {
            billtype = ""
        }
        
        self.billType.text =  billtype
        
        var openingBal: Double = 0.0
        if let bal = record.walletBalance, let amt = record.amount {
            openingBal = Double(truncating: bal) + Double(truncating: amt)
        }
        let behavior = NSDecimalNumberHandler(roundingMode: NSDecimalNumber.RoundingMode.plain,
                                              scale: 2,
                                              raiseOnExactness: false,
                                              raiseOnOverflow: false,
                                              raiseOnUnderflow: false,
                                              raiseOnDivideByZero: false)
        let calcDN = NSDecimalNumber.init(value: openingBal).rounding(accordingToBehavior: behavior)
        
        self.openingBalance.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: calcDN.toString())
        
        if let balanceAmount = record.walletBalance {
            self.balanceWallet.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: balanceAmount.toString()) 
        }
        
        if let bonusAmount = record.bonus {
            self.bonus.text =  bonusAmount.toStringNoDecimal() //  feeValue -- FEE(server key)
        }
        self.transactionDate.text = ""
        if let transDate = record.transactionDate {
            self.transactionDate.text = transDate.stringValue()
        }
        let desc = record.rawDesc?.removingPercentEncoding?.replacingOccurrences(of: "+", with: " ").components(separatedBy: "-")
        if record.rawDesc?.contains(find: "ELECTRICITY") ?? false {
            let dateFormat = self.formatDate(dateString: desc?.last ?? "")
            var name: [String]?
            if desc?[9].contains(find: "Name :") ?? false {
                name = desc?[9].components(separatedBy: "Name :")
            } else if desc?[10].contains(find: "Name :") ?? false {
                name = desc?[10].components(separatedBy: "Name :")
            }
            var diviLoc = ""
            if desc?[7].contains(find: "Division") ?? false {
                let division = desc?[7].components(separatedBy: "Division :")
                if let divLocal = division?[1] {
                    let arr = divLocal.components(separatedBy: "Division")
                    diviLoc = arr[0]
                }
            } else if desc?[8].contains(find: "Division") ?? false {
                let division = desc?[8].components(separatedBy: "Division :")
                if let divLocal = division?[1] {
                    let arr = divLocal.components(separatedBy: "Division")
                    diviLoc = arr[0]
                }
            }
            
            var township: [String]?
            if desc?[8].contains(find: "Township") ?? false {
                township = desc?[8].components(separatedBy: "Township")
            } else if desc?[9].contains(find: "Township") ?? false {
                township = desc?[9].components(separatedBy: "Township")
            }
            var meterNo = ""
            var payFor:[String]?
            if desc?[6].contains(find: "PayFor:") ?? false {
                meterNo = desc?[2] ?? ""
                payFor = desc?[6].components(separatedBy: "PayFor:")
            } else if desc?[7].contains(find: "PayFor:") ?? false {
                meterNo = "\(desc?[2] ?? "")-\(desc?[3] ?? "")"
                payFor = desc?[7].components(separatedBy: "PayFor:")
            }
            let fullDesc = "\(meterNo) \(name?[1] ?? ""), \(township?[1] ?? "") Township, \(diviLoc), \(diviLoc) Division, Bill Month: \(dateFormat.0), Due Date \(dateFormat.1), \(payFor?[1] ?? "")"
            self.transDescription.text = fullDesc
        }  else if record.rawDesc?.contains(find: "DTH") ?? false {
            self.transDescription.text = record.rawDesc?.components(separatedBy: "Destination").first ?? ""
        }  else {
            self.transDescription.text = record.desc
        }
        
        if let cashBackAmount = record.cashBack {
            if cashBackAmount.compare(0.0) == .orderedDescending {
                cashBack.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: cashBackAmount.toString()) //    feeAmount -- fee(server key)
            }
        }
    }
    
    func formatDate(dateString: String) -> (String, String) {
        let dateStr = dateString.components(separatedBy: "Due Date :")
        if dateStr.count > 0 {
            let currentDate = dateStr[1].replacingOccurrences(of: "#", with: "")
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy hh:mm:ss a"
            let date = formatter.date(from: currentDate)
            let previousMonth = Calendar.current.date(byAdding: .month, value: -1, to: date ?? Date())
            formatter.dateFormat = "EEE, dd-MMM-yyyy"
            let strDate = formatter.string(from: date ?? Date())
            formatter.dateFormat = "MMMM"
            let prevMonth = formatter.string(from: previousMonth ?? Date())
            return(prevMonth, strDate)
        }
        return ("", "")
    }
}

