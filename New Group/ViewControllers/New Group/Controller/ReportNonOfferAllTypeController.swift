//
//  ReportNonOfferAllTypeController.swift
//  OK
//
//  Created by iMac on 3/26/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol ReportNonOfferAllTypeDelegate {
    func processWithType(type : String)
}

class ReportNonOfferAllTypeController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var tblTypeList: UITableView!
    @IBOutlet weak var viewShadow: UIView!
    var delegate: ReportNonOfferAllTypeDelegate?

   // var typeArray : [[String:Any]] = [[:]]
    
    var typeArray : [NonOfferAllTransactionModel] = []
    
    @IBOutlet weak var btnCancel: UIButton!{
        didSet{
            btnCancel.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
            btnCancel.setTitle(btnCancel.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    private lazy var modelBReport: ReportBModel = {
        return ReportBModel()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblTypeList.tableFooterView = UIView()
        self.setUpNavigation()
        // Do any additional setup after loading the view.
        getAllTransactionTypeAPi()
    }
    
    @IBAction func cancelController() {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    //MARK:- API Request
    func getAllTransactionTypeAPi() {
        
        println_debug("Get Records")
        let request = NonOfferAllTransactionRequest(mobileNumber: UserModel.shared.mobileNo)
        
        modelBReport.reportBAllTransactionDelegate = self as ReportBModelAllTransaction
        modelBReport.getNonOfferPromotionAllTransactionAPICalling(request: request)
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ReportNonOfferAllTypeController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if typeArray.count == 0 {
            let noDataLabel = UILabel(frame: tableView.bounds)
            noDataLabel.text = "No Records".localized
            noDataLabel.textAlignment = .center
            noDataLabel.font = UIFont(name: appFont, size: 18)
            tableView.backgroundView = noDataLabel
        } else {
            tableView.backgroundView = nil
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return typeArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell
    
        cell.textLabel?.font = UIFont.init(name: appFont, size: 18)

        if indexPath.row == 0 {
            cell.textLabel?.text  = "All Transaction".localized
        }
        else if indexPath.row == 1 {
            cell.textLabel?.text  = "All Offers".localized
        }
        else if indexPath.row == 2 {
            cell.textLabel?.text  = "All Non Offers".localized
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let modelData = typeArray[indexPath.row].transactionTypeName
        //let modelData = typeArray[indexPath.row].safeValueForKey("transactionRequestedTypeName") as! String
        
        var typeStr = ""
        
        if indexPath.row == 0 {
             typeStr = "ALLTRAN"
        }
        else if indexPath.row == 1 {
             typeStr = "PROMOTIONTRAN"
        }
        else if indexPath.row == 2 {
             typeStr = "NONPROMOTIONTRAN"
        }
        
        self.navigationController?.popViewController(animated: true)
        delegate?.processWithType(type : typeStr)

    }
}

// MARK: - Additional Methods
extension ReportNonOfferAllTypeController: NavigationSetUpProtocol {
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  "Transaction Type".localized
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
}

extension ReportNonOfferAllTypeController: ReportBModelAllTransaction {
    
    func handleCustomError(error: CustomError) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
    
    func apiResultnonofferSummary(message: String?, statusCode: String?){
        
        self.typeArray = self.modelBReport.nonofferAllTransactionList
        print("apiResultnonofferAllTransaction called-----\(typeArray.count)")
        tblTypeList.delegate = self
        tblTypeList.dataSource = self
        tblTypeList.reloadData()
    }
}
