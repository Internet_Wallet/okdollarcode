//
//  AllTransactionPdfHeader.swift
//  OK
//
//  Created by ANTONY on 24/02/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class AllTransactionPdfHeader: UIView {
    // MARK: - Outlets
    
    @IBOutlet weak var businessNameHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var accName: UILabel!{
        didSet{
            self.accName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var accNumber: UILabel!{
        didSet{
            self.accNumber.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var email: UILabel!{
        didSet{
            self.email.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var address: UILabel!{
        didSet{
            self.address.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var period: UILabel!{
        didSet{
            self.period.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var creditAmount: UILabel!{
        didSet{
            self.creditAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var debitAmountTextLbl: UILabel!{
        didSet{
            self.debitAmountTextLbl.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var debitAmount: UILabel!{
        didSet{
            self.debitAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var totalTransaction: UILabel!{
        didSet{
            self.totalTransaction.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var balance: UILabel!{
        didSet{
            self.balance.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var dateTime: UILabel!{
        didSet{
            self.dateTime.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var totalTransactionAmount: UILabel!{
        didSet{
            self.totalTransactionAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterTypeBillPayment: UILabel!{
        didSet{
            self.filterTypeBillPayment.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterAmountBillPayment: UILabel!{
        didSet{
            self.filterAmountBillPayment.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterBillTypeBillPayment: UILabel!{
        didSet{
            self.filterBillTypeBillPayment.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterPeriodBillPayment: UILabel!{
        didSet{
            self.filterPeriodBillPayment.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterTypeTopUp: UILabel!{
        didSet{
            self.filterTypeTopUp.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterAmountTopUp: UILabel!{
        didSet{
            self.filterAmountTopUp.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterOperatorTopUp: UILabel!{
        didSet{
            self.filterOperatorTopUp.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterPlanTypeTopUp: UILabel!{
        didSet{
            self.filterPlanTypeTopUp.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterPeriodTopUp: UILabel!{
        didSet{
            self.filterPeriodTopUp.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterTypeReceivedMoney: UILabel!{
        didSet{
            self.filterTypeReceivedMoney.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterAmountReceivedMoney: UILabel!{
        didSet{
            self.filterAmountReceivedMoney.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterPeriodReceivedMoney: UILabel!{
        didSet{
            self.filterPeriodReceivedMoney.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var pdfHeaderLabel: UILabel!{
        didSet{
            self.pdfHeaderLabel.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var viewHeaderForBillPayments: UIView!
    @IBOutlet weak var viewHeaderForTopUpRecharge: UIView!
    @IBOutlet weak var viewHeaderforReceivedMoney: UIView!
    @IBOutlet weak var sideHeader: UILabel!{
        didSet{
            self.sideHeader.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var creditLblAmount: UILabel!{
        didSet{
            self.creditLblAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    @IBOutlet weak var businessNameLbl: UILabel!{
        didSet{
            self.businessNameLbl.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var businessNameVal: UILabel!{
        didSet{
            self.businessNameVal.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var accountTypeLbl: UILabel!{
        didSet{
            self.accountTypeLbl.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var accountTypeVal: UILabel!{
        didSet{
            self.accountTypeVal.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var addressHeight: NSLayoutConstraint!
    
    var enableBillPaymentMode: Bool {
        get {
            return !viewHeaderForBillPayments.isHidden
        }
        set(newValue) {
            if newValue {
                viewHeaderForBillPayments.isHidden = false
                viewHeaderForTopUpRecharge.isHidden = true
                viewHeaderforReceivedMoney.isHidden = true
            } else {
                viewHeaderForBillPayments.isHidden = true
            }
        }
    }
    var enableTopUpMode: Bool {
        get {
            return !viewHeaderForTopUpRecharge.isHidden
        }
        set(newValue) {
            if newValue {
                viewHeaderForTopUpRecharge.isHidden = false
                viewHeaderForBillPayments.isHidden = true
                viewHeaderforReceivedMoney.isHidden = true
            } else {
                viewHeaderForTopUpRecharge.isHidden = true
            }
        }
    }
    var enableReceivedMoneyMode: Bool {
        get {
            return !viewHeaderforReceivedMoney.isHidden
        }
        set(newValue) {
            if newValue {
                viewHeaderForTopUpRecharge.isHidden = true
                viewHeaderForBillPayments.isHidden = true
                viewHeaderforReceivedMoney.isHidden = false
            } else {
                viewHeaderforReceivedMoney.isHidden = true
            }
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func fillBillDetails(list: [ReportTransactionRecord], dateOne: String, dateTwo: String, currentBalance: String, amountRange: String,
                         billType: String, serviceProviderType: String, periodStr: String) {
        self.enableBillPaymentMode = true
        
        self.getCreditAndDebit(list: list, fromBill: true)
        self.pdfHeaderLabel.text = "Bill Payments Statement".localized
//        self.pdfHeaderLabel.textColor = ConstantsColor.navigationHeaderBillPayment
        self.getGeneralDetails()
        self.getPeriodInFormat(periodStr: periodStr, dateOne: dateOne, dateTwo: dateTwo)
        self.balance.text = currentBalance + " MMK"
        self.sideHeader.isHidden = true
        var titleStr = ""
        if billType == "Transaction".localized {
            titleStr = "All"
        } else {
            titleStr = billType
        }
        self.filterTypeBillPayment.text = titleStr
        if amountRange == "Amount".localized {
            titleStr = "All"
        } else {
            titleStr = amountRange
        }
        self.filterAmountBillPayment.text = titleStr
        if serviceProviderType == "Type".localized {
            titleStr = "All"
        } else {
            titleStr = serviceProviderType
        }
        self.filterBillTypeBillPayment.text = titleStr
        if periodStr == "Period".localized {
            titleStr = "All"
        } else {
            titleStr = periodStr
        }
        self.filterPeriodBillPayment.text = titleStr
    }
    
    func fillTopUpDetails(list: [ReportTransactionRecord], dateOne: String, dateTwo: String, currentBalance: String, amountRange: String,
                          transactionFilter: String, operatorFilter: String, typeFilter: String, periodStr: String) {
        self.enableTopUpMode = true
        self.getCreditAndDebit(list: list, fromTopUp: true)
        self.getGeneralDetails()
        self.getPeriodInFormat(periodStr: periodStr, dateOne: dateOne, dateTwo: dateTwo)
        self.pdfHeaderLabel.text = "Top-Up & Recharge Statement".localized
        self.pdfHeaderLabel.textColor = ConstantsColor.navigationHeaderTopUpRecharge
        self.balance.text = currentBalance + " MMK"
        var titleStr = ""
        if transactionFilter == "Transaction".localized {
            titleStr = "All"
        } else {
            titleStr = transactionFilter
        }
        self.filterTypeTopUp.text = titleStr
        if amountRange == "Amount".localized {
            titleStr = "All"
        } else {
            titleStr = amountRange
        }
        self.filterAmountTopUp.text = titleStr
        if operatorFilter == "Operator".localized {
            titleStr = "All"
        } else {
            titleStr = operatorFilter
        }
        self.filterOperatorTopUp.text = titleStr
        if typeFilter == "Type".localized {
            titleStr = "All"
        } else {
            titleStr = typeFilter
        }
        self.filterPlanTypeTopUp.text = titleStr
        if periodStr == "Period".localized {
            titleStr = "All"
        } else {
            titleStr = periodStr
        }
        self.filterPeriodTopUp.text = titleStr
        self.creditLblAmount.text = "Total Cashback Amount"
    }
    
    func fillReceivedDetails(list: [ReportTransactionRecord], dateOne: String, dateTwo: String, currentBalance: String, amountRange: String,
                          transactionTypeFilter: String, operatorFilter: String, typeFilter: String, periodStr: String) {
        self.sideHeader.text = "All"
        self.enableReceivedMoneyMode = true
        self.getCreditAndDebit(list: list, fromBill: true)
        self.pdfHeaderLabel.text = "Received Money Statement".localized
        self.debitAmountTextLbl.text = "Total Received Amount"
        self.getGeneralDetails()
        self.getPeriodInFormat(periodStr: periodStr, dateOne: dateOne, dateTwo: dateTwo)
        self.balance.text = currentBalance + " MMK"
        var titleStr = ""
        if transactionTypeFilter == "Transaction".localized {
            titleStr = "All"
        } else {
            titleStr = transactionTypeFilter
        }
        self.filterTypeReceivedMoney.text = titleStr
        if amountRange == "Amount".localized {
            titleStr = "All"
        } else {
            titleStr = amountRange
        }
        self.filterAmountReceivedMoney.text = titleStr
        if periodStr == "Period".localized {
            titleStr = "All"
        } else {
            titleStr = periodStr
        }
        self.filterPeriodReceivedMoney.text = titleStr
    }
    
    func getCreditAndDebit(list: [ReportTransactionRecord], fromBill: Bool = false, fromTopUp: Bool = false) {
        var creditAmount: NSDecimalNumber = 0
        var debitAmount: NSDecimalNumber = 0
        var cashBackAmount: NSDecimalNumber = 0
        var index = 0
        for record in list {
            if fromBill {
                if record.accTransType == "Dr" {
                    if let transAmount = record.amount {
                        debitAmount = debitAmount.adding(transAmount)
                    }
                    if let cashback = record.cashBack {
                        creditAmount = creditAmount.adding(cashback)
                    }
                } else {
                    if let transAmount = record.amount {
                        debitAmount = debitAmount.adding(transAmount)
                    }
                    if let cashback = record.cashBack {
                        creditAmount = creditAmount.adding(cashback)
                    }
                }
                index += 1
            } else {
                if record.accTransType == "Cr" {
                    if let transAmount = record.amount {
                        creditAmount = creditAmount.adding(transAmount)
                    }
                } else if record.accTransType == "Dr" {
                    if let transAmount = record.amount {
                        debitAmount = debitAmount.adding(transAmount)
                    }
                }
                index += 1
            }
            if index == 49 {
                break
            }
            
            if let cashBack = record.cashBack {
                cashBackAmount = cashBackAmount.adding(cashBack)
            }
        }
        self.totalTransaction.text = "\((list.count))"
        if fromTopUp {
            self.creditAmount.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(cashBackAmount)") + " MMK".localized
        } else {
            self.creditAmount.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(creditAmount)") + " MMK".localized
        }
        self.debitAmount.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(debitAmount)") + " MMK".localized
        let totalAmount = creditAmount.adding(debitAmount)
        self.totalTransactionAmount.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(totalAmount)") + " MMK".localized
    }
    
    func getPeriodInFormat(periodStr: String, dateOne: String, dateTwo: String) {
        var period = ""
        switch periodStr {
        case "Period".localized, "All".localized:
            period = "All".localized
        case "Date".localized:
            if let fromDate = dateOne.dateValue(dateFormatIs: "dd MMM yyyy") {
                let fDate = fromDate.stringValue(dateFormatIs: "dd-MMM-yyyy")
                if let toDate = dateTwo.dateValue(dateFormatIs: "dd MMM yyyy") {
                    let tDate = toDate.stringValue(dateFormatIs: "dd-MMM-yyyy")
                    period = "\(fDate) to \(tDate)"
                }
            }
        case "Month".localized:
            if let month = dateTwo.dateValue(dateFormatIs: "MMM yyyy") {
                let mon = month.stringValue(dateFormatIs: "MMM-yyyy")
                period = "\(mon)"
            }
        default:
            break
        }
        self.period.text = period
    }
    
    private func getDivision() -> String {
        return getDivisionAndTownShipForReport(divisionCode: UserModel.shared.state, townshipCode: UserModel.shared.township)
    }
    
    func getGeneralDetails() {
        self.accName.text = "\(UserModel.shared.name)"
        //self.businessNameVal.text = (UserModel.shared.businessName != "") ? UserModel.shared.businessName : "-"
        if UserModel.shared.agentType == .user {
                   businessNameHeightConstraint.constant = 0
               } else {
                   businessNameHeightConstraint.constant = 21
                   self.businessNameVal.text = (UserModel.shared.businessName != "") ? UserModel.shared.businessName : "-"
               }
        let accountType = self.agentServiceTypeString(type: UserModel.shared.agentType)
        var phNumber = UserModel.shared.mobileNo.replaceFirstTwoChar(withStr: "+")
        phNumber.insert(")", at: phNumber.index(phNumber.startIndex, offsetBy: 3))
        phNumber.insert("(", at: phNumber.startIndex)
        if UserModel.shared.mobileNo.hasPrefix("0095") {
           phNumber.insert("0", at: phNumber.index(phNumber.startIndex, offsetBy: 5))
        }
        self.accNumber.text = "\(phNumber)"
        self.email.text = "\(UserModel.shared.email)".components(separatedBy: ",").first ?? ""
        let division = self.getDivision().components(separatedBy: ",")
        let townShip = "Township"
        var div = ""
        if division.count > 0 {
            div = division.last?.components(separatedBy: "Division").first ?? ""
        }
        if UserModel.shared.language != "en" {
            self.accountTypeVal.text = getBurmeseAgentType()
            self.address.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? ""), \(division.last ?? "")".replacingOccurrences(of: ", ,", with: ",")
        } else {
            self.accountTypeVal.text = accountType
            self.address.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? "") \(townShip), \(div), \(division.last ?? "")".replacingOccurrences(of: ", ,", with: ",")
            self.addressHeight.constant = 65.0
            self.layoutIfNeeded()
        }
//        self.address.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? "") \(townShip), \(div), \(division.last ?? ""), \(UserModel.shared.country)".replacingOccurrences(of: ", ,", with: ",")
        self.dateTime.text = Date().stringValueWithOutUTC(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss")
    }
    
    private func agentServiceTypeString(type: AgentType) -> String {
        switch type {
        case .user:
            return "Personal"
        case .merchant:
            return "Merchant"
        case .agent:
            return "Agent"
        case .advancemerchant:
            return "Advance Merchant"
        case .dummymerchant:
            return "Safety Cashier"
        case .oneStopMart:
            return "One Stop Mart"
        }
    }
}
