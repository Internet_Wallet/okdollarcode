//
//  CashInOutViewController.swift
//  OK
//  This show the list of cash in out records
//  Created by ANTONY on 05/02/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class CashInOutViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var tableViewCashInOut: UITableView!
    @IBOutlet weak var tableViewFilter: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var constraintScrollContentHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintFilterTableLeading: NSLayoutConstraint!
    
    //For search functions
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var buttonCancel: UIButton!
    @IBOutlet weak var buttonClear: UIButton!
    
    //For filter functions
    @IBOutlet weak var buttonCashInOutFilter: ButtonWithImage!
    @IBOutlet weak var buttonAmountFilter: ButtonWithImage!
    @IBOutlet weak var buttonPeriodFilter: ButtonWithImage!
    
    //For sorting functions
    @IBOutlet weak var buttonAgentSort: ButtonWithImage!
    @IBOutlet weak var buttonTransactionSort: ButtonWithImage!
    @IBOutlet weak var buttonMobileNumberSort: ButtonWithImage!
    @IBOutlet weak var buttonAmountSort: ButtonWithImage!
    @IBOutlet weak var buttonServiceFeeSort: ButtonWithImage!
    @IBOutlet weak var buttonBalanceSort: ButtonWithImage!
    @IBOutlet weak var buttonDateSort: ButtonWithImage!
    
    // MARK: - Properties
    private let reportCashInCellId = "ReportCashInCell"
    private let cashInOutType = ["Filter", "Cash Out", "Cash In"]
    private let amountFilters = ["Amount", "All", "0 - 1,000", "1,001 - 10,000", "10,001 - 50,000", "50,001 - 1,00,000",
                                 "1,00,001 - 2,00,000", "2,00,001 - 5,00,000", "5,00,001 - Above"]
    private let periodFilters = ["Period", "All", "Date", "Month"]
    private var filterArray = [String]()
    private enum FilterType {
        case cashInOut, amount, period
    }
    private var currentFilterType = FilterType.cashInOut
    var isToShowSearchView: Bool {
        get {
            return viewSearch.isHidden
        }
        set(newValue) {
            if newValue {
                viewShadow.isHidden = false
                viewSearch.isHidden = false
                textFieldSearch.becomeFirstResponder()
            } else {
                textFieldSearch.text = ""
                viewShadow.isHidden = true
                viewSearch.isHidden = true
            }
        }
    }
    
    // MARK: - View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewCashInOut.tag = 0
        tableViewFilter.tag = 1
        tableViewCashInOut.tableFooterView = UIView()
        tableViewFilter.tableFooterView = UIView()
        
        buttonCancel.layer.cornerRadius = 15
        buttonClear.layer.cornerRadius = 15
        buttonCancel.addTarget(self, action: #selector(hideSearchView), for: .touchUpInside)
        buttonClear.addTarget(self, action: #selector(clearSerchTextField), for: .touchUpInside)
        
        let cellFilterNibName = UINib(nibName: ConstantsXibCell.filterTableViewCell, bundle: nil)
        tableViewFilter.register(cellFilterNibName, forCellReuseIdentifier: ConstantsXibCell.filterTableViewCell)
        AppDelegate.AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)
        setUpNavigation()
    }
    
    override func viewDidLayoutSubviews() {
        constraintScrollContentHeight.constant = scrollView.frame.size.height
        self.view.layoutIfNeeded()
    }
    
    // MARK: - Methods
    
    // MARK: - Target Methods
    @objc private func hideSearchView() {
        self.view.endEditing(true)
        isToShowSearchView = false
    }
    
    @objc private func clearSerchTextField() {
        textFieldSearch.text = ""
    }
    
    @objc private func showSearchView() {
        isToShowSearchView = true
    }
    
    @objc private func showShareOption() {
    }
    
    // MARK: - Button Actions
    // It filters and show the detail by transaction type
    @IBAction func filterCashInOutType(_ sender: UIButton) {
        func showFilterAtCashInOut() {
            filterArray = cashInOutType
            currentFilterType = .cashInOut
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 5
            self.view.layoutIfNeeded()
        }
        if tableViewFilter.isHidden {
            showFilterAtCashInOut()
        } else {
            switch currentFilterType {
            case .cashInOut:
                tableViewFilter.isHidden = true
            default:
                showFilterAtCashInOut()
            }
        }
    }
    
    // It filters and show the detail by amount range
    @IBAction func filterByAmount(_ sender: UIButton) {
        func showFilterAtAmount() {
            filterArray = amountFilters
            currentFilterType = .amount
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 205
            self.view.layoutIfNeeded()
        }
        if tableViewFilter.isHidden {
            showFilterAtAmount()
        } else {
            switch currentFilterType {
            case .amount:
                tableViewFilter.isHidden = true
            default:
                showFilterAtAmount()
            }
        }
    }
    
    // It filters and show the detail by period
    @IBAction func filterByPeriod(_ sender: UIButton) {
        func showFilterAtPeriod() {
            filterArray = periodFilters
            currentFilterType = .period
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 395
            self.view.layoutIfNeeded()
        }
        if tableViewFilter.isHidden {
            showFilterAtPeriod()
        } else {
            switch currentFilterType {
            case .period:
                tableViewFilter.isHidden = true
            default:
                showFilterAtPeriod()
            }
        }
    }
    
    @IBAction func sortByAgentID() {
    }
    
    @IBAction func sortByTransactionID() {
    }
    
    @IBAction func sortByMobileNumber() {
    }
    
    @IBAction func sortByAmount() {
    }
    
    @IBAction func sortByServiceFee() {
    }
    
    @IBAction func sortByBalance() {
    }
    
    @IBAction func sortByDate() {
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension CashInOutViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView.tag {
        case 0:
            return 5
        case 1:
            return filterArray.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView.tag {
        case 0:
            let reportCashInCell = tableView.dequeueReusableCell(withIdentifier: reportCashInCellId, for: indexPath) as? ReportCashInCell
            return reportCashInCell!
        case 1:
            let filterCell = tableView.dequeueReusableCell(withIdentifier: ConstantsXibCell.filterTableViewCell, for: indexPath) as? FilterTableViewCell
            filterCell?.configureTopUpFilter(filterName: filterArray[indexPath.row])
            return filterCell!
        default:
            let reportCashInCell = tableView.dequeueReusableCell(withIdentifier: reportCashInCellId, for: indexPath) as? ReportCashInCell
            return reportCashInCell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView.tag {
        case 0:
            break
        case 1:
            switch currentFilterType {
            case .cashInOut:
                buttonCashInOutFilter.setTitle(filterArray[indexPath.row], for: .normal)
                tableViewFilter.isHidden = true
            case .amount:
                buttonAmountFilter.setTitle(filterArray[indexPath.row], for: .normal)
                tableViewFilter.isHidden = true
            case .period:
                buttonPeriodFilter.setTitle(filterArray[indexPath.row], for: .normal)
                tableViewFilter.isHidden = true
            }
        default:
            break
        }
    }
}

// MARK: - Additional Methods
extension CashInOutViewController: NavigationSetUpProtocol {
    /// It set up the navigation bar
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = ConstantsColor.navigationCashInOut
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: titleViewWidth, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        
        let searchButton = UIButton(type: .custom)
        searchButton.setImage(#imageLiteral(resourceName: "reportSearch").withRenderingMode(.alwaysOriginal), for: .normal)
        searchButton.addTarget(self, action: #selector(showSearchView), for: .touchUpInside)
        
        let shareButton = UIButton(type: .custom)
        shareButton.setImage(#imageLiteral(resourceName: "reportShare").withRenderingMode(.alwaysOriginal), for: .normal)
        shareButton.addTarget(self, action: #selector(showShareOption), for: .touchUpInside)
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        let ypos = (titleViewSize.height - 30 ) / 2
        searchButton.frame = CGRect(x: navTitleView.frame.size.width - 70, y: ypos, width: 30, height: 30)
        shareButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 160, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text = ConstantsController.cashInOutViewController.screenTitle.localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: "Zawgyi-One", size: 23) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        navTitleView.addSubview(searchButton)
        navTitleView.addSubview(shareButton)
        self.navigationItem.titleView = navTitleView
    }
    
    /// It shows the home screen
    @objc func backAction() {
        AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        self.navigationController?.popViewController(animated: true)
    }
}
