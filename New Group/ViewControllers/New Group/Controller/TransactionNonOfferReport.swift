//
//  TransactionNonOfferReport.swift
//  OK
//
//  Created by iMac on 3/27/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class TransactionNonOfferReport: UIViewController, UIGestureRecognizerDelegate {
    
    private lazy var modelBReport: ReportBModel = {
        return ReportBModel()
    }()
    
    // MARK: - Outlets
    @IBOutlet weak var tableViewTransaction: UITableView!
    @IBOutlet weak var tableViewFilter: UITableView!
    @IBOutlet weak var constraintFilterTableLeading: NSLayoutConstraint!
    @IBOutlet weak var constraintFilterTableHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintScrollContentHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintScrollTop: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var viewDatePicker: UIView!
    @IBOutlet weak var buttonDateSubmit: UIButton!{
        didSet {
            buttonDateSubmit.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateCancel: UIButton!{
        didSet {
            buttonDateCancel.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDateOne: UILabel!{
        didSet {
            labelDateOne.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDateTwo: UILabel!{
        didSet {
            labelDateTwo.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateOne: UIButton!{
        didSet {
            buttonDateOne.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateTwo: UIButton!{
        didSet {
            buttonDateTwo.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblQR: UILabel!{
        didSet {
            lblQR.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var imageViewQR: UIImageView!
    @IBOutlet weak var viewToShareAllRecords: UIView!
    
    //For search functions
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var buttonCancel: UIButton!{
        didSet {
            buttonCancel.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonClear: UIButton!{
        didSet {
            buttonClear.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    //For filter functions
    @IBOutlet weak var buttonTransactionType: ButtonWithImage!{
        didSet {
            buttonTransactionType.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonAmountRange: ButtonWithImage!{
        didSet {
            buttonAmountRange.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonGender: ButtonWithImage!{
        didSet {
            buttonGender.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonAge: ButtonWithImage!{
        didSet {
            buttonAge.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonPeriod: ButtonWithImage!{
        didSet {
            buttonPeriod.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    //For sorting functions
    @IBOutlet weak var buttonUser: ButtonWithImage!{
        didSet {
            buttonUser.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonTransactionID: ButtonWithImage!{
        didSet {
            buttonTransactionID.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonAmount: ButtonWithImage!{
        didSet {
            buttonAmount.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonMobileNumber: ButtonWithImage!{
        didSet {
            buttonMobileNumber.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonBonus: ButtonWithImage!{
        didSet {
            buttonBonus.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonCashBack: ButtonWithImage!{
        didSet {
            buttonCashBack.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonBalance: ButtonWithImage!{
        didSet {
            buttonBalance.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDate: ButtonWithImage!{
        didSet {
            buttonDate.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
     let dateFormatter = DateFormatter()
    // MARK: - Properties
    var initialRecords = [NonOfferReportList]()
    var transactionRecords = [NonOfferReportList]()
    let cellTransactionCell = "NonOfferReportCell"
    let expandedTransactionTypes = [FilterType(rootFilter: .transactionType, filterTitle: "Transaction"),
                                    FilterType(rootFilter: .transactionType, filterTitle: "All"),
                                    FilterType(rootFilter: .transactionType, filterTitle: "Offers"),
                                    FilterType(rootFilter: .transactionType, filterTitle: "Non-Offers"),
                                    FilterType(rootFilter: .transactionType, filterTitle: "Remarks")]
    let amountRanges = [FilterType(rootFilter: .amount, filterTitle: "Amount"), FilterType(rootFilter: .amount, filterTitle: "All"), FilterType(rootFilter: .amount, filterTitle: "0 - 1,000"),
                        FilterType(rootFilter: .amount, filterTitle: "1,001 - 10,000"), FilterType(rootFilter: .amount, filterTitle: "10,001 - 50,000"),
                        FilterType(rootFilter: .amount, filterTitle: "50,001 - 1,00,000"), FilterType(rootFilter: .amount, filterTitle: "1,00,001 - 2,00,000"),
                        FilterType(rootFilter: .amount, filterTitle: "2,00,001 - 5,00,000"), FilterType(rootFilter: .amount, filterTitle: "5,00,001 - Above")]
    
    let genderTypes = [FilterType(rootFilter: .gender, filterTitle: "Gender"), FilterType(rootFilter: .gender, filterTitle: "All"), FilterType(rootFilter: .gender, filterTitle: "Male"),
                       FilterType(rootFilter: .gender, filterTitle: "Female")]
    let ageRanges = [FilterType(rootFilter: .age, filterTitle: "Age"), FilterType(rootFilter: .age, filterTitle: "All"), FilterType(rootFilter: .age, filterTitle: "0 - 19"),
                     FilterType(rootFilter: .age, filterTitle: "20 - 34"), FilterType(rootFilter: .age, filterTitle: "35 - 50"),
                     FilterType(rootFilter: .age, filterTitle: "51 - 64"), FilterType(rootFilter: .age, filterTitle: "65 - Above")]
    let periodTypes = [FilterType(rootFilter: .period, filterTitle: "Period"), FilterType(rootFilter: .period, filterTitle: "All"), FilterType(rootFilter: .period, filterTitle: "Date"), FilterType(rootFilter: .period, filterTitle: "Month")]
    var filterArray = [FilterType]()
    var lastSelectedFilter = RootFilter.transactionType
    var currentBalance: NSDecimalNumber = 0
    var isSearchViewShow: Bool {
        get {
            return !viewSearch.isHidden
        }
        set(newValue) {
            if newValue {
                viewSearch.isHidden = false
                textFieldSearch.becomeFirstResponder()
                constraintScrollTop.constant = 0
                self.view.layoutIfNeeded()
            } else {
                textFieldSearch.text = ""
                viewSearch.isHidden = true
                constraintScrollTop.constant = -40
                self.view.layoutIfNeeded()
                self.view.endEditing(true)
            }
        }
    }
    var isDateViewHidden: Bool {
        get {
            return viewDatePicker.isHidden
        }
        set(newValue) {
            if newValue {
                viewShadow.isHidden = true
                viewDatePicker.isHidden = true
            } else {
                viewShadow.isHidden = false
                viewDatePicker.isHidden = false
            }
        }
    }
    enum ParentType: String {
        case main = "Main", refunds = "Refunds", payment = "Payment", transportation = "Transportation", requestMoney = "Request Money"
    }
    enum RootFilter {
        case transactionType, amount, gender, age, period
    }
    struct FilterType {
        var rootFilter: RootFilter
        var filterTitle: String
        var parentType: ParentType
        var isSubShowing: Bool
        var hasChild: Bool
        init(rootFilter: RootFilter, filterTitle: String, parentType: ParentType = .main, isSubShowing: Bool = false, hasChild: Bool = false) {
            self.rootFilter = rootFilter
            self.filterTitle = filterTitle
            self.parentType = parentType
            self.isSubShowing = isSubShowing
            self.hasChild = hasChild
        }
    }
    private let refreshControl = UIRefreshControl()
    var modelReport: ReportBModel!
    var dictFilterData =  Dictionary<String,Any>()
    
    // MARK: - View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewTransaction.tag = 0
        tableViewFilter.tag = 1
        tableViewTransaction.tableFooterView = UIView()
        tableViewFilter.tableFooterView = UIView()
        tableViewTransaction.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshTableData), for: .valueChanged)
        buttonCancel.layer.cornerRadius = 15
        buttonClear.layer.cornerRadius = 15
        buttonCancel.addTarget(self, action: #selector(hideSearchView), for: .touchUpInside)
        buttonClear.addTarget(self, action: #selector(clearSerchTextField), for: .touchUpInside)
        
        buttonDateSubmit.layer.cornerRadius = 15
        buttonDateCancel.layer.cornerRadius = 15
        buttonDateSubmit.addTarget(self, action: #selector(filterSelectedDate), for: .touchUpInside)
        buttonDateCancel.addTarget(self, action: #selector(hideDateSelectionView), for: .touchUpInside)
        textFieldSearch.placeholder = "Search By Transaction ID or Amount or Mobile Number or Remarks".localized
        buttonCancel.setTitle("Cancel".localized, for: .normal)
        buttonClear.setTitle("Clear".localized, for: .normal)
        buttonDateSubmit.setTitle("Submit".localized, for: .normal)
        buttonDateCancel.setTitle("Cancel".localized, for: .normal)
        labelDateOne.text = "From Date".localized
        labelDateTwo.text = "To Date".localized
        
        let cellFilterNibName = UINib(nibName: ConstantsXibCell.filterTableViewCell, bundle: nil)
        tableViewFilter.register(cellFilterNibName, forCellReuseIdentifier: ConstantsXibCell.filterTableViewCell)
        datePicker.calendar = Calendar(identifier: .gregorian)
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        if #available(iOS 13.4, *) {
                       datePicker.preferredDatePickerStyle = .wheels
                   } else {
                       // Fallback on earlier versions
                   }
        dateFormatter.dateFormat = "dd MMM yyyy"
        self.setInitialValues()
        AppDelegate.AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)
        setUpNavigation()
        modelReport = ReportBModel()
        modelReport.reportBModelDelegate = self
        dateFormatter.calendar = Calendar.current
           dateFormatter.dateFormat = "dd MMM yyyy"
        //modelReport.getTransactionInfo()
        //Get Records
        self.getData()
    }
    
    override func viewDidLayoutSubviews() {
        let navBarHeight = navigationController?.navigationBar.frame.size.height ?? 44
        let heightToReduce = navBarHeight == 32 ? 56 : 44
        constraintScrollContentHeight.constant = scrollView.frame.size.height - CGFloat(heightToReduce) + 44
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    func getData() {
        println_debug("Get Records")
        println_debug(dictFilterData)
        print("passed dictionary------\(dictFilterData)")
        
        /*
         {
         "FromDate":"2019-03-01",
         "ToDate":"2019-03-20",
         "TransactionRequestedType":"ALLTRAN",
         "TownShipId":"3D29094D-D288-4B5F-A0E4-ADC19579EBDD",
         "MobileNumber":"00959443803578",UserModel.shared.mobileNo
         "AdvanceMerchantNumber":"00959894485030",
         "DummyMerchantMobileNumber":""
         }
         */
        
        
       // let request = NonOfferReporListDateRequest(mobileNumber: UserModel.shared.mobileNo, fromDate: "2019-03-01", toDate: "2019-05-23", transactionRequestedType: dictFilterData.safeValueForKey("TransactionRequestedType") as! String, townShipId: dictFilterData.safeValueForKey("TownShipId") as! String, advanceMerchantNumber: dictFilterData.safeValueForKey("AdvanceMerchantNumber") as! String, dummyMerchantMobileNumber: dictFilterData.safeValueForKey("DummyMerchantMobileNumber") as! String)
        
        
        
        let request = NonOfferReporListDateRequest(mobileNumber: UserModel.shared.mobileNo, fromDate: dictFilterData.safeValueForKey("FromDate") as! String, toDate: dictFilterData.safeValueForKey("ToDate") as! String, transactionRequestedType: dictFilterData.safeValueForKey("TransactionRequestedType") as! String, townShipId: dictFilterData.safeValueForKey("TownShipId") as! String, advanceMerchantNumber: dictFilterData.safeValueForKey("AdvanceMerchantNumber") as! String, dummyMerchantMobileNumber: "")
        
        modelBReport.reportBModelDelegate  = self
        modelBReport.getNonOfferListDateForReport(request: request)
    }
    
    // MARK: - Target Methods -
    @objc func showSearchView() {
        tableViewFilter.isHidden = true
        if !isSearchViewShow {
            removeGestureFromShadowView()
            viewShadow.isHidden = true
            /// For qr
            imageViewQR.isHidden = true
            lblQR.isHidden = true
            imageViewQR.image = nil
            /// For share all transaction
            viewToShareAllRecords.isHidden = true
            if transactionRecords.count > 0 {
                isSearchViewShow = true
                textFieldSearch.becomeFirstResponder()
            } else {
                /*
                 let alert = UIAlertController(title: "Alert", message: "No Records", preferredStyle: UIAlertControllerStyle.alert)
                 alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                 self.present(alert, animated: true, completion: nil)
                 */
                alertViewObj.wrapAlert(title: "Alert".localized, body: "No Records".localized, img: UIImage(named : "AppIcon"))
                alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                    
                })
                alertViewObj.showAlert(controller: self)
                
            }
        } else {
            textFieldSearch.becomeFirstResponder()
        }
    }
    
    func noRecordAlertAction() {
        /*
         let alert = UIAlertController(title: "Alert", message: "No Records", preferredStyle: UIAlertControllerStyle.alert)
         alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
         self.present(alert, animated: true, completion: nil)
         */
        alertViewObj.wrapAlert(title: "Alert".localized, body: "No Records".localized, img: UIImage(named : "AppIcon"))
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
        })
        alertViewObj.showAlert(controller: self)
    }
    
    @objc func showShareOption() {
        if viewToShareAllRecords.isHidden {
            if transactionRecords.count > 0 {
                let tapGestToRemoveView = UITapGestureRecognizer(target: self, action: #selector(shadowIsTapped))
                viewShadow.addGestureRecognizer(tapGestToRemoveView)
                viewShadow.isHidden = false
                viewToShareAllRecords.isHidden = false
            } else {
                viewShadow.isHidden = true
                noRecordAlertAction()
            }
        }
    }
    
    @objc func showGraph() {
        guard let statisticsTransactionController = UIStoryboard(name: "Reports", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.statisticsTransactionController.nameAndID) as? StatisticsTransactionAndBillController else { return }
        statisticsTransactionController.screenType = .transactionDetail
        //statisticsTransactionController.initialRecords = initialRecords
        self.navigationController?.pushViewController(statisticsTransactionController, animated: true)
    }
    
    @objc func hideSearchView() {
        self.view.endEditing(true)
        isSearchViewShow = false
        //applyFilter()
    }
    
    @objc func clearSerchTextField() {
        textFieldSearch.text = ""
        //applyFilter()
    }
    
    @objc func filterSelectedDate() {
        if let _ = buttonDateOne.title(for: .normal) {
            buttonPeriod.setTitle("Date".localized, for: .normal)
        } else {
            buttonPeriod.setTitle("Month".localized, for: .normal)
        }
        //applyFilter()
        isDateViewHidden = true
    }
    
    @objc func hideDateSelectionView() {
        isDateViewHidden = true
    }
    
    @objc func fromDatePickerChanged(datePicker:UIDatePicker) {
        buttonDateOne.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
    }
    
    @objc func toDatePickerChanged(datePicker:UIDatePicker) {
        if let startDateStr = buttonDateOne.title(for: .normal), let startDate = dateFormatter.date(from: startDateStr) {
            switch datePicker.date.compare(startDate) {
            case .orderedAscending:
                if let endDateStr = buttonDateTwo.title(for: .normal), let endDate = dateFormatter.date(from: endDateStr) {
                    datePicker.date = endDate
                }
            case .orderedDescending, .orderedSame:
                buttonDateTwo.setTitle(dateFormatter.string(from: datePicker.date), for: .normal)
            }
        } else {
            buttonDateTwo.setTitle(dateFormatter.string(from: datePicker.date), for: .normal)
        }
    }
    
    @objc func monthPickerChanged(datePicker:UIDatePicker) {
        buttonDateTwo.setTitle(dateFormatter.string(from: datePicker.date), for: .normal)
    }
    
    @objc func shadowIsTapped(sender: UITapGestureRecognizer) {
        removeGestureFromShadowView()
        viewShadow.isHidden = true
        /// For qr
        imageViewQR.isHidden = true
        lblQR.isHidden = true
        imageViewQR.image = nil
        /// For share all transaction
        viewToShareAllRecords.isHidden = true
    }
    
    @objc private func refreshTableData(_ sender: Any) {
        if viewSearch.isHidden {
            //modelReport.getTransactionInfo()
        } else {
            self.refreshControl.endRefreshing()
        }
    }
    
    // MARK: - Button Action Methods
    @IBAction func shareRecordInPdf(_ sender: UIButton) {
        
        guard let transactionCell = sender.superview?.superview?.superview as? NonOfferReportCell else { return }
        guard let indexPath = tableViewTransaction.indexPath(for: transactionCell) else { return }
        var pdfDictionary = [String: Any]()
        pdfDictionary["logoName"] = "appIcon_Ok"
        pdfDictionary["invoiceTitle"] = "Transaction Details Receipt".localized
        pdfDictionary["senderAccName"] = ""
        if let name = transactionRecords[indexPath.row].senderName, name.count > 0 {
            pdfDictionary["senderAccName"] = name
        } else {
            if let comnts = transactionRecords[indexPath.row].desc, comnts.lowercased().contains(find: "solar") {
                pdfDictionary["senderAccName"] = UserModel.shared.name
            }
        }
        pdfDictionary["receiverAccName"] = transactionRecords[indexPath.row].receiverName
        if let trType = transactionRecords[indexPath.row].accTransType {
            if trType == "Dr" {
                if let businessName = transactionRecords[indexPath.row].receiverBName, businessName.count > 0 {
                    pdfDictionary["businessName"] = businessName
                }
            } else {
                if let businessName = transactionRecords[indexPath.row].senderBName, businessName.count > 0 {
                    pdfDictionary["businessName"] = businessName
                }
            }
        }
        var tType = ""
        var pType: String? = nil
        if let desc = transactionRecords[indexPath.row].rawDesc {
            if desc.contains("#OK") {
                if desc.contains("Top-Up") || desc.contains("TopUp") || desc.contains("Bonus Point Top Up") || desc.contains("TopUpPlan"){
                    tType = "Top-Up"
                } else if desc.contains("Data Plan") || desc.contains("DataPlan"){
                    tType = "Data Plan"
                } else {
                    tType = "Special Offers"
                }
            } else {
                if let transType = transactionRecords[indexPath.row].transType {
                    tType = transType
                    if transType == "PAYWITHOUT ID" {
                        if let accTransType = transactionRecords[indexPath.row].accTransType {
//                            if accTransType == "Cr" {
//                                tType = "XXXXXX"
//                            } else {
                                tType = "Hide My Number"
                            //}
                        }
                    } else if transType == "TOLL" {
                        tType = "TOLL"
                    } else if transType == "TICKET" {
                        tType = "TICKET"
                    } else {
                        tType = "PAYTO"
                        if desc.contains(find: "-cashin") {
                            pType = "Cash In"
                        } else if desc.contains(find: "-cashout") {
                            pType = "Cash Out"
                        } else if desc.contains(find: "-transferto") {
                            pType = "Transfer To"
                        }
                    }
                }
            }
        }
        pdfDictionary["transactionType"] = tType
        pdfDictionary["paymentType"] = pType
        if let accTransType = transactionRecords[indexPath.row].accTransType {
            if accTransType == "Cr" {
                var sNumber = ""
                var rNumber = UserModel.shared.mobileNo
                if let des = transactionRecords[indexPath.row].destination {
                    sNumber = des
                    if des.hasPrefix("0095") {
                        sNumber = "(+95)0\(des.substring(from: 4))"
                    }
                }
                if rNumber.hasPrefix("0095") {
                    rNumber = "(+95)0\(rNumber.substring(from: 4))"
                }
                pdfDictionary["senderAccNo"] = sNumber
                pdfDictionary["receiverAccNo"] = rNumber
                if let transType = transactionRecords[indexPath.row].transType {
                    if transType == "PAYWITHOUT ID" {
                        pdfDictionary["senderAccNo"] = "XXXXXXXXXX"
                    }
                }
                if pdfDictionary["receiverAccName"] == nil {
                    pdfDictionary["receiverAccName"] = UserModel.shared.name
                }
            } else {
                var sNumber = UserModel.shared.mobileNo
                var rNumber = ""
                if let des = transactionRecords[indexPath.row].destination {
                    rNumber = des
                    if des.hasPrefix("0095") {
                        rNumber = "(+95)0\(des.substring(from: 4))"
                    } else {
                        if let cCode = transactionRecords[indexPath.row].countryCode, cCode.count > 0 {
                            rNumber = "(\(cCode))\(rNumber)"
                        }
                    }
                }
                if sNumber.hasPrefix("0095") {
                    sNumber = "(+95)0\(sNumber.substring(from: 4))"
                }
                pdfDictionary["receiverAccNo"] = rNumber
                pdfDictionary["senderAccNo"] = sNumber
                if let sName = pdfDictionary["senderAccName"] as? String, sName.count > 0 {
                    pdfDictionary["senderAccName"] = sName
                } else {
                    pdfDictionary["senderAccName"] = UserModel.shared.name
                }
                if let des = transactionRecords[indexPath.row].rawDesc {
                    if !des.contains(find: "#point-redeempoints") {
                        if !des.contains(find: "#OK-PMC") {
                            let desArr = des.components(separatedBy: "-")
                            if desArr.count > 2 {
                                pdfDictionary["operatorName"] = desArr[2]
                            }
                        }
                    } else {
                        if pdfDictionary["receiverAccName"] == nil ||  (pdfDictionary["receiverAccName"] as? String ?? "").count == 0 {
                            pdfDictionary["receiverAccName"] = "Unknown"
                        }
                    }
                }
            }
        }
        pdfDictionary["transactionID"] = transactionRecords[indexPath.row].transID
        if let nsBonus = transactionRecords[indexPath.row].bonus {
            pdfDictionary["bonusPoint"] = "\(nsBonus)" + " PTS"
        }
        if let transDate = transactionRecords[indexPath.row].transactionDate {
            pdfDictionary["transactionDate"] = transDate//(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss")
        }
        if let descriptionStr = transactionRecords[indexPath.row].desc, descriptionStr.count > 0 {
            pdfDictionary["remarks"] = descriptionStr
        }
        if let transAmount = transactionRecords[indexPath.row].amount {
            pdfDictionary["amount"] = "\(transAmount)" + " MMK" //CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: transAmount.toString()) + " MMK"
        }
        if let comnt = transactionRecords[indexPath.row].rawDesc, comnt.contains("#point-redeempoints-") {
            pdfDictionary["qrImage"] = nil
        } else {
            if let transType = transactionRecords[indexPath.row].accTransType, transType == "Dr" {
                //pdfDictionary["qrImage"] = getQRCodeImage(qrData: transactionRecords[indexPath.row])
            }
        }
        if let promoRawDesc = transactionRecords[indexPath.row].rawDesc, promoRawDesc.contains("#OK-PMC") {
            pdfDictionary["isForPromocode"] = true
            var helper: OfferCommentHelper! = OfferCommentHelper(comment: promoRawDesc.removingPercentEncoding)
            
            if helper.promotionTransactionCount == 1 && helper.paymentType == "1" {
                pdfDictionary["promoReceiverNumber"] = transactionRecords[indexPath.row].destination?.replacingOccurrences(of: "0095", with: "(+95)0")
                if let transType = transactionRecords[indexPath.row].accTransType, transType == "Dr" {
                    //Show tx done from user login
                    pdfDictionary["senderAccNo"] = UserModel.shared.mobileNo.replacingOccurrences(of: "0095", with: "(+95)0")
                    pdfDictionary["receiverAccNo"] = helper.promoReceiverNumber
                } else {
                    pdfDictionary["senderAccNo"] = helper.senderCreditNumber
                    pdfDictionary["receiverAccNo"] = UserModel.shared.mobileNo.replacingOccurrences(of: "0095", with: "(+95)0")
                }
            }
            pdfDictionary["promocode"] = helper.promoCode
            if let discPercent = helper.discountPercent {
                pdfDictionary["discountPercent"] = discPercent + " %"
            }
            if let discAmount = helper.discountAmount {
                pdfDictionary["discountAmount"] = discAmount + " MMK"
            }
            if let billAmount = helper.billAmount {
                pdfDictionary["billAmount"] = billAmount + " MMK"
            }
            pdfDictionary["companyName"] = helper.companyName
            pdfDictionary["productName"] = helper.productName
            pdfDictionary["buyingQuantity"] = helper.buyingQuantity
            pdfDictionary["freeQuantity"] = helper.freeQuantity
            helper = nil
        }
        guard let pdfUrl = CodeSnippets.createInvoicePDF(dictValues: pdfDictionary, pdfFile: "OK$ Non Offer Transaction") else {
            println_debug("Error - pdf not generated")
            return
        }
        let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
    
    
    @IBAction func shareRecordByQRCode(_ sender: UIButton) {
        self.view.endEditing(true)
        guard let transactionCell = sender.superview?.superview?.superview as? NonOfferReportCell else { return }
        guard let indexPath = tableViewTransaction.indexPath(for: transactionCell) else { return }
        if let qrImage = getQRCodeImage(qrData: transactionRecords[indexPath.row]) {
            let tapGestToRemoveView = UITapGestureRecognizer(target: self, action: #selector(shadowIsTapped))
            viewShadow.addGestureRecognizer(tapGestToRemoveView)
            imageViewQR.image = qrImage
            viewShadow.isHidden = false
            imageViewQR.isHidden = false
            lblQR.isHidden = false
        }
    }
    
}


// MARK: - UITextFieldDelegate
extension TransactionNonOfferReport: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tableViewFilter.isHidden = true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        tableViewTransaction.contentOffset = CGPoint(x: 0, y: 0)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            if updatedText.count > 50 || (updatedText == "" && text == "") {
                return false
            }
            if string != "\n" {
                //applyFilter(searchText: updatedText)
            }
        }
        return true
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension TransactionNonOfferReport: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        switch tableView.tag {
        case 0:
            if transactionRecords.count == 0 {
                let noDataLabel = UILabel(frame: tableView.bounds)
                noDataLabel.text = "No Records".localized
                noDataLabel.font = UIFont(name: appFont, size: 18)
                noDataLabel.textAlignment = .center
                tableView.backgroundView = noDataLabel
            } else {
                tableView.backgroundView = nil
            }
        default:
            break
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView.tag {
        case 0:
            return transactionRecords.count
        case 1:
            return filterArray.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView.tag {
        case 0:
            let transactionDetailCell = tableView.dequeueReusableCell(withIdentifier: cellTransactionCell, for: indexPath) as? NonOfferReportCell
            transactionDetailCell!.configureTransactionItemCell(transRecord: transactionRecords[indexPath.row])
           // print("cellforrowindex-----\(String(describing: transactionRecords[indexPath.row].desc))")
            return transactionDetailCell!
        case 1:
            let filterCell = tableView.dequeueReusableCell(withIdentifier: ConstantsXibCell.filterTableViewCell, for: indexPath) as? FilterTableViewCell
            configureFilterCell(cell: filterCell!, filterType: filterArray[indexPath.row])
            return filterCell!
        default:
            let transactionDetailCell = tableView.dequeueReusableCell(withIdentifier: cellTransactionCell, for: indexPath) as? TransactionDetailCell
            return transactionDetailCell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView.tag {
        case 0:
            break
        case 1:
            let selectedFilter = filterArray[indexPath.row]
            switch selectedFilter.rootFilter {
            case .transactionType:
                if selectedFilter.hasChild {
                    switch selectedFilter.filterTitle {
                    case ParentType.refunds.rawValue:
                        expandRefundOptions(selectedFilter: selectedFilter)
                    case ParentType.payment.rawValue:
                        expandPaymentOptions(selectedFilter: selectedFilter)
                    case ParentType.transportation.rawValue:
                        expandTransportOptions(selectedFilter: selectedFilter)
                    case ParentType.requestMoney.rawValue:
                        expandRequestMoneyOptions(selectedFilter: selectedFilter)
                    default:
                        buttonTransactionType.setTitle(selectedFilter.filterTitle.localized, for: .normal)
                        tableViewFilter.isHidden = true
                    }
                    updateFilterTableHeight()
                } else {
                    buttonTransactionType.setTitle(selectedFilter.filterTitle.localized, for: .normal)
                    tableViewFilter.isHidden = true
                    //applyFilter()
                }
            case .amount:
                buttonAmountRange.setTitle(selectedFilter.filterTitle.localized, for: .normal)
                tableViewFilter.isHidden = true
                //applyFilter()
            case .gender:
                buttonGender.setTitle(selectedFilter.filterTitle.localized, for: .normal)
                tableViewFilter.isHidden = true
                //applyFilter()
            case .age:
                buttonAge.setTitle(selectedFilter.filterTitle.localized, for: .normal)
                tableViewFilter.isHidden = true
                //applyFilter()
            case .period:
                tableViewFilter.isHidden = true
                //showDateOptions(selectedOption: selectedFilter.filterTitle.localized)
            }
        default:
            break
        }
    }
}

extension TransactionNonOfferReport: UIScrollViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        targetContentOffset.pointee.y = max(targetContentOffset.pointee.y - 1, 1)
    }
}

// MARK: - Additional Methods
extension TransactionNonOfferReport: NavigationSetUpProtocol {
    /// It set up the navigation bar
    func setUpNavigation() {
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationController?.navigationBar.barTintColor        = ConstantsColor.navigationHeaderTransaction
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: titleViewWidth, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        
        let searchButton = UIButton(type: .custom)
        searchButton.setImage(#imageLiteral(resourceName: "reportSearch").withRenderingMode(.alwaysOriginal), for: .normal)
        searchButton.addTarget(self, action: #selector(showSearchView), for: .touchUpInside)
        
        let shareButton = UIButton(type: .custom)
        shareButton.setImage(#imageLiteral(resourceName: "reportShare").withRenderingMode(.alwaysOriginal), for: .normal)
        shareButton.addTarget(self, action: #selector(showShareOption), for: .touchUpInside)
        
        let graphButton = UIButton(type: .custom)
        graphButton.setImage(#imageLiteral(resourceName: "reportGraph").withRenderingMode(.alwaysOriginal), for: .normal)
        graphButton.addTarget(self, action: #selector(showGraph), for: .touchUpInside)
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "tabBarBack").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        let ypos = (titleViewSize.height - 30 ) / 2
//        searchButton.frame = CGRect(x: navTitleView.frame.size.width - 110, y: ypos, width: 30, height: 30)
//        shareButton.frame = CGRect(x: navTitleView.frame.size.width - 70, y: ypos, width: 30, height: 30)
//        graphButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
        if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 100, y: ypos, width: 30, height: 30)
            backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        }
        else{
           searchButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
           backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        }
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 240, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text = ConstantsController.transactionNonOfferViewController.screenTitle.localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 23) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        navTitleView.addSubview(searchButton)
        //navTitleView.addSubview(shareButton)
        //navTitleView.addSubview(graphButton)
        self.navigationItem.titleView = navTitleView
    }
    
    /// It shows the home screen
    @objc func backAction() {
        AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        self.navigationController?.popViewController(animated: true)
    }
}

extension TransactionNonOfferReport: ReportBModelDelegate {

    func apiResult(message: [NonOfferReportList], statusCode: String?) {
        DispatchQueue.main.async {
            println_debug(message)
            self.transactionRecords.removeAll()
            self.initialRecords.removeAll()

            self.initialRecords = message
            self.transactionRecords = message
            self.tableViewTransaction.reloadData()
            self.resetSortButtons(sender: UIButton())
            self.resetFilterButtons()
            self.refreshControl.endRefreshing()
        }
    }
    func handleCustomError(error: CustomError) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
    
    
}

