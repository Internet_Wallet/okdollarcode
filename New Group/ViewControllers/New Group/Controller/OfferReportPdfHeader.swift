//
//  OfferReportPdfHeader.swift
//  OK
//
//  Created by E J ANTONY on 03/08/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class OfferReportPdfHeader: UIView {
    //MARK: - Outlets
    @IBOutlet weak var lblAccNameTitle: UILabel!{
    didSet {
        lblAccNameTitle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
      }
    @IBOutlet weak var lblAccNameValue: UILabel!{
        didSet {
            lblAccNameValue.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblAccNumberTitle: UILabel!{
        didSet {
            lblAccNumberTitle.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblAccNumberValue: UILabel!{
        didSet {
            lblAccNumberValue.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblEmailTitle: UILabel!{
        didSet {
            lblEmailTitle.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblEmailValue: UILabel!{
        didSet {
            lblEmailValue.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblAddressTitle: UILabel!{
        didSet {
            lblAddressTitle.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblAddressValue: UILabel!{
        didSet {
            lblAddressValue.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblPeriodTitle: UILabel!{
        didSet {
            lblPeriodTitle.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblPeriodValue: UILabel!{
        didSet {
            lblPeriodValue.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblTotalBillAmountTitle: UILabel!{
        didSet {
            lblTotalBillAmountTitle.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblTotalBillAmountValue: UILabel!{
        didSet {
            lblTotalBillAmountValue.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblTotalNetAmountTitle: UILabel!{
        didSet {
            lblTotalNetAmountTitle.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblTotalNetAmountValue: UILabel!{
        didSet {
            lblTotalNetAmountValue.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblTotalDiscountAmountTitle: UILabel!{
        didSet {
            lblTotalDiscountAmountTitle.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblTotalDiscountAmountValue: UILabel!{
        didSet {
            lblTotalDiscountAmountValue.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblTotalPercentageAmountTitle: UILabel!{
        didSet {
            lblTotalPercentageAmountTitle.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblTotalPercentageAmountValue: UILabel!{
        didSet {
            lblTotalPercentageAmountValue.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblTotalSaleQuantityTitle: UILabel!{
        didSet {
            lblTotalSaleQuantityTitle.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblTotalSaleQuantityValue: UILabel!{
        didSet {
            lblTotalSaleQuantityValue.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblTotalFreeQuantityTitle: UILabel!{
        didSet {
            lblTotalFreeQuantityTitle.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblTotalFreeQuantityValue: UILabel!{
        didSet {
            lblTotalFreeQuantityValue.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblDateTimeTitle: UILabel!{
        didSet {
            lblDateTimeTitle.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblDateTimeValue: UILabel!{
        didSet {
            lblDateTimeValue.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblTotalTransTitle: UILabel!{
        didSet {
            lblTotalTransTitle.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblTotalTransValue: UILabel!{
        didSet {
            lblTotalTransValue.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblAccountNameH: UILabel!{
        didSet {
            lblAccountNameH.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblAccountNumberH: UILabel!{
        didSet {
            lblAccountNumberH.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblBillAmountH: UILabel!{
        didSet {
            lblBillAmountH.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblDiscountAmountH: UILabel!{
        didSet {
            lblDiscountAmountH.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblNetRecvdAmountH: UILabel!{
        didSet {
            lblNetRecvdAmountH.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblSaleQntyH: UILabel!{
        didSet {
            lblSaleQntyH.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblFreeQntyH: UILabel!{
        didSet {
            lblFreeQntyH.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblTransactionIDH: UILabel!{
        didSet {
            lblTransactionIDH.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblPromoCodeH: UILabel!{
        didSet {
            lblPromoCodeH.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblDateTimeH: UILabel!{
        didSet {
            lblDateTimeH.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var viewForItemHeader: UIView!
    @IBOutlet weak var constraintTopDate: NSLayoutConstraint!
    @IBOutlet weak var constraintTopSale: NSLayoutConstraint!
    @IBOutlet weak var constraintHSeparator: NSLayoutConstraint!
    @IBOutlet weak var lblColonNet: UILabel!{
        didSet {
            lblColonNet.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblColonDiscount: UILabel!{
        didSet {
            lblColonDiscount.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblColonPercent: UILabel!{
        didSet {
            lblColonPercent.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblColonSale: UILabel!{
        didSet {
            lblColonSale.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblColonFree: UILabel!{
        didSet {
            lblColonFree.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    
    //MARK: - Properties
    var amountType = 0
    
    //MARK: - Methods
    func setLocalizationTitles() {
        self.lblAccNameTitle.text = "Account Name".localized
        self.lblAccNumberTitle.text = "Account Number".localized
        self.lblEmailTitle.text = "Email".localized
        self.lblAddressTitle.text = "Address".localized
        self.lblPeriodTitle.text = "Period".localized
        self.lblTotalBillAmountTitle.text = "Total Bill Amount".localized
        self.lblTotalNetAmountTitle.text = "Total Net Amount".localized
        self.lblTotalDiscountAmountTitle.text = "Total Discount Amount".localized
        self.lblTotalPercentageAmountTitle.text = "Total Percentage Amount".localized
        self.lblDateTimeTitle.text = "Date & Time".localized
        self.lblTotalTransTitle.text = "Total Transactions".localized
        self.lblPromoCodeH.text = "Promotion Code".localized
        self.lblAccountNameH.text = "Account Name".localized
        self.lblAccountNumberH.text = "Account Number".localized
        self.lblBillAmountH.text = "Bill Amount".localized + "\n" + "(MMK)"
        self.lblTransactionIDH.text = "Transaction ID".localized
        self.lblDateTimeH.text = "Date & Time".localized
        self.lblTotalSaleQuantityTitle.text = "Total Sale Quantity".localized
        self.lblTotalFreeQuantityTitle.text = "Total Free Quantity".localized
        self.lblDiscountAmountH.text = "Discount Amount".localized + "(MMK) or %"
        self.lblNetRecvdAmountH.text = "Net Amount".localized + "\n" + "(MMK)"
        self.lblSaleQntyH.text = "Sale Quantity".localized
        self.lblFreeQntyH.text = "Free Quantity".localized
    }
    
    func setupTotalInfoUI() {
        switch amountType {
        case 0,1:
            self.constraintTopDate.constant = -52
            self.constraintTopSale.constant = 10
            self.constraintHSeparator.constant = 233
            self.lblTotalNetAmountTitle.isHidden = false
            self.lblTotalNetAmountValue.isHidden = false
            self.lblColonNet.isHidden = false
            self.lblTotalDiscountAmountTitle.isHidden = false
            self.lblTotalDiscountAmountValue.isHidden = false
            self.lblColonDiscount.isHidden = false
            self.lblTotalPercentageAmountTitle.isHidden = false
            self.lblTotalPercentageAmountValue.isHidden = false
            self.lblColonPercent.isHidden = false
            self.lblTotalSaleQuantityTitle.isHidden = true
            self.lblTotalSaleQuantityValue.isHidden = true
            self.lblColonSale.isHidden = true
            self.lblTotalFreeQuantityTitle.isHidden = true
            self.lblTotalFreeQuantityValue.isHidden = true
            self.lblColonFree.isHidden = true
        case 2:
            self.constraintTopDate.constant = 10
            self.constraintTopSale.constant = -83
            self.constraintHSeparator.constant = 202
            self.lblTotalNetAmountTitle.isHidden = true
            self.lblTotalNetAmountValue.isHidden = true
            self.lblColonNet.isHidden = true
            self.lblTotalDiscountAmountTitle.isHidden = true
            self.lblTotalDiscountAmountValue.isHidden = true
            self.lblColonDiscount.isHidden = true
            self.lblTotalPercentageAmountTitle.isHidden = true
            self.lblTotalPercentageAmountValue.isHidden = true
            self.lblColonPercent.isHidden = true
            self.lblTotalSaleQuantityTitle.isHidden = false
            self.lblTotalSaleQuantityValue.isHidden = false
            self.lblColonSale.isHidden = false
            self.lblTotalFreeQuantityTitle.isHidden = false
            self.lblTotalFreeQuantityValue.isHidden = false
            self.lblColonFree.isHidden = false
        case 3,4:
            fallthrough
        default:
            break
        }
    }
    
    func setupHeaderUI() {
        switch amountType {
        case 0,1:
            self.lblDiscountAmountH.isHidden = false
            self.lblNetRecvdAmountH.isHidden = false
            self.lblSaleQntyH.isHidden = true
            self.lblFreeQntyH.isHidden = true
        case 2:
            self.lblDiscountAmountH.isHidden = true
            self.lblNetRecvdAmountH.isHidden = true
            self.lblSaleQntyH.isHidden = false
            self.lblFreeQntyH.isHidden = false
        case 3,4:
            fallthrough
        default:
            break
        }
    }
    
    func configureWithDatas(reportList: [OfferReporFilterList], amountType: Int) {
        self.amountType = amountType
        self.setLocalizationTitles()
        self.setupTotalInfoUI()
        self.setupHeaderUI()
        let rList = reportList.sorted {
            guard let date1 = $0.transactionTime , let date2 = $1.transactionTime else { return false }
            return date1.compare(date2) == .orderedAscending
        }
        var totalBillAmount = 0.0
        var totalDiscAmount = 0.0
        var totalNetAmount = 0.0
        var totalBuyingQty = 0
        var totalFreeQty = 0

        for record in rList {
            if let amount = record.totalNetAmount {
                totalBillAmount += amount
            }
            if let amount = record.totalDiscountAmount {
                totalDiscAmount += amount
            }
            if let amount = record.totalBillAmount {
                totalNetAmount += amount
            }
            if record.productValidationRuleValue == 2 {
                if let buying = record.buyingQty {
                    totalBuyingQty += buying
                }
                if let free = record.freeQty {
                    totalFreeQty += free
                }
            } else {
                if let buying = record.buyingQty {
                    totalBuyingQty += buying
                }
                if let free = record.freeQty {
                    totalFreeQty += free
                }
            }
        }
        totalBillAmount = Double(round(Double(100 * totalBillAmount))/100)
        totalDiscAmount = Double(round(Double(100 * totalDiscAmount))/100)
        totalNetAmount = Double(round(Double(100 * totalNetAmount))/100)
        self.lblTotalSaleQuantityValue.text = "\(totalBuyingQty)"
        self.lblTotalFreeQuantityValue.text = "\(totalFreeQty)"
        self.lblAccNameValue.text = "\(UserModel.shared.name)"
        self.lblAccNumberValue.text = UserModel.shared.formattedNumber
        self.lblEmailValue.text = "\(UserModel.shared.email)"
        self.lblAddressValue.text = "\(UserModel.shared.address2)" + " " + "\(UserModel.shared.address1)"
        if rList.count > 0 {
            let sDateStr = rList[0].transactionTime
            let eDateStr = rList[rList.count - 1].transactionTime //2018-07-14T18:46:57.897
            if let sDate = sDateStr?.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS"),
                let eDate = eDateStr?.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") {
                let fromDate = sDate.stringValue(dateFormatIs: "dd-MMM-yyyy")
                let toDate = eDate.stringValue(dateFormatIs: "dd-MMM-yyyy")
                self.lblPeriodValue.text = fromDate + " To " + toDate
            }
        }
        self.lblTotalBillAmountValue.text = "\(totalBillAmount)".replacingOccurrences(of: ".00", with: "").replacingOccurrences(of: ".0", with: "") + " MMK"
        self.lblTotalNetAmountValue.text = "\(totalNetAmount)".replacingOccurrences(of: ".00", with: "").replacingOccurrences(of: ".0", with: "") + " MMK"
        self.lblTotalDiscountAmountValue.text = "\(totalDiscAmount)".replacingOccurrences(of: ".00", with: "").replacingOccurrences(of: ".0", with: "") + " MMK"
        let percentageValue = Float((Float(totalDiscAmount) * 100) / Float(totalBillAmount))
        self.lblTotalPercentageAmountValue.text = "\(CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(percentageValue)")) %"
        self.lblDateTimeValue.text = "\(Date().stringValueWithOutUTC(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss"))"
        self.lblTotalTransValue.text = "\(reportList.count)"
        self.layoutIfNeeded()
    }
}
