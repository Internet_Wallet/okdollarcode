//
//  ReportSendMoneyToBankController.swift
//  OK
//  This show the report of sent money to bank
//  Created by ANTONY on 02/02/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportSendMoneyToBankController: UIViewController, UIScrollViewDelegate {
    // MARK: - Outlets
    @IBOutlet weak var tableViewSendMoneyToBank: UITableView!
    @IBOutlet weak var tableViewFilter: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var constraintScrollContentHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintFilterTableLeading: NSLayoutConstraint!
    @IBOutlet weak var constraintFilterTableHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintScrollTop: NSLayoutConstraint!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var viewDatePicker: UIView!
    @IBOutlet weak var buttonDateSubmit: UIButton!{
        didSet {
            buttonDateSubmit.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateCancel: UIButton!{
        didSet {
            buttonDateCancel.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDateOne: UILabel!{
        didSet {
            labelDateOne.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDateTwo: UILabel!{
        didSet {
            labelDateTwo.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateOne: UIButton!{
        didSet {
            buttonDateOne.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateTwo: UIButton!{
        didSet {
            buttonDateTwo.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var viewToShareAllRecords: UIView!
    
    //For search functions
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var buttonCancel: UIButton!{
        didSet {
            buttonCancel.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonClear: UIButton!{
        didSet {
            buttonClear.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    //For filter function
    @IBOutlet weak var buttonFilterAccount: ButtonWithImage!{
        didSet {
            buttonFilterAccount.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonFilterAmount: ButtonWithImage!{
        didSet {
            buttonFilterAmount.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonFilterPeriod: ButtonWithImage!{
        didSet {
            buttonFilterPeriod.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    //For sort function
    @IBOutlet weak var buttonSortReceiverName: ButtonWithImage!{
        didSet {
            buttonSortReceiverName.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonSortBankName: ButtonWithImage!{
        didSet {
            buttonSortBankName.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonSortBranchName: ButtonWithImage!{
        didSet {
            buttonSortBranchName.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonSortAccNumber: ButtonWithImage!{
        didSet {
            buttonSortAccNumber.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonSortAccType: ButtonWithImage!{
        didSet {
            buttonSortAccType.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonSortAmount: ButtonWithImage!{
        didSet {
            buttonSortAmount.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonSortTransactionID: ButtonWithImage!{
        didSet {
            buttonSortTransactionID.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonSortDateTime: ButtonWithImage!{
        didSet {
            buttonSortDateTime.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonSortBalance: ButtonWithImage!{
        didSet {
            buttonSortBalance.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    // MARK: - Properties
    private var modelReport: ReportModel!
    var sendMoneyBankReports = [ReportSendMoneyToBank]()
    let sendMoneyToBankCellId = "ReportSendMoneyToBankCell"
    var filterArray = [String]()
    let accountTypes = ["Account Type Report", "All", "Call Deposit Account", "Savings Account", "Current Account", "ATM Card Account"]
    let amountRanges = ["Amount", "All", "10,001 - 50,000", "50,001 - 1,00,000", "1,00,001 - 2,00,000", "2,00,001 - 5,00,000",  "5,00,001 - Above"]
    let periodTypes = ["Period", "All", "Date", "Month"]
    var lastSelectedFilter = RootFilter.accountType
    var currentBalance: NSDecimalNumber = 0
    var isSearchViewShow: Bool {
        get {
            return !viewSearch.isHidden
        }
        set(newValue) {
            if newValue {
                viewSearch.isHidden = false
                textFieldSearch.becomeFirstResponder()
                constraintScrollTop.constant = 0
                self.view.layoutIfNeeded()
            } else {
                textFieldSearch.text = ""
                viewSearch.isHidden = true
                constraintScrollTop.constant = -40
                self.view.layoutIfNeeded()
                self.view.endEditing(true)
            }
        }
    }
    var isDateViewHidden: Bool {
        get {
            return viewDatePicker.isHidden
        }
        set(newValue) {
            if newValue {
                viewShadow.isHidden = true
                viewDatePicker.isHidden = true
            } else {
                viewShadow.isHidden = false
                viewDatePicker.isHidden = false
            }
        }
    }
    enum RootFilter {
        case accountType, amount, period
    }
    private let refreshControl = UIRefreshControl()
     let dateFormatter = DateFormatter()
    // MARK:- Views life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatter.calendar = Calendar.current
           dateFormatter.dateFormat = "dd MMM yyyy"
        println_debug("ReportSendMoneyToBankController")
        modelReport = ReportModel()
        modelReport.reportModelDelegate = self
        textFieldSearch.placeholder = "Search By Transaction ID or Amount".localized
        tableViewSendMoneyToBank.tag = 0
        tableViewFilter.tag = 1
        tableViewSendMoneyToBank.tableFooterView = UIView()
        tableViewFilter.tableFooterView = UIView()
        tableViewSendMoneyToBank.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshTableData), for: .valueChanged)
        buttonCancel.layer.cornerRadius = 15
        buttonClear.layer.cornerRadius = 15
        buttonCancel.addTarget(self, action: #selector(hideSearchView), for: .touchUpInside)
        buttonClear.addTarget(self, action: #selector(clearSerchTextField), for: .touchUpInside)
        
        buttonDateSubmit.layer.cornerRadius = 15
        buttonDateCancel.layer.cornerRadius = 15
        buttonDateSubmit.addTarget(self, action: #selector(filterSelectedDate), for: .touchUpInside)
        buttonDateCancel.addTarget(self, action: #selector(hideDateSelectionView), for: .touchUpInside)
        
        buttonCancel.setTitle("Cancel".localized, for: .normal)
        buttonClear.setTitle("Clear".localized, for: .normal)
        labelDateOne.text = "From Date".localized
        labelDateTwo.text = "To Date".localized
        buttonDateSubmit.setTitle("Submit".localized, for: .normal)
        buttonDateCancel.setTitle("Cancel".localized, for: .normal)
        resetFilterButtons()
        setLocalizedTitles()
        let cellFilterNibName = UINib(nibName: ConstantsXibCell.filterTableViewCell, bundle: nil)
        tableViewFilter.register(cellFilterNibName, forCellReuseIdentifier: ConstantsXibCell.filterTableViewCell)
        datePicker.calendar = Calendar(identifier: .gregorian)
        if #available(iOS 13.4, *) {
                       datePicker.preferredDatePickerStyle = .wheels
                   } else {
                       // Fallback on earlier versions
                   }
        getReportList()
        AppDelegate.AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)
        setUpNavigation()
    }
    
    override func viewDidLayoutSubviews() {
        //let navBarHeight = navigationController?.navigationBar.frame.size.height ?? 44
        //let heightToReduce = navBarHeight == 32 ? 56 : 44
        constraintScrollContentHeight.constant = scrollView.frame.size.height //- CGFloat(heightToReduce) + 44
        self.view.layoutIfNeeded()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    // MARK: - Target Methods
    @objc private func showSearchView() {
        tableViewFilter.isHidden = true
        if !isSearchViewShow {
            removeGestureFromShadowView()
            viewShadow.isHidden = true
            /// For share all transaction
            viewToShareAllRecords.isHidden = true
            if sendMoneyBankReports.count > 0 {
                isSearchViewShow = true
                textFieldSearch.becomeFirstResponder()
            } else {
                noRecordAlertAction()
            }
        } else {
            self.hideSearchView()
//            textFieldSearch.becomeFirstResponder()
        }
    }
    
    @objc private func showShareOption() {
        if viewToShareAllRecords.isHidden {
            if sendMoneyBankReports.count > 0 {
                let tapGestToRemoveView = UITapGestureRecognizer(target: self, action: #selector(shadowIsTapped))
                viewShadow.addGestureRecognizer(tapGestToRemoveView)
                viewShadow.isHidden = false
                viewToShareAllRecords.isHidden = false
            } else {
                viewShadow.isHidden = true
                noRecordAlertAction()
            }
        }
    }
    
    @objc private func hideSearchView() {
        self.view.endEditing(true)
        isSearchViewShow = false
        applyFilter()
    }
    
    @objc private func clearSerchTextField() {
        textFieldSearch.text = ""
        applyFilter()
    }
    
    @objc func filterSelectedDate() {
        if var dateStartInStr = buttonDateOne.title(for: .normal) {
            if let dateEndInStr = buttonDateTwo.title(for: .normal) {
                dateStartInStr = dateStartInStr + " to\n" + dateEndInStr
            }
            buttonFilterPeriod.setTitle(dateStartInStr, for: .normal)
        } else {
            if let monthInStr = buttonDateTwo.title(for: .normal) {
                buttonFilterPeriod.setTitle(monthInStr, for: .normal)
            }
        }
        applyFilter(searchText: textFieldSearch.text ?? "")
        isDateViewHidden = true
    }
    
    @objc func hideDateSelectionView() {
        isDateViewHidden = true
    }
    
    @objc func fromDatePickerChanged(datePicker:UIDatePicker) {
        buttonDateOne.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
    }
    
    @objc func toDatePickerChanged(datePicker:UIDatePicker) {
        if let startDateStr = buttonDateOne.title(for: .normal), let startDate = startDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
            switch datePicker.date.compare(startDate) {
            case .orderedAscending:
                if let endDateStr = buttonDateTwo.title(for: .normal), let endDate = endDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                    datePicker.date = endDate
                }
            case .orderedDescending, .orderedSame:
                buttonDateTwo.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            }
        } else {
            buttonDateTwo.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
        }
    }
    
    @objc func monthPickerChanged(datePicker:UIDatePicker) {
        buttonDateTwo.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "MMM yyyy"), for: .normal)
    }
    
    @objc func shadowIsTapped(sender: UITapGestureRecognizer) {
        removeGestureFromShadowView()
        viewShadow.isHidden = true
        /// For share all transaction
        viewToShareAllRecords.isHidden = true
    }
    
    @objc private func refreshTableData(_ sender: Any) {
        if viewSearch.isHidden {
            PTLoader.shared.show()
            DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 1.0) {
                self.getReportList(showLoader: false)
            }
            
        } else {
            self.refreshControl.endRefreshing()
        }
    }
    
    // MARK: - Methods
    func noRecordAlertAction() {
        alertViewObj.wrapAlert(title: "Alert".localized, body: "No Records".localized, img: UIImage(named : "AppIcon"))
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
        })
        alertViewObj.showAlert(controller: self)
    }
    
    func setLocalizedTitles() {
        buttonSortReceiverName.setTitle("Receiver Account Name".localized, for: .normal)
        buttonSortBankName.setTitle("Bank Name".localized, for: .normal)
        buttonSortBranchName.setTitle("Branch Name".localized, for: .normal)
        buttonSortAccNumber.setTitle("Account Number".localized, for: .normal)
        buttonSortAccType.setTitle("Account Type Report".localized, for: .normal)
        buttonSortAmount.setTitle("Amount".localized + " " + "(MMK)".localized, for: .normal)
        buttonSortTransactionID.setTitle("Transaction ID".localized, for: .normal)
        buttonSortDateTime.setTitle("Date & Time".localized, for: .normal)
        buttonSortBalance.setTitle("Balance".localized + " " + "(MMK)".localized, for: .normal)
    }
    
    func removeGestureFromShadowView() {
        if let recognizers = viewShadow.gestureRecognizers {
            for recognizer in recognizers {
                if let recognizer = recognizer as? UITapGestureRecognizer {
                    viewShadow.removeGestureRecognizer(recognizer)
                }
            }
        }
    }
    
    func resetFilterButtons() {
        buttonFilterAccount.setTitle(accountTypes[0].localized, for: .normal)
        buttonFilterAmount.setTitle(amountRanges[0].localized, for: .normal)
        buttonFilterPeriod.setTitle(periodTypes[0].localized, for: .normal)
    }
    
    func resetSortButtons(sender: UIButton) {
        if sender != buttonSortReceiverName {
            buttonSortReceiverName.tag = 0
            buttonSortReceiverName.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonSortBankName {
            buttonSortBankName.tag = 0
            buttonSortBankName.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonSortBranchName {
            buttonSortBranchName.tag = 0
            buttonSortBranchName.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonSortAccNumber {
            buttonSortAccNumber.tag = 0
            buttonSortAccNumber.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonSortAccType {
            buttonSortAccType.tag = 0
            buttonSortAccType.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonSortAmount {
            buttonSortAmount.tag = 0
            buttonSortAmount.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonSortTransactionID {
            buttonSortTransactionID.tag = 0
            buttonSortTransactionID.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonSortDateTime {
            buttonSortDateTime.tag = 0
            buttonSortDateTime.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonSortBalance {
            buttonSortBalance.tag = 0
            buttonSortBalance.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
    }
    
    func updateFilterTableHeight() {
        constraintFilterTableHeight.constant = filterArray.count > 4 ? CGFloat(5*44) : CGFloat(filterArray.count * 44)
        self.view.layoutIfNeeded()
    }
    
    func reloadTableWithScrollToTop() {
        self.tableViewSendMoneyToBank.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            if self.sendMoneyBankReports.count > 0 {
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableViewSendMoneyToBank.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
    }
    
    // MARK: - Filter Actions
    func applyFilter(searchText: String = "") {
        var searchTxt = searchText
        if isSearchViewShow && searchTxt.count == 0 {
            searchTxt = textFieldSearch.text ?? ""
        }
        var filteredArray = modelReport.sendMoneyBankInitialReports
        filteredArray = filterProcess(forTransactionType: filteredArray)
        filteredArray = filterProcess(forAmountType: filteredArray)
        filteredArray = filterProcess(forPeriod: filteredArray)
        if isSearchViewShow && searchText.count > 0 && searchTxt.count > 0 {
            filteredArray = filteredArray.filter {
                if let amount = $0.transferredAmount {
                    if "\(amount)".range(of:searchTxt.lowercased()) != nil {
                        return true
                    }
                }
                if let transID = $0.transactionID {
                    if transID.lowercased().range(of: searchTxt.lowercased()) != nil {
                        return true
                    }
                }
                if let accNumber = $0.bankAccNumber {
                    if accNumber.lowercased().range(of: searchTxt.lowercased()) != nil {
                        return true
                    }
                }
                return false
            }
        }
        sendMoneyBankReports = filteredArray
        reloadTableWithScrollToTop()
    }
    
    func filterProcess(forTransactionType arrayToFilter: [ReportSendMoneyToBank]) -> [ReportSendMoneyToBank] {
        if let transFilter = buttonFilterAccount.currentTitle {
            switch transFilter {
            case "Account Type Report".localized, "All".localized:
                return arrayToFilter
            case "Call Deposit Account".localized:
                return arrayToFilter.filter {
                    guard let comments = $0.comments else { return false }
                    return comments.contains("CDA")
                }
            case "Savings Account".localized:
                return arrayToFilter.filter {
                    guard let comments = $0.comments else { return false }
                    return comments.contains("SAV")
                }
            case "Current Account".localized:
                return arrayToFilter.filter {
                    guard let comments = $0.comments else { return false }
                    return comments.contains("CUR")
                }
            case "ATM Card Account".localized:
                return arrayToFilter.filter {
                    guard let comments = $0.comments else { return false }
                    return comments.contains("ACA")
                }
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcess(forAmountType arrayToFilter: [ReportSendMoneyToBank]) -> [ReportSendMoneyToBank] {
        func filterFromDecimalNumbers(greaterThan: NSDecimalNumber, lesserThan: NSDecimalNumber)-> [ReportSendMoneyToBank] {
            return arrayToFilter.filter
                {
                    guard let amount = $0.transferredAmount else { return false }
                    return (amount.compare(greaterThan) == .orderedDescending && amount.compare(lesserThan) == .orderedAscending)
            }
        }
        if let transFilter = buttonFilterAmount.currentTitle {
            switch transFilter {
            case "Amount".localized, "All".localized:
                return arrayToFilter
            case "10,001 - 50,000".localized:
                return filterFromDecimalNumbers(greaterThan: 10000.99, lesserThan: 50001)
            case "50,001 - 1,00,000".localized:
                return filterFromDecimalNumbers(greaterThan: 50000.99, lesserThan: 100001)
            case "1,00,001 - 2,00,000".localized:
                return filterFromDecimalNumbers(greaterThan: 100000.99, lesserThan: 200001)
            case "2,00,001 - 5,00,000".localized:
                return filterFromDecimalNumbers(greaterThan: 200000.99, lesserThan: 500001)
            case "5,00,001 - Above".localized:
                return arrayToFilter.filter {
                    guard let amount = $0.transferredAmount else { return false }
                    return (amount.compare(500000.99) == .orderedDescending)
                }
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcess(forPeriod arrayToFilter: [ReportSendMoneyToBank]) -> [ReportSendMoneyToBank] {
        if let periodFilter = buttonFilterPeriod.currentTitle {
            switch periodFilter {
            case "Period".localized, "All".localized:
                return arrayToFilter
            default:
                break
            }
            if let _ = buttonDateOne.title(for: .normal) {
                return arrayToFilter.filter {
                    guard let tDate = $0.transactionDateTime else { return false }
                    let transDateStr = tDate.stringValue(dateFormatIs: "dd MMM yyyy")
                    guard let transactionDate = transDateStr.dateValue(dateFormatIs: "dd MMM yyyy") else { return false }
                    guard let dateStartStr = buttonDateOne.currentTitle else { return false }
                    guard let dateEndStr = buttonDateTwo.currentTitle else { return false }
                    guard let exactDateStart = dateStartStr.dateValue(dateFormatIs: "dd MMM yyyy") else { return false }
                    guard let dateStart = Calendar.current.date(byAdding: Calendar.Component.day, value: -1, to: exactDateStart) else { return false }
                    guard let exactDateEnd = dateEndStr.dateValue(dateFormatIs: "dd MMM yyyy") else { return false }
                    guard let dateEnd = Calendar.current.date(byAdding: Calendar.Component.day, value: 1, to: exactDateEnd) else { return false}
                    return (dateStart.compare(transactionDate) == .orderedAscending && transactionDate.compare(dateEnd) == .orderedAscending)
                }
            } else {
                return arrayToFilter.filter {
                    guard let transDate = $0.transactionDateTime else { return false }
                    let transDateStr = transDate.stringValue(dateFormatIs: "MMM yyyy")
                    guard let transactionDate = transDateStr.dateValue(dateFormatIs: "MMM yyyy") else { return false }
                    guard let dateStr = buttonDateTwo.currentTitle else { return false }
                    guard let monthToFilter = dateStr.dateValue(dateFormatIs: "MMM yyyy") else { return false }
                    return transactionDate.compare(monthToFilter) == .orderedSame
                }
            }
        }
        return []
    }
    
    // MARK: - DatePicker methods
    func showDateOptions(selectedOption: String) {
        switch selectedOption {
        case "Period".localized, "All".localized:
            buttonFilterPeriod.setTitle(selectedOption, for: .normal)
            applyFilter()
            isDateViewHidden = true
        case "Date".localized:
            buttonDateOne.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            buttonDateTwo.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            labelDateOne.isHidden = false
            buttonDateOne.isHidden = false
            labelDateTwo.text = "To Date".localized
            configureDatePicker(tag: 0)
            isDateViewHidden = false
        case "Month".localized:
            buttonDateOne.setTitle(nil, for: .normal)
            buttonDateTwo.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "MMM yyyy"), for: .normal)
            labelDateOne.isHidden = true
            buttonDateOne.isHidden = true
            labelDateTwo.text = "Month".localized
            configureDatePicker(tag: 2)
            isDateViewHidden = false
        default:
            break
        }
    }
    
    func configureDatePicker(tag: Int) {
        switch tag {
        case 0:
            buttonDateOne.setTitleColor(ConstantsColor.navigationHeaderSendMoney, for: .normal)
            buttonDateTwo.setTitleColor(UIColor(red: 30/255.0, green: 30/255, blue: 30/255, alpha: 1.0), for: .normal)
            datePicker.datePickerMode = .date
            datePicker.removeTarget(nil, action: nil, for: .allEvents)
            datePicker.addTarget(self, action: #selector(fromDatePickerChanged), for: .valueChanged)
            datePicker.minimumDate = nil
            if let endDateStr = buttonDateTwo.title(for: .normal), let endDate = endDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                datePicker.maximumDate = endDate
            } else {
                datePicker.maximumDate = Date()
            }
            if let currentDateStr = buttonDateOne.title(for: .normal), let currentDate = currentDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                datePicker.date = currentDate
            } else {
                buttonDateOne.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            }
        case 1:
            buttonDateOne.setTitleColor(UIColor(red: 30/255.0, green: 30/255, blue: 30/255, alpha: 1.0), for: .normal)
            buttonDateTwo.setTitleColor(ConstantsColor.navigationHeaderSendMoney, for: .normal)
            datePicker.datePickerMode = .date
            datePicker.maximumDate = Date()
            datePicker.removeTarget(nil, action: nil, for: .allEvents)
            datePicker.addTarget(self, action: #selector(toDatePickerChanged), for: .valueChanged)
           // datePicker.minimumDate = nil
            if let endDateStr = buttonDateOne.title(for: .normal), let endDate = dateFormatter.date(from: endDateStr) {
                datePicker.minimumDate = endDate
            } else {
                datePicker.minimumDate = nil
            }

            if let currentDateStr = buttonDateTwo.title(for: .normal), let currentDate = currentDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                datePicker.date = currentDate
            } else {
                buttonDateTwo.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            }
        case 2:
            buttonDateTwo.setTitleColor(ConstantsColor.navigationHeaderSendMoney, for: .normal)
            datePicker.datePickerMode = .date
            datePicker.maximumDate = Date()
            datePicker.date = Date()
            datePicker.removeTarget(nil, action: nil, for: .allEvents)
            datePicker.addTarget(self, action: #selector(monthPickerChanged), for: .valueChanged)
            datePicker.minimumDate = nil
            if let currentDateStr = buttonDateTwo.title(for: .normal), let currentDate = currentDateStr.dateValue(dateFormatIs: "MMM yyyy") {
                datePicker.date = currentDate
            } else {
                buttonDateTwo.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "MMM yyyy"), for: .normal)
            }
        default:
            break
        }
    }
    
    func getReportList(showLoader: Bool = true) {
        let applicationID = LoginParams.setUniqueIDLogin()
        let mobNumber = UserModel.shared.mobileNo
        let sendMoneyBankReq = SendMoneyToBankReportRequest(appID: applicationID, limit: "500", mobileNumber: mobNumber, msId: msid, simId: uuid)
        modelReport.getSendMoneyToBankReport(request: sendMoneyBankReq, showLoader: showLoader)
    }
    
    // MARK: - Button Action Methods
    @IBAction func shareRecordInPdf(_ sender: UIButton) {
        guard let reportSendMoneyToBankCell = sender.superview?.superview?.superview as? ReportSendMoneyToBankCell else { return }
        guard let indexPath = tableViewSendMoneyToBank.indexPath(for: reportSendMoneyToBankCell) else { return }
        let pdfView = UIView()
        if let reportView = Bundle.main.loadNibNamed("ReportSendMoneySingleRecordPdf", owner: self, options: nil)?.first as? ReportSendMoneySingleRecordPdf {
            reportView.fillDetails(report: sendMoneyBankReports[indexPath.row])
            pdfView.addSubview(reportView)
            pdfView.frame = CGRect(x: 0, y: 0, width: reportView.frame.size.width, height: reportView.viewSendMoneyBankReceipt.frame.maxY + 30)
            guard let pdfUrl = CodeSnippets.generatePDF(viewForPDF: pdfView, pdfFile: "OK$ Send Money To Bank Receipt") else {
                println_debug("Error - pdf not generated")
                return
            }
            let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
            DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
        }
    }
    
    @IBAction func shareAllTransactionsInPdf(_ sender: UIButton) {
        viewShadow.isHidden = true
        viewToShareAllRecords.isHidden = true
        removeGestureFromShadowView()
        if sendMoneyBankReports.count == 0 {
            return
        }
        let pdfView = UIView()
        if let pdfHeaderView = Bundle.main.loadNibNamed("ReportSendMoneyPdfHeader", owner: self, options: nil)?.first as? ReportSendMoneyPdfHeader {
            
            pdfHeaderView.fillDetails(sendMoneyBankReports: sendMoneyBankReports, filterPeriod: buttonFilterPeriod.currentTitle ?? "",
                                      filterAmount: buttonFilterAmount.currentTitle ?? "", filterAccount: buttonFilterAccount.currentTitle ?? "")
            let newHeight = pdfHeaderView.viewHeaderForReportSendMoney.frame.maxY
            pdfHeaderView.frame = CGRect(x: 0, y: 0, width: pdfHeaderView.frame.size.width, height: newHeight)
            pdfView.addSubview(pdfHeaderView)
            pdfView.frame = pdfHeaderView.bounds
            let xPos: CGFloat = pdfHeaderView.viewHeaderForReportSendMoney.frame.minX
            var yPos: CGFloat = pdfView.frame.maxY
            
            for record in sendMoneyBankReports {
                if let pdfBodyView = Bundle.main.loadNibNamed("ReportSendMoneyPdfBody", owner: self, options: nil)?.first as? ReportSendMoneyPdfBody {
                    pdfBodyView.fillDetails(record: record)
                    let recordHeight = pdfBodyView.frame.size.height
                    let yPosInPage = Int(yPos) % 842
                    if (yPosInPage + Int(recordHeight)) > 842 {
                        yPos = yPos + CGFloat((842 - yPosInPage) + 30 + 30)
                    }
                    pdfBodyView.frame = CGRect(x: xPos, y: yPos, width: pdfBodyView.frame.size.width, height: recordHeight)
                    yPos = yPos + recordHeight + 2
                    pdfView.addSubview(pdfBodyView)
                }
            }
            pdfView.frame = CGRect(x: 0, y: 0, width: pdfView.frame.size.width, height: yPos + 30)
            guard let pdfUrl = CodeSnippets.generatePDF(viewForPDF: pdfView, pdfFile: "OK$ Send Money To Bank Reports") else {
                println_debug("Error - pdf not generated")
                return
            }
            let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
            DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
        }
    }
    
    @IBAction func shareAllTransactionInExcel(_ sender: UIButton) {
        viewShadow.isHidden = true
        viewToShareAllRecords.isHidden = true
        removeGestureFromShadowView()
        
        if sendMoneyBankReports.count == 0 {
            return
        }
        var excelTxt = "Receiver Name,Bank ,Branch Name,Account Number,Account Type,Amount (MMK),Transaction ID, Balance (MMK), Date & Time\n"
        var totalTransAmount: NSDecimalNumber = 0
        for record in sendMoneyBankReports {
            var dateTimeStr = ""
            if let dateT = record.transactionDateTime {
                dateTimeStr = dateT.stringValue(dateFormatIs: "EEE dd-MMM-yyyy HH:mm:ss")
            }
            if let transAmount = record.transferredAmount {
                totalTransAmount = totalTransAmount.adding(transAmount)
            }
            var recText = ""
            let acType = record.bankAccType
            
            recText = "\(record.receiverOkAccName?.replacingOccurrences(of: ",", with: " ") ?? ""),\(record.bankName ?? ""),\(record.bankBranchName ?? ""),\(record.bankAccNumber ?? ""),\(acType ?? ""),\(record.transferredAmount?.toString() ?? ""),\(record.transactionID ?? ""),\(record.balance?.toString() ?? ""),\(dateTimeStr)\n\n"
            excelTxt.append(recText)
        }
        excelTxt.append("Total Transaction Amount,,,,,\(totalTransAmount)")
        guard let excelUrl = CodeSnippets.generateExcel(excelDataWithFormat: excelTxt, excelFileName: "OK$ Send Money To Bank CSV Report") else {
            println_debug("Error - excel not generated")
            return
        }
        let activityViewController = UIActivityViewController(activityItems: [excelUrl], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }

    @IBAction func filterByAccount(_ sender: UIButton) {
        self.view.endEditing(true)
        func showListAtTransaction() {
            filterArray = accountTypes
            tableViewFilter.reloadData()
            constraintFilterTableLeading.constant = 120
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
            tableViewFilter.isHidden = false
        }
        if tableViewFilter.isHidden {
            showListAtTransaction()
        } else {
            switch lastSelectedFilter {
            case .accountType :
                tableViewFilter.isHidden = true
            default:
                showListAtTransaction()
            }
        }
        lastSelectedFilter = .accountType
    }
    
    @IBAction func filterByAmount(_ sender: UIButton) {
        self.view.endEditing(true)
        func showListAtAmount() {
            filterArray = amountRanges
            tableViewFilter.reloadData()
            constraintFilterTableLeading.constant = 520
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
            tableViewFilter.isHidden = false
        }
        if tableViewFilter.isHidden {
            showListAtAmount()
        } else {
            switch lastSelectedFilter {
            case .amount :
                tableViewFilter.isHidden = true
            default:
                showListAtAmount()
            }
        }
        lastSelectedFilter = .amount
    }
    
    @IBAction func filterByPeriod(_ sender: UIButton) {
        self.view.endEditing(true)
        func showListAtPeriod() {
            filterArray = periodTypes
            tableViewFilter.reloadData()
            constraintFilterTableLeading.constant = 920
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
            tableViewFilter.isHidden = false
        }
        if tableViewFilter.isHidden {
            showListAtPeriod()
        } else {
            switch lastSelectedFilter {
            case .period :
                tableViewFilter.isHidden = true
            default:
                showListAtPeriod()
            }
        }
        lastSelectedFilter = .period
    }
    
    @IBAction func sortByReceiverName(_ sender: UIButton) {
        func sortReceiverName(orderType: ComparisonResult) -> [ReportSendMoneyToBank]{
            return sendMoneyBankReports.sorted {
                guard let name1 = $0.receiverOkAccName, let name2 = $1.receiverOkAccName else { return false }
                return name1.localizedStandardCompare(name2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            sendMoneyBankReports = sortReceiverName(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            sendMoneyBankReports = sortReceiverName(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByBankName(_ sender: UIButton) {
        func sortBankName(orderType: ComparisonResult) -> [ReportSendMoneyToBank]{
            return sendMoneyBankReports.sorted {
                guard let bankName1 = $0.bankName, let bankName2 = $1.bankName else { return false }
                return bankName1.localizedStandardCompare(bankName2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            sendMoneyBankReports = sortBankName(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            sendMoneyBankReports = sortBankName(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByBranchName(_ sender: UIButton) {
        func sortBranchName(orderType: ComparisonResult) -> [ReportSendMoneyToBank] {
            return sendMoneyBankReports.sorted {
                guard let branchName1 = $0.bankBranchName, let branchName2 = $1.bankBranchName else { return false }
                return branchName1.localizedStandardCompare(branchName2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            sendMoneyBankReports = sortBranchName(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            sendMoneyBankReports = sortBranchName(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByAccNumber(_ sender: UIButton) {
        func sortAccNumber(orderType: ComparisonResult) -> [ReportSendMoneyToBank] {
            return sendMoneyBankReports.sorted {
                guard let accNumber1 = $0.bankAccNumber, let accNumber2 = $1.bankAccNumber else { return false }
                return accNumber1.localizedStandardCompare(accNumber2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            sendMoneyBankReports = sortAccNumber(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            sendMoneyBankReports = sortAccNumber(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByAccType(_ sender: UIButton) {
        func sortAccType(orderType: ComparisonResult) -> [ReportSendMoneyToBank] {
            return sendMoneyBankReports.sorted {
                guard let accType1 = $0.bankAccType, let accType2 = $1.bankAccType else { return false }
                return accType1.localizedStandardCompare(accType2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            sendMoneyBankReports = sortAccType(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            sendMoneyBankReports = sortAccType(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByAmount(_ sender: UIButton) {
        func sortAmount(orderType: ComparisonResult) -> [ReportSendMoneyToBank] {
            return sendMoneyBankReports.sorted {
                guard let amount1 = $0.transferredAmount, let amount2 = $1.transferredAmount else { return false }
                return amount1.compare(amount2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            sendMoneyBankReports = sortAmount(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            sendMoneyBankReports = sortAmount(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByTransId(_ sender: UIButton) {
        func sortTransID(orderType: ComparisonResult) -> [ReportSendMoneyToBank] {
            return sendMoneyBankReports.sorted {
                guard let id1 = $0.transactionID, let id2 = $1.transactionID else { return false }
                return id1.localizedStandardCompare(id2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            sendMoneyBankReports = sortTransID(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            sendMoneyBankReports = sortTransID(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByDateTime(_ sender: UIButton) {
        func sortDates(orderType: ComparisonResult) -> [ReportSendMoneyToBank]{
            return sendMoneyBankReports.sorted {
                guard let date1 = $0.transactionDateTime, let date2 = $1.transactionDateTime else { return false }
                return date1.compare(date2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            sendMoneyBankReports = sortDates(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            sendMoneyBankReports = sortDates(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByBalance(_ sender: UIButton) {
        func sortBalance(orderType: ComparisonResult) -> [ReportSendMoneyToBank] {
            return sendMoneyBankReports.sorted {
                guard let balance1 = $0.balance, let balance2 = $1.balance else { return false }
                return balance1.compare(balance2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            sendMoneyBankReports = sortBalance(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            sendMoneyBankReports = sortBalance(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func configureDatePickerOne(_ sender: UIButton) {
        configureDatePicker(tag: 0)
    }
    
    @IBAction func configureDatePickerTwo(_ sender: UIButton) {
        if buttonDateOne.isHidden {
            configureDatePicker(tag: 2)
        } else {
            configureDatePicker(tag: 1)
        }
    }
}

// MARK: - UITextFieldDelegate
extension ReportSendMoneyToBankController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tableViewFilter.isHidden = true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        tableViewSendMoneyToBank.contentOffset = CGPoint(x: 0, y: 0)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            if updatedText.hasPrefix(" ") {
                return false
            }
            if updatedText.count > 50 || (updatedText == "" && text == "") {
                return false
            }
            if string != "\n" {
                applyFilter(searchText: updatedText)
            }
        }
        return true
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ReportSendMoneyToBankController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        switch tableView.tag {
        case 0:
            if sendMoneyBankReports.count == 0 {
                let noDataLabel = UILabel(frame: tableView.bounds)
                noDataLabel.text = "No Records".localized
                noDataLabel.font = UIFont(name: appFont, size: 18)
                noDataLabel.textAlignment = .center
                tableView.backgroundView = noDataLabel
            } else {
                tableView.backgroundView = nil
            }
            return 1
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView.tag {
        case 0:
            return sendMoneyBankReports.count
        case 1:
            return filterArray.count
        default:
            return 0
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        targetContentOffset.pointee.y = max(targetContentOffset.pointee.y - 1, 1)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView.tag {
        case 0:
            let sendMoneyToBankCell = tableView.dequeueReusableCell(withIdentifier: sendMoneyToBankCellId, for: indexPath) as? ReportSendMoneyToBankCell
            sendMoneyToBankCell?.configureReportSendMoneyToBankCell(report: sendMoneyBankReports[indexPath.row])
            return sendMoneyToBankCell!
        case 1:
            let filterCell = tableView.dequeueReusableCell(withIdentifier: ConstantsXibCell.filterTableViewCell, for: indexPath) as? FilterTableViewCell
            filterCell?.configureLabel(labelTitle: filterArray[indexPath.row])
            return filterCell!
        default:
            let sendMoneyToBankCell = tableView.dequeueReusableCell(withIdentifier: sendMoneyToBankCellId, for: indexPath) as? ReportSendMoneyToBankCell
            return sendMoneyToBankCell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView.tag {
        case 0:
            break
        case 1:
            switch lastSelectedFilter {
            case .accountType:
                buttonFilterAccount.setTitle(filterArray[indexPath.row].localized, for: .normal)
                tableViewFilter.isHidden = true
                applyFilter(searchText: textFieldSearch.text ?? "")
            case .amount:
                buttonFilterAmount.setTitle(filterArray[indexPath.row].localized, for: .normal)
                tableViewFilter.isHidden = true
                applyFilter(searchText: textFieldSearch.text ?? "")
            case .period:
                tableViewFilter.isHidden = true
                showDateOptions(selectedOption: filterArray[indexPath.row].localized)
            }
        default:
            break
        }
    }
}

// MARK: - Additional Methods
extension ReportSendMoneyToBankController: NavigationSetUpProtocol {
    /// It set up the navigation bar
    func setUpNavigation() {
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationController?.navigationBar.barTintColor        = ConstantsColor.navigationHeaderSendMoney
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: titleViewWidth, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        
        let searchButton = UIButton(type: .custom)
        searchButton.setImage(#imageLiteral(resourceName: "reportSearch").withRenderingMode(.alwaysOriginal), for: .normal)
        searchButton.addTarget(self, action: #selector(showSearchView), for: .touchUpInside)
        
        let shareButton = UIButton(type: .custom)
        shareButton.setImage(#imageLiteral(resourceName: "reportShare").withRenderingMode(.alwaysOriginal), for: .normal)
        shareButton.addTarget(self, action: #selector(showShareOption), for: .touchUpInside)
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        let ypos = (titleViewSize.height - 30 ) / 2
        if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 160, y: ypos, width: 30, height: 30)
            shareButton.frame = CGRect(x: navTitleView.frame.size.width - 120, y: ypos, width: 30, height: 30)
            backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        }
        else{
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 70, y: ypos, width: 30, height: 30)
            shareButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
            backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        }
       
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 160, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text = ConstantsController.reportSendMoneyToBankController.screenTitle.localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 23) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        navTitleView.addSubview(searchButton)
        navTitleView.addSubview(shareButton)
        self.navigationItem.titleView = navTitleView
    }
    
    /// It shows the home screen
    @objc func backAction() {
        //        AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        //        self.navigationController?.popViewController(animated: true)
        if isSearchViewShow {
            hideSearchView()
        } else {
            AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension ReportSendMoneyToBankController: ReportModelDelegate {
    
    func apiResult(message: String?, statusCode: String?) {
        DispatchQueue.main.async {
            self.sendMoneyBankReports = self.modelReport.sendMoneyBankInitialReports
            self.resetSortButtons(sender: UIButton())
            self.resetFilterButtons()
            self.reloadTableWithScrollToTop()
            self.refreshControl.endRefreshing()
        }
    }
    
    func handleCustomError(error: CustomError) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}
