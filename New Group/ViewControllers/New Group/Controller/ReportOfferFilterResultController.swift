//
//  ReportOfferFilterResultController.swift
//  OK
//
//  Created by E J ANTONY on 13/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import MapKit

class ReportOfferFilterResultController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var tableFilterList: UITableView!
    
    @IBOutlet weak var mapHolder: UIView!
    @IBOutlet weak var listHolder: UIView!
    @IBOutlet weak var viewMap: MKMapView!
    @IBOutlet weak var constraintBTable: NSLayoutConstraint!
    
    //MARK: - Properties
    private var multiButton : ActionButton?

    private var cellIdentifier = "ReportOfferFilterResultCell"
    private lazy var modelAReport: ReportAModel = {
        return ReportAModel()
    }()
    var offerFilterList = [OfferReporFilterList]()
    private var isDataRequested = false
    private lazy var searchBar = UISearchBar()
    var promoCode = ""
    var promotionID = ""
    var amountType = 0
    var fromDateString = ""
    var toDateString = ""
    var townshipId: String?
    var advMerchantNo: String?
    var dummyMerchantNumber: [String]?
    
    //MARK: - Views Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tableFilterList.tableFooterView = UIView()
        listHolder.isHidden = false
        mapHolder.isHidden = true
        setUpNavigation()
        registerKeyboardNotification()
        searchBar = CodeSnippets.getSearchBar()
        searchBar.delegate = self
        modelAReport.reportAModelDelegate  = self
        self.loadFloatButtons()
        
        //Get List Record
        self.getDataWithFilter()
    }
    
    //API Request
    func getDataWithFilter() {
        let request = OfferResultFilterRequest(mobileNumber: UserModel.shared.mobileNo, promotionId: promotionID, toTransactionDate: toDateString, fromTransactionDate: fromDateString ,townshipId: townshipId, advMerchantNo: advMerchantNo, dummyMerchantNo: dummyMerchantNumber)
        modelAReport.getOfferResultFilterRequest(request: request)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func registerKeyboardNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func populateDataInMap() {
        var firstLat: Double?
        var firstLon: Double?
        for (index,item) in self.offerFilterList.enumerated() {
            guard let strLat = item.lattitude, let doubleLat = Double(strLat), let strLon = item.longitude, let doubleLon = Double(strLon) else { return }
            if index == 0 {
                firstLat = doubleLat
                firstLon = doubleLon
            }
            var anotTitle: String?
            var anotSubtitle: String?
            anotTitle = item.promotionUsedMobileNumber ?? ""
            if let amount = item.totalBillAmount {
                anotSubtitle = "\(amount) " + "MMK"
            }
            let annotation = MKPointAnnotation()
            annotation.title = anotTitle
            annotation.subtitle = anotSubtitle
            annotation.coordinate = CLLocationCoordinate2D(latitude: doubleLat, longitude: doubleLon)
            viewMap.addAnnotation(annotation)
        }
        guard let safeFirstLat = firstLat, let safeFirstLon = firstLon else { return }
        let location = CLLocationCoordinate2D(latitude: safeFirstLat,
                                              longitude: safeFirstLon)
        let span = MKCoordinateSpan.init(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: location, span: span)
        viewMap.setRegion(region, animated: true)
    }
    
    private func applySearch(with searchText: String) {
        var filterList = modelAReport.offerInitialFilterReports
        if searchText.count > 0 {
            filterList = filterList.filter {
                
                if let mNumber = $0.promotionUsedMobileNumber {
                    if mNumber.lowercased().range(of:searchText.lowercased()) != nil {
                        return true
                    }
                }
                if let nAmount = $0.totalNetAmount {
                    if "\(nAmount)".range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                return false
            }
        }
        offerFilterList = filterList
        tableFilterList.reloadData()
        populateDataInMap()
    }
  
    private func navigateToReportOfferMapVC(index: Int) {
        guard let reportOfferMapViewController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportOfferMapViewController.nameAndID) as? ReportOfferMapViewController else { return }
        reportOfferMapViewController.screenType = ReportOfferMapViewController.ScreenType.recordSummary
        reportOfferMapViewController.sourceDataArray = [offerFilterList[index]]
        reportOfferMapViewController.amountType = amountType
        self.navigationController?.pushViewController(reportOfferMapViewController, animated: true)
    }
    
    private func showShareOption() {
        guard let reportSharingViewController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportSharingViewController.nameAndID) as? ReportSharingViewController else { return }
        reportSharingViewController.delegate = self as ReportSharingOptionDelegte
        reportSharingViewController.pdfImage = UIImage(named: "sendMoneyPdf")
        reportSharingViewController.excelImage = UIImage(named: "sendMoneyExcel")
        reportSharingViewController.modalPresentationStyle = .overCurrentContext
        self.present(reportSharingViewController, animated: false, completion: nil)
    }
    
    // MARK: - Custom Methods
    func loadFloatButtons() {
        var buttonItems = [ActionButtonItem]()
        
        if self.offerFilterList.count > 0 {
            let share = ActionButtonItem(title: "Share".localized, image: #imageLiteral(resourceName: "share_qr"))
            share.action = { item in
                self.multiButton?.toggleMenu()
                self.showShareOption()
            }
            
            let summary = ActionButtonItem(title: "Summary".localized, image: #imageLiteral(resourceName: "reportSummary"))
            summary.action = { item in
                self.multiButton?.toggleMenu()
                guard let reportOfferMapViewController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportOfferMapViewController.nameAndID) as? ReportOfferMapViewController else { return }
                reportOfferMapViewController.screenType = ReportOfferMapViewController.ScreenType.totalSummary
                reportOfferMapViewController.sourceDataArray = self.offerFilterList
                reportOfferMapViewController.amountType = self.amountType
                self.navigationController?.pushViewController(reportOfferMapViewController, animated: true)
            }
            
            buttonItems = [summary, share]
        }
        multiButton = ActionButton(attachedToView: self.view, items: buttonItems)
        multiButton?.notifyBlurDelegate = self
        multiButton?.action = {
            button in button.toggleMenu()
        }
        multiButton?.setImage(#imageLiteral(resourceName: "menu_white_payto"), forState: .normal)
        multiButton?.backgroundColor = kYellowColor
    }
    
    // MARK: - Target Methods
    @objc func showSearchOption() {
    
        searchBar.placeholder = "Search".localized
        searchBar.text = ""
        self.navigationItem.titleView = searchBar
        searchBar.becomeFirstResponder()
    }
    
    @objc func showMapListOption() {
       
        if listHolder.isHidden {
            listHolder.isHidden = false
            mapHolder.isHidden = true
        } else {
            listHolder.isHidden = true
            mapHolder.isHidden = false
        }
        setUpNavigation()
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.constraintBTable.constant = keyboardSize.height
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.constraintBTable.constant = 0
        self.view.layoutIfNeeded()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}



// MARK: - UISearchBarDelegate
extension ReportOfferFilterResultController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            uiButton.setTitle("Cancel".localized, for:.normal)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            offerFilterList = modelAReport.offerInitialFilterReports
            tableFilterList.reloadData()
            populateDataInMap()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
            let updatedText = searchText.replacingCharacters(in: textRange, with: text)
            if updatedText.count > 50 {
                return false
            }
            if updatedText != "" && text != "\n" {
                applySearch(with: updatedText)
            }
        }
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        offerFilterList = modelAReport.offerInitialFilterReports
        setUpNavigation()
        tableFilterList.reloadData()
        populateDataInMap()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ReportOfferFilterResultController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if offerFilterList.count == 0 {
            let noDataLabel = UILabel(frame: tableView.bounds)
            if isDataRequested {
                noDataLabel.text = "No Records".localized
            }
            noDataLabel.font = UIFont(name: appFont, size: 18)
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
        } else {
            tableView.backgroundView = nil
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offerFilterList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let reportOfferFilterResultCell =  tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ReportOfferFilterResultCell else {
            return ReportOfferFilterResultCell()
        }
        reportOfferFilterResultCell.configureCell(data: offerFilterList[indexPath.row], amountType: amountType)
        return reportOfferFilterResultCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigateToReportOfferMapVC(index: indexPath.row)
    }
}

// MARK: - MKMapViewDelegate
extension ReportOfferFilterResultController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }
        
        let annotationIdentifier = "AnnotationIdentifier"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
        
        if let viewAnnotation = annotationView {
            viewAnnotation.annotation = annotation
        } else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView!.canShowCallout = true
            let button = UIButton(type: UIButton.ButtonType.detailDisclosure) as UIButton // button with info sign in it
            annotationView?.rightCalloutAccessoryView = button
        }
        
        let pinImage = UIImage(named: "offer_report_map_annotation")
        annotationView!.image = pinImage
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if control == view.rightCalloutAccessoryView {
            if let coordinate = view.annotation?.coordinate {
                let itemIndex = offerFilterList.index {
                    guard let strLat = $0.lattitude, let doubleLat = Double(strLat), let strLon = $0.longitude, let doubleLon = Double(strLon) else { return false}
                    if doubleLat == coordinate.latitude && doubleLon == coordinate.longitude {
                        return true
                    } else {
                        return false
                    }
                }
                if let indexValue = itemIndex {
                    self.navigateToReportOfferMapVC(index: indexValue)
                }
            }
        }
    }
}

// MARK: - Additional Methods
extension ReportOfferFilterResultController: NavigationSetUpProtocol {
    func setUpNavigation() {
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        
        let ypos = (titleViewSize.height - 30 ) / 2
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        var label: MarqueeLabel!
        
        //if self.offerFilterList.count > 0 {
            let searchButton = UIButton(type: .custom)
            searchButton.setImage(#imageLiteral(resourceName: "search_white").withRenderingMode(.alwaysOriginal), for: .normal)
            searchButton.addTarget(self, action: #selector(showSearchOption), for: .touchUpInside)
            
            let mapListButton = UIButton(type: .custom)
            if listHolder.isHidden {
                mapListButton.setImage(#imageLiteral(resourceName: "offer_report_nav_list_icon").withRenderingMode(.alwaysOriginal), for: .normal)
            } else {
                mapListButton.setImage(#imageLiteral(resourceName: "map_white").withRenderingMode(.alwaysOriginal), for: .normal)
            }
            mapListButton.addTarget(self, action: #selector(showMapListOption), for: .touchUpInside)
        if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 140, y: ypos, width: 30, height: 30)
            mapListButton.frame = CGRect(x: navTitleView.frame.size.width - 100, y: ypos, width: 30, height: 30)
        }
        else{
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 70, y: ypos, width: 30, height: 30)
            mapListButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
        }
            label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 160, height: navTitleView.frame.size.height))
            
            //navTitleView.addSubview(searchButton)
            navTitleView.addSubview(mapListButton)
            
//        } else {
//            label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
//        }
        label.type = .continuous
        label.speed = .duration(4)
        label.text = ConstantsController.reportOfferFilterResultController.screenTitle.localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
}

//MARK: - NotifyBlurDelegate
extension ReportOfferFilterResultController: NotifyBlurDelegate {
    func blurRemoveDelegate() {
    }
}

// MARK: - ReportModelDelegate
extension ReportOfferFilterResultController: ReportModelDelegate {
    
    func apiResult(message: String?, statusCode: String?) {
        DispatchQueue.main.async {
             self.isDataRequested = true
             self.offerFilterList = self.modelAReport.offerInitialFilterReports
             
             if self.offerFilterList.count > 0 {
                self.loadFloatButtons()
                self.tableFilterList.reloadData()
                self.populateDataInMap()
             }
        }
    }
    
    func handleCustomError(error: CustomError) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}

//MARK: - ReportSharingOptionDelegte
extension ReportOfferFilterResultController: ReportSharingOptionDelegte {
    func share(by: ReportSharingViewController.ReportShareOption) {
        switch by {
        case .pdf:
            shareRecordsByPdf()
        case .excel:
            shareRecordsByExcel()
        }
    }
    
    private func shareRecordsByPdf() {
        let pdfView = UIView()
        if let pdfHeaderView = Bundle.main.loadNibNamed("OfferReportPdfHeader", owner: self, options: nil)?.first as? OfferReportPdfHeader {
            
            pdfHeaderView.configureWithDatas(reportList: offerFilterList, amountType: self.amountType)
            
            let newHeight = pdfHeaderView.viewForItemHeader.frame.maxY
            pdfHeaderView.frame = CGRect(x: 0, y: 0, width: pdfHeaderView.frame.size.width, height: newHeight)
            pdfView.addSubview(pdfHeaderView)
            pdfView.frame = pdfHeaderView.bounds
            let xPos: CGFloat = pdfHeaderView.viewForItemHeader.frame.minX
            var yPos: CGFloat = pdfView.frame.maxY
            
            for record in offerFilterList {
                if let pdfBodyView = Bundle.main.loadNibNamed("OfferReportPdfBody", owner: self, options: nil)?.first as? OfferReportPdfBody {
                    pdfBodyView.configureData(data: record, amountType: self.amountType)
                    let recordHeight = pdfBodyView.frame.size.height
                    let yPosInPage = Int(yPos) % 842
                    if (yPosInPage + Int(recordHeight)) > 842 {
                        yPos = yPos + CGFloat((842 - yPosInPage) + 30 + 30)
                    }
                    pdfBodyView.frame = CGRect(x: xPos, y: yPos, width: pdfBodyView.frame.size.width, height: recordHeight)
                    yPos = yPos + recordHeight + 2
                    pdfView.addSubview(pdfBodyView)
                }
            }
            pdfView.frame = CGRect(x: 0, y: 0, width: pdfView.frame.size.width, height: yPos + 30)
            guard let pdfUrl = CodeSnippets.generatePDF(viewForPDF: pdfView, pdfFile: "OK$ Offers Report") else {
                println_debug("Error - pdf not generated")
                return
            }
            let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
            DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
        }
    }
    
    private func shareRecordsByExcel() {
        var excelTxt = ""
        switch amountType {
        case 0,1:
            excelTxt = "Promotion Code,Account Name,Account Number,Bill Amount (MMK),Discount Amount (MMK),Discount Percentage (%),Net Amount (MMK),Transaction ID,Date & Time\n"
        case 2:
            excelTxt = "Promotion Code,Account Name,Account Number,Bill Amount (MMK),Sale Quantity,Free Quantity,Transaction ID,Date & Time\n"
        case 3,4:
            excelTxt = "Promotion Code,Account Name,Account Number,Bill Amount (MMK),Discount Amount (MMK),Discount Percentage,Net Amount (MMK),Sale Quantity,Free Quantity,Transaction ID,Date & Time\n"
        default:
            excelTxt = "Promotion Code,Account Name,Account Number,Bill Amount (MMK),Discount Amount (MMK),Discount Percentage,Net Amount (MMK),Sale Quantity,Free Quantity,Transaction ID,Date & Time\n"
        }
        for data in offerFilterList {
            var accName = ""
            if let cName = data.promotionUsedCustomerName {
                accName = cName.replacingOccurrences(of: ",", with: " ")
            }
            var accNumber = ""
            if var mN = data.promotionUsedMobileNumber {
                accNumber = mN
                if mN.hasPrefix("0095") {
                    mN = "0" + String(mN.substring(from: 4))
                    accNumber = mN
                }
            }
            var billAmount = "0"
            if let bAmount = data.totalNetAmount {
                billAmount = "\(bAmount)".replacingOccurrences(of: ".00", with: "").replacingOccurrences(of: ".0", with: "")
            }
            var discountAmount = "0"
            var discountPercentage = "0"
            if let discAmount = data.totalDiscountAmount {
                discountAmount = "\(discAmount)".replacingOccurrences(of: ".0", with: "")
                let percentageValue = Float((Float(discAmount) * 100) / Float(data.totalNetAmount ?? 0))
                discountPercentage = "\(CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(percentageValue)"))"
            }
            var saleQty = "0"
            var freeQty = "0"
            if data.productValidationRuleValue == 2 {
                if let nAmount = data.buyingQty {
                    saleQty = "\(nAmount)"
                }
                if let nAmount = data.freeQty {
                    freeQty = "\(nAmount)"
                }
            } else {
                if let nAmount = data.buyingQty {
                    saleQty = "\(nAmount)"
                }
                if let nAmount = data.freeQty {
                    freeQty = "\(nAmount)"
                }
            }
            var netReceivedAmount = "0"
            if let nAmount = data.totalBillAmount {
                netReceivedAmount = "\(nAmount)".replacingOccurrences(of: ".00", with: "").replacingOccurrences(of: ".0", with: "")
            }
            var transactionID = ""
            if let transID = data.parentTransactionID {
                transactionID = transID
            }
            var promoCode = ""
            if let pCode = data.promotionCode {
                promoCode = "\(pCode)"
            }
            var dateTime = ""
            let dateStr = data.transactionTime
            if let dateVal = dateStr?.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") {
                let transDate = dateVal.stringValue(dateFormatIs: "EEE dd-MMM-yyyy HH:mm:ss")
                dateTime = transDate
            }
            var recText = ""
            switch amountType {
            case 0,1:
                recText = "\(promoCode),\(accName),\(accNumber),\(billAmount),\(discountAmount),\(discountPercentage),\(netReceivedAmount),\(transactionID),\(dateTime)\n"
            case 2:
                recText = "\(promoCode),\(accName),\(accNumber),\(billAmount),\(saleQty),\(freeQty),\(transactionID),\(dateTime)\n"
            case 3,4:
                recText = "\(promoCode),\(accName),\(accNumber),\(billAmount),\(discountAmount),\(discountPercentage),\(netReceivedAmount),\(saleQty),\(freeQty),\(transactionID),\(dateTime)\n"
            default:
                recText = "\(promoCode),\(accName),\(accNumber),\(billAmount),\(discountAmount),\(discountPercentage),\(netReceivedAmount),\(saleQty),\(freeQty),\(transactionID),\(dateTime)\n"
            }
            excelTxt.append(recText)
        }
        guard let excelUrl = CodeSnippets.generateExcel(excelDataWithFormat: excelTxt, excelFileName: "OK$ Offers Report") else {
            println_debug("Error - excel not generated")
            return
        }
        let activityViewController = UIActivityViewController(activityItems: [excelUrl], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
}

