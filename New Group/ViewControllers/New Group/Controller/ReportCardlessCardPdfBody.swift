//
//  ReportAddWithdrawPdfBody.swift
//  OK
//
//  Created by E J ANTONY on 01/12/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit

class ReportCardlessCardPdfBody: UIView {
    //MARK: - Outlet
    @IBOutlet weak var lblReceiver: UILabel!{
        didSet {
            lblReceiver.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblCardType: UILabel!{
        didSet {
            lblCardType.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblCardNumber: UILabel!{
        didSet {
            lblCardNumber.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblAmount: UILabel!{
        didSet {
            lblAmount.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblFees: UILabel!{
        didSet {
            lblFees.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblTransID: UILabel!{
        didSet {
            lblTransID.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblBankTransID: UILabel!{
        didSet {
            lblBankTransID.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblDate: UILabel!{
        didSet {
            lblDate.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    //MARK: - Methods
    func configureData(awReportData: AddWithdrawData) {
        
        var country = ""
        var number = ""
        if let num = awReportData.beneficiaryAccountNumber, num.hasPrefix("00") {
            number = String(num.dropFirst(4))
            country = String(num.prefix(4))
            country = country.replacingOccurrences(of: "00", with: "+")
        } else {
            country = "+95"
            if let num = awReportData.beneficiaryAccountNumber {
                number = String(num.dropFirst())
            }
        }
        let rNo = CodeSnippets.getMobileNumWithBrackets(countryCode: country, number: "0\(number)")
        self.lblReceiver.text = rNo
        if let cardTypeL = awReportData.addMoneyType?.uppercased(), cardTypeL == "MASTERPAY" {
            self.lblCardType.text = "VISA"
        } else {
            self.lblCardType.text = awReportData.addMoneyType?.uppercased()
        }
        self.lblCardNumber.text = "XXXXXXXXXXXX"
        if let cNumber = awReportData.cardNumber {
            self.lblCardNumber.text = cNumber
        }
        self.lblAmount.text = ""
        self.lblFees.text = ""
        if let amount = awReportData.amount {
            self.lblAmount.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(amount)")
        }
        if let amount = awReportData.adminFee {
            self.lblFees.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(amount)")
        }
        self.lblTransID.text = awReportData.okTransactionID
        self.lblBankTransID.text = awReportData.bankTransactionID
        self.lblDate.text = ""
        if let dateStr = awReportData.transactionDate {
            if let dateValue = dateStr.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") {
                let dateString = dateValue.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss")
                self.lblDate.text = dateString
            }
        }
    }
}
