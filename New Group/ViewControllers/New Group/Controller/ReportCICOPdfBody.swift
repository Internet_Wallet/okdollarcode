//
//  ReportCICOPdfBody.swift
//  OK
//
//  Created by E J ANTONY on 04/10/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportCICOPdfBody: UIView {

    //MARK: - Outlet
    @IBOutlet weak var lblPaymentType: UILabel!{
        didSet {
            lblPaymentType.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblTransID: UILabel!{
        didSet {
            lblTransID.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblMobileNumber: UILabel!{
        didSet {
            lblMobileNumber.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblCashInAmount: UILabel!{
        didSet {
            lblCashInAmount.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblCashOutAmount: UILabel!{
        didSet {
            lblCashOutAmount.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblCommission: UILabel!{
        didSet {
            lblCommission.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblBalance: UILabel!{
        didSet {
            lblBalance.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblDateTime: UILabel!{
        didSet {
            lblDateTime.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var initiateAmountLbl: UILabel!{
        didSet {
            initiateAmountLbl.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    //MARK: - Methods
    func configureData(data: ReportCICOList) {
        self.lblPaymentType.text = "CashOut"
        self.lblCashInAmount.text = "0"
        self.lblCashOutAmount.text = "0"
        if data.cashType == 0 {
            self.lblPaymentType.text = "CashIn"
        }
        self.lblTransID.text = ""
        if let tID = data.transactionID {
            self.lblTransID.text = tID
        }
        if let phNo = data.receiverPhoneNumber {
            if phNo == UserModel.shared.mobileNo {
                if let rqstPhNo = data.requesterPhoneNumber {
                    if rqstPhNo.hasPrefix("0095") {
                        lblMobileNumber.text = "0\(rqstPhNo.substring(from: 4))"
                    }
                }
            } else {
                if let payType = data.paymentType, (payType == "PAYTOID" || payType == "PAYWITHOUT ID") {
                    lblMobileNumber.text = "XXXXXXXXXX"
                } else {
                    if phNo.hasPrefix("0095") {
                        lblMobileNumber.text = "0\(phNo.substring(from: 4))"
                    }
                }
            }
        }
        
//        if let tAmount = data.totalAmount, let commission = data.commissionPercent {
//             if data.cashType == 0 && ((data.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo) {
//                let amountStr = tAmount + commission
//                let value = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(amountStr)")
//                self.lblCashInAmount.text = value
//            } else {
//                if data.cashType == 0 && ((data.receiverPhoneNumber ?? "") == UserModel.shared.mobileNo) {
//                    let amountStr = tAmount - commission
//                    let value = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(amountStr)")
//                    self.lblCashInAmount.text = value
//                } else {
//                    let value = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(tAmount)")
//                    self.lblCashOutAmount.text = value
//                }
//            }
//        }
        
        if (data.cashType == 0 && ((data.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo)) ||  (data.cashType == 1 && ((data.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo)) {
            
            lblCashInAmount.text = "0"
            lblCashOutAmount.text = "0"
            if data.cashType == 0 && ((data.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo) {
                if let commisson = data.commissionPercent, let totAmount = data.rechargeAmount {
                    let initiateAmt = totAmount + commisson
                    lblCashInAmount.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(totAmount)")
                    initiateAmountLbl.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(initiateAmt)")
                }
            } else {
                if let amountStr = data.totalAmount {
                    lblCashInAmount.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(amountStr)")
                    initiateAmountLbl.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(amountStr)")
                }
            }
        }  else {
            lblCashInAmount.text = "0"
            lblCashOutAmount.text = "0"
            if data.cashType == 0 && ((data.receiverPhoneNumber ?? "") == UserModel.shared.mobileNo) {
                if let commisson = data.commissionPercent, let totAmount = data.rechargeAmount {
                    let initiateAmt = totAmount + commisson
                    lblCashOutAmount.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(totAmount)")
                    initiateAmountLbl.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(initiateAmt)")
                }
            } else {
                if let amountStr = data.totalAmount {
                    lblCashOutAmount.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(amountStr)")
                    initiateAmountLbl.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(amountStr)")                }
            }
        }

//        if data.cashType == 0 {
//            initiateAmountLbl.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(data.totalAmount ?? 0.0)")
//        }
        lblCommission.text = "0"
        if let amountStr = data.commissionPercent {
            lblCommission.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(amountStr)")
        }
        if let phNo = data.receiverPhoneNumber, phNo == UserModel.shared.mobileNo {
            lblBalance.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(UserLogin.shared.walletBal)")
        } else if let balance = data.senderClosingBalance {
            lblBalance.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(balance)")
        }
        if let dateStr = data.convertedDate {
            if let dateInDF = dateStr.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") {
                lblDateTime.text = dateInDF.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy\nHH:mm:ss")
            }
        }
    }
}
