//
//  ReportVotingListViewController.swift
//  OK
//
//  Created by E J ANTONY on 07/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportVotingListViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var tableViewVoteList: UITableView!
    @IBOutlet weak var tableViewFilter: UITableView!
    @IBOutlet weak var constraintScrollContentHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var constraintFilterTableLeading: NSLayoutConstraint!
    @IBOutlet weak var constraintFilterTableHeight: NSLayoutConstraint!
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var constraintScrollTop: NSLayoutConstraint!
    
    //For Search
    @IBOutlet weak var buttonCancel: UIButton!{
        didSet {
            buttonCancel.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonClear: UIButton!{
        didSet {
            buttonClear.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    //For filter function
    @IBOutlet weak var buttonFilterVotingType: ButtonWithImage!{
        didSet {
            buttonFilterVotingType.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonFilterPeriod: ButtonWithImage!{
        didSet {
            buttonFilterPeriod.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    //For sort function
    @IBOutlet weak var buttonProgName: ButtonWithImage!{
        didSet {
            buttonProgName.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonEventName: ButtonWithImage!{
        didSet {
            buttonEventName.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonParticipateName: ButtonWithImage!{
        didSet {
            buttonParticipateName.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonTransID: ButtonWithImage!{
        didSet {
            buttonTransID.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonVotes: ButtonWithImage!{
        didSet {
            buttonVotes.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonVotingType: ButtonWithImage!{
        didSet {
            buttonVotingType.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonAmount: ButtonWithImage!{
        didSet {
            buttonAmount.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDate: ButtonWithImage!{
        didSet {
            buttonDate.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    //MARK: - Properties
    private let reportVoteCellID = "ReportVotingTableCell"
    private lazy var modelAReport: ReportAModel = {
        return ReportAModel()
    }()
    var reportVotingList = [VotingReportList]()
    var filterArray = [String]()
    let voteTypes = ["Voting type", "All", "Online", "Sms"]
    let periodTypes = ["Period", "All", "Date", "Month"]
    var lastSelectedFilter = RootFilter.voteType
    var fromDate: Date?
    var toDate: Date?
    var isSearchViewShow: Bool {
        get {
            return !viewSearch.isHidden
        }
        set(newValue) {
            if newValue {
                viewSearch.isHidden = false
                textFieldSearch.becomeFirstResponder()
                constraintScrollTop.constant = 0
                self.view.layoutIfNeeded()
            } else {
                textFieldSearch.text = ""
                viewSearch.isHidden = true
                constraintScrollTop.constant = -40
                self.view.layoutIfNeeded()
                self.view.endEditing(true)
            }
        }
    }
    
    enum RootFilter {
        case voteType, period
    }
    
    private let refreshControl = UIRefreshControl()
    
    //MARK: - Views life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldSearch.placeholder = "Search By Programme Name, Event, Participate Name, Votes or Amount".localized
        tableViewVoteList.tag = 0
        tableViewFilter.tag = 1
        tableViewVoteList.tableFooterView = UIView()
        tableViewFilter.tableFooterView = UIView()
        tableViewVoteList.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshTableData), for: .valueChanged)
        buttonCancel.layer.cornerRadius = 15
        buttonClear.layer.cornerRadius = 15
        buttonCancel.addTarget(self, action: #selector(hideSearchView), for: .touchUpInside)
        buttonClear.addTarget(self, action: #selector(clearSerchTextField), for: .touchUpInside)
        buttonCancel.setTitle("Cancel".localized, for: .normal)
        buttonClear.setTitle("Clear".localized, for: .normal)
        setLocalizedTitles()
        let cellFilterNibName = UINib(nibName: ConstantsXibCell.filterTableViewCell, bundle: nil)
        tableViewFilter.register(cellFilterNibName, forCellReuseIdentifier: ConstantsXibCell.filterTableViewCell)
        AppDelegate.AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)
        setUpNavigation()
        modelAReport.reportAModelDelegate = self
        getVotingReport()
        resetFilterButtons()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        let navBarHeight = navigationController?.navigationBar.frame.size.height ?? 44
        let heightToReduce = navBarHeight == 32 ? 56 : 44
        constraintScrollContentHeight.constant = scrollView.frame.size.height - CGFloat(heightToReduce) + 44
        self.view.layoutIfNeeded()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Target Methods
    @objc func showSearchView() {
        tableViewFilter.isHidden = true
        if !isSearchViewShow {
            if reportVotingList.count > 0 {
                isSearchViewShow = true
                textFieldSearch.becomeFirstResponder()
            } else {
                noRecordAlertAction()
            }
        } else {
            isSearchViewShow = false
            self.hideSearchView()
        }
    }
    
    @objc func showShareOption() {
        tableViewFilter.isHidden = true
        if reportVotingList.count < 1 {
            noRecordAlertAction()
        } else {
            guard let reportSharingViewController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportSharingViewController.nameAndID) as? ReportSharingViewController else { return }
            reportSharingViewController.delegate = self
            reportSharingViewController.pdfImage = UIImage(named: "sendMoneyPdf")
            reportSharingViewController.excelImage = UIImage(named: "sendMoneyExcel")
            reportSharingViewController.modalPresentationStyle = .overCurrentContext
            self.present(reportSharingViewController, animated: false, completion: nil)
        }
    }
    
    @objc func refreshTableData() {
        isSearchViewShow = false
        self.hideSearchView()
        getVotingReport()
    }
    
    @objc func hideSearchView() {
        self.view.endEditing(true)
        textFieldSearch.text = ""
        viewSearch.isHidden = true
        constraintScrollTop.constant = -40
        self.view.layoutIfNeeded()
        self.view.endEditing(true)
        applyFilter()
    }
    
    @objc private func clearSerchTextField() {
        textFieldSearch.text = ""
        applyFilter()
    }
    
    //MARK: - Methods
    func getVotingReport() {
        getTokenFromServer(completionHandler: { (isSuccess, tokenString) in
            guard isSuccess, let tokenAuthStr = tokenString as? String else {
                progressViewObj.removeProgressView()
                self.showErrorAlert(errMessage: "try again")
                return
            }
            DispatchQueue.main.async(execute: {
                self.modelAReport.getVotingReportList(authToken: tokenAuthStr)
            })
        })
    }
    
    func getTokenFromServer(completionHandler: @escaping (Bool,Any?) -> Void) {
        
        let urlStr = String.init(format: Url.votingToken)
        let url = URL.init(string: urlStr)
        guard let tokenUrl = url else {
            showErrorAlert(errMessage: "Something went wrong : Dev need to check")
            return
        }
        println_debug(tokenUrl)
        let hashValue = VotingConstants.ApiKeys.accessKey.hmac_SHA1(key: VotingConstants.ApiKeys.secretKey)
        let inputVal = "password=\(hashValue)&grant_type=password"
        
        progressViewObj.showProgressView()
        println_debug("get token from server called........")
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: kMethod_Post, contentType: kContentType_urlencoded, inputVal: inputVal, authStr: nil)
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    println_debug("remove progress view called inside get token from server call")
                }
                
                completionHandler(false,nil)
                return
            }
            
            println_debug("response dict for get token from server :: \(String(describing: response))")
            let responseDic = response as! NSDictionary
            guard let tokentype = responseDic.value(forKey: "token_type"), let accesstoken = responseDic.value(forKey: "access_token") else {
                
                completionHandler(false,nil)
                return
            }
            
            let authorizationString =  "\(tokentype) \(accesstoken)"
            completionHandler(true,authorizationString)
        })
    }
    
    func showDateOptions(selectedOption: String) {
        switch selectedOption {
        case "Period".localized, "All".localized:
            self.fromDate = nil
            self.toDate = nil
            buttonFilterPeriod.setTitle(selectedOption, for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        case "Date".localized:
            presentDatePicker(mode: DateMode.fromToDate)
        case "Month".localized:
            presentDatePicker(mode: DateMode.month)
        default:
            break
        }
    }
    
    func presentDatePicker(mode: DateMode) {
        guard let reportDatePickerController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportDatePickerController.nameAndID) as? ReportDatePickerController else { return }
        reportDatePickerController.dateMode = mode
        reportDatePickerController.delegate = self
        reportDatePickerController.modalPresentationStyle = .overCurrentContext
        self.present(reportDatePickerController, animated: false, completion: nil)
    }
    
    func applyFilter(searchText: String = "") {
        var filteredArray = modelAReport.votingInitialReports
        filteredArray = filterProcess(forVotingType: filteredArray)
        filteredArray = filterProcess(forPeriod: filteredArray)
        if isSearchViewShow && searchText.count > 0 {
            filteredArray = filteredArray.filter {
                if let mProgram = $0.mainProgrammeName {
                    var programName = mProgram
                    if let program = $0.programmeName {
                        programName = programName + " " + program
                    }
                    if "\(programName.lowercased())".range(of:searchText.lowercased()) != nil {
                        return true
                    }
                }
                if let eventName = $0.eventName {
                    if eventName.lowercased().range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                if let participateName = $0.participantName {
                    if participateName.lowercased().range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                if let voteCount = $0.votes {
                    let voteCountString = String(voteCount)
                    if voteCountString.range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                if let voteCount = $0.votes {
                    let voteCountString = String(voteCount)
                    if voteCountString.range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                if let amount = $0.payment?.amount {
                    let paymentAmount = "\(amount)"
                    if paymentAmount.range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                return false
            }
        }
        reportVotingList = filteredArray
        reloadTableWithScrollToTop()
        resetSortButtons(sender: UIButton())
    }
    
    func filterProcess(forVotingType arrayToFilter: [VotingReportList]) -> [VotingReportList] {
        if let transFilter = buttonFilterVotingType.currentTitle {
            switch transFilter {
            case "Voting type".localized, "All".localized:
                return arrayToFilter
            case "Online".localized:
                return arrayToFilter.filter {
                    guard let voteType = $0.votingTypeName else { return false }
                    return voteType.contains("Online")
                }
            case "Sms".localized:
                return arrayToFilter.filter {
                    guard let voteType = $0.votingTypeName else { return false }
                    return voteType.contains("Sms")
                }
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcess(forPeriod arrayToFilter: [VotingReportList]) -> [VotingReportList] {
        if let periodFilter = buttonFilterPeriod.currentTitle {
            switch periodFilter {
            case "Period".localized, "All".localized:
                return arrayToFilter
            default:
                break
            }
            if let exactDateStart = self.fromDate {
                return arrayToFilter.filter {
                    guard let cDate = $0.createdDate else { return false }
                    guard let createdDate = cDate.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") else { return false }
                    guard let dateStart = Calendar.current.date(byAdding: Calendar.Component.day, value: -1, to: exactDateStart) else { return false }
                    guard let exactDateEnd = self.toDate else { return false }
                    guard let dateEnd = Calendar.current.date(byAdding: Calendar.Component.day, value: 1, to: exactDateEnd) else { return false}
                    return (dateStart.compare(createdDate) == .orderedAscending && createdDate.compare(dateEnd) == .orderedAscending)
                }
            } else {
                return arrayToFilter.filter {
                    guard let cDate = $0.createdDate else { return false }
                    guard let createdDate = cDate.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") else { return false }
                    let crDateStr = createdDate.stringValue(dateFormatIs: "yyyy-MM")
                    guard let createdMonth = crDateStr.dateValue(dateFormatIs: "yyyy-MM") else { return false }
                    guard let monthToFilter = self.toDate else { return false }
                    return createdMonth.compare(monthToFilter) == .orderedSame
                }
            }
        }
        return []
    }
    
    func reloadTableWithScrollToTop() {
        self.tableViewVoteList.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            if self.reportVotingList.count > 0 {
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableViewVoteList.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
    }
    
    //MARK: - Button Action Methods
    @IBAction func filterByVoteType(_ sender: UIButton) {
        self.view.endEditing(true)
        func showListAtVoteType() {
            filterArray = voteTypes
            tableViewFilter.reloadData()
            constraintFilterTableLeading.constant = 120
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
            tableViewFilter.isHidden = false
        }
        if tableViewFilter.isHidden {
            showListAtVoteType()
        } else {
            switch lastSelectedFilter {
            case .voteType :
                tableViewFilter.isHidden = true
            default:
                showListAtVoteType()
            }
        }
        lastSelectedFilter = .voteType
    }
    
    @IBAction func filterByPeriod(_ sender: UIButton) {
        self.view.endEditing(true)
        func showListAtPeriod() {
            filterArray = periodTypes
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 520
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
        }
        if tableViewFilter.isHidden {
            showListAtPeriod()
        } else {
            switch lastSelectedFilter {
            case .period :
                tableViewFilter.isHidden = true
            default:
                showListAtPeriod()
            }
        }
        lastSelectedFilter = .period
    }
}

//MARK: - Outlets
extension ReportVotingListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        switch tableView.tag {
        case 0:
            if reportVotingList.count == 0 {
                let noDataLabel = UILabel(frame: tableView.bounds)
                noDataLabel.text = "No Records".localized
                noDataLabel.font = UIFont(name: appFont, size: 18)
                noDataLabel.textAlignment = .center
                tableView.backgroundView = noDataLabel
            } else {
                tableView.backgroundView = nil
            }
            return 1
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView.tag {
        case 0:
            return reportVotingList.count
        case 1:
            return filterArray.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView.tag {
        case 0:
            guard let reportVoteCell = tableView.dequeueReusableCell(withIdentifier: reportVoteCellID, for: indexPath) as? ReportVotingTableCell else { return ReportVotingTableCell() }
            let voteData = reportVotingList[indexPath.row]
            reportVoteCell.configureCell(voteData: voteData)
            return reportVoteCell
        case 1:
            let filterCell = tableView.dequeueReusableCell(withIdentifier: ConstantsXibCell.filterTableViewCell, for: indexPath) as? FilterTableViewCell
            filterCell?.configureLabel(labelTitle: filterArray[indexPath.row])
            return filterCell!
        default:
            guard let reportVoteCell = tableView.dequeueReusableCell(withIdentifier: reportVoteCellID, for: indexPath) as? ReportVotingTableCell else { return ReportVotingTableCell() }
            return reportVoteCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView.tag {
        case 0:
            break
        case 1:
            switch lastSelectedFilter {
            case .voteType:
                buttonFilterVotingType.setTitle(filterArray[indexPath.row].localized, for: .normal)
                tableViewFilter.isHidden = true
                applyFilter(searchText: textFieldSearch.text ?? "")
            case .period:
                tableViewFilter.isHidden = true
                showDateOptions(selectedOption: filterArray[indexPath.row].localized)
            }
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch tableView.tag {
        case 1:
            return 44.0
        default:
            tableView.estimatedRowHeight = 160
            tableView.rowHeight = UITableView.automaticDimension
            return tableView.rowHeight
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        targetContentOffset.pointee.y = max(targetContentOffset.pointee.y - 1, 1)
    }
}

//MARK: - ReportSharingOptionDelegte
extension ReportVotingListViewController: ReportSharingOptionDelegte {
    func share(by: ReportSharingViewController.ReportShareOption) {
        switch by {
        case .pdf:
            shareRecordsByPdf()
        case .excel:
            shareRecordsByExcel()
        }
    }
}

// MARK: - ReportDatePickerDelegate
extension ReportVotingListViewController: ReportDatePickerDelegate {
    func processWithDate(fromDate: Date?, toDate: Date?, dateMode: DateMode) {
        self.fromDate = fromDate
        self.toDate = toDate
        var selectedDate = ""
        switch dateMode {
        case .fromToDate:
            if let safeFDate = fromDate, let safeTDate = toDate {
                let fDateInString = safeFDate.stringValue(dateFormatIs: "dd MMM yyyy")
                let tDateInString = safeTDate.stringValue(dateFormatIs: "dd MMM yyyy")
                selectedDate = fDateInString + " to\n" + tDateInString
            }
        case .month:
            if let safeDate = toDate {
                let monthInString = safeDate.stringValue(dateFormatIs: "MMM yyyy")
                selectedDate = monthInString
            }
        default:
            break
        }
        buttonFilterPeriod.setTitle(selectedDate, for: .normal)
        applyFilter(searchText: textFieldSearch.text ?? "")
    }
}

// MARK: - ReportModelDelegate
extension ReportVotingListViewController: ReportModelDelegate {
    func apiResult(message: String?, statusCode: String?) {
        DispatchQueue.main.async {
            self.reportVotingList = self.modelAReport.votingInitialReports
            self.tableViewVoteList.reloadData()
            self.resetSortButtons(sender: UIButton())
            self.resetFilterButtons()
            self.reloadTableWithScrollToTop()
            self.refreshControl.endRefreshing()
        }
    }
    
    func handleCustomError(error: CustomError) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}
