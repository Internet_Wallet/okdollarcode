//
//  LoyaltyReportOptionsViewController.swift
//  OK
//
//  Created by iMac on 7/15/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class LoyaltyReportOptionsViewController: UIViewController {
    
    var optionsLabel = ["Earning Point Transfer", "Point Distribution", "Redeem Points"]
    var optionsImages = ["earnpoint", "point_distribution", "redeempoint_receipt"]
    @IBOutlet weak var loyatyOptionsTableView: UITableView! {
        didSet {
            self.loyatyOptionsTableView.tableFooterView = UIView()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isEarningPointShow {
            optionsLabel = ["Earning Point Transfer", "Point Distribution", "Redeem Points"]
            optionsImages = ["earnpoint", "point_distribution", "redeempoint_receipt"]
        }else {
            optionsLabel = ["Point Distribution", "Redeem Points"]
            optionsImages = ["point_distribution", "redeempoint_receipt"]
        }
        
        if UserModel.shared.agentType == .user {
            optionsLabel.remove(at: 1)
            optionsImages.remove(at: 1)
        }
        setUpNavigation()
        self.loyatyOptionsTableView.reloadData()
    }
    
    func showLoyatyReportScreen(index: Int) {
        guard let reportLoyaltytViewController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportLoyaltyViewController.nameAndID) as? ReportLoyaltyViewController else { return }
        reportLoyaltytViewController.loyaltyReportType = index
        self.navigationController?.pushViewController(reportLoyaltytViewController, animated: true)
    }

}


extension LoyaltyReportOptionsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "loyaltyOptionCell", for: indexPath) as? LoyaltyOptionReportCell
        cell?.wrapData(labelName: optionsLabel[indexPath.row], imageName: optionsImages[indexPath.row])
        return cell!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return optionsLabel.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            showLoyatyReportScreen(index: 0)
        case 2:
            showLoyatyReportScreen(index: 2)
        case 1:
            if UserModel.shared.agentType == .user {
                showLoyatyReportScreen(index: 2)
            } else {
                showLoyatyReportScreen(index: 1)
            }
        default:
            break
        }
    }
}


extension LoyaltyReportOptionsViewController: NavigationSetUpProtocol {
    /// It set up the navigation bar
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = .red
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        let label = MarqueeLabel()
        label.type = .continuous
        label.speed = .duration(4)
        label.text = ConstantsController.reportLoyaltyOptionsViewController.screenTitle.localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 23) {
            label.font = navFont
        }
        label.textAlignment = .center
        self.navigationItem.titleView = label
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
}


class LoyaltyOptionReportCell: UITableViewCell {
    @IBOutlet weak var loyatyLabel: UILabel!{
        didSet {
            loyatyLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var loyatyImage: UIImageView!
    
    func wrapData(labelName: String, imageName: String) {
        self.loyatyLabel.text = labelName.localized
        self.loyatyImage.image = UIImage(named: imageName)
    }
}
