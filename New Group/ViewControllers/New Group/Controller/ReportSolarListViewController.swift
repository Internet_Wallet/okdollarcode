//
//  ReportSolarListViewController.swift
//  OK
//
//  Created by E J ANTONY on 09/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportSolarListViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var tblSolarList: UITableView!
    @IBOutlet weak var tableViewFilter: UITableView!
    @IBOutlet weak var constraintScrollContentHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintFilterTableLeading: NSLayoutConstraint!
    @IBOutlet weak var constraintFilterTableHeight: NSLayoutConstraint!
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var constraintScrollTop: NSLayoutConstraint!
    
    //For Search
    @IBOutlet weak var buttonCancel: UIButton!{
        didSet {
            buttonCancel.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonClear: UIButton!{
        didSet {
            buttonClear.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    //For filter function
    @IBOutlet weak var buttonFilterAmount: ButtonWithImage!{
        didSet {
            buttonFilterAmount.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonFilterPeriod: ButtonWithImage!{
        didSet {
            buttonFilterPeriod.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    //For sort function
    @IBOutlet weak var btnSortName: ButtonWithImage!{
        didSet {
            btnSortName.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var btnSortTransID: ButtonWithImage!{
        didSet {
            btnSortTransID.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var btnSortAmount: ButtonWithImage!{
        didSet {
            btnSortAmount.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var btnSortServiceFee: ButtonWithImage!{
        didSet {
            btnSortServiceFee.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var btnSortAccNumber: ButtonWithImage!{
        didSet {
            btnSortAccNumber.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var btnSortStatus: ButtonWithImage!{
        didSet {
            btnSortStatus.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var btnSortDate: ButtonWithImage!{
        didSet {
            btnSortDate.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    //MARK: - Properties
    private let reportSolarCellID = "ReportSolarTableViewCell"
    lazy var modelAReport: ReportAModel = {
        return ReportAModel()
    }()
    var reportSolarList = [SolarReport]()
    var filterArray = [String]()
    let amountTypes = ["Amount", "All", "0 - 1,000", "1,001 - 10,000", "10,001 - 50,000", "50,001 - 1,00,000", "1,00,001 - 2,00,000", "2,00,001 - 5,00,000", "5,00,001 - Above"]
    let periodTypes = ["Period", "All", "Date", "Month"]
    var lastSelectedFilter = RootFilter.amount
    var fromDate: Date?
    var toDate: Date?
    var isSearchViewShow: Bool {
        get {
            return !viewSearch.isHidden
        }
        set(newValue) {
            if newValue {
                viewSearch.isHidden = false
                textFieldSearch.becomeFirstResponder()
                constraintScrollTop.constant = 0
                self.view.layoutIfNeeded()
            } else {
                textFieldSearch.text = ""
                viewSearch.isHidden = true
                constraintScrollTop.constant = -40
                self.view.layoutIfNeeded()
                self.view.endEditing(true)
            }
        }
    }
    
    enum RootFilter {
        case amount, period
    }
    
    private let refreshControl = UIRefreshControl()
    
    //MARK: - Views life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldSearch.placeholder = "Search By Transaction ID or Amount or Remarks".localized
        tblSolarList.tag = 0
        tableViewFilter.tag = 1
        tblSolarList.tableFooterView = UIView()
        tableViewFilter.tableFooterView = UIView()
        tblSolarList.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshTableData), for: .valueChanged)
        buttonCancel.layer.cornerRadius = 15
        buttonClear.layer.cornerRadius = 15
        buttonCancel.addTarget(self, action: #selector(hideSearchView), for: .touchUpInside)
        buttonClear.addTarget(self, action: #selector(clearSerchTextField), for: .touchUpInside)
        buttonCancel.setTitle("Cancel".localized, for: .normal)
        buttonClear.setTitle("Clear".localized, for: .normal)
        setLocalizedTitles()
        let cellFilterNibName = UINib(nibName: ConstantsXibCell.filterTableViewCell, bundle: nil)
        tableViewFilter.register(cellFilterNibName, forCellReuseIdentifier: ConstantsXibCell.filterTableViewCell)
        AppDelegate.AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)
        setUpNavigation()
        modelAReport.reportAModelDelegate  = self
        getToken()
        resetFilterButtons()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        let navBarHeight = navigationController?.navigationBar.frame.size.height ?? 44
        let heightToReduce = navBarHeight == 32 ? 56 : 44
        constraintScrollContentHeight.constant = scrollView.frame.size.height - CGFloat(heightToReduce) + 44
        self.view.layoutIfNeeded()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Target Methods
    @objc func showSearchView() {
        tableViewFilter.isHidden = true
        if !isSearchViewShow {
            if reportSolarList.count > 0 {
                isSearchViewShow = true
                textFieldSearch.becomeFirstResponder()
            } else {
                noRecordAlertAction()
            }
        } else {
            isSearchViewShow = false
            self.hideSearchView()
        }
    }
    
    @objc func showShareOption() {
        tableViewFilter.isHidden = true
        if reportSolarList.count < 1 {
            noRecordAlertAction()
        } else {
            guard let reportSharingViewController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportSharingViewController.nameAndID) as? ReportSharingViewController else { return }
            reportSharingViewController.delegate = self
            reportSharingViewController.pdfImage = UIImage(named: "sendMoneyPdf")
            reportSharingViewController.excelImage = UIImage(named: "sendMoneyExcel")
            reportSharingViewController.modalPresentationStyle = .overCurrentContext
            self.present(reportSharingViewController, animated: false, completion: nil)
        }
    }
    
    @objc func refreshTableData() {
        self.hideSearchView()
        getToken()
    }
    
    @objc func hideSearchView() {
        self.view.endEditing(true)
        textFieldSearch.text = ""
        viewSearch.isHidden = true
        constraintScrollTop.constant = -40
        self.view.layoutIfNeeded()
        self.view.endEditing(true)
        applyFilter()
    }
    
    @objc private func clearSerchTextField() {
        textFieldSearch.text = ""
        applyFilter()
    }
    
    //MARK: - Methods
    func showDateOptions(selectedOption: String) {
        switch selectedOption {
        case "Period".localized, "All".localized:
            self.fromDate = nil
            self.toDate = nil
            buttonFilterPeriod.setTitle(selectedOption, for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        case "Date".localized:
            presentDatePicker(mode: DateMode.fromToDate)
        case "Month".localized:
            presentDatePicker(mode: DateMode.month)
        default:
            break
        }
    }
    
    func presentDatePicker(mode: DateMode) {
        guard let reportDatePickerController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportDatePickerController.nameAndID) as? ReportDatePickerController else { return }
        reportDatePickerController.dateMode = mode
        reportDatePickerController.delegate = self
        reportDatePickerController.modalPresentationStyle = .overCurrentContext
        self.present(reportDatePickerController, animated: false, completion: nil)
    }
    
    func applyFilter(searchText: String = "") {
        var filteredArray = modelAReport.solarInitialReports
        filteredArray = filterProcess(forAmount: filteredArray)
        filteredArray = filterProcess(forPeriod: filteredArray)
        if isSearchViewShow && searchText.count > 0 {
            filteredArray = filteredArray.filter {
                if let cName = $0.customer?.customerName {
                    if "\(cName.lowercased())".range(of:searchText.lowercased()) != nil {
                        return true
                    }
                }
                if let cPayID = $0.customerOkPaymentID {
                    if cPayID.lowercased().range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                if let amount = $0.amount {
                    let paymentAmount = "\(amount)"
                    if paymentAmount.range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                if let accNumber = $0.solarHomeAccountNumber {
                    if accNumber.lowercased().range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                if let status = $0.topupStatus {
                    if status.lowercased().range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                return false
            }
        }
        reportSolarList = filteredArray
        reloadTableWithScrollToTop()
        resetSortButtons(sender: UIButton())
    }
    
    func filterProcess(forAmount arrayToFilter: [SolarReport]) -> [SolarReport] {
        func filterAmountRange(minVal: Double, maxVal: Double)-> [SolarReport] {
            return arrayToFilter.filter
                {
                    guard let amount = $0.amount else { return false }
                    return ((amount > minVal) && (amount < maxVal))
            }
        }
        if let amountFilter = buttonFilterAmount.currentTitle {
            switch amountFilter {
            case "Amount".localized, "All".localized:
                return arrayToFilter
            case "0 - 1,000".localized:
                return filterAmountRange(minVal: -0.1, maxVal: 1001)
            case "1,001 - 10,000".localized:
                return filterAmountRange(minVal: 1000.99, maxVal: 10001)
            case "10,001 - 50,000".localized:
                return filterAmountRange(minVal: 10000.99, maxVal: 50001)
            case "50,001 - 1,00,000".localized:
                return filterAmountRange(minVal: 50000.99, maxVal: 100001)
            case "1,00,001 - 2,00,000".localized:
                return filterAmountRange(minVal: 100000.99, maxVal: 200001)
            case "2,00,001 - 5,00,000".localized:
                return filterAmountRange(minVal: 200000.99, maxVal: 500001)
            case "5,00,001 - Above".localized:
                return arrayToFilter.filter {
                    guard let amount = $0.amount else { return false }
                    return amount > 500000.99
                }
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcess(forPeriod arrayToFilter: [SolarReport]) -> [SolarReport] {
        if let periodFilter = buttonFilterPeriod.currentTitle {
            switch periodFilter {
            case "Period".localized, "All".localized:
                return arrayToFilter
            default:
                break
            }
            if let exactDateStart = self.fromDate {
                return arrayToFilter.filter {
                    guard let cDate = $0.createdDate else { return false }
                    guard let createdDate = cDate.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") else { return false }
                    guard let dateStart = Calendar.current.date(byAdding: Calendar.Component.day, value: -1, to: exactDateStart) else { return false }
                    guard let exactDateEnd = self.toDate else { return false }
                    guard let dateEnd = Calendar.current.date(byAdding: Calendar.Component.day, value: 1, to: exactDateEnd) else { return false}
                    return (dateStart.compare(createdDate) == .orderedAscending && createdDate.compare(dateEnd) == .orderedAscending)
                }
            } else {
                return arrayToFilter.filter {
                    guard let cDate = $0.createdDate else { return false }
                    guard let createdDate = cDate.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") else { return false }
                    let crDateStr = createdDate.stringValue(dateFormatIs: "yyyy-MM")
                    guard let createdMonth = crDateStr.dateValue(dateFormatIs: "yyyy-MM") else { return false }
                    guard let monthToFilter = self.toDate else { return false }
                    return createdMonth.compare(monthToFilter) == .orderedSame
                }
            }
        }
        return []
    }
    
    func reloadTableWithScrollToTop() {
        self.tblSolarList.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            if self.reportSolarList.count > 0 {
                let indexPath = IndexPath(row: 0, section: 0)
                self.tblSolarList.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
    }
    
    //MARK: - Button Action Methods
    @IBAction func filterByAmount(_ sender: UIButton) {
        self.view.endEditing(true)
        func showListAtAmount() {
            filterArray = amountTypes
            tableViewFilter.reloadData()
            constraintFilterTableLeading.constant = 120
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
            tableViewFilter.isHidden = false
        }
        if tableViewFilter.isHidden {
            showListAtAmount()
        } else {
            switch lastSelectedFilter {
            case .amount:
                tableViewFilter.isHidden = true
            default:
                showListAtAmount()
            }
        }
        lastSelectedFilter = .amount
    }
    
    @IBAction func filterByPeriod(_ sender: UIButton) {
        self.view.endEditing(true)
        func showListAtPeriod() {
            filterArray = periodTypes
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 520
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
        }
        if tableViewFilter.isHidden {
            showListAtPeriod()
        } else {
            switch lastSelectedFilter {
            case .period :
                tableViewFilter.isHidden = true
            default:
                showListAtPeriod()
            }
        }
        lastSelectedFilter = .period
    }
}

//MARK: - Outlets
extension ReportSolarListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        switch tableView.tag {
        case 0:
            if reportSolarList.count == 0 {
                let noDataLabel = UILabel(frame: tableView.bounds)
                if let myFont = UIFont(name: appFont, size: 14) {
                    noDataLabel.font = myFont
                }
                noDataLabel.text = "No Records".localized
                noDataLabel.textAlignment = .center
                tableView.backgroundView = noDataLabel
            } else {
                tableView.backgroundView = nil
            }
            return 1
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView.tag {
        case 0:
            return reportSolarList.count
        case 1:
            return filterArray.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView.tag {
        case 0:
            guard let reportSolarCell = tableView.dequeueReusableCell(withIdentifier: reportSolarCellID, for: indexPath) as? ReportSolarTableViewCell else { return ReportSolarTableViewCell() }
            let reportData = reportSolarList[indexPath.row]
            reportSolarCell.configureCell(solarReportData: reportData)
            return reportSolarCell
        case 1:
            let filterCell = tableView.dequeueReusableCell(withIdentifier: ConstantsXibCell.filterTableViewCell, for: indexPath) as? FilterTableViewCell
            filterCell?.configureLabel(labelTitle: filterArray[indexPath.row])
            return filterCell!
        default:
            guard let reportSolarCell = tableView.dequeueReusableCell(withIdentifier: reportSolarCellID, for: indexPath) as? ReportSolarTableViewCell else { return ReportSolarTableViewCell() }
            return reportSolarCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView.tag {
        case 0:
            break
        case 1:
            switch lastSelectedFilter {
            case .amount:
                buttonFilterAmount.setTitle(filterArray[indexPath.row].localized, for: .normal)
                tableViewFilter.isHidden = true
                applyFilter(searchText: textFieldSearch.text ?? "")
            case .period:
                tableViewFilter.isHidden = true
                showDateOptions(selectedOption: filterArray[indexPath.row].localized)
            }
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch tableView.tag {
        case 1:
            return 44.0
        default:
            tableView.estimatedRowHeight = 160
            tableView.rowHeight = UITableView.automaticDimension
            return tableView.rowHeight
        }
    }
}

//MARK: - UIScrollViewDelegate
extension ReportSolarListViewController: UIScrollViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        targetContentOffset.pointee.y = max(targetContentOffset.pointee.y - 1, 1)
    }
}

//MARK: - ReportSharingOptionDelegte
extension ReportSolarListViewController: ReportSharingOptionDelegte {
    func share(by: ReportSharingViewController.ReportShareOption) {
        switch by {
        case .pdf:
            shareRecordsByPdf()
        case .excel:
            shareRecordsByExcel()
        }
    }
}

// MARK: - ReportDatePickerDelegate
extension ReportSolarListViewController: ReportDatePickerDelegate {
    func processWithDate(fromDate: Date?, toDate: Date?, dateMode: DateMode) {
        self.fromDate = fromDate
        self.toDate = toDate
        var selectedDate = ""
        switch dateMode {
        case .fromToDate:
            if let safeFDate = fromDate, let safeTDate = toDate {
                let fDateInString = safeFDate.stringValue(dateFormatIs: "dd MMM yyyy")
                let tDateInString = safeTDate.stringValue(dateFormatIs: "dd MMM yyyy")
                selectedDate = fDateInString + " to\n" + tDateInString
            }
        case .month:
            if let safeDate = toDate {
                let monthInString = safeDate.stringValue(dateFormatIs: "MMM yyyy")
                selectedDate = monthInString
            }
        default:
            break
        }
        buttonFilterPeriod.setTitle(selectedDate, for: .normal)
        applyFilter(searchText: textFieldSearch.text ?? "")
    }
}

// MARK: - ReportModelDelegate
extension ReportSolarListViewController: ReportModelDelegate {
    func apiResult(message: String?, statusCode: String?) {
        DispatchQueue.main.async {
            self.reportSolarList = self.modelAReport.solarInitialReports
            self.tblSolarList.reloadData()
            self.resetSortButtons(sender: UIButton())
            self.resetFilterButtons()
            self.reloadTableWithScrollToTop()
            self.refreshControl.endRefreshing()
        }
    }
    
    func handleCustomError(error: CustomError) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}
