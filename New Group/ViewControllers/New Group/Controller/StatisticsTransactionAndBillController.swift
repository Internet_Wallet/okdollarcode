//
//  StatisticsTransactionAndBillController.swift
//  OK
//  This show the statistics for transaction detail / bill payments
//  Created by ANTONY on 06/02/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Charts

enum StatisticsScreenType {
    case transactionDetail, billPayments, topUp, receivedMoney, statistics
}
class StatisticsTransactionAndBillController: UIViewController {
    // MARK: - Outlets
  
    @IBOutlet weak var tableViewFilter: UITableView!
    @IBOutlet weak var tableNavigationOptions: UITableView!
    @IBOutlet weak var constraintFilterTableLeading: NSLayoutConstraint!
    @IBOutlet weak var viewFilter: UIView!
    @IBOutlet weak var viewBarChart: BarChartView!
    @IBOutlet weak var viewPieChart: PieChartView!
    @IBOutlet weak var viewLineChart: LineChartView!
    @IBOutlet weak var constraintTableNavigationTrailing: NSLayoutConstraint!
    @IBOutlet weak var constraintTableNavigationHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintTableFilterHeight: NSLayoutConstraint!
    @IBOutlet weak var viewDatePicker: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var buttonDateSubmit: UIButton!{
        didSet {
            buttonDateSubmit.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateCancel: UIButton!{
        didSet {
            buttonDateCancel.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDateOne: UILabel!{
        didSet {
            labelDateOne.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDateTwo: UILabel!{
        didSet {
            labelDateTwo.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateOne: UIButton!{
        didSet {
            buttonDateOne.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateTwo: UIButton!{
        didSet {
            buttonDateTwo.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var viewShadow: UIView!
    
    //For filter functions
    @IBOutlet weak var labelFilterOne: UILabel!{
        didSet {
            labelFilterOne.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelFilterTwo: UILabel!{
        didSet {
            labelFilterTwo.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelFilterThree: UILabel!{
        didSet {
            labelFilterThree.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelFilterFour: UILabel!{
        didSet {
            labelFilterFour.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelFilterFive: UILabel!{
        didSet {
            labelFilterFive.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonFilterOne: ButtonWithImage!
    @IBOutlet weak var buttonFilterTwo: ButtonWithImage!
    @IBOutlet weak var buttonFilterThree: ButtonWithImage!
    @IBOutlet weak var buttonFilterFour: ButtonWithImage!
    @IBOutlet weak var buttonFilterFive: ButtonWithImage!
    
    @IBOutlet weak var constraintWidthFilterOne: NSLayoutConstraint!
    @IBOutlet weak var constraintWidthFilterTwo: NSLayoutConstraint!
    @IBOutlet weak var constraintWidthFilterThree: NSLayoutConstraint!
    @IBOutlet weak var constraintWidthFilterFour: NSLayoutConstraint!
    @IBOutlet weak var constraintWidthFilterFive: NSLayoutConstraint!
    
    @IBOutlet weak var imgViewGenderArrow: UIImageView!
    @IBOutlet weak var imgViewAgeArrow: UIImageView!
    // MARK: - Properties
    var screenType = StatisticsScreenType.transactionDetail
    var filterOneArrayValues = [String]()
    var filterTwoArrayValues = [String]()
    var filterThreeArrayValues = [String]()
    var filterFourArrayValues = [String]()
    var filterFiveArrayValues = [String]()
    var currentFilterValues = [String]()
    var initialRecords = [ReportTransactionRecord]()
    var transactionRecords = [ReportTransactionRecord]()
    private var currentFilterType = FilterType.buttonOne
    private enum FilterType {
        case buttonOne, buttonTwo, buttonThree, buttonFour, buttonFive
    }
    weak var axisFormatDelegate: IAxisValueFormatter?
    private var currentMobileNumber = "Mobile Number".localized
    private var optionsInNavigation = [String]()
    private var graphOptions = ["Bar Graph", "Pie Graph", "Line Graph"]
    private var mobileNumberOptions = ["Mobile Number", "All"]
    private var lineChartEntry = [ChartDataEntry]()
    private enum BarButtonTypes {
        case mobileNumber, moreOption
    }
    private var currentBarButton = BarButtonTypes.mobileNumber
    var isDateViewHidden: Bool {
        get {
            return viewDatePicker.isHidden
        }
        set(newValue) {
            if newValue {
                viewShadow.isHidden = true
                viewDatePicker.isHidden = true
            } else {
                viewShadow.isHidden = false
                viewDatePicker.isHidden = false
            }
        }
    }
    
     let dateFormatter = DateFormatter()
    // MARK: - View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
      dateFormatter.calendar = Calendar.current
         dateFormatter.dateFormat = "dd MMM yyyy"
        switch screenType {
        case .transactionDetail:
            filterOneArrayValues = ["Transaction", "All", "Pay To","Hide My Number", "Toll", "Ticket"]
            filterTwoArrayValues = ["Amount", "All", "0 - 1,000", "1,001 - 10,000", "10,001 - 50,000", "50,001 - 1,00,000",
                                    "1,00,001 - 2,00,000", "2,00,001 - 5,00,000", "5,00,001 - Above"]
            filterThreeArrayValues = ["Gender", "All", "Male", "Female"]
            filterFourArrayValues = ["Age", "All", "0 - 19", "20 - 34", "35 - 50", "51 - 64", "65 - Above"]
            filterFiveArrayValues = ["Period", "All", "Date", "Month"]
            viewFilter.backgroundColor = ConstantsColor.navigationHeaderTransaction
            setupFilterTablePositions()
        case .statistics:
            filterOneArrayValues = ["Transaction", "All", "Pay To","Hide My Number", "Toll", "Ticket"]
            filterTwoArrayValues = ["Amount", "All", "0 - 1,000", "1,001 - 10,000", "10,001 - 50,000", "50,001 - 1,00,000",
                                    "1,00,001 - 2,00,000", "2,00,001 - 5,00,000", "5,00,001 - Above"]
            filterThreeArrayValues = ["Gender", "All", "Male", "Female"]
            filterFourArrayValues = ["Age", "All", "0 - 19", "20 - 34", "35 - 50", "51 - 64", "65 - Above"]
            filterFiveArrayValues = ["Period", "All", "Date", "Month"]
            viewFilter.backgroundColor = ConstantsColor.navigationHeaderTransaction
            initialRecords = ReportCoreDataManager.sharedInstance.fetchTransactionRecords()
            AppDelegate.AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)
        case .receivedMoney:
            filterOneArrayValues = ["Transaction", "All", "Pay To","Hide My Number", "Toll", "Ticket"]
            filterTwoArrayValues = ["Amount", "All", "0 - 1,000", "1,001 - 10,000", "10,001 - 50,000", "50,001 - 1,00,000",
                                    "1,00,001 - 2,00,000", "2,00,001 - 5,00,000", "5,00,001 - Above"]
            filterThreeArrayValues = ["Gender", "All", "Male", "Female"]
            filterFourArrayValues = ["Age", "All", "0 - 19", "20 - 34", "35 - 50", "51 - 64", "65 - Above"]
            filterFiveArrayValues = ["Period", "All", "Date", "Month"]
            viewFilter.backgroundColor = ConstantsColor.navigationHeaderTransaction
            AppDelegate.AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)
        case .billPayments:
            filterOneArrayValues = ["Transaction", "All", "PostPaid Mobile", "Landlines", "Electricity", "Ycdc", "Gift Card", "DTH"]
            filterTwoArrayValues = ["Amount", "All", "0 - 1,000", "1,001 - 10,000", "10,001 - 50,000", "50,001 - 1,00,000",
                                    "1,00,001 - 2,00,000", "2,00,001 - 5,00,000", "5,00,001 - Above"]
            filterThreeArrayValues = ["Gender", "All", "Male", "Female"]
            filterFourArrayValues = ["Age", "All", "0 - 19", "20 - 34", "35 - 50", "51 - 64", "65 - Above"]
            filterFiveArrayValues = ["Period", "All", "Date", "Month"] 
            viewFilter.backgroundColor = ConstantsColor.navigationHeaderBillPayment
            setupFilterTablePositions()
        case .topUp:
            filterOneArrayValues = ["Transaction", "All", "Cash Back", "Bonus Points"]
            filterTwoArrayValues = ["Amount", "All", "0 - 1,000", "1,001 - 10,000", "10,001 - 50,000", "50,001 - 1,00,000",
                                    "1,00,001 - 2,00,000", "2,00,001 - 5,00,000", "5,00,001 - Above"]
            filterThreeArrayValues = ["Operator", "All", "Mpt", "Telenor", "Ooredoo", "Mectel", "Mytel","International Top-Up"]
            filterFourArrayValues = ["Type", "All", "Top-Up", "Special Offers", "Data Plan", "Air Time"]
            filterFiveArrayValues = ["Period", "All", "Date", "Month"]
            viewFilter.backgroundColor = ConstantsColor.navigationHeaderTopUpRecharge
            setupFilterTablePositions()
        }
        buttonDateSubmit.layer.cornerRadius = 15
        buttonDateCancel.layer.cornerRadius = 15
        buttonDateSubmit.addTarget(self, action: #selector(filterSelectedDate), for: .touchUpInside)
        buttonDateCancel.addTarget(self, action: #selector(hideDateSelectionView), for: .touchUpInside)
        initialRecords = initialRecords.sorted {
            guard let date1 = $0.transactionDate, let date2 = $1.transactionDate else { return false }
            return date1.compare(date2) == .orderedAscending
        }
        transactionRecords = initialRecords
        currentFilterValues = filterOneArrayValues
        tableNavigationOptions.tableFooterView = UIView()
        tableViewFilter.tableFooterView = UIView()
        tableNavigationOptions.tag = 1
        tableViewFilter.tag = 0
        getRecentMobileNumbers()
        setFilterTitles()
        let cellFilterNibName = UINib(nibName: ConstantsXibCell.filterTableViewCell, bundle: nil)
        tableViewFilter.register(cellFilterNibName, forCellReuseIdentifier: ConstantsXibCell.filterTableViewCell)
        let cellMoreOptionNavigNibName = UINib(nibName: ConstantsXibCell.filterTableViewCell, bundle: nil)
        tableNavigationOptions.register(cellMoreOptionNavigNibName, forCellReuseIdentifier: ConstantsXibCell.filterTableViewCell)
        viewLineChart.isHidden = true
        viewPieChart.isHidden = true
        barChartUpdate()
        datePicker.calendar = Calendar(identifier: .gregorian)
        if #available(iOS 13.4, *) {
                       datePicker.preferredDatePickerStyle = .wheels
                   } else {
                       // Fallback on earlier versions
                   }
        labelDateOne.text = "From Date".localized
        labelDateTwo.text = "To Date".localized
        buttonDateSubmit.setTitle("Submit".localized, for: .normal)
        buttonDateCancel.setTitle("Cancel".localized, for: .normal)
        viewLineChart.leftAxis.axisMinimum = 0
        viewBarChart.leftAxis.axisMinimum = 0
        viewLineChart.rightAxis.axisMinimum = 0
        viewBarChart.rightAxis.axisMinimum = 0
        
        //Tap Guesture
        let gesterLine = UITapGestureRecognizer(target: self, action: #selector(hideFilterLineChartView(_:)))
        viewLineChart?.addGestureRecognizer(gesterLine)
        let gesterBar = UITapGestureRecognizer(target: self, action: #selector(hideFilterLineChartView(_:)))
        viewBarChart?.addGestureRecognizer(gesterBar)
        let gesterPie = UITapGestureRecognizer(target: self, action: #selector(hideFilterLineChartView(_:)))
        viewPieChart?.addGestureRecognizer(gesterPie)
    }
    
    @objc fileprivate func hideFilterLineChartView(_ sender: UITapGestureRecognizer) {
        tableViewFilter.isHidden = true
        tableNavigationOptions.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        setUpNavigation()
        switch screenType {
        case .statistics, .receivedMoney:
            setupFilterTablePositions()
        default:
            break
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AppDelegate.AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)
    }
    
    // MARK: - Methods
    func getRecentMobileNumbers() {
        var index = 0
        for record in transactionRecords {
            if let mobileNumber = record.destination, let transType = record.transType {
                if transType != "PAYWITHOUT ID" {
                    var check = mobileNumber
                    if mobileNumber.hasPrefix("00") {
                        let mobileNum = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: mobileNumber)
                        check = "(\(mobileNum.counrtycode))\(mobileNum.mobileNo)"
                    } else {
                        check = "(\(record.countryCode ?? ""))\(mobileNumber)"
                    }
                    if !mobileNumberOptions.contains(check) {
                        index = index + 1
                        mobileNumberOptions.append(check)
                    }
                    if index == 10 {
                        break
                    }
                }
            }
        }
    }
    private func setupFilterTablePositions() {
        let filterLabelWidth = ((UIScreen.main.bounds.size.width - 30) / 5) - 30
        constraintWidthFilterOne.constant = filterLabelWidth
        constraintWidthFilterTwo.constant = filterLabelWidth
        constraintWidthFilterThree.constant = filterLabelWidth
        constraintWidthFilterFour.constant = filterLabelWidth
        constraintWidthFilterFive.constant = filterLabelWidth
        switch screenType {
         case .transactionDetail, .statistics, .receivedMoney:
            constraintWidthFilterThree.constant = 85
            constraintWidthFilterFour.constant =  85
            buttonFilterThree.isHidden  = false
            buttonFilterFour.isHidden = false
            imgViewGenderArrow.isHidden  = false
            imgViewAgeArrow.isHidden = false
        case .billPayments:
            constraintWidthFilterThree.constant = 0
            constraintWidthFilterFour.constant =  0
            buttonFilterThree.isHidden  = true
            buttonFilterFour.isHidden = true
            imgViewGenderArrow.isHidden  = true
            imgViewAgeArrow.isHidden = true
        case .topUp:
            constraintWidthFilterThree.constant = 85
            constraintWidthFilterFour.constant =  85
            buttonFilterThree.isHidden  = false
            buttonFilterFour.isHidden = false
            imgViewGenderArrow.isHidden  = false
            imgViewAgeArrow.isHidden = false
        }
        self.view.layoutIfNeeded()
    }
    // MARK: - Populating Charts
    private func barChartUpdate() {
        viewBarChart.zoom(scaleX: 0.0, scaleY: 0.0, x: 0, y: 0)
        switch screenType {
        case .transactionDetail, .statistics, .receivedMoney:
            populateTransactionBarData()
        case .billPayments:
            populateBillPaymentsBarData()
        case .topUp:
            populateTopUpRechargeBarData()
        }
    }
    
    private func pieChartUpdate() {
        switch screenType {
        case .transactionDetail, .statistics, .receivedMoney:
            populateTransactionPieData()
        case .billPayments:
            populateBillPaymentsPieData()
        case .topUp:
            populateTopUpPieData()
        }
    }
    
    private func lineChartUpdate() {
        switch screenType {
        case .transactionDetail, .statistics, .receivedMoney:
            populateTransactionLineData()
        case .billPayments:
            populateBillPaymentsLineData()
        case .topUp:
            populateTopUpLineData()
        }
    }
    
    private func populateTransactionBarData() {
        if transactionRecords.count < 1 {
            viewBarChart.noDataText = "No chart data available".localized
            viewBarChart.noDataFont = UIFont(name: appFont, size: appFontSizeReport)
            viewBarChart.data = nil
            viewBarChart.notifyDataSetChanged()
            return
        }
        var startDate = Date()
        var endDate = Date()
        var xDates = [Date: NSDecimalNumber]()
        if let dateValue = transactionRecords[0].transactionDate, let dateWithStartDay =  dateValue.stringValue(dateFormatIs: "dd-MM-yyyy").dateValue(dateFormatIs: "dd-MM-yyyy") {
            startDate = dateWithStartDay
        }
        if let dateWithEndDay = endDate.stringValue(dateFormatIs: "dd-MM-yyyy").dateValue(dateFormatIs: "dd-MM-yyyy") {
            endDate = dateWithEndDay
        }
        if let labelFilterFive = labelFilterFive.text {
            switch labelFilterFive {
            case "Date".localized:
                if let dateStartStr = buttonDateOne.currentTitle, let selectedDate = dateStartStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                    startDate = selectedDate
                }
                if let dateEndStr = buttonDateTwo.currentTitle, let selectedDate = dateEndStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                    endDate = selectedDate
                }
            case "Month".localized:
                if let dateEndStr = buttonDateTwo.currentTitle, let selectedMonth = dateEndStr.dateValue(dateFormatIs: "MMM yyyy") {
                    startDate = selectedMonth.startOfMonth()
                    endDate = selectedMonth.endOfMonth()
                }
            default:
                break
            }
        }
        func setColor(value: Double) -> UIColor {
            if value < 500 {
                return UIColor.red
            }
            else if value > 499 && value < 1000 {
                return UIColor.green
            } else if value > 999 {
                return UIColor.blue
            }
            return UIColor.brown
        }
        var yValues = [BarChartDataEntry]()
        //var colors = [UIColor]()
        var xLabels = [String]()
        var index: Double = 1
        var maxValue: Double = 0
        var startingDate = startDate
        let datFormat = DateFormatter()
        datFormat.calendar = Calendar(identifier: .gregorian)
        datFormat.dateFormat = "dd-MMM-yyyy"
        var totalAmount: NSDecimalNumber = 0

        while startingDate <= endDate {
            var total: NSDecimalNumber = 0
            for record in transactionRecords {
                if let transDate = record.transactionDate {
                    if Calendar.current.isDate(startingDate, inSameDayAs:transDate) {
                        if let amount = record.amount {
                            total = total.adding(amount)
                            totalAmount = totalAmount.adding(amount)
                            xDates[startingDate] = total
                        }
                    }
                }
            }
            maxValue = maxValue > total.doubleValue ? maxValue : total.doubleValue
            xLabels.append(datFormat.string(from: startingDate))
            //xLabels.append(startingDate.stringValue())
            //let newColor = setColor(value: total.doubleValue)
            //if !colors.contains(newColor) {
                //colors.append(newColor)
            //}
            if total == 0 {
                
            } else {
                yValues.append(BarChartDataEntry(x: index - 1, y: total.doubleValue))
            }
            index += 1
            println_debug(startingDate)
            if let safeDate = Calendar.current.date(byAdding: .day, value: 1, to: startingDate) {
                startingDate = safeDate
            } else {
                break
            }
        }
        
        let font = UIFont(name: appFont, size: 9)
        var transAmtStr = "Total Amount"
        if appDel.currentLanguage == "uni" {
            transAmtStr = "စုစုပေါင်းငွေပမာဏ(ကျပ်)"
        } else if appDel.currentLanguage == "my" {
            transAmtStr = "စုစုပေါင်းငွေပမာဏ(ကျပ်)"
        }
        
         transAmtStr = transAmtStr + " " + "\(totalAmount.doubleValue)" + " " + "MMK"
        
        let dataSet = BarChartDataSet(values: yValues, label: transAmtStr)
        
        dataSet.valueFont = font ?? UIFont.systemFont(ofSize: 14)
        viewBarChart.zoom(scaleX: 3.0, scaleY: 1.0, x: 0, y: 0)
       
        //dataSet.colors = colors
        let data = BarChartData(dataSets: [dataSet])
        viewBarChart.data = data
        viewBarChart.animate(xAxisDuration: 1.0, yAxisDuration: 1.0)
        viewBarChart.chartDescription?.text = ""
        viewBarChart.drawGridBackgroundEnabled = true
        viewBarChart.gridBackgroundColor = UIColor.gray.withAlphaComponent(0.5)
        viewBarChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: xLabels)
        viewBarChart.xAxis.granularityEnabled = true
        viewBarChart.data?.setValueFont(font ?? UIFont.systemFont(ofSize: 14))
        //This must stay at end of function
        viewBarChart.notifyDataSetChanged()
    }
    
    private func pieLabelTotalTrans() -> String {
        switch screenType {
        case .receivedMoney:
            if appDel.currentLanguage == "uni" {
                return "လက်ခံရရှိထားသော အရေအတွက်"
            } else if appDel.currentLanguage == "my" {
                return "လက်ခံရရှိထားသော အရေအတွက်"
            }
            return "No of Received Payments"
        default:
            if appDel.currentLanguage == "uni" {
                return "လုပ်ဆောင်ချက် အရေအတွက်"//"လုပ္ေဆာင္ခ်က္ အေရအတြက္"
            } else if appDel.currentLanguage == "my" {
                return "လုပ်ဆောင်ချက် အရေအတွက်"
            }
            return "Number of Transaction"
        }
    }
    
    private func populateTransactionPieData() {
        if transactionRecords.count < 1 {
            viewPieChart.noDataText = "No chart data available".localized
            viewPieChart.noDataFont = UIFont(name: appFont, size: appFontSizeReport)
            viewPieChart.data = nil
            viewPieChart.notifyDataSetChanged()
            return
        }
        viewPieChart.isHidden = false
        var startDate = Date()
        var endDate = Date()
        if let dateValue = transactionRecords[0].transactionDate, let dateWithStartDay =  dateValue.stringValue(dateFormatIs: "dd-MM-yyyy").dateValue(dateFormatIs: "dd-MM-yyyy") {
            startDate = dateWithStartDay
        }
        if let dateWithEndDay = endDate.stringValue(dateFormatIs: "dd-MM-yyyy").dateValue(dateFormatIs: "dd-MM-yyyy") {
            endDate = dateWithEndDay
        }
        if let labelFilterFive = labelFilterFive.text {
            switch labelFilterFive {
            case "Date".localized:
                if let dateStartStr = buttonDateOne.currentTitle, let selectedDate = dateStartStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                    startDate = selectedDate
                }
                if let dateEndStr = buttonDateTwo.currentTitle, let selectedDate = dateEndStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                    endDate = selectedDate
                }
            case "Month".localized:
                if let dateEndStr = buttonDateTwo.currentTitle, let selectedMonth = dateEndStr.dateValue(dateFormatIs: "MMM yyyy") {
                    startDate = selectedMonth.startOfMonth()
                    endDate = selectedMonth.endOfMonth()
                }
            default:
                break
            }
        }
        var payToAmount: NSDecimalNumber = 0
        var hideMyNumberAmount: NSDecimalNumber = 0
        var ticketAmount: NSDecimalNumber = 0
        var tollAmount: NSDecimalNumber = 0
        var startingDate = startDate
        
        var ooredoAmount: NSDecimalNumber = 0
        var telenorAmount: NSDecimalNumber = 0
        var mptAmount: NSDecimalNumber = 0
        var mectelAmount: NSDecimalNumber = 0
        var mytelAmount: NSDecimalNumber = 0
        var overSeasAmount: NSDecimalNumber = 0
        var otherAmount: NSDecimalNumber = 0
        
        let pieChartLegend = viewPieChart.legend
        pieChartLegend.horizontalAlignment = Legend.HorizontalAlignment.right
        pieChartLegend.verticalAlignment = Legend.VerticalAlignment.top
        pieChartLegend.orientation = Legend.Orientation.vertical
        pieChartLegend.drawInside = false
        pieChartLegend.yOffset = 10.0
        viewPieChart.legend.enabled = true
        var values: [ChartDataEntry] = []
        var customColors: [UIColor] = []
        switch screenType {
        case .topUp:
            while startingDate <= endDate {
                for record in transactionRecords {
                    if let transDate = record.transactionDate {
                        if Calendar.current.isDate(startingDate, inSameDayAs:transDate) {
                            if let comment = record.rawDesc?.lowercased(), let transAmount = record.amount {
                                if comment.contains(find: "mpt") {
                                     mptAmount = mptAmount.adding(transAmount)
                                } else if comment.contains(find: "telenor") {
                                     telenorAmount = telenorAmount.adding(transAmount)
                                } else if comment.contains(find: "ooredoo") {
                                    ooredoAmount = ooredoAmount.adding(transAmount)
                                } else if comment.contains(find: "mectel") {
                                     mectelAmount = mectelAmount.adding(transAmount)
                                } else if comment.contains(find: "mytel") {
                                     mytelAmount = mytelAmount.adding(transAmount)
                                } else if comment.contains(find: "overseas") {
                                     overSeasAmount = overSeasAmount.adding(transAmount)
                                } else {
                                    otherAmount = otherAmount.adding(transAmount)
                                }
                            }
                        }
                    }
                }
                if let safeDate = Calendar.current.date(byAdding: .day, value: 1, to: startingDate) {
                    startingDate = safeDate
                } else {
                    break
                }
            }
            
            if mptAmount.doubleValue != Double(0) {
                values.append(PieChartDataEntry(value: mptAmount.doubleValue, label: "MPT"))
                customColors.append(MyNumberTopup.OperatorColorCode.mpt)
            }
            if telenorAmount.doubleValue != Double(0) {
                values.append(PieChartDataEntry(value: telenorAmount.doubleValue, label: "Telenor"))
                customColors.append(MyNumberTopup.OperatorColorCode.telenor)
            }
            if ooredoAmount.doubleValue != Double(0) {
                values.append(PieChartDataEntry(value: ooredoAmount.doubleValue, label: "Ooredoo"))
                customColors.append(MyNumberTopup.OperatorColorCode.oredo)
            }
            if mectelAmount.doubleValue != Double(0) {
                values.append(PieChartDataEntry(value: mectelAmount.doubleValue, label: "Mectel"))
                customColors.append(MyNumberTopup.OperatorColorCode.mactel)
            }
            if mytelAmount.doubleValue != Double(0) {
                values.append(PieChartDataEntry(value: mytelAmount.doubleValue, label: "MyTel"))
                customColors.append(MyNumberTopup.OperatorColorCode.mytel)
            }
            if overSeasAmount.doubleValue != Double(0) {
                values.append(PieChartDataEntry(value: overSeasAmount.doubleValue, label: "International Top-Up"))
                customColors.append(MyNumberTopup.OperatorColorCode.okDefault)
            }
            if otherAmount.doubleValue != Double(0) {
                values.append(PieChartDataEntry(value: otherAmount.doubleValue, label: "Other TopUp"))
                customColors.append(MyNumberTopup.OperatorColorCode.okDefault)
            }
        default:
            while startingDate <= endDate {
                for record in transactionRecords {
                    if let transDate = record.transactionDate {
                        if Calendar.current.isDate(startingDate, inSameDayAs:transDate) {
                            if let transType = record.transType, let transAmount = record.amount {
                                switch transType {
                                case "PAYTO":
                                    payToAmount = payToAmount.adding(transAmount)
                                case "PAYWITHOUT ID":
                                    hideMyNumberAmount = hideMyNumberAmount.adding(transAmount)
                                case "TICKET":
                                    ticketAmount = ticketAmount.adding(transAmount)
                                case "TOLL":
                                    tollAmount = tollAmount.adding(transAmount)
                                default:
                                    break
                                }
                            }
                        }
                    }
                }
                if let safeDate = Calendar.current.date(byAdding: .day, value: 1, to: startingDate) {
                    startingDate = safeDate
                } else {
                    break
                }
            }
            if payToAmount.doubleValue != Double(0) {
                values.append(PieChartDataEntry(value: payToAmount.doubleValue, label: "Pay To".localized))
                customColors.append(UIColor.red)
            }
            if hideMyNumberAmount.doubleValue != Double(0) {
                values.append(PieChartDataEntry(value: hideMyNumberAmount.doubleValue, label: "Hide My Number".localized))
                customColors.append(UIColor.green)
            }
            if ticketAmount.doubleValue != Double(0) {
                values.append(PieChartDataEntry(value: ticketAmount.doubleValue, label: "Ticket"))
                customColors.append(UIColor.blue)
            }
            if tollAmount.doubleValue != Double(0) {
                values.append(PieChartDataEntry(value: tollAmount.doubleValue, label: "Toll"))
                customColors.append(UIColor.cyan)
            }
        }
        
        let dataSetValues = PieChartDataSet(values: values, label: pieLabelTotalTrans() + " (" + "\(transactionRecords.count)" + ")")
        dataSetValues.colors = customColors
        let data = PieChartData(dataSet: dataSetValues)
        viewPieChart.data = data
        var totalAmount: NSDecimalNumber = 0
        switch screenType {
        case .topUp:
            totalAmount = mptAmount.adding(telenorAmount).adding(ooredoAmount).adding(mectelAmount).adding(mytelAmount).adding(overSeasAmount).adding(otherAmount)
        default:
            totalAmount = payToAmount.adding(hideMyNumberAmount).adding(ticketAmount).adding(tollAmount)
        }
        
        viewPieChart.centerText = "Pie Graph"
        var totAmt = "Total Amount"
        if appDel.currentLanguage == "uni" {
            totAmt = "စုစုပေါင်းငွေပမာဏ(ကျပ်)"
        } else if appDel.currentLanguage == "my" {
            totAmt = "စုစုပေါင်းငွေပမာဏ(ကျပ်)"
        }
        viewPieChart.chartDescription?.text = totAmt + " " + "\(totalAmount.doubleValue)" + " " + "MMK"
        viewPieChart.chartDescription?.textColor = UIColor.black
        let font = UIFont(name: appFont, size: 9)
        viewPieChart.chartDescription?.font = font ?? UIFont.systemFont(ofSize: 9)
        viewPieChart.data?.setValueFont(font ?? UIFont.systemFont(ofSize: 9))
        //All other additions to this function will go here
        
        //This must stay at end of function
        viewPieChart.notifyDataSetChanged()
        dataSetValues.valueColors = [UIColor.white]
    }
    
    private func populateTransactionLineData() {
        if transactionRecords.count < 1 {
            viewLineChart.noDataText = "No chart data available".localized
            viewLineChart.noDataFont = UIFont(name: appFont, size: appFontSizeReport)
            viewLineChart.data = nil
            viewLineChart.notifyDataSetChanged()
            return
        }
        viewLineChart.isHidden = false
        lineChartEntry = []
        var startDate = Date()
        var endDate = Date()
        if let dateValue = transactionRecords[0].transactionDate, let dateWithStartDay =  dateValue.stringValue(dateFormatIs: "dd-MM-yyyy").dateValue(dateFormatIs: "dd-MM-yyyy") {
            startDate = dateWithStartDay
        }
        if let dateWithEndDay = endDate.stringValue(dateFormatIs: "dd-MM-yyyy").dateValue(dateFormatIs: "dd-MM-yyyy") {
            endDate = dateWithEndDay
        }
        if let labelFilterFive = labelFilterFive.text {
            switch labelFilterFive {
            case "Date".localized:
                if let dateStartStr = buttonDateOne.currentTitle, let selectedDate = dateStartStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                    startDate = selectedDate
                }
                if let dateEndStr = buttonDateTwo.currentTitle, let selectedDate = dateEndStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                    endDate = selectedDate
                }
            case "Month".localized:
                if let dateEndStr = buttonDateTwo.currentTitle, let selectedMonth = dateEndStr.dateValue(dateFormatIs: "MMM yyyy") {
                    startDate = selectedMonth.startOfMonth()
                    endDate = selectedMonth.endOfMonth()
                }
            default:
                break
            }
        }
        var xLabels = [String]()
        var startingDate = startDate
        let datFormat = DateFormatter()
        datFormat.calendar = Calendar(identifier: .gregorian)
        datFormat.dateFormat = "dd-MMM-yyyy"
        var index: Double = 1
        var totalAmount: NSDecimalNumber = 0

        while startingDate <= endDate {
            var total: NSDecimalNumber = 0

            for record in transactionRecords {
                if let transDate = record.transactionDate {
                    if Calendar.current.isDate(startingDate, inSameDayAs:transDate) {
                        if let amount = record.amount {
                            total = total.adding(amount)
                            totalAmount =  totalAmount.adding(amount)
                        }
                    }
                }
            }
            xLabels.append(datFormat.string(from: startingDate))
            //xLabels.append(startingDate.stringValue())
            //let newColor = setColor(value: total.doubleValue)
            //if !colors.contains(newColor) {
            //colors.append(newColor)
            //}
            let value = ChartDataEntry(x: Double(index - 1), y: Double(total.doubleValue))
            lineChartEntry.append(value)
            index += 1
            if let safeDate = Calendar.current.date(byAdding: .day, value: 1, to: startingDate) {
                startingDate = safeDate
            } else {
                break
            }
        }
        
        
        var transAmtStr = "Total Amount".localized
        let font = UIFont(name: appFont, size: 9)
        if appDel.currentLanguage == "uni" {
            transAmtStr = "စုစုပေါင်းငွေပမာဏ(ကျပ်)"
        } else if appDel.currentLanguage == "my" {
          transAmtStr = "စုစုပေါင်းငွေပမာဏ(ကျပ်)"
        }
        
        transAmtStr = transAmtStr + " " + "\(totalAmount.doubleValue)" + " " + "MMK".localized
        
        let lineDataSet = LineChartDataSet(values: lineChartEntry, label: transAmtStr)
        lineDataSet.circleRadius = 4
        lineDataSet.valueFont = font ?? UIFont.systemFont(ofSize: 9)
        lineDataSet.circleHoleRadius = 2
        lineDataSet.lineWidth = 2
        //line1.colors = [NSUIColor.blue]
        
        let data = LineChartData()
        data.addDataSet(lineDataSet)
        viewLineChart.data = data
        viewLineChart.data?.setValueFont(font ?? UIFont.systemFont(ofSize: 9))
        viewLineChart.animate(xAxisDuration: 1.0, yAxisDuration: 1.0)
        viewLineChart.chartDescription?.text = ""
        viewLineChart.drawGridBackgroundEnabled = true
        viewLineChart.gridBackgroundColor = UIColor.gray.withAlphaComponent(0.5)
        viewLineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: xLabels)
        viewLineChart.xAxis.granularityEnabled = true
        viewLineChart.notifyDataSetChanged()
    }
    
    private func populateBillPaymentsBarData() {
        populateTransactionBarData()
    }
    
    private func populateBillPaymentsPieData() {
        populateTransactionPieData()
    }
    
    private func populateBillPaymentsLineData() {
        populateTransactionLineData()
    }
    
    private func populateTopUpRechargeBarData() {
        populateTransactionBarData()
    }
    
    private func populateTopUpPieData() {
        populateTransactionPieData()
    }
    
    private func populateTopUpLineData() {
        populateTransactionLineData()
    }
    
    private func setFilterTitles() {
        if filterOneArrayValues.count > 0 {
            labelFilterOne.text = filterOneArrayValues[0].localized
        } else {
            labelFilterOne.text = ""
            buttonFilterOne.isHidden = true
        }
        if filterTwoArrayValues.count > 0 {
            labelFilterTwo.text = filterTwoArrayValues[0].localized
        } else {
            labelFilterTwo.text = ""
            buttonFilterTwo.isHidden = true
        }
        if filterThreeArrayValues.count > 0 {
            labelFilterThree.text = filterThreeArrayValues[0].localized
        } else {
            labelFilterThree.text = ""
            buttonFilterThree.isHidden = true
        }
        if filterFourArrayValues.count > 0 {
            labelFilterFour.text = filterFourArrayValues[0].localized
        } else {
            labelFilterFour.text = ""
            buttonFilterFour.isHidden = true
        }
        if filterFiveArrayValues.count > 0 {
            labelFilterFive.text = filterFiveArrayValues[0].localized
        } else {
            labelFilterFive.text = ""
            buttonFilterFive.isHidden = true
        }
    }
    
    // MARK: - Filter Actions
    func applyFilter() {
        var filteredArray = initialRecords
        switch screenType {
        case .transactionDetail, .statistics, .receivedMoney:
            filteredArray = filterProcessTransaction(forButtonOne: filteredArray)
            filteredArray = filterProcessTransaction(forButtonTwo: filteredArray)
            filteredArray = filterProcessTransaction(forButtonThree: filteredArray)
            filteredArray = filterProcessTransaction(forButtonFour: filteredArray)
            filteredArray = filterProcessTransaction(forButtonFive: filteredArray)
            
        case .billPayments:
            filteredArray = filterProcessBillPayment(forButtonOne: filteredArray)
            filteredArray = filterProcessBillPayment(forButtonTwo: filteredArray)
            filteredArray = filterProcessBillPayment(forButtonThree: filteredArray)
            filteredArray = filterProcessBillPayment(forButtonFour: filteredArray)
            filteredArray = filterProcessBillPayment(forButtonFive: filteredArray)
        case .topUp:
            filteredArray = filterProcessTopUp(forButtonOne: filteredArray)
            filteredArray = filterProcessTopUp(forButtonTwo: filteredArray)
            filteredArray = filterProcessTopUp(forButtonThree: filteredArray)
            filteredArray = filterProcessTopUp(forButtonFour: filteredArray)
            filteredArray = filterProcessTopUp(forButtonFive: filteredArray)
        }
        filteredArray = filterProcess(forMobileNumber: filteredArray)
        transactionRecords = filteredArray
        
        if !viewBarChart.isHidden {
            barChartUpdate()
        } else if !viewPieChart.isHidden {
            pieChartUpdate()
        } else if !viewLineChart.isHidden {
            lineChartUpdate()
        }
    }
    
    func filterProcessTransaction(forButtonOne arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        func filterTransType(keyString: String) -> [ReportTransactionRecord] {
            return arrayToFilter.filter {
                guard let transType = $0.transType else { return false }
                return transType == keyString
            }
        }
        if let labelFilterOne = labelFilterOne.text {
            switch labelFilterOne {
            case "Transaction".localized, "All".localized:
                return arrayToFilter
            case "Pay To".localized:
                return filterTransType(keyString: "PAYTO")
            case "Hide My Number".localized:
                return filterTransType(keyString: "PAYWITHOUT ID")
            case "Toll".localized:
                return filterTransType(keyString: "TOLL")
            case "Ticket".localized:
                return filterTransType(keyString: "TICKET")
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcessTransaction(forButtonTwo arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        func filterFromDecimalNumbers(greaterThan: NSDecimalNumber, lesserThan: NSDecimalNumber)-> [ReportTransactionRecord] {
            return arrayToFilter.filter
                {
                    guard let amount = $0.amount else { return false }
                    return (amount.compare(greaterThan) == .orderedDescending && amount.compare(lesserThan) == .orderedAscending)
            }
        }
        if let labelFilterTwo = labelFilterTwo.text {
            switch labelFilterTwo {
            case "Amount".localized, "All".localized:
                return arrayToFilter
            case "0 - 1,000".localized:
                return filterFromDecimalNumbers(greaterThan: -0.01, lesserThan: 1001)
            case "1,001 - 10,000".localized:
                return filterFromDecimalNumbers(greaterThan: 1000.99, lesserThan: 10001)
            case "10,001 - 50,000".localized:
                return filterFromDecimalNumbers(greaterThan: 10000.99, lesserThan: 50001)
            case "50,001 - 1,00,000".localized:
                return filterFromDecimalNumbers(greaterThan: 50000.99, lesserThan: 100001)
            case "1,00,001 - 2,00,000".localized:
                return filterFromDecimalNumbers(greaterThan: 100000.99, lesserThan: 200001)
            case "2,00,001 - 5,00,000".localized:
                return filterFromDecimalNumbers(greaterThan: 200000.99, lesserThan: 500001)
            case "5,00,001 - Above".localized:
                return arrayToFilter.filter {
                    guard let amount = $0.amount else { return false }
                    return (amount.compare(500000.99) == .orderedDescending)
                }
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcessTransaction(forButtonThree arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        func filterString(filterString: String) -> [ReportTransactionRecord] {
            return arrayToFilter.filter {
                guard let gender = $0.gender else { return false }
                return gender == filterString
            }
        }
        switch screenType {
        case .billPayments:
            return arrayToFilter
        default:
            if let labelFilterThree = labelFilterThree.text {
                switch labelFilterThree {
                case "Gender".localized, "All".localized:
                    return arrayToFilter
                case "Male".localized:
                    return filterString(filterString: "1")//filterString(filterString: "M")
                case "Female".localized:
                    return filterString(filterString: "0")//filterString(filterString: "F")
                default:
                    return []
                }
            }
        }
        return []
    }
    
    func filterProcessTransaction(forButtonFour arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        func filterIntValue(greaterThan: Int16, lesserThan: Int16) -> [ReportTransactionRecord] {
            return arrayToFilter.filter {
               return  ($0.age > greaterThan && $0.age < lesserThan)
            }
        }
        switch screenType {
        case .billPayments:
            return arrayToFilter
        default:
            if let labelFilterFour = labelFilterFour.text {
                switch labelFilterFour {
                case "Age".localized, "All".localized:
                    return arrayToFilter
                case "0 - 19".localized:
                    return filterIntValue(greaterThan: -1, lesserThan: 20)
                case "20 - 34".localized:
                    return filterIntValue(greaterThan: 19, lesserThan: 35)
                case "35 - 50".localized:
                    return filterIntValue(greaterThan: 34, lesserThan: 51)
                case "51 - 64".localized:
                    return filterIntValue(greaterThan: 50, lesserThan: 65)
                case "65 - Above".localized:
                    return arrayToFilter.filter { $0.age > 64 }
                default:
                    return []
                }
            }
        }
        
        return []
    }
    
    func filterProcessTransaction(forButtonFive arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        if let labelFilterFive = labelFilterFive.text {
            switch labelFilterFive {
            case "Period".localized, "All".localized:
                return arrayToFilter
            case "Date".localized:
                return arrayToFilter.filter {
                    guard let transDate = $0.transactionDate else { return false }
                    let dFormatter = DateFormatter()
                    dFormatter.calendar = Calendar(identifier: .gregorian)
                    dFormatter.dateFormat = "dd MMM yyyy"
                    let transDateStr = dFormatter.string(from: transDate)
                    guard let transactionDate = dFormatter.date(from: transDateStr) else { return false }
                    guard let dateStartStr = buttonDateOne.currentTitle else { return false }
                    guard let dateEndStr = buttonDateTwo.currentTitle else { return false }
                    guard let exactDateStart = dFormatter.date(from: dateStartStr) else { return false }
                    guard let dateStart = Calendar.current.date(byAdding: Calendar.Component.day, value: -1, to: exactDateStart) else { return false }
                    guard let exactDateEnd = dFormatter.date(from: dateEndStr) else { return false }
                    guard let dateEnd = Calendar.current.date(byAdding: Calendar.Component.day, value: 1, to: exactDateEnd) else { return false}
                    return (dateStart.compare(transactionDate) == .orderedAscending && transactionDate.compare(dateEnd) == .orderedAscending)
                }
            case "Month".localized:
                return arrayToFilter.filter {
                    guard let transDate = $0.transactionDate else { return false }
//                    let transDateStr = transDate.stringValue(dateFormatIs: "MMM yyyy")
//                    guard let transactionDate = transDateStr.dateValue(dateFormatIs: "MMM yyyy") else { return false }
                    guard let dateStr = buttonDateTwo.currentTitle else { return false }
                    guard let monthToFilter = dateStr.dateValue(dateFormatIs: "MMM yyyy") else { return false }
                    return Calendar.current.isDate(transDate, equalTo: monthToFilter, toGranularity: .month)
                }
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcessBillPayment(forButtonOne arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        func filterCheck(str: String) -> [ReportTransactionRecord] {
            return arrayToFilter.filter {
                guard let descript = $0.desc else { return false }
                return descript.containsIgnoringCase(find: str)
            }
        }
        if let labelFilterOne = labelFilterOne.text {
            switch labelFilterOne {
            case "Transaction".localized, "All".localized:
                return arrayToFilter
            case "Postpaid Mobile".localized:
                return filterCheck(str: "POSTPAID")
            case "Landlines".localized:
                return filterCheck(str: "LANDLINES")
            case "Electricity".localized:
                return filterCheck(str: "ELECTRICITY")
            case "Ycdc".localized:
                return filterCheck(str: "YCDC")
            case "Gift Card".localized:
                return filterCheck(str: "GIFTCARD")
            case "DTH".localized:
                return filterCheck(str: "DTH")
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcessBillPayment(forButtonTwo arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        return filterProcessTransaction(forButtonTwo: arrayToFilter)
    }
    
    func filterProcessBillPayment(forButtonThree arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        return filterProcessTransaction(forButtonThree: arrayToFilter)
    }
    
    func filterProcessBillPayment(forButtonFour arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        return filterProcessTransaction(forButtonFour: arrayToFilter)
    }
    
    func filterProcessBillPayment(forButtonFive arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        return filterProcessTransaction(forButtonFive: arrayToFilter)
    }
    
    func filterProcessTopUp(forButtonOne arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        if let labelFilterOne = labelFilterOne.text {
            switch labelFilterOne {
            case "Transaction".localized, "All".localized:
                return arrayToFilter
            case "Cash Back".localized:
                return arrayToFilter.filter {
                    guard let cashBack = $0.cashBack else { return false }
                    return cashBack.compare(0) == .orderedDescending
                }
            case "Bonus Points".localized:
                return arrayToFilter.filter {
                    guard let bonus = $0.bonus else { return false }
                    return bonus.compare(0) == .orderedDescending
                }
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcessTopUp(forButtonTwo arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        return filterProcessTransaction(forButtonTwo: arrayToFilter)
    }
    
    func filterProcessTopUp(forButtonThree arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        func filterCheck(str: String) -> [ReportTransactionRecord] {
            return arrayToFilter.filter {
                guard let desc = $0.rawDesc else { return false }
                return desc.contains(find: str)
            }
        }
        if let labelFilterOne = labelFilterThree.text {
            switch labelFilterOne {
            case "Operator".localized, "All".localized:
                return arrayToFilter
            case "Mpt".localized:
                return filterCheck(str: "Mpt")
            case "Telenor".localized:
                return filterCheck(str: "Telenor")
            case "Ooredoo".localized:
                return filterCheck(str: "Ooredoo")
            case "Mectel".localized:
                return filterCheck(str: "Mectel")
            case "Mytel".localized:
                return filterCheck(str: "Mytel")
            case "International Top-Up".localized:
                return filterCheck(str: "Overseas")
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcessTopUp(forButtonFour arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        func filterCheck(str: String) -> [ReportTransactionRecord] {
            return arrayToFilter.filter {
                guard let desc = $0.desc else { return false }
                return desc.contains(find: str)
            }
        }
        if let labelFilterOne = labelFilterFour.text {
            switch labelFilterOne {
            case "Type".localized, "All".localized:
                return arrayToFilter
            case "Top-Up".localized:
                return filterCheck(str: "TopUp Plan")
            case "Special Offers".localized:
                return filterCheck(str: "SpecialOffers")
            case "Data Plan".localized:
                return filterCheck(str: "Data Plan")
            case "Air Time".localized:
                return filterCheck(str: "Air Time")
            case "Overseas Top-Up".localized:
                return filterCheck(str: "Overseas")
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcessTopUp(forButtonFive arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        return filterProcessTransaction(forButtonFive: arrayToFilter)
    }
    
    func filterProcessReceivedMoney(forButtonOne arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        return filterProcessTransaction(forButtonOne: arrayToFilter)
    }
    
    func filterProcessReceivedMoney(forButtonTwo arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        return filterProcessTransaction(forButtonTwo: arrayToFilter)
    }
    
    func filterProcessReceivedMoney(forButtonThree arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        return filterProcessTransaction(forButtonThree: arrayToFilter)
    }
    
    func filterProcessReceivedMoney(forButtonFour arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        return filterProcessTransaction(forButtonFour: arrayToFilter)
    }
    
    func filterProcessReceivedMoney(forButtonFive arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        return filterProcessTransaction(forButtonFive: arrayToFilter)
    }
    
    func filterProcess(forMobileNumber arrayToFilter: [ReportTransactionRecord]) -> [ReportTransactionRecord] {
        switch currentMobileNumber {
        case "Mobile Number".localized, "All".localized:
            return arrayToFilter
        default:
            return arrayToFilter.filter {
                guard let mobileNumber = $0.destination else { return false }
                var check = mobileNumber
                if mobileNumber.hasPrefix("00") {
                    let mobileNum = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: mobileNumber)
                    check = "(\(mobileNum.counrtycode))\(mobileNum.mobileNo)"
                } else {
                    check = "(\($0.countryCode ?? ""))\(mobileNumber)"
                }
                return check == currentMobileNumber
            }
        }
    }
    
    // MARK: - Date picker configurations
    func configureDatePicker(tag: Int) {
        switch tag {
        case 0:
            buttonDateOne.setTitleColor(UIColor.blue, for: .normal)
            buttonDateTwo.setTitleColor(UIColor(red: 30/255.0, green: 30/255, blue: 30/255, alpha: 1.0), for: .normal)
            datePicker.datePickerMode = .date
            datePicker.removeTarget(nil, action: nil, for: .allEvents)
            datePicker.addTarget(self, action: #selector(fromDatePickerChanged), for: .valueChanged)
            datePicker.minimumDate = nil
            if let endDateStr = buttonDateTwo.title(for: .normal), let endDate = endDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                datePicker.maximumDate = endDate
            } else {
                datePicker.maximumDate = Date()
            }
            if let currentDateStr = buttonDateOne.title(for: .normal), let currentDate = currentDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                datePicker.date = currentDate
            } else {
                buttonDateOne.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            }
        case 1:
            buttonDateOne.setTitleColor(UIColor(red: 30/255.0, green: 30/255, blue: 30/255, alpha: 1.0), for: .normal)
            buttonDateTwo.setTitleColor(UIColor.blue, for: .normal)
            datePicker.datePickerMode = .date
            datePicker.maximumDate = Date()
            datePicker.removeTarget(nil, action: nil, for: .allEvents)
            datePicker.addTarget(self, action: #selector(toDatePickerChanged), for: .valueChanged)
           // datePicker.minimumDate = nil
            if let endDateStr = buttonDateOne.title(for: .normal), let endDate = dateFormatter.date(from: endDateStr) {
                datePicker.minimumDate = endDate
            } else {
                datePicker.minimumDate = nil
            }

            if let currentDateStr = buttonDateTwo.title(for: .normal), let currentDate = currentDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                datePicker.date = currentDate
            } else {
                buttonDateTwo.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            }
        case 2:
            buttonDateTwo.setTitleColor(UIColor.blue, for: .normal)
            datePicker.datePickerMode = .date
            datePicker.maximumDate = Date()
            datePicker.date = Date()
            datePicker.removeTarget(nil, action: nil, for: .allEvents)
            datePicker.addTarget(self, action: #selector(monthPickerChanged), for: .valueChanged)
            datePicker.minimumDate = nil
            if let currentDateStr = buttonDateTwo.title(for: .normal), let currentDate = currentDateStr.dateValue(dateFormatIs: "MMM yyyy") {
                datePicker.date = currentDate
            } else {
                buttonDateTwo.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "MMM yyyy"), for: .normal)
            }
        default:
            break
        }
    }
    
    // MARK: - DatePicker methods
    func showDateOptions(selectedOption: String) {
        switch selectedOption {
        case "Period".localized, "All".localized:
            labelFilterFive.text = selectedOption
            applyFilter()
            isDateViewHidden = true
        case "Date".localized:
            buttonDateOne.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            buttonDateTwo.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            labelDateOne.isHidden = false
            buttonDateOne.isHidden = false
            labelDateTwo.text = "To Date".localized
            configureDatePicker(tag: 0)
            isDateViewHidden = false
        case "Month".localized:
            buttonDateOne.setTitle(nil, for: .normal)
            buttonDateTwo.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "MMM yyyy"), for: .normal)
            labelDateOne.isHidden = true
            buttonDateOne.isHidden = true
            labelDateTwo.text = "Month".localized
            configureDatePicker(tag: 2)
            isDateViewHidden = false
        default:
            break
        }
    }
    
    func updateFilterTableHeight() {
        constraintTableFilterHeight.constant = currentFilterValues.count > 4 ? CGFloat(5*44) : CGFloat(currentFilterValues.count * 44)
        self.view.layoutIfNeeded()
    }
    
    func updateNavigationTableHeight() {
        constraintTableNavigationHeight.constant = optionsInNavigation.count > 4 ? CGFloat(5*44) : CGFloat(currentFilterValues.count * 44)
        self.view.layoutIfNeeded()
    }
    
    @objc func fromDatePickerChanged(datePicker:UIDatePicker) {
        buttonDateOne.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
    }
    
    @objc func toDatePickerChanged(datePicker:UIDatePicker) {
        if let startDateStr = buttonDateOne.title(for: .normal), let startDate = startDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
            switch datePicker.date.compare(startDate) {
            case .orderedAscending:
                if let endDateStr = buttonDateTwo.title(for: .normal), let endDate = endDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                    datePicker.date = endDate
                }
            case .orderedDescending, .orderedSame:
                buttonDateTwo.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            }
        } else {
            buttonDateTwo.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
        }
    }
    
    @objc func monthPickerChanged(datePicker:UIDatePicker) {
        buttonDateTwo.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "MMM yyyy"), for: .normal)
    }
    
    // MARK: - Target Methods
    @objc private func showMoreOption() {
        if !tableNavigationOptions.isHidden {
            tableNavigationOptions.isHidden = true
        } else {
            currentBarButton = .moreOption
            tableViewFilter.isHidden = true
            constraintTableNavigationTrailing.constant = 5
            self.view.layoutIfNeeded()
            optionsInNavigation = graphOptions
            tableNavigationOptions.reloadData()
            updateNavigationTableHeight()
            tableNavigationOptions.isHidden = false
        }
    }
    
    @objc private func showNumberList() {
        if !tableNavigationOptions.isHidden {
            tableNavigationOptions.isHidden = true
        } else {
            currentBarButton = .mobileNumber
            tableViewFilter.isHidden = true
            constraintTableNavigationTrailing.constant = 60
            self.view.layoutIfNeeded()
            optionsInNavigation = mobileNumberOptions
            tableNavigationOptions.reloadData()
            updateNavigationTableHeight()
            tableNavigationOptions.isHidden = false
        }
    }
    
    @objc func filterSelectedDate() {
        if let _ = buttonDateOne.title(for: .normal) {
            labelFilterFive.text = "Date".localized
        } else {
            labelFilterFive.text = "Month".localized
        }
        applyFilter()
        isDateViewHidden = true
    }
    
    @objc func hideDateSelectionView() {
        isDateViewHidden = true
    }
    
    // MARK: - Button Actions
    // It filters and show the detail by transaction type
    @IBAction func filterButtonOneValue(_ sender: UIButton) {
        tableNavigationOptions.isHidden = true
        func showFilterAtButtonOne() {
            currentFilterValues = filterOneArrayValues
            currentFilterType = .buttonOne
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = labelFilterOne.frame.minX
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
        }
        if tableViewFilter.isHidden {
            showFilterAtButtonOne()
        } else {
            switch currentFilterType {
            case .buttonOne:
                tableViewFilter.isHidden = true
            default:
                showFilterAtButtonOne()
            }
        }
    }
    
    @IBAction func configureDatePickerOne(_ sender: UIButton) {
        configureDatePicker(tag: 0)
    }
    
    @IBAction func configureDatePickerTwo(_ sender: UIButton) {
        if buttonDateOne.isHidden {
            configureDatePicker(tag: 2)
        } else {
            configureDatePicker(tag: 1)
        }
    }
    
    // MARK: - Button Actions
    // It filters and show the detail by amount range
    @IBAction func filterButtonTwoValue(_ sender: UIButton) {
        tableNavigationOptions.isHidden = true
        func showFilterAtButtonTwo() {
            currentFilterValues = filterTwoArrayValues
            currentFilterType = .buttonTwo
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = labelFilterTwo.frame.minX
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
        }
        if tableViewFilter.isHidden {
            showFilterAtButtonTwo()
        } else {
            switch currentFilterType {
            case .buttonTwo:
                tableViewFilter.isHidden = true
            default:
                showFilterAtButtonTwo()
            }
        }
    }
    
    // It filters and show the detail by operator
    @IBAction func filterButtonThreeValue(_ sender: UIButton) {
        tableNavigationOptions.isHidden = true
        func showFilterAtButtonThree() {
            currentFilterValues = filterThreeArrayValues
            currentFilterType = .buttonThree
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = labelFilterThree.frame.minX
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
        }
        if tableViewFilter.isHidden {
            showFilterAtButtonThree()
        } else {
            switch currentFilterType {
            case .buttonThree:
                tableViewFilter.isHidden = true
            default:
                showFilterAtButtonThree()
            }
        }
    }
    
    // It filters and show the detail by offers
    @IBAction func filterButtonFourValue(_ sender: UIButton) {
        tableNavigationOptions.isHidden = true
        func showFilterAtButtonFour() {
            currentFilterValues = filterFourArrayValues
            currentFilterType = .buttonFour
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = labelFilterFour.frame.minX
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
        }
        if tableViewFilter.isHidden {
            showFilterAtButtonFour()
        } else {
            switch currentFilterType {
            case .buttonFour:
                tableViewFilter.isHidden = true
            default:
                showFilterAtButtonFour()
            }
        }
    }
    
    // It filters and show the detail by period
    @IBAction func filterButtonFiveValue(_ sender: UIButton) {
        tableNavigationOptions.isHidden = true
        func showFilterAtButtonFive() {
            currentFilterValues = filterFiveArrayValues
            currentFilterType = .buttonFive
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = labelFilterFive.frame.minX - 20
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
        }
        if tableViewFilter.isHidden {
            showFilterAtButtonFive()
        } else {
            switch currentFilterType {
            case .buttonFive:
                tableViewFilter.isHidden = true
            default:
                showFilterAtButtonFive()
            }
        }
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension StatisticsTransactionAndBillController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView.tag {
        case 0:
            return currentFilterValues.count
        case 1:
            return optionsInNavigation.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView.tag {
        case 0:
            let filterCell = tableView.dequeueReusableCell(withIdentifier: ConstantsXibCell.filterTableViewCell, for: indexPath) as? FilterTableViewCell
            filterCell?.configureStatistics(filterName: currentFilterValues[indexPath.row])
            return filterCell!
        case 1:
            switch currentBarButton {
            case .mobileNumber, .moreOption:
                let moreOptionCell = tableView.dequeueReusableCell(withIdentifier: ConstantsXibCell.filterTableViewCell, for: indexPath) as? FilterTableViewCell
                moreOptionCell?.configureStatistics(filterName: optionsInNavigation[indexPath.row])
                return moreOptionCell!
            }
        default:
            let filterCell = tableView.dequeueReusableCell(withIdentifier: ConstantsXibCell.filterTableViewCell, for: indexPath) as? FilterTableViewCell
            return filterCell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView.tag {
        case 0:
            switch currentFilterType {
            case .buttonOne:
                labelFilterOne.text = currentFilterValues[indexPath.row].localized
                tableViewFilter.isHidden = true
                applyFilter()
            case .buttonTwo:
                labelFilterTwo.text = currentFilterValues[indexPath.row].localized
                tableViewFilter.isHidden = true
                applyFilter()
            case .buttonThree:
                labelFilterThree.text = currentFilterValues[indexPath.row].localized
                tableViewFilter.isHidden = true
                applyFilter()
            case .buttonFour:
                labelFilterFour.text = currentFilterValues[indexPath.row].localized
                tableViewFilter.isHidden = true
                applyFilter()
            case .buttonFive:
                tableViewFilter.isHidden = true
                showDateOptions(selectedOption: currentFilterValues[indexPath.row].localized)
            }
        case 1:
            switch currentBarButton {
            case .mobileNumber:
                currentMobileNumber = optionsInNavigation[indexPath.row].localized
                setUpNavigation()
            case .moreOption:
                switch indexPath.row {
                case 0:
                    viewPieChart.isHidden = true
                    viewLineChart.isHidden = true
                    viewBarChart.isHidden = false
                    viewBarChart.data = nil
                    //barChartUpdate()
                case 1:
                    viewBarChart.isHidden = true
                    viewLineChart.isHidden = true
                    viewPieChart.isHidden = false
                    viewPieChart.data = nil
                    //pieChartUpdate()
                case 2:
                    viewBarChart.isHidden = true
                    viewPieChart.isHidden = true
                    viewLineChart.isHidden = false
                    viewLineChart.data = nil
                    //lineChartUpdate()
                default:
                    break
                }
            }
            tableNavigationOptions.isHidden = true
            applyFilter()
        default:
            break
        }
    }
}

// MARK: - Additional Methods
extension StatisticsTransactionAndBillController: NavigationSetUpProtocol {
    /// It set up the navigation bar
    func setUpNavigation() {
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: titleViewWidth, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: -15, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        
        let moreButton = UIButton(type: .custom)
        moreButton.setImage(#imageLiteral(resourceName: "topUpMore").withRenderingMode(.alwaysOriginal), for: .normal)
        moreButton.addTarget(self, action: #selector(showMoreOption), for: .touchUpInside)
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        let buttonView = UIView()
//        switch screenType {
//        case .billPayments:
//            break
//        default:
            let optionButton = UIButton.init(type: .custom)
            optionButton.addTarget(self, action: #selector(showNumberList), for: .touchUpInside)
            let optionLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 160, height: 30))
            optionLabel.text = currentMobileNumber
            optionLabel.font = UIFont(name: appFont, size: 18)
            optionLabel.textAlignment = .center
            optionLabel.textColor = UIColor.white
            let optionImageView = UIImageView(frame: CGRect(x: 160, y: 12, width: 10, height: 9))
            optionImageView.image = UIImage(named: "reportArrowDownWhite")
            optionButton.frame = CGRect(x: 0, y: 0, width: 170, height: 30)
            let ypos = (titleViewSize.height - 30 ) / 2
           // buttonView.frame = CGRect(x: navTitleView.frame.size.width - 210, y: ypos, width: 170, height: 30)
            buttonView.addSubview(optionButton)
            buttonView.addSubview(optionLabel)
            buttonView.addSubview(optionImageView)
             if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
                buttonView.frame = CGRect(x: navTitleView.frame.size.width - 280, y: ypos, width: 170, height: 30)
                moreButton.frame = CGRect(x: navTitleView.frame.size.width - 100, y: ypos, width: 30, height: 30)
             }
             else{
                buttonView.frame = CGRect(x: navTitleView.frame.size.width - 210, y: ypos, width: 170, height: 30)
                moreButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
             }
           
//        }
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 320, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        switch screenType {
        case .transactionDetail, .statistics:
            label.text = ConstantsController.statisticsTransactionController.screenTitle.localized + " "
            self.navigationController?.navigationBar.barTintColor        = ConstantsColor.navigationHeaderTransaction
        case .billPayments:
            label.text = ConstantsController.statisticsBillController.screenTitle.localized + " "
            self.navigationController?.navigationBar.barTintColor        = ConstantsColor.navigationHeaderBillPayment
        case .topUp:
            label.text = ConstantsController.statisticsTopUpController.screenTitle.localized + " "
            self.navigationController?.navigationBar.barTintColor        = ConstantsColor.navigationHeaderTopUpRecharge
        case .receivedMoney:
            label.text = ConstantsController.statisticsReceivedMoneyController.screenTitle.localized + " "
            self.navigationController?.navigationBar.barTintColor        = ConstantsColor.navigationHeaderTransaction
        }
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 23) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        navTitleView.addSubview(moreButton)
        switch screenType {
        case .billPayments:
            break
        default:
            navTitleView.addSubview(buttonView)
        }
        
        self.navigationItem.titleView = navTitleView
    }
    
    /// It shows the home screen
    @objc func backAction() {
        switch screenType {
        case .statistics:
            AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        default:
            break
        }
        self.navigationController?.popViewController(animated: true)
    }
}
