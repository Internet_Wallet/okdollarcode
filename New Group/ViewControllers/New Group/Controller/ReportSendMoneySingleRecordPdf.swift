//
//  ReportSendMoneySingleRecordPdf.swift
//  OK
//  For pdf file of single record in A4 Size - 595 x 842
//  Created by ANTONY on 15/03/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportSendMoneySingleRecordPdf: UIView {
    
    @IBOutlet weak var businessName: UILabel!{
        didSet{
            self.businessName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var receiverName: UILabel!{
        didSet{
            self.receiverName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var bankName: UILabel!{
        didSet{
            self.bankName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var branchName: UILabel!{
        didSet{
            self.branchName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var receiverAccNumber: UILabel!{
        didSet{
            self.receiverAccNumber.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var receiverAccountType: UILabel!{
        didSet{
            self.receiverAccountType.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var transactionID: UILabel!{
        didSet{
            self.transactionID.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var dateTime: UILabel!{
        didSet{
            self.dateTime.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var remarks: UILabel!{
        didSet{
            self.remarks.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var amount: UILabel!{
        didSet{
            self.amount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var viewSendMoneyBankReceipt: UIView!
    @IBOutlet weak var lblBusinessName: UILabel!{
        didSet{
            self.lblBusinessName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblBusinessSept: UILabel!{
        didSet{
            self.lblBusinessSept.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }

    func fillDetails(report: ReportSendMoneyToBank) {
        self.businessName.text = UserModel.shared.businessName
        if UserModel.shared.businessName.length == 0 {
            businessName.isHidden = true
            lblBusinessName.isHidden = true
            lblBusinessSept.isHidden = true
        } else {
             businessName.isHidden = false
            lblBusinessName.isHidden = false
            lblBusinessSept.isHidden = false
        }
//                    businessName.isHidden = true
//                 lblBusinessName.isHidden = true
//                 lblBusinessSept.isHidden = true
        
        self.receiverName.text = ""
        if let accName = report.receiverOkAccName {
            self.receiverName.text = accName
        }
        self.bankName.text = ""
        if let bnkName = report.bankName {
            self.bankName.text = bnkName
        }
        self.branchName.text = ""
        if let brnchName = report.bankBranchName {
            self.branchName.text = brnchName
        }
        self.receiverAccountType.text = ""
        if let accType = report.bankAccType {
            self.receiverAccountType.text = accType
        }
        self.receiverAccNumber.text = ""
        if let accNumber = report.bankAccNumber {
            self.receiverAccNumber.text = accNumber
        }
        self.amount.text = ""
        if let amount = report.transferredAmount {
            let amountInString = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: amount.toString())
            self.amount.text = UIViewController().amountInFormat(amountInString) + " " + "MMK".localized
        }
        self.transactionID.text = ""
        if let transID = report.transactionID {
            self.transactionID.text = transID
        }
        self.dateTime.text = ""
        if let dateTime = report.transactionDateTime {
            self.dateTime.text = dateTime.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy\nHH:mm:ss")
        }
        self.remarks.text = "Remarks".localized + ":- " + ""
        self.viewSendMoneyBankReceipt.layer.borderColor = UIColor.lightGray.cgColor
        self.viewSendMoneyBankReceipt.layer.borderWidth = 1
    }
}
