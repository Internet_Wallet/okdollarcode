//
//  ReportVotingListExtensionVC.swift
//  OK
//
//  Created by E J ANTONY on 24/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation

extension ReportVotingListViewController {
    //Methods
    func noRecordAlertAction() {
        alertViewObj.wrapAlert(title: "Alert".localized, body: "No Records".localized, img: UIImage(named : "AppIcon"))
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
        })
        alertViewObj.showAlert(controller: self)
    }
    
    func resetFilterButtons() {
        buttonFilterVotingType.setTitle(voteTypes[0].localized, for: .normal)
        buttonFilterPeriod.setTitle(periodTypes[0].localized, for: .normal)
    }
    
    func updateFilterTableHeight() {
        constraintFilterTableHeight.constant = filterArray.count > 4 ? CGFloat(5*44) : CGFloat(filterArray.count * 44)
        self.view.layoutIfNeeded()
    }
    
    func resetSortButtons(sender: UIButton) {
        if sender != buttonProgName {
            buttonProgName.tag = 0
            buttonProgName.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonEventName {
            buttonEventName.tag = 0
            buttonEventName.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonParticipateName {
            buttonParticipateName.tag = 0
            buttonParticipateName.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonTransID {
            buttonTransID.tag = 0
            buttonTransID.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonVotes {
            buttonVotes.tag = 0
            buttonVotes.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonVotingType {
            buttonVotingType.tag = 0
            buttonVotingType.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonAmount {
            buttonAmount.tag = 0
            buttonAmount.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonDate {
            buttonDate.tag = 0
            buttonDate.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
    }
    
    func setLocalizedTitles() {
        buttonProgName.setTitle("Programme Name".localized, for: .normal)
        buttonEventName.setTitle("Event".localized, for: .normal)
        buttonParticipateName.setTitle("Participate Name".localized, for: .normal)
        buttonTransID.setTitle("Transaction ID".localized, for: .normal)
        buttonVotes.setTitle("Votes".localized, for: .normal)
        buttonVotingType.setTitle("Voting type".localized, for: .normal)
        buttonAmount.setTitle("Amount".localized + " (MMK)".localized, for: .normal)
        buttonDate.setTitle("Date & Time".localized, for: .normal)
    }
    
    func shareRecordsByPdf() {
        let pdfView = UIView()
        if let pdfHeaderView = Bundle.main.loadNibNamed("ReportVotingPdfHeader", owner: self, options: nil)?.first as? ReportVotingPdfHeader {
            
            pdfHeaderView.fillDetails(voteReports: reportVotingList, filterPeriod: buttonFilterPeriod.currentTitle ?? "", filterVotingType: buttonFilterVotingType.currentTitle ?? "")
            
            let newHeight = pdfHeaderView.viewForFilterSort.frame.maxY
            pdfHeaderView.frame = CGRect(x: 0, y: 0, width: pdfHeaderView.frame.size.width, height: newHeight)
            pdfView.addSubview(pdfHeaderView)
            pdfView.frame = pdfHeaderView.bounds
            let xPos: CGFloat = pdfHeaderView.viewForFilterSort.frame.minX
            var yPos: CGFloat = pdfView.frame.maxY
            
            for record in reportVotingList {
                if let pdfBodyView = Bundle.main.loadNibNamed("ReportVotingPdfBody", owner: self, options: nil)?.first as? ReportVotingPdfBody {
                    pdfBodyView.configureData(voteData: record)
                    let recordHeight = pdfBodyView.frame.size.height
                    let yPosInPage = Int(yPos) % 842
                    if (yPosInPage + Int(recordHeight)) > 842 {
                        yPos = yPos + CGFloat((842 - yPosInPage) + 30 + 30)
                    }
                    pdfBodyView.frame = CGRect(x: xPos, y: yPos, width: pdfBodyView.frame.size.width, height: recordHeight)
                    yPos = yPos + recordHeight + 2
                    pdfView.addSubview(pdfBodyView)
                }
            }
            pdfView.frame = CGRect(x: 0, y: 0, width: pdfView.frame.size.width, height: yPos + 30)
            guard let pdfUrl = CodeSnippets.generatePDF(viewForPDF: pdfView, pdfFile: "OK$ Voting-Reports") else {
                println_debug("Error - pdf not generated")
                return
            }
            let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
            if self.presentedViewController != nil {
                self.dismiss(animated: false, completion: {
                    [unowned self] in
                    DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
                })
            }else{
                DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
            }
        }
    }
    
    func shareRecordsByExcel() {
        var excelTxt = "Selected Filter:\n\nVoting type,Period\n"
        var typeFilter = ""
        if let voteTypeFilter = buttonFilterVotingType.currentTitle {
            if voteTypeFilter  == "Voting type".localized {
                typeFilter = "All".localized
            } else {
                typeFilter = voteTypeFilter.localized
            }
        }
        var periodFilter = ""
        if let peridFilter = buttonFilterPeriod.currentTitle {
            if peridFilter  == "Period".localized {
                periodFilter = "All".localized
            } else {
                periodFilter = peridFilter.localized
            }
        }
        periodFilter = periodFilter.replacingOccurrences(of: "\n", with: " ")
        excelTxt.append("\(typeFilter),\(periodFilter)\n\n")
        excelTxt.append("Selected Filter Records:\n\n")
        excelTxt.append("Programme Name,Event,Participate Name,Transaction ID,Votes,Voting type,Amount (MMK),Date & Time\n")
        var totalTransAmount: Double = 0
        for record in reportVotingList {
            var progName = ""
            if let mainProgram = record.mainProgrammeName {
                progName = mainProgram
                if let subProgramName = record.programmeName {
                    progName += "\n\(subProgramName)"
                }
            }
            var event = ""
            if let eventName = record.eventName {
                event = eventName
            }
            var participName = ""
            if let participateName = record.participantName {
                participName = participateName
            }
            var transactionID = ""
            if let transId = record.payment?.transactionID {
                transactionID = transId
            }
            var voteCount = "0"
            if let safeVoteCount = record.votes {
                voteCount = "\(safeVoteCount)"
            }
            var votingType = ""
            if let type = record.votingTypeName {
                votingType = type
            }
            var amount = "0"
            if let amountVal = record.payment?.amount {
                amount = "\(amountVal)"
                totalTransAmount += amountVal
            }
            var dateVal = ""
            if let createdDateStr = record.createdDate {
                if let dateValue = createdDateStr.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") {
                    let dateString = dateValue.stringValue(dateFormatIs: "dd-MM-yyyy HH:mm:ss")
                    dateVal = dateString
                }
            }
            
            var recText = ""
            recText = "\(progName),\(event),\(participName),\(transactionID),\(voteCount),\(votingType),\(amount),\(dateVal)\n\n"
            excelTxt.append(recText)
        }
        excelTxt.append("Total Transaction Amount,,,,,,\(totalTransAmount) MMK")
        guard let excelUrl = CodeSnippets.generateExcel(excelDataWithFormat: excelTxt, excelFileName: "OK$ Voting-Reports") else {
            println_debug("Error - excel not generated")
            return
        }
        let activityViewController = UIActivityViewController(activityItems: [excelUrl], applicationActivities: nil)
        if self.presentedViewController != nil {
            self.dismiss(animated: false, completion: {
                [unowned self] in
                DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
            })
        }else{
            DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
        }
    }
    
    //Button Action Methods
    @IBAction func sortByProgramName(_ sender: UIButton) {
        func sortProgramName(orderType: ComparisonResult) -> [VotingReportList] {
            return reportVotingList.sorted {
                guard let mainPrg0 = $0.mainProgrammeName, let prg0 = $0.programmeName else { return false }
                guard let mainPrg1 = $1.mainProgrammeName, let prg1 = $1.programmeName else { return false }
                let programName1 = mainPrg0 + " " + prg0
                let programName2 = mainPrg1 + " " + prg1
                return programName1.lowercased().localizedStandardCompare(programName2.lowercased()) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            reportVotingList = sortProgramName(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            reportVotingList = sortProgramName(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByEventName(_ sender: UIButton) {
        func sortEventName(orderType: ComparisonResult) -> [VotingReportList]{
            return reportVotingList.sorted {
                guard let eventName1 = $0.eventName, let eventName2 = $1.eventName else { return false }
                return eventName1.lowercased().localizedStandardCompare(eventName2.lowercased()) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            reportVotingList = sortEventName(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            reportVotingList = sortEventName(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByParticipate(_ sender: UIButton) {
        func sortParticipateName(orderType: ComparisonResult) -> [VotingReportList]{
            return reportVotingList.sorted {
                guard let participate1 = $0.participantName, let participate2 = $1.participantName else { return false }
                return participate1.lowercased().localizedStandardCompare(participate2.lowercased()) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            reportVotingList = sortParticipateName(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            reportVotingList = sortParticipateName(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByTransactionID(_ sender: UIButton) {
        func sortTransID(orderType: ComparisonResult) -> [VotingReportList]{
            var firstArray = [VotingReportList]()
            var secondArray = [VotingReportList]()
            for item in reportVotingList {
                if let _ = item.payment?.amount {
                    firstArray.append(item)
                } else {
                    secondArray.append(item)
                }
            }
            switch orderType {
            case .orderedAscending:
                firstArray = firstArray.sorted {
                    guard let id1 = $0.payment?.transactionID, let id2 = $1.payment?.transactionID else { return false }
                    return id1 < id2
                }
                return secondArray + firstArray
            case .orderedDescending:
                firstArray = firstArray.sorted {
                    guard let id1 = $0.payment?.transactionID, let id2 = $1.payment?.transactionID else { return false }
                    return id1 > id2
                }
                return firstArray + secondArray
            default:
                return reportVotingList
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            reportVotingList = sortTransID(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            reportVotingList = sortTransID(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByVotes(_ sender: UIButton) {
        func sortVoteCount(orderType: ComparisonResult) -> [VotingReportList]{
            switch orderType {
            case .orderedAscending:
                return reportVotingList.sorted {
                    guard let vote1 = $0.votes, let vote2 = $1.votes else { return false }
                    return vote1 < vote2
                }
            case .orderedDescending:
                return reportVotingList.sorted {
                    guard let vote1 = $0.votes, let vote2 = $1.votes else { return false }
                    return vote1 > vote2
                }
            default:
                return reportVotingList
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            reportVotingList = sortVoteCount(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            reportVotingList = sortVoteCount(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByVoteType(_ sender: UIButton) {
        func sortVoteType(orderType: ComparisonResult) -> [VotingReportList]{
            return reportVotingList.sorted {
                guard let voteType1 = $0.votingTypeName, let voteType2 = $1.votingTypeName else { return false }
                return voteType1.lowercased().localizedStandardCompare(voteType2.lowercased()) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            reportVotingList = sortVoteType(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            reportVotingList = sortVoteType(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByAmount(_ sender: UIButton) {
        func sortAmount(orderType: ComparisonResult) -> [VotingReportList] {
            var firstArray = [VotingReportList]()
            var secondArray = [VotingReportList]()
            for item in reportVotingList {
                if let _ = item.payment?.amount {
                    firstArray.append(item)
                } else {
                    secondArray.append(item)
                }
            }
            switch orderType {
            case .orderedAscending:
                firstArray = firstArray.sorted {
                    guard let amount1 = $0.payment?.amount, let amount2 = $1.payment?.amount else { return false }
                    return amount1 < amount2
                }
                return secondArray + firstArray
            case .orderedDescending:
                firstArray = firstArray.sorted {
                    guard let amount1 = $0.payment?.amount, let amount2 = $1.payment?.amount else { return false }
                    return amount1 > amount2
                }
                return firstArray + secondArray
            default:
                return reportVotingList
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            reportVotingList = sortAmount(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            reportVotingList = sortAmount(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByDateTime(_ sender: UIButton) {
        func sortDates(orderType: ComparisonResult) -> [VotingReportList]{
            return reportVotingList.sorted {
                guard let date1 = $0.createdDate, let date2 = $1.createdDate else { return false }
                return date1.compare(date2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            reportVotingList = sortDates(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            reportVotingList = sortDates(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
}

// MARK: - UITextFieldDelegate
extension ReportVotingListViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tableViewFilter.isHidden = true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        tableViewVoteList.contentOffset = CGPoint(x: 0, y: 0)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if range.location == 0 && string == " " { return false }
        if let text = textField.text, let textRange = Range(range, in: text) {
            if text.last == " " && string == " " { return false }
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            if !(updatedText == updatedText.components(separatedBy: NSCharacterSet(charactersIn: SEARCHCHARSET).inverted).joined(separator: "")) { return false }
            
            let containsEmoji: Bool = updatedText.containsEmoji
            if (containsEmoji){
                return false
            }
            if updatedText.count > 50 || (updatedText == "" && text == "") {
                return false
            }
            if string != "\n" {
                applyFilter(searchText: updatedText)
            }
        }
        return true
    }
}

// MARK: - Additional Methods
extension ReportVotingListViewController: NavigationSetUpProtocol {
    /// It set up the navigation bar
    func setUpNavigation() {
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: titleViewWidth, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        
        let searchButton = UIButton(type: .custom)
        searchButton.setImage(#imageLiteral(resourceName: "reportSearch").withRenderingMode(.alwaysOriginal), for: .normal)
        searchButton.addTarget(self, action: #selector(showSearchView), for: .touchUpInside)
        
        let shareButton = UIButton(type: .custom)
        shareButton.setImage(#imageLiteral(resourceName: "reportShare").withRenderingMode(.alwaysOriginal), for: .normal)
        shareButton.addTarget(self, action: #selector(showShareOption), for: .touchUpInside)
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        let ypos = (titleViewSize.height - 30 ) / 2
        if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 140, y: ypos, width: 30, height: 30)
            shareButton.frame = CGRect(x: navTitleView.frame.size.width - 100, y: ypos, width: 30, height: 30)
            backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        }
        else{
        searchButton.frame = CGRect(x: navTitleView.frame.size.width - 70, y: ypos, width: 30, height: 30)
        shareButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        }
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 320, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text = ConstantsController.reportVotingListViewController.screenTitle.localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 23) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        navTitleView.addSubview(searchButton)
        navTitleView.addSubview(shareButton)
        self.navigationItem.titleView = navTitleView
    }
    
    /// It shows the home screen
    @objc func backAction() {
        
        if !isSearchViewShow {
            AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
            self.navigationController?.popViewController(animated: true)
        } else {
            
            self.hideSearchView()
        }
        
    }
}
