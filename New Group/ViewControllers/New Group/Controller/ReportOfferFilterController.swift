//
//  ReportOfferFilterController.swift
//  OK
//
//  Created by E J ANTONY on 11/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
enum ReportOfferFilterOptions {
    case promotion, location, advanceMerchantNo, dummyMerchantNo
}
protocol OfferAllFilterDelegate {
    func applyAllFilter(promotionID: String, townshipID: String,
                        advanceMerchantNo: String, dummyMerchantNo: [String])
}

class ReportOfferFilterController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var tableViewFilter : UITableView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var constraintHeightStack: NSLayoutConstraint!
    @IBOutlet weak var btnReset : UIButton!{
        didSet {
            btnReset.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
            btnReset.setTitle("Reset".localized, for: .normal)
        }
    }
    @IBOutlet weak var btnShowResult : UIButton!{
        didSet {
            btnShowResult.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
            btnShowResult.setTitle("Show Result".localized, for: .normal)
        }
    }

    // MARK: - Properties
    private var cellIdentifier = "ReportOfferFilterCell"
    private var arrayFilter = [OfferFilter]()
    private class OfferFilter {
        var title: String
        var value: String
        
        init(filterTitle: String, filterValue: String) {
            self.title = filterTitle
            self.value = filterValue
        }
    }
    var sourcePromoId = ""
    var sourcePromoCode = ""
    var promotionCode = ""
    var promotionID = ""
    var locationName = ""
    var locationId = ""
    var advanceMerchantName = ""
    var advanceMerchantNo = ""
    var dummyMerchantNo = ""
    var dummyMerchantNoList = [String]()
    var offerFilterResult = [OfferReportList]()
    var offerAllFilterDelegate: OfferAllFilterDelegate?
    
    // MARK: - View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewFilter.tableFooterView = UIView()
        promotionCode = sourcePromoCode
        promotionID = sourcePromoId
        loadData()
        setUpNavigation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let isHavingPromoCode = promotionCode.trimmingCharacters(in: .whitespacesAndNewlines).count > 0
        let isHavingLocName = locationName.trimmingCharacters(in: .whitespacesAndNewlines).count > 0
        let isHavingAdvMerchantNo = advanceMerchantNo.trimmingCharacters(in: .whitespacesAndNewlines).count > 0
        let isHavingDummyMerchantNo = dummyMerchantNo.trimmingCharacters(in: .whitespacesAndNewlines).count > 0
        if isHavingPromoCode && isHavingLocName && isHavingAdvMerchantNo && isHavingDummyMerchantNo {
            constraintHeightStack.constant = 50
            stackView.isHidden = false
        } else {
            constraintHeightStack.constant = 0
            stackView.isHidden = true
        }
        self.view.layoutIfNeeded()
    }
    //MARK: - Methods
    private func  loadData() {
        arrayFilter = []
        arrayFilter.append(OfferFilter(filterTitle: "Promotion Code", filterValue: promotionCode))
        arrayFilter.append(OfferFilter(filterTitle: "Location", filterValue: locationName))
        var filterValue = ""
        if advanceMerchantName.count > 0 && advanceMerchantNo.count > 0 {
            filterValue = advanceMerchantName + " - " + advanceMerchantNo
        }
        arrayFilter.append(OfferFilter(filterTitle: "Advance Merchant Mobile Number", filterValue: filterValue))
        arrayFilter.append(OfferFilter(filterTitle: "Safety Cashier Mobile Number", filterValue: dummyMerchantNo))
    }
    
    private func navigatToNeededScreen(selectedIndex: Int) {
        if promotionCode.trimmingCharacters(in: .whitespacesAndNewlines).count < 1 {
            navigateToPromotionScreen()
        } else if locationName.trimmingCharacters(in: .whitespacesAndNewlines).count < 1 {
            navigateToLocationScreen(promotionIDParam: self.promotionID)
        } else if advanceMerchantNo.trimmingCharacters(in: .whitespacesAndNewlines).count < 1 {
            navigateToAdvanceMerchant(townshipIDParam: self.locationId, promotionIDParam: self.promotionID)
        } else if dummyMerchantNo.trimmingCharacters(in: .whitespacesAndNewlines).count < 1 {
            navigateToDummyMerchant(townshipIDParam: self.locationId, promotionIDParam: self.promotionID, advanceMerchantNo: advanceMerchantNo)
        } else {
            switch selectedIndex {
            case 0:
                navigateToPromotionScreen()
            case 1:
                navigateToLocationScreen(promotionIDParam: self.promotionID)
            case 2:
                navigateToAdvanceMerchant(townshipIDParam: self.locationId, promotionIDParam: self.promotionID)
            case 3:
                navigateToDummyMerchant(townshipIDParam: self.locationId, promotionIDParam: self.promotionID, advanceMerchantNo: advanceMerchantNo)
            default:
                break
            }
        }
    }
    
    private func navigateToPromotionScreen() {
        guard let reportFilterController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportPromotionLocationFilterController.nameAndID) as? ReportPromotionLocationFilterController else { return }
        reportFilterController.screenName = ReportPromotionLocationFilterController.ScreenType.promotionCode
        reportFilterController.offerFilterInitialResult = self.offerFilterResult
        reportFilterController.delegate = self
        self.navigationController?.pushViewController(reportFilterController, animated: true)
    }
    
    private func navigateToLocationScreen(promotionIDParam: String) {
        guard let reportFilterController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportPromotionLocationFilterController.nameAndID) as? ReportPromotionLocationFilterController else { return }
        reportFilterController.screenName = ReportPromotionLocationFilterController.ScreenType.location
        reportFilterController.promoID = promotionIDParam
        reportFilterController.delegate = self
        self.navigationController?.pushViewController(reportFilterController, animated: true)
    }
    
    private func navigateToAdvanceMerchant(townshipIDParam: String, promotionIDParam: String) {
        guard let reportFilterController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportAdvanceDummyFilterController.nameAndID) as? ReportAdvanceDummyFilterController else { return }
        reportFilterController.screenName = ReportAdvanceDummyFilterController.ScreenType.advanceMerchant
        reportFilterController.townshipID = townshipIDParam
        reportFilterController.promotionID = promotionIDParam
        reportFilterController.advanceFilterDelegate = self
        self.navigationController?.pushViewController(reportFilterController, animated: true)
    }
    
    private func navigateToDummyMerchant(townshipIDParam: String, promotionIDParam: String, advanceMerchantNo: String) {
        guard let reportFilterController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportAdvanceDummyFilterController.nameAndID) as? ReportAdvanceDummyFilterController else { return }
        reportFilterController.screenName = ReportAdvanceDummyFilterController.ScreenType.dummyMerchant
        reportFilterController.townshipID = townshipIDParam
        reportFilterController.promotionID = promotionIDParam
        reportFilterController.advanceMerchantNo = advanceMerchantNo
        reportFilterController.dummyFilterDelegate = self
        self.navigationController?.pushViewController(reportFilterController, animated: true)
    }
    
    //MARK: - Button Action Methods
    @IBAction func showResult(sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        self.offerAllFilterDelegate?.applyAllFilter(promotionID: promotionID, townshipID: locationId, advanceMerchantNo: advanceMerchantNo, dummyMerchantNo: dummyMerchantNoList)
    }
    
    @IBAction func resetFilterData(_ sender: UIButton) {
        self.locationId = ""
        self.locationName = ""
        self.advanceMerchantName = ""
        self.advanceMerchantNo = ""
        self.dummyMerchantNo = ""
        self.dummyMerchantNoList = []
        promotionCode = sourcePromoCode
        promotionID = sourcePromoId
        self.loadData()
        self.tableViewFilter.reloadData()
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ReportOfferFilterController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let reportOfferFilterCell =  tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ReportOfferFilterCell else {
            return ReportOfferFilterCell()
        }
        let filter = arrayFilter[indexPath.row]
        reportOfferFilterCell.configureCell(titleName: filter.title, value: filter.value)
        return reportOfferFilterCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigatToNeededScreen(selectedIndex: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight = 250
        tableView.rowHeight          = UITableView.automaticDimension
        return tableView.rowHeight
    }
}

// MARK: - Additional Methods
extension ReportOfferFilterController: NavigationSetUpProtocol {
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  ConstantsController.reportOfferFilterController.screenTitle.localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
}

//MARK: - ReportModelDelegate
extension ReportOfferFilterController: PromotionLocationFilterDelegate {
    func assignPromoLocoFilter(filterType: ReportOfferFilterOptions, value: String) {
        switch filterType {
        case .promotion:
            let valueArray = value.components(separatedBy: "@@")
            if valueArray.count > 1 {
                self.promotionCode = valueArray[0]
                self.promotionID = valueArray[1]
                self.loadData()
                self.tableViewFilter.reloadData()
                self.navigateToLocationScreen(promotionIDParam: self.promotionID)
            }
        case .location:
            let valueArray = value.components(separatedBy: "@@")
            if valueArray.count > 1 {
                self.locationId = valueArray[0]
                self.locationName = valueArray[1]
                self.loadData()
                self.tableViewFilter.reloadData()
                self.navigateToAdvanceMerchant(townshipIDParam: self.locationId, promotionIDParam: self.promotionID)
            }
        default:
            break
        }
    }
}

//MARK: - ReportModelDelegate
extension ReportOfferFilterController: AdvanceFilterDelegate {
    func assignAdvanceFilter(filterType: ReportOfferFilterOptions, name: String, no: String, amount: String) {
        switch filterType {
        case .advanceMerchantNo:
            self.advanceMerchantName = name
            self.advanceMerchantNo = no
            self.loadData()
            self.tableViewFilter.reloadData()
            self.navigateToDummyMerchant(townshipIDParam: self.locationId, promotionIDParam: self.promotionID, advanceMerchantNo: self.advanceMerchantNo)
        default:
            break
        }
    }
}

//MARK: - ReportModelDelegate
extension ReportOfferFilterController: DummyFilterDelegate {
    func assignDummyFilter(filterType: ReportOfferFilterOptions, selectedItems: [OfferDummyMerchantList]) {
        switch filterType {
        case .dummyMerchantNo:
            if selectedItems.count == 1 {
                dummyMerchantNo = selectedItems[0].dummyMerchantMobileNumber ?? ""
            } else if selectedItems.count > 1 {
                dummyMerchantNo = "Multiple selected"
            }
            dummyMerchantNoList = []
            for item in selectedItems {
                if let no = item.dummyMerchantMobileNumber {
                    dummyMerchantNoList.append(no)
                }
            }
            self.loadData()
            self.tableViewFilter.reloadData()
        default:
            break
        }
    }
}
