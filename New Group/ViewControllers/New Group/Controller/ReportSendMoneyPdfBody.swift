//
//  ReportSendMoneyPdfBody.swift
//  OK
//
//  Created by ANTONY on 14/03/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportSendMoneyPdfBody: UIView {

    @IBOutlet weak var receiverName: UILabel!{
        didSet{
            self.receiverName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var bankAndBranchName: UILabel!{
        didSet{
            self.bankAndBranchName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var accNumberAndAccType: UILabel!{
        didSet{
            self.accNumberAndAccType.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var amount: UILabel!{
        didSet{
            self.amount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var transID: UILabel!{
        didSet{
            self.transID.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var dateAndTime: UILabel!{
        didSet{
            self.dateAndTime.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var balanceLbl: UILabel!{
        didSet{
            self.balanceLbl.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    func fillDetails(record: ReportSendMoneyToBank) {
        self.receiverName.text = ""
        if let accName = record.receiverOkAccName {
            self.receiverName.text = accName
        }
        var bankAndBranchName = ""
        var isAvail = false
        if let bankNam = record.bankName, bankNam.count > 0 {
            isAvail = true
            bankAndBranchName = bankNam
        }
        if let brnchName = record.bankBranchName, brnchName.count > 0 {
            if isAvail {
                bankAndBranchName = "\(bankAndBranchName) \n\(brnchName)"
            } else {
                bankAndBranchName = "\(brnchName)"
            }
        }
        isAvail = false
        self.bankAndBranchName.text = bankAndBranchName
        var accNumAndAccType = ""
        if let bankNumber = record.bankAccNumber, bankNumber.count > 0 {
            isAvail = true
            accNumAndAccType = bankNumber
        }
        if let accType = record.bankAccType, accType.count > 0 {
            let acType = accType
            if isAvail {
                accNumAndAccType = "\(accNumAndAccType) \n\(acType)"
            } else {
                accNumAndAccType = "\(acType)"
            }
        }
        isAvail = false
        self.accNumberAndAccType.text = accNumAndAccType
        if let amount = record.transferredAmount {
            let amountInString = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: amount.toString())
            self.amount.text = UIViewController().amountInFormat(amountInString)
        }
        if let balance = record.balance {
            let amountInString = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: balance.toString())
            self.balanceLbl.text = UIViewController().amountInFormat(amountInString)
        }
        
        self.transID.text = record.transactionID ?? ""
        self.dateAndTime.text = record.transactionDateTime?.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy\nHH:mm:ss")
    }
    
    private func getAccountType(str: String) -> String {
        var acType = ""
        if str == "SAV" {
            acType = "Savings Account"
        } else if str == "CUR" {
            acType = "Current Account"
        } else if str == "ATM" {
            acType = "ATM Card Account"
        } else {
            acType = "Call Deposit Account"
        }
        return acType
    }
}
