//
//  ReportSolarPdfHeader.swift
//  OK
//
//  Created by E J ANTONY on 28/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportSolarPdfHeader: UIView {
    //MARK: - Outlets
    
    @IBOutlet weak var businessNameHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelReportHeader: UILabel!{
        didSet{
            labelReportHeader.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelReportSideHeader: UILabel!{
        didSet{
            labelReportSideHeader.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAccNameTitle: UILabel!{
        didSet{
            labelAccNameTitle.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAccNameValue: UILabel!{
        didSet{
            labelAccNameValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAccNumberTitle: UILabel!{
        didSet{
            labelAccNumberTitle.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAccNumberValue: UILabel!{
        didSet{
            labelAccNumberValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelEmailTitle: UILabel!{
        didSet{
            labelEmailTitle.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelEmailValue: UILabel!{
        didSet{
            labelEmailValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelPeriodTitle: UILabel!{
        didSet{
            labelPeriodTitle.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelPeriodValue: UILabel!{
        didSet{
            labelPeriodValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelTotalAmountTitle: UILabel!{
        didSet{
            labelTotalAmountTitle.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelTotalAmountValue: UILabel!{
        didSet{
            labelTotalAmountValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAddressTitle: UILabel!{
        didSet{
            labelAddressTitle.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAddressValue: UILabel!{
        didSet{
            labelAddressValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAmountTypeFilterTitle: UILabel!{
        didSet{
            labelAmountTypeFilterTitle.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAmountTypeFilterValue: UILabel!{
        didSet{
            labelAmountTypeFilterValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelPeriodFilterTitle: UILabel!{
        didSet{
            labelPeriodFilterTitle.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelPeriodFilterValue: UILabel!{
        didSet{
            labelPeriodFilterValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelName: UILabel!{
        didSet{
            labelName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelTransID: UILabel!{
        didSet{
            labelTransID.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAmount: UILabel!{
        didSet{
            labelAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelServiceFee: UILabel!{
        didSet{
            labelServiceFee.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAccountNo: UILabel!{
        didSet{
            labelAccountNo.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelStatus: UILabel!{
        didSet{
            labelStatus.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDate: UILabel!{
        didSet{
            labelDate.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var viewForFilter: UIView!
    
    
    @IBOutlet weak var businessNameLbl: UILabel!{
        didSet{
            businessNameLbl.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var businessNameVal: UILabel!{
        didSet{
            businessNameVal.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var accountTypeLbl: UILabel!{
        didSet{
            accountTypeLbl.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var accountTypeVal: UILabel!{
        didSet{
            accountTypeVal.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    @IBOutlet weak var totalTransaction: UILabel!{
        didSet{
            totalTransaction.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var balance: UILabel!{
        didSet{
            balance.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var dateTime: UILabel!{
        didSet{
            dateTime.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var addressHeight: NSLayoutConstraint!
    
    func setLocalizationTitles() {
        labelReportHeader.text = "Solar Bill Detail Statement"
        labelReportSideHeader.text = "Account Details"
        labelAccNameTitle.text = "Account Name"
        labelAccNumberTitle.text = "Account Number"
        labelEmailTitle.text = "Email"
        labelPeriodTitle.text = "Period"
        labelTotalAmountTitle.text = "Total Amount"
        labelAddressTitle.text = "Address"
        labelAmountTypeFilterTitle.text = "Amount"
        labelPeriodFilterTitle.text = "Period"
        labelName.text = "Name"
        labelTransID.text = "Transaction ID"
        labelAmount.text = "Amount" + " (MMK)"
        labelServiceFee.text = "Service Fee"
        labelAccountNo.text = "Account No"
        labelStatus.text = "Status"
        labelDate.text = "Date & Time"
    }
    
    func fillDetails(solarReports: [SolarReport], filterPeriod: String, filterAmountType: String, currentBalance: String) {
        setLocalizationTitles()
        getGeneralDetails()
        var totalTransAmount: Double = 0
        for record in solarReports {
            if let amount = record.amount {
                totalTransAmount += amount
            }
        }
        self.balance.text = currentBalance + " MMK"
        self.totalTransaction.text = "\((solarReports.count))"
        self.labelTotalAmountValue.text = "\(totalTransAmount)" + " " + "MMK"
        var titleStr = ""
        if filterAmountType == "Amount".localized {
            titleStr = "All"
        } else {
            titleStr = filterAmountType
        }
        self.labelAmountTypeFilterValue.text = titleStr
        if filterPeriod == "Period".localized {
            titleStr = "All"
        } else {
            titleStr = filterPeriod
        }
        titleStr = titleStr.replacingOccurrences(of: "\n", with: " ")
        self.labelPeriodFilterValue.text = titleStr
        self.labelPeriodValue.text = titleStr
        self.layoutIfNeeded()
    }
    
    func getGeneralDetails() {
        self.labelAccNameValue.text = "\(UserModel.shared.name)"
        //self.businessNameVal.text = (UserModel.shared.businessName != "") ? UserModel.shared.businessName : "-"
        if UserModel.shared.agentType == .user {
            businessNameHeightConstraint.constant = 0
        } else {
            businessNameHeightConstraint.constant = 21
            self.businessNameVal.text = (UserModel.shared.businessName != "") ? UserModel.shared.businessName : "-"
        }

        let accountType = self.agentServiceTypeString(type: UserModel.shared.agentType)
        var phNumber = UserModel.shared.mobileNo.replaceFirstTwoChar(withStr: "+")
        phNumber.insert(")", at: phNumber.index(phNumber.startIndex, offsetBy: 3))
        phNumber.insert("(", at: phNumber.startIndex)
        if UserModel.shared.mobileNo.hasPrefix("0095") {
            phNumber.insert("0", at: phNumber.index(phNumber.startIndex, offsetBy: 5))
        }
        self.labelAccNumberValue.text = "\(phNumber)"
        self.labelEmailValue.text = "\(UserModel.shared.email)".components(separatedBy: ",").first ?? ""
        let division = self.getDivision().components(separatedBy: ",")
        let townShip = "Township"
        var div = ""
        if division.count > 0 {
            div = division.last?.components(separatedBy: "Division").first ?? ""
        }
        if UserModel.shared.language != "en" {
            self.accountTypeVal.text = getBurmeseAgentType()
            self.labelAddressValue.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? ""), \(division.last ?? "")".replacingOccurrences(of: ", ,", with: ",")
        } else {
            self.accountTypeVal.text = accountType
            self.labelAddressValue.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? "") \(townShip), \(div), \(division.last ?? "")".replacingOccurrences(of: ", ,", with: ",")
            self.addressHeight.constant = 65.0
            self.layoutIfNeeded()
        }
//        self.labelAddressValue.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? "") \(townShip), \(div), \(division.last ?? ""), \(UserModel.shared.country)".replacingOccurrences(of: ", ,", with: ",")
        self.dateTime.text = Date().stringValueWithOutUTC(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss")
    }
    private func getDivision() -> String {
        return getDivisionAndTownShipForReport(divisionCode: UserModel.shared.state, townshipCode: UserModel.shared.township)
    }
    
    private func agentServiceTypeString(type: AgentType) -> String {
        switch type {
        case .user:
            return "Personal"
        case .merchant:
            return "Merchant"
        case .agent:
            return "Agent"
        case .advancemerchant:
            return "Advance Merchant"
        case .dummymerchant:
            return "Safety Cashier"
        case .oneStopMart:
            return "One Stop Mart"
        }
    }
}
