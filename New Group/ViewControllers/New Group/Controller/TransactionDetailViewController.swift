//
//  TransactionDetailViewController.swift
//  OK
//  This shows the transaction detail where we can see the result by our choice using the filters
//  Created by ANTONY on 27/01/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class TransactionDetailViewController: OKBaseController, UIGestureRecognizerDelegate {
    // MARK: - Outlets
    @IBOutlet weak var tableViewTransaction: UITableView!
    @IBOutlet weak var tableViewFilter: UITableView! {
        didSet {
            self.tableViewFilter.layer.borderWidth = 1.0
            self.tableViewFilter.layer.borderColor = UIColor.gray.cgColor
            self.tableViewFilter.layer.cornerRadius = 2.0
        }
    }
    @IBOutlet weak var constraintFilterTableLeading: NSLayoutConstraint!
    @IBOutlet weak var constraintFilterTableHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintScrollContentHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintScrollTop: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var viewDatePicker: UIView!
    @IBOutlet weak var buttonDateSubmit: UIButton!{
        didSet{
            self.buttonDateSubmit.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateCancel: UIButton!{
        didSet{
            self.buttonDateCancel.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDateOne: UILabel!{
        didSet {
            labelDateOne.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDateTwo: UILabel!{
        didSet {
            labelDateOne.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateOne: UIButton!{
        didSet {
            buttonDateOne.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateTwo: UIButton!{
        didSet {
            buttonDateTwo.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblQR: UILabel! {
        didSet {
            self.lblQR.layer.borderColor = UIColor.white.cgColor
        }
    }
    @IBOutlet weak var imageViewQR: UIImageView!
    @IBOutlet weak var viewToShareAllRecords: UIView!
    
    //For search functions
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var textFieldSearch: UITextField!{
        didSet {
            textFieldSearch.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonCancel: UIButton!{
        didSet {
            buttonCancel.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonClear: UIButton!{
        didSet {
            buttonClear.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    @IBOutlet weak var scannedQRView: UIView!
    
    //For filter functions
    @IBOutlet weak var buttonTransactionType: ButtonWithImage!{
        didSet {
            buttonTransactionType.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonAmountRange: ButtonWithImage!{
        didSet {
            buttonAmountRange.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonGender: ButtonWithImage!{
        didSet {
            buttonGender.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonAge: ButtonWithImage!{
        didSet {
            buttonAge.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonPeriod: ButtonWithImage!{
        didSet {
            buttonPeriod.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    //For sorting functions
    @IBOutlet weak var buttonUser: ButtonWithImage!{
        didSet {
            buttonUser.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonTransactionID: ButtonWithImage!{
        didSet {
            buttonTransactionID.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonAmount: ButtonWithImage!{
        didSet {
            buttonAmount.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonMobileNumber: ButtonWithImage!{
        didSet {
            buttonMobileNumber.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonBonus: ButtonWithImage!{
        didSet {
            buttonBonus.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonCashBack: ButtonWithImage!{
        didSet {
            buttonCashBack.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonBalance: ButtonWithImage!{
        didSet {
            buttonBalance.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDate: ButtonWithImage!{
        didSet {
            buttonDate.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonBusinessName: ButtonWithImage!{
        didSet {
            buttonBusinessName.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonAccountType: ButtonWithImage!{
        didSet {
            buttonAccountType.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonPaymentType: ButtonWithImage!{
        didSet {
            buttonPaymentType.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    // MARK: - Properties
    var initialRecords = [ReportTransactionRecord]()
    var transactionRecords = [ReportTransactionRecord]()
    
    var addMore = 0
    
    let dateFormatter = DateFormatter()
    let cellTransactionCell = "TransactionDetailCell"
    let expandedTransactionTypes = [FilterType(rootFilter: .transactionType, filterTitle: "All Transaction"),
                                            FilterType(rootFilter: .transactionType, filterTitle: "Cash Back"),
                                            FilterType(rootFilter: .transactionType, filterTitle: "Bonus Points"),
                                            FilterType(rootFilter: .transactionType, filterTitle: "Remarks"),
                                            FilterType(rootFilter: .transactionType, filterTitle: "Refunds", hasChild: true),
                                            FilterType(rootFilter: .transactionType, filterTitle: "Data Plan", parentType: .refunds),
                                            FilterType(rootFilter: .transactionType, filterTitle: "Top-Up Plan", parentType: .refunds),
                                            FilterType(rootFilter: .transactionType, filterTitle: "Special Offer", parentType: .refunds),
                                            FilterType(rootFilter: .transactionType, filterTitle: "Payment", hasChild: true),
                                            FilterType(rootFilter: .transactionType, filterTitle: "Received Amount", parentType: .payment),
                                            FilterType(rootFilter: .transactionType, filterTitle: "Pay / Send", parentType: .payment),
                                            FilterType(rootFilter: .transactionType, filterTitle: "Hide My Number to pay", parentType: .payment),
                                            FilterType(rootFilter: .transactionType, filterTitle: "Received Amount no number", parentType: .payment),
                                            FilterType(rootFilter: .transactionType, filterTitle: "Request Money", hasChild: true),
                                            FilterType(rootFilter: .transactionType, filterTitle: "Sent Request Money", parentType: .requestMoney),
                                            FilterType(rootFilter: .transactionType, filterTitle: "Received Request Money", parentType: .requestMoney),
                                            FilterType(rootFilter: .transactionType, filterTitle: "Offers", hasChild: true),
                                            FilterType(rootFilter: .transactionType, filterTitle: "All Offers", parentType: .offers),
                                            FilterType(rootFilter: .transactionType, filterTitle: "Direct Offers", parentType: .offers),
                                            FilterType(rootFilter: .transactionType, filterTitle: "InDirect Offers", parentType: .offers),
                                            FilterType(rootFilter: .transactionType, filterTitle: "Account Type", hasChild: true),
                                            FilterType(rootFilter: .transactionType, filterTitle: "All", parentType: .accountType),
                                            FilterType(rootFilter: .transactionType, filterTitle: "Personal", parentType: .accountType),
                                            FilterType(rootFilter: .transactionType, filterTitle: "Merchant", parentType: .accountType),
                                            FilterType(rootFilter: .transactionType, filterTitle: "Agent", parentType: .accountType),
                                            FilterType(rootFilter: .transactionType, filterTitle: "Advance Merchant", parentType: .accountType),
                                            FilterType(rootFilter: .transactionType, filterTitle: "Safety Cashier", parentType: .accountType)
    ]
    let amountRanges = [FilterType(rootFilter: .amount, filterTitle: "Amount"), FilterType(rootFilter: .amount, filterTitle: "All"), FilterType(rootFilter: .amount, filterTitle: "0 - 1,000"),
                                FilterType(rootFilter: .amount, filterTitle: "1,001 - 10,000"), FilterType(rootFilter: .amount, filterTitle: "10,001 - 50,000"),
                                FilterType(rootFilter: .amount, filterTitle: "50,001 - 1,00,000"), FilterType(rootFilter: .amount, filterTitle: "1,00,001 - 2,00,000"),
                                FilterType(rootFilter: .amount, filterTitle: "2,00,001 - 5,00,000"), FilterType(rootFilter: .amount, filterTitle: "5,00,001 - Above")]
    
    let genderTypes = [FilterType(rootFilter: .gender, filterTitle: "Gender"), FilterType(rootFilter: .gender, filterTitle: "All"), FilterType(rootFilter: .gender, filterTitle: "Male"),
                               FilterType(rootFilter: .gender, filterTitle: "Female")]
    let ageRanges = [FilterType(rootFilter: .age, filterTitle: "Age"), FilterType(rootFilter: .age, filterTitle: "All"), FilterType(rootFilter: .age, filterTitle: "0 - 19"),
                             FilterType(rootFilter: .age, filterTitle: "20 - 34"), FilterType(rootFilter: .age, filterTitle: "35 - 50"),
                             FilterType(rootFilter: .age, filterTitle: "51 - 64"), FilterType(rootFilter: .age, filterTitle: "65 - Above")]
    let periodTypes = [FilterType(rootFilter: .period, filterTitle: "Period"), FilterType(rootFilter: .period, filterTitle: "All"), FilterType(rootFilter: .period, filterTitle: "Date"), FilterType(rootFilter: .period, filterTitle: "Month")]
    var filterArray = [FilterType]()
    var lastSelectedFilter = RootFilter.transactionType
    var currentBalance: NSDecimalNumber = 0
    var isSearchViewShow: Bool {
        get {
            return !viewSearch.isHidden
        }
        set(newValue) {
            if newValue {
                viewSearch.isHidden = false
                textFieldSearch.becomeFirstResponder()
                constraintScrollTop.constant = 0
                self.view.layoutIfNeeded()
            } else {
                textFieldSearch.text = ""
                viewSearch.isHidden = true
                constraintScrollTop.constant = -40
                self.view.layoutIfNeeded()
                self.view.endEditing(true)
            }
        }
    }
    var isDateViewHidden: Bool {
        get {
            return viewDatePicker.isHidden
        }
        set(newValue) {
            if newValue {
                viewShadow.isHidden = true
                viewDatePicker.isHidden = true
            } else {
                viewShadow.isHidden = false
                viewDatePicker.isHidden = false
            }
        }
    }
    enum ParentType: String {
        case main = "Main", refunds = "Refunds", payment = "Payment", requestMoney = "Request Money", offers = "Offers", accountType = "Account Type"
    }
    enum RootFilter {
        case transactionType, amount, gender, age, period
    }
    struct FilterType {
        var rootFilter: RootFilter
        var filterTitle: String
        var parentType: ParentType
        var isSubShowing: Bool
        var hasChild: Bool
        init(rootFilter: RootFilter, filterTitle: String, parentType: ParentType = .main, isSubShowing: Bool = false, hasChild: Bool = false) {
            self.rootFilter = rootFilter
            self.filterTitle = filterTitle
            self.parentType = parentType
            self.isSubShowing = isSubShowing
            self.hasChild = hasChild
        }
    }
    private let refreshControl = UIRefreshControl()
    var modelReport: ReportModel!
    // MARK: - View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        modelReport = ReportModel()
        modelReport.getALLTransactionInfo(first: "\(0)", last: "\(20)")
        modelReport.reportModelDelegate = self
        tableViewTransaction.tag = 0
        tableViewFilter.tag = 1
        tableViewTransaction.tableFooterView = UIView()
        tableViewFilter.tableFooterView = UIView()
        tableViewTransaction.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshTableData), for: .valueChanged)
        buttonCancel.layer.cornerRadius = 15
        buttonClear.layer.cornerRadius = 15
        buttonCancel.addTarget(self, action: #selector(hideSearchView), for: .touchUpInside)
        buttonClear.addTarget(self, action: #selector(clearSerchTextField), for: .touchUpInside)
        
        buttonDateSubmit.layer.cornerRadius = 15
        buttonDateCancel.layer.cornerRadius = 15
        buttonDateSubmit.addTarget(self, action: #selector(filterSelectedDate), for: .touchUpInside)
        buttonDateCancel.addTarget(self, action: #selector(hideDateSelectionView), for: .touchUpInside)
        textFieldSearch.placeholder = "Search By Transaction ID or Amount or Mobile Number or Remarks".localized
        buttonCancel.setTitle("Cancel".localized, for: .normal)
        buttonClear.setTitle("Clear".localized, for: .normal)
        buttonDateSubmit.setTitle("Submit".localized, for: .normal)
        buttonDateCancel.setTitle("Cancel".localized, for: .normal)
        labelDateOne.text = "From Date".localized
        labelDateTwo.text = "To Date".localized
        
        let cellFilterNibName = UINib(nibName: ConstantsXibCell.filterTableViewCell, bundle: nil)
        tableViewFilter.register(cellFilterNibName, forCellReuseIdentifier: ConstantsXibCell.filterTableViewCell)
        datePicker.calendar = Calendar.current
      // fetchRecordsFromDB()
        dateFormatter.calendar = Calendar.current
        dateFormatter.dateFormat = "dd MMM yyyy"
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
                   } else {
                       // Fallback on earlier versions
                   }
        self.setInitialValues()
        AppDelegate.AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)
        setUpNavigation()
        
    }
    
    
    override func viewDidLayoutSubviews() {
        let navBarHeight = navigationController?.navigationBar.frame.size.height ?? 44
        let heightToReduce = navBarHeight == 32 ? 56 : 44
        constraintScrollContentHeight.constant = scrollView.frame.size.height - CGFloat(heightToReduce) + 44
        self.view.layoutIfNeeded()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        if appDelegate.checkNetworkAvail(){}
        else {
            fetchRecordsFromDB()
        }
    }
    
    
    
    // MARK: - Target Methods -
    @objc func showSearchView() {
        tableViewFilter.isHidden = true
        if !isSearchViewShow {
            removeGestureFromShadowView()
            viewShadow.isHidden = true
            /// For qr
            imageViewQR.isHidden = true
            lblQR.isHidden = true
            imageViewQR.image = nil
            /// For share all transaction
            viewToShareAllRecords.isHidden = true
            if transactionRecords.count > 0 {
                isSearchViewShow = true
                textFieldSearch.becomeFirstResponder()
            } else {
                /*
                let alert = UIAlertController(title: "Alert", message: "No Records", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                 */
                alertViewObj.wrapAlert(title: "Alert".localized, body: "No Records".localized, img: UIImage(named : "AppIcon"))
                alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                    
                })
                alertViewObj.showAlert(controller: self)
                
            }
        } else {
            self.hideSearchView()
//            textFieldSearch.becomeFirstResponder()
        }
    }
    
    func noRecordAlertAction() {
        /*
        let alert = UIAlertController(title: "Alert", message: "No Records", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        */
        alertViewObj.wrapAlert(title: "Alert".localized, body: "No Records".localized, img: UIImage(named : "AppIcon"))
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
        })
        alertViewObj.showAlert(controller: self)
    }
    
    @objc func showShareOption() {
        if viewToShareAllRecords.isHidden {
            if transactionRecords.count > 0 {
                let tapGestToRemoveView = UITapGestureRecognizer(target: self, action: #selector(shadowIsTapped))
                viewShadow.addGestureRecognizer(tapGestToRemoveView)
                viewShadow.isHidden = false
                viewToShareAllRecords.isHidden = false
            } else {
                viewShadow.isHidden = true
                noRecordAlertAction()
            }
        }
    }
    
    @objc func showGraph() {
        guard let statisticsTransactionController = UIStoryboard(name: "Reports", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.statisticsTransactionController.nameAndID) as? StatisticsTransactionAndBillController else { return }
        statisticsTransactionController.screenType = .transactionDetail
        statisticsTransactionController.initialRecords = initialRecords
        self.navigationController?.pushViewController(statisticsTransactionController, animated: true)
    }
    
    @objc func hideSearchView() {
        self.view.endEditing(true)
        isSearchViewShow = false
        applyFilter()
    }
    
    @objc func clearSerchTextField() {
        textFieldSearch.text = ""
        applyFilter()
    }
    
    @objc func filterSelectedDate() {
        if let _ = buttonDateOne.title(for: .normal) {
            buttonPeriod.setTitle("Date".localized, for: .normal)
        } else {
            buttonPeriod.setTitle("Month".localized, for: .normal)
        }
        applyFilter()
        isDateViewHidden = true
    }
    
    @objc func hideDateSelectionView() {
        isDateViewHidden = true
    }
    
    @objc func fromDatePickerChanged(datePicker:UIDatePicker) {
        datePicker.calendar = Calendar.current
        buttonDateOne.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
       // buttonDateTwo.setTitle(datePicker.date.stringValue(dateFormatIs: "dd MMM yyyy"), for: .normal)
    }
    
    @objc func toDatePickerChanged(datePicker:UIDatePicker) {
        datePicker.calendar = Calendar.current
        if let startDateStr = buttonDateOne.title(for: .normal), let startDate = dateFormatter.date(from: startDateStr) {
            switch datePicker.date.compare(startDate) {
            case .orderedAscending:
                if let endDateStr = buttonDateTwo.title(for: .normal), let endDate = dateFormatter.date(from: endDateStr) {
                    datePicker.date = endDate
                }
            case .orderedDescending, .orderedSame:
                buttonDateTwo.setTitle(dateFormatter.string(from: datePicker.date), for: .normal)
            }
        } else {
            buttonDateTwo.setTitle(dateFormatter.string(from: datePicker.date), for: .normal)
        }
    }
    
    @objc func monthPickerChanged(datePicker:UIDatePicker) {
        datePicker.calendar = Calendar.current
        buttonDateTwo.setTitle(dateFormatter.string(from: datePicker.date), for: .normal)
    }
    
    @objc func shadowIsTapped(sender: UITapGestureRecognizer) {
        removeGestureFromShadowView()
        scannedQRView.isHidden = true
        viewShadow.isHidden = true
        /// For qr
        imageViewQR.isHidden = true
        lblQR.isHidden = true
        imageViewQR.image = nil
        /// For share all transaction
        viewToShareAllRecords.isHidden = true
    }
    
    @objc private func refreshTableData(_ sender: Any) {
//        if viewSearch.isHidden {
        self.filterArray = []
        if appDelegate.checkNetworkAvail() {
               self.hideSearchView()
               modelReport.getTransactionInfo()
           }
        else {
            fetchRecordsFromDB()
        }
//        } else {
//            self.refreshControl.endRefreshing()
//        }
    }
    
    // MARK: - Button Action Methods
    @IBAction func shareRecordInPdf(_ sender: UIButton) {
        guard let transactionCell = sender.superview?.superview?.superview as? TransactionDetailCell else { return }
        guard let indexPath = tableViewTransaction.indexPath(for: transactionCell) else { return }
        var pdfDictionary = [String: Any]()
        pdfDictionary["logoName"] = "appIcon_Ok"
        pdfDictionary["invoiceTitle"] = "Transaction Details Receipt".localized
        let senderNameArray = transactionRecords[indexPath.row].senderName?.components(separatedBy: "-")
        pdfDictionary["senderAccName"] = ""
        if let name = senderNameArray?.first, name.count > 0 {
            let value = senderNameArray?.first ?? ""
            if value.contains("@sender"){
                let newValue = value.components(separatedBy: "@sender")
                if newValue.indices.contains(1){
                    let name = newValue[1].components(separatedBy: "@businessname")
                    if name.indices.contains(0){
                        pdfDictionary["senderAccName"] = name[0]
                    }
                }
            }else{
              pdfDictionary["senderAccName"] = name
            }
        } else {
            if let comnts = transactionRecords[indexPath.row].desc, comnts.lowercased().contains(find: "solar") {
                pdfDictionary["senderAccName"] = UserModel.shared.name
            }
        }
        pdfDictionary["receiverAccName"] = transactionRecords[indexPath.row].receiverName
        
        if let trType = transactionRecords[indexPath.row].accTransType {
            if trType == "Dr" {
                if let businessName = transactionRecords[indexPath.row].receiverBName, businessName.count > 0 {
                    pdfDictionary["businessName"] = businessName
                }
            } else {
                if let businessName = transactionRecords[indexPath.row].senderBName, businessName.count > 0 {
                    pdfDictionary["businessName"] = businessName
                }
            }
        }
        var tType = ""
        var pType: String? = nil
        if let desc = transactionRecords[indexPath.row].rawDesc {
            if desc.contains("#OK-") || desc.contains("OOK") {
                if desc.contains("Top-Up") || desc.contains("TopUp") || desc.contains("Bonus Point Top Up") || desc.contains("TopUpPlan"){
                    tType = "Top-Up"
                } else if desc.contains("Data Plan") || desc.contains("DataPlan") {
                    tType = "Data Plan"
                } else if desc.contains("ELECTRICITY") {
                    tType = "ELECTRICITY"
                } else if desc.contains("MPU") {
                    tType = "MPU"
                } else if desc.contains("Visa Card") {
                    tType = "VISA"
                } else if desc.contains("meter refund") {
                    tType = "REFUND"
                } else {
                    tType = "Special Offers"
                }
            } else {
                if let transType = transactionRecords[indexPath.row].transType {
                    tType = transType
                    if transType == "PAYWITHOUT ID" {
                        if let accTransType = transactionRecords[indexPath.row].accTransType {
//                            if accTransType == "Cr" {
//                                tType = "XXXXXX"
//                            } else {
                                tType = "Hide My Number"
                            //}
                        }
                        if desc.contains(find: "#Tax Code") {
                            tType = "Tax Payment"
                        }
                    } else if transType == "TOLL" {
                        tType = "TOLL"
                    } else if transType == "TICKET" {
                        tType = "TICKET"
                    } else {
                        tType = "PAYTO"
                        if desc.contains(find: "-cashin") {
                            pType = "Cash In"
                        } else if desc.contains(find: "-cashout") {
                            pType = "Cash Out"
                        } else if desc.contains(find: "-transferto") {
                            pType = "Transfer To"
                        }
                    }
                }
            }
        }
        pdfDictionary["transactionType"] = tType
        pdfDictionary["paymentType"] = pType
        if let accTransType = transactionRecords[indexPath.row].accTransType {
                if accTransType == "Cr" {
                    var sNumber = ""
                    var rNumber = UserModel.shared.mobileNo
                    if let des = transactionRecords[indexPath.row].destination {
                        sNumber = des
                        if des.hasPrefix("0095") {
                            sNumber = "(+95)0\(des.substring(from: 4))"
                        }
                    }
                    if rNumber.hasPrefix("0095") {
                        rNumber = "(+95)0\(rNumber.substring(from: 4))"
                    }
                    pdfDictionary["senderAccNo"] = sNumber
                    pdfDictionary["receiverAccNo"] = rNumber
                    if let transType = transactionRecords[indexPath.row].transType {
                        if transType == "PAYWITHOUT ID" {
                            pdfDictionary["senderAccNo"] = "XXXXXXXXXX"
                        }
                    }
                    if let desc = transactionRecords[indexPath.row].rawDesc, desc.contains("meter refund") {
                        pdfDictionary["senderAccNo"] = "XXXXXXXXXX"
                    }
                    if pdfDictionary["receiverAccName"] == nil {
                        pdfDictionary["receiverAccName"] = UserModel.shared.name
                    }
                    if  transactionRecords[indexPath.row].rawDesc?.contains(find: "Bank To Wallet") ?? false {
                       sNumber = "XXXXXXXXXX"
                        pdfDictionary["senderAccNo"] = "XXXXXXXXXX"
                   }
                } else {
                var sNumber = UserModel.shared.mobileNo
                var rNumber = ""
                    if let des = transactionRecords[indexPath.row].destination {
                        rNumber = des
                        if des.hasPrefix("0095") {
                            rNumber = "(+95)0\(des.substring(from: 4))"
                        } else {
                            if let cCode = transactionRecords[indexPath.row].countryCode, cCode.count > 0 {
                                rNumber = "(\(cCode))\(rNumber)"
                            }
                        }
                    }
                if sNumber.hasPrefix("0095") {
                    sNumber = "(+95)0\(sNumber.substring(from: 4))"
                }
                    //If operator number avaibale
                    if transactionRecords[indexPath.row].operatorName?.count ?? 0 > 2 {
                        if let mobileNumber = transactionRecords[indexPath.row].operatorName, mobileNumber.hasPrefix("0095") {
                           //rNumber = mobileNumber.replacingOccurrences(of: "0095", with: "(+95)0")
                            rNumber = CodeSnippets.getMobileNumber(transRecord: transactionRecords[indexPath.row])
                        } else {
                            rNumber = transactionRecords[indexPath.row].operatorName ?? ""
                        }
                    }
                    
                pdfDictionary["receiverAccNo"] = rNumber
                pdfDictionary["senderAccNo"] = sNumber
                if let sName = pdfDictionary["senderAccName"] as? String, sName.count > 0 {
                    pdfDictionary["senderAccName"] = sName
                } else {
                    pdfDictionary["senderAccName"] = UserModel.shared.name
                }
                if let des = transactionRecords[indexPath.row].rawDesc {
                    if !des.contains(find: "#point-redeempoints") {
                        if !des.contains(find: "#OK-PMC") {
                             pdfDictionary["operatorName"] = PayToValidations().getNumberRangeValidation(rNumber.replacingOccurrences(of: "(+95)", with: "")).operator

                            let desArr = des.components(separatedBy: "-")
                            if desArr.count > 3 {
                                if desArr[3] == "cashin" || desArr[3] == "cashout" {
                                    pdfDictionary["operatorName"] = PayToValidations().getNumberRangeValidation(rNumber.replacingOccurrences(of: "(+95)", with: "")).operator
                                } else if desArr.indices.contains(6){
                                    if desArr[6] == "Overseas" {
                                    pdfDictionary["operatorName"] = desArr[2]
                                    }
                                }
                                else {
                                    pdfDictionary["operatorName"] = PayToValidations().getNumberRangeValidation(rNumber.replacingOccurrences(of: "(+95)", with: "")).operator
                                }
                            }
                        }
                    } else {
                        if pdfDictionary["receiverAccName"] == nil ||  (pdfDictionary["receiverAccName"] as? String ?? "").count == 0 {
                            pdfDictionary["receiverAccName"] = "Unknown"
                        }
                    }
                    
                }
            }
        }
        pdfDictionary["transactionID"] = transactionRecords[indexPath.row].transID
        if let nsBonus = transactionRecords[indexPath.row].bonus, nsBonus > 0 {
            pdfDictionary["bonusPoint"] = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: nsBonus.toString()) + " PTS"
        }
        if let transDate = transactionRecords[indexPath.row].transactionDate {
            pdfDictionary["transactionDate"] = transDate.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss")
        }
        
        if transactionRecords[indexPath.row].desc?.contains(find: "ProjectId") ?? false {
            var mobileNo = "XXXXXXXXXX"
            let value = transactionRecords[indexPath.row].desc ?? ""
            if value.contains(find: "ProjectId"){
                let newValue = value.components(separatedBy: " Wallet Number :")
                if newValue.count > 2 {
                    if newValue.indices.contains(1){
                        let name = newValue[2].components(separatedBy: "ProjectId")
                        if name.indices.contains(0){
                            mobileNo = "(+95)".appending(name[0])
                        }
                    }
                }  else {
                    let arrVal = value.components(separatedBy: "Account Number :")
                    if arrVal.indices.contains(1){
                        let name = arrVal[1].components(separatedBy: "ProjectId")
                        if name.indices.contains(0){
                            mobileNo = "(+95)".appending(name[0])
                        }
                    }
                }
            }
            pdfDictionary["receiverAccNo"] = mobileNo
            pdfDictionary["remarks"] = transactionRecords[indexPath.row].desc?.components(separatedBy: "ProjectId").first ?? ""
        } else if transactionRecords[indexPath.row].desc?.contains(find: "#Tax Code") ?? false {
            pdfDictionary["receiverAccNo"] = "XXXXXXXXXX"
            let tmpDesc = transactionRecords[indexPath.row].desc?.replacingOccurrences(of: "#", with: "") ?? ""
            pdfDictionary["remarks"] = tmpDesc.replacingOccurrences(of: "Tax Code :", with: "Tax Payment - IRD Transaction ID :")
        }
        else if transactionRecords[indexPath.row].desc?.contains(find: "Bonus Point Exchange Payment") ?? false {
            pdfDictionary["receiverAccNo"] = "XXXXXXXXXX"
       }
        else if transactionRecords[indexPath.row].desc?.contains(find: "ELECTRICITY") ?? false {
            pdfDictionary["remarks"] = transactionRecords[indexPath.row].desc?.replacingOccurrences(of: ",", with: "")
            let displayNumber = UitilityClass.showMobileNumberFromDesc(desc: transactionRecords[indexPath.row].desc)
            if displayNumber.sourceNumber == false {
                pdfDictionary["senderAccNo"] = "XXXXXXXXXX"
            }
            if displayNumber.destinationNumber == false {
                pdfDictionary["receiverAccNo"] = "XXXXXXXXXX"
            }
        }
        else if transactionRecords[indexPath.row].desc?.contains(find: "Overseas") ?? false {
           if transactionRecords[indexPath.row].desc?.contains(find: "TopUpPlan") ?? false {
            pdfDictionary["remarks"] = "Remarks: TopUp Plan International"
           } else {
            pdfDictionary["remarks"] = "Remarks: Data Plan International"
           }
       }
        else {
                   pdfDictionary["remarks"] = transactionRecords[indexPath.row].desc
               }
     
        if let transAmount = transactionRecords[indexPath.row].amount {
            pdfDictionary["amount"] = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: transAmount.toString()) + " MMK"
        }
        if let comnt = transactionRecords[indexPath.row].rawDesc, comnt.contains("#point-redeempoints-") {
            pdfDictionary["qrImage"] = nil
        } else {
            if let transType = transactionRecords[indexPath.row].accTransType, transType == "Dr" {
                pdfDictionary["qrImage"] = getQRCodeImage(qrData: transactionRecords[indexPath.row])
            }
        }
        if let promoRawDesc = transactionRecords[indexPath.row].rawDesc, promoRawDesc.contains("#OK-PMC") {
            pdfDictionary["isForPromocode"] = true
            var helper: OfferCommentHelper! = OfferCommentHelper(comment: promoRawDesc.removingPercentEncoding)
            
            if helper.promotionTransactionCount == 1 && helper.paymentType == "1" {
                pdfDictionary["promoReceiverNumber"] = transactionRecords[indexPath.row].destination?.replacingOccurrences(of: "0095", with: "(+95)0")
                if let transType = transactionRecords[indexPath.row].accTransType, transType == "Dr" {
                    //Show tx done from user login
                    pdfDictionary["senderAccNo"] = UserModel.shared.mobileNo.replacingOccurrences(of: "0095", with: "(+95)0")
                    pdfDictionary["receiverAccNo"] = helper.promoReceiverNumber
                } else {
                    pdfDictionary["senderAccNo"] = helper.senderCreditNumber
                    pdfDictionary["receiverAccNo"] = UserModel.shared.mobileNo.replacingOccurrences(of: "0095", with: "(+95)0")
                }
            } else {
                if let transType = transactionRecords[indexPath.row].accTransType, transType == "Dr" {
                    //Show tx done from user login
                    pdfDictionary["senderAccNo"] = UserModel.shared.mobileNo.replacingOccurrences(of: "0095", with: "(+95)0")
                    if let dest = helper.promoReceiverNumber {
                        pdfDictionary["receiverAccNo"] = "XXXXXX" + String(dest.suffix(4))
                    }
                    
                } else {
                    if let dest = helper.senderCreditNumber {
                        pdfDictionary["senderAccNo"] = "XXXXXX" + String(dest.suffix(4))
                    }
                    pdfDictionary["receiverAccNo"] = UserModel.shared.mobileNo.replacingOccurrences(of: "0095", with: "(+95)0")
                }
                
            }
            pdfDictionary["promocode"] = helper.promoCode
            if let discPercent = helper.discountPercent {
                pdfDictionary["discountPercent"] = discPercent + " %"
            }
            if let discAmount = helper.discountAmount {
                let amont = wrapAmountWithCommaDecimal(key: "\(discAmount)")
                pdfDictionary["discountAmount"] = amont + " MMK"
            }
            if let billAmount = helper.billAmount {
                pdfDictionary["billAmount"] = billAmount + " MMK"
            }
            pdfDictionary["companyName"] = helper.companyName
            pdfDictionary["productName"] = helper.productName
            pdfDictionary["buyingQuantity"] = helper.buyingQuantity
            pdfDictionary["freeQuantity"] = helper.freeQuantity
            helper = nil
        }
        guard let pdfUrl = CodeSnippets.createInvoicePDF(dictValues: pdfDictionary, pdfFile: "OK$ Transaction Receipt") else {
            println_debug("Error - pdf not generated")
            return
        }
        let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
    
    @IBAction func shareRecordByQRCode(_ sender: UIButton) {
        self.view.endEditing(true)
        guard let transactionCell = sender.superview?.superview?.superview as? TransactionDetailCell else { return }
        guard let indexPath = tableViewTransaction.indexPath(for: transactionCell) else { return }
        if let qrImage = getQRCodeImage(qrData: transactionRecords[indexPath.row]) {
            let tapGestToRemoveView = UITapGestureRecognizer(target: self, action: #selector(shadowIsTapped))
            viewShadow.addGestureRecognizer(tapGestToRemoveView)
            imageViewQR.image = qrImage
            viewShadow.isHidden = false
            imageViewQR.isHidden = false
            lblQR.isHidden = false
            scannedQRView.isHidden = false
        }
    }
    
    @IBAction func shareAllTransactionsInPdf(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.viewShadow.isHidden = true
            self.viewToShareAllRecords.isHidden = true
            self.removeGestureFromShadowView()
            PTLoader.shared.show()
        }
        if transactionRecords.count == 0 {
            PTLoader.shared.hide()
            return
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            self.generateAllPdfRecords { (isSuccess, pdfView) in
                if isSuccess {
                    guard let pdfVew = pdfView else {
                        PTLoader.shared.hide()
                        return
                    }
                    guard let pdfUrl = CodeSnippets.generatePDF(viewForPDF: pdfVew, pdfFile: "OK$ Transaction Report") else {
                        PTLoader.shared.hide()
                        println_debug("Error - pdf not generated")
                        return
                    }
                    let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
                    DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
                    PTLoader.shared.hide()
                    
                } else {
                    PTLoader.shared.hide()
                }
            }
        })
    }
    
    func formatDate(dateString: String) -> (String, String) {
        let dateStr = dateString.components(separatedBy: "Due Date :")
        if dateStr.count > 0 {
            let currentDate = dateStr[1].replacingOccurrences(of: "#", with: "")
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy hh:mm:ss a"
            let date = formatter.date(from: currentDate)
            let previousMonth = Calendar.current.date(byAdding: .month, value: -1, to: date ?? Date())
            formatter.dateFormat = "EEE, dd-MMM-yyyy"
            let strDate = formatter.string(from: date ?? Date())
            formatter.dateFormat = "MMMM"
            let prevMonth = formatter.string(from: previousMonth ?? Date())
            return(prevMonth, strDate)
        }
        return ("", "")
    }
    
    @IBAction func shareAllTransactionInExcel(_ sender: UIButton) {
        viewShadow.isHidden = true
        viewToShareAllRecords.isHidden = true
        removeGestureFromShadowView()
        
        if transactionRecords.count == 0 {
            return
        }
        var excelTxt = "Transaction ID,Transaction Amount (MMK),Account Holder Number, Source Number, Destination Number, Status, Business Name,Account Type,Payment Type, Opening Balance (MMK),Bonus / Loyalty Points,Cash Back (MMK),Credit (MMK),Debit (MMK),Closing Balance (MMK),Date&Time,Remarks\n"
        var totalTransAmount: NSDecimalNumber = 0
        var bonusTransAmount: NSDecimalNumber = 0
        var cashBackAmt: NSDecimalNumber = 0
        var creditTransAmount: NSDecimalNumber = 0
        var debitTransAmount: NSDecimalNumber = 0
        
        for record in transactionRecords {
            var isAmountGTZ = false
            var recText = record.transID ?? ""
            var phNo = ""
            var cashBack = "0"
            if record.accTransType == "Dr" {
                phNo = CodeSnippets.getMobileNumber(transRecord: record)
            } else {
                if record.transType == "PAYWITHOUT ID" || record.transType == "TICKET" || record.transType == "TOLL" {
                    phNo = "XXXXXXXXXX"
                } else {
                    phNo = CodeSnippets.getMobileNumber(transRecord: record)
                }
            }
            if let transAmount = record.amount {
                totalTransAmount = totalTransAmount.adding(transAmount)
                if transAmount.compare(0.0) == .orderedDescending {
                    isAmountGTZ = true
                }
            }
            if let cashBackAmount = record.cashBack {
                cashBackAmt = cashBackAmt.adding(cashBackAmount)
                if cashBackAmount.compare(0.0) == .orderedDescending {
                    cashBack = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: cashBackAmount.toString()).replacingOccurrences(of: ",", with: "")
                    if !isAmountGTZ {
                        phNo = record.countryCode ?? "(+95)"
                    }
                }
            }
            var payType = "PAYTO"
            if let desc = record.rawDesc {
                if desc.contains("#OK-") || desc.contains("OOK") {
                    if desc.contains("Top-Up") || desc.contains("TopUp") || desc.contains("Bonus Point Top Up") || desc.contains("TopUpPlan"){
                        payType = "Top-Up"
                    } else if desc.contains("Data Plan") || desc.contains("DataPlan"){
                        payType = "Data Plan"
                    } else if desc.contains("ELECTRICITY") {
                        payType = "ELECTRICITY"
                    } else if desc.contains("REFUND") {
                        payType = "REFUND"
                    }  else if record.desc?.contains(find: "POSTPAID") ?? false {
                        payType = "POSTPAID"
                    } else if record.desc?.contains(find: "MPU") ?? false {
                        payType = "MPU"
                    } else if record.desc?.contains(find: "Visa") ?? false {
                        payType = "VISA"
                    } else if record.desc?.contains(find: "LANDLINE") ?? false {
                        payType = "LANDLINE"
                    } else if record.desc?.contains(find: "SUNKING") ?? false {
                        payType = "SUNKING"
                    } else if record.desc?.contains(find: "INSURANCE") ?? false {
                        payType = "INSURANCE"
                    } else if record.desc?.contains(find: "MERCHANTPAY") ?? false {
                        payType = "MERCHANTPAY"
                    } else if record.desc?.contains(find: "PAYMENTTYPE:GiftCard") ?? false {
                        payType = "GIFTCARD"
                    } else if desc.contains("MPU Card") {
                        payType = "MPU"
                    } else if desc.contains("DTH") {
                        payType = "DTH"
                    } else if desc.contains("Visa Card") {
                        payType = "VISA"
                    } else if desc.contains("meter refund") {
                        payType = "REFUND"
                    } else {
                        payType = "Special Offers"
                    }
                } else {
                    if let transType = record.transType {
                        if transType == "PAYTO" {
                            if desc.contains(find: "-cashin") {
                                payType = "Cash In"
                            } else if desc.contains(find: "-cashout") {
                                payType = "Cash Out"
                            } else if desc.contains(find: "-transferto") {
                                payType = "Transfer To"
                            }
                            else {
                                payType = "PAYTO"
                            }
                        } else if desc.contains(find: "ProjectId") {
                            payType = "Hide My Number"
                        } else if desc.contains(find: "Wallet Bank TransactionId") {
                            payType = "Hide My Number"
                        }
                    }
                }
            }
            
            var descWithNoComa = ""
            
            let desc = record.rawDesc?.removingPercentEncoding?.replacingOccurrences(of: "+", with: " ").components(separatedBy: "-")
            if record.rawDesc?.contains(find: "ELECTRICITY") ?? false {
                payType = "ELECTRICITY"
                let dateFormat = self.formatDate(dateString: desc?.last ?? "")
                var name: [String]?
                if desc?[9].contains(find: "Name :") ?? false {
                    name = desc?[9].components(separatedBy: "Name :")
                } else if desc?[10].contains(find: "Name :") ?? false {
                    name = desc?[10].components(separatedBy: "Name :")
                }
                var diviLoc = ""
                if desc?[7].contains(find: "Division") ?? false {
                    let division = desc?[7].components(separatedBy: "Division :")
                    if let divLocal = division?[1] {
                        let arr = divLocal.components(separatedBy: "Division")
                        diviLoc = arr[0]
                    }
                } else if desc?[8].contains(find: "Division") ?? false {
                    let division = desc?[8].components(separatedBy: "Division :")
                    if let divLocal = division?[1] {
                        let arr = divLocal.components(separatedBy: "Division")
                        diviLoc = arr[0]
                    }
                }
                
                var township: [String]?
                var meterNo = ""
                if desc?[8].contains(find: "Township") ?? false {
                    township = desc?[8].components(separatedBy: "Township")
                } else if desc?[9].contains(find: "Township") ?? false {
                    township = desc?[9].components(separatedBy: "Township")
                }
                var payFor: [String]?
                if desc?[6].contains(find: "PayFor:") ?? false {
                    meterNo = desc?[2] ?? ""
                    payFor = desc?[6].components(separatedBy: "PayFor:")
                } else if desc?[7].contains(find: "PayFor:") ?? false {
                    meterNo = "\(desc?[2] ?? "")-\(desc?[3] ?? "")"
                    payFor = desc?[7].components(separatedBy: "PayFor:")
                }
                let fullDesc = "\(meterNo) \(name?[1] ?? "") \(township?[1] ?? "") Township \(diviLoc) \(diviLoc) Division Bill Month: \(dateFormat.0) Due Date \(dateFormat.1) \(payFor?[1] ?? "")"
                descWithNoComa = fullDesc.replacingOccurrences(of: ",", with: "")
            } else if record.rawDesc?.contains(find: "DTH") ?? false {
                descWithNoComa = record.rawDesc?.components(separatedBy: "Destination").first ?? ""
                descWithNoComa = descWithNoComa.replacingOccurrences(of: ",", with: "")
            }  else if let desc = record.rawDesc, desc.contains(find: "SEND MONEY TO BANK:") {
                if let array = record.rawDesc?.components(separatedBy: "SEND MONEY TO BANK:"), array.count > 0 {
                    descWithNoComa = "SEND MONEY TO BANK: \(array[1])"
                }
            }  else if let desc = record.rawDesc, desc.contains(find: "ProjectId") {
                if let array = record.rawDesc?.components(separatedBy: "ProjectId"), array.count > 0 {
                    descWithNoComa = "Remarks: \(array[0])"
                }
            } else {
                descWithNoComa = record.desc?.replacingOccurrences(of: ",", with: "") ?? ""
            }
            descWithNoComa = descWithNoComa.replacingOccurrences(of: "1#", with: "").replacingOccurrences(of: "2#", with: "")
            
            let userNo = CodeSnippets.getMobileNumWithBrackets(countryCode: UserModel.shared.countryCode, number: UserModel.shared.formattedNumber)
            var dest = ""
            var source = ""
            var accountType = "XXXXXXXXXX"
            var businessNm = "-"
//            let senderNameArray = record.senderName?.components(separatedBy: "-")
            if record.accTransType == "Dr" {
                if record.transType == "PAYWITHOUT ID" || record.transType == "TICKET" || record.transType == "TOLL" {
                    dest = phNo
                    source = userNo
                } else {
                    source = userNo
                    dest = phNo
                }
                if let businessName = record.receiverBName, businessName.count > 0 {
                    businessNm = businessName
                    accountType = "Merchant"
                }
//                let type = (((senderNameArray?.count ?? 0) > 1) ? senderNameArray?.last ?? "" : "").components(separatedBy: ":")
//                accountType = UitilityClass.returnAccountType(type: type.last)
            } else {
                if record.transType == "PAYWITHOUT ID" || record.transType == "TICKET" || record.transType == "TOLL" {
                    source = "XXXXXXXXXX"
                    dest = userNo
                } else {
                    source = phNo
                    dest = userNo
                }
                if let businessName = record.senderBName, businessName.count > 0 {
                    businessNm = businessName
                    accountType = "Merchant"
                }
//                let type = (((senderNameArray?.count ?? 0) > 1) ? senderNameArray?[1] ?? "" : "").components(separatedBy: ":")
//                accountType = UitilityClass.returnAccountType(type: type.last)
            }
            var creditAmount = ""
            var debitAmount = ""
            if record.accTransType == "Dr" {
                creditAmount = record.amount?.toString() ?? ""
                if let credit = record.amount {
                    creditTransAmount = creditTransAmount.adding(credit)
                }
            } else {
                debitAmount = record.amount?.toString() ?? ""
                if let debit = record.amount {
                    debitTransAmount = debitTransAmount.adding(debit)
                }
            }
            if (record.rawDesc?.contains(find: "ELECTRICITY") ?? false) || (record.rawDesc?.contains(find: "DTH") ?? false) {
                source = userNo
                dest = "XXXXXXXXXX"
            } else if record.rawDesc?.contains(find: "MPU") ?? false || record.rawDesc?.contains(find: "Visa Card") ??  false{
                source = userNo
                dest = "XXXXXXXXXX"
            } else if record.rawDesc?.contains(find: "REFUND") ?? false {
                source = "XXXXXXXXXX"
                dest = userNo
            } else if record.rawDesc?.contains(find: "POSTPAID") ?? false || record.rawDesc?.contains(find: "MERCHANTPAY") ?? false || record.rawDesc?.contains(find: "LANDLINE") ?? false || record.rawDesc?.contains(find: "SUNKING") ?? false || record.rawDesc?.contains(find: "INSURANCE") ?? false {
                source = userNo
                dest = "XXXXXXXXXX"
            } else if  record.rawDesc?.contains("#OK") ?? false {
                if record.rawDesc?.contains("Top-Up") ?? false || record.rawDesc?.contains("TopUp") ?? false || record.rawDesc?.contains("Bonus Point Top Up") ?? false || record.rawDesc?.contains("Data Plan") ?? false || record.rawDesc?.contains("DataPlan") ?? false || record.rawDesc?.contains("Special Offers") ?? false || record.rawDesc?.contains("SpecialOffers") ?? false{
                    source = userNo
                    dest = phNo//"XXXXXXXXXX"
                } else if  record.rawDesc?.contains(find: "PAYMENTTYPE:GiftCard") ?? false {
                    source = userNo
                    dest = "XXXXXXXXXX"
                } else if let desc = record.rawDesc, desc.contains(find: "meter refund") {
                    source = "XXXXXXXXXX"
                    dest = userNo
                }
            }  else if record.destination == nil, let cashback = record.cashBack, cashback > 0 {
                source = "XXXXXXXXXX"
                dest = userNo
            } else if let desc = record.rawDesc, desc.contains(find: "SEND MONEY TO BANK:") {
                source = userNo
                dest = "XXXXXXXXXX"
            } else if let desc = record.rawDesc, desc.contains(find: "Bonus Point Exchange Payment") {
                source = "XXXXXXXXXX"
                dest = userNo
            } else if let desc = record.rawDesc, desc.contains(find: "ProjectId") {
                source = userNo
                dest = "XXXXXXXXXX"
            }  else if let desc = record.rawDesc, desc.contains(find: "Bank To Wallet") {
                source = "XXXXXXXXXX"
                dest = userNo
            }  else if let value = record.rawDesc, value.contains(find: "ProjectId") {
                        var mobileNo = "XXXXXXXXXX"
                        if value.contains(find: "ProjectId"){
                            let newValue = value.components(separatedBy: " Wallet Number :")
                            if newValue.count > 2 {
                                if newValue.indices.contains(1){
                                    let name = newValue[2].components(separatedBy: "ProjectId")
                                    if name.indices.contains(0){
                                        mobileNo = "(+95)".appending(name[0])
                                    }
                                }
                            }  else {
                                let arrVal = value.components(separatedBy: "Account Number :")
                                if arrVal.indices.contains(1){
                                    let name = arrVal[1].components(separatedBy: "ProjectId")
                                    if name.indices.contains(0){
                                        mobileNo = "(+95)".appending(name[0])
                                    }
                                }
                            }
                        }
                source = userNo
                dest = mobileNo
            } else if let desc = record.rawDesc, desc.contains(find: "#Tax Code") {
                source = userNo
                dest = "XXXXXXXXXX"
            }
            
            if let bonus = record.bonus {
                bonusTransAmount = bonusTransAmount.adding(bonus)
            }
            let dateFormat = (record.transactionDate?.stringValue() ?? "").replacingOccurrences(of: ",", with: "")
            var openingBal: Double = 0.0
            if let bal = record.walletBalance, let amt = record.amount {
                if let trType = record.accTransType, trType == "Dr" {
                    openingBal = Double(truncating: bal) + Double(truncating: amt)
                } else {
                    openingBal = Double(truncating: bal) - Double(truncating: amt)
                }
            }
            var status = ""
            if record.accTransType == "Dr" {
                status = "Debit"
            } else {
                status = "Credit"
            }
            descWithNoComa = descWithNoComa.replacingOccurrences(of: ",", with: "")
            let behavior = NSDecimalNumberHandler(roundingMode: NSDecimalNumber.RoundingMode.plain,
                                                  scale: 2,
                                                  raiseOnExactness: false,
                                                  raiseOnOverflow: false,
                                                  raiseOnUnderflow: false,
                                                  raiseOnDivideByZero: false)
            let calcDN = NSDecimalNumber.init(value: openingBal).rounding(accordingToBehavior: behavior)
            
            recText = recText + ",\(record.amount?.toString() ?? ""),\(userNo),\(source),\(dest),\(status),\(businessNm),\(accountType),\(payType),\(calcDN.toString() ),\((record.bonus?.toString() ?? "").replacingOccurrences(of: ",", with: "")),\(cashBack),\(debitAmount),\(creditAmount),\(record.walletBalance?.toString() ?? ""),\(dateFormat),\(descWithNoComa)\n"
            
            println_debug(recText)
            excelTxt.append(recText)
        }
        excelTxt.append("\nTotal,\(totalTransAmount.toString()),,,,,,,,,\(bonusTransAmount.toString()),\(cashBackAmt.toString()),\(creditTransAmount.toString()),\(debitTransAmount.toString())")
        guard let excelUrl = CodeSnippets.generateExcel(excelDataWithFormat: excelTxt, excelFileName: "OK$ Transaction Report") else {
            println_debug("Error - excel not generated")
            return
        }
        let activityViewController = UIActivityViewController(activityItems: [excelUrl], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
}

// MARK: - UITextFieldDelegate
extension TransactionDetailViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tableViewFilter.isHidden = true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        tableViewTransaction.contentOffset = CGPoint(x: 0, y: 0)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if range.location == 0 && string == " " { return false}
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            if text.last == " " && string == " " { return false }
            if updatedText.count > 50 || (updatedText == "" && text == "") {
                return false
            }
            if string != "\n" {
                applyFilter(searchText: updatedText)
            }
        }
        return true
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension TransactionDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        switch tableView.tag {
        case 0:
            if transactionRecords.count == 0 {
                let noDataLabel = UILabel(frame: tableView.bounds)
                noDataLabel.text = "No Records".localized
                noDataLabel.font = UIFont(name: appFont, size: 18)
                noDataLabel.textAlignment = .center
                tableView.backgroundView = noDataLabel
            } else {
                tableView.backgroundView = nil
            }
        default:
            break
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView.tag {
        case 0:
            return transactionRecords.count
        case 1:
            return filterArray.count
        default:
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView.tag {
        case 0:
            let transactionDetailCell = tableView.dequeueReusableCell(withIdentifier: cellTransactionCell, for: indexPath) as? TransactionDetailCell
            transactionDetailCell!.configureTransactionItemCell(transRecord: transactionRecords[indexPath.row])
            return transactionDetailCell!
        case 1:
        let filterCell = tableView.dequeueReusableCell(withIdentifier: ConstantsXibCell.filterTableViewCell, for: indexPath) as? FilterTableViewCell
        configureFilterCell(cell: filterCell!, filterType: filterArray[indexPath.row])
        return filterCell!
        default:
            let transactionDetailCell = tableView.dequeueReusableCell(withIdentifier: cellTransactionCell, for: indexPath) as? TransactionDetailCell
            return transactionDetailCell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView.tag {
        case 0:
            break
        case 1:
            let selectedFilter = filterArray[indexPath.row]
            switch selectedFilter.rootFilter {
            case .transactionType:
                if selectedFilter.hasChild {
                    switch selectedFilter.filterTitle {
                    case ParentType.refunds.rawValue:
                        expandRefundOptions(selectedFilter: selectedFilter)
                    case ParentType.payment.rawValue:
                        expandPaymentOptions(selectedFilter: selectedFilter)
                    case ParentType.requestMoney.rawValue:
                        expandRequestMoneyOptions(selectedFilter: selectedFilter)
                    case ParentType.offers.rawValue:
                        expandOffersOptions(selectedFilter: selectedFilter)
                    case ParentType.accountType.rawValue:
                        expandAccountTypeOptions(selectedFilter: selectedFilter)
                    default:
                        buttonTransactionType.setTitle(selectedFilter.filterTitle.localized, for: .normal)
                        tableViewFilter.isHidden = true
                    }
                    updateFilterTableHeight()
                } else {
                    buttonTransactionType.setTitle(selectedFilter.filterTitle.localized, for: .normal)
                    tableViewFilter.isHidden = true
                    applyFilter()
                }
            case .amount:
                buttonAmountRange.setTitle(selectedFilter.filterTitle.localized, for: .normal)
                tableViewFilter.isHidden = true
                applyFilter()
            case .gender:
                buttonGender.setTitle(selectedFilter.filterTitle.localized, for: .normal)
                tableViewFilter.isHidden = true
                applyFilter()
            case .age:
                buttonAge.setTitle(selectedFilter.filterTitle.localized, for: .normal)
                tableViewFilter.isHidden = true
                applyFilter()
            case .period:
                tableViewFilter.isHidden = true
                showDateOptions(selectedOption: selectedFilter.filterTitle.localized)
            }
         default:
            break
        }
    }
}

extension TransactionDetailViewController: UIScrollViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        targetContentOffset.pointee.y = max(targetContentOffset.pointee.y - 1, 1)
                
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
      let location = scrollView.panGestureRecognizer.location(in: tableViewTransaction)
      guard let indexPath = tableViewTransaction.indexPathForRow(at: location) else {
            print("could not specify an indexpath")
            return
        }

        if indexPath.row != 0{

            if (indexPath.row == transactionRecords.count - 2){
                //calling the API here
                //  let firstIndex  =
                if transactionRecords.count >= 20 {
                    if filterArray.count > 0 {
                        print("Not fatching records from the server")
                    }
                    else {
                        addMore = transactionRecords.count + 20
                                           if appDelegate.checkNetworkAvail(){
                                               //PTLoader.shared.show()
                                               modelReport.getALLTransactionInfo(first: "\(addMore)", last: "\(20)")
                        }
                    }
                }
            }
        }
        print("will begin dragging at row \(indexPath.row)")
    }
}

// MARK: - Additional Methods
extension TransactionDetailViewController: NavigationSetUpProtocol {
    /// It set up the navigation bar
    func setUpNavigation() {
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationController?.navigationBar.barTintColor        = ConstantsColor.navigationHeaderTransaction
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: titleViewWidth, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        
        let searchButton = UIButton(type: .custom)
        searchButton.setImage(#imageLiteral(resourceName: "reportSearch").withRenderingMode(.alwaysOriginal), for: .normal)
        searchButton.addTarget(self, action: #selector(showSearchView), for: .touchUpInside)
        
        let shareButton = UIButton(type: .custom)
        shareButton.setImage(#imageLiteral(resourceName: "reportShare").withRenderingMode(.alwaysOriginal), for: .normal)
        shareButton.addTarget(self, action: #selector(showShareOption), for: .touchUpInside)
        
        let graphButton = UIButton(type: .custom)
        graphButton.setImage(#imageLiteral(resourceName: "reportGraph").withRenderingMode(.alwaysOriginal), for: .normal)
        graphButton.addTarget(self, action: #selector(showGraph), for: .touchUpInside)
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "tabBarBack").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(reportBackAction), for: .touchUpInside)
        
        let ypos = (titleViewSize.height - 30 ) / 2
        if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 180, y: ypos, width: 30, height: 30)
            shareButton.frame = CGRect(x: navTitleView.frame.size.width - 140, y: ypos, width: 30, height: 30)
            graphButton.frame = CGRect(x: navTitleView.frame.size.width - 100, y: ypos, width: 30, height: 30)
            backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        }
        else{
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 110, y: ypos, width: 30, height: 30)
            shareButton.frame = CGRect(x: navTitleView.frame.size.width - 70, y: ypos, width: 30, height: 30)
            graphButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
            backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        }
        
       
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 240, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text = ConstantsController.transactionDetailViewController.screenTitle.localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 23) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        navTitleView.addSubview(searchButton)
        navTitleView.addSubview(shareButton)
        navTitleView.addSubview(graphButton)
        self.navigationItem.titleView = navTitleView
    }
    
    /// It shows the home screen
    @objc func reportBackAction() {
        //        AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        //        self.navigationController?.popViewController(animated: true)
        if isSearchViewShow {
            hideSearchView()
        } else {
            AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension TransactionDetailViewController: ReportModelDelegate {
    func apiResult(message: String?, statusCode: String?) {
        DispatchQueue.main.async {
            PTLoader.shared.show()
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                self.fetchRecordsFromDB()
                self.tableViewTransaction.reloadData()
                self.resetSortButtons(sender: UIButton())
                self.resetFilterButtons()
                self.refreshControl.endRefreshing()
                PTLoader.shared.hide()
            
            })
        }
    }
    func handleCustomError(error: CustomError) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                progressViewObj.removeProgressView()
            }
        }
    }
}



