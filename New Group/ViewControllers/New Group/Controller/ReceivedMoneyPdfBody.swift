//
//  ReceivedMoneyPdfBody.swift
//  OK
//
//  Created by iMac on 5/14/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class ReceivedMoneyPdfBody: UIView {

    @IBOutlet weak var transactionID: UILabel!{
        didSet{
            self.transactionID.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var mobileNo: UILabel!{
        didSet{
            self.mobileNo.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var bonus: UILabel!{
        didSet{
            self.bonus.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var cashBack: UILabel!{
        didSet{
            self.cashBack.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var openingBalance: UILabel!{
        didSet{
            self.openingBalance.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var closingBalance: UILabel!{
        didSet{
            self.closingBalance.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var amount: UILabel!{
        didSet{
            self.amount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var transactionDate: UILabel!{
        didSet{
            self.transactionDate.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var transDescription: UILabel!{
        didSet{
            self.transDescription.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var businessName: UILabel!{
        didSet{
            self.businessName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    
    func fillDetails(record: ReportTransactionRecord) {
        self.transactionID.text = record.transID ?? ""
        cashBack.text = "0" //For receiver side, since no cashback for receiver
        if let transAmount = record.amount {
            self.amount.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: transAmount.toString())
        }
        
        var openingBal: Double = 0.0
        if let bal = record.walletBalance, let amt = record.amount {
            openingBal = Double(truncating: bal) - Double(truncating: amt)
        }
        let behavior = NSDecimalNumberHandler(roundingMode: NSDecimalNumber.RoundingMode.plain,
                                              scale: 2,
                                              raiseOnExactness: false,
                                              raiseOnOverflow: false,
                                              raiseOnUnderflow: false,
                                              raiseOnDivideByZero: false)
        let calcDN = NSDecimalNumber.init(value: openingBal).rounding(accordingToBehavior: behavior)
        
        if record.destination == nil, let cashBack = record.cashBack, cashBack > 0 {
            self.openingBalance.text = "0.0"
            self.mobileNo.text = "XXXXXXXXXX"
        } else {
            self.openingBalance.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: calcDN.toString())
            self.mobileNo.text = CodeSnippets.getMobileNumber(transRecord: record)
        }
        
        if record.rawDesc?.contains(find: "MPU") ?? false || record.rawDesc?.contains(find: "Visa") ?? false{
            self.mobileNo.text = "XXXXXXXXXX"
        } else if record.rawDesc?.contains(find: "meter refund") ?? false {
            self.mobileNo.text = "XXXXXXXXXX"
        } else if let desc = record.rawDesc, desc.contains(find: "Bonus Point Exchange Payment") {
            self.mobileNo.text = "XXXXXXXXXX"
        } else if record.transType == "PAYWITHOUT ID" {
            self.mobileNo.text = "XXXXXXXXXX"
        }  else if let desc = record.rawDesc, desc.contains(find: "Bank To Wallet") {
            self.mobileNo.text = "XXXXXXXXXX"
        }
        
        if let balanceAmount = record.walletBalance {
            self.closingBalance.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: balanceAmount.toString())
        }
        
        if let bonusAmount = record.bonus {
            self.bonus.text =  bonusAmount.toStringNoDecimal() //  feeValue -- FEE(server key)
        }
        
        if let businessName = record.senderBName {
            self.businessName.text = businessName
        } else {
            self.businessName.text = "-"
        }
        self.transactionDate.text = ""
        if let transDate = record.transactionDate {
            self.transactionDate.text = transDate.stringValue()
        }
        self.transDescription.text = record.desc?.replacingOccurrences(of: "1#", with: "").replacingOccurrences(of: "2#", with: "")
        
        if let cashBackAmount = record.cashBack {
            if cashBackAmount.compare(0.0) == .orderedDescending {
                cashBack.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: cashBackAmount.toString()) //    feeAmount -- fee(server key)
            }
        }
    }
}

