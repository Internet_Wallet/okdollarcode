//
//  ReportVotingPdfHeader.swift
//  OK
//
//  Created by E J ANTONY on 25/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportVotingPdfHeader: UIView {
    //MARK: - Outlets
    @IBOutlet weak var labelReportHeader: UILabel!{
        didSet{
            labelReportHeader.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelReportSideHeader: UILabel!{
        didSet{
            labelReportSideHeader.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAccNameTitle: UILabel!{
        didSet{
            labelAccNameTitle.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAccNameValue: UILabel!{
        didSet{
            labelAccNameValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAccNumberTitle: UILabel!{
        didSet{
            labelAccNumberTitle.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAccNumberValue: UILabel!{
        didSet{
            labelAccNumberValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelEmailTitle: UILabel!{
        didSet{
            labelEmailTitle.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelEmailValue: UILabel!{
        didSet{
            labelEmailValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelPeriodTitle: UILabel!{
        didSet{
            labelPeriodTitle.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelPeriodValue: UILabel!{
        didSet{
            labelPeriodValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelTotalAmountTitle: UILabel!{
        didSet{
            labelTotalAmountTitle.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelTotalAmountValue: UILabel!{
        didSet{
            labelTotalAmountValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAddressTitle: UILabel!{
        didSet{
            labelAddressTitle.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAddressValue: UILabel!{
        didSet{
            labelAddressValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelVotingTypeFilterTitle: UILabel!{
        didSet{
            labelVotingTypeFilterTitle.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelVotingTypeFilterValue: UILabel!{
        didSet{
            labelVotingTypeFilterValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelPeriodFilterTitle: UILabel!{
        didSet{
            labelPeriodFilterTitle.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelPeriodFilterValue: UILabel!{
        didSet{
            labelPeriodFilterValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelProgNameSort: UILabel!{
        didSet{
            labelProgNameSort.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelEventNameSort: UILabel!{
        didSet{
            labelEventNameSort.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelParticipateNameSort: UILabel!{
        didSet{
            labelParticipateNameSort.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelTransIDSort: UILabel!{
        didSet{
            labelTransIDSort.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelVotesSort: UILabel!{
        didSet{
            labelVotesSort.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelVotingTypeSort: UILabel!{
        didSet{
            labelVotingTypeSort.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAmountSort: UILabel!{
        didSet{
            labelAmountSort.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDateSort: UILabel!{
        didSet{
            labelDateSort.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var viewForFilterSort: UIView!
    @IBOutlet weak var addressHeight: NSLayoutConstraint!
    
    func setLocalizationTitles() {
        labelReportHeader.text = "Voting Detail Statement".localized
        labelReportSideHeader.text = "Account Details".localized
        labelAccNameTitle.text = "Account Name".localized
        labelAccNumberTitle.text = "Account Number".localized
        labelEmailTitle.text = "Email".localized
        labelPeriodTitle.text = "Period".localized
        labelTotalAmountTitle.text = "Total Amount".localized
        labelAddressTitle.text = "Address".localized
        labelVotingTypeFilterTitle.text = "Voting type".localized
        labelPeriodFilterTitle.text = "Period".localized
        labelProgNameSort.text = "Programme Name".localized
        labelEventNameSort.text = "Event".localized
        labelParticipateNameSort.text = "Participate Name".localized
        labelTransIDSort.text = "Transaction ID".localized
        labelVotesSort.text = "Votes".localized
        labelVotingTypeSort.text = "Voting type".localized
        labelAmountSort.text = "Amount".localized + "\n" + "MMK".localized
        labelDateSort.text = "Date & Time".localized
    }
    
    func fillDetails(voteReports: [VotingReportList], filterPeriod: String, filterVotingType: String) {
        setLocalizationTitles()
        var totalTransAmount: Double = 0
        for record in voteReports {
            if let amount = record.payment?.amount {
                totalTransAmount += amount
            }
        }
        self.labelAccNameValue.text = "\(UserModel.shared.name)"
        self.labelAccNumberValue.text = UserModel.shared.formattedNumber //mobileNo.replaceFirstTwoChar(withStr: "+")
        self.labelEmailValue.text = "\(UserModel.shared.email)"
        let division = self.getDivision().components(separatedBy: ",")
        let townShip = "Township"
        var div = ""
        if division.count > 0 {
            div = division.last?.components(separatedBy: "Division").first ?? ""
        }
        if UserModel.shared.language != "en" {
//            self.accountTypeVal.text = getBurmeseAgentType()
            self.labelAddressValue.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? ""), \(division.last ?? "")".replacingOccurrences(of: ", ,", with: ",")
        } else {
//            self.accountTypeVal.text = accountType
            self.labelAddressValue.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? "") \(townShip), \(div), \(division.last ?? "")".replacingOccurrences(of: ", ,", with: ",")
            self.addressHeight.constant = 65.0
            self.layoutIfNeeded()
        }
        self.labelTotalAmountValue.text = "\(totalTransAmount)" + " " + "MMK"
        var titleStr = ""
        if filterVotingType == "Voting type".localized {
            titleStr = "All".localized
        } else {
            titleStr = filterVotingType
        }
        self.labelVotingTypeFilterValue.text = titleStr
        if filterPeriod == "Period".localized {
            titleStr = "All".localized
        } else {
            titleStr = filterPeriod
        }
        titleStr = titleStr.replacingOccurrences(of: "\n", with: " ")
        self.labelPeriodFilterValue.text = titleStr
        self.labelPeriodValue.text = titleStr
        self.layoutIfNeeded()
    }
    
    private func getDivision() -> String {
        return getDivisionAndTownShipForReport(divisionCode: UserModel.shared.state, townshipCode: UserModel.shared.township)
    }
}
