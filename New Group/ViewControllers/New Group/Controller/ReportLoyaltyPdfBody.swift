//
//  ReportLoyaltyPdfBody.swift
//  OK
//
//  Created by E J ANTONY on 11/10/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportLoyaltyPdfBody: UIView {
    //MARK: - Outlets
    @IBOutlet weak var lblTransID: UILabel!{
        didSet {
            lblTransID.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblMerchantNo: UILabel!{
        didSet {
            lblMerchantNo.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblShopName: UILabel!{
        didSet {
            lblShopName.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblReceiver: UILabel!{
        didSet {
            lblReceiver.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblTransType: UILabel!{
        didSet {
            lblTransType.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblBalanceBonusPoint: UILabel!{
        didSet {
            lblBalanceBonusPoint.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblDateTime: UILabel!{
        didSet {
            lblDateTime.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblComments: UILabel!{
        didSet {
            lblComments.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    //MARK: - Methods
    func configureData(record: LoyaltyData) {
        lblTransID.text = record.transId ?? ""
        let mobileNum = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: record.merchantCode.safelyWrappingString())
        lblMerchantNo.text = "(\(mobileNum.counrtycode))\(mobileNum.mobileNo)"
        lblShopName.text = record.merchantName ?? ""
        lblReceiver.text = (record.transferPoint ?? "").replacingOccurrences(of: ".00", with: "")
        if let date = record.validateDate {
            let dateStr = date.dateValue(dateFormatIs: "yyyy-Mm-dd") ?? Date()
            lblTransType.text = dateStr.stringValue(dateFormatIs: "dd-MMM-yyyy")
        }
        lblBalanceBonusPoint.text = (record.closingPoint ?? "").replacingOccurrences(of: ".00", with: "")
        if let date = record.transDate {
            let dateStr = date.dateValue(dateFormatIs: "yyyy-Mm-dd HH:mm:ss") ?? Date()
            lblDateTime.text = dateStr.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy\nHH:mm:ss")
        }
        lblComments.text = "Comments: \(record.comments ?? "")"
    }
}
