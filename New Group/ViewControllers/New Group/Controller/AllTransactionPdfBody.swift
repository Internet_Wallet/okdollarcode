//
//  AllTransactionPdfBody.swift
//  OK
//
//  Created by ANTONY on 24/02/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class AllTransactionPdfBody: UIView {
    // MARK: - Outlets
    @IBOutlet weak var transactionID: UILabel!{
        didSet{
            self.transactionID.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var transactionAmount: UILabel!{
        didSet{
            self.transactionAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var phoneNumber: UILabel!{
        didSet{
            self.phoneNumber.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var name: UILabel!{
        didSet{
            self.name.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var transactionType: UILabel!{
        didSet{
            self.transactionType.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var bonus: UILabel!{
        didSet{
            self.bonus.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var cashBack: UILabel!{
        didSet{
            self.cashBack.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var balanceWallet: UILabel!{
        didSet{
            self.balanceWallet.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var transactionDate: UILabel!{
        didSet{
            self.transactionDate.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var transDescription: UILabel!{
        didSet{
            self.transDescription.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var businessName: UILabel!{
        didSet{
            self.businessName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    @IBOutlet weak var creditAmount: UILabel!{
        didSet{
            self.creditAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var debitAmount: UILabel!{
        didSet{
            self.debitAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var openingBal: UILabel!{
        didSet{
            self.openingBal.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    
    func fillDetails(record: ReportTransactionRecord) {
        var isAmountGTZ = false
        self.transactionID.text = record.transID ?? ""
        phoneNumber.text = "XXXXXXXXXX"
        businessName.text = record.receiverBName ?? "-"
        cashBack.text = "0" //For receiver side, since no cashback for receiver
        creditAmount.text = "0"
        debitAmount.text = "0"
        if let transAmount = record.amount {
            self.transactionAmount.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: transAmount.toString())
            if transAmount.compare(0.0) == .orderedDescending {
                isAmountGTZ = true
            }
        }
        if record.accTransType == "Dr" {
            if record.rawDesc?.contains(find: "PAYMENTTYPE:GiftCard") ?? false {
                phoneNumber.text = "XXXXXXXXXX"
            } else  if record.transType == "PAYWITHOUT ID" || record.transType == "TICKET" || record.transType == "TOLL" {
                phoneNumber.text = CodeSnippets.getMobileNumber(transRecord: record)
            } else {
                phoneNumber.text = CodeSnippets.getMobileNumber(transRecord: record)
            }
            name.text = record.receiverName?.replacingOccurrences(of: "-cashin", with: "").replacingOccurrences(of: "-cashout", with: "")
            if record.receiverBName?.count ?? 0 > 1 {
                businessName.text = record.receiverBName ?? "-"
            } else {
                businessName.text = "-"
            }
            debitAmount.text = self.transactionAmount.text
        } else {
            
            let data = record.senderName?.replacingOccurrences(of: "-cashin", with: "").replacingOccurrences(of: "-cashout", with: "")
            
            if let value = data{
                if value.contains("@sender"){
                    let newValue = value.components(separatedBy: "@sender")
                    if newValue.indices.contains(1){
                        let nameNew = newValue[1].components(separatedBy: "@businessname")
                        if nameNew.indices.contains(0){
                            name.text = nameNew[0]
                        }
                    }
                }else{
                    name.text = record.senderName?.replacingOccurrences(of: "-cashin", with: "").replacingOccurrences(of: "-cashout", with: "")
                }
            }
            
            //businessName.text = record.senderBName ?? "-"
            if record.senderBName?.count ?? 0 > 1 {
                           businessName.text = record.senderBName ?? "-"
                       } else {
                           businessName.text = "-"
                       }
            if record.rawDesc?.contains(find: "PAYMENTTYPE:GiftCard") ?? false {
                phoneNumber.text = "XXXXXXXXXX"
            } else if record.transType == "PAYWITHOUT ID" || record.transType == "TICKET" || record.transType == "TOLL" {
                phoneNumber.text = "XXXXXXXXXX"
                name.text = "XXXXXXXX"
            } else {
                phoneNumber.text = CodeSnippets.getMobileNumber(transRecord: record)
            }
            creditAmount.text = self.transactionAmount.text
        }
        
        if record.rawDesc?.contains(find: "MPU") ?? false {
            phoneNumber.text = "XXXXXXXXXX"
        } else if record.rawDesc?.contains(find: "ELECTRICITY") ?? false {
            phoneNumber.text = "XXXXXXXXXX"
        } else if let desc = record.rawDesc, desc.contains(find: "SEND MONEY TO BANK:") {
             phoneNumber.text = "XXXXXXXXXX"
        } else if let desc = record.rawDesc, desc.contains("meter refund") {
            phoneNumber.text = "XXXXXXXXXX"
        } else if let desc = record.rawDesc, desc.contains("DTH") {
            phoneNumber.text = "XXXXXXXXXX"
        } else if let desc = record.rawDesc, desc.contains(find: "Bonus Point Exchange Payment") {
            phoneNumber.text = "XXXXXXXXXX"
        } else if let des = record.rawDesc, des.contains(find: "#OK-PMC") {
            let descSecondString = des.components(separatedBy: "[").last ?? ""
            let descArray = descSecondString.components(separatedBy: ",")
            if descArray.count > 3 {
                if let value = Int(descArray[3]), value == 0 {
                    if let dest = record.destination {
                        phoneNumber.text = "XXXXXX" + String(dest.suffix(4))
                    }
                }
            }
        }
        var tType = ""
        if let desc = record.rawDesc {
            if desc.contains("#OK-") || desc.contains("OOK")
            {
                if desc.contains("Top-Up") || desc.contains("TopUp") || desc.contains("Bonus Point Top Up") || desc.contains("TopUpPlan"){
                    tType = "Top-Up"
                    //phoneNumber.text = "XXXXXXXXXX"
                } else if desc.contains("Data Plan") || desc.contains("DataPlan") {
                    tType = "Data Plan"
                    //phoneNumber.text = "XXXXXXXXXX"
                } else if desc.contains("MPU Card") {
                    tType = "MPU"
                } else if desc.contains("meter refund") {
                    tType = "Refund"
                } else {
                    tType = "Special Offers"
                }
            } else {
                if let transType = record.transType {
                    tType = transType
                    if transType == "PAYWITHOUT ID" {
                        if let accTransType = record.accTransType {
                            if accTransType == "Cr" {
                                tType = "HIDE MY NUMBER"
                            } else if desc.contains(find: "#Tax Code") {
                                tType = "Tax Payment"
                            }
                            else {
                                tType = "HIDE MY NUMBER"
                            }
                        }
                        
                    } else if transType == "TOLL" {
                        tType = "TOLL"
                    } else if transType == "TICKET" {
                        tType = "TICKET"
                    } else {
                        tType = "PAYTO"
                        if desc.contains(find: "-cashin") {
                            tType = "Cash In"
                        } else if desc.contains(find: "-cashout") {
                            tType = "Cash Out"
                        } else if desc.contains(find: "-transferto") {
                            tType = "Transfer To"
                        }
                    }
                }
            }
        }
        
        transactionType.text = tType
        if let type = record.rawDesc, type.contains(find: "MPU") {
            transactionType.text = "MPU"
        } else if let type = record.rawDesc, type.contains(find: "ELECTRICITY") {
            transactionType.text = "ELECTRICITY"
        } else if let type = record.rawDesc, type.contains(find: "GiftCard") {
            transactionType.text = "Gift Card"
        } else if let type = record.rawDesc, type.contains(find: "meter refund") {
            transactionType.text = "Refund"
        } else if let type = record.rawDesc, type.contains(find: "DTH") {
            transactionType.text = "DTH"
        } else if let type = record.rawDesc?.lowercased(), type.contains(find: "visa") {
            transactionType.text = "VISA"
        } else if let type = record.rawDesc?.lowercased(), type.contains(find: "#Tax Code") {
            transactionType.text = "Tax Payment"
        }
        
        var openingBal: Double = 0.0
        if let bal = record.walletBalance, let amt = record.amount {
            if let trType = record.accTransType, trType == "Dr" {
                openingBal = Double(truncating: bal) + Double(truncating: amt)
            } else {
                openingBal = Double(truncating: bal) - Double(truncating: amt)
            }
        }
        
        let behavior = NSDecimalNumberHandler(roundingMode: NSDecimalNumber.RoundingMode.plain,
                                              scale: 2,
                                              raiseOnExactness: false,
                                              raiseOnOverflow: false,
                                              raiseOnUnderflow: false,
                                              raiseOnDivideByZero: false)
        let calcDN = NSDecimalNumber.init(value: openingBal).rounding(accordingToBehavior: behavior)
        self.openingBal.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount:calcDN.toString())
        
        if let balanceAmount = record.walletBalance {
            self.balanceWallet.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: balanceAmount.toString())
        }
        if let bonusAmount = record.bonus {
            self.bonus.text =  bonusAmount.toStringNoDecimal() //  feeValue -- FEE(server key)
        }
        self.transactionDate.text = ""
        if let transDate = record.transactionDate {
            self.transactionDate.text = transDate.stringValue()
        }
        let desc = record.rawDesc?.removingPercentEncoding?.replacingOccurrences(of: "+", with: " ").components(separatedBy: "-")
        if record.rawDesc?.contains(find: "ELECTRICITY") ?? false {
            let dateFormat = self.formatDate(dateString: desc?.last ?? "")
            var name: [String]?
            if desc?[9].contains(find: "Name :") ?? false {
                name = desc?[9].components(separatedBy: "Name :")
            } else if desc?[10].contains(find: "Name :") ?? false {
                name = desc?[10].components(separatedBy: "Name :")
            }
            var diviLoc = ""
            if desc?[7].contains(find: "Division") ?? false {
                let division = desc?[7].components(separatedBy: "Division :")
                if let divLocal = division?[1] {
                    let arr = divLocal.components(separatedBy: "Division")
                    diviLoc = arr[0]
                }
            } else if desc?[8].contains(find: "Division") ?? false {
                let division = desc?[8].components(separatedBy: "Division :")
                if let divLocal = division?[1] {
                    let arr = divLocal.components(separatedBy: "Division")
                    diviLoc = arr[0]
                }
            }
            
            var township: [String]?
            if desc?[8].contains(find: "Township") ?? false {
                township = desc?[8].components(separatedBy: "Township")
            } else if desc?[9].contains(find: "Township") ?? false {
                township = desc?[9].components(separatedBy: "Township")
            }
            var payFor: [String]?
            var meterNo = ""
            if desc?[6].contains(find: "PayFor:") ?? false {
                payFor = desc?[6].components(separatedBy: "PayFor:")
                meterNo = desc?[2] ?? ""
            } else if desc?[7].contains(find: "PayFor:") ?? false {
                meterNo = "\(desc?[2] ?? "")-\(desc?[3] ?? "")"
                payFor = desc?[7].components(separatedBy: "PayFor:")
            }
            let fullDesc = "\(meterNo) \(name?[1] ?? ""), \(township?[1] ?? "") Township, \(diviLoc), \(diviLoc) Division, Bill Month: \(dateFormat.0) Due Date \(dateFormat.1) \(payFor?[1] ?? "")"
            self.transDescription.text = fullDesc.replacingOccurrences(of: ",", with: "")
            self.name.text = "Unknown"
        } else if let desc = record.rawDesc, desc.contains(find: "SEND MONEY TO BANK:") {
            if let array = record.rawDesc?.components(separatedBy: "SEND MONEY TO BANK:"), array.count > 0 {
                self.transDescription.text = "SEND MONEY TO BANK: \(array[1])"
            }
        } else if record.rawDesc?.contains(find: "DTH") ?? false {
            self.transDescription.text = record.rawDesc?.components(separatedBy: "Destination").first ?? ""
        } else if record.rawDesc?.contains(find: "Bank To Wallet") ?? false {
            phoneNumber.text = "XXXXXXXXXX"
            self.transDescription.text = "Remarks: " + (record.rawDesc?.components(separatedBy: "ProjectId").first ?? "")
        } else if record.rawDesc?.contains(find: "ProjectId") ?? false {
            //phoneNumber.text = "XXXXXXXXXX"
            var mobileNo = "XXXXXXXXXX"
            let value = record.rawDesc ?? ""
            if value.contains(find: "ProjectId"){
                let newValue = value.components(separatedBy: " Wallet Number :")
                if newValue.count > 2 {
                    if newValue.indices.contains(1){
                        let name = newValue[2].components(separatedBy: "ProjectId")
                        if name.indices.contains(0){
                            mobileNo = "(+95)".appending(name[0])
                        }
                    }
                }  else {
                    let arrVal = value.components(separatedBy: "Account Number :")
                    if arrVal.indices.contains(1){
                        let name = arrVal[1].components(separatedBy: "ProjectId")
                        if name.indices.contains(0){
                            mobileNo = "(+95)".appending(name[0])
                        }
                    }
                }
            }
            phoneNumber.text = mobileNo
            self.transDescription.text = "Remarks: " + (record.rawDesc?.components(separatedBy: "ProjectId").first ?? "")
        } else if record.rawDesc?.contains(find: "#Tax Code") ?? false {
            phoneNumber.text = "XXXXXXXXXX"
            let tmpDesc = record.rawDesc?.replacingOccurrences(of: "#", with: "") ?? ""
            self.transDescription.text = "Remarks: " + (tmpDesc.replacingOccurrences(of: "Tax Code :", with: "Tax Payment - IRD Transaction ID :")) //+ (tmpDesc.components(separatedBy: "Tax Code :").first ?? "")
        } else if record.rawDesc?.contains(find: "Overseas") ?? false {
            if record.rawDesc?.contains(find: "TopUpPlan") ?? false {
                self.transDescription.text = "Remarks: TopUp Plan International"
            } else {
                self.transDescription.text = "Remarks: Data Plan International"
            }
        }
        else {
            self.transDescription.text = record.desc?.replacingOccurrences(of: "1#", with: "").replacingOccurrences(of: "2#", with: "")
        }
        
        if let cashBackAmount = record.cashBack {
            if cashBackAmount.compare(0.0) == .orderedDescending {
                cashBack.text =  cashBackAmount.toString() //    feeAmount -- fee(server key)
                if !isAmountGTZ {
                    self.phoneNumber.text = record.countryCode ?? "(+95)"
                }
            }
        }
    }
    
    func formatDate(dateString: String) -> (String, String) {
        let dateStr = dateString.components(separatedBy: "Due Date :")
        if dateStr.count > 0 {
            let currentDate = dateStr[1].replacingOccurrences(of: "#", with: "")
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy hh:mm:ss a"
            let date = formatter.date(from: currentDate)
            let previousMonth = Calendar.current.date(byAdding: .month, value: -1, to: date ?? Date())
            formatter.dateFormat = "dd-MMM-yyyy"
            let strDate = formatter.string(from: date ?? Date())
            formatter.dateFormat = "MMMM"
            let prevMonth = formatter.string(from: previousMonth ?? Date())
            return(prevMonth, strDate)
        }
        return ("", "")
    }
}
