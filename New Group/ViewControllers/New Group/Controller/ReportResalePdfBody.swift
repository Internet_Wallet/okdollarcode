//
//  ReportResalePdfBody.swift
//  OK
//
//  Created by ANTONY on 16/03/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportResalePdfBody: UIView {
    
    // MARK: - Outlets
    @IBOutlet weak var sourceNumber: UILabel!{
        didSet{
            self.sourceNumber.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var destinationNumber: UILabel!{
        didSet{
            self.destinationNumber.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var sellingAmount: UILabel!{
        didSet{
            self.sellingAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var sellingPrice: UILabel!{
        didSet{
            self.sellingPrice.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var dateTime: UILabel!{
        didSet{
            self.dateTime.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var status: UILabel!{
        didSet{
            self.status.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    
    func fillDetails(record: ReportResaleMainBalance) {
        self.sourceNumber.text = ""
        self.destinationNumber.text = ""
        self.sellingAmount.text = ""
        self.sellingPrice.text = ""
        self.dateTime.text = ""
        self.status.text = ""
        if let sNum = record.phoneNumber {
            var phNumber = sNum.replaceFirstTwoChar(withStr: "+")
            phNumber.insert(")", at: phNumber.index(phNumber.startIndex, offsetBy: 3))
            phNumber.insert("(", at: phNumber.startIndex)
            if UserModel.shared.mobileNo.hasPrefix("0095") {
                phNumber.insert("0", at: phNumber.index(phNumber.startIndex, offsetBy: 5))
            }
            self.sourceNumber.text = phNumber
        }
        if let dNum = record.destinationNumber {
            var phNumber = dNum.replaceFirstTwoChar(withStr: "+")
            phNumber.insert(")", at: phNumber.index(phNumber.startIndex, offsetBy: 3))
            phNumber.insert("(", at: phNumber.startIndex)
            if UserModel.shared.mobileNo.hasPrefix("0095") {
                phNumber.insert("0", at: phNumber.index(phNumber.startIndex, offsetBy: 5))
            }
            self.destinationNumber.text = phNumber
        }
        if let sellingAmount = record.airtimeAmount {
            self.sellingAmount.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: sellingAmount.toString())
        }
        if let sellingPrice = record.sellingAmount {
            self.sellingPrice.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: sellingPrice.toString())
        }
        if let dateT = record.createDate {
            self.dateTime.text = dateT.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy\nHH:mm:ss")
        }
        if let statusR = record.status {
            self.status.text = statusR
        }
    }
}
