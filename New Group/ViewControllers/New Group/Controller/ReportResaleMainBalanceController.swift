//
//  ReportResaleMainBalanceController.swift
//  OK
//  This show the list of re-sale main balance
//  Created by ANTONY on 05/02/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportResaleMainBalanceController: UIViewController, UIScrollViewDelegate {
    // MARK: - Outlets
    @IBOutlet weak var tableViewReSaleMainBalance: UITableView!
    @IBOutlet weak var tableViewFilter: UITableView!
    @IBOutlet weak var tableViewOption: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var constraintScrollContentHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintFilterTableLeading: NSLayoutConstraint!
    @IBOutlet weak var constraintFilterTableHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintScrollTop: NSLayoutConstraint!
    
    //For search functions
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var buttonCancel: UIButton!{
        didSet {
            buttonCancel.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonClear: UIButton!{
        didSet {
            buttonClear.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    //For filter functions
    @IBOutlet weak var buttonSellingAmountFilter: ButtonWithImage!{
        didSet {
            buttonSellingAmountFilter.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonSellingPriceFilter: ButtonWithImage!{
        didSet {
            buttonSellingPriceFilter.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonOperatorFilter: ButtonWithImage!{
        didSet {
            buttonOperatorFilter.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonStatusFilter: ButtonWithImage!{
        didSet {
            buttonStatusFilter.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonPeriodFilter: ButtonWithImage!{
        didSet {
            buttonPeriodFilter.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    //For sorting functions
    @IBOutlet weak var buttonSourceNumberSort: ButtonWithImage!{
        didSet {
            buttonSourceNumberSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDestinationNumberSort: ButtonWithImage!{
        didSet {
            buttonDestinationNumberSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonSellingAmountSort: ButtonWithImage!{
        didSet {
            buttonSellingAmountSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonSellingPriceSort: ButtonWithImage!{
        didSet {
            buttonSellingPriceSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonStatusSort: ButtonWithImage!{
        didSet {
            buttonStatusSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateSort: ButtonWithImage!{
        didSet {
            buttonDateSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    @IBOutlet weak var viewDatePicker: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var buttonDateSubmit: UIButton!{
        didSet {
            buttonDateSubmit.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateCancel: UIButton!{
        didSet {
            buttonDateCancel.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDateOne: UILabel!{
        didSet {
            labelDateOne.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDateTwo: UILabel!{
        didSet {
            labelDateTwo.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateOne: UIButton!{
        didSet {
            buttonDateOne.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateTwo: UIButton!{
        didSet {
            buttonDateTwo.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var viewToShareAllRecords: UIView!
    @IBOutlet weak var viewCircle: UIView!
    @IBOutlet weak var imgViewInCircle: UIImageView!
    @IBOutlet weak var labelInCircle: UILabel!{
        didSet {
            labelInCircle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var amountInCircle: UILabel!{
        didSet {
            amountInCircle.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    // MARK: - Properties
    var modelReport: ReportModel!
    var resaleList = [ReportResaleMainBalance]()
    let reSaleMainBalanceCellId = "ReportReSaleMainBalanceCell"
    let moreOptionsArray = ["Total Selling Price", "Total Selling Amount", "Total Transactions"]
    let sellingAmountFilters = ["Selling Amount", "All", "0 - 500", "501 - 1,000", "1,001 - 5,000", "5,001 - 10,000"]
    let sellingPriceFilters = ["Selling Price", "All", "0 - 500", "501 - 1,000", "1,001 - 5,000", "5,001 - 10,000"]
    let operatorFilters = ["Operator", "All", "Mpt", "Telenor", "Ooredoo", "Mectel", "Mytel"]
    let statusFilters = ["Status", "All", "Added", "Completed", "Failed"]
    let periodFilters = ["Period", "All", "Date", "Month"]
    var filterArray = [String]()
    private var totalSellingPrice: NSDecimalNumber = 0
    private var totalSellingAmount: NSDecimalNumber = 0
    private var totalTransection : Int = 0
    enum FilterType {
        case sellingAmount, sellingPrice, operatorFilter, status, period
    }
    private var currentFilterType = FilterType.sellingAmount
    var isSearchViewShow: Bool {
        get {
            return !viewSearch.isHidden
        }
        set(newValue) {
            if newValue {
                viewSearch.isHidden = false
                textFieldSearch.becomeFirstResponder()
                constraintScrollTop.constant = 0
                self.view.layoutIfNeeded()
            } else {
                textFieldSearch.text = ""
                viewSearch.isHidden = true
                constraintScrollTop.constant = -40
                self.view.layoutIfNeeded()
                self.view.endEditing(true)
            }
        }
    }
    private var circleViewTimer: Timer!
    var isDateViewHidden: Bool {
        get {
            return viewDatePicker.isHidden
        }
        set(newValue) {
            if newValue {
                viewShadow.isHidden = true
                viewDatePicker.isHidden = true
            } else {
                viewShadow.isHidden = false
                viewDatePicker.isHidden = false
            }
        }
    }
    private let refreshControl = UIRefreshControl()
     let dateFormatter = DateFormatter()
    // MARK: - View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatter.calendar = Calendar.current
           dateFormatter.dateFormat = "dd MMM yyyy"
        AppDelegate.AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)
        setUpNavigation()
        modelReport = ReportModel()
        modelReport.reportModelDelegate = self
        textFieldSearch.placeholder = "Search By Amount or Mobile Number".localized
        tableViewReSaleMainBalance.tag = 0
        tableViewFilter.tag = 1
        tableViewOption.tag = 2
        tableViewReSaleMainBalance.tableFooterView = UIView()
        tableViewFilter.tableFooterView = UIView()
        tableViewOption.tableFooterView = UIView()
        tableViewReSaleMainBalance.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshTableData), for: .valueChanged)
        buttonCancel.layer.cornerRadius = 15
        buttonClear.layer.cornerRadius = 15
        buttonCancel.addTarget(self, action: #selector(hideSearchView), for: .touchUpInside)
        buttonClear.addTarget(self, action: #selector(clearSerchTextField), for: .touchUpInside)
        
        buttonDateSubmit.layer.cornerRadius = 15
        buttonDateCancel.layer.cornerRadius = 15
        buttonDateSubmit.addTarget(self, action: #selector(filterSelectedDate), for: .touchUpInside)
        buttonDateCancel.addTarget(self, action: #selector(hideDateSelectionView), for: .touchUpInside)
        
        buttonCancel.setTitle("Cancel".localized, for: .normal)
        buttonClear.setTitle("Clear".localized, for: .normal)
        labelDateOne.text = "From Date".localized
        labelDateTwo.text = "To Date".localized
        buttonDateSubmit.setTitle("Submit".localized, for: .normal)
        buttonDateCancel.setTitle("Cancel".localized, for: .normal)
        resetFilterButtons()
        setLocalizedTitles()
        datePicker.calendar = Calendar(identifier: .gregorian)
        if #available(iOS 13.4, *) {
                       datePicker.preferredDatePickerStyle = .wheels
                   } else {
                       // Fallback on earlier versions
                   }
        let cellFilterNibName = UINib(nibName: ConstantsXibCell.filterTableViewCell, bundle: nil)
        tableViewFilter.register(cellFilterNibName, forCellReuseIdentifier: ConstantsXibCell.filterTableViewCell)
        tableViewOption.register(cellFilterNibName, forCellReuseIdentifier: ConstantsXibCell.filterTableViewCell)
        
        fetchingDataProcess()
        viewCircle.layer.cornerRadius = viewCircle.frame.size.height / 2
    }
    
    override func viewDidLayoutSubviews() {
        let navBarHeight = navigationController?.navigationBar.frame.size.height ?? 44
        let heightToReduce = navBarHeight == 32 ? 56 : 44
        constraintScrollContentHeight.constant = scrollView.frame.size.height - CGFloat(heightToReduce) + 44
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    // MARK: - Target Methods
    @objc private func refreshTableData(_ sender: Any) {
        self.filterArray = []
        if appDelegate.checkNetworkAvail() {
               self.hideSearchView()
               modelReport.getTransactionInfo()
           }
        else {
            fetchingDataProcess()
        }
        
//        if viewSearch.isHidden {
//            fetchingDataProcess()
//        } else {
//            self.refreshControl.endRefreshing()
//        }
    }
    
    @objc func runTimedCode() {
        self.viewCircle.isHidden = true
        self.viewShadow.isHidden = true
        circleViewTimer?.invalidate()
    }
    
    @objc func monthPickerChanged(datePicker:UIDatePicker) {
        buttonDateTwo.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "MMM yyyy"), for: .normal)
    }
    
    @objc func fromDatePickerChanged(datePicker:UIDatePicker) {
        buttonDateOne.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
    }
    
    @objc func toDatePickerChanged(datePicker:UIDatePicker) {
        if let startDateStr = buttonDateOne.title(for: .normal), let startDate = startDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
            datePicker.minimumDate = startDate
            switch datePicker.date.compare(startDate) {
            case .orderedAscending:
                if let endDateStr = buttonDateTwo.title(for: .normal), let endDate = endDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                    datePicker.date = endDate
                }
            case .orderedDescending, .orderedSame:
                buttonDateTwo.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            }
        } else {
            buttonDateTwo.setTitle(datePicker.date.stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
        }
    }
    
    @objc private func hideSearchView() {
        self.view.endEditing(true)
        isSearchViewShow = false
        applyFilter()
    }
    
    @objc private func clearSerchTextField() {
        textFieldSearch.text = ""
        applyFilter()
    }
    
    @objc private func showSearchView() {
        circleViewTimer?.invalidate()
        tableViewFilter.isHidden = true
        tableViewOption.isHidden = true
        viewCircle.isHidden = true
        viewShadow.isHidden = true
        removeGestureFromShadowView()
        /// For share all transaction
        viewToShareAllRecords.isHidden = true
        if !isSearchViewShow {
            if resaleList.count > 0 {
                isSearchViewShow = true
                textFieldSearch.becomeFirstResponder()
            } else {
                noRecordAlertAction()
            }
        } else {
            //textFieldSearch.becomeFirstResponder()
            hideSearchView()
        }
    }
    
    @objc func shadowIsTapped(sender: UITapGestureRecognizer) {
        removeGestureFromShadowView()
        viewShadow.isHidden = true
        viewToShareAllRecords.isHidden = true
    }
    
    @objc private func showShareOption() {
        circleViewTimer?.invalidate()
        if viewToShareAllRecords.isHidden {
            viewCircle.isHidden = true
            isSearchViewShow = false
            viewCircle.isHidden = true
            tableViewOption.isHidden = true
//            resaleList = modelReport.resaleMainBalanceReports
//            resetFilterButtons()
//            resetSortButtons(sender: UIButton())
            if resaleList.count > 0 {
                tableViewReSaleMainBalance.reloadData()
                let tapGestToRemoveView = UITapGestureRecognizer(target: self, action: #selector(shadowIsTapped))
                viewShadow.addGestureRecognizer(tapGestToRemoveView)
                viewShadow.isHidden = false
                viewToShareAllRecords.isHidden = false
            } else {
                viewShadow.isHidden = true
                noRecordAlertAction()
            }
        }
    }
    
    @objc private func showMoreOption() {
        circleViewTimer?.invalidate()
        if resaleList.count == 0 {
            noRecordAlertAction()
            return
        }
        if tableViewOption.isHidden {
            viewCircle.isHidden = true
            viewShadow.isHidden = true
            tableViewFilter.isHidden = true
            viewToShareAllRecords.isHidden = true
            self.view.endEditing(true)
            tableViewOption.isHidden = false
        } else {
            tableViewOption.isHidden = true
        }
    }
    
    @objc func filterSelectedDate() {
        if let _ = buttonDateOne.title(for: .normal) {
            buttonPeriodFilter.setTitle("Date".localized, for: .normal)
        } else {
            buttonPeriodFilter.setTitle("Month".localized, for: .normal)
        }
        applyFilter()
        isDateViewHidden = true
    }
    
    @objc func hideDateSelectionView() {
        isDateViewHidden = true
    }
    
    // MARK: - Methods
    func fetchingDataProcess() {
        getTokenFromServer(completionHandler: { (isSuccess, tokenString) in
            guard isSuccess, let tokenAuthStr = tokenString as? String else {
                progressViewObj.removeProgressView()
                self.showErrorAlert(errMessage: "try again")
                return
            }
            DispatchQueue.main.async(execute: {
                self.modelReport.getResaleMainBalanceReport(authToken: tokenAuthStr)
            })
        })
    }
    
    func noRecordAlertAction() {
        alertViewObj.wrapAlert(title: "Alert".localized, body: "No Records".localized, img: UIImage(named : "AppIcon"))
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
        })
        alertViewObj.showAlert(controller: self)
    }
    
    func setLocalizedTitles() {
        buttonSourceNumberSort.setTitle("Source Number".localized, for: .normal)
        buttonDestinationNumberSort.setTitle("Destination Number".localized, for: .normal)
        buttonSellingAmountSort.setTitle("Selling Bill Amount".localized, for: .normal)
        buttonSellingPriceSort.setTitle("Selling Receive Price".localized, for: .normal)
        buttonStatusSort.setTitle("Transaction Status".localized, for: .normal)
        buttonDateSort.setTitle("Date & Time".localized, for: .normal)
    }
    
    func getTotalSellingPriceAmount() {
        totalSellingPrice = 0
        totalSellingAmount = 0
        totalTransection = 0
        for rec in modelReport.resaleMainBalanceReports {
            if let sAmount = rec.airtimeAmount {
                totalSellingAmount = totalSellingAmount.adding(sAmount)
            }
            if let sPrice = rec.sellingAmount {
                totalSellingPrice = totalSellingPrice.adding(sPrice)
            }
            totalTransection+=1
        }
    }
    
    func getTokenFromServer(completionHandler: @escaping (Bool,Any?) -> Void) {
        
        let urlStr = String.init(format: Url.airtimeTokenApi)
        let url: URL? = getUrl(urlStr: urlStr, serverType: .airtimeUrl)
        
        guard let tokenUrl = url else {
            showErrorAlert(errMessage: "Something went wrong : Dev need to check")
            return
        }
        println_debug(tokenUrl)
        let hashValue = Url.aKey_airtime.hmac_SHA1(key: Url.sKey_airtime)
        let inputVal = "password=\(hashValue)&grant_type=password"
        
        progressViewObj.showProgressView()
        println_debug("get token from server called........")
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: kMethod_Post, contentType: kContentType_urlencoded, inputVal: inputVal, authStr: nil)
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    println_debug("remove progress view called inside get token from server call")
                }
                
                completionHandler(false,nil)
                return
            }
            
            println_debug("response dict for get token from server :: \(String(describing: response))")
            let responseDic = response as! NSDictionary
            guard let tokentype = responseDic.value(forKey: "token_type"), let accesstoken = responseDic.value(forKey: "access_token") else {
                
                completionHandler(false,nil)
                return
            }
            
            let authorizationString =  "\(tokentype) \(accesstoken)"
            completionHandler(true,authorizationString)
        })
    }
    
    func removeGestureFromShadowView() {
        if let recognizers = viewShadow.gestureRecognizers {
            for recognizer in recognizers {
                if let recognizer = recognizer as? UITapGestureRecognizer {
                    viewShadow.removeGestureRecognizer(recognizer)
                }
            }
        }
    }
    
    func resetSortButtons(sender: UIButton) {
        if sender != buttonSourceNumberSort {
            buttonSourceNumberSort.tag = 0
            buttonSourceNumberSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonDestinationNumberSort {
            buttonDestinationNumberSort.tag = 0
            buttonDestinationNumberSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonSellingAmountSort {
            buttonSellingAmountSort.tag = 0
            buttonSellingAmountSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonSellingPriceSort {
            buttonSellingPriceSort.tag = 0
            buttonSellingPriceSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonStatusSort {
            buttonStatusSort.tag = 0
            buttonStatusSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonDateSort {
            buttonDateSort.tag = 0
            buttonDateSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
    }
    
    func configureDatePicker(tag: Int) {
        switch tag {
        case 0:
            buttonDateOne.setTitleColor(ConstantsColor.navigationHeaderReSale, for: .normal)
            buttonDateTwo.setTitleColor(UIColor(red: 30/255.0, green: 30/255, blue: 30/255, alpha: 1.0), for: .normal)
            datePicker.datePickerMode = .date
            datePicker.removeTarget(nil, action: nil, for: .allEvents)
            datePicker.addTarget(self, action: #selector(fromDatePickerChanged), for: .valueChanged)
            datePicker.minimumDate = nil
            if let endDateStr = buttonDateTwo.title(for: .normal), let endDate = endDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                datePicker.maximumDate = endDate
            } else {
                datePicker.maximumDate = Date()
            }
            if let currentDateStr = buttonDateOne.title(for: .normal), let currentDate = currentDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                datePicker.date = currentDate
            } else {
                buttonDateOne.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            }
        case 1:
            buttonDateOne.setTitleColor(UIColor(red: 30/255.0, green: 30/255, blue: 30/255, alpha: 1.0), for: .normal)
            buttonDateTwo.setTitleColor(ConstantsColor.navigationHeaderReSale, for: .normal)
            datePicker.datePickerMode = .date
            datePicker.maximumDate = Date()
            datePicker.removeTarget(nil, action: nil, for: .allEvents)
            datePicker.addTarget(self, action: #selector(toDatePickerChanged), for: .valueChanged)
           // datePicker.minimumDate = nil
            if let endDateStr = buttonDateOne.title(for: .normal), let endDate = dateFormatter.date(from: endDateStr) {
                datePicker.minimumDate = endDate
            } else {
                datePicker.minimumDate = nil
            }

            if let currentDateStr = buttonDateTwo.title(for: .normal), let currentDate = currentDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                datePicker.date = currentDate
            } else {
                buttonDateTwo.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            }
        case 2:
            buttonDateTwo.setTitleColor(ConstantsColor.navigationHeaderReSale, for: .normal)
            datePicker.datePickerMode = .date
            datePicker.maximumDate = Date()
            datePicker.date = Date()
            datePicker.removeTarget(nil, action: nil, for: .allEvents)
            datePicker.addTarget(self, action: #selector(monthPickerChanged), for: .valueChanged)
            datePicker.minimumDate = nil
            if let currentDateStr = buttonDateTwo.title(for: .normal), let currentDate = currentDateStr.dateValue(dateFormatIs: "MMM yyyy") {
                datePicker.date = currentDate
            } else {
                buttonDateTwo.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "MMM yyyy"), for: .normal)
            }
        default:
            break
        }
    }
    
    func updateFilterTableHeight() {
        constraintFilterTableHeight.constant = filterArray.count > 4 ? CGFloat(5*44) : CGFloat(filterArray.count * 44)
        self.view.layoutIfNeeded()
    }
    
    func resetFilterButtons() {
        buttonSellingAmountFilter.setTitle(sellingAmountFilters[0].localized, for: .normal)
        buttonSellingPriceFilter.setTitle(sellingPriceFilters[0].localized, for: .normal)
        buttonOperatorFilter.setTitle(operatorFilters[0].localized, for: .normal)
        buttonStatusFilter.setTitle(statusFilters[0].localized, for: .normal)
        buttonPeriodFilter.setTitle(periodFilters[0].localized, for: .normal)
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        targetContentOffset.pointee.y = max(targetContentOffset.pointee.y - 1, 1)
    }
    
    func applyFilter(searchText: String = "") {
        var searchTxt = searchText
        var filteredArray = modelReport.resaleMainBalanceReports
        if isSearchViewShow && searchTxt.count == 0 {
            searchTxt = textFieldSearch.text ?? ""
        }
        filteredArray = filterProcess(forSellingAmount: filteredArray)
        filteredArray = filterProcess(forSellingPrice: filteredArray)
        filteredArray = filterProcess(forOperator: filteredArray)
        filteredArray = filterProcess(forStatus: filteredArray)
        filteredArray = filterProcess(forPeriod: filteredArray)
        if isSearchViewShow && searchTxt.count > 0 {
            filteredArray = filteredArray.filter {
                if let airtimeAmount = $0.airtimeAmount {
                    if airtimeAmount.toString().range(of: searchTxt.lowercased()) != nil {
                        return true
                    }
                }
                if let sellingAmount = $0.sellingAmount {
                    if sellingAmount.toString().range(of: searchTxt.lowercased()) != nil {
                        return true
                    }
                }
                if let destNumber = $0.destinationNumber {
                    if searchTxt.hasPrefix("0") {
                        let searchTt = searchTxt.dropFirst()
                        if destNumber.range(of: searchTt.lowercased()) != nil {
                            return true
                        }
                    } else {
                        if destNumber.range(of: searchTxt.lowercased()) != nil {
                            return true
                        }
                    }
                }
                if let mobileNumber = $0.phoneNumber {
                    if mobileNumber.range(of: searchTxt.lowercased()) != nil {
                        return true
                    }
                }
                return false
            }
        }
        resaleList = filteredArray
        tableViewReSaleMainBalance.reloadData()
    }
    
    private func searchSort() {
        let searchText = textFieldSearch.text ?? ""
        var filteredArray = modelReport.resaleMainBalanceReports
        if isSearchViewShow && searchText.count > 0 {
            filteredArray = filteredArray.filter {
                if let airtimeAmount = $0.airtimeAmount {
                    if airtimeAmount.toString().range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                if let sellingAmount = $0.sellingAmount {
                    if sellingAmount.toString().range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                if let destNumber = $0.destinationNumber {
                    if destNumber.range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                if let mobileNumber = $0.phoneNumber {
                    if mobileNumber.range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                return false
            }
        }
        resaleList = filteredArray
    }
    
    func filterProcess(forSellingAmount arrayToFilter: [ReportResaleMainBalance]) -> [ReportResaleMainBalance] {
        func filterFromDecimalNumbers(greaterThan: NSDecimalNumber, lesserThan: NSDecimalNumber)-> [ReportResaleMainBalance] {
            return arrayToFilter.filter
                {
                    guard let sAmount = $0.airtimeAmount else { return false }
                    return (sAmount.compare(greaterThan) == .orderedDescending && sAmount.compare(lesserThan) == .orderedAscending)
            }
        }
        if let sAmountFilter = buttonSellingAmountFilter.currentTitle {
            switch sAmountFilter {
            case "Selling Amount".localized, "All".localized:
                return arrayToFilter
            case "0 - 500".localized:
                return filterFromDecimalNumbers(greaterThan: 0, lesserThan: 501)
            case "501 - 1,000".localized:
                return filterFromDecimalNumbers(greaterThan: 500.99, lesserThan: 1001)
            case "1,001 - 5,000".localized:
                return filterFromDecimalNumbers(greaterThan: 1000.99, lesserThan: 5001)
            case "5,001 - 10,000".localized:
                return filterFromDecimalNumbers(greaterThan: 5000.99, lesserThan: 10001)
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcess(forSellingPrice arrayToFilter: [ReportResaleMainBalance]) -> [ReportResaleMainBalance] {
        func filterFromDecimalNumbers(greaterThan: NSDecimalNumber, lesserThan: NSDecimalNumber)-> [ReportResaleMainBalance] {
            return arrayToFilter.filter
                {
                    guard let sAmount = $0.sellingAmount else { return false }
                    return (sAmount.compare(greaterThan) == .orderedDescending && sAmount.compare(lesserThan) == .orderedAscending)
            }
        }
        if let sAmountFilter = buttonSellingPriceFilter.currentTitle {
            switch sAmountFilter {
            case "Selling Price".localized, "All".localized:
                return arrayToFilter
            case "0 - 500".localized:
                return filterFromDecimalNumbers(greaterThan: 0, lesserThan: 501)
            case "501 - 1,000".localized:
                return filterFromDecimalNumbers(greaterThan: 500.99, lesserThan: 1001)
            case "1,001 - 5,000".localized:
                return filterFromDecimalNumbers(greaterThan: 1000.99, lesserThan: 5001)
            case "5,001 - 10,000".localized:
                return filterFromDecimalNumbers(greaterThan: 5000.99, lesserThan: 10001)
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcess(forOperator arrayToFilter: [ReportResaleMainBalance]) -> [ReportResaleMainBalance] {
        func filterBy(string: String) -> [ReportResaleMainBalance] {
            return arrayToFilter.filter {
                guard let operatorName = $0.telcoName else { return false }
                return operatorName.lowercased() == string.lowercased()
            }
        }
        if let operatorFilter = buttonOperatorFilter.currentTitle {
            switch operatorFilter {
            case "Operator".localized, "All".localized:
                return arrayToFilter
            case "Mpt".localized:
                return filterBy(string: "Mpt")
            case "Telenor".localized:
                return filterBy(string: "Telenor")
            case "Ooredoo".localized:
                return filterBy(string: "Ooredoo")
            case "Mectel".localized:
                return filterBy(string: "Mectel")
            case "Mytel".localized:
                return filterBy(string: "Mytel")
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcess(forStatus arrayToFilter: [ReportResaleMainBalance]) -> [ReportResaleMainBalance] {
        func filterBy(string: String) -> [ReportResaleMainBalance] {
            return arrayToFilter.filter {
                guard let status = $0.status else { return false }
                return status.lowercased() == string.lowercased()
            }
        }
        if let statusFilter = buttonStatusFilter.currentTitle {
            switch statusFilter {
            case "Status".localized, "All".localized:
                return arrayToFilter
            case "Added".localized:
                return filterBy(string: "Added")
            case "Completed".localized:
                return filterBy(string: "Completed")
            case "Failed".localized:
                return filterBy(string: "Failed")
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcess(forPeriod arrayToFilter: [ReportResaleMainBalance]) -> [ReportResaleMainBalance] {
        if let periodFilter = buttonPeriodFilter.currentTitle {
            switch periodFilter {
            case "Period".localized, "All".localized:
                return arrayToFilter
            case "Date".localized:
                return arrayToFilter.filter {
                    guard let tDate = $0.createDate else { return false }
                    let transDateStr = tDate.stringValue(dateFormatIs: "dd MMM yyyy")
                    guard let transactionDate = transDateStr.dateValue(dateFormatIs: "dd MMM yyyy") else { return false }
                    guard let dateStartStr = buttonDateOne.currentTitle else { return false }
                    guard let dateEndStr = buttonDateTwo.currentTitle else { return false }
                    guard let exactDateStart = dateStartStr.dateValue(dateFormatIs: "dd MMM yyyy") else { return false }
                    guard let dateStart = Calendar.current.date(byAdding: Calendar.Component.day, value: -1, to: exactDateStart) else { return false }
                    guard let exactDateEnd = dateEndStr.dateValue(dateFormatIs: "dd MMM yyyy") else { return false }
                    guard let dateEnd = Calendar.current.date(byAdding: Calendar.Component.day, value: 1, to: exactDateEnd) else { return false}
                    return (dateStart.compare(transactionDate) == .orderedAscending && transactionDate.compare(dateEnd) == .orderedAscending)
                }
            case "Month".localized:
                return arrayToFilter.filter {
                    guard let transDate = $0.createDate else { return false }
                    let transDateStr = transDate.stringValue(dateFormatIs: "MMM yyyy")
                    guard let transactionDate = transDateStr.dateValue(dateFormatIs: "MMM yyyy") else { return false }
                    guard let dateStr = buttonDateTwo.currentTitle else { return false }
                    guard let monthToFilter = dateStr.dateValue(dateFormatIs: "MMM yyyy") else { return false }
                    return transactionDate.compare(monthToFilter) == .orderedSame
                }
            default:
                return []
            }
        }
        return []
    }
    
    func showDateOptions(selectedOption: String) {
        switch selectedOption {
        case "Period".localized, "All".localized:
            buttonPeriodFilter.setTitle(selectedOption, for: .normal)
            applyFilter()
            isDateViewHidden = true
        case "Date".localized:
            buttonDateOne.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            buttonDateTwo.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "dd MMM yyyy"), for: .normal)
            labelDateOne.isHidden = false
            buttonDateOne.isHidden = false
            labelDateTwo.text = "To Date".localized
            configureDatePicker(tag: 0)
            isDateViewHidden = false
        case "Month".localized:
            buttonDateOne.setTitle(nil, for: .normal)
            buttonDateTwo.setTitle(Date().stringValueWithOutUTC(dateFormatIs: "MMM yyyy"), for: .normal)
            labelDateOne.isHidden = true
            buttonDateOne.isHidden = true
            labelDateTwo.text = "Month".localized
            configureDatePicker(tag: 2)
            isDateViewHidden = false
        default:
            break
        }
    }
    
    // MARK: - Button Actions
    // It filters and show the detail by selling amount
    @IBAction func filterSellingAmount(_ sender: UIButton) {
        self.view.endEditing(true)
        func showFilterAtSellingAmount() {
            filterArray = sellingAmountFilters
            currentFilterType = .sellingAmount
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 25
            self.view.layoutIfNeeded()
        }
        if tableViewFilter.isHidden {
            showFilterAtSellingAmount()
        } else {
            switch currentFilterType {
            case .sellingAmount:
                tableViewFilter.isHidden = true
            default:
                showFilterAtSellingAmount()
            }
        }
    }
    
    // It filters and show the detail by selling price
    @IBAction func filterSellingPrice(_ sender: UIButton) {
        self.view.endEditing(true)
        func showFilterAtSellingPrice() {
            filterArray = sellingPriceFilters
            currentFilterType = .sellingPrice
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 235
            self.view.layoutIfNeeded()
        }
        if tableViewFilter.isHidden {
            showFilterAtSellingPrice()
        } else {
            switch currentFilterType {
            case .sellingPrice:
                tableViewFilter.isHidden = true
            default:
                showFilterAtSellingPrice()
            }
        }
    }
    
    // It filters and show the detail by operator
    @IBAction func filterByOperator(_ sender: UIButton) {
        self.view.endEditing(true)
        func showFilterAtOperator() {
            filterArray = operatorFilters
            currentFilterType = .operatorFilter
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 455
            self.view.layoutIfNeeded()
        }
        if tableViewFilter.isHidden {
            showFilterAtOperator()
        } else {
            switch currentFilterType {
            case .operatorFilter:
                tableViewFilter.isHidden = true
            default:
                showFilterAtOperator()
            }
        }
    }
    
    // It filters and show the detail by offers
    @IBAction func filterByStatus(_ sender: UIButton) {
        self.view.endEditing(true)
        func showFilterAtStatus() {
            filterArray = statusFilters
            currentFilterType = .status
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 665
            self.view.layoutIfNeeded()
        }
        if tableViewFilter.isHidden {
            showFilterAtStatus()
        } else {
            switch currentFilterType {
            case .status:
                tableViewFilter.isHidden = true
            default:
                showFilterAtStatus()
            }
        }
    }
    
    // It filters and show the detail by period
    @IBAction func filterByPeriod(_ sender: UIButton) {
        self.view.endEditing(true)
        func showFilterAtPeriod() {
            filterArray = periodFilters
            currentFilterType = .period
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 875
            self.view.layoutIfNeeded()
        }
        if tableViewFilter.isHidden {
            showFilterAtPeriod()
        } else {
            switch currentFilterType {
            case .period:
                tableViewFilter.isHidden = true
            default:
                showFilterAtPeriod()
            }
        }
    }
    
    @IBAction func sortBySourceNumber(_ sender: UIButton) {
        func sortSNumber(orderType: ComparisonResult) -> [ReportResaleMainBalance]{
            return resaleList.sorted {
                guard let sNum1 = $0.phoneNumber, let sNum2 = $1.phoneNumber else { return false }
                return sNum1.localizedStandardCompare(sNum2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            searchSort()
            resaleList = sortSNumber(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            searchSort()
            resaleList = sortSNumber(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewReSaleMainBalance.reloadData()
    }
    
    @IBAction func sortByDestinationNumber(_ sender: UIButton) {
        func sortDNumber(orderType: ComparisonResult) -> [ReportResaleMainBalance]{
            return resaleList.sorted {
                guard let dNum1 = $0.destinationNumber, let dNum2 = $1.destinationNumber else { return false }
                return dNum1.localizedStandardCompare(dNum2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            searchSort()
            resaleList = sortDNumber(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            searchSort()
            resaleList = sortDNumber(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewReSaleMainBalance.reloadData()
    }
    
    @IBAction func sortBySellingAmount(_ sender: UIButton) {
        func sortAmount(orderType: ComparisonResult) -> [ReportResaleMainBalance]{
            return resaleList.sorted {
                guard let amount1 = $0.airtimeAmount, let amount2 = $1.airtimeAmount else { return false }
                return amount1.compare(amount2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            searchSort()
            resaleList = sortAmount(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            searchSort()
            resaleList = sortAmount(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewReSaleMainBalance.reloadData()
    }
    
    @IBAction func sortBySellingPrice(_ sender: UIButton) {
        func sortAmount(orderType: ComparisonResult) -> [ReportResaleMainBalance]{
            return resaleList.sorted {
                guard let amount1 = $0.sellingAmount, let amount2 = $1.sellingAmount else { return false }
                return amount1.compare(amount2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            searchSort()
            resaleList = sortAmount(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            searchSort()
            resaleList = sortAmount(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewReSaleMainBalance.reloadData()
    }
    
    @IBAction func sortByStatus(_ sender: UIButton) {
        func sortStatus(orderType: ComparisonResult) -> [ReportResaleMainBalance]{
            return resaleList.sorted {
                guard let status1 = $0.status, let status2 = $1.status else { return false }
                return status1.localizedStandardCompare(status2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            searchSort()
            resaleList = sortStatus(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            searchSort()
            resaleList = sortStatus(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewReSaleMainBalance.reloadData()
    }
    
    @IBAction func sortByDate(_ sender: UIButton) {
        func sortDates(orderType: ComparisonResult) -> [ReportResaleMainBalance]{
            return resaleList.sorted {
                guard let date1 = $0.createDate, let date2 = $1.createDate else { return false }
                return date1.compare(date2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            searchSort()
            resaleList = sortDates(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            searchSort()
            resaleList = sortDates(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewReSaleMainBalance.reloadData()
    }
    @IBAction func configureDatePickerOne(_ sender: UIButton) {
        configureDatePicker(tag: 0)
    }
    
    @IBAction func configureDatePickerTwo(_ sender: UIButton) {
        if buttonDateOne.isHidden {
            configureDatePicker(tag: 2)
        } else {
            configureDatePicker(tag: 1)
        }
    }
    
    @IBAction func shareAllTransactionInExcel(_ sender: UIButton) {
        viewShadow.isHidden = true
        viewToShareAllRecords.isHidden = true
        removeGestureFromShadowView()
        var sellingAmt: NSDecimalNumber = 0
         var recAmt: NSDecimalNumber = 0
        if resaleList.count == 0 {
            return
        }
        var excelTxt = "Source Number,Destination Number,Selling Amount (MMK),Selling Price (MMK),Status,Date & Time\n"
        for record in resaleList {
            var recText = ""
            var sPhNo = ""
            if let mobileNo = record.phoneNumber {                
                var phNumber = mobileNo.replaceFirstTwoChar(withStr: "+")
                phNumber.insert(")", at: phNumber.index(phNumber.startIndex, offsetBy: 3))
                phNumber.insert("(", at: phNumber.startIndex)
                if UserModel.shared.mobileNo.hasPrefix("0095") {
                    phNumber.insert("0", at: phNumber.index(phNumber.startIndex, offsetBy: 5))
                }
                sPhNo = phNumber
            }
            var dPhno = ""
            if let mobileNo = record.destinationNumber {
                var phNumber = mobileNo.replaceFirstTwoChar(withStr: "+")
                phNumber.insert(")", at: phNumber.index(phNumber.startIndex, offsetBy: 3))
                phNumber.insert("(", at: phNumber.startIndex)
                if UserModel.shared.mobileNo.hasPrefix("0095") {
                    phNumber.insert("0", at: phNumber.index(phNumber.startIndex, offsetBy: 5))
                }
                dPhno = phNumber
            }
            var sellPrice = ""
            if let sellingPrice = record.sellingAmount {
                sellingAmt = sellingAmt.adding(sellingPrice)
                sellPrice = sellingPrice.toString()
            }
            var sellAmount = ""
            if let sellingAmount = record.airtimeAmount {
                recAmt = recAmt.adding(sellingAmount)
                sellAmount = sellingAmount.toString()
            }
            var status = ""
            if let stats = record.status {
                status = stats
            }
            var date = ""
            if let rDate = record.createDate {
                date = rDate.stringValue(dateFormatIs: "EEE dd-MMM-yyyy HH:mm:ss")
            }
            recText = recText + "\(sPhNo),\(dPhno),\(sellAmount),\(sellPrice),\(status),\(date)\n"
            excelTxt.append(recText)
        }
        excelTxt.append("\n\nTotal,,\(sellingAmt.toString()),\(recAmt.toString())")
        guard let excelUrl = CodeSnippets.generateExcel(excelDataWithFormat: excelTxt, excelFileName: "OK$ Resale Main Balance Report") else {
            println_debug("Error - excel not generated")
            return
        }
        let activityViewController = UIActivityViewController(activityItems: [excelUrl], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
    
    @IBAction func shareAllTransactionsInPdf(_ sender: UIButton) {
        viewShadow.isHidden = true
        viewToShareAllRecords.isHidden = true
        removeGestureFromShadowView()
        
        if resaleList.count == 0 {
            return
        }
        
        let pdfView = UIView()
        if let pdfHeaderView = Bundle.main.loadNibNamed("ReportResalePdfHeader", owner: self, options: nil)?.first as? ReportResalePdfHeader {
            pdfHeaderView.fillDetails(periodFilter: buttonPeriodFilter.currentTitle ?? "", dateOne: buttonDateOne.currentTitle ?? "", dateTwo: buttonDateTwo.currentTitle ?? "",
                                      sellAmountFilter: buttonSellingAmountFilter.currentTitle ?? "", sellPriceFilter: buttonSellingPriceFilter.currentTitle ?? "",
                                      operatorFilter: buttonOperatorFilter.currentTitle ?? "", statusFilter: buttonStatusFilter.currentTitle ?? "", record: resaleList, currentBalance: CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: String(walletAmount())))
            
            let newHeight = pdfHeaderView.viewForResaleReportRecHeader.frame.maxY
            pdfHeaderView.frame = CGRect(x: 0, y: 0, width: pdfHeaderView.frame.size.width, height: newHeight)
            pdfView.addSubview(pdfHeaderView)
            pdfView.frame = pdfHeaderView.bounds
            let xPos: CGFloat = pdfHeaderView.viewForResaleReportRecHeader.frame.minX
            var yPos: CGFloat = pdfView.frame.maxY
            
            for record in resaleList {
                if let pdfBodyView = Bundle.main.loadNibNamed("ReportResalePdfBody", owner: self, options: nil)?.first as? ReportResalePdfBody {
                    pdfBodyView.fillDetails(record: record)
                    let recordHeight = pdfBodyView.frame.size.height
                    let yPosInPage = Int(yPos) % 842
                    if (yPosInPage + Int(recordHeight)) > 842 {
                        yPos = yPos + CGFloat((842 - yPosInPage) + 30 + 30)
                    }
                    pdfBodyView.frame = CGRect(x: xPos, y: yPos, width: pdfBodyView.frame.size.width, height: recordHeight)
                    yPos = yPos + recordHeight + 2
                    pdfView.addSubview(pdfBodyView)
                }
            }
            pdfView.frame = CGRect(x: 0, y: 0, width: pdfView.frame.size.width, height: yPos + 30)
            guard let pdfUrl = CodeSnippets.generatePDF(viewForPDF: pdfView, pdfFile: "OK$ Resale Main Balance Report") else {
                println_debug("Error - pdf not generated")
                return
            }
            let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
            DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
        }
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ReportResaleMainBalanceController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        switch tableView.tag {
        case 0:
            if resaleList.count == 0 {
                let noDataLabel = UILabel(frame: tableView.bounds)
                noDataLabel.text = "No Records".localized
                noDataLabel.font = UIFont(name: appFont, size: 18)
                noDataLabel.textAlignment = .center
                tableView.backgroundView = noDataLabel
            } else {
                tableView.backgroundView = nil
            }
        default:
            break
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView.tag {
        case 0:
            return resaleList.count
        case 1:
            return filterArray.count
        case 2:
            return moreOptionsArray.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView.tag {
        case 0:
            let reSaleMainBalanceCell = tableView.dequeueReusableCell(withIdentifier: reSaleMainBalanceCellId, for: indexPath) as? ReportReSaleMainBalanceCell
            reSaleMainBalanceCell?.configureResaleReportCell(resaleReportRecord: resaleList[indexPath.row])
            return reSaleMainBalanceCell!
        case 1:
            let filterCell = tableView.dequeueReusableCell(withIdentifier: ConstantsXibCell.filterTableViewCell, for: indexPath) as? FilterTableViewCell
            filterCell?.configureReSaleMainBalance(filterName: filterArray[indexPath.row])
            return filterCell!
        case 2:
            let filterCell = tableView.dequeueReusableCell(withIdentifier: ConstantsXibCell.filterTableViewCell, for: indexPath) as? FilterTableViewCell
            filterCell?.configureMoreOptionInTopUp(filterName: moreOptionsArray[indexPath.row])
            return filterCell!
        default:
            let reSaleMainBalanceCell = tableView.dequeueReusableCell(withIdentifier: reSaleMainBalanceCellId, for: indexPath) as? ReportReSaleMainBalanceCell
            return reSaleMainBalanceCell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView.tag {
        case 0:
            break
        case 1:
            switch currentFilterType {
            case .sellingAmount:
                buttonSellingAmountFilter.setTitle(filterArray[indexPath.row].localized, for: .normal)
                tableViewFilter.isHidden = true
                applyFilter()
            case .sellingPrice:
                buttonSellingPriceFilter.setTitle(filterArray[indexPath.row].localized, for: .normal)
                tableViewFilter.isHidden = true
                applyFilter()
            case .operatorFilter:
                buttonOperatorFilter.setTitle(filterArray[indexPath.row].localized, for: .normal)
                tableViewFilter.isHidden = true
                applyFilter()
            case .status:
                buttonStatusFilter.setTitle(filterArray[indexPath.row].localized, for: .normal)
                tableViewFilter.isHidden = true
                applyFilter()
            case .period:
                tableViewFilter.isHidden = true
                showDateOptions(selectedOption: filterArray[indexPath.row].localized)
            }
        case 2:
            viewShadow.isHidden = false
            switch indexPath.row {
            case 0:
                if let image = UIImage(named: "topUpCashBack"){
                    imgViewInCircle.image = image
                }
                labelInCircle.text = "Total Selling Price".localized + " " + "(MMK)".localized
                amountInCircle.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: totalSellingPrice.toString())
            case 1:
                if let image = UIImage(named: "topUpCashBack"){
                    imgViewInCircle.image = image
                }
                labelInCircle.text = "Total Selling Amount".localized + " " + "(MMK)".localized
                amountInCircle.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: totalSellingAmount.toString())
            case 2:
                if let image = UIImage(named: "totalTransaction"){
                    imgViewInCircle.image = image
                    imgViewInCircle.image = imgViewInCircle.image!.withRenderingMode(.alwaysTemplate)
                    imgViewInCircle.tintColor = UIColor(red: 49.0/255.0, green: 154.0/255.0, blue: 202.0/255.0, alpha: 1.0)
                }
                labelInCircle.text = "Total Transactions".localized
                amountInCircle.text = "\(totalTransection)"
            default:
                break
            }
            viewCircle.isHidden = false
            tableViewOption.isHidden = true
            circleViewTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: false)
        default:
            break
        }
    }
}

// MARK: - UITextFieldDelegate
extension ReportResaleMainBalanceController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tableViewFilter.isHidden = true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        tableViewReSaleMainBalance.contentOffset = CGPoint(x: 0, y: 0)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            if updatedText.count > 50 || (updatedText == "" && text == "") {
                return false
            }
            if string != "\n" {
                applyFilter(searchText: updatedText)
            }
        }
        return true
    }
}

// MARK: - Additional Methods
extension ReportResaleMainBalanceController: NavigationSetUpProtocol {
    /// It set up the navigation bar
    func setUpNavigation() {
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationController?.navigationBar.barTintColor        = ConstantsColor.navigationHeaderReSale
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: titleViewWidth, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        
        let searchButton = UIButton(type: .custom)
        searchButton.setImage(#imageLiteral(resourceName: "reportSearch").withRenderingMode(.alwaysOriginal), for: .normal)
        searchButton.addTarget(self, action: #selector(showSearchView), for: .touchUpInside)
        
        let shareButton = UIButton(type: .custom)
        shareButton.setImage(#imageLiteral(resourceName: "reportShare").withRenderingMode(.alwaysOriginal), for: .normal)
        shareButton.addTarget(self, action: #selector(showShareOption), for: .touchUpInside)
        
        let moreButton = UIButton(type: .custom)
        moreButton.setImage(#imageLiteral(resourceName: "topUpMore").withRenderingMode(.alwaysOriginal), for: .normal)
        moreButton.addTarget(self, action: #selector(showMoreOption), for: .touchUpInside)
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        let ypos = (titleViewSize.height - 30 ) / 2
        if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 180, y: ypos, width: 30, height: 30)
            shareButton.frame = CGRect(x: navTitleView.frame.size.width - 140, y: ypos, width: 30, height: 30)
            moreButton.frame = CGRect(x: navTitleView.frame.size.width - 100, y: ypos, width: 30, height: 30)
            backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        }
        else{
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 110, y: ypos, width: 30, height: 30)
            shareButton.frame = CGRect(x: navTitleView.frame.size.width - 70, y: ypos, width: 30, height: 30)
            moreButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
            backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
            
        }
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 240, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text = ConstantsController.reportResaleMainBalanceController.screenTitle.localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 23) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        navTitleView.addSubview(searchButton)
        navTitleView.addSubview(shareButton)
        navTitleView.addSubview(moreButton)
        self.navigationItem.titleView = navTitleView
    }
    
    /// It shows the home screen
    @objc func backAction() {
//        AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
//        self.navigationController?.popViewController(animated: true)
        if isSearchViewShow {
            hideSearchView()
        } else {
            AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension ReportResaleMainBalanceController: ReportModelDelegate {
    func apiResult(message: String?, statusCode: String?) {
        DispatchQueue.main.async {
            self.resaleList = self.modelReport.resaleMainBalanceReports
            self.resetSortButtons(sender: UIButton())
            self.resetFilterButtons()
            self.tableViewReSaleMainBalance.reloadData()
            self.refreshControl.endRefreshing()
            self.getTotalSellingPriceAmount()
        }
    }
    
    func handleCustomError(error: CustomError) {
        DispatchQueue.main.async {
            self.refreshControl.endRefreshing()
            switch error {
            case .noInternet, .serverNotReachable:
                let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            default:
                println_debug("Abonded error")
            }
        }
    }
}
