//
//  ReportSolarListExtension.swift
//  OK
//
//  Created by E J ANTONY on 26/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

extension ReportSolarListViewController {
    //Methods
    func resetFilterButtons() {
        buttonFilterAmount.setTitle(amountTypes[0].localized, for: .normal)
        buttonFilterPeriod.setTitle(periodTypes[0].localized, for: .normal)
    }
    
    func noRecordAlertAction() {
        alertViewObj.wrapAlert(title: "Alert".localized, body: "No Records".localized, img: UIImage(named : "AppIcon"))
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
        })
        alertViewObj.showAlert(controller: self)
    }
    
    func updateFilterTableHeight() {
        constraintFilterTableHeight.constant = filterArray.count > 4 ? CGFloat(5*44) : CGFloat(filterArray.count * 44)
        self.view.layoutIfNeeded()
    }
    
    func resetSortButtons(sender: UIButton) {
        if sender != btnSortName {
            btnSortName.tag = 0
            btnSortName.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != btnSortTransID {
            btnSortTransID.tag = 0
            btnSortTransID.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != btnSortAmount {
            btnSortAmount.tag = 0
            btnSortAmount.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != btnSortServiceFee {
            btnSortServiceFee.tag = 0
            btnSortServiceFee.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != btnSortAccNumber {
            btnSortAccNumber.tag = 0
            btnSortAccNumber.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != btnSortStatus {
            btnSortStatus.tag = 0
            btnSortStatus.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != btnSortDate {
            btnSortDate.tag = 0
            btnSortDate.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
    }
    
    func setLocalizedTitles() {
        btnSortName.setTitle("Name".localized, for: .normal)
        btnSortTransID.setTitle("Transaction ID".localized, for: .normal)
        btnSortAmount.setTitle("Amount".localized  + " (MMK)".localized, for: .normal)
        btnSortServiceFee.setTitle("Service Fees".localized  + " (MMK)".localized, for: .normal)
        btnSortAccNumber.setTitle("Account No".localized, for: .normal)
        btnSortStatus.setTitle("Status".localized, for: .normal)
        btnSortDate.setTitle("Date & Time".localized, for: .normal)
    }
    
    func shareRecordsByPdf() {
        let pdfView = UIView()
        if let pdfHeaderView = Bundle.main.loadNibNamed("ReportSolarPdfHeader", owner: self, options: nil)?.first as? ReportSolarPdfHeader {

            pdfHeaderView.fillDetails(solarReports: reportSolarList, filterPeriod: buttonFilterPeriod.currentTitle ?? "", filterAmountType: buttonFilterAmount.currentTitle ?? "", currentBalance: CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: String(walletAmount())))

            let newHeight = pdfHeaderView.viewForFilter.frame.maxY
            pdfHeaderView.frame = CGRect(x: 0, y: 0, width: pdfHeaderView.frame.size.width, height: newHeight)
            pdfView.addSubview(pdfHeaderView)
            pdfView.frame = pdfHeaderView.bounds
            let xPos: CGFloat = pdfHeaderView.viewForFilter.frame.minX
            var yPos: CGFloat = pdfView.frame.maxY

            for record in reportSolarList {
                if let pdfBodyView = Bundle.main.loadNibNamed("ReportSolarPdfBody", owner: self, options: nil)?.first as? ReportSolarPdfBody {
                    pdfBodyView.configureData(report: record)
                    let recordHeight = pdfBodyView.frame.size.height
                    let yPosInPage = Int(yPos) % 842
                    if (yPosInPage + Int(recordHeight)) > 842 {
                        yPos = yPos + CGFloat((842 - yPosInPage) + 30 + 30)
                    }
                    pdfBodyView.frame = CGRect(x: xPos, y: yPos, width: pdfBodyView.frame.size.width, height: recordHeight)
                    yPos = yPos + recordHeight + 2
                    pdfView.addSubview(pdfBodyView)
                }
            }
            pdfView.frame = CGRect(x: 0, y: 0, width: pdfView.frame.size.width, height: yPos + 30)
            guard let pdfUrl = CodeSnippets.generatePDF(viewForPDF: pdfView, pdfFile: "OK$ Solar Reports") else {
                println_debug("Error - pdf not generated")
                return
            }
            let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
            
            if self.presentedViewController != nil {
                self.dismiss(animated: false, completion: {
                    [unowned self] in
                    DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
                })
            }else{
                DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
            }
        }
    }
    
    func shareRecordsByExcel() {
        var excelTxt = "" //"Selected Filter:\n\nAmount,Period\n"
        var typeFilter = ""
        if let voteTypeFilter = buttonFilterAmount.currentTitle {
            if voteTypeFilter  == "Amount".localized {
                typeFilter = "All".localized
            } else {
                typeFilter = voteTypeFilter.localized
            }
        }
        var periodFilter = ""
        if let peridFilter = buttonFilterPeriod.currentTitle {
            if peridFilter  == "Period".localized {
                periodFilter = "All".localized
            } else {
                periodFilter = peridFilter.localized
            }
        }
        periodFilter = periodFilter.replacingOccurrences(of: "\n", with: " ")
//        excelTxt.append("\(typeFilter),\(periodFilter)\n\n")
//        excelTxt.append("Selected Filter Records:\n\n")
        excelTxt.append("Name,Transaction ID,Amount (MMK), Service Fees (MMK), Account Number, Status, Date & Time\n")
        var totalTransAmount: Double = 0
        var totalServiceFees: Double = 0
        for record in reportSolarList {
            var name = record.customer?.customerName ?? ""
            name = name.replacingOccurrences(of: ",", with: ".")
            let transID = record.customerOkPaymentID ?? ""
            var amount = ""
            if let amnt = record.amount {
                amount = "\(amnt)"
                totalTransAmount += amnt
            }
            var sFees = ""
            if let sCharge = record.serviceCharges {
                sFees = "\(sCharge)"
                totalServiceFees += sCharge
            }
            let accNo = record.solarHomeAccountNumber ?? ""
            let status = record.topupStatus ?? ""
            var dateStr = ""
            if let createdDateStr = record.createdDate {
                if let dateValue = createdDateStr.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") {
                    let dateString = dateValue.stringValue(dateFormatIs: "EEE dd-MMM-yyyy HH:mm:ss")
                    dateStr = dateString
                }
            }
            
            var recText = ""
            recText = "\(name),\(transID),\(amount),\(sFees),\(accNo),\(status),\(dateStr)\n"
            excelTxt.append(recText)
        }
        excelTxt.append("Total,,\(totalTransAmount) MMK,\(totalServiceFees) MMK,,,,")
        guard let excelUrl = CodeSnippets.generateExcel(excelDataWithFormat: excelTxt, excelFileName: "OK$ Solar Reports") else {
            println_debug("Error - excel not generated")
            return
        }
        let activityViewController = UIActivityViewController(activityItems: [excelUrl], applicationActivities: nil)
        if self.presentedViewController != nil {
            self.dismiss(animated: false, completion: {
                [unowned self] in
                DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
            })
        }else{
            DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
        }
    }
    
    func getToken() {
        getTokenFromServer(completionHandler: { (isSuccess, tokenString) in
            guard isSuccess, let tokenAuthStr = tokenString as? String else {
                progressViewObj.removeProgressView()
                self.showErrorAlert(errMessage: "try again")
                return
            }
            DispatchQueue.main.async(execute: {
                self.modelAReport.getSolarElectricity(authToken: tokenAuthStr)
            })
        })
    }
    
    func getTokenFromServer(completionHandler: @escaping (Bool,Any?) -> Void) {
        
        let urlStr = String.init(format: Url.SolarElectricityTokenAPIURL)
        let url = URL.init(string: urlStr)
        guard let tokenUrl = url else {
            showErrorAlert(errMessage: "Something went wrong : Dev need to check")
            return
        }
        println_debug(tokenUrl)
        let hashValue = Url.aKey_solor_bill.hmac_SHA1(key: Url.sKey_solor_bill)
        let inputVal = "password=\(hashValue)&grant_type=password"
        
        progressViewObj.showProgressView()
        println_debug("get token from server called........")
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: kMethod_Post, contentType: kContentType_urlencoded, inputVal: inputVal, authStr: nil)
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    println_debug("remove progress view called inside get token from server call")
                }
                
                completionHandler(false,nil)
                return
            }
            
            println_debug("response dict for get token from server :: \(String(describing: response))")
            let responseDic = response as! NSDictionary
            guard let tokentype = responseDic.value(forKey: "token_type"), let accesstoken = responseDic.value(forKey: "access_token") else {
                
                completionHandler(false,nil)
                return
            }
            
            let authorizationString =  "\(tokentype) \(accesstoken)"
            completionHandler(true,authorizationString)
        })
    }
    
    //MARK: - Button Action Methods
    @IBAction func sortByName(sender: UIButton) {
        func sortByName(orderType: ComparisonResult) -> [SolarReport]{
            return reportSolarList.sorted {
                guard let name1 = $0.customer?.customerName, let name2 = $1.customer?.customerName else { return false }
                return name1.lowercased().localizedStandardCompare(name2.lowercased()) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            reportSolarList = sortByName(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            reportSolarList = sortByName(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByTransID(sender: UIButton) {
        func sortByTransID(orderType: ComparisonResult) -> [SolarReport]{
            return reportSolarList.sorted {
                guard let transID1 = $0.customerOkPaymentID, let transID2 = $1.customerOkPaymentID else { return false }
                return transID1.localizedStandardCompare(transID2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            reportSolarList = sortByTransID(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            reportSolarList = sortByTransID(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByAmount(sender: UIButton) {
        func sortAmount(orderType: ComparisonResult) -> [SolarReport]{
            switch orderType {
            case .orderedAscending:
                return reportSolarList.sorted {
                    guard let amount1 = $0.amount, let amount2 = $1.amount else { return false }
                    return amount1 < amount2
                }
            case .orderedDescending:
                return reportSolarList.sorted {
                    guard let amount1 = $0.amount, let amount2 = $1.amount else { return false }
                    return amount1 > amount2
                }
            default:
                return reportSolarList
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            reportSolarList = sortAmount(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            reportSolarList = sortAmount(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByServiceFee(sender: UIButton) {
        func sortServiceFee(orderType: ComparisonResult) -> [SolarReport]{
            switch orderType {
            case .orderedAscending:
                return reportSolarList.sorted {
                    guard let amount1 = $0.serviceCharges, let amount2 = $1.serviceCharges else { return false }
                    return amount1 < amount2
                }
            case .orderedDescending:
                return reportSolarList.sorted {
                    guard let amount1 = $0.serviceCharges, let amount2 = $1.serviceCharges else { return false }
                    return amount1 > amount2
                }
            default:
                return reportSolarList
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            reportSolarList = sortServiceFee(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            reportSolarList = sortServiceFee(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByAccNum(sender: UIButton) {
        func sortByAccNo(orderType: ComparisonResult) -> [SolarReport]{
            return reportSolarList.sorted {
                guard let accNo1 = $0.solarHomeAccountNumber, let accNo2 = $1.solarHomeAccountNumber else { return false }
                return accNo1.localizedStandardCompare(accNo2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            reportSolarList = sortByAccNo(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            reportSolarList = sortByAccNo(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByStatus(sender: UIButton) {
        func sortStatus(orderType: ComparisonResult) -> [SolarReport]{
            return reportSolarList.sorted {
                guard let status1 = $0.topupStatus, let status2 = $1.topupStatus else { return false }
                return status1.lowercased().localizedStandardCompare(status2.lowercased()) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            reportSolarList = sortStatus(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            reportSolarList = sortStatus(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByDate(sender: UIButton) {
        func sortDates(orderType: ComparisonResult) -> [SolarReport]{
            return reportSolarList.sorted {
                guard let date1 = $0.createdDate, let date2 = $1.createdDate else { return false }
                return date1.compare(date2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            reportSolarList = sortDates(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            reportSolarList = sortDates(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
}

// MARK: - UITextFieldDelegate
extension ReportSolarListViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tableViewFilter.isHidden = true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        tblSolarList.contentOffset = CGPoint(x: 0, y: 0)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if range.location == 0 && string == " " { return false }
        if let text = textField.text, let textRange = Range(range, in: text) {
            if text.last == " " && string == " " { return false }
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            if !(updatedText == updatedText.components(separatedBy: NSCharacterSet(charactersIn: SEARCHCHARSET).inverted).joined(separator: "")) { return false }
            
            let containsEmoji: Bool = updatedText.containsEmoji
            if (containsEmoji){
                return false
            }
            if updatedText.count > 50 || (updatedText == "" && text == "") {
                return false
            }
            if string != "\n" {
                applyFilter(searchText: updatedText)
            }
        }
        return true
    }
}

// MARK: - Additional Methods
extension ReportSolarListViewController: NavigationSetUpProtocol {
    /// It set up the navigation bar
    func setUpNavigation() {
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: titleViewWidth, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        
        let searchButton = UIButton(type: .custom)
        searchButton.setImage(#imageLiteral(resourceName: "reportSearch").withRenderingMode(.alwaysOriginal), for: .normal)
        searchButton.addTarget(self, action: #selector(showSearchView), for: .touchUpInside)
        
        let shareButton = UIButton(type: .custom)
        shareButton.setImage(#imageLiteral(resourceName: "reportShare").withRenderingMode(.alwaysOriginal), for: .normal)
        shareButton.addTarget(self, action: #selector(showShareOption), for: .touchUpInside)
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        let ypos = (titleViewSize.height - 30 ) / 2
        if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 140, y: ypos, width: 30, height: 30)
            shareButton.frame = CGRect(x: navTitleView.frame.size.width - 100, y: ypos, width: 30, height: 30)
            backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        }
        else{
        searchButton.frame = CGRect(x: navTitleView.frame.size.width - 70, y: ypos, width: 30, height: 30)
        shareButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        }
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 320, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text = ConstantsController.reportSolarListViewController.screenTitle.localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 23) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        navTitleView.addSubview(searchButton)
        navTitleView.addSubview(shareButton)
        self.navigationItem.titleView = navTitleView
    }
    
    /// It shows the home screen
    @objc func backAction() {
//        AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
//        self.navigationController?.popViewController(animated: true)
        if isSearchViewShow {
            hideSearchView()
        } else {
            AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
            self.navigationController?.popViewController(animated: true)
        }
    }
}
