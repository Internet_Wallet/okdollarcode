//
//  ReportNonOfferViewController.swift
//  OK
//
//  Created by iMac on 3/26/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class ReportNonOfferViewController: UIViewController {
   
    private lazy var modelBReport: ReportBModel = {
        return ReportBModel()
    }()
    
    private var multiButton : ActionButton?
    @IBOutlet weak var lblTitleDateTime: UILabel!{
        didSet{
            lblTitleDateTime.font = UIFont(name: appFont, size: appFontSizeReport)
            lblTitleDateTime.text = "Date".localized
        }
    }
    @IBOutlet weak var lblValueDateTime: UILabel!{
        didSet{
            lblValueDateTime.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblTitleAllType: UILabel!{
        didSet{
            lblTitleAllType.font = UIFont(name: appFont, size: appFontSizeReport)
            lblTitleAllType.text = "Transaction Type".localized
        }
    }
    @IBOutlet weak var lblValueAllType: UILabel!{
        didSet{
            lblValueAllType.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblTitleCollectionAmnt: UILabel!{
        didSet{
            lblTitleCollectionAmnt.font = UIFont(name: appFont, size: appFontSizeReport)
            lblTitleCollectionAmnt.text = "Total Received Amount".localized
        }
    }
    @IBOutlet weak var lblValueCollectionAmnt: UILabel!{
        didSet{
            lblValueCollectionAmnt.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblTitleAvalAmnt: UILabel!{
        didSet{
            lblTitleAvalAmnt.font = UIFont(name: appFont, size: appFontSizeReport)
            lblTitleAvalAmnt.text = "Total Available Amount".localized
        }
    }
    @IBOutlet weak var lblValueAvalAmnt: UILabel!{
        didSet{
            lblValueAvalAmnt.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblTitleDiscAmnt: UILabel!{
        didSet{
            lblTitleDiscAmnt.font = UIFont(name: appFont, size: appFontSizeReport)
            lblTitleDiscAmnt.text = "Total Discount Amount".localized
        }
    }
    @IBOutlet weak var lblValueDiscAmnt: UILabel!{
        didSet{
            lblValueDiscAmnt.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblTitleSaleQty: UILabel!{
        didSet{
            lblTitleSaleQty.font = UIFont(name: appFont, size: appFontSizeReport)
            lblTitleSaleQty.text = "Total Sale Quantity".localized
        }
    }
    @IBOutlet weak var lblValueSaleQty: UILabel!{
        didSet{
            lblValueSaleQty.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblTitleFreeQty: UILabel!{
        didSet{
            lblTitleFreeQty.font = UIFont(name: appFont, size: appFontSizeReport)
            lblTitleFreeQty.text = "Total Free Quantity".localized
        }
    }
    @IBOutlet weak var lblValueFreeQty: UILabel!{
        didSet{
            lblValueFreeQty.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    var fromDateString = Date().stringValueWithOutUTC(dateFormatIs: "yyyy-MM-dd")
    var toDateString = Date().stringValueWithOutUTC(dateFormatIs: "yyyy-MM-dd")
    var typeStr : String = "ALLTRAN"

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblValueAllType.text = "All Transaction".localized
        
        setUpNavigation()
        setRecvdAndDiscount()
        setSellingFreeQty()
        // Do any additional setup after loading the view.
        self.loadFloatButtons()
        
        lblValueDateTime.text =  Date().stringValueWithOutUTC(dateFormatIs: "E,dd-MMMM-yyyy")
        self.getDataWithDate()
    }

    //MARK:- API Request
    func getDataWithDate() {
        println_debug("Get Records")
        
        let request = NonOfferReporListDateRequestNew(mobileNumber: UserModel.shared.mobileNo, fromDate: fromDateString, toDate: toDateString, transactionRequestedType: typeStr,advanceMerchantNumber: "")
        
        modelBReport.reportBsummaryDelegate = self
        modelBReport.getNonOfferPromotionTransactionSummaryAPICalling(request: request)
    }
    
    //MARK:- Amount update based on api response
    func setSellingFreeQty(withSale: Int = 0, withFree: Int = 0) {
        lblValueSaleQty.text = "\(withSale)"
        lblValueFreeQty.text = "\(withFree)"
    }
    
    func setRecvdAndDiscount(withBillAmount: Double = 0.0, withRcvd: Double = 0.0, withDiscount: Double = 0.0) {
        lblValueCollectionAmnt.attributedText = CodeSnippets.getCurrencyInAttributed(withString: "\(withBillAmount)")
        lblValueAvalAmnt.attributedText = CodeSnippets.getCurrencyInAttributed(withString: "\(withRcvd)")
        lblValueDiscAmnt.attributedText = CodeSnippets.getCurrencyInAttributed(withString: "\(withDiscount)")
        
    }
    
    //MARK:- Floating Buttons
    func loadFloatButtons() {
        var buttonItems = [ActionButtonItem]()
        let filter = ActionButtonItem(title: "Filter".localized, image: #imageLiteral(resourceName: "offer_report_menu_icon_filter"))
        filter.action = { item in
            self.multiButton?.toggleMenu()
            guard let reportOfferFilterController = UIStoryboard(name: "ReportsB", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportNonOfferFilterController.nameAndID) as? ReportNonOfferFilterController else { return }
            
            reportOfferFilterController.strFromToDate = self.lblValueDateTime.text ?? ""
            reportOfferFilterController.strFromDate = self.fromDateString
            reportOfferFilterController.strToDate = self.toDateString
        self.navigationController?.pushViewController(reportOfferFilterController, animated: true)
        }
        buttonItems = [filter]
        
        multiButton = ActionButton(attachedToView: self.view, items: buttonItems)
        multiButton?.action = {
            button in button.toggleMenu()
        }
        multiButton?.setImage(#imageLiteral(resourceName: "menu_white_payto"), forState: .normal)
        multiButton?.backgroundColor = kYellowColor
    }

    @IBAction func selectedOption(sender: UIButton) {
        let selectedIndex = sender.tag
        
            switch selectedIndex {
            case 0:
                //Date & Time
                println_debug("Date & Time")
                guard let reportDatePickerController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportDatePickerController.nameAndID) as? ReportDatePickerController else { return }
                reportDatePickerController.dateMode = .fromToDate
                reportDatePickerController.delegate = self
                reportDatePickerController.viewOrientation = .portrait
                reportDatePickerController.modalPresentationStyle = .overCurrentContext
                self.present(reportDatePickerController, animated: false, completion: nil)
            case 1:
                //All Type
                println_debug("All Type")
             
            case 2:
                //Collection Amount
                println_debug("Collection Amount")
                navigateToAmountDetail(amountType: "totalReceivedAmount")
            case 3:
                //Avaialable Amount
                println_debug("Avaialable Amount")
                navigateToAmountDetail(amountType: "totalAvailableAmount")
            case 4:
                //Discount Amount
                println_debug("Discount Amount")
                navigateToAmountDetail(amountType: "totalDiscountAmount")
           default:
                break
            }
    }
    
    private func navigateToAdvanceMerchant(townshipIDParam: String, promotionIDParam: String) {
        guard let reportFilterController = UIStoryboard(name: "ReportsB", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportNonOfferAdvanceDummyController.nameAndID) as? ReportNonOfferAdvanceDummyController else { return }
        self.navigationController?.pushViewController(reportFilterController, animated: true)
    }
    
    private func navigateToAmountDetail(amountType: String?) {
        guard let reportFilterController = UIStoryboard(name: "ReportsB", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportNonOfferAmountDetailController.nameAndID) as? ReportNonOfferAmountDetailController else { return }
        reportFilterController.screenTypeName = "AdvanceMerchant"
        reportFilterController.fromdate = fromDateString
        reportFilterController.todate = toDateString
        reportFilterController.transactionType = typeStr
        reportFilterController.amountType = amountType!

    self.navigationController?.pushViewController(reportFilterController, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK:- All Transaction Type Selection
extension ReportNonOfferViewController: ReportNonOfferAllTypeDelegate {
    func processWithType(type : String) {
        println_debug(type)
        
        typeStr = type
        
        if type == "NONPROMOTIONTRAN" {
            lblValueAllType.text = "NON PROMOTION TRANSACTION"
        }
        else if type == "PROMOTIONTRAN" {
            lblValueAllType.text = "PROMOTION TRANSACTION"
        }
        else if type == "ALLTRAN" {
            lblValueAllType.text = "All Transaction".localized
        }
        
        self.getDataWithDate()

    }
}

//MARK:- From Date & To Date Selection Delegate
extension ReportNonOfferViewController: ReportDatePickerDelegate {
    func processWithDate(fromDate: Date?, toDate: Date?, dateMode: DateMode) {
        var selectedDate = ""
        switch dateMode {
        case .fromToDate:
            if let safeFDate = fromDate, let safeTDate = toDate {
                fromDateString = safeFDate.stringValue(dateFormatIs: "yyyy-MM-dd")
                toDateString = safeTDate.stringValue(dateFormatIs: "yyyy-MM-dd")
                
                if fromDateString == toDateString {
                    selectedDate = safeFDate.stringValue(dateFormatIs: "E,dd-MMM-yyyy")
                } else {
                    selectedDate = safeFDate.stringValue(dateFormatIs: "E,dd-MMM-yyyy") + " - " + safeTDate.stringValue(dateFormatIs: "E,dd-MMM-yyyy")
                }
                lblValueDateTime.text = selectedDate
                getDataWithDate()
            }
        default:
            break
        }
    }
}

extension ReportNonOfferViewController: ReportBModelTransactionsummaryDelegate {
    
    func handleCustomError(error: CustomError) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
    
    func apiResultnonofferSummary(message: [String:Any], statusCode: String?){
        
        print("apiResultnonofferSummarymethod called-----\(message)")
        
        guard let collectionamount = message.safeValueForKey("totalReceivedAmount") else { return }
        
        guard let discountamount = message.safeValueForKey("totalAvailableAmount") else { return }
        
        guard let finalamount = message.safeValueForKey("totalDiscountAmount") else  { return }
        
        self.setRecvdAndDiscount(withBillAmount: collectionamount as! Double, withRcvd: discountamount as! Double, withDiscount: finalamount as! Double)
        
        guard let salequntity = message.safeValueForKey("totalBuyingQty") else { return }
        
        guard let freequntity = message.safeValueForKey("totalFreeQty") else { return }
        
        self.setSellingFreeQty(withSale: salequntity as! Int, withFree: freequntity as! Int)
    }
}

// MARK: - Additional Methods
extension ReportNonOfferViewController: NavigationSetUpProtocol {
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  ConstantsController.reportNonOfferViewController.screenTitle.localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
}

