//
//  ReportSharingViewController.swift
//  OK
//
//  Created by E J ANTONY on 25/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
protocol ReportSharingOptionDelegte {
    func share(by: ReportSharingViewController.ReportShareOption)
}

class ReportSharingViewController: UIViewController {

    //MARK: - Outlet
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var buttonPdfShare: UIButton!
    @IBOutlet weak var buttonExcelShare: UIButton!{
        didSet {
            buttonExcelShare.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var lblPdfShare: UILabel!{
        didSet{
            
            lblPdfShare.font = UIFont(name: appFont, size: appFontSizeReport)
            lblPdfShare.text = "PDF".localized
        }
    }
    @IBOutlet weak var lblExcelShare: UILabel!{
        didSet{
           
            lblExcelShare.font = UIFont(name: appFont, size: appFontSizeReport)
            lblExcelShare.text = "Excel".localized
        }
    }

    @IBOutlet weak var constraintViewW: NSLayoutConstraint!
    @IBOutlet weak var constraintViewH: NSLayoutConstraint!
    @IBOutlet weak var constraintCentreYPdf: NSLayoutConstraint!
    @IBOutlet weak var constraintCentreYExc: NSLayoutConstraint!
    
    //MARK: - Properties
    var pdfImage: UIImage?
    var excelImage: UIImage?
    var delegate: ReportSharingOptionDelegte?
    
    enum ReportShareOption {
       case pdf, excel
    }
    
    //MARK: - Views Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        if UIDevice.current.orientation.isPortrait {
            constraintViewW.constant = 285
            constraintViewH.constant = 200
            constraintCentreYPdf.constant = -15
            constraintCentreYExc.constant = -15
            self.view.layoutIfNeeded()
        }
        if let pdfButtonImage = pdfImage {
            self.buttonPdfShare.setImage(pdfButtonImage, for: .normal)
        }
        if let excelButtonImage = excelImage {
            self.buttonExcelShare.setImage(excelButtonImage, for: .normal)
        }
        addGesture()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Methods
    private func addGesture() {
        let tapGestToRemoveView = UITapGestureRecognizer(target: self, action: #selector(shadowIsTapped))
        viewShadow.addGestureRecognizer(tapGestToRemoveView)
    }
    
    //MARK: - Target Methods
    @objc func shadowIsTapped(sender: UITapGestureRecognizer) {
        dismissPickerController()
    }
    
    @objc func dismissPickerController() {
        self.dismiss(animated: false, completion: nil)
    }
    
    //MARK: - Button Action Methods
    @IBAction func shareByPdf(_ sender: UIButton) {
        self.delegate?.share(by: .pdf)
        dismissPickerController()
    }
    
    @IBAction func shareByExcel(_ sender: UIButton) {
        self.delegate?.share(by: .excel)
        dismissPickerController()
    }
    
    deinit {
        if let recognizers = viewShadow.gestureRecognizers {
            for recognizer in recognizers {
                if let recognizer = recognizer as? UITapGestureRecognizer {
                    viewShadow.removeGestureRecognizer(recognizer)
                }
            }
        }
    }
}
