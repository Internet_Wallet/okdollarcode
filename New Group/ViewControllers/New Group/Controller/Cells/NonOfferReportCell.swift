//
//  NonOfferReportCell.swift
//  OK
//
//  Created by iMac on 4/5/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class NonOfferReportCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var imgViewUser: UIImageView!
    @IBOutlet weak var labelTransaction: UILabel!{
        didSet{
            self.labelTransaction.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAmount: UILabel!{
        didSet{
            self.labelAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var imgViewFlag: UIImageView!
    @IBOutlet weak var imgViewArrow: UIImageView!
    @IBOutlet weak var labelMobileNo: UILabel!{
        didSet{
            self.labelMobileNo.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelName: UILabel!{
        didSet{
            self.labelName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelTransType: UILabel!{
        didSet{
            self.labelTransType.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelBonusPoints: UILabel!{
        didSet{
            self.labelBonusPoints.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelCashBack: UILabel!{
        didSet{
            self.labelCashBack.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelBalance: UILabel!{
        didSet{
            self.labelBalance.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDate: UILabel!{
        didSet{
            self.labelDate.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDescription: UILabel!{
        didSet{
            self.labelDescription.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonPdf: UIButton!{
        didSet{
            self.buttonPdf.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonQR: UIButton!{
        didSet{
            self.buttonQR.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelBusiName: UILabel!{
        didSet{
            self.labelBusiName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewContent.layer.cornerRadius = 5
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureTransactionItemCell(transRecord: NonOfferReportList?) {
        //var isAmountGTZ = false
        labelMobileNo.text = ""
        labelName.text = ""
        labelBusiName.text = ""
        labelCashBack.text = "0.00" //For receiver side, since no cashback for receiver
        labelTransType.text = transRecord?.transType
        labelTransaction.text = transRecord?.transID
        labelDescription.text = ""

        var tType = ""
        if let desc = transRecord?.rawDesc {
            if desc.contains("#OK") {
                if desc.contains("Top-Up") || desc.contains("TopUp") || desc.contains("TopUpPlan") || desc.contains("Bonus Point Top Up") {
                    tType = "Top-Up"
                } else if desc.contains("Data Plan") || desc.contains("DataPlan"){
                    tType = "Data Plan"
                } else {
                    tType = "Special Offers"
                }
            } else {
                if let transType = transRecord?.transType {
                    tType = transType
                    if transType == "PAYWITHOUT ID" {
                        if let accTransType = transRecord?.accTransType {
                            if accTransType == "Cr" {
                                tType = "XXXXXXXXXX"
                            } else {
                                tType = "HIDE MY NUMBER"
                            }
                        }
                    } else if transType == "TOLL" {
                        tType = "TOLL"
                    } else if transType == "TICKET" {
                        tType = "TICKET"
                    } else {
                        tType = "PAYTO"
                        if desc.contains(find: "-cashin") {
                            tType = "Cash In"
                        } else if desc.contains(find: "-cashout") {
                            tType = "Cash Out"
                        } else if desc.contains(find: "-transferto") {
                            tType = "Transfer To"
                        }
                    }
                }
            }
        }
        labelTransType.text = tType
        if transRecord?.accTransType == "Dr" {
            buttonQR.isHidden = false
            imgViewArrow.image = UIImage(named: "reportDebitArrow")
            labelMobileNo.text = transRecord?.destination//CodeSnippets.getMobileNumber(transRecord: transRecord)
            labelName.text = transRecord?.receiverName
            labelBusiName.text = transRecord?.receiverBName
        } else {
            buttonQR.isHidden = true
            imgViewArrow.image = UIImage(named: "reportCreditArrow")
            labelName.text = transRecord?.senderName
            labelBusiName.text = transRecord?.senderBName
            if transRecord?.transType == "PAYWITHOUT ID" || transRecord?.transType == "TICKET" || transRecord?.transType == "TOLL" {
                labelMobileNo.text = "XXXXXXXXXX"
            } else {
                labelMobileNo.text = transRecord?.source//CodeSnippets.getMobileNumber(transRecord: transRecord)
            }
        }
    
        labelAmount.text = ""
        if let transAmount = transRecord?.amount {
            labelAmount.text = "\(transAmount)"//CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: transAmount.toString())
//            if transAmount.compare(0.0) == .orderedDescending {
//                isAmountGTZ = true
//            }
        }
        if let balanceAmount = transRecord?.walletBalance {
            labelBalance.text = "\(balanceAmount)"//CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: balanceAmount.toString())
        }
        if let bonusAmount = transRecord?.bonus {
            labelBonusPoints.text = "\(bonusAmount)"
        }
        labelDate.text = ""
        if let transDate = transRecord?.transactionDate {
            labelDate.text = transDate
        }
        self.imgViewFlag.image = nil
        DispatchQueue.global(qos: .userInitiated).async {
            if let countryCode = transRecord?.countryCode, countryCode.count > 0 {
                let countryData = identifyCountry(withPhoneNumber: countryCode)
                if let safeImage = UIImage(named: countryData.countryFlag) {
                    DispatchQueue.main.async {
                        self.imgViewFlag.image = safeImage
                    }
                }
            } else {
                if let mobileNumber = transRecord?.destination {
                    let countryData = identifyCountry(withPhoneNumber: mobileNumber.replaceFirstTwoChar(withStr: "+"))
                    if let safeImage = UIImage(named: countryData.countryFlag) {
                        DispatchQueue.main.async {
                            self.imgViewFlag.image = safeImage
                        }
                    }
                }
            }
        }
        /*if let cashBackAmount = transRecord?.cashBack {
            if cashBackAmount.compare(0.0) == .orderedDescending {
                labelCashBack.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: cashBackAmount.toString()) //    feeAmount -- fee(server key)
                self.imgViewFlag.image = #imageLiteral(resourceName: "myanmar_country_flag")
                if !isAmountGTZ {
                    self.labelMobileNo.text = transRecord?.countryCode ?? "(+95)"
                }
            }
        }*/
        
        //Manage comments
        //For description value
        if let safeComments = transRecord?.desc {
            let destArray = safeComments.components(separatedBy: "-")
            if safeComments.contains(find: "Overseas") {
                let cCodeArray = safeComments.components(separatedBy: "Overseas-")
                if cCodeArray.count > 1 {
                    let countryCode = cCodeArray[1].components(separatedBy: "-")
                    if countryCode.count > 0 {
                        transRecord?.countryCode = countryCode[0]
                    }
                }
                if destArray.count > 3 {
                    transRecord?.destination = destArray[3]
                }
            } else if safeComments.contains(find: "Top-Up") || safeComments.contains(find: "TopUp Plan") || safeComments.contains(find: "TopUpPlan") || safeComments.contains(find: "Data Plan") || safeComments.contains(find: "DataPlan") || safeComments.contains(find:"Special Offers" ) || safeComments.contains(find: "Special Offer") || safeComments.contains(find: "SpecialOffers"){
                if destArray.count > 3 {
                    let destNumber = destArray[3]
                    if Array(destNumber).count > 1 {
                        if Array(destNumber)[0] == "0" && Array(destNumber)[1] != "0" {
                            let destNo = "0095" + destNumber.dropFirst()
                            transRecord?.destination = destNo
                            if destNo == UserModel.shared.mobileNo {
                                transRecord?.receiverName = UserModel.shared.name
                            }
                        }
                    }
                }
            }
            let commentArray = safeComments.components(separatedBy: "[")
            if commentArray.count > 0 {
                labelDescription.text = ""
                var description = commentArray[0].count == 0 ? "" : "Remarks: \(commentArray[0])"
                if safeComments.contains(find: "#OK-PMC") {
                    description = safeComments
                   // buttonQR.isHidden = false
                    print("#OK-PMC----yes")
                } else {
                   // buttonQR.isHidden = true
                    print("#OK-PMC----no")

                }
                
               // transRecord?.desc = getRequiredComments(commentsToChange: description.removingPercentEncoding ?? "", rec: transRecord)
                
                let newcomment = getRequiredComments(commentsToChange: description.removingPercentEncoding ?? "", rec: transRecord)
                
                labelDescription.text = newcomment

                //For age value and gender
                if commentArray.count > 1 {
                    let sBracketRemovedArray = commentArray[1].components(separatedBy: "]")
                    if sBracketRemovedArray.count > 0 {
                        let commaSeparatedArray = sBracketRemovedArray[0].components(separatedBy: ",")
                        print("seperated array\(commaSeparatedArray.count)")
                        
                        if safeComments.contains(find: "#OK-PMC") {
                            if commaSeparatedArray.count > 13 {
                                
                                if (commaSeparatedArray.count == 12 || commaSeparatedArray.count == 14 || commaSeparatedArray.count == 15) {
                                    
                                    transRecord?.latitude = commaSeparatedArray[7]
                                    transRecord?.longitude = commaSeparatedArray[8]
                                    transRecord?.gender = commaSeparatedArray[10]
                                    if let ageInt = Int16(commaSeparatedArray[11]) {
                                        transRecord?.age = ageInt
                                        
                                    }
                                    
                                }
                                
                                else if commaSeparatedArray.count == 20 {
                                    
                                transRecord?.latitude = commaSeparatedArray[12]
                                transRecord?.longitude = commaSeparatedArray[13]
                                transRecord?.gender = commaSeparatedArray[15]
                                if let ageInt = Int16(commaSeparatedArray[16]) {
                                    transRecord?.age = ageInt
                                
                                }
                                
                                } else if (commaSeparatedArray.count == 23 ) {
                                    
                                    transRecord?.latitude = commaSeparatedArray[15]
                                    transRecord?.longitude = commaSeparatedArray[16]
                                    transRecord?.gender = commaSeparatedArray[18]
                                    if let ageInt = Int16(commaSeparatedArray[19]) {
                                        transRecord?.age = ageInt
                                        
                                    }
                                    
                                } else if commaSeparatedArray.count == 24 {
                                    
                                    transRecord?.latitude = commaSeparatedArray[16]
                                    transRecord?.longitude = commaSeparatedArray[17]
                                    transRecord?.gender = commaSeparatedArray[19]
                                    if let ageInt = Int16(commaSeparatedArray[20]) {
                                        transRecord?.age = ageInt
                                        
                                    }
                                    
                                }else if commaSeparatedArray.count == 25 {
                                    
                                    transRecord?.latitude = commaSeparatedArray[17]
                                    transRecord?.longitude = commaSeparatedArray[18]
                                    transRecord?.gender = commaSeparatedArray[20]
                                    if let ageInt = Int16(commaSeparatedArray[21]) {
                                        transRecord?.age = ageInt
                                        
                                    }
                                    
                                }
                                
                                //print("Greater fifty value\(transRecord?.latitude)--\(transRecord?.longitude)--\(transRecord?.gender)---\(transRecord?.age)")
                            }
                        } else {
                            if commaSeparatedArray.count > 4 {
                                transRecord?.latitude = commaSeparatedArray[0]
                                transRecord?.longitude = commaSeparatedArray[1]
                                transRecord?.gender = commaSeparatedArray[3]
                                if let ageInt = Int16(commaSeparatedArray[4]) {
                                    transRecord?.age = ageInt
                                }
                                
                                //print("Lessthan fifty value\(transRecord?.latitude)--\(transRecord?.longitude)--\(transRecord?.gender)---\(transRecord?.age)")
                            }
                        }
                        // For business name and merchant name
                        if sBracketRemovedArray.count > 1 {
                            let nameSeparatedArray = sBracketRemovedArray[0].components(separatedBy: "##")
                            transRecord?.receiverName = nameSeparatedArray.count > 1 ? nameSeparatedArray[1] : ""
                            transRecord?.receiverBName = nameSeparatedArray.count > 2 ? nameSeparatedArray[2] : ""
                            transRecord?.senderBName = nameSeparatedArray.count > 3 ? nameSeparatedArray[3] : ""
                        }
                    }
                }
            }
            //For comments value
            let snArray = safeComments.components(separatedBy: "OKNAME-")
            if snArray.count > 1 {
                if snArray[1].contains("@rating@") {
                    let snsubArray = snArray[1].components(separatedBy: "-")
                    if snsubArray.count > 2 {
                        transRecord?.rating = snsubArray[1]
                        transRecord?.ratingFeedback = snsubArray[2]
                    }
                    if snsubArray.count > 0 {
                        transRecord?.senderName = snsubArray[snsubArray.count - 1]
                    }
                } else {
                    transRecord?.senderName = snArray[1]
                }
            }
            if safeComments.contains(find: "#point-redeempoints-") {
                let redeemArray = safeComments.components(separatedBy: "#point-redeempoints-")
                if redeemArray.count > 1 {
                    transRecord?.desc = "Buy Bonus Point"
                    transRecord?.bonus = Int(redeemArray[1])
                }
            }
        }
    }
    
    // MARK: - Comments for BillPayments
    func getRequiredComments(commentsToChange: String, rec: NonOfferReportList?) -> String {
        if commentsToChange.contains(find: "#OKBILLPAYMENT") {
            let mutableComments = commentsToChange.replacingOccurrences(of: "+", with: " ")
            let arrayComment = mutableComments.components(separatedBy: "-")
            var changedComments = ""
            for (index, item) in arrayComment.enumerated() {
                if index == 0 || index == 1 || index == 2 || index == 4 || index == 6 {
                    if item.contains(find: ":") {
                        changedComments += item + " "
                    }
                }
            }
            return changedComments
        } else if commentsToChange.contains(find: "Overseas") {
            return "Remarks: TopUp Plan International"
        } else if commentsToChange.contains(find: "Top-Up") || commentsToChange.contains(find: "TopUp Plan") || commentsToChange.contains(find: "TopUpPlan") {
            return "Remarks: TopUp Plan"
        } else if commentsToChange.contains(find: "Data Plan") || commentsToChange.contains(find: "DataPlan"){
            return "Remarks: Data Plan"
        } else if commentsToChange.contains(find: "Special Offers") || commentsToChange.contains(find: "Special Offer") || commentsToChange.contains(find: "SpecialOffers"){
            return "Remarks: Special Offer Plan"
        } else if commentsToChange.contains(find: "#OK-PMC") {
            let finalComment = promocodeComments(fullComment: commentsToChange, rec: rec)
            return finalComment
        }
        return commentsToChange
    }
    
    private func promocodeComments(fullComment: String, rec: NonOfferReportList?) -> String {
        var commentsToSend = ""
        let cArray = fullComment.components(separatedBy: "[")
        commentsToSend = cArray[0]
        if cArray.count > 1 {
            let cArray = cArray[1].components(separatedBy: ",")
            commentsToSend += " Promocode \(cArray[1])"
            if let accTransType = rec?.accTransType, accTransType == "Cr" {
                
                if cArray.count > 20 {
                commentsToSend += " Sender Account Number \(cArray[14])".replacingOccurrences(of: "0095", with: "(+95)0")
                } else {
                    commentsToSend = ""
                }
            } else {
                if cArray.count > 20 {
                commentsToSend += " Receiver Account Number \(cArray[15])".replacingOccurrences(of: "0095", with: "(+95)0")
                }else {
                    commentsToSend = ""
                }
            }
            commentsToSend += " Company Name \(cArray[7])"
            let getCompProdQuanStr = getCompProdQuan(comArray: cArray)
            commentsToSend += getCompProdQuanStr
            let discountString = promoDiscountPercent(discountString: cArray)
            commentsToSend += discountString
            let discountAmount = promoDiscountAmount(discountString: cArray)
            commentsToSend += discountAmount
            if cArray.count > 5 {
                let billAmount = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: cArray[5])
                commentsToSend += " Bill Amount \(billAmount) MMK"
            }
        }
        return commentsToSend
    }
    
    private func getCompProdQuan(comArray: [String]) -> String {
        var neededStr = ""
        if comArray.count > 9 {
            if comArray[2] == "2" || comArray[2] == "3" || comArray[2] == "4" {
                neededStr += " Product Name " + comArray[11] + " Buying Quantity " + comArray[8] + " Free Quantity " + comArray[9]
                return neededStr
            }
        }
        return ""
    }
    
    private func promoDiscountPercent(discountString: [String]) -> String {
        if discountString[2] == "0" || discountString[2] == "3" {
            let amount = Float(discountString[5]) ?? 0.0
            let percentageAmount = Float(discountString[4]) ?? 0.0
            let percentageValue = Float((percentageAmount * 100) / amount)
            let displayPercentage = String.init(format: "%.2f", percentageValue)
            return " Discount Percentage " + displayPercentage + "%"
        } else if discountString[2] == "1" || discountString[2] == "4"{
            let value = Float(discountString[4]) ?? 0.0
            let displayPercentage = String.init(format: "%.2f", value)
            return " Discount Percentage " + displayPercentage + "%"
        } else {
            return ""
        }
        
    }
    
    private func promoDiscountAmount(discountString: [String]) -> String {
        if discountString[2] == "0" || discountString[2] == "3" {
            let value = Float(discountString[4]) ?? 0.0
            let displayAmount = String.init(format: "%.2f", value)
            return " Discount Amount " + displayAmount + " MMK"
        } else if discountString[2] == "1" || discountString[2] == "4"{
            let amount = Float(discountString[5]) ?? 0.0
            let percentageAmount = Float(discountString[4]) ?? 0.0
            let percentageValue = Float((percentageAmount * amount) / 100)
            let displayAmount = String.init(format: "%.2f", percentageValue)
            return " Discount Amount " + displayAmount + " MMK"
        } else {
            return ""
        }
    }
}
