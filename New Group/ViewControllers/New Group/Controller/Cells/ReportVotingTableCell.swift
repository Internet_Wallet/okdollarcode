//
//  ReportVotingTableCell.swift
//  OK
//
//  Created by E J ANTONY on 07/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportVotingTableCell: UITableViewCell {

    //MARK: - Outlet
    @IBOutlet weak var lblProgramName: UILabel!{
        didSet{
            self.lblProgramName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblEvent: UILabel!{
        didSet{
            self.lblEvent.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblParticipantName: UILabel!{
        didSet{
            self.lblParticipantName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblTransactionID: UILabel!{
        didSet{
            self.lblTransactionID.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblVoteCount: UILabel!{
        didSet{
            self.lblVoteCount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblVotingType: UILabel!{
        didSet{
            self.lblVotingType.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblAmount: UILabel!{
        didSet{
            self.lblAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblDate: UILabel!{
        didSet{
            self.lblDate.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var boxView: UIView!
    
    //MARK: - Views life cycle methods
    override func awakeFromNib() {
        self.boxView.layer.cornerRadius = 5
        self.layer.masksToBounds = true
        self.boxView.layer.masksToBounds = false
        self.boxView.layerShadow(offsetValue: CGSize(width: 0, height: 1.0), opacity: 0.7)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Methods
    func configureCell(voteData: VotingReportList) {
        self.lblProgramName.text = ""
        if let mainProgram = voteData.mainProgrammeName {
            var programName = mainProgram
            if let subProgramName = voteData.programmeName {
                programName += "\n\(subProgramName)"
            }
            self.lblProgramName.text = programName
        }
        self.lblEvent.text = ""
        if let eventName = voteData.eventName {
            self.lblEvent.text = eventName
        }
        self.lblParticipantName.text = ""
        if let participateName = voteData.participantName {
            self.lblParticipantName.text = participateName
        }
        self.lblTransactionID.text = "xxxxxxx"
        if let transId = voteData.payment?.transactionID {
            self.lblTransactionID.text = transId
        }
        self.lblVoteCount.text = "0"
        if let voteCount = voteData.votes {
            self.lblVoteCount.text = "\(voteCount)"
        }
        self.lblVotingType.text = ""
        if let type = voteData.votingTypeName {
            self.lblVotingType.text = type
        }
        self.lblAmount.text = "0"
        if let amount = voteData.payment?.amount {
            self.lblAmount.text = "\(amount)"
        }
        self.lblDate.text = ""
        if let createdDateStr = voteData.createdDate {
            if let dateValue = createdDateStr.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") {
                let dateString = dateValue.stringValue(dateFormatIs: "dd-MMM-yyyy\nHH:mm:ss")
                self.lblDate.text = dateString
            }
        }
    }
}
