//
//  ReportAddWithdrawCell.swift
//  OK
//  This show a record of add / withdraw
//  Created by ANTONY on 06/02/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportCardlessCashCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var lblReceiver: UILabel!{
        didSet{
            self.lblReceiver.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblCardType: UILabel!{
        didSet{
            self.lblCardType.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblCardNumber: UILabel!{
        didSet{
            self.lblCardNumber.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblAmount: UILabel!{
        didSet{
            self.lblAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblFees: UILabel!{
        didSet{
            self.lblFees.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblTransID: UILabel!{
        didSet{
            self.lblTransID.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblBankTransID: UILabel!{
        didSet{
            self.lblBankTransID.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblDate: UILabel!{
        didSet{
            self.lblDate.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewContent.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(awReportData: AddWithdrawData) {
        var number = ""
        var country = ""
        if let num = awReportData.beneficiaryAccountNumber, num.hasPrefix("00") {
            number = String(num.dropFirst(4))
            country = String(num.prefix(4))
            country = country.replacingOccurrences(of: "00", with: "+")
        } else {
            country = "+95"
            if let num = awReportData.beneficiaryAccountNumber {
                number = String(num.dropFirst())
            }
        }
        let rNo = CodeSnippets.getMobileNumWithBrackets(countryCode: country, number: "0\(number)")
        self.lblReceiver.text = rNo
        if let cardType = awReportData.addMoneyType?.uppercased(), cardType == "MASTERPAY" {
            self.lblCardType.text = "VISA"
        } else {
            self.lblCardType.text = awReportData.addMoneyType?.uppercased()
        }
        self.lblCardNumber.text = "XXXXXXXXXXXX"
        if let cNumber = awReportData.cardNumber {
           self.lblCardNumber.text = cNumber
        }
        self.lblAmount.text = ""
        self.lblFees.text = ""
        if let amount = awReportData.amount {
            self.lblAmount.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(amount)")
        }
        if let amount = awReportData.adminFee {
            self.lblFees.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(amount)")
        }
        self.lblTransID.text = awReportData.okTransactionID
        self.lblBankTransID.text = awReportData.bankTransactionID
        self.lblDate.text = ""
        if let dateStr = awReportData.transactionDate {
            if let dateValue = dateStr.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") {
                let dateString = dateValue.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss")
                self.lblDate.text = dateString
            }
        }
    }
}
