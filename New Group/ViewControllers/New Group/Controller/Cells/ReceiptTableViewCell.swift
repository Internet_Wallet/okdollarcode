//
//  ReceiptTableViewCell.swift
//  OK
//  It shows the receipt item
//  Created by ANTONY on 26/01/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Contacts

class ReceiptTableViewCell: UITableViewCell {

    @IBOutlet weak var buttonPayType: UIButton!{
        didSet{
            self.buttonPayType.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var imgViewPayType: UIImageView!
    @IBOutlet weak var labelName: UILabel!{
        didSet{
            self.labelName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelMobileNumber: UILabel!{
        didSet{
            self.labelMobileNumber.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAmount: UILabel!{
        didSet{
            self.labelAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var imageViewStar: UIButton!
    fileprivate var modelFav = [FavoriteContacts]()
    let favoriteManager = FavoriteDBManager()

    override func awakeFromNib() {
        super.awakeFromNib()
        self.imgViewPayType.layer.cornerRadius = imgViewPayType.frame.size.height / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureReceiptCell(record: ReportTransactionRecord) {
        var isAmountGTZ = false
        labelName.text = ""
        labelMobileNumber.text = ""
        if record.accTransType == "Dr" {
            labelMobileNumber.text = CodeSnippets.getMobileNumber(transRecord: record)
           
            let name = record.receiverName?.replacingOccurrences(of: "-cashin", with: "").replacingOccurrences(of: "-cashout", with: "")
            if name?.count == 0 || name == nil{
                labelName.text = "Unknown"
            } else {
                labelName.text = name
            }
        } else {
            let name = record.senderName?.replacingOccurrences(of: "-cashin", with: "").replacingOccurrences(of: "-cashout", with: "")
            if let value = name{
                if value.contains("@sender"){
                    let newValue = value.components(separatedBy: "@sender")
                    if newValue.indices.contains(1){
                        let nameNew = newValue[1].components(separatedBy: "@businessname")
                        if nameNew.indices.contains(0){
                            //labelName.text = nameNew[0]
                            if nameNew[0].count == 0 {
                                labelName.text = "Unknown"
                            } else {
                                labelName.text = nameNew[0]
                            }
                        }
                    }
                }else{
                    if value.count == 0 {
                        labelName.text = "Unknown"
                    } else {
                        labelName.text = value
                    }
                }
            }
            if record.transType == "PAYWITHOUT ID" || record.transType == "TICKET" || record.transType == "TOLL" {
                labelMobileNumber.text = "XXXXXXXXXX"
            } else {
                labelMobileNumber.text = CodeSnippets.getMobileNumber(transRecord: record)
            }
        }        
        
        if record.rawDesc?.contains(find: "ELECTRICITY") ?? false {
            labelMobileNumber.text = "XXXXXXXXXX"
        } else if record.rawDesc?.contains(find: "#OK Wallet Topup") ?? false {
            labelMobileNumber.text = "XXXXXXXXXX"
        } else if record.rawDesc?.contains(find: "REFUND") ?? false {
            labelMobileNumber.text = "XXXXXXXXXX"
        } else if record.rawDesc?.contains(find: "POSTPAID") ?? false || record.rawDesc?.contains(find: "MERCHANTPAY") ?? false || record.rawDesc?.contains(find: "LANDLINE") ?? false || record.rawDesc?.contains(find: "SUNKING") ?? false || record.rawDesc?.contains(find: "INSURANCE") ?? false {
            labelMobileNumber.text = "XXXXXXXXXX"
        } else if  record.rawDesc?.contains(find: "PAYMENTTYPE:GiftCard") ?? false {
            labelMobileNumber.text = "XXXXXXXXXX"
        } else if record.rawDesc?.contains(find: "DTH") ?? false {
            labelMobileNumber.text = "XXXXXXXXXX"
        }  else if record.destination == nil, let cashback = record.cashBack, cashback > 0 {
            labelMobileNumber.text = "XXXXXXXXXX"
        } else if let desc = record.rawDesc, desc.contains(find: "SEND MONEY TO BANK:") {
            labelMobileNumber.text = "XXXXXXXXXX"
        }  else if record.rawDesc?.contains(find: "Bonus Point Exchange Payment") ?? false {
            labelMobileNumber.text = "XXXXXXXXXX"
        }  else if record.rawDesc?.contains(find: "Bank To Wallet") ?? false {
            labelMobileNumber.text = "XXXXXXXXXX"
        } else if record.rawDesc?.contains(find: "ProjectId") ?? false {
            //labelMobileNumber.text = "XXXXXXXXXX"
            var mobileNo = "XXXXXXXXXX"
            let value = record.rawDesc ?? ""
            if value.contains(find: "ProjectId"){
                let newValue = value.components(separatedBy: " Wallet Number :")
                if newValue.count > 2 {
                    if newValue.indices.contains(1){
                        let name = newValue[2].components(separatedBy: "ProjectId")
                        if name.indices.contains(0){
                            mobileNo = "(+95)".appending(name[0])
                        }
                    }
                }  else {
                    //OK Dollar Wallet To MAB , OK Dollar Wallet Number :  00959790681381 Account Number :09976669357ProjectIdMAB767898KUEOJBU_IEHK787UOEJDHF
                    let arrVal = value.components(separatedBy: "Account Number :")
                    if arrVal.indices.contains(1){
                        let name = arrVal[1].components(separatedBy: "ProjectId")
                        if name.indices.contains(0){
                            mobileNo = "(+95)".appending(name[0])
                        }
                    }
                }
            }
            labelMobileNumber.text = mobileNo
            let valueData = self.returnCorespondentName(value:mobileNo.replacingOccurrences(of: "(+95)0", with: ""))
            labelName.text = valueData["name"] as? String ?? "Unknown"
        } else if record.rawDesc?.contains(find: "#Tax Code") ?? false {
            labelMobileNumber.text = "XXXXXXXXXX"
        } else if record.rawDesc?.contains(find: "Overseas") ?? false {
            let delimiter = "-"
            let newstr = record.rawDesc ?? ""
            var mobileNo = "XXXXXXXXXX"
            let token = newstr.components(separatedBy: delimiter)
            if token[6].hasPrefix("Overseas"){
                mobileNo = "\(token[3])"
            }
            
            let isContactPresent = ContactManager().agentgentCodePresentinContactsLoc(number: mobileNo)
            if isContactPresent {
                let valueData = self.returnCorespondentName(value:mobileNo)
                labelName.text = valueData["name"] as? String ?? "Unknown"
            }
            
            if labelName.text == "Unknown" {
                let isInFav = favoriteManager.agentgentCodePresentinFavoriteLoc(number: mobileNo)
                if isInFav {
                    let valueData = self.returnFavoriteName(value:mobileNo)
                    labelName.text = valueData["name"] as? String ?? "Unknown"
                }
            }
            labelMobileNumber.text = "(\(token[7]))" + mobileNo
        }
        
        if labelName.text == "Unknown" {
            let mobileNo = labelMobileNumber.text ?? ""
            let valueData = self.returnCorespondentName(value:mobileNo.replacingOccurrences(of: "(+95)0", with: ""))
            labelName.text = valueData["name"] as? String ?? "Unknown"
        }
        
        labelAmount.text = ""
        if let transAmount = record.amount {
            let amountToDisplay = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: transAmount.toString())
            labelAmount.text = amountToDisplay
            if transAmount.compare(0.0) == .orderedDescending {
                isAmountGTZ = true
            }
        }
        if let cashBackAmount = record.cashBack {
            if cashBackAmount.compare(0.0) == .orderedDescending {
                if !isAmountGTZ {
                    self.labelMobileNumber.text = record.countryCode ?? "(+95)"
                    labelAmount.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: cashBackAmount.toString())
                }
            }
        }
        imgViewPayType.image = nil
        guard let commentStr = record.rawDesc else { return }
        self.setButtonImage(comments: commentStr, transactionType: record.transType, record: record) //commentStr
    }
    
    func setButtonImage(comments: String, transactionType: String?, record: ReportTransactionRecord) {
        println_debug(comments)
        println_debug(transactionType)
        if comments.contains("#OK") || comments.contains("OOK") {
            switch comments {
            case let telenor where telenor.contains("-Telenor-"):
                imgViewPayType.image = #imageLiteral(resourceName: "receiptTelenor").withRenderingMode(.alwaysOriginal)
            case let telenor where telenor.contains("-Mpt-"):
                imgViewPayType.image = #imageLiteral(resourceName: "receiptMpt").withRenderingMode(.alwaysOriginal)
            case let telenor where telenor.contains("-Ooredoo-"):
                imgViewPayType.image = #imageLiteral(resourceName: "receiptOoredoo").withRenderingMode(.alwaysOriginal)
            case let telenor where telenor.lowercased().contains("-mectel-"):
                imgViewPayType.image = #imageLiteral(resourceName: "receiptMectel").withRenderingMode(.alwaysOriginal)
            case let telenor where telenor.contains("#OK-voting"):
                imgViewPayType.image = #imageLiteral(resourceName: "receiptVote").withRenderingMode(.alwaysOriginal)
            case let telenor where telenor.lowercased().contains("-mytel-"):
                imgViewPayType.image = UIImage(named: "mytel_recent.png")?.withRenderingMode(.alwaysOriginal)
            case let telenor where telenor.contains(find: "PAYMENTTYPE:GiftCard"):
                let image = UIImage(named: "gift_card_trans")?.withRenderingMode(.alwaysOriginal)
                imgViewPayType.image = image
            default:
                if comments.contains(find: "#OK Wallet Topup") {
                    let image = UIImage(named: "send_money_bank_trans")?.withRenderingMode(.alwaysOriginal)
                    imgViewPayType.image = image
                }  else if record.rawDesc?.contains(find: "MPU") ?? false {
                    let image = UIImage(named: "mpu_trans")?.withRenderingMode(.alwaysOriginal)
                    imgViewPayType.image = image
                } else if record.rawDesc?.contains(find: "Overseas") ?? false {
                    let image = #imageLiteral(resourceName: "top_up_trans").withRenderingMode(.alwaysOriginal)
                    imgViewPayType.image = image
                }else if comments.lowercased().contains("topup") {
                    let image = UIImage(named: "top_up_trans")?.withRenderingMode(.alwaysOriginal)
                    imgViewPayType.image = image
                } else if record.rawDesc?.contains(find: "POSTPAID") ?? false {
                    let image = UIImage(named: "postpaid_bill_trans")?.withRenderingMode(.alwaysOriginal)
                    imgViewPayType.image = image
                } else if record.rawDesc?.contains(find: "MERCHANTPAY") ?? false {
                    let image = UIImage(named: "pay_trans")?.withRenderingMode(.alwaysOriginal)
                    imgViewPayType.image = image
                } else if record.rawDesc?.contains(find: "LANDLINE") ?? false {
                    let image = UIImage(named: "landline_bill_trans")?.withRenderingMode(.alwaysOriginal)
                    imgViewPayType.image = image
                } else if record.rawDesc?.contains(find: "SUNKING") ?? false {
                    let image = UIImage(named: "insurance_trans")?.withRenderingMode(.alwaysOriginal)
                    imgViewPayType.image = image
                } else if record.rawDesc?.contains(find: "INSURANCE") ?? false {
                    let image = UIImage(named: "insurance_trans")?.withRenderingMode(.alwaysOriginal)
                    imgViewPayType.image = image
                } else if let desc = record.rawDesc, desc.contains(find: "DTH") {
                    let image = UIImage(named: "dth_trans")?.withRenderingMode(.alwaysOriginal)
                    imgViewPayType.image = image
                } else if let desc = record.rawDesc, desc.contains(find: "SolarHome Topup") {
                    let image = UIImage(named: "soloar_home")?.withRenderingMode(.alwaysOriginal)
                    imgViewPayType.image = image
                } else if record.rawDesc?.contains(find: "ELECTRICITY") ?? false {
                    let image = UIImage(named: "electricity_trans")?.withRenderingMode(.alwaysOriginal)
                    imgViewPayType.image = image
                } else if record.transType == "PAYTO" {
                    let image = UIImage(named: "pay_trans")?.withRenderingMode(.alwaysOriginal)
                    imgViewPayType.image = image
                } else if record.transType == "PAYWITHOUT ID" {
                    let image = UIImage(named: "pay_trans")?.withRenderingMode(.alwaysOriginal)
                    imgViewPayType.image = image
                }
                
                else {
                    imgViewPayType.image = #imageLiteral(resourceName: "receiptMpt").withRenderingMode(.alwaysOriginal)
                }
            }
        } else {
            if let transType = transactionType {
                 if transType == "PAYWITHOUT ID" {
                    let image = UIImage(named: "pay_trans")?.withRenderingMode(.alwaysOriginal)
                    imgViewPayType.image = image
                } else if transType == "TOLL" {
                    imgViewPayType.image = #imageLiteral(resourceName: "receiptToll").withRenderingMode(.alwaysOriginal)
                 } else if transType == "TICKET" {
                    imgViewPayType.image = #imageLiteral(resourceName: "receiptTicket").withRenderingMode(.alwaysOriginal)
                 } else {
                    imgViewPayType.image = #imageLiteral(resourceName: "receiptPay").withRenderingMode(.alwaysOriginal)
                }
            } else {
                let image = UIImage(named: "pay_trans")?.withRenderingMode(.alwaysOriginal)
                imgViewPayType.image = image
            }
            
            
            if record.rawDesc?.contains(find: "MPU") ?? false {
                let image = UIImage(named: "mpu_trans")?.withRenderingMode(.alwaysOriginal)
                imgViewPayType.image = image
            } else if record.rawDesc?.contains(find: "REFUND") ?? false {
                imgViewPayType.image = #imageLiteral(resourceName: "receiptPay").withRenderingMode(.alwaysOriginal)
            } else if record.destination == nil, let cashback = record.cashBack, cashback > 0 {
                let image = UIImage(named: "cash_back_trans")?.withRenderingMode(.alwaysOriginal)
                imgViewPayType.image = image
            } else if let desc = record.rawDesc, desc.contains(find: "SEND MONEY TO BANK:") {
                let image = UIImage(named: "send_money_bank_trans")?.withRenderingMode(.alwaysOriginal)
                imgViewPayType.image = image
            }  else if let desc = record.rawDesc, desc.contains(find: "-cashin") {
                if record.accTransType ?? "" == "Cr" {
                    let image = UIImage(named: "cash_in_trans")?.withRenderingMode(.alwaysOriginal)
                    imgViewPayType.image = image
                } else {
                    let image = UIImage(named: "transfer_to_trans")?.withRenderingMode(.alwaysOriginal)
                    imgViewPayType.image = image
                }
            } else if let desc = record.rawDesc, desc.contains(find: "-cashout") {
                let image = UIImage(named: "cash_out_trans")?.withRenderingMode(.alwaysOriginal)
                imgViewPayType.image = image
            } else if let desc = record.rawDesc, desc.contains(find: "ProjectId") {
                let image = UIImage(named: "send_money_bank_trans")?.withRenderingMode(.alwaysOriginal)
                imgViewPayType.image = image
            }  else if let desc = record.rawDesc, desc.contains(find: "Bonus Point Exchange Payment") {
                let image = UIImage(named: "tr_earn_point")?.withRenderingMode(.alwaysOriginal)
                imgViewPayType.image = image
            }  else if let desc = record.rawDesc, desc.contains(find: "#Tax Code") {
                let image = UIImage(named: "tax_Logo")?.withRenderingMode(.alwaysOriginal)
                imgViewPayType.image = image
            } else if let desc = record.rawDesc, desc.contains(find: "Bank TransactionId :") {
                let image = UIImage(named: "send_money_bank_trans")?.withRenderingMode(.alwaysOriginal)
                imgViewPayType.image = image
            }
            else {
                if let transType = transactionType, (transType == "PAYTO" || transType == "PAYWITHOUT ID") {
                    let image = UIImage(named: "pay_trans")?.withRenderingMode(.alwaysOriginal)
                    imgViewPayType.image = image
                } else {
                    let image = UIImage(named: "recieved_money_trans")?.withRenderingMode(.alwaysOriginal)
                    imgViewPayType.image = image
                }
            }
        }
    }
    
    //MARK:- Get Favorite List
    func returnFavoriteName(value: String) -> [String:AnyObject] {
        var userName = [String:AnyObject]()

        let number = "\(value.replacingOccurrences(of: "0095", with: ""))"
        
        self.modelFav = favoriteManager.fetchRecordsForFavoriteContactsEntity()

        let indx: Int?
        for (indexN, item) in self.modelFav.enumerated() {
            if let ph1 = item.phone {
                if ph1.contains(find: number) {
                    indx = indexN
                    println_debug(indx)
                    userName["name"] =  item.name as AnyObject
                    userName["pic"] = nil
                    return userName
                } else {
                    userName["name"] = "Unknown" as AnyObject
                    userName["pic"] = nil
                }
            }
        }
        return userName
    }

    func returnCorespondentName(value: String) -> [String:AnyObject]{
        
        if okContacts.isEmpty {
            return ["Unknown":"Unknown" as AnyObject]
        } else {
            return self.getDetails(Ucontacts: okContacts, number: "\(value.replacingOccurrences(of: "0095", with: ""))")
        }
    }
    
    func getDetails(Ucontacts:Array<CNContact>, number:String) -> [String:AnyObject]{
        /* Get mobile number with mobile country code */
        
        var userName = [String:AnyObject]()
        
        for i in 0...(Ucontacts.count-1) {
            
            for ContctNumVar: CNLabeledValue in Ucontacts[i].phoneNumbers {
                 let FulMobNumVar  = ContctNumVar.value
                 var MobNumVar = ""
                if let numFull = FulMobNumVar.value(forKey: "digits") as? String {
                    MobNumVar = numFull
                }
//                println_debug("user Mobile Number : \(number)")
//                println_debug("Comapre Mobile Number : \(String(describing: MobNumVar))")
                if MobNumVar.contains(find: number) {
                    userName["name"] = (Ucontacts[i].givenName + " " + Ucontacts[i].familyName) as AnyObject
                    userName["pic"] = Ucontacts[i].imageData as AnyObject
                    return userName
                } else {
                    userName["name"] = "Unknown" as AnyObject
                    userName["pic"] = nil
                }
            }
        }
        return userName
    }
}
