//
//  ReportOfferFilterResultCell.swift
//  OK
//
//  Created by E J ANTONY on 13/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportOfferFilterResultCell: UITableViewCell {

    //MARK: - Outlet
    @IBOutlet weak var lblTitleAccNo: UILabel!{
        didSet{
            self.lblTitleAccNo.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblValAccNo: UILabel!{
        didSet{
            self.lblValAccNo.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblTitleBillAmount: UILabel!{
        didSet{
            self.lblTitleBillAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblValBillAmount: UILabel!{
        didSet{
            self.lblValBillAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var constraintHQuantity: NSLayoutConstraint!
    @IBOutlet weak var lblTitleSaleQty: UILabel!{
        didSet{
            self.lblTitleSaleQty.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblTitleFreeQty: UILabel!{
        didSet{
            self.lblTitleFreeQty.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblValSaleQty: UILabel!{
        didSet{
            self.lblValSaleQty.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblValFreeQty: UILabel!{
        didSet{
            self.lblValFreeQty.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblTitleAccNo.text = "Account Number".localized
        self.lblTitleBillAmount.text = "Net Received Amount".localized
        self.lblTitleSaleQty.text = "Sale Quantity".localized
        self.lblTitleFreeQty.text = "Free Quantity".localized
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(data: OfferReporFilterList, amountType: Int) {
        self.lblValAccNo.text = ""
        if var mN = data.promotionUsedMobileNumber {
            self.lblValAccNo.text = mN
            if mN.hasPrefix("0095") {
                mN = "0" + String(mN.substring(from: 4))
                self.lblValAccNo.text = mN
            }
        }
        
        self.lblValBillAmount.text = ""
        if let amount = data.totalBillAmount {
            self.lblValBillAmount.attributedText = CodeSnippets.getCurrencyInAttributed(withString: "\(amount)", amountSize: 16, amountColor: .black, currencyDenomSize: 13, currencyDenomColor: .black)
        }
        if amountType == 2 || amountType == 3 || amountType == 4 {
            constraintHQuantity.constant = 100
            self.layoutIfNeeded()
            if data.productValidationRuleValue == 2 {
                self.lblValSaleQty.text = ""
                if let sale = data.buyingQty {
                    self.lblValSaleQty.text = "\(sale)"
                }
                
                self.lblValFreeQty.text = ""
                if let free = data.freeQty {
                    self.lblValFreeQty.text = "\(free)"
                }
            } else {
                self.lblValSaleQty.text = ""
                if let sale = data.buyingQty {
                    self.lblValSaleQty.text = "\(sale)"
                }
                
                self.lblValFreeQty.text = ""
                if let free = data.freeQty {
                    self.lblValFreeQty.text = "\(free)"
                }
            }
        } else {
            constraintHQuantity.constant = 0
            self.layoutIfNeeded()
        }
        
    }
}
