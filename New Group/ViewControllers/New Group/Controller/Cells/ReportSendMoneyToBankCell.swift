//
//  ReportSendMoneyToBankCell.swift
//  OK
//  This show the record of sent money to bank
//  Created by ANTONY on 02/02/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportSendMoneyToBankCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var receiverAccName: UILabel!{
        didSet{
            self.receiverAccName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var bankName: UILabel!{
        didSet{
            self.bankName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var branchName: UILabel!{
        didSet{
            self.branchName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var accNumber: UILabel!{
        didSet{
            self.accNumber.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var accType: UILabel!{
        didSet{
            self.accType.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var amountTransferred: UILabel!{
        didSet{
            self.amountTransferred.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var transactionID: UILabel!{
        didSet{
            self.transactionID.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var dateTime: UILabel!{
        didSet{
            self.dateTime.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var balance: UILabel!{
        didSet{
            self.balance.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var comments: UILabel!{
        didSet{
            self.comments.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var imgViewBackground: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imgViewBackground.layer.cornerRadius = 5
        self.imgViewBackground.image = UIImage(named: "strip2")
        //self.viewContent.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureReportSendMoneyToBankCell(report: ReportSendMoneyToBank) {
        self.receiverAccName.text = report.receiverOkAccName ?? ""
        self.bankName.text = report.bankName ?? ""
        self.branchName.text = report.bankBranchName ?? ""
        self.accType.text = report.bankAccType
        
        self.accNumber.text = ""
        if let accNumber = report.bankAccNumber {
            self.accNumber.text = accNumber
        }
        self.amountTransferred.text = ""
        if let amount = report.transferredAmount {
            let amountInString = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: amount.toString())
            self.amountTransferred.text = UIViewController().amountInFormat(amountInString)
        }
        self.transactionID.text = ""
        if let transID = report.transactionID {
            self.transactionID.text = transID
        }
        self.dateTime.text = ""
        if let dateTime = report.transactionDateTime {
            self.dateTime.text = dateTime.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy\nHH:mm:ss")
        }
        self.balance.text = ""
        if let amount = report.balance {
            let amountInString = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: amount.toString())
            self.balance.text = UIViewController().amountInFormat(amountInString)
        }
        self.comments.text = ""
//        if let comments = report.comments {
//            self.comments.text = comments
//        }
    }
}
