//
//  ReportReSaleMainBalanceCell.swift
//  OK
//  This show each reecord of re-sale main balace data
//  Created by ANTONY on 05/02/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportReSaleMainBalanceCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var sourceNumber: UILabel!{
        didSet{
            self.sourceNumber.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var destNumber: UILabel!{
        didSet{
            self.destNumber.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var sellingAmount: UILabel!{
        didSet{
            self.sellingAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var sellingPrice: UILabel!{
        didSet{
            self.sellingPrice.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var status: UILabel!{
        didSet{
            self.status.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var date: UILabel!{
        didSet{
            self.date.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewContent.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureResaleReportCell(resaleReportRecord: ReportResaleMainBalance) {
        sourceNumber.text = ""
        destNumber.text = ""
        sellingAmount.text = ""
        sellingPrice.text = ""
        status.text = ""
        date.text = ""
        
        if let sNumber = resaleReportRecord.phoneNumber {
            sourceNumber.text = self.getFormattedNumber(number: sNumber)
        }
        if let dNumber = resaleReportRecord.destinationNumber {
            destNumber.text = self.getFormattedNumber(number: dNumber)
        }
        if let recSellingAmount = resaleReportRecord.airtimeAmount {
            let transAmt = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: recSellingAmount.toString())
            sellingAmount.attributedText = CodeSnippets.appendAttributedMMK(forTheAmount: transAmt)
        }
        if let recSellingPrice = resaleReportRecord.sellingAmount {
            let transAmt = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: recSellingPrice.toString())
            sellingPrice.attributedText = CodeSnippets.appendAttributedMMK(forTheAmount: transAmt)
        }
        if let recStatus = resaleReportRecord.status {
            status.text = recStatus
        }
        if let recDate = resaleReportRecord.createDate {
            date.text = recDate.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy\nHH:mm:ss")
        }
    }
    
    private func getFormattedNumber(number: String) -> String {
        var forNum = ""
        var number1 = number
        if number.hasPrefix("0095") {
            number1 = String(number1.dropFirst(4))
            forNum = "(+95)0\(number1)"
        } else {
            let country = number1.prefix(4).dropFirst(2)
            number1 = String(number1.dropFirst(4))
            forNum = "(+\(country))\(number1)"
        }
        return forNum
    }

}
