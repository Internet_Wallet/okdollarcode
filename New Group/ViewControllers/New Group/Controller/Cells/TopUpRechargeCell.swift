//
//  TopUpRechargeCell.swift
//  OK
//  This shows the record of top up recharge
//  Created by ANTONY on 31/01/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class TopUpRechargeCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var labelTransaction: UILabel!{
        didSet{
            self.labelTransaction.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAmount: UILabel!{
        didSet{
            self.labelAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelMobileNo: UILabel!{
        didSet{
            self.labelMobileNo.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelBonusPoints: UILabel!{
        didSet{
            self.labelBonusPoints.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelCashBack: UILabel!{
        didSet{
            self.labelCashBack.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelBalance: UILabel!{
        didSet{
            self.labelBalance.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDate: UILabel!{
        didSet{
            self.labelDate.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelStatus: UILabel!{
        didSet{
            self.labelStatus.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelComment: UILabel!{
        didSet{
            self.labelComment.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewContent.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureTopUpRechargeCell(topUpRecord: ReportTransactionRecord?) {
        labelStatus.text = "Success"
        labelComment.text = ""
        if var commentsToShow = topUpRecord?.desc {
            if commentsToShow.hasPrefix("Remarks: ") {
                commentsToShow = commentsToShow.replacingOccurrences(of: "Overseas", with: "International")
                labelComment.text = "\(commentsToShow.substring(from: 9))"
            } else {
                labelComment.text = commentsToShow
            }
        }
        labelMobileNo.text = ""
        labelCashBack.text = "0" //For receiver side, since no cashback for receiver
        if topUpRecord?.accTransType == "Dr" {
            labelMobileNo.text = CodeSnippets.getMobileNumber(transRecord: topUpRecord)
            if let cashBackAmount = topUpRecord?.cashBack {
                let cashBAmt = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: cashBackAmount.toString())
                labelCashBack.attributedText = CodeSnippets.appendAttributedMMK(forTheAmount: cashBAmt)
            }
        } else {
            if topUpRecord?.transType == "PAYWITHOUT ID" {
                labelMobileNo.text = "XXXXXXXXXX"
            } else {
                labelMobileNo.text = CodeSnippets.getMobileNumber(transRecord: topUpRecord)
            }
        }
        
        labelAmount.text = ""
        if let transAmount = topUpRecord?.amount {
            let trnsAmt = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: transAmount.toString())
            labelAmount.attributedText = CodeSnippets.appendAttributedMMK(forTheAmount: trnsAmt)
        }
        if let balanceAmount = topUpRecord?.walletBalance {
            let balAmt = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: balanceAmount.toString())
            labelBalance.attributedText = CodeSnippets.appendAttributedMMK(forTheAmount: balAmt)
        }
        if let bonusAmount = topUpRecord?.bonus {
            labelBonusPoints.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: bonusAmount.toStringNoDecimal()) //  feeValue -- FEE(server key)
        }
        
        labelDate.text = ""
        if let transDate = topUpRecord?.transactionDate {
            labelDate.text = transDate.stringValue()
        }
        
        labelTransaction.text = topUpRecord?.transID
    }
}
