//
//  ReportPromotionLocationFilterCell.swift
//  OK
//
//  Created by E J ANTONY on 12/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportPromotionLocationFilterCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet weak var labelTitle: UILabel!{
        didSet{
            self.labelTitle.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelValue: UILabel!{
        didSet{
            self.labelValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    //MARK: - Views Life Cycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    //MARK: - Methods
    func configureCell(primeData: String, secondData: String) {
        self.labelTitle.text = primeData
        self.labelValue.text = secondData
    }
    
}
