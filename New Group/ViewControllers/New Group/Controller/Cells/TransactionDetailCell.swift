//
//  TransactionDetailCell.swift
//  OK
//  It shows the transaction item
//  Created by ANTONY on 27/01/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Contacts

class TransactionDetailCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var imgViewUser: UIImageView!
    @IBOutlet weak var labelTransaction: UILabel!{
        didSet{
            self.labelTransaction.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAmount: UILabel!{
        didSet{
            self.labelAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var imgViewFlag: UIImageView!
    @IBOutlet weak var imgViewArrow: UIImageView!
    @IBOutlet weak var labelMobileNo: UILabel!{
        didSet{
            self.labelMobileNo.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelName: UILabel!{
        didSet{
            self.labelName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelTransType: UILabel!{
        didSet{
            self.labelTransType.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelBonusPoints: UILabel!{
        didSet{
            self.labelBonusPoints.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelCashBack: UILabel!{
        didSet{
            self.labelCashBack.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelBalance: UILabel!{
        didSet{
            self.labelBalance.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDate: UILabel!{
        didSet{
            self.labelDate.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDescription: UILabel!{
        didSet{
            self.labelDescription.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonPdf: UIButton!{
        didSet{
            self.buttonPdf.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonQR: UIButton!{
        didSet{
            self.buttonQR.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelBusiName: UILabel!{
        didSet{
            self.labelBusiName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    @IBOutlet weak var labelAccountType: UILabel!{
        didSet{
            self.labelAccountType.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelPaymentType: UILabel!{
        didSet{
            self.labelPaymentType.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewContent.layer.cornerRadius = 5
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
        
    func configureTransactionItemCell(transRecord: ReportTransactionRecord?) {
        //println_debug(transRecord)
        var accountTyp = "Personal"
        var isAmountGTZ = false
        labelMobileNo.text = "XXXXXXXXXX"
        labelName.text = "Unknown"
        labelBusiName.text = "-"
        labelCashBack.text = "-" //For receiver side, since no cashback for receiver
        labelTransType.text = transRecord?.transType
        labelTransaction.text = transRecord?.transID
        
        var tType = ""
        if let desc = transRecord?.rawDesc {
            if desc.contains("#OK-") || desc.contains("OOK"){
                if desc.contains("Top-Up") || desc.contains("TopUp") || desc.contains("TopUpPlan") || desc.contains("Bonus Point Top Up") {
                    tType = "Top-Up"
                } else if desc.contains("Data Plan") || desc.contains("DataPlan") {
                    tType = "Data Plan"
                } else if desc.contains("PAYMENTTYPE:GiftCard") {
                    tType = "GiftCard"
                } else if desc.contains("PAYMENTTYPE:DTH") {
                    tType = "DTH"
                } else if desc.contains("MPU Card") {
                    tType = "MPU"
                } else if desc.contains("Visa Card") {
                    tType = "VISA"
                } else if desc.contains("meter refund") {
                    tType = "REFUND"
                } else if desc.contains(find: "#OK-PMC"){
                    tType = "Offers"
                } else {
                    tType = "Special Offers"
                }
                labelPaymentType.text = tType
            } else {
                if let transType = transRecord?.transType {
                    tType = transType
                    if transType == "PAYWITHOUT ID" {
                        if let accTransType = transRecord?.accTransType {
                            if accTransType == "Cr" {
                                //tType = "XXXXXXXXXX"
                                tType = "HIDE MY NUMBER"
                                labelName.text = "XXXXXXX"
                            } else {
                                tType = "HIDE MY NUMBER"
                            }
                        }
                        //tType = "PAYTO"
                        if desc.contains(find: "-cashin") {
                            tType = "Cash In"
                        } else if desc.contains(find: "-cashout") {
                            tType = "Cash Out"
                        } else if desc.contains(find: "-transferto") {
                            tType = "Transfer To"
                        }  else if desc.contains(find: "#Tax Code") {
                            tType = "Tax Payment"
                        }
                        labelPaymentType.text = tType
                    } else if transType == "TOLL" {
                        tType = "TOLL"
                    } else if transType == "TICKET" {
                        tType = "TICKET"
                    } else {
                        tType = "PAYTO"
                        if desc.contains(find: "-cashin") {
                            tType = "Cash In"
                        } else if desc.contains(find: "-cashout") {
                            tType = "Cash Out"
                        } else if desc.contains(find: "-transferto") {
                            tType = "Transfer To"
                        }
                        labelPaymentType.text = tType
                     }
                }
            }
        }
        if let desc = transRecord?.rawDesc, desc.contains(find: "ELECTRICITY") {
            labelPaymentType.text = "ELECTRICITY"
        } else if let type = transRecord?.rawDesc, type.contains(find: "MPU") {
            labelPaymentType.text = "MPU"
        } else if let type = transRecord?.rawDesc, type.contains(find: "Visa") {
            labelPaymentType.text = "VISA"
        }
        
        print(transRecord!)
        labelTransType.text = tType
        let senderNameArray = transRecord?.senderName?.components(separatedBy: "-")
        if transRecord?.accTransType == "Dr" {
            buttonQR.isHidden = false
            imgViewArrow.image = UIImage(named: "reportDebitArrow")
            if transRecord?.rawDesc?.contains(find: "PAYMENTTYPE:GiftCard") ?? false {
                labelMobileNo.text = "XXXXXXXXXX"
            } else {
                if transRecord?.transType == "PAYWITHOUT ID" || transRecord?.transType == "TICKET" || transRecord?.transType == "TOLL" {
                    labelMobileNo.text = CodeSnippets.getMobileNumber(transRecord: transRecord)
                } else {
                    labelMobileNo.text = CodeSnippets.getMobileNumber(transRecord: transRecord)
                }
            }
            
            if let receiverName = transRecord?.receiverName, receiverName.count > 0 {
                           self.labelName.text = receiverName
                       }
            if let receiverBName = transRecord?.receiverBName, receiverBName.count > 0 {
                self.labelBusiName.text = receiverBName
            }
            
            let value = senderNameArray?.first ?? ""
            if value.contains("@receiveraccounttype"){
                let newValue = value.components(separatedBy: "@receiveraccounttype")
                if newValue.indices.contains(1){
                    accountTyp = UitilityClass.returnAccountNameFromType(type: newValue[1])
                }
            } else {
                if let receiverBName = transRecord?.receiverBName, receiverBName.count > 0 {
                    accountTyp = "Merchant"
                } else {
                    accountTyp = "Personal"
                }
            }
            
            if let desc = transRecord?.rawDesc {
                if desc.contains("#OK-") || desc.contains("OOK"){
                    if desc.contains("Top-Up") || desc.contains("TopUp") || desc.contains("TopUpPlan") || desc.contains("Bonus Point Top Up") {
                        accountTyp = "XXXXXXX"
                    } else if desc.contains("Data Plan") || desc.contains("DataPlan") {
                        accountTyp = "XXXXXXX"
                    }
                }
            }
            
        } else {
            buttonQR.isHidden = true
            imgViewArrow.image = UIImage(named: "reportCreditArrow")
            if let transType = transRecord?.transType, transType == "PAYWITHOUT ID" {
                    let value = senderNameArray?.first ?? ""
                    if value.contains("@sender"){
                        if value.contains("@accounttype"){
                            let newValue = value.components(separatedBy: "@accounttype")
                            
                            if newValue.indices.contains(1){
                                    let name = newValue[1].components(separatedBy: "@receiveraccounttype")
                                    if name.indices.contains(0){
                                        accountTyp = UitilityClass.returnAccountNameFromType(type: name[0])
                                    }
                            }
                        } else {
                            if let senderBName = transRecord?.senderBName, senderBName.count > 0 {
                                accountTyp = "Merchant"
                            } else {
                                accountTyp = "Personal"
                            }
                        }
                    }else{
                        if let senderBName = transRecord?.senderBName, senderBName.count > 0 {
                            accountTyp = "Merchant"
                        } else {
                            accountTyp = "Personal"
                        }
                    }
                labelName.text = "XXXXXXX"
            } else {
                
                let value = senderNameArray?.first ?? ""
                if value.contains("@sender"){
                    let newValue = value.components(separatedBy: "@sender")
                    if newValue.indices.contains(1){
                        let name = newValue[1].components(separatedBy: "@businessname")
                        if name.indices.contains(0){
                            labelName.text = name[0]
                        }
                    }
                    
                    if value.contains("@accounttype"){
                        let newValue = value.components(separatedBy: "@accounttype")
                        
                        if newValue.indices.contains(1){
                                let name = newValue[1].components(separatedBy: "@receiveraccounttype")
                                if name.indices.contains(0){
                                    accountTyp = UitilityClass.returnAccountNameFromType(type: name[0])
                                }
                        }
                    } else {
                        if let senderBName = transRecord?.senderBName, senderBName.count > 0 {
                            accountTyp = "Merchant"
                        } else {
                            accountTyp = "Personal"
                        }
                    }
                }else{
                    if let senderBName = transRecord?.senderBName, senderBName.count > 0 {
                        accountTyp = "Merchant"
                    } else {
                        accountTyp = "Personal"
                    }
                   labelName.text = senderNameArray?.first ?? ""
                }
            }
            if let senderBName = transRecord?.senderBName, senderBName.count > 0 {
                labelBusiName.text = senderBName
            }
            if transRecord?.destination == nil, let cashback = transRecord?.cashBack, cashback > 0 {
                labelMobileNo.text = "XXXXXXXXXX"
            } else {
                if transRecord?.transType == "PAYWITHOUT ID" || transRecord?.transType == "TICKET" || transRecord?.transType == "TOLL" {
                    labelMobileNo.text = "XXXXXXXXXX"
                } else {
                    labelMobileNo.text = CodeSnippets.getMobileNumber(transRecord: transRecord)
                }
            }
        }
        if self.labelName.text == "" {
            self.labelName.text = "Unknown"
        }
        
        if let desc = transRecord?.rawDesc, desc.contains(find: "ELECTRICITY") {
            labelMobileNo.text = "XXXXXXXXXX"
        } else if let type = transRecord?.rawDesc, type.contains(find: "MPU") {
            labelMobileNo.text = "XXXXXXXXXX"
        } else if let type = transRecord?.rawDesc, type.contains(find: "Visa") {
            labelMobileNo.text = "XXXXXXXXXX"
        } else if let desc = transRecord?.rawDesc, desc.contains(find: "SEND MONEY TO BANK:") {
            labelMobileNo.text = "XXXXXXXXXX"
        } else if let desc = transRecord?.rawDesc?.lowercased(), desc.contains(find: "refund".lowercased()) {
            labelMobileNo.text = "XXXXXXXXXX"
        } else if let type = transRecord?.rawDesc, (type.contains(find: "DTH") || type.contains(find: "POSTPAID") || type.contains(find: "LANDLINE") || type.contains(find: "INSURANCE") || type.contains(find: "SUNKING") || type.contains(find: "MERCHANTPAY")) {
            labelMobileNo.text = "XXXXXXXXXX"
        }  else if let type = transRecord?.rawDesc, (type.contains(find: "#OK Wallet Topup") || type.contains(find: "REFUND") || type.contains(find: "REFUND") || type.contains(find: "REFUND")) {
            labelMobileNo.text = "XXXXXXXXXX"
        } else if let type = transRecord?.rawDesc, type.contains(find: "SEND MONEY TO BANK:") {
            labelMobileNo.text = "XXXXXXXXXX"
        } else if let record = transRecord, record.destination == nil, let cashback = record.cashBack, cashback > 0 {
            labelMobileNo.text = "XXXXXXXXXX"
        } else if let desc = transRecord?.rawDesc, desc.contains(find: "Bonus Point Exchange Payment") {
            labelMobileNo.text = "XXXXXXXXXX"
        }
       
        if let desc = transRecord?.rawDesc, desc.contains(find: "#OK-PMC") {
            let descSecondString = desc.components(separatedBy: "[").last ?? ""
            let descArray = descSecondString.components(separatedBy: ",")
            if descArray.count > 3 {
                if let value = Int(descArray[3]), value == 0 {
                    if let dest = transRecord?.destination {
                        labelMobileNo.text = "XXXXXX" + String(dest.suffix(4))
                    }
                }
            }
        }
        
        labelAccountType.text = accountTyp
        labelAmount.text = ""
        if let transAmount = transRecord?.amount {
            let transAmt = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: transAmount.toString())
            labelAmount.attributedText = CodeSnippets.appendAttributedMMK(forTheAmount: transAmt)
            if transAmount.compare(0.0) == .orderedDescending {
                isAmountGTZ = true
            }
        }
        if let balanceAmount = transRecord?.walletBalance, balanceAmount != 0 {
            let transAmt = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: balanceAmount.toString())
            labelBalance.attributedText = CodeSnippets.appendAttributedMMK(forTheAmount: transAmt)
        } else {
            let transAmt = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: UserLogin.shared.walletBal)
            labelBalance.attributedText = CodeSnippets.appendAttributedMMK(forTheAmount: transAmt)
        }
        if let bonusAmount = transRecord?.bonus {
            labelBonusPoints.text =  CodeSnippets.commaFormatWithNoPrecision(forTheAmount: bonusAmount.toString()) //  feeValue -- FEE(server key)
        }
        labelDate.text = ""
        if let transDate = transRecord?.transactionDate {
            labelDate.text = transDate.stringValue()
        }
        
        let desc = transRecord?.rawDesc?.removingPercentEncoding?.replacingOccurrences(of: "+", with: " ").components(separatedBy: "-")
        if transRecord?.rawDesc?.contains(find: "ELECTRICITY") ?? false {
            let dateFormat = self.formatDate(dateString: desc?.last ?? "")
            var name: [String]?
            if desc?[9].contains(find: "Name :") ?? false {
                name = desc?[9].components(separatedBy: "Name :")
            } else if desc?[10].contains(find: "Name :") ?? false {
                name = desc?[10].components(separatedBy: "Name :")
            }
            var diviLoc = ""
            if desc?[7].contains(find: "Division") ?? false {
                let division = desc?[7].components(separatedBy: "Division :")
                if let divLocal = division?[1] {
                    let arr = divLocal.components(separatedBy: "Division")
                    diviLoc = arr[0]
                }
            } else if desc?[8].contains(find: "Division") ?? false {
                let division = desc?[8].components(separatedBy: "Division :")
                if let divLocal = division?[1] {
                    let arr = divLocal.components(separatedBy: "Division")
                    diviLoc = arr[0]
                }
            }
            
            var township: [String]?
            if desc?[8].contains(find: "Township") ?? false {
                township = desc?[8].components(separatedBy: "Township")
            } else if desc?[9].contains(find: "Township") ?? false {
                township = desc?[9].components(separatedBy: "Township")
            }
            var meterNo = ""
            var payFor:[String]?
            if desc?[6].contains(find: "PayFor:") ?? false {
                meterNo = desc?[2] ?? ""
                payFor = desc?[6].components(separatedBy: "PayFor:")
            } else if desc?[7].contains(find: "PayFor:") ?? false {
                meterNo = "\(desc?[2] ?? "")-\(desc?[3] ?? "")"
                payFor = desc?[7].components(separatedBy: "PayFor:")
            }
            let fullDesc = "\(meterNo) \(name?[1] ?? ""), \(township?[1] ?? "") Township, \(diviLoc), \(diviLoc) Division, Bill Month: \(dateFormat.0) Due Date \(dateFormat.1) \(payFor?[1] ?? "")"
            self.labelDescription.text = fullDesc.replacingOccurrences(of: ",", with: "")
            labelName.text = "Unknown"
            labelTransType.text = "ELECTRICITY"
            
            let displayNumber = UitilityClass.showMobileNumberFromDesc(desc: transRecord?.rawDesc)
            if displayNumber.destinationNumber == false {
                labelMobileNo.text = "XXXXXXXXXX"
            }
            
        } else if let desc = transRecord?.rawDesc, desc.contains(find: "SEND MONEY TO BANK:") {
            if let array = transRecord?.rawDesc?.components(separatedBy: "SEND MONEY TO BANK:"), array.count > 0 {
                self.labelDescription.text = "SEND MONEY TO BANK: \(array[1])"
            }
        } else if transRecord?.rawDesc?.contains(find: "DTH") ?? false {
            labelDescription.text = transRecord?.rawDesc?.components(separatedBy: "Destination").first ?? ""
        } else if transRecord?.rawDesc?.contains(find: "Bank To Wallet") ?? false {
            labelMobileNo.text = "XXXXXXXXXX"
            labelName.text = "XXXXXXX"
            labelDescription.text = "Remarks: " + (transRecord?.rawDesc?.components(separatedBy: "ProjectId").first ?? "")
        } else if transRecord?.rawDesc?.contains(find: "ProjectId") ?? false {
            //labelMobileNo.text = "XXXXXXXXXX"
            
            var mobileNo = "XXXXXXXXXX"
            let value = transRecord?.rawDesc ?? ""
            if value.contains(find: "ProjectId"){
                let newValue = value.components(separatedBy: " Wallet Number :")
                if newValue.count > 2 {
                    if newValue.indices.contains(1){
                        let name = newValue[2].components(separatedBy: "ProjectId")
                        if name.indices.contains(0){
                            mobileNo = "(+95)".appending(name[0])
                        }
                    }
                }  else {
                    let arrVal = value.components(separatedBy: "Account Number :")
                    if arrVal.indices.contains(1){
                        let name = arrVal[1].components(separatedBy: "ProjectId")
                        if name.indices.contains(0){
                            mobileNo = "(+95)".appending(name[0])
                        }
                    }
                }
            }
            labelMobileNo.text = mobileNo
            labelName.text = "Unknown"
            labelDescription.text = "Remarks: " + (transRecord?.rawDesc?.components(separatedBy: "ProjectId").first ?? "")
        } else if transRecord?.rawDesc?.contains(find: "#Tax Code") ?? false {
            labelMobileNo.text = "XXXXXXXXXX"
            labelName.text = "XXXXXXX"
            let tmpDesc = transRecord?.rawDesc?.replacingOccurrences(of: "#", with: "") ?? ""
            labelDescription.text = "Remarks: " + (tmpDesc.replacingOccurrences(of: "Tax Code :", with: "Tax Payment - IRD Transaction ID :"))
        } else if transRecord?.rawDesc?.contains(find: "Overseas") ?? false {
            if transRecord?.rawDesc?.contains(find: "TopUpPlan") ?? false {
                labelDescription.text = "Remarks: TopUp Plan International"
            } else {
                labelDescription.text = "Remarks: Data Plan International"
            }
        }
        else {
            
            labelDescription.text = transRecord?.desc?.replacingOccurrences(of: "#OKBILLPAYMENT", with: "").replacingOccurrences(of: "1#", with: "").replacingOccurrences(of: "2#", with: "")
        }
        
        self.imgViewFlag.image = nil
        DispatchQueue.global(qos: .userInitiated).async {
            if let countryCode = transRecord?.countryCode, countryCode.count > 0 {
                let countryData = identifyCountry(withPhoneNumber: countryCode)
                if let safeImage = UIImage(named: countryData.countryFlag) {
                    DispatchQueue.main.async {
                        self.imgViewFlag.image = safeImage
                    }
                }
            } else {
                if let mobileNumber = transRecord?.destination {
                    let countryData = identifyCountry(withPhoneNumber: mobileNumber.replaceFirstTwoChar(withStr: "+"))
                    if let safeImage = UIImage(named: countryData.countryFlag) {
                        DispatchQueue.main.async {
                            self.imgViewFlag.image = safeImage
                        }
                    }
                }
            }
        }
        if let cashBackAmount = transRecord?.cashBack, let bal = transRecord?.walletBalance, bal != 0 {
            if cashBackAmount.compare(0.0) == .orderedDescending {
                let cashBackAmt = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: cashBackAmount.toString())
                labelCashBack.attributedText = CodeSnippets.appendAttributedMMK(forTheAmount: cashBackAmt)
                self.imgViewFlag.image = #imageLiteral(resourceName: "myanmar_country_flag")
                if !isAmountGTZ {
                    self.labelMobileNo.text = transRecord?.countryCode ?? "(+95)"
                }
            }
        } else {
            labelCashBack.text = "-"
        }
        
        //Display Contact Names
        if labelName.text == "Unknown" {
            let mobileNo = labelMobileNo.text ?? ""
            let valueData = self.returnCorespondentName(value:mobileNo.replacingOccurrences(of: "(+95)0", with: ""))
            labelName.text = valueData["name"] as? String ?? "Unknown"
        }
    }
    
    func formatDate(dateString: String) -> (String, String) {
        let dateStr = dateString.components(separatedBy: "Due Date :")
        if dateStr.count > 0 {
            let currentDate = dateStr[1].replacingOccurrences(of: "#", with: "")
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy hh:mm:ss a"
            let date = formatter.date(from: currentDate)
            let previousMonth = Calendar.current.date(byAdding: .month, value: -1, to: date ?? Date())
            formatter.dateFormat = "dd-MMM-yyyy"
            let strDate = formatter.string(from: date ?? Date())
            formatter.dateFormat = "MMMM"
            let prevMonth = formatter.string(from: previousMonth ?? Date())
            return(prevMonth, strDate)
        }
        return ("", "")
    }
    
    func returnCorespondentName(value: String) -> [String:AnyObject]{
        
        if okContacts.isEmpty {
            return ["Unknown":"Unknown" as AnyObject]
        } else {
            return self.getDetails(Ucontacts: okContacts, number: "\(value.replacingOccurrences(of: "0095", with: ""))")
        }
    }
    
    func getDetails(Ucontacts:Array<CNContact>, number:String) -> [String:AnyObject]{
        /* Get mobile number with mobile country code */
        
        var userName = [String:AnyObject]()
        
        for i in 0...(Ucontacts.count-1) {
            
            for ContctNumVar: CNLabeledValue in Ucontacts[i].phoneNumbers {
                 let FulMobNumVar  = ContctNumVar.value
                 var MobNumVar = ""
                if let numFull = FulMobNumVar.value(forKey: "digits") as? String {
                    MobNumVar = numFull
                }
//                println_debug("user Mobile Number : \(number)")
//                println_debug("Comapre Mobile Number : \(String(describing: MobNumVar))")
                if MobNumVar.contains(find: number) {
                    userName["name"] = (Ucontacts[i].givenName + " " + Ucontacts[i].familyName) as AnyObject
                    userName["pic"] = Ucontacts[i].imageData as AnyObject
                    return userName
                } else {
                    userName["name"] = "Unknown" as AnyObject
                    userName["pic"] = nil
                }
            }
        }
        return userName
    }
}
