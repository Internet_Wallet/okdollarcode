//
//  ReportBillPaymentCell.swift
//  OK
//  This will show the report bill payment record
//  Created by ANTONY on 02/02/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportBillPaymentCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var labelTransaction: UILabel!{
        didSet{
            self.labelTransaction.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAmount: UILabel!{
        didSet{
            self.labelAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var imgViewFlag: UIImageView!
    @IBOutlet weak var imgViewArrow: UIImageView!
    @IBOutlet weak var labelMobileNo: UILabel!{
        didSet{
            self.labelMobileNo.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelComments: UILabel!{
        didSet{
            self.labelComments.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelBonusPoints: UILabel!{
        didSet{
            self.labelBonusPoints.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelCashBack: UILabel!{
        didSet{
            self.labelCashBack.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelBalance: UILabel!{
        didSet{
            self.labelBalance.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDate: UILabel!{
        didSet{
            self.labelDate.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDescription: UILabel!{
        didSet{
            self.labelDescription.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonPdf: UIButton!{
        didSet{
            self.buttonPdf.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonQR: UIButton! {
        didSet {
            let image = UIImage(named: "generate_agentcode_st")?.withRenderingMode(.alwaysTemplate).imageWithColor(tintColor: .white)
            self.buttonQR.setImage(image, for: .normal)
        }
    }
    
    @IBOutlet weak var billMonth: UILabel!
    @IBOutlet weak var billDueDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewContent.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureReportBillItemCell(billRecord: ReportTransactionRecord?) {
        labelMobileNo.text = ""
        //self.viewContent.backgroundColor = ConstantsColor.navigationHeaderBillPayment
        imgViewArrow.image = nil
        labelCashBack.text = "0" //For receiver side, since no cashback for receiver
        if let accTransType = billRecord?.accTransType {
            if accTransType == "Dr" {
                buttonQR.isHidden = false
                imgViewArrow.image = UIImage(named: "reportDebitArrow")
                //labelMobileNo.text = CodeSnippets.getMobileNumber(transRecord: billRecord)
                if let cashBackAmount = billRecord?.cashBack {
                    labelCashBack.text =  CodeSnippets.commaFormatWithNoPrecision(forTheAmount: cashBackAmount.toString()) //    feeAmount -- fee(server key)
                }
            } else {
                buttonQR.isHidden = true
                imgViewArrow.image = UIImage(named: "reportCreditArrow")
//                if billRecord?.transType == "PAYWITHOUT ID" {
//                    labelMobileNo.text = "XXXXXXXXXX"
//                } else {
//                    labelMobileNo.text = CodeSnippets.getMobileNumber(transRecord: billRecord)
//                }
            }
        }
        
        labelAmount.text = ""
        if let transAmount = billRecord?.amount {
            let transAmt = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: transAmount.toString())
            labelAmount.attributedText = CodeSnippets.appendAttributedMMK(forTheAmount: transAmt)
        }
        if let balanceAmount = billRecord?.walletBalance {
            let balAmt = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: balanceAmount.toString())
            self.labelBalance.attributedText = CodeSnippets.appendAttributedMMK(forTheAmount: balAmt)
        }
        if let bonusAmount = billRecord?.bonus {
            labelBonusPoints.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: bonusAmount.toStringNoDecimal()) //  feeValue -- FEE(server key)
        }
        
        labelDate.text = ""
        if let transDate = billRecord?.transactionDate {
            labelDate.text = transDate.stringValue()
        }
        var billtype = ""
        if billRecord?.desc?.contains(find: "ELECTRICITY") ?? false {
           billtype = "ELECTRICITY"
        } else if billRecord?.desc?.contains(find: "POSTPAID") ?? false || billRecord?.desc?.contains(find: "Post paid") ?? false {
            billtype = "POSTPAID"
        } else if billRecord?.desc?.contains(find: "LANDLINE") ?? false || billRecord?.rawDesc?.contains("Landline") ?? false {
            billtype = "LANDLINE"
        } else if billRecord?.desc?.contains(find: "SUNKING") ?? false {
            billtype = "SUNKING"
        } else if billRecord?.desc?.contains(find: "INSURANCE") ?? false {
            billtype = "INSURANCE"
        } else if billRecord?.desc?.contains(find: "MERCHANTPAY") ?? false {
            billtype = "MERCHANTPAY"
        } else if billRecord?.desc?.contains(find: "DTH") ?? false {
            billtype = "DTH"
        } else if billRecord?.desc?.contains(find: "GiftCard") ?? false {
            billtype = "GiftCard"
        } else {
            billtype = ""
        }
        labelMobileNo.text = billtype
        let desc = billRecord?.rawDesc?.removingPercentEncoding?.replacingOccurrences(of: "+", with: " ").components(separatedBy: "-")
        if billRecord?.rawDesc?.contains(find: "ELECTRICITY") ?? false {
            
            let dateFormat = self.formatDate(dateString: desc?.last ?? "")
            var name: [String]?
            if desc?[9].contains(find: "Name :") ?? false {
                name = desc?[9].components(separatedBy: "Name :")
            } else if desc?[10].contains(find: "Name :") ?? false {
                name = desc?[10].components(separatedBy: "Name :")
            }
            var diviLoc = ""
            if desc?[7].contains(find: "Division") ?? false {
                let division = desc?[7].components(separatedBy: "Division :")
                if let divLocal = division?[1] {
                    let arr = divLocal.components(separatedBy: "Division")
                    diviLoc = arr[0]
                }
            } else if desc?[8].contains(find: "Division") ?? false {
                let division = desc?[8].components(separatedBy: "Division :")
                if let divLocal = division?[1] {
                    let arr = divLocal.components(separatedBy: "Division")
                    diviLoc = arr[0]
                }
            }
            
            var township: [String]?
            if desc?[8].contains(find: "Township") ?? false {
                township = desc?[8].components(separatedBy: "Township")
            } else if desc?[9].contains(find: "Township") ?? false {
               township = desc?[9].components(separatedBy: "Township")
            }
            var payFor: [String]?
            var meterNo = ""
            if desc?[6].contains(find: "PayFor:") ?? false {
                meterNo = desc?[2] ?? ""
                payFor = desc?[6].components(separatedBy: "PayFor:")
            } else if desc?[7].contains(find: "PayFor:") ?? false {
                meterNo = "\(desc?[2] ?? "")-\(desc?[3] ?? "")"
                payFor = desc?[7].components(separatedBy: "PayFor:")
            }
            let fullDesc = "\(meterNo), \(name?[1] ?? ""), \(township?[1] ?? "") Township, \(diviLoc), \(diviLoc) Division, Bill Month: \(dateFormat.0), Due Date \(dateFormat.1), \(payFor?[1] ?? "")"
            labelDescription.text = fullDesc
        } else if billRecord?.rawDesc?.contains(find: "DTH") ?? false {
            labelDescription.text = billRecord?.rawDesc?.components(separatedBy: "Destination").first ?? ""
        }  else {
            labelDescription.text = billRecord?.desc
        }
        if billRecord?.rawDesc?.contains(find: "PAYMENTTYPE:GiftCard") ?? false {
            billMonth.text = ""
            billDueDate.text = ""
        } else if (billRecord?.rawDesc?.contains("Post-paid") ?? false || billRecord?.rawDesc?.contains("Post paid") ?? false || billRecord?.rawDesc?.contains("landline") ?? false || billRecord?.rawDesc?.contains("Landline") ?? false)  {
            let value = billRecord?.rawDesc ?? ""
            if value.contains(",Month-"){
                let newValue = value.components(separatedBy: ",Month-")
                if newValue.indices.contains(1){
                    let name = newValue[1].components(separatedBy: ", Due Date-")
                    if name.indices.contains(1){
                        billMonth.text = name[0]
                        billDueDate.text = name[1]
                    }
                }
            } else {
                billMonth.text = ""
                billDueDate.text = ""
            }
        }
        else {
            let dateFormat = self.formatDate(dateString: desc?.last ?? "")
            billMonth.text = dateFormat.0
            billDueDate.text = dateFormat.1
        }
        labelComments.text = billRecord?.senderName
        labelTransaction.text = billRecord?.transID
        
        self.imgViewFlag.image = nil
        DispatchQueue.global(qos: .userInitiated).async {
            if let mobileNumber = billRecord?.destination {
                let countryData = identifyCountry(withPhoneNumber: mobileNumber.replaceFirstTwoChar(withStr: "+"))
                if let safeImage = UIImage(named: countryData.countryFlag) {
                    DispatchQueue.main.async {
                        self.imgViewFlag.image = safeImage
                    }
                }
            }
        }
    }
    
    func formatDate(dateString: String) -> (String, String) {
        let dateStr = dateString.components(separatedBy: "Due Date :")
        if dateStr.count > 1 {
            let currentDate = dateStr[1].replacingOccurrences(of: "#", with: "")
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy hh:mm:ss a"
            let date = formatter.date(from: currentDate)
            let previousMonth = Calendar.current.date(byAdding: .month, value: -1, to: date ?? Date())
            formatter.dateFormat = "EEE, dd-MMM-yyyy"
            let strDate = formatter.string(from: date ?? Date())
            formatter.dateFormat = "MMMM"
            let prevMonth = formatter.string(from: previousMonth ?? Date())
            return(prevMonth, strDate)
        }
        return ("", "")
    }
}
