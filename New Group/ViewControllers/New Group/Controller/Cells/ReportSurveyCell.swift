//
//  ReportSurveyCell.swift
//  OK
//  This show the survey detail record of the list
//  Created by ANTONY on 05/02/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportSurveyCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var mobileNumber: UILabel!{
        didSet{
            self.mobileNumber.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var btnStarOne: UIButton!
    @IBOutlet weak var btnStarTwo: UIButton!
    @IBOutlet weak var btnStarThree: UIButton!
    @IBOutlet weak var btnStarFour: UIButton!
    @IBOutlet weak var btnStarFive: UIButton!
    @IBOutlet weak var feedBack: MarqueeLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureResaleReportCell(transRecord: ReportTransactionRecord) {
        
        self.viewContent.layer.cornerRadius = 5
        self.layerShadow()
        self.feedBack.type = .continuous
        self.feedBack.speed = .duration(35)
        self.feedBack.text = ""
        btnStarFive.setImage(#imageLiteral(resourceName: "receiptStarUnfilled"), for: .normal)
        btnStarFour.setImage(#imageLiteral(resourceName: "receiptStarUnfilled"), for: .normal)
        btnStarThree.setImage(#imageLiteral(resourceName: "receiptStarUnfilled"), for: .normal)
        btnStarTwo.setImage(#imageLiteral(resourceName: "receiptStarUnfilled"), for: .normal)
        btnStarOne.setImage(#imageLiteral(resourceName: "receiptStarUnfilled"), for: .normal)
        mobileNumber.text = ""
        
        if let transType = transRecord.transType {
            if transType == "PAYWITHOUT ID" {
                mobileNumber.text = "XXXXXXXXXX"
            } else {
                if let mobileNum = transRecord.destination {
                    var phNumber = mobileNum.replaceFirstTwoChar(withStr: "+")
                    phNumber.insert(")", at: phNumber.index(phNumber.startIndex, offsetBy: 3))
                    phNumber.insert("(", at: phNumber.startIndex)
                    if phNumber.hasPrefix("(+95)") {
                        phNumber.insert("0", at: phNumber.index(phNumber.startIndex, offsetBy: 5))
                    }
                    mobileNumber.text = phNumber
                }
            }
            let rating = transRecord.rating ?? ""
            let feedBack = transRecord.ratingFeedback ?? ""
            self.fillRatingsFeedback(starCount: rating, feedback: feedBack)
        }
    }
    
    func fillRatingsFeedback(starCount: String, feedback: String) {
        feedBack.text = feedback + " "
        switch starCount {
        case "5":
            btnStarFive.setImage(#imageLiteral(resourceName: "receiptStarFilled"), for: .normal)
            fallthrough
        case "4":
            btnStarFour.setImage(#imageLiteral(resourceName: "receiptStarFilled"), for: .normal)
            fallthrough
        case "3":
            btnStarThree.setImage(#imageLiteral(resourceName: "receiptStarFilled"), for: .normal)
            fallthrough
        case "2":
            btnStarTwo.setImage(#imageLiteral(resourceName: "receiptStarFilled"), for: .normal)
            fallthrough
        case "1":
            btnStarOne.setImage(#imageLiteral(resourceName: "receiptStarFilled"), for: .normal)
        default:
            break
        }
    }
}
