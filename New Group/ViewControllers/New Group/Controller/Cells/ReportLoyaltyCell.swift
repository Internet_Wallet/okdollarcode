//
//  ReportLoyaltyCell.swift
//  OK
//
//  Created by E J ANTONY on 10/10/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportLoyaltyCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var lblTransID: UILabel!{
        didSet{
            self.lblTransID.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblMercNumber: UILabel!{
        didSet{
            self.lblMercNumber.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblShopName: UILabel!{
        didSet{
            self.lblShopName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblDestNumber: UILabel!{
        didSet{
            self.lblDestNumber.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblTransType: UILabel!{
        didSet{
            self.lblTransType.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
//    @IBOutlet weak var lblBonusPoint: UILabel!
    @IBOutlet weak var lblBalanceBonusPoint: UILabel!{
        didSet{
            self.lblBalanceBonusPoint.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblDate: UILabel!{
        didSet{
            self.lblDate.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblComments: UILabel!{
        didSet{
            self.lblComments.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewContent.layer.cornerRadius = 5
        self.layer.masksToBounds = true
        self.viewContent.layer.masksToBounds = false
        self.viewContent.layerShadow(offsetValue: CGSize(width: 0, height: 1.0), opacity: 0.7)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(record: LoyaltyData) {
        lblTransID.text = record.transId ?? ""
        let mobileNum = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: record.merchantCode.safelyWrappingString())
        lblMercNumber.text = "(\(mobileNum.counrtycode))\(mobileNum.mobileNo)"
        lblShopName.text = record.merchantName ?? ""
        lblDestNumber.text = (record.transferPoint ?? "").replacingOccurrences(of: ".00", with: "")
        if let date = record.validateDate {
            let stringdate = date.dateValue(dateFormatIs: "yyyy-MM-dd") ?? Date()
            lblTransType.text = stringdate.stringValue(dateFormatIs: "dd-MMM-yyyy")
        }
        lblBalanceBonusPoint.text = (record.closingPoint ?? "").replacingOccurrences(of: ".00", with: "")
        if let date = record.transDate {
            let stringdate = date.dateValue(dateFormatIs: "yyyy-MM-dd HH:mm:ss") ?? Date()
            lblDate.text = stringdate.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy\nHH:mm:ss")
        }
        lblComments.text = "Comments: \(record.comments ?? "")"
    }
}
