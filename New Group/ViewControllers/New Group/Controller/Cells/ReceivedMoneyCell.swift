//
//  ReceivedMoneyCell.swift
//  OK
//  This cell is used to show the record of received money
//  Created by ANTONY on 09/03/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReceivedMoneyCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var imgViewUser: UIImageView!
    @IBOutlet weak var labelTransaction: UILabel!{
        didSet{
            self.labelTransaction.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAmount: UILabel!{
        didSet{
            self.labelAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var imgViewFlag: UIImageView!
    @IBOutlet weak var imgViewArrow: UIImageView!
    @IBOutlet weak var labelMobileNo: UILabel!{
        didSet{
            self.labelMobileNo.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelComments: UILabel!{
        didSet{
            self.labelComments.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelBalance: UILabel!{
        didSet{
            self.labelBalance.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDate: UILabel!{
        didSet{
            self.labelDate.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelAccountType: UILabel!{
        didSet{
            self.labelAccountType.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelDescription: UILabel!{
        didSet{
            self.labelDescription.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelBusinessName: UILabel!{
        didSet{
            self.labelBusinessName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonPdf: UIButton!{
        didSet{
            self.buttonPdf.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonQR: UIButton!{
        didSet{
            self.buttonQR.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewContent.layer.cornerRadius = 5
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureReceivedMoneyCell(transRecord: ReportTransactionRecord?) {
        
        let senderNameArray = transRecord?.senderName?.components(separatedBy: "-")
    
//        let type = (((senderNameArray?.count ?? 0) > 1) ? senderNameArray?.last ?? "" : "").components(separatedBy: ":")
//        let accountType = UitilityClass.returnAccountType(type: type.last)
//        labelAccountType.text = accountType
        
        labelMobileNo.text = ""
        buttonQR.isHidden = true
        
        if transRecord?.destination == nil, let cashBack = transRecord?.cashBack, cashBack > 0 {
            labelMobileNo.text = "XXXXXXXXXX"
        } else {
            if transRecord?.transType == "PAYWITHOUT ID" {
                labelMobileNo.text = "XXXXXXXXXX"
            } else {
                labelMobileNo.text = CodeSnippets.getMobileNumber(transRecord: transRecord)
            }
        }
        
        if transRecord?.rawDesc?.contains(find: "MPU") ?? false || transRecord?.rawDesc?.contains(find: "Visa") ?? false {
            labelMobileNo.text = "XXXXXXXXXX"
        } else if transRecord?.rawDesc?.contains(find: "meter refund") ?? false {
            labelMobileNo.text = "XXXXXXXXXX"
        } else if let desc = transRecord?.rawDesc, desc.contains(find: "Bonus Point Exchange Payment") {
            labelMobileNo.text = "XXXXXXXXXX"
        } else if let desc = transRecord?.rawDesc, desc.contains(find: "Bank To Wallet") {
            labelMobileNo.text = "XXXXXXXXXX"
        } 
        
        labelBusinessName.text = "-"
        if let receiverName = transRecord?.senderBName, receiverName.count > 0 {
            labelBusinessName.text = receiverName
        }
        
        labelAmount.text = ""
        if let transAmount = transRecord?.amount {
            let transAmt = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: transAmount.toString())
            labelAmount.attributedText = CodeSnippets.appendAttributedMMK(forTheAmount: transAmt)
        }
        if let balanceAmount = transRecord?.walletBalance {
            let transAmt = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: balanceAmount.toString())
            labelBalance.attributedText = CodeSnippets.appendAttributedMMK(forTheAmount: transAmt)
        }
        
        labelDate.text = ""
        if let transDate = transRecord?.transactionDate {
            labelDate.text = transDate.stringValue()
        }
        
        labelDescription.text = transRecord?.desc?.replacingOccurrences(of: "1#", with: "").replacingOccurrences(of: "2#", with: "")
        
        let value = senderNameArray?.first ?? ""
        if value.contains("@sender"){
            let newValue = value.components(separatedBy: "@sender")
            if newValue.indices.contains(1){
                let name = newValue[1].components(separatedBy: "@businessname")
                if name.indices.contains(0){
                     labelComments.text = name[0]
                }
            }
           // let newData = newValue.components(separatedBy: "@businessname")
           // print(newData)
        }else{
           labelComments.text = ((senderNameArray?.first ?? "") != "") ? senderNameArray?.first : "Unknown"
        }
        
        labelTransaction.text = transRecord?.transID
        //labelAccountType.text = ((transRecord?.senderBName?.count ?? 0) > 0) ? "Merchant" : "Personal"
        if value.contains("@accounttype"){
            let newValue = value.components(separatedBy: "@accounttype")
            
            if newValue.indices.contains(1){
                    let name = newValue[1].components(separatedBy: "@receiveraccounttype")
                    if name.indices.contains(0){
                        labelAccountType.text = UitilityClass.returnAccountNameFromType(type: name[0])
                    }
            }
        } else {
            if let senderBName = transRecord?.senderBName, senderBName.count > 0 {
                labelAccountType.text = "Merchant"
            } else {
                labelAccountType.text = "Personal"
            }
        }
        
        self.imgViewFlag.image = nil
        DispatchQueue.global(qos: .userInitiated).async {
            if let mobileNumber = transRecord?.destination {
                let countryData = identifyCountry(withPhoneNumber: mobileNumber.replaceFirstTwoChar(withStr: "+"))
                if let safeImage = UIImage(named: countryData.countryFlag) {
                    DispatchQueue.main.async {
                        self.imgViewFlag.image = safeImage
                    }
                }
            }
        }
    }
}
