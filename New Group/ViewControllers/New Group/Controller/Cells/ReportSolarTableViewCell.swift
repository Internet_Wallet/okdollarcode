//
//  ReportSolarTableViewCell.swift
//  OK
//
//  Created by E J ANTONY on 09/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportSolarTableViewCell: UITableViewCell {

    //MARK: - Outlet
    @IBOutlet weak var lblName: UILabel!{
        didSet{
            self.lblName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblTransactionID: UILabel!{
        didSet{
            self.lblTransactionID.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblAmount: UILabel!{
        didSet{
            self.lblAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblServiceFees: UILabel!{
        didSet{
            self.lblServiceFees.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblAccountNo: UILabel!{
        didSet{
            self.lblAccountNo.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblStatus: UILabel!{
        didSet{
            self.lblStatus.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblDate: UILabel!{
        didSet{
            self.lblDate.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var boxView: UIView!
    
    //MARK: - Views Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        self.boxView.layer.cornerRadius = 5
        self.layer.masksToBounds = true
        self.boxView.layer.masksToBounds = false
        self.boxView.layerShadow(offsetValue: CGSize(width: 0, height: 1.0), opacity: 0.7)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(solarReportData: SolarReport) {
        lblName.text = solarReportData.customer?.customerName ?? ""
        lblTransactionID.text = solarReportData.customerOkPaymentID ?? ""
        lblAmount.text = ""
        if let amnt = solarReportData.amount {
           let amont = wrapAmountWithCommaDecimal(key: "\(amnt)")
            lblAmount.attributedText = wrapAmountWithMMK(bal: amont)
        }
        lblServiceFees.text = ""
        if let sCharge = solarReportData.serviceCharges {
            lblServiceFees.text = "\(sCharge)".replacingOccurrences(of: ".0", with: "")
        }
        lblAccountNo.text = solarReportData.solarHomeAccountNumber ?? ""
        lblStatus.text = solarReportData.topupStatus ?? ""
        lblDate.text = ""
        if let createdDateStr = solarReportData.createdDate {
            if let dateValue = createdDateStr.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") {
                let dateString = dateValue.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy\nHH:mm:ss")
                self.lblDate.text = dateString
            }
        }
    }
}
