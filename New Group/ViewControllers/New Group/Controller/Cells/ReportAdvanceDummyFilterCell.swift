//
//  ReportAdvanceDummyFilterCell.swift
//  OK
//
//  Created by E J ANTONY on 13/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportAdvanceDummyFilterCell: UITableViewCell {
    //MARK: - Properties
    @IBOutlet weak var lblName: UILabel!{
        didSet{
            self.lblName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblNumber: UILabel!{
        didSet{
            self.lblNumber.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblAmount: UILabel!{
        didSet{
            self.lblAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var imgViewTick: UIImageView!
    @IBOutlet weak var imgViewAvatar: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    //MARK: - Methods
    func configureCell(name: String, no: String, amount: String, isSelected: Bool) {
        self.lblName.text = name
        self.lblNumber.text = no
       // let formtdNumber = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: amount)
       // self.lblAmount.attributedText = CodeSnippets.getCurrencyInAttributed(withString: formtdNumber, amountSize: 16, amountColor: .black, currencyDenomSize: 12, currencyDenomColor: .black)
        if isSelected {
            imgViewTick.image = #imageLiteral(resourceName: "reportGreenTick")
        } else {
            imgViewTick.image = nil
        }
    }
    
    func configureCellNew(name: String, no: String, amount: Double, isSelected: Bool) {
        let amountstr = "\(amount)"
        
        self.lblName.text = name
        self.lblNumber.text = no
        
       //  let formtdNumber = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: amountstr)
        // self.lblAmount.attributedText = CodeSnippets.getCurrencyInAttributed(withString: formtdNumber, amountSize: 16, amountColor: .black, currencyDenomSize: 12, currencyDenomColor: .black)
        
       self.lblAmount.attributedText = CodeSnippets.getCurrencyInAttributed(withString: "\(amountstr)")
        
        if isSelected {
            imgViewTick.image = #imageLiteral(resourceName: "reportGreenTick")
        } else {
            imgViewTick.image = nil
        }
    }
}
