//
//  ReportOfferFilterCell.swift
//  OK
//
//  Created by E J ANTONY on 11/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportOfferFilterCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet weak var labelTitle: UILabel!{
        didSet{
            self.labelTitle.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var labelValue: UILabel!{
        didSet{
            self.labelValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configureCell(titleName: String, value: String) {
        self.labelTitle.text = titleName.localized
        self.labelValue.text = value
        if value.count == 0 {
            self.labelValue.isHidden = true
        } else {
            self.labelValue.isHidden = false
        }
    }
}
