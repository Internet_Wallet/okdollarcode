//
//  FilterTableViewCell.swift
//  OK
//  It shows the filter item
//  Created by ANTONY on 29/01/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var imgViewBackground: UIImageView!
    @IBOutlet weak var labelFilterName: MarqueeLabel!{
        didSet{
            self.labelFilterName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var imgViewDropDown: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //self.imgViewBackground.layer.cornerRadius = 5
        //self.imgViewBackground.image = UIImage(named: "reportFilterCellBg")
        self.imgViewDropDown.image = UIImage(named: "reportArrowDownBlue")
        self.backgroundColor = UIColor.white
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureLabel(labelTitle: String) {
        self.imgViewDropDown.isHidden = true
        labelFilterName.text = labelTitle.localized + " "
       // labelFilterName.font = UIFont(name: appFont, size: appFontSizeReport)
    }
    
    func configureTopUpFilter(filterName: String) {
        configureLabel(labelTitle: filterName)
    }
    
    func configureMoreOptionInTopUp(filterName: String) {
        configureLabel(labelTitle: filterName)
    }
    
    func configureMoreOptionInCICO(filterName: String) {
        configureLabel(labelTitle: filterName)
    }
    
    func configureSendMoneyToBank(filterName: String) {
        configureLabel(labelTitle: filterName)
    }
    
    func configureReSaleMainBalance(filterName: String) {
        configureLabel(labelTitle: filterName)
    }
    
    func configureStatistics(filterName: String) {
        configureLabel(labelTitle: filterName)
    }
    
    
}
