//
//  ReportItemTableViewCell.swift
//  OK
//  This is used to show the report item
//  Created by ANTONY on 26/01/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportItemTableViewCell: UITableViewCell {
    // MARK: - Outlet
    @IBOutlet weak var labelItemName: UILabel!{
        didSet{
            self.labelItemName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imageViewItem: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - It configures the cell
    func configureCell(itemName: String, imageName: String) {
        self.labelItemName.text = itemName.localized
        guard let reportImage = UIImage(named: imageName) else { return }
        self.imageViewItem.image = reportImage
    }
}
