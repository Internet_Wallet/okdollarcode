//
//  ReportCashInCell.swift
//  OK
//  This show the record of cash in and out
//  Created by ANTONY on 05/02/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportCashInCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var lblTransID: UILabel!{
        didSet{
            self.lblTransID.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblMobileNumber: UILabel!{
        didSet{
            self.lblMobileNumber.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblName: UILabel!{
        didSet{
            self.lblName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblCashInAmount: UILabel!{
        didSet{
            self.lblCashInAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblCashOutAmount: UILabel!{
        didSet{
            self.lblCashOutAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblServiceFee: UILabel!{
        didSet{
            self.lblServiceFee.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblBalance: UILabel!{
        didSet{
            self.lblBalance.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblDate: UILabel!{
        didSet{
            self.lblDate.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblComments: UILabel!{
        didSet{
            self.lblComments.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var initiateAmtLbl: UILabel!{
        didSet{
            self.initiateAmtLbl.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var imgViewFlag: UIImageView!
    @IBOutlet weak var imgViewArrow: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewContent.layer.cornerRadius = 5
        self.layer.masksToBounds = true
        self.viewContent.layer.masksToBounds = false
        self.viewContent.layerShadow(offsetValue: CGSize(width: 0, height: 1.0), opacity: 0.7)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(record: ReportCICOList) {
        lblName.text = "Unknown"
        lblBalance.text = ""
        lblDate.text = ""
        lblTransID.text = record.transactionID ?? ""
        if let phNo = record.receiverPhoneNumber {
            if phNo == UserModel.shared.mobileNo {
                if let rqstPhNo = record.requesterPhoneNumber {
                    if rqstPhNo.hasPrefix("0095") {
                        lblMobileNumber.text = "0\(rqstPhNo.substring(from: 4))"
                    }
                }
                lblName.text = record.senderName ?? ""
                if let payType = record.paymentType, (payType == "PAYTOID" || payType == "PAYWITHOUT ID") {
                    lblMobileNumber.text = "XXXXXXXXXX"
                }
            } else {
                if phNo.hasPrefix("0095") {
                    lblMobileNumber.text = "0\(phNo.substring(from: 4))"
                }
                lblName.text = record.receiverName ?? ""
            }
        }
        if let amountStr = record.commissionPercent {
            lblServiceFee.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(amountStr)")
        }
        if let phNo = record.receiverPhoneNumber, phNo == UserModel.shared.mobileNo {
            lblBalance.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(UserLogin.shared.walletBal)")
        } else if let balance = record.senderClosingBalance {
            lblBalance.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(balance)")
        }
        if let dateStr = record.convertedDate {
            if let dateInDF = dateStr.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") {
                lblDate.text = dateInDF.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss")
            }
        }
//        if let commisson = record.commissionPercent, let totAmount = record.totalAmount {
//            if record.cashType == 0 && ((record.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo) {
//                let initiateAmt = totAmount + commisson
//                self.initiateAmtLbl.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(initiateAmt)")
//            } else {
//                self.initiateAmtLbl.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(totAmount)")
//            }
//        }
        
        lblComments.text = ""
        if (record.cashType == 0 && ((record.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo)) ||  (record.cashType == 1 && ((record.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo)) {
            
            lblComments.text = (record.cashType == 0) ? "Service: Cash-In" : "Service: Cash-Out"
            lblCashInAmount.text = "0"
            lblCashOutAmount.text = "0"
            if record.cashType == 0 && ((record.requesterPhoneNumber ?? "") == UserModel.shared.mobileNo) {
                if let commisson = record.commissionPercent, let totAmount = record.rechargeAmount {
                    let initiateAmt = totAmount + commisson
                    lblCashInAmount.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(totAmount)")
                    initiateAmtLbl.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(initiateAmt)")
                }
            } else {
                if let amountStr = record.totalAmount {
                    lblCashInAmount.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(amountStr)")
                    initiateAmtLbl.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(amountStr)")
                }
            }
            self.imgViewArrow.image = UIImage(named: "reportDebitArrow")
        }  else {
            lblComments.text = (record.cashType == 0) ? "Service: Cash-In" : "Service: Cash-Out"
            lblCashInAmount.text = "0"
            lblCashOutAmount.text = "0"
            if record.cashType == 0 && ((record.receiverPhoneNumber ?? "") == UserModel.shared.mobileNo) {
                if let commisson = record.commissionPercent, let totAmount = record.rechargeAmount {
                    let initiateAmt = totAmount + commisson
                    lblCashOutAmount.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(totAmount)")
                    initiateAmtLbl.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(initiateAmt)")
                }
            } else {
                if let amountStr = record.totalAmount {
                    lblCashOutAmount.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(amountStr)")
                    initiateAmtLbl.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: "\(amountStr)")                }
            }
            self.imgViewArrow.image = UIImage(named: "reportCreditArrow")
        }
        self.imgViewFlag.image = UIImage(named: "myanmar")
    }
}
