//
//  ReportSendMoneyPdfHeader.swift
//  OK
//
//  Created by ANTONY on 14/03/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportSendMoneyPdfHeader: UIView {

    @IBOutlet weak var businessNameheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var accName: UILabel!{
        didSet{
            self.accName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var accNumber: UILabel!{
        didSet{
            self.accNumber.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var email: UILabel!{
        didSet{
            self.email.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var address: UILabel!{
        didSet{
            self.address.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var period: UILabel!{
        didSet{
            self.period.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var totalTransaction: UILabel!{
        didSet{
            self.totalTransaction.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var walletBalanceLbl: UILabel!{
        didSet{
            self.walletBalanceLbl.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var dateTime: UILabel!{
        didSet{
            self.dateTime.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var totalTransactionAmount: UILabel!{
        didSet{
            self.totalTransactionAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterAccountType: UILabel!{
        didSet{
            self.filterAccountType.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterAmount: UILabel!{
        didSet{
            self.filterAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var filterPeriod: UILabel!{
        didSet{
            self.filterPeriod.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var pdfHeaderLabel: UILabel!{
        didSet{
            self.pdfHeaderLabel.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var viewHeaderForReportSendMoney: UIView!
    
    @IBOutlet weak var businessNameLbl: UILabel!{
        didSet{
            self.businessNameLbl.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var businessNameVal: UILabel!{
        didSet{
            self.businessNameVal.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var accountTypeLbl: UILabel!{
        didSet{
            self.accountTypeLbl.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var accountTypeVal: UILabel!{
        didSet{
            self.accountTypeVal.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var addressHeight: NSLayoutConstraint!
    func fillDetails(sendMoneyBankReports: [ReportSendMoneyToBank], filterPeriod: String, filterAmount: String, filterAccount: String) {
        var totalTransAmount: NSDecimalNumber = 0
        for record in sendMoneyBankReports {
            if let amount = record.transferredAmount {
                totalTransAmount = totalTransAmount.adding(amount)
            }
        }
        let balance : Double = (UserLogin.shared.walletBal as NSString).doubleValue
        let doubleStrBalance = String(format: "%.2f", balance)
        self.walletBalanceLbl.text = wrapAmountWithCommaDecimal(key: doubleStrBalance) + " MMK"
        self.accName.text = "\(UserModel.shared.name)"
        if UserModel.shared.agentType == .user {
            businessNameheightConstraint.constant = 0
        } else {
            businessNameheightConstraint.constant = 21
            self.businessNameVal.text = (UserModel.shared.businessName != "") ? UserModel.shared.businessName : "-"
        }
        let accountType = self.agentServiceTypeString(type: UserModel.shared.agentType)
        var phNumber = UserModel.shared.mobileNo.replaceFirstTwoChar(withStr: "+")
        phNumber.insert(")", at: phNumber.index(phNumber.startIndex, offsetBy: 3))
        phNumber.insert("(", at: phNumber.startIndex)
        if UserModel.shared.mobileNo.hasPrefix("0095") {
            phNumber.insert("0", at: phNumber.index(phNumber.startIndex, offsetBy: 5))
        }
        self.accNumber.text = "\(phNumber)"
        self.email.text = "\(UserModel.shared.email)".components(separatedBy: ",").first ?? ""
        let amountInString = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: totalTransAmount.toString())
        self.totalTransactionAmount.text = UIViewController().amountInFormat(amountInString) + " " + "MMK".localized
        self.dateTime.text = "\(Date().stringValueWithOutUTC(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss"))"
        self.totalTransaction.text = "\(sendMoneyBankReports.count)"
        var titleStr = ""
        if filterAccount == "Account Type".localized {
            titleStr = "All".localized
        } else {
            titleStr = filterAccount
        }
        self.filterAccountType.text = titleStr
        if filterAmount == "Amount".localized {
            titleStr = "All".localized
        } else {
            titleStr = filterAmount
        }
        self.filterAmount.text = titleStr
        if filterPeriod == "Period".localized {
            titleStr = "All".localized
        } else {
            titleStr = filterPeriod
        }
        titleStr = titleStr.replacingOccurrences(of: "\n", with: " ")
        self.filterPeriod.text = titleStr
        self.period.text = titleStr
        
        let division = self.getDivision().components(separatedBy: ",")
        var div = ""
        if division.count > 0 {
            div = division.last?.components(separatedBy: "Division").first ?? ""
        }
        let townShip = "Township"
        if UserModel.shared.language != "en" {
            self.accountTypeVal.text = getBurmeseAgentType()
            self.address.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? ""), \(division.last ?? "")".replacingOccurrences(of: ", ,", with: ",")
        } else {
            self.accountTypeVal.text = accountType
            self.address.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? "") \(townShip), \(div), \(division.last ?? "")".replacingOccurrences(of: ", ,", with: ",")
            self.addressHeight.constant = 65.0
            self.layoutIfNeeded()
        }
//        self.address.text = "\(UserModel.shared.address2), \(UserModel.shared.street), \(division.first ?? "") \(townShip), \(div), \(division.last ?? ""), \(UserModel.shared.country)".replacingOccurrences(of: ", ,", with: ",")
    }
    
    
    
    private func getDivision() -> String {
        return getDivisionAndTownShipForReport(divisionCode: UserModel.shared.state, townshipCode: UserModel.shared.township)
    }
    
    private func agentServiceTypeString(type: AgentType) -> String {
        switch type {
        case .user:
            return "Personal"
        case .merchant:
            return "Merchant"
        case .agent:
            return "Agent"
        case .advancemerchant:
            return "Advance Merchant"
        case .dummymerchant:
            return "Safety Cashier"
        case .oneStopMart:
            return "One Stop Mart"
        }
    }
        
}
