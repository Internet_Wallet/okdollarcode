//
//  ReportReceiptListViewController.swift
//  OK
//  It shows the receipt list
//  Created by ANTONY on 26/01/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportReceiptListViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var tableViewReceipt: UITableView!
    var sortEnable = false
    // MARK: - Properties
    var filterSelected = false
    private let cellIdSort = "CellIdSort"
    private let cellIdentifier = "ReceiptTableViewCell"
    private var initialRecords = [ReportTransactionRecord]()
    private var transactionRecords = [ReportTransactionRecord]()
    private var filteredRecords = [ReportTransactionRecord]()
    private var headersKeys = [Date]()
    private var dictValues = [Date: [ReportTransactionRecord]]()
    private lazy var searchBar = UISearchBar()
    private var currentNavTitle = ""
    private var defaultNavTitle = ConstantsController.reportReceiptListViewController.screenTitle.localized
    private var isSearchNeed = true
    var multiButton : ActionButton?
    var sortAmountAscending = false
    var sortNameAscending = false
    var sortCashBackAscending = false
    var sortBonusAscending = false
    var sortDateAscending = true
    var sortedRecent = false
    private let refreshControl = UIRefreshControl()
    var modelReport: ReportModel!
    // MARK: - View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
//        tableViewReceipt.tableFooterView = UIView()
        tableViewReceipt.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshTableData), for: .valueChanged)
        currentNavTitle = defaultNavTitle
        fetchRecordsFromDB()
        buildingDataSet()
        setUpNavigation()
        searchBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40)
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        searchBar.tintColor = UIColor.white
        searchBar.placeholder = "Search".localized
        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
                searchTextField.keyboardType = .asciiCapable
            }
        }
        searchBar.tintColor = UIColor.blue
        
        if let cancelButton : UIButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            cancelButton.setTitle("Cancel".localized, for:.normal)
            switch appDelegate.currentLanguage {
            case "en":
                if let navFont = UIFont(name: appFont, size: 18) {
                    cancelButton.titleLabel?.font = navFont
                }
            default:
                if let navFont = UIFont(name: appFont, size: 14) {
                    cancelButton.titleLabel?.font = navFont
                }
            }
        }
        
        let view = self.searchBar.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                //subView.tintColor = UIColor.gray
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        loadFloatButtons()
        modelReport = ReportModel()
        modelReport.reportModelDelegate = self
        modelReport.getTransactionInfo()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Methods
    @objc func refreshTableData() {
        self.tableViewReceipt.backgroundView = nil
        transactionRecords = initialRecords
        buildingDataSet()
        currentNavTitle = defaultNavTitle
        setUpNavigation()
        sortEnable = false
        filterSelected = false
        tableViewReceipt.reloadData()
        multiButton?.show()
        guard let isHidden = multiButton?.floatButton.isHidden, !isHidden else {
            self.refreshControl.endRefreshing()
            return
        }
        modelReport.getTransactionInfo(showLoader: false)
    }
    
    private func fetchRecordsFromDB() {
        initialRecords = ReportCoreDataManager.sharedInstance.fetchTransactionRecords()
        transactionRecords = initialRecords
    }
    
    private func buildingDataSet() {
        dictValues = [:]
        headersKeys = []
        formDictionaryForSort()
    }
    
    private func formDictionaryForSort() {
        for checkRecord in transactionRecords {
            var rowRecords = [ReportTransactionRecord]()
            if let chckDate = checkRecord.transactionDate {
                let chckDateStr = chckDate.stringValue(dateFormatIs: "dd MMM yyyy")
                if let chckFormattedDate = chckDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                    if Array(dictValues.keys).contains(chckFormattedDate) {
                        continue
                    }
                }
            }
            for record in transactionRecords {
                if let transDate = record.transactionDate?.stringValue(dateFormatIs: "dd MMM yyyy"), let checkDate = checkRecord.transactionDate?.stringValue(dateFormatIs: "dd MMM yyyy") {
                
                    if checkDate == transDate
                   {
                        rowRecords.append(record)
                        let keyStr = checkDate//.stringValue(dateFormatIs: "dd MMM yyyy")
                        if let keyDate = keyStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                            if !headersKeys.contains(keyDate) {
                                headersKeys.append(keyDate)
                            }
                            dictValues[keyDate] = rowRecords
                        }
                    }
                }
            }
        }
        headersKeys = headersKeys.sorted(by: >)
    }
    
    private func sortByAmount() {
        filterSelected = false
        sortEnable = true
        headersKeys = []
        dictValues = [:]
        transactionRecords = initialRecords
        var orderType = ComparisonResult.orderedAscending
        if sortAmountAscending {
            orderType = .orderedDescending
            sortAmountAscending = false
        } else {
            orderType = .orderedAscending
            sortAmountAscending = true
        }
        transactionRecords = transactionRecords.sorted(by: {
            guard let amount1 = $0.amount , let amount2 = $1.amount else { return false }
            return amount1.compare(amount2) == orderType
        })
        formDictionaryForSort()
    }
    
    private func sortByName() {
        println_debug("Sort by name list")
        filterSelected = false
        sortEnable = true
        headersKeys = []
        dictValues = [:]
        transactionRecords = initialRecords
        if sortNameAscending {
            transactionRecords = transactionRecords.sorted(by: {
                guard var name1 = ($0.accTransType == "Dr") ? $0.receiverName?.lowercased() : $0.senderName?.lowercased() , var name2 = ($1.accTransType == "Dr") ? $1.receiverName?.lowercased() : $1.senderName?.lowercased() else { return false }
              
                let nameTemp = name1.replacingOccurrences(of: "-cashin", with: "").replacingOccurrences(of: "-cashout", with: "")
                if nameTemp.contains("@sender"){
                    let newValue = nameTemp.components(separatedBy: "@sender")
                    if newValue.indices.contains(1){
                        let nameNew = newValue[1].components(separatedBy: "@businessname")
                        if nameNew.indices.contains(0){
                            let arrComma = nameNew[0].components(separatedBy: ",")
                            if arrComma.count > 1{
                                name1 = arrComma[1]
                            }  else {
                                name1 = nameNew[0]
                            }
                        }
                    }
                }else{
                    let arrComma = nameTemp.components(separatedBy: ",")
                    if arrComma.count > 1{
                        name1 = arrComma[1]
                    }  else {
                        name1 = nameTemp
                    }
                }
                
                let nameTemp2 = name2.replacingOccurrences(of: "-cashin", with: "").replacingOccurrences(of: "-cashout", with: "")
                if nameTemp2.contains("@sender"){
                    let newValue = nameTemp2.components(separatedBy: "@sender")
                    if newValue.indices.contains(1){
                        let nameNew = newValue[1].components(separatedBy: "@businessname")
                        if nameNew.indices.contains(0){
                            let arrComma = nameNew[0].components(separatedBy: ",")
                            if arrComma.count > 1 {
                                name2 = arrComma[1]
                            } else {
                                name2 = nameNew[0]
                            }
                        }
                    }
                }else{
                    let arrComma = nameTemp2.components(separatedBy: ",")
                    if arrComma.count > 1 {
                        name2 = arrComma[1]
                    } else {
                        name2 = nameTemp2
                    }
                }
                println_debug(name1)
                println_debug(name2)
                return name1 > name2
            })
            sortNameAscending = false
        } else {
            transactionRecords = transactionRecords.sorted(by: {
                guard var name1 = ($0.accTransType == "Dr") ? $0.receiverName?.lowercased() : $0.senderName?.lowercased() , var name2 = ($1.accTransType == "Dr") ? $1.receiverName?.lowercased() : $1.senderName?.lowercased() else { return false }
               
                let nameTemp = name1.replacingOccurrences(of: "-cashin", with: "").replacingOccurrences(of: "-cashout", with: "")
                if nameTemp.contains("@sender"){
                    let newValue = nameTemp.components(separatedBy: "@sender")
                    if newValue.indices.contains(1){
                        let nameNew = newValue[1].components(separatedBy: "@businessname")
                        if nameNew.indices.contains(0){
                            let arrComma = nameNew[0].components(separatedBy: ",")
                            if arrComma.count > 1{
                                name1 = arrComma[1]
                            }  else {
                                name1 = nameNew[0]
                            }
                        }
                    }
                }else{
                    let arrComma = nameTemp.components(separatedBy: ",")
                    if arrComma.count > 1{
                        name1 = arrComma[1]
                    }  else {
                        name1 = nameTemp
                    }
                }
                
                let nameTemp2 = name2.replacingOccurrences(of: "-cashin", with: "").replacingOccurrences(of: "-cashout", with: "")
                if nameTemp2.contains("@sender"){
                    let newValue = nameTemp2.components(separatedBy: "@sender")
                    if newValue.indices.contains(1){
                        let nameNew = newValue[1].components(separatedBy: "@businessname")
                        if nameNew.indices.contains(0){
                            let arrComma = nameNew[0].components(separatedBy: ",")
                            if arrComma.count > 1 {
                                name2 = arrComma[1]
                            } else {
                                name2 = nameNew[0]
                            }
                        }
                    }
                }else{
                    let arrComma = nameTemp2.components(separatedBy: ",")
                    if arrComma.count > 1 {
                        name2 = arrComma[1]
                    } else {
                        name2 = nameTemp2
                    }
                }
                println_debug(name1)
                println_debug(name2)
                return name1 < name2
            })
            sortNameAscending = true
        }
        formDictionaryForSort()
    }
    
    private func sortByCashBack() {
        filterSelected = false
        sortEnable = true
        headersKeys = []
        dictValues = [:]
        //transactionRecords = initialRecords
        if sortCashBackAscending {
            transactionRecords = initialRecords.sorted(by: {
                guard let cashBack1 = $0.cashBack , let cashBack2 = $1.cashBack else { return false }
                return cashBack1.compare(cashBack2) == .orderedDescending
            })
            sortCashBackAscending = false
        } else {
            transactionRecords = initialRecords.sorted(by: {
                guard let cashBack1 = $0.cashBack , let cashBack2 = $1.cashBack else { return false }
                return cashBack1.compare(cashBack2) == .orderedAscending
            })
            sortCashBackAscending = true
        }
        //formDictionaryForSort()
        
        if self.transactionRecords.count == 0 {
            self.showErrorAlert(errMessage: "No Records Found!".localized)
        }
        
        self.formDictionaryForSort()
        self.tableViewReceipt.reloadData()
    }
    
    private func sortByBonus() {
        filterSelected = false
        sortEnable = true
        headersKeys = []
        dictValues = [:]
        //transactionRecords = initialRecords
        if sortBonusAscending {
            transactionRecords = initialRecords.sorted(by: {
                guard let bonus1 = $0.bonus , let bonus2 = $1.bonus else { return false }
                return bonus1.compare(bonus2) == .orderedDescending
            })
            sortBonusAscending = false
        } else {
            transactionRecords = initialRecords.sorted(by: {
                guard let bonus1 = $0.bonus , let bonus2 = $1.bonus else { return false }
                return bonus1.compare(bonus2) == .orderedAscending
            })
            sortBonusAscending = true
        }
        
        if self.transactionRecords.count == 0 {
            self.showErrorAlert(errMessage: "No Records Found!".localized)
        }
       
        self.formDictionaryForSort()
        self.tableViewReceipt.reloadData()
        //formDictionaryForSort()
    }
    
    private func sortByDate() {
//        sortEnable = false
//        tableViewReceipt.reloadData()
        self.displayDateView()
    }
    
    private func displayDateView() {
        let storyBoard = UIStoryboard(name: "PendingRequestSB", bundle: nil)
        if let dateVC = storyBoard.instantiateViewController(withIdentifier: "RequestMoneyDateSelectViewController_ID") as? RequestMoneyDateSelectViewController {
            dateVC.delegate = self
            dateVC.screenFrom = "ReportReceipt"
            self.navigationController?.pushViewController(dateVC, animated: true)
        }
    }
    
    private func sortByRecent() {
        transactionRecords = initialRecords
        filterSelected = false
//        if sortedRecent {
//            headersKeys = headersKeys.sorted(by: <)
//            sortedRecent = false
//        } else {
            headersKeys = headersKeys.sorted(by: >)
            sortedRecent = true
//        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let contentInset = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: keyboardSize.height, right: 0.0)
            self.tableViewReceipt.contentInset = contentInset
            self.tableViewReceipt.scrollIndicatorInsets = contentInset
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.tableViewReceipt.contentInset = UIEdgeInsets.zero
        self.tableViewReceipt.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    @objc private func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func showSearchOption() {
        searchBar.placeholder = "Search".localized
        searchBar.text = ""
        self.navigationItem.titleView = searchBar
        searchBar.becomeFirstResponder()
        multiButton?.hide()
    }
    
    private func applySearch(with searchText: String) {
        var tempRecords = initialRecords
        if filterSelected {
            tempRecords = filteredRecords
        }
        if searchText.count > 0 {
            tempRecords = tempRecords.filter {
                if let accTransType = $0.accTransType {
                    var senderName = $0.senderName ?? ""
                    if accTransType == "Dr" {
                        senderName = $0.receiverName ?? ""
                    }
                    if senderName.lowercased().range(of:searchText.lowercased()) != nil {
                        return true
                    }
                }
                if let mobileNumber = $0.destination {
                    if mobileNumber.range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                if let amount = $0.amount {
                    if amount.toString().range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                return false
            }
        }
        transactionRecords = tempRecords
        buildingDataSet()
        tableViewReceipt.reloadData()
    }
    
    func loadFloatButtons() {
        let sortByDate = ActionButtonItem(title: "Sort By Date".localized, image: #imageLiteral(resourceName: "receiptSortDate"))
        sortByDate.action = { item in
            self.sortByDate()
            self.currentNavTitle = "Sort By Date".localized
            self.isSearchNeed = true
            self.setUpNavigation()
            self.tableViewReceipt.reloadData()
            self.multiButton?.toggleMenu()
        }
        let cashBack = ActionButtonItem(title: "Cash Back".localized, image: #imageLiteral(resourceName: "receiptCashBack"))
        cashBack.action = { item in
            self.sortByCashBack()
            self.currentNavTitle = "Sort By Cash Back".localized
            self.isSearchNeed = true
            self.setUpNavigation()
            self.tableViewReceipt.reloadData()
            self.multiButton?.toggleMenu()
        }
        let bonus = ActionButtonItem(title: "Bonus".localized, image: #imageLiteral(resourceName: "receiptBonus"))
        bonus.action = { item in
            self.sortByBonus()
            self.currentNavTitle = "Sort By Bonus".localized
            self.isSearchNeed = true
            self.setUpNavigation()
            self.tableViewReceipt.reloadData()
            self.multiButton?.toggleMenu()
        }
        let recent = ActionButtonItem(title: "Recent".localized, image: #imageLiteral(resourceName: "receiptRecent"))
        recent.action = { item in
            self.sortByRecent()
            self.currentNavTitle = "Sort By Recent".localized
            self.isSearchNeed = true
            self.setUpNavigation()
            self.tableViewReceipt.reloadData({
                self.tableViewReceipt.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
            })
            self.multiButton?.toggleMenu()
        }
        let sortByName = ActionButtonItem(title: "Sort By Name".localized, image: #imageLiteral(resourceName: "receiptSortName"))
        sortByName.action = { item in
            self.sortByName()
            self.currentNavTitle = "Sort By Name".localized
            self.isSearchNeed = true
            self.setUpNavigation()
            self.tableViewReceipt.reloadData()
            self.multiButton?.toggleMenu()
        }
        let sortByAmount = ActionButtonItem(title: "Sort By Amount".localized, image: #imageLiteral(resourceName: "receiptSortAmount"))
        sortByAmount.action = { item in
            self.sortByAmount()
            self.currentNavTitle = "Sort By Amount".localized
            self.isSearchNeed = true
            self.setUpNavigation()
            self.tableViewReceipt.reloadData()
            self.multiButton?.toggleMenu()
        }
        multiButton = ActionButton(attachedToView: self.view, items: [sortByAmount,sortByName, recent, bonus, cashBack, sortByDate])
        multiButton?.notifyBlurDelegate = self
        multiButton?.action = {
            button in button.toggleMenu()
            if let isExpanded = self.multiButton?.active, isExpanded == true {
                self.isSearchNeed = false
                self.setUpNavigation()
            } else {
                self.isSearchNeed = true
                self.setUpNavigation()
            }
            
        }
        multiButton?.setImage(#imageLiteral(resourceName: "menu_white_payto"), forState: .normal)
        multiButton?.backgroundColor = kYellowColor
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        self.multiButton = nil
    }
}

// MARK: -UISearchBarDelegate
extension ReportReceiptListViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
//            uiButton.setTitle("Cancel".localized, for:.normal)
//        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if range.location == 0 && text == " " { return false }
        if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
            let updatedText = searchText.replacingCharacters(in: textRange, with: text)
            if updatedText.containsEmoji {
                return false
            }
            if searchBar.text?.last == " " && text == " " {
                return false
            }
            if updatedText.count > 50 {
                return false
            }
            
            applySearch(with: updatedText)
        }
        return true
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            applySearch(with: "")
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearchNeed = true
        setUpNavigation()
        if filterSelected {
            transactionRecords = filteredRecords
        } else {
            transactionRecords = initialRecords
        }
        buildingDataSet()
        tableViewReceipt.reloadData()
        multiButton?.show()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.applySearch(with: searchBar.text ?? "")
        searchBar.endEditing(true)
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ReportReceiptListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
       
//        if sortEnable {
//            if filteredRecords.count == 0{
//                let noDataLabel = UILabel(frame: tableView.bounds)
//                           noDataLabel.text = "No records found!".localized
//                           noDataLabel.textAlignment = .center
//                           tableView.backgroundView = noDataLabel
//            }else{
//                tableView.backgroundView = nil
//            }
//
//            return 1
//            //return headersKeys.count
//        }
        if headersKeys.count == 0 {
            let noDataLabel = UILabel(frame: tableView.bounds)
            noDataLabel.text = "No records found!".localized
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
        } else {
            tableView.backgroundView = nil
        }
        return headersKeys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if sortEnable {
//            return transactionRecords.count
//        }
        guard let dicts = dictValues[headersKeys[section]] else { return 0 }
        return dicts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let receiptCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        guard let transactionReceiptCell =  receiptCell as? ReceiptTableViewCell else {
            return receiptCell
        }
//        if sortEnable {
//            transactionReceiptCell.configureReceiptCell(record: transactionRecords[indexPath.row])
//        } else {
            if let dicts = dictValues[headersKeys[indexPath.section]] {
                transactionReceiptCell.configureReceiptCell(record: dicts[indexPath.row])
            }
       // }
        return transactionReceiptCell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        if sortEnable {
//            return ""
//        }
        return headersKeys[section].stringValue(dateFormatIs: "dd MMM yyyy")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let receiptCell = tableView.cellForRow(at: IndexPath.init(row: indexPath.row, section: indexPath.section)) as? ReceiptTableViewCell
        
        guard let receiptDetailListViewController = UIStoryboard(name: "Reports", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.receiptDetailListViewController.nameAndID) as? ReceiptDetailListViewController else { return }
//        if sortEnable {
//           receiptDetailListViewController.transactionDetail = transactionRecords[indexPath.row]
//        } else {
        if let dicts = dictValues[headersKeys[indexPath.section]] {
            receiptDetailListViewController.transactionDetail = dicts[indexPath.row]
            receiptDetailListViewController.strSenderName = receiptCell?.labelName.text ?? ""
        }
        //}
        self.navigationController?.pushViewController(receiptDetailListViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.tintColor = UIColor.init(hex: "0321AA")
        header.textLabel?.textColor = UIColor.white
        header.textLabel?.font = UIFont(name: appFont, size: 17)
        header.textLabel?.textAlignment = .center
    }
}

// MARK: - Additional Methods
extension ReportReceiptListViewController: NavigationSetUpProtocol {
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        if isSearchNeed {
            let searchButton = UIButton(type: .custom)
            searchButton.setImage(#imageLiteral(resourceName: "reportSearch").withRenderingMode(.alwaysOriginal), for: .normal)
            searchButton.addTarget(self, action: #selector(showSearchOption), for: .touchUpInside)
//            if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
//                searchButton.frame = CGRect(x: navTitleView.frame.size.width - 100, y: ypos, width: 30, height: 30)
//            }else{
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
           // }
            navTitleView.addSubview(searchButton)
        }
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "tabBarBack").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  currentNavTitle + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
}

//MARK: - NotifyBlurDelegate
extension ReportReceiptListViewController: NotifyBlurDelegate {
    func blurRemoveDelegate() {
        isSearchNeed = true
        setUpNavigation()
    }
}

//MARK: - ReportModelDelegate
extension ReportReceiptListViewController: ReportModelDelegate {
    func apiResult(message: String?, statusCode: String?) {
        DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 1.5) {
            self.fetchRecordsFromDB()
            DispatchQueue.main.async {
                self.buildingDataSet()
                self.tableViewReceipt.reloadData()
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    func handleCustomError(error: CustomError) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                progressViewObj.removeProgressView()
            }
        }
    }
}

extension ReportReceiptListViewController : dateSelectForSort {
    func okButtonAction(fromDate : Date, toDate : Date, reset: Bool) {
        if reset {
            DispatchQueue.main.async {
                self.filterSelected = false
                self.transactionRecords = self.initialRecords
                self.sortEnable = false
                self.tableViewReceipt.reloadData()
                self.currentNavTitle = self.defaultNavTitle
                self.setUpNavigation()
            }
        } else {
            guard let toDte = Calendar.current.date(bySettingHour: 23, minute: 59, second: 59, of: toDate) else { return }
            var calendar = Calendar(identifier: .gregorian)
            calendar.timeZone = TimeZone(secondsFromGMT: 0)!
            let startOfDate = calendar.startOfDay(for: fromDate)
            DispatchQueue.main.async {
                self.filterSelected = true
                self.transactionRecords = self.initialRecords.filter({ (record) -> Bool in
                    guard let date = record.transactionDate else { return false }
                    let range = startOfDate...toDte
                    return range.contains(date)
                })
                self.filteredRecords = self.transactionRecords
                self.sortEnable = true
                
                if self.transactionRecords.count == 0 {
                    self.showErrorAlert(errMessage: "No Records Found!".localized)
                }
                self.dictValues = [:]
                self.headersKeys = []
                self.formDictionaryForSort()
                self.tableViewReceipt.reloadData()
            }
        }
    }
}
