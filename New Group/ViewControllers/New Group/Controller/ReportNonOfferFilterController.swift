//
//  ReportNonOfferFIlterController.swift
//  OK
//
//  Created by iMac on 3/26/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol NonofferAdvanceFilterDelegate {
    func assignAdvanceFilter(filterType: String, name: String, no: String, amount: String)
}
protocol NonofferDummyFilterDelegate {
    
    func assignDummyFilter(filterType: String, selectedItems: String)
}

class ReportNonOfferFilterController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var tableViewFilter : UITableView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var constraintHeightStack: NSLayoutConstraint!
    @IBOutlet weak var btnReset : UIButton!{
        didSet {
            btnReset.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
            btnReset.setTitle("Reset".localized, for: .normal)
        }
    }
    @IBOutlet weak var btnShowResult : UIButton!{
        didSet {
            btnShowResult.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
            btnShowResult.setTitle("Show Result".localized, for: .normal)
        }
    }

    // MARK: - Properties
    private var cellIdentifier = "ReportOfferFilterCell"
    private var arrayFilter = [OfferFilter]()
    private class OfferFilter {
        var title: String
        var value: String
        
        init(filterTitle: String, filterValue: String) {
            self.title = filterTitle.localized
            self.value = filterValue
        }
    }
    var sourcePromoId = ""
    var sourcePromoCode = ""
    var promotionCode = ""
    var promotionID = ""
    var strFromToDate = ""
    var strFromDate = ""
    var strToDate = ""
    var locationName = ""
    var locationId = ""
    var transcationType = ""
    var selectedtranscationType = ""
    var advanceMerchantName = ""
    var advanceMerchantNo = ""
    var dummyMerchantNo = ""
    var dummyMerchantNoList = [String]()
    var offerFilterResult = [OfferReportList]()
    var offerAllFilterDelegate: OfferAllFilterDelegate?
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tableViewFilter.tableFooterView = UIView()
        promotionCode = sourcePromoCode
        promotionID = sourcePromoId
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpNavigation()

        //let isHavingDateRange = strFromToDate.trimmingCharacters(in: .whitespacesAndNewlines).count > 0
        //let isHavingTxType = transcationType.trimmingCharacters(in: .whitespacesAndNewlines).count > 0
        let isHavingLocName = locationName.trimmingCharacters(in: .whitespacesAndNewlines).count > 0
        let isHavingAdvMerchantNo = advanceMerchantNo.trimmingCharacters(in: .whitespacesAndNewlines).count > 0
        let isHavingDummyMerchantNo = dummyMerchantNo.trimmingCharacters(in: .whitespacesAndNewlines).count > 0
        if isHavingLocName && isHavingAdvMerchantNo && isHavingDummyMerchantNo {
            constraintHeightStack.constant = 50
            stackView.isHidden = false
        } else {
            constraintHeightStack.constant = 0
            stackView.isHidden = true
        }
        constraintHeightStack.constant = 50
        //stackView.isHidden = false
        self.view.layoutIfNeeded()
    }
    
    //MARK: - Methods
    private func  loadData() {
        arrayFilter = []
        
        print("transaction------\(transcationType)")
        
        if strFromToDate.count > 0 {
            arrayFilter.append(OfferFilter(filterTitle: "Date", filterValue: strFromToDate))
        } else {
            arrayFilter.append(OfferFilter(filterTitle: "Select Date", filterValue: strFromToDate))
        }
        if transcationType.count > 0 {
            arrayFilter.append(OfferFilter(filterTitle: "Transaction Type", filterValue: transcationType))
        } else {
            arrayFilter.append(OfferFilter(filterTitle: "Select Transaction Type", filterValue: transcationType))
        }
        if locationName.count > 0 {
            arrayFilter.append(OfferFilter(filterTitle: "Location", filterValue: locationName))
        } else {
            arrayFilter.append(OfferFilter(filterTitle: "Select Location", filterValue: locationName))
        }
        if advanceMerchantNo.count > 0 {
            arrayFilter.append(OfferFilter(filterTitle: "Advance Merchant Number", filterValue: advanceMerchantNo))
        } else {
            arrayFilter.append(OfferFilter(filterTitle: "Select Advance Merchant Number", filterValue: advanceMerchantNo))
        }
        if dummyMerchantNo.count > 0 {
            arrayFilter.append(OfferFilter(filterTitle: "Safety Cashier Number", filterValue: dummyMerchantNo))
        } else {
            arrayFilter.append(OfferFilter(filterTitle: "Select Safety Cashier Number", filterValue: dummyMerchantNo))
        }
    }

    private func navigatToNeededScreen(selectedIndex: Int) {
        if  strFromToDate.trimmingCharacters(in: .whitespacesAndNewlines).count < 1 {
            //navigateToDayTimeScreen()
        }else if transcationType.trimmingCharacters(in: .whitespacesAndNewlines).count < 1 {
            navigateToTransactionType()
        } else if locationName.trimmingCharacters(in: .whitespacesAndNewlines).count < 1 {
            navigateToLocationScreen(promotionIDParam: self.promotionID)
        } else if advanceMerchantNo.trimmingCharacters(in: .whitespacesAndNewlines).count < 1 {
            navigateToAdvanceMerchant(townshipIDParam: self.locationId, promotionIDParam: self.promotionID)
        } else if dummyMerchantNo.trimmingCharacters(in: .whitespacesAndNewlines).count < 1 {
            navigateToDummyMerchant(townshipIDParam: self.locationId, promotionIDParam: self.promotionID, advanceMerchantNo: advanceMerchantNo)
        } else {
            switch selectedIndex {
            case 1:
                navigateToTransactionType()
            case 2:
                navigateToLocationScreen(promotionIDParam: "")
            case 3:
                navigateToAdvanceMerchant(townshipIDParam: self.locationId, promotionIDParam: self.promotionID)
            case 4:
                navigateToDummyMerchant(townshipIDParam: self.locationId, promotionIDParam: self.promotionID, advanceMerchantNo: advanceMerchantNo)
            default:
                break
            }
        }
    }
    
    private func navigateToDayTimeScreen(){
        guard let reportDatePickerController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportDatePickerController.nameAndID) as? ReportDatePickerController else { return }
        reportDatePickerController.dateMode = .fromToDate
        reportDatePickerController.delegate = self
        reportDatePickerController.viewOrientation = .portrait
        reportDatePickerController.modalPresentationStyle = .overCurrentContext
        self.present(reportDatePickerController, animated: false, completion: nil)
    }
    
    private func navigateToTransactionType(){
        guard let reportDatePickerController = UIStoryboard(name: "ReportsB", bundle: Bundle.main).instantiateViewController(withIdentifier: "ReportNonOfferAllTypeController") as? ReportNonOfferAllTypeController else { return }
        reportDatePickerController.delegate = self
        self.navigationController?.pushViewController(reportDatePickerController, animated: true)
    }
    
    private func navigateToLocationScreen(promotionIDParam: String) {
        guard let reportFilterController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportPromotionLocationFilterController.nameAndID) as? ReportPromotionLocationFilterController else { return }
        reportFilterController.screenName = ReportPromotionLocationFilterController.ScreenType.Nonofferlocation
        reportFilterController.promoID = promotionIDParam
        reportFilterController.delegate = self
        self.navigationController?.pushViewController(reportFilterController, animated: true)
    }
    
    private func navigateToAdvanceMerchant(townshipIDParam: String, promotionIDParam: String) {
      
        guard let reportFilterController = UIStoryboard(name: "ReportsB", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportNonOfferAdvanceDummyFilterController.nameAndID) as? ReportNonOfferAdvanceDummyFilterController else { return }
       reportFilterController.screenTypeName = "AdvanceMerchant"
        reportFilterController.townshipID = townshipIDParam
        reportFilterController.advanceFilterDelegate = self as NonofferAdvanceFilterDelegate
        self.navigationController?.pushViewController(reportFilterController, animated: true)
    }
    
    private func navigateToDummyMerchant(townshipIDParam: String, promotionIDParam: String, advanceMerchantNo: String) {
        guard let reportFilterController = UIStoryboard(name: "ReportsB", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportNonOfferAdvanceDummyController.nameAndID) as? ReportNonOfferAdvanceDummyController else { return }
        reportFilterController.screenTypeName = "DummyMerchant"
      
         reportFilterController.townshipID = townshipIDParam
        // reportFilterController.promotionID = promotionIDParam
        reportFilterController.advanceMerchantNo = advanceMerchantNo

        reportFilterController.dummyFilterDelegate = self as NonofferDummyFilterDelegate
        self.navigationController?.pushViewController(reportFilterController, animated: true)
    }
    
    //MARK: - Button Action Methods
    @IBAction func showResult(sender: UIButton) {
       
        var dictTemp = Dictionary<String,Any>()
        dictTemp["FromDate"] = strFromDate
        dictTemp["ToDate"] = strToDate
        dictTemp["TransactionRequestedType"] = self.selectedtranscationType
        dictTemp["TownShipId"] = locationId
        dictTemp["AdvanceMerchantNumber"] = advanceMerchantNo
        dictTemp["DummyMerchantMobileNumber"] = dummyMerchantNo

        guard let transactionDetailViewController = UIStoryboard(name: "ReportsB", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.transactionNonOfferViewController.nameAndID) as? TransactionNonOfferReport else { return }
        transactionDetailViewController.dictFilterData = dictTemp
        self.navigationController?.pushViewController(transactionDetailViewController, animated: true)
    }
    
    @IBAction func resetFilterData(_ sender: UIButton) {
       
        self.locationId = ""
        self.locationName = ""
        self.transcationType = ""
        self.selectedtranscationType = ""
        self.advanceMerchantName = ""
        self.advanceMerchantNo = ""
        self.dummyMerchantNo = ""
        self.dummyMerchantNoList = []
        self.loadData()
        self.tableViewFilter.reloadData()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ReportNonOfferFilterController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let reportOfferFilterCell =  tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ReportOfferFilterCell else {
            return ReportOfferFilterCell()
        }
        if indexPath.row == 0  {
            reportOfferFilterCell.accessoryType = .none;
        } else {
            reportOfferFilterCell.accessoryType = .disclosureIndicator
        }
        let filter = arrayFilter[indexPath.row]
        reportOfferFilterCell.configureCell(titleName: filter.title, value: filter.value)
        
        return reportOfferFilterCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != 0  {
            navigatToNeededScreen(selectedIndex: indexPath.row)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight = 250
        tableView.rowHeight          = UITableView.automaticDimension
        return tableView.rowHeight
    }
}

//MARK: - ReportModelDelegate
extension ReportNonOfferFilterController: NonofferAdvanceFilterDelegate {
    func assignAdvanceFilter(filterType: String, name: String, no: String, amount: String) {
        
            self.advanceMerchantName = name
            self.advanceMerchantNo = no
            self.loadData()
            self.tableViewFilter.reloadData()
            self.navigateToDummyMerchant(townshipIDParam: self.locationId, promotionIDParam: self.promotionID, advanceMerchantNo: self.advanceMerchantNo)
        
    }
}

//MARK: - ReportModelDelegate
extension ReportNonOfferFilterController: NonofferDummyFilterDelegate {
   
    func assignDummyFilter(filterType: String, selectedItems: String) {
        
        dummyMerchantNo = selectedItems
        dummyMerchantNoList = []
        dummyMerchantNoList.append(selectedItems)
        self.loadData()
        self.tableViewFilter.reloadData()
    }
}

//MARK: - ReportModelDelegate
extension ReportNonOfferFilterController: PromotionLocationFilterDelegate {
    func assignPromoLocoFilter(filterType: ReportOfferFilterOptions, value: String) {
        switch filterType {
        case .promotion:
            let valueArray = value.components(separatedBy: "@@")
            if valueArray.count > 1 {
                self.promotionCode = valueArray[0]
                self.promotionID = valueArray[1]
                self.loadData()
                self.tableViewFilter.reloadData()
                self.navigateToLocationScreen(promotionIDParam: self.promotionID)
            }
        case .location:
            let valueArray = value.components(separatedBy: "@@")
            if valueArray.count > 1 {
                self.locationId = valueArray[0]
                self.locationName = valueArray[1]
                self.loadData()
                self.tableViewFilter.reloadData()
                //self.navigateToTransactionType()
                self.navigateToAdvanceMerchant(townshipIDParam: self.locationId, promotionIDParam: self.promotionID)
            }
        default:
            break
        }
    }
}

extension ReportNonOfferFilterController: ReportDatePickerDelegate {
    func processWithDate(fromDate: Date?, toDate: Date?, dateMode: DateMode) {
        //var selectedDate = ""
        switch dateMode {
        case .fromToDate:
            if let safeFDate = fromDate, let safeTDate = toDate {
                
                strFromDate = safeFDate.stringValue(dateFormatIs: "yyyy-MM-dd")
                strToDate = safeTDate.stringValue(dateFormatIs: "yyyy-MM-dd")
                
                if strFromDate == strToDate {
                    strFromToDate = safeFDate.stringValue(dateFormatIs: "E,dd-MMMM-yyyy")
                } else {
                    strFromToDate = safeFDate.stringValue(dateFormatIs: "E,dd-MMMM-yyyy") + " To " + safeTDate.stringValue(dateFormatIs: "E,dd-MMMM-yyyy")
                }
                
                self.loadData()
                self.tableViewFilter.reloadData()
                self.navigateToLocationScreen(promotionIDParam: self.promotionID)
            }
        default:
            break
        }
    }
}


// MARK: - Additional Methods
extension ReportNonOfferFilterController: NavigationSetUpProtocol {
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  ConstantsController.reportOfferFilterController.screenTitle.localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
}

extension ReportNonOfferFilterController: ReportNonOfferAllTypeDelegate {
    func processWithType(type : String) {
        println_debug(type)
        self.selectedtranscationType = type
        
        if type == "NONPROMOTIONTRAN" {
            transcationType = "All Non Offers".localized
        }
        else if type == "PROMOTIONTRAN" {
            transcationType = "All Offers".localized
        }
        else if type == "ALLTRAN" {
            transcationType = "All Transaction".localized
        }
        
        self.loadData()
        self.tableViewFilter.reloadData()
        
        navigateToLocationScreen(promotionIDParam: "")

    }
}
