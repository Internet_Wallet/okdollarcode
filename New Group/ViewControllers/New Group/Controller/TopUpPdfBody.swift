//
//  TopUpPdfBody.swift
//  OK
//
//  Created by iMac on 5/14/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class TopUpPdfBody: UIView {

    @IBOutlet weak var transactionID: UILabel!{
        didSet{
            self.transactionID.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var debitAmount: UILabel!{
        didSet{
            self.debitAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var billType: UILabel!{
        didSet{
            self.billType.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var bonus: UILabel!{
        didSet{
            self.bonus.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var cashBack: UILabel!{
        didSet{
            self.cashBack.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var balanceWallet: UILabel!{
        didSet{
            self.balanceWallet.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var transactionDate: UILabel!{
        didSet{
            self.transactionDate.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var transDescription: UILabel!{
        didSet{
            self.transDescription.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    
    func fillDetails(record: ReportTransactionRecord) {
        self.transactionID.text = record.transID ?? ""
        cashBack.text = "0" //For receiver side, since no cashback for receiver
        if let transAmount = record.amount {
            self.debitAmount.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: transAmount.toString())
        }
        
        if let countryCode = record.countryCode, let destination = record.destination {
            self.billType.text = "(\(countryCode))\(destination)"
        } else {
            self.billType.text = CodeSnippets.getMobileNumber(transRecord: record)
        }
        
        if let balanceAmount = record.walletBalance {
            self.balanceWallet.text = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: balanceAmount.toString())
        }
        
        if let bonusAmount = record.bonus {
            self.bonus.text =  bonusAmount.toStringNoDecimal() //  feeValue -- FEE(server key)
        }
        self.transactionDate.text = ""
        if let transDate = record.transactionDate {
            self.transactionDate.text = transDate.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy \nHH:mm:ss")
        }
        self.transDescription.text = record.desc
        
        if let cashBackAmount = record.cashBack {
            if cashBackAmount.compare(0.0) == .orderedDescending {
                cashBack.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: cashBackAmount.toString()) //    feeAmount -- fee(server key)
            }
        }
    }
}

