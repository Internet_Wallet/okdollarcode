//
//  ReportLoyaltyViewController.swift
//  OK
//
//  Created by E J ANTONY on 08/10/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreData

class ReportLoyaltyViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var tableViewLoyalty: UITableView!
    @IBOutlet weak var tableViewFilter: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var constraintScrollContentHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintFilterTableLeading: NSLayoutConstraint!
    @IBOutlet weak var constraintFilterTableHeight: NSLayoutConstraint!
    
    //For search functions
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var buttonCancel: UIButton!{
        didSet {
            buttonCancel.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonClear: UIButton!{
        didSet {
            buttonClear.titleLabel?.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    //For filter functions
    @IBOutlet weak var buttonMerchantNoFilter: ButtonWithImage!{
        didSet {
            buttonMerchantNoFilter.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonTransTypeFilter: ButtonWithImage!{
        didSet {
            buttonTransTypeFilter.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonPeriodFilter: ButtonWithImage!{
        didSet {
            buttonPeriodFilter.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    //For sorting functions
    @IBOutlet weak var buttonTransIDSort: ButtonWithImage!{
        didSet {
            buttonTransIDSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonMercNoSort: ButtonWithImage!{
        didSet {
            buttonMercNoSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonShopNameSort: ButtonWithImage!{
        didSet {
            buttonShopNameSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDestNoSort: ButtonWithImage!{
        didSet {
            buttonDestNoSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
//    @IBOutlet weak var buttonTransTypeSort: ButtonWithImage!
    @IBOutlet weak var buttonBonusPointSort: ButtonWithImage!{
        didSet {
            buttonBonusPointSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonBalanceSort: ButtonWithImage!{
        didSet {
            buttonBalanceSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDateSort: ButtonWithImage!{
        didSet {
            buttonDateSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var constraintScrollTop: NSLayoutConstraint!
    
    // MARK: - Properties
    private let reportLoyaltyCellID = "ReportLoyaltyCell"
    private var merchantNumbers = ["Merchant Number", "All"]
    private let transType = ["Transaction Type", "All", "LOYALTY TRANSFER", "LOYALTY DISTRIBUTION", "LOYALTY REDEEM"]
    private let periodFilters = ["Period", "All", "Date", "Month"]
    private var filterArray = [String]()
    private enum FilterType {
        case merchantNumbers, transType, period
    }
    private var loyaltyReportList = [LoyaltyData]()
    
    private var initialLoyaltyList = [ReportLoyalty]()
    private var loyaltyList = [ReportLoyalty]()
    private var lastSelectedFilter = FilterType.merchantNumbers
    var fromDate: Date?
    var toDate: Date?
    private let refreshControl = UIRefreshControl()
    var isSearchViewShow: Bool {
        get {
            return !viewSearch.isHidden
        }
        set(newValue) {
            if newValue {
                viewSearch.isHidden = false
                textFieldSearch.becomeFirstResponder()
                constraintScrollTop.constant = 0
                self.view.layoutIfNeeded()
            } else {
                textFieldSearch.text = ""
                viewSearch.isHidden = true
                constraintScrollTop.constant = -40
                self.view.layoutIfNeeded()
                self.view.endEditing(true)
            }
        }
    }
    var loyaltyReportType: Int = 0
    var modelReport: ReportModel!
    var typeLoyalty = ""
    // MARK: - View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewLoyalty.tag = 0
        tableViewFilter.tag = 1
        tableViewLoyalty.tableFooterView = UIView()
        tableViewFilter.tableFooterView = UIView()
        tableViewLoyalty.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshTableData), for: .valueChanged)
        buttonCancel.layer.cornerRadius = 15
        buttonClear.layer.cornerRadius = 15
        buttonCancel.addTarget(self, action: #selector(hideSearchView), for: .touchUpInside)
        buttonClear.addTarget(self, action: #selector(clearSerchTextField), for: .touchUpInside)
        buttonCancel.setTitle("Cancel".localized, for: .normal)
        buttonClear.setTitle("Clear".localized, for: .normal)
        textFieldSearch.placeholder = "Search By Transaction ID or Amount or Mobile Number or Remarks".localized
        let cellFilterNibName = UINib(nibName: ConstantsXibCell.filterTableViewCell, bundle: nil)
        tableViewFilter.register(cellFilterNibName, forCellReuseIdentifier: ConstantsXibCell.filterTableViewCell)
        AppDelegate.AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)
        modelReport = ReportModel()
        modelReport.reportModelDelegate = self
        fetchLoyaltyReports()
        switch loyaltyReportType {
            case 0:
                typeLoyalty = "Earning Point Transfer"
                modelReport.getRecordsfForEarnPointTrf(index: 0)
            case 1:
                typeLoyalty = "Point Distribution"
                modelReport.getRecordsfForEarnPointTrf(index: 1)
            case 2:
                typeLoyalty = "Redeem Points"
                modelReport.getRecordsfForEarnPointTrf(index: 2)
            default:
                typeLoyalty = "Earning Point Transfer"
                modelReport.getRecordsfForEarnPointTrf(index: 0)
        }
        setUpNavigation()
        setLocalizedTitles()
        //fetchLoyaltyReports()
        resetFilterButtons()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        let navBarHeight = navigationController?.navigationBar.frame.size.height ?? 44
        let heightToReduce = navBarHeight == 32 ? 56 : 44
        constraintScrollContentHeight.constant = scrollView.frame.size.height - CGFloat(heightToReduce) + 44
        self.view.layoutIfNeeded()
    }
    
    // MARK: - Methods
    private func resetFilterButtons() {
        buttonMerchantNoFilter.setTitle(merchantNumbers[0].localized, for: .normal)
        buttonTransTypeFilter.setTitle(transType[0].localized, for: .normal)
        buttonPeriodFilter.setTitle(periodFilters[0].localized, for: .normal)
    }
    
    private func setLocalizedTitles() {
        buttonTransIDSort.setTitle("Transaction ID".localized, for: .normal)
        buttonMercNoSort.setTitle("Buyer Number".localized, for: .normal)
        buttonShopNameSort.setTitle("Shop Name".localized, for: .normal)
        buttonDestNoSort.setTitle(typeLoyalty.localized, for: .normal)
//        buttonTransTypeSort.setTitle("Transaction Type".localized, for: .normal)
        buttonBonusPointSort.setTitle("Valid Till".localized, for: .normal)
        buttonBalanceSort.setTitle("Balance Point(s)".localized, for: .normal)
        buttonDateSort.setTitle("Date & Time".localized, for: .normal)
    }
    
    private func noRecordAlertAction() {
        alertViewObj.wrapAlert(title: "Alert".localized, body: "No Records".localized, img: UIImage(named : "AppIcon"))
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
        })
        alertViewObj.showAlert(controller: self)
    }
    
    private func showDateOptions(selectedOption: String) {
        switch selectedOption {
        case "Period".localized, "All".localized:
            self.fromDate = nil
            self.toDate = nil
            buttonPeriodFilter.setTitle(selectedOption, for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        case "Date".localized:
            presentDatePicker(mode: DateMode.fromToDate)
        case "Month".localized:
            presentDatePicker(mode: DateMode.month)
        default:
            break
        }
    }
    
    private func presentDatePicker(mode: DateMode) {
        guard let reportDatePickerController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportDatePickerController.nameAndID) as? ReportDatePickerController else { return }
        reportDatePickerController.dateMode = mode
        reportDatePickerController.delegate = self
        reportDatePickerController.modalPresentationStyle = .overCurrentContext
        self.present(reportDatePickerController, animated: false, completion: nil)
    }
    
    private func reloadTableWithScrollToTop() {
        self.tableViewLoyalty.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            if self.loyaltyReportList.count > 0 {
                println_debug(self.loyaltyReportList.count)
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableViewLoyalty.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
    }
    
    private func resetSortButtons(sender: UIButton) {
        if sender != buttonTransIDSort {
            buttonTransIDSort.tag = 0
            buttonTransIDSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonMercNoSort {
            buttonMercNoSort.tag = 0
            buttonMercNoSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonShopNameSort {
            buttonShopNameSort.tag = 0
            buttonShopNameSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonDestNoSort {
            buttonDestNoSort.tag = 0
            buttonDestNoSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
//        if sender != buttonTransTypeSort {
//            buttonTransTypeSort.tag = 0
//            buttonTransTypeSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
//        }
        if sender != buttonBonusPointSort {
            buttonBonusPointSort.tag = 0
            buttonBonusPointSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonBalanceSort {
            buttonBalanceSort.tag = 0
            buttonBalanceSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonDateSort {
            buttonDateSort.tag = 0
            buttonDateSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
    }
    
    private func applyFilter(searchText: String = "") {
        var filteredArray = modelReport.loyaltyRecords
        filteredArray = filterProcess(forMerchNo: filteredArray)
//        filteredArray = filterProcess(forTransType: filteredArray)
        filteredArray = filterProcess(forPeriod: filteredArray)
        if isSearchViewShow && searchText.count > 0 {
            filteredArray = filteredArray.filter {
                if let tID = $0.transId {
                    if tID.lowercased().range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                if let mNo = $0.merchantCode {
                    if mNo.lowercased().range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                if let dNo = $0.merchantName {
                    if dNo.lowercased().range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                if var bPoint = $0.closingPoint {
                    bPoint = bPoint.replacingOccurrences(of: ",", with: "")
                    if bPoint.range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                
                return false
            }
        }
        loyaltyReportList = filteredArray
        reloadTableWithScrollToTop()
        resetSortButtons(sender: UIButton())
    }
    
    private func filterProcess(forMerchNo arrayToFilter: [LoyaltyData]) -> [LoyaltyData] {
        if let transFilter = buttonMerchantNoFilter.currentTitle {
            switch transFilter {
            case "Merchant Number".localized, "All".localized:
                return arrayToFilter
            default:
                var number = ""
                var name = ""
                var result = transFilter.components(separatedBy: " ")
                if result.count > 0 {
                    number = result[0]
                    result.remove(at: 0)
                    if result.count > 0 {
                        name = result.joined(separator: " ")
                    }
                }
                return arrayToFilter.filter {
                    guard let mNo = $0.merchantCode else { return false}
                    let mobileNum = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: mNo)
                    let merchantNo = "(\(mobileNum.counrtycode))\(mobileNum.mobileNo)"
                    return (merchantNo == number)
                }
            }
        }
        return []
    }
    
//    private func filterProcess(forTransType arrayToFilter: [LoyaltyData]) -> [Loyal] {
//        if let transFilter = buttonTransTypeFilter.currentTitle {
//            switch transFilter {
//            case "Transaction Type".localized, "All".localized:
//                return arrayToFilter
//            case "LOYALTY TRANSFER".localized:
//                return arrayToFilter.filter {
//                    return $0.type == "LOYALTY TRANSFER"
//                }
//            case "LOYALTY DISTRIBUTION".localized:
//                return arrayToFilter.filter {
//                    return $0.type == "LOYALTY DISTRIBUTION"
//                }
//            case "LOYALTY REDEEM".localized:
//                return arrayToFilter.filter {
//                    return $0.type == "LOYALTY REDEEM"
//                }
//            default:
//                return []
//            }
//        }
//        return []
//    }
    
    private func filterProcess(forPeriod arrayToFilter: [LoyaltyData]) -> [LoyaltyData] {
        if let periodFilter = buttonPeriodFilter.currentTitle {
            switch periodFilter {
            case "Period".localized, "All".localized:
                return arrayToFilter
            default:
                break
            }
            if let exactDateStart = self.fromDate {
                return arrayToFilter.filter {
                    guard let createdDate = $0.transDate?.dateValue(dateFormatIs: "yyyy-MM-dd HH:mm:ss") else { return false }
                    guard let exactDateEnd = self.toDate else { return false }
                    guard let dateEnd = Calendar.current.date(byAdding: Calendar.Component.day, value: 1, to: exactDateEnd) else { return false}
                    return (exactDateStart.compare(createdDate) == .orderedAscending && createdDate.compare(dateEnd) == .orderedAscending)
                }
            } else {
                return arrayToFilter.filter {
                    guard let createdDate = $0.transDate?.dateValue(dateFormatIs: "yyyy-MM-dd HH:mm:ss") else { return false }
                    let crDateStr = createdDate.stringValue(dateFormatIs: "yyyy-MM")
                    guard let createdMonth = crDateStr.dateValue(dateFormatIs: "yyyy-MM") else { return false }
                    guard let monthToFilter = self.toDate else { return false }
                    return createdMonth.compare(monthToFilter) == .orderedSame
                }
            }
        }
        return []
    }
    
    private func updateFilterTableHeight() {
        constraintFilterTableHeight.constant = filterArray.count > 4 ? CGFloat(5*44) : CGFloat(filterArray.count * 44)
        self.view.layoutIfNeeded()
    }
    
    private func shareRecordsByPdf() {
        let pdfView = UIView()
        if let pdfHeaderView = Bundle.main.loadNibNamed("ReportLoyaltyPdfHeader", owner: self, options: nil)?.first as? ReportLoyaltyPdfHeader {

            pdfHeaderView.fillDetails(totalCount: loyaltyList.count, filterPeriod: buttonPeriodFilter.currentTitle ?? "", type: typeLoyalty)

            let newHeight = pdfHeaderView.viewForTitles.frame.maxY
            pdfHeaderView.frame = CGRect(x: 0, y: 0, width: pdfHeaderView.frame.size.width, height: newHeight)
            pdfView.addSubview(pdfHeaderView)
            pdfView.frame = pdfHeaderView.bounds
            let xPos: CGFloat = pdfHeaderView.viewForTitles.frame.minX
            var yPos: CGFloat = pdfView.frame.maxY

            for record in loyaltyReportList {
                if let pdfBodyView = Bundle.main.loadNibNamed("ReportLoyaltyPdfBody", owner: self, options: nil)?.first as? ReportLoyaltyPdfBody {
                    pdfBodyView.configureData(record: record)
                    let recordHeight = pdfBodyView.frame.size.height
                    let yPosInPage = Int(yPos) % 842
                    if (yPosInPage + Int(recordHeight)) > 842 {
                        yPos = yPos + CGFloat((842 - yPosInPage) + 30 + 30)
                    }
                    pdfBodyView.frame = CGRect(x: xPos, y: yPos, width: pdfBodyView.frame.size.width, height: recordHeight)
                    yPos = yPos + recordHeight + 2
                    pdfView.addSubview(pdfBodyView)
                }
            }
            pdfView.frame = CGRect(x: 0, y: 0, width: pdfView.frame.size.width, height: yPos + 30)
            guard let pdfUrl = CodeSnippets.generatePDF(viewForPDF: pdfView, pdfFile: "OK$ \(typeLoyalty) Report") else {
                println_debug("Error - pdf not generated")
                return
            }
            
            let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
            if self.presentedViewController != nil {
                self.dismiss(animated: false, completion: {
                    [unowned self] in
                    DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
                })
            }else{
                DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
            }
//            let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
//            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    private func shareRecordsByExcel() {
        var excelTxt = "Transaction ID,Buyer Number,Shop Name,\(typeLoyalty),Valid Till, Balance Bonus Points,Date & Time,Comments\n"
        var redeemPoints = 0.0
        for record in loyaltyReportList {
            let transID = record.transId ?? ""
            let mobileNum = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: record.merchantCode.safelyWrappingString())
            let merchantNo = "(\(mobileNum.counrtycode))\(mobileNum.mobileNo)"

            let shopName = (record.merchantName ?? "").replacingOccurrences(of: ",", with: "")
            var points = ""
            if var pts = record.transferPoint {
                pts = pts.replacingOccurrences(of: ".00", with: "")
                points = pts
                redeemPoints = redeemPoints + (pts as NSString).doubleValue
            }
            var validTill = ""
            if let date = record.validateDate {
                let dateStr = date.dateValue(dateFormatIs: "yyyy-MM-dd") ?? Date()
                validTill = dateStr.stringValue(dateFormatIs: "dd-MMM-yyyy")
            }
            var balanceBonusPoint = (record.closingPoint ?? "").replacingOccurrences(of: ".00", with: "")
            balanceBonusPoint = balanceBonusPoint.replacingOccurrences(of: ",", with: "")
            var dateTime = ""
            if let date = record.transDate {
                let dateStr = date.dateValue(dateFormatIs: "yyyy-MM-dd HH:mm:ss") ?? Date()
                dateTime = dateStr.stringValue(dateFormatIs: "EEE dd-MMM-yyyy HH:mm:ss")
            }
            let comments = "Comments: \(record.comments ?? "")".replacingOccurrences(of: ",", with: "")
            let recText = "\(transID),\(merchantNo),\(shopName),\(points),\(validTill),\(balanceBonusPoint),\(dateTime),\(comments)\n"
            excelTxt.append(recText)
        }
        excelTxt.append("Total,,,\(redeemPoints)".replacingOccurrences(of: ".00", with: ""))
        guard let excelUrl = CodeSnippets.generateExcel(excelDataWithFormat: excelTxt, excelFileName: "OK$ \(typeLoyalty) Report") else {
            println_debug("Error - excel not generated")
            return
        }
        let activityViewController = UIActivityViewController(activityItems: [excelUrl], applicationActivities: nil)
        if self.presentedViewController != nil {
            self.dismiss(animated: false, completion: {
                [unowned self] in
                DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
            })
        }else{
            DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
        }
//        let activityViewController = UIActivityViewController(activityItems: [excelUrl], applicationActivities: nil)
//        self.present(activityViewController, animated: true, completion: nil)
    }
    
    //MARK: - Target Methods
    @objc func refreshTableData() {
        self.hideSearchView()
        switch loyaltyReportType {
            case 0:
                typeLoyalty = "Earning Point Transfer"
                modelReport.getRecordsfForEarnPointTrf(index: 0)
            case 1:
                typeLoyalty = "Point Distribution"
                modelReport.getRecordsfForEarnPointTrf(index: 1)
            case 2:
                typeLoyalty = "Redeem Points"
                modelReport.getRecordsfForEarnPointTrf(index: 2)
            default:
                typeLoyalty = "Earning Point Transfer"
                modelReport.getRecordsfForEarnPointTrf(index: 0)
        }
//        fetchLoyaltyReports()
    }
    
    @objc private func hideSearchView() {
        self.view.endEditing(true)
        textFieldSearch.text = ""
        viewSearch.isHidden = true
        constraintScrollTop.constant = -40
        self.view.layoutIfNeeded()
        self.view.endEditing(true)
        applyFilter()
    }
    
    @objc private func clearSerchTextField() {
        textFieldSearch.text = ""
        applyFilter()
    }
    
    @objc private func showSearchView() {
        tableViewFilter.isHidden = true
        if !isSearchViewShow {
            if loyaltyReportList.count > 0 {
                isSearchViewShow = true
                textFieldSearch.becomeFirstResponder()
            } else {
                noRecordAlertAction()
            }
        } else {
            hideSearchView()
            //textFieldSearch.becomeFirstResponder()
        }
    }
    
    @objc private func showShareOption() {
        tableViewFilter.isHidden = true
        if loyaltyReportList.count < 1 {
            noRecordAlertAction()
        } else {
            guard let reportSharingViewController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportSharingViewController.nameAndID) as? ReportSharingViewController else { return }
            reportSharingViewController.delegate = self
            reportSharingViewController.pdfImage = UIImage(named: "topUpPdf")
            reportSharingViewController.excelImage = UIImage(named: "topUpExcel")
            reportSharingViewController.modalPresentationStyle = .overCurrentContext
            self.present(reportSharingViewController, animated: false, completion: nil)
        }
    }
    
    func fetchLoyaltyReports() {
        initialLoyaltyList = LoyaltyCoreDataManager.sharedInstance.fetchLoyaltyRecords()
        loyaltyList = initialLoyaltyList
        println_debug(loyaltyList)
        var mercList = ["Merchant Number", "All"]
        for rec in initialLoyaltyList {
            if let merNo = rec.merchantNo, let shopName = rec.shopName {
                let item = merNo + " " + shopName
                if !mercList.contains(item) {
                    mercList.append(item)
                }
            }
        }
        merchantNumbers = mercList
        tableViewLoyalty.reloadData()
    }
    
    func fetchLoyaltyReportsFromApi() {
        loyaltyReportList = modelReport.loyaltyRecords
        var mercList = ["Merchant Number", "All"]
        for rec in loyaltyReportList {
            if let merNo = rec.merchantCode {
                let mobileNum = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: merNo)
                let merchantNo = "(\(mobileNum.counrtycode))\(mobileNum.mobileNo)"
                if !mercList.contains(merchantNo) {
                    mercList.append(merchantNo)
                }
            }
        }
        merchantNumbers = mercList
        tableViewLoyalty.reloadData()
    }
    
    // MARK: - Button Actions
    @IBAction func filterMerchantNumbers(_ sender: UIButton) {
        self.view.endEditing(true)
        func showFilterAtCashInOut() {
            filterArray = merchantNumbers
            tableViewFilter.reloadData()
            constraintFilterTableLeading.constant = 100
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
            tableViewFilter.isHidden = false
        }
        if tableViewFilter.isHidden {
            showFilterAtCashInOut()
        } else {
            switch lastSelectedFilter {
            case .merchantNumbers:
                tableViewFilter.isHidden = true
            default:
                showFilterAtCashInOut()
            }
        }
        lastSelectedFilter = .merchantNumbers
    }
    
    @IBAction func filterByTransType(_ sender: UIButton) {
        self.view.endEditing(true)
        func showFilterAtTransType() {
            filterArray = transType
            tableViewFilter.reloadData()
            constraintFilterTableLeading.constant = 450
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
            tableViewFilter.isHidden = false
        }
        if tableViewFilter.isHidden {
            showFilterAtTransType()
        } else {
            switch lastSelectedFilter {
            case .transType:
                tableViewFilter.isHidden = true
            default:
                showFilterAtTransType()
            }
        }
        lastSelectedFilter = .transType
    }
    
    @IBAction func filterByPeriod(_ sender: UIButton) {
        self.view.endEditing(true)
        func showFilterAtPeriod() {
            filterArray = periodFilters
            tableViewFilter.reloadData()
            constraintFilterTableLeading.constant = 450
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
            tableViewFilter.isHidden = false
        }
        if tableViewFilter.isHidden {
            showFilterAtPeriod()
        } else {
            switch lastSelectedFilter {
            case .period:
                tableViewFilter.isHidden = true
            default:
                showFilterAtPeriod()
            }
        }
        lastSelectedFilter = .period
    }
    
    @IBAction func sortByTransID(_ sender: UIButton) {
        func sortTransactionID(orderType: ComparisonResult) -> [LoyaltyData] {
            return loyaltyReportList.sorted {
                guard let transID1 = $0.transId, let transID2 = $1.transId else { return false }
                return transID1.localizedStandardCompare(transID2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            loyaltyReportList = sortTransactionID(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            loyaltyReportList = sortTransactionID(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            break
        }
        resetSortButtons(sender: sender)
        tableViewLoyalty.reloadData()
    }
    
    @IBAction func sortByMercNumber(_ sender: UIButton) {
        func sortMercNumber(orderType: ComparisonResult) -> [LoyaltyData] {
            return loyaltyReportList.sorted {
                guard let phNumber1 = $0.merchantCode, let phNumber2 = $1.merchantCode else { return false }
                return phNumber1.compare(phNumber2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            loyaltyReportList = sortMercNumber(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            loyaltyReportList = sortMercNumber(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewLoyalty.reloadData()
    }
    
    @IBAction func sortByShopName(_ sender: UIButton) {
        func sortShopName(orderType: ComparisonResult) -> [LoyaltyData] {
            return loyaltyReportList.sorted {
                guard let phNumber1 = $0.merchantName, let phNumber2 = $1.merchantName else { return false }
                return phNumber1.compare(phNumber2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            loyaltyReportList = sortShopName(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            loyaltyReportList = sortShopName(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewLoyalty.reloadData()
    }
    
    @IBAction func sortByDestNumber(_ sender: UIButton) {
        func sortMobileNumber(orderType: ComparisonResult) -> [LoyaltyData] {
            return loyaltyReportList.sorted {
                guard let phNumber1 = $0.transferPoint, let phNumber2 = $1.transferPoint else { return false }
                return phNumber1.compare(phNumber2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            loyaltyReportList = sortMobileNumber(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            loyaltyReportList = sortMobileNumber(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewLoyalty.reloadData()
    }
    
    
    @IBAction func sortByBonus(_ sender: UIButton) {
        func sortDates(orderType: ComparisonResult) -> [LoyaltyData]{
            return loyaltyReportList.sorted {
                guard let date1 = $0.validateDate else { return false }
                guard let date2 = $1.validateDate else { return false }
                return date1.compare(date2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            loyaltyReportList = sortDates(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            loyaltyReportList = sortDates(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewLoyalty.reloadData()
    }
    
    @IBAction func sortByBalanceBonus(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            loyaltyReportList = loyaltyReportList.sorted {
                guard var amountStr1 = $0.closingPoint, var amountStr2 = $1.closingPoint else { return false }
                amountStr1 = amountStr1.replacingOccurrences(of: ",", with: "")
                amountStr2 = amountStr2.replacingOccurrences(of: ",", with: "")
                guard let amount1 = Double(amountStr1), let amount2 = Double(amountStr2) else { return false }
                return amount1 < amount2
            }
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            loyaltyReportList = loyaltyReportList.sorted {
                guard var amountStr1 = $0.closingPoint, var amountStr2 = $1.closingPoint else { return false }
                amountStr1 = amountStr1.replacingOccurrences(of: ",", with: "")
                amountStr2 = amountStr2.replacingOccurrences(of: ",", with: "")
                guard let amount1 = Double(amountStr1), let amount2 = Double(amountStr2) else { return false }
                return amount1 > amount2
            }
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewLoyalty.reloadData()
    }
    
    @IBAction func sortByDate(_ sender: UIButton) {
        func sortDates(orderType: ComparisonResult) -> [LoyaltyData]{
            return loyaltyReportList.sorted {
                guard let date1 = $0.transDate?.dateValue(dateFormatIs: "yyyy-MM-dd HH:mm:ss") else { return false }
                guard let date2 = $1.transDate?.dateValue(dateFormatIs: "yyyy-MM-dd HH:mm:ss") else { return false }
                return date1.compare(date2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            loyaltyReportList = sortDates(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            loyaltyReportList = sortDates(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter()
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        tableViewLoyalty.reloadData()
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ReportLoyaltyViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        switch tableView.tag {
        case 0:
            if loyaltyReportList.count > 0 {
                tableView.backgroundView = nil
            } else {
                let noDataLabel = UILabel(frame: tableView.bounds)
                noDataLabel.text = "No Records".localized
                noDataLabel.font = UIFont(name: appFont, size: 18)
                noDataLabel.textAlignment = .center
                tableView.backgroundView = noDataLabel
                self.refreshControl.endRefreshing()
            }
            return 1
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView.tag {
        case 0:
            return loyaltyReportList.count
        case 1:
            return filterArray.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView.tag {
        case 0:
            let reportLoyaltyCell = tableView.dequeueReusableCell(withIdentifier: reportLoyaltyCellID, for: indexPath) as? ReportLoyaltyCell
            let record = loyaltyReportList[indexPath.row]
            reportLoyaltyCell?.configureCell(record: record)
            return reportLoyaltyCell!
        case 1:
            let filterCell = tableView.dequeueReusableCell(withIdentifier: ConstantsXibCell.filterTableViewCell, for: indexPath) as? FilterTableViewCell
            filterCell?.configureTopUpFilter(filterName: filterArray[indexPath.row])
            return filterCell!
        default:
            let reportLoyaltyCell = tableView.dequeueReusableCell(withIdentifier: reportLoyaltyCellID, for: indexPath) as? ReportLoyaltyCell
            return reportLoyaltyCell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView.tag {
        case 0:
            break
        case 1:
            switch lastSelectedFilter {
            case .merchantNumbers:
                buttonMerchantNoFilter.setTitle(filterArray[indexPath.row].localized, for: .normal)
                tableViewFilter.isHidden = true
                applyFilter(searchText: textFieldSearch.text ?? "")
            case .transType:
                buttonTransTypeFilter.setTitle(filterArray[indexPath.row], for: .normal)
                tableViewFilter.isHidden = true
                applyFilter(searchText: textFieldSearch.text ?? "")
            case .period:
                tableViewFilter.isHidden = true
                showDateOptions(selectedOption: filterArray[indexPath.row].localized)
            }
            break
        default:
            break
        }
    }
}

// MARK: - Additional Methods
extension ReportLoyaltyViewController: NavigationSetUpProtocol {
    /// It set up the navigation bar
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = .red
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: titleViewWidth, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        
        let searchButton = UIButton(type: .custom)
        searchButton.setImage(#imageLiteral(resourceName: "reportSearch").withRenderingMode(.alwaysOriginal), for: .normal)
        searchButton.addTarget(self, action: #selector(showSearchView), for: .touchUpInside)
        
        let shareButton = UIButton(type: .custom)
        shareButton.setImage(#imageLiteral(resourceName: "reportShare").withRenderingMode(.alwaysOriginal), for: .normal)
        shareButton.addTarget(self, action: #selector(showShareOption), for: .touchUpInside)
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        let ypos = (titleViewSize.height - 30 ) / 2
        if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 140, y: ypos, width: 30, height: 30)
            shareButton.frame = CGRect(x: navTitleView.frame.size.width - 100, y: ypos, width: 30, height: 30)
            backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        }
        else{
        searchButton.frame = CGRect(x: navTitleView.frame.size.width - 70, y: ypos, width: 30, height: 30)
        shareButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        }
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 160, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text = typeLoyalty.localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 23) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        navTitleView.addSubview(searchButton)
        navTitleView.addSubview(shareButton)
        self.navigationItem.titleView = navTitleView
    }
    
    /// It shows the home screen
    @objc func backAction() {
//        AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
//        self.navigationController?.popViewController(animated: true)
        if isSearchViewShow {
            hideSearchView()
        } else {
            AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
            self.navigationController?.popViewController(animated: true)
        }
    }
}

// MARK: - UITextFieldDelegate
extension ReportLoyaltyViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tableViewFilter.isHidden = true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        tableViewLoyalty.contentOffset = CGPoint(x: 0, y: 0)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            if updatedText.count > 50 || (updatedText == "" && text == "") {
                return false
            }
            if string != "\n" {
                applyFilter(searchText: updatedText)
            }
        }
        return true
    }
}

// MARK: - ReportDatePickerDelegate
extension ReportLoyaltyViewController: ReportDatePickerDelegate {
    func processWithDate(fromDate: Date?, toDate: Date?, dateMode: DateMode) {
        self.fromDate = fromDate
        self.toDate = toDate
        var selectedDate = ""
        switch dateMode {
        case .fromToDate:
            if let safeFDate = fromDate, let safeTDate = toDate {
                let fDateInString = safeFDate.stringValue(dateFormatIs: "dd MMM yyyy")
                let tDateInString = safeTDate.stringValue(dateFormatIs: "dd MMM yyyy")
                selectedDate = fDateInString + " to\n" + tDateInString
            }
        case .month:
            if let safeDate = toDate {
                let monthInString = safeDate.stringValue(dateFormatIs: "MMM yyyy")
                selectedDate = monthInString
            }
        default:
            break
        }
        buttonPeriodFilter.setTitle(selectedDate, for: .normal)
        applyFilter(searchText: textFieldSearch.text ?? "")
    }
}

//MARK: - ReportSharingOptionDelegte
extension ReportLoyaltyViewController: ReportSharingOptionDelegte {
    func share(by: ReportSharingViewController.ReportShareOption) {
        switch by {
        case .pdf:
            shareRecordsByPdf()
        case .excel:
            shareRecordsByExcel()
        }
    }
}



extension ReportLoyaltyViewController: ReportModelDelegate {
    func apiResult(message: String?, statusCode: String?) {
        DispatchQueue.main.async {
            self.fetchLoyaltyReportsFromApi()
            self.loyaltyReportList = self.modelReport.loyaltyRecords
            self.tableViewLoyalty.reloadData()
            self.resetSortButtons(sender: UIButton())
            self.resetFilterButtons()
            self.refreshControl.endRefreshing()
        }
    }
    func handleCustomError(error: CustomError) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
            self.refreshControl.endRefreshing()
        }
    }
}
