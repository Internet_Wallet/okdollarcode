//
//  ReportAddWithdrawViewController.swift
//  OK
//  This show the list of add / withdraw records
//  Created by ANTONY on 06/02/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportAddWithdrawViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var tableViewAddWithdraw: UITableView!
    @IBOutlet weak var tableViewFilter: UITableView!
    @IBOutlet weak var constraintFilterTableLeading: NSLayoutConstraint!
    @IBOutlet weak var constraintScrollContentHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintFilterTableHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    //For search functions
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var buttonCancel: UIButton!{
        didSet {
            buttonCancel.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonClear: UIButton!{
        didSet {
            buttonClear.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    //For filter functions
    @IBOutlet weak var buttonAccountType: ButtonWithImage!{
        didSet {
            buttonAccountType.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonAmountRange: ButtonWithImage!{
        didSet {
            buttonAmountRange.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonPeriod: ButtonWithImage!{
        didSet {
            buttonPeriod.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    //For sorting functions
    @IBOutlet weak var buttonReceiverSort: ButtonWithImage!{
        didSet {
            buttonReceiverSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonCashTypeSort: ButtonWithImage!{
        didSet {
            buttonCashTypeSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonCardNumberSort: ButtonWithImage!{
        didSet {
            buttonCardNumberSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonAmountSort: ButtonWithImage!{
        didSet {
            buttonAmountSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonFeesSort: ButtonWithImage!{
        didSet {
            buttonFeesSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonOkTransIDSort: ButtonWithImage!{
        didSet {
            buttonOkTransIDSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonBankTransIDSort: ButtonWithImage!{
        didSet {
            buttonBankTransIDSort.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    @IBOutlet weak var buttonDate: ButtonWithImage!{
        didSet {
            buttonDate.titleLabel?.font = UIFont(name : appFont, size : appFontSizeReport)
        }
    }
    
    @IBOutlet weak var constraintScrollTop: NSLayoutConstraint!
    
    // MARK: - Properties
    let cellIDAddWithdraw = "ReportAddWithdrawCell"
    private let accountFilters = ["Account Type send Money report", "All", "MPU", "Visa", "Master Add Money", "CB Bank", "KBZ Bank"]
    private let amountRanges = ["Amount", "All", "0 - 1,000", "1,001 - 10,000", "10,001 - 50,000",
                                "50,001 - 1,00,000", "1,00,001 - 2,00,000",
                                "2,00,001 - 5,00,000", "5,00,001 - Above"]
    private let periodTypes = ["Period", "All", "Date", "Month"]
    private var filterArray = [String]()
    private enum FilterType {
        case account, amount, period
    }
    private var lastSelectedFilter = FilterType.account
    private let refreshControl = UIRefreshControl()
    private var awReportList = [AddWithdrawData]()
    lazy var modelReport: ReportModel = {
        return ReportModel()
    }()
    var fromDate: Date?
    var toDate: Date?
    var isSearchViewShow: Bool {
        get {
            return !viewSearch.isHidden
        }
        set(newValue) {
            if newValue {
                viewSearch.isHidden = false
                textFieldSearch.becomeFirstResponder()
                constraintScrollTop.constant = 0
                self.view.layoutIfNeeded()
            } else {
                textFieldSearch.text = ""
                viewSearch.isHidden = true
                constraintScrollTop.constant = -40
                self.view.layoutIfNeeded()
                self.view.endEditing(true)
            }
        }
    }
    
    // MARK: - View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewAddWithdraw.tag = 0
        tableViewFilter.tag = 1
        tableViewAddWithdraw.tableFooterView = UIView()
        tableViewFilter.tableFooterView = UIView()
        tableViewAddWithdraw.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshTableData), for: .valueChanged)
        buttonCancel.layer.cornerRadius = 15
        buttonClear.layer.cornerRadius = 15
        buttonCancel.addTarget(self, action: #selector(hideSearchView), for: .touchUpInside)
        buttonClear.addTarget(self, action: #selector(clearSerchTextField), for: .touchUpInside)
        buttonCancel.setTitle("Cancel".localized, for: .normal)
        buttonClear.setTitle("Clear".localized, for: .normal)
        setLocalizedTitles()
        let cellFilterNibName = UINib(nibName: ConstantsXibCell.filterTableViewCell, bundle: nil)
        tableViewFilter.register(cellFilterNibName, forCellReuseIdentifier: ConstantsXibCell.filterTableViewCell)
        AppDelegate.AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)
        setUpNavigation()
        modelReport.reportModelDelegate = self
        resetFilterButtons()
        getAddWithdrawReport()
    }
    
    override func viewDidLayoutSubviews() {
        let navBarHeight = navigationController?.navigationBar.frame.size.height ?? 44
        let heightToReduce = navBarHeight == 32 ? 56 : 44
        constraintScrollContentHeight.constant = scrollView.frame.size.height - CGFloat(heightToReduce) + 44
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    // MARK: - Target Methods
    @objc private func showSearchView() {
        tableViewFilter.isHidden = true
        if !isSearchViewShow {
            if awReportList.count > 0 {
                isSearchViewShow = true
                textFieldSearch.becomeFirstResponder()
            } else {
                noRecordAlertAction()
            }
        } else {
            //textFieldSearch.becomeFirstResponder()
            hideSearchView()
        }
    }
    
    @objc private func showShareOption() {
        tableViewFilter.isHidden = true
        if awReportList.count < 1 {
            noRecordAlertAction()
        } else {
            guard let reportSharingViewController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportSharingViewController.nameAndID) as? ReportSharingViewController else { return }
            reportSharingViewController.delegate = self
            reportSharingViewController.pdfImage = UIImage(named: "sendMoneyPdf")
            reportSharingViewController.excelImage = UIImage(named: "sendMoneyExcel")
            reportSharingViewController.modalPresentationStyle = .overCurrentContext
            self.present(reportSharingViewController, animated: false, completion: nil)
        }
    }
    
    @objc func refreshTableData() {
        getAddWithdrawReport()
    }
    
    @objc private func hideSearchView() {
        self.view.endEditing(true)
        textFieldSearch.text = ""
        viewSearch.isHidden = true
        constraintScrollTop.constant = -40
        self.view.layoutIfNeeded()
        self.view.endEditing(true)
        applyFilter()
    }
    
    @objc private func clearSerchTextField() {
        textFieldSearch.text = ""
        applyFilter()
    }
    
    // MARK: - Methods
    func showDateOptions(selectedOption: String) {
        switch selectedOption {
        case "Period".localized, "All".localized:
            self.fromDate = nil
            self.toDate = nil
            buttonPeriod.setTitle(selectedOption, for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        case "Date".localized:
            presentDatePicker(mode: DateMode.fromToDate)
        case "Month".localized:
            presentDatePicker(mode: DateMode.month)
        default:
            break
        }
    }
    
    func presentDatePicker(mode: DateMode) {
        guard let reportDatePickerController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportDatePickerController.nameAndID) as? ReportDatePickerController else { return }
        reportDatePickerController.dateMode = mode
        reportDatePickerController.delegate = self
        reportDatePickerController.modalPresentationStyle = .overCurrentContext
        self.present(reportDatePickerController, animated: false, completion: nil)
    }
    
    func applyFilter(searchText: String = "") {
        var filteredArray = modelReport.addWithdrawReports
        filteredArray = filterProcess(forAccount: filteredArray)
        filteredArray = filterProcess(forAmount: filteredArray)
        filteredArray = filterProcess(forPeriod: filteredArray)
        if isSearchViewShow && searchText.count > 0 {
            filteredArray = filteredArray.filter {
                if let rNo = $0.beneficiaryAccountNumber {
                    if "\(rNo.lowercased())".range(of:searchText.lowercased()) != nil {
                        return true
                    }
                }
                if let tID = $0.okTransactionID {
                    if tID.lowercased().range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                if let amount = $0.amount {
                    let paymentAmount = "\(amount)"
                    if paymentAmount.range(of: searchText.lowercased()) != nil {
                        return true
                    }
                }
                return false
            }
        }
        awReportList = filteredArray
        reloadTableWithScrollToTop()
        resetSortButtons(sender: UIButton())
    }
    
    func filterProcess(forAccount arrayToFilter: [AddWithdrawData]) -> [AddWithdrawData] {
        func accountFilter(str: String) -> [AddWithdrawData] {
            return arrayToFilter.filter {
                guard let type = $0.addMoneyType else { return false }
                return type.lowercased().contains(str)
            }
        }
        if let accountType = buttonAccountType.currentTitle {
            switch accountType {
            case "Account Type send Money report".localized, "All".localized:
                return arrayToFilter
            case "MPU".localized:
                return accountFilter(str: "mpu")
            case "Visa".localized:
                return accountFilter(str: "visa")
            case "Master Add Money".localized:
                return accountFilter(str: "master")
            case "CB Bank".localized:
                return accountFilter(str: "cb bank")
            case "KBZ Bank".localized:
                return accountFilter(str: "kbz bank")
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcess(forAmount arrayToFilter: [AddWithdrawData]) -> [AddWithdrawData] {
        func filterAmountRange(minVal: Double, maxVal: Double)-> [AddWithdrawData] {
            return arrayToFilter.filter
                {
                    guard let amount = $0.amount else { return false }
                    return ((amount > minVal) && (amount < maxVal))
            }
        }
        if let amountFilter = buttonAmountRange.currentTitle {
            switch amountFilter {
            case "Amount".localized, "All".localized:
                return arrayToFilter
            case "0 - 1,000".localized:
                return filterAmountRange(minVal: -0.1, maxVal: 1001)
            case "1,001 - 10,000".localized:
                return filterAmountRange(minVal: 1000.99, maxVal: 10001)
            case "10,001 - 50,000".localized:
                return filterAmountRange(minVal: 10000.99, maxVal: 50001)
            case "50,001 - 1,00,000".localized:
                return filterAmountRange(minVal: 50000.99, maxVal: 100001)
            case "1,00,001 - 2,00,000".localized:
                return filterAmountRange(minVal: 100000.99, maxVal: 200001)
            case "2,00,001 - 5,00,000".localized:
                return filterAmountRange(minVal: 200000.99, maxVal: 500001)
            case "5,00,001 - Above".localized:
                return arrayToFilter.filter {
                    guard let amount = $0.amount else { return false }
                    return amount > 500000.99
                }
            default:
                return []
            }
        }
        return []
    }
    
    func filterProcess(forPeriod arrayToFilter: [AddWithdrawData]) -> [AddWithdrawData] {
        if let periodFilter = buttonPeriod.currentTitle {
            switch periodFilter {
            case "Period".localized, "All".localized:
                return arrayToFilter
            default:
                break
            }
            if let exactDateStart = self.fromDate {
                return arrayToFilter.filter {
                    guard let tDate = $0.transactionDate else { return false }
                    guard let transDate = tDate.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") else { return false }
                    guard let dateStart = Calendar.current.date(byAdding: Calendar.Component.day, value: -1, to: exactDateStart) else { return false }
                    guard let exactDateEnd = self.toDate else { return false }
                    guard let dateEnd = Calendar.current.date(byAdding: Calendar.Component.day, value: 1, to: exactDateEnd) else { return false}
                    return (dateStart.compare(transDate) == .orderedAscending && transDate.compare(dateEnd) == .orderedAscending)
                }
            } else {
                return arrayToFilter.filter {
                    guard let tDate = $0.transactionDate else { return false }
                    guard let transDate = tDate.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") else { return false }
                    let trDateStr = transDate.stringValue(dateFormatIs: "yyyy-MM")
                    guard let transMonth = trDateStr.dateValue(dateFormatIs: "yyyy-MM") else { return false }
                    guard let monthToFilter = self.toDate else { return false }
                    return transMonth.compare(monthToFilter) == .orderedSame
                }
            }
        }
        return []
    }
    
    func reloadTableWithScrollToTop() {
        self.tableViewAddWithdraw.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            if self.awReportList.count > 0 {
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableViewAddWithdraw.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
    }
    
    func resetFilterButtons() {
        buttonAccountType.setTitle(accountFilters[0].localized, for: .normal)
        buttonAmountRange.setTitle(amountRanges[0].localized, for: .normal)
        buttonPeriod.setTitle(periodTypes[0].localized, for: .normal)
    }
    
    func noRecordAlertAction() {
        alertViewObj.wrapAlert(title: "Alert".localized, body: "No Records".localized, img: UIImage(named : "AppIcon"))
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
        })
        alertViewObj.showAlert(controller: self)
    }
    
    func updateFilterTableHeight() {
        constraintFilterTableHeight.constant = filterArray.count > 4 ? CGFloat(5*44) : CGFloat(filterArray.count * 44)
        self.view.layoutIfNeeded()
    }
    
    func resetSortButtons(sender: UIButton) {
        if sender != buttonReceiverSort {
            buttonReceiverSort.tag = 0
            buttonReceiverSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonCashTypeSort {
            buttonCashTypeSort.tag = 0
            buttonCashTypeSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonCardNumberSort {
            buttonCardNumberSort.tag = 0
            buttonCardNumberSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonAmountSort {
            buttonAmountSort.tag = 0
            buttonAmountSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonFeesSort {
            buttonFeesSort.tag = 0
            buttonFeesSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonOkTransIDSort {
            buttonOkTransIDSort.tag = 0
            buttonOkTransIDSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonBankTransIDSort {
            buttonBankTransIDSort.tag = 0
            buttonBankTransIDSort.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
        if sender != buttonDate {
            buttonDate.tag = 0
            buttonDate.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
        }
    }
    
    func setLocalizedTitles() {
        buttonReceiverSort.setTitle("Receiver OK$ Number".localized, for: .normal)
        buttonCashTypeSort.setTitle("Card Type".localized, for: .normal)
        buttonCardNumberSort.setTitle("Card Number".localized, for: .normal)
        buttonAmountSort.setTitle("Amount".localized + " " + "(MMK)".localized, for: .normal)
        buttonFeesSort.setTitle("Fees".localized + " " + "(MMK)".localized, for: .normal)
        buttonOkTransIDSort.setTitle("OK$ Transaction ID".localized, for: .normal)
        buttonBankTransIDSort.setTitle("Bank Transaction ID".localized, for: .normal)
        buttonDate.setTitle("Date & Time".localized, for: .normal)
        textFieldSearch.placeholder = "Search By Transaction ID or Amount or Mobile Number or Remarks".localized
    }
    
    func shareRecordsByPdf() {
        let pdfView = UIView()
        if let pdfHeaderView = Bundle.main.loadNibNamed("ReportAddWithdrawPdfHeader", owner: self, options: nil)?.first as? ReportAddWithdrawPdfHeader {
            
            pdfHeaderView.fillDetails(awReports: awReportList, filterAccType: buttonAccountType.currentTitle ?? "", filterAmount: buttonAmountRange.currentTitle ?? "", filterPeriod: buttonPeriod.currentTitle ?? "", walletBal: String(walletAmount()))
            
            let newHeight = pdfHeaderView.viewForFilter.frame.maxY
            pdfHeaderView.frame = CGRect(x: 0, y: 0, width: pdfHeaderView.frame.size.width, height: newHeight)
            pdfView.addSubview(pdfHeaderView)
            pdfView.frame = pdfHeaderView.bounds
            let xPos: CGFloat = pdfHeaderView.viewForFilter.frame.minX
            var yPos: CGFloat = pdfView.frame.maxY
            
            for record in awReportList {
               if let pdfBodyView = Bundle.main.loadNibNamed("ReportAddWithdrawPdfBody", owner: self, options: nil)?.first as? ReportAddWithdrawPdfBody {
                    pdfBodyView.configureData(awReportData: record)
                    let recordHeight = pdfBodyView.frame.size.height
                    let yPosInPage = Int(yPos) % 842
                    if (yPosInPage + Int(recordHeight)) > 842 {
                        yPos = yPos + CGFloat((842 - yPosInPage) + 30 + 30)
                    }
                    pdfBodyView.frame = CGRect(x: xPos, y: yPos, width: pdfBodyView.frame.size.width, height: recordHeight)
                    yPos = yPos + recordHeight + 2
                    pdfView.addSubview(pdfBodyView)
                }
            }
            pdfView.frame = CGRect(x: 0, y: 0, width: pdfView.frame.size.width, height: yPos + 30)
            guard let pdfUrl = CodeSnippets.generatePDF(viewForPDF: pdfView, pdfFile: "OK$ Add Money To Bank Report") else {
                println_debug("Error - pdf not generated")
                return
            }
            let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
            if self.presentedViewController != nil {
                self.dismiss(animated: false, completion: {
                    [unowned self] in
                    DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
                })
            }else{
                DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
            }
        }
    }
    
    func shareRecordsByExcel() {
        var excelTxt = "Receiver OK$ Number,Card Type,Card Number,Amount (MMK),Fees (MMK),OK$ Transaction ID,Bank Transaction ID,Date & Time\n"
        var totalAmount: Double = 0
        var totalFAmount: Double = 0
        for record in awReportList {
            var country = ""
            var number = ""
            if let num = record.beneficiaryAccountNumber, num.hasPrefix("00") {
                number = String(num.dropFirst(4))
                country = String(num.prefix(4))
                country = country.replacingOccurrences(of: "00", with: "+")
            } else {
                country = "+95"
                if let num = record.beneficiaryAccountNumber {
                    number = String(num.dropFirst())
                }
            }
            let rNo = CodeSnippets.getMobileNumWithBrackets(countryCode: country, number: "0\(number)")
            var cardType = ""
            if let cardTypeL = record.addMoneyType?.uppercased(), cardTypeL == "MASTERPAY" {
                cardType = "VISA"
            } else {
                cardType = record.addMoneyType?.uppercased() ?? ""
            }
            var cardNo = "XXXXXXXXXXXX"
            if let cNumber = record.cardNumber {
                cardNo = cNumber
            }
            var tAmount = ""
            var fees = ""
            if let amount = record.amount {
                totalAmount += amount
                tAmount = "\(amount)".replacingOccurrences(of: ".0", with: "")
            }
            if let amount = record.adminFee {
                totalFAmount += amount
                fees = "\(amount)".replacingOccurrences(of: ".0", with: "")
            }
            let transID = record.okTransactionID ?? ""
            let bankTransID = record.bankTransactionID ?? ""
            var tDate = ""
            if let dateStr = record.transactionDate {
                if let dateValue = dateStr.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") {
                    let dateString = dateValue.stringValue(dateFormatIs: "EEE dd-MMM-yyyy HH:mm:ss")
                    tDate = dateString
                }
            }
            
            let recText = "\(rNo),\(cardType),\(cardNo),\(tAmount),\(fees),\(transID),\(bankTransID),\(tDate)\n"
            excelTxt.append(recText)
        }
        let totAmtStr = "\(totalAmount)".replacingOccurrences(of: ".0", with: "")
        let totFAmt = "\(totalFAmount)".replacingOccurrences(of: ".0", with: "")
        excelTxt.append("Total,,,\(totAmtStr),\(totFAmt),\n")
        guard let excelUrl = CodeSnippets.generateExcel(excelDataWithFormat: excelTxt, excelFileName: "OK$ Add Money To Bank Report") else {
            println_debug("Error - excel not generated")
            return
        }
        let activityViewController = UIActivityViewController(activityItems: [excelUrl], applicationActivities: nil)
        if self.presentedViewController != nil {
            self.dismiss(animated: false, completion: {
                [unowned self] in
                DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
            })
        }else{
            DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
        }
    }
    
    private func getAddWithdrawReport() {
        getTokenFromServer(completionHandler: { (isSuccess, tokenString) in
            guard isSuccess, let tokenAuthStr = tokenString as? String else {
                progressViewObj.removeProgressView()
                self.showErrorAlert(errMessage: "try again")
                return
            }
            DispatchQueue.main.async(execute: {
                progressViewObj.removeProgressView()
                self.modelReport.getAddWithdrawInfo(authToken: tokenAuthStr)
            })
        })
    }
    
    func getTokenFromServer(completionHandler: @escaping (Bool,Any?) -> Void) {
//        let urlStr = serverUrl == .productionUrl ? Url.AddMoney_GetToken : Url.AddMoney_GetToken_Test
//        let url = URL(string: urlStr)
        
        let tokenUrl = getUrl(urlStr: "", serverType: .AddMoney_GetToken)
        
        let hashValue = Url.aKey_AddMoney.hmac_SHA1(key: Url.sKey_AddMoney)
        let inputVal = "password=\(hashValue)&grant_type=password"
        
        progressViewObj.showProgressView()
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: kMethod_Post, contentType: kContentType_urlencoded, inputVal: inputVal, authStr: nil)
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                }
                completionHandler(false,nil)
                return
            }
            
            let responseDic = response as! NSDictionary
            guard let tokentype = responseDic.value(forKey: "token_type"), let accesstoken = responseDic.value(forKey: "access_token") else {
                
                completionHandler(false,nil)
                return
            }
            let authorizationString =  "\(tokentype) \(accesstoken)"
            completionHandler(true,authorizationString)
        })
    }
    
    // MARK: - Button Actions
    // It filters and show the detail by transaction type
    @IBAction func filterAccountType(_ sender: UIButton) {
        self.view.endEditing(true)
        func showListAtAccountType() {
            filterArray = accountFilters
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 125
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
            tableViewFilter.isHidden = false
        }
        if tableViewFilter.isHidden {
            showListAtAccountType()
        } else {
            switch lastSelectedFilter {
            case .account :
                tableViewFilter.isHidden = true
            default:
                showListAtAccountType()
            }
        }
        lastSelectedFilter = .account
    }
    
    // It filters and show the detail by amount range
    @IBAction func filterByAmount(_ sender: UIButton) {
        self.view.endEditing(true)
        func showListAtAmount() {
            filterArray = amountRanges
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 530
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
            tableViewFilter.isHidden = false
        }
        if tableViewFilter.isHidden {
            showListAtAmount()
        } else {
            switch lastSelectedFilter {
            case .amount :
                tableViewFilter.isHidden = true
            default:
                showListAtAmount()
            }
        }
        lastSelectedFilter = .amount
    }
    
    // It filters and show the detail by period type
    @IBAction func filterByPeriod(_ sender: UIButton) {
        self.view.endEditing(true)
        func showListAtPeriod() {
            filterArray = periodTypes
            tableViewFilter.reloadData()
            tableViewFilter.isHidden = false
            constraintFilterTableLeading.constant = 930
            self.view.layoutIfNeeded()
            updateFilterTableHeight()
        }
        if tableViewFilter.isHidden {
            showListAtPeriod()
        } else {
            switch lastSelectedFilter {
            case .period :
                tableViewFilter.isHidden = true
            default:
                showListAtPeriod()
            }
        }
        lastSelectedFilter = .period
    }
    
    @IBAction private func sortByOkNumber(_ sender: UIButton) {
        func sortByAccNo(orderType: ComparisonResult) -> [AddWithdrawData]{
            return awReportList.sorted {
                guard let accNo1 = $0.beneficiaryAccountNumber, let accNo2 = $1.beneficiaryAccountNumber else { return false }
                return accNo1.localizedStandardCompare(accNo2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            awReportList = sortByAccNo(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            awReportList = sortByAccNo(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction private func sortByCardType(_ sender: UIButton) {
        func sortByCardType(orderType: ComparisonResult) -> [AddWithdrawData]{
            return awReportList.sorted {
                guard let cardType1 = $0.addMoneyType, let cardType2 = $1.addMoneyType else { return false }
                return cardType1.localizedStandardCompare(cardType2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            awReportList = sortByCardType(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            awReportList = sortByCardType(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByCardNumber(_ sender: UIButton) {
        func sortByCardNo(orderType: ComparisonResult) -> [AddWithdrawData]{
            return awReportList.sorted {
                guard let cardNo1 = $0.cardNumber, let cardNo2 = $1.cardNumber else { return false }
                return cardNo1.localizedStandardCompare(cardNo2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            awReportList = sortByCardNo(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            awReportList = sortByCardNo(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByAmount(_ sender: UIButton) {
        func sortByAmount(orderType: ComparisonResult) -> [AddWithdrawData]{
            switch orderType {
            case .orderedAscending:
                return awReportList.sorted {
                    guard let amount1 = $0.amount, let amount2 = $1.amount else { return false }
                    return amount1 < amount2
                }
            case .orderedDescending:
                return awReportList.sorted {
                    guard let amount1 = $0.amount, let amount2 = $1.amount else { return false }
                    return amount1 > amount2
                }
            default:
                return awReportList
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            awReportList = sortByAmount(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            awReportList = sortByAmount(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByFees(_ sender: UIButton) {
        func sortByFees(orderType: ComparisonResult) -> [AddWithdrawData]{
            switch orderType {
            case .orderedAscending:
                return awReportList.sorted {
                    guard let amount1 = $0.adminFee, let amount2 = $1.adminFee else { return false }
                    return amount1 < amount2
                }
            case .orderedDescending:
                return awReportList.sorted {
                    guard let amount1 = $0.adminFee, let amount2 = $1.adminFee else { return false }
                    return amount1 > amount2
                }
            default:
                return awReportList
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            awReportList = sortByFees(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            awReportList = sortByFees(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByOkTransID(_ sender: UIButton) {
        func sortByTransID(orderType: ComparisonResult) -> [AddWithdrawData]{
            return awReportList.sorted {
                guard let trans1 = $0.okTransactionID, let trans2 = $1.okTransactionID else { return false }
                return trans1.localizedStandardCompare(trans2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            awReportList = sortByTransID(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            awReportList = sortByTransID(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByBankTransID(_ sender: UIButton) {
        func sortByBankTransID(orderType: ComparisonResult) -> [AddWithdrawData]{
            return awReportList.sorted {
                guard let trans1 = $0.bankTransactionID, let trans2 = $1.bankTransactionID else { return false }
                return trans1.localizedStandardCompare(trans2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            awReportList = sortByBankTransID(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            awReportList = sortByBankTransID(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            break
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
    
    @IBAction func sortByDate(_ sender: UIButton) {
        func sortDates(orderType: ComparisonResult) -> [AddWithdrawData]{
            return awReportList.sorted {
                guard let date1 = $0.transactionDate, let date2 = $1.transactionDate else { return false }
                return date1.compare(date2) == orderType
            }
        }
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpWhite"), for: .normal)
            awReportList = sortDates(orderType: .orderedAscending)
        case 1:
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "reportArrowDownWhite"), for: .normal)
            awReportList = sortDates(orderType: .orderedDescending)
        case 2:
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "reportArrowUpDownWhite"), for: .normal)
            applyFilter(searchText: textFieldSearch.text ?? "")
        default:
            sender.tag = 0
        }
        resetSortButtons(sender: sender)
        reloadTableWithScrollToTop()
    }
}

//MARK: - ReportSharingOptionDelegte
extension ReportAddWithdrawViewController: ReportSharingOptionDelegte {
    func share(by: ReportSharingViewController.ReportShareOption) {
        switch by {
        case .pdf:
            shareRecordsByPdf()
        case .excel:
            shareRecordsByExcel()
        }
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ReportAddWithdrawViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        switch tableView.tag {
        case 0:
            if awReportList.count == 0 {
                let noDataLabel = UILabel(frame: tableView.bounds)
                if let myFont = UIFont(name: appFont, size: 14) {
                    noDataLabel.font = myFont
                }
                noDataLabel.text = "No Records".localized
                noDataLabel.textAlignment = .center
                tableView.backgroundView = noDataLabel
            } else {
                tableView.backgroundView = nil
            }
            return 1
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView.tag {
        case 0:
            return awReportList.count
        case 1:
            return filterArray.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView.tag {
        case 0:
            guard let reportAddWithdrawCell = tableView.dequeueReusableCell(withIdentifier: cellIDAddWithdraw, for: indexPath) as? ReportAddWithdrawCell else { return ReportAddWithdrawCell() }
            let reportData = awReportList[indexPath.row]
            reportAddWithdrawCell.configureCell(awReportData: reportData)
            return reportAddWithdrawCell
        case 1:
            guard let filterCell = tableView.dequeueReusableCell(withIdentifier: ConstantsXibCell.filterTableViewCell, for: indexPath) as? FilterTableViewCell else { return FilterTableViewCell() }
            filterCell.configureLabel(labelTitle: filterArray[indexPath.row])
            return filterCell
        default:
            let reportAddWithdrawCell = tableView.dequeueReusableCell(withIdentifier: cellIDAddWithdraw, for: indexPath) as? ReportAddWithdrawCell
            return reportAddWithdrawCell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView.tag {
        case 0:
            break
        case 1:
            switch lastSelectedFilter {
            case .account:
                buttonAccountType.setTitle(filterArray[indexPath.row].localized, for: .normal)
                tableViewFilter.isHidden = true
                applyFilter(searchText: textFieldSearch.text ?? "")
            case .amount:
                buttonAmountRange.setTitle(filterArray[indexPath.row].localized, for: .normal)
                tableViewFilter.isHidden = true
                applyFilter(searchText: textFieldSearch.text ?? "")
            case .period:
                tableViewFilter.isHidden = true
                showDateOptions(selectedOption: filterArray[indexPath.row].localized)
            }
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch tableView.tag {
        case 1:
            return 44.0
        default:
            return 80.0
        }
    }
}

//MARK: - UIScrollViewDelegate
extension ReportAddWithdrawViewController: UIScrollViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        targetContentOffset.pointee.y = max(targetContentOffset.pointee.y - 1, 1)
    }
}

// MARK: - ReportDatePickerDelegate
extension ReportAddWithdrawViewController: ReportDatePickerDelegate {
    func processWithDate(fromDate: Date?, toDate: Date?, dateMode: DateMode) {
        self.fromDate = fromDate
        self.toDate = toDate
        var selectedDate = ""
        switch dateMode {
        case .fromToDate:
            if let safeFDate = fromDate, let safeTDate = toDate {
                let fDateInString = safeFDate.stringValue(dateFormatIs: "dd MMM yyyy")
                let tDateInString = safeTDate.stringValue(dateFormatIs: "dd MMM yyyy")
                selectedDate = fDateInString + " to\n" + tDateInString
            }
        case .month:
            if let safeDate = toDate {
                let monthInString = safeDate.stringValue(dateFormatIs: "MMM yyyy")
                selectedDate = monthInString
            }
        default:
            break
        }
        buttonPeriod.setTitle(selectedDate, for: .normal)
        applyFilter(searchText: textFieldSearch.text ?? "")
    }
}

// MARK: - UITextFieldDelegate
extension ReportAddWithdrawViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tableViewFilter.isHidden = true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        tableViewAddWithdraw.contentOffset = CGPoint(x: 0, y: 0)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            if updatedText.count > 50 || (updatedText == "" && text == "") {
                return false
            }
            if string != "\n" {
                applyFilter(searchText: updatedText)
            }
        }
        return true
    }
}

// MARK: - Additional Methods
extension ReportAddWithdrawViewController: NavigationSetUpProtocol  {
    /// It set up the navigation bar
    func setUpNavigation() {
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationController?.navigationBar.barTintColor        = ConstantsColor.navigationHeaderAddWithdraw
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: titleViewWidth, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        
        let searchButton = UIButton(type: .custom)
        searchButton.setImage(#imageLiteral(resourceName: "reportSearch").withRenderingMode(.alwaysOriginal), for: .normal)
        searchButton.addTarget(self, action: #selector(showSearchView), for: .touchUpInside)
        
        let shareButton = UIButton(type: .custom)
        shareButton.setImage(#imageLiteral(resourceName: "reportShare").withRenderingMode(.alwaysOriginal), for: .normal)
        shareButton.addTarget(self, action: #selector(showShareOption), for: .touchUpInside)
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        let ypos = (titleViewSize.height - 30 ) / 2
        if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 140, y: ypos, width: 30, height: 30)
            shareButton.frame = CGRect(x: navTitleView.frame.size.width - 100, y: ypos, width: 30, height: 30)
            backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        }
        else
        {
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 70, y: ypos, width: 30, height: 30)
            shareButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
            backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
            
        }
      
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 160, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text = ConstantsController.reportAddWithdrawViewController.screenTitle.localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 23) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        navTitleView.addSubview(searchButton)
        navTitleView.addSubview(shareButton)
        self.navigationItem.titleView = navTitleView
    }
    
    /// It shows the home screen
    @objc func backAction() {
//        AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
//        self.navigationController?.popViewController(animated: true)
        if isSearchViewShow {
            hideSearchView()
        } else {
            AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
            self.navigationController?.popViewController(animated: true)
        }
    }
}

//MARK: - ReportModelDelegate
extension ReportAddWithdrawViewController: ReportModelDelegate {
    func apiResult(message: String?, statusCode: String?) {
        DispatchQueue.main.async {
            self.awReportList = self.modelReport.addWithdrawReports
            self.awReportList = self.awReportList.sorted {
                    guard let date1 = $0.transactionDate, let date2 = $1.transactionDate else { return false }
                    return date1.compare(date2) == .orderedDescending
            }
            self.tableViewAddWithdraw.reloadData()
            self.resetFilterButtons()
            self.resetSortButtons(sender: UIButton())
            self.refreshControl.endRefreshing()
            self.reloadTableWithScrollToTop()
        }
    }
    func handleCustomError(error: CustomError) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}
