//
//  ReportVotingPdfBody.swift
//  OK
//
//  Created by E J ANTONY on 26/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReportVotingPdfBody: UIView {
    
    //MARK: - Outlet
    @IBOutlet weak var lblProgramName: UILabel!{
        didSet{
            lblProgramName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblEvent: UILabel!{
        didSet{
            lblEvent.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblParticipantName: UILabel!{
        didSet{
            lblParticipantName.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblTransactionID: UILabel!{
        didSet{
            lblTransactionID.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblVoteCount: UILabel!{
        didSet{
            lblVoteCount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblVotingType: UILabel!{
        didSet{
            lblVotingType.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblAmount: UILabel!{
        didSet{
            lblAmount.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblDate: UILabel!{
        didSet{
            lblDate.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    //MARK: - Methods
    func configureData(voteData: VotingReportList) {
        self.lblProgramName.text = ""
        if let mainProgram = voteData.mainProgrammeName {
            var programName = mainProgram
            if let subProgramName = voteData.programmeName {
                programName += "\n\(subProgramName)"
            }
            self.lblProgramName.text = programName
        }
        self.lblEvent.text = ""
        if let eventName = voteData.eventName {
            self.lblEvent.text = eventName
        }
        self.lblParticipantName.text = ""
        if let participateName = voteData.participantName {
            self.lblParticipantName.text = participateName
        }
        self.lblTransactionID.text = "xxxxxxx"
        if let transId = voteData.payment?.transactionID {
            self.lblTransactionID.text = transId
        }
        self.lblVoteCount.text = "0"
        if let voteCount = voteData.votes {
            self.lblVoteCount.text = "\(voteCount)"
        }
        self.lblVotingType.text = ""
        if let type = voteData.votingTypeName {
            self.lblVotingType.text = type
        }
        self.lblAmount.text = "0.0"
        if let amount = voteData.payment?.amount {
            self.lblAmount.text = "\(amount)"
        }
        self.lblDate.text = ""
        if let createdDateStr = voteData.createdDate {
            let dateValue = createdDateStr.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS")
            let dateString = dateValue?.stringValue(dateFormatIs: "dd-MM-yyyy\nHH:mm:ss")
            self.lblDate.text = dateString
        }
    }
}
