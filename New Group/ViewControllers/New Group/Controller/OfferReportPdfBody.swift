//
//  OfferReportPdfBody.swift
//  OK
//
//  Created by E J ANTONY on 04/08/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class OfferReportPdfBody: UIView {
    //MARK: - Outlet
    @IBOutlet weak var lblAccName: UILabel!{
        didSet {
            lblAccName.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblAccNumber: UILabel!{
        didSet {
            lblAccNumber.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblBillAmount: UILabel!{
        didSet {
            lblBillAmount.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblDiscountAmount: UILabel!{
        didSet {
            lblDiscountAmount.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblNetReceivedAmount: UILabel!{
        didSet {
            lblNetReceivedAmount.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblTransactionID: UILabel!{
        didSet {
            lblTransactionID.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblPromoCode: UILabel!{
        didSet {
            lblPromoCode.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblDateTime: UILabel!{
        didSet {
            lblDateTime.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblSaleQnty: UILabel!{
        didSet {
            lblSaleQnty.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    @IBOutlet weak var lblFreeQnty: UILabel!{
        didSet {
            lblFreeQnty.font = UIFont(name : appFont, size : appFontSizeReport)
            }
          }
    
    //MARK: - Methods
    func configureData(data: OfferReporFilterList, amountType: Int) {
        self.lblPromoCode.text = ""
        if let pCode = data.promotionCode {
            self.lblPromoCode.text = "\(pCode)"
        }
        self.lblAccName.text = ""
        if let cName = data.promotionUsedCustomerName {
            self.lblAccName.text = cName
        }
        self.lblAccNumber.text = ""
        if var mN = data.promotionUsedMobileNumber {
            self.lblAccNumber.text = mN
            if mN.hasPrefix("0095") {
                mN = "0" + String(mN.substring(from: 4))
                self.lblAccNumber.text = mN
            }
        }
        self.lblBillAmount.text = "0"
        if let bAmount = data.totalNetAmount {
            self.lblBillAmount.text = "\(bAmount)".replacingOccurrences(of: ".00", with: "").replacingOccurrences(of: ".0", with: "")
        }
        self.lblTransactionID.text = ""
        if let id = data.parentTransactionID {
            self.lblTransactionID.text = id
        }
        
        self.lblDateTime.text = ""
        let dateStr = data.transactionTime
        if let dateVal = dateStr?.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") {
            let fromDate = dateVal.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy \n HH:mm:ss")
            self.lblDateTime.text = fromDate
        }
        switch amountType {
        case 0, 1:
            self.lblSaleQnty.isHidden = true
            self.lblFreeQnty.isHidden = true
            self.fillDiscountNet(data: data)
            
        case 2:
            self.lblSaleQnty.isHidden = false
            self.lblFreeQnty.isHidden = false
            self.lblNetReceivedAmount.isHidden = true
            self.lblDiscountAmount.isHidden = true
            self.fillSaleFreeQnty(data: data)
        case 3,4:
            fallthrough
        case 5,6,7:
            self.lblSaleQnty.isHidden = false
            self.lblFreeQnty.isHidden = true
            self.lblNetReceivedAmount.isHidden = true
            self.lblDiscountAmount.isHidden = true
            self.fillSaleQnty(data: data)
        default:
            self.fillDiscountNet(data: data)
            self.fillSaleFreeQnty(data: data)
        }
        self.layoutIfNeeded()
    }
    
    func fillDiscountNet(data: OfferReporFilterList) {
        self.lblDiscountAmount.text = "0"
        if let discAmount = data.totalDiscountAmount {
            let percentageValue = Float((Float(discAmount) * 100) / Float(data.totalNetAmount ?? 0))
            self.lblDiscountAmount.text = "\(discAmount)".replacingOccurrences(of: ".00", with: "").replacingOccurrences(of: ".0", with: "") + " or " + "\(CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(percentageValue)")) %"
        }
        self.lblNetReceivedAmount.text = "0"
        if let nAmount = data.totalBillAmount {
            self.lblNetReceivedAmount.text = "\(nAmount)".replacingOccurrences(of: ".00", with: "").replacingOccurrences(of: ".0", with: "")
        }
    }
    
    func fillSaleFreeQnty(data: OfferReporFilterList) {
            self.lblSaleQnty.text = "\(data.buyingQty ?? 0)"
            self.lblFreeQnty.text = "\(data.freeQty ?? 0)"
    }
    
    func fillSaleQnty(data: OfferReporFilterList) {
        self.lblSaleQnty.text = "\(data.buyingQty ?? 0)"
    }
}
