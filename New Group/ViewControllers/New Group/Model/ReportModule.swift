//
//  ReportModule.swift
//  OK
//  This is model class for report module
//  Created by ANTONY on 09/02/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation

enum CustomError: String {
    case noInternet = "Please check your internet connection"
    case errorResponse = "Error Response"
    case noResponse = "No response code"
    case xmlSerializeFailed = "XML serialization failed"
    case jsonSerializeFailed = "JSON serialization failed"
    case serverNotReachable = "Server is not reachable"
}
/// This protocol is to implement the api response action
protocol ReportModelDelegate: class {
    func apiResult(message: String?, statusCode: String?)
    func handleCustomError(error: CustomError)
}





class ReportModel {
    // MARK: - Properties
    weak var reportModelDelegate: ReportModelDelegate?    
    
    var reportAllTransactionModelObj =  [ReportAllTransactionModel]()
    
    var sendMoneyBankInitialReports = [ReportSendMoneyToBank]()
    var resaleMainBalanceReports = [ReportResaleMainBalance]()
    var addWithdrawReports = [AddWithdrawData]()
    var cicoReports = [ReportCICOList]()
    var loyaltyReports = [LoyaltyAllReports]()
    var loyaltyRecords = [LoyaltyData]()
    private enum ReportApiType: String {
    case allTransaction = "All Transaction", sendMoneyToBank = "Send Money To Bank", resaleMainBalance = "Resale Main Balance", addWithdraw = "Add Withdraw", cico = "CashIn CashOut", loyaltyReport = "Loyalty Report"
    }
    //MARK: - Methods
    func getTransactionInfo(showLoader: Bool = true) {
        if appDelegate.checkNetworkAvail() {
            if showLoader {
                PTLoader.shared.show()
            }
            let web      = WebApiClass()
            web.delegate = self
            guard let pin = ok_password else {
                PTLoader.shared.hide()
                return
            }
            let urlStr   = String.init(format: Url.getReport, UserModel.shared.mobileNo, pin,UserLogin.shared.token, "0", "500")
            let getReportUrl = getUrl(urlStr: urlStr, serverType: .serverEstel)
            let params = [String: String]()
            println_debug("REQUEST URL: \(getReportUrl)")
            web.genericClass(url: getReportUrl, param: params as AnyObject, httpMethod: "GET", mScreen: ReportApiType.allTransaction.rawValue)
        } else {
            reportModelDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    //these will show only 20 records record 
    func getALLTransactionInfo(showLoader: Bool = true,first: String,last: String) {
           if appDelegate.checkNetworkAvail() {
               if showLoader {
                   PTLoader.shared.show()
               }
               let web      = WebApiClass()
               web.delegate = self
               guard let pin = ok_password else {
                   PTLoader.shared.hide()
                   return
               }
               let urlStr   = String.init(format: Url.getReport, UserModel.shared.mobileNo, pin,UserLogin.shared.token, first, last)
               let getReportUrl = getUrl(urlStr: urlStr, serverType: .serverEstel)
               let params = [String: String]()
               println_debug("REQUEST URL: \(getReportUrl)")
               web.genericClass(url: getReportUrl, param: params as AnyObject, httpMethod: "GET", mScreen: ReportApiType.allTransaction.rawValue)
           } else {
               reportModelDelegate?.handleCustomError(error: CustomError.noInternet)
           }
       }
    
    
    
    
    func getSendMoneyToBankReport(request: SendMoneyToBankReportRequest, showLoader: Bool = true) {
        if appDelegate.checkNetworkAvail() {
            if showLoader {
                PTLoader.shared.show()
            }
            let web      = WebApiClass()
            web.delegate = self
            
            let urlStrSuffix   = Url.getSendMoneyBankReport
            let sendMoneyToBankUrl = getUrl(urlStr: urlStrSuffix, serverType: .sendMoneyBankReportUrl)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(sendMoneyToBankUrl)")
            
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: sendMoneyToBankUrl, param: params , httpMethod: "POST", mScreen: ReportApiType.sendMoneyToBank.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            reportModelDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    func getResaleMainBalanceReport(authToken: String) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = String.init(format: Url.getResaleMainBalanceReport,UserModel.shared.mobileNo, "500")
            let getResaleMainBalanceReportUrl = getUrl(urlStr: urlStrSuffix, serverType: .airtimeUrl)
            let params = [String: String]()
            println_debug("REQUEST URL: \(getResaleMainBalanceReportUrl)")
            web.genericClass(url: getResaleMainBalanceReportUrl, param: params as AnyObject, httpMethod: "GET", mScreen: ReportApiType.resaleMainBalance.rawValue, authToken: authToken)
        } else {
            reportModelDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    func getAddWithdrawInfo(authToken: String) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let mobNo = serverUrl == .productionUrl ? UserModel.shared.mobileNo : UserModel.shared.mobileNo //"00959451248993"
            let urlStr   = String.init(format: Url.reportAddMoney, mobNo)
            let getAWReportUrl = getUrl(urlStr: urlStr, serverType: .addWithdrawUrl)
            let params = [String: String]()
            println_debug("REQUEST URL: \(getAWReportUrl)")
            web.genericClass(url: getAWReportUrl, param: params as AnyObject, httpMethod: "GET", mScreen: ReportApiType.addWithdraw.rawValue, authToken: authToken)
        } else {
            reportModelDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    func getFromDate(fromDateBool: Bool) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "ddMMyyyyHHmmss"
        if fromDateBool {
            return dateFormatter.string(from: Date())
        }
        
        let calendar = Calendar.init(identifier: .gregorian)
        let date = calendar.date(byAdding: .month, value: -1, to: Date()) ?? Date()
        return dateFormatter.string(from: date)
    }
    
    func getRecordsfForEarnPointTrf(index: Int) {
        if appDel.checkNetworkAvail() {
            let toDate = getFromDate(fromDateBool: true)
            let fromDate = getFromDate(fromDateBool: false)
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            var ip = ""
            if let ipAdd = OKBaseController.getIPAddress() {
                ip = ipAdd
            }
            var urlString = ""
            switch index {
            case 0:
                urlString = Url.earnPointTrfReportUrl
            case 1:
                urlString = Url.pointDistributionReportUrl
            case 2:
                urlString = Url.redeempointsReportUrl
            default:
                urlString = Url.earnPointTrfReportUrl
            }
            let urlStr   = String.init(format: urlString, UserModel.shared.mobileNo, toDate, ip, UserLogin.shared.token, fromDate)
            let getReportUrl = getUrl(urlStr: urlStr, serverType: .serverEstel)
            let params = [String: String]()
            println_debug("REQUEST URL: \(getReportUrl)")
            web.genericClass(url: getReportUrl, param: params as AnyObject, httpMethod: "POST", mScreen: ReportApiType.loyaltyReport.rawValue)
            
        } else {
            PaytoConstants.alert.showErrorAlert(title: "", body: PaytoConstants.messages.noInternet.localized)
        }
    }
    
    func handleTransactionResponse(data: AnyObject) {
        let responseString = String(data: data as! Data, encoding: .utf8)
        let xmlIndexer = SWXMLHash.parse(responseString!)
        do {
            PTLoader.shared.show()
            let transactionReportResponse = try GetTransReportResponse.deserialize(xmlIndexer)
            if let resultCodeString = transactionReportResponse.estel?.response?.resultCode {
                switch resultCodeString {
                case "0":
                    if let estel = transactionReportResponse.estel {
                        func uploadDB() {
                            println_debug("INSERTION INSERTION ****")
                            PTLoader.shared.show()
                            ReportCoreDataManager.sharedInstance.callerDelegate = self
                            ReportCoreDataManager.sharedInstance.insertTransactionInfoInDB(dataToInsert: estel)
                             // check for the records savinng in to the database 
                            ReportCoreDataManager.sharedInstance.insertAllTransactionRecords(dataForInsertion: estel)
                        }
                        func uploadSingleRecordInDB(record : RecordData) {
                            println_debug("INSERTION (Single Records in DB) INSERTION ****")
                            ReportCoreDataManager.sharedInstance.callerDelegate = self
                            ReportCoreDataManager.sharedInstance.insertSingleTransectionInfoInDB(dataSingleTransaction: record)
                        }
                        if let record = estel.response?.records?.record, record.count > 0 {
                            print(record.compactMap({$0.transactionId}))
                            let dbRecords = ReportCoreDataManager.sharedInstance.fetchTransactionRecords()
                            if dbRecords.count > 0 {
                                for (index , value) in record.enumerated() {
                                   let recordTransID = record[index].transactionId
                                    var isPresent = false
                                     for (indexA , _) in dbRecords.enumerated() {
                                       let recordTransDB = dbRecords[indexA].transID
                                        if recordTransID == recordTransDB {
                                            isPresent = true
                                             println_debug("Not INSERTION Not INSERTION ****")
                                          }
                                      }
                                    if isPresent != true
                                    {
                                        uploadSingleRecordInDB(record: value)
                                    }
                                }
                                self.callReceived()
                            } else {
                                uploadDB()
                            }
                             PTLoader.shared.hide()
                        } else {
                            PTLoader.shared.hide()
                        }
                    } else {
                         PTLoader.shared.hide()
                    }
                    //had a word with kirti about this reponse code and thats the reason implemented because when there are no records from server we get 711 as response
                  case "711":
                    //in this case the user doesnt have new records
                    PTLoader.shared.hide()
                default:
                    //PTLoader.shared.hide()
                    reportModelDelegate?.handleCustomError(error: CustomError.errorResponse)
                }
            } else {
              //  PTLoader.shared.hide()
                reportModelDelegate?.handleCustomError(error: CustomError.noResponse)
            }
        } catch {
            //PTLoader.shared.hide()
            reportModelDelegate?.handleCustomError(error: CustomError.xmlSerializeFailed)
        }
    }
    
    func handleSendMoneyToBankResponse(data: AnyObject) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let result = try decoder.decode(SendMoneyToBankReportResponse.self, from: castedData)
            guard let comments = result.response else { PTLoader.shared.hide(); return }
            println_debug("\n\nRESPONSE: \(result)")
            guard let commentData = comments.data(using: .utf8) else { PTLoader.shared.hide(); return }
            let smtbReport = try decoder.decode([SendMoneyToBankReport].self, from: commentData)
            if smtbReport.count > 0 {
                ReportCoreDataManager.sharedInstance.insertReportSendMoneyToBankInDB(datasToInsert: smtbReport) {
                    resultInt in
                    if resultInt == 1 {
                        if var list = ReportCoreDataManager.sharedInstance.fetchSendMoneyToBankReportRecords() {
                            list = list.sorted {
                                guard let date1 = $0.transactionDateTime, let date2 = $1.transactionDateTime else { return false }
                                return date1.compare(date2) == .orderedDescending
                            }
                            sendMoneyBankInitialReports = list
                        }
                    }
                    DispatchQueue.main.async {
                        PTLoader.shared.hide()
                        self.reportModelDelegate?.apiResult(message: "", statusCode: nil)
                    }
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch _ {
            reportModelDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
            PTLoader.shared.hide()
        }
    }
    
    func handleResaleMainBalanceResponse(data: AnyObject) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            var resaleReportList = [ResaleMainBalanceReport]()
            resaleReportList = try decoder.decode([ResaleMainBalanceReport].self, from: castedData)
            if resaleReportList.count > 0 {
                DispatchQueue.main.async {
                    ReportCoreDataManager.sharedInstance.insertReportResaleMainBalanceInDB(datasToInsert: resaleReportList) { resultInt in
                        if resultInt == 1 {
                            if var list = ReportCoreDataManager.sharedInstance.fetchResaleMainBalanceRecords() {
                                list = list.sorted {
                                    guard let date1 = $0.createDate, let date2 = $1.createDate else { return false }
                                    return date1.compare(date2) == .orderedDescending
                                }
                                self.resaleMainBalanceReports = list
                                println_debug(self.resaleMainBalanceReports)
                            }
                        }
                        PTLoader.shared.hide()
                        self.reportModelDelegate?.apiResult(message: "", statusCode: nil)
                    }
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch let error {
            reportModelDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
            println_debug(error.localizedDescription)
            PTLoader.shared.hide()
        }
    }
    
    func handleAddWithdrawResponse(data: AnyObject) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            var addWithdrawList = [AddWithdrawData]()
            addWithdrawList = try decoder.decode([AddWithdrawData].self, from: castedData)
            if addWithdrawList.count > 0 {
                DispatchQueue.main.async {
                    self.addWithdrawReports = addWithdrawList
                    println_debug(self.addWithdrawReports)
                    PTLoader.shared.hide()
                    self.reportModelDelegate?.apiResult(message: "", statusCode: nil)
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch let error {
            reportModelDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
            println_debug(error.localizedDescription)
            PTLoader.shared.hide()
        }
    }
    
    func getCashInCashOutReport(request: ReportCICORequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            
            let urlStrSuffix   = Url.reportCICOList
            let cashInCashOutUrl = getUrl(urlStr: urlStrSuffix, serverType: .cashInCashOutUrl)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(cashInCashOutUrl)")
            
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: cashInCashOutUrl, param: params , httpMethod: "POST", mScreen: ReportApiType.cico.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            reportModelDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    func handleCICOResponse(data: AnyObject) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let result = try decoder.decode(ReportCICOResponse.self, from: castedData)
            guard let cicoList = result.content else { PTLoader.shared.hide(); return }
            println_debug("\n\nRESPONSE: \(cicoList)")
            if cicoList.count > 0 {
                cicoReports = cicoList
                    DispatchQueue.main.async {
                        PTLoader.shared.hide()
                        self.reportModelDelegate?.apiResult(message: "", statusCode: nil)
                    }
                }
            }
        catch _ {
            reportModelDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
            PTLoader.shared.hide()
        }
    }
    
    func handleLoyaltyResponse(data: AnyObject) {
        let responseString = String(data: data as! Data, encoding: .utf8)
        let xmlIndexer = SWXMLHash.parse(responseString!)
        do {
            let loyaltyReportResponse = try GetLoyaltyReportResponse.deserialize(xmlIndexer)
            if let records = loyaltyReportResponse.estel?.response?.records?.record {
                if records.count > 0 {
                    loyaltyRecords = records
                    reportModelDelegate?.apiResult(message: loyaltyReportResponse.estel?.response?.resultDescription ?? "", statusCode: loyaltyReportResponse.estel?.response?.resultCode)
                }
            } else {
                reportModelDelegate?.handleCustomError(error: CustomError.noResponse)
            }
        } catch {
            reportModelDelegate?.handleCustomError(error: CustomError.xmlSerializeFailed)
        }
        PTLoader.shared.hide()
    }
}

extension ReportModel: WebServiceResponseDelegate {
    func webResponse(withJson json: AnyObject, screen: String) {
        guard let screen = ReportApiType(rawValue: screen) else { return }
        switch screen {
        case .allTransaction:
            handleTransactionResponse(data: json)
        case .sendMoneyToBank:
            handleSendMoneyToBankResponse(data: json)
        case .resaleMainBalance:
            handleResaleMainBalanceResponse(data: json)
        case .addWithdraw:
            handleAddWithdrawResponse(data: json)
        case .cico:
            handleCICOResponse(data: json)
        case .loyaltyReport:
            handleLoyaltyResponse(data: json)
        }
    }
    
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        PTLoader.shared.hide()
        switch withErrorCode {
        case 404:
            guard let screen = ReportApiType(rawValue: screenName) else { return }
            switch screen {
            case .resaleMainBalance:
                self.resaleMainBalanceReports = []
                self.reportModelDelegate?.apiResult(message: "", statusCode:  nil)
            case .addWithdraw:
                self.addWithdrawReports = []
                self.reportModelDelegate?.apiResult(message: "", statusCode: nil)
            default:
                reportModelDelegate?.handleCustomError(error: .serverNotReachable)
            }
        default:
            println_debug("Error \(withErrorCode)")
            break
        }
    }
}


extension ReportModel: SendBackCallerToAllTransaction {
    func callReceived(){
        PTLoader.shared.hide()
        self.reportModelDelegate?.apiResult(message: "", statusCode: "")
    }
}
