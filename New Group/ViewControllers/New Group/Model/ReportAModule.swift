//
//  ReportAModule.swift
//  OK
//
//  Created by E J ANTONY on 18/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation

class ReportAModel {
    // MARK: - Properties
    weak var reportAModelDelegate: ReportModelDelegate?
    private enum ReportApiType: String {
        case votingList = "Report Voting List", solarElectricity = "Report Solar Electricity", offerList = "Report Offer List", offerListFilter = "Report Offer List Filter", offerSummaryFilter = "Report Offer Summary Filter"
        case locationList = "Location List", advMerchantList = "Advance Merchant List", dummyMerchantList = "Dummy Merchant List"
    }
    var votingInitialReports = [VotingReportList]()
    var solarInitialReports = [SolarReport]()
    var offerInitialReports = [OfferReportList]()
    var offerInitialFilterReports = [OfferReporFilterList]()
    var offerInitialLocation = [OfferLocationList]()
    var initialAdvanceMerchantList = [OfferAdvanceMerchantList]()
    var initialDummyMerchantList = [OfferDummyMerchantList]()
    var offerReportFilterSummary: OfferReportFilterSummary?
    
    func getVotingReportList(authToken: String) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let getVotingReportUrl = getUrl(urlStr: UserModel.shared.mobileNo, serverType: .votingReportUrl)
            let params = [String: String]()
            println_debug("REQUEST URL: \(getVotingReportUrl)")
            web.genericClass(url: getVotingReportUrl, param: params as AnyObject, httpMethod: "GET", mScreen: ReportApiType.votingList.rawValue, authToken: authToken)
        } else {
            reportAModelDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    func getSolarElectricity(authToken: String) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = String.init(format: Url.getSolarElectricityReport,UserModel.shared.mobileNo)
            let getSolareReportUrl = getUrl(urlStr: urlStrSuffix, serverType: .solarReportUrl)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(getSolareReportUrl)")
            web.genericClass(url: getSolareReportUrl, param: params, httpMethod: "GET", mScreen: ReportApiType.solarElectricity.rawValue, authToken: authToken)
        } else {
            reportAModelDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    func getOfferListCompanyForReport(request: OfferReporListCompanyRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.getOfferReport
            let getOfferReportUrl = getUrl(urlStr: urlStrSuffix, serverType: .serverApp)
            let params = [String: String]() as AnyObject
            
            println_debug("Params: \(request)")
            println_debug("REQUEST URL: \(getOfferReportUrl)")
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: getOfferReportUrl, param: params , httpMethod: "POST", mScreen: ReportApiType.offerList.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            reportAModelDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    func getOfferListDateForReport(request: OfferReporListDateRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.getOfferReport
            let getOfferReportUrl = getUrl(urlStr: urlStrSuffix, serverType: .serverApp)
            let params = [String: String]() as AnyObject
        
            println_debug("Params: \(request)")
            println_debug("REQUEST URL: \(getOfferReportUrl)")
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: getOfferReportUrl, param: params , httpMethod: "POST", mScreen: ReportApiType.offerList.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            reportAModelDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    func getOfferListForReport(request: OfferReporListRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.getOfferReport
            let getOfferReportUrl = getUrl(urlStr: urlStrSuffix, serverType: .serverApp)
            let params = [String: String]() as AnyObject
            //{"MobileNumber":"00959894485053","OffSet":"0","Limit":"20"}
          
            println_debug("REQUEST URL: \(getOfferReportUrl)")
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: getOfferReportUrl, param: params , httpMethod: "POST", mScreen: ReportApiType.offerList.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            reportAModelDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    func getOfferSummaryFilterRequest(request: OfferSummaryFilterRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            print("request summary----\(request)")
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.getOfferSummaryReport
            let getOfferFilterReportUrl = getUrl(urlStr: urlStrSuffix, serverType: .serverApp)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(getOfferFilterReportUrl)")
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: getOfferFilterReportUrl, param: params , httpMethod: "POST", mScreen: ReportApiType.offerSummaryFilter.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            reportAModelDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    func getOfferResultFilterRequest(request: OfferResultFilterRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.getOfferFilterReport
            let getOfferFilterReportUrl = getUrl(urlStr: urlStrSuffix, serverType: .serverApp)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(getOfferFilterReportUrl)")
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: getOfferFilterReportUrl, param: params , httpMethod: "POST", mScreen: ReportApiType.offerListFilter.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            reportAModelDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    func getOfferListFilterForOneRequest(request: OfferAllTransactionRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.getOfferFilterReport
            let getOfferFilterReportUrl = getUrl(urlStr: urlStrSuffix, serverType: .serverApp)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(getOfferFilterReportUrl)")
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: getOfferFilterReportUrl, param: params , httpMethod: "POST", mScreen: ReportApiType.offerListFilter.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            reportAModelDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    func getOfferListFilterWithDate(request: OfferListOneRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.getOfferFilterReport
            let getOfferFilterReportUrl = getUrl(urlStr: urlStrSuffix, serverType: .serverApp)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(getOfferFilterReportUrl)")
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: getOfferFilterReportUrl, param: params , httpMethod: "POST", mScreen: ReportApiType.offerListFilter.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            reportAModelDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    func getOfferListFilterForReport(request: OfferListFilterRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.getOfferFilterReport
            let getOfferFilterReportUrl = getUrl(urlStr: urlStrSuffix, serverType: .serverApp)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(getOfferFilterReportUrl)")
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: getOfferFilterReportUrl, param: params , httpMethod: "POST", mScreen: ReportApiType.offerListFilter.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            reportAModelDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    func getOfferListLocation(request: OfferListFilterRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.getOfferListLocation
            let getOfferFilterReportUrl = getUrl(urlStr: urlStrSuffix, serverType: .serverApp)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(getOfferFilterReportUrl)")
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: getOfferFilterReportUrl, param: params , httpMethod: "POST", mScreen: ReportApiType.locationList.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            reportAModelDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    func getOfferAdvanceMerchant(request: OfferListFilterRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.getOfferAdvanceMerchantList
            let getOfferFilterReportUrl = getUrl(urlStr: urlStrSuffix, serverType: .serverApp)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(getOfferFilterReportUrl)")
            print("offeradvance---\(request)")
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: getOfferFilterReportUrl, param: params , httpMethod: "POST", mScreen: ReportApiType.advMerchantList.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            reportAModelDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    func getOfferDummyMerchant(request: OfferListFilterRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.getOfferDummyMerchantList
            let getOfferFilterReportUrl = getUrl(urlStr: urlStrSuffix, serverType: .serverApp)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(getOfferFilterReportUrl)")
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: getOfferFilterReportUrl, param: params , httpMethod: "POST", mScreen: ReportApiType.dummyMerchantList.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            reportAModelDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    func handleVotingReportResponse(data: AnyObject) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let votingReports = try decoder.decode(VotingReportResponseDTO.self, from: castedData)
            if let resultList = votingReports.results, resultList.count > 0 {
                let reportList = resultList.sorted {
                    guard let date1 = $0.createdDate, let date2 = $1.createdDate else { return false }
                    return date1.compare(date2) == .orderedDescending
                }
                votingInitialReports = reportList
                DispatchQueue.main.async {
                    PTLoader.shared.hide()
                    self.reportAModelDelegate?.apiResult(message: "", statusCode: nil)
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch _ {
            reportAModelDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
            PTLoader.shared.hide()
        }
    }
    
    func handleSolarReportResponse(data: Any) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let solarResponse = try decoder.decode(SolarReportResponseDTO.self, from: castedData)
            if let resultList = solarResponse.results, resultList.count > 0 {
                let reportList = resultList.sorted {
                    guard let date1 = $0.createdDate, let date2 = $1.createdDate else { return false }
                    return date1.compare(date2) == .orderedDescending
                }
                solarInitialReports = reportList
                DispatchQueue.main.async {
                    PTLoader.shared.hide()
                    self.reportAModelDelegate?.apiResult(message: "", statusCode: nil)
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch _ {
            reportAModelDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
            PTLoader.shared.hide()
        }
    }
    
    func handleOfferListResponse(data: Any) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let offerListResponse = try decoder.decode(OfferReportResponseDTO.self, from: castedData)
            if let resultListStr = offerListResponse.data {
                println_debug(resultListStr)
                if resultListStr == "null" {
                    offerInitialReports = []
                } else {
                    guard let resultListData = resultListStr.data(using: .utf8) else { return }
                    let offerList = try decoder.decode([OfferReportList].self, from: resultListData)
                    offerInitialReports = offerList
                }
                DispatchQueue.main.async {
                    PTLoader.shared.hide()
                    self.reportAModelDelegate?.apiResult(message: "", statusCode: nil)
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch _ {
            reportAModelDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
            PTLoader.shared.hide()
        }
    }
    
    func handleOfferListFilterResponse(data: Any) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let offerListResponse = try decoder.decode(OfferReportResponseDTO.self, from: castedData)
            if let resultListStr = offerListResponse.data {
                println_debug(resultListStr)
                if resultListStr == "null" {
                    offerInitialFilterReports = []
                } else {
                    guard let resultListData = resultListStr.data(using: .utf8) else { return }
                    let offerFilterList = try decoder.decode([OfferReporFilterList].self, from: resultListData)
                    offerInitialFilterReports = offerFilterList
                }
                DispatchQueue.main.async {
                    PTLoader.shared.hide()
                    self.reportAModelDelegate?.apiResult(message: "", statusCode: nil)
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch _ {
            reportAModelDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
            PTLoader.shared.hide()
        }
    }
    
    func handleOfferSummaryFilterResponse(data: Any) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let offerListResponse = try decoder.decode(OfferReportResponseDTO.self, from: castedData)
            if let resultListStr = offerListResponse.data {
                println_debug(resultListStr)
                if resultListStr == "null" {
                    //offerReportFilterSummary = nil
                } else {
                    guard let resultListData = resultListStr.data(using: .utf8) else { return }
                    let offerFilterSummary = try decoder.decode(OfferReportFilterSummary.self, from: resultListData)
                    offerReportFilterSummary = offerFilterSummary
                }
                DispatchQueue.main.async {
                    PTLoader.shared.hide()
                    self.reportAModelDelegate?.apiResult(message: "", statusCode: nil)
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch _ {
            reportAModelDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
            PTLoader.shared.hide()
        }
    }
    
    func handleOfferListLocationResponse(data: Any) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let offerListResponse = try decoder.decode(OfferReportResponseDTO.self, from: castedData)
            if let resultListStr = offerListResponse.data {
                println_debug(resultListStr)
                guard let resultListData = resultListStr.data(using: .utf8) else { return }
                let offerLocationList = try decoder.decode([OfferLocationList].self, from: resultListData)
                offerInitialLocation = offerLocationList
                DispatchQueue.main.async {
                    PTLoader.shared.hide()
                    self.reportAModelDelegate?.apiResult(message: "", statusCode: nil)
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch _ {
            reportAModelDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
            PTLoader.shared.hide()
        }
    }
    
    func handleAdvanceMerchantListResponse(data: Any) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let offerListResponse = try decoder.decode(OfferReportResponseDTO.self, from: castedData)
            if let resultListStr = offerListResponse.data {
                println_debug(resultListStr)
                guard let resultListData = resultListStr.data(using: .utf8) else { return }
                let offerAdvanceMerchantList = try decoder.decode([OfferAdvanceMerchantList].self, from: resultListData)
                initialAdvanceMerchantList = offerAdvanceMerchantList
                DispatchQueue.main.async {
                    PTLoader.shared.hide()
                    self.reportAModelDelegate?.apiResult(message: "", statusCode: nil)
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch _ {
            reportAModelDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
            PTLoader.shared.hide()
        }
    }
    
    func handleDummyMerchantListResponse(data: Any) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let offerListResponse = try decoder.decode(OfferReportResponseDTO.self, from: castedData)
            if let resultListStr = offerListResponse.data {
                println_debug(resultListStr)
                guard let resultListData = resultListStr.data(using: .utf8) else { return }
                let offerDummyMerchantList = try decoder.decode([OfferDummyMerchantList].self, from: resultListData)
                initialDummyMerchantList = offerDummyMerchantList
                DispatchQueue.main.async {
                    PTLoader.shared.hide()
                    self.reportAModelDelegate?.apiResult(message: "", statusCode: nil)
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch _ {
            reportAModelDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
            PTLoader.shared.hide()
        }
    }
}

extension ReportAModel: WebServiceResponseDelegate {
    func webResponse(withJson json: AnyObject, screen: String) {
        guard let screen = ReportApiType(rawValue: screen) else { return }
        switch screen {
        case .votingList:
            handleVotingReportResponse(data: json)
        case .solarElectricity:
            handleSolarReportResponse(data: json)
        case .offerList:
            handleOfferListResponse(data: json)
        case .offerListFilter:
            handleOfferListFilterResponse(data: json)
        case .offerSummaryFilter:
            handleOfferSummaryFilterResponse(data: json)
        case .locationList:
            handleOfferListLocationResponse(data: json)
        case .advMerchantList:
            handleAdvanceMerchantListResponse(data: json)
        case .dummyMerchantList:
            handleDummyMerchantListResponse(data: json)
        }
    }
    
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        PTLoader.shared.hide()
        switch withErrorCode {
        case 404:
            guard let screen = ReportApiType(rawValue: screenName) else { return }
            switch screen {
            case .votingList:
                self.votingInitialReports = []
                self.reportAModelDelegate?.apiResult(message: "", statusCode: nil)
            case .solarElectricity:
                self.solarInitialReports = []
                self.reportAModelDelegate?.apiResult(message: "", statusCode: nil)
            case .offerList:
                self.offerInitialReports = []
                self.reportAModelDelegate?.apiResult(message: "", statusCode: nil)
            case .offerListFilter:
                self.offerInitialFilterReports = []
                self.reportAModelDelegate?.apiResult(message: "", statusCode: nil)
            case .offerSummaryFilter:
                self.offerInitialFilterReports = []
                self.reportAModelDelegate?.apiResult(message: "", statusCode: nil)
            case .locationList:
                self.offerInitialLocation = []
                self.reportAModelDelegate?.apiResult(message: "", statusCode: nil)
            case .advMerchantList:
                self.initialAdvanceMerchantList = []
                self.reportAModelDelegate?.apiResult(message: "", statusCode: nil)
            case .dummyMerchantList:
                self.initialDummyMerchantList = []
                self.reportAModelDelegate?.apiResult(message: "", statusCode: nil)
            }
        default:
            println_debug("Error \(withErrorCode)")
            break
        }
    }
}
