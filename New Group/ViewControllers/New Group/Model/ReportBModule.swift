//
//  ReportBModule.swift
//  OK
//
//  Created by iMac on 3/30/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation

/// This protocol is to implement the api response action
protocol ReportBModelDelegate: class {
    func apiResult(message: [NonOfferReportList], statusCode: String?)
    func handleCustomError(error: CustomError)
}

protocol ReportBModelTransactionsummaryDelegate: class {
 func apiResultnonofferSummary(message: [String:Any], statusCode: String?)
    func handleCustomError(error: CustomError)

}

protocol ReportBModelAllTransaction: class {
    func apiResultnonofferSummary(message: String?, statusCode: String?)
    func handleCustomError(error: CustomError)
    
}

class ReportBModel {

    // MARK: - Properties
    weak var reportBModelDelegate: ReportBModelDelegate?
    weak var reportBsummaryDelegate: ReportBModelTransactionsummaryDelegate?
    weak var reportBAllTransactionDelegate: ReportBModelAllTransaction?

    private enum ReportApiType: String {
        case offerList = "Report NonOffer List", offerListFilter = "Report NonOffer List Filter", NonofferSummaryList = "NonOffer Summary" , NonofferAllTransaction = "NonOffer AllTransaction" , NonofferAllLocation = "NonOffer AllLocation" , NonofferAdvanceMerchant = "NonofferAdvanceMerchant" , NonofferDummyMerchant = "NonOfferDummyMerchant" , NonofferAmountDetail = "NonofferAmountDetail"
        case locationList = "Location List", advMerchantList = "Advance Merchant List", dummyMerchantList = "Dummy Merchant List"
    }
    
    var offerInitialReports = [NonOfferReportList]()
    var offerInitialFilterReports = [OfferReporFilterList]()
    var offerInitialLocation = [OfferLocationList]()
    var initialAdvanceMerchantList = [OfferAdvanceMerchantList]()
    var initialDummyMerchantList = [OfferDummyMerchantList]()
    var nonoffersummaryList = NonOfferTransactionSummaryModel.self
    var nonofferAllTransactionList = [NonOfferAllTransactionModel]()
    var nonofferAllLocationList = [NonOfferAllLocationModel]()
    var nonofferAdancemerchantList = [NonOfferAdvanceMerchantModel]()
    var nonofferDummymerchantList = [NonOfferDummyMerchantModel]()
    var nonofferAmountDetailList = [NonOfferAdvanceMerchantModel]()



    
    //MARK:- NonOffer Summary API Request
    func getNonOfferSummaryByDateType(request: NonOfferSummaryRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self as WebServiceResponseDelegate
            let urlStrSuffix   = Url.getNonOfferReport
            let getOfferReportUrl = getUrl(urlStr: urlStrSuffix, serverType: .serverApp)
            let params = [String: String]() as AnyObject
            
            println_debug("Params: \(request)")
            println_debug("REQUEST URL: \(getOfferReportUrl)")
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: getOfferReportUrl, param: params , httpMethod: "POST", mScreen: ReportApiType.offerList.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            reportBModelDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    //MARK:- NonOffer Main Report API Request
    func getNonOfferListDateForReport(request: NonOfferReporListDateRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self as WebServiceResponseDelegate
            let urlStrSuffix   = Url.getNonOfferReport
            let getOfferReportUrl = getUrl(urlStr: urlStrSuffix, serverType: .serverApp)
            let params = [String: String]() as AnyObject
            
            println_debug("Params: \(request)")
            println_debug("REQUEST URL: \(getOfferReportUrl)")
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: getOfferReportUrl, param: params , httpMethod: "POST", mScreen: ReportApiType.offerList.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            reportBModelDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    //MARK:- Prabu NonOffer  API Request
    //Transaction Summary Request
    func getNonOfferPromotionTransactionSummaryAPICalling(request: NonOfferReporListDateRequestNew) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self as WebServiceResponseDelegate
            let urlStrSuffix   = Url.getSafetyCashierTransctionSummary
            let getOfferReportUrl = getUrl(urlStr: urlStrSuffix, serverType: .serverApp)
            let params = [String: String]() as AnyObject
            
            println_debug("Params: \(request)")
            println_debug("REQUEST URL: \(getOfferReportUrl)")
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: getOfferReportUrl, param: params , httpMethod: "POST", mScreen: ReportApiType.NonofferSummaryList.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            reportBsummaryDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    //AllTransaction Request
    func getNonOfferPromotionAllTransactionAPICalling(request: NonOfferAllTransactionRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self as WebServiceResponseDelegate
            let urlStrSuffix   = Url.getNonOfferPromotionAllTransactionRequestedType
            let getOfferReportUrl = getUrl(urlStr: urlStrSuffix, serverType: .serverApp)
            let params = [String: String]() as AnyObject
            
            println_debug("Params: \(request)")
            println_debug("REQUEST URL: \(getOfferReportUrl)")
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: getOfferReportUrl, param: params , httpMethod: "POST", mScreen: ReportApiType.NonofferAllTransaction.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            reportBAllTransactionDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }

    //AllLocation Request
    func getNonOfferPromotionAllLocationAPICalling(request: NonOfferAllTransactionRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self as WebServiceResponseDelegate
            let urlStrSuffix   = Url.getNonOfferPromotionLocation
            let getOfferReportUrl = getUrl(urlStr: urlStrSuffix, serverType: .serverApp)
            let params = [String: String]() as AnyObject
            
            println_debug("Params: \(request)")
            println_debug("REQUEST URL: \(getOfferReportUrl)")
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: getOfferReportUrl, param: params , httpMethod: "POST", mScreen: ReportApiType.NonofferAllLocation.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            reportBAllTransactionDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    //NonOffer Advancemerchant
    func getNonOfferAdavanceMerchantAPICalling(request: NonOfferAdvanceMerchantRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self as WebServiceResponseDelegate
            let urlStrSuffix   = Url.getNonOfferPromotionAdvanceMerchant
            let getOfferReportUrl = getUrl(urlStr: urlStrSuffix, serverType: .serverApp)
            let params = [String: String]() as AnyObject
            
            println_debug("Params: \(request)")
            println_debug("REQUEST URL: \(getOfferReportUrl)")
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: getOfferReportUrl, param: params , httpMethod: "POST", mScreen: ReportApiType.NonofferAdvanceMerchant.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            reportBAllTransactionDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    //NonOffer Dummy merchant
    func getNonOfferDummyMerchantAPICalling(request: NonOfferDummyMerchantRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self as WebServiceResponseDelegate
            let urlStrSuffix   = Url.getNonOfferPromotionDummyMerchant
            let getOfferReportUrl = getUrl(urlStr: urlStrSuffix, serverType: .serverApp)
            let params = [String: String]() as AnyObject
            
            println_debug("Params: \(request)")
            println_debug("REQUEST URL: \(getOfferReportUrl)")
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: getOfferReportUrl, param: params , httpMethod: "POST", mScreen: ReportApiType.NonofferDummyMerchant.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            reportBAllTransactionDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    //NonOffer AmountDetail
    func getNonOfferAmountDetailAPICalling(request: NonOfferReporListAmountRequest , Str : String) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self as WebServiceResponseDelegate
            
            var urlStrSuffix : String = ""
            
            if Str == "totalAvailableAmount" {
                 urlStrSuffix   = Url.getSafetyCashierTransctionByTotalAvailableAmount
            }
            else if Str == "totalReceivedAmount" {
                urlStrSuffix   = Url.getSafetyCashierTransctionByTotalReceivedAmount
            }
            else if Str == "totalDiscountAmount" {
                urlStrSuffix   = Url.getSafetyCashierTransctionByTotalDiscountAmount
            }
            
            let getOfferReportUrl = getUrl(urlStr: urlStrSuffix, serverType: .serverApp)
            let params = [String: String]() as AnyObject
            
            println_debug("Params: \(request)")
            println_debug("REQUEST URL: \(getOfferReportUrl)")
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: getOfferReportUrl, param: params , httpMethod: "POST", mScreen: ReportApiType.NonofferAmountDetail.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            reportBAllTransactionDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    //MARK:- NonOffer  API Response
    func handleOfferListResponse(data: Any) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let offerListResponse = try decoder.decode(OfferReportResponseDTO.self, from: castedData)
            if let resultListStr = offerListResponse.data {
                println_debug(resultListStr)
                if resultListStr == "null" {
                    offerInitialReports = []
                } else {
                    
                    let jsonResponse = try? JSONSerialization.jsonObject(with: castedData, options:.allowFragments)
                    if let jsonResponse = jsonResponse as? [String: AnyObject] {
                        if jsonResponse["Msg"] as! String == "Success" {
                            if let dicData = jsonResponse["Data"] as? String {
                                let arrData = OKBaseController.convertToArrDictionary(text: dicData)
                                
                                if arrData == nil { return }
                                
                                offerInitialReports.removeAll()
                                
                                for element in arrData! {
                                    
                                    println_debug(element)
                                    let notificationModel =  NonOfferReportList()
                                
                                    notificationModel.townShipName = element["townShipName"] as? String ?? ""
                                    notificationModel.advanceMerchantNumber = element["advanceMerchantNumber"] as? String ?? ""
                                    notificationModel.dummyMerchentNumber = element["dummyMerchentNumber"] as? String ?? ""
                                    notificationModel.source = element["source"] as? String ?? ""
                                    notificationModel.destination = element["destination"] as? String ?? ""
                                    notificationModel.amount = Int(element["amount"] as? Double ?? 0)
                                    
                                    if let strAmount = element["balance"] {
                                        notificationModel.balance = Int(strAmount as? Double ?? 0)
                                    }
                                    if let strAmount = element["destinationNumberWalletBalance"] {
                                        notificationModel.walletBalance = Int(strAmount as? Double ?? 0)
                                    }
                                   
                                    notificationModel.transID = element["transactionId"] as? String ?? ""
                                    
                                    let dateStr = element["transactionTime"] as? String ?? ""
                                    if let dateVal = dateStr.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss") {
                                        let transDate = dateVal.stringValue(dateFormatIs: "dd-MMM-yyyy HH:mm:ss")
                                        notificationModel.transactionDate = transDate
                                    }
                                    
                                    notificationModel.rawDesc = element["comments"] as? String ?? ""
                                    notificationModel.desc = element["comments"] as? String ?? ""
                                    notificationModel.accTransType = element["accountTransactionType"] as? String ?? ""
                                    notificationModel.transType = element["transactionTypeName"] as? String ?? ""
                                    
                                    //print("Nonoffer report listTransaction id-----\(notificationModel.transID)")
                                    offerInitialReports.append(notificationModel)
                                }
                            }
                        }
                    }
                    
                }
                
                print("Nonoffer report listTransaction id-----\(offerInitialReports.count)")

                print("Nonoffer report list-----\(offerInitialReports.count)")
                DispatchQueue.main.async {
                    PTLoader.shared.hide()
                    self.reportBModelDelegate?.apiResult(message: self.offerInitialReports, statusCode: nil)
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch _ {
            reportBModelDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
            PTLoader.shared.hide()
        }
    }
    
    func handleOfferListFilterResponse(data: Any) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let offerListResponse = try decoder.decode(OfferReportResponseDTO.self, from: castedData)
            if let resultListStr = offerListResponse.data {
                println_debug(resultListStr)
                if resultListStr == "null" {
                    offerInitialFilterReports = []
                } else {
                    guard let resultListData = resultListStr.data(using: .utf8) else { return }
                    let offerFilterList = try decoder.decode([OfferReporFilterList].self, from: resultListData)
                    offerInitialFilterReports = offerFilterList
                }
                DispatchQueue.main.async {
                    PTLoader.shared.hide()
                    //self.reportBModelDelegate?.apiResult(message: "", statusCode: nil)
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch _ {
            reportBModelDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
            PTLoader.shared.hide()
        }
    }
    
    func handleOfferListLocationResponse(data: Any) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let offerListResponse = try decoder.decode(OfferReportResponseDTO.self, from: castedData)
            if let resultListStr = offerListResponse.data {
                println_debug(resultListStr)
                guard let resultListData = resultListStr.data(using: .utf8) else { return }
                let offerLocationList = try decoder.decode([OfferLocationList].self, from: resultListData)
                offerInitialLocation = offerLocationList
                DispatchQueue.main.async {
                    PTLoader.shared.hide()
                    //self.reportBModelDelegate?.apiResult(message: "", statusCode: nil)
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch _ {
            reportBModelDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
            PTLoader.shared.hide()
        }
    }
    
    func handleAdvanceMerchantListResponse(data: Any) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let offerListResponse = try decoder.decode(OfferReportResponseDTO.self, from: castedData)
            if let resultListStr = offerListResponse.data {
                println_debug(resultListStr)
                guard let resultListData = resultListStr.data(using: .utf8) else { return }
                let offerAdvanceMerchantList = try decoder.decode([OfferAdvanceMerchantList].self, from: resultListData)
                initialAdvanceMerchantList = offerAdvanceMerchantList
                DispatchQueue.main.async {
                    PTLoader.shared.hide()
                    //self.reportBModelDelegate?.apiResult(message: "", statusCode: nil)
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch _ {
            reportBModelDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
            PTLoader.shared.hide()
        }
    }
    
    func handleDummyMerchantListResponse(data: Any) {
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let offerListResponse = try decoder.decode(OfferReportResponseDTO.self, from: castedData)
            if let resultListStr = offerListResponse.data {
                println_debug(resultListStr)
                guard let resultListData = resultListStr.data(using: .utf8) else { return }
                let offerDummyMerchantList = try decoder.decode([OfferDummyMerchantList].self, from: resultListData)
                initialDummyMerchantList = offerDummyMerchantList
                DispatchQueue.main.async {
                    PTLoader.shared.hide()
                    //self.reportBModelDelegate?.apiResult(message: "", statusCode: nil)
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch _ {
            reportBModelDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
            PTLoader.shared.hide()
        }
    }
    
    //MARK: Prabu NonOfferAPIResponse
    
    //Transaction Summary API Response
    func handleNonOfferSummaryListResponse(data: Any) {
        
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let offerListResponse = try decoder.decode(OfferReportResponseDTO.self, from: castedData)
            if let resultListStr = offerListResponse.data {
                println_debug(resultListStr)
                print("nonoffersummary----\(resultListStr))")
                
                //Encode
               /* guard let resultListData = resultListStr.data(using: .utf8) else { return }
                 let offerDummyMerchantList = try decoder.decode([NonOfferSafetyCashierSummaryModel].self, from: resultListData)
                print("nonoffersummary1111----\(offerDummyMerchantList)") */

                let dic = self.convertToDictionary(text: resultListStr)
                
                //print("nonoffersummary222----\(dic))")
//                print("nonoffersummary222----\(dic?.safeValueForKey("transactionSummaryRequestModel"))")
               
                DispatchQueue.main.async {
                    PTLoader.shared.hide()
                   
                    guard dic != nil else { return }
                self.reportBsummaryDelegate?.apiResultnonofferSummary(message: dic!, statusCode: "")
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch _ {
            self.reportBsummaryDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
            PTLoader.shared.hide()
        }

        
    }
    
    
    func convertToDictionary(text: String) -> [String:Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: [])  as? [String : Any]
            } catch {
                println_debug(error.localizedDescription)
            }
        }
        return nil
    }
    
    //NonOffer AllTransaction Response
    func handleNonOfferALLTransactionResponse(data: Any) {
        
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let offerListResponse = try decoder.decode(OfferReportResponseDTO.self, from: castedData)
            if let resultListStr = offerListResponse.data {
                println_debug(resultListStr)
                print("AllTransaction----\(resultListStr))")
                
                //Encode
                 guard let resultListData = resultListStr.data(using: .utf8) else { return }
                 let offerDummyMerchantList = try decoder.decode([NonOfferAllTransactionModel].self, from: resultListData)
                nonofferAllTransactionList = offerDummyMerchantList
                print("AllTransaction1111----\(nonofferAllTransactionList)")
                
               // let allTransactionArray = self.convertToArray(text: resultListStr)
                
               // print("nonoffersummary1111----\(allTransactionArray!.count)")
              //  print("nonoffersummary222----\(allTransactionArray))")
               // print("nonoffersummary333----\((allTransactionArray![0].safeValueForKey("transactionRequestedTypeValue")))")
               
                
                DispatchQueue.main.async {
                    PTLoader.shared.hide()
                    
                    guard offerDummyMerchantList != nil else { return }
                    self.reportBAllTransactionDelegate?.apiResultnonofferSummary(message: "", statusCode: "")
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch _ {
            reportBAllTransactionDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
            PTLoader.shared.hide()
        }
        
        
    }
    
    func handleNonOfferALLLocationResponse(data: Any) {
        
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let offerListResponse = try decoder.decode(OfferReportResponseDTO.self, from: castedData)
            if let resultListStr = offerListResponse.data {
                println_debug(resultListStr)
                print("AllTransaction----\(resultListStr))")
                
                //Encode
                guard let resultListData = resultListStr.data(using: .utf8) else { return }
                let offerDummyMerchantList = try decoder.decode([NonOfferAllLocationModel].self, from: resultListData)
                nonofferAllLocationList = offerDummyMerchantList
                print("AllTransaction1111----\(nonofferAllLocationList)")
                
                
                DispatchQueue.main.async {
                    PTLoader.shared.hide()
                    
                    guard offerDummyMerchantList != nil else { return }
                    self.reportBAllTransactionDelegate?.apiResultnonofferSummary(message: "", statusCode: "")
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch _ {
            reportBAllTransactionDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
            PTLoader.shared.hide()
        }
        
        
    }
    
    func convertToArray(text: String) -> [[String:Any]]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: [])  as?  [[String : Any]]
            } catch {
                println_debug(error.localizedDescription)
            }
        }
        return nil
    }

    func handleNonOfferAdvanceMerchantResponse(data: Any) {
        
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let offerListResponse = try decoder.decode(OfferReportResponseDTO.self, from: castedData)
            if let resultListStr = offerListResponse.data {
                println_debug(resultListStr)
                print("AdvanceMerchant----\(resultListStr))")
                
                //Encode
              /*  guard let resultListData = resultListStr.data(using: .utf8) else { return }
                let offerDummyMerchantList = try decoder.decode([OfferAdvanceMerchantList].self, from: resultListData)
               // nonofferAdancemerchantList = offerDummyMerchantList
                print("AdvanceMerchant111----\(offerDummyMerchantList)") */
              //  print("AdvanceMerchant222----\(offerDummyMerchantList.advanceMerchantTotal)")
               //  print("AdvanceMerchant333----\(offerDummyMerchantList.AdvanceMerchantModelList)")

                
                DispatchQueue.main.async {
                    PTLoader.shared.hide()
                    guard resultListStr != nil else { return }
                    self.reportBAllTransactionDelegate?.apiResultnonofferSummary(message: resultListStr, statusCode: "")
                }
                
            } else {
                PTLoader.shared.hide()
            }
        } catch _ {
            reportBAllTransactionDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
            PTLoader.shared.hide()
        }
        
        
    }
    
    func handleNonOfferADummyMerchantResponse(data: Any) {

        
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let offerListResponse = try decoder.decode(OfferReportResponseDTO.self, from: castedData)
            if let resultListStr = offerListResponse.data {
                println_debug(resultListStr)
                print("Dummymerchant----\(resultListStr))")
                
                //Encode
              /*  guard let resultListData = resultListStr.data(using: .utf8) else { return }
                let offerDummyMerchantList = try decoder.decode([OfferDummyMerchantList].self, from: resultListData)
                //nonofferDummymerchantList = offerDummyMerchantList
                print("Dummymerchant1111----\(offerDummyMerchantList)") */
                
                DispatchQueue.main.async {
                    PTLoader.shared.hide()
                    
                    guard resultListStr != nil else { return }
                    self.reportBAllTransactionDelegate?.apiResultnonofferSummary(message: resultListStr, statusCode: "")
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch _ {
            reportBAllTransactionDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
            PTLoader.shared.hide()
        }
        
        
    }
    
    
    ///AmountDEtail Response
    func handleNonOfferAmountDetailResponse(data: Any) {
        
        print("handleNonOfferAmountDetailResponse----\(data))")

        
        guard let castedData = data as? Data else {
            PTLoader.shared.hide()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let offerListResponse = try decoder.decode(OfferReportResponseDTO.self, from: castedData)
            if let resultListStr = offerListResponse.data {
                println_debug(resultListStr)
                print("Dummymerchant----\(resultListStr))")
                
                //Encode
                /*  guard let resultListData = resultListStr.data(using: .utf8) else { return }
                 let offerDummyMerchantList = try decoder.decode([OfferDummyMerchantList].self, from: resultListData)
                 //nonofferDummymerchantList = offerDummyMerchantList
                 print("Dummymerchant1111----\(offerDummyMerchantList)") */
                
                DispatchQueue.main.async {
                    PTLoader.shared.hide()
                    
                    guard resultListStr != nil else { return }
                    self.reportBAllTransactionDelegate?.apiResultnonofferSummary(message: resultListStr, statusCode: "")
                }
            } else {
                PTLoader.shared.hide()
            }
        } catch _ {
            reportBAllTransactionDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
            PTLoader.shared.hide()
        }
        
        
    }

    
}




extension ReportBModel: WebServiceResponseDelegate {
    func webResponse(withJson json: AnyObject, screen: String) {
        guard let screen = ReportApiType(rawValue: screen) else { return }
        switch screen {
        case .NonofferSummaryList:
            handleNonOfferSummaryListResponse(data: json)
        case .NonofferAllTransaction:
            handleNonOfferALLTransactionResponse(data: json)
        case .NonofferAllLocation:
            handleNonOfferALLLocationResponse(data: json)
        case .NonofferAdvanceMerchant:
            handleNonOfferAdvanceMerchantResponse(data: json)
        case .NonofferDummyMerchant:
            handleNonOfferADummyMerchantResponse(data: json)
        case .NonofferAmountDetail:
            handleNonOfferAmountDetailResponse(data: json)
        case .offerList:
            handleOfferListResponse(data: json)
        case .offerListFilter:
            handleOfferListFilterResponse(data: json)
        case .locationList:
            handleOfferListLocationResponse(data: json)
        case .advMerchantList:
            handleAdvanceMerchantListResponse(data: json)
        case .dummyMerchantList:
            handleDummyMerchantListResponse(data: json)
        }
    }
    
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        PTLoader.shared.hide()
        switch withErrorCode {
        case 404:
            guard let screen = ReportApiType(rawValue: screenName) else { return }
            switch screen {
            case .NonofferSummaryList:
                self.nonoffersummaryList = NonOfferTransactionSummaryModel.self
            case .NonofferAllTransaction:
                self.nonofferAllTransactionList = []
            case   .NonofferAllLocation:
                self.nonofferAllLocationList = []
            case   .NonofferAdvanceMerchant:
                self.nonofferAdancemerchantList = []
            case   .NonofferDummyMerchant:
                self.nonofferDummymerchantList = []
            case .NonofferAmountDetail:
                self.nonofferAmountDetailList = []
            case .offerList:
                self.offerInitialReports = []
                //self.reportBModelDelegate?.apiResult(message: "", statusCode: nil)
            case .offerListFilter:
                self.offerInitialFilterReports = []
                //self.reportBModelDelegate?.apiResult(message: "", statusCode: nil)
            case .locationList:
                self.offerInitialLocation = []
                //self.reportBModelDelegate?.apiResult(message: "", statusCode: nil)
            case .advMerchantList:
                self.initialAdvanceMerchantList = []
                //self.reportBModelDelegate?.apiResult(message: "", statusCode: nil)
            case .dummyMerchantList:
                self.initialDummyMerchantList = []
                //self.reportBModelDelegate?.apiResult(message: "", statusCode: nil)
            }
        default:
            println_debug("Error \(withErrorCode)")
            break
        }
    }
}

