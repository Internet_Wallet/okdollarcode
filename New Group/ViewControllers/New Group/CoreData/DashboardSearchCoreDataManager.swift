//
//  DashboardSearchCoreDataManager.swift
//  OK
//
//  Created by ANTONY on 27/03/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation

import CoreData

class DashboardSearchCoreDataManager {
    
    static let sharedInstance = DashboardSearchCoreDataManager()
    let managedContext  =  appDelegate.persistentContainer.viewContext
    
    // MARK: - Inserting Search Key
    func insertUpdateSearchKeyCountInDB(caseString: String) {
        let fetchReq = NSFetchRequest<NSManagedObject>(entityName: "DashboardSearchCountKeys")
        do {
            guard let fetchedObjects = try managedContext.fetch(fetchReq) as? [DashboardSearchCountKeys] else { return }
            if fetchedObjects.count > 0 {
                if let foundObject = fetchedObjects.first(where: {
                    if let searchKeyName = $0.searchKeyName {
                        return searchKeyName.lowercased() == caseString.lowercased()
                    }
                    return false
                }) {
                    foundObject.visitingCount = foundObject.visitingCount + 1
                    foundObject.updateDate = Date()
                    self.saveContext()
                } else {
                    let searchKeyData = DashboardSearchCountKeys(context: managedContext)
                    searchKeyData.searchKeyName = caseString
                    searchKeyData.visitingCount = 1
                    searchKeyData.updateDate = Date()
                    self.saveContext()
                }
            } else {
                let searchKeyData = DashboardSearchCountKeys(context: managedContext)
                searchKeyData.searchKeyName = caseString
                searchKeyData.visitingCount = 1
                searchKeyData.updateDate = Date()
                self.saveContext()
            }
        } catch {
            println_debug("Inserting or updating dashboard search operation failed \(error.localizedDescription)")
        }
    }
    
    // MARK: - Saving Context
    func saveContext() {
        let managedContext  =  appDelegate.persistentContainer.viewContext
        do {
            try managedContext.save()
        } catch let error as NSError {
            println_debug("could not save \(error.localizedDescription)")
        }
    }
    
    // MARK: - Fetching Transaction Records
    func fetchRecentDashboardSearch() -> [DashboardSearchCountKeys]? {
        let fetchReq = NSFetchRequest<NSManagedObject>(entityName: "DashboardSearchCountKeys")
        do {
            guard let fetchedObjects = try managedContext.fetch(fetchReq) as? [DashboardSearchCountKeys] else { return nil }
            return fetchedObjects
        }catch {
            println_debug("Fetching operation failed \(error.localizedDescription)")
        }
        return nil
    }
}
