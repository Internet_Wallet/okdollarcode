//
//  ReportCoreDataManager.swift
//  OK
//
//  Created by ANTONY on 13/02/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation
import CoreData


protocol SendBackCallerToAllTransaction {

    func callReceived()
}

class ReportCoreDataManager {
    
    static let sharedInstance = ReportCoreDataManager()
    let managedContext  =  appDelegate.persistentContainer.viewContext
    var callerDelegate: SendBackCallerToAllTransaction?
    
    
    // MARK: - Inserting Transaction Records
    func insertTransactionInfoInDB(dataToInsert: TransEstel) {
        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: "ReportTransaction")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchReq)
        do {
            try managedContext.execute(deleteRequest)
            try managedContext.save()
        } catch {
            println_debug("There was an error")
        }
        let insertDataTo = ReportTransaction(context: managedContext)
        insertDataTo.agentCode = dataToInsert.response?.agentCode
        insertDataTo.clientType = dataToInsert.response?.clientType
        insertDataTo.comments = dataToInsert.response?.comments
        insertDataTo.lastIndex = dataToInsert.response?.lastIndex
        insertDataTo.prewalletBalance = dataToInsert.response?.prewalletBalance
        insertDataTo.recordCount = dataToInsert.response?.recordCount
        insertDataTo.responseDate = dataToInsert.response?.responseDate
        insertDataTo.responseType = dataToInsert.header?.responseType
        insertDataTo.resultDescription = dataToInsert.response?.resultDescription
        insertDataTo.secureToken = dataToInsert.response?.secureToken
        insertDataTo.source = dataToInsert.response?.source
        insertDataTo.startIndex = dataToInsert.response?.startIndex
        insertDataTo.transId = dataToInsert.response?.transId
        insertDataTo.vendorCode = dataToInsert.response?.vendorCode
        self.saveContext()
    }
    
    
    func insertSingleTransectionInfoInDB(dataSingleTransaction : RecordData ) {
        appDel.persistentContainer.performBackgroundTask { (context) in
               do {
                     if let record = dataSingleTransaction as? RecordData {
                           print(record)
                           let transactionRecord = ReportTransactionRecord(context: context)
                           transactionRecord.isFromApi = Int16(1)
                           if let strAmount = record.amount {
                               transactionRecord.amount = NSDecimalNumber(string: strAmount)
                           }
                           if let strAmount = record.walletBalance {
                               transactionRecord.walletBalance = NSDecimalNumber(string: strAmount)
                           }
                           if let strAmount = record.bonus {
                               transactionRecord.bonus = NSDecimalNumber(string: strAmount)
                           }
                           if let strAmount = record.cashBack {
                               transactionRecord.cashBack = NSDecimalNumber(string: strAmount)
                           }
                           transactionRecord.destination = record.destination
                           transactionRecord.transID = record.transactionId
                           transactionRecord.transType = record.transactionType
                           transactionRecord.operatorName = record.operatorName
                           transactionRecord.accTransType = record.accTransactionType
                           transactionRecord.refTransID = record.refTransID
                           if let responseDate = record.responseDate {
                               transactionRecord.transactionDate = responseDate.dateValue()
                           }
                           transactionRecord.desc = ""
                           transactionRecord.rawDesc = record.comments
                           transactionRecord.senderName = ""
                           if let fee = record.fee {
                               transactionRecord.fee = NSDecimalNumber(string: fee)
                           }
                           //For description value
                           if let safeComments = record.comments {
                               let destArray = safeComments.components(separatedBy: "-")
                               if safeComments.contains(find: "Overseas") {
                                   let cCodeArray = safeComments.components(separatedBy: "Overseas-")
                                   if cCodeArray.count > 1 {
                                       let countryCode = cCodeArray[1].components(separatedBy: "-")
                                       if countryCode.count > 0 {
                                           transactionRecord.countryCode = countryCode[0]
                                       }
                                   }
                                   if destArray.count > 3 {
                                       transactionRecord.destination = destArray[3]
                                   }
                               } else if safeComments.contains(find: "Top-Up") || safeComments.contains(find: "TopUp Plan") || safeComments.contains(find: "TopUpPlan") || safeComments.contains(find: "Data Plan") || safeComments.contains(find: "DataPlan") || safeComments.contains(find: "Special Offers") || safeComments.contains(find: "Special Offer") || safeComments.contains(find: "SpecialOffers"){
                                   if destArray.count > 3 {
                                       let destNumber = destArray[3]
                                       if Array(destNumber).count > 1 {
                                           if Array(destNumber)[0] == "0" && Array(destNumber)[1] != "0" {
                                               let destNo = "0095" + destNumber.dropFirst()
                                               transactionRecord.destination = destNo
                                               if destNo == UserModel.shared.mobileNo {
                                                   transactionRecord.receiverName = UserModel.shared.name
                                               }
                                           }
                                       }
                                   }
                               }
                               let commentArray = safeComments.components(separatedBy: "[")
                               if commentArray.count > 0 {
                                   var description = commentArray[0].count == 0 ? "" : "Remarks: \(commentArray[0])"
                                   if safeComments.contains(find: "#OK-PMC") {
                                       description = safeComments
                                   }
                                   transactionRecord.desc = self.getRequiredComments(commentsToChange: description.removingPercentEncoding ?? "", rec: transactionRecord)
                                   //For age value and gender
                                   if commentArray.count > 1 {
                                       let sBracketRemovedArray = commentArray[1].components(separatedBy: "]")
                                       if sBracketRemovedArray.count > 0 {
                                           let commaSeparatedArray = sBracketRemovedArray[0].components(separatedBy: ",")
                                           if safeComments.contains(find: "#OK-PMC") {
                                               if commaSeparatedArray.count > 15 {
                                                   transactionRecord.latitude = commaSeparatedArray[16]
                                                   transactionRecord.longitude = commaSeparatedArray[17]
                                                   transactionRecord.gender = "\(commaSeparatedArray[19])"
                                                   if let ageInt = Int16(commaSeparatedArray[20]) {
                                                       transactionRecord.age = ageInt
                                                   }
                                               }
                                           } else {
                                               if commaSeparatedArray.count > 4 {
                                                   transactionRecord.latitude = commaSeparatedArray[0]
                                                   transactionRecord.longitude = commaSeparatedArray[1]
                                                   transactionRecord.gender = "\(commaSeparatedArray[3])"
                                                   if let ageInt = Int16(commaSeparatedArray[4]) {
                                                       transactionRecord.age = ageInt
                                                   }
                                               }
                                           }
                                           // For business name and merchant name
                                           if safeComments.contains(find: "#OK-PMC") {
                                               if sBracketRemovedArray.count > 1 {
                                                   let nameSeparatedArray = sBracketRemovedArray[0].components(separatedBy: "##")
                                                   transactionRecord.receiverName = nameSeparatedArray.count > 1 ? nameSeparatedArray[1] : ""
                                                   transactionRecord.receiverBName = nameSeparatedArray.count > 2 ? nameSeparatedArray[2] : ""
                                                   transactionRecord.senderBName = nameSeparatedArray.count > 3 ? nameSeparatedArray[3] : ""
                                               }
                                           } else {
                                               if sBracketRemovedArray.count > 1 {
                                                   let nameSeparatedArray = sBracketRemovedArray[0].components(separatedBy: "##")
                                                   transactionRecord.receiverName = nameSeparatedArray.count > 1 ? nameSeparatedArray[1] : ""
                                                   transactionRecord.receiverBName = nameSeparatedArray.count > 2 ? nameSeparatedArray[2] : ""
                                                   transactionRecord.senderBName = nameSeparatedArray.count > 3 ? nameSeparatedArray[3] : ""
                                               }
                                           }
                                       }
                                   }
                               }
                               //For comments value
                               let snArray = safeComments.components(separatedBy: "OKNAME-")
                               if snArray.count > 1 {
                                   if snArray[1].contains("@rating@") {
                                       let snsubArray = snArray[1].components(separatedBy: "-")
                                       if snsubArray.count > 1 {
                                           if snsubArray[1].contains(find: "cashin") || snsubArray[1].contains(find: "cashout") {
                                               if snsubArray.count > 3 {
                                                   transactionRecord.rating = snsubArray[2]
                                                   transactionRecord.ratingFeedback = snsubArray[3]
                                               }
                                           } else {
                                               if snsubArray.count > 2 {
                                                   transactionRecord.rating = snsubArray[1]
                                                   transactionRecord.ratingFeedback = snsubArray[2]
                                               }
                                           }
                                       }
                                       if snsubArray.count > 0 {
                                           transactionRecord.senderName = snsubArray[0].components(separatedBy: "@rating").first ?? ""
                                       }
                                   } else {
                                       transactionRecord.senderName = snArray[1]
                                   }
                               }
                               if safeComments.contains(find: "#point-redeempoints-") {
                                   let redeemArray = safeComments.components(separatedBy: "#point-redeempoints-")
                                   if redeemArray.count > 1 {
                                       transactionRecord.desc = "Buy Bonus Point"
                                       transactionRecord.bonus = NSDecimalNumber(string: redeemArray[1])
                                   }
                               }
                           }
                       }
                   try context.save()
                   //self.callerDelegate?.callReceived()
               } catch {
                   println_debug("There was an error")
                   //self.callerDelegate?.callReceived()
               }
         }
    }
    
    func  insertAllTransactionRecords(dataForInsertion: TransEstel) {
        appDel.persistentContainer.performBackgroundTask { (context) in
            let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: "ReportTransactionRecord")
            fetchReq.predicate = NSPredicate(format: "isFromApi == %@", "1")
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchReq)
            do {
                try context.execute(deleteRequest)
                try context.save()
            } catch {
                println_debug("There was an error")
            }
            do {
                
                if let records = dataForInsertion.response?.records?.record, records.count > 0 {
                    for record in records {
                        print(record)
                        let transactionRecord = ReportTransactionRecord(context: context)
                        transactionRecord.isFromApi = Int16(1)
                        if let strAmount = record.amount {
                            transactionRecord.amount = NSDecimalNumber(string: strAmount)
                        }
                        if let strAmount = record.walletBalance {
                            transactionRecord.walletBalance = NSDecimalNumber(string: strAmount)
                        }
                        if let strAmount = record.bonus {
                            transactionRecord.bonus = NSDecimalNumber(string: strAmount)
                        }
                        if let strAmount = record.cashBack {
                            transactionRecord.cashBack = NSDecimalNumber(string: strAmount)
                        }
                        transactionRecord.destination = record.destination
                        transactionRecord.transID = record.transactionId
                        transactionRecord.transType = record.transactionType
                        transactionRecord.operatorName = record.operatorName
                        transactionRecord.accTransType = record.accTransactionType
                        transactionRecord.refTransID = record.refTransID
                        if let responseDate = record.responseDate {
                            transactionRecord.transactionDate = responseDate.dateValue()
                        }
                        transactionRecord.desc = ""
                        transactionRecord.rawDesc = record.comments
                        transactionRecord.senderName = ""
                        if let fee = record.fee {
                            transactionRecord.fee = NSDecimalNumber(string: fee)
                        }
                        //For description value
                        if let safeComments = record.comments {
                            let destArray = safeComments.components(separatedBy: "-")
                            if safeComments.contains(find: "Overseas") {
                                let cCodeArray = safeComments.components(separatedBy: "Overseas-")
                                if cCodeArray.count > 1 {
                                    let countryCode = cCodeArray[1].components(separatedBy: "-")
                                    if countryCode.count > 0 {
                                        transactionRecord.countryCode = countryCode[0]
                                    }
                                }
                                if destArray.count > 3 {
                                    transactionRecord.destination = destArray[3]
                                }
                            } else if safeComments.contains(find: "Top-Up") || safeComments.contains(find: "TopUp Plan") || safeComments.contains(find: "TopUpPlan") || safeComments.contains(find: "Data Plan") || safeComments.contains(find: "DataPlan") || safeComments.contains(find: "Special Offers") || safeComments.contains(find: "Special Offer") || safeComments.contains(find: "SpecialOffers"){
                                if destArray.count > 3 {
                                    let destNumber = destArray[3]
                                    if Array(destNumber).count > 1 {
                                        if Array(destNumber)[0] == "0" && Array(destNumber)[1] != "0" {
                                            let destNo = "0095" + destNumber.dropFirst()
                                            transactionRecord.destination = destNo
                                            if destNo == UserModel.shared.mobileNo {
                                                transactionRecord.receiverName = UserModel.shared.name
                                            }
                                        }
                                    }
                                }
                            }
                            let commentArray = safeComments.components(separatedBy: "[")
                            if commentArray.count > 0 {
                                var description = commentArray[0].count == 0 ? "" : "Remarks: \(commentArray[0])"
                                if safeComments.contains(find: "#OK-PMC") {
                                    description = safeComments
                                }
                                transactionRecord.desc = self.getRequiredComments(commentsToChange: description.removingPercentEncoding ?? "", rec: transactionRecord)
                                //For age value and gender
                                if commentArray.count > 1 {
                                    let sBracketRemovedArray = commentArray[1].components(separatedBy: "]")
                                    if sBracketRemovedArray.count > 0 {
                                        let commaSeparatedArray = sBracketRemovedArray[0].components(separatedBy: ",")
                                        if safeComments.contains(find: "#OK-PMC") {
                                            if commaSeparatedArray.count > 15 {
                                                transactionRecord.latitude = commaSeparatedArray[16]
                                                transactionRecord.longitude = commaSeparatedArray[17]
                                                transactionRecord.gender = "\(commaSeparatedArray[19])"
                                                if let ageInt = Int16(commaSeparatedArray[20]) {
                                                    transactionRecord.age = ageInt
                                                }
                                            }
                                        } else {
                                            if commaSeparatedArray.count > 4 {
                                                transactionRecord.latitude = commaSeparatedArray[0]
                                                transactionRecord.longitude = commaSeparatedArray[1]
                                                transactionRecord.gender = "\(commaSeparatedArray[3])"
                                                if let ageInt = Int16(commaSeparatedArray[4]) {
                                                    transactionRecord.age = ageInt
                                                }
                                            }
                                        }
                                        // For business name and merchant name
                                        if safeComments.contains(find: "#OK-PMC") {
                                            if sBracketRemovedArray.count > 1 {
                                                let nameSeparatedArray = sBracketRemovedArray[0].components(separatedBy: "##")
                                                transactionRecord.receiverName = nameSeparatedArray.count > 1 ? nameSeparatedArray[1] : ""
                                                transactionRecord.receiverBName = nameSeparatedArray.count > 2 ? nameSeparatedArray[2] : ""
                                                transactionRecord.senderBName = nameSeparatedArray.count > 3 ? nameSeparatedArray[3] : ""
                                            }
                                        } else {
                                            if sBracketRemovedArray.count > 1 {
                                                let nameSeparatedArray = sBracketRemovedArray[0].components(separatedBy: "##")
                                                transactionRecord.receiverName = nameSeparatedArray.count > 1 ? nameSeparatedArray[1] : ""
                                                transactionRecord.receiverBName = nameSeparatedArray.count > 2 ? nameSeparatedArray[2] : ""
                                                transactionRecord.senderBName = nameSeparatedArray.count > 3 ? nameSeparatedArray[3] : ""
                                            }
                                        }
                                    }
                                }
                            }
                            //For comments value
                            let snArray = safeComments.components(separatedBy: "OKNAME-")
                            if snArray.count > 1 {
                                if snArray[1].contains("@rating@") {
                                    let snsubArray = snArray[1].components(separatedBy: "-")
                                    if snsubArray.count > 1 {
                                        if snsubArray[1].contains(find: "cashin") || snsubArray[1].contains(find: "cashout") {
                                            if snsubArray.count > 3 {
                                                transactionRecord.rating = snsubArray[2]
                                                transactionRecord.ratingFeedback = snsubArray[3]
                                            }
                                        } else {
                                            if snsubArray.count > 2 {
                                                transactionRecord.rating = snsubArray[1]
                                                transactionRecord.ratingFeedback = snsubArray[2]
                                            }
                                        }
                                    }
                                    if snsubArray.count > 0 {
                                        transactionRecord.senderName = snsubArray[0].components(separatedBy: "@rating").first ?? ""
                                    }
                                } else {
                                    transactionRecord.senderName = snArray[1]
                                }
                            }
                            if safeComments.contains(find: "#point-redeempoints-") {
                                let redeemArray = safeComments.components(separatedBy: "#point-redeempoints-")
                                if redeemArray.count > 1 {
                                    transactionRecord.desc = "Buy Bonus Point"
                                    transactionRecord.bonus = NSDecimalNumber(string: redeemArray[1])
                                }
                            }
                        }
                    }
                }
                
                try context.save()
                self.callerDelegate?.callReceived()
            } catch {
                println_debug("There was an error")
                self.callerDelegate?.callReceived()
            }
        }
    }
    
    // MARK: - Comments for BillPayments
    func getRequiredComments(commentsToChange: String, rec: ReportTransactionRecord) -> String {
        if commentsToChange.contains(find: "#OKBILLPAYMENT") {
            let mutableComments = commentsToChange.replacingOccurrences(of: "+", with: " ")
            let arrayComment = mutableComments.components(separatedBy: "-")
            var changedComments = ""
            for (index, item) in arrayComment.enumerated() {
                if index == 0 || index == 1 || index == 2 || index == 4 || index == 6 {
                    if item.contains(find: ":") {
                        changedComments += item + " "
                    }
                }
            }
            return changedComments
        } else if commentsToChange.contains(find: "Overseas") {
            if commentsToChange.contains(find: "TopUpPlan") {
                return "Remarks: TopUp Plan International"
            } else {
                return "Remarks: Data Plan International"
            }
            //return "Remarks: TopUp Plan International"
        } else if commentsToChange.contains(find: "Top-Up") || commentsToChange.contains(find: "TopUp Plan") || commentsToChange.contains(find: "TopUpPlan") {
            return "Remarks: TopUp Plan"
        } else if commentsToChange.contains(find: "Data Plan") || commentsToChange.contains(find: "DataPlan"){
            return "Remarks: Data Plan"
        } else if commentsToChange.contains(find: "Special Offers") || commentsToChange.contains(find: "Special Offer") || commentsToChange.contains(find: "SpecialOffers"){
            return "Remarks: Special Offer Plan"
        } else if commentsToChange.contains(find: "#OK-PMC") {
            let finalComment = promocodeComments(fullComment: commentsToChange, rec: rec)
            return finalComment
        }
        return commentsToChange
    }
    
    private func promocodeComments(fullComment: String, rec: ReportTransactionRecord) -> String {
        var commentsToSend = ""
        let cArray = fullComment.components(separatedBy: "[")
        commentsToSend = cArray[0]
        if cArray.count > 1 {
            let cArray = cArray[1].components(separatedBy: ",")
            commentsToSend += " Promocode \(cArray[1])"
            if let accTransType = rec.accTransType, accTransType == "Cr" {
                commentsToSend += " Sender Account Number \(cArray[14])".replacingOccurrences(of: "0095", with: "(+95)0")
            } else {
                commentsToSend += " Receiver Account Number \(cArray[15])".replacingOccurrences(of: "0095", with: "(+95)0")
            }
            commentsToSend += " Company Name \(cArray[7])"
            let getCompProdQuanStr = getCompProdQuan(comArray: cArray)
            commentsToSend += getCompProdQuanStr
            let discountString = promoDiscountPercent(discountString: cArray)
            commentsToSend += discountString
            let discountAmount = promoDiscountAmount(discountString: cArray)
            commentsToSend += discountAmount
            if cArray.count > 5 {
                let billAmount = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: cArray[5])
                commentsToSend += " Bill Amount \(billAmount) MMK"
            }
        }
        return commentsToSend
    }
    
    private func getCompProdQuan(comArray: [String]) -> String {
        var neededStr = ""
        if comArray.count > 9 {
            if comArray[2] == "2" || comArray[2] == "3" || comArray[2] == "4" {
                neededStr += " Product Name " + comArray[11] + " Buying Quantity " + comArray[8] + " Free Quantity " + comArray[9]
                return neededStr
            }
        }
        return ""
    }
    
    private func promoDiscountPercent(discountString: [String]) -> String {
        if discountString[2] == "0" || discountString[2] == "3" {
            let amount = Float(discountString[5]) ?? 0.0
            let percentageAmount = Float(discountString[4]) ?? 0.0
            let percentageValue = Float((percentageAmount * 100) / amount)
            let displayPercentage = String.init(format: "%.0f", percentageValue)
            return " Discount Percentage " + displayPercentage + " %"
        } else if discountString[2] == "1" || discountString[2] == "4"{
            let value = Float(discountString[4]) ?? 0.0
            let displayPercentage = String.init(format: "%.0f", value)
            return " Discount Percentage " + displayPercentage + " %"
        } else {
            return ""
        }
        
    }
    
    private func promoDiscountAmount(discountString: [String]) -> String {
        if discountString[2] == "0" || discountString[2] == "3" {
            let value = Float(discountString[4]) ?? 0.0
            let displayAmount = String.init(format: "%.0f", value)
            let dispAmount = wrapAmountWithCommaDecimal(key: displayAmount)
            return " Discount Amount " + dispAmount + " MMK"
        } else if discountString[2] == "1" || discountString[2] == "4"{
            let amount = Float(discountString[5]) ?? 0.0
            let percentageAmount = Float(discountString[4]) ?? 0.0
            let percentageValue = Float((percentageAmount * amount) / 100)
            let displayAmount = String.init(format: "%.0f", percentageValue)
            let dispAmount = wrapAmountWithCommaDecimal(key: displayAmount)
            return " Discount Amount " + dispAmount + " MMK"
        } else {
            return ""
        }
    }
    
    // MARK: - Saving Context
    func saveContext() {
        DispatchQueue.main.async {
            let managedContext  =  appDelegate.persistentContainer.viewContext
            do {
                try managedContext.save()
            } catch let error as NSError {
                println_debug("could not save \(error.localizedDescription)")
            }
        }
    }
    
    // MARK: - Fetching Transaction Records
    func fetchTransactionRecords() -> [ReportTransactionRecord] {
        let fetchReq = NSFetchRequest<NSManagedObject>(entityName: "ReportTransactionRecord")
       // print(ReportTransactionRecord.transactionDate)
        let sortDescr = NSSortDescriptor(key: #keyPath(ReportTransactionRecord.transactionDate), ascending: false)
        fetchReq.sortDescriptors = [sortDescr]
        do {
            guard let fetchedObjects    = try managedContext.fetch(fetchReq) as? [ReportTransactionRecord] else { return [] }
            return fetchedObjects
        } catch {
            println_debug("Fetching operation failed \(error.localizedDescription)")
        }
        return []
    }
    
    
    
    
    
    
    // MARK: - Inserting Transaction Records
    func insertReportSendMoneyToBankInDB(datasToInsert: [SendMoneyToBankReport], completion: (_ resultInt: Int) -> Void) {
        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: "ReportSendMoneyToBank")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchReq)
        do {
            try managedContext.execute(deleteRequest)
            try managedContext.save()
        } catch {
            println_debug("Error delete in smb")
            completion(0)
        }
        
        for data in datasToInsert {
            let reportSendMoneyToBankDB = ReportSendMoneyToBank(context: managedContext)
            self.insertReportSMTBIntoDB(record: data, insertDataTo: reportSendMoneyToBankDB)
        }
        completion(1)
    }
    
    
    private func insertReportSMTBIntoDB(record: SendMoneyToBankReport, insertDataTo: ReportSendMoneyToBank) {
        var receiverName = ""
        var bankName = ""
        var branchName = ""
        var accType = ""
        if let safeComments = record.comments {
            let detailArray = safeComments.components(separatedBy: "# SEND MONEY TO BANK:")
            if detailArray.count > 0 {
                if detailArray.count > 1 {
                    let commaRemovedArray = detailArray[1].components(separatedBy: ",")
                    if commaRemovedArray.count > 4 {
                        receiverName = commaRemovedArray[3].trimmingCharacters(in: .whitespacesAndNewlines)
                        bankName = commaRemovedArray[0].trimmingCharacters(in: .whitespacesAndNewlines)
                        branchName = commaRemovedArray[1].trimmingCharacters(in: .whitespacesAndNewlines)
                        accType = commaRemovedArray[4].trimmingCharacters(in: .whitespacesAndNewlines)
                        if accountTypeValid(accountType: accType) {
                            if let accountTypeEnum = AccountTypesOfBank(rawValue: accType) {
                                accType = accountTypeEnum.stringDescription
                            }
                        } else {
                            receiverName = receiverName + "," + commaRemovedArray[4]
                            if commaRemovedArray.count > 5 {
                                accType = commaRemovedArray[5].trimmingCharacters(in: .whitespacesAndNewlines)
                                if let accountTypeEnum = AccountTypesOfBank(rawValue: accType) {
                                    accType = accountTypeEnum.stringDescription
                                }
                            }
                        }
                    }
                }
            }
        }
        insertDataTo.receiverOkAccName = receiverName
        insertDataTo.bankName = bankName
        insertDataTo.bankBranchName = branchName
        insertDataTo.bankAccType = accType
        if let doubleAmount = record.balance {
            insertDataTo.balance = NSDecimalNumber(value: doubleAmount)
        }
        if let doubleAmount = record.transferredAmount {
            insertDataTo.transferredAmount = NSDecimalNumber(value: doubleAmount)
        }
        insertDataTo.bankAccNumber = record.bankAccountNumber
        insertDataTo.comments = record.comments
        if let strDate = record.transactionDateTime, let transDate = strDate.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss") {
            insertDataTo.transactionDateTime = transDate
        }
        insertDataTo.transactionID = record.transactionId
        self.saveContext()
    }
    
    func accountTypeValid(accountType: String) -> Bool {
        return ["cda", "sav", "cur", "aca"].contains(accountType.lowercased())
    }
    
    // MARK: - Fetching Send Money To Bank Records
    func fetchSendMoneyToBankReportRecords() -> [ReportSendMoneyToBank]? {
        let fetchReq = NSFetchRequest<NSManagedObject>(entityName: "ReportSendMoneyToBank")
        do {
            guard let fetchedObjects    = try managedContext.fetch(fetchReq) as? [ReportSendMoneyToBank] else { return nil }
            if fetchedObjects.count > 0 {
                return fetchedObjects
            }
        } catch {
            println_debug("Fetching operation failed")
        }
        return nil
    }
    
    // MARK: - Inserting Report Resale Main Balance
    func insertReportResaleMainBalanceInDB(datasToInsert: [ResaleMainBalanceReport], completion: (_ resultInt: Int) -> Void) {
        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: "ReportResaleMainBalance")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchReq)
        do {
            try managedContext.execute(deleteRequest)
            try managedContext.save()
        } catch {
            println_debug("There was an error")
            completion(0)
        }
        for dataToInsert in datasToInsert {
            let reportResaleMainBalanceInfo = ReportResaleMainBalance(context: managedContext)
            self.insertReportResaleIntoDB(dataToInsert: dataToInsert, insertDataTo: reportResaleMainBalanceInfo)
        }
        completion(1)
    }
    
    private func insertReportResaleIntoDB(dataToInsert: ResaleMainBalanceReport, insertDataTo: ReportResaleMainBalance) {
        insertDataTo.productId = dataToInsert.productId
        insertDataTo.phoneNumber = dataToInsert.phoneNumber
        if let doubleAmount = dataToInsert.airtimeAmount {
            insertDataTo.airtimeAmount = NSDecimalNumber(value: doubleAmount)
        }
        insertDataTo.destinationNumber = dataToInsert.destinationNumber
        insertDataTo.status = dataToInsert.status
        insertDataTo.createDate = dataToInsert.createDate?.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SS")
        insertDataTo.customerId = dataToInsert.customerId
        insertDataTo.telcoId = dataToInsert.telcoId
        insertDataTo.telcoName = dataToInsert.telcoName
        if let doubleAmount = dataToInsert.sellingAmount {
            insertDataTo.sellingAmount = NSDecimalNumber(value: doubleAmount)
        }
        
        if let customerRecord = dataToInsert.customer {
            let customerDBRecord = ReportResaleCustomer(context: managedContext)
            
            customerDBRecord.customerId = customerRecord.customerId
            customerDBRecord.customerPhoneNumber = customerRecord.customerPhoneNumber
            customerDBRecord.gcmRegistrationId = customerRecord.gcmRegistrationId
            insertDataTo.customer = customerDBRecord
        }
        self.saveContext()
    }
    
    // MARK: - Fetching Send Money To Bank Records
    func fetchResaleMainBalanceRecords() -> [ReportResaleMainBalance]? {
        let fetchReq = NSFetchRequest<NSManagedObject>(entityName: "ReportResaleMainBalance")
        do {
            guard let fetchedObjects    = try managedContext.fetch(fetchReq) as? [ReportResaleMainBalance] else { return nil }
            if fetchedObjects.count > 0 {
                return fetchedObjects
            }
        } catch {
            println_debug("Fetching operation failed")
        }
        return nil
    }
}
