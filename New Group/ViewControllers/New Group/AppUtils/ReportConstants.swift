//
//  ReportConstants.swift
//  OK
//  This holds all the constants in report module
//  Created by ANTONY on 29/01/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation

struct ConstantsColor {
    static let navigationHeaderTransaction              = UIColor(red: 9.0/255.0, green: 90.0/255.0, blue: 210.0/255.0, alpha: 1.0)
    static let navigationHeaderTopUpRecharge            = UIColor(red: 224.0/255.0, green: 70.0/255.0, blue: 70.0/255.0, alpha: 1.0)
    static let navigationHeaderSendMoney                = UIColor(red: 21.0/255.0, green: 44.0/255.0, blue: 156.0/255.0, alpha: 1.0)
    static let navigationHeaderBillPayment              = UIColor(red: 47.0/255.0, green: 151.0/255.0, blue: 128.0/255.0, alpha: 1.0)
    static let sender                                   = UIColor(red: 34.0/255.0, green: 113.0/255.0, blue: 185.0/255.0, alpha: 1.0)
    static let receiver                                 = UIColor(red: 56.0/255.0, green: 142.0/255.0, blue: 60.0/255.0, alpha: 1.0)

    static let navigationHeaderReSale                   = UIColor(red: 49.0/255.0, green: 154.0/255.0, blue: 202.0/255.0, alpha: 1.0)
    static let navigationSurveyDetails                  = UIColor(red: 245.0/255.0, green: 197.0/255.0, blue: 45.0/255.0, alpha: 1.0)
    static let navigationCashInOut                      = UIColor(red: 245.0/255.0, green: 197.0/255.0, blue: 45.0/255.0, alpha: 1.0)
    static let navigationHeaderAddWithdraw              = UIColor(red: 25.0/255.0, green: 50.0/255.0, blue: 156.0/255.0, alpha: 1.0)
}

struct ConstantsController {
    static let reportListViewController: (nameAndID: String, screenTitle: String)                   = ("ReportListViewController", "Report")
    static let reportReceiptListViewController: (nameAndID: String, screenTitle: String)            = ("ReportReceiptListViewController", "Transaction Receipt")
    static let transactionDetailViewController: (nameAndID: String, screenTitle: String)            = ("TransactionDetailViewController", "Transaction Detail")
    static let reportTopUpViewController: (nameAndID: String, screenTitle: String)                  = ("ReportTopUpViewController", "Top-Up & Recharge")
    static let receiptDetailListViewController: (nameAndID: String, screenTitle: String)            = ("ReceiptDetailListViewController", "Receipt")
    static let reportSendMoneyToBankController: (nameAndID: String, screenTitle: String)            = ("ReportSendMoneyToBankController", "Send Money To Bank")
    static let reportBillPaymentsViewController: (nameAndID: String, screenTitle: String)           = ("ReportBillPaymentsViewController", "Bill Payments")
    static let reportOfferViewController: (nameAndID: String, screenTitle: String)                  = ("ReportOfferViewController", "Offer Report")
    static let reportNonOfferViewController: (nameAndID: String, screenTitle: String)               = ("ReportNonOfferViewController", "Advance & Safety Cashier")
    static let reportNonOfferFilterController: (nameAndID: String, screenTitle: String)             = ("ReportNonOfferFilterController", "Filter")
    static let transactionNonOfferViewController: (nameAndID: String, screenTitle: String)          = ("TransactionNonOfferReport", "Advance & Safety Cashier")
    static let reportResaleMainBalanceController: (nameAndID: String, screenTitle: String)          = ("ReportResaleMainBalanceController", "Re-Sale Main Balance")
    static let reportSurveyListController: (nameAndID: String, screenTitle: String)                 = ("ReportSurveyListController", "Merchant Survey Details")
    static let reportCashInOutViewController: (nameAndID: String, screenTitle: String)              = ("ReportCashInOutViewController", "Sent & Received Digital Money Report")
    static let reportAddWithdrawViewController: (nameAndID: String, screenTitle: String)            = ("ReportAddWithdrawViewController", "Add Money Report")
    static let reportCardlessCashViewController: (nameAndID: String, screenTitle: String)           = ("ReportCardlessCashViewController", "Cardless Cash Withdrawal Report")
    static let receivedMoneyViewController: (nameAndID: String, screenTitle: String)                = ("ReceivedMoneyViewController", "Received Money")
    static let statisticsTransactionController: (nameAndID: String, screenTitle: String)            = ("StatisticsTransactionAndBillController", "Transaction Details Statistics")
    static let statisticsBillController: (nameAndID: String, screenTitle: String)                   = ("StatisticsTransactionAndBillController", "Bill Payment Statistics")
    static let statisticsTopUpController: (nameAndID: String, screenTitle: String)                  = ("StatisticsTransactionAndBillController", "Top-Up & Recharge Statistics")
    static let statisticsReceivedMoneyController: (nameAndID: String, screenTitle: String)          = ("StatisticsTransactionAndBillController", "Received Money Statistics")
    static let searchHomeViewController: (nameAndID: String, screenTitle: String)                   = ("SearchHomeViewController", "Search")
    static let reportVotingListViewController: (nameAndID: String, screenTitle: String)             = ("ReportVotingListViewController", "Voting Report")
    static let reportSolarListViewController: (nameAndID: String, screenTitle: String)              = ("ReportSolarListViewController", "Solar Home Report")
    static let reportOfferFilterController: (nameAndID: String, screenTitle: String)                = ("ReportOfferFilterController", "Filter")
    static let reportPromotionLocationFilterController: (nameAndID: String, screenTitle: String)    = ("ReportPromotionLocationFilterController", "Filter")
    static let reportAdvanceDummyFilterController: (nameAndID: String, screenTitle: String)         = ("ReportAdvanceDummyFilterController", "Filter")
    static let reportOfferFilterResultController: (nameAndID: String, screenTitle: String)          = ("ReportOfferFilterResultController", "Offer Report")
    static let reportOfferFilterMainVC: (nameAndID: String, screenTitle: String)          = ("ReportOfferFilterMainVC", "Offer Report")
    static let reportOfferMapViewController: (nameAndID: String, screenTitle: String)               = ("ReportOfferMapViewController", "")
    static let reportDatePickerController: (nameAndID: String, screenTitle: String)                 = ("ReportDatePickerController", "")
    static let reportCompanyController: (nameAndID: String, screenTitle: String)                    = ("ReportCompanyViewController", "Transaction Type")
    static let reportSharingViewController: (nameAndID: String, screenTitle: String)                = ("ReportSharingViewController", "")
    static let reportOfferPdfScreenController: (nameAndID: String, screenTitle: String)             = ("ReportOfferPdfScreenController", "Receipt")
    static let reportLoyaltyViewController: (nameAndID: String, screenTitle: String)                = ("ReportLoyaltyViewController", "Loyalty Report")
    static let reportNonOfferAdvanceDummyFilterController: (nameAndID: String, screenTitle: String)         = ("ReportNonOfferAdvanceDummyFilterController", "Nonoffer Merchant")
    static let reportNonOfferAdvanceDummyController: (nameAndID: String, screenTitle: String)         = ("ReportNonOfferAdvanceDummyController", "Nonoffer Merchant")
     static let reportNonOfferAmountDetailController: (nameAndID: String, screenTitle: String)         = ("ReportNonOfferAmountDetailController", "Nonoffer AmountDetail")
    static let reportLoyaltyOptionsViewController: (nameAndID: String, screenTitle: String)         = ("LoyaltyReportOptionsViewController", "Bonus Point Report")
}

struct ConstantsXibCell {
    static let filterTableViewCell                  = "FilterTableViewCell"
    static let receiptHeaderView                    = "ReceiptHeaderView"
}

struct Device {
    // iDevice detection code
    static let IS_IPAD             = UIDevice.current.userInterfaceIdiom == .pad
    static let IS_IPHONE           = UIDevice.current.userInterfaceIdiom == .phone
    static let IS_RETINA           = UIScreen.main.scale >= 2.0
    
    static let SCREEN_WIDTH        = Int(UIScreen.main.bounds.size.width)
    static let SCREEN_HEIGHT       = Int(UIScreen.main.bounds.size.height)
    static let SCREEN_MAX_LENGTH   = Int( max(SCREEN_WIDTH, SCREEN_HEIGHT) )
    static let SCREEN_MIN_LENGTH   = Int( min(SCREEN_WIDTH, SCREEN_HEIGHT) )
    
    static let IS_IPHONE_4_OR_LESS = IS_IPHONE && SCREEN_MAX_LENGTH  < 568
    static let IS_IPHONE_5         = IS_IPHONE && SCREEN_MAX_LENGTH == 568
    static let IS_IPHONE_6         = IS_IPHONE && SCREEN_MAX_LENGTH == 667
    static let IS_IPHONE_6P        = IS_IPHONE && SCREEN_MAX_LENGTH == 736
    static let IS_IPHONE_X         = IS_IPHONE && SCREEN_MAX_LENGTH == 812
}
