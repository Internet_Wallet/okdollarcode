//
//  CodeSnippets.swift
//  OK
//  It contains the helper function
//  Created by ANTONY on 16/02/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation

class CodeSnippets {
    //MARK: - Properties
    private static let pdfSize = CGSize(width: 620, height: 842)
    private static let pdfMargin: CGFloat = 20
    private static let pdfLineSpacing: CGFloat = 10
    private static let pdfLineSpaceBtwnTitles: CGFloat = 15
    private static let pdfHeaderFooterMargin: CGFloat = 30
    private static let pdfCenterSeparatorWidth: CGFloat = 6
    /// It delete the file if the filename at the path already exists and create pd
    ///
    /// - Parameters:
    ///   - dictValues: Dictionary values to fetch the data to show
    ///   - pdfFileName: Default file name is "Invoice"
    /// - Returns: It returns Optional Url.
    
    class func createInvoicePDF(dictValues: [String: Any], pdfFile: String?) -> URL? {
        let pdfFileName = pdfFile ?? "OK_Dollar_Invoice"
        var currentYPosition: CGFloat = 0
        let mainViewFrameSize = CGSize(width: 390, height: 400)
        let viewMain = UIView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: mainViewFrameSize))
        //Adding logo
        if let logoImageName = dictValues["logoName"] as? String, let logoImage = UIImage(named: logoImageName) {
            let logoSize = CGSize(width: 70, height: 70)
            let xPos = (mainViewFrameSize.width - logoSize.width) / 2
            let imageViewLogo = UIImageView(frame: CGRect(origin: CGPoint(x: xPos, y: 50), size: logoSize))
            imageViewLogo.image = logoImage
            imageViewLogo.contentMode = .scaleAspectFit
            currentYPosition = imageViewLogo.frame.maxY
            viewMain.addSubview(imageViewLogo)
        }
        //Adding Invoice Title
        if let invoiceTitle = dictValues["invoiceTitle"] as? String {
            let marginPadding: CGFloat = 20
            let labelSize = CGSize(width: mainViewFrameSize.width - (marginPadding * 2), height: 30)
            let labelTitle = UILabel(frame: CGRect(origin: CGPoint(x: marginPadding, y: currentYPosition + 20), size: labelSize))
            labelTitle.adjustsFontSizeToFitWidth = true
            labelTitle.textAlignment = .center
            labelTitle.text = invoiceTitle
            if let myFont = UIFont(name: appFont, size: 17) {
                labelTitle.font = myFont
            }
            labelTitle.textColor = UIColor.blue
            if let titleColor = dictValues["invoiceTitleColor"] as? UIColor {
                labelTitle.textColor = titleColor
            }
            currentYPosition = labelTitle.frame.maxY
            viewMain.addSubview(labelTitle)
        }
        
        func heightForView(text:String , width:CGFloat) -> CGFloat {
            let label:UILabel = UILabel(frame: CGRect.init(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
            label.numberOfLines = 0
            label.lineBreakMode = NSLineBreakMode.byWordWrapping
            label.font = UIFont(name: appFont, size: 12)
            label.text = text
            label.sizeToFit()
            //print("Height : \(label.frame.height) For text : \(text)")
            return label.frame.height
        }
        
        func addRecord(title: String, attributed: NSAttributedString? = nil, value: String, yPadding: CGFloat) {
            //Title
            let marginPadding: CGFloat = 15
            let labelSize = CGSize(width: (mainViewFrameSize.width / 2) - 30, height: heightForView(text: value, width: (mainViewFrameSize.width / 2) - marginPadding))
            let labelTitle = UILabel(frame: CGRect(origin: CGPoint(x: marginPadding, y: currentYPosition + yPadding), size: labelSize))
            labelTitle.adjustsFontSizeToFitWidth = true
            labelTitle.textAlignment = .left
            labelTitle.text = title
            if let myFont = UIFont(name: appFont, size: 12) {
                labelTitle.font = myFont
            }
            labelTitle.textColor = UIColor.black
            
            let newLabel = UILabel(frame: CGRect(origin: CGPoint(x: labelTitle.frame.maxX, y: currentYPosition + yPadding), size: CGSize(width: 20, height: 20)))
            newLabel.text = ":"
            newLabel.textColor = .black
            
            //Value
            let labelValue = UILabel(frame: CGRect(origin: CGPoint(x: newLabel.frame.origin.x + 25, y: currentYPosition + yPadding), size: labelSize))
            //labelValue.adjustsFontSizeToFitWidth = false
            labelValue.lineBreakMode = NSLineBreakMode.byWordWrapping
            labelValue.numberOfLines = 0
            labelValue.textAlignment = .left
            if let atrValue = attributed {
                labelValue.attributedText = atrValue.length == 0 ? NSMutableAttributedString.init(string: ":    -") : atrValue
            } else {
              //  labelValue.text = value.count == 0 ? ":    -" : "\(value)"
                labelValue.text = value.count == 0 ? "-" : "\(value)"
            }
            if let myFont = UIFont(name: appFont, size: 12) {
                labelValue.font = myFont
            }
            labelValue.textColor = UIColor.black
            //labelValue.backgroundColor = .red
            currentYPosition = labelValue.frame.maxY
            //viewMain.backgroundColor = .yellow
            viewMain.addSubview(newLabel)
            viewMain.addSubview(labelTitle)
            viewMain.addSubview(labelValue)
        }
        
        var yPad: CGFloat = 0
        //Adding business name title and value
        if let businessName = dictValues["businessName"] as? String {
            yPad = 40
            addRecord(title: "Business Name".localized, value: businessName, yPadding: yPad)
        }
        //Adding sender account name title and value
        if let senderAccName = dictValues["senderAccName"] as? String {
            addRecord(title: "Sender Account Name".localized, value: senderAccName, yPadding: yPad == 0 ? 40 : 10)
        }
        //Adding sender account number title and value
        if let senderAccNumber = dictValues["senderAccNo"] as? String {
            addRecord(title: "Sender Account Number".localized, value: senderAccNumber, yPadding: 10)
        }
        //Adding promo receiver number
        if let promoReceiverNumber = dictValues["promoReceiverNumber"] as? String {
            addRecord(title: "Promo Receiver Number".localized, value: promoReceiverNumber, yPadding: 10)
        }
        //Adding receiver account name title and value
        if let receiverAccName = dictValues["receiverAccName"] as? String {
            addRecord(title: "Receiver Account Name".localized, value: receiverAccName, yPadding: 10)
        }
        //Adding receiver account number title and value
        if let receiverAccNumber = dictValues["receiverAccNo"] as? String {
            addRecord(title: "Receiver Account Number".localized, value: receiverAccNumber, yPadding: 10)
        }
        
        if let agent = dictValues["Payment Type"] as? String {
            if agent == "Cash In" {
                addRecord(title: "Payment Type".localized, value: "Sent Digital Money", yPadding: 10)
            } else if agent == "Cash Out" {
                addRecord(title: "Payment Type".localized, value: "Received Digital Money", yPadding: 10)
            } else {
                addRecord(title: "Payment Type".localized, value: agent, yPadding: 10)
            }
        }
        
        // extra parameter for Mobile Number field for topup // changes by Ashish
        if let receiverAccNumber = dictValues["mobileNumber"] as? String {
            addRecord(title: "Mobile Number".localized, value: receiverAccNumber, yPadding: 10)
        }
        
        //changing name of mobile Number // Ashish Added
        if let receiverAccNumber = dictValues["mobileNumberTopup"] as? String {
            addRecord(title: "Receiver Mobile Number".localized, value: receiverAccNumber, yPadding: 10)
        }
        if let operatorName = dictValues["operatorName"] as? String {
            addRecord(title: "Operator Name".localized, value: operatorName, yPadding: 10)
        }
        if let isForPromocode = dictValues["isForPromocode"] as? Bool, isForPromocode == true {
            if let promocode = dictValues["promocode"] as? String, promocode.count > 0  {
                addRecord(title: "Promo Code".localized, value: promocode, yPadding: 10)
            }
            if let cName = dictValues["companyName"] as? String, cName.count > 0  {
                addRecord(title: "Company Name".localized, value: cName, yPadding: 10)
            }
            if let productName = dictValues["productName"] as? String, productName.count > 0  {
                addRecord(title: "Product Name".localized, value: productName, yPadding: 10)
            }
            if let productName = dictValues["freeProductName"] as? String, productName.count > 0  {
                addRecord(title: "Free Product Name".localized, value: productName, yPadding: 10)
            }
            if let buyingQuantity = dictValues["buyingQuantity"] as? String, buyingQuantity.count > 0  {
                let amont = wrapAmountWithCommaDecimal(key: "\(buyingQuantity)")
                addRecord(title: "Buying Quantity".localized, value: amont, yPadding: 10)
            }
            if let freeQuantity = dictValues["freeQuantity"] as? String, freeQuantity.count > 0  {
                let amont = wrapAmountWithCommaDecimal(key: "\(freeQuantity)")
                addRecord(title: "Free Quantity".localized, value: amont, yPadding: 10)
            }
            if let discountPercent = dictValues["discountPercent"] as? String, discountPercent.count > 0 {
                addRecord(title: "Discount Percentage".localized, value: discountPercent, yPadding: 10)
            }
            if let discountAmount = dictValues["discountAmount"] as? String, discountAmount.count > 0  {
                  let amont = wrapAmountWithCommaDecimal(key: "\(discountAmount)")
                addRecord(title: "Discount Amount".localized, value: amont, yPadding: 10)
            }
            if let billAmount = dictValues["billAmount"] as? String, billAmount.count > 0 {
                addRecord(title: "Bill Amount".localized, value: billAmount, yPadding: 10)
            }
        }
        //Adding transaction id
        if let transactionID = dictValues["transactionID"] as? String {
            addRecord(title: "Transaction ID".localized, value: transactionID, yPadding: 10)
        }
        //Adding transaction type
        if let transactionType = dictValues["transactionType"] as? String {
            addRecord(title: "Transaction Type".localized, value: transactionType, yPadding: 10)
        }
        //Adding Payment Type
        if let paymentType = dictValues["paymentType"] as? String {
            if paymentType == "Cash In" {
                addRecord(title: "Payment Type".localized, value: "Cash-In", yPadding: 10)
            } else if paymentType == "Cash Out" {
                addRecord(title: "Payment Type".localized, value: "Cash-Out", yPadding: 10)
            } else {
                addRecord(title: "Payment Type".localized, value: paymentType, yPadding: 10)
            }
        }
        //Bonus Point
        if let buyBonusPoint = dictValues["Buy Bonus Point"] as? NSMutableAttributedString {
            addRecord(title: "Received Bonus Points".localized, attributed: buyBonusPoint, value: "", yPadding: 10)
        } else if let bonusPoint = dictValues["bonusPoint"] as? String {
            addRecord(title: "Received Bonus Points".localized, value: bonusPoint, yPadding: 10)
        }
        
        if let redeemPoint = dictValues["Redeem Point"] as? String {
            addRecord(title: "Redeem Point".localized, value: redeemPoint, yPadding: 10)
        }
        
        // extra parameter for Cashback field for topup // changes by Ashish
        if let cashBack = dictValues["cashback"] as? String {
            addRecord(title: "Cashback".localized, value: cashBack, yPadding: 10)
        } else  if let cashBackAttr = dictValues["cashback"] as? NSMutableAttributedString {
            addRecord(title: "Cashback", attributed: cashBackAttr, value: "", yPadding: 10)
        }
        
        if let buyInValue = dictValues["buyIn"] as? NSMutableAttributedString {
            var buyInTitleDenom = ""
            if let denom = dictValues["buyInTitle"] as? String {
                buyInTitleDenom = denom
            }
            addRecord(title: "Buy in".localized + " \(buyInTitleDenom)", attributed: buyInValue,
                      value: "", yPadding: 10)
        }
        if let amountValue = dictValues["cicoServiceFee"] as? String {
            addRecord(title: "Commission Fee".localized, value: amountValue + " MMK", yPadding: 10)
        }
        if let cicoAmount = dictValues["cicoAmount"] as? String {
            var titleCICO = "Transaction Type".localized
            if let str = dictValues["paymentType"] as? String {
                if str.lowercased() == "CASH IN".lowercased() {
                    titleCICO = "Cash In Amount".localized
                } else {
                    titleCICO = "Paid Digital Amount".localized
                }
            }
            addRecord(title: titleCICO, value: cicoAmount + " MMK", yPadding: 10)
        }
        if let commissionCICO = dictValues["amountCICO"] as? String {
            if let agent = dictValues["Payment Type"] as? String, agent == "Cash In" {
                if let type = dictValues["Received"] as? String, type == "true" {
                    addRecord(title: "Net Paid Digital Amount".localized, value: commissionCICO + " MMK", yPadding: 10)
                } else {
                    addRecord(title: "Net Received Amount".localized, value: commissionCICO + " MMK", yPadding: 10)
                }
                
            } else if let agent = dictValues["Payment Type"] as? String, agent == "Cash Out" {
                if let type = dictValues["Received"] as? String, type == "true" {
                    addRecord(title: "Net Paid Amount".localized, value: commissionCICO + " MMK", yPadding: 10)
                } else {
                    addRecord(title: "Net Received Amount".localized, value: commissionCICO + " MMK", yPadding: 10)
                }
            } else {
                addRecord(title: "Amount".localized, value: commissionCICO + " MMK", yPadding: 10)
            }
        }
        if let commissionCICO = dictValues["commissionCICO"] as? String {
            addRecord(title: "Agent Commission".localized, value: commissionCICO + " MMK", yPadding: 10)
        }
        if let totalAmount = dictValues["totalAmount"] as? String {
            if let str = dictValues["paymentType"] as? String, str.lowercased() == "Cash In".lowercased() {
                addRecord(title: "Net Paid Digital Amount".localized, value: totalAmount + " MMK", yPadding: 10)
            } else {
                addRecord(title: "Net Receive Amount".localized, value: totalAmount + " MMK", yPadding: 10)
            }
        }
        
        //adding offers values
        if let transactionOffer = dictValues["OfferObject"] as? OfferConfirmationScreen, let transID = dictValues["OfferObjectTransID"] as? String {

          //Adding sender account name title and value
          if let senderAccName = dictValues["senderAccNameOffer"] as? String {
            addRecord(title: "Sender Account Name".localized, value: senderAccName, yPadding: 5)
          }
          //Adding sender account number title and value
          if let senderAccNumber = dictValues["senderAccNoOffer"] as? String {
            addRecord(title: "Sender Account Number".localized, value: senderAccNumber, yPadding: 5)
          }
          
            //Adding promo receiver number
            if let promoReceiverNumber = dictValues["promoReceiverNumber"] as? String {
                addRecord(title: "Promo Receiver Number".localized, value: promoReceiverNumber, yPadding: 5)
            }
            
          if let offer = transactionOffer.offers, offer.paymentType == "0" {
            //Adding receiver account number title and value
            if let receiverAccNumber = dictValues["receiverAccNoOffer"] as? String {
                addRecord(title: "Receiver Account Number".localized, value: "XXXXXX" + String(receiverAccNumber.suffix(4)), yPadding: 5)
            }
          } else {
            //Adding receiver account name title and value
            if let receiverAccName = dictValues["receiverAccNameOffer"] as? String {
              addRecord(title: "Receiver Account Name".localized, value: receiverAccName, yPadding: 5)
            }
            //Adding receiver account number title and value
            if let receiverAccNumber = dictValues["receiverAccNoOffer"] as? String {
              addRecord(title: "Receiver Account Number".localized, value: receiverAccNumber, yPadding: 5)
            }
          }
            addRecord(title: "Category".localized, value: "Offers", yPadding: 5)
            addRecord(title: "Promo Code".localized, value: transactionOffer.offers?.promotionCode ?? "", yPadding: 5)
            if appDel.currentLanguage == "my" {
                addRecord(title: "Company Name".localized, value: transactionOffer.offers?.categoryBName ?? "", yPadding: 5)
            }
            else if appDel.currentLanguage == "zh-Hans" {
                addRecord(title: "Company Name".localized, value: transactionOffer.offers?.categoryCName ?? "", yPadding: 5)
            }
            else {
                addRecord(title: "Company Name".localized, value: transactionOffer.offers?.categoryName ?? "", yPadding: 5)
            }
        
          let offersModel = transactionOffer.offers
            let freeQty = String(transactionOffer.offers?.freeQty ?? 0)
            let freeamont = wrapAmountWithCommaDecimal(key: "\(freeQty)")
            let buyQty = String(transactionOffer.offers?.buyingQty ?? 0)
            let buyamont = wrapAmountWithCommaDecimal(key: "\(buyQty)")
            
            if offersModel?.amountType == "2" || offersModel?.amountType == "3" || offersModel?.amountType == "4" {
                if appDel.currentLanguage == "my" {
                    addRecord(title: "Product Name".localized, value: transactionOffer.offers?.productBName ?? "", yPadding: 5)
                }
                else if appDel.currentLanguage == "zh-Hans" {
                    addRecord(title: "Product Name".localized, value: transactionOffer.offers?.productCName ?? "", yPadding: 5)
                }
                else {
                    addRecord(title: "Product Name".localized, value: transactionOffer.offers?.productName ?? "", yPadding: 5)
                }
                addRecord(title: "Free Product Name".localized, value: transactionOffer.offers?.freeProductName ?? "", yPadding: 5)
                addRecord(title: "Buying Quantity".localized, value: "\(buyamont)", yPadding: 5)
                addRecord(title: "Free Quantity".localized, value: "\(freeamont)", yPadding: 5)
            }
            if offersModel?.amountType == "5" || offersModel?.amountType == "6" || offersModel?.amountType == "7" {
                if appDel.currentLanguage == "my" {
                    addRecord(title: "Product Name".localized, value: transactionOffer.offers?.productBName ?? "", yPadding: 5)
                }
                else if appDel.currentLanguage == "zh-Hans" {
                    addRecord(title: "Product Name".localized, value: transactionOffer.offers?.productCName ?? "", yPadding: 5)
                }
                else {
                    addRecord(title: "Product Name".localized, value: transactionOffer.offers?.productName ?? "", yPadding: 5)
                }
                addRecord(title: "Buying Quantity".localized, value: "\(buyamont)", yPadding: 5)
            }
            
          if offersModel?.amountType == "0" || offersModel?.amountType == "3" || offersModel?.amountType == "6"//Fixed=Percentage calculate
          {
            let amount = Float(transactionOffer.amount ?? "0") ?? 0.0
            let percentageAmount = Float(offersModel?.value ?? Int(0.0))
            let percentageValue = Float((percentageAmount * 100) / amount)
            addRecord(title: "Discount Percentage".localized, value: CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(percentageValue)") + "%", yPadding: 5)
            addRecord(title: "Discount Amount".localized, value: self.getDigitDisplay(digit: percentageAmount)  + " MMK", yPadding: 5)
          } else if offersModel?.amountType == "1" || offersModel?.amountType == "4" || offersModel?.amountType == "7" { //Fixed value display
            let value = Float(offersModel?.value ?? Int(0.0))
            addRecord(title: "Discount Percentage".localized, value: self.getDigitDisplay(digit: value) + "%", yPadding: 5)
            let amount = Float(transactionOffer.amount ?? "0") ?? 0.0
            let percentageAmount = Float(offersModel?.value ?? Int(0.0))
            let percentageValue = Float((percentageAmount * amount) / 100)
            addRecord(title: "Discount Amount".localized, value: self.getDigitDisplay(digit: percentageValue)  + " MMK", yPadding: 5)
          }
            let amount = Float(transactionOffer.amount ?? "0") ?? 0.0
           
            addRecord(title: "Bill Amount".localized, value: self.getDigitDisplay(digit: amount) + " MMK", yPadding: 5)

            addRecord(title: "Transaction ID".localized, value: transID, yPadding: 5)
            addRecord(title: "Transaction Type".localized, value: "PAYTO", yPadding: 5)
          //Adding date and time
          if let transactionDate = dictValues["transactionDateOffer"] as? String {
            addRecord(title: "Date & Time".localized, value: transactionDate, yPadding: 5)
          }
          
          if transactionOffer.remarks?.count ?? 0 > 0 {
            addRecord(title: "Remarks".localized, value: transactionOffer.remarks ?? "", yPadding: 5)
          }
        }
        //Adding date and time
        if let transactionDate = dictValues["transactionDate"] as? String {
            addRecord(title: "Date & Time".localized, value: transactionDate, yPadding: 10)
        }
        
        let paddingRemarksAmount: CGFloat = 30
        let sizeRemarksAmount = CGSize(width: mainViewFrameSize.width - (paddingRemarksAmount * 2), height: 0)
        
        let viewRemarksAmount = UIView(frame: CGRect(origin: CGPoint(x: paddingRemarksAmount, y: currentYPosition + 30), size: sizeRemarksAmount))
        
        //Adding Remarks
        if let remarks = dictValues["remarks"] as? String {
            let paddinglabelRemark: CGFloat = 5
            let labelSize = CGSize(width: viewRemarksAmount.frame.size.width - (paddinglabelRemark * 2), height: 42)
            let labelRemarks = UILabel(frame: CGRect(origin: CGPoint(x: paddinglabelRemark, y: 0), size: labelSize))
            labelRemarks.adjustsFontSizeToFitWidth = true
            labelRemarks.numberOfLines = 2
            labelRemarks.textAlignment = .left
            labelRemarks.text = remarks
            if let myFont = UIFont(name: appFont, size: 12) {
                labelRemarks.font = myFont
            }
            labelRemarks.textColor = UIColor.black
            if ((viewRemarksAmount.frame.origin.y <= 842) && ((viewRemarksAmount.frame.origin.y + labelRemarks.frame.size.height)) > 842) {
                viewRemarksAmount.frame.origin.y = 842 + 50
            }
            viewRemarksAmount.frame = CGRect(x: viewRemarksAmount.frame.origin.x, y: viewRemarksAmount.frame.origin.y, width: viewRemarksAmount.frame.size.width, height: viewRemarksAmount.frame.size.height + labelRemarks.frame.size.height)
            viewRemarksAmount.addSubview(labelRemarks)
        }
        
        //Adding Amount
        if let amount = dictValues["amount"] as? NSMutableAttributedString {
            let paddinglabelAmount: CGFloat = 15
            let equalWidth: CGFloat = (viewRemarksAmount.frame.size.width - (paddinglabelAmount * 2)) / 2
            let labelSize = CGSize(width: equalWidth, height: 42)
            var yPos = viewRemarksAmount.frame.size.height
            if viewRemarksAmount.frame.size.height > 0 {
                let separator = UIView(frame: CGRect(x: 0, y: yPos, width: viewRemarksAmount.frame.size.width, height: 1))
                separator.backgroundColor = UIColor.gray
                viewRemarksAmount.addSubview(separator)
                yPos = yPos + 1
            }
            
            let labelAmountTitle = UILabel(frame: CGRect(origin: CGPoint(x: paddinglabelAmount, y: yPos), size: labelSize))
            labelAmountTitle.adjustsFontSizeToFitWidth = true
            labelAmountTitle.textAlignment = .left
            if let str = dictValues["paymentType"] as? String, (str == "Cash in" || str == "Cash Out") {
                labelAmountTitle.text =  (str == "Cash In") ? "Net Paid Digital Amount".localized : "Paid Digital Amount".localized
            } else if let agent = dictValues["Payment Type"] as? String, (agent == "Cash In" || agent == "Cash Out") {
                labelAmountTitle.text =  (agent == "Cash In") ? "Net Paid Digital Amount".localized : "Paid Digital Amount".localized
            } else {
                labelAmountTitle.text = "Amount Paid".localized
            }
            if let myFont = UIFont(name: appFont, size: 12) {
                labelAmountTitle.font = myFont
            }
            labelAmountTitle.textColor = UIColor.black
            
            let labelAmountValue = UILabel(frame: CGRect(origin: CGPoint(x: labelAmountTitle.frame.maxX, y: yPos), size: labelSize))
            labelAmountValue.adjustsFontSizeToFitWidth = true
            labelAmountValue.textAlignment = .right
            labelAmountValue.attributedText = amount
//            if let myFont = UIFont(name: "Zawgyi-One", size: 12) {
//                labelAmountValue.font = myFont
//            }
            labelAmountValue.textColor = UIColor.black
            viewRemarksAmount.frame = CGRect(x: viewRemarksAmount.frame.origin.x, y: viewRemarksAmount.frame.origin.y, width: viewRemarksAmount.frame.size.width, height: labelAmountTitle.frame.maxY)
            viewRemarksAmount.addSubview(labelAmountTitle)
            viewRemarksAmount.addSubview(labelAmountValue)
        } else if var amount = dictValues["amount"] as? String {
            let paddinglabelAmount: CGFloat = 15
            let equalWidth: CGFloat = (viewRemarksAmount.frame.size.width - (paddinglabelAmount * 2)) / 2
            let labelSize = CGSize(width: equalWidth, height: 42)
            var yPos = viewRemarksAmount.frame.size.height
            if viewRemarksAmount.frame.size.height > 0 {
                let separator = UIView(frame: CGRect(x: 0, y: yPos, width: viewRemarksAmount.frame.size.width, height: 1))
                separator.backgroundColor = UIColor.gray
                viewRemarksAmount.addSubview(separator)
                yPos = yPos + 1
            }
            
            let labelAmountTitle = UILabel(frame: CGRect(origin: CGPoint(x: paddinglabelAmount, y: yPos), size: labelSize))
            labelAmountTitle.adjustsFontSizeToFitWidth = true
            labelAmountTitle.textAlignment = .left
            if let str = dictValues["paymentType"] as? String, (str == "Cash in" || str == "Cash Out") {
                if let type = dictValues["Received"] as? String, type == "true" {
                    labelAmountTitle.text =  (type == "true") ? "Net Received Digital Amount".localized : "Paid Digital Amount".localized
                } else {
                   labelAmountTitle.text =  (str == "Cash In") ? "Net Paid Digital Amount".localized : "Paid Digital Amount".localized
                }
                
            } else if let agent = dictValues["Payment Type"] as? String, (agent == "Cash In" || agent == "Cash Out") {
                if let type = dictValues["Received"] as? String, type == "true" {
                    labelAmountTitle.text =  (type == "true") ? "Net Received Digital Amount".localized : "Paid Digital Amount".localized
                } else {
                    labelAmountTitle.text =  (agent == "Cash In") ? "Net Paid Digital Amount".localized : "Paid Digital Amount".localized
                }
            } else if let typeLoyalty = dictValues["loyaltyType"] as? String, typeLoyalty != "" {
                if typeLoyalty == "PointDistribution" {
                    labelAmountTitle.text = "Distributed Points".localized
                } else if typeLoyalty == "RedeemPoints" {
                    labelAmountTitle.text = "Redeem Points".localized
                } else {
                    labelAmountTitle.text = "Points Transferred".localized
                }
            } else {
                labelAmountTitle.text = "Amount Paid".localized
            }
            if let myFont = UIFont(name: appFont, size: 12) {
                labelAmountTitle.font = myFont
            }
            labelAmountTitle.textColor = UIColor.black
            
            let labelAmountValue = UILabel(frame: CGRect(origin: CGPoint(x: labelAmountTitle.frame.maxX, y: yPos), size: labelSize))
            labelAmountValue.adjustsFontSizeToFitWidth = true
            labelAmountValue.textAlignment = .right
            if amount.lowercased().contains(find: "points") {
                labelAmountValue.text = amount
                if let myFont = UIFont(name: appFont, size: 12) {
                    labelAmountValue.font = myFont
                }
            } else {
                amount = amount.replacingOccurrences(of: "MMK", with: "")
                labelAmountValue.attributedText = wrapAmountWithMMK(bal: amount)
            }
            labelAmountValue.textColor = UIColor.black
            viewRemarksAmount.frame = CGRect(x: viewRemarksAmount.frame.origin.x, y: viewRemarksAmount.frame.origin.y, width: viewRemarksAmount.frame.size.width, height: labelAmountTitle.frame.maxY)
            if ((viewRemarksAmount.frame.minY <= 842) && (viewRemarksAmount.frame.maxY > 842)) {
                viewRemarksAmount.frame.origin.y = 842 + 50
            }
            viewRemarksAmount.addSubview(labelAmountTitle)
            viewRemarksAmount.addSubview(labelAmountValue)
        } else if let amount = dictValues["amountBuyBonus"] as? NSMutableAttributedString {
            let paddinglabelAmount: CGFloat = 15
            let equalWidth: CGFloat = (viewRemarksAmount.frame.size.width - (paddinglabelAmount * 2)) / 2
            let labelSize = CGSize(width: equalWidth, height: 42)
            var yPos = viewRemarksAmount.frame.size.height
            if viewRemarksAmount.frame.size.height > 0 {
                let separator = UIView(frame: CGRect(x: 0, y: yPos, width: viewRemarksAmount.frame.size.width, height: 1))
                separator.backgroundColor = UIColor.gray
                viewRemarksAmount.addSubview(separator)
                yPos = yPos + 1
            }
            
            let labelAmountTitle = UILabel(frame: CGRect(origin: CGPoint(x: paddinglabelAmount, y: yPos), size: labelSize))
            labelAmountTitle.adjustsFontSizeToFitWidth = true
            labelAmountTitle.textAlignment = .left
            labelAmountTitle.text = "Topup Amount".localized
            if let myFont = UIFont(name: appFont, size: 12) {
                labelAmountTitle.font = myFont
            }
            labelAmountTitle.textColor = UIColor.black
            
            let labelAmountValue = UILabel(frame: CGRect(origin: CGPoint(x: labelAmountTitle.frame.maxX, y: yPos), size: labelSize))
            labelAmountValue.adjustsFontSizeToFitWidth = true
            labelAmountValue.textAlignment = .right
            labelAmountValue.attributedText = amount
            if let myFont = UIFont(name: appFont, size: 12) {
                labelAmountValue.font = myFont
            }
            labelAmountValue.textColor = UIColor.black
            viewRemarksAmount.frame = CGRect(x: viewRemarksAmount.frame.origin.x, y: viewRemarksAmount.frame.origin.y, width: viewRemarksAmount.frame.size.width, height: labelAmountTitle.frame.maxY)
            viewRemarksAmount.addSubview(labelAmountTitle)
            viewRemarksAmount.addSubview(labelAmountValue)
        } else if let amount = dictValues["amountBuyBonus"] as? String {
            let paddinglabelAmount: CGFloat = 15
            let equalWidth: CGFloat = (viewRemarksAmount.frame.size.width - (paddinglabelAmount * 2)) / 2
            let labelSize = CGSize(width: equalWidth, height: 42)
            var yPos = viewRemarksAmount.frame.size.height
            if viewRemarksAmount.frame.size.height > 0 {
                let separator = UIView(frame: CGRect(x: 0, y: yPos, width: viewRemarksAmount.frame.size.width, height: 1))
                separator.backgroundColor = UIColor.gray
                viewRemarksAmount.addSubview(separator)
                yPos = yPos + 1
            }
            
            let labelAmountTitle = UILabel(frame: CGRect(origin: CGPoint(x: paddinglabelAmount, y: yPos), size: labelSize))
            labelAmountTitle.adjustsFontSizeToFitWidth = true
            labelAmountTitle.textAlignment = .left
            labelAmountTitle.text = "Topup Amount".localized
            if let myFont = UIFont(name: appFont, size: 12) {
                labelAmountTitle.font = myFont
            }
            labelAmountTitle.textColor = UIColor.black
            
            let labelAmountValue = UILabel(frame: CGRect(origin: CGPoint(x: labelAmountTitle.frame.maxX, y: yPos), size: labelSize))
            labelAmountValue.adjustsFontSizeToFitWidth = true
            labelAmountValue.textAlignment = .right
            labelAmountValue.text = amount
            if let myFont = UIFont(name: appFont, size: 12) {
                labelAmountValue.font = myFont
            }
            labelAmountValue.textColor = UIColor.black
            viewRemarksAmount.frame = CGRect(x: viewRemarksAmount.frame.origin.x, y: viewRemarksAmount.frame.origin.y, width: viewRemarksAmount.frame.size.width, height: labelAmountTitle.frame.maxY)
            viewRemarksAmount.addSubview(labelAmountTitle)
            viewRemarksAmount.addSubview(labelAmountValue)
        } else if let amount = dictValues["amountOffers"] as? String {
            let paddinglabelAmount: CGFloat = 15
            let equalWidth: CGFloat = (viewRemarksAmount.frame.size.width - (paddinglabelAmount * 2)) / 2
            let labelSize = CGSize(width: equalWidth, height: 42)
            var yPos = viewRemarksAmount.frame.size.height
            if viewRemarksAmount.frame.size.height > 0 {
                let separator = UIView(frame: CGRect(x: 0, y: yPos, width: viewRemarksAmount.frame.size.width, height: 1))
                separator.backgroundColor = UIColor.gray
                viewRemarksAmount.addSubview(separator)
                yPos = yPos + 1
            }
            
            let labelAmountTitle = UILabel(frame: CGRect(origin: CGPoint(x: paddinglabelAmount, y: yPos), size: labelSize))
            labelAmountTitle.adjustsFontSizeToFitWidth = true
            labelAmountTitle.textAlignment = .left
            labelAmountTitle.text = "Net Paid Amount".localized
            if let myFont = UIFont(name: appFont, size: 12) {
                labelAmountTitle.font = myFont
            }
            labelAmountTitle.textColor = UIColor.black
            
            let labelAmountValue = UILabel(frame: CGRect(origin: CGPoint(x: labelAmountTitle.frame.maxX, y: yPos), size: labelSize))
            labelAmountValue.adjustsFontSizeToFitWidth = true
            labelAmountValue.textAlignment = .right
            labelAmountValue.text = amount
            if let myFont = UIFont(name: appFont, size: 12) {
                labelAmountValue.font = myFont
            }
            labelAmountValue.textColor = UIColor.black
            viewRemarksAmount.frame = CGRect(x: viewRemarksAmount.frame.origin.x, y: viewRemarksAmount.frame.origin.y, width: viewRemarksAmount.frame.size.width, height: labelAmountTitle.frame.maxY)
            viewRemarksAmount.addSubview(labelAmountTitle)
            viewRemarksAmount.addSubview(labelAmountValue)
        }

        viewRemarksAmount.layer.borderWidth = 1
        viewRemarksAmount.layer.borderColor = UIColor.gray.cgColor
        viewMain.addSubview(viewRemarksAmount)
        currentYPosition = viewRemarksAmount.frame.maxY
        //Adding qr image
        if let qrImage = dictValues["qrImage"] as? UIImage {
            let sizeQRImage = CGSize(width: 100, height: 100)
            let xPos = (mainViewFrameSize.width - sizeQRImage.width) / 2
            if (((currentYPosition + 50) <= 842) && (currentYPosition + 150) > 842) {
                currentYPosition = 842
            }
            let imageViewQR = UIImageView(frame: CGRect(origin: CGPoint(x: xPos, y: currentYPosition + 50), size: sizeQRImage))
            imageViewQR.image = qrImage
            imageViewQR.contentMode = .scaleAspectFit
            currentYPosition = imageViewQR.frame.maxY
            viewMain.addSubview(imageViewQR)
        }
        viewMain.frame = CGRect(x: viewMain.frame.origin.x, y: viewMain.frame.origin.y, width: viewMain.frame.size.width, height: currentYPosition)
        //Generate pdf
        return generatePDF(viewForPDF: viewMain, pdfFile: pdfFileName)
    }
    
    private class func getYposInPdf(minY: CGFloat, height: CGFloat) -> CGFloat {
        if height <= (CodeSnippets.pdfSize.height - pdfHeaderFooterMargin) {
            let minimumY = minY.truncatingRemainder(dividingBy: CodeSnippets.pdfSize.height)
            let maximumY = minimumY + height
            if (minimumY <= CodeSnippets.pdfSize.height) && (maximumY > CodeSnippets.pdfSize.height) {
                return minY + minimumY + pdfHeaderFooterMargin
            }
        }
        return minY
    }
    
    class func getZawgyiFont(with size: CGFloat) -> UIFont? {
        return UIFont(name: "Zawgyi-One", size: size)
    }
    
    private class func label(width: CGFloat, text: PdfKeyValues.Text) -> UILabel? {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.adjustsFontSizeToFitWidth = true
        label.lineBreakMode = .byWordWrapping
        if let safeProperties = text.attributedTextProperties {
            label.attributedText = safeProperties.attributedText
            label.textAlignment = safeProperties.alignment
            label.numberOfLines = safeProperties.numberOfLines
        } else if let safeProperties = text.textProperties {
            label.text = safeProperties.text
            label.font = safeProperties.font
            label.textColor = safeProperties.fontColor
            label.textAlignment = safeProperties.alignment
            label.numberOfLines = safeProperties.numberOfLines
            label.backgroundColor = safeProperties.backgroundColor
        } else {
            return nil
        }
        label.sizeToFit()
        return label
    }
    
    private class func addTitleImage(yPos: CGFloat, imageObject: PdfKeyValues.TitleImage) -> UIView {
        let imageSize = CGSize(width: 70, height: 70)
        var xPos: CGFloat = 0
        var yPosition = yPos
        switch imageObject.imageAlignment {
        case .left:
            xPos = pdfMargin
        case .right:
            xPos = (CodeSnippets.pdfSize.width - imageSize.width - pdfMargin)
        default:
            xPos = (CodeSnippets.pdfSize.width - imageSize.width) / 2
        }
        yPosition = getYposInPdf(minY: yPos, height: imageSize.height)
        
        if let safeImageTitle = imageObject.title, let safeTitleLabel = label(width: CodeSnippets.pdfSize.width - (2 * pdfMargin), text: safeImageTitle) {
            let titleYPos = imageSize.height + pdfLineSpaceBtwnTitles
            let holderHeigth = titleYPos + safeTitleLabel.frame.size.height
            let titleImageHolder = UIView(frame: CGRect(origin: CGPoint(x: 0, y: yPosition),
                                                        size: CGSize(width: CodeSnippets.pdfSize.width, height: holderHeigth)))
            let imageViewLogo = UIImageView(frame: CGRect(origin: CGPoint(x: xPos, y: 0), size: imageSize))
            imageViewLogo.image = imageObject.image
            imageViewLogo.contentMode = .scaleAspectFit
            titleImageHolder.addSubview(imageViewLogo)
            let titleFrame = CGRect(x: pdfMargin, y: titleYPos, width: safeTitleLabel.frame.size.width, height: safeTitleLabel.frame.size.height)
            safeTitleLabel.frame = titleFrame
            titleImageHolder.addSubview(safeTitleLabel)
            return titleImageHolder
        } else {
            let imageViewLogo = UIImageView(frame: CGRect(origin: CGPoint(x: xPos, y: yPosition), size: imageSize))
            imageViewLogo.image = imageObject.image
            imageViewLogo.contentMode = .scaleAspectFit
            return imageViewLogo
        }
    }
    
    private class func addTitleRecord(yPos: CGFloat, titleObject: PdfKeyValues.TitleRecord) -> UIView {
        let xPos = pdfMargin
        if let safeTitle = titleObject.title, let safeTitleLabel = label(width: CodeSnippets.pdfSize.width - (2 * pdfMargin), text: safeTitle) {
            let yPosition = getYposInPdf(minY: yPos, height: safeTitleLabel.frame.size.height)
            let labelFrame = safeTitleLabel.frame
            safeTitleLabel.frame = CGRect(x: xPos, y: yPosition, width: labelFrame.size.width, height: labelFrame.size.height)
            return safeTitleLabel
        }
        return UIView.init(frame: CGRect.zero)
    }
    
    private class func addRecord(yPos: CGFloat, recordObject: PdfKeyValues.Record) -> UIView {
        let separatorXPos = (CodeSnippets.pdfSize.width - pdfCenterSeparatorWidth) / 2
        let valueXPos = (CodeSnippets.pdfSize.width + pdfCenterSeparatorWidth) / 2
        let holderView = UIView.init(frame: CGRect.zero)
        let labelWidth = ((CodeSnippets.pdfSize.width - pdfCenterSeparatorWidth) / 2) - pdfMargin
        var keyLabel = UILabel(frame: CGRect.zero)
        var valueLabel = UILabel(frame: CGRect.zero)
        var separatorLabel = UILabel(frame: CGRect.zero)
        if let keyObject = recordObject.key, let safeKeyLabel = label(width: labelWidth, text: keyObject) {
            keyLabel = safeKeyLabel
        }
        if let safeSeparatorLabel = label(width: labelWidth, text: recordObject.separator) {
            separatorLabel = safeSeparatorLabel
        }
        if let valueObject = recordObject.value, let safeValueLabel = label(width: labelWidth, text: valueObject) {
            valueLabel = safeValueLabel
        }
        var heightHolder = holderView.frame.size.height
        if keyLabel.frame.size.height > valueLabel.frame.size.height {
            heightHolder = keyLabel.frame.size.height
        } else {
            heightHolder = valueLabel.frame.size.height
        }
        let yPosition = getYposInPdf(minY: yPos, height: heightHolder)
        holderView.frame = CGRect(x: pdfMargin, y: yPosition, width: CodeSnippets.pdfSize.width - (2 * pdfMargin), height: heightHolder)
        keyLabel.frame = CGRect(x: 0, y: 0, width: keyLabel.frame.size.width, height: keyLabel.frame.size.height)
        separatorLabel.frame = CGRect(x: separatorXPos, y: 0, width: pdfCenterSeparatorWidth, height: heightHolder)
        valueLabel.frame = CGRect(x: valueXPos, y: 0, width: valueLabel.frame.size.width, height: valueLabel.frame.size.height)
        holderView.addSubview(keyLabel)
        holderView.addSubview(separatorLabel)
        holderView.addSubview(valueLabel)
        return holderView
    }
    
    private class func addTable(yPos: CGFloat, tableObject: PdfKeyValues.RecordTable) -> UIView {
        let holderView = UIView(frame: CGRect.zero)
        let holderWidth = CodeSnippets.pdfSize.width - (2 * pdfMargin)
        var holderHeight: CGFloat = 0
        
        var labelYPos: CGFloat = 0
        for (rowIndex, rowObject) in tableObject.rows.enumerated() {
            let columnCount = rowObject.column.count
            let columnWidth = holderWidth / CGFloat(columnCount)
            var maxHeight: CGFloat = 0
            for (columnIndex, columnObject) in rowObject.column.enumerated() {
                if let safeText = columnObject.value, let safeLabel = label(width: columnWidth, text: safeText) {
                    let labelXPos = CGFloat(columnIndex) * columnWidth
                    maxHeight = max(maxHeight, safeLabel.frame.size.height)
                    safeLabel.frame = CGRect(x: labelXPos, y: labelYPos, width: columnWidth, height: maxHeight)
                    holderView.addSubview(safeLabel)
                }
            }
            labelYPos += maxHeight
            holderHeight += maxHeight
            if rowIndex < tableObject.rows.count - 1 {
                let separatorView = UIView(frame: CGRect(x: 0, y: labelYPos, width: holderView.frame.size.width, height: 1))
                separatorView.backgroundColor = UIColor.black
                labelYPos += 1
                holderHeight += 1
            }
        }
        
        let holderY = getYposInPdf(minY: yPos, height: holderHeight)
        holderView.frame = CGRect(x: pdfMargin, y: holderY, width: holderWidth, height: holderHeight)
        holderView.layer.borderWidth = 1
        holderView.layer.borderColor = UIColor.black.cgColor
        return holderView
    }
    
    class func createPDFNew(pdfObjects: [PdfKeyValues], pdfFile: String?) -> URL? {
        let pdfFileName = pdfFile ?? "OK_Dollar_Invoice"
        var currentYPosition: CGFloat = pdfHeaderFooterMargin
        let viewMain = UIView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CodeSnippets.pdfSize))

        for object in pdfObjects {
            if let imageObject = object.titleImage {
                let titleImageView = addTitleImage(yPos: currentYPosition + pdfLineSpacing, imageObject: imageObject)
                currentYPosition = titleImageView.frame.maxY
                viewMain.addSubview(titleImageView)
            } else if let titleObject = object.titleRecord {
                let titleView = addTitleRecord(yPos: currentYPosition + pdfLineSpaceBtwnTitles, titleObject: titleObject)
                currentYPosition = titleView.frame.maxY
                viewMain.addSubview(titleView)
            } else if let recordObject = object.record {
                let recordView = addRecord(yPos: currentYPosition + pdfLineSpacing, recordObject: recordObject)
                currentYPosition = recordView.frame.maxY
                viewMain.addSubview(recordView)
            } else if let tableObject = object.table {
                let tableRecordView = addTable(yPos: currentYPosition + pdfLineSpacing, tableObject: tableObject)
                currentYPosition = tableRecordView.frame.maxY
                viewMain.addSubview(tableRecordView)
            }
        }
        viewMain.frame = CGRect(x: viewMain.frame.origin.x, y: viewMain.frame.origin.y, width: viewMain.frame.size.width, height: currentYPosition)
        //Generate pdf
        return generatePDF(viewForPDF: viewMain, pdfFile: pdfFileName)
    }
    
    class func generatePDF(viewForPDF: UIView, pdfFile: String?) -> URL? {
        
        let pdfFileName = pdfFile ?? "OK_Dollar_Invoice"
        
        let render = UIPrintPageRenderer()
        
        // 3. Assign paperRect and printableRect
        let page = CGRect(x: 0, y: 0, width: 595, height: 842) // A4, 72 dpi
        let printable = page.insetBy(dx: 0, dy: 0)
        
        // render.addPrintFormatter(aView.viewPrintFormatter(), startingAtPageAt: 0)
        
        render.setValue(NSValue(cgRect: page), forKey: "paperRect")
        render.setValue(NSValue(cgRect: printable), forKey: "printableRect")
        
        // 4. Create PDF context and draw
        
        let priorBounds = viewForPDF.bounds
        let fittedSize = viewForPDF.sizeThatFits(CGSize(width:priorBounds.size.width, height:viewForPDF.bounds.size.height))
        viewForPDF.bounds = CGRect(x:0, y:0, width:fittedSize.width, height:fittedSize.height)
        let pdfPageBounds = CGRect(x:0, y:0, width:viewForPDF.frame.width, height: page.size.height)
        
        
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds, nil)
        var pageOriginY: CGFloat = 0
        while pageOriginY < fittedSize.height {
            UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
            UIGraphicsGetCurrentContext()!.saveGState()
            UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: -pageOriginY)
            viewForPDF.layer.render(in: UIGraphicsGetCurrentContext()!)
            UIGraphicsGetCurrentContext()!.restoreGState()
            pageOriginY += pdfPageBounds.size.height
        }
        UIGraphicsEndPDFContext()
        viewForPDF.bounds = priorBounds
        
        //get file path
        guard let documentDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else  { return nil}
        let properPDFFileName = pdfFileName.replacingOccurrences(of: ".pdf", with: "") + ".pdf"
        let pdfUrl = documentDirectories + "/" + properPDFFileName
        //Delete file if alreay exists
        do {
            try FileManager.default.removeItem(atPath: pdfUrl)
        } catch let error as NSError {
            println_debug("Error: \(error.domain)")
        }
        //Write file
        if pdfData.write(toFile: pdfUrl, atomically: true) {
            println_debug("Successfully wrote to \(pdfUrl)")
            
            let pathUrl = URL(fileURLWithPath: documentDirectories)
            let fileUrl = pathUrl.appendingPathComponent(properPDFFileName)
            return fileUrl
        }
        return nil
    }
    
    class func generateExcel(excelDataWithFormat: String, excelFileName: String) -> URL? {
        guard let documentDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else  { return nil}
        let properExcelName = excelFileName.replacingOccurrences(of: ".csv", with: "") + ".csv"
        let excelPathUrlString = documentDirectories + "/" + properExcelName
        //Delete file if alreay exists
        do {
            try FileManager.default.removeItem(atPath: excelPathUrlString)
        } catch let error as NSError {
            println_debug("Error: \(error.domain)")
        }
        do {
            try excelDataWithFormat.write(toFile: excelPathUrlString, atomically: true, encoding: String.Encoding.utf8)
            let pathUrl = URL(fileURLWithPath: documentDirectories)
            let fileUrl = pathUrl.appendingPathComponent(properExcelName)
            return fileUrl
        } catch {
            return nil
        }
    }
    
    class func getDigitDisplay(digit : Float) -> String {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        return formatter.string(from: digit as NSNumber)!
    }
    
    class func commaFormatWithDoublePrecision(forTheAmount amount: String) -> String {
        guard let amountInDouble = Double(amount) else { return "" }
        var formattedIntAmount = String(format: "%.2f", locale: Locale.current, amountInDouble)
        if formattedIntAmount.contains(find: ".00") {
            formattedIntAmount = formattedIntAmount.replacingOccurrences(of: ".00", with: "")
        }
        return formattedIntAmount
    }
    
    class func commaFormatWithNoPrecision(forTheAmount amount: String) -> String {
        guard let amountInDouble = Double(amount) else { return "" }
        var formattedIntAmount = String(format: "%.2f", locale: Locale.current, amountInDouble)
        if formattedIntAmount.contains(find: ".00") {
            formattedIntAmount = formattedIntAmount.replacingOccurrences(of: ".00", with: "")
        }
        return formattedIntAmount
    }
    
    class func appendAttributedMMK(forTheAmount amount: String) -> NSAttributedString {
        let denomAttr = [NSAttributedString.Key.font: UIFont(name: appFont, size: 15.0) ?? UIFont.systemFont(ofSize: 15)]
        let amntAttr = [NSAttributedString.Key.font: UIFont(name: appFont, size: 9.0) ?? UIFont.systemFont(ofSize: 9)]
        let denomStr = NSMutableAttributedString(string: amount, attributes: denomAttr)
        let amountStr = NSMutableAttributedString.init(string: " MMK", attributes: amntAttr)
        
        let amountAttr = NSMutableAttributedString()
        amountAttr.append(denomStr)
        amountAttr.append(amountStr)
        return amountAttr
    }
    
    class func getCurrencyPercentageInAttributed(withString: String, amountSize: CGFloat = 17,
                                       amountColor: UIColor = .red, currencyDenomSize: CGFloat = 12,
                                       currencyDenomColor: UIColor = .black) -> NSMutableAttributedString {
        let formtdNumber = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: withString)
        
        var myAmountStyle = UIFont.boldSystemFont(ofSize: amountSize)
        var myCurrencyDenomStyle = UIFont.boldSystemFont(ofSize: currencyDenomSize)
        if let myFont = UIFont(name: appFont, size: amountSize) {
            myAmountStyle = myFont
        }
        if let myFont = UIFont(name: appFont, size: currencyDenomSize) {
            myCurrencyDenomStyle = myFont
        }
        let attrs1 = [NSAttributedString.Key.font: myAmountStyle, NSAttributedString.Key.foregroundColor: amountColor]
        let attrs2 = [NSAttributedString.Key.font: myCurrencyDenomStyle, NSAttributedString.Key.foregroundColor: currencyDenomColor]
        let attributedString1 = NSMutableAttributedString(string: formtdNumber, attributes: attrs1)
        let attributedString2 = NSMutableAttributedString(string: " %", attributes: attrs2)
        attributedString1.append(attributedString2)
        return attributedString1
    }
    
    class func getCurrencyInAttributed(withString: String, amountSize: CGFloat = 17,
                                       amountColor: UIColor = .red, currencyDenomSize: CGFloat = 12,
                                       currencyDenomColor: UIColor = .black) -> NSMutableAttributedString {
        let formtdNumber = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: withString)
        
        var myAmountStyle = UIFont.boldSystemFont(ofSize: amountSize)
        var myCurrencyDenomStyle = UIFont.boldSystemFont(ofSize: currencyDenomSize)
        if let myFont = UIFont(name: appFont, size: amountSize) {
            myAmountStyle = myFont
        }
        if let myFont = UIFont(name: appFont, size: currencyDenomSize) {
            myCurrencyDenomStyle = myFont
        }
        let attrs1 = [NSAttributedString.Key.font: myAmountStyle, NSAttributedString.Key.foregroundColor: amountColor]
        let attrs2 = [NSAttributedString.Key.font: myCurrencyDenomStyle, NSAttributedString.Key.foregroundColor: currencyDenomColor]
        let attributedString1 = NSMutableAttributedString(string: formtdNumber, attributes: attrs1)
        let attributedString2 = NSMutableAttributedString(string: " MMK", attributes: attrs2)
        attributedString1.append(attributedString2)
        return attributedString1
    }
    
    class func getSearchBar() -> UISearchBar {
        let searchBar = UISearchBar()
        searchBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40)
        searchBar.showsCancelButton = true
        searchBar.tintColor = UIColor.white
        searchBar.placeholder = "Search".localized
        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
                searchTextField.keyboardType = .asciiCapable
            }
        }
        let view = searchBar.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                subView.tintColor = UIColor.gray
            }
        }
        return searchBar
    }
    
    class func performReceivedCount(isToAdd: Bool) {
        var receivedCountValue = UserDefaults.standard.integer(forKey: "ReceivedCount")
        if isToAdd {
            receivedCountValue += 1
            UIApplication.shared.applicationIconBadgeNumber = receivedCountValue
            UserDefaults.standard.set(receivedCountValue, forKey: "ReceivedCount")
        } else {
            UIApplication.shared.applicationIconBadgeNumber = 0
            UserDefaults.standard.set(0, forKey: "ReceivedCount")
        }
    }
    
    class func getMobileNumber(transRecord: ReportTransactionRecord?) -> String {
        if let comments = transRecord?.desc {
            if comments.contains(find: "Overseas") {
                if let cCode = transRecord?.countryCode, cCode.count > 0 {
                    return "(" + cCode + ")" + (transRecord?.destination ?? "")
                }
            }
            else if comments.contains(find: "TopUp Plan") {
                if let mobileNumber = transRecord?.destination, mobileNumber.hasPrefix("0095") {
                    return "(+95)0" + mobileNumber.dropFirst().dropFirst().dropFirst().dropFirst()
                } else {
                    return transRecord?.destination ?? ""
                }
            } else if comments.contains(find: "Data Plan") {
                if let mobileNumber = transRecord?.destination, mobileNumber.hasPrefix("0095") {
                    return "(+95)0" + mobileNumber.dropFirst().dropFirst().dropFirst().dropFirst()
                } else {
                    return transRecord?.destination ?? ""
                }
            }
            else {
                if transRecord?.operatorName?.count ?? 0 > 2 {
                    if let mobileNumber = transRecord?.operatorName, mobileNumber.hasPrefix("0095") {
                        return "(+95)0" + mobileNumber.dropFirst().dropFirst().dropFirst().dropFirst()
                    } else {
                        return transRecord?.operatorName ?? ""
                    }
                } else {
                    if let mobileNumber = transRecord?.destination, mobileNumber.hasPrefix("0095") {
                        return "(+95)0" + mobileNumber.dropFirst().dropFirst().dropFirst().dropFirst()
                    } else {
                        return transRecord?.destination ?? ""
                    }
                }
            }
        }
        return ""
    }
    
    class func getMobileNumWithBrackets(countryCode: String, number: String) -> String {
        if countryCode != "+95"  {
            return "(" + countryCode + ")" + number.dropFirst()
        }
        else {
            return "(+95)" + number
        }
    }
    
    class func getOparatorName(_ mobileNumber: String) -> String {
        let operatorArray = ["Select Operator", "Telenor", "Ooredoo", "MPT", "MecTel", "MyTel", "MecTel CDMA", "Mpt CDMA(800)", "Mpt CDMA(450)"]
        var oparatorName: String
        
        if mobileNumber.hasPrefix("979") || mobileNumber.hasPrefix("0979") || mobileNumber.hasPrefix("0095979") || mobileNumber.hasPrefix("00950979") || mobileNumber.hasPrefix("978") || mobileNumber.hasPrefix("0978") || mobileNumber.hasPrefix("0095978") || mobileNumber.hasPrefix("00950978") || mobileNumber.hasPrefix("0976") || mobileNumber.hasPrefix("976") || mobileNumber.hasPrefix("00950976") || mobileNumber.hasPrefix("0095976") || mobileNumber.hasPrefix("977") || mobileNumber.hasPrefix("0977") || mobileNumber.hasPrefix("00950977") || mobileNumber.hasPrefix("0095977") {
            oparatorName = operatorArray[1]
            //oparatorName =@"Telenor";
        } else if mobileNumber.hasPrefix("0997") || mobileNumber.hasPrefix("997") || mobileNumber.hasPrefix("00950997") || mobileNumber.hasPrefix("0095997") || mobileNumber.hasPrefix("99") || mobileNumber.hasPrefix("099") || mobileNumber.hasPrefix("0095099") || mobileNumber.hasPrefix("009599") {
            oparatorName = operatorArray[2]
            //oparatorName =@"Oreedoo";
            if mobileNumber.hasPrefix("0095991") || mobileNumber.hasPrefix("0991") || mobileNumber.hasPrefix("991") {
                oparatorName = operatorArray[3]
                //oparatorName =@"mpt";
            }
        } else if mobileNumber.hasPrefix("093") || mobileNumber.hasPrefix("93") || mobileNumber.hasPrefix("0095093") || mobileNumber.hasPrefix("009593") {
            oparatorName = operatorArray[4]
            //oparatorName =@"Mectel";
        } else if mobileNumber.hasPrefix("0931") || mobileNumber.hasPrefix("931") || mobileNumber.hasPrefix("0933") || mobileNumber.hasPrefix("933") || mobileNumber.hasPrefix("00950931") || mobileNumber.hasPrefix("0095931") || mobileNumber.hasPrefix("00950933") || mobileNumber.hasPrefix("0095933") {
            oparatorName = operatorArray[5]
            //oparatorName =@"Mectel CDMA";
        } else if mobileNumber.hasPrefix("0973") || mobileNumber.hasPrefix("973") || mobileNumber.hasPrefix("00950973") || mobileNumber.hasPrefix("0095973") || mobileNumber.hasPrefix("980") || mobileNumber.hasPrefix("0980") || mobileNumber.hasPrefix("00950980") || mobileNumber.hasPrefix("0095980") {
            oparatorName = operatorArray[6]
            //oparatorName =@"MPT 800";
        } else if mobileNumber.hasPrefix("0949") || mobileNumber.hasPrefix("949") || mobileNumber.hasPrefix("00950949") || mobileNumber.hasPrefix("0095949") || mobileNumber.hasPrefix("0947") || mobileNumber.hasPrefix("947") || mobileNumber.hasPrefix("0095947") || mobileNumber.hasPrefix("00950947") || mobileNumber.hasPrefix("0986") || mobileNumber.hasPrefix("986") || mobileNumber.hasPrefix("00950986") || mobileNumber.hasPrefix("0095986") {
            oparatorName = operatorArray[7]
            //oparatorName =@"MPT 400";
        } else if mobileNumber.hasPrefix("0969") || mobileNumber.hasPrefix("969") || mobileNumber.hasPrefix("00950969") || mobileNumber.hasPrefix("0095969") {
            oparatorName = operatorArray[5]
        } else {
            oparatorName = operatorArray[3]
            //oparatorName =@"MPT ";
        }
        return oparatorName
    }
    
    class func getDiscountSaleFreeQty(discountAmount: Int, totalAmount: Int, productValue: Int, saleQty: Int, freeQty: Int) -> (saleQuantity: Int, freeQuantity: Int, discountAmount: Int) {
        var qtyData = (0,0,0)
        let amountValue = Int(totalAmount)
        let productValue = Int(productValue)
        if Int(amountValue % productValue) == 0 {
            let qtyValue = Int(amountValue / productValue)
            
            qtyData = (saleQty * qtyValue, qtyValue * freeQty, discountAmount * qtyValue)
        }
        return qtyData
    }
    
    class func getSaleFreeQty(totalAmount: Int, productValue: Int, saleQty: Int, freeQty: Int) -> (saleQuantity: Int, freeQuantity: Int) {
        var qtyData = (0,0)
        let amountValue = Int(totalAmount)
        let productValue = Int(productValue)
        guard productValue != 0 else { return (0, 0)}
        if Int(amountValue % productValue) == 0 {
            let qtyValue = Int(amountValue / productValue)
            
            qtyData = (saleQty * qtyValue, qtyValue * freeQty)
        }
        return qtyData
    }
}
