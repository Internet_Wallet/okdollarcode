//
//  Helper.swift
//  OK
//  It is having helper classes and methods
//  Created by ANTONY on 30/01/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation

//MARK: - Protocol
protocol NavigationSetUpProtocol: class {
    var titleViewWidth: CGFloat { get }
    func setUpNavigation()
}

//MARK: - Classes
class PdfKeyValues {
    class TitleImage {
        var image: UIImage
        var imageAlignment: NSTextAlignment
        var title: Text?
        init(image: UIImage, alignment: NSTextAlignment = .center) {
            self.image = image
            self.imageAlignment = alignment
        }
    }
    class TitleRecord {
        var title: Text?
    }
    class Record {
        var key: Text?
        var value: Text?
        var separator: Text
        init() {
            let textProperties = PdfKeyValues.TextWithProperties(text: ":", alignment: .center)
            let separatorText = PdfKeyValues.Text(textProperties: textProperties)
            self.separator = separatorText
        }
    }
    class RecordTable {
        var rows: [RecordRow]
        
        init(rows: [RecordRow]) {
            self.rows = rows
        }
    }
    class RecordRow {
        var column: [Record]
        
        init(column: [Record]) {
            self.column = column
        }
    }
    class Text {
        var textProperties: TextWithProperties?
        var attributedTextProperties: AttributedTextWithProperties?
        init(textProperties: TextWithProperties) {
            self.textProperties = textProperties
        }
        init(attributedTextProperties: AttributedTextWithProperties) {
            self.attributedTextProperties = attributedTextProperties
        }
    }
    class AttributedTextWithProperties {
        var attributedText: NSAttributedString
        var alignment: NSTextAlignment
        var numberOfLines: Int
        init(attributedText: NSAttributedString, alignment: NSTextAlignment = .left, numberOfLines: Int = 0) {
            self.attributedText = attributedText
            self.alignment = alignment
            self.numberOfLines = numberOfLines
        }
    }
    class TextWithProperties {
        var text: String
        var font: UIFont?
        var fontColor: UIColor
        var backgroundColor: UIColor
        var alignment: NSTextAlignment
        var numberOfLines: Int
        init(text: String, fontSize: CGFloat = 15, fontColor: UIColor = .black, backgroundColor: UIColor = .clear, alignment: NSTextAlignment = .left, numberOfLines: Int = 0) {
            self.text = text
            self.font = CodeSnippets.getZawgyiFont(with: fontSize)
            self.fontColor = fontColor
            self.backgroundColor = backgroundColor
            self.alignment = alignment
            self.numberOfLines = numberOfLines
        }
    }
    
    var titleImage: TitleImage?
    var titleRecord: TitleRecord?
    var record: Record?
    var table: RecordTable?
    init(titleImage: TitleImage) {
        self.titleImage = titleImage
    }
    init(titleRecord: TitleRecord) {
        self.titleRecord = titleRecord
    }
    init(record: Record) {
        self.record = record
    }
    init(table: RecordTable) {
        self.table = table
    }
}

//MARK: - Subclasses
class ButtonWithImage: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        if imageView != nil {
            titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: (imageView?.frame.size.width)! + 10)
            imageEdgeInsets = UIEdgeInsets(top: 0, left: bounds.width - 25, bottom: 0, right: 0)
        }
    }
}

//MARK: - Date Extensions
extension Date {
    func stringValueWithOutUTC(dateFormatIs: String = "EEE, dd-MMM-yyyy HH:mm:ss") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = dateFormatIs
        return dateFormatter.string(from: self)
    }
    func stringValue(dateFormatIs: String = "EEE, dd-MMM-yyyy HH:mm:ss") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = dateFormatIs
        return dateFormatter.string(from: self)
    }
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
}

//MARK: - String Extensions
extension String {
    var localized: String {
        return appDel.getlocaLizationLanguage(key: self)
    }
    func dateValue(dateFormatIs: String = "dd-MMM-yyyy HH:mm:ss") -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.setLocale()
        dateFormatter.dateFormat = dateFormatIs
        return dateFormatter.date(from: self)
    }
    func replaceFirstTwoChar(withStr: Character) -> String {
        if self.count > 1 {
            var stringToTrim = self
            stringToTrim.remove(at: stringToTrim.startIndex)
            stringToTrim.remove(at: stringToTrim.startIndex)
            stringToTrim.insert(withStr, at: stringToTrim.startIndex)
            return stringToTrim
        } else {
            return "\(withStr)"
        }
    }
}

//MARK:- Dateformatter Extension
extension DateFormatter {
    func setLocale() {
        let enUSPOSIXLocale:NSLocale = NSLocale(localeIdentifier: "en_US")
        self.locale = enUSPOSIXLocale as Locale
        self.timeZone = TimeZone(identifier: "UTC")
    }
}

//MARK: - NSDecimalNumber Extensions
extension NSDecimalNumber {
    func toString(minIntDigits: Int = 1, mimFractionDigits: Int = 2) -> String {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = mimFractionDigits
        formatter.minimumIntegerDigits = minIntDigits
        var formattedString = formatter.string(from: self) ?? ""
        formattedString = formattedString.replacingOccurrences(of: ".00", with: "")
        return formattedString 
    }
    
    func toStringNoDecimal(minIntDigits: Int = 1, mimFractionDigits: Int = 0) -> String {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = mimFractionDigits
        formatter.minimumIntegerDigits = minIntDigits
        return formatter.string(from: self) ?? ""
    }
}

//MARK: - NavigationSetUpProtocol Extensions
extension NavigationSetUpProtocol {
    var titleViewWidth: CGFloat {
        switch device.type {
        case .iPhoneX, .iPhoneXS, .iPhoneXSMax, .iPhoneXR, .iPhone11, .iPhone11Pro, .iPhone11ProMax:
            return UIScreen.main.bounds.size.width - 112
        default:
            return UIScreen.main.bounds.size.width - 30
        }
    }
}

//MARK: - Textvalidation
class TextValidations {
    class func trimSuccessiveSpaces( textField: UITextField, range: NSRange, string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        var commaRestrStr = text
        
        if string == " " {
            var boundValue = range.upperBound + 1
            if text.contains("  ") {
                commaRestrStr = commaRestrStr.replacingOccurrences(of: "  ", with: " ")
                boundValue = range.lowerBound
            }
            if text.contains("  ") {
                boundValue = range.lowerBound
                commaRestrStr = commaRestrStr.replacingOccurrences(of: "  ", with: " ")
            }
            textField.text = commaRestrStr
            if range.upperBound == range.lowerBound {
                if let newPosition = textField.position(from: textField.beginningOfDocument, offset: boundValue) {
                    textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
                }
            } else {
                if let newPosition = textField.position(from: textField.beginningOfDocument, offset: range.lowerBound) {
                    textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
                }
            }
        } else if string == "" {
            let boundValue = range.lowerBound
            if text.contains("  ") {
                commaRestrStr = commaRestrStr.replacingOccurrences(of: "  ", with: " ")
            }
            if text.contains("  ") {
                commaRestrStr = commaRestrStr.replacingOccurrences(of: "  ", with: " ")
            }
            textField.text = commaRestrStr
            if let newPosition = textField.position(from: textField.beginningOfDocument, offset: boundValue) {
                textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
            }
        }
        return false
    }
}
