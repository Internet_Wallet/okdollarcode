//
//  ReportsAllRecordMoDel.swift
//  OK
//
//  Created by Tushar Lama on 12/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation

open class ReportAllTransactionModel{

var accTransType: String?
var age: Int16?
var amount: NSDecimalNumber?
var bonus: NSDecimalNumber?
var cashBack: NSDecimalNumber?
var countryCode: String?
var desc: String?
var destination: String?
var fee: NSDecimalNumber?
var gender: String?
var isFromApi: Int16
var latitude: String?
var longitude: String?
var operatorName: String?
var rating: String?
var ratingFeedback: String?
var rawDesc: String?
var receiverBName: String?
var receiverName: String?
var refTransID: String?
var senderBName: String?
var senderName: String?
var transactionDate: Date?
var transID: String?
var transType: String?
var walletBalance: NSDecimalNumber?

     init(record: RecordData){

            self.isFromApi = Int16(1)
            if let strAmount = record.amount {
                self.amount = NSDecimalNumber(string: strAmount)
            }
            if let strAmount = record.walletBalance {
                self.walletBalance = NSDecimalNumber(string: strAmount)
            }
            if let strAmount = record.bonus {
                self.bonus = NSDecimalNumber(string: strAmount)
            }
            if let strAmount = record.cashBack {
                self.cashBack = NSDecimalNumber(string: strAmount)
            }
            self.destination = record.destination
            self.transID = record.transactionId
            self.transType = record.transactionType
            self.operatorName = record.operatorName
            self.accTransType = record.accTransactionType
            self.refTransID = record.refTransID
            if let responseDate = record.responseDate {
                self.transactionDate = responseDate.dateValue()
            }
            self.desc = ""
            self.rawDesc = record.comments
            self.senderName = ""
            if let fee = record.fee {
                self.fee = NSDecimalNumber(string: fee)
            }
            //For description value
            if let safeComments = record.comments {
                let destArray = safeComments.components(separatedBy: "-")
                if safeComments.contains(find: "Overseas") {
                    let cCodeArray = safeComments.components(separatedBy: "Overseas-")
                    if cCodeArray.count > 1 {
                        let countryCode = cCodeArray[1].components(separatedBy: "-")
                        if countryCode.count > 0 {
                            self.countryCode = countryCode[0]
                        }
                    }
                    if destArray.count > 3 {
                        self.destination = destArray[3]
                    }
                } else if safeComments.contains(find: "Top-Up") || safeComments.contains(find: "TopUp Plan") || safeComments.contains(find: "TopUpPlan") || safeComments.contains(find: "Data Plan") || safeComments.contains(find: "DataPlan") || safeComments.contains(find: "Special Offers") || safeComments.contains(find: "Special Offer") || safeComments.contains(find: "SpecialOffers"){
                    if destArray.count > 3 {
                        let destNumber = destArray[3]
                        if Array(destNumber).count > 1 {
                            if Array(destNumber)[0] == "0" && Array(destNumber)[1] != "0" {
                                let destNo = "0095" + destNumber.dropFirst()
                                self.destination = destNo
                                if destNo == UserModel.shared.mobileNo {
                                    self.receiverName = UserModel.shared.name
                                }
                            }
                        }
                    }
                }
                let commentArray = safeComments.components(separatedBy: "[")
                if commentArray.count > 0 {
                    var description = commentArray[0].count == 0 ? "" : "Remarks: \(commentArray[0])"
                    if safeComments.contains(find: "#OK-PMC") {
                        description = safeComments
                    }
                    self.desc = ""//self.getRequiredComments(commentsToChange: description.removingPercentEncoding ?? "", rec: self)
                    //For age value and gender
                    if commentArray.count > 1 {
                        let sBracketRemovedArray = commentArray[1].components(separatedBy: "]")
                        if sBracketRemovedArray.count > 0 {
                            let commaSeparatedArray = sBracketRemovedArray[0].components(separatedBy: ",")
                            if safeComments.contains(find: "#OK-PMC") {
                                if commaSeparatedArray.count > 15 {
                                    self.latitude = commaSeparatedArray[16]
                                    self.longitude = commaSeparatedArray[17]
                                    self.gender = "\(commaSeparatedArray[19])"
                                    if let ageInt = Int16(commaSeparatedArray[20]) {
                                        self.age = ageInt
                                    }
                                }
                            } else {
                                if commaSeparatedArray.count > 4 {
                                    self.latitude = commaSeparatedArray[0]
                                    self.longitude = commaSeparatedArray[1]
                                    self.gender = "\(commaSeparatedArray[3])"
                                    if let ageInt = Int16(commaSeparatedArray[4]) {
                                        self.age = ageInt
                                    }
                                }
                            }
                            // For business name and merchant name
                            if safeComments.contains(find: "#OK-PMC") {
                                if sBracketRemovedArray.count > 1 {
                                    let nameSeparatedArray = sBracketRemovedArray[0].components(separatedBy: "##")
                                    self.receiverName = nameSeparatedArray.count > 1 ? nameSeparatedArray[1] : ""
                                    self.receiverBName = nameSeparatedArray.count > 2 ? nameSeparatedArray[2] : ""
                                    self.senderBName = nameSeparatedArray.count > 3 ? nameSeparatedArray[3] : ""
                                }
                            } else {
                                if sBracketRemovedArray.count > 1 {
                                    let nameSeparatedArray = sBracketRemovedArray[0].components(separatedBy: "##")
                                    self.receiverName = nameSeparatedArray.count > 1 ? nameSeparatedArray[1] : ""
                                    self.receiverBName = nameSeparatedArray.count > 2 ? nameSeparatedArray[2] : ""
                                    self.senderBName = nameSeparatedArray.count > 3 ? nameSeparatedArray[3] : ""
                                }
                            }
                        }
                    }
                }
                //For comments value
                let snArray = safeComments.components(separatedBy: "OKNAME-")
             
                if snArray.count > 1 {
                    if snArray[1].contains("@rating@") {
                        let snsubArray = snArray[1].components(separatedBy: "-")
                        if snsubArray.count > 1 {
                            if snsubArray[1].contains(find: "cashin") || snsubArray[1].contains(find: "cashout") {
                                if snsubArray.count > 3 {
                                    self.rating = snsubArray[2]
                                    self.ratingFeedback = snsubArray[3]
                                }
                            } else {
                                if snsubArray.count > 2 {
                                    self.rating = snsubArray[1]
                                    self.ratingFeedback = snsubArray[2]
                                }
                            }
                        }
                        if snsubArray.count > 0 {
                            self.senderName = snsubArray[0].components(separatedBy: "@rating").first ?? ""
                        }
                    } else {
                        self.senderName = snArray[1]
                    }
                }
             
                
                if safeComments.contains(find: "#point-redeempoints-") {
                    let redeemArray = safeComments.components(separatedBy: "#point-redeempoints-")
                    if redeemArray.count > 1 {
                        self.desc = "Buy Bonus Point"
                        self.bonus = NSDecimalNumber(string: redeemArray[1])
                    }
                }
            }
        
        
        
        
    }
    
    

}

