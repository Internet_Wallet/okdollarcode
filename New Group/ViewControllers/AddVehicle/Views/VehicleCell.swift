//
//  VehicleCell.swift
//  OK
//
//  Created by Uma Rajendran on 2/13/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class VehicleCell: UITableViewCell {

    @IBOutlet weak var mandatoryImg     : UIImageView!
    @IBOutlet weak var contentTxtField  : SkyFloatingLabelTextField!
    @IBOutlet weak var arrowImgView     : UIImageView!
    
    
    var cellindex       : Int = 0
    var cellname        : VehicleList?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.textFieldSettings()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }
    
    func textFieldSettings() {
        contentTxtField.font                    = UIFont(name: appFont, size: 16)
        contentTxtField.titleFont               = UIFont(name: appFont, size: 16) ?? UIFont.systemFont(ofSize: 16)
        contentTxtField.placeholderFont         = UIFont(name: appFont, size: 16)
        contentTxtField.textColor               = UIColor.black
        contentTxtField.placeholderColor        = UIColor.lightGray
        contentTxtField.lineColor               = UIColor.clear
        contentTxtField.selectedLineColor       = UIColor.clear
        contentTxtField.selectedTitleColor      = UIColor.clear
        contentTxtField.titleColor              = UIColor.clear
 
    }
    
    
    func wrap_Data()  {
//        self.contentTxtField.text               = ""
//        if accDetails.accountHolderName.count > 0 {
//            self.contentTxtField.title          = setLocStr("OK$ Account Name")
//        }else {
//            self.contentTxtField.placeholder    = setLocStr("Enter Account Holder Name")
//        }
        
    }
}
