//
//  VehicleDetailCell.swift
//  OK
//
//  Created by Uma Rajendran on 2/13/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class VehicleDetailCell: UITableViewCell {

    @IBOutlet weak var highLightLabel       : UILabel!{
        didSet{
            self.highLightLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var defaultColortLabel       : UILabel!{
        didSet{
            self.defaultColortLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var vehicleNameLabel     : UILabel!{
        didSet{
            self.vehicleNameLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var vehicleImgView       : UIImageView!
    @IBOutlet weak var deleteBtn            : UIButton!{
        didSet{
            self.deleteBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        vehicleNameLabel.font = UIFont(name: appFont, size: 17)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func wrapData() {
        
    }
    
}
