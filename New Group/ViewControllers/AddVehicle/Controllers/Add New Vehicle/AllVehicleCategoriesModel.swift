//
//  AllVehicleCategoriesModel.swift
//  OK
//
//  Created by Shobhit Singhal on 3/10/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class AllVehicleCategoriesModel: NSObject {
    var vehicleArrayDataModel = [VehiclesArrayModel]()
    var categoryId:String = ""
    var categoryName:String = ""
    var categoryNameBurmese:String = ""
    var categoryChildArray:NSArray = []

        init(data: JSON) {
            self.categoryId = data["CategoryId"].stringValue
            self.categoryName = data["CategoryName"].stringValue
            self.categoryNameBurmese = data["CategoryNameBurmese"].stringValue
            self.categoryChildArray = data["VehicleTypes"].arrayObject! as NSArray
            
            if data["VehicleTypes"].arrayObject != nil{
                let arrayData = data["VehicleTypes"].arrayObject! as NSArray
                vehicleArrayDataModel =  arrayData.map({(value) -> VehiclesArrayModel in
                    return  VehiclesArrayModel(data:JSON(value))
                })
            }
    }
    
    var getChildCategoryData:Array<VehiclesArrayModel>{
        return vehicleArrayDataModel
    }
}


class VehiclesArrayModel {
    var categoryName:String = ""
    var categoryNameBurmese:String = ""
    var categoryId:String = ""
    var vehicleTypeId:String = ""
    
    init(data: JSON) {
        self.categoryName = data["VehicleTypeName"].stringValue
        self.categoryNameBurmese = data["VehicleTypeNameBurmese"].stringValue
        self.categoryId = data["CategoryId"].stringValue
        self.vehicleTypeId = data["VehicleTypeId"].stringValue
    }
}

class AllVehicleCategoriesViewModel {
    var allVehicleCategoriesModel = [AllVehicleCategoriesModel]()
        init(data:JSON) {
            if data != .null {
                let arrayData = data.arrayObject! as NSArray
                allVehicleCategoriesModel =  arrayData.map({(value) -> AllVehicleCategoriesModel in
                    return  AllVehicleCategoriesModel(data:JSON(value))
                })
            }
        }
    
    var getAllVehicleCategoriesData:Array<AllVehicleCategoriesModel>{
        return allVehicleCategoriesModel
    }
}


//////////////////////////////////////////////////////////// Vehicle Brand Model ////////////////////////////////////////////////////////////////////////////

class BrandModel: NSObject {
    var brandId:String = ""
    var brandName:String = ""
    
    init(data: JSON) {
        self.brandId = data["Id"].stringValue
        self.brandName = data["BrandName"].stringValue
    }
}


class BrandViewModel {
    var brandModel = [BrandModel]()
    init(data:JSON) {
        if data != .null {
            let arrayData = data.arrayObject! as NSArray
            brandModel =  arrayData.map({(value) -> BrandModel in
                return  BrandModel(data:JSON(value))
            })
        }
    }
    
    var getBrandData:Array<BrandModel>{
        return brandModel
    }
}


//////////////////////////////////////////////////////////// Vehicle Brand Model ////////////////////////////////////////////////////////////////////////////

class SubBrandModel: NSObject {
    var subBrandId:String = ""
    var subBrandName:String = ""
    
    init(data: JSON) {
        self.subBrandId = data["Id"].stringValue
        self.subBrandName = data["ModelName"].stringValue
    }
}


class SubBrandViewModel {
    var subBrandModel = [SubBrandModel]()
    init(data:JSON) {
        if data != .null {
            let arrayData = data.arrayObject! as NSArray
            subBrandModel =  arrayData.map({(value) -> SubBrandModel in
                return  SubBrandModel(data:JSON(value))
            })
        }
    }
    
    var getSubBrandData:Array<SubBrandModel>{
        return subBrandModel
    }
}


struct AllVehicleDetails {
    var attributes = [String : String]() // A dictionary of String keys and String values
}








