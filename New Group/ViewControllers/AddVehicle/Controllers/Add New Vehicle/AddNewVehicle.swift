//
//  AddNewVehicle.swift
//  OK
//
//  Created by Shobhit Singhal on 3/10/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import IQKeyboardManagerSwift
import Foundation

class AddNewVehicle: OKBaseController, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate, vehicleColorDelegate, WebServiceResponseDelegate, vehicleSubCategoryTypeDelegate, BioMetricLoginDelegate {

    var whichApiCall:String = ""
    var passStringForApi:String = ""
    var ind:Int!
    let web = WebApiClass()
    var allVehicleCategoriesViewModel:AllVehicleCategoriesViewModel!
    var nextNavigationTitle:String = ""
    var copyAllVehicleServerMutableArr:NSMutableArray = []
    var allVehicleServerMutableArr:NSMutableArray = []
    var vehicleCategoryName:String = ""
    var vehicleTypeName:String = ""
    var vehicleCategoryId:String = ""
    var vehicleTypeId:String = ""
    var brandId:String = "-1"
    var vehicleModelId:String = "-1"
    var vehicleColorName:String = ""
    var addVehicleDICT = [String: Any]()
    let pickerView = UIPickerView()
    var isBurmese:Bool = false
    var firstSlash = false
    var firstSlashVehicleNumber = false
    var secondSalsh = false
    var firstChar = false
    var firstCharVehicleNumber = false
    var isSameDivisionExists = false
    var isSameVehicleNumberExists = false
    var isSearchBarActive:Bool = false
    let ACCEPTABLE_CHARACTERSLocal = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

    @IBOutlet weak var vehicleTypeLabel: UILabel!{
        didSet{
            self.vehicleTypeLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var vehicleBrandLabel: UILabel!{
        didSet{
            self.vehicleBrandLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var vehicleSubBrandLabel: UILabel!{
        didSet{
            self.vehicleSubBrandLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var vehicleColorLabel: UILabel!{
        didSet{
            self.vehicleColorLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var vehicleNumberLabel: UILabel!{
        didSet{
            self.vehicleNumberLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var selectDivisionStateLabel: UILabel!{
        didSet{
            self.selectDivisionStateLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var DLNumberLabel: UILabel!{
        didSet{
            self.DLNumberLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var crossButtonOutlet: UIButton!{
        didSet{
            self.crossButtonOutlet.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    

    @IBOutlet weak var vehicleTypePickerTextField: SkyFloatingLabelTextField!{
        didSet{
           // self.vehicleTypePickerTextField.font = UIFont(name: appFont, size: 17.0)
            self.vehicleTypePickerTextField.titleFont = UIFont(name: appFont, size: 17.0) ?? UIFont.systemFont(ofSize: appFontSize)
            self.vehicleTypePickerTextField.placeholderFont = UIFont(name: appFont, size: 17.0)
        }
    }
    @IBOutlet weak var vehicleBrandTextField: SkyFloatingLabelTextField!{
        didSet{
            self.vehicleBrandTextField.font = UIFont(name: appFont, size: 17.0)
            self.vehicleBrandTextField.titleFont = UIFont(name: appFont, size: 17.0) ?? UIFont.systemFont(ofSize: appFontSize)
            self.vehicleBrandTextField.placeholderFont = UIFont(name: appFont, size: 17.0)
            
        }
    }
    @IBOutlet weak var vehicleSubBrandTextField: SkyFloatingLabelTextField!{
        didSet{
            self.vehicleSubBrandTextField.font = UIFont(name: appFont, size: 17.0)
            self.vehicleSubBrandTextField.titleFont = UIFont(name: appFont, size: 17.0) ?? UIFont.systemFont(ofSize: appFontSize)
            self.vehicleSubBrandTextField.placeholderFont = UIFont(name: appFont, size: 17.0)
           
        }
    }
    @IBOutlet weak var vehicleColorTextField: SkyFloatingLabelTextField!{
        didSet{
          //  self.vehicleColorTextField.font = UIFont(name: appFont, size: 17.0)
            self.vehicleColorTextField.titleFont = UIFont(name: appFont, size: 17.0) ?? UIFont.systemFont(ofSize: appFontSize)
            self.vehicleColorTextField.placeholderFont = UIFont(name: appFont, size: 17.0)
        }
    }
    @IBOutlet weak var vehicleNumberTextField: SkyFloatingLabelTextField!{
        didSet{
            self.vehicleNumberTextField.font = UIFont(name: appFont, size: 17.0)
            self.vehicleNumberTextField.titleFont = UIFont(name: appFont, size: 17.0) ?? UIFont.systemFont(ofSize: appFontSize)
            self.vehicleNumberTextField.placeholderFont = UIFont(name: appFont, size: 17.0)
           
        }
    }
    @IBOutlet weak var selectDivisionStateTextField: SkyFloatingLabelTextField!{
        didSet{
            self.selectDivisionStateTextField.font = UIFont(name: appFont, size: 17.0)
            self.selectDivisionStateTextField.titleFont = UIFont(name: appFont, size: 17.0) ?? UIFont.systemFont(ofSize: appFontSize)
            self.selectDivisionStateTextField.placeholderFont = UIFont(name: appFont, size: 17.0)
            
        }
    }
    @IBOutlet weak var DLNumberTextField: SkyFloatingLabelTextField!{
        didSet{
            self.DLNumberTextField.font = UIFont(name: appFont, size: 17.0)
            self.DLNumberTextField.titleFont = UIFont(name: appFont, size: 17.0) ?? UIFont.systemFont(ofSize: appFontSize)
            self.DLNumberTextField.placeholderFont = UIFont(name: appFont, size: 17.0)
            
        }
    }
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var mainViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var vehicleTypeHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var vehicleTypeTFHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var selectBrandDetailsView: UIView!
    @IBOutlet weak var divisionStateView: UIView!
    @IBOutlet weak var DLNumberView: UIView!
    @IBOutlet weak var selectBrandDetailsHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var divisionStateHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var DLNumberHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var saveBtn : UIButton!{
        didSet{
            self.saveBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet var vehicleTypeArrowImg: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        crossButtonOutlet.isHidden = true
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.title = "Add Vehicle".localized
        web.delegate = self
        selectBrandDetailsView.isHidden = true
        divisionStateView.isHidden = true
        DLNumberView.isHidden = true
        whichApiCall = "allVehicleCategories"
        copyAllVehicleServerMutableArr = allVehicleServerMutableArr
        saveBtn.isHidden = true
        vehicleTypePickerTextField.placeholder = ""
        vehicleBrandTextField.placeholder = ""
        vehicleSubBrandTextField.placeholder = ""
        vehicleColorTextField.placeholder = ""
        vehicleNumberTextField.placeholder = ""
        selectDivisionStateTextField.placeholder = ""
        DLNumberTextField.placeholder = ""
        
        vehicleTypeLabel.text = "Select Vehicle Type".localized
        vehicleBrandLabel.text = "Select Vehicle Brand".localized
        vehicleSubBrandLabel.text = "Select Vehicle Model".localized
        vehicleColorLabel.text = "Select Vehicle Color".localized
        vehicleNumberLabel.text = "Enter Vehicle Number".localized
        selectDivisionStateLabel.text = "Select Division/State".localized
        DLNumberLabel.text = "Enter Myanmar Driving License Number".localized

        self.saveBtn.setTitle("Save".localized, for: .normal)
        if UserDefaults.standard.value(forKey: "currentLanguage") as! String == "en" {
            self.isBurmese = false
        }
        else {
            self.isBurmese = true
        }
        vehicleTypeArrowImg.isHighlighted = false
        self.allVehicleCategoriesViewModel = AllVehicleCategoriesViewModel(data: [])
        callingHttp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.applicationDidTimeout(notification:)),
                                               name: .appTimeout,
                                               object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .appTimeout, object: nil)
    }
    
    
    @IBAction func onClickCrossButton(_ sender: Any) {
        crossButtonOutlet.isHidden = true
        vehicleNumberTextField.text = ""
        DLNumberView.isHidden = true
        divisionStateView.isHidden = true
        saveBtn.isHidden = true
        vehicleNumberTextField.keyboardType = .default
        vehicleNumberTextField.resignFirstResponder()
        
    }
    
    @objc func applicationDidTimeout(notification: NSNotification) {
        println_debug(OKSessionValidation.isMainAppSessionExpired)
        UserDefaults.standard.set(true, forKey: "isLogOut")
        DispatchQueue.main.async {

        alertViewObj.wrapAlert(title:"", body:"Session expired".localized, img: nil)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            OKPayment.main.authenticate(screenName: "AddVehicleScreen", delegate: self)
        })
            alertViewObj.showAlert(controller: self)
        }
        
    }
    
    @IBAction func dismissControllerAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func callingHttp() {
        if self.whichApiCall == "allVehicleCategories" {
            if appDelegate.checkNetworkAvail() {
                let type:Int = 1
                let mobStr:String = UserModel.shared.mobileNo
                var urlString =  Url.AllVehicleCategories + "MobileNumber=" + mobStr + "&SimId="
                var newURL = uuid + "&MsId=" + msid + "&OsType=" + "\(type)" + "&Otp=" + uuid
                var final = urlString + newURL
                final = final.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                let url = getUrl(urlStr: final, serverType: .serverApp)
                println_debug(url)
                let params = [String: AnyObject]() as AnyObject
                web.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", mScreen: "allVehicleCategories")
                DispatchQueue.main.async {
                    progressViewObj.showProgressView()
                }
            }else{
                DispatchQueue.main.async {

                alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                }
                alertViewObj.showAlert(controller: UIViewController.init())
                }
            }
        }
        else if self.whichApiCall == "addVehicle" {
            if appDelegate.checkNetworkAvail() {
                DispatchQueue.main.async {
                    progressViewObj.showProgressView()
                }
                var urlString =  Url.AddCarAPI
                urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                let url = getUrl(urlStr: urlString, serverType: .serverApp)
                println_debug(url)
                web.genericClassReg(url: url, param: addVehicleDICT , httpMethod: "POST", mScreen: "allVehicleCategories")
            } else{
                println_debug("network is not avialable")
                DispatchQueue.main.async {

                alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                }
                alertViewObj.showAlert(controller: UIViewController.init())
                }
            }
        }
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        if self.whichApiCall == "addVehicle" {
            DispatchQueue.main.async {
                progressViewObj.removeProgressView()
            }
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    println_debug(dict)
                    if let dic = dict as? Dictionary<String,Any> {
                        println_debug(dic)
                        if dic["Code"] as? NSNumber == 200 {
                            if let stringData = String(data: data, encoding: String.Encoding.utf8) {
                                let dict = OKBaseController.convertToDictionary(text: stringData)
                                let jsonData = JSON(dict!)
                                println_debug(jsonData)
                                DispatchQueue.main.async {
                                alertViewObj.wrapAlert(title:"", body:"Vehicle Added Successfully".localized, img: #imageLiteral(resourceName: "Car_Add"))
                                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                })
                                    alertViewObj.showAlert(controller: self)
                                    self.navigationController?.popViewController(animated: true)
                                }
                            }
                        }else {
                            DispatchQueue.main.async {
                                self.showAlert(alertTitle: "", alertBody: dic["Msg"] as! String , alertImage: #imageLiteral(resourceName: "r_user"))}
                          /*
                          if dic["Msg"].contains("Technical failure")
                          {
                            showAlert(alertTitle: "", alertBody: "Please Try Again!".localized, alertImage: #imageLiteral(resourceName: "r_user"))
                          }
                          else
                          {
                            showAlert(alertTitle: "", alertBody: dic["Msg"] as! String , alertImage: #imageLiteral(resourceName: "r_user"))
                          }*/
                        }
                    }
                }
            } catch {
                DispatchQueue.main.async {
                    self.showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))}
            }
        }
        else {
            DispatchQueue.main.async {
                progressViewObj.removeProgressView()
            }
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,Any> {
                        if dic["Code"] as? NSNumber == 200 {
                            if let stringData = String(data: data, encoding: String.Encoding.utf8) {
                                let dict = OKBaseController.convertToDictionary(text: stringData)
                                let jsonData = JSON(dict!)
                                println_debug(jsonData)
                                let categoryData = jsonData["Data"].stringValue
                                let categorydDict = OKBaseController.convertToArrDictionary(text: categoryData)
                                let categoryjsonData = JSON(categorydDict!)
                                println_debug(categoryjsonData)
                                self.allVehicleCategoriesViewModel = AllVehicleCategoriesViewModel(data: categoryjsonData)
                                DispatchQueue.main.async {
                                    self.pickerView.delegate = self
                                }
                            }
                        }else {
                          
                            DispatchQueue.main.async {
                                self.showAlert(alertTitle: "", alertBody: dic["Msg"] as! String , alertImage: #imageLiteral(resourceName: "r_user"))}
                        }
                    }
                }
            } catch {
                DispatchQueue.main.async {
                    self.showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))}
            }
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        DispatchQueue.main.async {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
            alertViewObj.showAlert(controller: self)
        }
    }
    
    @IBAction func vehicleTypeEditingEnd(_ sender: SkyFloatingLabelTextField) {
        self.navigationController?.navigationBar.isUserInteractionEnabled = true
        let str:String = vehicleTypeLabel.text!
        self.vehicleTypePickerTextField.resignFirstResponder()
        vehicleTypePickerTextField.text = ""
        vehicleTypeLabel.text = "Select Vehicle Type".localized
        if str.count > 1 {
            vehicleTypeArrowImg.isHighlighted = false
            passStringForApi = "vehicleType"
            isSearchBarActive = false
            nextNavigationTitle = "Select Vehicle Sub-Category Type"
            self.performSegue(withIdentifier: "addVehicleToSelectVehicleCategorySegue", sender: self)
        }
        else {
            vehicleTypeArrowImg.isHighlighted = true
            self.saveBtn.isHidden = true
            selectBrandDetailsView.isHidden = true
            divisionStateView.isHidden = true
            DLNumberView.isHidden = true
        }
    }
    
    @IBAction func selectDivisionStateDidEnd(_ sender: SkyFloatingLabelTextField) {
        DLNumberTextField.keyboardType = .default
        if selectDivisionStateTextField.text != "" {
            DLNumberView.isHidden = false
            selectDivisionStateTextField.placeholderFont = UIFont(name: appFont, size: 17.0)
            selectDivisionStateTextField.placeholder = "Division/State".localized
        }
        else {
            selectDivisionStateTextField.placeholder = ""
            selectDivisionStateLabel.text = "Select Division/State".localized
            DLNumberView.isHidden = true
        }
    }
    
    @IBAction func DlNumberTFEditingChanged(_ sender: SkyFloatingLabelTextField) {
        if DLNumberTextField.text?.count == 0 {
            DLNumberLabel.text = "Enter Myanmar Driving License Number".localized
            DLNumberTextField.placeholder = ""
        }
        else {
            DLNumberLabel.text = ""
            DLNumberTextField.placeholder = "License Number".localized
        }
        if sender.text?.count == 1{
            sender.text?.append("/")
            firstSlash = true
        }
        if sender.text?.count == 7{
            DLNumberTextField.text?.append("/")
            secondSalsh = true
        }
        if sender.text?.count == 10{
            if (DLNumberTextField.text?.contains(find: "00000/00"))! {
                DLNumberTextField.text = DLNumberTextField.text?.replacingOccurrences(of: "00000/00", with: "")
            }
            else {
                DLNumberTextField.resignFirstResponder()
            }
        }
    }
    
    @IBAction func vehicleNumberEditingChanged(_ sender: SkyFloatingLabelTextField) {
        if sender.text?.count == 0 {
            vehicleNumberLabel.text = "Enter Vehicle Number".localized
            vehicleNumberTextField.placeholder = ""
            vehicleNumberTextField.isSelected = true
        }
        else {
            vehicleNumberTextField.placeholder = "Vehicle Number".localized
            vehicleNumberLabel.text = ""
            vehicleNumberTextField.isSelected = false
//            vehicleNumberTextField.allowedActions = [.copy, .cut]
//            vehicleNumberTextField.notAllowedActions = [.copy, .cut, .paste, .select , .selectAll]
        }
        if sender.text?.count == 2 {
            if vehicleNumberTextField.text != "ဟ/" {
                sender.text?.append("/")
            }
            firstSlashVehicleNumber = true
            vehicleNumberTextField.resignFirstResponder()
            vehicleNumberTextField.keyboardType = .numberPad
            vehicleNumberTextField.becomeFirstResponder()
        }
        if sender.text?.count == 7 {
            if (vehicleNumberTextField.text?.contains(find: "0000"))! {
                vehicleNumberTextField.text = vehicleNumberTextField.text?.replacingOccurrences(of: "0000", with: "")
            }
            else {
                divisionStateView.isHidden = false
                vehicleNumberTextField.resignFirstResponder()
            }
        }
        else {
            saveBtn.isHidden = true
            selectDivisionStateTextField.text = ""
            selectDivisionStateLabel.text = "Select Division/State".localized
            selectDivisionStateTextField.placeholder = ""
            DLNumberTextField.text = ""
            divisionStateView.isHidden = true
            DLNumberView.isHidden = true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addVehicleToSelectVehicleColorSegue" {
            let vc = segue.destination as! UINavigationController
            let rootViewController = vc.viewControllers.first as! SelectVehicleColor
            rootViewController.delegate = self
        }
        else if segue.identifier == "addVehicleToSelectVehicleCategorySegue" {
            let vc = segue.destination as! UINavigationController
            let controller = vc.viewControllers.first as! SelectVehicleCategory
            controller.delegate = self
            controller.ind = self.ind
            controller.whichApiCall = passStringForApi
            controller.brandId = self.brandId
            controller.navigationTitle = self.nextNavigationTitle
            controller.isSearchBarActive = isSearchBarActive
            controller.allVehicleCategoriesViewModel = self.allVehicleCategoriesViewModel
            }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return allVehicleCategoriesViewModel.getAllVehicleCategoriesData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        if appDel.currentLanguage == "my"{
//            return  allVehicleCategoriesViewModel.getAllVehicleCategoriesData[row].categoryNameBurmese
//        }else{
//            return   allVehicleCategoriesViewModel.getAllVehicleCategoriesData[row].categoryName
//        }
        return self.isBurmese ? allVehicleCategoriesViewModel.getAllVehicleCategoriesData[row].categoryNameBurmese : allVehicleCategoriesViewModel.getAllVehicleCategoriesData[row].categoryName
    }

    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = (view as? UILabel) ?? UILabel()
        label.textColor = .black
        label.textAlignment = .center
      //  label.font = UIFont(name: appFont, size: 20)
        // where data is an Array of String
            label.text = self.isBurmese ? allVehicleCategoriesViewModel.getAllVehicleCategoriesData[row].categoryNameBurmese : allVehicleCategoriesViewModel.getAllVehicleCategoriesData[row].categoryName
//        label.text = self.isBurmese ? allVehicleCategoriesViewModel.getAllVehicleCategoriesData[row].categoryNameBurmese : allVehicleCategoriesViewModel.getAllVehicleCategoriesData[row].categoryName
        return label
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.ind = row
        vehicleTypeLabel.text = self.isBurmese ? allVehicleCategoriesViewModel.getAllVehicleCategoriesData[row].categoryNameBurmese : allVehicleCategoriesViewModel.getAllVehicleCategoriesData[row].categoryName
//        vehicleTypePickerTextField.text = self.isBurmese ? allVehicleCategoriesViewModel.getAllVehicleCategoriesData[row].categoryNameBurmese : allVehicleCategoriesViewModel.getAllVehicleCategoriesData[row].categoryName
        
        
        
//        vehicleCategoryName = self.isBurmese ? allVehicleCategoriesViewModel.getAllVehicleCategoriesData[row].categoryNameBurmese : allVehicleCategoriesViewModel.getAllVehicleCategoriesData[row].categoryName
      vehicleCategoryName = allVehicleCategoriesViewModel.getAllVehicleCategoriesData[row].categoryName
        vehicleCategoryId = allVehicleCategoriesViewModel.getAllVehicleCategoriesData[row].categoryId
    }
    
    func getVehicleColor(colorName: String) {
        self.vehicleColorTextField.text = "\(colorName)"
        self.vehicleColorName = colorName
        if colorName == "" {
            vehicleColorLabel.text = "Select Vehicle Color".localized
        }
        else {
            vehicleColorLabel.text = ""
            vehicleColorTextField.placeholder = "Vehicle Color".localized
        }
    }

  func getVehicleSubCategory(subCategoryBurmeseName: String , subCategoryName: String, vehicleTypeId: String) {
        self.vehicleTypePickerTextField.text = "\(subCategoryBurmeseName)"
        vehicleTypeLabel.font = UIFont(name: "Zawgyi-One", size: appFontSize)
        vehicleTypeLabel.text = "\(subCategoryBurmeseName)"
        self.vehicleTypeName = subCategoryName
        self.vehicleTypeId = vehicleTypeId
        self.vehicleTypePickerTextField.resignFirstResponder()
        if subCategoryName.count > 1 {
            selectBrandDetailsView.isHidden = false
            vehicleTypePickerTextField.placeholder = "Vehicle Type".localized
        }
        else {
            self.saveBtn.isHidden = true
            selectBrandDetailsView.isHidden = true
            divisionStateView.isHidden = true
            DLNumberView.isHidden = true
            self.vehicleTypePickerTextField.text = ""
            vehicleTypeLabel.text = "Select Vehicle Type".localized
        }
        let vehicleTypeLableHeight:CGFloat = vehicleTypeLabel.bounds.size.height
        vehicleTypeHeightConstraint.constant = 62
        if vehicleTypeLableHeight > 20 {
            vehicleTypeHeightConstraint.constant = 80
            vehicleTypeTFHeightConstraint.constant = 54
        }
        else {
            vehicleTypeHeightConstraint.constant = 62
            vehicleTypeTFHeightConstraint.constant = 46
        }
        if subCategoryName == "Three wheel motorcycle " || subCategoryName == "Trailer jeep" || subCategoryName == "သံုးဘီးဆိုင္ကယ္ " || subCategoryName == "လယ္ထြန္စက္ " {
            vehicleNumberTextField.text = "ဟ/"
            vehicleNumberLabel.text = ""
            vehicleNumberTextField.placeholder = "Vehicle Number".localized
        }
        else {
            vehicleNumberTextField.keyboardType = .default
            vehicleNumberTextField.becomeFirstResponder()
        }
    }

    func getBrandCategory(brandCategory: String, brandId: String) {
        self.vehicleSubBrandTextField.text = ""
        self.vehicleBrandTextField.text = "\(brandCategory)"
        self.brandId = brandId
        if brandCategory == "" {
            vehicleBrandLabel.text = "Select Vehicle Brand".localized
        }
        else {
            vehicleBrandLabel.text = ""
            vehicleBrandTextField.placeholder = "Vehicle Brand".localized
            vehicleSubBrandLabel.text = "Select Vehicle Model".localized
            vehicleSubBrandTextField.placeholder = ""
        }
    }

    func getSubBrandCategory(subBrandCategory: String, modelId: String) {
        self.vehicleSubBrandTextField.text = "\(subBrandCategory)"
        self.vehicleModelId = modelId
        if subBrandCategory == "" {
            vehicleSubBrandLabel.text = "Select Vehicle Model".localized
        }
        else {
            vehicleSubBrandLabel.text = ""
            vehicleSubBrandTextField.placeholder = "Vehicle Model".localized
        }
    }
    
    func getDivisionState(divisionState: String) {
        self.selectDivisionStateTextField.text = "\(divisionState)"
        self.saveBtn.isHidden = false
        if divisionState != "" {
            DLNumberView.isHidden = false
            selectDivisionStateTextField.placeholder = "Division/State".localized
            selectDivisionStateLabel.text = ""
            DLNumberTextField.text = ""
            DLNumberLabel.text = "Enter Myanmar Driving License Number".localized
            DLNumberTextField.placeholder = ""
        }
        else {
            DLNumberView.isHidden = true
            selectDivisionStateLabel.text = ""
            selectDivisionStateTextField.placeholder = ""
            DLNumberLabel.text = "License Number".localized
            DLNumberTextField.placeholder = ""
        }
    }
    
    @IBAction func vehicleNumberEndEditing(_ sender: SkyFloatingLabelTextField) {
        self.view.endEditing(true)
    }
    
    @IBAction func DLNumberEndEditing(_ sender: SkyFloatingLabelTextField) {
        self.DLNumberTextField.resignFirstResponder()
    }

    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == vehicleTypePickerTextField {
//            self.view.endEditing(true)
            self.navigationController?.navigationBar.isUserInteractionEnabled = false
            vehicleTypePickerTextField.inputView = pickerView
            self.pickerView.selectRow(0, inComponent: 0, animated: true)
            if appDelegate.checkNetworkAvail() {
                vehicleTypeArrowImg.isHighlighted = true
                vehicleTypeLabel.textColor = UIColor.darkGray
                if allVehicleCategoriesViewModel.getAllVehicleCategoriesData.count > 0 {
                    vehicleTypeLabel.text = self.isBurmese ? allVehicleCategoriesViewModel.getAllVehicleCategoriesData[0].categoryNameBurmese : allVehicleCategoriesViewModel.getAllVehicleCategoriesData[0].categoryName
                    vehicleTypePickerTextField.text = self.isBurmese ? allVehicleCategoriesViewModel.getAllVehicleCategoriesData[0].categoryNameBurmese : allVehicleCategoriesViewModel.getAllVehicleCategoriesData[0].categoryName
//                    vehicleCategoryName = allVehicleCategoriesViewModel.getAllVehicleCategoriesData[0].categoryName
                  vehicleCategoryName = allVehicleCategoriesViewModel.getAllVehicleCategoriesData[0].categoryName
                    vehicleCategoryId = allVehicleCategoriesViewModel.getAllVehicleCategoriesData[0].categoryId
                }
                vehicleTypePickerTextField.placeholder = "Vehicle Type".localized
            }
            else {
                println_debug("network is not avialable")
                DispatchQueue.main.async {

                alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                }
                alertViewObj.showAlert(controller: UIViewController.init())
                }
            }
            self.ind = 0
            vehicleBrandTextField.text = ""
            vehicleSubBrandTextField.text = ""
            vehicleColorTextField.text = ""
            vehicleNumberTextField.text = ""
            selectBrandDetailsView.isHidden = true
            divisionStateView.isHidden = true
            DLNumberView.isHidden = true
            saveBtn.isHidden = true
            if vehicleTypeLabel.text?.count == 0 {
                vehicleTypeLabel.text = "Select Vehicle Type".localized
            }else {
                if allVehicleCategoriesViewModel.getAllVehicleCategoriesData.count > 0 {

                vehicleTypeLabel.text = self.isBurmese ? allVehicleCategoriesViewModel.getAllVehicleCategoriesData[0].categoryNameBurmese : allVehicleCategoriesViewModel.getAllVehicleCategoriesData[0].categoryName
                vehicleTypePickerTextField.text = self.isBurmese ? allVehicleCategoriesViewModel.getAllVehicleCategoriesData[0].categoryNameBurmese : allVehicleCategoriesViewModel.getAllVehicleCategoriesData[0].categoryName
                }
                vehicleTypePickerTextField.placeholder = "Vehicle Type".localized
                
            }
            vehicleBrandLabel.text = "Select Vehicle Brand".localized
            vehicleSubBrandLabel.text = "Select Vehicle Model".localized
            vehicleColorLabel.text = "Select Vehicle Color".localized
            vehicleNumberLabel.text = "Enter Vehicle Number".localized
            selectDivisionStateLabel.text = "Select Division/State".localized
            DLNumberLabel.text = "Enter Myanmar Driving License Number".localized
            
            vehicleTypePickerTextField.placeholder = ""
            vehicleBrandTextField.placeholder = ""
            vehicleSubBrandTextField.placeholder = ""
            vehicleColorTextField.placeholder = ""
            vehicleNumberTextField.placeholder = ""
            selectDivisionStateTextField.placeholder = ""
            DLNumberTextField.placeholder = ""
            return true
        }
        else if textField == vehicleBrandTextField {
            self.view.endEditing(true)
            passStringForApi = "vehicleBrand"
            nextNavigationTitle = "Search Vehicle Brand"
            isSearchBarActive = true
            self.performSegue(withIdentifier: "addVehicleToSelectVehicleCategorySegue", sender: self)

            return true
        }
        else if textField == vehicleSubBrandTextField {
            self.view.endEditing(true)
            self.vehicleSubBrandTextField.resignFirstResponder()
            let str:String = vehicleBrandTextField.text!
            if str.count < 1 {
                alertViewObj.wrapAlert(title: "", body: "Select Vehicle Brand".localized, img: #imageLiteral(resourceName: "alert-icon") )
                alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {})
                alertViewObj.showAlert(controller: self)
            }
            else {
                passStringForApi = "vehicleSubBrand"
                nextNavigationTitle = "Search Vehicle Model"
                isSearchBarActive = true
                self.performSegue(withIdentifier: "addVehicleToSelectVehicleCategorySegue", sender: self)
            }
            return true
        }
        else if textField == vehicleColorTextField {
            self.view.endEditing(true)
            self.performSegue(withIdentifier: "addVehicleToSelectVehicleColorSegue", sender: self)
            return true
        }
        else if textField == selectDivisionStateTextField {
            self.view.endEditing(true)
            passStringForApi = "division/State"
            nextNavigationTitle = "Select Division/State"
            isSearchBarActive = false
            self.selectDivisionStateTextField.resignFirstResponder()
            self.performSegue(withIdentifier: "addVehicleToSelectVehicleCategorySegue", sender: self)
            return true
        }
        else{
            return true
        }
    }
    
    @IBAction func vehicleNumberEditingBegin(_ sender: SkyFloatingLabelTextField) {
        if (vehicleNumberTextField.text?.contains(find: "ဟ/"))! {
            vehicleNumberTextField.keyboardType = .numberPad
            vehicleNumberTextField.becomeFirstResponder()
        }
    }
    
    @IBAction func addVehicleSaveAction(_ sender: UIButton) {
        allVehicleServerMutableArr = []
        allVehicleServerMutableArr.addObjects(from: copyAllVehicleServerMutableArr as! [Any])
        println_debug(allVehicleServerMutableArr)
        let addVehicleDict = self.inputDicForAddNewVehicle()
        self.addVehicleDICT = addVehicleDict
        println_debug(self.addVehicleDICT)
        if isSameDivisionExists == true && isSameVehicleNumberExists == true {
            DispatchQueue.main.async {
                self.showAlert(alertTitle: "", alertBody: "Vehicle with same vehicle number and division is already added".localized , alertImage: #imageLiteral(resourceName: "r_user"))}
        }
        else {
            if UserLogin.shared.loginSessionExpired {
                OKPayment.main.authenticate(screenName: "AddVehicleSubmit", delegate: self)
            } else{
                self.whichApiCall = "addVehicle"
                self.callingHttp()
            }
        }
    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful {
            if screen == "AddVehicleSubmit" {
                self.whichApiCall = "addVehicle"
                self.callingHttp()
            }
            else if screen == "AddVehicleScreen" {
                println_debug("Recieved Token")
            }

        }
    }
    
    func inputDicForAddNewVehicle() -> [String : Any] {
        let vehicleFullName = vehicleTypeName.replacingOccurrences(of: "/", with:"-") //vehicleCategoryName + "-" + vehicleTypeName
        let vehicleNumber = vehicleNumberTextField.text
        let divisionCode = selectDivisionStateTextField.text
        let licenceNumber = DLNumberTextField.text
        let createdDate = self.getCurrentDate()
        let vehicleNumberNewString = vehicleNumber?.replacingOccurrences(of: "/", with: "-")
        var isdefaultCar:Int = 0
        if self.allVehicleServerMutableArr.count == 0 {
            isdefaultCar = 1
        }
        let appServerDic = [
            "CarName" : vehicleFullName,
            "CarNumber" : vehicleNumberNewString ?? "",
            "IsDefaultCar" : isdefaultCar,
            "DivisionCode" : divisionCode ?? "",
            "ClassificationId" : "",
            "CategoryId" : vehicleCategoryId,
            "VehicleTypeId" : vehicleTypeId,
            "LicenseNo" : licenceNumber ?? "",
            "VehicleBrandId" : brandId,
            "VehicleModelId" : vehicleModelId,
            "Vehiclecolor" : vehicleColorName,
            "CreatedDate" : createdDate
            ] as Dictionary<String,Any>
//        println_debug("appserverDic ",appServerDic)
        allVehicleServerMutableArr.add(appServerDic)
//        allVehicleServerMutableArr.setValuesForKeys(appServerDic)
        println_debug(allVehicleServerMutableArr)
        let jsonVehicleServerArr = JSON(self.allVehicleServerMutableArr)
        var vehicleData:String = ""
        var defaultCar:String = ""
        for i in 0..<jsonVehicleServerArr.count {
            var str:String = jsonVehicleServerArr[i]["DivisionCode"].stringValue + " " + jsonVehicleServerArr[i]["CarNumber"].stringValue + "-" + jsonVehicleServerArr[i]["CarName"].stringValue
            if i < jsonVehicleServerArr.count - 1 {
                str.append(",")
            }
            vehicleData.append(str)
            if jsonVehicleServerArr[i]["IsDefaultCar"].boolValue == true {
                defaultCar = jsonVehicleServerArr[i]["DivisionCode"].stringValue + " " + jsonVehicleServerArr[i]["CarNumber"].stringValue + "-" + jsonVehicleServerArr[i]["CarName"].stringValue
            }
        }
        if jsonVehicleServerArr.count > 1 {
            for i in 0..<jsonVehicleServerArr.count - 1 {
                if divisionCode == jsonVehicleServerArr[i]["DivisionCode"].stringValue {
                    isSameDivisionExists = true
                    if vehicleNumberNewString == jsonVehicleServerArr[i]["CarNumber"].stringValue {
                        isSameVehicleNumberExists = true
                        break
                    }
                    else {
                        isSameVehicleNumberExists = false
                    }
                }
                else {
                    isSameDivisionExists = false
                }
            }
        }
        let type:Int = 1
        let mobStr:String = UserModel.shared.mobileNo
        let inputDic = [
            "MobileNumber" : mobStr,
            "SimId" : uuid,
            "MsId" : msid,
            "OsType" : "\(type)",
            "Otp" : uuid,
            "Password" : ok_password ?? "",
            "SecureToken" : UserLogin.shared.token,
            "EstelCarData" : vehicleData,
            "DefaultCar" : defaultCar,
            "AppServerCarDetails" : self.allVehicleServerMutableArr,
            "AssetCategoryType" : "TollGate"
            ] as [String : Any]
//        println_debug("inininin : ",inputDic)
        return inputDic
    }
    
    func getCurrentDate() -> String {
        let date = Date()
        let calender = Calendar.current
        let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
        let year = components.year
        let month = components.month
        let day = components.day
        let hour = components.hour
        let minute = components.minute
        let second = components.second
        let today_string = String(month!) + "/" + String(day!)  + "/" + String(year!) +  " " + String(hour!)  + ":" + String(minute!) + ":" +  String(second!)
        return today_string
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == DLNumberTextField {
            let str = DLNumberTextField.text!

            if range.location > 9{
                DLNumberTextField.resignFirstResponder()
                return false
            }
            if DLNumberTextField.text?.count == 10 {
                if range.location == 9 {
                    return true
                }
                else {
                    return false
                }
            }
            if str.count > 1 && range.location < 2 {
                if str.count == 2 {
                }
                else {
                    return false
                }
            }
            if range.location == 0{
                let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERSLocal).inverted
                let filteredStr = string.components(separatedBy: cs).joined(separator: "")
                guard string == filteredStr else {
                    return false
                }
                if string >= "A" && string <= "Z"{
                    if (DLNumberTextField.text?.contains(find: "ဟ/"))! {
                        return false
                    }
                    else {
                        firstChar = true
                        DLNumberTextField.resignFirstResponder()
                        DLNumberTextField.keyboardType = .numberPad
                        DLNumberTextField.becomeFirstResponder()
                        return true
                    }
                }
                else {
                    if firstChar == true {
                        firstChar = false
                        DLNumberTextField.resignFirstResponder()
                        DLNumberTextField.keyboardType = .default
                        DLNumberTextField.becomeFirstResponder()
                        return true
                    }
                    else {
                        if range.location == 0 {
                            return true
                        }
                        else {
                            return false
                        }
                    }
                }
            }
            if range.location == 1 {
                if firstSlash {
                    firstSlash = false
                    DLNumberTextField.text?.removeLast()
                    return false
                }
                else{
                    firstSlash = true
                    DLNumberTextField.text?.append("/")
                    return true
                }
            }
            if range.location == 7 {
                if secondSalsh {
                    secondSalsh = false
                    DLNumberTextField.text?.removeLast()
                    return false
                }
                else {
                    secondSalsh = true
                    DLNumberTextField.text?.append("/")
                    return true
                }
            }
        }
        else if textField == vehicleNumberTextField {
            let str = vehicleNumberTextField.text!.uppercased();
//            (textField.text! as NSString).replacingCharacters(in: range, with: string.uppercased())
        
            guard let tfText   = textField.text else { return false }
            let text     = (tfText as NSString)
            let txt      = text.replacingCharacters(in: range, with: string)
            
          
            if txt.count>=1{
                crossButtonOutlet.isHidden = false
            }else{
                crossButtonOutlet.isHidden = true
            }
            

            
            if range.location > 6 {
                vehicleNumberTextField.resignFirstResponder()
                return false
            }
            if vehicleNumberTextField.text?.count == 7 {
                if range.location == 6 {
                    return true
                }
                else {
                    return false
                }
            }
            if str.count > 2 && range.location < 3 {
                if str.count == 3 {
                }
                else {
                    return false
                }
            }
            if range.location >= 0 {
                let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERSLocal).inverted
                let filteredStr = string.components(separatedBy: cs).joined(separator: "")
                guard string == filteredStr else {
                    return false
                }
            }
            if (vehicleNumberTextField.text?.contains(find: "ဟ/"))! {
                if range.location == 1 {
                    if vehicleNumberTextField.text == "ဟ/" {
                        return false
                    }
                }
            }
            if !((vehicleNumberTextField.text?.contains(find: "ဟ/"))!) {
                if range.location == 1{
                    if (string >= "a" && string <= "z") || (string >= "A" && string <= "Z") {
                        firstCharVehicleNumber = true
                        vehicleNumberTextField.resignFirstResponder()
                        vehicleNumberTextField.keyboardType = .numberPad
                        vehicleNumberTextField.becomeFirstResponder()
                        return true
                    }
                    else {
                        if firstCharVehicleNumber == true {
                            firstCharVehicleNumber = false
                            vehicleNumberTextField.resignFirstResponder()
                            vehicleNumberTextField.keyboardType = .default
                            vehicleNumberTextField.becomeFirstResponder()
                            return true
                        }
                        else {
                            return false
                        }
                    }
                }
                if range.location == 2 {
                    if firstSlashVehicleNumber {
                        firstSlashVehicleNumber = false
                        vehicleNumberTextField.text?.removeLast()
                        return false
                    }
                    else{
                        firstSlashVehicleNumber = true
                        vehicleNumberTextField.text?.append("/")
                        return true
                    }
                }
            }
        }
        return true
    }
    
}

