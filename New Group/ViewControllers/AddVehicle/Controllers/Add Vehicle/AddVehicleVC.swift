//
//  AddVehicleVC.swift
//  OK
//
//  Created by Shobhit Singhal on 3/7/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Rabbit_Swift


protocol defaultVehicleDelegate: class {
    func slelectedVehicle(defaultVehicleStr: String, categoryId: String)
    func noVehicleFound()
}


class AddVehicleVC: OKBaseController, UITableViewDelegate, UITableViewDataSource, WebServiceResponseDelegate, BioMetricLoginDelegate {
    private var longPressGestureRecognizer:UILongPressGestureRecognizer!

    @IBOutlet weak var vehiclesTable : UITableView!
    @IBOutlet weak var infoView : UIView!
    @IBOutlet weak var infoLabel : UILabel!{
        didSet{
            self.infoLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var addBtn : UIButton!{
        didSet{
            self.addBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    var addVehicleViewsModel:AddVehicleViewsModel!
    var whichApiCall:String = ""
    let web = WebApiClass()
    var allVehicleServerArr:NSArray = []
    var allVehicleServerMutableArr:NSMutableArray = []
    var deleteVehicleDICT = [String: Any]()
    var selectedInd:Int = 0
    var deleteButtonInd:Int = 0
    var isRedirectFromAddNewVehicle:Bool = false
    var isUpdateVehicle:Bool = false
    var defautltVehicleStr:String = ""
    weak var delegate: defaultVehicleDelegate?
    var isFromPayto : Bool = false
    var vehicleDetail: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //Long Press
        longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGestureRecognizer.minimumPressDuration = 0.2
        longPressGestureRecognizer.numberOfTapsRequired = 1
        longPressGestureRecognizer.numberOfTouchesRequired = 1
        self.vehiclesTable.addGestureRecognizer(longPressGestureRecognizer)
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.title = "Select Your Vehicle".localized
        self.infoLabel.text = "No Vehicle Found".localized
        vehiclesTable.register(UINib(nibName: "VehicleDetailCell", bundle: nil), forCellReuseIdentifier: "vehiclecellidentifier")
        self.infoView.isHidden = true
        web.delegate = self
    }

    @objc func networkLost() {
        self.addBtn.isHidden = true
    }
    
    @objc func networkConnected() {
        self.addBtn.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(TopupMyDataPlanViewController.networkLost), name: .networkLost, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TopupMyDataPlanViewController.networkConnected), name: .networkConnected, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.applicationDidTimeout(notification:)),
                                               name: .appTimeout,
                                               object: nil)
        self.addBtn.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .appTimeout, object: nil)
    }

    @objc func handleLongPress(longPressGesture: UILongPressGestureRecognizer) {
        let p = longPressGesture.location(in: self.vehiclesTable)
        let indexPath = self.vehiclesTable.indexPathForRow(at: p)
        if indexPath == nil {
            print("Long press on table view, not row.")
        } else if longPressGesture.state == UIGestureRecognizer.State.began {
            print("Long press on row, at \(indexPath!.row)")
            
        }
    }
    
    @objc func applicationDidTimeout(notification: NSNotification) {
        println_debug(OKSessionValidation.isMainAppSessionExpired)
        DispatchQueue.main.async {
        UserDefaults.standard.set(true, forKey: "isLogOut")
        alertViewObj.wrapAlert(title:"", body:"Session expired".localized, img: nil)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            OKPayment.main.authenticate(screenName: "SelectYourVehicle", delegate: self)
        })
        
            alertViewObj.showAlert(controller: self)
        }

    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful {
            if screen == "SelectYourVehicle" {
                println_debug("Recieved Token")
            }
        }
    }


    override func viewDidAppear(_ animated: Bool) {
        self.addVehicleViewsModel = AddVehicleViewsModel(data: [])
        if self.addVehicleViewsModel.getVehicleData.count == 0 {
            self.addBtn.isHidden = true
        } else {
            self.addBtn.isHidden = false
        }
        defautltVehicleStr = ""
        whichApiCall = ""
        self.callingHttp()
    }

    func callingHttp() {
        if self.whichApiCall == "deleteVehicle" {
            if appDelegate.checkNetworkAvail() {
                DispatchQueue.main.async {
                    progressViewObj.showProgressView()
                }
                
                var urlString =  Url.AddCarAPI
                urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                let url = getUrl(urlStr: urlString, serverType: .serverApp)
                println_debug(url)
                web.genericClass(url: url, param: deleteVehicleDICT as AnyObject, httpMethod: "POST", mScreen: "updateVehicle")
            }else{
                println_debug("network is not avialable")
                DispatchQueue.main.async {
                alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                }
                    alertViewObj.showAlert(controller: UIViewController.init())}
            }
        } else if self.whichApiCall == "updateVehicle" {
            if appDelegate.checkNetworkAvail() {
                DispatchQueue.main.async {
                    progressViewObj.showProgressView()
                }
                
                var urlString =  Url.AddCarAPI
                urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                let url = getUrl(urlStr: urlString, serverType: .serverApp)
                println_debug(url)
                web.genericClassReg(url: url, param: deleteVehicleDICT , httpMethod: "POST", mScreen: "deleteVehicle")
            }else{
                vehiclesTable.isUserInteractionEnabled = false
                println_debug("network is not avialable")
                DispatchQueue.main.async {
                alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                }
                    alertViewObj.showAlert(controller: UIViewController.init())}
            }
        } else {
            if appDelegate.checkNetworkAvail() {
                let type:Int = 1
                let mobStr:String = UserModel.shared.mobileNo
                var urlString =  "\(Url.GetAllVehicleAPI)MobileNumber=\(mobStr)&SimId=\(uuid)&MsId=\(msid)&OsType=\(type)&Otp=\(uuid)"

                urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                let url = getUrl(urlStr: urlString, serverType: .serverApp)
                println_debug(url)
                let params = [String: AnyObject]() as AnyObject
                web.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", mScreen: "allVehicle")
                DispatchQueue.main.async {
                    progressViewObj.showProgressView()
                }
            } else {
                println_debug("network is not avialable")
                DispatchQueue.main.async {
                alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                }
                    alertViewObj.showAlert(controller: UIViewController.init())}
            }
        }
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        
        if self.whichApiCall == "deleteVehicle" {
            DispatchQueue.main.async {
                progressViewObj.removeProgressView()
            }
            do {
                if let data = json as? Data {
                    
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,Any> {
                        println_debug(dic)
                        if dic["Code"] as? NSNumber == 200 {
                            DispatchQueue.main.async {
                                self.addVehicleViewsModel = AddVehicleViewsModel(data: [JSON(self.allVehicleServerMutableArr)])
                                self.vehiclesTable.delegate = self
                                self.vehiclesTable.dataSource = self
                                self.vehiclesTable.reloadData()
                                if self.addVehicleViewsModel.getVehicleData.count > 0 {
                                    self.infoView.isHidden = true
                                } else {
                                    self.infoView.isHidden = false
                                    self.performSegue(withIdentifier: "allVehicleToAddNewVehicle", sender: self)
                                }
                                if self.addVehicleViewsModel.getVehicleData.count == 3 {
                                    self.addBtn.isHidden = true
                                } else {
                                    self.addBtn.isHidden = false
                                }
                                alertViewObj.wrapAlert(title:"", body:"Vehicle Deleted Successfully".localized, img: #imageLiteral(resourceName: "Car_Add"))
                                alertViewObj.addAction(title: "OK".localized, style: .target , action: {

                                })
                            }
                            
                            DispatchQueue.main.async {
                                alertViewObj.showAlert(controller: self)
//                                if self.deleteButtonInd > 0 {
                                    self.whichApiCall = ""
                                    self.selectedInd = 0
                                    self.deleteVehicleDICT = [:]
                                    let updateVehicleDict = self.callInputDicForUpdateVehicle()
                                    self.deleteVehicleDICT = updateVehicleDict
                                    self.whichApiCall = "updateVehicle"
                                    self.callingHttp()
//                                }
                            }

                        }else {
                            DispatchQueue.main.async {
                                self.showAlert(alertTitle: "", alertBody: dic["Msg"] as? String ?? "" , alertImage: #imageLiteral(resourceName: "r_user"))}
                        }
                    }
                }
            } catch {
                
                DispatchQueue.main.async {
                    self.showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))}
            }
        }
        else if self.whichApiCall == "updateVehicle" {
            DispatchQueue.main.async {
                self.vehiclesTable.isUserInteractionEnabled = true
                progressViewObj.removeProgressView()
            }
            do {
                if let data = json as? Data {
                    
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,Any> {
                        println_debug(dic)
                        if dic["Code"] as? NSNumber == 200 {
                            if self.isUpdateVehicle {
                                self.isUpdateVehicle = false
                                self.whichApiCall = ""
                                self.callingHttp()
                                DispatchQueue.main.async {
                                alertViewObj.wrapAlert(title:"", body:"Default Vehicle Changed Successfully".localized, img: #imageLiteral(resourceName: "Car_Add"))
                                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                    if self.isFromPayto {
                                        self.dismiss(animated: true, completion: nil)
                                    }
                                })
                                    alertViewObj.showAlert(controller: self)
                                }
                            }
                            else {
                                self.whichApiCall = ""
                                self.callingHttp()
                            }
                        }
                        else {
                            DispatchQueue.main.async {
                                self.showAlert(alertTitle: "", alertBody: dic["Msg"] as? String ?? "" , alertImage: #imageLiteral(resourceName: "r_user"))
                                
                            }
                        }
                    }
                }
            } catch {
                
                DispatchQueue.main.async {
                    self.showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))
                    
                }
            }
        }
        else {
            DispatchQueue.main.async {
                progressViewObj.removeProgressView()
            }
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,Any> {
                        println_debug(dic)
                        if dic["Code"] as? NSNumber == 200 {
                            DispatchQueue.main.async {
                                if let stringData = String(data: data, encoding: String.Encoding.utf8) {
                                    let dict = OKBaseController.convertToDictionary(text: stringData)
                                    let jsonData = JSON(dict!)
                                    println_debug(jsonData)
                                    let categoryData = jsonData["Data"].stringValue
                                    if let categorydDict = OKBaseController.convertToArrDictionary(text: categoryData) {
                                        let categoryjsonData = JSON(categorydDict)
                                        println_debug(categoryjsonData)
                                        var temp:[JSON] = []
                                        for i in 0..<categoryjsonData.count {
                                            if categoryjsonData[i]["IsDefaultCar"].boolValue == true {
                                                temp = categoryjsonData.arrayValue  //take out the one you want to move
                                                let tttt = temp.remove(at: i)  //take out the one you want to move
                                                temp.insert(tttt, at: 0)  //insert it where you want it to go
                                                println_debug("ssssssss")
                                                println_debug(tttt)
                                                println_debug("tttttttt")
                                                println_debug(temp)
                                            }
                                        }
                                        
                                        self.addVehicleViewsModel = AddVehicleViewsModel(data: temp)
                                        self.allVehicleServerArr = temp as NSArray
                                        let jsonAllVehicleServerArr = JSON(self.allVehicleServerArr)
                                        var jsonAllVehicleServerArr2 : JSON = []
                                        self.allVehicleServerArr = []
                                        for i in 0..<jsonAllVehicleServerArr.count {
                                            var dict = jsonAllVehicleServerArr[i].dictionary
                                            if dict!["VehicleBrandId"]?.stringValue == "" {
                                                dict?.updateValue(-1, forKey: "VehicleBrandId")
                                            }
                                            if dict!["VehicleModelId"]?.stringValue == "" {
                                                dict?.updateValue(-1, forKey: "VehicleModelId")
                                            }
                                            let dict2 = dict! as NSDictionary
                                            jsonAllVehicleServerArr2.arrayObject?.append(dict2)
                                        }
                                        self.allVehicleServerArr = jsonAllVehicleServerArr2.arrayObject! as NSArray
                                        self.allVehicleServerMutableArr = NSMutableArray(array: self.allVehicleServerArr)
                                        //                                    println_debug("ksksksks",self.allVehicleServerMutableArr)
                                        self.vehiclesTable.delegate = self
                                        self.vehiclesTable.dataSource = self
                                        self.vehiclesTable.reloadData()
                                        if self.addVehicleViewsModel.getVehicleData.count > 0 {
                                            self.infoView.isHidden = true
                                            if self.isFromPayto {
                                                let containVehicleStatus = self.addVehicleViewsModel.getVehicleData.contains(where: { (singleVehicle) -> Bool in
                                                    return "\(singleVehicle.divisionCode)-\(singleVehicle.carNumber)-\(singleVehicle.carName)" == (self.vehicleDetail ?? "")
                                                })
                                                if !containVehicleStatus {
                                                    let vehicle = "\(self.addVehicleViewsModel.getVehicleData[0].divisionCode)-\(self.addVehicleViewsModel.getVehicleData[0].carNumber)-\(self.addVehicleViewsModel.getVehicleData[0].carName)"
                                                    self.delegate?.slelectedVehicle(defaultVehicleStr: vehicle, categoryId: self.addVehicleViewsModel.getVehicleData[0].categoryId)
                                                }
                                            }
                                        } else {
                                            if self.isFromPayto {
                                                self.delegate?.noVehicleFound()
                                            }
                                            if self.isRedirectFromAddNewVehicle == false {
                                                self.performSegue(withIdentifier: "allVehicleToAddNewVehicle", sender: self)
                                                self.isRedirectFromAddNewVehicle = true
                                            }
                                            self.infoView.isHidden = false
                                        }
                                        if self.addVehicleViewsModel.getVehicleData.count == 3 {
                                            self.addBtn.isHidden = true
                                        } else {
                                            self.addBtn.isHidden = false
                                        }
                                    } else {
                                        DispatchQueue.main.async {
                                            self.showAlert(alertTitle: "", alertBody: dic["Msg"] as? String ?? "" , alertImage: #imageLiteral(resourceName: "r_user"))}
                                    }
                                }
                            }
                        }else {
                            DispatchQueue.main.async {
                                self.showAlert(alertTitle: "", alertBody: dic["Msg"] as? String ?? "" , alertImage: #imageLiteral(resourceName: "r_user"))}
                        }
                    }
                }
            } catch {
                DispatchQueue.main.async {
                    self.showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))}
            }
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        DispatchQueue.main.async {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        
            alertViewObj.showAlert(controller: self)
        }
    }
    
    @IBAction func dismissControllerAction(_ sender: UIBarButtonItem) {
        if let navigation = self.navigationController {
            navigation.dismiss(animated: true, completion: nil)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addVehicleViewsModel.getVehicleData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = vehiclesTable.dequeueReusableCell(withIdentifier: "vehiclecellidentifier") as! VehicleDetailCell

        cell.vehicleNameLabel.text = addVehicleViewsModel.getVehicleData[indexPath.row].divisionCode + " " + addVehicleViewsModel.getVehicleData[indexPath.row].carNumber + "-" + addVehicleViewsModel.getVehicleData[indexPath.row].carName
        
        if addVehicleViewsModel.getVehicleData[indexPath.row].isDefaultCar == true {
            cell.defaultColortLabel.isHidden = false
            defautltVehicleStr = cell.vehicleNameLabel.text!
        }
        else {
            cell.defaultColortLabel.isHidden = true
        }
//        if addVehicleViewsModel.getVehicleData[indexPath.row].carName.contains(find: "Truck") {
//            cell.vehicleImgView.image = #imageLiteral(resourceName: "add_Vech_Truck")
//        }
//        else if addVehicleViewsModel.getVehicleData[indexPath.row].carName.contains(find: "Bus") {
//            cell.vehicleImgView.image = #imageLiteral(resourceName: "add_Vech_Bus")
//        }
//        else if addVehicleViewsModel.getVehicleData[indexPath.row].carName.contains(find: "Three wheel motorcycle") || addVehicleViewsModel.getVehicleData[indexPath.row].carName.contains(find: "Trailer jeep") {
//            cell.vehicleImgView.image = #imageLiteral(resourceName: "add_Vech_Bike")
//        }
//        else {
//            cell.vehicleImgView.image = #imageLiteral(resourceName: "add_Vech_Car")
//        }
        
        if addVehicleViewsModel.getVehicleData[indexPath.row].categoryId.contains(find: "b43e2875-23e3-46ff-a994-2b6f6a3808af") {
            cell.vehicleImgView.image = #imageLiteral(resourceName: "add_Vech_Truck")
        }
        else if addVehicleViewsModel.getVehicleData[indexPath.row].categoryId.contains(find: "44c5a536-60dc-45d4-95dd-98ba8e6cc32f") {
            cell.vehicleImgView.image = #imageLiteral(resourceName: "add_Vech_Bus")
        }
        else if addVehicleViewsModel.getVehicleData[indexPath.row].categoryId.contains(find: "a0eedf0b-7c03-4aac-b0a5-ff45b8106651") {
            cell.vehicleImgView.image = #imageLiteral(resourceName: "add_Vech_Bike")
        }
        else {
            cell.vehicleImgView.image = #imageLiteral(resourceName: "add_Vech_Car")
        }

        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(AddVehicleVC.deleteVehicle(sender:)), for: .touchUpInside)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.isUserInteractionEnabled = false
        if isFromPayto {
            let vehicle = addVehicleViewsModel.getVehicleData[indexPath.row].divisionCode + "-" + addVehicleViewsModel.getVehicleData[indexPath.row].carNumber + "-" + addVehicleViewsModel.getVehicleData[indexPath.row].carName
            delegate?.slelectedVehicle(defaultVehicleStr: vehicle, categoryId: self.addVehicleViewsModel.getVehicleData[indexPath.row].categoryId)
            if addVehicleViewsModel.getVehicleData[indexPath.row].isDefaultCar != true {
                isUpdateVehicle = true
                selectedInd = indexPath.row
                self.deleteVehicleDICT = [:]
                let updateVehicleDict = self.callInputDicForUpdateVehicle()
                self.deleteVehicleDICT = updateVehicleDict
                self.whichApiCall = "updateVehicle"
                self.callingHttp()
            } else {
                self.dismiss(animated: true, completion: nil)
            }
            return
        }
        if addVehicleViewsModel.getVehicleData[indexPath.row].isDefaultCar != true {
            isUpdateVehicle = true
            selectedInd = indexPath.row
            self.deleteVehicleDICT = [:]
            let updateVehicleDict = self.callInputDicForUpdateVehicle()
            self.deleteVehicleDICT = updateVehicleDict
            self.whichApiCall = "updateVehicle"
            self.callingHttp()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
   @objc func deleteVehicle (sender: UIButton) {
    DispatchQueue.main.async {
    appDel.floatingButtonControl?.window.isHidden = true
    
    alertViewObj.wrapAlert(title:"", body:"Do you want to Delete Add Vehicle?".localized, img: #imageLiteral(resourceName: "Car_Remove"))
    alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
        appDel.floatingButtonControl?.window.isHidden = false
    })
    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        self.deleteButtonInd = sender.tag
        if appDelegate.checkNetworkAvail() {
            self.allVehicleServerMutableArr.removeObject(at: sender.tag)
            println_debug(self.allVehicleServerMutableArr)
            let deleteVehicleDict = self.callInputDicForDeleteExistVehicle()
            self.deleteVehicleDICT = [:]
            self.deleteVehicleDICT = deleteVehicleDict
            println_debug(self.deleteVehicleDICT)
            self.whichApiCall = "deleteVehicle"
            self.callingHttp()
        }else{
            println_debug("network is not avialable")
            DispatchQueue.main.async {
                alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                }
                alertViewObj.showAlert(controller: UIViewController.init())
            }
            
        }
        appDel.floatingButtonControl?.window.isHidden = false
    })
    
        alertViewObj.showAlert(controller: self)
    }

    }

    func callInputDicForUpdateVehicle() -> [String : Any] {
        var jsonVehicleServerArr = JSON(self.allVehicleServerMutableArr)
        var vehicleData:String = ""
        var defaultCar:String = ""
//        println_debug("ffffffff",self.allVehicleServerMutableArr)
        for i in 0..<jsonVehicleServerArr.count {
            var str:String = jsonVehicleServerArr[i]["DivisionCode"].stringValue + " " + jsonVehicleServerArr[i]["CarNumber"].stringValue + "-" + jsonVehicleServerArr[i]["CarName"].stringValue
            if i < jsonVehicleServerArr.count - 1 {
                str.append(",")
            }
            vehicleData.append(str)
            if jsonVehicleServerArr[i]["IsDefaultCar"].boolValue == true {
                defaultCar = jsonVehicleServerArr[i]["DivisionCode"].stringValue + " " + jsonVehicleServerArr[i]["CarNumber"].stringValue + "-" + jsonVehicleServerArr[i]["CarName"].stringValue
            }
            if selectedInd == i {
                jsonVehicleServerArr[i]["IsDefaultCar"].boolValue = true
            } else {
                jsonVehicleServerArr[i]["IsDefaultCar"].boolValue = false
            }
        }
        self.allVehicleServerArr = jsonVehicleServerArr.arrayObject! as NSArray
        self.allVehicleServerMutableArr = NSMutableArray(array: self.allVehicleServerArr)
        println_debug(self.allVehicleServerMutableArr)
        
        let type:Int = 1
        let mobStr:String = UserModel.shared.mobileNo
        let inputDic = [
            "MobileNumber" : mobStr,
            "SimId" : uuid,
            "MsId" : msid,
            "OsType" : "\(type)",
            "Otp" : uuid,
            "Password" : ok_password ?? "",
            "SecureToken" : UserLogin.shared.token,
            "EstelCarData" : vehicleData,
            "DefaultCar" : defaultCar,
            "AppServerCarDetails" : self.allVehicleServerMutableArr,
            "AssetCategoryType" : "TollGate"
            ] as [String : Any]
        return inputDic
    }
    
    func callInputDicForDeleteExistVehicle() -> [String : Any] {
        let jsonVehicleServerArr = JSON(self.allVehicleServerMutableArr)
        var vehicleData:String = ""
        var defaultCar:String = ""
        for i in 0..<jsonVehicleServerArr.count {
            var str:String = jsonVehicleServerArr[i]["DivisionCode"].stringValue + " " + jsonVehicleServerArr[i]["CarNumber"].stringValue + "-" + jsonVehicleServerArr[i]["CarName"].stringValue
            if i < jsonVehicleServerArr.count - 1 {
                str.append(",")
            }
            vehicleData.append(str)
            if jsonVehicleServerArr[i]["IsDefaultCar"].boolValue == true {
                defaultCar = jsonVehicleServerArr[i]["DivisionCode"].stringValue + " " + jsonVehicleServerArr[i]["CarNumber"].stringValue + "-" + jsonVehicleServerArr[i]["CarName"].stringValue
            }
        }

        let type:Int = 1
        let mobStr:String = UserModel.shared.mobileNo
        let inputDic = [
            "MobileNumber" : mobStr,
            "SimId" : uuid,
            "MsId" : msid,
            "OsType" : "\(type)",
            "Otp" : uuid,
            "Password" : ok_password ?? "",
            "SecureToken" : UserLogin.shared.token,
            "EstelCarData" : vehicleData,
            "DefaultCar" : defaultCar,
            "AppServerCarDetails" : self.allVehicleServerMutableArr,
            "AssetCategoryType" : "TollGate"
            ] as [String : Any]
        return inputDic
    }
    
    @IBAction func addNewVehicleAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "allVehicleToAddNewVehicle", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "allVehicleToAddNewVehicle" {
            let controller = segue.destination as! AddNewVehicle
            controller.allVehicleServerMutableArr = self.allVehicleServerMutableArr
        }
    }
    
}
