//
//  AddVehicleModel.swift
//  OK
//
//  Created by Shobhit Singhal on 3/7/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class AddVehicleModel: NSObject {
    var id:String = ""
    var carName:String = ""
    var carNumber:String = ""
    var categoryId:String = ""
    var classificationId:String = ""
    var vehicleTypeId:String = ""
    var divisionCode:String = ""
    var licenseNumber:String = ""
    var isDefaultCar:Bool!
    var vehicleBrandId:String = ""
    var vehicleModelId:String = ""
    var vehicleColor:String = ""
    var createDate:String = ""

    init(data: JSON) {
        self.id = data["Id"].stringValue
        self.carName = data["CarName"].stringValue
        self.carNumber = data["CarNumber"].stringValue
        self.categoryId = data["CategoryId"].stringValue
        self.classificationId = data["ClassificationId"].stringValue
        self.vehicleTypeId = data["VehicleTypeId"].stringValue
        self.divisionCode = data["DivisionCode"].stringValue
        self.licenseNumber = data["LicenseNo"].stringValue
        self.isDefaultCar = data["IsDefaultCar"].boolValue
        self.vehicleBrandId = data["VehicleBrandId"].stringValue
        self.vehicleModelId = data["VehicleModelId"].stringValue
        self.vehicleColor = data["Vehiclecolor"].stringValue
        self.createDate = data["CreatedDate"].stringValue
    }
}

class AddVehicleViewsModel {
    var addVehicleModel = [AddVehicleModel]()
    init(data:[JSON]) {
//        let dat = data as JSON
//        if data != .null {
            let arrayData = data as NSArray
            addVehicleModel =  arrayData.map({(value) -> AddVehicleModel in
                return  AddVehicleModel(data:JSON(value))
            })
//        }
    }
    
    var getVehicleData:Array<AddVehicleModel>{
        return addVehicleModel
    }
}
