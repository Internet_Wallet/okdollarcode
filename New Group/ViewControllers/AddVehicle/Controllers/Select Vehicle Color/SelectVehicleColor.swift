//
//  SelectVehicleColor.swift
//  OK
//
//  Created by Shobhit Singhal on 3/8/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit


protocol vehicleColorDelegate:class {
    func getVehicleColor(colorName: String)
}


class SelectVehicleColor: OKBaseController, UITableViewDelegate, UITableViewDataSource {
    var vehicleColorArray = ["Black", "White", "Green", "Blue", "Gray", "Silver", "Red", "Brown", "Yellow", "Others"]
    var vehicleColorArrayBurmese = ["အမဲေရာင္", "အျဖဴေရာင္", "အစိမ္းေရာင္", "အျပာေရာင္", "ခဲေရာင္", "ေငြေရာင္", "အနီေရာင္", "အညိုေရာင္", "အ၀ါေရာင္", "အျခားအေရာင္"]
    
    weak var delegate: vehicleColorDelegate?
    var isBurmese:Bool = false
    
    @IBOutlet weak var vehicleColorTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.title = "Select Vehicle Color".localized
        if UserDefaults.standard.value(forKey: "currentLanguage") as! String == "en" {
            self.isBurmese = false
        }
        else {
            self.isBurmese = true
        }
        vehicleColorTableView.delegate = self
        vehicleColorTableView.dataSource = self
        vehicleColorTableView.reloadData()
        vehicleColorTableView.tableFooterView = UIView.init(frame: CGRect.zero)
    }

   
    @IBAction func dismissControllerAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vehicleColorArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = vehicleColorTableView.dequeueReusableCell(withIdentifier: "vehicleColor") as! VehicleColorTableViewCell
        if indexPath.row == 0 {
            cell.colorImgView.backgroundColor = UIColor.colorWithRedValue(redValue: 0, greenValue: 0, blueValue: 0, alpha: 1)
        }
        else if indexPath.row == 1 {
            cell.colorImgView.backgroundColor = UIColor.colorWithRedValue(redValue: 250, greenValue: 250, blueValue: 255, alpha: 1)
        }
        else if indexPath.row == 2 {
            cell.colorImgView.backgroundColor = UIColor.colorWithRedValue(redValue: 0, greenValue: 128, blueValue: 0, alpha: 1)
        }
        else if indexPath.row == 3 {
            cell.colorImgView.backgroundColor = UIColor.colorWithRedValue(redValue: 0, greenValue: 0, blueValue: 255, alpha: 1)
        }
        else if indexPath.row == 4 {
            cell.colorImgView.backgroundColor = UIColor.colorWithRedValue(redValue: 128, greenValue: 128, blueValue: 128, alpha: 1)
        }
        else if indexPath.row == 5 {
            cell.colorImgView.backgroundColor = UIColor.colorWithRedValue(redValue: 192, greenValue: 192, blueValue: 192, alpha: 1)
        }
        else if indexPath.row == 6 {
            cell.colorImgView.backgroundColor = UIColor.colorWithRedValue(redValue: 255, greenValue: 0, blueValue: 0, alpha: 1)
        }
        else if indexPath.row == 7 {
            cell.colorImgView.backgroundColor = UIColor.colorWithRedValue(redValue: 170, greenValue: 121, blueValue: 66, alpha: 1)
        }
        else if indexPath.row == 8 {
            cell.colorImgView.backgroundColor = UIColor.colorWithRedValue(redValue: 255, greenValue: 255, blueValue: 0, alpha: 1)
        }
        else if indexPath.row == 9 {
            cell.colorImgView.backgroundColor = UIColor.colorWithRedValue(redValue: 255, greenValue: 147, blueValue: 0, alpha: 1)
        }
        cell.colorLabel.text = self.isBurmese ? vehicleColorArrayBurmese[indexPath.row] : vehicleColorArray[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.getVehicleColor(colorName: self.isBurmese ? vehicleColorArrayBurmese[indexPath.row] : vehicleColorArray[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
}


