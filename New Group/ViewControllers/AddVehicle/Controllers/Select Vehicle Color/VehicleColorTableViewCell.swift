//
//  VehicleColorTableViewCell.swift
//  OK
//
//  Created by Shobhit Singhal on 3/8/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class VehicleColorTableViewCell: UITableViewCell {

    @IBOutlet weak var colorLabel : UILabel!
    @IBOutlet weak var colorImgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
