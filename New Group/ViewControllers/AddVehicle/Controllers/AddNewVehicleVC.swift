//
//  AddNewVehicleVC.swift
//  OK
//
//  Created by Uma Rajendran on 2/13/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class AddNewVehicleVC: OKBaseController {


    @IBOutlet weak var newVehicleTable  : UITableView!
    @IBOutlet weak var saveBtn          : UIButton!
    
    var addVehicle_TableCells  =   [VehicleCell]()
    var vehicleRowsList        =   [VehicleList]()
    
    let vehiclesBackUp : [VehicleList]  = [.vehicle_name, .vehicle_brand, .vehicle_subbrand, .vehicle_color, .vehicle_number, .vehicle_division, .vehicle_licensenumber ]
    
    private var submitView : UIView {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 50))
        let btn = UIButton.init(frame: view.frame)
        btn.backgroundColor = kYellowColor
        btn.setTitle("Submit", for: .normal)
        btn.addTarget(self, action: #selector(accessorySubmitBtnAction(_ :)), for: UIControlEvents.touchUpInside)
        view.addSubview(btn)
        return view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func loadInitialization() {
        
    }
    
    
    func loadTableCells() {
        
        for i in 0 ... 6 {
            let cell = self.newVehicleTable.dequeueReusableCell(withIdentifier: "vehicleCellIdentifier") as! VehicleCell
            cell.cellindex                                      =  i
            cell.cellname                                       =  vehiclesBackUp[i]
            cell.contentTxtField.delegate                       =  self
            cell.contentTxtField.inputAccessoryView             =  self.submitView
            cell.contentTxtField.inputAccessoryView?.isHidden   =  true
            addVehicle_TableCells.append(cell)
        }
        
    }
    
    func loadBasicCells() {
        vehicleRowsList.removeAll()
        if vehiclesBackUp.count > 0 {
            vehicleRowsList.append(vehiclesBackUp.first!)
        }
        newVehicleTable.reloadData()
    }
    
    func insertRow(curRow: VehicleList) {
        
    }
    
    func deleteRow(curRow: VehicleList) {
        
    }
    
    @objc func accessorySubmitBtnAction(_ sender: UIButton) {
        println_debug("accessorySubmitBtnAction/////////////////////////")
 
    }
}

extension AddNewVehicleVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return vehicleRowsList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let rowCount : Int = 0
//        if section == BankItems.bank_bankname.rawValue {
//            rowCount  =  isBranchExpand ? 2 : 1
//        }else {
//            rowCount  =  1
//        }
        return rowCount
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let accountDetails = getAccountDetailsDic()
//
//        if indexPath.section == BankItems.bank_bankname.rawValue && indexPath.row == 1 {
//            let branchCell = tableView.dequeueReusableCell(withIdentifier: "subcellidentifier", for: indexPath) as! SendMoneySubCell
//            branchCell.wrapData(accDetails: accountDetails)
//            return branchCell
//        }else {
//            let bankmainCell = myAcc_TableCells[indexPath.section] as SendMoneyMainCell
//            bankmainCell.wrap_CellData(accDetails: accountDetails, cellIndex: indexPath.section)
//            bankmainCell.rowActionBtn.addTarget(self, action: #selector(cellAction(_:)), for: .touchUpInside)
//            bankmainCell.contactNumberCloseBtn.addTarget(self, action: #selector(contactNumberCloseBtnAction(_ :)), for: .touchUpInside)
//            return bankmainCell
//        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath.section == BankItems.bank_bankname.rawValue && indexPath.row == 0 {
//            let accountDetails = getAccountDetailsDic()
//            if !accountDetails.isLoadFromExistingBank {
//                self.showBanksView()
//            }
//        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.section == BankItems.bank_bankname.rawValue && indexPath.row == 1 {
//            tableView.estimatedRowHeight    = 250
//            tableView.rowHeight             = UITableViewAutomaticDimension
//            return tableView.rowHeight
//
//        }else {
            return 70
//        }
    }
    
}

extension AddNewVehicleVC: UITextFieldDelegate {

  
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("textFieldDidBeginEditing")
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("textFieldDidEndEditing")
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        //        activeField = textField
        print("textFieldShouldBeginEditing")
        
        return true;
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("textFieldShouldClear")
        textField.text = "";
        
        return true;
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("textFieldShouldEndEditing")
        
        return true;
    }

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("shouldChangeCharactersIn")
        
        let text = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        print("text entering in nrc :::: \(text)")
        
        
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filteredStr = string.components(separatedBy: cs).joined(separator: "")
        guard string == filteredStr else {
            return false
        }
        
        return true
    }
}
