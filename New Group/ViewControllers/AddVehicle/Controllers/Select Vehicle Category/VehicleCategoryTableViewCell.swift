//
//  VehicleCategoryTableViewCell.swift
//  OK
//
//  Created by Shobhit Singhal on 3/8/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class VehicleCategoryTableViewCell: UITableViewCell {
    @IBOutlet weak var subCategoryLabel : UILabel!{
        didSet{//API Response only zawayi or english getting
            self.subCategoryLabel.font = UIFont(name: "Zawgyi-One", size: appFontSize)
        }
    }
    @IBOutlet weak var countryCodeLabel : UILabel!{
        didSet{
            self.countryCodeLabel.font = UIFont(name: "Zawgyi-One", size: appFontSize)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
