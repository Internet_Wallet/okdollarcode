//
//  SelectVehicleCategory.swift
//  OK
//
//  Created by Shobhit Singhal on 3/8/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol vehicleSubCategoryTypeDelegate: class {
    func getVehicleSubCategory(subCategoryBurmeseName: String, subCategoryName: String, vehicleTypeId: String)
    func getBrandCategory(brandCategory: String, brandId: String)
    func getSubBrandCategory(subBrandCategory: String, modelId: String)
    func getDivisionState(divisionState: String)
}


class SelectVehicleCategory: OKBaseController, WebServiceResponseDelegate, UITableViewDelegate, UITableViewDataSource {
    var ind:Int!
    var allVehicleCategoriesViewModel:AllVehicleCategoriesViewModel!
    var brandViewModel:BrandViewModel!
    var subBrandViewModel:SubBrandViewModel!
    weak var delegate: vehicleSubCategoryTypeDelegate?
    var whichApiCall:String = ""
    var brandId:String = ""
    var navigationTitle:String = ""
    let web = WebApiClass()
    var isBurmese:Bool = false
    var isSearchBarActive:Bool = false
    private lazy var searchBar = UISearchBar()
    private var searchResultList = [String]()
    private var keyArr : [String] = []

    @IBOutlet weak var noRecordsView : UIView!
    @IBOutlet weak var noRecordsLabel : UILabel!{
        didSet{
            noRecordsLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }

    private enum ScreenMode {
        case search, normal
    }
    private var screenMode = ScreenMode.normal

    
    var divisionArr = ["Ayeyarwady Division", "Bago Division", "Chin State", "Kachin State", "Kayah State", "Kayin State", "Magway Division", "Mandalay Division", "Mon State", "Naypyitaw Union Territory", "Rakhine State", "Sagaing Division", "Shan State", "Tanintharyi Division", "Yangon Division"]    
    var divisionCodeArr = ["AYY", "BGO", "CHN", "KCN", "KYH", "KYN", "MGY", "MDY", "MON", "NPW", "RKE", "SGG", "SHN", "TNL", "YGN"]
    
    var divisionArrBurmese = ["ဧရာ၀တီတိုင္း", "ပဲခူးတိုင္း", "ခ်င္းျပည္နယ္", "ကခ်င္ျပည္နယ္", "ကယားျပည္နယ္", "ကရင္ျပည္နယ္", "မေကြးတိုင္း", "မႏၲေလးတိုင္း", "မြန္ျပည္နယ္", "ေနျပည္ေတာ္ ျပည္ေထာင္စုနယ္ေျမ", "ရခိုင္ျပည္နယ္", "စစ္ကိုင္းတိုင္း", "ရွမ္းျပည္နယ္", "တနသၤာရီတိုင္း", "ရန္ကုန္တိုင္း"]

    var divisionCodeArrBurmese = ["ဧရာ၀တီ", "ပဲခူး", "ခ်င္း", "ကခ်င္", "ကယား", "ကရင္", "မေကြး", "မႏၱေလး", "မြန္", "ေနျပည္ေတာ္", "ရခိုင္", "စစ္ကိုင္း", "ရွမ္း", "တနသၤာရီ", "ရန္ကုန္"]
    
    @IBOutlet weak var vehicleCategoryTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        web.delegate = self
        vehicleCategoryTableView.estimatedRowHeight = 44.0
        vehicleCategoryTableView.rowHeight = UITableView.automaticDimension
        vehicleCategoryTableView.tableFooterView = UIView.init(frame: CGRect.zero)
        if UserDefaults.standard.value(forKey: "currentLanguage") as! String == "en" {
            self.isBurmese = false
        }
        else {
            self.isBurmese = true
        }
        noRecordsView.isHidden = true
        noRecordsLabel.text = "No records found!".localized
        callingHttp()
//        println_debug("ssss", isSearchBarActive)
        if isSearchBarActive {
            searchBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40)
            searchBar.showsCancelButton = true
            searchBar.delegate = self
            searchBar.tintColor = UIColor.white
            searchBar.placeholder = navigationTitle.localized
          if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            uiButton.setTitle("Cancel".localized, for:.normal)
            if let navFont = UIFont(name: appFont, size: 18) {
              uiButton.titleLabel?.font = navFont
            }
          }
          
            if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 14) {
                if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                    if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                        searchTextField.font = myFont
                    }else{
                        //
                    }
                    searchTextField.placeholder = "Search".localized
                    searchTextField.backgroundColor  = .white
                    searchTextField.keyboardType = .asciiCapable
                }
            }
            let view = self.searchBar.subviews[0] as UIView
            let subViewsArray = view.subviews
            for subView: UIView in subViewsArray {
                if subView.isKind(of: UITextField.self) {
                    subView.tintColor = UIColor.colorWithRedValue(redValue: 0, greenValue: 144, blueValue: 81, alpha: 1)
                }
            }
            let searchBarButton = UIBarButtonItem(customView:searchBar)
            let backButton = UIButton(type: .custom)
            backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
            backButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
            backButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
            backButton.addTarget(self, action: #selector(dismissAction), for: .touchUpInside)
            self.navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: backButton), searchBarButton]
        }
        else {
            self.title = navigationTitle.localized
        }        
    }
    
    @IBAction func searchAction(_ sender: UIBarButtonItem) {
        
    }
    
    @objc func dismissAction() {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }

    private func searchFromKey(ketText: String) {
        searchResultList = []
        searchResultList = keyArr.filter {
            return $0.lowercased().contains(ketText.lowercased())
        }
        vehicleCategoryTableView.reloadData()
    }
    
    func callingHttp() {
        if self.whichApiCall == "vehicleType" {
            vehicleCategoryTableView.delegate = self
            vehicleCategoryTableView.dataSource = self
            vehicleCategoryTableView.reloadData()
            self.searchBar.isHidden = true
        }
        else if self.whichApiCall == "division/State" {
            for i in 0..<self.divisionArr.count {
                let str = divisionArr[i] + " (" + divisionCodeArr[i] + ")"
                self.keyArr.append(str)
            }
            vehicleCategoryTableView.delegate = self
            vehicleCategoryTableView.dataSource = self
            vehicleCategoryTableView.reloadData()
        }
        else if self.whichApiCall == "vehicleBrand" {
            if appDelegate.checkNetworkAvail() {
                let type:Int = 1
                let mobStr:String = UserModel.shared.mobileNo
                var urlString =  Url.GetVehicleBrands + "MobileNumber=" + mobStr + "&SimId=" + uuid + "&MsId=" + msid + "&OsType=" + "\(type)" + "&Otp=" + uuid + "&vehicleName=Empty cargo Truck"
                urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                let url = getUrl(urlStr: urlString, serverType: .serverApp)
                println_debug(url)
                let params = [String: AnyObject]() as AnyObject
                web.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", mScreen: "vehicleBrand")
                DispatchQueue.main.async {
                    progressViewObj.showProgressView()
                }
            }else{
                println_debug("network is not avialable")
                alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img:#imageLiteral(resourceName: "r_user"))
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                })
                DispatchQueue.main.async {
                    alertViewObj.showAlert(controller: self)
                }
            }
        }
        else if self.whichApiCall == "vehicleSubBrand" {
            if appDelegate.checkNetworkAvail() {
                let type:Int = 1
                let mobStr:String = UserModel.shared.mobileNo
                var urlString =  Url.GetVehicleSubBrands + "MobileNumber=" + mobStr + "&SimId=" + uuid + "&MsId=" + msid + "&OsType=" + "\(type)" + "&Otp=" + uuid + "&BrandId=" + brandId
                urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                let url = getUrl(urlStr: urlString, serverType: .serverApp)
                println_debug(url)
                let params = [String: AnyObject]() as AnyObject
                web.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", mScreen: "vehicleSubBrand")
                DispatchQueue.main.async {
                    progressViewObj.showProgressView()
                }
            }else{
                println_debug("network is not avialable")
                alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img:#imageLiteral(resourceName: "r_user"))
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                })
                DispatchQueue.main.async {
                    alertViewObj.showAlert(controller: self)
                }
            }
        }
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        if self.whichApiCall == "vehicleBrand" {
            DispatchQueue.main.async {
                progressViewObj.removeProgressView()
            }
            do {
                if let data = json as? Data {
                    
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,Any> {
                        if dic["Code"] as? NSNumber == 200 {
                            DispatchQueue.main.async {
                                if let stringData = String(data: data, encoding: String.Encoding.utf8) {
                                    let dict = OKBaseController.convertToDictionary(text: stringData)
                                    let jsonData = JSON(dict!)
                                    println_debug(jsonData)
                                    let categoryData = jsonData["Data"].stringValue
                                    let categorydDict = OKBaseController.convertToArrDictionary(text: categoryData)
                                    let categoryjsonData = JSON(categorydDict!)
                                    println_debug(categoryjsonData)
                                    self.brandViewModel = BrandViewModel(data: categoryjsonData)
                                    self.vehicleCategoryTableView.delegate = self
                                    self.vehicleCategoryTableView.dataSource = self
                                    self.vehicleCategoryTableView.reloadData()
                                    for i in 0..<self.brandViewModel.getBrandData.count {
                                        self.keyArr.append(self.brandViewModel.getBrandData[i].brandName)
                                    }
                                }
                            }
                        }else {
                            DispatchQueue.main.async {
                                self.showAlert(alertTitle: "", alertBody: dic["Msg"] as? String ?? "" , alertImage: #imageLiteral(resourceName: "r_user"))}
                        }
                    }
                }
            } catch {
                showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))
            }
        }
        else if self.whichApiCall == "vehicleSubBrand" {
            DispatchQueue.main.async {
                progressViewObj.removeProgressView()
            }
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,Any> {
                        if dic["Code"] as? NSNumber == 200 {
                            DispatchQueue.main.async {
                                if let stringData = String(data: data, encoding: String.Encoding.utf8) {
                                    let dict = OKBaseController.convertToDictionary(text: stringData)
                                    let jsonData = JSON(dict!)
                                    println_debug(jsonData)
                                    let categoryData = jsonData["Data"].stringValue
                                    let categorydDict = OKBaseController.convertToArrDictionary(text: categoryData)
                                    let categoryjsonData = JSON(categorydDict!)
                                    println_debug(categoryjsonData)
                                    self.subBrandViewModel = SubBrandViewModel(data: categoryjsonData)
                                    self.vehicleCategoryTableView.delegate = self
                                    self.vehicleCategoryTableView.dataSource = self
                                    self.vehicleCategoryTableView.reloadData()
                                    for i in 0..<self.subBrandViewModel.getSubBrandData.count {
                                        self.keyArr.append(self.subBrandViewModel.getSubBrandData[i].subBrandName)
                                    }
                                }
                            }
                        }else {
                            DispatchQueue.main.async {
                                self.showAlert(alertTitle: "", alertBody: dic["Msg"] as? String ?? "" , alertImage: #imageLiteral(resourceName: "r_user"))}
                        }
                    }
                }
            } catch {
                DispatchQueue.main.async {
                    self.showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))}
            }
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    @IBAction func dismissControllerAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.whichApiCall == "vehicleType" {
            switch screenMode {
            case .normal:
                return allVehicleCategoriesViewModel.getAllVehicleCategoriesData[ind].getChildCategoryData.count
            case .search:
                return searchResultList.count
            }
        }
        else if self.whichApiCall == "vehicleBrand" {
            switch screenMode {
            case .normal:
                self.noRecordsView.isHidden = true
                return brandViewModel.getBrandData.count
            case .search:
                if searchResultList.count == 0 {
                    self.noRecordsView.isHidden = false
                }
                else {
                    self.noRecordsView.isHidden = true
                }
                return searchResultList.count
            }
        }
        else if self.whichApiCall == "vehicleSubBrand" {
            switch screenMode {
            case .normal:
                self.noRecordsView.isHidden = true
                return subBrandViewModel.getSubBrandData.count
            case .search:
                if searchResultList.count == 0 {
                    self.noRecordsView.isHidden = false
                }
                else {
                    self.noRecordsView.isHidden = true
                }
                return searchResultList.count
            }
        }
        else if self.whichApiCall == "division/State" {
            switch screenMode {
            case .normal:
                return divisionArr.count
            case .search:
                return searchResultList.count
            }
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = vehicleCategoryTableView.dequeueReusableCell(withIdentifier: "subVehicleCategoryCell") as! VehicleCategoryTableViewCell
        if self.whichApiCall == "vehicleType" {
            cell.subCategoryLabel.text = self.isBurmese ? allVehicleCategoriesViewModel.getAllVehicleCategoriesData[ind].getChildCategoryData[indexPath.row].categoryNameBurmese : allVehicleCategoriesViewModel.getAllVehicleCategoriesData[ind].getChildCategoryData[indexPath.row].categoryName
        }
        else if self.whichApiCall == "vehicleBrand" {
            switch screenMode {
            case .normal:
                cell.subCategoryLabel.text = brandViewModel.getBrandData[indexPath.row].brandName
            case .search:
                cell.subCategoryLabel.text = searchResultList[indexPath.row]
            }
        }
        else if self.whichApiCall == "vehicleSubBrand" {
            switch screenMode {
            case .normal:
                cell.subCategoryLabel.text = subBrandViewModel.getSubBrandData[indexPath.row].subBrandName
            case .search:
                cell.subCategoryLabel.text = searchResultList[indexPath.row]
            }
        }
        else if self.whichApiCall == "division/State" {
            switch screenMode {
            case .normal:
                cell.subCategoryLabel.text = divisionArr[indexPath.row]
                cell.countryCodeLabel.text = "(\(divisionCodeArr[indexPath.row]))"
                
            case .search:
                cell.subCategoryLabel.text = searchResultList[indexPath.row]
            }
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.whichApiCall == "vehicleType" {
          delegate?.getVehicleSubCategory(subCategoryBurmeseName: self.isBurmese ? allVehicleCategoriesViewModel.getAllVehicleCategoriesData[ind].getChildCategoryData[indexPath.row].categoryNameBurmese : allVehicleCategoriesViewModel.getAllVehicleCategoriesData[ind].getChildCategoryData[indexPath.row].categoryName, subCategoryName: allVehicleCategoriesViewModel.getAllVehicleCategoriesData[ind].getChildCategoryData[indexPath.row].categoryName, vehicleTypeId : allVehicleCategoriesViewModel.getAllVehicleCategoriesData[ind].getChildCategoryData[indexPath.row].vehicleTypeId)
//            delegate?.getVehicleSubCategory(subCategoryName: self.isBurmese ? allVehicleCategoriesViewModel.getAllVehicleCategoriesData[ind].getChildCategoryData[indexPath.row].categoryNameBurmese : allVehicleCategoriesViewModel.getAllVehicleCategoriesData[ind].getChildCategoryData[indexPath.row].categoryName, vehicleTypeId : allVehicleCategoriesViewModel.getAllVehicleCategoriesData[ind].getChildCategoryData[indexPath.row].vehicleTypeId)
        }
        else if self.whichApiCall == "vehicleBrand" {
            switch screenMode {
            case .normal:
                delegate?.getBrandCategory(brandCategory: brandViewModel.getBrandData[indexPath.row].brandName, brandId: brandViewModel.getBrandData[indexPath.row].brandId)
            case .search:
                for i in 0..<brandViewModel.getBrandData.count {
                    if brandViewModel.getBrandData[i].brandName.contains(find: searchResultList[indexPath.row]) {
                        delegate?.getBrandCategory(brandCategory: brandViewModel.getBrandData[i].brandName, brandId: brandViewModel.getBrandData[i].brandId)
                    }
                }
            }
        }
        else if self.whichApiCall == "vehicleSubBrand" {
            switch screenMode {
            case .normal:
                delegate?.getSubBrandCategory(subBrandCategory: subBrandViewModel.getSubBrandData[indexPath.row].subBrandName, modelId: subBrandViewModel.getSubBrandData[indexPath.row].subBrandId)
            case .search:
                for i in 0..<subBrandViewModel.getSubBrandData.count {
                    if subBrandViewModel.getSubBrandData[i].subBrandName.contains(find: searchResultList[indexPath.row]) {
                        delegate?.getSubBrandCategory(subBrandCategory: subBrandViewModel.getSubBrandData[i].subBrandName, modelId: subBrandViewModel.getSubBrandData[i].subBrandId)
                    }
                }
            }
        }
        else if self.whichApiCall == "division/State" {
            switch screenMode {
            case .normal:
                delegate?.getDivisionState(divisionState: divisionCodeArr[indexPath.row])
            case .search:
                delegate?.getDivisionState(divisionState: searchResultList[indexPath.row])
            }
        }
        self.dismiss(animated: true, completion: nil)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}


extension SelectVehicleCategory: UISearchBarDelegate {
    // MARK: -UISearchBarDelegate
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            uiButton.setTitle("Cancel".localized, for:.normal)
          
          if let navFont = UIFont(name: appFont, size: 18) {
            uiButton.titleLabel?.font = navFont
          }
          
        }
    
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            screenMode = .normal
            vehicleCategoryTableView.reloadData()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
            let updatedText = searchText.replacingCharacters(in: textRange, with: text)
            if updatedText.count > 50 {
                return false
            }
            if updatedText != "" && text != "\n" {
                screenMode = .search
                searchFromKey(ketText: updatedText)
            }
        }
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}


extension UIColor {
    static func colorWithRedValue(redValue: CGFloat, greenValue: CGFloat, blueValue: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: redValue/255.0, green: greenValue/255.0, blue: blueValue/255.0, alpha: alpha)
    }
}

