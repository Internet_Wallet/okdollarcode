//
//  OpenGallaryVC.swift
//  OK
//
//  Created by Subhash Arya on 20/11/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol OpenGallaryVCDelegate {
    func openGalleryOptioon(str : String,tagnumber : Int,viewController : UIViewController)
}
    


class OpenGallaryVC: UIViewController {
    
    var tagNo = 0
    var delegate : OpenGallaryVCDelegate?
    @IBOutlet weak var lblHeader : UILabel!{
        didSet {
           lblHeader.text = "Select Photo".localized
        }
    }
    @IBOutlet weak var lblGallary : UILabel! {
        didSet {
          lblGallary.text = "Gallery".localized
        }
    }
    @IBOutlet weak var lblCamera : UILabel!{
        didSet {
          lblCamera.text = "Camera".localized
        }
    }
    @IBOutlet weak var btnCancel : UIButton!{
        didSet {
            btnCancel.setTitle("Cancel".localized, for: .normal)
        }
    }
    
    weak var localVC : UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func setTag(tg : Int, vc : UIViewController){
      tagNo = tg
        localVC = vc
    }
    
    
    
    @IBAction func onClickGallery(_ sender: UIButton) {
        if let del = delegate {
            self.view.removeFromSuperview()
            del.openGalleryOptioon(str: "Gallery", tagnumber: tagNo, viewController: localVC! )
        }

    }

    @IBAction func onClickCamera(_ sender: UIButton) {
        if let del = delegate {
            self.view.removeFromSuperview()
            del.openGalleryOptioon(str: "Camera", tagnumber: tagNo, viewController: localVC!)
        }
    }

    @IBAction func onClickCancel(_ sender: UIButton) {
        self.view.removeFromSuperview()
    }


}
