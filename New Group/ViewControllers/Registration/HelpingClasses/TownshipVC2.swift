//
//  TownshipVC2.swift
//  DemoUpdateProfile
//
//  Created by SHUBH on 10/3/17.
//  Copyright © 2017 SHUBH. All rights reserved.
//

import UIKit
protocol TownshipVC2Delegate : class {
    func NRCValueString(nrcSting : String)
}


class TownshipVC2: OKBaseController,UITextFieldDelegate {
   weak var townshipVC2Delegate : TownshipVC2Delegate?
    
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var tableview: UITableView!
    var selectedDivState: LocationDetail?
    var townshipArr : [TownShipDetail]?

    @IBOutlet weak var lblNoRecord: UILabel!
    var barButton = UIBarButtonItem()
    var tfSearch = UITextField()
    var isSearch = false
    var array = [TownShipDetail]()
    var CHARSET = ""
    var registrationScreen = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
            btnSearch.tag = 1
    
    self.view.backgroundColor = kBackGroundGreyColor
    self.setMarqueLabelInNavigation()
    loadInitialize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadInitialize() {
        if ok_default_language == "my" {
        townshipArr = selectedDivState!.townShipArray.sorted(by: { $0.townShipNameMY < $1.townShipNameMY} )
        CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ "
        }else if ok_default_language == "en"{
        townshipArr = selectedDivState!.townShipArray.sorted(by: { $0.townShipNameEN < $1.townShipNameEN} )
        CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 "
        }else {
            townshipArr = selectedDivState!.townShipArray.sorted(by: { $0.townShipNameUni < $1.townShipNameUni} )
            CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ "
        }
        
        lblNoRecord.isHidden = true
        tableview.delegate = self
        tableview.dataSource = self
        tableview.register(UINib(nibName: "TownshipCell", bundle: nil), forCellReuseIdentifier: "townshipcellidentifier")
        tableview.tableFooterView = UIView(frame: CGRect.zero)
        tableview.reloadData()
    }
    
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
        //this is down my tushar lama this case will execute when user will not select any township and click on back button so the problem was in label nrc number will displaying
        self.townshipVC2Delegate?.NRCValueString(nrcSting: "")
        
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        if btnSearch.tag == 1 {
            self.navigationItem.titleView = nil
            self.navigationItem.title = ""
            tfSearch.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
            self.navigationItem.titleView = tfSearch
            tfSearch.backgroundColor = UIColor.white
            tfSearch.delegate = self
            tfSearch.layer.cornerRadius = 5.0
            tfSearch.textColor = UIColor.black
            tfSearch.font = UIFont(name: "Zawgyi-One", size: 17)
            btnSearch.setTitleColor(UIColor.white, for: .normal)
            tfSearch.placeholder = "Search".localized
            tfSearch.tintColor = UIColor.init(red: 0/255, green: 144/255, blue: 81/255, alpha: 1)
            tfSearch.delegate = self
            tfSearch.clearButtonMode = .whileEditing
            
            let tfserarcView = UIView()
            tfserarcView.frame = CGRect(x: 10, y: 0, width: 30, height: 30)
            let tfserarchIcon = UIImageView()
            tfserarchIcon.frame = CGRect(x: 8, y: 8, width: 14, height: 14)
            tfserarcView.addSubview(tfserarchIcon)
            tfserarchIcon.image = #imageLiteral(resourceName: "search")
            tfSearch.leftView = tfserarcView
            tfSearch.leftViewMode = UITextField.ViewMode.always
            
            btnSearch.setImage(nil, for: .normal)
            btnSearch.setTitle("Cancel".localized, for: .normal)
            btnSearch.titleLabel?.font = UIFont(name: appFont, size: 13)
            tfSearch.becomeFirstResponder()
            btnSearch.tag = 2
        } else if btnSearch.tag == 2{
            btnSearch.tag = 1
            self.navigationController?.view.endEditing(false)
            tfSearch.text = ""
            lblNoRecord.isHidden = true
            isSearch = false
            self.navigationItem.titleView = nil
            self.setMarqueLabelInNavigation()
            tableview.reloadData()
        } else if btnSearch.tag == 3 {
            btnSearch.tag = 2
            self.navigationController?.view.endEditing(true)
            view.endEditing(true)
            lblNoRecord.isHidden = true
            tfSearch.text = ""
            isSearch = false
            tableview.reloadData()
        }
    }
    
    private func setMarqueLabelInNavigation() {
        lblNoRecord.isHidden = true
        lblNoRecord.text = "No Record Found".localized
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.textColor = UIColor.white
        lblMarque.textAlignment = .center
        lblMarque.font =  UIFont(name: appFont, size: appFontSize)
        lblMarque.text = "Select NRC Code ".localized
        btnSearch.setTitle("", for: .normal)
        btnSearch.setImage(#imageLiteral(resourceName: "r_search"), for: .normal)
        self.navigationItem.titleView = nil
        self.navigationItem.titleView = lblMarque
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        tfSearch.resignFirstResponder()
        return true
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        lblNoRecord.isHidden = true
        tfSearch.text = ""
        btnSearch.tag = 2
        isSearch = false
        tableview.reloadData()
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if range.location == 0 && string == " " {
            return false
        }
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
        
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            if updatedText == "" {
                isSearch = false
                lblNoRecord.isHidden = true
                tableview.reloadData()
            }
            else{
              //  lblNoRecord.isHidden = true
                getSearchArrayContains(updatedText)
            }
        }
           return restrictMultipleSpaces(str: string, textField: textField)
    }
    
    func getSearchArrayContains(_ text : String) {
        if ok_default_language == "my" {
            array = (self.townshipArr?.filter({$0.nrcDisplayCodeMY.lowercased().contains(find: text.lowercased())}))!
        }else if ok_default_language == "en"{
            array = (self.townshipArr?.filter({$0.nrcDisplayCodeEN.lowercased().contains(find: text.lowercased())}))!
        }else {
            array = (self.townshipArr?.filter({$0.nrcDisplayCodeUni.lowercased().contains(find: text.lowercased())}))!
        }
    
        if array.count == 0 {
            lblNoRecord.isHidden = false
            self.btnSearch.tag = 2
            self.btnSearch.tintColor = UIColor.lightGray
        }else {
           lblNoRecord.isHidden = true
        }
        isSearch = true
        tableview.reloadData()
    }
    
}




extension TownshipVC2 : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearch {
           return array.count
        }else{
            return (townshipArr?.count)!
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let townshipCell = tableView.dequeueReusableCell(withIdentifier: "townshipcellidentifier", for: indexPath) as! TownshipCell
        if isSearch {
            let model = array[indexPath.row]
            if ok_default_language == "my" {
                townshipCell.townshipNameLbl.text = model.nrcDisplayCodeMY
            }else if ok_default_language == "en"{
            townshipCell.townshipNameLbl.text = model.nrcDisplayCodeEN
            }else {
                townshipCell.townshipNameLbl.text = model.nrcDisplayCodeUni
            }
            return townshipCell
        }else{
          let model = townshipArr![indexPath.row]
            if ok_default_language == "my" {
            townshipCell.townshipNameLbl.text = model.nrcDisplayCodeMY
            }else if ok_default_language == "en"{
            townshipCell.townshipNameLbl.text = model.nrcDisplayCodeEN
            }else {
                townshipCell.townshipNameLbl.text = model.nrcDisplayCodeUni
            }
            return townshipCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
            var nrcStr = ""
        if isSearch {
            let model = array[indexPath.row]
            if ok_default_language == "my" {
                nrcStr = "\(model.nrcCodeNumberMy)" + "/" + "\(model.nrcBCode)" + "(N)".localized as String
              //  self.navigationController?.popViewController(animated: false)
            }else  if ok_default_language == "en"{
                nrcStr = "\(model.nrcCodeNumber)" + "/" + "\(model.nrcCode)" + "(N)".localized as String
              //  self.navigationController?.popViewController(animated: false)
            }else {
                nrcStr = "\(model.nrcCodeNumberUni)" + "/" + "\(model.nrcBUCode)" + "(N)".localized as String
               // self.navigationController?.popViewController(animated: false)
            }
        }else{
            let model = townshipArr![indexPath.row]
            if ok_default_language == "my" {
                nrcStr = "\(model.nrcCodeNumberMy)" + "/" + "\(model.nrcBCode)" + "(N)".localized as String
              //  self.navigationController?.popViewController(animated: false)
            }else if ok_default_language == "en"{
                nrcStr = "\(model.nrcCodeNumber)" + "/" + "\(model.nrcCode)" + "(N)".localized as String
             //   self.navigationController?.popViewController(animated: false)
            }else {
                nrcStr = "\(model.nrcCodeNumberUni)" + "/" + "\(model.nrcBUCode)" + "(N)".localized as String
              //  self.navigationController?.popViewController(animated: false)
            }
        }
      //  DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {

//        guard (self.townshipVC2Delegate?.NRCValueString(nrcSting: nrcStr) != nil)  else {
//            return
//        }
       // })
        
        if registrationScreen == "Registration"{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
                
                guard let loginVC = self.navigationController?.viewControllers.first(where: { $0 is ReRegistrationVC}) as? ReRegistrationVC else { return }

                self.navigationController?.popToViewController(loginVC, animated: true)

            guard (self.townshipVC2Delegate?.NRCValueString(nrcSting: nrcStr) != nil)  else {
                return
            }
            })

        }else{
            self.navigationController?.popViewController(animated: false)

        guard (self.townshipVC2Delegate?.NRCValueString(nrcSting: nrcStr) != nil)  else {
            return
        }
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    }
    
    
    
