//
//  BusinessDaysVC.swift
//  OK
//
//  Created by Subhash Arya on 12/04/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol BusinessDaysVCDelegate: class {
    func setDays(viewcontroller : UIViewController, Days : Array<String>)
}

class BusinessDaysVC: OKBaseController {
    weak var delegate : BusinessDaysVCDelegate?
    @IBOutlet weak var sunday: UIButton!
    @IBOutlet weak var monday: UIButton!
    @IBOutlet weak var tuesday: UIButton!
    @IBOutlet weak var wednessday: UIButton!
    @IBOutlet weak var thursday: UIButton!
    @IBOutlet weak var friday: UIButton!
    @IBOutlet weak var saturday: UIButton!
    @IBOutlet weak var publicHoliday: UIButton!
    @IBOutlet weak var subbarthDay: UIButton!
    @IBOutlet weak var openAllDays: UIButton!
  
    @IBOutlet weak var lblHeader: MarqueeLabel!
    @IBOutlet weak var lblAllDays: UILabel!
    @IBOutlet weak var lblSunday: UILabel!
    @IBOutlet weak var lblMonday: UILabel!
    @IBOutlet weak var lblTueday: UILabel!
    @IBOutlet weak var lblWednesday: UILabel!
    @IBOutlet weak var lblThursday: UILabel!
    @IBOutlet weak var lblFriday: UILabel!
    @IBOutlet weak var lblSaturday: UILabel!
    @IBOutlet weak var lblPublicHoliday: UILabel!
    @IBOutlet weak var lblSubbarthday: UILabel!
    @IBOutlet weak var btnSet: UIButton!

    
    var Days = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    lblHeader.text = "Select Shop Closed Day".localized
    lblAllDays.text = "Open All 7 Days".localized
        lblSunday.text = "Sunday".localized
        lblMonday.text = "Monday".localized
        lblTueday.text = "Tuesday".localized
        lblWednesday.text = "Wednesday".localized
        lblThursday.text = "Thursday".localized
        lblFriday.text = "Friday".localized
        lblSaturday.text = "Saturday".localized
        lblPublicHoliday.text = "Public Holidays".localized
        lblSubbarthday.text = "Subbarth day".localized
        btnSet.setTitle("SET".localized, for: .normal)
        openAllDays.isSelected = false
        btnSet.isHighlighted = true
        btnSet.isUserInteractionEnabled = false
    }
    
    @IBAction func selfviewremoveFromSuperviewtapGesture(_ sender: Any) {
    }
    
    @IBAction func OpenAllDays(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    
        if sender.isSelected {
            sunday.isSelected = false
            monday.isSelected = false
            tuesday.isSelected = false
            wednessday.isSelected = false
            thursday.isSelected = false
            friday.isSelected = false
            saturday.isSelected = false
            publicHoliday.isSelected = false
            subbarthDay.isSelected = false
            lblSunday.textColor = UIColor.lightGray
            lblMonday.textColor = UIColor.lightGray
            lblTueday.textColor = UIColor.lightGray
            lblWednesday.textColor = UIColor.lightGray
            lblThursday.textColor = UIColor.lightGray
            lblFriday.textColor = UIColor.lightGray
            lblSaturday.textColor = UIColor.lightGray
            lblPublicHoliday.textColor = UIColor.lightGray
            lblSubbarthday.textColor = UIColor.lightGray
            sunday.isUserInteractionEnabled = false
            monday.isUserInteractionEnabled = false
            tuesday.isUserInteractionEnabled = false
            wednessday.isUserInteractionEnabled = false
            thursday.isUserInteractionEnabled = false
            friday.isUserInteractionEnabled = false
            saturday.isUserInteractionEnabled = false
            publicHoliday.isUserInteractionEnabled = false
            subbarthDay.isUserInteractionEnabled = false
//            sunday.isHighlighted = false
//            monday.isHighlighted = false
//            tuesday.isHighlighted = false
//            wednessday.isHighlighted = false
//            thursday.isHighlighted = false
//            friday.isHighlighted = false
//            saturday.isHighlighted = false
//            publicHoliday.isHighlighted = false
//            subbarthDay.isHighlighted = false

        }else {
            lblSunday.textColor = UIColor.black
            lblMonday.textColor = UIColor.black
            lblTueday.textColor = UIColor.black
            lblWednesday.textColor = UIColor.black
            lblThursday.textColor = UIColor.black
            lblFriday.textColor = UIColor.black
            lblSaturday.textColor = UIColor.black
            lblPublicHoliday.textColor = UIColor.black
            lblSubbarthday.textColor = UIColor.black
            sunday.isUserInteractionEnabled = true
            monday.isUserInteractionEnabled = true
            tuesday.isUserInteractionEnabled = true
            wednessday.isUserInteractionEnabled = true
            thursday.isUserInteractionEnabled = true
            friday.isUserInteractionEnabled = true
            saturday.isUserInteractionEnabled = true
            publicHoliday.isUserInteractionEnabled = true
            subbarthDay.isUserInteractionEnabled = true
//            sunday.isHighlighted = true
//            monday.isHighlighted = true
//            tuesday.isHighlighted = true
//            wednessday.isHighlighted = true
//            thursday.isHighlighted = true
//            friday.isHighlighted = true
//            saturday.isHighlighted = true
//            publicHoliday.isHighlighted = true
//            subbarthDay.isHighlighted = true

        }
         makeSetButtonHighlighted()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func Sunday(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        makeSetButtonHighlighted()
    }
    
    @IBAction func Monday(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
         makeSetButtonHighlighted()
    }
    
    @IBAction func Tuesday(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
         makeSetButtonHighlighted()
    }
    
    @IBAction func Wenessday(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
         makeSetButtonHighlighted()
    }
    @IBAction func Thursday(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
         makeSetButtonHighlighted()
    }
    @IBAction func Friday(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
         makeSetButtonHighlighted()
    }
    
    @IBAction func Satureday(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
         makeSetButtonHighlighted()
    }
    
    @IBAction func PublicHoliday(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
         makeSetButtonHighlighted()
    }
    @IBAction func SubbarthDay(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
         makeSetButtonHighlighted()
    }
    
    @IBAction func SetAction(_ sender: UIButton) {
        if openAllDays.isSelected {
    Days.removeAll()
    Days = ["Open All Days"]
        }else {
             Days.removeAll()
            if sunday.isSelected {
                Days.append("Sunday")
            }
            if monday.isSelected {
                Days.append("Monday")
            }
            if tuesday.isSelected {
                Days.append("Tuesday")
            }
            if wednessday.isSelected {
                Days.append("Wednesday")
            }
            if thursday.isSelected {
                Days.append("Thursday")
            }
            if friday.isSelected {
                Days.append("Friday")
            }
            if saturday.isSelected {
                Days.append("Saturday")
            }
            if publicHoliday.isSelected {
                Days.append("Public Holidays")
            }
            if subbarthDay.isSelected {
                Days.append("Subbarth Day")
            }
        }
        
        if let del = self.delegate {
        del.setDays(viewcontroller: self, Days: Days)
    }
        
    }
    
    func makeSetButtonHighlighted(){
        if openAllDays.isSelected || sunday.isSelected || monday.isSelected || tuesday.isSelected || wednessday.isSelected || thursday.isSelected || friday.isSelected || saturday.isSelected || publicHoliday.isSelected || subbarthDay.isSelected {
            btnSet.isHighlighted = false
            btnSet.isUserInteractionEnabled = true
        }else {
            btnSet.isHighlighted = true
            btnSet.isUserInteractionEnabled = false
        }
    }
    

}
