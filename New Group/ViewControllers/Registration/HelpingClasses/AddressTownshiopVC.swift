//
//  AddressTownshiopVC.swift
//  DemoUpdateProfile
//
//  Created by SHUBH on 9/15/17.
//  Copyright © 2017 SHUBH. All rights reserved.
//

import UIKit

protocol AddressTownshiopVCDelegate : class {
    func setTownshipCityName(cityDic : TownShipDetailForAddress)
}

//This delegate is calle when moving back and front from agent ot state and division
protocol MapDelegateToPop{
    func setVariable(value: Bool)
}

class AddressTownshiopVC: UIViewController,UITextFieldDelegate {
   weak var addressTownshiopVCDelegate : AddressTownshiopVCDelegate?
    var delegateMap: MapDelegateToPop?
    var selectedDivState: LocationDetail?
    var townshipArr : [TownShipDetailForAddress]?
    var tempTownship : [TownShipDetailForAddress]?
    var tfSearch = UITextField()
    var isSearch = false
    var array = [TownShipDetailForAddress]()
    var CHARSET = ""
    
    //This is created because of navigation problem
    var isComingFromAgent = false
    
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var noRecord: UILabel!{
        didSet{
            noRecord.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet var btnSearchBar: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = kYellowColor
        btnSearch.titleLabel?.font = UIFont(name: appFont, size: 12)
        self.navigationController?.navigationBar.backItem?.title = " "
        self.setMarqueLabelInNavigation()
        btnSearch.tag = 1
        noRecord.text = "No Record Found".localized
        loadUI()
        loadInitialize()
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       // self.navigationItem.titleView = nil
       // btnSearch.image = #imageLiteral(resourceName: "r_search")
      
    }

    func loadUI() {
        self.view.backgroundColor = kBackGroundGreyColor
        noRecord.isHidden = true
        tableview.register(UINib(nibName: "TownshipCell", bundle: nil), forCellReuseIdentifier: "townshipcellidentifier")
        tableview.tableFooterView = UIView(frame: CGRect.zero)
        tableview.delegate = self
        tableview.dataSource = self
      
    }
    
    func loadInitialize() {
        if ok_default_language == "my"{
            townshipArr = selectedDivState?.townshipArryForAddress.sorted(by: { $0.filteredTownshipCityNameMy < $1.filteredTownshipCityNameMy} )
            CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ "
        }else if ok_default_language == "en"{
            townshipArr = selectedDivState?.townshipArryForAddress.sorted(by: { $0.filteredTownshipCityNameEn < $1.filteredTownshipCityNameEn } )
            CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 "
        }else {
            townshipArr = selectedDivState?.townshipArryForAddress.sorted(by: { $0.filteredTownshipCityNameUni < $1.filteredTownshipCityNameUni } )
            CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ "
        }
    }
  
    @IBAction func backAction(_ sender: Any){
        if isComingFromAgent{
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    //@IBAction func btnSearchAction(_ sender: Any) {
    
    @IBAction func btnSearchAction(_ sender: Any) {
        
        if btnSearch.tag == 1 {
            self.navigationItem.titleView = nil
            // self.navigationItem.title = ""
            // let tfSearch = UITextField()
            tfSearch.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
            self.navigationItem.titleView = tfSearch
            tfSearch.backgroundColor = UIColor.white
            tfSearch.delegate = self
            tfSearch.layer.cornerRadius = 5.0
            tfSearch.textColor = UIColor.black
            tfSearch.font = UIFont(name: appFont, size: 16)
            btnSearch.setTitleColor(UIColor.white, for: .normal)
            tfSearch.placeholder = "Search".localized
            // tfSearch.tintColor = UIColor.darkGray
            tfSearch.tintColor = UIColor.init(red: 0/255, green: 144/255, blue: 81/255, alpha: 1)
            tfSearch.delegate = self
            tfSearch.clearButtonMode = .whileEditing
            
            let tfserarcView = UIView()
            tfserarcView.frame = CGRect(x: 10, y: 0, width: 30, height: 30)
            let tfserarchIcon = UIImageView()
            tfserarchIcon.frame = CGRect(x: 8, y: 8, width: 14, height: 14)
            tfserarcView.addSubview(tfserarchIcon)
            tfserarchIcon.image = #imageLiteral(resourceName: "search")
            tfSearch.leftView = tfserarcView
            tfSearch.leftViewMode = UITextField.ViewMode.always
            btnSearch.setTitle("Cancel".localized, for: .normal)
            btnSearch.setImage(nil, for: .normal)
            tfSearch.becomeFirstResponder()
            btnSearch.tag = 2
        } else if btnSearch.tag == 2 {
            btnSearch.tag = 1
            btnSearch.tintColor = UIColor.white
            self.navigationController?.view.endEditing(true)
            tfSearch.text = ""
            noRecord.isHidden = true
            self.navigationItem.titleView = nil
            self.setMarqueLabelInNavigation()
            isSearch = false
            tableview.reloadData()
        } else if btnSearch.tag == 3 {
            btnSearch.tag = 2
            self.navigationController?.view.endEditing(true)
            noRecord.isHidden = true
            tfSearch.text = ""
            isSearch = false
            tableview.reloadData()
        }
    }
    
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: 16)
        lblMarque.textAlignment = .center
        lblMarque.text = "Select Township/City".localized
        lblMarque.textColor = UIColor.white
        btnSearch.setTitle("", for: .normal)
        btnSearch.setImage(#imageLiteral(resourceName: "r_search"), for: .normal)
        self.navigationItem.titleView = lblMarque
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        tfSearch.resignFirstResponder()
        return true
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        noRecord.isHidden = true
        tfSearch.text = ""
        btnSearch.tag = 2
        btnSearch.tintColor = UIColor.lightGray
        isSearch = false
        tableview.reloadData()
        println_debug("textFieldShouldClear")
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if range.location == 0 && string == " " {
            return false
        }
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            if updatedText == "" {
                isSearch = false
                tableview.reloadData()
                noRecord.isHidden = true
            }
            else{
                getSearchArrayContains(updatedText)
            }
        }
           return restrictMultipleSpaces(str: string, textField: textField)
    }
    
    func getSearchArrayContains(_ text : String) {
        if ok_default_language == "my" {
             array = (self.townshipArr?.filter({$0.filteredTownshipCityNameMy.lowercased().contains(find: text.lowercased())}))!
        }else if ok_default_language == "en"{
            array = (self.townshipArr?.filter({$0.filteredTownshipCityNameEn.lowercased().contains(find: text.lowercased())}))!
        }else {
            array = (self.townshipArr?.filter({$0.filteredTownshipCityNameUni.lowercased().contains(find: text.lowercased())}))!
        }
        
        if array.count == 0 {
            self.btnSearch.tag = 2
            self.btnSearch.tintColor = UIColor.lightGray
            noRecord.isHidden = false
        }else{
            noRecord.isHidden = true
        }
        isSearch = true
        tableview.reloadData()
    }
}

    extension AddressTownshiopVC : UITableViewDelegate,UITableViewDataSource {
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if isSearch {
                return array.count
            }else{
                return (townshipArr?.count)!
            }
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            println_debug(isSearch)
            let townshipCell = tableView.dequeueReusableCell(withIdentifier: "townshipcellidentifier", for: indexPath) as! TownshipCell
            if isSearch {
                let model = array[indexPath.row]
                if ok_default_language == "my" {
                        townshipCell.townshipNameLbl.text = model.filteredTownshipCityNameMy
                }else if ok_default_language == "en"{
                        townshipCell.townshipNameLbl.text = model.filteredTownshipCityNameEn
                }else {
                    townshipCell.townshipNameLbl.text = model.filteredTownshipCityNameUni
                }
            }else{
                let model = townshipArr![indexPath.row]
                if ok_default_language == "my" {
                    townshipCell.townshipNameLbl.text = model.filteredTownshipCityNameMy
                }else if ok_default_language == "en"{
                     townshipCell.townshipNameLbl.text = model.filteredTownshipCityNameEn
                }else {
                    townshipCell.townshipNameLbl.text = model.filteredTownshipCityNameUni
                }
            }
            return townshipCell
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            tableView.deselectRow(at: indexPath, animated: true)
            DispatchQueue.main.async {
                if self.isSearch {
                    let model = self.array[indexPath.row]
                    
                    if self.isComingFromAgent{
                        self.delegateMap?.setVariable(value: true)
                        self.dismiss(animated: true, completion: nil)
                    }else{
                        self.navigationController?.popViewController(animated: false)
                    }
                    
                    guard (self.addressTownshiopVCDelegate?.setTownshipCityName(cityDic: model) != nil) else {
                        return
                    }
                }else{
                    let model = self.townshipArr![indexPath.row]
                    
                    if self.isComingFromAgent{
                        self.delegateMap?.setVariable(value: true)
                        self.dismiss(animated: true, completion: nil)
                    }else{
                        self.navigationController?.popViewController(animated: false)
                    }
                    guard (self.addressTownshiopVCDelegate?.setTownshipCityName(cityDic: model) != nil) else {
                        return
                    }
                }
            }
            
            }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 60
        }
        }



