//
//  SecurityQuestionCell.swift
//  OK
//
//  Created by SHUBH on 11/29/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class SecurityQuestionCell: UITableViewCell {

    @IBOutlet weak var lblSecurityQuestion: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    func setSecurityQuestion(securityQuestion : String) {
        self.lblSecurityQuestion.text = securityQuestion + " "
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
