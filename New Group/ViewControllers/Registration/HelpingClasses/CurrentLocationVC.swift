//
//  CurrentLocationVC.swift
//  DemoUpdateProfile
//
//  Created by SHUBH on 9/12/17.
//  Copyright © 2017 SHUBH. All rights reserved.
//

import UIKit
import CoreLocation
import Rabbit_Swift

protocol CurrentLocationVCDelegate {
    func SetAddress (Divison : LocationDetail, Township : TownShipDetailForAddress , City : String, Stret : String)
    func CancelLocationAddress()
}

class CurrentLocationVC: OKBaseController {
    var curLocaitonDelegate : CurrentLocationVCDelegate?
    
    fileprivate  var adress : CurrentAddress?
    fileprivate  var adressB : CurrentAddressB?
    
    @IBOutlet var lbConfirmPayment: MarqueeLabel!{
        didSet{
            lbConfirmPayment.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var lbTownship: UILabel!{
        didSet{
            lbTownship.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lbCity: UILabel!{
        didSet{
            lbCity.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lbStreet: UILabel!{
        didSet{
            lbStreet.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lbCurrentlocation: UILabel!{
        didSet{
            lbCurrentlocation.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var viewBG: UIView!
    
    @IBOutlet weak var lbDivision: MarqueeLabel!{
        didSet{
            lbDivision.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lbDivisionValue: MarqueeLabel!{
        didSet{
            lbDivisionValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lbTownShipValue: UILabel!{
        didSet{
            lbTownShipValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lbCityValue: UILabel!{
        didSet{
            lbCityValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lbStreetValue: UILabel!{
        didSet{
            lbStreetValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnCancel: UIButton!{
        didSet{
            btnCancel.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnOk: UIButton!{
        didSet{
            btnOk.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    var allLocationsList = [LocationDetail]()
    var allDivisionList = [LocationDetail]()
    var allStateList = [LocationDetail]()
    var selectedDivState: LocationDetail?
    var state : LocationDetail?
    var township : TownShipDetailForAddress?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadInitialize()
        self.addview()
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
//        loadInitialize()
//        self.addview()
//    }
//
    
    
    
    func loadInitialize() {
        lbConfirmPayment.text = "Confirm Your Permanent/Current Address".localized
        lbCurrentlocation.text = "If Your Current Location of below address is correct. Please Click OK Button".localized
        lbDivision.text = "Division".localized
        lbDivisionValue.text = "".localized
        lbTownship.text = "Township".localized
        lbTownShipValue.text = "".localized
        lbCity.text = "City/Region".localized
        lbCityValue.text = "".localized
        lbStreet.text = "Street".localized
        lbStreetValue.text = "".localized
        btnCancel.setTitle("Cancel".localized, for: .normal)
        btnOk.setTitle("OK".localized, for: .normal)
        allLocationsList = TownshipManager.allLocationsList
        allDivisionList  = TownshipManager.allDivisionList
        allStateList     = TownshipManager.allStateList
        btnOk.isHighlighted = true
        btnOk.tag = 10
        
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined:
                println_debug("notDetermined")
                locationUpdates()
                break
            case .denied :
                println_debug("denied")
                showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                
                break
            case .restricted :
                println_debug("restricted")
                showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                break
            case .authorizedAlways, .authorizedWhenInUse:
                locationUpdates()
                break
            }
        } else {
            alertViewObj.wrapAlert(title:"", body: "Please Enable Location Service".localized, img:#imageLiteral(resourceName: "location"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    @IBAction func btnCancelAction(_ sender: UIButton) {
        guard (self.curLocaitonDelegate?.CancelLocationAddress()) != nil else {
            return
        }
        //self.view.removeFromSuperview()
        self.dismiss(animated: false, completion: nil)
    }
    
    
    
    @IBAction func btnOKAction(_ sender: UIButton) {
        
        if btnOk.tag == 20 {
            for item in allLocationsList {
                let state_name : LocationDetail = item
                if stringBeforeSpace(str: self.adress!.division) == stringBeforeSpace(str: state_name.stateOrDivitionNameEn) {
                    state = item
                    break
                }
            }
            if let locDetail = state {
                for item in locDetail.townshipArryForAddress {
                    let townshipDetails: TownShipDetailForAddress = item
                    if self.adress!.township.contains(find: townshipDetails.townShipNameEN) {
                        township = item
                        if ok_default_language == "my" {
                            guard (self.curLocaitonDelegate?.SetAddress(Divison: state!, Township: township!, City: Rabbit.uni2zg(self.adressB!.cityB), Stret: Rabbit.uni2zg( self.adressB!.streetB))) != nil else {
                                return
                            }
                        }else if ok_default_language == "en" {
                            guard (self.curLocaitonDelegate?.SetAddress(Divison: state!, Township: township!, City: self.adress!.city, Stret: self.adress!.street)) != nil else {
                                return
                            }
                        }else {
                            guard (self.curLocaitonDelegate?.SetAddress(Divison: state!, Township: township!, City: self.adressB!.cityB, Stret: self.adressB!.streetB)) != nil else {
                                return
                            }
                        }
                        self.dismiss(animated: false, completion: nil)
                    }else if self.adress!.township.contains(find: "zundaung") {
                        township = item
                        if ok_default_language == "my" {
                            guard (self.curLocaitonDelegate?.SetAddress(Divison: state!, Township: township!, City: Rabbit.uni2zg(self.adressB!.cityB), Stret: Rabbit.uni2zg( self.adressB!.streetB))) != nil else {
                                return
                            }
                        }else if ok_default_language == "en" {
                            guard (self.curLocaitonDelegate?.SetAddress(Divison: state!, Township: township!, City: self.adress!.city, Stret: self.adress!.street)) != nil else {
                                return
                            }
                        }else {
                            guard (self.curLocaitonDelegate?.SetAddress(Divison: state!, Township: township!, City: self.adressB!.cityB, Stret: self.adressB!.streetB)) != nil else {
                                return
                            }
                        }
                        self.dismiss(animated: false, completion: nil)
                    }
                }
            }
        }
    }
    
    func navigationbarController(toEnable: Bool) {
        if toEnable {
            self.navigationController?.navigationBar.layer.zPosition = 0
        } else {
            self.navigationController?.navigationBar.layer.zPosition = -64
        }
        self.navigationController?.navigationBar.isUserInteractionEnabled = toEnable
    }
    
    func stringBeforeSpace(str : String) -> String{
        let delimiter = " "
        let token = str.components(separatedBy: delimiter)
        return token[0]
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        
        alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
           // self.view.removeFromSuperview()
             self.dismiss(animated: false, completion: nil)
            let btn = UIButton()
            self.btnCancelAction(btn)
        })
        
        alertViewObj.addAction(title: "Settings".localized, style: .target , action: {
        //self.view.removeFromSuperview()
             self.dismiss(animated: false, completion: nil)
        let btn = UIButton()
         self.btnCancelAction(btn)
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
            }
        })
        
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func addview() {
        // to Hide Language view on touch on screen
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(nextStepTap))
        tapGestureRecognizer.numberOfTapsRequired = 1
        viewBG.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc public func nextStepTap(sender: UITapGestureRecognizer) {
        guard (self.curLocaitonDelegate?.CancelLocationAddress()) != nil else {
            return
        }
       // self.view.removeFromSuperview()
         self.dismiss(animated: false, completion: nil)
    }
}

//MARK:- Current Location Delegates & Methods

extension CurrentLocationVC {
    
    func locationUpdates() {
        
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
        }
        
        LocationManager.sharedInstance.getLocation { (location: CLLocation?, error) in
            if error != nil {
                println_debug(error.debugDescription)
                return
            }
            
            guard let _ = location else { return }
            if let location = location {
                let lat = String(location.coordinate.latitude)
                let long = String(location.coordinate.longitude)
                ReRegistrationModel.shared.Latitude = lat
                ReRegistrationModel.shared.Longitude = long
                GeoLocationManager.shared.getAddressFrom(lattitude: lat, longitude:  long,language: ok_default_language) { (isSuccess, dic) in
                    //print(dic)
                    if isSuccess == false {
                        DispatchQueue.main.async { progressViewObj.removeProgressView() }
                        return
                    }
                    
                    if let dict = dic as? Dictionary<String,String>{
                        let country = dict["Country"]
                        if country == "MM" {
                            if ok_default_language == "en" {
                                self.adress = CurrentAddress.init(dic: dict)
                                DispatchQueue.main.async {
                                    progressViewObj.removeProgressView()
                                    if let adress = self.adress {
                                        self.lbDivisionValue.text = adress.division.replacingOccurrences(of: "Region", with: "Division")
                                        self.lbCityValue.text = adress.city
                                        self.lbStreetValue.text = adress.street
                                        self.lbTownShipValue.text = adress.township
                                        self.btnOk.isHighlighted = false
                                        self.btnOk.tag = 20
                                    }
                                }
                            }else if ok_default_language == "my"{
                                self.adressB = CurrentAddressB.init(dic: dict)
                                DispatchQueue.main.async {
                                    progressViewObj.removeProgressView()
                                    if let adress = self.adressB {
                                        self.lbDivisionValue.text = Rabbit.uni2zg(adress.divisionB)
                                        self.lbCityValue.text = Rabbit.uni2zg(adress.cityB)
                                        self.lbStreetValue.text = Rabbit.uni2zg(adress.streetB)
                                        self.lbTownShipValue.text = Rabbit.uni2zg(adress.townshipB)
                                        GeoLocationManager.shared.getAddressFrom(lattitude: lat, longitude:  long,language: "en") { (isSuccess, dic) in
                                            if isSuccess == false {
                                                DispatchQueue.main.async { progressViewObj.removeProgressView() }
                                                return
                                            }
                                            if let dict = dic as? Dictionary<String,String> {
                                                self.adress = CurrentAddress.init(dic: dict)
                                                DispatchQueue.main.async {
                                                    progressViewObj.removeProgressView()
                                                    if let adress = self.adress {
                                                        println_debug(adress)
                                                        self.btnOk.isHighlighted = false
                                                        self.btnOk.tag = 20
                                                    }
                                                }
                                            }
                                        }
                                        
                                    }
                                }
                            }else {
                                self.adressB = CurrentAddressB.init(dic: dict)
                                DispatchQueue.main.async {
                                    progressViewObj.removeProgressView()
                                    if let adress = self.adressB {
                                        self.lbDivisionValue.text = adress.divisionB
                                        self.lbCityValue.text = adress.cityB
                                        self.lbStreetValue.text = adress.streetB
                                        self.lbTownShipValue.text = adress.townshipB
                                        GeoLocationManager.shared.getAddressFrom(lattitude: lat, longitude:  long,language: "en") { (isSuccess, dic) in
                                            if isSuccess == false {
                                                DispatchQueue.main.async { progressViewObj.removeProgressView() }
                                                return
                                            }
                                            if let dict = dic as? Dictionary<String,String> {
                                                self.adress = CurrentAddress.init(dic: dict)
                                                DispatchQueue.main.async {
                                                    progressViewObj.removeProgressView()
                                                    if let adress = self.adress {
                                                        println_debug(adress)
                                                        self.btnOk.isHighlighted = false
                                                        self.btnOk.tag = 20
                                                    }
                                                }
                                            }
                                        }
                                        
                                    }
                                }
                            }
                        }else {
                            alertViewObj.wrapAlert(title:"", body:"Please select address manually".localized, img: #imageLiteral(resourceName: "alert-icon.png"))
                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                guard (self.curLocaitonDelegate?.CancelLocationAddress()) != nil else {
                                    return
                                }
                               // self.view.removeFromSuperview()
                                self.dismiss(animated: false, completion: nil)
                            })
                            DispatchQueue.main.async {
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                    }
                    }
                }
            }
        }
    }
 struct CurrentAddress {
    var street = ""
    var city = ""
    var division = ""
    var township = ""
 
    
    init(dic: Dictionary<String,String>) {
        if let strt = dic["street"] {
            self.street   = strt
        }
        
        if let cityData = dic["city"] {
            self.city = cityData
        }
        
        if let dvsn = dic["region"] {
            self.division = dvsn
        }
        
        if let ts = dic["township"] {
          self.township = ts
        }
    }
}

struct CurrentAddressB {
    var streetB = ""
    var cityB = ""
    var divisionB = ""
    var townshipB = ""
    
    init(dic: Dictionary<String,String>) {
        self.streetB  = dic["street"] ?? ""
        self.streetB   = dic["street"] ?? ""
        self.cityB     = dic["city"] ?? ""
        self.divisionB = dic["region"] ?? ""
        if let ts = dic["township"] {
             self.townshipB = ts
        }
       
    }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
