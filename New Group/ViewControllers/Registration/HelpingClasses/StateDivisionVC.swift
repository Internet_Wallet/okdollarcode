//
//  StateDivisionVC.swift
//  DemoUpdateProfile
//
//  Created by SHUBH on 9/13/17.
//  Copyright © 2017 SHUBH. All rights reserved.
//

import UIKit


protocol StateDivisionVCDelegate : class {
    func setDivisionStateName(SelectedDic : LocationDetail)
    func setTownshipCityNameFromDivison(Dic : TownShipDetailForAddress)
}

class StateDivisionVC: UIViewController,AddressTownshiopVCDelegate,UITextFieldDelegate{
    
    @IBOutlet weak var tableview: UITableView!
  weak var stateDivisonVCDelegate : StateDivisionVCDelegate?
    var delegateMap: MapDelegateToPop?
    var allLocationsList = [LocationDetail]()
     var arr_city = [LocationDetail]()
     var array = [LocationDetail]()
    var isSearch = false
    var tfSearch = UITextField()
    var CHARSET = ""
    @IBOutlet weak var lblNoRecord: UILabel!{
        didSet {
            lblNoRecord.font  = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet var btnSearchBar: UIBarButtonItem!
    var navigateFrom: String?
    
    //To Dismiss
    var isComingFromAgent = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
            btnSearch.tag = 1
        navigationController?.navigationBar.barTintColor = kYellowColor

       self.view.backgroundColor = kBackGroundGreyColor
        loadInitialize()
        self.setMarqueLabelInNavigation()
    }

    func loadInitialize() {
        lblNoRecord.text = "No Record Found".localized
        lblNoRecord.isHidden = true
        tableview.register(UINib(nibName: "DivisionStateCell", bundle: nil), forCellReuseIdentifier: "divisionstatecellidentifier")
        tableview.tableFooterView = UIView(frame: CGRect.zero)
        arr_city = TownshipManager.allLocationsList
        if ok_default_language == "my"{
            CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ "
        }else  if ok_default_language == "en"{
            CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 "
        }else {
            CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ "
        }
        tableview.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.titleView = nil
        self.setMarqueLabelInNavigation()
        setMarqueLabelInNavigation()
        lblNoRecord.isHidden = true
        isSearch = false
        tfSearch.text = ""
        tableview.reloadData()
    }
   
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: appFontSize)
        lblMarque.text = "Select State / Division".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white
        btnSearch.setTitle("", for: .normal)
        btnSearch.setImage(#imageLiteral(resourceName: "r_search"), for: .normal)
        self.navigationItem.titleView = lblMarque
    }
    
    @IBAction func backAction(_ sender: Any) {
        if isComingFromAgent{
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    @IBAction func searchAction(_ sender: Any) {
        if btnSearch.tag == 1 {
            self.navigationItem.titleView = nil
            self.navigationItem.title = ""
            tfSearch.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
            self.navigationItem.titleView = tfSearch
            tfSearch.backgroundColor = UIColor.white
            tfSearch.delegate = self
            tfSearch.layer.cornerRadius = 5.0
            tfSearch.textColor = UIColor.black
            tfSearch.font = UIFont(name: "Zawgyi-One", size: 17)
            btnSearch.setTitleColor(UIColor.white, for: .normal)
            tfSearch.placeholder = "Search".localized
            //tfSearch.tintColor = UIColor.darkGray
            tfSearch.tintColor = UIColor.init(red: 0/255, green: 144/255, blue: 81/255, alpha: 1)
            tfSearch.delegate = self
            tfSearch.clearButtonMode = .whileEditing
            
            let tfserarcView = UIView()
            tfserarcView.frame = CGRect(x: 10, y: 0, width: 30, height: 30)
            let tfserarchIcon = UIImageView()
            tfserarchIcon.frame = CGRect(x: 8, y: 8, width: 14, height: 14)
            tfserarcView.addSubview(tfserarchIcon)
            tfserarchIcon.image = #imageLiteral(resourceName: "search")
            tfSearch.leftView = tfserarcView
            tfSearch.leftViewMode = UITextField.ViewMode.always
            
            btnSearch.setImage(nil, for: .normal)
            btnSearch.setTitle("Cancel".localized, for: .normal)
            tfSearch.becomeFirstResponder()
            btnSearch.tag = 2
            
        } else if btnSearch.tag == 2{
            btnSearch.tag = 1
            self.navigationController?.view.endEditing(false)
            lblNoRecord.isHidden = true
            isSearch = false
            tfSearch.text = ""
            self.navigationItem.titleView = nil
            self.setMarqueLabelInNavigation()
            tableview.reloadData()
        } else if btnSearch.tag == 3 {
            btnSearch.tag = 2
            self.navigationController?.view.endEditing(true)
            view.endEditing(true)
            self.navigationController?.view.endEditing(false)
            lblNoRecord.isHidden = true
            isSearch = false
            tfSearch.text = ""
            tableview.reloadData()
        }
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        tfSearch.resignFirstResponder()
        return true
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        lblNoRecord.isHidden = true
        tfSearch.text = ""
        btnSearch.tag = 2
        isSearch = false
        tableview.reloadData()
        println_debug("textFieldShouldClear")
        return true
    }

    func setTownshipCityName(cityDic : TownShipDetailForAddress){
        
        guard(self.stateDivisonVCDelegate?.setTownshipCityNameFromDivison(Dic: cityDic) != nil) else {
            return
        }
        
        if isComingFromAgent{
            delegateMap?.setVariable(value: isComingFromAgent)
            self.dismiss(animated: true, completion: nil)
        }else{
          self.navigationController?.popViewController(animated: false)
        }
        
       
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if range.location == 0 && string == " " {
            return false
        }
      if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
        
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            if updatedText == "" {
                isSearch = false
                tableview.reloadData()
                 lblNoRecord.isHidden = true
            }
            else{
                getSearchArrayContains(updatedText)
            }
        }
        return restrictMultipleSpaces(str: string, textField: textField)
    }

    func getSearchArrayContains(_ text : String) {
        if ok_default_language == "my" {
        array =  self.arr_city.filter({$0.stateOrDivitionNameMy.lowercased().contains(find: text.lowercased())})
        }else if ok_default_language == "en"{
        array =  self.arr_city.filter({$0.stateOrDivitionNameEn.lowercased().contains(find: text.lowercased())})
        }else {
        array =  self.arr_city.filter({$0.stateOrDivitionNameUni.lowercased().contains(find: text.lowercased())})
        }

        if array.count == 0 {
            self.btnSearch.tag = 2
             lblNoRecord.isHidden = false
        }else{
            lblNoRecord.isHidden = true
        }
        isSearch = true
        tableview.reloadData()
    }
    
}


extension StateDivisionVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearch {
            return array.count
        }else {
            return arr_city.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let divisionCell = tableView.dequeueReusableCell(withIdentifier: "divisionstatecellidentifier", for: indexPath) as! DivisionStateCell
        
        if isSearch {
            let dic = array[indexPath.row]
            if ok_default_language == "my"{
                divisionCell.wrapData(title:dic.stateOrDivitionNameMy, count: dic.townshipArryForAddress.count)
            }else if ok_default_language == "en"{
                divisionCell.wrapData(title:dic.stateOrDivitionNameEn, count: dic.townshipArryForAddress.count)
            }else {
                divisionCell.wrapData(title:dic.stateOrDivitionNameUni, count: dic.townshipArryForAddress.count)
            }
        }else {
            let dic = arr_city[indexPath.row]
            if ok_default_language == "my"{
                divisionCell.wrapData(title:dic.stateOrDivitionNameMy, count: dic.townshipArryForAddress.count)
            }else  if ok_default_language == "en"{
                divisionCell.wrapData(title:dic.stateOrDivitionNameEn, count: dic.townshipArryForAddress.count)
            }else {
                divisionCell.wrapData(title:dic.stateOrDivitionNameUni, count: dic.townshipArryForAddress.count)
            }
        }
        return divisionCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
  
        
        tableView.deselectRow(at: indexPath, animated: true)
        btnSearch.tag = 1
        let addressTownshiopVC  = self.storyboard?.instantiateViewController(withIdentifier: "AddressTownshiopVC") as! AddressTownshiopVC
        addressTownshiopVC.isComingFromAgent = isComingFromAgent
        addressTownshiopVC.delegateMap = self
        if isSearch {
            if let del = self.stateDivisonVCDelegate {
                del.setDivisionStateName(SelectedDic:array[indexPath.row])
                addressTownshiopVC.selectedDivState = array[indexPath.row]
            }
        }else {
            if let del = self.stateDivisonVCDelegate {
                del.setDivisionStateName(SelectedDic:arr_city[indexPath.row])
                addressTownshiopVC.selectedDivState = arr_city[indexPath.row]
            }
        }
        
        if navigateFrom == "FACEPAY" {
            self.navigationController?.popViewController(animated: false)
            return
        }
        self.setMarqueLabelInNavigation()
        addressTownshiopVC.addressTownshiopVCDelegate = self
        
        if isComingFromAgent{
            let navController = UINavigationController(rootViewController: addressTownshiopVC)
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            self.presentDetail(navController)
        }else{
           self.navigationController?.pushViewController(addressTownshiopVC, animated: false)
        }

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}



//This delegate will be only used when presenting view controller from agent because to manage the flow
extension StateDivisionVC: MapDelegateToPop {
    func setVariable(value: Bool){
        isComingFromAgent = value
    }
}
