//
//  NRCTypesVC.swift
//  DemoUpdateProfile
//
//  Created by SHUBH on 10/9/17.
//  Copyright © 2017 SHUBH. All rights reserved.
//

import UIKit

protocol  NRCTypesVCDelegate{
    func setNrcTypeName(nrcType : String)
}

class NRCTypesVC: UIViewController{

    @IBOutlet weak var viewBG: UIView!
    var nRCTypesVCDelegate : NRCTypesVCDelegate?

    @IBOutlet weak var btnP: UIButton!
    @IBOutlet weak var btnN: UIButton!
    @IBOutlet weak var btnE: UIButton!
    @IBOutlet weak var btnT: UIButton!
    var tranView = UIView()
    override func viewDidLoad() {
        super.viewDidLoad()
       // println_debug(strButtonName)
        
        addview()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func addview() {
        // to Hide Language view on touch on screen
     
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(nextStepTap))
        tapGestureRecognizer.numberOfTapsRequired = 1
        viewBG.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc public func nextStepTap(sender: UITapGestureRecognizer) {
        //self.view.removeFromSuperview()
             self.dismiss(animated: false, completion: nil)
       // tranView.removeFromSuperview()
    }
    
    

    @IBAction func btnNrcTypeAction(_ sender: UIButton) {
        guard (self.nRCTypesVCDelegate?.setNrcTypeName(nrcType: (sender.titleLabel?.text)!) != nil) else {
            return
        }
        navigationbarController(toEnable: true)
        //self.view.removeFromSuperview()
        self.dismiss(animated: false, completion: nil)
    }
    
    func navigationbarController(toEnable: Bool) {
        if toEnable {
            self.navigationController?.navigationBar.layer.zPosition = 0
        } else {
            self.navigationController?.navigationBar.layer.zPosition = -64
        }
        self.navigationController?.navigationBar.isUserInteractionEnabled = toEnable
    }
    
   public func setName(nrcName : String) {
    let first = nrcName.components(separatedBy: "(").first!
    
    let fnt = UIFont(name: appFont, size: appButtonSize) ?? UIFont.systemFont(ofSize: 18.0)
//    if ok_default_language == "uni" {
//        fnt = UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18.0)
//    }
    self.btnN.titleLabel?.font =  fnt
    self.btnP.titleLabel?.font =  fnt
    self.btnE.titleLabel?.font =  fnt
    self.btnT.titleLabel?.font =  fnt
    
    if ok_default_language == "my"{
        self.btnN.setTitle("\(first)" + "(ႏိုင္)", for: .normal)
        self.btnP.setTitle("\(first)" + "(ျပဳ)", for: .normal)
        self.btnE.setTitle("\(first)" + "(ဧည့္)", for: .normal)
        self.btnT.setTitle("\(first)" + "(သာ)", for: .normal)
    }else if ok_default_language == "en" {
        self.btnN.setTitle("\(first)" + "(N)", for: .normal)
        self.btnP.setTitle("\(first)" + "(P)", for: .normal)
        self.btnE.setTitle("\(first)" + "(E)", for: .normal)
        self.btnT.setTitle("\(first)" + "(T)", for: .normal)
    }else {
        self.btnN.setTitle("\(first)" + "(နိုင်)", for: .normal)
        self.btnP.setTitle("\(first)" + "(ပြု)", for: .normal)
        self.btnE.setTitle("\(first)" + "(ဧည့်)", for: .normal)
        self.btnT.setTitle("\(first)" + "(သာ)", for: .normal)
    }
    }
    
  }
