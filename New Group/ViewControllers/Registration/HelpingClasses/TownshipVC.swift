 //
 //  TownshipVC.swift
 //  DemoUpdateProfile
 //
 //  Created by SHUBH on 8/21/17.
 //  Copyright © 2017 SHUBH. All rights reserved.
 //
 
 import UIKit
 
 protocol TownShipVcDeletage : class {
    func setDivisionAndTownship(strDivison_township : String)
    func setDivison(division : LocationDetail)
 }
 
 class TownshipVC: OKBaseController,TownshipVC2Delegate,UITextFieldDelegate{
  weak var townShipDelegate : TownShipVcDeletage?
    
    @IBOutlet weak var btnSearch: UIButton!
    var allLocationsList = [LocationDetail]()
     var tfSearch = UITextField()
  //  @IBOutlet weak var btnSearch: UIBarButtonItem!
    
    
    @IBOutlet weak var lblNoRecordFound: UILabel!
    @IBOutlet weak var tableview: UITableView!
    var arr_city = [LocationDetail]()
    var isSearch = false
    var array = [LocationDetail]()
      var CHARSET = ""
    var registrationScreen = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        btnSearch.titleLabel?.font = UIFont(name: appFont, size: 13)
        tableview.tableFooterView = UIView.init(frame: CGRect.zero)
        self.view.backgroundColor = kBackGroundGreyColor
        btnSearch.tag = 1
        loadInitialize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.titleView = nil
        self.setMarqueLabelInNavigation()
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        lblNoRecordFound.isHidden = true
        tfSearch.text = ""
        isSearch = false
        tableview.reloadData()
    }
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func searchAction(_ sender: Any) {
         if btnSearch.tag == 1 {
            self.navigationItem.titleView = nil
            self.navigationItem.title = ""
           // let tfSearch = UITextField()
            tfSearch.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
            self.navigationItem.titleView = tfSearch
            tfSearch.backgroundColor = UIColor.white
            tfSearch.delegate = self
            tfSearch.layer.cornerRadius = 5.0
            tfSearch.textColor = UIColor.black
            tfSearch.tintColor = UIColor.init(red: 0/255, green: 144/255, blue: 81/255, alpha: 1)
            tfSearch.font = UIFont(name: "Zawgyi-One", size: 17)
            btnSearch.setTitleColor(UIColor.white, for: .normal)
            tfSearch.placeholder = "Search".localized
            tfSearch.delegate = self
            tfSearch.clearButtonMode = .whileEditing
            
            let tfserarcView = UIView()
            tfserarcView.frame = CGRect(x: 10, y: 0, width: 30, height: 30)
            let tfserarchIcon = UIImageView()
            tfserarchIcon.frame = CGRect(x: 8, y: 8, width: 14, height: 14)
            tfserarcView.addSubview(tfserarchIcon)
            tfserarchIcon.image = #imageLiteral(resourceName: "search")
            tfSearch.leftView = tfserarcView
            tfSearch.leftViewMode = UITextField.ViewMode.always
            
            btnSearch.setImage(nil, for: .normal)
            btnSearch.setTitle("Cancel".localized, for: .normal)
            btnSearch.setTitleColor(UIColor.white, for: .normal)
            tfSearch.becomeFirstResponder()
            btnSearch.tag = 2
    } else if btnSearch.tag == 2{
            btnSearch.tag = 1
            self.navigationController?.view.endEditing(false)
            isSearch = false
            tableview.reloadData()
            tfSearch.text = ""
            lblNoRecordFound.isHidden = true
            self.navigationItem.titleView = nil
            self.setMarqueLabelInNavigation()
         } else if btnSearch.tag == 3 {
            btnSearch.tag = 2
            lblNoRecordFound.isHidden = true
            tfSearch.text = ""
            self.navigationController?.view.endEditing(true)
            view.endEditing(true)
            isSearch = false
            tableview.reloadData()
        
        }
    }
    
   private func setMarqueLabelInNavigation() {
       self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: appFontSize)
        lblMarque.text = "Select NRC Code ".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
        btnSearch.setTitle("", for: .normal)
        btnSearch.setImage(#imageLiteral(resourceName: "r_search"), for: .normal)
        lblNoRecordFound.isHidden = true
        lblNoRecordFound.text = "No Record Found".localized
    }
    
    
    func loadInitialize() {
        tableview.register(UINib(nibName: "DivisionStateCell", bundle: nil), forCellReuseIdentifier: "divisionstatecellidentifier")
        tableview.tableFooterView = UIView(frame: CGRect.zero)
    
        arr_city = TownshipManager.allLocationsList
        if ok_default_language == "my"{
        CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ "
        }else if ok_default_language == "en"{
        CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 "
        }else {
        CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ "
        }

        DispatchQueue.main.async {
          self.tableview.reloadData()
        }
    }
    
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        tfSearch.resignFirstResponder()
        return true
    }

    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        lblNoRecordFound.isHidden = true
        tfSearch.text = ""
        btnSearch.tag = 2
        isSearch = false
        tableview.reloadData()
        return true
    }
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if range.location == 0 && string == " " {
            return false
        }
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
            if updatedText == "" {
                isSearch = false
                lblNoRecordFound.isHidden = true
                tableview.reloadData()
            }
            else{
                getSearchArrayContains(updatedText)
           }
        }
           return restrictMultipleSpaces(str: string, textField: textField)
    }
    
    func getSearchArrayContains(_ text : String) {
        if ok_default_language == "my" {
        array = self.arr_city.filter({$0.nrcDisplayStateDivMY.lowercased().contains(find: text.lowercased())})
        }else if ok_default_language == "en"{
        array = self.arr_city.filter({$0.nrcDisplayStateDivEN.lowercased().contains(find: text.lowercased())})
        }else{
            array = self.arr_city.filter({$0.nrcDisplayStateDivUni.lowercased().contains(find: text.lowercased())})
        }

        if array.count == 0 {
            lblNoRecordFound.isHidden = false
            self.btnSearch.tag = 2
            self.btnSearch.tintColor = UIColor.lightGray
        }else {
          lblNoRecordFound.isHidden = true
        }
        isSearch = true
        tableview.reloadData()
    }
 }
 
 
 extension TownshipVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearch {
            return array.count
        }else {
           return arr_city.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "divisionstatecellidentifier", for: indexPath) as! DivisionStateCell
        if isSearch {
            let dic = array[indexPath.row]
            if ok_default_language == "my"{
                cell.wrapData(title: dic.nrcDisplayStateDivMY, count: dic.townShipArray.count)
            }else if ok_default_language == "en" {
                cell.wrapData(title: dic.nrcDisplayStateDivEN, count: dic.townShipArray.count)
            }else{
                cell.wrapData(title: dic.nrcDisplayStateDivUni, count: dic.townShipArray.count)
            }
        }else {
            let dic = arr_city[indexPath.row]
            if ok_default_language == "my"{
                cell.wrapData(title: dic.nrcDisplayStateDivMY , count: dic.townShipArray.count)
            }else if ok_default_language == "en"{
                cell.wrapData(title: dic.nrcDisplayStateDivEN, count: dic.townShipArray.count)
            }else{
                cell.wrapData(title: dic.nrcDisplayStateDivUni, count: dic.townShipArray.count)
            }
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let townshipVC2 = self.storyboard?.instantiateViewController(withIdentifier: "TownshipVC2") as! TownshipVC2
        if isSearch {
            if let delegate = self.townShipDelegate {
                delegate.setDivison(division: array[indexPath.row])
                townshipVC2.selectedDivState = array[indexPath.row]
            }
        }else {
            if let delegate = self.townShipDelegate {
                delegate.setDivison(division: arr_city[indexPath.row])
                townshipVC2.selectedDivState = arr_city[indexPath.row]
            }
        }
       // self.setMarqueLabelInNavigation()
       // btnSearch.setImage(UIImage(named: "r_search"), for: .normal)
        btnSearch.tag = 1
        if registrationScreen == "Registration"{
            townshipVC2.registrationScreen = registrationScreen
        }
        townshipVC2.townshipVC2Delegate = self
        self.navigationController?.pushViewController(townshipVC2, animated: false)
    }
    
    func NRCValueString(nrcSting : String) {
        self.navigationController?.popViewController(animated: false)
        guard (self.townShipDelegate?.setDivisionAndTownship(strDivison_township: nrcSting) != nil) else {
            return
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
 }

 
