//
//  selectReferralNumberVc.swift
//  OK
//
//  Created by Subhash Arya on 13/03/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol ReferralVCDelegate {
    func dismissWithDetail(selectedOption : String, selfView : UIViewController)
}

class selectReferralNumberVc: UIViewController {
    var delegate : ReferralVCDelegate?
    @IBOutlet weak var btnMyself: UIButton!
    @IBOutlet weak var btnOthers: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnMyself.setTitle("Myself".localized, for: .normal)
        self.btnOthers.setTitle("Others".localized, for: .normal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func dismissView(_ sender: Any) {
      //  self.view.removeFromSuperview()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func selectAction(_ sender: UIButton) {
            if let dl = self.delegate {
                if sender.tag == 1{
                    dl.dismissWithDetail(selectedOption: "MySelf".localized,selfView: self)
                }else {
                  dl.dismissWithDetail(selectedOption: "Others".localized,selfView: self)
                }
            }
        self.dismiss(animated: false, completion: nil)
    }
}
