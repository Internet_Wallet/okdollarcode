//
//  BusinessHoursVC.swift
//  OK
//
//  Created by Subhash Arya on 12/04/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol BusinessHoursVCDelegte: class {
    func setTime(viewcontroller : UIViewController, Opentime : String, Cloestime : String)
}

class BusinessHoursVC: OKBaseController {
    weak var delegate : BusinessHoursVCDelegte?
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var btnHourMode: UIButton!
    @IBOutlet weak var lblHourMode: UILabel!
    @IBOutlet weak var lblHoursStatus: UILabel!
    @IBOutlet weak var btnSetHours: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    var dateFormatterGet = DateFormatter()
    var dateFormatter24 = DateFormatter()
    var OpenTime = ""
    var CloseTime = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
                   } else {
                       // Fallback on earlier versions
                   }
        dateFormatterGet.calendar = Calendar(identifier: .gregorian)
        dateFormatter24.calendar = Calendar(identifier: .gregorian)

            dateFormatterGet.dateFormat = "hh:mm a"
            dateFormatter24.dateFormat = "HH:mm"
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    @IBAction func tapGesture(_ sender: Any) {
        self.view.removeFromSuperview()
    }
    
    @IBAction func CloseAlert(_ sender: Any) {
        self.view.removeFromSuperview()
    }
    @IBAction func HourModeAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
          btnSetHours.setTitle("DONE".localized, for: .normal)
        }else {
           btnSetHours.setTitle("SET".localized, for: .normal)
        }
    }
    
    @IBAction func setHoursAction(_ sender: Any) {
        let selectedTime = dateFormatterGet.string(from: datePicker.date)
        println_debug("selectedDate")
        println_debug(selectedTime)
        if btnHourMode.isSelected {
            if let del = self.delegate {
                del.setTime(viewcontroller: self,Opentime: "24 Hours".localized, Cloestime:"24 Hours".localized )
            }
        }else {
            if lblHoursStatus.text == "Open Time".localized {
                OpenTime = selectedTime
                lblHoursStatus.text = "Close Time".localized
            }else if lblHoursStatus.text == "Close Time".localized{
                CloseTime = selectedTime
                if let del = self.delegate {
                    del.setTime(viewcontroller: self,Opentime: OpenTime, Cloestime: CloseTime )
                }
            }
        }
    }
    
    @IBAction func datePickerAction(_ sender: UIDatePicker) {
        let selectedDate = dateFormatterGet.string(from: sender.date)
        println_debug("selectedDate")
        println_debug(selectedDate)
    }
    
    
}
