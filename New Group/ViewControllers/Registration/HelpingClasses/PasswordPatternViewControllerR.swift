//
//  PasswordPatternViewController.swift
//  OKPayment
//
//  Created by Ashish Kumar Singh on 7/17/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol PasswordPatternViewControllerDelegate {
    func ShowPatternPasswordViewAndPasswordString(strPassword : String, PasswordPatternViewControllerR : UIViewController)
    func CancelPatternPasswordVC(PasswordPatternViewControllerR : UIViewController)
}

 class PasswordPatternViewControllerR: OKBaseController{
    @IBOutlet var patternLock: GesturePatternLock!
    var delegate : PasswordPatternViewControllerDelegate?
    @IBOutlet weak var btnCancel: UIButton!{
        didSet{
            btnCancel.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnContinue: UIButton!{
        didSet{
            btnContinue.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var lblCreateyourPattern: UILabel!{
        didSet{
            lblCreateyourPattern.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    var strPassword = ""
    var passwordString = ""
    var checkVC = ""
    var mobilNumber: String?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        //verify
        if VerifyUPModel.share.DrowCurrentPatternTag == "verify"{
          lblCreateyourPattern.text = "Draw Your current Pattern".localized
        }else {
           lblCreateyourPattern.text = "Create Your Pattern".localized
        }
        btnCancel.setTitle("Cancel".localized, for: .normal)
        btnContinue.setTitle("Continue".localized, for: .normal)
        self.navigationItem.title = "Pattern"
        setuplock()
        if checkVC == "VUP"{
          self.btnContinue.setTitle("Done".localized,for: .normal)
        
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    
    private func setuplock() {
        
        // Set number of sensors
        patternLock.lockSize = (3, 3)
        
        // Sensor grid customisations
        patternLock.edgeInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        
        // Sensor point customisation (normal)
        patternLock.setSensorAppearance(
            type: .inner,
            radius: 5,
            width: 5,
            color: UIColor(displayP3Red: 247.0/255.0, green:199.0/255.0 , blue: 0.0/255.0, alpha: 1.0),
            forState: .normal
        )
        patternLock.setSensorAppearance(
            type: .outer,
            color: .gray,
            forState: .normal
        )
        
        // Sensor point customisation (selected)
        patternLock.setSensorAppearance(
            type: .inner,
            radius: 5, //6 6 white
            width: 5,
            color: UIColor(displayP3Red: 247.0/255.0, green:199.0/255.0 , blue: 0.0/255.0, alpha: 1.0),
            forState: .selected
        )
        patternLock.setSensorAppearance(
            type: .outer,
            radius: 40,
            width: 5,
            color: .gray,
            forState: .selected
        )
        
        // Sensor point customisation (wrong password)
        patternLock.setSensorAppearance(
            type: .inner,
            radius: 03,
            width: 15,
            color: .red,
            forState: .error
        )
        patternLock.setSensorAppearance(
            type: .outer,
            radius: 0,
            width: 1,
            color: .red,
            forState: .error
        )
        
        // Line connecting sensor points (normal/selected)
        [GesturePatternLock.GestureLockState.normal, GesturePatternLock.GestureLockState.selected].forEach { (state) in
            patternLock.setLineAppearance(
                width: 5.5, color: UIColor(red: 241/255, green: 188/255, blue: 20/255, alpha: 1.0), // UIColor.clear UIColor.yellow.withAlphaComponent(0.5)
                forState: state
            )
        }
        
        // Line connection sensor points (wrong password)
        patternLock.setLineAppearance(
            width: 5.5, color: UIColor.red.withAlphaComponent(0.5),
            forState: .error
        )
        
        patternLock.addTarget(
            self, action: #selector(gestureComplete),
            for: .gestureComplete
        )
        
    }
    
    func capturePasswordWithDetails(_ code: String) -> (password: String,type: Bool) {
        return (retainingEncryptedPassword(code),false)
    }
    
    private  func retainingEncryptedPassword(_ aCode: String) -> String {
        
        if passwordString == "" {
            alertViewObj.wrapAlert(title: "", body: "Draw the Pattern to Login".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
            })
            alertViewObj.showAlert(controller: self)
        }
        return passwordString
    }
    
    
    @objc func gestureComplete(gestureLock: GesturePatternLock){

        passwordString = ""
        for element in gestureLock.lockSequence {
            passwordString.append(element.stringValue)
        }
        self.btnCancel.setTitle("Reset".localized,for: .normal)
        
        if checkVC == "VUP" {
              strPassword = passwordString
        }
        
        if self.btnContinue.titleLabel!.text == "Continue".localized{
            if passwordString.count > 3 {
                strPassword = passwordString
                passwordString = ""
            }
        }else{
            if !(strPassword == passwordString){
//             strPassword = ""
//            showAlert(txtTitle: "", body:"You have Drawn Incorrect Pattern. Please try again".localized , titleImg: #imageLiteral(resourceName: "r_password"), btnTitle: "OK".localized)
//            lblCreateyourPattern.text = "Create Your Pattern".localized
//             gestureLock.gestureLockState = .normal
//                self.btnCancel.setTitle("Cancel".localized,for: .normal)
//                self.btnContinue.setTitle("Continue".localized,for: .normal)
//             self.patternLock.resetLock()
            }else{
              //gestureLock.gestureLockState = .normal
            }
        }
    }
    
    
    @IBAction func back(_ sender: Any) {
        guard (self.delegate?.CancelPatternPasswordVC(PasswordPatternViewControllerR: self) != nil) else {
                    return
                }
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        self.patternLock.resetLock()
        if btnCancel.titleLabel!.text == "Cancel".localized {
            guard (self.delegate?.CancelPatternPasswordVC(PasswordPatternViewControllerR: self) != nil) else {
                return
            }
        }else if btnCancel.titleLabel!.text == "Reset".localized{
            if checkVC == "VUP"{
                self.btnContinue.setTitle("Done".localized,for: .normal)
                lblCreateyourPattern.text = "Draw Your current Pattern".localized
            }else {
             //   self.btnContinue.setTitle("Continue".localized,for: .normal)
               // lblCreateyourPattern.text = "Create Your Pattern".localized
            }
            self.btnCancel.setTitle("Cancel".localized,for: .normal)
         //   strPassword = ""
        }
    }
    @IBAction func loginPattern(_ sender: UIButton) {
       
        if self.btnContinue.titleLabel!.text == "Continue".localized {
            self.btnCancel.setTitle("Cancel".localized,for: .normal)
            if strPassword == "" || strPassword.count < 4{
                self.patternLock.resetLock()
                showAlert(txtTitle: "", body:"Please Draw Minimum 4 Dots Pattern".localized , titleImg: #imageLiteral(resourceName: "r_password"), btnTitle: "OK".localized)
                 self.btnCancel.setTitle("Cancel".localized,for: .normal)
            }else {
                lblCreateyourPattern.text = "Confirm Your Pattern Again".localized
                self.btnContinue.setTitle("Confirm".localized,for: .normal)
                //self.btnCancel.setTitle("Reset",for: .normal)
                self.patternLock.resetLock()
            }
        }
        else if self.btnContinue.titleLabel!.text == "Done".localized {
            self.btnCancel.setTitle("Cancel".localized,for: .normal)
            if strPassword == "" || strPassword.count < 4{
                self.patternLock.resetLock()
                showAlert(txtTitle: "", body:"Please Draw Minimum 4 Dots Pattern".localized , titleImg: #imageLiteral(resourceName: "r_password"), btnTitle: "OK".localized)
                 self.btnCancel.setTitle("Cancel".localized,for: .normal)
            }else {
                if checkVC == "VUP"{
                    let object = PatternValidationClass()
                        
                        var numStr = ""
                        if let no = mobilNumber {
                         numStr = no
                        }
                     
                        if numStr == "" {
                            numStr = UserModel.shared.mobileNo
                        }
                        if numStr == "" {
                            numStr = ReRegistrationModel.shared.MobileNumber
                        }
                        if numStr == "" {
                            numStr = VerifyUPModel.share.MobileNumber
                        }
                      let finalString = object.patternEncryption(strPassword, withnumber: numStr)
                        guard(self.delegate?.ShowPatternPasswordViewAndPasswordString(strPassword : finalString!, PasswordPatternViewControllerR : self)) != nil else {
                        return
                    }
                }
                else{
                    lblCreateyourPattern.text = "Confirm Your Pattern Again".localized
                    self.btnContinue.setTitle("Confirm".localized,for: .normal)
                    //self.btnCancel.setTitle("Reset",for: .normal)
                    self.patternLock.resetLock()
                }
              
            }
        }
        
        else {
                if !(strPassword == passwordString){
               // strPassword = ""
                showAlert(txtTitle: "", body:"You have Drawn Incorrect Pattern. Please try again".localized , titleImg: #imageLiteral(resourceName: "r_password"), btnTitle: "OK".localized)
                self.btnCancel.setTitle("Cancel".localized,for: .normal)
             //   lblCreateyourPattern.text = "Create Your Pattern".localized
                self.patternLock.resetLock()
              //  self.btnCancel.setTitle("Cancel".localized,for: .normal)
              //  self.btnContinue.setTitle("Continue".localized,for: .normal)
                self.patternLock.resetLock()
            } else{
                let object = PatternValidationClass()
                    
                    var numStr = ""
                    if let no = mobilNumber {
                     numStr = no
                    }
                 
                    if numStr == "" {
                        numStr = UserModel.shared.mobileNo
                    }
                    if numStr == "" {
                        numStr = ReRegistrationModel.shared.MobileNumber
                    }
                    if numStr == "" {
                        numStr = VerifyUPModel.share.MobileNumber
                    }
                  let finalString = object.patternEncryption(strPassword, withnumber: numStr)
                    guard(self.delegate?.ShowPatternPasswordViewAndPasswordString(strPassword : finalString!, PasswordPatternViewControllerR : self)) != nil else {
                    return
                }
            }
        }
    }
    
    func showAlert(txtTitle : String ,body : String,titleImg : UIImage, btnTitle : String){
        alertViewObj.wrapAlert(title: txtTitle, body: body, img:titleImg)
        alertViewObj.addAction(title: btnTitle, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
}
