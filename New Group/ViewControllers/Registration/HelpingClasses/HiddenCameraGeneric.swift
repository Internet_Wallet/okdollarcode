//
//  HiddenCameraGeneric.swift
//  OK
//
//  Created by Subhash Arya on 05/03/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import AVFoundation

protocol HiddenCameraDelegate {
    func didCameraFinishedUpdate(success: Bool , images : [UIImage])
}

class GenericPhotoClass: NSObject,AVCaptureVideoDataOutputSampleBufferDelegate {
    
    var captureSession = AVCaptureSession()
    var previewLayer : CALayer!
    var takePhoto = false
    var cameraOpen = false
    var timer : Timer!
    var imageArray = [UIImage]()
    var fromPayto = false
    
    var delegate : HiddenCameraDelegate?
    
    func beginSession() {
        
        let captureDevice = AVCaptureDevice.default(.builtInWideAngleCamera,
                                                    for: AVMediaType.video,
                                                    position: .front)
        do {
            let cat = try AVCaptureDeviceInput(device: captureDevice!)
            captureSession.addInput(cat)
        }catch{
            println_debug(error.localizedDescription)
        }
        
        self.previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        captureSession.startRunning()
        let dataoutput = AVCaptureVideoDataOutput()
        dataoutput.videoSettings = [((kCVPixelBufferPixelFormatTypeKey as NSString) as  String):NSNumber(value:kCVPixelFormatType_32BGRA)]
        dataoutput.alwaysDiscardsLateVideoFrames = true
        
        if captureSession.canAddOutput(dataoutput){
            captureSession.addOutput(dataoutput)
        }
        captureSession.commitConfiguration()
        let queue = DispatchQueue(label: "com.VBHiddenCamera")
        dataoutput.setSampleBufferDelegate(self, queue: queue)
    }
    
    func stopSession(handle: (_ cameraUpdate: Bool) -> Void) {
        self.captureSession.stopRunning()
        if let inputs = captureSession.inputs as? [AVCaptureDeviceInput]{
            for input in inputs {
                self.captureSession.removeInput(input)
            }
            handle(true)
        }
    }
    
    func getImagefromBuffer(buffer : CMSampleBuffer) -> UIImage? {
        if let pixelBuffer = CMSampleBufferGetImageBuffer(buffer){
            let ciImage = CIImage(cvPixelBuffer: pixelBuffer)
            let context = CIContext()
            let imageRect = CGRect(x: 0, y: 0, width:CVPixelBufferGetWidth(pixelBuffer) , height:CVPixelBufferGetHeight(pixelBuffer))
            if let image = context.createCGImage(ciImage, from: imageRect){
                return UIImage(cgImage: image, scale: UIScreen.main.scale, orientation: .right)
            }
        }
        return nil
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection){
        if takePhoto {
            takePhoto = false
            if fromPayto {
                DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 0.5) {
                    self.takePhoto = true
                }
            }
            if let image = self.getImagefromBuffer(buffer: sampleBuffer){
                // UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                if imageArray.count > 3 {
                    timer.invalidate()
                    stopSession(handle: { (success) in
                        if let delegate = self.delegate {
                            delegate.didCameraFinishedUpdate(success: success , images: imageArray)
                        }
                    })
                }else {
                    imageArray.append(image)
                }
            }
        }
    }
    
    func cameraStart(){
        imageArray.removeAll()
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
        beginSession()
        if fromPayto {
            takePhoto = true
        }
        timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(GenericPhotoClass.photo), userInfo:nil, repeats: true)
    }
    
    @objc func photo() {
        takePhoto = true
    }
    
    
}

