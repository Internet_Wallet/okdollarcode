//
//  BusinessCategoryVC.swift
//  DemoUpdateProfile
//
//  Created by SHUBH on 8/30/17.
//  Copyright © 2017 SHUBH. All rights reserved.
//

import UIKit

protocol BusinessCategoryVCDelegate : class{
    
    func setBusinessCategoryNameImage(Dicti : CategoryDetail)
    func setBusinessSubcategoryNameImage(sub_dictionary :SubCategoryDetail)
}

class BusinessCategoryVC: UIViewController,BusinessSubCategoryVCDelegate,UITextFieldDelegate{

    @IBOutlet var tableview: UITableView!
  weak var bCatDelegate:BusinessCategoryVCDelegate?
    var category = CategoriesManager.categoriesList
    @IBOutlet weak var lblNoRecordFound: UILabel!{
        didSet{
            lblNoRecordFound.font = UIFont (name: appFont, size: appFontSize)
        }
    }
    var isSearch = false
    var array = [CategoryDetail]()
    var tfSearch = UITextField()
        var CHARSET = ""
    @IBOutlet weak var btnSearch: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblNoRecordFound.text = "No Record Found".localized
        if ok_default_language == "my"{
            CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ "
            array = category.sorted(by: { $0.categoryBurmeseName.lowercased() < $1.categoryBurmeseName.lowercased()} )
        }else if ok_default_language == "en"{
            CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 "
            array = category.sorted(by: { $0.categoryName.lowercased() < $1.categoryName.lowercased()} )
        }else {
            CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ "
            array = category.sorted(by: { $0.categoryBurmeseUName.lowercased() < $1.categoryBurmeseUName.lowercased()} )
        }
        
        tableview.tableFooterView = UIView(frame: CGRect.zero)
        tableview.delegate = self
        tableview.dataSource = self
        
        category.removeAll()
        category = array
        tableview.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func setUpMenuButton(){
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.setImage(UIImage(named:"menuIcon"), for: .normal)
       // menuBtn.addTarget(self, action: #selector(vc.onMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        self.navigationItem.leftBarButtonItem = menuBarItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.titleView = nil
        self.setMarqueLabelInNavigation()
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        isSearch = false
        array = category
         tfSearch.text = ""
        tableview.reloadData()
    }
    
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated:false)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        if btnSearch.tag == 1 {
            self.navigationItem.titleView = nil
            self.navigationItem.title = ""
            tfSearch.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
            tfSearch.autocorrectionType = .no
            self.navigationItem.titleView = tfSearch
            tfSearch.backgroundColor = UIColor.white
            tfSearch.delegate = self
            tfSearch.layer.cornerRadius = 5.0
            tfSearch.textColor = UIColor.black
            tfSearch.font = UIFont(name: appFont, size: 17)
            btnSearch.setTitleColor(UIColor.white, for: .normal)
            tfSearch.placeholder = "Search".localized
            tfSearch.tintColor = UIColor.init(red: 0/255, green: 144/255, blue: 81/255, alpha: 1)
            tfSearch.delegate = self
            tfSearch.clearButtonMode = .whileEditing
            
            let tfserarcView = UIView()
            tfserarcView.frame = CGRect(x: 10, y: 0, width: 30, height: 30)
            let tfserarchIcon = UIImageView()
            tfserarchIcon.frame = CGRect(x: 8, y: 8, width: 14, height: 14)
            tfserarcView.addSubview(tfserarchIcon)
            tfserarchIcon.image = #imageLiteral(resourceName: "search")
            tfSearch.leftView = tfserarcView
            tfSearch.leftViewMode = UITextField.ViewMode.always
            
            btnSearch.setImage(nil, for: .normal)
            btnSearch.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnSearch.setTitle("Cancel".localized, for: .normal)
            tfSearch.becomeFirstResponder()
            btnSearch.tag = 2
        } else if btnSearch.tag == 2 {
            //called when cancel button pressed
            btnSearch.tag = 1
            self.navigationController?.view.endEditing(false)
            tfSearch.text = ""
            isSearch = false
            self.setMarqueLabelInNavigation()
            tableview.reloadData()
        } else if btnSearch.tag == 3 {
            btnSearch.tag = 2
            tfSearch.text = ""
            self.navigationController?.view.endEditing(true)
            view.endEditing(true)
            isSearch = false
            tableview.reloadData()
        }
    }
    
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: 18)
        lblMarque.text = "Select Business Category".localized
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
        btnSearch.setTitle("", for: .normal)
        btnSearch.setImage(#imageLiteral(resourceName: "r_search"), for: .normal)
        lblNoRecordFound.isHidden = true
    }
    
    func backFromBusinessSubCategoryVCWithNameImage(dicSubCate : SubCategoryDetail) {
         self.navigationController?.popViewController(animated: false)
        guard (self.bCatDelegate?.setBusinessSubcategoryNameImage(sub_dictionary : dicSubCate) != nil) else {
            return
        }
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        tfSearch.resignFirstResponder()
        return true
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        lblNoRecordFound.isHidden = true
        tfSearch.text = ""
        btnSearch.tag = 2
        isSearch = false
        tableview.reloadData()
        tableview.reloadData()
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if range.location == 0 && string == " " {
            return false
        }
        let cs = NSCharacterSet(charactersIn: CHARSET).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        if !(string == filtered) {
            return false
        }
        
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            if updatedText == "" {
                isSearch = false
                 lblNoRecordFound.isHidden = true
                tableview.reloadData()
            }
            else{
                getSearchArrayContains(updatedText)
            }
        }
        return restrictMultipleSpaces(str: string, textField: textField)
    }
    
    func getSearchArrayContains(_ text : String) {
        if ok_default_language == "my" {
            array = self.category.filter({$0.categoryBurmeseName.contains(find: text)})
        }else if ok_default_language == "en" {
           array = self.category.filter({$0.categoryName.contains(find: text)})
        }else {
            array = self.category.filter({$0.categoryBurmeseUName.contains(find: text)})
        }
       
        isSearch = true
        tableview.reloadData()
        
        if array.count == 0 {
            lblNoRecordFound.isHidden = false
            self.btnSearch.tag = 2
            self.btnSearch.tintColor = UIColor.lightGray
        }else {
           lblNoRecordFound.isHidden = true
        }
    }
    
}

extension BusinessCategoryVC : UITableViewDelegate,UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearch {
            return array.count
        }else{
            return category.count
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessCategoryCell", for: indexPath) as? BusinessCategoryCell
        if isSearch {
        let dic = array[indexPath.row]
            if ok_default_language == "my" {
                cell?.setBusinessCategoryNameAndImage(businessCategoryName: dic.categoryBurmeseName, imageName: dic.category_image)
            }else if ok_default_language == "en"{
                cell?.setBusinessCategoryNameAndImage(businessCategoryName: dic.categoryName, imageName: dic.category_image)
            }else {
                cell?.setBusinessCategoryNameAndImage(businessCategoryName: dic.categoryBurmeseUName, imageName: dic.category_image)
            }
        }else {
        let dic = category[indexPath.row]
            if ok_default_language == "my" {
                cell?.setBusinessCategoryNameAndImage(businessCategoryName: dic.categoryBurmeseName, imageName: dic.category_image)
            }else if ok_default_language == "en"{
                cell?.setBusinessCategoryNameAndImage(businessCategoryName: dic.categoryName, imageName: dic.category_image)
            }else {
                cell?.setBusinessCategoryNameAndImage(businessCategoryName: dic.categoryBurmeseUName, imageName: dic.category_image)
            }
        }
        return cell!
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if isSearch {
           let dic = array[indexPath.row]
            guard (self.bCatDelegate?.setBusinessCategoryNameImage(Dicti : dic) != nil) else {
                return
            }
            btnSearch.tag = 1
            let businessSubCategoryVC  = self.storyboard?.instantiateViewController(withIdentifier: "BusinessSubCategoryVC") as! BusinessSubCategoryVC
            businessSubCategoryVC.businessSubCategoryVCDelegate = self;
            businessSubCategoryVC.dicName = dic
            self.navigationController?.pushViewController(businessSubCategoryVC, animated: false)
        }else {
            let dic = category[indexPath.row]
            guard (self.bCatDelegate?.setBusinessCategoryNameImage(Dicti : dic) != nil) else {
                return
            }
            btnSearch.tag = 1
            let businessSubCategoryVC  = self.storyboard?.instantiateViewController(withIdentifier: "BusinessSubCategoryVC") as! BusinessSubCategoryVC
            businessSubCategoryVC.businessSubCategoryVCDelegate = self;
            businessSubCategoryVC.dicName = dic
            self.navigationController?.pushViewController(businessSubCategoryVC, animated: false)
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

class BusinessCategoryCell: UITableViewCell {
    
    @IBOutlet var lblBusinessCategoryName: MarqueeLabel!{
        didSet{
            lblBusinessCategoryName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgCategory: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setBusinessCategoryNameAndImage(businessCategoryName : String ,imageName: String) {
        self.lblBusinessCategoryName.text = businessCategoryName
        imgCategory.image = UIImage(named: imageName)
    }

}
