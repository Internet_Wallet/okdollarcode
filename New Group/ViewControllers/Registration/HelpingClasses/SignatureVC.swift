//
//  SignatureVC.swift
//  DemoUpdateProfile
//
//  Created by SHUBH on 9/23/17.
//  Copyright © 2017 SHUBH. All rights reserved.
//

import UIKit

protocol SignatureVCDelegate : class {
    func setSignatureImage(image : UIImage)
}

class SignatureVC: OKBaseController,SignatureViewDelegate{
  weak var delegate : SignatureVCDelegate?
    
    @IBOutlet var signatureView: SignatureView!
    @IBOutlet weak var lblDay: UILabel!{
        didSet{
            lblDay.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblTime: UILabel!{
        didSet{
            lblTime.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var lbSignature: UILabel!{
        didSet{
            lbSignature.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnReset: UIButton!{
        didSet{
            btnReset.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
        @IBOutlet weak var btnSubmit: UIButton!{
            didSet{
                btnSubmit.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            }
        }
    @IBOutlet weak var btnCancel: UIButton!{
        didSet{
            btnCancel.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppDelegate.AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)
        lbSignature.text = "Please use your index finger to draw signature".localized
        btnCancel.setTitle("Cancel".localized, for: .normal)
        btnReset.setTitle("Reset".localized, for: .normal)
        btnSubmit.setTitle("Submit_R".localized, for: .normal)
        signatureView.delegate = self
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateFormat = "hh:mm:ss a"
        lblTime.text = formatter.string(from: date)
        formatter.dateFormat = "EEE,dd-MMM-yyyy"
         lblDay.text = formatter.string(from: date)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    @IBAction func btnCaptureSignatureAction(_ sender: Any) {
        signatureView.captureSignature()
    }

    @IBAction func btnCancel(_ sender: Any) {
       AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        self.dismiss(animated: false, completion:nil)
    }
    
    @IBAction func btnResetSignatureAction(_ sender: UIButton) {
             signatureView.clearCanvas()
    }

    func SignatureViewDidCaptureSignature(view: SignatureView, signature: Signature?) {
        if signature != nil {
            if let del = self.delegate{
                del.setSignatureImage(image: (signature?.image)!)
            }
          //  self.view.removeFromSuperview()
            AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
            self.dismiss(animated: false, completion:nil)
        } else {
            if signatureView.signaturePresent == false {
                println_debug("Signature is blank")
            } else {
                println_debug("Failed to Capture Signature")
            }
        showAlert(alertTitle: "", alertBody: "Please Draw Signature".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
        }
    }

    func SignatureViewDidBeginDrawing(view: SignatureView) {
      //  println_debug("Began drawing Signature")
    }

    func SignatureViewIsDrawing(view: SignatureView) {
        //println_debug("Is drawing Signature")
    }

    func SignatureViewDidFinishDrawing(view: SignatureView) {
      //  println_debug("Did finish drawing Signature")
    }

    func SignatureViewDidCancelDrawing(view: SignatureView) {
       // println_debug("Did cancel drawing signature")
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
}
