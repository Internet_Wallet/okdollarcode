//
//  BusinessSubCategoryVC.swift
//  DemoUpdateProfile
//
//  Created by SHUBH on 10/11/17.
//  Copyright © 2017 SHUBH. All rights reserved.
//

import UIKit


protocol BusinessSubCategoryVCDelegate{
    
    func backFromBusinessSubCategoryVCWithNameImage(dicSubCate : SubCategoryDetail)
}

class BusinessSubCategoryVC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var lblNoRecordFound: UILabel!{
        didSet{
            lblNoRecordFound.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var tableview: UITableView!
    var businessSubCategoryVCDelegate: BusinessSubCategoryVCDelegate?
    var subCategory = [SubCategoryDetail]()
    var dicName : CategoryDetail?
    var array = [SubCategoryDetail]()
    var isSearch = false
    var tfSearch = UITextField()
    var CHARSET = ""
    @IBOutlet weak var btnSearch: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.tableFooterView = UIView.init(frame: CGRect.zero)
        self.setMarqueLabelInNavigation()
        
        if ok_default_language == "my"{
            CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ "
            subCategory = (dicName?.subCategoryList.sorted(by: { $0.subCategoryBurmeseName.lowercased() < $1.subCategoryBurmeseName.lowercased()} ))!
        }else if ok_default_language == "en"{
            CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789 "
           subCategory = (dicName?.subCategoryList.sorted(by: { $0.subCategoryName.lowercased() < $1.subCategoryName.lowercased()} ))!
        }else {
            CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ "
            subCategory = (dicName?.subCategoryList.sorted(by: { $0.subCategoryBurmeseUName.lowercased() < $1.subCategoryBurmeseUName.lowercased()} ))!
        }
        tableview.delegate = self
        tableview.dataSource = self
        array = subCategory
        tableview.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
          isSearch = false
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        if btnSearch.tag == 1 {
           
            self.navigationItem.title = ""
            let tfSearch = UITextField()
            tfSearch.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
            tfSearch.autocorrectionType = .no
            self.navigationItem.titleView = tfSearch
            tfSearch.backgroundColor = UIColor.white
            tfSearch.delegate = self
            tfSearch.layer.cornerRadius = 5.0
            tfSearch.textColor = UIColor.black
            tfSearch.font = UIFont(name: appFont, size: 17)
            btnSearch.setTitleColor(UIColor.white, for: .normal)
            tfSearch.placeholder = "Search".localized
            tfSearch.tintColor = UIColor.init(red: 0/255, green: 144/255, blue: 81/255, alpha: 1)
            tfSearch.delegate = self
            tfSearch.clearButtonMode = .whileEditing
            
            let tfserarcView = UIView()
            tfserarcView.frame = CGRect(x: 10, y: 0, width: 30, height: 30)
            let tfserarchIcon = UIImageView()
            tfserarchIcon.frame = CGRect(x: 8, y: 8, width: 14, height: 14)
            tfserarcView.addSubview(tfserarchIcon)
            tfserarchIcon.image = #imageLiteral(resourceName: "search")
            tfSearch.leftView = tfserarcView
            tfSearch.leftViewMode = UITextField.ViewMode.always
            
            btnSearch.setImage(nil, for: .normal)
            btnSearch.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
            btnSearch.setTitle("Cancel".localized, for: .normal)
            tfSearch.becomeFirstResponder()
            btnSearch.tag = 2
        } else if btnSearch.tag == 2 {
            btnSearch.tag = 1
            self.navigationController?.view.endEditing(false)
            isSearch = false
            tfSearch.text = ""
            self.setMarqueLabelInNavigation()
            tableview.reloadData()
        } else if btnSearch.tag == 3 {
            btnSearch.tag = 2
            self.navigationController?.view.endEditing(true)
            view.endEditing(true)
            tfSearch.text = ""
        }
    }
    
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: 18)
        lblMarque.text = "Select Business Sub-Category".localized
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
        lblNoRecordFound.isHidden = true
        btnSearch.setTitle("", for: .normal)
        btnSearch.setImage(#imageLiteral(resourceName: "r_search"), for: .normal)
        lblNoRecordFound.text = "No Record Found".localized
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        tfSearch.resignFirstResponder()
        return true
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        lblNoRecordFound.isHidden = true
        tfSearch.text = ""
        btnSearch.tag = 2
        isSearch = false
        tableview.reloadData()
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        if range.location == 0 && string == " " {
            return false
        }
        let cs = NSCharacterSet(charactersIn: CHARSET).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        if !(string == filtered) {
            return false
        }
        
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            if updatedText == "" {
                isSearch = false
                 lblNoRecordFound.isHidden = true
                tableview.reloadData()
            }
            else{
                getSearchArrayContains(updatedText)
            }
        }
        return restrictMultipleSpaces(str: string, textField: textField)
    }
    
    func getSearchArrayContains(_ text : String) {
        if ok_default_language == "my" {
              array = self.subCategory.filter({$0.subCategoryBurmeseName.contains(find: text)})
        }else if ok_default_language == "en"{
              array = self.subCategory.filter({$0.subCategoryName.contains(find: text)})
        }else {
            array = self.subCategory.filter({$0.subCategoryBurmeseUName.contains(find: text)})
        }
    
        isSearch = true
        tableview.reloadData()
        
        if array.count == 0 {
            lblNoRecordFound.isHidden = false
            self.btnSearch.tag = 2
            self.btnSearch.tintColor = UIColor.lightGray
        }else {
            lblNoRecordFound.isHidden = true
        }
    }
}

extension BusinessSubCategoryVC : UITableViewDelegate,UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearch {
            return array.count
        }else{
           return subCategory.count
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessSubCategoryCell", for: indexPath) as? BusinessSubCategoryCell
        if isSearch {
            let dic = array[indexPath.row]
            if ok_default_language == "my"{
                cell?.setBusinessCategoryNameAndImage(businessCategoryName: dic.subCategoryBurmeseName, imageName:dic.subCategoryEmoji)
            }else if ok_default_language == "en"{
                cell?.setBusinessCategoryNameAndImage(businessCategoryName: dic.subCategoryName, imageName:dic.subCategoryEmoji)
            }else{
                cell?.setBusinessCategoryNameAndImage(businessCategoryName: dic.subCategoryBurmeseUName, imageName:dic.subCategoryEmoji)
            }
        }else{
            let dic = subCategory[indexPath.row]
            if ok_default_language == "my"{
                cell?.setBusinessCategoryNameAndImage(businessCategoryName: dic.subCategoryBurmeseName, imageName:dic.subCategoryEmoji)
            }else if ok_default_language == "en" {
                cell?.setBusinessCategoryNameAndImage(businessCategoryName: dic.subCategoryName, imageName:dic.subCategoryEmoji)
            }else{
                cell?.setBusinessCategoryNameAndImage(businessCategoryName: dic.subCategoryBurmeseUName, imageName:dic.subCategoryEmoji)
            }
        }
        return cell!
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
     
        
        if isSearch {
             let dic = array[indexPath.row]
            self.navigationController?.popViewController(animated: false)
            btnSearch.tag = 1
            guard (self.businessSubCategoryVCDelegate?.backFromBusinessSubCategoryVCWithNameImage(dicSubCate : dic) != nil) else {
                return
            }
        }else {
              let dic = subCategory[indexPath.row]
            self.navigationController?.popViewController(animated: false)
            btnSearch.tag = 1
            guard (self.businessSubCategoryVCDelegate?.backFromBusinessSubCategoryVCWithNameImage(dicSubCate : dic) != nil) else {
                return
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

class BusinessSubCategoryCell: UITableViewCell {
    
    @IBOutlet var lblBusinessCategoryName: MarqueeLabel!{
        didSet{
            lblBusinessCategoryName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblBusinessCategoryImage: UILabel!{
        didSet{
            lblBusinessCategoryImage.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setBusinessCategoryNameAndImage(businessCategoryName : String ,imageName: String) {
        self.lblBusinessCategoryName.text = businessCategoryName
        self.lblBusinessCategoryImage.text = imageName
    }
    
}

