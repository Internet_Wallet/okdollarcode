//
//  ZoomImageVC.swift
//  OK
//
//  Created by Subhash Arya on 02/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ZoomImageVC: UIViewController,UIScrollViewDelegate {
    @IBOutlet weak var imageView : UIImageView!
    @IBOutlet weak var lblHeader : UILabel!
    @IBOutlet weak var scrollview: UIScrollView!
    var imageRotated = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollview.delegate = self
        scrollview.minimumZoomScale = 1.0
        scrollview.maximumZoomScale = 10.0//maximum zoom scale you want
        scrollview.zoomScale = 1.0
        scrollview.showsVerticalScrollIndicator = false
        scrollview.showsHorizontalScrollIndicator = false
        lblHeader.text = "Attached File".localized
        imageView.contentMode = .scaleAspectFit
    }
    
    //zoom in/out image for attach file
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    
 
    @IBAction func dismissview(_ sender : UIButton) {
      self.view.removeFromSuperview()
    }
    @IBAction func dismiss(_ sender: Any){
        self.view.removeFromSuperview()
    }
    
    @IBAction func onClickRotate(_ sender : UIButton){
        var portraitImage  = UIImage()
        if imageRotated == 0 {
            portraitImage = UIImage(cgImage: (imageView.image?.cgImage!)! ,
                                    scale: 1.0 ,
                                    orientation: UIImage.Orientation.right)
            imageRotated = 90
        } else if imageRotated == 90 {
            portraitImage = UIImage(cgImage: (imageView.image?.cgImage!)! ,
                                    scale: 1.0 ,
                                    orientation: UIImage.Orientation.down)
            imageRotated = 180
        } else if imageRotated == 180 {
            portraitImage = UIImage(cgImage: (imageView.image?.cgImage!)! ,
                                    scale: 1.0 ,
                                    orientation: UIImage.Orientation.left)
            imageRotated = 270
        } else {
            portraitImage = UIImage(cgImage: (imageView.image?.cgImage!)! ,
                                    scale: 1.0 ,
                                    orientation: UIImage.Orientation.up)
            imageRotated = 0
        }
        imageView.image = portraitImage
    }

    func setImageToZoom(myImage : UIImage) {
             self.imageView.image = myImage
    }
    
}
