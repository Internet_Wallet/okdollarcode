//
//  TakeSelfiVUP.swift
//  OK
//
//  Created by SHUBH on 11/23/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

protocol TakeSelfiVUPDelegate{
    func ShowIDTypeVC(controller: TakeSelfiVUP)
  //  func removeSecurityView(removeViews : String)
}

class TakeSelfiVUP: OKBaseController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate {
    
    var delegate : TakeSelfiVUPDelegate?
    
    @IBOutlet weak var imgCountry: UIImageView!
    @IBOutlet weak var lblMobilNumber: UILabel!
    
    @IBOutlet weak var viewUserName: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var tfUserName: UITextField!
    @IBOutlet weak var viewMaleFemale: UIView!
    @IBOutlet weak var viewMalePrefix: UIView!
    @IBOutlet weak var viewFemalePrifix: UIView!
    @IBOutlet weak var btnClearUserName: UIButton!
  
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnU: UIButton!
    @IBOutlet weak var btnMg: UIButton!
    @IBOutlet weak var btnMr: UIButton!
    @IBOutlet weak var btnDr: UIButton!
    @IBOutlet weak var btnDaw: UIButton!
    @IBOutlet weak var btnMa: UIButton!
    @IBOutlet weak var btnMs: UIButton!
    @IBOutlet weak var btnMrs: UIButton!
    @IBOutlet weak var btnMDr: UIButton!
    
    @IBOutlet weak var btnBFirst: UIButton!
    @IBOutlet weak var btnBSecond: UIButton!
    @IBOutlet weak var btnBThired: UIButton!
    @IBOutlet weak var viewBMaleFemaleCategory: UIView!
     @IBOutlet weak var btnReset: UIButton!

    var isYourNameKeyboardHidden = false
    var namePrefix = ""
    var countryCode = ""
    var mobilNO = ""
    var isNextViewShow = false
    
  //  var hideViewOnEdit = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.dHeight = 154.0
        tfUserName.delegate = self
        IQKeyboardManager.sharedManager().enable = false

        var font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
            font = UIFont.systemFont(ofSize: 18)
        }
        tfUserName.font = font
        
        if ok_default_language == "en" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue"), for: .normal)
            tfUserName.font = UIFont.systemFont(ofSize: 18)
        }else if ok_default_language == "my" {
           btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
           tfUserName.font = UIFont(name: appFont, size: 18)
        }else {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)            
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfUserName.attributedPlaceholder = NSAttributedString(string: "Enter Your Name", attributes:attributes as [NSAttributedString.Key : Any])
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfUserName.attributedPlaceholder = NSAttributedString(string: "Enter Your Name", attributes:attributes)
                tfUserName.font = UIFont.systemFont(ofSize: 18)
            }
        }
        
        if let operatorName = preNetInfo?.operatorName {
            VerifyUPModel.share.NetworkOperatorName = operatorName
        }
        if let nName = preNetInfo?.networkName {
            VerifyUPModel.share.NetworkOperator = nName
        }
        
        VerifyUPModel.share.DrowCurrentPatternTag = "verify"
  
        if !(VerifyUPModel.share.MobileNumber == "") {
            let (mobileNumber,CountryCode) = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: VerifyUPModel.share.MobileNumber)
            lblMobilNumber.text = "(" + CountryCode + ") " + mobileNumber
            let countryData = identifyCountry(withPhoneNumber:CountryCode)
            if let safeImage = UIImage(named: countryData.countryFlag) {
                DispatchQueue.main.async {
                    self.imgCountry.image = safeImage
                }
            }
        }
        viewBMaleFemaleCategory.isHidden = true
        viewMalePrefix.isHidden = true
        viewFemalePrifix.isHidden = true
       self.tfUserName.placeholder = "Enter Your Name".localized
        self.btnMale.setTitle("Male".localized, for: .normal)
        self.btnFemale.setTitle("Female".localized, for: .normal)
        self.btnU.setTitle("U".localized, for: .normal)
        self.btnMg.setTitle("Mg".localized, for: .normal)
        self.btnMr.setTitle("Mr".localized, for: .normal)
        self.btnDr.setTitle("Dr".localized, for: .normal)
        self.btnDaw.setTitle("Daw".localized, for: .normal)
        self.btnMa.setTitle("Ma".localized, for: .normal)
        self.btnMs.setTitle("Ms".localized, for: .normal)
        self.btnMrs.setTitle("Mrs".localized, for: .normal)
        self.btnMDr.setTitle("Dr".localized, for: .normal)
        self.btnBThired.setTitle("Dr".localized, for: .normal)
    }
        
    @IBAction func btnSelectGenderAction(_ sender: UIButton) {
        if sender.title(for: .normal) == "Male".localized{
            if ok_default_language == "my" || ok_default_language == "uni"{
                viewBMaleFemaleCategory.isHidden = false
                btnBFirst.setTitle("U".localized, for: .normal)
                btnBSecond.setTitle("Mg".localized, for: .normal)
            }else {
                viewMalePrefix.isHidden = false
            }
            imgUser.image = UIImage(named: "r_user")
        }else {
            if ok_default_language == "my" || ok_default_language == "uni"{
                viewBMaleFemaleCategory.isHidden = false
                btnBFirst.setTitle("Daw".localized, for: .normal)
                btnBSecond.setTitle("Ma".localized, for: .normal)
            }else {
                viewFemalePrifix.isHidden = false
            }
            imgUser.image = UIImage(named: "r_female")
        }
        self.viewMaleFemale.isHidden = true
    }
    
    @IBAction func btnMalePrefixAction(_ sender: UIButton) {
        viewMalePrefix.isHidden = true
        setPrefixName(prefixName: sender.title(for: .normal)!, txtField: tfUserName)
    }
    
    @IBAction func btnFeMalePrefixAction(_ sender: UIButton) {
        viewFemalePrifix.isHidden = true
        setPrefixName(prefixName: sender.title(for: .normal)!, txtField: tfUserName)
    }
    
    @IBAction func btnBurmeseUserNameInitialAction(sender : UIButton) {
        setPrefixName(prefixName: sender.title(for: .normal)!, txtField: tfUserName)
    }
    
    func setPrefixName(prefixName : String, txtField : UITextField) {
        namePrefix = prefixName
        let myString = prefixName
        let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: "Zawgyi-One", size: 18) ?? UIFont.systemFont(ofSize: 18.0) ])
        let lblName = UILabel()
        lblName.frame = CGRect(x: 12, y: 2.5, width:stringSize.width + 10 , height: 52)
        lblName.textAlignment = .left
        lblName.font =  UIFont(name: "Zawgyi-One", size: 18)
        lblName.text = myString
        let btnView = UIView()
        btnView.frame = CGRect(x: 0, y: 0, width: lblName.frame.width + 10 , height: 52)
        btnView.addSubview(lblName)
        txtField.leftView = btnView
        txtField.leftViewMode = UITextField.ViewMode.always
        isYourNameKeyboardHidden = true
        viewMalePrefix.isHidden = true
        viewFemalePrifix.isHidden = true
        viewBMaleFemaleCategory.isHidden = true
        txtField.becomeFirstResponder()
    }
    
    @IBAction func btnClearUserName(_ sender: Any) {
        VerifyUPModel.share.Name = ""
        isYourNameKeyboardHidden = false
        viewMaleFemale.isHidden = true
        viewFemalePrifix.isHidden = true
        viewMalePrefix.isHidden = true
        viewBMaleFemaleCategory.isHidden = true
        tfUserName.text = ""
        tfUserName.leftView = nil
        tfUserName.resignFirstResponder()
        imgUser.image = UIImage(named: "r_user")
        isNextViewShow = false
    }
    
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == tfUserName {
            var emptyFields = ""
            
            if VerifyUPModel.share.ProfilePicAwsUrl == "" {
                emptyFields = emptyFields + " " + "Profile Image".localized
                DispatchQueue.main.async {
                alertViewObj.wrapAlert(title: "Please Fill All Fields".localized, body:emptyFields , img:#imageLiteral(resourceName: "alert-icon"))
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                })
                 alertViewObj.showAlert(controller: self)
                }
                
            }
            else{
                if isYourNameKeyboardHidden {
                    viewMaleFemale.isHidden = true
                    return true
                }else{
                    viewMaleFemale.isHidden = false
                    return false
                }
            }
        }
        return true
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
//        var emptyFields = ""
//        if VerifyUPModel.share.Name == "" {
//            emptyFields = "User Name".localized
//            DispatchQueue.main.async {
//            alertViewObj.wrapAlert(title: "Please Fill All Fields".localized, body:emptyFields , img:#imageLiteral(resourceName: "alert-icon"))
//            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//            })
//             alertViewObj.showAlert(controller: self)
//            }
//
//        }else {
            if tfUserName.text!.count > 0 {
                VerifyUPModel.share.Name = namePrefix + "," + tfUserName.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            }else {
                VerifyUPModel.share.Name = ""
            }
      //  }
        
        
        
       
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var CHARSET = ""
        if ok_default_language == "my"{
            CHARSET = NAME_CHAR_SET_My
        }else if ok_default_language == "en" {
            CHARSET = NAME_CHAR_SET_En
        }else {
           CHARSET = NAME_CHAR_SET_Uni
        }
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        
        if text.count > 2 {
            if !isNextViewShow {
                tfUserName.becomeFirstResponder()
                guard(self.delegate?.ShowIDTypeVC(controller: self) != nil)else{
                    return false
                }

                isNextViewShow = true
            }
        }
        if (range.location == 0 && string == " ") {
                   return false
        }
        
        
        if text.count > 0 {
                 //btnClearUserName.isHidden = false
        }else{
               //  btnClearUserName.isHidden = true
        }
        
        if text.count > 50 { return false }
        if restrictMultipleSpaces(str: string, textField: tfUserName) {
            return true
        }else {
            if  textField.text?.last == " " {
                textField.text?.removeLast()
            }
            return false
        }
    }

}

