//
//  IDTypeVUP.swift
//  OK
//
//  Created by SHUBH on 11/23/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

protocol IDTypeVUPelegate {
    func updateMainUIUI()
    func showPasswordSecurity()
    func showTownShip()
    func resetIDProofDetails()
    func showCountry()
    func removeSecurityView(removeViews : String)
    func showNRCTypeOption()
}

protocol MyTextFieldDelegate {
    func textFieldDidDelete()
}

class IDTypeVUP: OKBaseController,TownShipVcDeletage,CountryViewControllerDelegate,NRCTypesVCDelegate,UITextFieldDelegate,TownshipVC2Delegate,MyTextFieldDelegate{
    func textFieldDidDelete() {
        println_debug("Delete")
        if tfNRCNo.text?.count == 0 {
           self.showNRCSuggestion()
        }
    }
    
    @IBOutlet weak var viewNRC: UIView!
    @IBOutlet weak var viewNRCButton: UIView!
    @IBOutlet weak var viewNRCTF: UIView!
    @IBOutlet weak var tfNRCNo: MyTextField!
    @IBOutlet weak var btnNRCNo: UIButton!
    
    @IBOutlet weak var viewSelectNRCType: UIView!
    @IBOutlet weak var viewPassport: UIView!
    @IBOutlet weak var viewCountry: UIView!
    @IBOutlet weak var viewPassportNo: UIView!
    @IBOutlet weak var viewPassportExpiry: UIView!
    
    @IBOutlet weak var imgCountry: UIImageView!
    @IBOutlet weak var btnContry: UIButton!
    
    @IBOutlet weak var tfPassportNo: UITextField!
    @IBOutlet weak var tfPasspotExpiryDate: UITextField!
    
    @IBOutlet weak var viewDOB: UIView!
    @IBOutlet weak var viewDOB_FN: UIView!
    
    @IBOutlet weak var tfDOB: UITextField!
    @IBOutlet weak var tfFatherName: UITextField!
    @IBOutlet weak var view_FN: UIView!
    
    @IBOutlet weak var viewFatherPrefix: UIView!
    @IBOutlet weak var topConstraintsDOBFN: NSLayoutConstraint!
    
    @IBOutlet weak var btnFU: UIButton!
    @IBOutlet weak var btnFMr: UIButton!
    @IBOutlet weak var btnFDr: UIButton!
    @IBOutlet weak var viewBFatherInitial: UIView!
    @IBOutlet weak var btnBFatherFirst: UIButton!
    @IBOutlet weak var btnBFatherSecond: UIButton!
    @IBOutlet weak var lblHeader: MarqueeLabel!
    @IBOutlet weak var btnEnterNRC: UIButton!
    @IBOutlet weak var btnOR: UIButton!
    @IBOutlet weak var btnEnterPassport: UIButton!
    @IBOutlet weak var lblCountryCityzen: MarqueeLabel!
    @IBOutlet weak var lblPassportDetails: MarqueeLabel!
    @IBOutlet weak var lblNRCDetails: MarqueeLabel!
         @IBOutlet weak var btnReset: UIButton!
    var delegate : IDTypeVUPelegate?
    
    var isFatherNameKeyboardHidden = false
    
    @IBOutlet weak var fathercloseButton: UIButton!
    
    let btnnrc = UIButton()
    var strDOB = ""
    var fatherPrefix = ""

    var strExpiryDate = ""
    
    var isNextViewHidden = true
        var locationDetails: LocationDetail?
    let dateFormatter = DateFormatter()
    var NRCSET = ""
    var PASSWORDSET = ""
    var NAMESET = ""
    var fnt = UIFont.systemFont(ofSize: 18)
    @IBOutlet weak var btnDOBClose: UIButton!
    override func viewDidLoad() {
        btnnrc.titleLabel?.font = UIFont(name: appFont, size: 18)
        
        var font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
            font = UIFont.systemFont(ofSize: 18)
        }
        tfFatherName.font = font
        tfPassportNo.font = font
        tfPasspotExpiryDate.font = font
        tfDOB.font = font
        
        if ok_default_language == "en" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue"), for: .normal)
            NAMESET = NAME_CHAR_SET_En
            tfFatherName.font = UIFont.systemFont(ofSize: 18)
        }else if ok_default_language == "my" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
            NAMESET = NAME_CHAR_SET_My
            tfFatherName.font = UIFont(name: appFont, size: 18)
            fnt = UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18.0)
        }else {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
            NAMESET = NAME_CHAR_SET_Uni
            
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfFatherName.attributedPlaceholder = NSAttributedString(string: "Enter Father Name", attributes:attributes as [NSAttributedString.Key : Any])
                
                let attributes1 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfPassportNo.attributedPlaceholder = NSAttributedString(string: "Enter Number", attributes:attributes1 as [NSAttributedString.Key : Any])
                
                let attributes2 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfPasspotExpiryDate.attributedPlaceholder = NSAttributedString(string: "Enter Passport Expiry Date", attributes:attributes2 as [NSAttributedString.Key : Any])
                
                let attributes3 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfDOB.attributedPlaceholder = NSAttributedString(string: "Select Date of Birth", attributes:attributes3 as [NSAttributedString.Key : Any])
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfFatherName.attributedPlaceholder = NSAttributedString(string: "Enter Father Name", attributes:attributes)
                tfFatherName.font = UIFont.systemFont(ofSize: 18)
                
                let attributes1 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfPassportNo.attributedPlaceholder = NSAttributedString(string: "Enter Number", attributes:attributes1)
                tfPassportNo.font = UIFont.systemFont(ofSize: 18)
                
                let attributes2 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfPasspotExpiryDate.attributedPlaceholder = NSAttributedString(string: "Enter Passport Expiry Date", attributes:attributes2)
                tfPasspotExpiryDate.font = UIFont.systemFont(ofSize: 18)
                
                let attributes3 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfDOB.attributedPlaceholder = NSAttributedString(string: "Select Date of Birth", attributes:attributes3)
                tfDOB.font = UIFont.systemFont(ofSize: 18)
            }
            
        }
        
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        IQKeyboardManager.sharedManager().enable = false
        super.viewDidLoad()
        viewFatherPrefix.isHidden = true
        viewBFatherInitial.isHidden = true
        tfNRCNo.delegate = self
        tfNRCNo.myDelegate = self
        tfPassportNo.delegate = self
        tfDOB.delegate = self
        tfFatherName.delegate = self
        tfPasspotExpiryDate.delegate = self
        self.dHeight = 184.0
        
        self.tfNRCNo.addTarget(self, action: #selector(IDTypeVUP.textFieldDidChange(_:)),
                               for: UIControl.Event.editingChanged)
        self.tfNRCNo.myDelegate = self
        
        self.btnFU.setTitle("U".localized, for: .normal)
        self.btnFMr.setTitle("Mr".localized, for: .normal)
        self.btnFDr.setTitle("Dr".localized, for: .normal)
        self.btnBFatherFirst.setTitle("U".localized, for: .normal)
        self.btnBFatherSecond.setTitle("Dr".localized, for: .normal)
        
        self.lblHeader.text = "Select ID Type".localized
        lblNRCDetails.text = "Enter NRC Details".localized
        self.btnOR.setTitle("(OR)".localized, for: .normal)
        lblPassportDetails.text = "Enter Passport Details".localized
        btnNRCNo.setTitle("Enter NRC Number".localized, for: .normal)
        lblCountryCityzen.text = "Select Country of Citizenship".localized
        tfPassportNo.placeholder = "Enter Number".localized
        tfPasspotExpiryDate.placeholder = "Enter Passport Expiry Date".localized
        self.tfDOB.placeholder = "Select Date of Birth".localized
        self.tfFatherName.placeholder = "Enter Father Name".localized
        
        fathercloseButton.isHidden = true
   
        NRCSET = "0123456789"
        PASSWORDSET = PASSPORT_CHAR_SET
    }
    
    @IBAction func btnSelectPassportAction(_ sender: Any) {
//        var emptyFields = ""
//        if VerifyUPModel.share.Name == "" {
//            emptyFields = "User Name".localized
//            DispatchQueue.main.async {
//            alertViewObj.wrapAlert(title: "Please Fill All Fields".localized, body:emptyFields , img:#imageLiteral(resourceName: "alert-icon"))
//            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//            })
//             alertViewObj.showAlert(controller: self)
//            }
//
//        }else {
//            let token = VerifyUPModel.share.Name.components(separatedBy: ",")
//            if token[1].count < 3 {
//                emptyFields = emptyFields + "\n" + "User Name must be more than 2 characters".localized
//                DispatchQueue.main.async {
//                alertViewObj.wrapAlert(title: "Please Fill All Fields".localized, body:emptyFields , img:#imageLiteral(resourceName: "alert-icon"))
//                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//                })
//                 alertViewObj.showAlert(controller: self)
//                }
//            }
         //   else{
                self.dHeight = viewCountry.frame.height + viewCountry.frame.origin.y + viewPassport.frame.origin.y
                runGuard()
                viewSelectNRCType.isHidden = true
                viewPassport.isHidden = false
                VerifyUPModel.share.IdType = "04"
          //  }
       // }
    }
    
    @IBAction func fathercloseButton(_ sender: Any) {
        
        VerifyUPModel.share.FatherName = ""
        fatherPrefix = ""
        tfFatherName.text = ""
        fathercloseButton.isHidden = true
        tfFatherName.leftView = nil
        isFatherNameKeyboardHidden = false
       // viewFatherPrefix.isHidden = false
        tfFatherName.resignFirstResponder()
    }
    
    @IBAction func btnSelectNRCDetailsAction(_ sender: Any) {
//        var emptyFields = ""
//        if VerifyUPModel.share.Name == "" {
//            emptyFields = "User Name".localized
//            DispatchQueue.main.async {
//            alertViewObj.wrapAlert(title: "Please Fill All Fields".localized, body:emptyFields , img:#imageLiteral(resourceName: "alert-icon"))
//            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//            })
//             alertViewObj.showAlert(controller: self)
//            }
//
//        }
//        else{
//            let token = VerifyUPModel.share.Name.components(separatedBy: ",")
//            if token[1].count < 3 {
//                emptyFields = emptyFields + "\n" + "User Name must be more than 2 characters".localized
//                DispatchQueue.main.async {
//                alertViewObj.wrapAlert(title: "Please Fill All Fields".localized, body:emptyFields , img:#imageLiteral(resourceName: "alert-icon"))
//                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//                })
//                 alertViewObj.showAlert(controller: self)
//                }
//            }
         //   else{
                guard (self.delegate?.showTownShip() != nil) else {
                    return
                }
         //   }
            
       // }
    }
    
    @IBAction func btnNRCNoAction(_ sender: UIButton) {
        let sb = UIStoryboard(name: "Registration", bundle: nil)
        let townshipVC2 = sb.instantiateViewController(withIdentifier: "TownshipVC2") as! TownshipVC2
        townshipVC2.selectedDivState = locationDetails
        townshipVC2.townshipVC2Delegate = self
        self.navigationController?.pushViewController(townshipVC2, animated: true)
    }
    
    @IBAction func btnSelectCountryAction(_ sender: Any) {
        guard (self.delegate?.showCountry() != nil) else {
            return
        }
        tfPassportNo.text = ""
    }
    
    @IBAction func btnSelectFatherPrefixAction(_ sender: UIButton) {
        viewFatherPrefix.isHidden = true
        self.setPrefixName(prefixName: sender.title(for: .normal)!, txtField: tfFatherName)
    }
    
    @IBAction func btnBFatherInitialAction(_ sender: UIButton) {
        viewBFatherInitial.isHidden = true
        self.setPrefixName(prefixName: sender.title(for: .normal)!, txtField: tfFatherName)
    }

    func setPrefixName(prefixName : String, txtField : UITextField) {
        isFatherNameKeyboardHidden = true
        fathercloseButton.isHidden = false
        fatherPrefix = prefixName
        let myString = prefixName
        let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: "Zawgyi-One", size: 18) ?? UIFont.systemFont(ofSize: 18.0) ])
        let lblName = UILabel()
        lblName.frame = CGRect(x: 2, y: 2.5, width:stringSize.width + 8 , height: 52)
        lblName.textAlignment = .left
        lblName.font =  UIFont(name: "Zawgyi-One", size: 18)
        lblName.text = myString
        let btnView = UIView()
        btnView.frame = CGRect(x: 0, y: 0, width: lblName.frame.width + 2 , height: 52)
        btnView.addSubview(lblName)
        txtField.leftView = btnView
        txtField.leftViewMode = UITextField.ViewMode.always
        txtField.becomeFirstResponder()
    }
    
    
    
    // Division Details
    func setDivison(division : LocationDetail) {
        locationDetails = division
        if ok_default_language == "my" {
            let divStr = division.nrcCodeNumberMy + "/"
            btnNRCNo.setTitle(divStr, for: .normal)
        }else if ok_default_language == "en"{
            let divStr = division.nrcCodeNumber + "/"
            btnNRCNo.setTitle(divStr, for: .normal)
        }else {
            let divStr = division.nrcCodeNumberUni + "/"
            btnNRCNo.setTitle(divStr, for: .normal)
        }
        
        VerifyUPModel.share.IdType = "01"
        self.dHeight = viewNRCTF.frame.height + viewNRCTF.frame.origin.y + 40
        runGuard()
        viewSelectNRCType.isHidden = true
        viewNRC.isHidden = false
    }
    
    // Division and township details
    func setDivisionAndTownship(strDivison_township : String){
        self.setNRCDetails(nrcDetails: strDivison_township)
    }
    
    func NRCValueString(nrcSting : String) {
        self.setNRCDetails(nrcDetails: nrcSting)
    }

    private func setNRCDetails(nrcDetails : String) {
        
        if nrcDetails == ""{
            guard(self.delegate?.resetIDProofDetails() != nil) else {
                return
            }
            VerifyUPModel.share.IdType = ""
            VerifyUPModel.share.Nrc = ""
            VerifyUPModel.share.DateOfBirth = ""
            VerifyUPModel.share.FatherName = ""
       
            VerifyUPModel.share.PassportCountry = ""
            VerifyUPModel.share.PassportNumber = ""
            VerifyUPModel.share.PassportExpiryDate = ""
        }else{
            
            self.viewNRCButton.isHidden = true
            self.viewNRCTF.isHidden = false
            
            let myString = nrcDetails
            let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: fnt])
            btnnrc.frame = CGRect(x: -5, y: -1, width:stringSize.width + 12, height: 52)
            btnnrc.setTitleColor(.black, for: .normal)
            btnnrc.addTarget(self, action: #selector(BtnNrcTypeAction), for: .touchUpInside)
            btnnrc.setTitle(nrcDetails, for: UIControl.State.normal)
            let btnView = UIView()
            btnView.frame = CGRect(x: 0, y: 0, width: btnnrc.frame.width , height: 52)
            btnView.addSubview(btnnrc)
            tfNRCNo.leftView = nil
            tfNRCNo.leftView = btnView
            tfNRCNo.leftViewMode = UITextField.ViewMode.always
            tfNRCNo.becomeFirstResponder()
            
            
        }
        
        
        
    }
    
    @objc func BtnNrcTypeAction() {
        self.showNRCSuggestion()
    }
    
    func countryViewController(_ list: CountryViewController, country: Country) {
//        VerifyUPModel.share.DateOfBirth = ""
//        VerifyUPModel.share.PassportExpiryDate = ""
//        isNextViewHidden = true
        lblCountryCityzen.text = country.name
        lblCountryCityzen.textAlignment = .left
       // btnContry.setTitle(country.name,for: .normal)
        btnContry.setTitleColor(UIColor.gray, for: .normal)
        imgCountry.image = UIImage(named: country.code)
        VerifyUPModel.share.PassportCountry = country.dialCode
        viewPassportNo.isHidden = false
        
        if  VerifyUPModel.share.DateOfBirth == "" || VerifyUPModel.share.PassportExpiryDate == "" {
            self.dHeight = viewPassportNo.frame.origin.y + viewPassportNo.frame.height + 40.0
            runGuard()
        }
        list.dismiss(animated: false, completion: nil)
        tfPassportNo.becomeFirstResponder()
    }
    
    func countryViewControllerCloseAction(_ list: CountryViewController) {
        list.dismiss(animated: false, completion: nil)
    }
    func countryDelegate(img: String, txt: String) {
        
    }
    
    func setNrcTypeName(nrcType: String) {
        btnnrc.setTitle(nrcType, for: UIControl.State.normal)
        btnnrc.setTitle(nrcType, for: .normal)
        let myString = nrcType
        var fnt = UIFont.systemFont(ofSize: 18)
        if ok_default_language == "my" {
            fnt = UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18.0)
        }
        
        let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: fnt])
        btnnrc.frame = CGRect(x: 0, y: -2, width:stringSize.width + 4, height: 52)
        btnnrc.setTitle(nrcType, for: .normal)
        let btnView = UIView()
        btnView.frame = CGRect(x: 0, y: 0, width: btnnrc.frame.width , height: 52)
        btnView.addSubview(btnnrc)
        tfNRCNo.leftView = nil
        tfNRCNo.leftView = btnView
        tfNRCNo.leftViewMode = UITextField.ViewMode.always
        tfNRCNo.becomeFirstResponder()
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == tfPassportNo{
            VerifyUPModel.share.PassportNumber = tfPassportNo.text!
            tfPassportNo.resignFirstResponder()
        }else if textField == tfFatherName {
            if tfFatherName.text!.count > 0{
                fathercloseButton.isHidden = false
           
                VerifyUPModel.share.FatherName = fatherPrefix + "," + tfFatherName.text!.trimmingCharacters(in: .whitespacesAndNewlines)
                self.isNextViewHidden = false
                guard(self.delegate?.showPasswordSecurity() != nil)else{
                    return false
                }
            //  self.ValidateReverifyRequestWS()
            }else {
                VerifyUPModel.share.FatherName = ""
                if fatherPrefix == ""{
                    fathercloseButton.isHidden = true
                }else{
                    fathercloseButton.isHidden = false
                }
               
            }
        }else if textField == tfNRCNo {
            if tfNRCNo.text == "000000" {
               // tfNRCNo.text = ""
                tfNRCNo.text?.removeLast()
                tfNRCNo.becomeFirstResponder()
                return false
            }
        }
        return true
    }

    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == tfDOB{
         
//
//                if VerifyUPModel.share.Nrc != "" {
//                    var emptyFields = ""
//                    let token = VerifyUPModel.share.Nrc.components(separatedBy: ")")
//                    if token[1].count < 6 {
//                        emptyFields = emptyFields + "\n" + "NRC number must be 6 characters".localized
//                        DispatchQueue.main.async {
//                            alertViewObj.wrapAlert(title: "Please Fill All Fields".localized, body:emptyFields , img:#imageLiteral(resourceName: "alert-icon"))
//                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//                            })
//                            alertViewObj.showAlert(controller: self)
//                        }
//
//
//                    }else if token[1].count == 0 {
//                        emptyFields = emptyFields + "\n" + "NRC number".localized
//                        DispatchQueue.main.async {
//                            alertViewObj.wrapAlert(title: "Please Fill All Fields".localized, body:emptyFields , img:#imageLiteral(resourceName: "alert-icon"))
//                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//                            })
//                            alertViewObj.showAlert(controller: self)
//                        }
//
//
//                    }
//
//                }
//                else if VerifyUPModel.share.PassportNumber == "" {
//                    var emptyFields = ""
//                    emptyFields = emptyFields + "\n" + "Passport number".localized
//                    DispatchQueue.main.async {
//                        alertViewObj.wrapAlert(title: "Please Fill All Fields".localized, body:emptyFields , img:#imageLiteral(resourceName: "alert-icon"))
//                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//                        })
//                        alertViewObj.showAlert(controller: self)
//                    }
//                }else if VerifyUPModel.share.PassportNumber.count < 5 {
//                    var emptyFields = ""
//                    emptyFields = emptyFields + "\n" + "Passport Number must be more than 4 characters".localized
//                    DispatchQueue.main.async {
//                        alertViewObj.wrapAlert(title: "Please Fill All Fields".localized, body:emptyFields , img:#imageLiteral(resourceName: "alert-icon"))
//                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//                        })
//                        alertViewObj.showAlert(controller: self)
//                    }
//                }
//
//                if VerifyUPModel.share.PassportExpiryDate == "" {
//                    var emptyFields = ""
//                    emptyFields = emptyFields + "\n" + "Passport Expiry Date".localized
//                    DispatchQueue.main.async {
//                        alertViewObj.wrapAlert(title: "Please Fill All Fields".localized, body:emptyFields , img:#imageLiteral(resourceName: "alert-icon"))
//                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//                        })
//                        alertViewObj.showAlert(controller: self)
//                    }
//                }
                
        
            
            let date = Calendar.current.date(byAdding: .year, value: -12, to: Date())
            let datePicker = UIDatePicker()
            datePicker.tag = 1
            datePicker.datePickerMode = .date
            datePicker.backgroundColor = UIColor.white
            datePicker.maximumDate = date!
            datePicker.calendar = Calendar(identifier: .gregorian)
            if #available(iOS 13.4, *) {
                           datePicker.preferredDatePickerStyle = .wheels
                       } else {
                           // Fallback on earlier versions
                       }
            if tfDOB.text?.count == 0 {
                datePicker.date = date!
            }else{
                datePicker.date = dateFormatter.date(from: tfDOB.text!)!
            }
            
            let calendar = Calendar.current
            var minDateComponent = calendar.dateComponents([.day,.month,.year], from: Date())
            minDateComponent.day = 01
            minDateComponent.month = 01
            minDateComponent.year = 1900
            datePicker.minimumDate = calendar.date(from: minDateComponent)
            
            datePicker.addTarget(self, action: #selector(datePickerChanged(sender:)), for: .valueChanged)
            textField.inputView = datePicker
            
            let screenSize: CGRect = UIScreen.main.bounds
            let viewDOBKeyboard = UIView(frame: CGRect(x: 0, y: 0, width:screenSize.width , height: 40))
            viewDOBKeyboard.backgroundColor = UIColor(red: 244/255, green: 199/255, blue: 22/255, alpha: 1)
            
            let btnCancel  = UIButton(frame: CGRect(x: 10, y: 0, width:100, height: 40))
            btnCancel.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
            btnCancel.setTitle("Cancel".localized, for: UIControl.State.normal)
            btnCancel.addTarget(self, action:#selector(btnCancelDOBAction), for: UIControl.Event.touchUpInside)
            viewDOBKeyboard.addSubview(btnCancel)
            
            let btnDone  = UIButton(frame: CGRect(x: screenSize.width - 110, y: 0, width:100, height: 40))
            btnDone.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
            btnDone.setTitle("Done".localized, for: UIControl.State.normal)
            btnDone.addTarget(self, action:#selector(btnDoneDOBAction), for: UIControl.Event.touchUpInside)
            strDOB = dateFormatter.string(from: datePicker.date)
            viewDOBKeyboard.addSubview(btnDone)
            tfDOB.inputAccessoryView = viewDOBKeyboard
            
        }else if textField == tfFatherName{
//            var emptyFields = ""
//            if VerifyUPModel.share.DateOfBirth == "" {
//                emptyFields = emptyFields + "\n" + "Date of Birth".localized
//                DispatchQueue.main.async {
//                    alertViewObj.wrapAlert(title: "Please Fill All Fields".localized, body:emptyFields , img:#imageLiteral(resourceName: "alert-icon"))
//                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//                    })
//                    alertViewObj.showAlert(controller: self)
//                }
//
//            }
          //  else{
            
            
            if fatherPrefix == ""{
                fathercloseButton.isHidden = true
            }else{
                fathercloseButton.isHidden = false
            }
            
                
                if isFatherNameKeyboardHidden {
                    return true
                }else{
                    if ok_default_language  == "my" || ok_default_language  == "uni"{
                        viewBFatherInitial.isHidden = false
                    } else{
                        viewFatherPrefix.isHidden = false
                    }
                    return false
                }
          //  }
        
        }else if textField == tfPasspotExpiryDate{
//               var emptyFields = ""
//                if VerifyUPModel.share.PassportNumber == "" {
//                    emptyFields = emptyFields + "\n" + "Passport number".localized
//                    DispatchQueue.main.async {
//                        alertViewObj.wrapAlert(title: "Please Fill All Fields".localized, body:emptyFields , img:#imageLiteral(resourceName: "alert-icon"))
//                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//                        })
//                        alertViewObj.showAlert(controller: self)
//                    }
//                }
            
        //    else {
            
            let date = Date()
            let datePicker = UIDatePicker()
            datePicker.tag = 2
            datePicker.datePickerMode = .date
            datePicker.backgroundColor = UIColor.white
            datePicker.calendar = Calendar(identifier: .gregorian)
            if #available(iOS 13.4, *) {
                           datePicker.preferredDatePickerStyle = .wheels
                       } else {
                           // Fallback on earlier versions
                       }
            datePicker.minimumDate = Date(timeInterval: (24 * 60 * 60), since: date)
            let maxDate = Calendar.current.date(byAdding: .year, value: 100, to: Date())
            datePicker.maximumDate = maxDate
            
            if tfPasspotExpiryDate.text?.count == 0 {
                datePicker.date = date
            }else{
                datePicker.date = dateFormatter.date(from: tfPasspotExpiryDate.text!)!
            }
            if tfPasspotExpiryDate.text?.count == 0 {
                datePicker.date = Date(timeInterval: (24 * 60 * 60), since: date)
            }else{
                datePicker.date = dateFormatter.date(from: tfPasspotExpiryDate.text!)!
            }
            datePicker.addTarget(self, action: #selector(datePickerChanged(sender:)), for: .valueChanged)
            textField.inputView = datePicker
            
            let screenSize: CGRect = UIScreen.main.bounds
            let viewExpiryDateKeyboard = UIView(frame: CGRect(x: 0, y: 0, width:screenSize.width , height: 40))
            viewExpiryDateKeyboard.backgroundColor = UIColor(red: 244/255, green: 199/255, blue: 22/255, alpha: 1)
            
            let btnCancel  = UIButton(frame: CGRect(x: 10, y: 0, width:100, height: 40))
            btnCancel.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
            btnCancel.setTitle("Cancel".localized, for: UIControl.State.normal)
            btnCancel.addTarget(self, action:#selector(btnCancelPassportExpiryAction), for: UIControl.Event.touchUpInside)
            viewExpiryDateKeyboard.addSubview(btnCancel)
            
            let btnDone  = UIButton(frame: CGRect(x: screenSize.width - 110, y: 0, width:100, height: 40))
            btnDone.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
            btnDone.setTitle("Done".localized, for: UIControl.State.normal)
            btnDone.addTarget(self, action:#selector(btnDonePassportExpiryAction), for: UIControl.Event.touchUpInside)
            strExpiryDate = dateFormatter.string(from: datePicker.date)
            viewExpiryDateKeyboard.addSubview(btnDone)
            tfPasspotExpiryDate.inputAccessoryView = viewExpiryDateKeyboard
                
          //  }
        }
        
        return true
    }
    
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if textField == tfNRCNo {
                if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: NRCSET).inverted).joined(separator: "")) { return false }
            
            if text.count > 6 {
                tfNRCNo.resignFirstResponder()
                return false
            }
            if tfNRCNo.text == "000000" {
                // tfNRC.text = ""
                tfNRCNo.text?.removeLast()
                tfNRCNo.becomeFirstResponder()

                return false
            }
        }else if textField == tfPassportNo {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: PASSWORDSET).inverted).joined(separator: "")) { return false }

            if text.count > 14
            {
                return false
            }
            
           
                if text.count > 4{
                    if isNextViewHidden{
                        if VerifyUPModel.share.DateOfBirth == "" || VerifyUPModel.share.PassportExpiryDate == "" {
                        self.dHeight = viewPassportExpiry.frame.origin.y + viewPassportExpiry.frame.height + 40.0
                        runGuard()
                        tfPasspotExpiryDate.becomeFirstResponder()
                            
                    }
                    }
                    tfPassportNo.becomeFirstResponder()
                }
            
          
        }else if textField == tfFatherName {
              if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: NAMESET).inverted).joined(separator: "")) {
                println_debug("do not Allow")
                return false
              }else {
                println_debug("Allow")
            }
            if text.count > 2 {
                fathercloseButton.isHidden = false
                if isNextViewHidden {
                    self.dHeight = view_FN.frame.origin.y + view_FN.frame.height + viewDOB_FN.frame.origin.y
                    runGuard()
                }
            }
            
            if (range.location == 0 && string == " ") {
                       return false
            }
            
            if text.count == 0 {
                fathercloseButton.isHidden = true
            }
            
            if text.count > 50 { return false }
            if restrictMultipleSpaces(str: string, textField: tfFatherName) {
                return true
            }else {
                if  textField.text?.last == " " {
                    textField.text?.removeLast()
                }
                return false
            }
        }
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
//        if tfNRCNo.text!.count >= 6 {
//            tfNRCNo.resignFirstResponder()
//            topConstraintsDOBFN.constant = -114
//            tfNRCNo.resignFirstResponder()
//            tfDOB.becomeFirstResponder()
//            if self.dHeight! <= viewDOB.frame.height + viewDOB_FN.frame.origin.y {
//              self.dHeight = 154.0
//                runGuard()
//            } //154.0
//        }
        
        if tfNRCNo.text == "000000" {
            tfNRCNo.text?.removeLast()
        }else if tfNRCNo.text!.count == 0 {
           // VerifyUPModel.share.Nrc = ""
        }else if tfNRCNo.text!.count > 6 {
            tfNRCNo.text?.removeLast()
            tfNRCNo.resignFirstResponder()
        }else if tfNRCNo.text!.count == 6 {
            tfNRCNo.resignFirstResponder()
            topConstraintsDOBFN.constant = -114
            tfNRCNo.resignFirstResponder()
            tfDOB.becomeFirstResponder()
            if self.dHeight! <= viewDOB.frame.height + viewDOB_FN.frame.origin.y {
              self.dHeight = 154.0
                runGuard()
            } //154.0
            
               
        }
        
        
        VerifyUPModel.share.Nrc = (btnnrc.titleLabel?.text)! + tfNRCNo.text!
    }
    
    @objc func datePickerChanged(sender: UIDatePicker) {
        sender.calendar = Calendar(identifier: .gregorian)
        if sender.tag == 1 {
            strDOB = dateFormatter.string(from: sender.date)
        }else{
            strExpiryDate = dateFormatter.string(from: sender.date)
        }
    }
    
    @objc func btnCancelDOBAction () {
        tfDOB.resignFirstResponder()
    }
    
    @objc func btnCancelPassportExpiryAction() {
        tfPasspotExpiryDate.resignFirstResponder()
    }
    
    @objc func btnDoneDOBAction () {
        tfDOB.resignFirstResponder()
        tfDOB.text = strDOB
        if strDOB.count > 0 {
            VerifyUPModel.share.DateOfBirth = strDOB
            if isNextViewHidden == false {
                if tfFatherName.text == ""{
                    isFatherNameKeyboardHidden = false
                     isNextViewHidden = true
                }
            }else {
                self.dHeight = view_FN.frame.origin.y + view_FN.frame.height + viewDOB_FN.frame.origin.y
                runGuard()
            }
            

        }
    }
    
    @objc func btnDonePassportExpiryAction() {
        tfPasspotExpiryDate.resignFirstResponder()
        tfPasspotExpiryDate.text = strExpiryDate
        if strExpiryDate.count > 0 {
            tfPasspotExpiryDate.text = strExpiryDate
            topConstraintsDOBFN.constant = 0
            
            if VerifyUPModel.share.DateOfBirth == ""{
                self.dHeight = viewDOB.frame.height + viewDOB_FN.frame.origin.y
                runGuard()
            }
            VerifyUPModel.share.PassportExpiryDate = tfPasspotExpiryDate.text!
        }
    }
    
    @IBAction func btnDOBCloseAction(_ sender: Any) {
        self.tfDOB.text = ""
    }
    
    @IBAction func btnResetAction(_ sender: Any) {
        guard(self.delegate?.resetIDProofDetails() != nil) else {
            return
        }
        VerifyUPModel.share.IdType = ""
        VerifyUPModel.share.Nrc = ""
        VerifyUPModel.share.DateOfBirth = ""
        VerifyUPModel.share.FatherName = ""
   
        VerifyUPModel.share.PassportCountry = ""
        VerifyUPModel.share.PassportNumber = ""
        VerifyUPModel.share.PassportExpiryDate = ""
    }
    
    func runGuard() {
        guard(self.delegate?.updateMainUIUI() != nil) else {
            return
        }
    }
    
    func ValidateReverifyRequestWS() {
        var urlStr   = Url.URL_ValidateReverifyForgotPassword + "MobileNumber=" + VerifyUPModel.share.MobileNumber + "&Name=" + VerifyUPModel.share.Name + "&FatherName=" + VerifyUPModel.share.FatherName  + "&Dob=" + VerifyUPModel.share.DateOfBirth
        
        urlStr = urlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let urlS = getUrl(urlStr: urlStr, serverType: .serverApp)
        println_debug(urlS)
        let param = [String:String]() as AnyObject
        
        TopupWeb.genericClass(url: urlS, param: param, httpMethod: "GET", handle: { (response , success) in
            if success {
                println_debug(response)
                let dic = response as? Dictionary<String , Any>
                println_debug(dic!)
                if dic!["Code"] as! NSInteger == 200 {
                    if self.isNextViewHidden{
                        self.isNextViewHidden = false
                        DispatchQueue.main.async {
                            guard(self.delegate?.showPasswordSecurity() != nil)else{
                                return
                            }
                        }
                    }
                }else {
                    self.showAlert(alertTitle: "", alertBody: "You Entered Incorrect Details".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
                }
            }else {
                self.showAlert(alertTitle: "", alertBody: "Network error Please try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
                println_debug(response)
            }
        })
    }
    
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK", style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
   private func showNRCSuggestion(){
    self.tfNRCNo.resignFirstResponder()
    if let del = delegate {
        del.showNRCTypeOption()
    }
    }

}

class MyTextField: UITextField {
    var myDelegate: MyTextFieldDelegate?
    override func deleteBackward() {
        super.deleteBackward()
        myDelegate?.textFieldDidDelete()
    }
    
}

