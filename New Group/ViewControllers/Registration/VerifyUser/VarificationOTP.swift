//
//  VarificationOTP.swift
//  OK
//
//  Created by Imac on 4/9/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol VarificationOTPDelegate: class {
   func selectedOTPNumber(otpNumber: String , vc: UIViewController)
}


class VarificationOTP: OKBaseController,UITextFieldDelegate {
  var delegate: VarificationOTPDelegate?
    @IBOutlet weak var lblVarificationCode: UILabel!
    @IBOutlet weak var lblSentTo: UILabel!
    @IBOutlet weak var lblReceive: UILabel!
    @IBOutlet weak var lblWait: UILabel!
    @IBOutlet weak var tfFirst: UITextField!
    @IBOutlet weak var tfSecond: UITextField!
    @IBOutlet weak var tfThired: UITextField!
    @IBOutlet weak var tfFourth: UITextField!
    @IBOutlet weak var tfFifth: UITextField!
    @IBOutlet weak var tfSix: UITextField!
    var timer: Timer?
    var timeCount = 59
    var fullOTPNumber = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        lblVarificationCode.text = "Verification code".localized
        lblSentTo.text = "Please type the verification code sent to".localized + " " + VerifyUPModel.share.MobileNumber
        lblReceive.text = "Didn't receive a code?".localized
        lblWait.text = "Please wait ".localized + "0:47"
        tfFirst.delegate = self
        tfFirst.tag = 1
        tfSecond.delegate = self
        tfSecond.tag = 2
        tfThired.delegate = self
        tfThired.tag = 3
        tfFourth.delegate = self
        tfFourth.tag = 4
        tfFifth.delegate = self
        tfFifth.tag = 5
        tfSix.delegate = self
        tfSix.tag = 6
        tfFirst.becomeFirstResponder()
        tfFirst.addTarget(self, action: #selector(textFieldEditing(_:)), for: .editingChanged)
        tfSecond.addTarget(self, action: #selector(textFieldEditing(_:)), for: .editingChanged)
        tfThired.addTarget(self, action: #selector(textFieldEditing(_:)), for: .editingChanged)
        tfFourth.addTarget(self, action: #selector(textFieldEditing(_:)), for: .editingChanged)
        tfFifth.addTarget(self, action: #selector(textFieldEditing(_:)), for: .editingChanged)
        tfSix.addTarget(self, action: #selector(textFieldEditing(_:)), for: .editingChanged)
        
        timer = Timer.scheduledTimer(timeInterval: 1,
                                     target: self,
                                     selector: #selector(self.updateTime(_:)),
                                     userInfo: nil,
                                     repeats: true)
        //Verification OTP
    }
    @objc func updateTime(_ timer: Timer) {
        timeCount -= 1
        lblWait.text = "Please wait ".localized + "0:" + String(timeCount)
        if timeCount == 1 {
        timeCount = 59
        timer.invalidate()
        self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func onClickBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func validateAllFields() -> Bool {
        if ((tfFirst.text?.count)! > 0) && ((tfSecond.text?.count)! > 0) && ((tfThired.text?.count)! > 0) && ((tfFourth.text?.count)! > 0) && ((tfFifth.text?.count)! > 0) && ((tfSix.text?.count)! > 0) {
            return true
        }
        return false
    }
    
    @objc func textFieldEditing(_ textField: UITextField) {
        if textField.tag == 600 {
          // call API
            if !validateAllFields() {
                return
            }
            var str = tfFirst.text!
            str =  str + tfSecond.text!
            str =  str + tfThired.text!
            str =  str + tfFourth.text!
            str =  str + tfFifth.text!
            str =  str + tfSix.text!
            self.fullOTPNumber = str
            println_debug("full otp number :: \(fullOTPNumber)")
            if let del = delegate {
                self.navigationController?.popViewController(animated: true)
                del.selectedOTPNumber(otpNumber: fullOTPNumber, vc: self)
            }
        }
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tfFirst.keyboardType = .numberPad
        tfSecond.keyboardType = .numberPad
        tfThired.keyboardType = .numberPad
        tfFourth.keyboardType = .numberPad
        tfFifth.keyboardType = .numberPad
        tfSix.keyboardType = .numberPad
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text!.count < 1  && string.count > 0){
            let nextTag = textField.tag + 1
            // get next responder
            var nextResponder = textField.superview?.viewWithTag(nextTag)
            if (nextResponder == nil){
                nextResponder = textField.superview?.viewWithTag(1)
            }
            textField.text = string
            println_debug("added numbers are ::: \(string)")
            nextResponder?.becomeFirstResponder()
            return false
        }else if (textField.text!.count >= 1  && string.count == 0){
            // on deleting value from Textfield
            let previousTag = textField.tag - 1
            // get next responder
            var previousResponder = textField.superview?.viewWithTag(previousTag)
            if (previousResponder == nil){
                previousResponder = textField.superview?.viewWithTag(1)
            }
            textField.text = ""
            println_debug("removed numbers are ::: \(string)")
            previousResponder?.becomeFirstResponder()
            return false
        }else if (textField.text!.count >= 1  && string.count > 0) {
            return false
        }
        return true
    }
}
