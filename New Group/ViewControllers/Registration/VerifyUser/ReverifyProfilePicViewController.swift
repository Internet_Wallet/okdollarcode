//
//  ReverifyProfilePicViewController.swift
//  OK
//
//  Created by iMac on 29/12/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit
import AVFoundation
import Rabbit_Swift

protocol  ReverifyProfilePicDelegate{
    func updateMainUIUI()
    func profilePhotoConfirm()
    func showProfilePhotoVC()
}

class ReverifyProfilePicViewController: OKBaseController,CameraVCUPDelegate {
    
    var session: AVCaptureSession?
    var stillImageOutput: AVCaptureStillImageOutput?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    
    var delegate : ReverifyProfilePicDelegate?

    @IBOutlet var previewView: UIView!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var capturedImage: UIImageView!
    @IBOutlet var btnTakeImage: UIButton!
    @IBOutlet var imgCamera: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        capturedImage.layer.cornerRadius = 150/2
        capturedImage.layer.masksToBounds = true
        previewView.backgroundColor = UIColor.clear
 
        self.dHeight = 210.0
        runGuard()

    }
    
    func runGuard() {
        guard(self.delegate?.updateMainUIUI() != nil) else {
            return
        }
    }
 
    @IBAction func btnTakePhoto(_ sender: Any) {
        if let del = delegate {
            del.showProfilePhotoVC()
        }
        
    }
    
    func updateProflePic(imageUrl: String, realImage: UIImage) {
          VerifyUPModel.share.ProfilePicAwsUrl = imageUrl
        capturedImage.image = realImage
       // UserDefaults.standard.set("NO", forKey: "ALL_FIELDS_FILLED")
        self.imgCamera.isHidden = false
        guard(self.delegate?.profilePhotoConfirm() != nil)else{
        return
        }
        runGuard()
    }
    
    func setupCamera() {
        session = AVCaptureSession()
        session!.sessionPreset = AVCaptureSession.Preset.photo
        let backCamera = AVCaptureDevice.default(.builtInWideAngleCamera,
                                                 for: AVMediaType.video,
                                                 position: .front)
        var error: NSError?
        var input: AVCaptureDeviceInput!
        do {
            input = try AVCaptureDeviceInput(device: backCamera!)
        } catch let error1 as NSError {
            error = error1
            input = nil
            println_debug(error!.localizedDescription)
        }
        if error == nil && session!.canAddInput(input) {
            session!.addInput(input)
            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput?.outputSettings = [AVVideoCodecKey: AVVideoCodecType.jpeg]
            if session!.canAddOutput(stillImageOutput!) {
                session!.addOutput(stillImageOutput!)
                videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session!)
                videoPreviewLayer!.frame = self.previewView.layer.bounds
                videoPreviewLayer!.videoGravity = AVLayerVideoGravity.resizeAspectFill
                videoPreviewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
                previewView.layer.addSublayer(videoPreviewLayer!)
                session!.startRunning()
            }
        }
    }
    
    
    func takePhoto() {
        let videoConnection = stillImageOutput!.connection(with: AVMediaType.video)
        if(videoConnection != nil) {
            stillImageOutput?.captureStillImageAsynchronously(from: videoConnection!, completionHandler: { (sampleBuffer, error) -> Void in
                if sampleBuffer != nil {
                    let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer!)
                    let dataProvider = CGDataProvider(data: imageData! as CFData)
                    let cgImageRef = CGImage.init(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: CGColorRenderingIntent.relativeColorimetric)
                    let image = UIImage(cgImage: cgImageRef!, scale: 1.0, orientation: UIImage.Orientation.right)
                    self.detect(userImage: image)
                }
            })
        }
    }
    
    func detect(userImage : UIImage) {
        let imageOptions =  NSDictionary(object: NSNumber(value: 5) as NSNumber, forKey: CIDetectorImageOrientation as NSString)
        let personciImage = CIImage.init(cgImage: userImage.cgImage!)
        let accuracy = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
        let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: accuracy)
        let faces = faceDetector?.features(in: personciImage, options: imageOptions as? [String : AnyObject])
        
        if let faces = faces {
            if faces.count > 1 {
               // showAlert(alertTitle:"", alertBody: "More Than One face was detected. Please try again".localized, alertImage: #imageLiteral(resourceName: "user"))
                DispatchQueue.main.async {
                alertViewObj.wrapAlert(title:"", body:"More Than One face was detected. Please try again".localized, img:#imageLiteral(resourceName: "alert-icon"))
                    
                alertViewObj.showAlert(controller: self)
            }
                btnTakeImage.isUserInteractionEnabled = true
            } else {
                if let face = faces.first as? CIFaceFeature {
                    println_debug("found bounds are \(face.bounds)")
                    if face.hasMouthPosition && face.hasLeftEyePosition &&  face.hasRightEyePosition {
                        session?.stopRunning()
                        self.capturedImage.isHidden = false
                        self.capturedImage.image = userImage
                        //lblStaus.font = UIFont(name: "Zawgyi-One", size: 20)
                        //seconds = 2
                       // timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(ProfilePhotoVC.updateTimerForMesasge)), userInfo: nil, repeats: true)
                        self.uploadImageToServer(u_image: userImage)
                    }else {
                        btnTakeImage.isUserInteractionEnabled = true
                        self.imgCamera.isHidden = false
                        alertViewObj.wrapAlert(title:"No Face".localized, body:"No face was detected. Please try again".localized, img:#imageLiteral(resourceName: "alert-icon"))
                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                           // self.timer.invalidate()
                           // self.seconds = 0
                           // self.openCamera()
                        })
                        DispatchQueue.main.async {
                            alertViewObj.showAlert(controller: self)
                        }
                    }
                } else {
                    btnTakeImage.isUserInteractionEnabled = true
                    self.imgCamera.isHidden = false
                    DispatchQueue.main.async {
                    alertViewObj.wrapAlert(title:"No Face".localized, body:"No face was detected. Please try again".localized, img:#imageLiteral(resourceName: "alert-icon"))
                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                        //self.timer.invalidate()
                       // self.seconds = 0
                       // self.openCamera()
                    })
                  
                        alertViewObj.showAlert(controller: self)
                    }
                    
                }
            }
        }
    }
    
    func uploadImageToServer(u_image: UIImage) {
        VerifyUPModel.share.ProfilePicAwsUrl = OKBaseController.imageTobase64(image: u_image)
    }
}
