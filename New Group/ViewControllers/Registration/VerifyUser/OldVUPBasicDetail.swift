//
//  OldVUPBasicDetail.swift
//  OK
//
//  Created by Subhash Arya on 24/03/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol OldVUPBasicDetailDelegate : class{
  func updateMainUIUI()
    func showPasswordSecurity()
   // func showPasswordSecurity(controller: SecuritQuestionPasswordVUP)
}

class OldVUPBasicDetail: OKBaseController,UITextFieldDelegate {
    
   weak var delegate : OldVUPBasicDetailDelegate?
    
    @IBOutlet weak var imgCountry: UIImageView!
    @IBOutlet weak var lblMobilNumber: UILabel!
    
    @IBOutlet weak var viewUserName: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var tfUserName: UITextField!
    @IBOutlet weak var viewMaleFemale: UIView!
    @IBOutlet weak var viewMalePrefix: UIView!
    @IBOutlet weak var viewFemalePrifix: UIView!
    @IBOutlet weak var btnClearUserName: UIButton!
    
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnU: UIButton!
    @IBOutlet weak var btnMg: UIButton!
    @IBOutlet weak var btnMr: UIButton!
    @IBOutlet weak var btnDr: UIButton!
    @IBOutlet weak var btnDaw: UIButton!
    @IBOutlet weak var btnMa: UIButton!
    @IBOutlet weak var btnMs: UIButton!
    @IBOutlet weak var btnMrs: UIButton!
    @IBOutlet weak var btnMDr: UIButton!
    @IBOutlet weak var btnBFirst: UIButton!
    @IBOutlet weak var btnBSecond: UIButton!
    @IBOutlet weak var btnBThired: UIButton!
    @IBOutlet weak var viewBMaleFemaleCategory: UIView!
    
    @IBOutlet weak var viewOTP: UIView!
    @IBOutlet weak var viewFather: UIView!
    @IBOutlet weak var viewDOB: UIView!
    @IBOutlet weak var tfDOB: UITextField!
    @IBOutlet weak var tfFatherName: UITextField!
    @IBOutlet weak var viewFatherPrefix: UIView!
    @IBOutlet weak var tfOTP: UITextField!
    
    @IBOutlet weak var viewBFatherInitial: UIView!
    @IBOutlet weak var btnBFatherFirst: UIButton!
    @IBOutlet weak var btnBFatherSecond: UIButton!
    @IBOutlet weak var btnFU: UIButton!
    @IBOutlet weak var btnFMr: UIButton!
    @IBOutlet weak var btnFDr: UIButton!
    @IBOutlet weak var lblHeaderOTP: MarqueeLabel!
     @IBOutlet weak var btnReset: UIButton!
    
   
    var isYourNameKeyboardHidden = false
     var isFatherNameKeyboardHidden = false
    var namePrefix = ""
    var countryCode = ""
    var mobilNO = ""
    var isNextViewShow = false
    
    var strDOB = ""
    var fatherPrefix = ""
    let dateFormatter = DateFormatter()
    var NRCSET = ""
    var PASSWORDSET = ""
    var NAMESET = ""
    let  nameCHAR = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
    let mobilNUMBERCHAR  = "1234567890"
    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        tfUserName.delegate = self
        tfDOB.delegate = self
        tfFatherName.delegate = self
        tfOTP.delegate = self
        self.dHeight = 154.0
        
        if let oName = preNetInfo?.operatorName {
         VerifyUPModel.share.NetworkOperatorName = oName
        }
        if let nName = preNetInfo?.networkName {
            VerifyUPModel.share.NetworkOperator = nName
        }
    
 
        if !(VerifyUPModel.share.MobileNumber == "") {
            let (mobileNumber,CountryCode) = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: VerifyUPModel.share.MobileNumber)
            lblMobilNumber.text = "(" + CountryCode + ") " + mobileNumber
            let countryData = identifyCountry(withPhoneNumber:CountryCode )
            if let safeImage = UIImage(named: countryData.countryFlag) {
                DispatchQueue.main.async {
                    self.imgCountry.image = safeImage
                }
            }
        }
        
        if ok_default_language == "en" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue"), for: .normal)
            NAMESET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
        }else {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
            NAMESET =  "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာ "
        }
        NRCSET = "0123456789"
        PASSWORDSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        viewBMaleFemaleCategory.isHidden = true
        viewBFatherInitial.isHidden = true
        self.tfUserName.placeholder = "Enter Your Name".localized
        self.btnMale.setTitle("Male".localized, for: .normal)
        self.btnFemale.setTitle("Female".localized, for: .normal)
        self.btnU.setTitle("U".localized, for: .normal)
        self.btnMg.setTitle("Mg".localized, for: .normal)
        self.btnMr.setTitle("Mr".localized, for: .normal)
        self.btnDr.setTitle("Dr".localized, for: .normal)
        self.btnDaw.setTitle("Daw".localized, for: .normal)
        self.btnMa.setTitle("Ma".localized, for: .normal)
        self.btnMs.setTitle("Ms".localized, for: .normal)
        self.btnMrs.setTitle("Mrs".localized, for: .normal)
        self.btnMDr.setTitle("Dr".localized, for: .normal)
        self.btnBThired.setTitle("Dr".localized, for: .normal)
        
        self.btnFU.setTitle("U".localized, for: .normal)
        self.btnFMr.setTitle("Mr".localized, for: .normal)
        self.btnFDr.setTitle("Dr".localized, for: .normal)
        self.btnBFatherFirst.setTitle("U".localized, for: .normal)
        self.btnBFatherSecond.setTitle("Dr".localized, for: .normal)
        self.tfDOB.placeholder = "Select Date of Birth".localized
        self.tfFatherName.placeholder = "Enter Father Name".localized
        
        self.tfUserName.placeholder = "Enter Your Name".localized
        self.tfFatherName.placeholder = "Enter Father Name".localized
        self.tfDOB.placeholder = "Select Date of Birth".localized
        self.lblHeaderOTP.text = "Your OTP will be sent to your registered email please check your email".localized
        self.tfOTP.placeholder = "Enter your OTP".localized
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    @IBAction func btnSelectGenderAction(_ sender: UIButton) {
        viewMaleFemale.isHidden = true
        if sender.title(for: .normal) == "Male".localized{
            if ok_default_language == "my" {
                viewMalePrefix.isHidden = true
                viewFemalePrifix.isHidden = true
                viewBMaleFemaleCategory.isHidden = false
                btnBFirst.setTitle("U".localized, for: .normal)
                btnBSecond.setTitle("Mg".localized, for: .normal)
            }else {
                viewMalePrefix.isHidden = false
                viewFemalePrifix.isHidden = true
                viewBMaleFemaleCategory.isHidden = true
            }
            imgUser.image = UIImage(named: "r_user")
        }else {
            if ok_default_language == "my" {
                viewBMaleFemaleCategory.isHidden = false
                viewFemalePrifix.isHidden = true
                viewMalePrefix.isHidden = true
                btnBFirst.setTitle("Daw".localized, for: .normal)
                btnBSecond.setTitle("Ma".localized, for: .normal)
            }else {
                viewFemalePrifix.isHidden = false
                viewMalePrefix.isHidden = true
                viewBMaleFemaleCategory.isHidden = true
            }
            imgUser.image = UIImage(named: "r_female")
        }
    }
    
    @IBAction func btnMalePrefixAction(_ sender: UIButton) {
        isYourNameKeyboardHidden = true
        viewMalePrefix.isHidden = true
        namePrefix = sender.title(for: .normal)!
        self.setPrefixName(prefixName: sender.title(for: .normal)!, txtField: tfUserName)
    }
    
    @IBAction func btnFeMalePrefixAction(_ sender: UIButton) {
        isYourNameKeyboardHidden = true
        viewFemalePrifix.isHidden = true
        namePrefix = sender.title(for: .normal)!
        self.setPrefixName(prefixName: sender.title(for: .normal)!, txtField: tfUserName)
    }
    
    @IBAction func btnBurmeseUserNameInitialAction(sender : UIButton) {
        isYourNameKeyboardHidden = true
        viewBMaleFemaleCategory.isHidden = true
        namePrefix = sender.title(for: .normal)!
        self.setPrefixName(prefixName: sender.title(for: .normal)!, txtField: tfUserName)
    }
    @IBAction func btnSelectFatherPrefixAction(_ sender: UIButton) {
        isFatherNameKeyboardHidden = true
        fatherPrefix = sender.title(for: .normal)!
        viewFatherPrefix.isHidden = true
        tfFatherName.textAlignment = .left
        self.setPrefixName(prefixName: sender.title(for: .normal)!, txtField: tfFatherName)
    }
    
    @IBAction func btnBFatherInitialAction(_ sender: UIButton) {
        isFatherNameKeyboardHidden = true
        fatherPrefix = sender.title(for: .normal)!
        viewBFatherInitial.isHidden = true
        self.setPrefixName(prefixName: sender.title(for: .normal)!, txtField: tfFatherName)
    }
    
//    func setPrefixName(prefixName : String, txtField : UITextField) {
//        let prelabel = PreNameView.loadView() as! PreNameView
//        prelabel.setPreName(name : prefixName, alignment : .left)
//        txtField.leftView = prelabel
//        txtField.leftViewMode = UITextField.ViewMode.always
//        txtField.textAlignment = .left
//        btnClearUserName.isHidden = false
//        txtField.becomeFirstResponder()
//    }
    
    func setPrefixName(prefixName : String, txtField : UITextField) {
        let myString = prefixName
        let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: "Zawgyi-One", size: 18) ?? UIFont.systemFont(ofSize: 18.0) ])
        let lblName = UILabel()
        lblName.frame = CGRect(x: 0, y: -1, width:stringSize.width + 10 , height: 52)
        lblName.textAlignment = .left
        lblName.font =  UIFont(name: "Zawgyi-One", size: 18)
        lblName.text = myString
        let btnView = UIView()
        btnView.frame = CGRect(x: 0, y: 0, width: lblName.frame.width , height: 52)
        btnView.addSubview(lblName)
        txtField.leftView = btnView
        txtField.leftViewMode = UITextField.ViewMode.always
        txtField.textAlignment = .left
        txtField.becomeFirstResponder()
    }
    
    @IBAction func btnClearUserName(_ sender: Any) {
        isYourNameKeyboardHidden = false
        viewMaleFemale.isHidden = true
        viewFemalePrifix.isHidden = true
        viewMalePrefix.isHidden = true
        tfUserName.text = ""
        tfUserName.leftView = nil
        tfUserName.resignFirstResponder()
        imgUser.image = UIImage(named: "r_user")
        tfUserName.textAlignment = .center
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == tfDOB {
            
            let date = Calendar.current.date(byAdding: .year, value: -12, to: Date())
            let datePicker = UIDatePicker()
            datePicker.tag = 1
            datePicker.datePickerMode = .date
            datePicker.backgroundColor = UIColor.white
            datePicker.maximumDate = date!
            datePicker.calendar = Calendar(identifier: .gregorian)
            if #available(iOS 13.4, *) {
                           datePicker.preferredDatePickerStyle = .wheels
                       } else {
                           // Fallback on earlier versions
                       }
            if tfDOB.text?.count == 0 {
                datePicker.date = date!
            }else{
                datePicker.date = dateFormatter.date(from: tfDOB.text!)!
            }
            
            let calendar = Calendar.current
            var minDateComponent = calendar.dateComponents([.day,.month,.year], from: Date())
            minDateComponent.day = 01
            minDateComponent.month = 01
            minDateComponent.year = 1900
            datePicker.minimumDate = calendar.date(from: minDateComponent)
            
            datePicker.addTarget(self, action: #selector(datePickerChanged(sender:)), for: .valueChanged)
            textField.inputView = datePicker
            
            let screenSize: CGRect = UIScreen.main.bounds
            let viewDOBKeyboard = UIView(frame: CGRect(x: 0, y: 0, width:screenSize.width , height: 40))
            viewDOBKeyboard.backgroundColor = UIColor(red: 244/255, green: 199/255, blue: 22/255, alpha: 1)
            
            let btnCancel  = UIButton(frame: CGRect(x: 10, y: 0, width:100, height: 40))
            btnCancel.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
            btnCancel.setTitle("Cancel", for: UIControl.State.normal)
            btnCancel.addTarget(self, action:#selector(btnCancelDOBAction), for: UIControl.Event.touchUpInside)
            viewDOBKeyboard.addSubview(btnCancel)
            
            let btnDone  = UIButton(frame: CGRect(x: screenSize.width - 110, y: 0, width:100, height: 40))
            btnDone.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
            btnDone.setTitle("Done", for: UIControl.State.normal)
            btnDone.addTarget(self, action:#selector(btnDoneDOBAction), for: UIControl.Event.touchUpInside)
            strDOB = dateFormatter.string(from: datePicker.date)
            viewDOBKeyboard.addSubview(btnDone)
            tfDOB.inputAccessoryView = viewDOBKeyboard
            tfDOB.textAlignment = .left
        }else if textField == tfFatherName{
            if isFatherNameKeyboardHidden {
                return true
            }else{
                if ok_default_language == "my" {
                    viewBFatherInitial.isHidden = false
                }else {
                  viewFatherPrefix.isHidden = false
                }
                return false
            }
        }else if textField ==  tfUserName {
            if isYourNameKeyboardHidden {
                viewMaleFemale.isHidden = true
                return true
            }else{
                viewMaleFemale.isHidden = false
                return false
            }
        }else if textField == tfOTP {
            tfOTP.keyboardType = .numberPad
//            if ok_default_language == "my" {
//                UserDefaults.standard.set(true, forKey: "Languagechange")
//                ok_default_language = "en"
//            }
        }
        return true
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == tfUserName {
            VerifyUPModel.share.Name = namePrefix + "," + tfUserName.text!
        }else if textField == tfFatherName {
            VerifyUPModel.share.FatherName = fatherPrefix + "," + tfFatherName.text!
        }else if textField == tfOTP {
//            if UserDefaults.standard.bool(forKey: "Languagechange") {
//              ok_default_language = "my"
//            }
            if !isNextViewShow {
                VerifyUPModel.share.ReRegisterOtp = tfOTP.text!
                if let dl = self.delegate {
                    dl.showPasswordSecurity()
                }
            }
        }
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if textField == tfUserName{
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: NAMESET).inverted).joined(separator: "")) { return false }
            if text.count > 2 {
                if !isNextViewShow {
                    if self.dHeight! <= self.viewFather.frame.height  + self.viewFather.frame.origin.y{
                       self.dHeight = self.viewFather.frame.height  + self.viewFather.frame.origin.y
                        self.runGuard()
                    }
                }
            }
            if (range.location == 0 && string == " ") {
                       return false
            }
            if text.count > 50 { return false }
            if restrictMultipleSpaces(str: string, textField: tfUserName) {
                return true
            }else {
                if  textField.text?.last == " " {
                    textField.text?.removeLast()
                }
                return false
            }
            
          
      
        }else if textField == tfFatherName {
         if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: NAMESET).inverted).joined(separator: "")) { return false }
            if text.count > 2 {
                if !isNextViewShow {
                    if self.dHeight! <= self.viewDOB.frame.height  + self.viewDOB.frame.origin.y{
                        self.dHeight = self.viewDOB.frame.height  + self.viewDOB.frame.origin.y
                         self.runGuard()
                    }
                }
            }
            if text.count > 50 { return false }
            if restrictMultipleSpaces(str: string, textField: tfFatherName) {
                return true
            }else {
                if  textField.text?.last == " " {
                    textField.text?.removeLast()
                }
                return false
            }
        }else if textField == tfOTP {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: NRCSET).inverted).joined(separator: "")) { return false }
        }
        return true
    }
    override var textInputMode: UITextInputMode? {
        let language = ok_default_language
        if language.isEmpty {
            return super.textInputMode
        } else {
            for tim in UITextInputMode.activeInputModes {
                if tim.primaryLanguage!.contains(language) {
                    return tim
                }
            }
            return super.textInputMode
        }
    }
    

    @objc func datePickerChanged(sender: UIDatePicker) {
        sender.calendar = Calendar(identifier: .gregorian)
        strDOB = dateFormatter.string(from: sender.date)
    }
    
    @objc func btnCancelDOBAction () {
        tfDOB.resignFirstResponder()
    }
    
    
    @objc func btnDoneDOBAction () {
        tfDOB.resignFirstResponder()
        tfDOB.text = strDOB
        tfDOB.textAlignment = .left
        if strDOB.count > 0 {
            VerifyUPModel.share.DateOfBirth = strDOB
         //   self.ValidateReverifyRequestWS()
            
            if self.dHeight! <= self.viewOTP.frame.height  + self.viewOTP.frame.origin.y{
                self.dHeight = self.viewOTP.frame.height  + self.viewOTP.frame.origin.y
                self.runGuard()
            }
            
//            self.dHeight = 365
//            self.runGuard()
        }
    }
    
    @IBAction func btnResetAction(_ sender: Any) {
        isYourNameKeyboardHidden = false
        tfUserName.text = ""
        tfUserName.textAlignment = .center
        tfUserName.leftView = nil
        imgUser.image = UIImage(named: "r_user")
        viewMaleFemale.isHidden = true
        viewMalePrefix.isHidden = true
        viewFemalePrifix.isHidden = true
        VerifyUPModel.share.Name = ""
        
        isFatherNameKeyboardHidden = false
        tfFatherName.text = ""
        tfFatherName.textAlignment = .center
        tfFatherName.leftView = nil
        viewFatherPrefix.isHidden = true
        VerifyUPModel.share.FatherName = ""
        
        tfDOB.text = ""
        tfDOB.textAlignment = .center
        strDOB = ""
        VerifyUPModel.share.DateOfBirth = ""
        
        tfOTP.text = ""

    }
    
    func runGuard() {
        if let del = self.delegate {
            del.updateMainUIUI()
        }else{
            println_debug("Delegate is not working")
        }
     
    }
    
    func ValidateReverifyRequestWS() {
        var urlStr   = Url.URL_ValidateReverifyForgotPassword + "MobileNumber=" + VerifyUPModel.share.MobileNumber + "&Name=" + VerifyUPModel.share.Name + "&FatherName=" + VerifyUPModel.share.FatherName  + "&Dob=" + VerifyUPModel.share.DateOfBirth
        
        urlStr = urlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let urlS = getUrl(urlStr: urlStr, serverType: .serverApp)
        println_debug(urlS)
        let param = [String:String]() as AnyObject
        
        TopupWeb.genericClass(url: urlS, param: param, httpMethod: "GET", handle: { (response , success) in
            if success {
                println_debug(response)
                let dic = response as? Dictionary<String , Any>
                println_debug(dic!)
                if dic!["Code"] as! NSInteger == 200 {
                        DispatchQueue.main.async {
                            if self.dHeight! <= self.viewOTP.frame.height  + self.viewOTP.frame.origin.y{
                              self.dHeight = self.viewOTP.frame.height  + self.viewOTP.frame.origin.y
                                self.runGuard()
                            }
                        }
                }else {
                    self.showAlert(alertTitle: "", alertBody: "You Entered Incorrect Details".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
                }
            }else {
                self.showAlert(alertTitle: "", alertBody: "Network error Please try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
                println_debug(response)
            }
        })
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
}
