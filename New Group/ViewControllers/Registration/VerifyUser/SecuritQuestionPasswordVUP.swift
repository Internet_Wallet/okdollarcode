//
//  SecuritQuestionPasswordVUP.swift
//  OK
//
//  Created by SHUBH on 11/23/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.

import UIKit
import IQKeyboardManagerSwift
import MessageUI

protocol  SecuritQuestionPasswordVUPDelegate{
    func updateMainUIUI()
    func patternPasswordVC()
    func resetSecurityType()
    func resignKeyboard()
  //  func showPasswordSecurity(controller: SecuritQuestionPasswordVUP)
}

class SecuritQuestionPasswordVUP: OKBaseController,PasswordPatternViewControllerDelegate,UITextFieldDelegate,WebServiceResponseDelegate,OTPDelegate,MFMessageComposeViewControllerDelegate{
 
    @IBOutlet weak var btnPattenOnField: UIButton!
    
    var delegate : SecuritQuestionPasswordVUPDelegate?
    
    let notificationCenter = NotificationCenter.default
    
    @IBOutlet weak var btnSecurity_Q: UIButton!
    @IBOutlet weak var topConstraintsSSQues: NSLayoutConstraint!
    @IBOutlet weak var imgPassword: UIImageView!
    @IBOutlet weak var imgPattern: UIImageView!
    @IBOutlet weak var tfS_Answer: UITextField!
    @IBOutlet weak var tfEnterPassword: UITextField!
    @IBOutlet weak var viewSAnswer: UIView!
    @IBOutlet weak var viewQuestion: UIView!
    @IBOutlet weak var viewSecurityQA: UIView!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var paswordViewButton: UIButton!
    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblPattern: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var lblQuestion: MarqueeLabel!
    @IBOutlet weak var buttonContriants: NSLayoutConstraint!
     @IBOutlet weak var btnReset: UIButton!
    var paramsFailed = Dictionary<String,Any>()
    
    var password: String = ""
    
    var codeObj = CodePreLogin(cName: "Myanmar", codes: "+95", flag: "myanmar")
    var verifyStatus = ""
     var isTFAnwerShow = false
    //VillageManager.shared.data.registrationStatus == "0" || VillageManager.shared.data.registrationStatus == "1"
    var NAMESET = ""
    var securityQuestion = ""
    var isansKeyboardHidden = false
    var fnt = UIFont.systemFont(ofSize: 18)
    
    var loader = false
    var failureCount = 0
    
    var pattrenselect = ""
    var resetpasswordtype = ""
    
    //MARK: - Views life cycle methods
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        failureCount = 0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
            font = UIFont.systemFont(ofSize: 18)
        }
        tfEnterPassword.font = font
        tfS_Answer.font = font
       
        if ok_default_language == "en" {
        btnReset.setBackgroundImage(UIImage(named: "reset_blue"), for: .normal)
            NAMESET = NAME_CHAR_SET_En
            tfEnterPassword.font = UIFont.systemFont(ofSize: 18)
            tfS_Answer.font = UIFont.systemFont(ofSize: 18)
          //  tfEnterPassword.isSecureTextEntry = true
        }else if ok_default_language == "my" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
            NAMESET =  NAME_CHAR_SET_My//STREETNAME_CHAR_SET_My
            tfEnterPassword.font = UIFont(name: appFont, size: 18)
            tfS_Answer.font = UIFont(name: appFont, size: 18)
            tfEnterPassword.isSecureTextEntry = false
        }else {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
            NAMESET =  NAME_CHAR_SET_Uni//STREETNAME_CHAR_SET_Uni
            
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfS_Answer.attributedPlaceholder = NSAttributedString(string: "Enter Answer", attributes:attributes as [NSAttributedString.Key : Any])
                
                let attributes1 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfEnterPassword.attributedPlaceholder = NSAttributedString(string: "Enter Password", attributes:attributes1 as [NSAttributedString.Key : Any])
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfS_Answer.attributedPlaceholder = NSAttributedString(string: "Enter Answer", attributes:attributes)
                tfS_Answer.font = UIFont.systemFont(ofSize: 18)
                
                let attributes1 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfEnterPassword.attributedPlaceholder = NSAttributedString(string: "Enter Password", attributes:attributes1)
                tfEnterPassword.font = UIFont.systemFont(ofSize: 18)
            }
            
        }
       // IQKeyboardManager.sharedManager().enable = false
        tfEnterPassword.delegate  = self
        tfS_Answer.delegate = self
        self.dHeight = 97.0
        tfS_Answer.isSecureTextEntry = false

        verifyStatus = VillageManager.shared.data.registrationStatus
        if verifyStatus == "1" || verifyStatus == "0" {
            println_debug(VillageManager.shared.data.securityQ)
            if ok_default_language  == "my" {
                lblQuestion.text = VillageManager.shared.data.securityQ["QuestionBurmese"] as? String
                securityQuestion = VillageManager.shared.data.securityQ["QuestionBurmese"] as? String ?? ""
            }else if ok_default_language  == "en"{
                lblQuestion.text = VillageManager.shared.data.securityQ["Question"] as? String
                securityQuestion = VillageManager.shared.data.securityQ["Question"] as? String ?? ""
            }else {
                lblQuestion.text = VillageManager.shared.data.securityQ["QuestionBurmeseUniCode"] as? String
                securityQuestion = VillageManager.shared.data.securityQ["QuestionBurmeseUniCode"] as? String ?? ""
            }
        }else {
            println_debug("Registrion status not found Please try login again")
        }
        
    lblHeader.text = "Select Security Type".localized
    tfEnterPassword.placeholder = "Enter Password".localized
    tfS_Answer.placeholder = "Enter Answer".localized
    lblPattern.text = "Pattern".localized
    lblPassword.text = "Password".localized
    btnSubmit.setTitle("Submit".localized, for: .normal)
        
        tfEnterPassword.keyboardType = .default
        
        notificationCenter.addObserver(self, selector:#selector(appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
          
}

    @objc func appMovedToForeground() {
      print("App moved to foreground!")
        if loader == true{
            PTLoader.shared.show()
           
        }
      }

    func runGuard() {
        guard(self.delegate?.updateMainUIUI() != nil)else {
            return
        }
    }
    
    @IBAction func btnPatternAction(_ sender: Any) {
     //   if !(imgPattern.image == UIImage(named:"select_radio")) {
        //   resetSecurity()
//            imgPassword.image = UIImage(named:"r_radio_btn")
//            VerifyUPModel.share.PasswordType = "1"
    //    }
        guard(self.delegate?.patternPasswordVC() != nil)else{
            return
        }
    }
    
    @IBAction func btnPattenActionOnField(_ sender: Any) {
        guard(self.delegate?.patternPasswordVC() != nil)else{
            return
        }
    }
    
    
    
    @IBAction func btnPasswordAction(_ sender: Any) {
        
//        var emptyFields = ""
//         if VerifyUPModel.share.FatherName == "" {
//            emptyFields = emptyFields + "\n" + "Father Name".localized
//             DispatchQueue.main.async {
//                 alertViewObj.wrapAlert(title: "Please Fill All Fields".localized, body:emptyFields , img:#imageLiteral(resourceName: "alert-icon"))
//                 alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//                 })
//                 alertViewObj.showAlert(controller: self)
//             }
//         }
       // else{
            VerifyUPModel.share.Password = ""
            userDef.removeObject(forKey: "Reset")
           if !(imgPassword.image == UIImage(named:"select_radio")) {
             //  self.topConstraintsSSQues.constant = 0
               self.viewPassword.isHidden = false
               self.tfEnterPassword.becomeFirstResponder()
               resetSecurity()
               imgPassword.image = UIImage(named:"select_radio")
               imgPattern.image = UIImage(named:"r_radio_btn")
               VerifyUPModel.share.PasswordType = 0
               if !isTFAnwerShow {
                   self.dHeight = 154.0
                   runGuard()
               }
           }
      //  }
    
        tfEnterPassword.isUserInteractionEnabled = true
        btnPattenOnField.isHidden = true
    }
    
    @IBAction func passwordViewClick(_ sender: Any) {
        
    }
    
    
    func ShowPatternPasswordViewAndPasswordString(strPassword: String, PasswordPatternViewControllerR: UIViewController) {
        
//        var emptyFields = ""
//         if VerifyUPModel.share.FatherName == "" {
//            emptyFields = emptyFields + "\n" + "Father Name".localized
//             DispatchQueue.main.async {
//                 alertViewObj.wrapAlert(title: "Please Fill All Fields".localized, body:emptyFields , img:#imageLiteral(resourceName: "alert-icon"))
//                 alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//                 })
//                 alertViewObj.showAlert(controller: self)
//             }
//         }
//        else{
        pattrenselect = "Pattern"
        imgPassword.image = UIImage(named:"r_radio_btn")
        imgPattern.image = UIImage(named:"select_radio")
        VerifyUPModel.share.Password = strPassword
        //topConstraintsSSQues.constant = -57
       // self.viewPassword.isHidden = true
        self.viewPassword.isHidden = false
        resetSecurity()
        imgPassword.image = UIImage(named:"r_radio_btn")
        VerifyUPModel.share.PasswordType = 1
        var hashPassword = String()
      // tfEnterPassword.text = strPassword
        for _ in 0..<strPassword.count {  hashPassword += "*" }
        tfEnterPassword.text = hashPassword
        tfEnterPassword.isUserInteractionEnabled = false
        btnPattenOnField.isHidden = false
        //verifyStatus == "0" || verifyStatus == "-1"
        if securityQuestion == "" {
            buttonContriants.constant = -114
            self.dHeight = 211.0
            runGuard()
        }else  {
            self.dHeight = 268.0
            runGuard()
        }
        PasswordPatternViewControllerR.dismiss(animated: false, completion: nil)
            
     //   }
    }
    
    
    func CancelPatternPasswordVC(PasswordPatternViewControllerR: UIViewController) {
        PasswordPatternViewControllerR.dismiss(animated: false, completion: nil)
       // imgPassword.image = UIImage(named:"r_radio_btn")
       // VerifyUPModel.share.Password = ""
       if  VerifyUPModel.share.Password != "" {
        
        if  VerifyUPModel.share.PasswordType == 1 {
            imgPassword.image = UIImage(named:"r_radio_btn")
            self.viewPassword.isHidden = false
        }else{
            self.viewPassword.isHidden = false
            imgPassword.image = UIImage(named:"select_radio")
        }
          
        }
        else{
            resetpasswordtype = userDef.value(forKey: "Reset") as? String ?? ""
            
            if resetpasswordtype == "Reset"{
              //  resetpasswordtype = ""
                self.viewPassword.isHidden = false
                imgPassword.image = UIImage(named:"r_radio_btn")
            }else{
                resetpasswordtype = ""
                userDef.removeObject(forKey: "Reset")
                self.viewPassword.isHidden = false
                imgPassword.image = UIImage(named:"select_radio")
            }
           
      }
       
    }
    
    func showAlert(txtTitle : String ,body : String,titleImg : UIImage, btnTitle : String){
        alertViewObj.wrapAlert(title: txtTitle, body: body, img:titleImg)
        alertViewObj.addAction(title: btnTitle, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }

    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == tfEnterPassword {
            if textField.text?.count ?? 0 < 6 {
                showAlert(txtTitle: "", body:"Password must be minimum 6 characters".localized , titleImg: #imageLiteral(resourceName: "r_password"), btnTitle: "OK".localized)
                tfS_Answer.text = ""
                tfEnterPassword.text = ""
                password = ""
                self.buttonContriants.constant = 0
                runGuard()
            }
            else{
                VerifyUPModel.share.Password = password//tfEnterPassword.text ?? ""
              //  tfEnterPassword.isSecureTextEntry = false
                //verifyStatus == "0" || verifyStatus == "-1"
                if securityQuestion == "" {
                    self.buttonContriants.constant = -114
                    self.dHeight = 211.0
                    runGuard()
                }else {
                    self.dHeight = 268.0
                    runGuard()
                }
                if  VerifyUPModel.share.SecurityAnswer != ""{
                    dHeight = btnSubmit.frame.origin.y + btnSubmit.frame.height
                   runGuard()
                }
                
            }
            
        }else if textField == tfS_Answer{
            isTFAnwerShow = true
            VerifyUPModel.share.SecurityAnswer = tfS_Answer.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            dHeight = btnSubmit.frame.origin.y + btnSubmit.frame.height
           runGuard()
        }
        return true
    }
    
//    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
//        if textField == tfEnterPassword {
//           // VerifyUPModel.share.Password = tfEnterPassword.text ?? ""
//            tfS_Answer.isSecureTextEntry = false
//            runGuard()
//        }
//    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == tfS_Answer {
           // tfEnterPassword.isSecureTextEntry = false
            tfS_Answer.isSecureTextEntry = false
            tfS_Answer.textContentType = .name//init(rawValue: "")

        }
        if textField == tfEnterPassword {
          //  tfEnterPassword.isSecureTextEntry = true
        }
        return true
    }
    
//    public func textFieldDidBeginEditing(_ textField: UITextField) {
//        if textField == tfS_Answer {
//          tfS_Answer.isSecureTextEntry = false
//          tfS_Answer.textContentType = .init(rawValue: "")
//        //  self.delegate?.showPasswordSecurity(controller: self)
//        }
//    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
     
        if textField == tfEnterPassword {
           // tfEnterPassword.isSecureTextEntry = true
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: PASSWORD_CHAR_SET).inverted).joined(separator: ""))

            { return false }
            
           // password = textField.text ?? ""
            var hashPassword = String()
                  let newChar = string.first
                  let offsetToUpdate = password.index(password.startIndex, offsetBy: range.location)

                  if string == "" {
                    password.remove(at: offsetToUpdate)
                      return true
                  }
                  else { password.insert(newChar!, at: offsetToUpdate) }

                print("@@@", password)

                  for _ in 0..<password.count {  hashPassword += "*" }
                  textField.text = hashPassword
                  return false
          
        }else if textField == tfS_Answer {
            tfS_Answer.isSecureTextEntry = false
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: NAMESET).inverted).joined(separator: "")) { return false }
            
            if text.count > 0{
//             dHeight = btnSubmit.frame.origin.y + btnSubmit.frame.height
//            runGuard()
            }
            if text.count > 50 { return false }
            if restrictMultipleSpaces(str: string, textField: tfS_Answer) {
                return true
            }else {
                if  textField.text?.last == " " {
                    textField.text?.removeLast()
                }
                return false
            }
        }
        return true
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) { 
        
        if let del = delegate {
            del.resignKeyboard()
        }
        
        checkEmptyfield(handler: {(messase,success) in
            if success {
                if  VillageManager.shared.data.registrationStatus == "-1" {
//                    DispatchQueue.main.async {
//                    alertViewObj.wrapAlert(title: "", body:"Please make sure you have balance to send sms and send sms from primary sim that you have entered as in OK$ Number", img:#imageLiteral(resourceName: "mobile_bcd"))
//                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                        self.openSMS()
//                    })
//                     alertViewObj.showAlert(controller: self)
//                    }
                }else {
                    let oTPViewController  = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                    oTPViewController.modalPresentationStyle = .overCurrentContext
                    oTPViewController.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                    oTPViewController.delegate = self
                    self.present(oTPViewController, animated: false, completion: nil)
//                    if let window = UIApplication.shared.keyWindow {
//                        window.rootViewController?.addChild(oTPViewController)
//                        oTPViewController.view.frame = .init(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
//                        oTPViewController.delegate = self
//                        window.addSubview(oTPViewController.view)
//                        window.makeKeyAndVisible()
//                    }
                }
            }else {
                DispatchQueue.main.async {
                alertViewObj.wrapAlert(title: "Please Fill All Fields".localized, body:messase , img:#imageLiteral(resourceName: "alert-icon"))
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                })
                 alertViewObj.showAlert(controller: self)
                }
            }
        })
    }
    
    func APIFailedWebService(failureFor: String) {
        if appDelegate.checkNetworkAvail() {
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
          
            do {
                let web = WebApiClass()
                web.delegate = self
                let url = getUrl(urlStr: Url.URL_FailureTrackInfo, serverType: .serverApp)
                let param = VerifyUPModel.share.wrapData()
                let jsonData = try JSONSerialization.data(withJSONObject: param, options:JSONSerialization.WritingOptions(rawValue: 0))
                let dic = ["AppBuildNumber": buildNumber,"AppBuildVersion": buildVersion,"AppType": "1","CellTowerId": "","DeviceInfo": "deviceinfo","FailureStage": failureFor,"HasSimCard": "1","IsRoaming": "0","Latitude": UserModel.shared.lat,
                           "Longitude": UserModel.shared.long,"MobileNumber": VerifyUPModel.share.MobileNumber,"OsType": "1","Remarks": String(data: jsonData, encoding: .utf8)]
                println_debug(url)
                println_debug(dic)
                
                web.genericClassReg(url: url, param: dic as Dictionary<String, Any>, httpMethod: "POST", mScreen: "AddRegistraionFailureTrackInfo", hbData: nil, authToken: nil)
            }catch {
                DispatchQueue.main.async {
                    self.removeProgressView()
                }
            }
        } else {
       
            DispatchQueue.main.async {
                alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img:#imageLiteral(resourceName: "r_user"))
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                })
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    
    
    func selectedOTPNumber(otpNumber: String , vc:OTPViewController){
        println_debug(otpNumber)
        VerifyUPModel.share.ReRegisterOtp = otpNumber
        vc.view.removeFromSuperview()
//        DispatchQueue.main.async {
//        alertViewObj.wrapAlert(title: "", body:"Please make sure you have balance to send sms and send sms from primary sim that you have entered as in OK$ Number", img:#imageLiteral(resourceName: "mobile_bcd"))
//        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            self.openSMS()
      //  })
      //   alertViewObj.showAlert(controller: self)
      //  }
       
    }
    
    func checkEmptyfield(handler : (_ emptyString : String , _ isBool : Bool) -> Void){
        var emptyFields = ""
        
        if VerifyUPModel.share.ProfilePicAwsUrl == "" {
            emptyFields = emptyFields + " " + "Profile Image".localized
        }
        
        
        if VerifyUPModel.share.Name == "" {
            emptyFields = "User Name".localized
        }else {
            let token = VerifyUPModel.share.Name.components(separatedBy: ",")
            if token[1].count < 3 {
                emptyFields = emptyFields + "\n" + "User Name must be more than 2 characters".localized
            }
        }
        
        if VerifyUPModel.share.FatherName == "" {
            emptyFields = emptyFields + "\n" + "Father Name".localized
        }else if VerifyUPModel.share.FatherName.contains(find: ",") {
            let token = VerifyUPModel.share.FatherName.components(separatedBy: ",")
            if token[1].count < 3 {
                emptyFields = emptyFields + "\n" + "Father Name must be more than 2 characters".localized
            }
        }
        
        if VerifyUPModel.share.IdType == "01" {
         if VerifyUPModel.share.Nrc.contains(find: ")") {
            let token = VerifyUPModel.share.Nrc.components(separatedBy: ")")
                if token[1].count < 6 {
                    emptyFields = emptyFields + "\n" + "NRC number must be 6 characters".localized
                }else if token[1].count == 0 {
                     emptyFields = emptyFields + "\n" + "NRC number".localized
            }
            }
        }else if VerifyUPModel.share.IdType == "04" {
            if VerifyUPModel.share.PassportCountry == "" {
                emptyFields = emptyFields + "\n" + "Country of Citizen".localized
            }
            
            if VerifyUPModel.share.PassportNumber == "" {
                emptyFields = emptyFields + "\n" + "Passport number".localized
            }else if VerifyUPModel.share.PassportNumber.count < 5 {
                emptyFields = emptyFields + "\n" + "Passport Number must be more than 4 characters".localized
            }
            
            if VerifyUPModel.share.PassportExpiryDate == "" {
                emptyFields = emptyFields + "\n" + "Passport Expiry Date".localized
            }
        }else{
            emptyFields = emptyFields + "\n" + "ID Type".localized
        }
        
        
        if VerifyUPModel.share.DateOfBirth == "" {
            emptyFields = emptyFields + "\n" + "Date of Birth".localized
        }

        if VerifyUPModel.share.Password == "" {
            emptyFields = emptyFields + "\n" + "Password".localized
        }else if VerifyUPModel.share.Password.count < 6 {
            emptyFields = emptyFields + "\n" + "Password must be minimum 6 characters".localized
        }
        
        if securityQuestion != "" {
        if VerifyUPModel.share.SecurityAnswer == "" {
            emptyFields = emptyFields + "\n" + "Security Answer".localized
            }
        }
       
        if emptyFields == "" {
            handler("",true)
        }else  {
            handler(emptyFields, false)
        }
    }

    
    func resetSecurity() {
        tfS_Answer.text = ""
        tfEnterPassword.text = ""
        buttonContriants.constant = 0
//        topConstraintsSSQues.constant = 0
        
    }
    
    @IBAction func btnResetSecurityType(_ sender: Any) {
        
        self.dHeight = 97.0
        resetpasswordtype = "Reset"
        
        userDef.setValue(resetpasswordtype, forKey: "Reset")
        
        if let delegate = self.delegate {
            delegate.resetSecurityType()
        }
        
        tfEnterPassword.isUserInteractionEnabled = true
        btnPattenOnField.isHidden = true
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        self.removeProgressView()
       if screen == "ReVerify"{
            do {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                }
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug(dic)
                        if dic["Msg"] as! String == "Transaction Successful" {
                            println_debug("Navigate to Profile Page and remove verify controller from navigation stack")
                                if let login = dic["Data"] as? String, login != "" {
                                    guard let convertedDic = OKBaseController.convertToDictionary(text: login) else { return }
                                    if let log = convertedDic["AgentDetails"] as? Array<Any> {
                                        if let logi = log.first as? Dictionary<String,Any> {
                                            println_debug(logi)
                                               // ok_password = VerifyUPModel.share.Password
                                              //  userDef.set(VerifyUPModel.share.PasswordType, forKey: "passwordLogin_local")
                                              //  userDef.synchronize()
                                            
                                            if UserDefaults.standard.bool(forKey: "LanguageStatusChanged") {
                                                let perLaguage =  UserDefaults.standard.value(forKey: "PreviousLanguage") as! String
                                                UserDefaults.standard.set(perLaguage, forKey: "currentLanguage")
                                                appDel.setSeletedlocaLizationLanguage(language: perLaguage)
                                                UserDefaults.standard.synchronize()
                                            }
                                            
                                            userDef.set(VerifyUPModel.share.Password, forKey: "passwordLogin_local")
                                            if VerifyUPModel.share.PasswordType == 1 {
                                                userDef.set(true, forKey: "passwordLoginType")
                                            } else {
                                                userDef.set(false, forKey: "passwordLoginType")
                                            }
                                            userDef.synchronize()
                                            ok_password         = userDef.string(forKey: "passwordLogin_local")
                                            ok_password_type    = userDef.bool(forKey: "passwordLoginType")
                                            
                                            profileObj.callLoginApi(aCode: VerifyUPModel.share.MobileNumber, withPassword: ok_password! , handler: { (success) in
                                                if success {
                                            
                                                profileObj.callUpdateProfileApi(aCode: VerifyUPModel.share.MobileNumber, handler: { (success) in
                                                    if success {
                                                        DispatchQueue.main.async {
                                                            
                                                            // Vinnu Added call notification api
                                                            
                                                            let web      = WebApiClass()
                                                            web.delegate = self
                                                            let urlStr   = Url.pushNotificationApi
                                                            let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
                                                            println_debug(ur)
                                                            let params   = self.getParamsForAPN()
                                                            println_debug(params)
                                                            PTLoader.shared.show()
                                                            web.genericClass(url: ur, param: params as AnyObject, httpMethod: "POST", mScreen: "PUSHNOTIFICATION")
                                                            
                                                            let rootNav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "navigationMain")
                                                            rootNav.modalPresentationStyle = .fullScreen
                                                            self.present(rootNav, animated: true, completion: nil)
                                                            userDef.set(true, forKey: keyLoginStatus)
                                                        }
                                                    }else {
                                                        println_debug("callUpdateProfileApi Failed")
                                                    }
                                                })
                                            
                                                    }
                                            })
                                        }
                                    }
                                }
                        }else {
                            DispatchQueue.main.async {
                                alertViewObj.wrapAlert(title: "", body: dic["Msg"] as! String, img:#imageLiteral(resourceName: "r_security_question"))
                                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                    self.APIFailedWebService(failureFor: "Verify")
                                    
                                    self.failureCount = self.failureCount + 1
                                    if self.failureCount == 1 {
                                        let failureViewModel = FailureViewModel()
                                        failureViewModel.sendDataToFailureApi(request: self.paramsFailed as Any, response: dic, type: FailureHelper.FailureType.REVERIFY.rawValue, finished: {
                                        })
                                    }
                                })
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                    }
                }
            } catch {
                
                DispatchQueue.main.async {
                    self.removeProgressView()
                }
//
//                DispatchQueue.main.async {
//                    alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img:#imageLiteral(resourceName: "r_user"))
//                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//                    })
//                    alertViewObj.showAlert(controller: self)
//                }
            }
        }
        
        if screen == "PUSHNOTIFICATION"{
            if let data = json as? Data {
                do {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dataDict = dict as? Dictionary<String,AnyObject> {
                        print("*********************\(dataDict)****************************")
                        if let dic = dataDict["Data"] as? String {
                            
                            println_debug("Push Notification API Response::: \(dic)")
                            
                        }
                    }
                } catch {
                }
            }
        }
        
        if screen == "AddRegistraionFailureTrackInfo" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug(dic["Msg"] as! String)
                        if dic["Code"] as? NSNumber == 200 {
                            println_debug(dic)
                        }else {
                         
                            DispatchQueue.main.async {
                                alertViewObj.wrapAlert(title: "", body: dic["Msg"] as! String, img:#imageLiteral(resourceName: "r_security_question"))
                                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                })
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        self.removeProgressView()
                    }
//                    DispatchQueue.main.async {
//                        alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img:#imageLiteral(resourceName: "r_security_question"))
//                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//                        })
//                        alertViewObj.showAlert(controller: self)
//                    }
                }
            } catch {
                
                DispatchQueue.main.async {
                    self.removeProgressView()
                }
//                DispatchQueue.main.async {
//                    alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img:#imageLiteral(resourceName: "r_security_question"))
//                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//                    })
//                    alertViewObj.showAlert(controller: self)
//                }
            }
        }
        
    }
    
    func getParamsForAPN() -> Dictionary<String, Any> {
        
        var dict = Dictionary<String, Any>()
        
        //{"GcmId":,"MSID":"869045026338991","MobileNumber":"00959264816841","OsType":0,"Password":"123456","SecureToken":"pIWnEmfB3U","SimId":"89950101521679095193"}
        
        dict["GcmId"] = UserDefaults.standard.value(forKey: "deviceToken")
        dict["MSID"] = UserModel.shared.msid
        dict["MobileNumber"] = UserModel.shared.mobileNo
        dict["OsType"] = 1
        dict["Password"] = ok_password ?? ""
        dict["SecureToken"] = UserLogin.shared.token
        dict["SimId"] = uuid
        
        return dict
        
        
    }

    func SubmitVerifyWS () {
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self
            let param = VerifyUPModel.share.wrapData()
            println_debug(param)
            paramsFailed = param as! [String : Any]
            var urlStr   = Url.URL_ReVerify
            urlStr = urlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let url = getUrl(urlStr: urlStr, serverType: .serverApp)
            println_debug(url)
            web.genericClassReg1(url: url, param: param as AnyObject, httpMethod: "POST", mScreen: "ReVerify")
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
        }else{
        
            DispatchQueue.main.async {
                alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img:#imageLiteral(resourceName: "r_user"))
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                })
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    func openSMS() {
        var defaultNumber = [String]()
        if codeObj.code == "+95" {
            defaultNumber.append("+95930000044")
        } else {
            defaultNumber.append("+14809009123")
        }
        let manager  = EncryptionUUID.encryptionmanager() as! EncryptionUUID
        let encryptMsg = manager.entryptNumber(uuid)
        
        #if DEBUG
        //let msg = String.init(format: "Registration Security #%@", encryptMsg ?? "")
       // let msg = String.init(format: "Registration Security #%@#!TEST!#", encryptMsg ?? "")
       //    serverUrl == .productionUrl ? String.init(format: "Registration Security #%@", encryptMsg ?? "") : String.init(format: "Registration Security #%@#!TEST!#", encryptMsg ?? "")
        
        let msg = serverUrl == .productionUrl ? String.init(format: "Registration Security #%@", encryptMsg ?? "") : String.init(format: "Registration Security #%@#!TEST!#", encryptMsg ?? "")
        #else
        let msg = String.init(format: "Registration Security #%@", encryptMsg ?? "")
        
        #endif
        if (MFMessageComposeViewController.canSendText()) {
            let controller        = MFMessageComposeViewController()
            controller.body       = msg
            controller.recipients = defaultNumber
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult){
        
        switch result {
            
        case .sent:
            controller.dismiss(animated: true, completion: nil)
            let time : DispatchTime = DispatchTime.now() + 20.0
            PTLoader.shared.show()
            loader = true
            DispatchQueue.main.async {
                let sb = UIStoryboard(name: "Login", bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: String.init(describing: LoginTimerViewController.self)) as! LoginTimerViewController
                if let keyWindow = UIApplication.shared.keyWindow {
                    vc.view.frame = .init(x: screenWidth/2 - 64, y: screenHeight - 148, width: 128.00, height: 128.00)
                    vc.view.tag = 1473
                    keyWindow.addSubview(vc.view)
                    keyWindow.makeKeyAndVisible()
                    keyWindow.bringSubviewToFront(vc.view)
                }
            }
            DispatchQueue.main.asyncAfter(deadline: time, execute: {
                self.loader = false
                self.SubmitVerifyWS()
            })
            break
        case .cancelled:
            controller.dismiss(animated: true, completion: nil)
            println_debug("remove window")
            break
        case .failed:
            controller.dismiss(animated: true, completion: nil)
            println_debug("popup remove, dismiss")
            break
        }
    }
}

class BaseTextField: UITextField {

// ru, en, ....
var languageCode: String? {

    didSet{

        if self.isFirstResponder{

            self.resignFirstResponder();
            self.becomeFirstResponder();
        }
    }
}

override var textInputMode: UITextInputMode? {

    if let language_code = self.languageCode {

        for keyboard in UITextInputMode.activeInputModes {

            if let language = keyboard.primaryLanguage {

                let locale = Locale.init(identifier: language);
                if locale.languageCode == language_code {

                    return keyboard;
                }
            }
        }
    }

    return super.textInputMode;
}
}
