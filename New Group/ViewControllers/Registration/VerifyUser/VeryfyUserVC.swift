//
//  VeryfyUserVC.swift
//  OK
//
//  Created by SHUBH on 11/22/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class VeryfyUserVC: OKBaseController,UITableViewDataSource,UITableViewDelegate,TakeSelfiVUPDelegate,IDTypeVUPelegate,SecuritQuestionPasswordVUPDelegate,OldVUPBasicDetailDelegate,ReverifyProfilePicDelegate{
   
    
  
    var takeSelfiVUP = TakeSelfiVUP()
    var iDTypeVUP = IDTypeVUP()
    var securitQuestionPasswordVUP = SecuritQuestionPasswordVUP()
    var oldVUPBasicDetail = OldVUPBasicDetail()
    var reverifyprofilepic = ReverifyProfilePicViewController()
    var controllers = [UIViewController]()
    @IBOutlet weak var tableViewVerifyUser: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        removeProgressView()
        self.navigationItem.titleView = nil

        if UserModel.shared.language != "" {
            ok_default_language = UserModel.shared.language
        } else {
            ok_default_language = userDef.value(forKey: "AppRegisteredLanguage") as? String ?? ""
        }
        let currentLanguage = appDel.getSelectedLanguage()
        if ok_default_language != "" {
            if ok_default_language != currentLanguage {
                UserDefaults.standard.set(true, forKey: "LanguageStatusChanged")
                UserDefaults.standard.set(currentLanguage, forKey: "PreviousLanguage")
                UserDefaults.standard.set(ok_default_language, forKey: "currentLanguage")
                appDel.setSeletedlocaLizationLanguage(language: ok_default_language)
                UserDefaults.standard.synchronize()
            }
        }
        
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: "Zawgyi-One", size: 18)
        lblMarque.text = "Verify User".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
        
        reverifyprofilepic = self.storyboard?.instantiateViewController(withIdentifier: "ReverifyProfilePicViewController") as! ReverifyProfilePicViewController
        reverifyprofilepic.view.frame =  CGRect.zero
        reverifyprofilepic.delegate = self
        controllers.append(reverifyprofilepic)
       
        if VillageManager.shared.data.registrationStatus == "-1" {
            // Very old
            oldVUPBasicDetail = self.storyboard?.instantiateViewController(withIdentifier: "OldVUPBasicDetail") as! OldVUPBasicDetail
            oldVUPBasicDetail.view.frame =  CGRect.zero
            oldVUPBasicDetail.delegate = self
            controllers.append(oldVUPBasicDetail)
            
        }else if VillageManager.shared.data.registrationStatus == "1" || VillageManager.shared.data.registrationStatus == "0" {
            // new & old
            takeSelfiVUP = self.storyboard?.instantiateViewController(withIdentifier: "TakeSelfiVUP") as! TakeSelfiVUP
            takeSelfiVUP.view.frame =  CGRect.zero
            takeSelfiVUP.delegate = self
            controllers.append(takeSelfiVUP)
        }else {
            alertViewObj.wrapAlert(title:"", body:"Network error Please try again", img:#imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK", style: .target , action: {
                self.navigationController?.popViewController(animated: true)
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
        tableViewVerifyUser.delegate = self
        tableViewVerifyUser.dataSource = self
    }

    @IBAction func backAction(_ sender: Any) {
        
        
        alertViewObj.wrapAlert(title: "", body: "If you click back button, you have entered data will be automatically cleared and re-direct to OK $  user login screen.".localized, img:#imageLiteral(resourceName: "r_user"))
        alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
        })
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            
            VerifyUPModel.share.ProfilePicAwsUrl = ""
            if UserDefaults.standard.bool(forKey: "LanguageStatusChanged") {
                let perLaguage =  UserDefaults.standard.value(forKey: "PreviousLanguage") as! String
                UserDefaults.standard.set(perLaguage, forKey: "currentLanguage")
                appDel.setSeletedlocaLizationLanguage(language: perLaguage)
                UserDefaults.standard.set(false, forKey: "LanguageStatusChanged")
                UserDefaults.standard.synchronize()
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowLoginBothViews"), object: nil)
            self.dismiss(animated: false, completion: nil)
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return controllers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = tableViewVerifyUser.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let vc = controllers[indexPath.row]
        addChild(vc)
        vc.view.frame = cell.bounds
        vc.didMove(toParent: self)
        vc.view.layoutSubviews()
        vc.view.layoutIfNeeded()
        cell.addSubview(vc.view)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = controllers[indexPath.row].dHeight {
            if controllers[indexPath.row] == oldVUPBasicDetail {
                   let vc = controllers[indexPath.row]
                            vc.view.frame = .init(x: 0, y: 0, width: vc.view.frame.width, height: height)
                            vc.view.layoutSubviews()
                            vc.view.layoutIfNeeded()
            }
            return height
        }
        return 0.00
    }
    
    func UICellUpdate() {
        
        UIView.setAnimationsEnabled(false)
        self.tableViewVerifyUser.beginUpdates()
        self.tableViewVerifyUser.layoutIfNeeded()
        self.tableViewVerifyUser.layoutSubviews()
        self.tableViewVerifyUser.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
    func updateMainUIUI() {
        self.UICellUpdate()
    }
    
    func showProfilePhotoVC() {
        let sb = UIStoryboard(name: "MyProfile", bundle: nil)
        let cameraVC = sb.instantiateViewController(withIdentifier: "CameraVCUP") as! CameraVCUP
        cameraVC.delegate = reverifyprofilepic
        self.navigationController?.pushViewController(cameraVC, animated: true)

    }
    
    func profilePhotoConfirm() {
        self.UICellUpdate()
    }
    
    
//    func ShowIDTypeVC(controller: TakeSelfiVUP) {
//       if !controllers.contains(reverifyprofilepic) {
//        reverifyprofilepic = self.storyboard?.instantiateViewController(withIdentifier: "ReverifyProfilePicViewController") as! ReverifyProfilePicViewController
//            reverifyprofilepic.delegate = self
//           controllers.append(reverifyprofilepic)
//           DispatchQueue.main.async {
//               self.tableViewVerifyUser.reloadData()
//
//               if let cellAtFirst = self.tableViewVerifyUser.visibleCells.first {
//                   cellAtFirst.subviews.forEach({ (view) in
//                       if view is UITextField {
//                           view.becomeFirstResponder()
//                       } else if let tf = controller.tfUserName {
//                           tf.becomeFirstResponder()
//                       }
//                   })
//               }
//           }
//       }
//   }
    
     func ShowIDTypeVC(controller: TakeSelfiVUP) {
        if !controllers.contains(iDTypeVUP) {
            iDTypeVUP = self.storyboard?.instantiateViewController(withIdentifier: "IDTypeVUP") as! IDTypeVUP
            iDTypeVUP.delegate = self
            controllers.append(iDTypeVUP)
            DispatchQueue.main.async {
                self.tableViewVerifyUser.reloadData()

                if let cellAtFirst = self.tableViewVerifyUser.visibleCells.first {
                    cellAtFirst.subviews.forEach({ (view) in
                        if view is UITextField {
                            view.becomeFirstResponder()
                        } else if let tf = controller.tfUserName {
                            tf.becomeFirstResponder()
                        }
                    })
                }
            }
        }
    }
    
    func showNRCTypeOption() {
        let sb = UIStoryboard(name: "Registration" , bundle: nil)
        let nRCTypesVC  = sb.instantiateViewController(withIdentifier: "NRCTypesVC") as! NRCTypesVC
        nRCTypesVC.modalPresentationStyle = .overCurrentContext
        nRCTypesVC.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        nRCTypesVC.setName(nrcName: (iDTypeVUP.btnnrc.titleLabel?.text)!)
        nRCTypesVC.nRCTypesVCDelegate = iDTypeVUP
        self.present(nRCTypesVC, animated: false, completion: nil)
    }

    func resetIDProofDetails() {
    controllers.remove(at: 2)
        iDTypeVUP = self.storyboard?.instantiateViewController(withIdentifier: "IDTypeVUP") as! IDTypeVUP
        iDTypeVUP.delegate = self
        controllers.insert(iDTypeVUP, at: 2)
        DispatchQueue.main.async {
            self.tableViewVerifyUser.reloadData()
        }
    }
 
    func showPasswordSecurity(){
        if !controllers.contains(securitQuestionPasswordVUP) {
            securitQuestionPasswordVUP = self.storyboard?.instantiateViewController(withIdentifier: "SecuritQuestionPasswordVUP") as! SecuritQuestionPasswordVUP
            securitQuestionPasswordVUP.delegate = self
            controllers.append(securitQuestionPasswordVUP)
            DispatchQueue.main.async {
                self.tableViewVerifyUser.reloadData()
            }
        }
    }
    
    func resetSecurityType() {
        
        controllers.remove(at: 3)
        securitQuestionPasswordVUP = self.storyboard?.instantiateViewController(withIdentifier: "SecuritQuestionPasswordVUP") as! SecuritQuestionPasswordVUP
        securitQuestionPasswordVUP.delegate = self
        controllers.insert(securitQuestionPasswordVUP, at: 3)
        
        if VillageManager.shared.data.registrationStatus == "1" {
        }else {
//            controllers.remove(at: 1)
//            securitQuestionPasswordVUP = self.storyboard?.instantiateViewController(withIdentifier: "SecuritQuestionPasswordVUP") as! SecuritQuestionPasswordVUP
//            securitQuestionPasswordVUP.delegate = self
//            controllers.insert(securitQuestionPasswordVUP, at: 1)
        }

        DispatchQueue.main.async {
            self.tableViewVerifyUser.reloadData()
        }
        scrollToBottom()
    }
    
   func removeSecurityView(removeViews : String) {
//
//        if removeViews == "security" {
//               controllers.remove(at: 2)
//            DispatchQueue.main.async {
//                self.tableViewVerifyUser.reloadData()
//            }
//        }else if removeViews == "idType" {
//            controllers.remove(at: 1)
//            DispatchQueue.main.async {
//                self.tableViewVerifyUser.reloadData()
//            }
//        }
//
    }
   func resignKeyboard(){
    self.view.endEditing(true)
    }
    
    
    
    //Basic Detailw FP Delegate
    func showTownShip() {
        let sb = UIStoryboard(name: "Registration", bundle: nil)
        let townVC = sb.instantiateViewController(withIdentifier: "TownshipVC") as! TownshipVC
        townVC.townShipDelegate = iDTypeVUP
        self.navigationController?.pushViewController(townVC, animated: false)
    }
    func showCountry(){
        let sb = UIStoryboard(name: "Country", bundle: nil)
        let countryVC = sb.instantiateViewController(withIdentifier: "CountryViewController") as! CountryViewController
        countryVC.delegate = iDTypeVUP
        countryVC.modalPresentationStyle = .fullScreen
        self.navigationController?.present(countryVC, animated: false, completion: nil)
    }
    
   func patternPasswordVC() {
    let sb = UIStoryboard(name: "Registration" , bundle: nil)
    let passwordPatternViewController  = sb.instantiateViewController(withIdentifier: "PasswordPatternViewControllerR") as! PasswordPatternViewControllerR
    passwordPatternViewController.delegate = securitQuestionPasswordVUP
    passwordPatternViewController.checkVC = "VUP"
    passwordPatternViewController.mobilNumber =  VerifyUPModel.share.MobileNumber
    passwordPatternViewController.modalPresentationStyle = .fullScreen
    self.navigationController?.present(passwordPatternViewController, animated: false, completion: nil)
    }

    func scrollToBottom() {
        let indexPath = IndexPath(row: controllers.count - 1, section: 0)
        tableViewVerifyUser.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.middle, animated: true)
    }
}
