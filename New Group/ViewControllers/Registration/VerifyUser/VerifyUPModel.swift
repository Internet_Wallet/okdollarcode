//
//  VerifyUPModel.swift
//  OK
//
//  Created by Subhash Arya on 20/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class VerifyUPModel: NSObject {
    
    static let share = VerifyUPModel()
    
    var LoginPassword : String = ""
    var LoginPasswordType : String = ""
    var MobileNumber      :      String = ""
    var Name      :      String = ""
    var IdType      :      String = ""
    var Nrc      :      String = ""
    var PassportCountry      :      String = ""
    var PassportNumber      :      String = ""
    var PassportExpiryDate      :      String = ""
    var DateOfBirth      :      String = ""
    var FatherName      :      String = ""
    var PasswordType      :      Int = 0
    var Password      :      String = ""
    var SecurityAnswer      :      String = ""
    var DrowCurrentPatternTag   : String = ""

    var CellId      :      String = ""
    var HiddenSSID      = 1
    var SIMCountryIso      :      String = ""
    var NetworkSignal    = 1
    var LinkSpeed      = 1
    var isNetworkRoaming   = 1
    var VoiceMailNo      :      String = ""
    var IosOtp      :      String = uuid
    var ConnectedNetworkType      :      String = ""
    var NetworkOperatorName      :      String = "" //Ooredoo
    var BSSID      :      String = ""
    var AuthCode      :      String = ""
    var BluetoothAddress      :      String = ""
    var BluetoothName      :      String = ""
    var ReRegisterOtp      :      String = "" //123456
    var NetworkCountryIso      :      String = ""
    var SSID      :      String = ""
    var Msid      :      String = msid //05
    var IPAddress      :      String = ""
    var Longitude      :      Double = 0.00000
    var AppID      :      String = "com.cgm.OKDollar"
    var NetworkOperator      :      String = "" //Ooredoo Myanmar
    var SIMOperator      :      String = ""
    var SIMOperatorName      :      String = ""
    var MACAddress          : String = ""
    var AuthCodeStatus    = 0
    var Latitude      :      Double = 0.00000
    var NetworkType      :      String = ""
    var NetworkID      = 1
    var SimId      :      String = uuid
    var DeviceSoftwareVersion      :      String = UIDevice.current.systemVersion
    var OsType      = 1
    var ProfilePic :   String = "" //base 64 string
    var ProfilePicAwsUrl : String = ""
    var CheckRegistration : String = ""
  //  geoLocManager.currentLongitude ?? "0.0",
    func wrapData() -> NSDictionary {
        var languageType = ""
        if ok_default_language == "my" {
            languageType = "0"
        }else if ok_default_language == "uni" {
            languageType = "1"
        }
        else
        {
            languageType = "2"
        }
        
        let dict : NSDictionary = ["MobileNumber":MobileNumber,"Name":Name,"IdType":IdType,"Nrc":Nrc,"PassportCountry":PassportCountry,"PassportNumber":PassportNumber,"PassportExpiryDate":PassportExpiryDate,"DateOfBirth":DateOfBirth,"FatherName":FatherName,"PasswordType":PasswordType,"Password":Password,"SecurityAnswer":SecurityAnswer,"CellId":CellId,"HiddenSSID":HiddenSSID,"SIMCountryIso":SIMCountryIso,"NetworkSignal":NetworkSignal,"LinkSpeed":LinkSpeed,"isNetworkRoaming":isNetworkRoaming,"VoiceMailNo":VoiceMailNo,"IosOtp":IosOtp,"ConnectedNetworkType":ConnectedNetworkType,"NetworkOperatorName":NetworkOperatorName,"BSSID":BSSID,"AuthCode":AuthCode,"BluetoothAddress":BluetoothAddress,"BluetoothName":BluetoothName,"ReRegisterOtp":ReRegisterOtp,"NetworkCountryIso":NetworkCountryIso,"SSID":SSID,"Msid":Msid,"IPAddress":IPAddress,"Longitude":geoLocManager.currentLongitude ?? "0.0","AppId":AppID,"NetworkOperator":NetworkOperator,"SIMOperator":SIMOperator,"SIMOperatorName":SIMOperatorName,"MACAddress":MACAddress,"AuthCodeStatus":AuthCodeStatus,"Latitude":geoLocManager.currentLatitude ?? "0.0" ,"NetworkType":NetworkType,"NetworkID":NetworkID,"SimId":SimId,"DeviceSoftwareVersion":DeviceSoftwareVersion,"OsType":OsType,"TransactionsCellTower": NetworlInfo(),"FontType": languageType,"AndroidVersion":"","QuestionCode":"","SimCardPos":"","SimID2":"","LoginIdentityType":0,"LoginAppKey":"","ProfilePicImgUrl":ProfilePicAwsUrl,"IsRoaming":false,"HasSimId":true,"AppVersion":buildVersion,"LoginAnalysis":"","GoogleTokenFor":"","SimId2":"","Msid2":"","WifiDetails":"", "DivisionCode":"", "TownshipCode":"", "ProfilePic": ProfilePic, "DummyDeviceLockCheck":0, "DeviceInformation":DeviceInfo()]
         return dict
    }
    
    func NetworlInfo() -> NSDictionary {
        
        var wifiName = ""
        var ssid = ""
        var mac = ""
        if UitilityClass.isConnectedToWifi(){
            
            let wifiInfo = UitilityClass.getNetworkInfos()[0].components(separatedBy: ",")
            wifiName = wifiInfo[0]
            ssid = wifiInfo[0]
            mac = wifiInfo[1]
        }
           let dic : NSDictionary =  ["ConnectedwifiName" : wifiName,"Lac": "","Mac": mac,"Mcc": mccStatus.mcc,"Mnc": mccStatus.mnc,"NetworkType": UitilityClass.getNetworkType(),"SignalStrength": "","Ssid": ssid, "NetworkSpeed": UitilityClass.getSignalStrength()]
        return dic
    }
    
    func DeviceInfo() -> NSDictionary {
        var wifiName = ""
        var ssid = ""
        var mac = ""
        if UitilityClass.isConnectedToWifi(){
            
            let wifiInfo = UitilityClass.getNetworkInfos()[0].components(separatedBy: ",")
            wifiName = wifiInfo[0]
            ssid = wifiInfo[0]
            mac = wifiInfo[1]
        }
        
        
        let dic : NSDictionary = ["DeviceSoftwareVersion" : DeviceSoftwareVersion, "NetworkCountryIso" : NetworkCountryIso, "NetworkOperator":Msid,"NetworkOperatorName":NetworkOperatorName,"NetworkType":UitilityClass.getNetworkType(),"PhoneType":"GSM","SIMCountryIso":SIMCountryIso,"SIMOperator":SIMOperator,"SIMOperatorName":SIMOperatorName,"VoiceMailNo":VoiceMailNo,"isNetworkRoaming":false,"BSSID":BSSID,"MACAddress":"","HiddenSSID":true,"IPAddress":IPAddress,"LinkSpeed":LinkSpeed,"NetworkID":NetworkID,"SSID":ssid,"NetworkSignal":NetworkSignal,"BluetoothAddress":BluetoothAddress,"BluetoothName":BluetoothName,"ConnectedNetworkType":ConnectedNetworkType]
        
        return dic
    }
}

