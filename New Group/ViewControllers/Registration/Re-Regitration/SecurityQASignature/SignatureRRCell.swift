//
//  SignatureRRCell.swift
//  OK
//
//  Created by Imac on 6/3/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol SignatureRRCellDelegate {
    func presentSignatureVC()
}

class SignatureRRCell: UITableViewCell {
    
    var delegate: SignatureRRCellDelegate?
    @IBOutlet weak var imgreturn: UIImageView!
    @IBOutlet var imgSign: UIImageView!
    @IBOutlet weak var lblHeader: UILabel!{
        didSet{
            lblHeader.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblDescription: UILabel!{
        didSet{
            lblDescription.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnReset: UIButton!{
        didSet{
            btnReset.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    var isNextViewHidden = true
    let gifManager = SwiftyGifManager(memoryLimit:1)
    override func awakeFromNib() {
        super.awakeFromNib()
        imgreturn.isHidden = true
        ReRegistrationModel.shared.signature = ""
        self.imgSign.gifImage = nil
        let gif = UIImage(gifName: "sign_demo")
        self.imgSign.setGifImage(gif, manager: gifManager)
    }
    func updateLaguageCell() {
        lblHeader.attributedText = lblHeader.attributedStringFinal(str1: "Signature".localized)
        lblDescription.text = "Tap here to sign please use your index finger to draw signature".localized
        
        if ReRegistrationModel.shared.Language.lowercased() == "en"{
            btnReset.setBackgroundImage(UIImage(named: "reset_blue"), for: .normal)
        }else if ReRegistrationModel.shared.Language.lowercased() == "my" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
        }else  {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
        }
    }
    
    @IBAction func btnSignAction(_ sender: Any) {
        if let del = delegate {
            del.presentSignatureVC()
        }
    }
    
    @IBAction func btnResetAction(_ sender: Any) {
        imgreturn.isHidden = true
        ReRegistrationModel.shared.signature = ""
        self.imgSign.gifImage = nil
        let gif = UIImage(gifName: "sign_demo")
        self.imgSign.setGifImage(gif, manager: gifManager)
    }

}
