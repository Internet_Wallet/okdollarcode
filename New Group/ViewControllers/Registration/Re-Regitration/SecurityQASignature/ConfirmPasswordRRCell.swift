//
//  ConfirmPasswordRRCell.swift
//  OK
//
//  Created by Imac on 6/9/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
protocol ConfirmPasswordRRCellDelegate {
    func showAlertForPasswordInfor(txt : String)
    func showSecurityQuestionField()
}
class ConfirmPasswordRRCell: UITableViewCell,UITextFieldDelegate {
    var delegate: ConfirmPasswordRRCellDelegate?
    
    @IBOutlet weak var tfConfirmPassword: UITextField!{
        didSet{
            tfConfirmPassword.font = UIFont(name: appFont, size: 18.0)
        }
    }
    @IBOutlet weak var btnConfirmEye: UIButton!
    var isNextViewHidden = true
    let validObj = PayToValidations()
    override func awakeFromNib() {
        super.awakeFromNib()
        tfConfirmPassword.delegate = self
        
        var font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
            font = UIFont.systemFont(ofSize: 18)
        }
        tfConfirmPassword.font = font
        
        if ReRegistrationModel.shared.Language.lowercased() == "en"{
            tfConfirmPassword.font = UIFont(name: appFont, size: 18)
        }else if ReRegistrationModel.shared.Language.lowercased() == "my" {
            tfConfirmPassword.font = UIFont(name: appFont, size: 18)
        }else {
            
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfConfirmPassword.attributedPlaceholder = NSAttributedString(string: "Confirm Password", attributes:attributes as [NSAttributedString.Key : Any])
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfConfirmPassword.attributedPlaceholder = NSAttributedString(string: "Confirm Password", attributes:attributes)
                tfConfirmPassword.font = UIFont.systemFont(ofSize: 18)
            }
        }
 
         tfConfirmPassword.placeholder = "Confirm Password".localized
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        ReRegistrationModel.shared.ConfirmPassword = tfConfirmPassword.text!
        if tfConfirmPassword.text!.count < ReRegistrationModel.shared.Password.count {
            if let del = delegate {
                del.showAlertForPasswordInfor(txt:  "Password and Confirm Password does not matched".localized)
            }
        }
        return true
    }
    
    @IBAction func editchanged(_ sender: UITextField) {

    }
    
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if range.location == 0 && string == " " {
            return false
        }
        
        if validObj.checkMatchingNumber(string: text, withString: ReRegistrationModel.shared.Password) {
            self.tfConfirmPassword.text = text
            if text == ReRegistrationModel.shared.Password {
                tfConfirmPassword.resignFirstResponder()
                if isNextViewHidden {
                    isNextViewHidden = true
                    if let del = delegate {
                        del.showSecurityQuestionField()
                    }
                }
            }
            return false
        } else {  return false  }
    }
    
    @IBAction func btnShowConfirmPassword(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            tfConfirmPassword.isSecureTextEntry = false
            btnConfirmEye.setImage(#imageLiteral(resourceName: "r_hide"), for: .normal)
        }else {
            tfConfirmPassword.isSecureTextEntry = true
            btnConfirmEye.setImage(#imageLiteral(resourceName: "r_show"), for: .normal)
        }
    }
    
}
