//
//  SecurityAnswerRRcell.swift
//  OK
//
//  Created by Imac on 6/3/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
protocol SecurityAnswerRRcellDelegate {
    func showSignatureView()
}

class SecurityAnswerRRcell: UITableViewCell,UITextFieldDelegate {
    var delegate: SecurityAnswerRRcellDelegate?
    @IBOutlet weak var tfAnswer: UITextField!{
        didSet{
            tfAnswer.font = UIFont(name: appFont, size: 18.0)
        }
    }
    var isNextViewHidden = true
    var ANSWERCHARSET = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        tfAnswer.delegate = self
    }
    
    func updateLaguageCell() {
        var font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
            font = UIFont.systemFont(ofSize: 18)
        }
        tfAnswer.font = font
        
        
        tfAnswer.text = ReRegistrationModel.shared.SecurityQuestionAnswer
        if ReRegistrationModel.shared.Language.lowercased() == "en"{
            ANSWERCHARSET = STREETNAME_CHAR_SET_En
            tfAnswer.font = UIFont.systemFont(ofSize: 18)
        }else if ReRegistrationModel.shared.Language.lowercased() == "my" {
            ANSWERCHARSET = STREETNAME_CHAR_SET_My
            tfAnswer.font = UIFont(name: appFont, size: 18)
        }else {
            ANSWERCHARSET = STREETNAME_CHAR_SET_Uni
            
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfAnswer.attributedPlaceholder = NSAttributedString(string: "Enter Answer", attributes:attributes as [NSAttributedString.Key : Any])
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfAnswer.attributedPlaceholder = NSAttributedString(string: "Enter Answer", attributes:attributes)
                tfAnswer.font = UIFont.systemFont(ofSize: 18)
            }
        }
          tfAnswer.placeholder = "Enter Answer".localized
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if tfAnswer.text!.count > 0 {
            if isNextViewHidden{
                if let del = delegate {
                    del.showSignatureView()
                     isNextViewHidden = false
                }
            }
        }
        removeSpaceAtLast(txtField:tfAnswer)
        ReRegistrationModel.shared.SecurityQuestionAnswer = tfAnswer.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        return true
    }
    
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if range.location == 0 && string == " " {
            return false
        }
        
        if ReRegistrationModel.shared.SecurityQuestionCode == "" {
            return false
        }
        
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: ANSWERCHARSET).inverted).joined(separator: "")) { return false }
        if text.count > 50 {
            return false
        }
        if restrictMultipleSpaces(str: string, textField: tfAnswer) {
            return true
        }else {
            if  textField.text?.last == " " {
                textField.text?.removeLast()
            }
            return false
        }
    }
    
    func removeSpaceAtLast(txtField : UITextField){
        if txtField.text?.last == " " {
            txtField.text?.removeLast()
        }
    }
    
   override var textInputMode: UITextInputMode? {
        let language = ReRegistrationModel.shared.Language.lowercased()
        if language.isEmpty {
            return super.textInputMode
        } else {
            for tim in UITextInputMode.activeInputModes {
                if tim.primaryLanguage!.contains(language) {
                    return tim
                }
            }
            return super.textInputMode
        }
    }
    
}
