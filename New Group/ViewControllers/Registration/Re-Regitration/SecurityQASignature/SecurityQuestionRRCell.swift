//
//  SecurityQuestionRRCell.swift
//  OK
//
//  Created by Imac on 6/3/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
protocol SecurityQuestionRRCellDelegate {
    func showSecurityQuestionView()
}


class SecurityQuestionRRCell: UITableViewCell {
    var delegate: SecurityQuestionRRCellDelegate?
    @IBOutlet weak var lblSecurityHeader: UILabel!{
        didSet{
            lblSecurityHeader.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblQuestion: MarqueeLabel!{
        didSet{
            lblQuestion.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnSecuritQuestion: UIButton!{
        didSet{
            btnSecuritQuestion.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    var isNextViewHidden = true
    override func awakeFromNib() {
        super.awakeFromNib()
      
    }
    
    func updateLaguageCell(text : String) {
        if text == "" {
            lblQuestion.text = "Select Security Question".localized
        }else {
            lblQuestion.text = text
        }
        lblSecurityHeader.attributedText = lblSecurityHeader.attributedStringFinal(str1: "Select Security Question".localized)
        
    }
    
    @IBAction func btnSecuritQuestionAction(_ sender: Any) {
        if let del = delegate {
            del.showSecurityQuestionView()
        }
    }
    
}
