//
//  PasswordHeaderRRCell.swift
//  OK
//
//  Created by Imac on 6/9/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol  PasswordHeaderRRCellDelegate {
    func showPatternVC()
    func showPasspwordView(isHidden : Bool)
    func showAlertForPasswordInfor(txt : String)
    func showDemoSecurityPassword()
    func resetSecurityType()
}

class PasswordHeaderRRCell: UITableViewCell {
    var delegate: PasswordHeaderRRCellDelegate?
    @IBOutlet weak var lblHeader: MarqueeLabel!{
        didSet{
            lblHeader.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnReset: UIButton!{
        didSet{
            btnReset.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var imgPattern: UIImageView!
    @IBOutlet weak var imgPassword: UIImageView!
    @IBOutlet weak var lblPassword: UILabel!{
        didSet{
            lblPassword.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    var isNextViewHidden = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func updateLaguageCell() {
        lblHeader.attributedText = lblHeader.attributedStringFinal(str1: "Select Security Type".localized)
        if ReRegistrationModel.shared.Language.lowercased() == "en" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue"), for: .normal)
        }else if ReRegistrationModel.shared.Language == "my" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
        }else {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
        }
        lblPassword.text = "Password".localized
    }
    
    
    @IBAction func btnPasswordAction(_ sender: Any) {
        if !(imgPassword.image == UIImage(named:"select_radio")) {
            isNextViewHidden = true
            ReRegistrationModel.shared.Password = ""
            ReRegistrationModel.shared.PasswordType = "0"
            imgPassword.image = UIImage(named:"select_radio")
            imgPattern.image = UIImage(named:"r_Unradio")
            if let del = delegate {
                del.showAlertForPasswordInfor(txt: "Password must be atleast 6 digits. It should not be same with your mobile number or name and also case-sensitive".localized)
            }
            if isNextViewHidden {
                if let del = delegate {
                    del.showPasspwordView(isHidden: true)
                }
                isNextViewHidden = false
            }else {
                if let del = delegate {
                    del.showPasspwordView(isHidden: false)
                }
            }
       
        }
    }
    
    @IBAction func btnPatternAction(_ sender: Any) {
        
        if !(imgPattern.image == UIImage(named:"select_radio")) || !(imgPattern.image == UIImage(named:"r_Unradio")) {
            if let del = self.delegate {
                del.showPatternVC()
            }
        }
    }
    
    @IBAction func onClickResetAction(_ sender: Any) {
        if imgPattern.image == UIImage(named:"select_radio") || imgPassword.image == UIImage(named:"select_radio") || ReRegistrationModel.shared.SecurityQuestionAnswer != "" || ReRegistrationModel.shared.SecurityQuestionCode != "" {
                if let del = delegate {
                    del.resetSecurityType()
                }
        }
      
    }
    
    
    @IBAction func onClickIDTypeInfoAction(_ sender: Any) {
        if let del = delegate {
            del.showDemoSecurityPassword()
        }
    }

}
