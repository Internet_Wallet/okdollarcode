//
//  PasswordRRCell.swift
//  OK
//
//  Created by Imac on 6/9/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
protocol PasswordRRCellDelegate {
    func passwordEdit(txt : String)
    func showAlertForPasswordInfor(txt : String)
}

class PasswordRRCell: UITableViewCell,UITextFieldDelegate {
    var delegate: PasswordRRCellDelegate?
    @IBOutlet weak var tfPassword: UITextField!{
        didSet{
            tfPassword.font = UIFont(name: appFont, size: 18.0)
        }
    }
    @IBOutlet weak var lblPasswordMarque: MarqueeLabel!{
        didSet{
            lblPasswordMarque.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnEyes: UIButton!
    var isNextViewHidden = true
    var VALIDCHAR = "1234567890"
    var updatestring = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
         lblPasswordMarque.attributedText = lblPasswordMarque.scrollMarqueLabel(title: "Create Password(Min 6 digits)".localized)
        tfPassword.delegate = self
        tfPassword.isSecureTextEntry = true
    }
    
    func updateLaguageCell() {
        if tfPassword.text == "" {
            lblPasswordMarque.text = "Create Password(Min 6 digits)".localized
        }else{
           lblPasswordMarque.text = ""
        }
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.clearsOnBeginEditing = false
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.clearsOnBeginEditing = false
       
    }
    
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
            ReRegistrationModel.shared.Password = tfPassword.text!
            if tfPassword.text!.count < 6 && tfPassword.text!.count > 0 {
                //      showAlert(alertTitle: "", alertBody: "Password must be at least 6 characters".localized, alertImage: #imageLiteral(resourceName: "password"))
            }
       
        return true
    }

    @IBAction func editChanged(_ sender: UITextField) {
        if let del = delegate {
            del.passwordEdit(txt : updatestring)
        }
    }
    
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if range.location == 0 && string == " " {
            return false
        }
    
        updatestring = text
        
        if textField == tfPassword {
            //tfConfirmPassword.text = ""
            ReRegistrationModel.shared.ConfirmPassword = ""
            ReRegistrationModel.shared.Password = ""
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: PASSWORD_CHAR_SET).inverted).joined(separator: "")) { return false }
            
            if text.count > 1 {
                if (string == string.components(separatedBy: NSCharacterSet(charactersIn: VALIDCHAR).inverted).joined(separator: "")) {
                    let last = text.index(text.endIndex, offsetBy: -1)
                    let secondLast = text.index(text.endIndex, offsetBy: -2)
                    if text[last] == text[secondLast] {
                        if string != "" {
                            tfPassword.text = ""
                            updatestring = ""
                            lblPasswordMarque.text = "Create Password(Min 6 digits)".localized
                            if let del = delegate {
                                del.showAlertForPasswordInfor(txt: "Please enter valid password. Password like 111111, 112233 and your mobile number is not allowed for security reasons".localized)
                            }
                            return false
                        }
                    }
                }
            }
             if text.count > 0 { btnEyes.isHidden = false }else{ btnEyes.isHidden = true }
            let str = ReRegistrationModel.shared.MobileNumber
            let startIndex = str.index(str.startIndex, offsetBy: 4)
            if text == String(str[startIndex...]) {
                tfPassword.text = ""
                updatestring = ""
                lblPasswordMarque.text = "Create Password(Min 6 digits)".localized
                if let del = delegate {
                    del.showAlertForPasswordInfor(txt: "Please enter valid password. Password like 111111, 112233 and your mobile number is not allowed for security reasons".localized)
                }
                return false
            }
            if text.count > 30 {
                return false
            }
        }
        return true
    }
    
    @IBAction func btnShowConfirmPassword(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            tfPassword.isSecureTextEntry = false
            btnEyes.setImage(#imageLiteral(resourceName: "r_hide"), for: .normal)
        }else {
            tfPassword.isSecureTextEntry = true
            btnEyes.setImage(#imageLiteral(resourceName: "r_show"), for: .normal)
        }
    }
    
    
    
    
    
    

}
