//
//  PersonalInfoExtensioni.swift
//  OK
//
//  Created by Imac on 5/30/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

extension ReRegistrationVC: BirthDateRRCellDelegate,NameRRCellDelegate,TakeProfileImageRRCellDelegate,CameraVCUPDelegate,ProfileImageCellRRDelegate,FatherNameRRCellDelegate,PersonalInfoRRCellDelegate,AccountTypeRRCellDelegate,EmailRRCellDelegate {

    

    func showDOBField() {
//        var emptyFields = ""
//        if ReRegistrationModel.shared.Name == "" {
//            emptyFields = "User Name".localized
//
//            showAlert(alertTitle: "Please Fill All Mandatory Fields".localized, alertBody: emptyFields, alertImage: #imageLiteral(resourceName: "alert-icon"))
//
//        }else if ReRegistrationModel.shared.Name.contains(find: ",") {
//            let token = ReRegistrationModel.shared.Name.components(separatedBy: ",")
//            if token[1].count < 3 {
//                emptyFields = emptyFields + "\n" + "User Name must be more than 2 characters".localized
//                showAlert(alertTitle: "Please Fill All Mandatory Fields".localized, alertBody: emptyFields, alertImage: #imageLiteral(resourceName: "alert-icon"))
//            }
//        }
        personalInfoRowCount = 2
        insertRow(index: 1, withSeciton: 1)
    }
    func showTakeUserImage() {
            personalInfoRowCount = 3
            insertRow(index: 2, withSeciton: 1)
    }
    func navigateToTakeImage() {
        let sb = UIStoryboard(name: "MyProfile", bundle: nil)
        let cameraVC = sb.instantiateViewController(withIdentifier: "CameraVCUP") as! CameraVCUP
        cameraVC.delegate = self
        self.navigationController?.pushViewController(cameraVC, animated: true)

    }
    func updateProflePic(imageUrl: String, realImage: UIImage) {
        ReRegistrationModel.shared.ProfilePicAwsUrl = imageUrl
        self.profilImage = realImage
        
        //cell.capturedImage.image = realImage
        if !self.userImageAvaiable {
            self.userImageAvaiable = true
            if let _ = self.tableViewRagi.cellForRow(at: IndexPath(row: 3, section: 1)) as? FatherNameRRCell {
                if let _ = self.tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 1)) as? TakeProfileImageRRCell {
                    self.reloadRows(index: 2, withSeciton: 1)
                }
            }else {
                if self.personalInfoRowCount != 5 {
                    self.personalInfoRowCount = 4
                    self.insertRow(index: 3, withSeciton: 1)
                }
            }
         
        }
        if let _ = self.tableViewRagi.cellForRow(at: IndexPath(row: 0, section: 0)) as? ProfileImageCellRR {
            self.reloadRows(index: 0, withSeciton: 0)
        }
        
    }
    
    func showEmailID() {
        personalInfoRowCount = 5
        insertRow(index: 4, withSeciton: 1)
        
        if self.tableViewRagi.cellForRow(at: IndexPath(row: 0, section: 2)) as? IDTypeHeaderRRCell == nil {
            idTyperRowCount = 1
            insertRow(index: 0, withSeciton: 2)
        }
        if self.tableViewRagi.cellForRow(at: IndexPath(row: 1, section: 2)) as? SelectIDTypeRRCell == nil {
            idTyperRowCount = 2
            insertRow(index: 1, withSeciton: 2)
        }
    }
    
    func showIDType() {
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 4, section: 1)) as? EmailRRCell   {
            if cell.isNextViewHidden {
                cell.isNextViewHidden = false

                if self.tableViewRagi.cellForRow(at: IndexPath(row: 0, section: 2)) as? IDTypeHeaderRRCell == nil {
                    idTyperRowCount = 1
                    insertRow(index: 0, withSeciton: 2)
                }
                if self.tableViewRagi.cellForRow(at: IndexPath(row: 1, section: 2)) as? SelectIDTypeRRCell == nil {
                    idTyperRowCount = 2
                    insertRow(index: 1, withSeciton: 2)
                }
            }
        }
        
    }
    
    func resetPersonalInfo() {
        self.userImageAvaiable =  false
        self.reloadRows(index: 0, withSeciton: 0)
    
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 0, section: 1)) as? NameRRCell {
             ReRegistrationModel.shared.Name = ""
            let btn = UIButton()
            cell.onClickUserNameClose(btn)
        }
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 1, section: 1)) as? BirthDateRRCell {
             ReRegistrationModel.shared.DateOfBirth = ""
            cell.tfDOB.text = ""
        }
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 1)) as? TakeProfileImageRRCell {
            ReRegistrationModel.shared.ProfilePicAwsUrl = ""
            println_debug(cell)
            self.reloadRows(index: 2, withSeciton: 1)
        }
        
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 3, section: 1)) as? FatherNameRRCell {
               ReRegistrationModel.shared.Father = ""
            let btn = UIButton()
            cell.btnFatherNameResetAction(btn)
        }
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 4, section: 1)) as? EmailRRCell {
               ReRegistrationModel.shared.EmailId = ""
            cell.tfEmailID.text = ""
            cell.btnMailIDClear.isHidden = true
        }
    }
    
    func showDemo(strURL : String, imgDemo : UIImage, demoTitile:String){
        self.view.endEditing(true)
        webviewContainer.isHidden = false
        webview.scalesPageToFit = true
        webview.delegate = self
        lblDemo.text = demoTitile.localized
        webview.loadRequest(URLRequest(url: URL(string: strURL)!))
    }
    
    func showInfoForPersonal() {
        var strURL = ""
        var title = ""
        if ReRegistrationModel.shared.Language.lowercased() == "my"{
            if ReRegistrationModel.shared.AccountType == 1 {
                strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/e18055fe-3137-447f-bfb5-124fcaeee3fdReqDocumentsJuly_03_2018%2003_15_43%20AM.jpg"
                title = "Personal".localized
            }else {
                strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/95055398-2b8b-4fe2-9198-deefbaaff14cReqDocumentsJuly_02_2018%2001_22_59%20PM.jpg";
                title = "Merchant".localized
            }
        }else {
            if ReRegistrationModel.shared.AccountType == 1 {
                strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/655446d1-1bbd-4c00-8685-729b7cef201eReqDocumentsJuly_03_2018%2003_13_17%20AM.jpg"
                  title = "Personal".localized
            }else {
                strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/9fd41fe5-297f-4f79-abdb-eef5efbb3d0eReqDocumentsJuly_02_2018%2001_27_55%20PM.jpg";
                  title = "Merchant".localized
            }
        }
        self.view.endEditing(true)
        webviewContainer.isHidden = false
        webview.scalesPageToFit = true
        webview.delegate = self
        lblDemo.text = title
        webview.loadRequest(URLRequest(url: URL(string: strURL)!))
    }
    
    
    func showAlertPersonalInfo(message : String) {
        alertViewObj.wrapAlert(title:"", body: message , img:#imageLiteral(resourceName: "r_personal"))
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
             UserDefaults.standard.set(true, forKey: "RigiRedirect")
            self.dismiss(animated: false, completion: {
                NotificationCenter.default.post(name: Notification.Name("CHANGELAGUAGE"), object: nil)
            })
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    func comeBackFromMainScreen() {
        UserDefaults.standard.set(true, forKey: "RigiRedirect")
        self.dismiss(animated: false, completion: {
            NotificationCenter.default.post(name: Notification.Name("CHANGELAGUAGE"), object: nil)
        })
    }
    
}

