//
//  ReRegistrationExtension.swift
//  OK
//
//  Created by Imac on 6/4/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import AppTrackingTransparency
import AdSupport

extension ReRegistrationVC : ReRegiteredRRCellDelegate,BioMetricLoginDelegate,WebServiceResponseDelegate {
    

    func reRegister() {
        
        self.view.endEditing(true)
        
//        if ok_default_language == "uni" {
//
//            ReRegistrationModel.shared.Name = parabaik.zawgyi(toUni: ReRegistrationModel.shared.Name) ?? ""
//
//            ReRegistrationModel.shared.Father = parabaik.zawgyi(toUni: ReRegistrationModel.shared.Father) ?? ""
//
//            ReRegistrationModel.shared.BusinessName = parabaik.zawgyi(toUni: ReRegistrationModel.shared.BusinessName) ?? ""
//
//            ReRegistrationModel.shared.RoomNumber = parabaik.zawgyi(toUni: ReRegistrationModel.shared.RoomNumber) ?? ""
//
//            ReRegistrationModel.shared.HouseName = parabaik.zawgyi(toUni: ReRegistrationModel.shared.HouseName) ?? ""
//
//            ReRegistrationModel.shared.SecurityQuestionAnswer = parabaik.zawgyi(toUni: ReRegistrationModel.shared.SecurityQuestionAnswer) ?? ""
//
//            ReRegistrationModel.shared.FBEmailId = parabaik.zawgyi(toUni: ReRegistrationModel.shared.FBEmailId) ?? ""
//        }
        
        if UserDefaults.standard.bool(forKey: "RE_REGITRATION")  {
            
           checkEmptyField(handler: { (success , emptyField) in
                if success {
                    if UserLogin.shared.loginSessionExpired {
                        UserDefaults.standard.set(true, forKey: "Re_Registration_Presented")
                        OKPayment.main.authenticate(screenName: "ReRegistration", delegate: self)
                    }else {
                        self.checkImageURLS()
                    }
                }
        else{
                    showAlert(alertTitle: "Please Fill All Mandatory Fields".localized, alertBody: emptyField , alertImage: #imageLiteral(resourceName: "alert-icon"))
               }
            })
        }else {
           checkEmptyField(handler: { (success , emptyField) in
              if success {
                    OKPayment.main.authenticateForRegistration(screenName: "Login", delegate: self)
               }else{
                  showAlert(alertTitle: "Please Fill All Mandatory Fields".localized, alertBody: emptyField , alertImage: #imageLiteral(resourceName: "alert-icon"))
               }
            })
        }
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.HitReRegistrationnAPI),
            name: NSNotification.Name(rawValue: "HIT_RE_REGISTRATION_API"),
            object: nil)
        
    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful{
            self.checkImageURLS()
        }else {
            if UserDefaults.standard.bool(forKey: "Re_Registration_Presented") {
                UserDefaults.standard.set(false, forKey: "Re_Registration_Presented")
            }
            //self.showAlert(alertTitle: "", alertBody: "Image uploading failed. Please press again on Accept and Register button".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
        }
    }
    
    @objc private func HitReRegistrationnAPI(notification: NSNotification) {
         self.checkImageURLS()
    }

    func checkImageURLS() {
        self.uploadProfileAndSingnatureImages(handler: {(success) in
            if success {
                DispatchQueue.main.async {
                    self.SendSignUpAPI()
                }
            }else {
                UserDefaults.standard.set(false, forKey: "Re_Registration_Presented")
                self.showAlert(alertTitle: "", alertBody:"Image uploading failed. Please press again on Accept and Register button".localized , alertImage: #imageLiteral(resourceName: "alert-icon"))
            }
        })
    }
    
    func SendSignUpAPI () {
        ReRegistrationModel.shared.setModelValues()
        let dic = ReRegistrationModel.shared.wrapDataModel()
        println_debug(dic)
        
        if !checkUUIDVerification() {
            
            if #available(iOS 14, *) {
                ATTrackingManager.requestTrackingAuthorization { status in
                    switch status {
                    case .authorized:
                        // Tracking authorization dialog was shown
                        // and we are authorized
                        print("Authorized")
                        
                        // Now that we are authorized we can get the IDFA
                        print(ASIdentifierManager.shared().advertisingIdentifier)
                    case .denied:
                        // Tracking authorization dialog was
                        // shown and permission is denied
                        self.showToAlert("Please Allow Tracking to Register with OK$ \n Settings -> OK$ -> Allow Tracking -> Allow Apps to Request to Track", image: #imageLiteral(resourceName: "alert-icon"), simChanges: false)
                        print("Denied")
                    case .notDetermined:
                        // Tracking authorization dialog has not been shown
                        print("Not Determined")
                    case .restricted:
                        print("Restricted")
                    @unknown default:
                        print("Unknown")
                    }
                }
            }else{
                showToAlert("Please enable Advertising to register with UUID in OK$ \n #iOS 13 \n Settings -> Privacy -> Advertising -> Limit Ad Tracking \n #iOS 14\n Settings -> Privacy -> Tracking -> Allow Apps to Request to Track", image: #imageLiteral(resourceName: "alert-icon"), simChanges: false)
            }
            
//            showToAlert("Please enable Advertising to register with UUID in OK$ \n #iOS 13 \n Settings -> Privacy -> Advertising -> Limit Ad Tracking \n #iOS 14\n Settings -> Privacy -> Tracking -> Allow Apps to Request to Track", image: #imageLiteral(resourceName: "alert-icon"), simChanges: false)
            return
        }
        
        if appDelegate.checkNetworkAvail() {
            let web = WebApiClass()
            web.delegate = self
            var urlStr = ""
            if UserDefaults.standard.bool(forKey: "RE_REGITRATION")  {
                urlStr   = Url.URL_UpdateProfile
            }else {
                urlStr   = Url.URL_Registration
            }
            
            let url = getUrl(urlStr: urlStr, serverType: .serverApp)
            //  let url = URL(string: "http://69.160.4.151:8001/RestService.svc/UpdateProfileDetails")
            println_debug(url)
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
            println_debug(String(data: dic!, encoding: .utf8))
            web.genericClassReg(url: url, param: Dictionary<String,Any>(), httpMethod: "POST", mScreen: "ReRegistration", hbData: dic, authToken: nil)
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
        } else {
            alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img:#imageLiteral(resourceName: "r_user"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        DispatchQueue.main.async{
            progressViewObj.removeProgressView()
        }
        if screen == "ReRegistration" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug(dic["Msg"] as! String)
                        if dic["Code"] as? NSNumber == 200 {
                            if ReRegistrationModel.shared.Password.isNumeric {
                                 userDef.set(true, forKey: "passKeyBoardType")
                             }else {
                                 userDef.set(false, forKey: "passKeyBoardType")
                             }
                            ReRegistrationModel.shared.playSound()
                            if UserDefaults.standard.bool(forKey: "RE_REGITRATION") {
                                NotificationCenter.default.removeObserver(self, name: Notification.Name("HIT_RE_REGISTRATION_API"), object: nil)
                                UserDefaults.standard.set(false, forKey: "Re_Registration_Presented")
                                self.dismiss(animated: false, completion: {
                                    NotificationCenter.default.post(name: Notification.Name("BACK_TO_LOGIN"), object: nil)
                                })
                            }else {
                                userDef.set(ReRegistrationModel.shared.Password, forKey: "passwordLogin_local")
                                if ReRegistrationModel.shared.PasswordType == "0" {
                                    userDef.set(false, forKey: "passwordLoginType")
                                    ok_password_type = false
                                } else {
                                    userDef.set(true, forKey: "passwordLoginType")
                                    ok_password_type = true
                                }
                                ok_password = ReRegistrationModel.shared.Password
                                UserDefaults.standard.set(0, forKey: "App_Password_Type")
                                self.navigateToLoginController()
                            }
                        }else {
                            UserDefaults.standard.set(false, forKey: "Re_Registration_Presented")
                            showAlert(alertTitle: "", alertBody: dic["Msg"] as! String , alertImage: #imageLiteral(resourceName: "r_user"))
                            if UserDefaults.standard.bool(forKey: "RE_REGITRATION")  {
                                self.APIFailedWebService(failureFor: "Re-Registration")
                            }else {
                                self.APIFailedWebService(failureFor: "Registration")
                            }
                            
                            self.failureCount = self.failureCount + 1
                            if self.failureCount == 1 {
                                let failureViewModel = FailureViewModel()
                                let dic = ReRegistrationModel.shared.wrapDataModel()
                                failureViewModel.sendDataToFailureApi(request: dic as Any, response: json as Any, type: FailureHelper.FailureType.Registration.rawValue, finished: {
                                })
                            }
                        }
                    }
                }
            } catch {
                UserDefaults.standard.set(false, forKey: "Re_Registration_Presented")
                showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))
            }
        }else if screen == "AddRegistraionFailureTrackInfo" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug(dic["Msg"] as! String)
                        if dic["Code"] as? NSNumber == 200 {
                            println_debug(dic)
                        }else {
                           showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))
                            
                        }
                    }
                }else {
                     showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))
                }
            } catch {
                showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))
            }
        }
   
    }
    
    func APIFailedWebService(failureFor: String) {
        if appDelegate.checkNetworkAvail() {
            let web = WebApiClass()
            web.delegate = self
            let url = getUrl(urlStr: Url.URL_FailureTrackInfo, serverType: .serverApp)
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
            guard let dic1 = ReRegistrationModel.shared.wrapDataModel() else {
                return
            }
            
            if let strData = String(data: dic1, encoding: .utf8) {
                let dic = ["AppBuildNumber": buildNumber,"AppBuildVersion": buildVersion,"AppType": "1","CellTowerId": "","DeviceInfo": "deviceinfo","FailureStage": failureFor,"HasSimCard": "1","IsRoaming": "0","Latitude": ReRegistrationModel.shared.Latitude,
                               "Longitude": ReRegistrationModel.shared.Longitude,"MobileNumber": ReRegistrationModel.shared.MobileNumber,"OsType": "1","Remarks": strData]
                    println_debug(url)
                    println_debug(dic)
                    web.genericClassReg(url: url, param: dic, httpMethod: "POST", mScreen: "AddRegistraionFailureTrackInfo", hbData: nil, authToken: nil)
                    DispatchQueue.main.async {
                        progressViewObj.showProgressView()
                    }
            }
        } else {
            alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img:#imageLiteral(resourceName: "r_user"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    private func uploadProfileAndSingnatureImages(handler : @escaping (_ isSuccess : Bool) -> Void) {
        
        let paramString : [String:Any] = [
            "MobileNumber":ReRegistrationModel.shared.MobileNumber,
            "Base64String":[ReRegistrationModel.shared.signature,"","","",""]
        ]
        
        VillageManager.shared.uploadImageToS3(imageBase64String :paramString, handler: {(imageURL,success) in
            if success {
                ReRegistrationModel.shared.signatureAwsUrl = imageURL[0].replacingOccurrences(of: " ", with: "%20")
                handler(true)
            }else {
                handler(false)
            }
        })
    }
    
    
    func checkEmptyField(handler : (_ success : Bool,_ emptyField : String) -> Void){
        var emptyFields = ""
        
        if ReRegistrationModel.shared.Name == "" {
            emptyFields = "User Name".localized
        }else if ReRegistrationModel.shared.Name.contains(find: ",") {
            let token = ReRegistrationModel.shared.Name.components(separatedBy: ",")
            if token[1].count < 3 {
                emptyFields = emptyFields + "\n" + "User Name must be more than 2 characters".localized
            }
        }
        
        if ReRegistrationModel.shared.DateOfBirth == "" {
            emptyFields = emptyFields + " " + "Date of Birth".localized
        }
        
        if ReRegistrationModel.shared.ProfilePicAwsUrl == "" {
            emptyFields = emptyFields + " " + "Profile Image".localized
        }
        
        if ReRegistrationModel.shared.Father == "" {
            emptyFields = emptyFields + " " + "Father Name".localized
        }else if ReRegistrationModel.shared.Father.contains(find: ",") {
            let token = ReRegistrationModel.shared.Father.components(separatedBy: ",")
            if token[1].count < 3 {
                emptyFields = emptyFields + "\n" + "Father Name must be more than 2 characters".localized
            }
        }
        
        if ReRegistrationModel.shared.EmailId == "" {
          //  emptyFields = emptyFields + "\n" + "Email ID".localized
        }else {
            if isValidEmail(testStr: ReRegistrationModel.shared.EmailId) {
                
            }else {
                emptyFields = emptyFields + "\n" + "Invalid Email Format".localized
            }
        }
        
        if ReRegistrationModel.shared.IDType == "01" {
            if ReRegistrationModel.shared.NRC == "" {
                emptyFields = emptyFields + "\n" + "NRC number".localized
            }else if ReRegistrationModel.shared.NRC.contains(find: "@") {
                let token = ReRegistrationModel.shared.NRC.components(separatedBy: ")")
                if token[1].count < 6 {
                    emptyFields = emptyFields + "\n" + "NRC number must be 6 characters".localized
                }
            }
            
            
            if ReRegistrationModel.shared.IdPhotoAwsUrl == "" {
                emptyFields = emptyFields + "\n" + "NRC Front Image Missing".localized
            }
            if ReRegistrationModel.shared.IdPhoto1AwsUrl == "" {
                emptyFields = emptyFields + "\n" + "NRC Back Image Missing".localized
            }
            
        }else if ReRegistrationModel.shared.IDType == "04" {
            if ReRegistrationModel.shared.CountryOfCitizen == "" {
                emptyFields = emptyFields + "\n" + "Country of Citizen".localized
            }
            
            if ReRegistrationModel.shared.NRC == "" {
                emptyFields = emptyFields + "\n" + "Passport Number".localized
            }else if ReRegistrationModel.shared.NRC.count < 5 {
                emptyFields = emptyFields + "\n" + "Passport Number must be more than 4 characters".localized
            }
            
            if ReRegistrationModel.shared.IdExpDate == "" {
                emptyFields = emptyFields + "\n" + "Passport Expiry Date".localized
            }
            
            if ReRegistrationModel.shared.IdPhotoAwsUrl == "" {
                emptyFields = emptyFields + "\n" + "Passport Front Image Missing".localized
            }
            if ReRegistrationModel.shared.IdPhoto1AwsUrl == "" {
                emptyFields = emptyFields + "\n" + "Passport Back Image Missing".localized
            }
        }else{
            emptyFields = emptyFields + "\n" + "ID Type".localized
        }
        
        if ReRegistrationModel.shared.AccountType == 0 {
            if ReRegistrationModel.shared.BusinessName == "" {
                emptyFields = emptyFields + "\n" + "Business Name".localized
            }else if ReRegistrationModel.shared.BusinessName.count < 3 {
                emptyFields = emptyFields + "\n" + "Business Name must be more than 2 characters".localized
            }
            
            if ReRegistrationModel.shared.BusinessCategory == "" {
                emptyFields = emptyFields + "\n" + "Business Category".localized
            }
            
            if ReRegistrationModel.shared.BusinessType == "" {
                emptyFields = emptyFields + "\n" + "Business Sub-Category".localized
            }
            
            if ReRegistrationModel.shared.BusinessIdPhoto1AwsUrl == "" {
                emptyFields = emptyFields + "\n" + "Business License Image 1 Missing".localized
            }
            if ReRegistrationModel.shared.BusinessIdPhoto1AwsUrl == "" {
                emptyFields = emptyFields + "\n" + "Business License Image 2 Missing".localized
            }
        }
        
        if ReRegistrationModel.shared.lType == "AGENT" {
            if ReRegistrationModel.shared.ShopPicOneAwsUrl == "" {
                emptyFields = emptyFields + "\n" + "Agent first shop image missing".localized
            }
            if ReRegistrationModel.shared.ShopPicTwoAwsUrl == "" {
                emptyFields = emptyFields + "\n" + "Agent second shop image missing".localized
            }
        }

        if ReRegistrationModel.shared.AddressType == "" {
            emptyFields = emptyFields + "\n" + "Address Type".localized
        }else {
            if ReRegistrationModel.shared.State == "" {
                emptyFields = emptyFields + "\n" + "State/Division Name".localized
            }
            if ReRegistrationModel.shared.Township == "" {
                emptyFields = emptyFields + "\n" + "Township Name".localized
            }
            
            if ReRegistrationModel.shared.Address2 == "" {
                emptyFields = emptyFields + "\n" + "Street Name".localized
            }
            
            if   ReRegistrationModel.shared.HouseBlockNo == "" && ReRegistrationModel.shared.FloorNumber ==  "" && ReRegistrationModel.shared.RoomNumber == "" {
                emptyFields = emptyFields + "\n" + "House No/Floor No/Room No".localized
            }
        }
        
        
        if ReRegistrationModel.shared.PasswordType == "" {
            emptyFields = emptyFields + "\n" + "Security Type".localized
        }
        
        if ReRegistrationModel.shared.Password == "" {
            emptyFields = emptyFields + "\n" + "Enter Password".localized
        }else if ReRegistrationModel.shared.Password.count < 6{
            emptyFields = emptyFields + "\n" + "Password must be more than 5 digits".localized
        }
        
        if UserDefaults.standard.bool(forKey: "RE_REGITRATION")  {
            
        }else {
            if ReRegistrationModel.shared.PasswordType == "0" {
                if ReRegistrationModel.shared.ConfirmPassword == "" {
                    emptyFields = emptyFields + "\n" + "Enter Confirm Password".localized
                }else if ReRegistrationModel.shared.Password != ReRegistrationModel.shared.ConfirmPassword {
                    emptyFields = emptyFields + "\n" + "Password and Confirm Password does not matched".localized
                }
            }
        }
        
        if ReRegistrationModel.shared.SecurityQuestionCode == "" {
            emptyFields = emptyFields + "\n" + "Security Question".localized
        }
        if ReRegistrationModel.shared.SecurityQuestionAnswer == "" {
            emptyFields = emptyFields + "\n" + "Security Answer".localized
        }
        
        if ReRegistrationModel.shared.signature == "" {
            emptyFields = emptyFields + "\n" + "User Signature".localized
        }
 
        if emptyFields == "" {
            handler (true, "")
        }else {
            handler (false, emptyFields)
        }
    }
    
    func formValidNumber(countryCode: String, m_number: String) -> String{
        var num = m_number
        var valid_numver = ""
        var c_code = countryCode
        c_code.removeFirst()
        if countryCode == "+95" {
           num.removeFirst()
           valid_numver = "00" + c_code + num
        }else {
           valid_numver = "00" + num
        }
        return valid_numver
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}
