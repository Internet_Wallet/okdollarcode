//
//  AddressExtension.swift
//  OK
//
//  Created by Imac on 6/2/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import Rabbit_Swift

extension ReRegistrationVC: AddressHeaderRRCellDelegte,CurrentLocationVCDelegate,DivisionRRCellDelegate,StateDivisionVCDelegate,TownshipRRCellDelegate,AddressTownshiopVCDelegate,OtherAddressRRCellDelegte,HouseNumberRRCellDelegate,StreetRRCellDelegate,VillageStreetListViewDelegate,StreetListViewDelegate,VillageRRCellDelegate{
 
    
 
    

    func resetAddress() {
        ReRegistrationModel.shared.AddressType = ""
        ReRegistrationModel.shared.State = ""
        ReRegistrationModel.shared.Township = ""
        ReRegistrationModel.shared.Address1 = ""
        ReRegistrationModel.shared.VillageName = ""
        ReRegistrationModel.shared.Address2 = ""
        ReRegistrationModel.shared.HouseBlockNo = ""
        ReRegistrationModel.shared.FloorNumber = ""
        ReRegistrationModel.shared.RoomNumber = ""
        ReRegistrationModel.shared.HouseName = ""
        Other = false
        
        self.DivisionName = "Select State / Division".localized
        self.TownshipName = "Select Township".localized
  
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 1, section: 4)) as? OtherAddressRRCell {
            cell.tfTypeHere.text = ""
            cell.btnTypeHereClear.isHidden = true
          
        }
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 4)) as? DivisionRRCell {
          cell.lblDivision.text = self.DivisionName
        }
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 3, section: 4)) as? TownshipRRCell {
            cell.btnTownship.setTitle(TownshipName, for: .normal)
        }
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 4, section: 4)) as? CityRRCell {
            cell.tfCity.text = ""
        }
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 5, section: 4)) as? VillageRRCell {
            cell.tfVillageName.text = ""
        }
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 6, section: 4)) as? StreetRRCell {
            cell.tfStreetName.text = ""
        }
        
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 7, section: 4)) as? HouseNumberRRCell {
            cell.tfHouseNo.text = ""
            cell.tfFloorNo.text = ""
            cell.tfRoomNo.text = ""
        }
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 8, section: 4)) as? HousingAddressRRCell {
            cell.tfHousingZone.text = ""
        }
        
        reloadRows(index: 1, withSeciton: 4)
        
    }
    func showVillageView(text: String) {
        
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 3, section: 4)) as? TownshipRRCell {
            if cell.btnTownship.titleLabel?.text == "Select Township".localized {
               if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 6, section: 4)) as? VillageRRCell {
                cell.tfVillageName.text = ""
                ReRegistrationModel.shared.VillageName = ""
                DispatchQueue.main.async {
                    self.showAlert(alertTitle: "", alertBody: "Please select township".localized, alertImage: UIImage(named: "alert-icon")!)}
                return
                }
            }
        }
        
        if text.count > 0 {
            if self.view.contains(tvVillageList) {
            }else {
                self.setUpVillageList()
                tvVillageList.tvList.isHidden = false
            }
            tvVillageList.searchString(string: text)
        }else {
            if self.view.contains(tvVillageList) {
                self.removeVillageList()
            }
        }
    }
    
    func showStreetView(text: String) {
        if text.count > 0 {
            if self.view.contains(tvStreetList) {
            }else {
                setUpStreetList()
                tvStreetList.tvList.isHidden = false
            }
            tvStreetList.getAllAddressDetails(searchTextStr: text, yAsix: 0)
        }else {
            if self.view.contains(tvStreetList) {
                self.removeStreetList()
            }
        }
    }
//if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 7, section: 4)) as? StreetRRCell {}
    
    func setUpVillageList() {
        tvVillageList.frame = CGRect(x: 10, y: 0, width: self.view.frame.width - 20, height: 200)
        tvVillageList.backgroundColor = UIColor.clear
        tvVillageList.delegate = self
        tvVillageList.tvList.isHidden = true
        tvVillageList.isUserInteractionEnabled = true
        self.view.addSubview(tvVillageList)
        self.view.bringSubviewToFront(tvVillageList)
    }

    func setUpStreetList() {
        tvStreetList.frame = CGRect(x: 10, y: 0, width: self.view.frame.width - 20, height: 200)
        tvStreetList.backgroundColor = UIColor.clear
        tvStreetList.delegate = self
        tvStreetList.tvList.isHidden = true
        tvStreetList.isUserInteractionEnabled = true
        self.view.addSubview(tvStreetList)
        self.view.bringSubviewToFront(tvStreetList)
    }

    func villageNameSelected(vill_Name: String) {
        let indexPath = IndexPath(row: 5, section: 4)
        if let cell = self.tableViewRagi.cellForRow(at: indexPath) as? VillageRRCell {
            cell.tfVillageName.font = UIFont(name: appFont, size: 18.0)
            cell.tfVillageName.text = vill_Name
            cell.tfVillageName.resignFirstResponder()
            ReRegistrationModel.shared.VillageName = vill_Name
            self.removeVillageList()
        }
    }
    
    func streetNameSelected(street_Name: addressModel) {
        let indexPath = IndexPath(row: 6, section: 4)
        if let cell = self.tableViewRagi.cellForRow(at: indexPath) as? StreetRRCell {
            cell.tfStreetName.font = UIFont(name: appFont, size: 18.0)
            cell.tfStreetName.text = street_Name.shortName
            cell.tfStreetName.resignFirstResponder()
            ReRegistrationModel.shared.Address2 = street_Name.shortName ?? ""
            self.removeStreetList()
        }
    }
    func removeVillageList() {
        if self.view.contains(tvVillageList) {
            tvVillageList.removeFromSuperview()
        }
    }
    func removeStreetList() {
        if self.view.contains(tvStreetList) {
            tvStreetList.removeFromSuperview()
        }
    }

    
    func showDivision() {
        division = true
        reloadRows(index: 2, withSeciton: 4)
    }

    func showRoomNumberView() {
        self.room = true
        reloadRows(index: 7, withSeciton: 4)
    }
    
    func showSecurityVC() {
        housing = true
        reloadRows(index: 8, withSeciton: 4)
        if UserDefaults.standard.bool(forKey: "RE_REGITRATION") {
            if self.tableViewRagi.cellForRow(at: IndexPath(row: 0, section: 5)) as? PasswordHeaderRRCell == nil {
                securityRowCount = 1
                insertRow(index: 0, withSeciton: 5)
            }
            
            if self.tableViewRagi.cellForRow(at: IndexPath(row: 1, section: 5)) as? PasswordRRCell == nil {
                securityRowCount = 2
                insertRow(index: 1, withSeciton: 5)
            }
            
            if self.tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 5)) as? ConfirmPasswordRRCell == nil {
                securityRowCount = 3
                insertRow(index: 2, withSeciton: 5)
            }
            
            if self.tableViewRagi.cellForRow(at: IndexPath(row: 3, section: 5)) as? SecurityQuestionRRCell == nil {
                securityRowCount = 4
                insertRow(index: 3, withSeciton: 5)
            }
        }else {
            if self.tableViewRagi.cellForRow(at: IndexPath(row: 0, section: 5)) as? PasswordHeaderRRCell == nil {
                securityRowCount = 1
                insertRow(index: 0, withSeciton: 5)
            }
        }
 
    }
    
    func navigateToDiviosionVC() {
        self.view.endEditing(true)
        let sb = UIStoryboard(name: "Registration", bundle: nil)
        let stateDivisionVC  = sb.instantiateViewController(withIdentifier: "StateDivisionVC") as! StateDivisionVC
        stateDivisionVC.stateDivisonVCDelegate = self
        self.navigationController?.pushViewController(stateDivisionVC, animated: true)
    }
    func navigteToTownshipVC() {
        
        self.view.endEditing(true)
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 4)) as? DivisionRRCell {
            if  cell.lblDivision.text  == "Select State / Division".localized {
                DispatchQueue.main.async {
                    self.showAlert(alertTitle: "", alertBody: "Select State / Division".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))}
            }else {
                let sb = UIStoryboard(name: "Registration", bundle: nil)
                let addressTownshiopVC  = sb.instantiateViewController(withIdentifier: "AddressTownshiopVC") as! AddressTownshiopVC
                addressTownshiopVC.addressTownshiopVCDelegate = self
                addressTownshiopVC.selectedDivState = locationDetailsAddress
                self.navigationController?.pushViewController(addressTownshiopVC, animated: true)
            }
        }
    }
    
    func setDivisionStateName(SelectedDic: LocationDetail) {
        self.view.endEditing(true)
        ReRegistrationModel.shared.State = SelectedDic.stateOrDivitionCode
        ReRegistrationModel.shared.Address1 = ""
        ReRegistrationModel.shared.Address2 = ""
        ReRegistrationModel.shared.Township = ""
        ReRegistrationModel.shared.VillageName = ""
        locationDetailsAddress = SelectedDic
        if ReRegistrationModel.shared.Language.lowercased() == "my" {
            DivisionName = SelectedDic.stateOrDivitionNameMy
        }else if ReRegistrationModel.shared.Language.lowercased() == "en"{
            DivisionName = SelectedDic.stateOrDivitionNameEn
        }else {
            DivisionName = SelectedDic.stateOrDivitionNameUni
        }
        TownshipName = "Select Township".localized
        
        township = true
        reloadRows(index: 2, withSeciton: 4)
        reloadRows(index: 3, withSeciton: 4)
        reloadRows(index: 4, withSeciton: 4)
        reloadRows(index: 5, withSeciton: 4)
        reloadRows(index: 6, withSeciton: 4)
        
        if self.addressScrollcount == 2 {
            self.addressScrollcount = 3
            UIView.animate(withDuration: 1.5, animations: {
                _ = Timer.scheduledTimer(withTimeInterval: 0.8, repeats: false) { timer in
                    let indexPath = IndexPath(row: 3, section: 4)
                    self.tableViewRagi.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                }
            })
        }
    }
    
    func setTownshipCityNameFromDivison(Dic: TownShipDetailForAddress) {
        self.setAddressFromDivisionAndTownship(dic: Dic)
    }
    
    func setTownshipCityName(cityDic : TownShipDetailForAddress) {
        self.setAddressFromDivisionAndTownship(dic: cityDic)
    }
    
    func setAddressFromDivisionAndTownship( dic : TownShipDetailForAddress) {
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 5, section: 4)) as? VillageRRCell {
            cell.tfVillageName.text = ""
        }
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 6, section: 4)) as? StreetRRCell {
            cell.tfStreetName.text = ""
        }
        
        townshipCode = dic.townShipCode
        ReRegistrationModel.shared.Township = dic.townShipCode
        ReRegistrationModel.shared.VillageName = ""
        ReRegistrationModel.shared.Address2 = ""
        if ReRegistrationModel.shared.Language.lowercased() == "my" {
            TownshipName = dic.townShipNameMY
            if dic.GroupName == "" {
                ReRegistrationModel.shared.Address1 = dic.cityNameMY
            }else {
                if dic.isDefaultCity == "1" {
                ReRegistrationModel.shared.Address1 = dic.DefaultCityNameMY
                }else{
                ReRegistrationModel.shared.Address1 = dic.cityNameMY
                }
            }
        }else if ReRegistrationModel.shared.Language.lowercased() == "en"{
            TownshipName = dic.townShipNameEN
            if dic.GroupName == "" {
                ReRegistrationModel.shared.Address1 = dic.cityNameEN
            }else {
                if dic.isDefaultCity == "1" {
                    ReRegistrationModel.shared.Address1 = dic.DefaultCityNameEN
                }else {
                    ReRegistrationModel.shared.Address1 = dic.cityNameEN
                }
            }
        }else {
            TownshipName = dic.townShipNameUni
            if dic.GroupName == "" {
                ReRegistrationModel.shared.Address1 = dic.cityNameUni
            }else {
                if dic.isDefaultCity == "1" {
                    ReRegistrationModel.shared.Address1 = dic.DefaultCityNameUni
                }else {
                    ReRegistrationModel.shared.Address1 = dic.cityNameUni
                }
            }
        }
        var isVillageFieldHidden = false
        
        self.township = true
        self.city = true
        self.reloadRows(index: 3, withSeciton: 4)
        self.reloadRows(index: 4, withSeciton: 4)
        
        if dic.GroupName.contains(find: "YCDC") || dic.GroupName.contains(find: "MCDC") {
            isVillageFieldHidden = true
        }
        
        if dic.cityNameEN == "Amarapura" || dic.cityNameEN == "Patheingyi" || dic.townShipNameEN == "PatheinGyi" {
            isVillageFieldHidden = false
        }
        
        
        if isVillageFieldHidden {
            self.village = false
        }else {
            self.village = true
        }
        self.street = true
        self.reloadRows(index: 5, withSeciton: 4)
        self.reloadRows(index: 6, withSeciton: 4)
            UIView.animate(withDuration: 1.5, animations: {
                _ = Timer.scheduledTimer(withTimeInterval: 0.8, repeats: false) { timer in
                    let indexPath = IndexPath(row: 6, section: 4)
                    self.tableViewRagi.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                }
            })
        self.callStreetVillageAPI(townshipcode: dic.townShipCode)
    }
    
    
    func ShowCurrentLocationVC(wihttype: String) {
         self.view.endEditing(true)
        adddressMode = wihttype
        let sb = UIStoryboard(name: "Registration", bundle: nil)
        let currentLocationVC  = sb.instantiateViewController(withIdentifier: "CurrentLocationVC") as? CurrentLocationVC
        currentLocationVC?.view.frame = CGRect(x:0 , y:0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        currentLocationVC?.curLocaitonDelegate = self
        currentLocationVC?.modalPresentationStyle = .overCurrentContext
        self.present(currentLocationVC!, animated: false, completion: nil)
    }
    
    
    func SetAddress(Divison: LocationDetail, Township: TownShipDetailForAddress, City: String, Stret: String) {
        self.view.endEditing(true)
        addressScrollcount = 5
        ReRegistrationModel.shared.State = Divison.stateOrDivitionCode
        ReRegistrationModel.shared.Township = Township.townShipCode
       
        ReRegistrationModel.shared.Address2 =  Stret//Rabbit.uni2zg(Stret)  ///parabaik.zawgyi(toUni: Stret) //parabaik.uni(toZawgyi:Stret)
        ReRegistrationModel.shared.VillageName = ""
        townshipCode = Township.townShipCode
        locationDetailsAddress = Divison
        
        if ReRegistrationModel.shared.Language.lowercased() == "my" {
            self.DivisionName = Divison.stateOrDivitionNameMy
            TownshipName = Township.townShipNameMY
            if Township.GroupName == "" {
                ReRegistrationModel.shared.Address1 = Township.cityNameMY
            }else {
                if Township.isDefaultCity == "1" {
                    ReRegistrationModel.shared.Address1 = Township.DefaultCityNameMY
                }else{
                    ReRegistrationModel.shared.Address1 = Township.cityNameMY
                }
            }
        }else if ReRegistrationModel.shared.Language.lowercased() == "en"{
            self.DivisionName = Divison.stateOrDivitionNameEn
            TownshipName = Township.townShipNameEN
            if Township.GroupName == "" {
                ReRegistrationModel.shared.Address1 = Township.cityNameEN
            }else {
                if Township.isDefaultCity == "1" {
                    ReRegistrationModel.shared.Address1 = Township.DefaultCityNameEN
                }else{
                    ReRegistrationModel.shared.Address1 = Township.cityNameEN
                }
            }
        }else {
            self.DivisionName = Divison.stateOrDivitionNameUni
            TownshipName = Township.townShipNameUni
            if Township.GroupName == "" {
                ReRegistrationModel.shared.Address1 = Township.cityNameUni
            }else {
                if Township.isDefaultCity == "1" {
                    ReRegistrationModel.shared.Address1 = Township.DefaultCityNameUni
                }else{
                    ReRegistrationModel.shared.Address1 = Township.cityNameUni
                }
            }
        }
        
        
        var isVillageFieldHidden = false
        
        if self.adddressMode == "other" {
            self.Other = true
        }else {
            self.Other = false
        }
        self.division = true
        self.township = true
        self.city = true
        self.reloadRows(index: 1, withSeciton: 4)
        self.reloadRows(index: 2, withSeciton: 4)
        self.reloadRows(index: 3, withSeciton: 4)
        self.reloadRows(index: 4, withSeciton: 4)
        
        if Township.GroupName.contains(find: "YCDC") || Township.GroupName.contains(find: "MCDC") {
            isVillageFieldHidden = true
        }
        if Township.cityNameEN == "Amarapura" || Township.cityNameEN == "Patheingyi" || Township.townShipNameEN == "PatheinGyi" {
            isVillageFieldHidden = false
        }
        
        
        
        if isVillageFieldHidden {
            self.village = false
        }else {
            self.village = true
        }
        self.street = true
        self.reloadRows(index: 5, withSeciton: 4)
        self.reloadRows(index: 6, withSeciton: 4)
        
        if Stret != "" {
            self.room = true
            self.reloadRows(index: 7, withSeciton: 4)
            UIView.animate(withDuration: 1.5, animations: {
                _ = Timer.scheduledTimer(withTimeInterval: 0.8, repeats: false) { timer in
                    let indexPath = IndexPath(row: 7, section: 4)
                    self.tableViewRagi.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                }
            })
        }else {
            UIView.animate(withDuration: 1.5, animations: {
                _ = Timer.scheduledTimer(withTimeInterval: 0.8, repeats: false) { timer in
                    let indexPath = IndexPath(row: 6, section: 4)
                    self.tableViewRagi.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                }
            })
        }

        self.callStreetVillageAPI(townshipcode: Township.townShipCode)
    }
    
    
    func CancelLocationAddress() {
        if adddressMode == "other" {
            Other = true
            reloadRows(index: 1, withSeciton: 4)
            if self.addressScrollcount == 0 {
                self.addressScrollcount = 1
                UIView.animate(withDuration: 1.5, animations: {
                    _ = Timer.scheduledTimer(withTimeInterval: 0.8, repeats: false) { timer in
                        let indexPath = IndexPath(row: 1, section: 4)
                        self.tableViewRagi.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                    }
                })
            }
        }else {
            division = true
            Other = false
            reloadRows(index: 1, withSeciton: 4)
            reloadRows(index: 2, withSeciton: 4)
            if self.addressScrollcount == 1 || self.addressScrollcount == 0 {
                self.addressScrollcount = 2
                UIView.animate(withDuration: 1.5, animations: {
                    _ = Timer.scheduledTimer(withTimeInterval: 0.8, repeats: false) { timer in
                        let indexPath = IndexPath(row: 2, section: 4)
                        self.tableViewRagi.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                    }
                })
            }
        }
    }
    
    func callStreetVillageAPI(townshipcode : String){
        let urlStr   = Url.URL_VillageList + townshipcode
        let url = getUrl(urlStr: urlStr, serverType: .serverApp)
        println_debug("Village list url: \(url)")
        let params = Dictionary<String,String>()
        TopupWeb.genericClass(url:url, param: params as AnyObject, httpMethod: "GET", handle: {response, isSuccess in
            if isSuccess {
                if let json = response as? Dictionary<String,Any> {
                    let response = json["Data"] as Any
                    let dic = OKBaseController.convertToDictionary(text: response as! String)
                    if let d = dic {
                        if let arrDic = d["Streets"] as? Array<Dictionary<String,String>> {
                            let streetList = StreetList()
                            streetList.isStreetList = true
                            streetList.streetArray = arrDic
                            VillageManager.shared.allStreetList = streetList
                            
                        }else{
                            let streetList = StreetList()
                            streetList.streetArray.removeAll()
                            streetList.isStreetList = false
                            VillageManager.shared.allStreetList = streetList
                        }
                        if let arrDic1 = d["Villages"] as? Array<Dictionary<String,String>>{
                            DispatchQueue.main.async {
                                let villageList = VillageList()
                                villageList.villageArray = arrDic1
                                VillageManager.shared.allVillageList = villageList
                                villageList.isVillageList = true
                                if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 5, section: 4)) as? VillageRRCell {
                                    cell.tfVillageName.isUserInteractionEnabled = true
                                }
                            }
                        }else{
                            DispatchQueue.main.async {
                                let villageList = VillageList()
                                villageList.villageArray.removeAll()
                                VillageManager.shared.allVillageList = villageList
                                villageList.isVillageList = false
                                if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 5, section: 4)) as? VillageRRCell {
                                    cell.tfVillageName.isUserInteractionEnabled = true
                                }
                            }
                        }
                        println_debug(VillageManager.shared.allVillageList)
                    }
                }
            }else {
                DispatchQueue.main.async {
                    self.showAlert(alertTitle: "", alertBody: "Network error and try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))}
            }
        })
    }
    
    func showAddressDemo() {
        var strURL = ""
        if ReRegistrationModel.shared.Language.lowercased() == "my"{
            strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/831520e3-ca48-4202-9347-6d76c1f32c6aReqDocumentsJuly_02_2018%2001_20_09%20PM.jpg";
        }else {
            strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/ccebdb05-bb2e-43da-8af4-293147597627ReqDocumentsJuly_02_2018%2001_33_23%20PM.jpg";
        }
        self.view.endEditing(true)
        webviewContainer.isHidden = false
        webview.scalesPageToFit = true
        webview.delegate = self
        lblDemo.text = "Address Details".localized
        webview.loadRequest(URLRequest(url: URL(string: strURL)!))
    }
    
}
