//
//  ReRegitrationSecurityVC.swift
//  OK
//
//  Created by Imac on 5/28/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol ReRegitrationSecurityVCDelegate {
    func updateMainUIUI()
    func showSignatureVC()
    func viewEndEditing()
}


class ReRegitrationSecurityVC: OKBaseController,UITextFieldDelegate,SecurityQuestionRDelegate {
    var delegate : ReRegitrationSecurityVCDelegate?
    @IBOutlet weak var viewSelectSecurityQuestion: UIView!
    @IBOutlet weak var veiwSecurityQuestion: UIView!
    @IBOutlet weak var veiwAnswer: UIView!
    @IBOutlet weak var lblSecurityHeader: UILabel!
    @IBOutlet weak var lblQuestion: MarqueeLabel!
    @IBOutlet weak var btnSecuritQuestion: UIButton!
    @IBOutlet weak var tfAnswer: UITextField!
    var  isNextViewHidden = true
    var ANSWERCHARSET = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dHeight = 97
        lblSecurityHeader.attributedText = lblSecurityHeader.attributedStringFinal(str1: "Select Security Question ".localized)
        lblQuestion.attributedText =  lblQuestion.attributedStringFinal(str1: "Select Security Question".localized)
        tfAnswer.placeholder = "Enter Answer".localized
        tfAnswer.delegate = self
        if ReRegistrationModel.shared.Language.lowercased() == "en"{
            ANSWERCHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890/-,() "
        }else if ReRegistrationModel.shared.Language.lowercased() == "my" {
            ANSWERCHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာ၁၂၃၄၅၆၇၈၉၀/-,() "
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    @IBAction func btnSecuritQuestionAction(_ sender: Any) {
        if let del = delegate {
            del.viewEndEditing()
        }
        
            tfAnswer.text = ""
            tfAnswer.resignFirstResponder()
            let sb = UIStoryboard(name: "Registration" , bundle: nil)
            let securityQuestionR  = sb.instantiateViewController(withIdentifier: "SecurityQuestionR") as! SecurityQuestionR
            securityQuestionR.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            securityQuestionR.securityQuestionRDelegate = self
            securityQuestionR.modalPresentationStyle = .overCurrentContext
            self.present(securityQuestionR, animated: false, completion: nil)
    }
    
    
    func SelectedQuestion(questionDic: Dictionary<String, AnyObject>) {
        
        ReRegistrationModel.shared.SecurityQuestionCode = questionDic["QuestionCode"] as! String
        if ReRegistrationModel.shared.Language.lowercased() == "my" {
            lblQuestion.text = questionDic["QuestionBurmese"] as? String
        }else {
            lblQuestion.text = questionDic["Question"] as? String
        }
        
        if isNextViewHidden {
            if self.dHeight! <= viewSelectSecurityQuestion.frame.origin.y + veiwAnswer.frame.height + veiwAnswer.frame.origin.y {
                self.dHeight = viewSelectSecurityQuestion.frame.origin.y + veiwAnswer.frame.height + veiwAnswer.frame.origin.y
                runGuard()
            }
        }
        // tfAnswer.becomeFirstResponder()
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
      if textField == tfAnswer {
            if lblQuestion.text ==  "Select Security Question".localized {
                showAlert(alertTitle: "", alertBody: "Please Select Security Question First".localized, alertImage: #imageLiteral(resourceName: "password"))
                return false
            }
        }
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool { if textField == tfAnswer {
            if tfAnswer.text!.count > 0 {
                if self.dHeight! <= viewSelectSecurityQuestion.frame.origin.y + viewSelectSecurityQuestion.frame.height{
                    self.dHeight = viewSelectSecurityQuestion.frame.origin.y + viewSelectSecurityQuestion.frame.height
                    runGuard()
                }
                
                if isNextViewHidden {
                    isNextViewHidden = false
                    guard(self.delegate?.showSignatureVC() != nil)else { return false }
                }
                
            }
            removeSpaceAtLast(txtField:tfAnswer)
            //ANTONY
            ReRegistrationModel.shared.SecurityQuestionAnswer = tfAnswer.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        }
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if range.location == 0 && string == " " {
            return false
        }
            if textField == tfAnswer {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: ANSWERCHARSET).inverted).joined(separator: "")) { return false }
            if text.count > 50 {
                return false
            }
            if restrictMultipleSpaces(str: string, textField: tfAnswer) {
                return true
            }else {
                if  textField.text?.last == " " {
                    textField.text?.removeLast()
                }
                return false
            }
        }
        return true
    }
    
    private func removeSpaceAtLast(txtField : UITextField){
        if txtField.text?.last == " " {
            txtField.text?.removeLast()
        }
    }
    func runGuard() {
        guard(self.delegate?.updateMainUIUI() != nil) else {
            return
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
}
