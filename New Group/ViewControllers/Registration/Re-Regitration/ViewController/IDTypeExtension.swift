//
//  IDTypeExtension.swift
//  OK
//
//  Created by Imac on 6/1/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

extension ReRegistrationVC: SelectIDTypeRRCellDelegate,TownShipVcDeletage,NRCTypesVCDelegate,IDTypeHeaderRRCellDelegate,NRCNumberRRCellDelegate,TownshipVC2Delegate,CountryViewControllerDelegate,PassportCountryRRCellDelegate,PassportNumberRRCellDelegate,PassportExpiryDateRRCellDelegate,IDPhotoRRCellDelegate,NRCPhtotoUploadwithScanDelegate {


    func resetIDType() {
        
        if ReRegistrationModel.shared.IDType == "01" {
            
            if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 2)) as? NRCNumberRRCell {
                cell.isNextViewHidden = true
                cell.tfNRC.text = ""
                ReRegistrationModel.shared.NRC = ""
                cell.viewNRCButton.isHidden = true
            }
            if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 3, section: 2)) as? IDPhotoRRCell {
               cell.imgFront.image = UIImage(named: "r_nrc")
               cell.imgBack.image = UIImage(named: "r_nrc")
            }
        }else {
            if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 2)) as? PassportCountryRRCell {
                cell.isNextViewHidden = true
                cell.btnSelectCountry.setTitle("Select Country of Citizenship".localized, for: .normal)
                cell.imgCountry.image = UIImage(named: "r_country")
            }
            if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 3, section: 2)) as? PassportNumberRRCell {
                cell.isNextViewHidden = true
                cell.tfPassportNo.placeholder = "Enter Passport Number".localized
                cell.tfPassportNo.text = ""
            }
            if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 4, section: 2)) as? PassportExpiryDateRRCell {
                cell.isNextViewHidden = true
                cell.tfDate_Of_Expiry.placeholder = "Enter Passport Expiry Date".localized
                cell.tfDate_Of_Expiry.text = ""
            }
            if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 5, section: 2)) as? IDPhotoRRCell {
                cell.imgFront.image = UIImage(named: "r_nrc")
                cell.imgBack.image = UIImage(named: "r_nrc")
            }
        }
        imageListIDType.removeAll()
        isIDPhotoAttached = false
        ReRegistrationModel.shared.IDType = ""
        ReRegistrationModel.shared.NRC = ""
        ReRegistrationModel.shared.CountryOfCitizen = ""
        ReRegistrationModel.shared.IdExpDate = ""
        ReRegistrationModel.shared.IdPhotoAwsUrl = ""
        ReRegistrationModel.shared.IdPhoto1AwsUrl = ""
        ReRegistrationModel.shared.countryImage = ""
        isTypeSelected = "NONE"
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 0, section: 2)) as? IDTypeHeaderRRCell {
            cell.btnReset.isUserInteractionEnabled = false
        }
        reloadRows(index: 1, withSeciton: 2)
        UIView.animate(withDuration: 0.3, animations: {
            _ = Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false) { timer in
                let indexPath = IndexPath(row: 1, section: 2)
                self.tableViewRagi.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
            }
        })
        
        idTyperRowCount = 2
        tableViewRagi.reloadSections([2], with: .none)
        tableViewRagi.reloadData()
    }
 
    func showStateDivision() {
        self.view.endEditing(true)
        let sb = UIStoryboard(name: "Registration", bundle: nil)
        let townVC = sb.instantiateViewController(withIdentifier: "TownshipVC") as! TownshipVC
        townVC.townShipDelegate = self
        townVC.registrationScreen = "Registration"
        self.navigationController?.pushViewController(townVC, animated: false)
    }
    
    func showTownshipVC() {
        self.view.endEditing(true)
        let sb = UIStoryboard(name: "Registration", bundle: nil)
        let townshipVC2 = sb.instantiateViewController(withIdentifier: "TownshipVC2") as! TownshipVC2
        townshipVC2.selectedDivState = locationDetails
        townshipVC2.townshipVC2Delegate = self
        self.navigationController?.pushViewController(townshipVC2, animated: true)
    }
    
    
    
    func setDivison(division: LocationDetail) {
        // After Selecting NRC Divison
        locationDetails = division
        ReRegistrationModel.shared.IDType = "01"
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 0, section: 2)) as? IDTypeHeaderRRCell {
            cell.btnReset.isUserInteractionEnabled = true//false
        }
        isTypeSelected = "NRC"
            if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 1, section: 2)) as? SelectIDTypeRRCell {
                if !cell.isHidden {
                    cell.isHidden = true
                    self.reloadRows(index: 1, withSeciton: 2)
                    DispatchQueue.main.async {
                        if self.tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 2)) as? NRCNumberRRCell == nil {
                            self.idTyperRowCount = 3
                            self.insertRow(index: 2, withSeciton: 2)
                           // self.reloadRows(index: 2, withSeciton: 2)
                        }
                        self.reloadRows(index: 2, withSeciton: 2)
                    }
                }
            }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
            if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 2)) as? NRCNumberRRCell {
                cell.viewNRCButton.isHidden = false
                cell.viewNRCTF.isHidden = true
                cell.tfNRC.text = ""
                var divStr = ""
                if ok_default_language == "my"{
                    divStr = division.nrcCodeNumberMy + "/"
                }else if ok_default_language == "en"{
                    divStr = division.nrcCodeNumber + "/"
                }else {
                    divStr = division.nrcCodeNumberUni + "/"
                }
                
                if divStr != ""{
                    cell.btnNRC.setTitle(divStr, for: .normal)
                }
                //else{
//                    cell.tfNRC.text = ""
//                    cell.btnNRC.setTitle("Enter NRC Number".localized, for: .normal)
//                }
                
              //  cell.tfNRC.text = ""
            }
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 0, section: 2)) as? IDTypeHeaderRRCell {
                cell.btnReset.isUserInteractionEnabled = true
            }
        })
    }
    
    func setDivisionAndTownship(strDivison_township: String) {
        self.setNRCDetails(nrcDetails: strDivison_township)
    }
    
    func NRCValueString(nrcSting : String) {
        self.setNRCDetails(nrcDetails: nrcSting)
    }
    
     func setNRCDetails(nrcDetails : String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
            if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 2)) as? NRCNumberRRCell  {
                
                let myString = nrcDetails
                
                if myString == "" {
//                    cell.viewNRCTF.isHidden = false
//                    cell.tfNRC.text = ""
//                    ReRegistrationModel.shared.NRC = ""
//                    cell.viewNRCButton.isHidden = true
                    self.resetIDType()
                    
                }else{
                    cell.viewNRCButton.isHidden = true
                    cell.viewNRCTF.isHidden = false
                    
                    var fnt = UIFont.systemFont(ofSize: 18)
                    if ok_default_language == "my" {
                        fnt = UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18.0)
                    }
                    
                    let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: fnt])
                    cell.btnnrc.frame = CGRect(x: 0, y: 0, width:stringSize.width + 12, height: 52)
                    cell.btnnrc.setTitleColor(.black, for: .normal)
                    cell.btnnrc.titleLabel?.font =  UIFont(name: appFont, size:appButtonSize)
                    cell.btnnrc.addTarget(self, action: #selector(self.BtnNrcTypeAction(button:)), for: .touchUpInside)
                    cell.btnnrc.setTitle(nrcDetails, for: UIControl.State.normal)
                    let btnView = UIView()
                    btnView.frame = CGRect(x: 0, y: 0, width: cell.btnnrc.frame.width , height: 52)
                    cell.tfNRC.leftView = nil
                    btnView.addSubview(cell.btnnrc)
                    cell.tfNRC.leftView = btnView
                    cell.tfNRC.leftViewMode = UITextField.ViewMode.always
                    self.showNRCSuggestion()
                }
               
            } else {
                self.reloadRows(index: 2, withSeciton: 2)
                self.showNRCSuggestion()
            }
        })
    }
    
    @objc func BtnNrcTypeAction(button : UIButton) {
        self.showNRCSuggestion()
    }
    private func showNRCSuggestion() {
        self.view.endEditing(true)
//
//        if self.tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 2)) as? NRCNumberRRCell == nil {
//            self.idTyperRowCount = 3
//            self.insertRow(index: 2, withSeciton: 2)
//        }
            if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 2)) as? NRCNumberRRCell {
            let sb = UIStoryboard(name: "Registration" , bundle: nil)
            let nRCTypesVC  = sb.instantiateViewController(withIdentifier: "NRCTypesVC") as! NRCTypesVC
            nRCTypesVC.modalPresentationStyle = .overCurrentContext
            nRCTypesVC.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            nRCTypesVC.setName(nrcName: (cell.btnnrc.titleLabel?.text)!)
            nRCTypesVC.nRCTypesVCDelegate = self
            self.present(nRCTypesVC, animated: false, completion: nil)
            cell.tfNRC.resignFirstResponder()
        }
    }
    
    func setNrcTypeName(nrcType : String) {
        if let cell = tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 2)) as? NRCNumberRRCell {
            let myString = nrcType
            
            if myString == ""{
                self.resetIDType()
            }
            else{
                var fnt = UIFont.systemFont(ofSize: 18)
                if ok_default_language == "my" {
                    fnt = UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18.0)
                }
            cell.btnnrc.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
            let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: fnt])
            cell.btnnrc.frame = CGRect(x: 0, y: 2, width:stringSize.width + 12, height: 52)
            cell.btnnrc.setTitle(nrcType, for: .normal)
            let btnView = UIView()
            btnView.frame = CGRect(x: 0, y: 0, width: cell.btnnrc.frame.width , height: 52)
            btnView.addSubview(cell.btnnrc)
            cell.tfNRC.leftView = nil
            cell.tfNRC.leftView = btnView
            cell.tfNRC.leftViewMode = UITextField.ViewMode.always
            if (cell.tfNRC.text?.length)! < 6 {
                cell.tfNRC.becomeFirstResponder()
            }
        
        }
    }
    }
    func showNRCSuggestionView() {
     self.showNRCSuggestion()
    }
    
    func showCountryList() {
        self.view.endEditing(true)
        if ReRegistrationModel.shared.Language == "my" || ReRegistrationModel.shared.Language == "uni" {
            alertViewObj.wrapAlert(title: "", body: "If you want to Register with passport details than you need to change language".localized, img:#imageLiteral(resourceName: "r_user"))
            alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
            })
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                self.changeCurrentLanguage(language: "en")
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }else {
            ReRegistrationModel.shared.IDType = "04"
            isTypeSelected = "PASSPORT"
            if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 0, section: 2)) as? IDTypeHeaderRRCell {
                cell.btnReset.isUserInteractionEnabled = false
            }
            if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 1, section: 2)) as? SelectIDTypeRRCell {
                if !cell.isHidden {
                    cell.isHidden = true
                    self.reloadRows(index: 1, withSeciton: 2)
                    if self.tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 2)) as? PassportCountryRRCell == nil {
                        self.idTyperRowCount = 3
                        self.insertRow(index: 2, withSeciton: 2)
                    }
                }
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 0, section: 2)) as? IDTypeHeaderRRCell {
                    cell.btnReset.isUserInteractionEnabled = true
                }
            })
            
        }
    }
    
    func showCountryVC() {
         DispatchQueue.main.async {
        let sb = UIStoryboard(name: "Country", bundle: nil)
        let countryVC = sb.instantiateViewController(withIdentifier: "CountryViewController") as! CountryViewController
        countryVC.delegate = self
        countryVC.regiCheck = "removeMyanmarCountry"
        self.countryListShownFor = "removeMyanmarCountry"
        self.navigationController?.present(countryVC, animated: false, completion: nil)
        }
    }
  
    func countryViewController(_ list: CountryViewController, country: Country) {
        if countryListShownFor == "removeMyanmarCountry" {
            ReRegistrationModel.shared.CountryOfCitizen = country.name
            ReRegistrationModel.shared.IDType = "04"
            if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 0, section: 2)) as? IDTypeHeaderRRCell {
                cell.btnReset.isUserInteractionEnabled = false
            }
            DispatchQueue.main.async {
                if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 2)) as? PassportCountryRRCell {
                    cell.btnSelectCountry.setTitle(country.name,for: .normal)
                    cell.btnSelectCountry.setTitleColor(UIColor.black, for: .normal)
                    cell.imgCountry.image = UIImage(named: country.code)
                    ReRegistrationModel.shared.countryImage = country.code
                    if cell.isNextViewHidden {
                        cell.isNextViewHidden = false
                        if self.tableViewRagi.cellForRow(at: IndexPath(row: 3, section: 2)) as? PassportNumberRRCell == nil {
                            self.idTyperRowCount = 4
                            self.insertRow(index: 3, withSeciton: 2)
                        }
                    }
                }
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 0, section: 2)) as? IDTypeHeaderRRCell {
                    cell.btnReset.isUserInteractionEnabled = true
                }
            })
            
            if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 3, section: 2)) as? PassportNumberRRCell {
                cell.tfPassportNo.placeholder = "Enter Passport Number".localized
                cell.tfPassportNo.text = ""
                cell.tfPassportNo.becomeFirstResponder()
                ReRegistrationModel.shared.NRC = ""
            }
            if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 4, section: 2)) as? PassportExpiryDateRRCell {
                cell.tfDate_Of_Expiry.placeholder = "Enter Passport Expiry Date".localized
                cell.tfDate_Of_Expiry.text = ""
                ReRegistrationModel.shared.IdExpDate = ""
            }
            
            if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 5, section: 2)) as? IDPhotoRRCell {
                cell.imgFront.image = UIImage(named: "r_nrc")
                cell.imgBack.image = UIImage(named: "r_nrc")
                ReRegistrationModel.shared.IdPhotoAwsUrl = ""
                ReRegistrationModel.shared.IdPhoto1AwsUrl = ""
            }
        }else if countryListShownFor == "alternate" {
            ReRegistrationModel.shared.Phone = ""
            ReRegistrationModel.shared.CodeAlternate = ""
            flagCodeAlternate = country.code
            countryCodeAlternate = country.dialCode
            AlternatePhoneNumber = ""
            AlternateConfirmPhoneNumber = ""
            if let cell = tableViewRagi.cellForRow(at: IndexPath(row: 1, section: 7)) as? AlternatNumberRRCell {
                cell.btnClose.isHidden = true
                reloadRows(index: 1, withSeciton: 7)
            }
            if let cell = tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 7)) as? AlternateConfirmNumberRRCell {
                cell.btnClose.isHidden = true
                self.AlternateConfirmNumber = false
                reloadRows(index: 2, withSeciton: 7)
                
            }
        }else if countryListShownFor == "referral" {
            ReRegistrationModel.shared.Recommended = ""
            ReRegistrationModel.shared.CodeRecommended = ""
            flagCodeReferral = country.code
            countryCodeReferral = country.dialCode
            ReferralPhoneNumber = ""
            ReferralConfirmPhoneNumber = ""
            if let cell = tableViewRagi.cellForRow(at: IndexPath(row: 1, section: 8)) as? ReferralNumberRRCell {
                cell.btnClose.isHidden = true
                reloadRows(index: 1, withSeciton: 8)
            }
            if let cell = tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 8)) as? ReferralConfirmNumberRRCell {
                cell.btnClose.isHidden = true
                self.ReferralConfirmNumber =  false
                reloadRows(index: 2, withSeciton: 8)
            }
        }
        list.dismiss(animated: false, completion: nil)
    }
    
    func countryViewControllerCloseAction(_ list: CountryViewController) {
        list.dismiss(animated: false, completion: nil)
    }
    
    func showPassportExpiryDateView() {
        self.idTyperRowCount = 5
       // self.insertRow(index: 4, withSeciton: 2)
           let indexPath = IndexPath(row: 4, section: 2)
        self.tableViewRagi.insertRows(at: [indexPath], with: .none)
        UIView.animate(withDuration: 0.5, animations: {
            _ = Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false) { timer in
                self.tableViewRagi.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: false)
            }
        })
    }
    
    func showIDPhoto() {
        isIDPhotoAttached = false
        if ReRegistrationModel.shared.IDType == "01" {
             DispatchQueue.main.async {
            if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 2)) as? NRCNumberRRCell {
                if cell.isNextViewHidden {
                    cell.isNextViewHidden = false
                    if self.tableViewRagi.cellForRow(at: IndexPath(row: 3, section: 2)) as? IDPhotoRRCell == nil {
                        self.idTyperRowCount = 4
                        self.insertRow(index: 3, withSeciton: 2)
                    }
                }
                }
            }
        }else {
            if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 4, section: 2)) as? PassportExpiryDateRRCell {
                if cell.isNextViewHidden {
                    cell.isNextViewHidden = false
                    self.idTyperRowCount = 6
                    self.insertRow(index: 5, withSeciton: 2)
                }
            }
        }
        
    }
    
    func navigateToNRCImage() {
        DispatchQueue.main.async {
            let imageVC = UIStoryboard(name: "Registration", bundle: Bundle.main).instantiateViewController(withIdentifier: "NRCPhtotoUploadwithScan")  as! NRCPhtotoUploadwithScan
            if ReRegistrationModel.shared.IDType == "01" {
                imageVC.setMarqueLabelInNavigation(title: "NRC Images".localized)
                imageVC.buttonName = ["Tap to take Photo of your NRC's front side".localized, "Tap to take Photo of your NRC's back side".localized]
                imageVC.buttonTitleName =  ["Front side of your NRC".localized,"Back side of your NRC".localized]
                imageVC.screenTitle = "NRC Images"
            }else {
                imageVC.setMarqueLabelInNavigation(title: "Passport Images".localized)
                imageVC.buttonName = ["Tap to take Main Page of your Passport".localized, "Tap to take Visa or Residence page of your Passport".localized]
                imageVC.buttonTitleName =  ["Main Page of your Passport".localized,"Visa or Residence page of your Passport".localized]
                imageVC.screenTitle = "Passport Images"
            }
            imageVC.imageList = self.imageListIDType
            imageVC.imagesURL = [ReRegistrationModel.shared.IdPhotoAwsUrl,ReRegistrationModel.shared.IdPhoto1AwsUrl]
            
            imageVC.delegate = self
            self.navigationController?.pushViewController(imageVC, animated: true)
            NotificationCenter.default.addObserver(self, selector: #selector(IDProofVC.setNRCNumber), name: NSNotification.Name(rawValue: "NRC_NUMBER_CARD"), object: nil)
        }
    }
    
    func sendImagesToMainController(images: [String],screen: String, imagArr: [UIImage]) {
        if screen == "NRC Images" || screen == "Passport Images" {
            ReRegistrationModel.shared.IdPhotoAwsUrl = images[0].replacingOccurrences(of: " ", with: "%20")
            ReRegistrationModel.shared.IdPhoto1AwsUrl = images[1].replacingOccurrences(of: " ", with: "%20")
            var rowIndex = 0
            if ReRegistrationModel.shared.IDType == "01" {
                rowIndex = 3
            }else {
                rowIndex = 5
            }
            
            if let cell = tableViewRagi.cellForRow(at: IndexPath(row: rowIndex, section: 2)) as? IDPhotoRRCell {
                
                if !isIDPhotoAttached {
                    if (ReRegistrationModel.shared.IdPhotoAwsUrl != "") && (ReRegistrationModel.shared.IdPhoto1AwsUrl != "") {
                        cell.isNextViewHidden = false
                        isIDPhotoAttached = true
                        if ReRegistrationModel.shared.AccountType == 0 {
                            if isBusinessViewHidden {
                                isBusinessViewHidden = false
                                businessRowCount = 1
                                insertRow(index: 0, withSeciton: 3)
                                businessRowCount = 2
                                insertRow(index: 1, withSeciton: 3)
                            }
                        }else {
                            if isAddressViewHidden {
                                isAddressViewHidden = false
                                addressRowCount = 1
                                insertRow(index: 0, withSeciton: 4)
                                addressRowCount = 2
                                insertRow(index: 1, withSeciton: 4)
                                addressRowCount = 3
                                insertRow(index: 2, withSeciton: 4)
                                addressRowCount = 4
                                insertRow(index: 3, withSeciton: 4)
                                addressRowCount = 5
                                insertRow(index: 4, withSeciton: 4)
                                addressRowCount = 6
                                insertRow(index: 5, withSeciton: 4)
                                addressRowCount = 7
                                insertRow(index: 6, withSeciton: 4)
                                addressRowCount = 8
                                insertRow(index: 7, withSeciton: 4)
                                addressRowCount = 9
                                insertRow(index: 8, withSeciton: 4)
                            }
                        }
                    }
                }
                imageListIDType = imagArr
                self.reloadRows(index: rowIndex, withSeciton: 2)
            }
        }else if screen == "businessRR" {
            ReRegistrationModel.shared.BusinessIdPhoto1AwsUrl = images[0].replacingOccurrences(of: " ", with: "%20")
            ReRegistrationModel.shared.BusinessIdPhoto2AwsUrl = images[1].replacingOccurrences(of: " ", with: "%20")
            isIDBusinessAttached = true
            self.reloadRows(index: 4, withSeciton: 3)
            if let cell = tableViewRagi.cellForRow(at: IndexPath(row: 4, section: 3)) as? BusinessPhotoRRCell {
                if isBusinessViewHidden {
                    if (ReRegistrationModel.shared.BusinessIdPhoto1AwsUrl != "") && (ReRegistrationModel.shared.BusinessIdPhoto2AwsUrl != "") {
                    isBusinessViewHidden = false
                    if cell.isNextViewHidden {
                        cell.isNextViewHidden = false
                        if ReRegistrationModel.shared.lType == "AGENT" {
                            self.businessRowCount = 6
                            self.insertRow(index: 5, withSeciton: 3)
                        }else {
                            addressRowCount = 1
                            insertRow(index: 0, withSeciton: 4)
                            addressRowCount = 2
                            insertRow(index: 1, withSeciton: 4)
                            addressRowCount = 3
                            insertRow(index: 2, withSeciton: 4)
                            addressRowCount = 4
                            insertRow(index: 3, withSeciton: 4)
                            addressRowCount = 5
                            insertRow(index: 4, withSeciton: 4)
                            addressRowCount = 6
                            insertRow(index: 5, withSeciton: 4)
                            addressRowCount = 7
                            insertRow(index: 6, withSeciton: 4)
                            addressRowCount = 8
                            insertRow(index: 7, withSeciton: 4)
                            addressRowCount = 9
                            insertRow(index: 8, withSeciton: 4)
                        }
                        }
                    }
                }
                cell.imgFront.sd_setImage(with: URL(string: ReRegistrationModel.shared.BusinessIdPhoto1AwsUrl), placeholderImage: UIImage(named: "nrc"))
                cell.imgBack.sd_setImage(with: URL(string: ReRegistrationModel.shared.BusinessIdPhoto2AwsUrl), placeholderImage: UIImage(named: "nrc"))
            }
        }else if screen == "agentRR" {
            ReRegistrationModel.shared.ShopPicOneAwsUrl = images[0].replacingOccurrences(of: " ", with: "%20")
            ReRegistrationModel.shared.ShopPicTwoAwsUrl = images[1].replacingOccurrences(of: " ", with: "%20")
            isIDAgentAttached = true
            self.reloadRows(index: 5, withSeciton: 3)
            if let cell = tableViewRagi.cellForRow(at: IndexPath(row: 5, section: 3)) as? AgentShopImages {
                if cell.isNextViewHidden {
                    if (ReRegistrationModel.shared.ShopPicOneAwsUrl != "") && (ReRegistrationModel.shared.ShopPicTwoAwsUrl != "") {
                        if cell.isNextViewHidden {
                            cell.isNextViewHidden = false
                            addressRowCount = 1
                            insertRow(index: 0, withSeciton: 4)
                            addressRowCount = 2
                            insertRow(index: 1, withSeciton: 4)
                            addressRowCount = 3
                            insertRow(index: 2, withSeciton: 4)
                            addressRowCount = 4
                            insertRow(index: 3, withSeciton: 4)
                            addressRowCount = 5
                            insertRow(index: 4, withSeciton: 4)
                            addressRowCount = 6
                            insertRow(index: 5, withSeciton: 4)
                            addressRowCount = 7
                            insertRow(index: 6, withSeciton: 4)
                            addressRowCount = 8
                            insertRow(index: 7, withSeciton: 4)
                            addressRowCount = 9
                            insertRow(index: 8, withSeciton: 4)
                        }
                    }
                }
                cell.imgFront.sd_setImage(with: URL(string: ReRegistrationModel.shared.ShopPicOneAwsUrl), placeholderImage: UIImage(named: "nrc"))
                cell.imgBack.sd_setImage(with: URL(string: ReRegistrationModel.shared.ShopPicTwoAwsUrl), placeholderImage: UIImage(named: "nrc"))
            }
        }
    }
    
    @objc func setNRCNumber() {
        
        if ReRegistrationModel.shared.ocr_F_nrcid.contains(find: ")") {
            var fnt = UIFont.systemFont(ofSize: 18)
            if ok_default_language == "my" {
                fnt = UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18.0)
            }
            
            let nrcNum = ReRegistrationModel.shared.ocr_F_nrcid.components(separatedBy: ")")
            let myString = nrcNum[0] + ")"
            let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: fnt ])
            var finalEnglishNRC = ""
            //Mapping burmese Number: ၁၂၃၄၅၆၇၈၉၀
            for char in nrcNum[1].map(String.init) {
                if char == "၁" {
                    finalEnglishNRC = finalEnglishNRC + "1"
                }else if char == "၂" {
                    finalEnglishNRC = finalEnglishNRC + "2"
                }else if char == "၃" {
                    finalEnglishNRC = finalEnglishNRC + "3"
                }else if char == "၄" {
                    finalEnglishNRC = finalEnglishNRC + "4"
                }else if char == "၅" {
                    finalEnglishNRC = finalEnglishNRC + "5"
                }else if char == "၆" {
                    finalEnglishNRC = finalEnglishNRC + "6"
                }else if char == "၇" {
                    finalEnglishNRC = finalEnglishNRC + "7"
                }else if char == "၈" {
                    finalEnglishNRC = finalEnglishNRC + "8"
                }else if char == "၉" {
                    finalEnglishNRC = finalEnglishNRC + "9"
                }else if char == "၀" {
                    finalEnglishNRC = finalEnglishNRC + "0"
                }
            }

            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 2)) as? NRCNumberRRCell {
                    cell.btnnrc.frame = CGRect(x: 0, y: 40, width:stringSize.width + 12, height: 52)
                    cell.btnnrc.setTitleColor(.black, for: .normal)
                    cell.btnnrc.titleLabel?.font =  fnt
                    cell.btnnrc.addTarget(self, action: #selector(self.BtnNrcTypeAction(button:)), for: .touchUpInside)
                    cell.btnnrc.setTitle(myString, for: UIControl.State.normal)
                    let btnView = UIView()
                    btnView.frame = CGRect(x: 0, y: 40, width: cell.btnnrc.frame.width , height: 52)
                    btnView.addSubview(cell.btnnrc)
                    cell.tfNRC.leftView = btnView
                    cell.tfNRC.leftViewMode = UITextField.ViewMode.always
                    cell.tfNRC.text = finalEnglishNRC
                    let helpingFuntion = HelpingFunctionR()
                    ReRegistrationModel.shared.NRC = helpingFuntion.charecterReplace(charecter: "/", replacingWith: "@", mainString: cell.btnnrc.title(for: .normal)! + cell.tfNRC.text!)
                    ReRegistrationModel.shared.ocr_F_nrcid = ReRegistrationModel.shared.NRC
                }
            })
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "NRC_NUMBER_CARD"), object: nil)
    }
    
    
    func showIDTypeInfo() {
        var strURL = ""
        if ReRegistrationModel.shared.Language.lowercased() == "my"{
            strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/1c40c13b-57c4-4c91-8f94-a79fe4badc8fReqDocumentsJuly_02_2018%2001_24_29%20PM.jpg";
        }else {
            strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/813d381e-40da-4917-8539-7774225e6f82ReqDocumentsJuly_02_2018%2001_32_50%20PM.jpg";
        }
        
        self.view.endEditing(true)
        webviewContainer.isHidden = false
        webview.scalesPageToFit = true
        webview.delegate = self
        lblDemo.text = "Select ID Type".localized
        webview.loadRequest(URLRequest(url: URL(string: strURL)!))
    }

    }

