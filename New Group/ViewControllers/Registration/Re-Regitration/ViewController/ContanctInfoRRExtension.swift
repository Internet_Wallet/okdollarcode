//
//  ContanctInfoRRExtension.swift
//  OK
//
//  Created by Imac on 6/3/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

extension ReRegistrationVC: ContactHeaderRRCellDelegate,AlternatNumberRRCellDelegate,AlternateConfirmNumberRRCellDelegate {
    
    func resetAlternateNumber() {
        ReRegistrationModel.shared.Phone = ""
        ReRegistrationModel.shared.CodeAlternate = ""
        if let cell = tableViewRagi.cellForRow(at: IndexPath(row: 1, section: 7)) as? AlternatNumberRRCell {
            cell.btnClose.isHidden = true
            cell.imgCountryFlag.image =  UIImage(named: "myanmar")
            cell.lblCountyCode.text = "(+95)"
            cell.tfAlternateNumber.text = "09"
            flagCodeAlternate = "myanmar"
            countryCodeAlternate = "+95"
            AlternatePhoneNumber = ""
            AlternateConfirmPhoneNumber = ""
        }
        self.AlternateConfirmNumber = false
        reloadRows(index: 2, withSeciton: 7)
    }
    func closeAlternateNumber() {
        ReRegistrationModel.shared.Phone = ""
        ReRegistrationModel.shared.CodeAlternate = ""
        if let cell = tableViewRagi.cellForRow(at: IndexPath(row: 1, section: 7)) as? AlternatNumberRRCell {
            cell.btnClose.isHidden = true
            if cell.imgCountryFlag.image == UIImage(named: "myanmar") {
                cell.tfAlternateNumber.text = "09"
            }else {
                cell.tfAlternateNumber.text = ""
            }
            //cell.tfAlternateNumber.resignFirstResponder()
        }
        AlternatePhoneNumber = ""
        AlternateConfirmPhoneNumber = ""
        self.AlternateConfirmNumber = false
        reloadRows(index: 2, withSeciton: 7)
    }
    
    func endField(text : String) {
        var num = text
        var c_code = countryCodeAlternate
        AlternatePhoneNumber  = num
        c_code.removeFirst()
        if flagCodeAlternate == "myanmar" {
            num.removeFirst()
            ReRegistrationModel.shared.Phone = "00" + c_code + num
        }else {
            ReRegistrationModel.shared.Phone = "00" + num
        }
         ReRegistrationModel.shared.CodeAlternate = c_code
       //
    }
    
    func editingField(text: String) {
        
        if numberSelectedFromContact {
            numberSelectedFromContact = false
            self.AlternateConfirmNumber = false
            contactNameAlternate = ""
            reloadRows(index: 2, withSeciton: 7)
        }
        
        
        let mbLength = validObj.getNumberRangeValidation(text)
        PersonalProfileModelUP.shared.AlternatePhonenumber = ""
        
        if let cell = tableViewRagi.cellForRow(at: IndexPath(row: 1, section: 7)) as? AlternatNumberRRCell {
            if cell.imgCountryFlag.image == UIImage(named: "myanmar") {
                if text.count < mbLength.min {
                    if self.AlternateConfirmNumber {
                        if let _ = tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 7)) as? AlternateConfirmNumberRRCell {
                           self.AlternateConfirmNumber = false
                            AlternateConfirmPhoneNumber = ""
                            reloadRows(index: 2, withSeciton: 7)
                        }
                    }
                }else if text.count > mbLength.min {
                    if !AlternateConfirmNumber {
                        if let _ = tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 7)) as? AlternateConfirmNumberRRCell {
                            self.AlternateConfirmNumber = true
                            reloadRows(index: 2, withSeciton: 7)
                        }
                    }
                }else if text.count == mbLength.max {
                    cell.tfAlternateNumber.resignFirstResponder()
                    if let cc = tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 7)) as? AlternateConfirmNumberRRCell {
                         self.AlternateConfirmNumber = true
                        self.reloadRows(index: 2, withSeciton: 7)
                            UIView.animate(withDuration: 1.0, animations: {
                                _ = Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false) { timer in
                                    let indexPath = IndexPath(row: 2, section: 7)
                                    self.tableViewRagi.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                                }
                            })
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                          cc.tfConfirmMobilNumber.becomeFirstResponder()
                        })
                    }
                }
            }else {
                if text.count < 4 {
                    if self.AlternateConfirmNumber {
                        if let _ = tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 7)) as? AlternateConfirmNumberRRCell {
                            self.AlternateConfirmNumber = false
                            AlternateConfirmPhoneNumber = ""
                            reloadRows(index: 2, withSeciton: 7)
                        }
                    }
                }else if text.count > 3 && text.count < 13 {
                    if !AlternateConfirmNumber {
                        self.AlternateConfirmNumber = true
                        reloadRows(index: 2, withSeciton: 7)
                    }
                }else if text.count == 13 {
                    cell.tfAlternateNumber.resignFirstResponder()
                    self.AlternateConfirmNumber = true
                    reloadRows(index: 2, withSeciton: 7)
                }
            }
        }
    }
    
    func editChangedAlternateConfirme(text : String) {
        
        if validObj.checkMatchingNumber(string: text, withString: AlternatePhoneNumber) {
            if text == AlternatePhoneNumber {
                if let cell = tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 7)) as? AlternateConfirmNumberRRCell {
                    cell.tfConfirmMobilNumber.resignFirstResponder()
                    if cell.isNextFieldHidden {
                        cell.isNextFieldHidden = false
                        if isAddtitionalInformatonHiddenForPersonal {
                             isAddtitionalInformatonHiddenForPersonal = false
                            ReferralRowCount = 1
                            self.insertRow(index: 0, withSeciton: 8)
                            ReferralRowCount = 2
                            self.insertRow(index: 1, withSeciton: 8)
                            ReferralRowCount = 3
                            self.insertRow(index: 2, withSeciton: 8)
                            
                            self.AdditonalRowCount = 1
                            self.insertRow(index: 0, withSeciton: 9)
                            self.AdditonalRowCount = 2
                            self.insertRow(index: 1, withSeciton: 9)
                            self.AdditonalRowCount = 3
                            self.insertRow(index: 2, withSeciton: 9)
                        }
                    }
                }
            }
            AlternateConfirmPhoneNumber = text
        }else {
            if let cell = tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 7)) as? AlternateConfirmNumberRRCell {
                cell.tfConfirmMobilNumber.text?.removeLast()
                AlternateConfirmPhoneNumber = String(text.dropLast())
            }
        }
    }
    func endTextFieldAltrenateNumber(text : String) {
   
    }
    
    func clearAlternateConfirmNumber() {
        if let cell = tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 7)) as? AlternateConfirmNumberRRCell {
            cell.btnClose.isHidden = true
            if cell.imgCountryFlag.image == UIImage(named: "myanmar") {
                cell.tfConfirmMobilNumber.text = "09"
                AlternateConfirmPhoneNumber = "09"
            }else {
                cell.tfConfirmMobilNumber.text = ""
                AlternateConfirmPhoneNumber = ""
            }
           // cell.tfConfirmMobilNumber.resignFirstResponder()
        }
    }
    func showAlert(text : String) {
        DispatchQueue.main.async {
            self.showAlert(alertTitle: "", alertBody: text, alertImage: UIImage(named: "alert-icon")!)}
    }
    
    
    func showContactView() {
        let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
        self.navigationController?.present(nav, animated: true, completion: nil)
        contactListShownFor = "alternate"
    }
    
    func showAlternateCountryList() {
        let sb = UIStoryboard(name: "Country", bundle: nil)
        let countryVC = sb.instantiateViewController(withIdentifier: "CountryViewController") as! CountryViewController
        countryVC.delegate = self
        countryVC.regiCheck = "AllCountry"
        countryVC.modalPresentationStyle = .fullScreen
        self.navigationController?.present(countryVC, animated: true, completion: nil)
        countryListShownFor = "alternate"
    }
    
    
    
}
