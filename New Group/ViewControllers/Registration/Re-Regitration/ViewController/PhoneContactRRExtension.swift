//
//  PhoneContactRRExtension.swift
//  OK
//
//  Created by Imac on 6/3/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

extension ReRegistrationVC : ContactPickerDelegate {
    func contact(_: ContactPickersPicker, didContactFetchFailed error: NSError){}
    func contact(_: ContactPickersPicker, didCancel error: NSError){}
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker){ decodeContact(contact: contact)}
    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]){}
    func decodeContact(contact: ContactPicker) {
        var countryCode = ""
        var countryFlag = ""
        var tempContryCode = ""
        var tempPhoneNumber = ""
        let phoneNumberCount = contact.phoneNumbers.count
        var phoneNumber = ""
        
        if phoneNumberCount == 1  {
            phoneNumber = "\(contact.phoneNumbers[0].phoneNumber)"
        }
        else if phoneNumberCount > 1 {
            phoneNumber = "\(contact.phoneNumbers[0].phoneNumber)"
        }else {}
        
        if phoneNumber != "" {
            phoneNumber = phoneNumber.replacingOccurrences(of: ")", with: "")
            phoneNumber = phoneNumber.replacingOccurrences(of: "(", with: "")
            phoneNumber = phoneNumber.trimmingCharacters(in: .whitespaces)
        }
        
        let cDetails = identifyCountry(withPhoneNumber: phoneNumber)
        if cDetails.0 == "" || cDetails.1 == "" {
            countryFlag = "myanmar"
            countryCode = "+95"
            tempContryCode = countryCode
        } else {
            countryFlag = cDetails.1
            countryCode = cDetails.0
            tempContryCode = countryCode
        }
        println_debug(tempContryCode)
        let phone = phoneNumber.replacingOccurrences(of: cDetails.0, with: "0")
        if phone.hasPrefix("0") {
            if countryFlag == "myanmar" {
                tempPhoneNumber = phone
            } else {
                tempPhoneNumber = phoneNumber.replacingOccurrences(of: cDetails.0, with: "")
            }
        } else {
            if countryFlag == "myanmar" {
                tempPhoneNumber =   "0" + phone
            } else {
                tempPhoneNumber =  phone
            }
        }
        
        countryCode.remove(at: countryCode.startIndex)
        if tempPhoneNumber.hasPrefix("0"){
            tempPhoneNumber.remove(at: tempPhoneNumber.startIndex)
        }
        let finalMob =  "00" + countryCode + tempPhoneNumber
        let noToCheck = "0" + tempPhoneNumber
        if finalMob.count > 8 {
            if countryCode == "95" {
                let mbLength = (validObj.getNumberRangeValidation(noToCheck))
                if !mbLength.isRejected {
                    if noToCheck.count < mbLength.min || noToCheck.count > mbLength.max {
                        self.clearNumberFromDB()
                        DispatchQueue.main.async {
                            self.showAlert(alertTitle: "", alertBody: "You selected invalid number, Please try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))}
                    }else {
                        if self.contactListShownFor == "alternate" {
                            numberSelectedFromContact = true
                            let countryData = identifyCountryByCode(withPhoneNumber: "+" + countryCode)
                            ReRegistrationModel.shared.Phone = finalMob
                            ReRegistrationModel.shared.CodeAlternate = countryData.countryCode
                            flagCodeAlternate = countryData.countryFlag
                            countryCodeAlternate =  countryData.countryCode
                            contactNameAlternate = contact.displayName()
                            AlternatePhoneNumber = noToCheck
                            println_debug("Final Number :\(ReRegistrationModel.shared.Phone) ,flag = \(flagCodeAlternate) , CountryCode: \(countryCodeAlternate)")
                            self.reloadRows(index: 1, withSeciton: 7)
                            self.AlternateConfirmNumber = true
                            self.reloadRows(index: 2, withSeciton: 7)
                            
                            if isAddtitionalInformatonHiddenForPersonal {
                                isAddtitionalInformatonHiddenForPersonal = false
                                ReferralRowCount = 1
                                self.insertRow(index: 0, withSeciton: 8)
                                ReferralRowCount = 2
                                self.insertRow(index: 1, withSeciton: 8)
                                ReferralRowCount = 3
                                self.insertRow(index: 2, withSeciton: 8)
                                
                                self.AdditonalRowCount = 1
                                self.insertRow(index: 0, withSeciton: 9)
                                self.AdditonalRowCount = 2
                                self.insertRow(index: 1, withSeciton: 9)
                                self.AdditonalRowCount = 3
                                self.insertRow(index: 2, withSeciton: 9)
                            }
                        }else if self.contactListShownFor == "referral" {
                              numberReferrralSelectedFromContact = true
                            let countryData = identifyCountryByCode(withPhoneNumber: "+" + countryCode)
                            ReRegistrationModel.shared.Recommended = finalMob
                            ReRegistrationModel.shared.CodeRecommended = countryData.countryCode
                            flagCodeReferral = countryData.countryFlag
                            countryCodeReferral =  countryData.countryCode
                            contactNameReferral = contact.displayName()
                            ReferralPhoneNumber = noToCheck
                            println_debug("Final Number :\(ReRegistrationModel.shared.Recommended) ,flag = \(flagCodeReferral) , CountryCode: \(countryCodeReferral)")
                            self.reloadRows(index: 1, withSeciton: 8)
                            self.ReferralConfirmNumber = true
                            self.reloadRows(index: 2, withSeciton: 8)
                        }
                    }
                }else{
                    self.clearNumberFromDB()
                    DispatchQueue.main.async {
                        self.showAlert(alertTitle: "", alertBody: "You selected invalid number, Please try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))}
                }
            }else{
               
                if self.contactListShownFor == "alternate" {
                    numberSelectedFromContact = true
                    let countryData = identifyCountryByCode(withPhoneNumber: "+" + countryCode)
                    ReRegistrationModel.shared.Phone = finalMob
                    ReRegistrationModel.shared.CodeAlternate = countryData.countryCode
                    flagCodeAlternate = countryData.countryFlag
                    countryCodeAlternate =  countryData.countryCode
                    contactNameAlternate = contact.displayName()
                    AlternatePhoneNumber = tempPhoneNumber
                    println_debug("Final Number :\(ReRegistrationModel.shared.Phone) ,flag = \(flagCodeAlternate) , CountryCode: \(countryCodeAlternate)")
                    self.reloadRows(index: 1, withSeciton: 7)
                    self.AlternateConfirmNumber = true
                    self.reloadRows(index: 2, withSeciton: 7)
                    
                    if isAddtitionalInformatonHiddenForPersonal {
                        isAddtitionalInformatonHiddenForPersonal = false
                        ReferralConfirmNumber = false
                        ReferralRowCount = 1
                        self.insertRow(index: 0, withSeciton: 8)
                        ReferralRowCount = 2
                        self.insertRow(index: 1, withSeciton: 8)
                        ReferralRowCount = 3
                        self.insertRow(index: 2, withSeciton: 8)
                        
                        self.AdditonalRowCount = 1
                        self.insertRow(index: 0, withSeciton: 9)
                        self.AdditonalRowCount = 2
                        self.insertRow(index: 1, withSeciton: 9)
                        self.AdditonalRowCount = 3
                        self.insertRow(index: 2, withSeciton: 9)
                    }
                }else if self.contactListShownFor == "referral" {
                      numberReferrralSelectedFromContact = true
                    let countryData = identifyCountryByCode(withPhoneNumber: "+" + countryCode)
                    ReRegistrationModel.shared.Recommended = finalMob
                    ReRegistrationModel.shared.CodeRecommended = countryData.countryCode
                    flagCodeReferral = countryData.countryFlag
                    countryCodeReferral =  countryData.countryCode
                    contactNameReferral = contact.displayName()
                    ReferralPhoneNumber = tempPhoneNumber
                    println_debug("Final Number :\(ReRegistrationModel.shared.Recommended) ,flag = \(countryCodeReferral) , CountryCode: \(contactNameReferral)")
                    self.reloadRows(index: 1, withSeciton: 8)
                     self.ReferralConfirmNumber = true
                    self.reloadRows(index: 2, withSeciton: 8)
                }
            }
        }else{
            DispatchQueue.main.async {
                self.clearNumberFromDB()
                self.showAlert(alertTitle: "", alertBody: "You selected invalid number, Please try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
            }
        }
    }
    
    func clearNumberFromDB() {
        if self.contactListShownFor == "alternate" {
            ReRegistrationModel.shared.Phone = ""
            ReRegistrationModel.shared.CodeAlternate = ""
        }else if self.contactListShownFor == "referral" {
            ReRegistrationModel.shared.Recommended = ""
            ReRegistrationModel.shared.CodeRecommended = ""
        }
    }
}
