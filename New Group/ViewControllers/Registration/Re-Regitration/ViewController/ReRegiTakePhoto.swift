//
//  ReRegiTakePhoto.swift
//  OK
//
//  Created by Imac on 6/18/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol ReRegiTakePhotoDelegate {
    func imagewithURLs(fromScreen: String, imageURLs: [String], images: [UIImage])
}


class ReRegiTakePhoto: OKBaseController, UIScrollViewDelegate  {

    var delegate : ReRegiTakePhotoDelegate?
    
    @IBOutlet weak var btnUpdateCancel: UIButton!{
        didSet{
            btnUpdateCancel.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnUpdate: UIButton!{
        didSet{
            btnUpdate.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnCamera : UIButton!{
        didSet{
            btnCamera.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnGallery : UIButton!{
        didSet{
            btnGallery.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var lblSubtitle: UILabel!{
        didSet{
            lblSubtitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var imgViewAttachFile: UIImageView!
    @IBOutlet weak var imgBG: UIImageView!
    
    var imageRotated = 0
    var imageFile :UIImage!
    let imagePicker = UIImagePickerController()
    var strTitle: String?
    var imagesAdded: [UIImage]?
    var imageURLs: [String]?
    var screenFrom: String = ""
    var completeUploadImageMode: String?
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var btnCancel: UIButton!{
        didSet{
            btnCancel.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 10.0
        scrollView.zoomScale = 1.0
        imageRotated = 0
        imagePicker.delegate = self
        btnUpdateCancel.setTitle("Cancel".localized, for : .normal)
        btnCancel.setTitle("Cancel".localized, for : .normal)
        btnUpdate.setTitle("Save".localized, for : .normal)
        btnCamera.setTitle("Take Photo".localized, for : .normal)
        btnGallery.setTitle("Gallery".localized, for : .normal)
     
        if self.completeUploadImageMode ?? "" == "FIRST" {
            imageURLs?.removeAll()
            if screenFrom == "Business" {
             lblSubtitle.text = "Attach Front Registration".localized
            }else {
             lblSubtitle.text = "First Image".localized
            }
        }else if self.completeUploadImageMode ?? "" == "FIRST_SHOW" {
            imgViewAttachFile.image = imagesAdded?[0]
           
            if screenFrom == "Business" {
               lblSubtitle.text = "Attach Front Registration".localized
            }else {
              lblSubtitle.text = "First Image".localized
            }
        }else if self.completeUploadImageMode ?? "" == "SECOND_SHOW" {
            if imagesAdded?.count == 2 {
             imgViewAttachFile.image =  imagesAdded?[1]
            }else {
            imageURLs?.removeLast()
            }
          
            if screenFrom == "Business" {
             lblSubtitle.text = "Attach Back Registration".localized
            }else {
              lblSubtitle.text = "Second Image".localized
            }
        }
        imageFile = imgViewAttachFile.image
        btnCancel.isHidden = false
        stackView.isHidden = true
        setMarqueLabelInNavigation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
    }
    
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: 16)
        lblMarque.text = strTitle ?? ""
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
    }
    @IBAction func imageRotationAction(_ sender: Any) {
        if imageFile == nil {
            return
        }
        var portraitImage  = UIImage()
        if imageRotated == 0 {
            portraitImage = UIImage(cgImage: imageFile.cgImage! ,
                                    scale: 1.0 ,
                                    orientation: UIImage.Orientation.right)
            imageRotated = 90
        }else if imageRotated == 90 {
            portraitImage = UIImage(cgImage: imageFile.cgImage! ,
                                    scale: 1.0 ,
                                    orientation: UIImage.Orientation.down)
            imageRotated = 180
        }else if imageRotated == 180 {
            portraitImage = UIImage(cgImage: imageFile.cgImage! ,
                                    scale: 1.0 ,
                                    orientation: UIImage.Orientation.left)
            imageRotated = 270
        }else {
            portraitImage = UIImage(cgImage: imageFile.cgImage! ,
                                    scale: 1.0 ,
                                    orientation: UIImage.Orientation.up)
            imageRotated = 0
        }
        imgViewAttachFile.image = portraitImage
    }
    
    @IBAction func backAction(_ sender: Any) {
        if  stackView.isHidden == false {
            alertViewObj.wrapAlert(title: nil, body: "Clicking back without update will clear all entered details, Are you sure to exit?".localized, img: UIImage(named: "attachBig")!)
            alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
            }
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                self.navigationController?.popViewController(animated: true)
            }
            alertViewObj.showAlert(controller: self)
        }else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //zoom in/out image for attach file
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imgViewAttachFile
    }
    
    @IBAction func btnOnClickCencel(_ sender: Any) {
        self.navigationController?.popViewController(animated : true)
    }
    

}



extension ReRegiTakePhoto: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBAction func saveAction(_ sender: UIButton) {
        self.imageUploadOnServer()
//        if let del = delegate {
//            del.imagewithURLs(fromScreen: screenFrom, imageURLs: imageURLs ?? [], images: imagesAdded ?? [])
//            self.navigationController?.popViewController(animated : true)
//        }
//        if imageURLs?.count == 0 {
//            alertViewObj.wrapAlert(title: nil, body: "Please Attach Business Registration".localized, img: #imageLiteral(resourceName: "info_one"))
//            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
//            }
//            alertViewObj.showAlert(controller: self)
//        }else {
//        if let del = delegate {
//            del.imagewithURLs(fromScreen: screenFrom, imageURLs: imageURLs ?? [], images: imagesAdded ?? [])
//        }
//        self.navigationController?.popViewController(animated : true)
//        }
    }
    
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        alertViewObj.wrapAlert(title: nil, body: "Clicking back without update will clear all entered details, Are you sure to exit?".localized, img: UIImage(named: "attachBig")!)
        alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
        }
        alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            self.navigationController?.popViewController(animated: true)
        }
        alertViewObj.showAlert(controller: self)
 
    }
    
    @IBAction func getImageFromCamera(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.allowsEditing = false
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.cameraCaptureMode = .photo
            imagePicker.modalPresentationStyle = .fullScreen
            self.present(imagePicker,animated: true,completion: nil)
        }
    }
    
    @IBAction func getImageFromGallery(_ sender: UIButton) {
         let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.modalPresentationStyle = .popover
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let info = convertFromUIImagePickerControllerInfoKeyDictionarytwo(info)
        if let image = info[convertFromUIImagePickerControllerInfoKeytwo(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            imageFile = image
            imgViewAttachFile.image = image
            dismiss(animated: true, completion: {
                //self.imageUploadOnServer()
                self.btnCancel.isHidden = true
                self.stackView.isHidden = false
            })
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[convertFromUIImagePickerControllerInfoKeytwo(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            imageFile = pickedImage
            imgViewAttachFile.image = pickedImage
        }
        dismiss(animated: true, completion: {
            //self.imageUploadOnServer()
            self.btnCancel.isHidden = true
            self.stackView.isHidden = false
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
//        btnUpdate.isHidden = true
//        btnUpdateCancel.isHidden = true
        dismiss(animated: true, completion: nil)
    }
    
}

extension ReRegiTakePhoto: WebServiceResponseDelegate {
    //Upload image to server
    func imageUploadOnServer() {
        showProgressView()
        DispatchQueue.main.async {
            let convertedStr = self.imageFile.base64(format: ImageFormat.jpeg(0.4))
            let paramString : [String:Any] = [
                "MobileNumber":UserModel.shared.mobileNo,
                "Base64String":convertedStr!
            ]
            let web      = WebApiClass()
            web.delegate = self
            let ur = URL(string: "https://www.okdollar.co/RestService.svc/GetImageUrlByBase64String")//getUrl(urlStr: urlStr, serverType: .imageUpload)
            web.genericClass(url: ur!, param: paramString as AnyObject, httpMethod: "POST", mScreen: "mImageUpload")
        }
    }
    
    //MARK:- API Response
    func webResponse(withJson json: AnyObject, screen: String) {
        self.removeProgressView()
        do {
            if let data = json as? Data {
                let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let dic = dict as? Dictionary<String,AnyObject> {
                    let strURL = dic["Data"] as! String
                    if strURL != "" {
                        
                        DispatchQueue.main.async {
                            alertViewObj.wrapAlert(title: nil, body: "Image uploaded successfully".localized, img: #imageLiteral(resourceName: "info_one"))
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                if self.completeUploadImageMode ?? "" == "FIRST" {
                                    if self.screenFrom == "Business" {
                                        if self.lblSubtitle.text == "Attach Front Registration".localized {
                                            self.lblSubtitle.text = "Attach Back Registration".localized
                                            self.btnCancel.isHidden = false
                                            self.stackView.isHidden = true
                                           self.imgViewAttachFile.image = nil
                                        }
                                    }else {
                                        if self.lblSubtitle.text == "First Image".localized {
                                            self.lblSubtitle.text = "Second Image".localized
                                            self.btnCancel.isHidden = false
                                            self.stackView.isHidden = true
                                        }
                                    }
                                    self.imagesAdded?.append(self.imageFile)
                                    self.imageURLs?.append(strURL.replacingOccurrences(of: " ", with: "%20"))
                                    if self.imagesAdded?.count ?? 0 > 1 {
                                        if let del = self.delegate {
                                            del.imagewithURLs(fromScreen: self.screenFrom, imageURLs: self.imageURLs ?? [], images: self.imagesAdded ?? [])
                                            self.navigationController?.popViewController(animated : true)
                                        }
                                    }
                                    
                                }else if self.completeUploadImageMode ?? "" == "FIRST_SHOW" {
                                    self.imagesAdded?[0] = self.imageFile
                                    self.imageURLs?[0] = strURL.replacingOccurrences(of: " ", with: "%20")
                                    if self.imagesAdded?.count ?? 0 > 1 {
                                        if let del = self.delegate {
                                            del.imagewithURLs(fromScreen: self.screenFrom, imageURLs: self.imageURLs ?? [], images: self.imagesAdded ?? [])
                                            self.navigationController?.popViewController(animated : true)
                                        }
                                    }
                                }else if self.completeUploadImageMode ?? "" == "SECOND_SHOW" {
                                    if self.imageURLs?.count == 2 {
                                        self.imagesAdded?[1] = self.imageFile
                                        self.imageURLs?[1] = strURL.replacingOccurrences(of: " ", with: "%20")
                                        if let del = self.delegate {
                                            del.imagewithURLs(fromScreen: self.screenFrom, imageURLs: self.imageURLs ?? [], images: self.imagesAdded ?? [])
                                            self.navigationController?.popViewController(animated : true)
                                        }
                                    }else {
                                        self.imagesAdded?.append(self.imageFile)
                                        self.imageURLs?.append(strURL.replacingOccurrences(of: " ", with: "%20"))
                                        if self.imagesAdded?.count ?? 0 > 1 {
                                            if let del = self.delegate {
                                                del.imagewithURLs(fromScreen: self.screenFrom, imageURLs: self.imageURLs ?? [], images: self.imagesAdded ?? [])
                                                self.navigationController?.popViewController(animated : true)
                                            }
                                        }
                                    }
                                }
                              
                            }
                            alertViewObj.showAlert(controller: self)
                        }
                    }else {
                        alertViewObj.wrapAlert(title: nil, body: "Image uploading failed. Please try again".localized, img: #imageLiteral(resourceName: "info_one"))
                        alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                        }
                        alertViewObj.showAlert(controller: self)
                    }
                }
            }else {
                alertViewObj.wrapAlert(title: nil, body: "Image uploading failed. Please try again".localized, img: #imageLiteral(resourceName: "info_one"))
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                }
                alertViewObj.showAlert(controller: self)
            }
        } catch {}
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionarytwo(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeytwo(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
