//
//  SecurityQuestionVC.swift
//  OK
//
//  Created by Imac on 8/5/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol SecurityQuestionVCREgiDelegate {
    func SelectedQuestion(questionDic : Dictionary<String,String>)
}

class SecurityQuestionVCREgi: OKBaseController,WebServiceResponseDelegate,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var btnCancel: UIButton!
    var delegate: SecurityQuestionVCREgiDelegate?
    var Question_arr = Array<Dictionary<String,String>>()
    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        //securityQuestionLabel.text = "Select Security Question".localized
        btnCancel.setTitle("Cancel".localized, for: .normal)
        setMarqueLabelInNavigation()
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self
            let urlStr   = Url.URL_Security_Question
            let url = getUrl(urlStr: urlStr, serverType: .serverApp)
            println_debug("Security QuestionList url : \(url)")
            let param = [String:String]() as AnyObject
            web.genericClass(url: url, param: param, httpMethod: "GET", mScreen: "mLogin")
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
        }else{
            DispatchQueue.main.async {
                progressViewObj.removeProgressView()
            }
            alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img:#imageLiteral(resourceName: "r_user"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: 16)
        lblMarque.text = "Select Security Question".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.init(hex: "#1C2C99")
        self.navigationItem.titleView = lblMarque
    }

    @IBAction func onClickAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
 
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return Question_arr.count
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let cell:QuestionCell = tableView.dequeueReusableCell(withIdentifier:"QuestionCell") as! QuestionCell
        let cell: UITableViewCell =  UITableViewCell(style: .default, reuseIdentifier: "cell")
        cell.textLabel?.font = UIFont(name: appFont, size: 17.0) ?? UIFont.systemFont(ofSize: 15.0)
        cell.textLabel?.numberOfLines = 0
        let dic = Question_arr[indexPath.row]
        if ok_default_language == "my"{
            cell.textLabel?.text =  dic["QuestionBurmese"]
        }else if ok_default_language == "en"{
            cell.textLabel?.text =  dic["Question"]
        }else {
            cell.textLabel?.text =  dic["QuestionUnicode"]
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard (self.delegate?.SelectedQuestion(questionDic: Question_arr[indexPath.row]) != nil) else {
            return
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        DispatchQueue.main.async {
            progressViewObj.removeProgressView()
        }
        do {
            if let data = json as? Data {
                let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let dic = dict as? Dictionary<String,AnyObject> {
                    println_debug(dic)
                    println_debug(dic["Msg"] as! String)
                    
                    if dic["Msg"] as? String == "Success" {
                        if let Resopnse = dic["Data"] as? String {
                            let dic = OKBaseController.convertToArrDictionary(text: Resopnse)
                            if let log = dic as? Array<Dictionary<String,AnyObject>> {
                                for item in log {
                                    var qCode = ""
                                    var question = ""
                                    var questionB = ""
                                    var qurstionUnicode = ""
                                    if let code = item["QuestionCode"] as? String {
                                        qCode = code
                                    }
                                    if let code = item["Question"] as? String {
                                        question = code
                                    }
                                    if let code = item["QuestionBurmese"] as? String {
                                        questionB = code
                                    }
                                    if let code = item["QuestionBurmeseUniCode"] as? String {
                                        qurstionUnicode = code
                                    }
                                    Question_arr.append(["QuestionCode": qCode,"Question": question,"QuestionBurmese": questionB,"QuestionUnicode": qurstionUnicode])
                                }
                                let tempArr = Question_arr
                                Question_arr.removeAll()
                                Question_arr = tempArr.sorted {$0["Question"]! < $1["Question"]!}
                            }
                            DispatchQueue.main.async {
                                self.tableview.delegate = self
                                self.tableview.dataSource = self
                                self.tableview.reloadData()
                            }
                        }
                    }
                    println_debug(Question_arr)
                }
            }
        } catch {
            alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img:#imageLiteral(resourceName: "r_user"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }

}

class QuestionCell: UITableViewCell {
    @IBOutlet weak var lblQuestion: UILabel!
}
