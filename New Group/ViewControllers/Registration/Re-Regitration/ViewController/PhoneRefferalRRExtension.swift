//
//  PhoneRefferalRRExtension.swift
//  OK
//
//  Created by Imac on 6/4/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit


extension ReRegistrationVC : AdditionalAgentInfoRRCellDelegate,AddtionalInfoRRCellDelegate,ContactReferralHeaderRRCellDelegate,ReferralNumberRRCellDelegate,ReferralConfirmNumberRRCellDelegate,AgenTermConditionDelegate,ReferralVCDelegate {
 
    func showSelectSelfNumber() {
        let sb = UIStoryboard(name: "Registration", bundle: nil)
        let selectVC  = sb.instantiateViewController(withIdentifier: "selectReferralNumberVc") as! selectReferralNumberVc
        selectVC.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        selectVC.delegate = self
        selectVC.modalPresentationStyle = .overCurrentContext
        self.present(selectVC, animated: false, completion: nil)
    }
    
    func dismissWithDetail(selectedOption: String, selfView: UIViewController) {

        if let cell = tableViewRagi.cellForRow(at: IndexPath(row: 1, section: 8)) as? ReferralNumberRRCell {
            if selectedOption == "MySelf".localized {
                cell.btnSelf.isUserInteractionEnabled = false
                cell.tfReferralNumber.isUserInteractionEnabled = false
                let (mobileNumber,CountryCode) = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: ReRegistrationModel.shared.MobileNumber)
                let countryData = identifyCountry(withPhoneNumber: CountryCode)
                countryCodeReferral = countryData.countryCode
                flagCodeReferral = countryData.countryFlag
                cell.imgCountryFlag.image = UIImage(named: countryData.countryFlag)
                cell.tfReferralNumber.text = mobileNumber
                ReferralPhoneNumber = mobileNumber
                ReferralConfirmPhoneNumber = mobileNumber
                ReRegistrationModel.shared.Recommended = ReRegistrationModel.shared.MobileNumber
                ReRegistrationModel.shared.CodeRecommended = countryCodeReferral
            }else {
                 cell.tfReferralNumber.becomeFirstResponder()
                 cell.btnSelf.isHidden = true
            }
        }
    }

    //Refferal Nubmer
    func resetReferralNumber() {
        ReRegistrationModel.shared.Recommended = ""
        ReRegistrationModel.shared.CodeRecommended = ""
        if let cell = tableViewRagi.cellForRow(at: IndexPath(row: 1, section: 8)) as? ReferralNumberRRCell {
            cell.btnClose.isHidden = true
            cell.btnSelf.isUserInteractionEnabled = true
            cell.tfReferralNumber.isUserInteractionEnabled = true
            cell.btnSelf.isHidden = false
            cell.imgCountryFlag.image =  UIImage(named: "myanmar")
            cell.lblCountyCode.text = "(+95)"
            cell.tfReferralNumber.text = "09"
            flagCodeReferral = "myanmar"
            countryCodeReferral = "+95"
            ReferralPhoneNumber = ""
            ReferralConfirmPhoneNumber = ""
        }
        self.ReferralConfirmNumber = false
        reloadRows(index: 2, withSeciton: 8)
    }
    
    func closeReferralNumber() {
        ReRegistrationModel.shared.Recommended = ""
        ReRegistrationModel.shared.CodeRecommended = ""
        if let cell = tableViewRagi.cellForRow(at: IndexPath(row: 1, section: 8)) as? ReferralNumberRRCell {
            cell.btnClose.isHidden = true
            if cell.imgCountryFlag.image == UIImage(named: "myanmar") {
                cell.tfReferralNumber.text = "09"
            }else {
                cell.tfReferralNumber.text = ""
            }
            //cell.tfReferralNumber.resignFirstResponder()
        }
        ReferralPhoneNumber = ""
        ReferralConfirmPhoneNumber = ""
        self.ReferralConfirmNumber = false
        reloadRows(index: 2, withSeciton: 8)
    }
    
    func endFieldReferral(text: String) {
        var num = text
        ReferralPhoneNumber  = num
        var c_code = countryCodeReferral
        c_code.removeFirst()
        if flagCodeReferral == "myanmar" {
            num.removeFirst()
            ReRegistrationModel.shared.Recommended = "00" + c_code + num
        }else {
            ReRegistrationModel.shared.Recommended = "00" + num
        }
        ReRegistrationModel.shared.CodeRecommended = c_code
    }
    func editingFieldReferral(text: String) {
        if numberReferrralSelectedFromContact {
            numberReferrralSelectedFromContact = false
            self.ReferralConfirmNumber = false
            contactNameReferral = ""
            reloadRows(index: 2, withSeciton: 8)
        }
        let mbLength = validObj.getNumberRangeValidation(text)
        PersonalProfileModelUP.shared.Recommended = ""
        if let cell = tableViewRagi.cellForRow(at: IndexPath(row: 1, section: 8)) as? ReferralNumberRRCell {
            if cell.imgCountryFlag.image == UIImage(named: "myanmar") {
                if text.count < mbLength.min {
                    if self.ReferralConfirmNumber {
                        if let _ = tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 8)) as? ReferralConfirmNumberRRCell {
                        self.ReferralConfirmNumber = false
                        ReferralConfirmPhoneNumber = ""
                        reloadRows(index: 2, withSeciton: 8)
                        }
                    }
                }else if text.count > mbLength.min {
                    ReferralConfirmPhoneNumber = ""
                    if !ReferralConfirmNumber {
                        if let _ = tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 8)) as? ReferralConfirmNumberRRCell {
                            self.ReferralConfirmNumber = true
                            reloadRows(index: 2, withSeciton: 8)
                        }
                    }
                }else if text.count == mbLength.max {
                    cell.tfReferralNumber.resignFirstResponder()
                    ReferralConfirmPhoneNumber = ""
                    if let cc = tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 8)) as? ReferralConfirmNumberRRCell {
                        self.ReferralConfirmNumber = true
                        reloadRows(index: 2, withSeciton: 8)
                        UIView.animate(withDuration: 1.0, animations: {
                            _ = Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false) { timer in
                                let indexPath = IndexPath(row: 2, section: 8)
                                self.tableViewRagi.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                            }
                        })
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                            cc.tfConfirmMobilNumber.becomeFirstResponder()
                        })
                        
                    }
                }

            }else {
                if text.count < 4 {
                    if self.ReferralConfirmNumber {
                        if let _ = tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 8)) as? ReferralConfirmNumberRRCell {
                            self.ReferralConfirmNumber = false
                            ReferralConfirmPhoneNumber = ""
                            reloadRows(index: 2, withSeciton: 8)
                        }
                    }
                }else if text.count > 3 && text.count < 13 {
                    if !ReferralConfirmNumber {
                        self.ReferralConfirmNumber = true
                        reloadRows(index: 2, withSeciton: 8)
                    }
                }else if text.count == 13 {
                    cell.tfReferralNumber.resignFirstResponder()
                    self.ReferralConfirmNumber = true
                    reloadRows(index: 2, withSeciton: 8)
                }
            }
        }
       
    }
    
    func showContactViewReferral() {
        let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
        self.navigationController?.present(nav, animated: true, completion: nil)
        contactListShownFor = "referral"
    }
    
    func showReferralCountryList() {
        let sb = UIStoryboard(name: "Country", bundle: nil)
        let countryVC = sb.instantiateViewController(withIdentifier: "CountryViewController") as! CountryViewController
        countryVC.delegate = self
        countryVC.regiCheck = "AllCountry"
        countryVC.modalPresentationStyle = .fullScreen
        self.navigationController?.present(countryVC, animated: true, completion: nil)
        countryListShownFor = "referral"
    }



    func showAlertRefferal(text: String) {
        DispatchQueue.main.async {
            self.showAlert(alertTitle: "", alertBody: text, alertImage: UIImage(named: "alert-icon")!)}
    }
    
    //Confirme Refferal Nubmer
    
    func editChangedReferralConfirm(text :String) {
        if validObj.checkMatchingNumber(string: text, withString: ReferralPhoneNumber) {
            if text == ReferralPhoneNumber {
                if let cell = tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 8)) as? ReferralConfirmNumberRRCell {
                    cell.tfConfirmMobilNumber.resignFirstResponder()
                }
            }
            ReferralConfirmPhoneNumber = text
        }else {
            if let cell = tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 8)) as? ReferralConfirmNumberRRCell {
                cell.tfConfirmMobilNumber.text?.removeLast()
                ReferralConfirmPhoneNumber = String(text.dropLast())
            }
        }
    }
    func endTextFieldReferralNumber(text: String) {
        
    }
    func clearReferralConfirmNumber() {
        if let cell = tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 8)) as? ReferralConfirmNumberRRCell {
            cell.btnClose.isHidden = true
            if cell.imgCountryFlag.image == UIImage(named: "myanmar") {
                cell.tfConfirmMobilNumber.text = "09"
                ReferralConfirmPhoneNumber = "09"
            }else {
                cell.tfConfirmMobilNumber.text = ""
                ReferralConfirmPhoneNumber = ""
            }
           // cell.tfConfirmMobilNumber.resignFirstResponder()
        }
    }
    // Agent
    func agentCode(isAgentCode :Bool) {
       self.view.endEditing(true)
        if isAgentCode {
            ReRegistrationModel.shared.AgentAuthCode = ""
            let sb = UIStoryboard(name: "Registration" , bundle: nil)
            let agentTCVC  = sb.instantiateViewController(withIdentifier: "AgenTermCondition") as! AgenTermCondition
            agentTCVC.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            agentTCVC.delegate = self
            agentTCVC.modalPresentationStyle = .overCurrentContext
            self.present(agentTCVC, animated: false, completion: nil)
        }else {
            self.isAgentcode = false
            ReRegistrationModel.shared.AgentAuthCode = ""
            reloadRows(index: 0, withSeciton: 8)
            reloadRows(index: 1, withSeciton: 8)
        }
    }
    func removeVC(vc: UIViewController) {
        self.isAgentcode = true
        ReRegistrationModel.shared.AgentAuthCode = ""
        reloadRows(index: 0, withSeciton: 8)
        reloadRows(index: 1, withSeciton: 8)
    }
    
    //Facebook
    func facebook(isFb: Bool) {
        self.isFacebook = isFb
        if !self.isFacebook {
            ReRegistrationModel.shared.FBEmailId = ""
        }
        reloadRows(index: 1, withSeciton: 9)
    }
    
}
