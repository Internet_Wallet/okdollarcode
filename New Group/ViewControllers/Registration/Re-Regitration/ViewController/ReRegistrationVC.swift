//
//  ReRegistrationVC.swift
//  OK
//
//  Created by Imac on 5/29/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import CoreLocation

protocol  ReRegistrationVCVCDelegate : class{
    func updateLanguageModel(language : String)
}

class ReRegistrationVC: OKBaseController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var imgMyFlag: UIImageView!
    @IBOutlet weak var imgUnicodeFlag: UIImageView!
    @IBOutlet weak var imgEngFlag: UIImageView!

    @IBOutlet weak var lblMyanmar: UILabel!
    @IBOutlet weak var lblUnicode: UILabel!
    @IBOutlet weak var lblEnglish: UILabel!
    
    @IBOutlet weak var viewMyanmar: UIView! {
        didSet {
            self.viewMyanmar.backgroundColor = UIColor.lightGray
            self.viewMyanmar.layer.cornerRadius = 15.0
        }
    }

    @IBOutlet weak var viewEnglish: UIView!{
        didSet {
            self.viewEnglish.backgroundColor = UIColor.lightGray
            self.viewEnglish.layer.cornerRadius = 15.0
        }
    }
    
    @IBOutlet weak var viewUnicode: UIView!{
        didSet {
            self.viewUnicode.backgroundColor = UIColor.lightGray
            self.viewUnicode.layer.cornerRadius = 15.0
        }
    }
    
    
    @IBOutlet var selfView: UIView!
    var delegate: ReRegistrationVCVCDelegate?
    @IBOutlet weak var tableViewRagi: UITableView!
    @IBOutlet weak var imgDemo: UIImageView!
    @IBOutlet weak var lblDemo: MarqueeLabel!{
        didSet{
            lblDemo.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var webviewContainer: UIView!
    @IBOutlet weak var webview: UIWebView!
   
    let lblMarque = MarqueeLabel()
    let tranView = UIView()
    let appDel = UIApplication.shared.delegate as! AppDelegate
    let strokeTextAttributes = [
        NSAttributedString.Key.font : UIFont.init(name: appFont, size: 18) ?? UIFont.systemFont(ofSize:18)
        ] as [NSAttributedString.Key : Any]

    
    var rowSection = 10
    var accountTypeRowCount = 0
    var personalInfoRowCount = 0
    var idTyperRowCount = 0
    var businessRowCount = 0
    var addressRowCount = 0
    var securityRowCount = 0
    var singatureRowCount = 0
    var AlternateRowCount = 0
    var ReferralRowCount = 0
    var AdditonalRowCount = 0
    
    
    var locationDetails: LocationDetail?
    var categoryDetail : CategoryDetail?
    var userImageAvaiable = false
    var locationDetailsAddress: LocationDetail?
    var townShipDetail : TownShipDetail?
    var townshipCode = ""
    var adddressMode = ""
     var Other = false
    var division = false
    var township = false
    var city = false
    var village = false
    var street = false
    var room = false
    var housing = false
    var DivisionName = ""
    var TownshipName = ""
    var SecurityQuestion = ""
    
    var AlternateConfirmNumber = false
    var ReferralConfirmNumber = false
    var numberSelectedFromContact = false
    var numberReferrralSelectedFromContact = false
    var countryListShownFor = ""
    var contactListShownFor = ""
    let validObj = PayToValidations()
    var flagCodeAlternate = "myanmar"
    var countryCodeAlternate = "+95"
    var contactNameAlternate = ""
    var flagCodeReferral = "myanmar"
    var countryCodeReferral = "+95"
    var contactNameReferral = ""
    var AlternatePhoneNumber  = ""
    var AlternateConfirmPhoneNumber  = ""
    var ReferralPhoneNumber  = ""
    var ReferralConfirmPhoneNumber  = ""
    var isAddtitionalInformatonHiddenForPersonal = true
    
    var isAgentcode = false
     var isFacebook = false
    var isPasswordFieldHidden = true
    var isPasswordConrimFieldHidden = true
    var isSecurityAnsHidden = true
    var isBusinessViewHidden = true
    var isAgentViewHidden = true
    var isAddressViewHidden = true
    var isPasswordShow = false
    
    var tvVillageList = Bundle.main.loadNibNamed("VillageStreetListView", owner: self, options: nil)?[0] as! VillageStreetListView
    var tvStreetList = Bundle.main.loadNibNamed("StreetListView", owner: self, options: nil)?[0] as! StreetListView
    var addressScrollcount = 0
    var titleScreen = ""
    
    var isIDPhotoAttached = false
    var isIDBusinessAttached = false
    var isIDAgentAttached = false
    var isTypeSelected = "NONE"
    var profilImage = UIImage(named: "account_type_Add")
    var imageListIDType = [UIImage]()
    var imageListBusiness = [UIImage]()
    var imageListAgent = [UIImage]()
    
    var failureCount = 0
    
    var englishlanguage = ""
    var pattrenpassword = ""
    var crashcheckValue = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        if let country = preCodeCountry?.country {
            ReRegistrationModel.shared.countryISO = country
        }else {
            ReRegistrationModel.shared.countryISO = "Myanmar"
        }
        
        imgMyFlag.layer.borderWidth = 1.0
        imgMyFlag.layer.borderColor = UIColor.white.cgColor
        imgEngFlag.layer.borderWidth = 1.0
        imgEngFlag.layer.borderColor = UIColor.white.cgColor
        imgUnicodeFlag.layer.borderWidth = 1.0
        imgUnicodeFlag.layer.borderColor = UIColor.white.cgColor
        
        self.getAddressData()
        //UserLogin.shared.loginSessionExpired
        webviewContainer.isHidden = true
        
        tableViewRagi.register(UINib(nibName: "ProfileImageCellRR", bundle: nil), forCellReuseIdentifier: "ProfileImageCellRR")
        tableViewRagi.register(UINib(nibName: "AccountTypeRRCell", bundle: nil), forCellReuseIdentifier: "AccountTypeRRCell")
        tableViewRagi.register(UINib(nibName: "PersonalInfoRRCell", bundle: nil), forCellReuseIdentifier: "PersonalInfoRRCell")
        tableViewRagi.register(UINib(nibName: "NameRRCell", bundle: nil), forCellReuseIdentifier: "NameRRCell")
        tableViewRagi.register(UINib(nibName: "BirthDateRRCell", bundle: nil), forCellReuseIdentifier: "BirthDateRRCell")
        tableViewRagi.register(UINib(nibName: "TakeProfileImageRRCell", bundle: nil), forCellReuseIdentifier: "TakeProfileImageRRCell")
        tableViewRagi.register(UINib(nibName: "FatherNameRRCell", bundle: nil), forCellReuseIdentifier: "FatherNameRRCell")
        tableViewRagi.register(UINib(nibName: "EmailRRCell", bundle: nil), forCellReuseIdentifier: "EmailRRCell")
        
        tableViewRagi.register(UINib(nibName: "IDTypeHeaderRRCell", bundle: nil), forCellReuseIdentifier: "IDTypeHeaderRRCell")
        tableViewRagi.register(UINib(nibName: "IDPhotoRRCell", bundle: nil), forCellReuseIdentifier: "IDPhotoRRCell")
        tableViewRagi.register(UINib(nibName: "SelectIDTypeRRCell", bundle: nil), forCellReuseIdentifier: "SelectIDTypeRRCell")
        tableViewRagi.register(UINib(nibName: "NRCNumberRRCell", bundle: nil), forCellReuseIdentifier: "NRCNumberRRCell")
        tableViewRagi.register(UINib(nibName: "PassportCountryRRCell", bundle: nil), forCellReuseIdentifier: "PassportCountryRRCell")
        tableViewRagi.register(UINib(nibName: "PassportNumberRRCell", bundle: nil), forCellReuseIdentifier: "PassportNumberRRCell")
        tableViewRagi.register(UINib(nibName: "PassportExpiryDateRRCell", bundle: nil), forCellReuseIdentifier: "PassportExpiryDateRRCell")
        
        tableViewRagi.register(UINib(nibName: "BusinessHeaderRRCell", bundle: nil), forCellReuseIdentifier: "BusinessHeaderRRCell")
        tableViewRagi.register(UINib(nibName: "BusinessNameRRCell", bundle: nil), forCellReuseIdentifier: "BusinessNameRRCell")
        tableViewRagi.register(UINib(nibName: "BusinessCategoryRRCell", bundle: nil), forCellReuseIdentifier: "BusinessCategoryRRCell")
        tableViewRagi.register(UINib(nibName: "BusinessSubCategoryRRCell", bundle: nil), forCellReuseIdentifier: "BusinessSubCategoryRRCell")
        tableViewRagi.register(UINib(nibName: "BusinessPhotoRRCell", bundle: nil), forCellReuseIdentifier: "BusinessPhotoRRCell")
        tableViewRagi.register(UINib(nibName: "AgentShopImages", bundle: nil), forCellReuseIdentifier: "AgentShopImages")
        
        tableViewRagi.register(UINib(nibName: "AddressHeaderRRCell", bundle: nil), forCellReuseIdentifier: "AddressHeaderRRCell")
        tableViewRagi.register(UINib(nibName: "OtherAddressRRCell", bundle: nil), forCellReuseIdentifier: "OtherAddressRRCell")
        tableViewRagi.register(UINib(nibName: "DivisionRRCell", bundle: nil), forCellReuseIdentifier: "DivisionRRCell")
        tableViewRagi.register(UINib(nibName: "TownshipRRCell", bundle: nil), forCellReuseIdentifier: "TownshipRRCell")
        tableViewRagi.register(UINib(nibName: "CityRRCell", bundle: nil), forCellReuseIdentifier: "CityRRCell")
        tableViewRagi.register(UINib(nibName: "VillageRRCell", bundle: nil), forCellReuseIdentifier: "VillageRRCell")
        tableViewRagi.register(UINib(nibName: "StreetRRCell", bundle: nil), forCellReuseIdentifier: "StreetRRCell")
        tableViewRagi.register(UINib(nibName: "HouseNumberRRCell", bundle: nil), forCellReuseIdentifier: "HouseNumberRRCell")
        tableViewRagi.register(UINib(nibName: "HousingAddressRRCell", bundle: nil), forCellReuseIdentifier: "HousingAddressRRCell")
        
        tableViewRagi.register(UINib(nibName: "PasswordHeaderRRCell", bundle: nil), forCellReuseIdentifier: "PasswordHeaderRRCell")
        tableViewRagi.register(UINib(nibName: "PasswordRRCell", bundle: nil), forCellReuseIdentifier: "PasswordRRCell")
        tableViewRagi.register(UINib(nibName: "ConfirmPasswordRRCell", bundle: nil), forCellReuseIdentifier: "ConfirmPasswordRRCell")
        tableViewRagi.register(UINib(nibName: "SecurityQuestionRRCell", bundle: nil), forCellReuseIdentifier: "SecurityQuestionRRCell")
        tableViewRagi.register(UINib(nibName: "SecurityAnswerRRcell", bundle: nil), forCellReuseIdentifier: "SecurityAnswerRRcell")
        tableViewRagi.register(UINib(nibName: "SignatureRRCell", bundle: nil), forCellReuseIdentifier: "SignatureRRCell")
        
        
        tableViewRagi.register(UINib(nibName: "ContactHeaderRRCell", bundle: nil), forCellReuseIdentifier: "ContactHeaderRRCell")
        tableViewRagi.register(UINib(nibName: "AlternatNumberRRCell", bundle: nil), forCellReuseIdentifier: "AlternatNumberRRCell")
        tableViewRagi.register(UINib(nibName: "AlternateConfirmNumberRRCell", bundle: nil), forCellReuseIdentifier: "AlternateConfirmNumberRRCell")
        
        tableViewRagi.register(UINib(nibName: "ContactReferralHeaderRRCell", bundle: nil), forCellReuseIdentifier: "ContactReferralHeaderRRCell")
        tableViewRagi.register(UINib(nibName: "ReferralNumberRRCell", bundle: nil), forCellReuseIdentifier: "ReferralNumberRRCell")
        tableViewRagi.register(UINib(nibName: "ReferralConfirmNumberRRCell", bundle: nil), forCellReuseIdentifier: "ReferralConfirmNumberRRCell")
        
        tableViewRagi.register(UINib(nibName: "AddtionalInfoRRCell", bundle: nil), forCellReuseIdentifier: "AddtionalInfoRRCell")
        tableViewRagi.register(UINib(nibName: "FaceBookRRCell", bundle: nil), forCellReuseIdentifier: "FaceBookRRCell")
        tableViewRagi.register(UINib(nibName: "ReRegiteredRRCell", bundle: nil), forCellReuseIdentifier: "ReRegiteredRRCell")
        
        tableViewRagi.register(UINib(nibName: "AdditionalAgentInfoRRCell", bundle: nil), forCellReuseIdentifier: "AdditionalAgentInfoRRCell")
        tableViewRagi.register(UINib(nibName: "AgetCodeRRCell", bundle: nil), forCellReuseIdentifier: "AgetCodeRRCell")
        if ReRegistrationModel.shared.screenFrom == "Regi_HelpVC" {
            titleScreen = "OK$ Registration".localized
        }else {
            titleScreen = "Re-Regitration".localized
      
        }
        
        if UserDefaults.standard.bool(forKey: "RE_REGITRATION") || ReRegistrationModel.shared.lType != "" {
            accountTypeRowCount = 3
            personalInfoRowCount = 1
        }else {
            accountTypeRowCount = 2
            personalInfoRowCount = 0
        }
        
        ok_default_language =  ReRegistrationModel.shared.Language
        
        if ok_default_language == "my"{
           englishlanguage = "my"
           ok_default_language = englishlanguage
        }
        else if ok_default_language == "en"{
            englishlanguage = "en"
            ok_default_language = englishlanguage
        }
        else{
            englishlanguage = "uni"
            ok_default_language = englishlanguage
        }
         ReRegistrationModel.shared.clearValues()
        
        self.tableViewRagi.delegate = self
        self.tableViewRagi.dataSource = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
            //self.navigationItem.titleView = nil
            lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
            lblMarque.font =  UIFont(name: appFont, size: 18)
            lblMarque.textAlignment = .center
            lblMarque.textColor = UIColor.white
        if ReRegistrationModel.shared.Language.localizedLowercase == "my" {
            viewMyanmar.backgroundColor = kYellowColor
            viewUnicode.backgroundColor = UIColor.lightGray
            viewEnglish.backgroundColor = UIColor.lightGray
        }else if ReRegistrationModel.shared.Language.localizedLowercase == "en"{
            viewMyanmar.backgroundColor = UIColor.lightGray
            viewUnicode.backgroundColor = UIColor.lightGray
            viewEnglish.backgroundColor =  kYellowColor
        }else {
            viewMyanmar.backgroundColor = UIColor.lightGray
            viewUnicode.backgroundColor = kYellowColor
            viewEnglish.backgroundColor = UIColor.lightGray
        }
    
            lblMarque.text = titleScreen
            self.navigationItem.titleView = lblMarque
            lblMyanmar.text = "ေဇာ္ဂ်ီ"
            lblUnicode.text = "ယူနစ္ကုဒ္"
            lblEnglish.text = "English"
        self.tableViewRagi.reloadData()

    }
    
   
    @IBAction func backAction(_ sender: Any) {
        webviewContainer.isHidden = true
        
        if ReRegistrationModel.shared.screenFrom == "Regi_HelpVC" {
            if  ReRegistrationModel.shared.Name == "" {
                ReRegistrationModel.shared.clearValues()
                UserDefaults.standard.set(false, forKey: "RigiRedirect")
                self.dismiss(animated: false, completion: nil)
            }else {
                alertViewObj.wrapAlert(title: "", body: "If you click back button, you have entered data will be automatically cleared and re-direct to OK $ new user registration screen.".localized, img:#imageLiteral(resourceName: "r_user"))
                alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
                })
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                    ReRegistrationModel.shared.clearValues()
                    UserDefaults.standard.set(false, forKey: "RigiRedirect")
                    self.dismiss(animated: false, completion: nil)
                })
                DispatchQueue.main.async {
                    alertViewObj.showAlert(controller: self)
                }
            }
            ReRegistrationModel.shared.lType = ""
        }else {
            if  ReRegistrationModel.shared.Name == "" {
                ReRegistrationModel.shared.clearValues()
                UserDefaults.standard.set(false, forKey: "RigiRedirect")
                self.dismiss(animated: false, completion: nil)
            }else {
                alertViewObj.wrapAlert(title: "", body: "If you click back button, you have entered data will be automatically cleared and re-direct to OK $ new user registration screen.".localized, img:#imageLiteral(resourceName: "r_user"))
                alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
                })
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                    ReRegistrationModel.shared.clearValues()
                    UserDefaults.standard.set(false, forKey: "RigiRedirect")
                    self.dismiss(animated: false, completion: nil)
                })
                DispatchQueue.main.async {
                    alertViewObj.showAlert(controller: self)
                }
            }
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return rowSection
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return accountTypeRowCount
        }else if section == 1 {
            return personalInfoRowCount
        }else if section == 2 {
            return idTyperRowCount
        }else if section == 3 {
            return businessRowCount
        }else if section == 4 {
            return addressRowCount
        }else if section == 5 {
            return securityRowCount
        }else if section == 6 {
            return singatureRowCount
        }else if section == 7 {
                return AlternateRowCount
        }else if section == 8 {
                return ReferralRowCount
        }else if section == 9 {
            return AdditonalRowCount
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileImageCellRR", for: indexPath) as! ProfileImageCellRR
                cell.capturedImage.image = profilImage
                cell.delegate = self
                cell.selectionStyle = .none
                return cell
            }else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AccountTypeRRCell", for: indexPath) as! AccountTypeRRCell
                cell.updateLaguageCell()
                cell.delegate = self
                cell.selectionStyle = .none
                return cell
            }else if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PersonalInfoRRCell", for: indexPath) as! PersonalInfoRRCell
                cell.updateLaguageCell()
                cell.delegate = self
                cell.selectionStyle = .none
                return cell
            }
        }else if indexPath.section == 1 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NameRRCell", for: indexPath) as! NameRRCell
                cell.wrapData()
                cell.updateLaguageCell()
                cell.delegate = self
                cell.selectionStyle = .none
                return cell
            }else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "BirthDateRRCell", for: indexPath) as! BirthDateRRCell
                cell.wrapData()
                cell.updateLaguageCell()
                cell.delegate = self
                cell.selectionStyle = .none
                return cell
            }else if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TakeProfileImageRRCell", for: indexPath) as! TakeProfileImageRRCell
                cell.updateLaguageCell()
                if userImageAvaiable {
                    cell.isHidden = true
                }else {
                    cell.isHidden = false
                }
                cell.delegate = self
                cell.selectionStyle = .none
                return cell
            }else if indexPath.row == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "FatherNameRRCell", for: indexPath) as! FatherNameRRCell
                cell.wrapData()
                cell.updateLaguageCell()
                cell.delegate = self
                cell.selectionStyle = .none
                return cell
            }else if indexPath.row == 4 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "EmailRRCell", for: indexPath) as! EmailRRCell
                cell.delegate = self
                cell.wrapData()
                cell.updateLaguageCell()
                cell.selectionStyle = .none
                return cell
            }
        }else if indexPath.section == 2 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "IDTypeHeaderRRCell", for: indexPath) as! IDTypeHeaderRRCell
                cell.delegate = self
                cell.updateLaguageCell()
                cell.selectionStyle = .none
                return cell
            }else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SelectIDTypeRRCell", for: indexPath) as! SelectIDTypeRRCell
                if ReRegistrationModel.shared.IDType == "" {
                    cell.isHidden = false
                }else {
                    cell.isHidden = true
                }
                cell.delegate = self
                cell.updateLaguageCell()
                cell.selectionStyle = .none
                return cell
            }else if indexPath.row == 2 {
                if ReRegistrationModel.shared.IDType == "01" {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "NRCNumberRRCell", for: indexPath) as! NRCNumberRRCell
                    cell.delegate = self
                    cell.updateLaguageCell()
                    cell.selectionStyle = .none
                    return cell
                }else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PassportCountryRRCell", for: indexPath) as! PassportCountryRRCell
                    cell.delegate = self
                    cell.updateLaguageCell()
                    cell.selectionStyle = .none
                    return cell
                }
            }else if indexPath.row == 3 {
                if ReRegistrationModel.shared.IDType == "01" {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "IDPhotoRRCell", for: indexPath) as! IDPhotoRRCell
                    cell.imgFront.sd_setImage(with: URL(string: ReRegistrationModel.shared.IdPhotoAwsUrl), placeholderImage: UIImage(named: "nrc"))
                    cell.imgBack.sd_setImage(with: URL(string: ReRegistrationModel.shared.IdPhoto1AwsUrl), placeholderImage: UIImage(named: "nrc"))
                    cell.delegate = self
                    cell.updateLaguageCell()
                    cell.selectionStyle = .none
                    return cell
                }else if ReRegistrationModel.shared.IDType == "04" {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PassportNumberRRCell", for: indexPath) as! PassportNumberRRCell
                    cell.delegate = self
                    cell.updateLaguageCell()
                    cell.selectionStyle = .none
                    return cell
                }
            }else if indexPath.row == 4 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PassportExpiryDateRRCell", for: indexPath) as! PassportExpiryDateRRCell
                cell.delegate = self
                cell.updateLaguageCell()
                cell.selectionStyle = .none
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "IDPhotoRRCell", for: indexPath) as! IDPhotoRRCell
                cell.imgFront.sd_setImage(with: URL(string: ReRegistrationModel.shared.IdPhotoAwsUrl), placeholderImage: UIImage(named: "nrc"))
                cell.imgBack.sd_setImage(with: URL(string: ReRegistrationModel.shared.IdPhoto1AwsUrl), placeholderImage: UIImage(named: "nrc"))
                cell.delegate = self
                cell.updateLaguageCell()
                cell.selectionStyle = .none
                return cell
            }
        }else if indexPath.section == 3 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessHeaderRRCell", for: indexPath) as! BusinessHeaderRRCell
                cell.delegate = self
                cell.updateLaguageCell()
                cell.selectionStyle = .none
                return cell
            }else if indexPath.row == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessNameRRCell", for: indexPath) as! BusinessNameRRCell
                cell.delegate = self
                cell.updateLaguageCell()
                cell.selectionStyle = .none
                return cell
            }else if indexPath.row == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessCategoryRRCell", for: indexPath) as! BusinessCategoryRRCell
                cell.delegate = self
                cell.updateLaguageCell()
                cell.selectionStyle = .none
                return cell
            }else if indexPath.row == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessSubCategoryRRCell", for: indexPath) as! BusinessSubCategoryRRCell
                cell.delegate = self
                cell.updateLaguageCell()
                cell.selectionStyle = .none
                return cell
            }else if indexPath.row == 4 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessPhotoRRCell", for: indexPath) as! BusinessPhotoRRCell
                cell.imgFront.sd_setImage(with: URL(string: ReRegistrationModel.shared.BusinessIdPhoto1AwsUrl), placeholderImage: UIImage(named: "nrc"))
                cell.imgBack.sd_setImage(with: URL(string: ReRegistrationModel.shared.BusinessIdPhoto2AwsUrl), placeholderImage: UIImage(named: "nrc"))
                cell.delegate = self
                cell.updateLaguageCell()
                cell.selectionStyle = .none
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AgentShopImages", for: indexPath) as! AgentShopImages
                cell.imgFront.sd_setImage(with: URL(string: ReRegistrationModel.shared.ShopPicOneAwsUrl), placeholderImage: UIImage(named: "nrc"))
                cell.imgBack.sd_setImage(with: URL(string: ReRegistrationModel.shared.ShopPicTwoAwsUrl), placeholderImage: UIImage(named: "nrc"))
                cell.delegate = self
                cell.updateLaguageCell()
                cell.selectionStyle = .none
                return cell
            }
        }else if indexPath.section == 4 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddressHeaderRRCell", for: indexPath) as! AddressHeaderRRCell
                cell.delegate = self
                cell.updateLaguageCell()
                cell.selectionStyle = .none
                return cell
            }else if indexPath.row == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "OtherAddressRRCell", for: indexPath) as! OtherAddressRRCell
                cell.delegate = self
                if Other { cell.isHidden = false }else {cell.isHidden = true }
                cell.updateLaguageCell()
                cell.selectionStyle = .none
                return cell
            }else if indexPath.row == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "DivisionRRCell", for: indexPath) as! DivisionRRCell
                cell.delegate = self
                if division { cell.isHidden = false }else {cell.isHidden = true }
                cell.updateLaguageCell(name: DivisionName)
                cell.selectionStyle = .none
                return cell
            }else if indexPath.row == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TownshipRRCell", for: indexPath) as! TownshipRRCell
                cell.delegate = self
                if township { cell.isHidden = false }else {cell.isHidden = true }
                cell.updateLaguageCell(name: TownshipName)
                cell.selectionStyle = .none
                return cell
            }else  if indexPath.row == 4 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CityRRCell", for: indexPath) as! CityRRCell
                //cell.delegate = self
                if city { cell.isHidden = false }else {cell.isHidden = true }
                cell.updateLaguageCell(name: ReRegistrationModel.shared.Address1)
                cell.selectionStyle = .none
                return cell
            }else  if indexPath.row == 5{
                let cell = tableView.dequeueReusableCell(withIdentifier: "VillageRRCell", for: indexPath) as! VillageRRCell
                cell.delegate = self
                if village { cell.isHidden = false }else {cell.isHidden = true }
                cell.updateLaguageCell(name: ReRegistrationModel.shared.VillageName)
                cell.selectionStyle = .none
                return cell
            }else  if indexPath.row == 6 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "StreetRRCell", for: indexPath) as! StreetRRCell
                cell.delegate = self
                if street { cell.isHidden = false }else {cell.isHidden = true }
                cell.updateLaguageCell(name: ReRegistrationModel.shared.Address2)
                cell.selectionStyle = .none
                return cell
            }else  if indexPath.row == 7 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HouseNumberRRCell", for: indexPath) as! HouseNumberRRCell
                cell.delegate = self
                if room { cell.isHidden = false }else {cell.isHidden = true }
                cell.updateLaguageCell(house: ReRegistrationModel.shared.HouseBlockNo, floor: ReRegistrationModel.shared.FloorNumber, room: ReRegistrationModel.shared.RoomNumber)
                cell.selectionStyle = .none
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HousingAddressRRCell", for: indexPath) as! HousingAddressRRCell
                if housing { cell.isHidden = false }else {cell.isHidden = true }
                cell.updateLaguageCell(house: ReRegistrationModel.shared.HouseName)
                cell.selectionStyle = .none
                return cell
            }
        }else if indexPath.section == 5 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PasswordHeaderRRCell", for: indexPath) as! PasswordHeaderRRCell
                cell.delegate = self
                cell.selectionStyle = .none
                cell.updateLaguageCell()
                return cell
            }else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PasswordRRCell", for: indexPath) as! PasswordRRCell
                if isPasswordFieldHidden {
                    cell.tfPassword.text = ""
                    cell.lblPasswordMarque.text = "Create Password(Min 6 digits)".localized
                }
                cell.updateLaguageCell()
                cell.delegate = self
                cell.selectionStyle = .none
                return cell
            }else if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ConfirmPasswordRRCell", for: indexPath) as! ConfirmPasswordRRCell
                if isPasswordConrimFieldHidden {
                    cell.tfConfirmPassword.text = ""
                   // isPasswordConrimFieldHidden = false
                }
                cell.delegate = self
                cell.selectionStyle = .none
                return cell
            }else if indexPath.row == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SecurityQuestionRRCell", for: indexPath) as! SecurityQuestionRRCell
                cell.delegate = self
                cell.updateLaguageCell(text: SecurityQuestion)
                cell.selectionStyle = .none
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SecurityAnswerRRcell", for: indexPath) as! SecurityAnswerRRcell
                cell.delegate = self
                cell.updateLaguageCell()
                
                cell.selectionStyle = .none
                return cell
            }
        }else if indexPath.section == 6 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SignatureRRCell", for: indexPath) as! SignatureRRCell
            cell.delegate = self
            cell.updateLaguageCell()
            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 7 {
            if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ContactHeaderRRCell", for: indexPath) as! ContactHeaderRRCell
                    cell.delegate = self
                    cell.updateLaguageCell()
                    cell.selectionStyle = .none
                    return cell
            }else if indexPath.row == 1 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "AlternatNumberRRCell", for: indexPath) as! AlternatNumberRRCell
                    cell.delegate = self
                    cell.updateLaguageCell(flag: flagCodeAlternate, countryCode: countryCodeAlternate, number: AlternatePhoneNumber)
                    cell.selectionStyle = .none
                    return cell
            }else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "AlternateConfirmNumberRRCell", for: indexPath) as! AlternateConfirmNumberRRCell
                    cell.delegate = self
                    if AlternateConfirmNumber {
                        cell.isHidden = false
                    }else {
                        cell.isHidden = true
                    }
                if numberSelectedFromContact {
                    cell.updateLaguageCell(flag: "sim_R", countryCode: countryCodeAlternate, number: AlternateConfirmPhoneNumber, contactName: contactNameAlternate)
                }else {
                   cell.updateLaguageCell(flag: flagCodeAlternate, countryCode: countryCodeAlternate, number: AlternateConfirmPhoneNumber, contactName: contactNameAlternate)
                }
                    cell.selectionStyle = .none
                    return cell
            }
        }else if indexPath.section == 8 {
            if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ContactReferralHeaderRRCell", for: indexPath) as! ContactReferralHeaderRRCell
                    cell.delegate = self
                    cell.updateLaguageCell()
                    cell.selectionStyle = .none
                    return cell
            }else if indexPath.row == 1 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ReferralNumberRRCell", for: indexPath) as! ReferralNumberRRCell
                    cell.delegate = self
                    cell.updateLaguageCell(flag: flagCodeReferral, countryCode: countryCodeReferral, number: ReferralPhoneNumber)
                    cell.selectionStyle = .none
                    return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReferralConfirmNumberRRCell", for: indexPath) as! ReferralConfirmNumberRRCell
                cell.delegate = self
                if ReferralConfirmNumber {
                    cell.isHidden = false
                }else {
                    cell.isHidden = true
                }
                if numberReferrralSelectedFromContact {
                   cell.updateLaguageCell(flag: "sim_R", countryCode: countryCodeReferral, number: ReferralConfirmPhoneNumber, contactName: contactNameReferral)
                }else {
                    cell.updateLaguageCell(flag: flagCodeReferral, countryCode: countryCodeReferral, number: ReferralConfirmPhoneNumber, contactName: contactNameReferral)
                }
                cell.selectionStyle = .none
                return cell
            }
        }else if indexPath.section == 9 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddtionalInfoRRCell", for: indexPath) as! AddtionalInfoRRCell
                cell.delegate = self
                cell.updateLaguageCell(facebook: isFacebook)
                cell.selectionStyle = .none
                return cell
            }else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "FaceBookRRCell", for: indexPath) as! FaceBookRRCell
                if isFacebook  {
                   cell.isHidden = false
                }else {
                    cell.isHidden = true
                }
                cell.selectionStyle = .none
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReRegiteredRRCell", for: indexPath) as! ReRegiteredRRCell
                cell.delegate = self
                cell.updateLaguageCell()
                cell.selectionStyle = .none
                return cell
            }
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "EmailRRCell", for: indexPath) as! EmailRRCell
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                if userImageAvaiable {
                    return 200
                }else{
                    return 0
                }
            }else if indexPath.row == 1 {
                return 260
            }else {
                return 40
            }
        }else if indexPath.section == 1 {
            if userImageAvaiable && indexPath.row == 2{
                return 0
            }else {
                return 60
            }
        }else if indexPath.section == 2 {
            if indexPath.row == 0 {
                return 40
            }else if indexPath.row == 1 {
                if isTypeSelected == "NONE" {
                    return 130
                }else {
                    return 0
                }
            }else if indexPath.row == 2 {
                return 60
            }else if indexPath.row == 3 {
                if ReRegistrationModel.shared.IDType == "01" {
                    if isIDPhotoAttached {
                         return 160
                    }else {
                         return 60
                    }
                }else {
                    return 60
                }
            }else if indexPath.row == 4 {
                return 60
            }else {
                if isIDPhotoAttached {
                    return 160
                }else {
                    return 60
                }
            }
        }else if indexPath.section == 3 {
            if ReRegistrationModel.shared.AccountType == 0 {
                if indexPath.row == 0 {
                    return 40
                }else if indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3 {
                    return 60
                }else if indexPath.row == 4 {
                    if isIDBusinessAttached {
                           return 160
                    }else {
                        return 60
                    }
                }else {
                    if isIDAgentAttached {
                        return 160
                    }else {
                        return 60
                    }
                }
            }else {
                return 0
            }
        }else if indexPath.section == 4 {
            if indexPath.row == 0 {
                return 90
            }else if indexPath.row == 1 {
                if  Other{
                    return 60
                }else {
                    return 0
                }
            }else if indexPath.row == 2 {
                if division{
                    return 60
                }else {
                    return 0
                }
            }else if indexPath.row == 3 {
                if  township{
                    return 60
                }else {
                    return 0
                }
            }else if indexPath.row == 4 {
                if  city{
                    return 60
                }else {
                    return 0
                }
            }else if indexPath.row == 5 {
                if  village{
                    return 60
                }else {
                    return 0
                }
            }else if indexPath.row == 6 {
                if  street{
                    return 60
                }else {
                    return 0
                }
            }
            else if indexPath.row == 7 {
                if  room {
                    return 80
                }else {
                    return 0
                }
            }else if indexPath.row == 8 {
                if  housing {
                    return 80
                }else {
                    return 0
                }
            }else {
                return 0
            }
        }else if indexPath.section == 5 {
            if UserDefaults.standard.bool(forKey: "RE_REGITRATION") {
                if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2  {
                    return 0
                }else if indexPath.row == 3 {
                    return 100
                }else {
                    return 60
                }
            }else {
                if indexPath.row == 0 {
                    return 80
                }else if indexPath.row == 1 {
                    if isPasswordFieldHidden {
                        return 0
                    }else {
                      return 60
                    }
                }else if indexPath.row == 2 {
                    if isPasswordConrimFieldHidden {
                        return 0
                    }else {
                        return 60
                    }
                }else if indexPath.row == 3 {
                    return 100
                }else {
                    return 60
                }
            }
        }else if indexPath.section == 6 {
            return 225
        }else if indexPath.section == 7 {
//            if indexPath.row == 0 {
//                return 40
//            }else if indexPath.row == 1 {
//                return 60
//            }else {
//                if AlternateConfirmNumber {
//                    return 60
//                }else {
//                    return 0
//                }
//            }
                 return 0
        }else if indexPath.section == 8 {
//            if indexPath.row == 0 {
//                return 40
//            }else if indexPath.row == 1 {
//                return 60
//            }else {
//                if ReferralConfirmNumber {
//                    return 60
//                }else {
//                    return 0
//                }
//            }
                return 0
        }else if indexPath.section == 9 {
            if indexPath.row == 0 {
                return 60
            }else if indexPath.row == 1 {
                if isFacebook {
                    return 100
                }else {
                    return 0
                }
            }else {
                //return 60
                return 300
            }
        }else {
            return 0
        }
    }

    func deleteRow(index: Int, withSeciton sec: Int) {
            self.tableViewRagi.beginUpdates()
            self.tableViewRagi.insertRows(at: [IndexPath.init(row: index, section: sec)], with: .automatic)
            self.tableViewRagi.endUpdates()
        
                UIView.animate(withDuration: 1.0, animations: {
                    _ = Timer.scheduledTimer(withTimeInterval: 0.8, repeats: false) { timer in
                        let indexPath = IndexPath(row: index, section: sec)
                        self.tableViewRagi.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                    }
                })
    }

    
  
    func insertRow(index: Int, withSeciton sec: Int) {
            self.tableViewRagi.beginUpdates()
            self.tableViewRagi.insertRows(at: [IndexPath.init(row: index, section: sec)], with: .automatic)
            self.tableViewRagi.endUpdates()
        
                UIView.animate(withDuration: 1.0, animations: {
                    _ = Timer.scheduledTimer(withTimeInterval: 0.8, repeats: false) { timer in
                        let indexPath = IndexPath(row: index, section: sec)
                        self.tableViewRagi.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                    }
                })
    }
    //handler: @escaping (_ success: Bool) -> Void
    func insertRowWithBlock(index: Int, withSeciton sec: Int, handler: @escaping(_ success: Bool) -> Void) {
        self.tableViewRagi.beginUpdates()
        self.tableViewRagi.insertRows(at: [IndexPath.init(row: index, section: sec)], with: .automatic)
        self.tableViewRagi.endUpdates()
        handler(true)
    }
    
    func reloadRows(index: Int, withSeciton sec: Int) {
        UIView.setAnimationsEnabled(false)
        let indexPosition = IndexPath(row: index, section: sec)
        self.tableViewRagi.reloadRows(at: [indexPosition], with: .none)
        UIView.setAnimationsEnabled(true)
    }
    
    
    
    @IBAction func closeWebviewAction(_ sender: Any) {
        webviewContainer.isHidden = true
        webview.loadHTMLString("", baseURL: nil)
    }
    // Language Changes
    @IBAction func btnLanguageFirstAction(_ sender: Any) {
        englishlanguage = "my"
        if ReRegistrationModel.shared.Language == "my" {
            return
        }
        if ReRegistrationModel.shared.screenFrom != "Regi_HelpVC" {
            return
        }
        
        if personalInfoRowCount > 1 {
            alertViewObj.wrapAlert(title: "", body: "If you change Myanmar language all entered information will be deleted and you have to fill up all information again in Myanmar language only.".localized, img: UIImage(named: "r_user"))
            alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
            })
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                self.changeCurrentLanguage(language: "my")
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }else {
            self.changeCurrentLanguage(language: "my")
        }
        
    }
    @IBAction func btnLanguageSecondAction(_ sender: Any) {
        
        englishlanguage = "uni"
        if ReRegistrationModel.shared.Language == "uni" {
            return
        }
        
        if ReRegistrationModel.shared.screenFrom != "Regi_HelpVC" {
            return
        }
        self.changeCurrentLanguage(language: "uni")
    }
    
    @IBAction func btnLanguageThiredAction(_ sender: Any) {
        
        englishlanguage = "en"
        
        if ReRegistrationModel.shared.Language == "en" {
            return
        }
        
        if ReRegistrationModel.shared.screenFrom != "Regi_HelpVC" {
            return
        }
        
        self.changeCurrentLanguage(language: "en")
    }
    
    
    func navigateToLoginController() {
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: "", body:"You have successfully registered with OK$".localized , img: #imageLiteral(resourceName: "user"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                    let sb = UIStoryboard(name: "Login", bundle: nil)
                    let loginVC = sb.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                    println_debug(ReRegistrationModel.shared.MobileNumber)
                    preLoginModel.agentCode = ReRegistrationModel.shared.MobileNumber
                    self.navigationController?.pushViewController(loginVC!, animated: true)
                    self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            })
            alertViewObj.showAlert(controller: self)
        }
    }
  
}



extension ReRegistrationVC {

     func changeCurrentLanguage(language: String) {
        webviewContainer.isHidden = true
        if language == "my" {
            viewMyanmar.backgroundColor = kYellowColor
            viewEnglish.backgroundColor =  UIColor.lightGray
            viewUnicode.backgroundColor =  UIColor.lightGray
        }else if language == "en" {
            viewEnglish.backgroundColor = kYellowColor
            viewMyanmar.backgroundColor = UIColor.lightGray
            viewUnicode.backgroundColor = UIColor.lightGray
        }else {
            viewEnglish.backgroundColor = UIColor.lightGray
            viewMyanmar.backgroundColor = UIColor.lightGray
            viewUnicode.backgroundColor = kYellowColor
        }
        
        ReRegistrationModel.shared.Language = englishlanguage//language
        ok_default_language = englishlanguage// language
       // UserDefaults.standard.set(language, forKey:"currentLanguage")
        UserDefaults.standard.set(englishlanguage, forKey:"currentLanguage")
       // appDel.setSeletedlocaLizationLanguage(language: language)
        appDel.setSeletedlocaLizationLanguage(language: englishlanguage)
        tranView.removeFromSuperview()
        lblMarque.text = titleScreen
        if ReRegistrationModel.shared.screenFrom == "Regi_HelpVC" {
          ReRegistrationModel.shared.lType = ""
            
        }
        //let newImage = selfView.toImage()
        //bgImage = newImage
        UserDefaults.standard.set(true, forKey: "RigiRedirect")
        ReRegistrationModel.shared.ProfilePicAwsUrl = ""
        self.dismiss(animated: false, completion: {
            NotificationCenter.default.post(name: Notification.Name("CHANGELAGUAGE"), object: nil)
        })
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }

}

extension ReRegistrationVC : UIWebViewDelegate {
    
    public func webViewDidStartLoad(_ webView: UIWebView) {
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
        }
    }
    
    public func webViewDidFinishLoad(_ webView: UIWebView) {
        DispatchQueue.main.async {
            progressViewObj.removeProgressView()
        }
    }
}


extension ReRegistrationVC {
    
    func getAddressData() {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined:
                println_debug("notDetermined")
                locationUpdates()
                break
            case .denied :
                println_debug("denied")
                showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                
                break
            case .restricted :
                println_debug("restricted")
                showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                break
            case .authorizedAlways, .authorizedWhenInUse:
                locationUpdates()
                break
            }
        } else {
            alertViewObj.wrapAlert(title:"", body: "Please Enable Location Service".localized, img:#imageLiteral(resourceName: "location"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    func locationUpdates() {
        LocationManager.sharedInstance.getLocation { (location: CLLocation?, error) in
            if error != nil {
                println_debug(error.debugDescription)
                return
            }
            guard let _ = location else { return }
            if let location = location {
                ReRegistrationModel.shared.Latitude = String(location.coordinate.latitude)
                ReRegistrationModel.shared.Longitude = String(location.coordinate.longitude)
            }
        }
        
    }
}
