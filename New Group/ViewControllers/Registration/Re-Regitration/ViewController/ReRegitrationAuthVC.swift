//
//  ReRegitrationAuthVC.swift
//  OK
//
//  Created by Imac on 5/27/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class ReRegitrationAuthVC: OKBaseController {
  
    

    @IBOutlet weak var txtView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtView.text = "According to the Central Bank of Myanmar Regulation on Mobile Financial Services, those who open account in OK $ have been respectfully requested to provide correct personal information data and Citizenship Scrutiny Card(NRC) Number (or) Name of Citizenship Country. If you are citizenship of other countries please provide passport details.".localized
    }
    
    @IBAction func onClickCloseAction(_ sender: Any) {
    UserDefaults.standard.set(false, forKey: "RE_REGITRATION")
      self.dismiss(animated: false, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
   
    }
    @IBAction func onClickOkAction(_ sender: Any) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("BACK_TO_LOGIN"), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name("CHANGELAGUAGE"), object: nil)
        DispatchQueue.main.async(execute: {
            let sb1 = UIStoryboard(name: "ReRegistration", bundle: nil)
            let VC  = sb1.instantiateViewController(withIdentifier: "ReRegistrationNav")
            VC.modalPresentationStyle = .fullScreen
            if var topController = UIApplication.shared.keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }
                topController.present(VC, animated: false, completion: nil)
            }

            NotificationCenter.default.addObserver(
                self,
                selector: #selector(self.navigateToLoginPage),
                name: NSNotification.Name(rawValue: "BACK_TO_LOGIN"),
                object: nil)
            
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(self.ChangeLanguage),
                name: NSNotification.Name(rawValue: "CHANGELAGUAGE"),
                object: nil)
        })
    }
    
    @objc private func navigateToLoginPage(notification: NSNotification){
        self.dismiss(animated: false, completion: nil)
         NotificationCenter.default.removeObserver(self, name: Notification.Name("BACK_TO_LOGIN"), object: nil)
         NotificationCenter.default.removeObserver(self, name: Notification.Name("CHANGELAGUAGE"), object: nil)
    }
    
    @objc private func ChangeLanguage(notification: NSNotification) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("BACK_TO_LOGIN"), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name("CHANGELAGUAGE"), object: nil)
        DispatchQueue.main.async(execute: {
            let sb1 = UIStoryboard(name: "ReRegistration", bundle: nil)
            let VC  = sb1.instantiateViewController(withIdentifier: "ReRegistrationNav")
            VC.modalPresentationStyle = .fullScreen
            if var topController = UIApplication.shared.keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }
                topController.present(VC, animated: false, completion: nil)
            }
            
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(self.navigateToLoginPage),
                name: NSNotification.Name(rawValue: "BACK_TO_LOGIN"),
                object: nil)
            
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(self.ChangeLanguage),
                name: NSNotification.Name(rawValue: "CHANGELAGUAGE"),
                object: nil)
        })
     
    }
    
}
