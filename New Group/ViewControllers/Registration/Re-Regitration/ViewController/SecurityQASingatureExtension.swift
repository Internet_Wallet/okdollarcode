//
//  SecurityQASingatureExtension.swift
//  OK
//
//  Created by Imac on 6/3/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

extension ReRegistrationVC: SecurityQuestionRRCellDelegate,SecurityAnswerRRcellDelegate,SignatureRRCellDelegate,SignatureVCDelegate,PasswordHeaderRRCellDelegate,PasswordPatternViewControllerDelegate,ConfirmPasswordRRCellDelegate,PasswordRRCellDelegate,SecurityQuestionVCREgiDelegate {

    func resetSecurityType() {
        ReRegistrationModel.shared.PasswordType = ""
        ReRegistrationModel.shared.Password = ""
        ReRegistrationModel.shared.ConfirmPassword = ""
        ReRegistrationModel.shared.SecurityQuestionCode = ""
        ReRegistrationModel.shared.SecurityQuestionAnswer = ""
        SecurityQuestion = "Select Security Question".localized
        
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 0, section: 5)) as? PasswordHeaderRRCell {
            cell.imgPattern.image = UIImage(named:"r_Unradio")
            cell.imgPassword.image = UIImage(named:"r_Unradio")
        }
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 1, section: 5)) as? PasswordRRCell {
            cell.tfPassword.text = ""
            self.isPasswordFieldHidden = true
            self.crashcheckValue = "PasswordRRCell"
            reloadRows(index: 1, withSeciton: 5)
        }
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 5)) as? ConfirmPasswordRRCell {
            cell.tfConfirmPassword.text = ""
            self.isPasswordConrimFieldHidden = true
            crashcheckValue = "ConfirmPasswordRRCell"
            reloadRows(index: 2, withSeciton: 5)
        }
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 3, section: 5)) as? SecurityQuestionRRCell {
            cell.lblQuestion.text = "Select Security Question".localized
            SecurityQuestion = "Select Security Question".localized
            self.crashcheckValue = "SecurityQuestionRRCell"
            reloadRows(index: 3, withSeciton: 5)
        }
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 4, section: 5)) as? SecurityAnswerRRcell {
            cell.tfAnswer.text = ""
            cell.isNextViewHidden = true
            self.crashcheckValue = "SecurityAnswerRRcell"
            if pattrenpassword != ""{
                pattrenpassword = ""
            }
            reloadRows(index: 4, withSeciton: 5)
        }
//        if  self.isPasswordConrimFieldHidden == true{
//            securityRowCount = 2
//            tableViewRagi.deleteRows(at: [IndexPath(row: 2, section: 5)], with: .none)
//            tableViewRagi.reloadData()
//        }else{
            
      //  }
        if pattrenpassword != ""{
            pattrenpassword = ""
            isPasswordShow = false
            securityRowCount = 3
            tableViewRagi.deleteRows(at: [IndexPath(row: 3, section: 5)], with: .none)
            securityRowCount = 1
            tableViewRagi.reloadData()
        } else if crashcheckValue == "PasswordRRCell" {
            crashcheckValue = ""
            isPasswordShow = false
            securityRowCount = 1
            tableViewRagi.deleteRows(at: [IndexPath(row: 1, section: 5)], with: .none)
            securityRowCount = 1
            tableViewRagi.reloadData()
        } else if crashcheckValue == "ConfirmPasswordRRCell" {
            crashcheckValue = ""
            self.isPasswordShow = false
            securityRowCount = 2
            tableViewRagi.deleteRows(at: [IndexPath(row: 2, section: 5)], with: .none)
            securityRowCount = 1
            tableViewRagi.reloadData()
            
        }
        else if crashcheckValue == "SecurityQuestionRRCell" {
            crashcheckValue = ""
            self.isPasswordShow = false
            securityRowCount = 3
            tableViewRagi.deleteRows(at: [IndexPath(row: 3, section: 5)], with: .none)
            securityRowCount = 1
            tableViewRagi.reloadData()
            
        }
        
        else{
            securityRowCount = 4
            tableViewRagi.deleteRows(at: [IndexPath(row: 4, section: 5)], with: .none)
            self.isPasswordShow = false
            securityRowCount = 1
            tableViewRagi.reloadData()
        }
       
    }
    
    func showAlertForPasswordInfor(txt : String) {
        DispatchQueue.main.async {
            self.showAlert(alertTitle: "", alertBody: txt, alertImage: UIImage(named: "alert-icon")!)}
        //  reloadRows(index: 1, withSeciton: 5)
    }
    
    func passwordEdit(txt : String) {
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 1, section: 5)) as? PasswordRRCell {
           //this will work when back space is given
            if txt.count == 0 || txt.count < 6 {
                cell.tfPassword.text = txt
                if cell.tfPassword.text == ""{
                    cell.lblPasswordMarque.text = "Create Password(Min 6 digits)".localized
                }
                else{
                    cell.lblPasswordMarque.text = ""
                }
               
                if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 5)) as? ConfirmPasswordRRCell {
                    cell.tfConfirmPassword.text = ""
                    self.isPasswordConrimFieldHidden = true
                    ReRegistrationModel.shared.ConfirmPassword = ""
                    self.isPasswordConrimFieldHidden = true
                    reloadRows(index: 2, withSeciton: 5)
                }
            }else if txt.count > 5 {
                if cell.isNextViewHidden {
                    if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 5)) as? ConfirmPasswordRRCell {
                        cell.tfConfirmPassword.text = ""
                        self.isPasswordConrimFieldHidden = false
                        reloadRows(index: 2, withSeciton: 5)
                    }else {
                        //this will work for the first time
                        if self.tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 5)) as? ConfirmPasswordRRCell == nil {
                        self.isPasswordConrimFieldHidden = false
                        securityRowCount = 3
                        insertRow(index: 2, withSeciton: 5)
                        }
                    }
                }
//                else {
//                    self.isPasswordConrimFieldHidden = false
//                    reloadRows(index: 2, withSeciton: 5)
//                }
            } else {
                if !cell.isNextViewHidden {
                    self.isPasswordConrimFieldHidden = true
                    reloadRows(index: 2, withSeciton: 5)
                }
                cell.lblPasswordMarque.text = ""
            }
        }
    }
    
    func showPatternVC() {
        self.view.endEditing(true)
        let sb = UIStoryboard(name: "Registration", bundle: nil)
       let  passwordPatternViewControllerR = sb.instantiateViewController(withIdentifier: "PasswordPatternViewControllerR") as! PasswordPatternViewControllerR
        passwordPatternViewControllerR.delegate = self
        passwordPatternViewControllerR.mobilNumber = ReRegistrationModel.shared.MobileNumber
        self.navigationController?.pushViewController(passwordPatternViewControllerR, animated: false)
    }
    
    func ShowPatternPasswordViewAndPasswordString(strPassword : String, PasswordPatternViewControllerR : UIViewController) {
        PasswordPatternViewControllerR.navigationController?.popViewController(animated: false)
        // isSecurityQShown = true
        ReRegistrationModel.shared.PasswordType = "1"
        ReRegistrationModel.shared.Password = strPassword
        pattrenpassword = strPassword
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 0, section: 5)) as? PasswordHeaderRRCell {
            cell.imgPattern.image = UIImage(named:"select_radio")
            cell.imgPassword.image = UIImage(named:"r_Unradio")
            cell.isNextViewHidden = true
            if cell.isNextViewHidden {
                cell.isNextViewHidden  = false
                if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 1, section: 5)) as? PasswordRRCell {
                    cell.tfPassword.text = ""
                    self.isPasswordFieldHidden = true
                    reloadRows(index: 1, withSeciton: 5)
                }else {
                   if self.tableViewRagi.cellForRow(at: IndexPath(row: 1, section: 5)) as? PasswordRRCell == nil {
                        self.isPasswordFieldHidden = true
                        securityRowCount = 2
                        insertRow(index: 1, withSeciton: 5)
                    }
                }
                
                if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 5)) as? ConfirmPasswordRRCell {
                    cell.tfConfirmPassword.text = ""
                    self.isPasswordConrimFieldHidden = true
                    reloadRows(index: 2, withSeciton: 5)
                }else {
                   if self.tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 5)) as? ConfirmPasswordRRCell == nil {
                    self.isPasswordConrimFieldHidden = true
                    securityRowCount = 3
                    insertRow(index: 2, withSeciton: 5)
                    }
                }
                
                if let _ = self.tableViewRagi.cellForRow(at: IndexPath(row: 3, section: 5)) as? SecurityQuestionRRCell {
                    reloadRows(index: 3, withSeciton: 5)
                }else {
                    if self.tableViewRagi.cellForRow(at: IndexPath(row: 3, section: 5)) as? SecurityQuestionRRCell == nil {
                    securityRowCount = 4
                    insertRow(index: 3, withSeciton: 5)
                    }
                }
            }else {
//                if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 1, section: 5)) as? PasswordRRCell {
//                    cell.tfPassword.text = ""
//                    self.isPasswordFieldHidden = true
//                    reloadRows(index: 1, withSeciton: 5)
//                }
//
//                if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 5)) as? ConfirmPasswordRRCell {
//
//                    self.isPasswordConrimFieldHidden = true
//                    reloadRows(index: 2, withSeciton: 5)
//                }
                
            }
        }
        
    }
    func CancelPatternPasswordVC(PasswordPatternViewControllerR : UIViewController) {
           PasswordPatternViewControllerR.navigationController?.popViewController(animated: false)
    }
    
    func showPasspwordView(isHidden: Bool) {
        if isHidden {
            if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 1, section: 5)) as? PasswordRRCell {
                self.isPasswordFieldHidden = false
                cell.tfPassword.becomeFirstResponder()
                reloadRows(index: 1, withSeciton: 5)
            }else {
                if !isPasswordShow {
                    isPasswordShow = true
                    securityRowCount = 2
                    self.isPasswordFieldHidden = false
                    insertRow(index: 1, withSeciton: 5)
                }else {
                        self.isPasswordFieldHidden = false
                        reloadRows(index: 1, withSeciton: 5)
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                    if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 1, section: 5)) as? PasswordRRCell {
                        cell.tfPassword.becomeFirstResponder()
                    }
                })
            }
        }else {
            if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 1, section: 5)) as? PasswordRRCell {
                self.isPasswordFieldHidden = false
                cell.tfPassword.becomeFirstResponder()
                reloadRows(index: 1, withSeciton: 5)
            }
            if let _ = self.tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 5)) as? ConfirmPasswordRRCell {
                self.isPasswordConrimFieldHidden = true
                reloadRows(index: 2, withSeciton: 5)
            }
        }
    }
    
    func showSecurityQuestionField() {
        if let _ = self.tableViewRagi.cellForRow(at: IndexPath(row: 3, section: 5)) as? SecurityQuestionRRCell {
            reloadRows(index: 3, withSeciton: 5)
        }else {
            securityRowCount = 4
            insertRow(index: 3, withSeciton: 5)
        }
    }

    func showSecurityQuestionView() {
        self.view.endEditing(true)
        let securityQuestionR  = self.storyboard?.instantiateViewController(withIdentifier: "SecurityQuestionVCREgi") as! SecurityQuestionVCREgi
        securityQuestionR.delegate = self
        self.navigationController?.pushViewController(securityQuestionR, animated: true)
    }
    func SelectedQuestion(questionDic: Dictionary<String, String>) {
        
        var questionString = ""
        if ReRegistrationModel.shared.Language.lowercased() == "my" {
            questionString = questionDic["QuestionBurmese"] ?? ""
        }else if ReRegistrationModel.shared.Language.lowercased() == "en"{
            questionString = questionDic["Question"] ?? ""
        }else {
            questionString = questionDic["QuestionUnicode"] ?? ""
        }
        if let cell = tableViewRagi.cellForRow(at: NSIndexPath(row: 3, section: 5) as IndexPath) as? SecurityQuestionRRCell {
            ReRegistrationModel.shared.SecurityQuestionCode = questionDic["QuestionCode"] ?? ""
            cell.lblQuestion.text = questionString
            self.SecurityQuestion = questionString
        }
        
        if let cell = tableViewRagi.cellForRow(at: NSIndexPath(row: 4, section: 5) as IndexPath) as? SecurityAnswerRRcell {
            ReRegistrationModel.shared.SecurityQuestionAnswer = ""
            cell.tfAnswer.text = ""
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                cell.tfAnswer.becomeFirstResponder()
            })
        }else {
            if tableViewRagi.cellForRow(at: NSIndexPath(row: 4, section: 5) as IndexPath) as? SecurityAnswerRRcell == nil {
                securityRowCount = 5
                insertRow(index: 4, withSeciton: 5)
            }
        }
        
    }
    
    func showSignatureView() {
        if singatureRowCount == 1{
            //
        }else{
            singatureRowCount = 1
            insertRow(index: 0, withSeciton: 6)
        }
   
    }
    
    func presentSignatureVC() {
        let sb = UIStoryboard(name: "Signature", bundle: nil)
        let signatureVC  = sb.instantiateViewController(withIdentifier: "SignatureVC") as! SignatureVC
        signatureVC.delegate = self
        self.present(signatureVC, animated:false , completion: nil)
    }
    
    func setSignatureImage(image: UIImage) {
        if let cell = tableViewRagi.cellForRow(at: NSIndexPath(row: 0, section: 6) as IndexPath) as? SignatureRRCell {
            cell.imgreturn.isHidden = false
            cell.imgreturn.image = image
            ReRegistrationModel.shared.signature = OKBaseController.imageTobase64(image: image)
            //shownext view
            if cell.isNextViewHidden {
                cell.isNextViewHidden = false
                //New Changes to hide alternate & confrimation number
                AlternateRowCount = 0
                ReferralRowCount = 0
                isAddtitionalInformatonHiddenForPersonal = false
                self.AdditonalRowCount = 1
                self.insertRow(index: 0, withSeciton: 9)
                self.AdditonalRowCount = 2
                self.insertRow(index: 1, withSeciton: 9)
                self.AdditonalRowCount = 3
                self.insertRow(index: 2, withSeciton: 9)
                
 /*               if ReRegistrationModel.shared.AccountType == 0 {
                    AlternateRowCount = 0
                    isAddtitionalInformatonHiddenForPersonal = false
                    ReferralRowCount = 1
                    insertRow(index: 0, withSeciton: 8)
                    ReferralRowCount = 2
                    insertRow(index: 1, withSeciton: 8)
                    ReferralRowCount = 3
                    insertRow(index: 2, withSeciton: 8)
                    
                    self.AdditonalRowCount = 1
                    self.insertRow(index: 0, withSeciton: 9)
                    self.AdditonalRowCount = 2
                    self.insertRow(index: 1, withSeciton: 9)
                    self.AdditonalRowCount = 3
                    self.insertRow(index: 2, withSeciton: 9)
                }else {
                    AlternateRowCount = 1
                    insertRow(index: 0, withSeciton: 7)
                    AlternateRowCount = 2
                    insertRow(index: 1, withSeciton: 7)
                    AlternateRowCount = 3
                    insertRow(index: 2, withSeciton: 7)
                }*/
                
            }
        }
    }
    
    
    func showDemoSecurityPassword() {
        var strURL = ""
        if ReRegistrationModel.shared.Language.lowercased() == "my"{
            strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/d9ded764-c099-4405-b56c-a1a41683748aReqDocumentsJuly_02_2018%2001_25_35%20PM.jpg";
        }else {
            strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/7c810532-0aad-4a05-8b65-2234d8104578ReqDocumentsJuly_02_2018%2001_35_58%20PM.jpg";
        }
        self.view.endEditing(true)
        webviewContainer.isHidden = false
        webview.scalesPageToFit = true
        webview.delegate = self
        lblDemo.text = "Select Security Type".localized
        webview.loadRequest(URLRequest(url: URL(string: strURL)!))
    }
    
    
}
