//
//  BusinessExtension.swift
//  OK
//
//  Created by Imac on 6/2/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

extension ReRegistrationVC: BusinessHeaderRRCellDelegate,BusinessNameRRCellDelegate,BusinessCategoryRRCellDelegate,BusinessSubCategoryRRCellDelegate,BusinessCategoryVCDelegate,BusinessSubCategoryVCDelegate,BusinessPhotoRRCellDelegate,AgentShopImagesDelegate,PAImagePreviewVCDelegate,ReRegiTakePhotoDelegate{

    
    func resetBusiness() {
        ReRegistrationModel.shared.BusinessName = ""
        ReRegistrationModel.shared.BusinessType = ""
        ReRegistrationModel.shared.BusinessCategory = ""
        ReRegistrationModel.shared.BusinessIdPhoto1AwsUrl = ""
        ReRegistrationModel.shared.BusinessIdPhoto2AwsUrl = ""
        ReRegistrationModel.shared.ShopPicOneAwsUrl = ""
        ReRegistrationModel.shared.ShopPicTwoAwsUrl = ""
 
        if let cell = tableViewRagi.cellForRow(at: IndexPath(row: 1, section: 3)) as? BusinessNameRRCell {
            cell.tfBusinessName.text = ""
        }
        
        if let cell = tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 3)) as? BusinessCategoryRRCell {
            cell.lblBusinessCategory.text = "Select Business Category".localized
            cell.imgCategory.image = UIImage(named: "sub_categories")
        }
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 3, section: 3)) as? BusinessSubCategoryRRCell {
            cell.lblSubBusinessCategory.text = "Select Business Sub-Category".localized
            cell.lblSubCatImg.text = ""
            cell.lblSubCatImg.isHidden = true
            cell.imgSubCategory.isHidden = false
        }
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 4, section: 3)) as? BusinessPhotoRRCell {
            cell.imgFront.image = UIImage(named: "r_nrc")
            cell.imgBack.image = UIImage(named: "r_nrc")
            isIDBusinessAttached = false
            self.imageListBusiness.removeAll()
            self.reloadRows(index: 4, withSeciton: 3)
        }
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 5, section: 3)) as? AgentShopImages {
            cell.imgFront.image = UIImage(named: "r_nrc")
            cell.imgBack.image = UIImage(named: "r_nrc")
            isIDAgentAttached = false
            self.imageListAgent.removeAll()
            self.reloadRows(index: 5, withSeciton: 3)
        }
    }
  

    func showCategory() {
        businessRowCount = 3
        insertRow(index: 2, withSeciton: 3)
    }
    
    func navigateToBusinessCategory() {
        let sb = UIStoryboard(name: "Registration", bundle: nil)
        let businessCategoryVC  = sb.instantiateViewController(withIdentifier: "BusinessCategoryVC") as! BusinessCategoryVC
        businessCategoryVC.bCatDelegate = self
        self.navigationController?.pushViewController(businessCategoryVC, animated: true)
    }
    
    func setBusinessCategoryNameImage(Dicti : CategoryDetail) {
        ReRegistrationModel.shared.BusinessCategory = Dicti.categoryCode
        ReRegistrationModel.shared.BusinessType = ""
        categoryDetail = Dicti
        
        if let cell = tableViewRagi.cellForRow(at: IndexPath(row: 2, section: 3)) as? BusinessCategoryRRCell {
            if ReRegistrationModel.shared.Language == "my" {
                cell.lblBusinessCategory.text = Dicti.categoryBurmeseName
            }else if ReRegistrationModel.shared.Language == "en" {
                cell.lblBusinessCategory.text = Dicti.categoryName
            }else {
                cell.lblBusinessCategory.text = Dicti.categoryBurmeseUName
            }
            cell.imgCategory.image = UIImage(named: Dicti.category_image)
            
            if cell.isNextViewHidden {
                if self.tableViewRagi.cellForRow(at: IndexPath(row: 3, section: 3)) as? BusinessSubCategoryRRCell == nil {
                    cell.isNextViewHidden = false
                    self.businessRowCount = 4
                    self.insertRow(index: 3, withSeciton: 3)
                }
            }
        }
        
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 3, section: 3)) as? BusinessSubCategoryRRCell {
            cell.lblSubBusinessCategory.text = "Select Business Sub-Category".localized
            cell.lblSubCatImg.text = ""
            cell.lblSubCatImg.isHidden = true
            cell.imgSubCategory.isHidden = false
        }
    }
    
    func setBusinessSubcategoryNameImage(sub_dictionary :SubCategoryDetail) {
        ReRegistrationModel.shared.BusinessType = replaceSubCat(sbCat: sub_dictionary.subCategoryCode)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 3, section: 3)) as? BusinessSubCategoryRRCell {
                if ReRegistrationModel.shared.Language == "my"{
                    cell.lblSubBusinessCategory.text = sub_dictionary.subCategoryBurmeseName
                }else if ReRegistrationModel.shared.Language == "en"{
                    cell.lblSubBusinessCategory.text = sub_dictionary.subCategoryName
                }else {
                    cell.lblSubBusinessCategory.text = sub_dictionary.subCategoryBurmeseUName
                }
                cell.lblSubCatImg.text = sub_dictionary.subCategoryEmoji
                cell.lblSubCatImg.isHidden = false
                cell.imgSubCategory.isHidden = true
                
                if cell.isNextViewHidden {
                    if self.tableViewRagi.cellForRow(at: IndexPath(row: 4, section: 3)) as? BusinessPhotoRRCell == nil {
                        self.businessRowCount = 5
                        cell.isNextViewHidden = false
                        self.insertRow(index: 4, withSeciton: 3)
                    }
                }
            }    })
        
    }
    
    func backFromBusinessSubCategoryVCWithNameImage(dicSubCate: SubCategoryDetail) {
        ReRegistrationModel.shared.BusinessType = replaceSubCat(sbCat: dicSubCate.subCategoryCode)
        if let cell = self.tableViewRagi.cellForRow(at: IndexPath(row: 3, section: 3)) as? BusinessSubCategoryRRCell {
            if ReRegistrationModel.shared.Language == "my"{
                cell.lblSubBusinessCategory.text = dicSubCate.subCategoryBurmeseName
            }else if ReRegistrationModel.shared.Language == "en"{
                cell.lblSubBusinessCategory.text = dicSubCate.subCategoryName
            }else {
                cell.lblSubBusinessCategory.text = dicSubCate.subCategoryBurmeseUName
            }
            cell.lblSubCatImg.text = dicSubCate.subCategoryEmoji
            cell.lblSubCatImg.isHidden = false
            cell.imgSubCategory.isHidden = true
            
            if cell.isNextViewHidden {
                if self.tableViewRagi.cellForRow(at: IndexPath(row: 4, section: 3)) as? BusinessPhotoRRCell == nil {
                    self.businessRowCount = 5
                    cell.isNextViewHidden = false
                    self.insertRow(index: 4, withSeciton: 3)
                }
            }
        }
    }
    
    func replaceSubCat(sbCat : String) ->String {
        var st = sbCat
        if st.count == 1 {
            st = "0" + st
        }
        return st
    }
    
    
    func navigateToBusinessSubCategory() {
          let sb = UIStoryboard(name: "Registration", bundle: nil)
        let businessSubCategoryVC  = sb.instantiateViewController(withIdentifier: "BusinessSubCategoryVC") as! BusinessSubCategoryVC
        businessSubCategoryVC.businessSubCategoryVCDelegate = self
        businessSubCategoryVC.dicName = categoryDetail
        self.navigationController?.pushViewController(businessSubCategoryVC, animated: true)
    }
    
      func navigateToTakeBussinesImages(tag : Int){
        DispatchQueue.main.async {
            let imgVC = self.storyboard?.instantiateViewController(withIdentifier: "ReRegiTakePhoto")  as! ReRegiTakePhoto
            imgVC.delegate = self
            imgVC.strTitle = "Attach Business".localized
            imgVC.imagesAdded = self.imageListBusiness
            imgVC.imageURLs = [ReRegistrationModel.shared.BusinessIdPhoto1AwsUrl,ReRegistrationModel.shared.BusinessIdPhoto2AwsUrl]
            imgVC.screenFrom = "Business"
            if tag == 100 {
                imgVC.completeUploadImageMode = "FIRST_SHOW"
            }else if tag == 200 {
                imgVC.completeUploadImageMode = "SECOND_SHOW"
            }else {
                if self.imageListBusiness.count == 0 {
                imgVC.completeUploadImageMode = "FIRST"
                }else if self.imageListBusiness.count == 1 {
                    imgVC.completeUploadImageMode = "SECOND_SHOW"
                }else {
                    return
                }
            }
            self.navigationController?.pushViewController(imgVC, animated: true)
        }
    }
    func navigateToAgentImages(tag : Int) {
        DispatchQueue.main.async {
            let imgVC = self.storyboard?.instantiateViewController(withIdentifier: "ReRegiTakePhoto")  as! ReRegiTakePhoto
            imgVC.delegate = self
            imgVC.strTitle = "Attach shop images".localized
            imgVC.imagesAdded = self.imageListAgent
            imgVC.imageURLs = [ReRegistrationModel.shared.ShopPicOneAwsUrl,ReRegistrationModel.shared.ShopPicTwoAwsUrl]
            imgVC.screenFrom = "Agent"
            if tag == 100 {
                 imgVC.completeUploadImageMode = "FIRST_SHOW"
            }else if tag == 200 {
                imgVC.completeUploadImageMode = "SECOND_SHOW"
            }else {
                if self.imageListAgent.count == 0 {
                    imgVC.completeUploadImageMode = "FIRST"
                }else if self.imageListAgent.count == 1 {
                    imgVC.completeUploadImageMode = "SECOND_SHOW"
                }else {
                    return
                }
            }
            self.navigationController?.pushViewController(imgVC, animated: true)
        }
    }
    func imagewithURLs(fromScreen: String, imageURLs: [String], images: [UIImage]) {
        if fromScreen == "Business" {
            self.imageListBusiness = images
            if imageURLs.count == 2 {
                ReRegistrationModel.shared.BusinessIdPhoto1AwsUrl = imageURLs[0].replacingOccurrences(of: " ", with: "%20")
                ReRegistrationModel.shared.BusinessIdPhoto2AwsUrl = imageURLs[1].replacingOccurrences(of: " ", with: "%20")
                isIDBusinessAttached = true
                self.reloadRows(index: 4, withSeciton: 3)
            if let cell = tableViewRagi.cellForRow(at: IndexPath(row: 4, section: 3)) as? BusinessPhotoRRCell {
                if cell.isNextViewHidden {
                    //isAddressViewHidden
                    if (ReRegistrationModel.shared.BusinessIdPhoto1AwsUrl != "") && (ReRegistrationModel.shared.BusinessIdPhoto2AwsUrl != "") {
                        if cell.isNextViewHidden {
                            cell.isNextViewHidden = false
                            if ReRegistrationModel.shared.lType == "AGENT" {
                                if isAgentViewHidden {
                                    isAgentViewHidden = false
                                    self.businessRowCount = 6
                                    self.insertRow(index: 5, withSeciton: 3)
                                }
                            }else {
                                if isAddressViewHidden {
                                    addressRowCount = 1
                                    insertRow(index: 0, withSeciton: 4)
                                    addressRowCount = 2
                                    insertRow(index: 1, withSeciton: 4)
                                    addressRowCount = 3
                                    insertRow(index: 2, withSeciton: 4)
                                    addressRowCount = 4
                                    insertRow(index: 3, withSeciton: 4)
                                    addressRowCount = 5
                                    insertRow(index: 4, withSeciton: 4)
                                    addressRowCount = 6
                                    insertRow(index: 5, withSeciton: 4)
                                    addressRowCount = 7
                                    insertRow(index: 6, withSeciton: 4)
                                    addressRowCount = 8
                                    insertRow(index: 7, withSeciton: 4)
                                    addressRowCount = 9
                                    insertRow(index: 8, withSeciton: 4)
                                    isAddressViewHidden = false
                                }
                            
                            }
                        }
                    }
                }
                }
            }else if imageURLs.count == 1{
                ReRegistrationModel.shared.BusinessIdPhoto1AwsUrl = imageURLs[0].replacingOccurrences(of: " ", with: "%20")
            }
        }else {
            self.imageListAgent = images
            if imageURLs.count == 2 {
                ReRegistrationModel.shared.ShopPicOneAwsUrl = imageURLs[0].replacingOccurrences(of: " ", with: "%20")
                ReRegistrationModel.shared.ShopPicTwoAwsUrl = imageURLs[1].replacingOccurrences(of: " ", with: "%20")
                isIDAgentAttached = true
                self.reloadRows(index: 5, withSeciton: 3)
            if let cell = tableViewRagi.cellForRow(at: IndexPath(row: 5, section: 3)) as? AgentShopImages {
                if cell.isNextViewHidden {
                    if (ReRegistrationModel.shared.ShopPicOneAwsUrl != "") && (ReRegistrationModel.shared.ShopPicTwoAwsUrl != "") {
                        if cell.isNextViewHidden {
                            cell.isNextViewHidden = false
                            if isAddressViewHidden {
                                isAddressViewHidden = false
                                addressRowCount = 1
                                insertRow(index: 0, withSeciton: 4)
                                addressRowCount = 2
                                insertRow(index: 1, withSeciton: 4)
                                addressRowCount = 3
                                insertRow(index: 2, withSeciton: 4)
                                addressRowCount = 4
                                insertRow(index: 3, withSeciton: 4)
                                addressRowCount = 5
                                insertRow(index: 4, withSeciton: 4)
                                addressRowCount = 6
                                insertRow(index: 5, withSeciton: 4)
                                addressRowCount = 7
                                insertRow(index: 6, withSeciton: 4)
                                addressRowCount = 8
                                insertRow(index: 7, withSeciton: 4)
                                addressRowCount = 9
                                insertRow(index: 8, withSeciton: 4)
                            }
                        }
                    }
                }
            }
            }else if imageURLs.count == 1{
                ReRegistrationModel.shared.ShopPicOneAwsUrl = imageURLs[0].replacingOccurrences(of: " ", with: "%20")
            }
        }
    }
    
    
    func showBusinessInfo() {
        var strURL = ""
        if ReRegistrationModel.shared.Language.lowercased() == "my"{
            strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/831520e3-ca48-4202-9347-6d76c1f32c6aReqDocumentsJuly_02_2018%2001_20_09%20PM.jpg";
        }else {
            strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/ccebdb05-bb2e-43da-8af4-293147597627ReqDocumentsJuly_02_2018%2001_33_23%20PM.jpg";
        }
        self.view.endEditing(true)
        webviewContainer.isHidden = false
        webview.scalesPageToFit = true
        webview.delegate = self
        lblDemo.text = "Business Details".localized
        webview.loadRequest(URLRequest(url: URL(string: strURL)!))
    }
    
}
