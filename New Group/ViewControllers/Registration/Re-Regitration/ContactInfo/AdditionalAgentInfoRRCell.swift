//
//  AdditionalAgentInfoRRCell.swift
//  OK
//
//  Created by Imac on 6/4/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol AdditionalAgentInfoRRCellDelegate {
    func agentCode(isAgentCode :Bool)
}

class AdditionalAgentInfoRRCell: UITableViewCell {
    var delegate: AdditionalAgentInfoRRCellDelegate?
    @IBOutlet weak var imgSelected: UIImageView!
    @IBOutlet weak var lblAgent: MarqueeLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func updateLaguageCell(agentCode : Bool) {
        if agentCode {
           imgSelected.image = UIImage(named: "radio_select")
        }else {
           imgSelected.image = UIImage(named: "radio")
        }
        lblAgent.text = "You have Agent code please click here".localized
    }
    @IBAction func onClickAgentAction(_ sender: Any) {
        if imgSelected.image == UIImage(named: "radio") {
           // imgSelected.image = UIImage(named: "radio_select")
            if let del = delegate {
                del.agentCode(isAgentCode: true)
            }
        }else {
           // imgSelected.image = UIImage(named: "radio")
            if let del = delegate {
                del.agentCode(isAgentCode: false)
            }
        }
    }
}
