//
//  ContactReferralHeaderRRCell.swift
//  OK
//
//  Created by Imac on 6/4/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
protocol ContactReferralHeaderRRCellDelegate {
    func resetReferralNumber()
}

class ContactReferralHeaderRRCell: UITableViewCell {
    var delegate: ContactReferralHeaderRRCellDelegate?
    @IBOutlet weak var lblHeader: MarqueeLabel!{
        didSet{
            lblHeader.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnReset: UIButton!{
        didSet{
            btnReset.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
        func updateLaguageCell() {
        if ReRegistrationModel.shared.Language.lowercased() == "en" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue"), for: .normal)
        }else if ReRegistrationModel.shared.Language.lowercased() == "my" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
        }
        lblHeader.attributedText = lblHeader.attributedStringFinal(str1: "Referral Number".localized)
    }
    
    @IBAction func onClickResetAction(_ sender: Any) {
        if let del = delegate {
            del.resetReferralNumber()
        }
    }
}
