//
//  AgetCodeRRCell.swift
//  OK
//
//  Created by Imac on 6/4/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class AgetCodeRRCell: UITableViewCell,UITextFieldDelegate {
    @IBOutlet weak var tfAgentCode: UITextField!
    @IBOutlet weak var btnClear: UIButton!
    var AGENTCODESET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890/-,:()"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tfAgentCode.delegate = self
    }
    
    func updateLaguageCell() {
        tfAgentCode.placeholder = "Enter Your Agent Code".localized
        tfAgentCode.text = ReRegistrationModel.shared.AgentAuthCode
        if ReRegistrationModel.shared.AgentAuthCode.count > 0 {
            btnClear.isHidden  = false
        }else {
             btnClear.isHidden  = true
        }
    }

    @IBAction func onClickClear(_ sender: Any) {
        btnClear.isHidden  = true
        tfAgentCode.text = ""
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        ReRegistrationModel.shared.AgentAuthCode = textField.text ?? ""
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if range.location == 0 && string == " " {
            return false
        }
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: AGENTCODESET).inverted).joined(separator: "")) { return false }
        if text.count > 0 {
            btnClear.isHidden = false
        }else {
            btnClear.isHidden = true
        }
        return true
    }
}
