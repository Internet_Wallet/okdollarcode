//
//  FaceBookRRCell.swift
//  OK
//
//  Created by Imac on 6/4/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class FaceBookRRCell: UITableViewCell,UITextFieldDelegate {
    @IBOutlet weak var tfFacebook: UITextField!{
        didSet{
            tfFacebook.font = UIFont(name: appFont, size: 18.0)
        }
    }
    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var lblHeader: UILabel!{
        didSet{
            lblHeader.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    var FACEBOOKCHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890.@_ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာ ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ "

    override func awakeFromNib() {
        super.awakeFromNib()
        tfFacebook.delegate = self
        lblHeader.text = "Additional Contact Information".localized
       
        var font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
            font = UIFont.systemFont(ofSize: 18)
        }
        tfFacebook.font = font
        
        
        
        if ReRegistrationModel.shared.Language.lowercased() == "my"{
          tfFacebook.font = UIFont(name: appFont, size: 18)
        }else if ReRegistrationModel.shared.Language.lowercased() == "en"{
          tfFacebook.font = UIFont.systemFont(ofSize: 18)
        }else {
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfFacebook.attributedPlaceholder = NSAttributedString(string: "Enter Facebook ID", attributes:attributes as [NSAttributedString.Key : Any])
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfFacebook.attributedPlaceholder = NSAttributedString(string: "Enter Facebook ID", attributes:attributes)
                tfFacebook.font = UIFont.systemFont(ofSize: 18)
            }
        }
         tfFacebook.placeholder = "Enter Facebook ID".localized
    }

    func updateLaguageCell() {
      
        tfFacebook.text = ReRegistrationModel.shared.FBEmailId
        if ReRegistrationModel.shared.FBEmailId.count > 0 {
            btnClear.isHidden  = false
        }else {
            btnClear.isHidden  = true
        }
    }
    
    @IBAction func onClickClear(_ sender: Any) {
        btnClear.isHidden  = true
        tfFacebook.text = ""
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        ReRegistrationModel.shared.FBEmailId = textField.text ?? ""
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if range.location == 0 && string == " " {
            return false
        }
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: FACEBOOKCHARSET).inverted).joined(separator: "")) { return false }
        if text.count > 0 {
            tfFacebook.isHidden = false
        }else {
            tfFacebook.isHidden = true
        }
        return true
    }
}
