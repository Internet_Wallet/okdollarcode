//
//  ReferralConfirmNumberRRCell.swift
//  OK
//
//  Created by Imac on 6/4/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
protocol ReferralConfirmNumberRRCellDelegate {
    func editChangedReferralConfirm(text :String)
    func endTextFieldReferralNumber(text: String)
    func clearReferralConfirmNumber()
}

class ReferralConfirmNumberRRCell: UITableViewCell,UITextFieldDelegate {
    var delegate: ReferralConfirmNumberRRCellDelegate?
    @IBOutlet weak var imgCountryFlag: UIImageView!
    @IBOutlet weak var btnClose: UIButton!{
        didSet{
            btnClose.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var tfConfirmMobilNumber: RestrictedCursorMovement!{
        didSet{
            tfConfirmMobilNumber.font = UIFont(name: appFont, size: 18.0)
        }
    }
    @IBOutlet weak var lblCountryCode: UILabel!{
        didSet{
            lblCountryCode.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tfConfirmMobilNumber.delegate = self
    }
    
    func updateLaguageCell(flag: String, countryCode: String, number: String, contactName: String) {
        tfConfirmMobilNumber.placeholder = "Confirm Referral No.".localized
        imgCountryFlag.image = UIImage(named: flag)
        if contactName != "" {
            lblCountryCode.text = ""
            tfConfirmMobilNumber.text = contactName
            tfConfirmMobilNumber.isUserInteractionEnabled = false
            btnClose.isHidden = true
        }else {
            lblCountryCode.text = "(" + countryCode + ")"
            if flag == "myanmar" && number == "" {
                tfConfirmMobilNumber.text = "09"
                btnClose.isHidden = true
            }else if number == "" {
                tfConfirmMobilNumber.text = ""
                btnClose.isHidden = true
            }else {
                 tfConfirmMobilNumber.text = number
            }
            tfConfirmMobilNumber.isUserInteractionEnabled = true
        }
        
    }
    
    @IBAction func editChanged(_ sender: UITextField) {
        if let del = delegate {
            del.editChangedReferralConfirm(text: sender.text ?? "")
        }
    }
    public func textFieldDidEndEditing(_ textField: UITextField) {
        if let del = delegate {
            del.endTextFieldReferralNumber(text: textField.text ?? "")
        }
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.keyboardType = .numberPad
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if range.location == 0 && string == " " {
            return false
        }
        if textField.selectedTextRange?.start != textField.selectedTextRange?.end {
            let endOfDocument = textField.endOfDocument
            textField.selectedTextRange = textField.textRange(from: endOfDocument, to: endOfDocument)
            return false
        }
        //let chars = textField.text! + string;
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "1234567890").inverted).joined(separator: "")) { return false }
        if text.count > 3 {
            btnClose.isHidden = false
        }else {
            btnClose.isHidden = true
        }
        if imgCountryFlag.image == UIImage(named: "myanmar") {
            if range.location == 0 && range.length > 1 {
                textField.text = "09"
                btnClose.isHidden = true
                return false
            }
            if range.location == 1 {
                return false
            }
        }
        
        return true
    }
    
    
    @IBAction func onClickClearAction(_ sender: Any) {
        if let del = delegate {
            del.clearReferralConfirmNumber()
        }
    }
}
