//
//  ContactHeaderRRCell.swift
//  OK
//
//  Created by Imac on 6/3/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
protocol ContactHeaderRRCellDelegate {
    func resetAlternateNumber()
}

class ContactHeaderRRCell: UITableViewCell {
    var delegate: ContactHeaderRRCellDelegate?
    
    @IBOutlet weak var lblHeader: MarqueeLabel!{
        didSet{
            lblHeader.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnReset: UIButton!{
        didSet{
            btnReset.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
        self.lblHeader.attributedText = self.lblHeader.attributedStringFinal(str1: "Please enter alternative number for transfer account balance in case of sim lost".localized)
        })
    }
    
    func updateLaguageCell() {
        if ReRegistrationModel.shared.Language.lowercased() == "en" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue"), for: .normal)
        }else if ReRegistrationModel.shared.Language.lowercased() == "my" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
        }else {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            self.lblHeader.attributedText = self.lblHeader.attributedStringFinal(str1: "Please enter alternative number for transfer account balance in case of sim lost".localized)
        })

    }

    @IBAction func onClickResetAction(_ sender: Any) {
        if let del = delegate {
            del.resetAlternateNumber()
        }
    }
}
