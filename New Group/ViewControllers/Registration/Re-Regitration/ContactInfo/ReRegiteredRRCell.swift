//
//  ReRegiteredRRCell.swift
//  OK
//
//  Created by Imac on 6/4/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import WebKit
protocol ReRegiteredRRCellDelegate {
    func reRegister()
}

class ReRegiteredRRCell: UITableViewCell {
    var delegate: ReRegiteredRRCellDelegate?
    @IBOutlet weak var btnRegister: UIButton!{
        didSet{
            btnRegister.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var lblTC: MarqueeLabel!{
        didSet{
            lblTC.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var webView: WKWebView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblTC.text = "Terms & Conditions".localized
        var str = ""
        if ReRegistrationModel.shared.AccountType == 1 {
            if ok_default_language == "my" {
                str = "okdollar_terms_personal_burmese_Unicode"
            }else if ok_default_language == "en" {
                str = "okdollar_terms_personal"
            }else {
                str = "okdollar_terms_personal_burmese"
            }
        }else {
            if ok_default_language == "my" {
                str = "okdollar_terms_merchant_burmese_Unicode"
            }else if ok_default_language == "en"{
                str = "okdollar_terms_merchant"
            }else {
               str = "okdollar_terms_merchant_burmese"
            }
        }
        
        DispatchQueue.main.async {
            do {
                guard let filePath = Bundle.main.path(forResource: str, ofType: "html")
                    else {
                        return
                }
                let contents =  try String(contentsOfFile: filePath, encoding: .utf8)
                let baseUrl = URL(fileURLWithPath: filePath)
                self.webView.loadHTMLString(contents as String, baseURL: baseUrl)
            }
            catch {
                print ("File HTML error")
            }
        }
        
    }
    func updateLaguageCell() {
        btnRegister.setTitle("ACCEPT & REGISTER".localized, for: .normal)
    }

    @IBAction func onClickRegisterAction(_ sender: Any) {
        if let del = delegate {
            del.reRegister()
        }
    }
}
