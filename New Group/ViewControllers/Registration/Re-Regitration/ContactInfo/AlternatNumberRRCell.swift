//
//  AlternatNumberRRCell.swift
//  OK
//
//  Created by Imac on 6/3/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit


protocol AlternatNumberRRCellDelegate {
    func closeAlternateNumber()
    func showContactView()
    func showAlternateCountryList()
    func editingField(text : String)
    func endField(text : String)
    func showAlert(text : String)
}

class AlternatNumberRRCell: UITableViewCell,UITextFieldDelegate {
    var delegate: AlternatNumberRRCellDelegate?
    @IBOutlet weak var imgCountryFlag: UIImageView!
    @IBOutlet weak var tfAlternateNumber: RestrictedCursorMovement!{
        didSet{
            tfAlternateNumber.font = UIFont(name: appFont, size: 18.0)
        }
    }

    @IBOutlet weak var btnClose: UIButton!{
        didSet{
            btnClose.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnContact: UIButton!{
        didSet{
            btnContact.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var lblCountyCode: UILabel!{
        didSet{
            lblCountyCode.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    let validObj = PayToValidations()
    override func awakeFromNib() {
        super.awakeFromNib()
        imgCountryFlag.isUserInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        imgCountryFlag.addGestureRecognizer(tapRecognizer)
        tfAlternateNumber.delegate = self
    }
    func updateLaguageCell(flag: String, countryCode: String, number: String) {
        tfAlternateNumber.placeholder = "Alternative Number".localized
        imgCountryFlag.image = UIImage(named: flag)
        lblCountyCode.text = "(" + countryCode + ")"
        if flag == "myanmar" && number == ""{
           tfAlternateNumber.text = "09"
            btnClose.isHidden = true
        }else if number == "" {
            tfAlternateNumber.text = ""
            btnClose.isHidden = true
        }else {
            tfAlternateNumber.text = number
            btnClose.isHidden = false
        }
        
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        if let del = delegate {
            del.endField(text: textField.text ?? "")
        }
    }
    
    @IBAction func editChanged(_ text: UITextField) {
        if let del = delegate {
            del.editingField(text: text.text ?? "")
        }
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.keyboardType = .numberPad
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if range.location == 0 && string == " " {
            return false
        }
        let chars = textField.text! + string;
        let mbLength = validObj.getNumberRangeValidation(chars)
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "1234567890").inverted).joined(separator: "")) { return false }
        if text.count > 3 {
            btnClose.isHidden = false
        }else {
            btnClose.isHidden = true
        }
        
        if textField.selectedTextRange?.start != textField.selectedTextRange?.end {
            let endOfDocument = textField.endOfDocument
            textField.selectedTextRange = textField.textRange(from: endOfDocument, to: endOfDocument)
            return false
        }
        
        if imgCountryFlag.image == UIImage(named: "myanmar") {
            if range.location == 0 && range.length > 1 {
                textField.text = "09"
                btnClose.isHidden = true
                return false
            }
            if range.location == 1 {
                return false
            }
            if mbLength.isRejected {
                if let del = delegate {
                    del.showAlert(text: "Rejected Number".localized)
                }
                textField.text = "09"
                btnClose.isHidden = true
                return false
            }
            if chars.count > mbLength.max {
                textField.resignFirstResponder()
                return false
            }
        }else {
            if text.count > 0 {
                btnClose.isHidden = false
            }else {
                btnClose.isHidden = true
            }
            if text.count > 13 {
                return false
            }
        }
        return true
    }
    
    
    
    
    
    
    @objc func imageTapped(sender: UIImageView) {
        if let del = delegate {
            del.showAlternateCountryList()
        }
    }

    @IBAction func onClickCloseAction(_ sender: Any) {
        if let del = delegate {
            del.closeAlternateNumber()
        }
    }
    @IBAction func onClickContactAction(_ sender: Any) {
        if let del = delegate {
            del.showContactView()
        }
    }
}
