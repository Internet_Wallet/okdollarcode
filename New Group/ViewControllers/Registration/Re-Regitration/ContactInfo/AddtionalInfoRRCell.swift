//
//  AddtionalInfoRRCell.swift
//  OK
//
//  Created by Imac on 6/4/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
protocol AddtionalInfoRRCellDelegate {
    func facebook(isFb : Bool)
}

class AddtionalInfoRRCell: UITableViewCell {
    @IBOutlet weak var viewBG: UIView!
    var delegate: AddtionalInfoRRCellDelegate?
    @IBOutlet weak var lblAdditional: UILabel!{
        didSet{
            lblAdditional.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgSelected: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewBG.layer.cornerRadius = 25
        viewBG.layer.masksToBounds = true
    }
    
    func updateLaguageCell(facebook : Bool) {
        if facebook {
            imgSelected.image = UIImage(named: "radio_select")
        }else {
            imgSelected.image = UIImage(named: "radio")
        }
        lblAdditional.text = "Additional Information".localized
    }
    @IBAction func onClickAgentAction(_ sender: Any) {
        if imgSelected.image == UIImage(named: "radio") {
            imgSelected.image = UIImage(named: "radio_select")
            if let del = delegate {
                del.facebook(isFb: true)
            }
        }else {
            imgSelected.image = UIImage(named: "radio")
            if let del = delegate {
                   del.facebook(isFb: false)
            }
        }
    }
}
