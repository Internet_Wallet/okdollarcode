//
//  ReRegistrationModel.swift
//  OK
//
//  Created by Imac on 5/30/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class ReRegistrationModel: NSObject {
    static let shared = ReRegistrationModel()
    
    var player: AVAudioPlayer?
    //20
    var CountryCode      : String = "" // registration country code
    var MobileNumber    : String = ""// Entered Phone NO
    
    var AccountType     : Int?//Merchent 0 - personal 1
    var Gender          : Bool? // Male- "true" , female - "false"
    var Name             : String = ""//"Mr,Dhruv"
    var DateOfBirth      : String = ""//"15-May-1989"
    var EmailId          : String = "" // ""
    var ProfilePicAwsUrl : String = "" //base 64 string
    //var ProfilePic       : String = ""
    var Father           : String = "" //
    
    var IDType           : String = "" // NRC - 01 , passport- 04
    var NRC              : String = "" // nrc no / passport no
    var IdExpDate        : String = "" // ""
    var IdPhotoAwsUrl    : String = "" //base 64 string
    //var IdPhoto          : String = ""
    var IdPhoto1AwsUrl   : String = "" //base 64 string
   // var IdPhoto1         : String = ""
    var CountryOfCitizen : String = "" // ""
    
    var BusinessName           : String = "" // ""
    var BusinessType           : String = "" // Sub-Category
    var BusinessCategory       : String = "" //Category
    var BusinessIdPhoto1AwsUrl : String = "" //base 64 string
    //var BusinessIdPhoto1       : String = "" //base 64 string
    var BusinessIdPhoto2AwsUrl : String = "" //base 64 string
    //var BusinessIdPhoto2       : String = "" //base 64 string
    //19
    var AddressType  : String = "" // Home , Business, Other
    var Address1 : String = "" // CityName
    var Address2 : String = "" // street name  - ""
    var State : String = "" // State Code
    var Township : String = "" //TownshipCode
    var VillageName : String = ""
    var HouseBlockNo : String = "" //Houuse No ""
    var HouseName : String = "" // Housing/ Zone Name
    var FloorNumber : String = "" // floor No
    var RoomNumber : String = "" // Room No
    
    var PasswordType : String = "" // 0 , 1
    var Password : String = ""//ask
    var ConfirmPassword : String = ""//ask
    var SecurityQuestionAnswer : String = ""
    var SecurityQuestionCode : String = "" // code
    
    var signatureAwsUrl : String = "" //base 64 string
    var signature       : String = ""
    
    var FBEmailId : String = "" //Stirng
    var CodeAlternate  : String = "" // country code "+95"
    var Phone : String = ""// alternate Number
    var PhoneNumber : String = "" // This param is to be used for update profile as Phone in Registration
    var CodeRecommended : String = "" //country code "+95"
    var Recommended : String = "" // Mobil No
    
    
    var AlternativeConfirmPhone : String = ""// alternate Number
    var ReferralConfirmPhone : String = ""
    //32
    var AgentAuthCode : String = ""// Agent Code
    
    var ShopPicOneAwsUrl : String = ""
    var ShopPicTwoAwsUrl : String = ""
    var IsValidateAgentOtp : String = ""
    var FontType : String = ""
    
    var AppID : String = "" //vinu AppID = "com.cgm.OKDollar";
    var Country : String = "" //"Myanmar"
    var Car : String = ""//null
    var CarType : Int?//null -1
    var CellTowerID : String = ""//null
    var CloseTime : String = ""//null
    // var CountryCode : String = "" // registration country code
    var DeviceId : String = "" // UDID
    var DeviceInfo : Dictionary<String,Any>?//ask
    var Encrypted : String = ""//ask
    var GcmID  : String = ""//ask
    var IMEI : String = "" //null
    var Kickback : Bool? //ask
    var Language : String = "" // en , my
    var Latitude : String = "" //""
    var Longitude : String = "" //""
    var Loyalty : Bool? //ask
    var MSID : String = msid //from stirng file have to fetch telco code
    var Msisdn = [String]() //ask
    var OSType = 1 //0- android, 1- ios
    var OpenTime : String = "" //null
    var OsVersion : String = "" //current os version
    var ParentAccount : String = ""//ask
    var PaymentGateway : Bool? // false
    var PhoneModel : String = "" // current phone model
    var Phonebrand = "" //"iPhone"
    var RegistrationStatus = 1
    var IosBuildNumber = 57
    var SecureToken : String = "" //ask
    var SimID : String = "" //UDID
    var lType : String = "" // MER= 0 , SUBSBR=1
    //total 71
    
    var BankDetails = [String]()
    var ClosedDays = [String]()
    //73
    
    var LoginAppKey : String = ""
    var LoginIdentityType : String = ""
    var countryISO : String = ""
    var operatorName : String = ""
    //OCR Front
    var ocr_F_birth : String = ""
    var ocr_F_bloodGroup : String = ""
    var ocr_F_bloodlinesReligion : String = ""
    var ocr_F_className : String = ""
    var ocr_F_fatherName : String = ""
    var ocr_F_height : String = ""
    var ocr_F_issueDate : String = ""
    var ocr_F_name : String = ""
    var ocr_F_nrcid : String = ""
    var ocr_F_side : String = ""
    //OCR BACK
    var ocr_B_address : String = ""
    var ocr_B_cardID : String = ""
    var ocr_B_className : String = ""
    var ocr_B_nrcIdback : String = ""
    var ocr_B_profession : String = ""
    var ocr_B_side : String = ""
    var screenFrom: String = ""
    
    var countryImage = ""
    
    func wrapDataModel() -> Data? {
        let accountType = (AccountType! as NSNumber).stringValue
        let carTypeStr = (CarType! as NSNumber).stringValue
        let gender = Gender! ? "1" : "0"
       // let buildNumber = buildNumber
        let kickback = "1"
        let loyalty = "1"
        let osType = "1"
        let paymentGateway = "0"
        let regStatus = "1"
        if lType == "AGENT" {
            IsValidateAgentOtp = "0"
        }else {
            IsValidateAgentOtp = ""
        }
        var languageType = ""
        if Language == "my" {
            languageType = "0"
        }else if Language == "uni" {
            languageType = "1"
            Language = "my"
        }
        
       let deviceInfo = ReRegDeviceInfo.init(bssid: "BluetoothAddress", bluetoothAddress: "02:00:00:00:00:00", bluetoothName: "", cellid: "", connectedNetworkType: "WIFI", deviceSoftwareVersion: UIDevice.current.systemVersion, hiddenSSID: "0", ipAddress: "", linkSpeed: "1", macAddress: "", msid1: "", msid2: "", networkCountryISO: countryISO, networkID: MSID, networkOperator: MSID, networkOperatorName: operatorName, networkSignal: "1", networkType: "4G", phoneType: "GSM", simCountryISO: countryISO, simOperator: MSID, simOperatorName: operatorName, ssid: operatorName, simid1: "", simid2: "", voiceMailNo: "", isNetworkRoaming: "0")
        
        let ocrFront =  ReRegiOcrNrcFrontInfo.init(birth: ocr_F_birth, bloodGroup: ocr_F_bloodGroup, bloodlinesReligion: ocr_F_bloodlinesReligion, className: ocr_F_className, fatherName: ocr_F_fatherName, height: ocr_F_height, issueDate: ocr_F_issueDate, name: ocr_F_name, nrcid: ocr_F_nrcid, side: ocr_F_side)
        
        let ocrBack  = ReRegiOcrNrcBackInfo.init(address: ocr_B_address, cardID: ocr_B_cardID, className: ocr_B_className, nrcIdback: ocr_B_nrcIdback, profession: ocr_B_profession, side: ocr_B_side)
        
        var wifiName = ""
        var ssid = ""
        var mac = ""
        if UitilityClass.isConnectedToWifi(){
            
            let wifiInfo = UitilityClass.getNetworkInfos()[0].components(separatedBy: ",")
            wifiName = wifiInfo[0]
            ssid = wifiInfo[0]
            mac = wifiInfo[1]
        }
        
        let networkInfo = TransCellTower.init(connectedwifiName: wifiName, lac: "", mac: mac, mcc: mccStatus.mcc, mnc: mccStatus.mnc, networkType: UitilityClass.getNetworkType(),networkSpeed: UitilityClass.getSignalStrength(), signalStrength: "", ssid: ssid)
        
        let model = ReRegistrationModelDetail.init(accountType: accountType, address1: Address1, address2: Address2, addressType: AddressType, agentAuthCode: AgentAuthCode, appID: buildNumber, appVersion: buildVersion, bankDetails: BankDetails, businessCategory: BusinessCategory, businessIDPhoto1: "", businessIDPhoto1AwsURL: BusinessIdPhoto1AwsUrl, businessIDPhoto2: "", businessIDPhoto2AwsURL: BusinessIdPhoto2AwsUrl, businessName: BusinessName, businessOutlet: "", businessType: BusinessType, car: Car, carType: carTypeStr, cellTowerID: CellTowerID, closeTime: CloseTime, closedDays: ClosedDays, codeAlternate: CodeAlternate, codeRecommended: CodeRecommended, country: Country, countryCode: CountryCode, countryOfCitizen: CountryOfCitizen, createdDate: "", dateOfBirth: DateOfBirth, deviceID: DeviceId, deviceInfo: deviceInfo, emailID: EmailId, encrypted: Encrypted, fbEmailID: FBEmailId, father: Father, floorNumber: FloorNumber, gcmID: GcmID, gender: gender, houseBlockNo: HouseBlockNo, houseName: HouseName, idType: IDType, imei: IMEI, idExpDate: IdExpDate, idPhoto: "", idPhoto1: "", idPhoto1AwsURL: IdPhoto1AwsUrl, idPhotoAwsURL: IdPhotoAwsUrl, iosBuildNumber: buildNumber, isValidateAgentOtp: "", kickback: kickback, language: Language, latitude: Latitude, loginAppKey: LoginAppKey, loginIdentityType: LoginIdentityType, logoImages: "", longitude: Longitude, loyalty: loyalty, msid: MSID, mobileNumber: MobileNumber, msisdn: Msisdn, nrc: NRC, name: Name, osType: osType, ocrEmbededInfo: "", ocrNrcBackInfo: ocrBack, ocrNrcFrontInfo: ocrFront, openTime: OpenTime, osVersion: OsVersion, ownershipType: "", parentAccount: ParentAccount, password: Password, passwordType: PasswordType, paymentGateway: paymentGateway, phone: Phone, phoneModel: PhoneModel, phonebrand: Phonebrand, profilePic: "", profilePicAwsURL: ProfilePicAwsUrl, recommended: Recommended, registrationStatus: regStatus, roomNumber: RoomNumber, secureToken: SecureToken, securityQuestionAnswer: SecurityQuestionAnswer, securityQuestionCode: SecurityQuestionCode, shopPicOneAwsURL: ShopPicOneAwsUrl, shopPicTwoAwsURL: ShopPicTwoAwsUrl, simID: SimID, state: State, transCellTower: networkInfo, township: Township, villageName: VillageName, lType: lType, signature: "", signatureAwsURL: signatureAwsUrl,fontType: languageType)
        
        do {
            let data  =  try JSONEncoder().encode(model)
            return data
        } catch {
            println_debug(error)
        }
        
        return Data.init()
        
        
    }
    
    func clearValues () {
        //AccountType         =        0
        Gender              =        true
        Name                =        ""
        DateOfBirth         =        ""
        EmailId             =        ""
        ProfilePicAwsUrl    =        ""
        Father              =        ""
        
        IDType              =        ""
        NRC                 =        ""
        IdExpDate           =        ""
        IdPhotoAwsUrl       =        ""
        IdPhoto1AwsUrl      =        ""
        CountryOfCitizen    =        ""
        
        BusinessName        =        ""
        BusinessType        =        ""
        BusinessCategory    =        ""
        BusinessIdPhoto1AwsUrl    =        ""
        BusinessIdPhoto2AwsUrl    =        ""
        
        AddressType         =        ""
        Address1            =        ""
        Address2            =        ""
        State               =        ""
        Township            =        ""
        VillageName         =        ""
        HouseBlockNo        =        ""
        HouseName           =        ""
        FloorNumber         =        ""
        RoomNumber          =        ""
        
        if UserDefaults.standard.bool(forKey: "RE_REGITRATION") {
            
        }else {
            PasswordType        =        ""
            Password            =        ""
        }

        SecurityQuestionAnswer =     ""
        SecurityQuestionCode   =     ""
        
        signatureAwsUrl     =        ""
        signature           =        ""
        
        FBEmailId           =        ""
        CodeAlternate       =        ""
        Phone               =        ""
        PhoneNumber         =        ""
        CodeRecommended     =        ""
        Recommended         =        ""
    }
    
    func setModelValues() {
        
        
        ReRegistrationModel.shared.AppID = "com.cgm.OKDollar"
        ReRegistrationModel.shared.Country = "Myanmar"
        ReRegistrationModel.shared.Car = ""
        ReRegistrationModel.shared.CarType = -1
        ReRegistrationModel.shared.CellTowerID = ""
        ReRegistrationModel.shared.CloseTime = "00:00"
        
        ReRegistrationModel.shared.DeviceId = uuid
        ReRegistrationModel.shared.Encrypted = ""
        ReRegistrationModel.shared.GcmID = ""
        ReRegistrationModel.shared.IMEI = ""
        ReRegistrationModel.shared.Kickback = true
        ReRegistrationModel.shared.Loyalty = true
        ReRegistrationModel.shared.MSID = MSID
        
        ReRegistrationModel.shared.Msisdn = []
        ReRegistrationModel.shared.ClosedDays = []
        ReRegistrationModel.shared.BankDetails = []
        ReRegistrationModel.shared.OSType = 1
        ReRegistrationModel.shared.OpenTime = "00:00"
        let floatVersion = (UIDevice.current.systemVersion as NSString).floatValue
        let stringFloat =  String(describing: floatVersion)
        ReRegistrationModel.shared.OsVersion = stringFloat
        ReRegistrationModel.shared.ParentAccount = ""
        ReRegistrationModel.shared.PaymentGateway = false
        //        ReRegistrationModel.shared.Phone = ""
        let deviceModel = String(UIDevice().type.rawValue)
        ReRegistrationModel.shared.PhoneModel = deviceModel
        ReRegistrationModel.shared.Phonebrand = "iPhone"
        ReRegistrationModel.shared.RegistrationStatus = 1
        if UserDefaults.standard.bool(forKey: "RE_REGITRATION") {
        }else {
  
          ReRegistrationModel.shared.SecureToken = ""
        }
        ReRegistrationModel.shared.IosBuildNumber = 57
        ReRegistrationModel.shared.SimID = uuid
        
        //ReRegistrationModel.shared.AgentAuthCode = ""
        
        ReRegistrationModel.shared.LoginAppKey = ""
        ReRegistrationModel.shared.LoginIdentityType = "1"
        ReRegistrationModel.shared.DeviceInfo = ["BSSID":"","BluetoothAddress":"02:00:00:00:00:00","BluetoothName":"",
                                               "ConnectedNetworkType":"WIFI","DeviceSoftwareVersion": UIDevice.current.systemVersion,"HiddenSSID":false,
                                               "IPAddress":"","LinkSpeed":1,"MACAddress":"",
                                               "NetworkCountryIso": countryISO,"NetworkID":MSID,"NetworkOperator":MSID,
                                               "NetworkOperatorName":operatorName,"NetworkSignal":1,"NetworkType":"4G",
                                               "PhoneType":"GSM","SIMCountryIso": countryISO,"SIMOperator":MSID,
                                               "SIMOperatorName":operatorName,"SSID":operatorName,"isNetworkRoaming":false] //as? Dictionary<String, Any>
    }
    
    
    
    func playSound() {
        guard let url = Bundle.main.url(forResource: "wellcome", withExtension: "mp3") else { return }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category(rawValue: convertFromAVAudioSessionCategory12(AVAudioSession.Category.playback)), mode: AVAudioSession.Mode.default)
            try AVAudioSession.sharedInstance().setActive(true)
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            guard let player = player else { return }
            player.play()
        } catch let error {
            println_debug(error.localizedDescription)
        }
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory12(_ input: AVAudioSession.Category) -> String {
    return input.rawValue
}
