//
//  ReRegistrationStructure.swift
//  OK
//
//  Created by Imac on 6/4/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

struct ReRegistrationModelDetail: Codable {
    let accountType: String
    let address1, address2, addressType, agentAuthCode: String
    let appID,appVersion: String
    let bankDetails: [String]?
    let businessCategory, businessIDPhoto1: String
    let businessIDPhoto1AwsURL: String
    let businessIDPhoto2: String
    let businessIDPhoto2AwsURL: String
    let businessName, businessOutlet, businessType, car, carType: String
    let cellTowerID, closeTime: String
    let closedDays: [String]?
    let codeAlternate, codeRecommended, country, countryCode: String
    let countryOfCitizen, createdDate, dateOfBirth, deviceID: String
    let deviceInfo: ReRegDeviceInfo?
    let emailID, encrypted, fbEmailID, father: String
    let floorNumber, gcmID, gender: String
    let houseBlockNo, houseName, idType, imei: String
    let idExpDate, idPhoto, idPhoto1: String
    let idPhoto1AwsURL, idPhotoAwsURL, iosBuildNumber: String
    let isValidateAgentOtp,kickback : String
    let language, latitude, loginAppKey, loginIdentityType: String
    let logoImages, longitude, loyalty: String
    let msid, mobileNumber: String
    let msisdn: [String]?
    let nrc, name, osType: String
    let ocrEmbededInfo: String
    let ocrNrcBackInfo: ReRegiOcrNrcBackInfo?
    let ocrNrcFrontInfo: ReRegiOcrNrcFrontInfo?
    let openTime, osVersion, ownershipType, parentAccount: String
    let password, passwordType,paymentGateway: String
    let phone, phoneModel, phonebrand, profilePic: String
    let profilePicAwsURL: String
    let recommended, registrationStatus : String
    let roomNumber, secureToken, securityQuestionAnswer, securityQuestionCode: String
    let shopPicOneAwsURL, shopPicTwoAwsURL, simID, state: String
    let transCellTower : TransCellTower?
    let township, villageName, lType, signature: String
    let signatureAwsURL: String
    let fontType: String
    
    enum CodingKeys: String, CodingKey {
        case accountType = "AccountType"
        case address1 = "Address1"
        case address2 = "Address2"
        case addressType = "AddressType"
        case agentAuthCode = "AgentAuthCode"
        case appID = "AppID"
        case appVersion = "AppVersion"
        case bankDetails = "BankDetails"
        case businessCategory = "BusinessCategory"
        case businessIDPhoto1 = "BusinessIdPhoto1"
        case businessIDPhoto1AwsURL = "BusinessIdPhoto1AwsUrl"
        case businessIDPhoto2 = "BusinessIdPhoto2"
        case businessIDPhoto2AwsURL = "BusinessIdPhoto2AwsUrl"
        case businessName = "BusinessName"
        case businessOutlet = "BusinessOutlet"
        case businessType = "BusinessType"
        case car = "Car"
        case carType = "CarType"
        case cellTowerID = "CellTowerID"
        case closeTime = "CloseTime"
        case closedDays = "ClosedDays"
        case codeAlternate = "CodeAlternate"
        case codeRecommended = "CodeRecommended"
        case country = "Country"
        case countryCode = "CountryCode"
        case countryOfCitizen = "CountryOfCitizen"
        case createdDate = "CreatedDate"
        case dateOfBirth = "DateOfBirth"
        case deviceID = "DeviceId"
        case deviceInfo = "DeviceInfo"
        case emailID = "EmailId"
        case encrypted = "Encrypted"
        case fbEmailID = "FBEmailId"
        case father = "Father"
        case floorNumber = "FloorNumber"
        case gcmID = "GcmID"
        case gender = "Gender"
        case houseBlockNo = "HouseBlockNo"
        case houseName = "HouseName"
        case idType = "IDType"
        case imei = "IMEI"
        case idExpDate = "IdExpDate"
        case idPhoto = "IdPhoto"
        case idPhoto1 = "IdPhoto1"
        case idPhoto1AwsURL = "IdPhoto1AwsUrl"
        case idPhotoAwsURL = "IdPhotoAwsUrl"
        case iosBuildNumber = "IosBuildNumber"
        case isValidateAgentOtp = "IsValidateAgentOtp"
        case kickback = "Kickback"
        case language = "Language"
        case latitude = "Latitude"
        case loginAppKey = "LoginAppKey"
        case loginIdentityType = "LoginIdentityType"
        case logoImages = "LogoImages"
        case longitude = "Longitude"
        case loyalty = "Loyalty"
        case msid = "MSID"
        case mobileNumber = "MobileNumber"
        case msisdn = "Msisdn"
        case nrc = "NRC"
        case name = "Name"
        case osType = "OSType"
        case ocrEmbededInfo = "OcrEmbededInfo"
        case ocrNrcBackInfo = "OcrNrcBackInfo"
        case ocrNrcFrontInfo = "OcrNrcFrontInfo"
        case openTime = "OpenTime"
        case osVersion = "OsVersion"
        case ownershipType = "OwnershipType"
        case parentAccount = "ParentAccount"
        case password = "Password"
        case passwordType = "PasswordType"
        case paymentGateway = "PaymentGateway"
        case phone = "Phone"
        case phoneModel = "PhoneModel"
        case phonebrand = "Phonebrand"
        case profilePic = "ProfilePic"
        case profilePicAwsURL = "ProfilePicAwsUrl"
        case recommended = "Recommended"
        case registrationStatus = "RegistrationStatus"
        case roomNumber = "RoomNumber"
        case secureToken = "SecureToken"
        case securityQuestionAnswer = "SecurityQuestionAnswer"
        case securityQuestionCode = "SecurityQuestionCode"
        case shopPicOneAwsURL = "ShopPicOneAwsUrl"
        case shopPicTwoAwsURL = "ShopPicTwoAwsUrl"
        case simID = "SimID"
        case state = "State"
        case township = "Township"
        case transCellTower = "TransCellTower"
        case villageName = "VillageName"
        case lType, signature
        case signatureAwsURL = "signatureAwsUrl"
        case fontType = "FontType"
    }
}

struct ReRegDeviceInfo: Codable {
    let bssid, bluetoothAddress, bluetoothName, cellid: String
    let connectedNetworkType, deviceSoftwareVersion, hiddenSSID, ipAddress, linkSpeed: String
    let macAddress, msid1, msid2, networkCountryISO, networkID: String
    let networkOperator, networkOperatorName, networkSignal: String
    let networkType, phoneType, simCountryISO, simOperator: String
    let simOperatorName, ssid, simid1, simid2: String
    let voiceMailNo, isNetworkRoaming: String
    
    enum CodingKeys: String, CodingKey {
        case bssid = "BSSID"
        case bluetoothAddress = "BluetoothAddress"
        case bluetoothName = "BluetoothName"
        case cellid = "Cellid"
        case connectedNetworkType = "ConnectedNetworkType"
        case deviceSoftwareVersion = "DeviceSoftwareVersion"
        case hiddenSSID = "HiddenSSID"
        case ipAddress = "IPAddress"
        case linkSpeed = "LinkSpeed"
        case macAddress = "MACAddress"
        case msid1 = "Msid1"
        case msid2 = "Msid2"
        case networkCountryISO = "NetworkCountryIso"
        case networkID = "NetworkID"
        case networkOperator = "NetworkOperator"
        case networkOperatorName = "NetworkOperatorName"
        case networkSignal = "NetworkSignal"
        case networkType = "NetworkType"
        case phoneType = "PhoneType"
        case simCountryISO = "SIMCountryIso"
        case simOperator = "SIMOperator"
        case simOperatorName = "SIMOperatorName"
        case ssid = "SSID"
        case simid1 = "Simid1"
        case simid2 = "Simid2"
        case voiceMailNo = "VoiceMailNo"
        case isNetworkRoaming
    }
}

struct ReRegiOcrNrcBackInfo: Codable {
    let address, cardID, className, nrcIdback: String
    let profession, side: String
    
    enum CodingKeys: String, CodingKey {
        case address = "Address"
        case cardID = "CardId"
        case className = "ClassName"
        case nrcIdback = "NrcIdback"
        case profession = "Profession"
        case side = "Side"
    }
}

struct ReRegiOcrNrcFrontInfo: Codable {
    let birth, bloodGroup, bloodlinesReligion, className: String
    let fatherName, height, issueDate, name: String
    let nrcid, side: String
    
    enum CodingKeys: String, CodingKey {
        case birth = "Birth"
        case bloodGroup = "BloodGroup"
        case bloodlinesReligion = "BloodlinesReligion"
        case className = "ClassName"
        case fatherName = "FatherName"
        case height = "Height"
        case issueDate = "IssueDate"
        case name = "Name"
        case nrcid = "Nrcid"
        case side = "Side"
    }
}


struct TransCellTower: Codable {
    let connectedwifiName, lac, mac, mcc: String
    let mnc, networkType, networkSpeed, signalStrength, ssid: String
    
    enum CodingKeys: String, CodingKey {
        case connectedwifiName = "ConnectedwifiName"
        case lac = "Lac"
        case mac = "Mac"
        case mcc = "Mcc"
        case mnc = "Mnc"
        case networkType = "NetworkType"
        case ssid = "Ssid"
        case signalStrength = "SignalStrength"
        case networkSpeed = "NetworkSpeed"
    }
}
//NetworkSpeed
