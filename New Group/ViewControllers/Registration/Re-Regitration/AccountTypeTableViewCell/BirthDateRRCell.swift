//
//  BirthDateRRCell.swift
//  OK
//
//  Created by Imac on 5/30/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
protocol BirthDateRRCellDelegate {
    func showTakeUserImage()
}

class BirthDateRRCell: UITableViewCell,UITextFieldDelegate {
    var delegate: BirthDateRRCellDelegate?
  @IBOutlet weak var tfDOB: UITextField!
    let dateFormatter = DateFormatter()
    var strDOB = ""
    var isProfileCameraHidden = true
    var btnCancel = UIButton()
    var btnDone = UIButton()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.tfDOB.delegate = self
       
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        
        let screenSize: CGRect = UIScreen.main.bounds
        let viewDOBKeyboard = UIView(frame: CGRect(x: 0, y: 0, width:screenSize.width , height: 40))
        viewDOBKeyboard.backgroundColor = UIColor(red: 244/255, green: 199/255, blue: 22/255, alpha: 1)
        
        btnCancel  = UIButton(frame: CGRect(x: 10, y: 0, width:100, height: 40))
        btnCancel.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
        btnCancel.setTitle("Cancel".localized, for: UIControl.State.normal)
        btnCancel.addTarget(self, action:#selector(btnCancelDOBAction), for: UIControl.Event.touchUpInside)
        viewDOBKeyboard.addSubview(btnCancel)
        
        btnDone  = UIButton(frame: CGRect(x: screenSize.width - 110, y: 0, width:100, height: 40))
        btnDone.setTitle("Done".localized, for: UIControl.State.normal)
        btnDone.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
        btnDone.addTarget(self, action:#selector(btnDoneDOBAction), for: UIControl.Event.touchUpInside)
        viewDOBKeyboard.addSubview(btnDone)
        tfDOB.inputAccessoryView = viewDOBKeyboard
    }
    
    func updateLaguageCell() {
        
        var font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
            font = UIFont.systemFont(ofSize: 18)
        }
        tfDOB.font = font
        
        if ReRegistrationModel.shared.Language.lowercased() == "my"{
            tfDOB.font = UIFont(name: appFont, size: 18)
        }else if ReRegistrationModel.shared.Language.lowercased() == "en"{
            tfDOB.font = UIFont.systemFont(ofSize: 18)
        }else {
            
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfDOB.attributedPlaceholder = NSAttributedString(string: "Select Date of Birth", attributes:attributes as [NSAttributedString.Key : Any])
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfDOB.attributedPlaceholder = NSAttributedString(string: "Select Date of Birth", attributes:attributes)
                tfDOB.font = UIFont.systemFont(ofSize: 18)
            }
        }
        
        self.tfDOB.placeholder = "Select Date of Birth".localized
        btnCancel.setTitle("Cancel".localized, for: UIControl.State.normal)
        btnDone.setTitle("Done".localized, for: UIControl.State.normal)
    }
    func wrapData(){
        self.tfDOB.text = ReRegistrationModel.shared.DateOfBirth
    }

    @objc func datePickerChanged(sender: UIDatePicker) {
        sender.calendar = Calendar(identifier: .gregorian)
        strDOB = dateFormatter.string(from: sender.date)
    }
    
    @objc func btnCancelDOBAction () {
        self.tfDOB.becomeFirstResponder()
        tfDOB.resignFirstResponder()
    }
    
    @objc func btnDoneDOBAction () {
        self.tfDOB.becomeFirstResponder()
        self.tfDOB.resignFirstResponder()
        tfDOB.text = strDOB
        if tfDOB.text!.count > 0 {
            tfDOB.text = strDOB
            ReRegistrationModel.shared.DateOfBirth = tfDOB.text!
            if isProfileCameraHidden {
                isProfileCameraHidden = false
                if let del = delegate {
                    del.showTakeUserImage()
                }
            }
        }
    }

    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == self.tfDOB {
            let date = Calendar.current.date(byAdding: .year, value: -12, to: Date())
            let minDate = Calendar.current.date(byAdding: .year, value: -117, to: Date())
            
            let datePicker = UIDatePicker()
            datePicker.datePickerMode = .date
            datePicker.backgroundColor = UIColor.white
            datePicker.calendar = Calendar(identifier: .gregorian)
            
            datePicker.maximumDate = date!
            datePicker.minimumDate = minDate
            tfDOB.inputView = datePicker
            if #available(iOS 13.4, *) {
                datePicker.preferredDatePickerStyle = .wheels
            } else {
                // Fallback on earlier versions
            }
            
            if tfDOB.text?.count == 0 {
                datePicker.date = date!
            }else{
                datePicker.date = dateFormatter.date(from: tfDOB.text!)!
            }
            strDOB = dateFormatter.string(from: datePicker.date)
            datePicker.addTarget(self, action: #selector(datePickerChanged(sender:)), for: .valueChanged)
        }
        return true
    }

}
