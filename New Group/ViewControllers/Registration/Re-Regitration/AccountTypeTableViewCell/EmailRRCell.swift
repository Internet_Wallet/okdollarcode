//
//  EmailRRCell.swift
//  OK
//
//  Created by Imac on 5/30/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol EmailRRCellDelegate {
    func showIDType()
}

class EmailRRCell: UITableViewCell,UITextFieldDelegate {
    var delegate: EmailRRCellDelegate!
    @IBOutlet weak var tfEmailID: UITextField!{
        didSet{
            tfEmailID.font = UIFont(name: appFont, size: 17.0)
        }
    }
  @IBOutlet weak var btnMailIDClear: UIButton!
    var EMAILCHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890.@_-"
    var isNextViewHidden = true
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.tfEmailID.delegate = self
    }
    func updateLaguageCell() {
        if ReRegistrationModel.shared.Language.lowercased() == "my"{
            tfEmailID.font = UIFont(name: appFont, size: 18)
        }else if ReRegistrationModel.shared.Language.lowercased() == "en"{
            tfEmailID.font = UIFont.systemFont(ofSize: 18)
        }else {
            let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                              .font : UIFont.systemFont(ofSize: 18)]
            tfEmailID.attributedPlaceholder = NSAttributedString(string: "Enter E-Mail ID", attributes:attributes)
            tfEmailID.font = UIFont.systemFont(ofSize: 18)
        }
        
        self.tfEmailID.placeholder = "Enter E-Mail ID".localized
        
    }
    func wrapData(){
        self.tfEmailID.text = ReRegistrationModel.shared.EmailId
    }
    
    @IBAction func mailIDClearAction(_ sender: UIButton) {
        tfEmailID.text = ""
        btnMailIDClear.isHidden = true
        ReRegistrationModel.shared.EmailId = ""
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        removeSpaceAtLast(txtField: tfEmailID)
        ReRegistrationModel.shared.EmailId = tfEmailID.text!
            if let del = delegate {
                del.showIDType()
            }
        return true
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
 
                tfEmailID.keyboardType = .emailAddress
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if range.location == 0 && string == " " {
            return false
        }
        ReRegistrationModel.shared.EmailId = tfEmailID.text!
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: EMAILCHARSET).inverted).joined(separator: "")) { return false }
            if text.count > 2 {
                btnMailIDClear.isHidden = false
            }else {
                btnMailIDClear.isHidden = true
            }
            return restrictMultipleSpaces(str: string, textField: tfEmailID)
    }
    override var textInputMode: UITextInputMode? {
        let language = "en"
        if language.isEmpty {
            return super.textInputMode
        } else {
            for tim in UITextInputMode.activeInputModes {
                if tim.primaryLanguage!.contains(language) {
                    return tim
                }
            }
            return super.textInputMode
        }
    }
    
    private func removeSpaceAtLast(txtField : UITextField){
        if txtField.text?.last == " " {
            txtField.text?.removeLast()
        }
    }
}
