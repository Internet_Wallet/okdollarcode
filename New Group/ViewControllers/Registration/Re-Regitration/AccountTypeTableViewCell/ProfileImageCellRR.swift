
//
//  ProflieImageCellRR.swift
//  OK
//
//  Created by Imac on 5/30/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
protocol ProfileImageCellRRDelegate {
    func navigateToTakeImage()
}



class ProfileImageCellRR: UITableViewCell {
    
    var delegate: ProfileImageCellRRDelegate?
    @IBOutlet weak var capturedImage: UIImageView!
    @IBOutlet weak var imgCamera: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        capturedImage.layer.cornerRadius = 70
        capturedImage.layer.masksToBounds = true
    }
    
    
    @IBAction func onClickCameraAction(_ sender: Any) {
        if let del = delegate {
            del.navigateToTakeImage()
        }
    }
}
