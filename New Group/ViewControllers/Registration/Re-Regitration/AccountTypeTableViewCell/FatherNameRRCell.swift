//
//  FatherNameRRCell.swift
//  OK
//
//  Created by Imac on 5/30/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
protocol FatherNameRRCellDelegate {
   func showEmailID()
}

class FatherNameRRCell: UITableViewCell,UITextFieldDelegate {
    var delegate : FatherNameRRCellDelegate?
    @IBOutlet weak var viewBFatherInitial: UIView!
    @IBOutlet weak var btnBFatherFirst: UIButton!{
        didSet{
            btnBFatherFirst.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnBFatherSecond: UIButton!{
        didSet{
            btnBFatherSecond.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var viewFatherInitial: UIView!
    @IBOutlet weak var btnFU: UIButton!{
        didSet{
            btnFU.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnFMr: UIButton!{
        didSet{
            btnFMr.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnFDr: UIButton!{
        didSet{
            btnFDr.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var tfFatherName: UITextField!{
        didSet{
            tfFatherName.font = UIFont(name: appFont, size: 17.0)
        }
    }
    @IBOutlet weak var btnFatherNameReset: UIButton!{
        didSet{
            btnFatherNameReset.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    var preFatherName = ""
    var NAMECHARSET = ""
    var isNextViewHidden = true
    var isFatherNameKeyboardHidden = true
    override func awakeFromNib() {
        super.awakeFromNib()
         self.tfFatherName.delegate = self
         self.viewBFatherInitial.isHidden  = true
        self.btnFatherNameReset.isHidden = true
    }
    
    func updateLaguageCell() {
        
        var font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
            font = UIFont.systemFont(ofSize: 18)
        }
        tfFatherName.font = font

      
        if ReRegistrationModel.shared.Language.lowercased() == "en" {
            NAMECHARSET = NAME_CHAR_SET_En
            tfFatherName.font = UIFont.systemFont(ofSize: 18)
        }else if ReRegistrationModel.shared.Language.lowercased() == "my" {
            NAMECHARSET = NAME_CHAR_SET_My
            tfFatherName.font = UIFont(name: appFont, size: 18)
        }else {
//            NAMECHARSET = NAME_CHAR_SET_Uni
//            let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
//                              .font : UIFont.systemFont(ofSize: 18)]
//            tfFatherName.attributedPlaceholder = NSAttributedString(string: "Enter Father Name", attributes:attributes)
//            tfFatherName.font = UIFont.systemFont(ofSize: 18)
            
            
        
                NAMECHARSET = NAME_CHAR_SET_Uni
                if #available(iOS 13.0, *) {
                    let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                      .font : UIFont(name: appFont, size: 18)]
                    tfFatherName.attributedPlaceholder = NSAttributedString(string: "Enter Father Name", attributes:attributes as [NSAttributedString.Key : Any])
                    //tfYourName.font = UIFont.systemFont(ofSize: 18)
                } else {
                    let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                      .font : UIFont.systemFont(ofSize: 18)]
                    tfFatherName.attributedPlaceholder = NSAttributedString(string: "Enter Father Name", attributes:attributes)
                    tfFatherName.font = UIFont.systemFont(ofSize: 18)
                }
                
            
        }
        self.btnFU.setTitle("U".localized, for: .normal)
        self.btnFMr.setTitle("Mr".localized, for: .normal)
        self.btnFDr.setTitle("Dr".localized, for: .normal)
        self.btnBFatherFirst.setTitle("U".localized, for: .normal)
        self.btnBFatherSecond.setTitle("Dr".localized, for: .normal)
         self.tfFatherName.placeholder = "Enter Father Name".localized
    }
    func wrapData(){
        //self.tfFatherName.text = ReRegistrationModel.shared.Father
    }

    
    @IBAction func btnFatherNameResetAction(_ sender: UIButton) {
        tfFatherName.leftView = nil
        isFatherNameKeyboardHidden  = true
        viewFatherInitial.isHidden = true
        tfFatherName.text = ""
        tfFatherName.resignFirstResponder()
        btnFatherNameReset.isHidden = true
        ReRegistrationModel.shared.Father = ""
    }

    @IBAction func btnFatherInitialAction(_ sender: UIButton) {
        viewFatherInitial.isHidden = true
        viewBFatherInitial.isHidden = true
        isFatherNameKeyboardHidden = false
        btnFatherNameReset.isHidden = false
        setPrefixFatherName(prefixName: sender.title(for: .normal)!, txtField: tfFatherName)
    }
    
    @IBAction func btnBFatherInitialAction(_ sender: UIButton) {
        viewFatherInitial.isHidden = true
        viewBFatherInitial.isHidden = true
        isFatherNameKeyboardHidden = false
        btnFatherNameReset.isHidden = false
        setPrefixFatherName(prefixName: sender.title(for: .normal)!, txtField: tfFatherName)
    }
    
    func setPrefixFatherName(prefixName : String, txtField : UITextField) {
        preFatherName = prefixName
        let myString = prefixName
        let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: "Zawgyi-One", size: 18) ?? UIFont.systemFont(ofSize: 18.0) ])
        let lblName = UILabel()
        lblName.frame = CGRect(x: 0, y: 2, width:stringSize.width + 10 , height: 52)
        lblName.textAlignment = .left
        lblName.font =  UIFont(name: appFont, size: 18)
        lblName.text = myString
        let btnView = UIView()
        btnView.frame = CGRect(x: 0, y: 0, width: lblName.frame.width , height: 52)
        btnView.addSubview(lblName)
        txtField.leftView = btnView
        txtField.leftViewMode = UITextField.ViewMode.always
        txtField.becomeFirstResponder()
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
            if tfFatherName.text!.count > 0 {
                ReRegistrationModel.shared.Father = preFatherName + "," +  tfFatherName.text!
                if isNextViewHidden {
                    isNextViewHidden = false
                    if let del = delegate {
                        del.showEmailID()
                    }
                }
                
            } else {
                ReRegistrationModel.shared.Father = ""
            }
            removeSpaceAtLast(txtField: tfFatherName)
      
        return true
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
            if !isFatherNameKeyboardHidden {
                if ReRegistrationModel.shared.Language.localizedLowercase == "my" || ReRegistrationModel.shared.Language.localizedLowercase == "uni"{
                    viewBFatherInitial.isHidden = true
                }else {
                    viewFatherInitial.isHidden = true
                }
            }else{
                if ReRegistrationModel.shared.Language.localizedLowercase == "my" || ReRegistrationModel.shared.Language.localizedLowercase == "uni"{
                    viewBFatherInitial.isHidden = false
                }else {
                    viewFatherInitial.isHidden = false
                }
                return false
            }
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if range.location == 0 && string == " " {
            return false
        }
        ReRegistrationModel.shared.Father = preFatherName + "," +  tfFatherName.text!
        if textField == self.tfFatherName {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: NAMECHARSET).inverted).joined(separator: "")) { return false }
            if text.count > 40 { return false }
    
            if text.count > 2 {
//                if isNextViewHidden {
//                    isNextViewHidden = false
//                    if let del = delegate {
//                        del.showEmailID()
//                    }
//                }
            }
            if restrictMultipleSpaces(str: string, textField: tfFatherName) {
                return true
            }else {
                if  textField.text?.last == " " {
                    textField.text?.removeLast()
                }
                return false
            }
        }
        return true
    }
    override var textInputMode: UITextInputMode? {
        let language = ReRegistrationModel.shared.Language
        if language.isEmpty {
            return super.textInputMode
        } else {
            for tim in UITextInputMode.activeInputModes {
                if tim.primaryLanguage!.contains(language) {
                    return tim
                }
            }
            return super.textInputMode
        }
    }
    
    private func removeSpaceAtLast(txtField : UITextField){
        if txtField.text?.last == " " {
            txtField.text?.removeLast()
        }
    }
}
