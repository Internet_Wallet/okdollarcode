//
//  NameRRCell.swift
//  OK
//
//  Created by Imac on 5/30/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import Rabbit_Swift

protocol NameRRCellDelegate {
    func showDOBField()
}

class NameRRCell: UITableViewCell,UITextFieldDelegate {
    var delegate: NameRRCellDelegate?
    @IBOutlet weak var imgMaleFemale: UIImageView!
    
    @IBOutlet weak var viewMaleFemale: UIView!
    @IBOutlet weak var viewMaleCategory: UIView!
    @IBOutlet weak var viewFemaleCategory: UIView!
    @IBOutlet weak var viewBMaleFemaleCategory: UIView!
    @IBOutlet weak var tfYourName: UITextField!{
        didSet{
            tfYourName.font = UIFont(name: appFont, size: 17.0)
        }
    }
    @IBOutlet weak var btnUserNameReset: UIButton!{
        didSet{
            btnUserNameReset.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var btnMale: UIButton!{
        didSet{
            btnMale.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnFemale: UIButton!{
        didSet{
            btnFemale.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var btnU: UIButton!{
        didSet{
            btnU.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnMg: UIButton!{
        didSet{
            btnMg.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnMr: UIButton!{
        didSet{
            btnMr.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnDr: UIButton!{
        didSet{
            btnDr.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var btnDaw: UIButton!{
        didSet{
            btnDaw.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnMa: UIButton!{
        didSet{
            btnMa.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnMs: UIButton!{
        didSet{
            btnMs.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnMrs: UIButton!{
        didSet{
            btnMrs.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnMDr: UIButton!{
        didSet{
            btnMDr.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var btnBFirst: UIButton!{
        didSet{
            btnBFirst.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnBSecond: UIButton!{
        didSet{
            btnBSecond.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnBThired: UIButton!{
        didSet{
            btnBThired.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    var preUserName = ""
    var NAMECHARSET = ""
    var isYourNameKeyboardHidden = true
    var isNextViewHidden = true
    override func awakeFromNib() {
        super.awakeFromNib()
  
        btnUserNameReset.isHidden = true
        self.viewBMaleFemaleCategory.isHidden = true
    }

    func updateLaguageCell() {
        var font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
            font = UIFont.systemFont(ofSize: 18)
        }
        tfYourName.font = font

        self.btnMale.setTitle("Male".localized, for: .normal)
        self.btnFemale.setTitle("Female".localized, for: .normal)
        
        self.btnU.setTitle("U".localized, for: .normal)
        self.btnMg.setTitle("Mg".localized, for: .normal)
        self.btnMr.setTitle("Mr".localized, for: .normal)
        self.btnDr.setTitle("Dr".localized, for: .normal)
        
        self.btnDaw.setTitle("Daw".localized, for: .normal)
        self.btnMa.setTitle("Ma".localized, for: .normal)
        self.btnMs.setTitle("Ms".localized, for: .normal)
        self.btnMrs.setTitle("Mrs".localized, for: .normal)
        self.btnMDr.setTitle("Dr".localized, for: .normal)
        self.btnBThired.setTitle("Dr".localized, for: .normal)
       
        self.tfYourName.delegate = self
        if ReRegistrationModel.shared.Language.lowercased() == "en" {
            NAMECHARSET = NAME_CHAR_SET_En
            tfYourName.font = UIFont.systemFont(ofSize: 18)
        }else if ReRegistrationModel.shared.Language.lowercased() == "my" {
            NAMECHARSET = NAME_CHAR_SET_My
            tfYourName.font = UIFont(name: appFont, size: 18)
        }else {
            NAMECHARSET = NAME_CHAR_SET_Uni
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfYourName.attributedPlaceholder = NSAttributedString(string: "Enter Your Name", attributes:attributes as [NSAttributedString.Key : Any])
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfYourName.attributedPlaceholder = NSAttributedString(string: "Enter Your Name", attributes:attributes)
                tfYourName.font = UIFont.systemFont(ofSize: 18)
            }
            
        }
         self.tfYourName.placeholder = "Enter Your Name".localized
    }
    
    func wrapData(){
       // self.tfYourName.text = ReRegistrationModel.shared.Name
    }
    
    @IBAction func onClickUserNameMale(_ sender: UIButton) {
        viewMaleFemale.isHidden = true
        imgMaleFemale.image = UIImage(named: "r_male")
        ReRegistrationModel.shared.Gender = true
        if ReRegistrationModel.shared.Language.lowercased() == "my" || ReRegistrationModel.shared.Language.lowercased() == "uni"{
            btnBFirst.setTitle("U".localized, for: .normal)
            btnBSecond.setTitle("Mg".localized, for: .normal)
            self.viewBMaleFemaleCategory.isHidden = false
        }else {
            self.viewMaleCategory.isHidden = false
        }
    }
    @IBAction func onClickUserNameFemale(_ sender: UIButton) {
        viewMaleFemale.isHidden = true
        imgMaleFemale.image = UIImage(named: "r_female")
        ReRegistrationModel.shared.Gender = false
        
        if ReRegistrationModel.shared.Language.lowercased() == "my" || ReRegistrationModel.shared.Language.lowercased() == "uni"{
            btnBFirst.setTitle("Daw".localized, for: .normal)
            btnBSecond.setTitle("Ma".localized, for: .normal)
            self.viewBMaleFemaleCategory.isHidden = false
        }else {
            viewFemaleCategory.isHidden = false
        }
    }
    @IBAction func onClickUserNameMaleEn(_ sender: UIButton) {
        viewMaleCategory.isHidden = true
        viewMaleFemale.isHidden = true
        btnUserNameReset.isHidden = false
        isYourNameKeyboardHidden = false
        setPrefixName(prefixName: sender.title(for: .normal)!, txtField: tfYourName)
    }
    @IBAction func onClickUserNameFemaleEn(_ sender: UIButton) {
        viewFemaleCategory.isHidden = true
        viewMaleFemale.isHidden = true
        btnUserNameReset.isHidden = false
        isYourNameKeyboardHidden = false
        setPrefixName(prefixName: sender.title(for: .normal)!, txtField: tfYourName)
    }
    
    @IBAction func onClickUserNameMaleFemaleMy(_ sender: UIButton) {
        viewFemaleCategory.isHidden = true
        viewMaleFemale.isHidden = true
        viewBMaleFemaleCategory.isHidden = true
        btnUserNameReset.isHidden = false
        isYourNameKeyboardHidden = false
        setPrefixName(prefixName: sender.title(for: .normal)!, txtField: tfYourName)
    }
    
    func setPrefixName(prefixName : String, txtField : UITextField) {
        preUserName = prefixName
        let myString = prefixName
        let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: "Zawgyi-One", size: 18) ?? UIFont.systemFont(ofSize: 18.0) ])
        let lblName = UILabel()
        lblName.frame = CGRect(x: 0, y: 2, width:stringSize.width + 10 , height: 52)
        lblName.textAlignment = .left
        lblName.font =  UIFont(name: appFont, size: 18)
        lblName.text = myString
        let btnView = UIView()
        btnView.frame = CGRect(x: 0, y: 0, width: lblName.frame.width , height: 52)
        btnView.addSubview(lblName)
        txtField.leftView = btnView
        txtField.leftViewMode = UITextField.ViewMode.always
        txtField.becomeFirstResponder()
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
            if tfYourName.text!.count > 0 {
                ReRegistrationModel.shared.Name = preUserName + "," + tfYourName.text!
            } else {
                ReRegistrationModel.shared.Name = ""
            }
            removeSpaceAtLast(txtField: tfYourName)
        return true
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
            if !isYourNameKeyboardHidden {
                viewMaleFemale.isHidden = true
            }else{
                viewMaleFemale.isHidden = false
                return false
            }
        return true
    }
    public func textFieldDidEndEditing(_ textField: UITextField) {
    
    }
    @IBAction func textFieldEditChanged(_ textField: UITextField) {
        
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if range.location == 0 && string == " " {
            return false
        }
        
        
        ReRegistrationModel.shared.Name = preUserName + "," + tfYourName.text!
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: NAMECHARSET).inverted).joined(separator: "")) { return false }
        if text.count > 40 { return false }
        if text.count > 2 {
            if isNextViewHidden {
                if let del = delegate {
                    del.showDOBField()
                    isNextViewHidden = false
                }
            }
        }
        if restrictMultipleSpaces(str: string, textField: tfYourName) {
            return true
        }else {
            if  textField.text?.last == " " {
                textField.text?.removeLast()
            }
            return false
        }
    }

    
    @IBAction func onClickUserNameClose(_ sender: UIButton) {
        tfYourName.text = ""
        viewMaleFemale.isHidden = true
        viewMaleCategory.isHidden = true
        viewFemaleCategory.isHidden = true
        viewBMaleFemaleCategory.isHidden = true
        isYourNameKeyboardHidden = true
        tfYourName.leftView = nil
        imgMaleFemale.image = UIImage(named : "r_user")
        tfYourName.resignFirstResponder()
        btnUserNameReset.isHidden = true
        ReRegistrationModel.shared.Name = ""
    }

    override var textInputMode: UITextInputMode? {
        let language = ReRegistrationModel.shared.Language.lowercased()
        if language.isEmpty {
            return super.textInputMode
        } else {
            for tim in UITextInputMode.activeInputModes {
                if tim.primaryLanguage!.contains(language) {
                    return tim
                }
            }
            return super.textInputMode
        }
    }
    
    private func removeSpaceAtLast(txtField : UITextField){
        if txtField.text?.last == " " {
            txtField.text?.removeLast()
        }
    }
}
