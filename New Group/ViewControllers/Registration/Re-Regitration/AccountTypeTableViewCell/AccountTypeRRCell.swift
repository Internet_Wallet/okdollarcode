//
//  AccountTypeRRCell.swift
//  OK
//
//  Created by Imac on 5/30/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol AccountTypeRRCellDelegate{
     func showDemo(strURL : String, imgDemo : UIImage, demoTitile:String)
    func showAlertPersonalInfo(message : String)
    func comeBackFromMainScreen()
}

class AccountTypeRRCell: UITableViewCell {
    var delegate: AccountTypeRRCellDelegate?
    @IBOutlet var imgCountryFlag: UIImageView!
    @IBOutlet var lblCountryCode: UILabel!
    @IBOutlet var lblMobilNo: UILabel!
    @IBOutlet var lblOperatorName: UILabel!
    @IBOutlet var lbAccountMobileNo: UILabel!{
        didSet{
            lbAccountMobileNo.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var lbSelectAccountType: UILabel!{
        didSet{
            lbSelectAccountType.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var lblMerchant: UILabel!{
        didSet{
            lblMerchant.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var lblPersonal: UILabel!{
        didSet{
            lblPersonal.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblAgent: UILabel!{
        didSet{
            lblAgent.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var imgRadioBtnMerchant: UIImageView!
    @IBOutlet weak var imgRadioBtnPersonal: UIImageView!
    @IBOutlet weak var imgRadioBtnAgent: UIImageView!
    
    @IBOutlet weak var lblRequired: UILabel!{
        didSet{
            lblRequired.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if ReRegistrationModel.shared.lType == "SUBSBR" {
            ReRegistrationModel.shared.AccountType = 1
            self.imgRadioBtnPersonal.image = UIImage(named: "select_radio")
            self.imgRadioBtnMerchant.image = UIImage(named: "r_Unradio")
            self.imgRadioBtnAgent.image = UIImage(named: "r_Unradio")
        }else if ReRegistrationModel.shared.lType == "MER" {
            ReRegistrationModel.shared.AccountType = 0
            self.imgRadioBtnPersonal.image = UIImage(named: "r_Unradio")
            self.imgRadioBtnMerchant.image = UIImage(named: "select_radio")
            self.imgRadioBtnAgent.image = UIImage(named: "r_Unradio")
        }else if ReRegistrationModel.shared.lType == "AGENT"{
            ReRegistrationModel.shared.AccountType = 0
            self.imgRadioBtnPersonal.image = UIImage(named: "r_Unradio")
            self.imgRadioBtnMerchant.image = UIImage(named: "r_Unradio")
            self.imgRadioBtnAgent.image = UIImage(named: "select_radio")
        }else {
            self.imgRadioBtnMerchant.image = UIImage(named: "r_Unradio")
            self.imgRadioBtnPersonal.image = UIImage(named: "r_Unradio")
            self.imgRadioBtnAgent.image = UIImage(named: "r_Unradio")
        }

        let (mobileNumber,CountryCode) = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: ReRegistrationModel.shared.MobileNumber)
        lblMobilNo.text = mobileNumber
        let countryData = identifyCountry(withPhoneNumber: CountryCode)
        lblCountryCode.text = "(" + countryData.countryCode + ")"
        ReRegistrationModel.shared.CountryCode = countryData.countryCode
        if let safeImage = UIImage(named: countryData.countryFlag) {
            DispatchQueue.main.async {
                self.imgCountryFlag.image = safeImage
            }
        }
    
        if let operatorName = preNetInfo?.operatorName {
            lblOperatorName.text = operatorName
            ReRegistrationModel.shared.operatorName = operatorName
        }
    }
    
    func updateLaguageCell() {
        lblRequired.text = "Required Fields".localized + " *"
        lbAccountMobileNo.text = "OK$ Account Open Number".localized
        lbSelectAccountType.text = "Select Account Type".localized
        lblMerchant.text = "Merchant_Account".localized
        lblPersonal.text = "Personal_Account".localized
        lblAgent.text = "Agent Account".localized
    }
    
    @IBAction func onClickMerchantAction(_ sender: Any) {
        if UserDefaults.standard.bool(forKey: "RE_REGITRATION") {
            return
        }
        
        if !(imgRadioBtnMerchant.image == UIImage(named: "select_radio")) {
            ReRegistrationModel.shared.AccountType = 0
            ReRegistrationModel.shared.lType = "MER"
            if let del = delegate {
                del.comeBackFromMainScreen()
            }
        }
    }
    
    @IBAction func onClickPersonalAction(_ sender: Any) {
        if UserDefaults.standard.bool(forKey: "RE_REGITRATION") {
            return
        }
        if !(imgRadioBtnPersonal.image == UIImage(named: "select_radio")) {
            ReRegistrationModel.shared.AccountType = 1
            ReRegistrationModel.shared.lType = "SUBSBR"
            if let del = delegate {
                del.showAlertPersonalInfo(message: "If you open Personal account there is a daily transfer limit  500,000 MMK.".localized)
            }
        }
    }
    
    @IBAction func onClickAgentAction(_ sender: Any) {
        if UserDefaults.standard.bool(forKey: "RE_REGITRATION") {
            return
        }
        if !(imgRadioBtnAgent.image == UIImage(named: "select_radio")) {
            ReRegistrationModel.shared.AccountType = 0
            ReRegistrationModel.shared.lType = "AGENT"
            if let del = delegate {
                del.comeBackFromMainScreen()
            }
        }
    }
    
    
    
    @IBAction func onClickDemoAction(_ sender: Any) {
            if let del = delegate {
                var strURL = ""
                if ReRegistrationModel.shared.Language.lowercased() == "my"{
                    strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/3f7b2c1e-a2e4-4563-8693-afb7f58c70daReqDocumentsJuly_02_2018%2001_38_59%20PM.jpg";
                }else {
                    strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/32e4f212-896a-4517-831b-88f96be0c855ReqDocumentsJuly_02_2018%2001_43_13%20PM.jpg";
                }
                del.showDemo(strURL: strURL, imgDemo: #imageLiteral(resourceName: "user") , demoTitile: "Registration".localized)
            }
    }
    
}
