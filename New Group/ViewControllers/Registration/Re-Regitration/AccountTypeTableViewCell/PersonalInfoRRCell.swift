//
//  PersonalInfoRRCell.swift
//  OK
//
//  Created by Imac on 5/30/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol PersonalInfoRRCellDelegate {
    func resetPersonalInfo()
    func showInfoForPersonal()
}

class PersonalInfoRRCell: UITableViewCell {
    var delegate: PersonalInfoRRCellDelegate?
    @IBOutlet weak var btnReset: UIButton!{
        didSet{
            btnReset.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnInfo: UIButton!{
        didSet{
            btnInfo.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblPersonalInfo: UILabel!{
        didSet{
            lblPersonalInfo.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
    }
 
    func updateLaguageCell() {
        lblPersonalInfo.attributedText = lblPersonalInfo.attributedStringFinal(str1: "Personal Information".localized)
        if ReRegistrationModel.shared.Language.lowercased() == "en" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue"), for: .normal)
        }else if ReRegistrationModel.shared.Language.lowercased() == "my" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
        }else {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
        }
    }
    @IBAction func onClickPersonalActioin(_ sender: Any) {
        if let del = self.delegate {
            del.resetPersonalInfo()
        }
    }
    @IBAction func onClickPersonalInfo(_ sender: Any) {
        if let del = delegate {
            del.showInfoForPersonal()
        }
        
    }
}
