//
//  TakeProfileImageRRCell.swift
//  OK
//
//  Created by Imac on 5/30/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol TakeProfileImageRRCellDelegate {
    func navigateToTakeImage()
}

class TakeProfileImageRRCell: UITableViewCell {
    var delegate: TakeProfileImageRRCellDelegate?
    @IBOutlet weak var btnTakeSalfie: UIButton!{
        didSet{
            btnTakeSalfie.titleLabel?.font = UIFont(name: appFont, size: 17.0)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func updateLaguageCell() {
      self.btnTakeSalfie.setTitle("Take Your Photo (Selfie)".localized, for: .normal)
    }
    @IBAction func onClickTakePhoto(_ sender: Any) {
        if let del = delegate {
            del.navigateToTakeImage()
        }
    }
}
