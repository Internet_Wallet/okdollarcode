//
//  AddressHeaderRRCell.swift
//  OK
//
//  Created by Imac on 6/2/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
protocol AddressHeaderRRCellDelegte {
    func ShowCurrentLocationVC(wihttype: String)
    func resetAddress()
    func showAddressDemo()
}
class AddressHeaderRRCell: UITableViewCell {
    var delegate: AddressHeaderRRCellDelegte?
    @IBOutlet weak var lblHeader: UILabel!{
        didSet{
            lblHeader.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblHome: UILabel!{
        didSet{
            lblHome.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblOther: UILabel!{
        didSet{
            lblOther.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var imgBusiness: UIImageView!
    @IBOutlet weak var imgOther: UIImageView!
    
    @IBOutlet weak var lblBusiness: MarqueeLabel!{
        didSet{
            lblBusiness.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnReset: UIButton!{
        didSet{
            btnReset.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func updateLaguageCell() {
        lblHeader.attributedText = lblHeader.attributedStringFinal(str1: "Address Details".localized)
        lblHome.text = "Home".localized
        lblBusiness.text = "Business".localized
        lblOther.text = "Other".localized
        
        if ReRegistrationModel.shared.Language.lowercased() == "en"{
            btnReset.setBackgroundImage(UIImage(named: "reset_blue"), for: .normal)
        }else if ReRegistrationModel.shared.Language.lowercased() == "my" {
             btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
        }else {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
        }
    }
    @IBAction func btnResetAction(_ sender: Any) {
        imgHome.image = UIImage(named: "r_Unradio")
        imgBusiness.image = UIImage(named: "r_Unradio")
        imgOther.image = UIImage(named: "r_Unradio")
        if let del = delegate {
            del.resetAddress()
        }
    }
    @IBAction func btnShowDemoAction(_ sender: Any) {
        if let del = delegate {
            del.showAddressDemo()
        }
    }
    
    @IBAction func btnHomeAction(_ sender: Any) {
        if !(imgHome.image == UIImage(named: "select_radio")) {
            imgHome.image = UIImage(named: "select_radio")
            imgBusiness.image = UIImage(named: "r_Unradio")
            imgOther.image = UIImage(named: "r_Unradio")
            ReRegistrationModel.shared.AddressType = lblHome.text!
       
            if let del = delegate {
                del.ShowCurrentLocationVC(wihttype: "home")
            }
        }
    }
    
    @IBAction func btnBusinessAction(_ sender: Any) {
        if !(imgBusiness.image == UIImage(named: "select_radio")) {
            imgHome.image = UIImage(named: "r_Unradio")
            imgBusiness.image = UIImage(named: "select_radio")
            imgOther.image = UIImage(named: "r_Unradio")
            ReRegistrationModel.shared.AddressType = lblBusiness.text!
            if let del = delegate {
                del.ShowCurrentLocationVC(wihttype: "business")
            }
        }
    }
    
    @IBAction func btnOtherAction(_ sender: Any) {
        if !(imgOther.image == UIImage(named: "select_radio")) {
            imgHome.image = UIImage(named: "r_Unradio")
            imgBusiness.image = UIImage(named: "r_Unradio")
            imgOther.image = UIImage(named: "select_radio")
            ReRegistrationModel.shared.AddressType = lblOther.text!
            if let del = delegate {
                del.ShowCurrentLocationVC(wihttype: "other")
            }
            
        }
    }
}
