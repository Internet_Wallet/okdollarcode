//
//  VillageRRCell.swift
//  OK
//
//  Created by Imac on 6/2/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
protocol VillageRRCellDelegate {
    func showVillageView(text: String)
    func removeVillageList()
}
class VillageRRCell: UITableViewCell,UITextFieldDelegate {
    var delegate: VillageRRCellDelegate?
    @IBOutlet weak var tfVillageName: UITextField!{
        didSet{
            tfVillageName.font = UIFont(name: appFont, size: 18.0)
        }
    }
    var CHARSET = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        
        var font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
            font = UIFont(name: appFont, size: 18.0)
        }
        tfVillageName.font = UIFont(name: appFont, size: 18.0)
        
        tfVillageName.delegate = self
        if ReRegistrationModel.shared.Language.lowercased() == "en"{
            CHARSET = STREETNAME_CHAR_SET_En
            tfVillageName.font = UIFont.systemFont(ofSize: 18)
        }else if ReRegistrationModel.shared.Language.lowercased() == "my" {
            CHARSET = STREETNAME_CHAR_SET_My
            tfVillageName.font = UIFont(name: appFont, size: 18)
        }else {
            CHARSET = STREETNAME_CHAR_SET_Uni
           
            
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfVillageName.attributedPlaceholder = NSAttributedString(string: "Village Tract", attributes:attributes as [NSAttributedString.Key : Any])
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfVillageName.attributedPlaceholder = NSAttributedString(string: "Village Tract", attributes:attributes as [NSAttributedString.Key : Any])
                tfVillageName.font = UIFont(name: appFont, size: 18)
            }
            tfVillageName.font = UIFont(name: appFont, size: 18)
            
        }
        tfVillageName.placeholder = "Village Tract".localized
    }
    
    func updateLaguageCell(name: String) {
        tfVillageName.font = UIFont(name: appFont, size: 18.0)
        tfVillageName.text = name
    }
    
    @IBAction func editChange(_ sender: UITextField) {
        if let del = delegate {
            del.showVillageView(text: sender.text ?? "")
        }
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        removeSpaceAtLast(txtField: tfVillageName)
        ReRegistrationModel.shared.VillageName = self.tfVillageName.text!
        if let del = delegate {
            del.removeVillageList()
        }
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if range.location == 0 && string == " " {
            return false
        }
        
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
        if text.count > 30 {
            if string == "" {
                return true
            }else{
                return false
            }
        }
        return true
    }
    
    func removeSpaceAtLast(txtField : UITextField){
        if txtField.text?.last == " " {
            txtField.text?.removeLast()
        }
    }
}
