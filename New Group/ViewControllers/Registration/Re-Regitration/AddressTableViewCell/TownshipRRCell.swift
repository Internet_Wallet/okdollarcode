//
//  TownshipRRCell.swift
//  OK
//
//  Created by Imac on 6/2/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
protocol TownshipRRCellDelegate {
    func navigteToTownshipVC()
}

class TownshipRRCell: UITableViewCell {
    var delegate: TownshipRRCellDelegate?
    @IBOutlet weak var btnTownship: UIButton!{
        didSet{
            btnTownship.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        btnTownship.setTitle("Select Township".localized, for: .normal)
    }

    func updateLaguageCell(name: String) {
        if name == ""{
           btnTownship.setTitle("Select Township".localized, for: .normal)
        }else {
              btnTownship.setTitle(name, for: .normal)
        }
    }
    
    @IBAction func onClickTownshipAction(_ sender: Any) {
        if let del = delegate {
            del.navigteToTownshipVC()
        }
    }
    
    
}
