//
//  OtherAddressRRCell.swift
//  OK
//
//  Created by Imac on 6/2/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol OtherAddressRRCellDelegte {
    func showDivision()
    
}
class OtherAddressRRCell: UITableViewCell,UITextFieldDelegate {
    
    var delegate: OtherAddressRRCellDelegte?
    @IBOutlet weak var tfTypeHere: UITextField!{
        didSet{
            tfTypeHere.font = UIFont(name: appFont, size: 18.0)
        }
    }
    @IBOutlet weak var btnTypeHereClear: UIButton!{
        didSet{
            btnTypeHereClear.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    var OTHERCHARSET = ""
    var isNextViewHidden = true
    override func awakeFromNib() {
        super.awakeFromNib()
        tfTypeHere.delegate = self
    }
    
    
    func updateLaguageCell() {
        var font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
            font = UIFont.systemFont(ofSize: 18)
        }
        tfTypeHere.font = font
        
        
        if ReRegistrationModel.shared.Language.lowercased() == "en"{
            OTHERCHARSET = STREETNAME_CHAR_SET_En
             tfTypeHere.font = UIFont.systemFont(ofSize: 18)
        }else if ReRegistrationModel.shared.Language.lowercased() == "my" {
            OTHERCHARSET = STREETNAME_CHAR_SET_My
             tfTypeHere.font = UIFont(name: appFont, size: 18)
        }else {
           OTHERCHARSET =  STREETNAME_CHAR_SET_Uni
            
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfTypeHere.attributedPlaceholder = NSAttributedString(string: "Enter here", attributes:attributes as [NSAttributedString.Key : Any])
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfTypeHere.attributedPlaceholder = NSAttributedString(string: "Enter here", attributes:attributes)
                tfTypeHere.font = UIFont.systemFont(ofSize: 18)
            }
        }
            tfTypeHere.placeholder = "Enter here".localized
    }
    
    @IBAction func btnTypeHereClearAction(_ sender: Any) {
        btnTypeHereClear.isHidden = true
        tfTypeHere.text = ""
    }
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
            removeSpaceAtLast(txtField: tfTypeHere)
            if tfTypeHere.text?.count == 0 {
                ReRegistrationModel.shared.AddressType = "Other".localized
            }else {
                ReRegistrationModel.shared.AddressType = self.tfTypeHere.text!
            }
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        
        
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if range.location == 0 && string == " " {
            return false
        }
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: OTHERCHARSET).inverted).joined(separator: "")) { return false }
            if text.count > 0 {
                btnTypeHereClear.isHidden = false
                if isNextViewHidden {
                    isNextViewHidden = false
                    if let del = delegate {
                        del.showDivision()
                    }
                }
            }else {
                btnTypeHereClear.isHidden = true
            }
        
            if text.count > 50 {
                return false
            }
            if restrictMultipleSpaces(str: string, textField: tfTypeHere) {
                return true
            }else {
                if  textField.text?.last == " " {
                    textField.text?.removeLast()
                }
                return false
            }
    }
    private func removeSpaceAtLast(txtField : UITextField){
        if txtField.text?.last == " " {
            txtField.text?.removeLast()
        }
    }
}
