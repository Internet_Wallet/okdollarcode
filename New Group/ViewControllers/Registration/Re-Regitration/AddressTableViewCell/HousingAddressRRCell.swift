//
//  HousingAddressRRCell.swift
//  OK
//
//  Created by Imac on 6/2/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class HousingAddressRRCell: UITableViewCell,UITextFieldDelegate {
    
    @IBOutlet weak var tfHousingZone: UITextField!{
        didSet{
            tfHousingZone.font = UIFont(name: appFont, size: 18.0)
        }
    }
    @IBOutlet weak var lblHousingZone: UILabel!{
        didSet{
            lblHousingZone.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    var CHARSET = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        tfHousingZone.delegate = self
    }
    
     func updateLaguageCell(house : String) {
      
        var font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
            font = UIFont.systemFont(ofSize: 18)
        }
        tfHousingZone.font = font
        
        tfHousingZone.text = house
        if ReRegistrationModel.shared.Language.lowercased() == "en"{
            CHARSET = STREETNAME_CHAR_SET_En
              tfHousingZone.font =  UIFont.systemFont(ofSize: 18)
        }else if ReRegistrationModel.shared.Language.lowercased() == "my" {
            CHARSET = STREETNAME_CHAR_SET_My
           tfHousingZone.font =  UIFont(name: appFont, size: 18)
        }else {
            CHARSET = STREETNAME_CHAR_SET_Uni
            
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfHousingZone.attributedPlaceholder = NSAttributedString(string: "Housing / Zone Name", attributes:attributes as [NSAttributedString.Key : Any])
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfHousingZone.attributedPlaceholder = NSAttributedString(string: "Housing / Zone Name", attributes:attributes)
                tfHousingZone.font = UIFont.systemFont(ofSize: 18)
            }
            
        }
        lblHousingZone.text = "Housing / Zone Name".localized
        tfHousingZone.placeholder = "Housing / Zone Name".localized
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
            removeSpaceAtLast(txtField: tfHousingZone)
             ReRegistrationModel.shared.HouseName = self.tfHousingZone.text!
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if range.location == 0 && string == " " {
            return false
        }
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
        if text.count > 30 {
            return false
        }
        if restrictMultipleSpaces(str: string, textField: tfHousingZone) {
            return true
        }else {
            if  textField.text?.last == " " {
                textField.text?.removeLast()
            }
            return false
        }
    }
    
    func removeSpaceAtLast(txtField : UITextField){
        if txtField.text?.last == " " {
            txtField.text?.removeLast()
        }
    }
}
