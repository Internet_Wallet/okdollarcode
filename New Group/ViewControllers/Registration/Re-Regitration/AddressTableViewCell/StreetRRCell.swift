//
//  StreetRRCell.swift
//  OK
//
//  Created by Imac on 6/2/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
protocol StreetRRCellDelegate {
    func showStreetView(text: String)
    func showRoomNumberView()
    func removeStreetList()
}
class StreetRRCell: UITableViewCell,UITextFieldDelegate{
    var delegate: StreetRRCellDelegate?
    @IBOutlet weak var tfStreetName: UITextField!{
        didSet{
            tfStreetName.font = UIFont(name: appFont, size: 18.0)
        }
    }
    var CHARSET = ""
    var isNextViewHidden = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        var font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
            font =  UIFont(name: appFont, size: 18.0)
        }
        tfStreetName.font = UIFont(name: appFont, size: 18.0)
        
        tfStreetName.delegate = self
        if ReRegistrationModel.shared.Language.lowercased() == "en"{
            CHARSET = STREETNAME_CHAR_SET_En
            tfStreetName.font = UIFont.systemFont(ofSize: 18)
        }else if ReRegistrationModel.shared.Language.lowercased() == "my" {
            CHARSET = STREETNAME_CHAR_SET_My
            tfStreetName.font = UIFont(name: appFont, size: 18)
        }else {
            CHARSET = STREETNAME_CHAR_SET_Uni
            
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfStreetName.attributedPlaceholder = NSAttributedString(string: "Enter street", attributes:attributes as [NSAttributedString.Key : Any])
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfStreetName.attributedPlaceholder = NSAttributedString(string: "Enter street", attributes:attributes as [NSAttributedString.Key : Any])
                tfStreetName.font =  UIFont(name: appFont, size: 18)
            }
            tfStreetName.font = UIFont(name: appFont, size: 18.0)
        }
        tfStreetName.placeholder  = "Enter street".localized
    }

    func updateLaguageCell(name: String) {
        tfStreetName.font = UIFont(name: appFont, size: 18.0)
      tfStreetName.text = name
    }
    @IBAction func editChanged(_ sender: UITextField) {
        if let del = delegate {
            del.showStreetView(text: sender.text ?? "")
        }
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
            removeSpaceAtLast(txtField: tfStreetName)
            ReRegistrationModel.shared.Address2 = self.tfStreetName.text!
        if isNextViewHidden {
        if let del = delegate {
            del.showRoomNumberView()
            }
            isNextViewHidden = false
        }
        
        if let del = delegate {
            del.removeStreetList()
        }
        
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if range.location == 0 && string == " " {
            return false
        }
   
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
        if text.count > 30 {
            if string == "" {
                return true
            }
            return false
        }
        return true
    }
    
    func removeSpaceAtLast(txtField : UITextField){
        if txtField.text?.last == " " {
            txtField.text?.removeLast()
        }
    }
}
