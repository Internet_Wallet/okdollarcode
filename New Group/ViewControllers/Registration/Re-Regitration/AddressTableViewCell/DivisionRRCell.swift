//
//  DivisionRRCell.swift
//  OK
//
//  Created by Imac on 6/2/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
protocol DivisionRRCellDelegate {
    func navigateToDiviosionVC()
}

class DivisionRRCell: UITableViewCell {
    var delegate: DivisionRRCellDelegate?
    @IBOutlet weak var btnDivison: UIButton!{
        didSet{
            btnDivison.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var lblDivision: UILabel!{
        didSet{
            lblDivision.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    var isNextViewHidden = true
    override func awakeFromNib() {
        super.awakeFromNib()
        lblDivision.text = "Select State / Division".localized //lblDivision.scrollMarqueLabel(title: "Select State / Division".localized)
    }
    
    func updateLaguageCell(name: String) {
        if name == "" {
            lblDivision.text = "Select State / Division".localized
        }else {
            lblDivision.text = name
        }
  
    }
    @IBAction func onClickDivisionAction(_ sender: Any) {
        if let del = delegate {
            del.navigateToDiviosionVC()
        }
    }
}
