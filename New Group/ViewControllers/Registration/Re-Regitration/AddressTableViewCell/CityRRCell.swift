//
//  CityRRCell.swift
//  OK
//
//  Created by Imac on 6/2/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class CityRRCell: UITableViewCell {
    @IBOutlet weak var tfCity: UITextField!{
        didSet{
            tfCity.font = UIFont(name: appFont, size: 18.0)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func updateLaguageCell(name: String) {
        tfCity.placeholder =  "City/Region".localized
        tfCity.text = name
    }
}
