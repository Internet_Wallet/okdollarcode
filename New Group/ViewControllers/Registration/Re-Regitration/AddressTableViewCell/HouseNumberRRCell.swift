//
//  HouseNumberRRCell.swift
//  OK
//
//  Created by Imac on 6/2/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol HouseNumberRRCellDelegate {
    func showSecurityVC()
}

class HouseNumberRRCell: UITableViewCell,UITextFieldDelegate {
    var delegate: HouseNumberRRCellDelegate?
    @IBOutlet weak var lblHouseNo: UILabel!{
        didSet{
            lblHouseNo.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblFloorNo: UILabel!{
        didSet{
            lblFloorNo.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblRoomNo: UILabel!{
        didSet{
            lblRoomNo.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var tfHouseNo: UITextField!{
        didSet{
            tfHouseNo.font = UIFont(name: appFont, size: 18.0)
        }
    }
    @IBOutlet weak var tfFloorNo: UITextField!{
        didSet{
            tfFloorNo.font = UIFont(name: appFont, size: 18.0)
        }
    }
    @IBOutlet weak var tfRoomNo: UITextField!{
        didSet{
            tfRoomNo.font = UIFont(name: appFont, size: 18.0)
        }
    }
    var OTHERCHARSET = ""
    var isNextViewHidden = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tfHouseNo.delegate = self
        tfFloorNo.delegate = self
        tfRoomNo.delegate = self
    }
    
    func updateLaguageCell(house : String, floor: String, room: String) {
        tfHouseNo.text = house
        tfFloorNo.text = floor
        tfRoomNo.text = room
   
        
        if ReRegistrationModel.shared.Language.lowercased() == "en"{
            OTHERCHARSET = STREETNAME_CHAR_SET_En
            tfHouseNo.font = UIFont.systemFont(ofSize: 18)
            tfFloorNo.font = UIFont.systemFont(ofSize: 18)
            tfRoomNo.font = UIFont.systemFont(ofSize: 18)
        }else if ReRegistrationModel.shared.Language.lowercased() == "my" {
            OTHERCHARSET = STREETNAME_CHAR_SET_My
            tfHouseNo.font = UIFont(name: appFont, size: 18)
            tfFloorNo.font = UIFont(name: appFont, size: 18)
            tfRoomNo.font = UIFont(name: appFont, size: 18)
        }else {
            OTHERCHARSET = STREETNAME_CHAR_SET_Uni
            let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                              .font : UIFont.systemFont(ofSize: 18)]
            tfHouseNo.attributedPlaceholder = NSAttributedString(string: "House No.", attributes:attributes)
            tfHouseNo.font = UIFont.systemFont(ofSize: 18)
            
            tfFloorNo.attributedPlaceholder = NSAttributedString(string: "Floor No.", attributes:attributes)
            tfFloorNo.font = UIFont.systemFont(ofSize: 18)
            
            tfRoomNo.attributedPlaceholder = NSAttributedString(string: "Room No.", attributes:attributes)
            tfRoomNo.font = UIFont.systemFont(ofSize: 18)
        }
        
        tfHouseNo.placeholder = "House No.".localized
        tfFloorNo.placeholder = "Floor No.".localized
        tfRoomNo.placeholder = "Room No.".localized
        
        lblHouseNo.text = "House No.".localized
        lblFloorNo.text = "Floor No.".localized
        lblRoomNo.text = "Room No.".localized
    }
    
    @IBAction func editChanged(_ textField: UITextField) {
        ReRegistrationModel.shared.HouseBlockNo = self.tfHouseNo.text!
        ReRegistrationModel.shared.FloorNumber = self.tfFloorNo.text!
        ReRegistrationModel.shared.RoomNumber = self.tfRoomNo.text!
    }
    
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField == tfRoomNo {
            if tfRoomNo.text!.count > 0 {
                if isNextViewHidden{
                    isNextViewHidden = false
                    if let del = delegate {
                        del.showSecurityVC()
                    }
                }
                removeSpaceAtLast(txtField: tfRoomNo)
            }
        }else if textField == tfHouseNo {
            if tfHouseNo.text!.count > 0 {
                if isNextViewHidden{
                    isNextViewHidden = false
                    if let del = delegate {
                        del.showSecurityVC()
                    }
                }
                removeSpaceAtLast(txtField: tfHouseNo)
            }
        }else if textField == tfFloorNo {
            if tfFloorNo.text!.count > 0 {
                if isNextViewHidden{
                    isNextViewHidden = false
                    if let del = delegate {
                        del.showSecurityVC()
                    }
                }
                removeSpaceAtLast(txtField: tfFloorNo)
           
            }
        }
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        
        
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if range.location == 0 && string == " " {
            return false
        }
        if textField == tfHouseNo {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: OTHERCHARSET).inverted).joined(separator: "")) { return false }
            if text.count > 6 {
                return false
            }
        }else if textField == tfFloorNo {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: OTHERCHARSET).inverted).joined(separator: "")) { return false }
            if  text.count > 6  {
                return false
            }
        }else if textField == tfRoomNo {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: OTHERCHARSET).inverted).joined(separator: "")) { return false }
            if  text.count > 6  {
                return false
            }
            
        }
        return true
    }
    
    func removeSpaceAtLast(txtField : UITextField){
        if txtField.text?.last == " " {
            txtField.text?.removeLast()
        }
    }
}
