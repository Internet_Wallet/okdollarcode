//
//  PassportExpiryDateRRCell.swift
//  OK
//
//  Created by Imac on 5/31/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
protocol PassportExpiryDateRRCellDelegate {
    func showIDPhoto()
}

class PassportExpiryDateRRCell: UITableViewCell,UITextFieldDelegate {
    var delegate: PassportExpiryDateRRCellDelegate?
    @IBOutlet weak var tfDate_Of_Expiry: UITextField!{
        didSet{
            tfDate_Of_Expiry.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    var btnCancel = UIButton()
    var btnDone = UIButton()
    var strDOB = ""
    let dateFormatter = DateFormatter()
    var isNextViewHidden = true
    override func awakeFromNib() {
        super.awakeFromNib()
        tfDate_Of_Expiry.delegate = self
        
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        let screenSize: CGRect = UIScreen.main.bounds
        let viewDOBKeyboard = UIView(frame: CGRect(x: 0, y: 0, width:screenSize.width , height: 40))
        viewDOBKeyboard.backgroundColor = UIColor(red: 244/255, green: 199/255, blue: 22/255, alpha: 1)
        
        btnCancel  = UIButton(frame: CGRect(x: 10, y: 0, width:100, height: 40))
        btnCancel.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
        btnCancel.setTitle("Cancel".localized, for: UIControl.State.normal)
        btnCancel.addTarget(self, action:#selector(btnCancelDOBAction), for: UIControl.Event.touchUpInside)
        viewDOBKeyboard.addSubview(btnCancel)
        
        btnDone  = UIButton(frame: CGRect(x: screenSize.width - 110, y: 0, width:100, height: 40))
        btnDone.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
        btnDone.setTitle("Done".localized, for: UIControl.State.normal)
        btnDone.addTarget(self, action:#selector(btnDoneDOBAction), for: UIControl.Event.touchUpInside)
        viewDOBKeyboard.addSubview(btnDone)
        tfDate_Of_Expiry.inputAccessoryView = viewDOBKeyboard
    }
    func updateLaguageCell() {
        var font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
            font = UIFont.systemFont(ofSize: 18)
        }
        tfDate_Of_Expiry.font = font
        
        if ReRegistrationModel.shared.Language.lowercased() == "my"{
            tfDate_Of_Expiry.font = UIFont(name: appFont, size: 18)
        }else if ReRegistrationModel.shared.Language.lowercased() == "en"{
            tfDate_Of_Expiry.font = UIFont.systemFont(ofSize: 18)
        }else {
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfDate_Of_Expiry.attributedPlaceholder = NSAttributedString(string: "Enter Passport Expiry Date", attributes:attributes as [NSAttributedString.Key : Any])
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfDate_Of_Expiry.attributedPlaceholder = NSAttributedString(string: "Enter Passport Expiry Date", attributes:attributes)
                tfDate_Of_Expiry.font = UIFont.systemFont(ofSize: 18)
            }
        }
        
        tfDate_Of_Expiry.placeholder = "Enter Passport Expiry Date".localized
    }
    
    func wrapData() {
    }
    
    @objc func datePickerChanged(sender: UIDatePicker) {
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        strDOB = dateFormatter.string(from: sender.date)
    }

    @objc func btnCancelDOBAction () {
        tfDate_Of_Expiry.resignFirstResponder()
    }
    
    @objc func btnDoneDOBAction () {
        tfDate_Of_Expiry.resignFirstResponder()
        if (strDOB.count) > 0 {
            tfDate_Of_Expiry.text = strDOB
            ReRegistrationModel.shared.IdExpDate = tfDate_Of_Expiry.text!
                if let del = delegate {
                    del.showIDPhoto()
                }
        }
    }

    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
            let date = Date()
            let datePicker = UIDatePicker()
            datePicker.datePickerMode = .date
            datePicker.backgroundColor = UIColor.white
            datePicker.calendar = Calendar(identifier: .gregorian)
        if #available(iOS 13.4, *) {
                       datePicker.preferredDatePickerStyle = .wheels
                   } else {
                       // Fallback on earlier versions
                   }
            if strDOB.count > 0 {
                datePicker.date = dateFormatter.date(from: strDOB)!
            }else {
                datePicker.date = date
            }
            datePicker.minimumDate = Date(timeInterval: (24 * 60 * 60), since: date)
            let maxDate = Calendar.current.date(byAdding: .year, value: 100, to: Date())
            datePicker.maximumDate = maxDate
            strDOB = dateFormatter.string(from: datePicker.date)
            datePicker.addTarget(self, action: #selector(datePickerChanged(sender:)), for: .valueChanged)
            tfDate_Of_Expiry.inputView = datePicker
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
    
}
