//
//  PassportNumberRRCell.swift
//  OK
//
//  Created by Imac on 5/31/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
protocol PassportNumberRRCellDelegate {
    func showPassportExpiryDateView()
}

class PassportNumberRRCell: UITableViewCell,UITextFieldDelegate {
    var delegate: PassportNumberRRCellDelegate?
    @IBOutlet weak var tfPassportNo: UITextField!{
        didSet{
            tfPassportNo.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    //var PASSPORTCHARSET = "(),_-1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    var isNextViewHidden = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tfPassportNo.delegate = self
    }
    func updateLaguageCell() {
        
        var font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
            font = UIFont.systemFont(ofSize: 18)
        }
        tfPassportNo.font = font
        
        
        if ReRegistrationModel.shared.Language.lowercased() == "my"{
            tfPassportNo.font = UIFont(name: appFont, size: 18)
        }else if ReRegistrationModel.shared.Language.lowercased() == "en"{
            tfPassportNo.font = UIFont.systemFont(ofSize: 18)
        }else {
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfPassportNo.attributedPlaceholder = NSAttributedString(string: "Enter Passport Number", attributes:attributes as [NSAttributedString.Key : Any])
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfPassportNo.attributedPlaceholder = NSAttributedString(string: "Enter Passport Number", attributes:attributes)
                tfPassportNo.font = UIFont.systemFont(ofSize: 18)
            }
            
        }
        
        tfPassportNo.placeholder = "Enter Passport Number".localized
    }
    func wrapData(){
        
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        ReRegistrationModel.shared.NRC = self.tfPassportNo.text!
        removeSpaceAtLast(txtField:tfPassportNo)
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                if range.location == 0 && string == " " {
            return false
        }
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: PASSPORT_CHAR_SET).inverted).joined(separator: "")) { return false }
        if text.count > 4 {
            if isNextViewHidden {
                if let del = delegate {
                   del.showPassportExpiryDateView()
                    self.isNextViewHidden = false
                }
            }
            return text.count <= 14
        }
        return true
    }
    private func removeSpaceAtLast(txtField : UITextField){
        if txtField.text?.last == " " {
            txtField.text?.removeLast()
        }
    }
}
