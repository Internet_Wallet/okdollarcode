//
//  NRCNumberRRCell.swift
//  OK
//
//  Created by Imac on 5/31/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol NRCNumberRRCellDelegate {
    func showTownshipVC()
    func showIDPhoto()
    func showNRCSuggestionView()
}

class NRCNumberRRCell: UITableViewCell,MyTextFieldDelegate {
    var delegate: NRCNumberRRCellDelegate?
    @IBOutlet var viewNRCTF: UIView!
    @IBOutlet var viewNRCButton: UIView!
    @IBOutlet var tfNRC: MyTextField!{
        didSet{
            tfNRC.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var btnNRC: UIButton!{
        didSet{
            btnNRC.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    let btnnrc = UIButton()
    var isNextViewHidden = true
    var  NRCCHARSET = "1234567890"
    override func awakeFromNib() {
        super.awakeFromNib()
        self.tfNRC.addTarget(self, action: #selector(IDProofVC.textFieldDidChange(_:)),
                             for: UIControl.Event.editingChanged)
        var fnt = UIFont.systemFont(ofSize: 18)
        if ok_default_language == "en" {
           self.tfNRC.font = UIFont.systemFont(ofSize: 18)
        }else if ok_default_language == "my" {
           self.tfNRC.font = UIFont(name: appFont, size: 18)
            fnt = UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18.0)
        }else {
           self.tfNRC.font = UIFont(name: appFont, size: 18)
        }
        self.tfNRC.myDelegate = self
        
        if ReRegistrationModel.shared.NRC != "" {
            if ReRegistrationModel.shared.NRC.contains(find: "@") {
                let nrc = ReRegistrationModel.shared.NRC.replacingOccurrences(of: "@", with: "/")
                if nrc.contains(find: ")") {
                    viewNRCTF.isHidden = false
                    viewNRCButton.isHidden = true
                    let nrcStringArr = nrc.components(separatedBy: ")")
                    let nrcPrefix = nrcStringArr[0]
                    viewNRCButton.isHidden = true
                    viewNRCTF.isHidden = false
                    let myString = nrcPrefix + ")"
                    let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: fnt])
                    btnnrc.frame = CGRect(x: 0, y: 0, width:stringSize.width + 12, height: 52)
                    btnnrc.setTitleColor(.black, for: .normal)
                    btnnrc.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
                    btnnrc.addTarget(self, action: #selector(self.BtnNrcTypeAction(button:)), for: .touchUpInside)
                    btnnrc.setTitle(myString, for: UIControl.State.normal)
                    let btnView = UIView()
                    btnView.frame = CGRect(x: 0, y: 0, width: btnnrc.frame.width , height: 52)
                    btnView.addSubview(btnnrc)
                    tfNRC.leftView = nil
                    tfNRC.leftView = btnView
                    tfNRC.leftViewMode = UITextField.ViewMode.always
                    let nrPostFix = nrcStringArr[1]
                    tfNRC.text = nrPostFix
                 }
            }
        }
    }
    
    @objc func BtnNrcTypeAction(button : UIButton) {
        if let del = delegate {
            del.showNRCSuggestionView()
        }
    }
    
    func updateLaguageCell() {
        btnNRC.setTitle("Enter NRC Number".localized, for: .normal)
    }
    func wrapData(){
        
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if btnnrc.titleLabel?.text == nil{
            //
            btnNRC.setTitle("Enter NRC Number".localized, for: .normal)
            tfNRC.text = ""
            
            viewNRCButton.isHidden = false
            viewNRCTF.isHidden = true
        }else{
            if tfNRC.text == "000000" {
                tfNRC.text?.removeLast()
            }else if tfNRC.text!.count == 0 {
                ReRegistrationModel.shared.NRC = ""
            }else if tfNRC.text!.count > 6 {
                tfNRC.text?.removeLast()
                tfNRC.resignFirstResponder()
            }else if tfNRC.text!.count == 6 {
                tfNRC.resignFirstResponder()
                    if let del = delegate {
                        del.showIDPhoto()
                    }
                let helpingFuntion = HelpingFunctionR()
                ReRegistrationModel.shared.NRC = helpingFuntion.charecterReplace(charecter: "/", replacingWith: "@", mainString: btnnrc.title(for: .normal)! + tfNRC.text!)
            }
        }
        
        
       
//        else {
//            btnNRC.setTitle("Enter NRC Number".localized, for: .normal)
//            tfNRC.text = ""
//
//            viewNRCButton.isHidden = false
//            viewNRCTF.isHidden = true
//
//
////            let helpingFuntion = HelpingFunctionR()
////            ReRegistrationModel.shared.NRC = helpingFuntion.charecterReplace(charecter: "/", replacingWith: "@", mainString: btnnrc.title(for: .normal)! + tfNRC.text!)
//        }
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let length = textField.text!.count + string.count - range.length
        if range.location == 0 && string == " " {
            return false
        }
        if textField == self.tfNRC {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: NRCCHARSET).inverted).joined(separator: "")) { return false }
            if length > 6 {
                tfNRC.resignFirstResponder()
                return false
            }
        }
        return true
    }
    func textFieldDidDelete() {
        if tfNRC.text?.count == 0 {
            if let del = delegate {
                del.showNRCSuggestionView()
            }
        }
    }
    @IBAction func btnNRCAction(_ sender: Any) {
        if let del = delegate {
            tfNRC.text = ""
            btnNRC.setTitle("Enter NRC Number".localized, for: .normal)
            del.showTownshipVC()
        }
    }
}
