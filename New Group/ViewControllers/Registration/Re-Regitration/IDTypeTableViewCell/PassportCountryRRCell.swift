//
//  PassportCountryRRCell.swift
//  OK
//
//  Created by Imac on 5/31/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol PassportCountryRRCellDelegate {
   func showCountryVC()
}

class PassportCountryRRCell: UITableViewCell {
    var delegate: PassportCountryRRCellDelegate?
    @IBOutlet weak var btnSelectCountry: UIButton!{
        didSet{
            btnSelectCountry.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var imgCountry: UIImageView!
    var isNextViewHidden = true
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func updateLaguageCell() {
        if ReRegistrationModel.shared.CountryOfCitizen == "" {
            btnSelectCountry.setTitle("Select Country of Citizenship".localized, for: .normal)
        }else {
            btnSelectCountry.setTitle(ReRegistrationModel.shared.CountryOfCitizen, for: .normal)
            imgCountry.image = UIImage(named: ReRegistrationModel.shared.countryImage)
        }
  
    }
    func wrapData(){
        
    }
    
    @IBAction func SelectCountryAction(_ sender: Any) {
        if let del = delegate {
            del.showCountryVC()
        }
        //tfPassportNo.text = ""
    }

}
