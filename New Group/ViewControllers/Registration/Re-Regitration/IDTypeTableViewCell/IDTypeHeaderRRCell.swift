//
//  IDTypeHeaderRRCell.swift
//  OK
//
//  Created by Imac on 5/31/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol IDTypeHeaderRRCellDelegate {
    func resetIDType()
    func showIDTypeInfo()
}

class IDTypeHeaderRRCell: UITableViewCell {
    var delegate: IDTypeHeaderRRCellDelegate?
    @IBOutlet weak var btnReset: UIButton!{
        didSet{
            btnReset.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnInfo: UIButton!{
        didSet{
            btnInfo.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblIDType: UILabel!{
        didSet{
            lblIDType.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    
    func updateLaguageCell() {
        lblIDType.attributedText = lblIDType.attributedStringFinal(str1: "Select ID Type".localized)
        if ReRegistrationModel.shared.Language.lowercased() == "en" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue"), for: .normal)
        }else if ReRegistrationModel.shared.Language == "my" {
           btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
        }else {
           btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
        }
    }
    @IBAction func onClickIDTypeResetAction(_ sender: Any) {
        if let del = delegate {
            del.resetIDType()
        }
    }
    
    @IBAction func onClickIDTypeInfoAction(_ sender: Any) {
        if let del = delegate {
            del.showIDTypeInfo()
        }
    }
}
