//
//  IDPhotoRRCell.swift
//  OK
//
//  Created by Imac on 5/31/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol IDPhotoRRCellDelegate {
    func navigateToNRCImage()
}


class IDPhotoRRCell: UITableViewCell {
    var delegate: IDPhotoRRCellDelegate?
    
    @IBOutlet weak var lblAttach: MarqueeLabel!{
        didSet{
            lblAttach.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgFront: UIImageView!
    @IBOutlet weak var lblFront: UILabel!{
        didSet{
            lblFront.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var lblBack: UILabel!{
        didSet{
            lblBack.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgArrow: UIImageView!
    
    var isNextViewHidden = true
    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        lblAttach.isUserInteractionEnabled = true
        lblAttach.addGestureRecognizer(tap)
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        imgArrow.isUserInteractionEnabled = true
        imgArrow.addGestureRecognizer(tap1)
    }
    func updateLaguageCell() {
        if ReRegistrationModel.shared.IDType == "01" {
            lblAttach.text = "Attach NRC ID Photo".localized
            lblFront.text = "Front Side".localized
            lblBack.text = "Back Side".localized
        }else {
            lblAttach.text = "Attach Passport ID Photo".localized
            lblFront.text = "Main Page".localized
            lblBack.text = "Validity Page".localized
        }
    }
    
    func wrapData(){
        
    }
    
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        if let del = delegate {
            del.navigateToNRCImage()
        }
    }
    
    
    @IBAction func onClickImageAction(_ sender: UIButton){
        if let del = delegate {
            del.navigateToNRCImage()
        }
    
    }
    @IBAction func onClickBackImage(_ sender: Any) {
        if let del = delegate {
            del.navigateToNRCImage()
        }
    }
}
