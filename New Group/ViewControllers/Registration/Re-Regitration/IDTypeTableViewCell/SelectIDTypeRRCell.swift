//
//  SelectIDTypeRRCell.swift
//  OK
//
//  Created by Imac on 5/31/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
protocol SelectIDTypeRRCellDelegate{
    func showStateDivision()
    func showCountryList()
}

class SelectIDTypeRRCell: UITableViewCell {
    var delegate: SelectIDTypeRRCellDelegate!
    @IBOutlet weak var lblNRC: UILabel!{
        didSet{
            lblNRC.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblPassport: UILabel!{
        didSet{
            lblPassport.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnOR: UIButton!{
        didSet{
            btnOR.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
 
    @IBOutlet weak var btnEnterNRC: UIButton!{
        didSet{
            btnEnterNRC.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnEnterPassport: UIButton!{
        didSet{
            btnEnterPassport.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //lblNRC.textColor = UIColor.init(red: 22/255.0, green: 45/255.0, blue: 159/255.0, alpha: 1)
    }
    
    func updateLaguageCell() {
        lblNRC.text = "Enter NRC Details".localized
        lblPassport.text = "Enter Passport Details".localized
        self.btnOR.setTitle("(OR)".localized, for: .normal)
    }
    func wrapData(){
        
    }
    
    @IBAction func btnPassportShowAction(_ sender: Any) {
            if let del = delegate {
                del.showCountryList()
            }
    }

    
    @IBAction func btnNRCShowAction(_ sender: Any) {
     //   if let del = delegate {
            delegate.showStateDivision()
      //  }
    }
}
