//
//  BusinessCategoryRRCell.swift
//  OK
//
//  Created by Imac on 6/1/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol BusinessCategoryRRCellDelegate {
    func navigateToBusinessCategory()
}
class BusinessCategoryRRCell: UITableViewCell {
    var delegate: BusinessCategoryRRCellDelegate?
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet var lblBusinessCategory: MarqueeLabel!{
        didSet{
            lblBusinessCategory.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    var isNextViewHidden = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
    lblBusinessCategory.text = "Select Business Category".localized
    }
    func updateLaguageCell() {
        if lblBusinessCategory.text == "Select Business Category".localized{
          lblBusinessCategory.text = "Select Business Category".localized
        }else {

        }
        
        
    }
    
    @IBAction func btnBusinessCatAction(_ sender: Any) {
        
        if let del = delegate {
            del.navigateToBusinessCategory()
        }
    }

}
