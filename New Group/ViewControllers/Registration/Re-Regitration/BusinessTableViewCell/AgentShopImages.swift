//
//  AgentShopImages.swift
//  OK
//
//  Created by Imac on 6/8/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
protocol AgentShopImagesDelegate {
    func navigateToAgentImages(tag: Int)
}

class AgentShopImages: UITableViewCell {
    var delegate: AgentShopImagesDelegate?
    @IBOutlet weak var lblAttach: MarqueeLabel!{
        didSet{
            lblAttach.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgFront: UIImageView!
    @IBOutlet weak var lblFront: UILabel!{
        didSet{
            lblFront.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var lblBack: UILabel!{
        didSet{
            lblBack.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    var isNextViewHidden = true
    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        lblAttach.isUserInteractionEnabled = true
        lblAttach.addGestureRecognizer(tap)
    }

    func updateLaguageCell() {
        lblAttach.text = "Attach Agent Photo".localized
        lblFront.text = "First Image".localized
        lblBack.text = "Second Image".localized
    }
    func wrapData(){
        
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        if let del = delegate {
            del.navigateToAgentImages(tag: 50)
        }
    }
    
    @IBAction func onClickImageAction(_ sender: UIButton){
        if let del = delegate {
            del.navigateToAgentImages(tag: sender.tag)
        }
    }
}
