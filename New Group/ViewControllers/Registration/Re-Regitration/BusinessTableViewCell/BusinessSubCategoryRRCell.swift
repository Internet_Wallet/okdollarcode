//
//  BusinessSubCategoryRRCell.swift
//  OK
//
//  Created by Imac on 6/1/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
protocol BusinessSubCategoryRRCellDelegate {
    func navigateToBusinessSubCategory()
}

class BusinessSubCategoryRRCell: UITableViewCell {
    var delegate: BusinessSubCategoryRRCellDelegate?
    @IBOutlet weak var lblSubCatImg: UILabel!{
        didSet{
            lblSubCatImg.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var lblSubBusinessCategory: MarqueeLabel!{
        didSet{
            lblSubBusinessCategory.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgSubCategory: UIImageView!
    var isNextViewHidden = true
    override func awakeFromNib() {
        super.awakeFromNib()
        lblSubBusinessCategory.text = "Select Business Sub-Category".localized
    }

    func updateLaguageCell() {
        if lblSubBusinessCategory.text == "Select Business Sub-Category".localized{
            lblSubBusinessCategory.text = "Select Business Sub-Category".localized
        }else {
            
        }
    }
    @IBAction func btnBusinessSubCatAction(_ sender: Any) {
        if let del = delegate {
            del.navigateToBusinessSubCategory()
        }
    }
}
