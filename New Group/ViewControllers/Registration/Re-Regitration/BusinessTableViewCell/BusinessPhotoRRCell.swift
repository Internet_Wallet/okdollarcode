//
//  BusinessPhotoRRCell.swift
//  OK
//
//  Created by Imac on 6/2/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
protocol BusinessPhotoRRCellDelegate {
    func navigateToTakeBussinesImages(tag : Int)
}

class BusinessPhotoRRCell: UITableViewCell {
    var delegate: BusinessPhotoRRCellDelegate?
    @IBOutlet weak var lblAttach: MarqueeLabel!{
        didSet{
            lblAttach.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgFront: UIImageView!
    @IBOutlet weak var lblFront: UILabel!{
        didSet{
            lblFront.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var lblBack: UILabel!{
        didSet{
            lblBack.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    var isNextViewHidden = true
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        lblAttach.isUserInteractionEnabled = true
        lblAttach.addGestureRecognizer(tap)
    }

    func updateLaguageCell() {
        lblAttach.text = "Attach Business Registration".localized
        lblFront.text = "Front Side".localized
        lblBack.text = "Back Side".localized
    }
    func wrapData(){
        
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        if let del = delegate {
            del.navigateToTakeBussinesImages(tag: 50)
        }
    }
    
    @IBAction func onClickImageAction(_ sender: UIButton) {
        if let del = delegate {
            del.navigateToTakeBussinesImages(tag: sender.tag)
        }
    }
}
