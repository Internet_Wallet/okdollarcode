//
//  BusinessHeaderRRCell.swift
//  OK
//
//  Created by Imac on 6/1/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
protocol BusinessHeaderRRCellDelegate {
        func resetBusiness()
    func showBusinessInfo()
}

class BusinessHeaderRRCell: UITableViewCell {
    var delegate: BusinessHeaderRRCellDelegate?
    @IBOutlet weak var btnReset: UIButton!{
        didSet{
            btnReset.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var lblHeader: UILabel!{
        didSet{
            lblHeader.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func updateLaguageCell() {
        lblHeader.attributedText = lblHeader.attributedStringFinal(str1: "Business Details".localized)
        if ReRegistrationModel.shared.Language.lowercased() == "en" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue"), for: .normal)
        }else if ok_default_language == "my" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
        }else {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
        }
    }
    
    @IBAction func onClickInfoAction(_ sender: Any) {
        if let del = delegate {
            del.showBusinessInfo()
        }
    }
    
    @IBAction func onClickResetAction(_ sender: Any) {
        if let del = delegate {
            del.resetBusiness()
        }
    }
}

