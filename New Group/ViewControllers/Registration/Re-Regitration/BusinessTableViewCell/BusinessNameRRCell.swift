//
//  BusinessNameRRCell.swift
//  OK
//
//  Created by Imac on 6/1/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol BusinessNameRRCellDelegate {
    func showCategory()
}
class BusinessNameRRCell: UITableViewCell,UITextFieldDelegate {
    var delegate: BusinessNameRRCellDelegate?
    @IBOutlet weak var tfBusinessName: UITextField!{
        didSet{
            tfBusinessName.font = UIFont(name: appFont, size: 18.0)
        }
    }
    var isNextviewHidden = true
    var BUSINESSNAMECHARSET = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tfBusinessName.delegate = self
        
    }
    func updateLaguageCell() {
        var font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
            font = UIFont.systemFont(ofSize: 18)
        }
        tfBusinessName.font = font
        
        
        if ReRegistrationModel.shared.Language.lowercased() == "en" {
            BUSINESSNAMECHARSET = BUSINESSNAME_CHAR_SET_En
            tfBusinessName.font = UIFont.systemFont(ofSize: 18)
        }else if ReRegistrationModel.shared.Language.lowercased() == "my" {
            BUSINESSNAMECHARSET = BUSINESSNAME_CHAR_SET_My
             tfBusinessName.font = UIFont(name: appFont, size: 18)
        }else{
            BUSINESSNAMECHARSET = BUSINESSNAME_CHAR_SET_Uni
    
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfBusinessName.attributedPlaceholder = NSAttributedString(string: "Enter Business Name", attributes:attributes as [NSAttributedString.Key : Any])
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfBusinessName.attributedPlaceholder = NSAttributedString(string: "Enter Business Name", attributes:attributes)
                tfBusinessName.font = UIFont.systemFont(ofSize: 18)
            }
            
            
        }
           tfBusinessName.placeholder = "Enter Business Name".localized
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        ReRegistrationModel.shared.BusinessName = tfBusinessName.text!
        removeSpaceAtLast(txtField: tfBusinessName)
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: BUSINESSNAMECHARSET).inverted).joined(separator: "")) { return false }
        if range.location == 0 && string == " " {
            return false
        }
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if text.count > 2 {
            if isNextviewHidden{
                isNextviewHidden = false
                if let del = delegate {
                    del.showCategory()
                }
            }
        }
        if text.count > 50 {
            return false
        }
        if restrictMultipleSpaces(str: string, textField: tfBusinessName) {
            return true
        }else {
            if  textField.text?.last == " " {
                textField.text?.removeLast()
            }
            return false
        }
    }
    
    private func removeSpaceAtLast(txtField : UITextField){
        if txtField.text?.last == " " {
            txtField.text?.removeLast()
        }
    }
}
