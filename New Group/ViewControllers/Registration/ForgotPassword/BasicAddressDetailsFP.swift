//
//  BasicAddressDetailsFP.swift
//  DemoUpdateProfile
//
//  Created by SHUBH on 11/14/17.
//  Copyright © 2017 SHUBH. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import  Rabbit_Swift

protocol BasicAddressDetailsFPDelegate : class {
    func updateMainUIUI()
    func showAddressDivisionVC()
    func showAddressTownshipVC(locDetail :LocationDetail)
    func showSecurityVC()
    func hideScurityType()
    func showSubmit()
}


class BasicAddressDetailsFP:
OKBaseController,UITextFieldDelegate,StateDivisionVCDelegate,AddressTownshiopVCDelegate{
    
    var locationDetails: LocationDetail?
    var townShipDetail : TownShipDetail?
    var townshipCode = ""
    var timer = Timer()
    var seconds = 0
  //  var isGooglAPIStart = false
    var addressArray = [addressModel]()
    var isVillageTableCreated =  false
    
    @IBOutlet weak var viewTypeHere: UIView!
    @IBOutlet weak var viewDivisionState: UIView!
    @IBOutlet weak var viewTownship: UIView!
    @IBOutlet weak var viewCity: UIView!
    @IBOutlet weak var viewVillage: UIView!
    @IBOutlet weak var viewStreet: UIView!
    
    @IBOutlet weak var btnHome: UIButton!{
        didSet{
            btnHome.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnOther: UIButton!{
        didSet{
            btnOther.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnBusiness: UIButton!{
        didSet{
            btnBusiness.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var tfStreetName: UITextField!{
        didSet{
            tfStreetName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var tfVillageName: UITextField!{
        didSet{
            tfVillageName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var btnDivison: UIButton!{
        didSet{
            btnDivison.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnTownship: UIButton!{
        didSet{
            btnTownship.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnCity: UIButton!{
        didSet{
            btnCity.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var lblHome: UILabel!{
        didSet{
            lblHome.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblBusiness: UILabel!{
        didSet{
            lblBusiness.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblOther: UILabel!{
        didSet{
            lblOther.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblDivision: MarqueeLabel!{
        didSet{
            lblDivision.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblTownship: MarqueeLabel!{
        didSet{
            lblTownship.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var imgBusiness: UIImageView!
    @IBOutlet weak var imgOther: UIImageView!
    
    @IBOutlet weak var topContraintsTypehere: NSLayoutConstraint!
    @IBOutlet weak var tfTypeHere: UITextField!{
        didSet{
            tfTypeHere.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblHeader: UILabel!{
        didSet{
            lblHeader.font = UIFont(name: appFont, size: appFontSize)
        }
    }

    @IBOutlet weak var topStreetConstraints: NSLayoutConstraint!
    @IBOutlet weak var btnReset: UIButton!{
        didSet{
            btnReset.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    
    
    var DicTownship : Dictionary<String,String>?
    weak var delegate : BasicAddressDetailsFPDelegate?
    
    var isStreetTableShown  =  Bool()
    var isVillageTableShown  =  Bool()
    var myTableView = UITableView()
    var arrFilterStreetData : [Dictionary<String,String>] = []
    var arrFilterVillageData : [Dictionary<String,String>] = []
    var isSearch  = false
    var kbHeight : CGFloat?
    var isNextViewHidden = true
    var otherAddress = false
    var STREETSET = ""
    var currentLocationVC : CurrentLocationVC?
     var fnt = UIFont(name: appFont, size: 18.0)
    var showaddress = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
            font = UIFont(name: appFont, size: 18.0)//UIFont.systemFont(ofSize: 18)
        }
        tfVillageName.font = font
        tfStreetName.font = font
        tfTypeHere.font = font
        
        if ok_default_language == "en" {
             btnReset.setBackgroundImage(UIImage(named: "reset_blue"), for: .normal)
            STREETSET = STREETNAME_CHAR_SET_En
            fnt = UIFont.systemFont(ofSize: 18)
        }else if ok_default_language == "my" {
             btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
            STREETSET =  STREETNAME_CHAR_SET_My
            fnt = UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18)
        }else {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
             STREETSET = STREETNAME_CHAR_SET_Uni
            fnt = UIFont.systemFont(ofSize: 18)
            
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfVillageName.attributedPlaceholder = NSAttributedString(string: "Village Tract", attributes:attributes as [NSAttributedString.Key : Any])
                
                let attributes1 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfStreetName.attributedPlaceholder = NSAttributedString(string: "Enter street", attributes:attributes1 as [NSAttributedString.Key : Any])
                
                let attributes2 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfTypeHere.attributedPlaceholder = NSAttributedString(string: "Enter here", attributes:attributes2 as [NSAttributedString.Key : Any])
                
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfVillageName.attributedPlaceholder = NSAttributedString(string: "Village Tract", attributes:attributes)
                tfVillageName.font = UIFont.systemFont(ofSize: 18)
                
                let attributes1 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfStreetName.attributedPlaceholder = NSAttributedString(string: "Enter street", attributes:attributes1)
                tfStreetName.font = UIFont.systemFont(ofSize: 18)
                
                let attributes2 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfTypeHere.attributedPlaceholder = NSAttributedString(string: "Enter here", attributes:attributes2)
                tfTypeHere.font = UIFont.systemFont(ofSize: 18)
            }
            
        }
        
        IQKeyboardManager.sharedManager().enable = false
        self.viewTypeHere.isHidden = true
        tfVillageName.delegate = self
        tfStreetName.delegate = self
        self.topContraintsTypehere.constant = -57
        viewTypeHere.isHidden = true
        self.dHeight = 137.0
        imgHome.image = UIImage(named: "select_radio")
        ForgotPasswordModel.share.AddressDetailsRadioSelect = "Home".localized
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        lblHeader.text = "Address Details".localized
        tfTypeHere.placeholder = "Enter here".localized
        lblHome.text = "Home".localized
        lblBusiness.text = "Business".localized
        lblOther.text = "Other".localized
        lblDivision.text = "Select State / Division".localized
        lblTownship.text = "Select Township".localized
        btnCity.setTitle("City/Region".localized, for: .normal)
        tfVillageName.placeholder = "Village Tract".localized
        tfStreetName.placeholder = "Enter street".localized
    }
    
    // Mark: Keyboard Height
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            kbHeight = keyboardHeight
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    @IBAction func btnResetAddressDetailsAction(_ sender: Any) {
        lblDivision.text = "Select State / Division".localized
        lblTownship.text = "Select Township".localized
        btnCity.setTitle("City/Region".localized, for: .normal)
        tfVillageName.text = ""
        tfStreetName.text = ""
        tfTypeHere.text = ""
        ForgotPasswordModel.share.DivisionCode = ""
        ForgotPasswordModel.share.TownshipCode = ""
        ForgotPasswordModel.share.CityName = ""
        ForgotPasswordModel.share.CityName = ""
        ForgotPasswordModel.share.Village = ""
        ForgotPasswordModel.share.Street = ""
        
        self.isNextViewHidden = true
        UserDefaults.standard.set("NO", forKey: "SecurityQshown")
        self.view.endEditing(true)
        
        if showaddress == ""{
            
        }else{
            showaddress = ""
            resignFirstResponder()
            if let del = delegate {
                del.hideScurityType()
            }
        }
        
       
    }
    
    @IBAction func btnHomeAction(_ sender: Any) {
        
        if !(imgHome.image == UIImage(named: "select_radio")) {
            ForgotPasswordModel.share.AddressDetailsRadioSelect = lblHome.text!
            imgHome.image = UIImage(named: "select_radio")
            imgBusiness.image = UIImage(named: "r_radio_btn")
            imgOther.image = UIImage(named: "r_radio_btn")
            self.topContraintsTypehere.constant = -57
            viewTypeHere.isHidden = true
            if otherAddress {
                self.dHeight = self.dHeight! - 57
                runGuard()
                otherAddress = false
            }
            if self.dHeight! <= viewDivisionState.frame.origin.y + viewDivisionState.frame.height {
                self.dHeight = viewDivisionState.frame.origin.y + viewDivisionState.frame.height
                runGuard()
            }
        }
    }
    
    @IBAction func btnBusinessAction(_ sender: Any) {
        if !(imgBusiness.image == UIImage(named: "select_radio")) {
            ForgotPasswordModel.share.AddressDetailsRadioSelect = lblBusiness.text!
            imgHome.image = UIImage(named: "r_radio_btn")
            imgBusiness.image = UIImage(named: "select_radio")
            imgOther.image = UIImage(named: "r_radio_btn")
            self.topContraintsTypehere.constant = -57
            viewTypeHere.isHidden = true
            if otherAddress {
                self.dHeight = self.dHeight! - 57
                runGuard()
                otherAddress = false
            }
            if self.dHeight! <= viewDivisionState.frame.origin.y + viewDivisionState.frame.height {
                self.dHeight = viewDivisionState.frame.origin.y + viewDivisionState.frame.height
                runGuard()
            }
        }
    }
    
    @IBAction func btnOtherAction(_ sender: Any) {
        if !(imgOther.image == UIImage(named: "select_radio")) {
            ForgotPasswordModel.share.AddressDetailsRadioSelect = lblOther.text!
            imgHome.image = UIImage(named: "r_radio_btn")
            imgBusiness.image = UIImage(named: "r_radio_btn")
            imgOther.image = UIImage(named: "select_radio")
            self.topContraintsTypehere.constant = 0
            viewTypeHere.isHidden = false
            tfTypeHere.becomeFirstResponder()
            otherAddress = true
            if self.dHeight! <= viewTypeHere.frame.origin.y + viewTypeHere.frame.height {
                self.dHeight = viewDivisionState.frame.origin.y + viewDivisionState.frame.height
                runGuard()
            }else if self.dHeight! <= viewDivisionState.frame.origin.y + viewDivisionState.frame.height {
                self.dHeight = viewTownship.frame.origin.y + viewTownship.frame.height
                runGuard()
            }else if self.dHeight! <= viewTownship.frame.origin.y + viewTownship.frame.height {
                self.dHeight = viewCity.frame.origin.y + viewCity.frame.height
                runGuard()
            }else if self.dHeight! <= viewStreet.frame.origin.y + viewStreet.frame.height {
                self.dHeight = viewStreet.frame.origin.y + viewStreet.frame.height + 57
                runGuard()
            }
        }
    }
    
    @IBAction func btnSelectDivisionStateAction(_ sender: Any) {
        guard (self.delegate?.showAddressDivisionVC() != nil) else {
            return
        }
        tfVillageName.text = ""
        tfStreetName.text = ""
        ForgotPasswordModel.share.Street = ""
    }
    
    @IBAction func btnSelectTownshipAction(_ sender: Any) {
        if !(lblDivision.text == "Select State / Division".localized){
            guard (self.delegate?.showAddressTownshipVC(locDetail: locationDetails!) != nil) else {
                return
            }
        }else {
            showAlert(alertTitle: "", alertBody: "Please Select Division/State".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
        }
    }
    func setTownshipCityNameFromDivison(Dic: TownShipDetailForAddress) {
        self.setTownShip(dic: Dic)
        tfStreetName.becomeFirstResponder()
    }
    
    func setTownshipCityName(cityDic: TownShipDetailForAddress) {
        btnCity.setTitle("", for: UIControl.State.normal)
        self.setTownShip(dic: cityDic)
        tfStreetName.becomeFirstResponder()
    }
    
    private func setTownShip(dic : TownShipDetailForAddress) {
        tfVillageName.text = ""
        tfStreetName.text = ""
        ForgotPasswordModel.share.Village = ""
        
        viewTownship.isHidden = false
        viewCity.isHidden = false
        viewVillage.isHidden = false
        viewStreet.isHidden = false
        townshipCode = dic.townShipCode
        ForgotPasswordModel.share.TownshipCode = dic.townShipCode
        
        if ok_default_language == "my" {
            lblTownship.text = dic.townShipNameMY
            if dic.GroupName == "" {
                btnCity.setTitle(dic.cityNameMY, for: UIControl.State.normal)
                ForgotPasswordModel.share.CityName = dic.cityNameMY
            }else {
                if dic.isDefaultCity == "1" {
                    btnCity.setTitle(dic.DefaultCityNameMY, for: UIControl.State.normal)
                    ForgotPasswordModel.share.CityName = dic.DefaultCityNameMY
                }else{
                    btnCity.setTitle(dic.cityNameMY, for: UIControl.State.normal)
                    ForgotPasswordModel.share.CityName = dic.cityNameMY
                }
            }
        }else if ok_default_language == "en"{
            lblTownship.text = dic.townShipNameEN
            if dic.GroupName == "" {
                btnCity.setTitle(dic.cityNameEN, for: UIControl.State.normal)
                ForgotPasswordModel.share.CityName = dic.cityNameEN
            }else {
                if dic.isDefaultCity == "1" {
                    btnCity.setTitle(dic.DefaultCityNameEN, for: UIControl.State.normal)
                    ForgotPasswordModel.share.CityName = dic.DefaultCityNameEN
                }else {
                    btnCity.setTitle(dic.cityNameEN, for: UIControl.State.normal)
                    ForgotPasswordModel.share.CityName = dic.cityNameEN
                }
            }
        }else {
            lblTownship.text = dic.townShipNameUni
            if dic.GroupName == "" {
                btnCity.setTitle(dic.cityNameUni, for: UIControl.State.normal)
                ForgotPasswordModel.share.CityName = dic.cityNameUni
            }else {
                if dic.isDefaultCity == "1" {
                    btnCity.setTitle(dic.DefaultCityNameUni, for: UIControl.State.normal)
                    ForgotPasswordModel.share.CityName = dic.DefaultCityNameUni
                }else{
                    btnCity.setTitle(dic.cityNameUni, for: UIControl.State.normal)
                    ForgotPasswordModel.share.CityName = dic.cityNameUni
                }
            }
        }
        
        var isVillageFieldHidden = false
        if dic.GroupName.contains(find: "YCDC") || dic.GroupName.contains(find: "MCDC"){
            isVillageFieldHidden = true
        }
        
        if dic.cityNameEN == "Amarapura" || dic.cityNameEN == "Patheingyi" || dic.townShipNameEN == "PatheinGyi" {
            isVillageFieldHidden = false
        }
        
        DispatchQueue.main.async {
//            if isVillageFieldHidden {
//                self.topStreetConstraints.constant = -57
//            }else {
//                self.topStreetConstraints.constant = 0
//            }
//            if self.dHeight! <= self.viewStreet.frame.origin.y + self.viewStreet.frame.height {
//                self.dHeight = self.viewStreet.frame.origin.y + self.viewStreet.frame.height
//            }else {
//                self.dHeight = self.dHeight
//            }
            
            
            if isVillageFieldHidden {
                self.tfVillageName.isHidden = true
                self.topStreetConstraints.constant = -57
                if self.imgOther.image == UIImage(named: "select_radio") {
                    self.dHeight = 365.0
                }else {
                    self.dHeight = 308.0
                }
            }else {
                self.tfVillageName.isHidden = false
                self.topStreetConstraints.constant = 0
                if self.imgOther.image == UIImage(named: "select_radio") {
                    self.dHeight = 422.0
                }else {
                    self.dHeight = 365.0
                }
            }
             self.runGuard()
        }
        self.callStreetVillageAPI(townshipcode: dic.townShipCode)
    }
    
    
    
    
    func setDivisionStateName(SelectedDic: LocationDetail) {
        viewTownship.isHidden = false
        ForgotPasswordModel.share.DivisionCode = SelectedDic.stateOrDivitionCode
        locationDetails = SelectedDic
        if ok_default_language == "my"{
            lblDivision.text = SelectedDic.stateOrDivitionNameMy
        }else if ok_default_language == "en"{
            lblDivision.text = SelectedDic.stateOrDivitionNameEn
        }else {
            lblDivision.text = SelectedDic.stateOrDivitionNameUni
        }
        
        if self.dHeight! <= viewTownship.frame.origin.y + viewTownship.frame.height{
            self.dHeight = viewTownship.frame.origin.y + viewTownship.frame.height
            runGuard()
        }
    }
    
    func callStreetVillageAPI(townshipcode : String){
        let urlStr   = Url.URL_VillageList + townshipcode
        let url = getUrl(urlStr: urlStr, serverType: .serverApp)
        let params = Dictionary<String,String>()
        TopupWeb.genericClass(url:url, param: params as AnyObject, httpMethod: "GET", handle: {response, isSuccess in
            if isSuccess {
                if let json = response as? Dictionary<String,Any> {
                    let response = json["Data"] as Any
                    let dic = OKBaseController.convertToDictionary(text: response as! String)
                    if let d = dic {
                        if let arrDic = d["Streets"] as? Array<Dictionary<String,String>> {
                            let streetList = StreetList()
                            streetList.isStreetList = true
                            streetList.streetArray = arrDic
                            VillageManager.shared.allStreetList = streetList
                            
                        }else{
                            let streetList = StreetList()
                            streetList.streetArray.removeAll()
                            streetList.isStreetList = false
                            VillageManager.shared.allStreetList = streetList
                        }
                        if let arrDic1 = d["Villages"] as? Array<Dictionary<String,String>>{
                            DispatchQueue.main.async {
                                let villageList = VillageList()
                                villageList.villageArray = arrDic1
                                VillageManager.shared.allVillageList = villageList
                                villageList.isVillageList = true
                                self.tfVillageName.isUserInteractionEnabled = true
                            }
                        }else{
                            DispatchQueue.main.async {
                                let villageList = VillageList()
                                villageList.villageArray.removeAll()
                                VillageManager.shared.allVillageList = villageList
                                villageList.isVillageList = false
                                self.tfVillageName.isUserInteractionEnabled = false
                            }
                        }
                        println_debug(VillageManager.shared.allVillageList)
                    }
                }
            }else {
                self.showAlert(alertTitle: "", alertBody: "Network error and try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
            }
        })
    }
    
    func showViewForVillage(tableviewTag : Int){
        if !isVillageTableCreated {
            myTableView = UITableView(frame: CGRect(x:10, y: 64, width: self.view.frame.width - 20, height: UIScreen.main.bounds.height - kbHeight! - 180))
            myTableView.layer.cornerRadius = 5
            myTableView.tableFooterView = UIView(frame: .zero)
            myTableView.register(UINib(nibName: "StreetCell", bundle: nil), forCellReuseIdentifier: "StreetCell")
            myTableView.dataSource = self
            myTableView.delegate = self
            myTableView.allowsMultipleSelection = false
            myTableView.tableFooterView = UIView.init(frame: CGRect.zero)
            myTableView.backgroundColor = UIColor.clear
            if let window = UIApplication.shared.keyWindow {
                window.addSubview(myTableView)
                window.makeKeyAndVisible()
            }
            isVillageTableCreated = true
        }
        myTableView.tag = tableviewTag
    }
    
    func getAllAddressDetails(searchTextStr : String) {
        var urlString = ""
        if ok_default_language == "my" || ok_default_language == "uni"{
        urlString = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(searchTextStr)&components=country:mm&language=my&key=\(googleAPIKey)"
        } else {
        urlString = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(searchTextStr)&components=country:mm&language=en&key=\(googleAPIKey)"
        }
        
        let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        let session = URLSession.shared
        if url != nil {
            let task = session.dataTask(with:url!) { (data, response, error) -> Void in
                if let data = data {
                    do {
                        let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                        if let dic = dict as? Dictionary<String,AnyObject> {
                            if let resultArray = dic["predictions"] as? [Dictionary<String,Any>] {
                                if self.addressArray.count > 0 {
                                self.addressArray.removeAll()
                                }
                                println_debug(resultArray)
                                DispatchQueue.main.async {
                                    for dic in resultArray {
                                        self.addressArray.append(addressModel.init(dict: dic))
                                    }
                                    if self.addressArray.count > 0 {
                                        DispatchQueue.main.async {
                                            if !self.isVillageTableCreated {
                                                self.myTableView.isHidden = false
                                                self.showViewForVillage(tableviewTag: 2)
                                                self.myTableView.allowsMultipleSelection = false
                                                self.myTableView.reloadData()
                                            }else {
                                                self.myTableView.isHidden = false
                                                self.myTableView.allowsMultipleSelection = false
                                                self.myTableView.reloadData()
                                            }
                                        }
                                    }else {
                                        if self.isVillageTableCreated {
                                            DispatchQueue.main.async {
                                                self.myTableView.isHidden = true
                                                self.myTableView.allowsMultipleSelection = true
                                            }
                                        }
                                    }
                                    self.myTableView.tag = 2
                                }
                            }
                        }
                    } catch let error as NSError {
                        println_debug(error.localizedDescription)
                    }
                }else {
                    println_debug("API NOT CALLING")
                }
            }
            task.resume()
        }
    }
    
    
    
    @IBAction func btnCityAction(_ sender: Any) {
        
    }
    
    @IBAction func streetDidEditChanged(_ sender: UITextField) {
        if tfStreetName.text?.count ?? 0 > 0 {
            getAllAddressDetails(searchTextStr: tfStreetName.text!)
        }
    }
    
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == tfVillageName {
            if !(lblDivision.text == "Select State / Division".localized) {
                if !(lblTownship.text == "Select Township".localized) {
                    return true
                }else { return false }
            }else { return false }
        }else if textField == tfStreetName {
         //   isGooglAPIStart = true
            if !(lblDivision.text == "Select State / Division".localized) {
                if !(lblTownship.text == "Select Township".localized) {
                    return true
                }else { return false }
            }else { return false }
        }
        return true
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == tfStreetName {
            //isGooglAPIStart = false
            self.myTableView.allowsMultipleSelection = true
            if isVillageTableCreated {
                myTableView.isHidden = true
            }
            if tfStreetName.text!.count > 2 {
             let questionB = VillageManager.shared.data.securityQ["QuestionBurmese"] as? String
             let question = VillageManager.shared.data.securityQ["Question"] as? String
                if (questionB != "" || question != "") && (questionB != nil || question != nil){
                    if UserDefaults.standard.string(forKey: "SecurityQshown") == "NO"  {
                         ValidateReverifyRequestWS()
                    }
                }else {
                    DispatchQueue.main.async {
                    self.prepareModel()
                    UserDefaults.standard.set("YES", forKey: "SecurityQshown")
                        if let del = self.delegate {
                        del.showSubmit()
                        }
                    }
                }
            }
            // self.prepareModel()
            //  guard(self.delegate?.showSecurityVC() != nil)else {return false}
        }else if textField == tfTypeHere {
            ForgotPasswordModel.share.AddressOtherDetails = tfTypeHere.text!
        }else if textField == tfVillageName {
            if isVillageTableCreated {
                myTableView.isHidden = true
                self.myTableView.allowsMultipleSelection = true
            }
        }
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: STREETSET).inverted).joined(separator: "")) { return false }
        
        if textField == tfStreetName {
            if text.count > 30 {
                return false
            }
        }else if textField == tfVillageName{
          tfStreetName.text = ""
        ForgotPasswordModel.share.Street = ""
        }else if textField == tfTypeHere{}
        if text.count > 50 { return false }
        if restrictMultipleSpaces(str: string, textField: tfTypeHere) {
            return true
        }else {
            if  textField.text?.last == " " {
                textField.text?.removeLast()
            }
            return false
        }
    }
    
    @IBAction func tfVillageEditChanged(_ sender: UITextField) {
        if (sender.text?.count)! > 0 {
            self.getSearchArrayVillageContains(sender.text!)
        }else{
            myTableView.isHidden = true
            myTableView.allowsMultipleSelection = true
            isSearch = false
        }
    }
    
    
    func ValidateReverifyRequestWS() {
        var urlStr   = Url.URL_ValidateReverifyForgotPassword + "MobileNumber=" + ForgotPasswordModel.share.MobileNumber + "&Name=" + ForgotPasswordModel.share.Name + "&FatherName=" + ForgotPasswordModel.share.FatherName  + "&Dob=" + ForgotPasswordModel.share.Dob
        
        urlStr = urlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let urlS = getUrl(urlStr: urlStr, serverType: .serverApp)
        println_debug(urlS)
        let param = [String:String]() as AnyObject
        
        TopupWeb.genericClass(url: urlS, param: param, httpMethod: "GET", handle: { (response , success) in
            if success {
                let dic = response as? Dictionary<String , Any>
                println_debug(dic!)
                if dic!["Code"] as! NSInteger == 200 {
                    if self.isNextViewHidden{
                        self.isNextViewHidden = false
                        self.showaddress = "Show"
                        DispatchQueue.main.async {
                            self.prepareModel()
                            UserDefaults.standard.set("YES", forKey: "SecurityQshown")
                            guard(self.delegate?.showSecurityVC() != nil)else {
                                return
                            }
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        self.tfStreetName.text = ""
                        ForgotPasswordModel.share.Street = ""
                        self.showAlert(alertTitle: "", alertBody:"You Entered Incorrect Details".localized, alertImage: #imageLiteral(resourceName: "error"))
                        UserDefaults.standard.set("NO", forKey: "SecurityQshown")
                    }
                }
            }else {
                DispatchQueue.main.async {
                    self.tfStreetName.text = ""
                    ForgotPasswordModel.share.Street = ""
                    self.showAlert(alertTitle: "", alertBody: "Network error Please try again".localized, alertImage: #imageLiteral(resourceName: "error"))
                }
            }
        })
    }
    
    func getSearchArrayVillageContains(_ text : String) {
        //BurmeseName
        let village = VillageManager.shared.allVillageList
        if ok_default_language == "my" {
            let predicate : NSPredicate = NSPredicate(format: "BurmeseName CONTAINS[c] %@", text.lowercased())
            arrFilterVillageData = (village.villageArray as NSArray).filtered(using: predicate) as! [Dictionary<String, String>]
        }else if ok_default_language == "en"{
            let predicate : NSPredicate = NSPredicate(format: "self.Name contains[c] %@", text.lowercased())
            arrFilterVillageData.removeAll()
            arrFilterVillageData = (village.villageArray as NSArray).filtered(using: predicate) as! [Dictionary<String, String>]
        }else {
            let predicate : NSPredicate = NSPredicate(format: "BurmeseName CONTAINS[c] %@", text.lowercased())
            arrFilterVillageData = (village.villageArray as NSArray).filtered(using: predicate) as! [Dictionary<String, String>]
        }
        
        if arrFilterVillageData.count > 0 {
            isSearch = true
            if !isVillageTableCreated {
                showViewForVillage(tableviewTag: 1)
            }else {
                myTableView.isHidden = false
                myTableView.allowsMultipleSelection = false
            }
            self.myTableView.reloadData()
        }else{
            if isVillageTableCreated {
                myTableView.isHidden = true
                myTableView.allowsMultipleSelection = true
                isSearch = false
            }
        }
        myTableView.tag = 1
    }
    
    func showViewForVillage(){
        myTableView = UITableView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width, height:self.view.frame.height - kbHeight!))
        myTableView.tableFooterView = UIView(frame: .zero)
        myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        myTableView.dataSource = self
        myTableView.delegate = self
        myTableView.tag = 1
        if let window = UIApplication.shared.keyWindow {
            window.addSubview(myTableView)
            window.makeKeyAndVisible()
        }
        myTableView.reloadData()
    }
    
    func prepareModel() {
        ForgotPasswordModel.share.Village = parabaik.uni(toZawgyi: tfVillageName.text!)
        ForgotPasswordModel.share.Street = parabaik.uni(toZawgyi: tfStreetName.text!)
    }
    
    func setVillageName(villageName : String) {
        tfVillageName.text = villageName
        RegistrationModel.shared.VillageName = villageName
    }
    
    func runGuard() {
        guard(self.delegate?.updateMainUIUI() != nil) else {
            return
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
}

extension BasicAddressDetailsFP : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView.tag == 1 {
            let village = VillageManager.shared.allVillageList
            if isSearch {
                return arrFilterVillageData.count
            }else{
                return village.villageArray.count
            }
        }else if tableView.tag == 2 {
            return addressArray.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : StreetCell = tableView.dequeueReusableCell(withIdentifier: "StreetCell") as! StreetCell
        
        cell.backgroundColor = UIColor(red: 236, green: 236, blue: 236, alpha: 1.0)
        if tableView.tag == 1 {
            cell.lblFullAdress.isHidden = true
            cell.lblStreetName.isHidden = true
            cell.lblVillage.isHidden = false
            if isSearch {
                let dic = arrFilterVillageData[indexPath.row]
                if ok_default_language == "my" {
                    cell.lblVillage.text = dic["BurmeseName"]
                }else if ok_default_language == "en"{
                    cell.lblVillage.text = dic["Name"]
                }else {
                   cell.lblVillage.text = dic["BurmeseName"]
                }
            }else {
                let village = VillageManager.shared.allVillageList
                let dic = village.villageArray[indexPath.row]
                if ok_default_language == "my" {
                    cell.lblVillage.text = dic["BurmeseName"]
                }else if ok_default_language == "en"{
                    cell.lblVillage.text = dic["Name"]
                }else {
                   cell.lblVillage.text = dic["BurmeseName"]
                }
            }
        }else {
            cell.lblFullAdress.isHidden = false
            cell.lblStreetName.isHidden = false
            cell.lblVillage.isHidden = true
         //   let dic = addressArray[indexPath.row]
            
            if let obj = addressArray[safe: indexPath.row] {
                cell.lblStreetName.text = parabaik.uni(toZawgyi: obj.shortName)
                cell.lblFullAdress.text = parabaik.uni(toZawgyi: obj.address)
            }
//            if let sname = dic.shortName {
//                cell.lblStreetName.text = parabaik.uni(toZawgyi:sname)
//            }
//            if let fadd = dic.address {
//                cell.lblFullAdress.text = parabaik.uni(toZawgyi:fadd)
//            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if tableView.tag == 1 {
            if isSearch {
                let dic = arrFilterVillageData[indexPath.row]
                if ok_default_language == "my" {
                    tfVillageName.text = dic["BurmeseName"]
                    if let vName = dic["BurmeseName"] {
                        setVillageName(villageName:vName)
                    }
                }else if ok_default_language == "en" {
                    tfVillageName.text = dic["Name"]
                    if let vName = dic["Name"] {
                        setVillageName(villageName:vName )
                    }
                }else {
                    tfVillageName.text = dic["BurmeseName"]
                    if let vName = dic["BurmeseName"] {
                        setVillageName(villageName:vName)
                    }
                }
                isSearch = false
            }else {
                let street = VillageManager.shared.allVillageList
                let dic = street.villageArray[indexPath.row]
                if ok_default_language == "my" {
                    tfVillageName.text = dic["BurmeseName"]
                    if let vName = dic["BurmeseName"] {
                        setVillageName(villageName:vName )
                    }
                }else if ok_default_language == "en"{
                    tfVillageName.text = dic["Name"]
                    if let vName = dic["Name"] {
                        setVillageName(villageName:vName )
                    }
                }else {
                    tfVillageName.text = dic["BurmeseName"]
                    if let vName = dic["BurmeseName"] {
                        setVillageName(villageName:vName )
                    }
                }
            }
            tfVillageName.resignFirstResponder()
        }else{
            let dic = addressArray[indexPath.row]
            tfStreetName.text = Rabbit.uni2zg(dic.shortName ?? "")
            tfStreetName.resignFirstResponder()
           // isGooglAPIStart = false
            addressArray.removeAll()
            UserDefaults.standard.set(false, forKey: "shown_to_tfstreet")
        }
        self.myTableView.allowsMultipleSelection = true
        myTableView.isHidden = true
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 1{
            return 50
        }else {
            return 60
        }
    }
}

