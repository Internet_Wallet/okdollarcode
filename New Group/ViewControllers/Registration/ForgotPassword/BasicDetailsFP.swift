//
//  BasicDetailsVC.swift
//  DemoUpdateProfile
//
//  Created by SHUBH on 11/14/17.
//  Copyright © 2017 SHUBH. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

protocol BasicDetailsFPDelegate : class{
    func updateMainUIUI()
    func showAddress()
    func showTownShip()
    func resetIDProofDetails()
    func showCountry()
    func hideAllViewIDTYPE(controller : BasicDetailsFP,isString : Bool)
    func showNRCTypeOption()
}

class BasicDetailsFP: OKBaseController,UITextFieldDelegate,TownShipVcDeletage,CountryViewControllerDelegate,NRCTypesVCDelegate,TownshipVC2Delegate,MyTextFieldDelegate{
   
    @IBOutlet weak var viewSelectIDType: UIView!
    
    @IBOutlet weak var viewNRC: UIView!
    @IBOutlet weak var viewNRCButton: UIView!
    @IBOutlet weak var viewNRCTF: UIView!
    @IBOutlet weak var tfNRCNo: MyTextField!{
        didSet{
            tfNRCNo.font = UIFont(name: appFont, size: 18.0)
        }
    }
    @IBOutlet weak var btnNRCNo: UIButton!{
        didSet{
            btnNRCNo.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    @IBOutlet weak var viewSelectNRCType: UIView!
    @IBOutlet weak var viewPassport: UIView!
    @IBOutlet weak var viewCountry: UIView!
    @IBOutlet weak var viewPassportNo: UIView!
    @IBOutlet weak var viewPassportExpiry: UIView!
    
    @IBOutlet weak var imgCountry: UIImageView!
    @IBOutlet weak var btnContry: UIButton!{
        didSet{
            btnContry.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    @IBOutlet weak var tfPassportNo: UITextField!{
        didSet{
            tfPassportNo.font = UIFont(name: appFont, size: 18.0)
        }
    }
    @IBOutlet weak var tfPasspotExpiryDate: UITextField!{
        didSet{
            tfPasspotExpiryDate.font = UIFont(name: appFont, size: 18.0)
        }
    }
    
    @IBOutlet weak var viewDOB_FN: UIView!
    @IBOutlet weak var viewDOB: UIView!
    @IBOutlet weak var view_FN: UIView!
    
    @IBOutlet weak var tfDOB: UITextField!{
        didSet{
            tfDOB.font = UIFont(name: appFont, size: 18.0)
        }
    }
    @IBOutlet weak var tfFatherName: UITextField!{
        didSet{
            tfFatherName.font = UIFont(name: appFont, size: 18.0)
        }
    }
    
    @IBOutlet weak var viewFatherPrefix: UIView!
    @IBOutlet weak var topConstraintsDOBFN: NSLayoutConstraint!
    
    
    @IBOutlet weak var lblHeader: MarqueeLabel!{
        didSet{
            lblHeader.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnFU: UIButton!{
        didSet{
            btnFU.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnFMr: UIButton!{
        didSet{
            btnFMr.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnFDr: UIButton!{
        didSet{
            btnFDr.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var viewBFatherInitial: UIView!
    @IBOutlet weak var btnBFatherFirst: UIButton!{
        didSet{
            btnBFatherFirst.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnBFatherSecond: UIButton!{
        didSet{
            btnBFatherSecond.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    @IBOutlet weak var btnEnterNRC: UIButton!{
        didSet{
            btnEnterNRC.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnOR: UIButton!{
        didSet{
            btnOR.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnEnterPassport: UIButton!{
        didSet{
            btnEnterPassport.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    @IBOutlet weak var btnClearFatherName: UIButton!{
        didSet{
            btnClearFatherName.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    @IBOutlet weak var lblNRCDetails: MarqueeLabel!{
        didSet{
            lblNRCDetails.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblPassportDetails: MarqueeLabel!{
        didSet{
            lblPassportDetails.font = UIFont(name: appFont, size: appFontSize)
        }
    }
     @IBOutlet weak var btnReset: UIButton!{
        didSet{
            btnReset.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    
   weak var delegate : BasicDetailsFPDelegate?
    var isFatherNameKeyboardHidden = false
    
    let btnnrc = UIButton()
    var strDOB = ""
    var strExpiryDate = ""
    var countryCode = ""
    var mobilNO = ""
    
    var isNextViewHidden = true
    var fatherPrefix = ""
    let dateFormatter = DateFormatter()
        var locationDetails: LocationDetail?
    var NRCSET = ""
    var NAMESET = ""
    var fnt = UIFont(name: appFont, size: 18.0)//UIFont.systemFont(ofSize: 18)
    override func viewDidLoad() {
        super.viewDidLoad()
       // IQKeyboardManager.sharedManager().enable = false
        
        var font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
            font =  UIFont(name: appFont, size: 18.0)//UIFont.systemFont(ofSize: 18)
        }
        tfFatherName.font = font
        tfDOB.font = font
        tfPassportNo.font = font
        tfPasspotExpiryDate.font = font
        
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        DispatchQueue.main.async {
            self.removeProgressView()
        }
        
        if ok_default_language == "en" {
            tfFatherName.font = UIFont.systemFont(ofSize: 18)
            tfDOB.font = UIFont.systemFont(ofSize: 18)
            tfPassportNo.font = UIFont.systemFont(ofSize: 18)
            tfPasspotExpiryDate.font = UIFont.systemFont(ofSize: 18)
            btnReset.setBackgroundImage(UIImage(named: "reset_blue"), for: .normal)
            NAMESET = NAME_CHAR_SET_En
           // btnnrc.titleLabel?.font = UIFont.systemFont(ofSize: 18.0)
        }else if ok_default_language == "my" {
            fnt = UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18)
            tfFatherName.font = UIFont(name: appFont, size: 18)
            tfDOB.font = UIFont(name: appFont, size: 18)
            tfPassportNo.font = UIFont(name: appFont, size: 18)
            tfPasspotExpiryDate.font = UIFont(name: appFont, size: 18)
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
            NAMESET =  NAME_CHAR_SET_My
           // btnnrc.titleLabel?.font = UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18.0)
        }else {
            NAMESET = NAME_CHAR_SET_Uni
            //btnnrc.titleLabel?.font = UIFont.systemFont(ofSize: 18.0)
            
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfFatherName.attributedPlaceholder = NSAttributedString(string: "Enter Father Name", attributes:attributes as [NSAttributedString.Key : Any])
                
                let attributes1 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfPassportNo.attributedPlaceholder = NSAttributedString(string: "Enter Passport Number", attributes:attributes1 as [NSAttributedString.Key : Any])
                
                let attributes2 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                   .font : UIFont(name: appFont, size: 18)]
                tfPasspotExpiryDate.attributedPlaceholder = NSAttributedString(string:  "Enter Passport Expiry Date", attributes:attributes2 as [NSAttributedString.Key : Any])
                
                let attributes3 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                   .font : UIFont(name: appFont, size: 18)]
                tfDOB.attributedPlaceholder = NSAttributedString(string: "Select Date of Birth", attributes:attributes3 as [NSAttributedString.Key : Any])
                
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfFatherName.attributedPlaceholder = NSAttributedString(string: "Enter Father Name", attributes:attributes)
                tfFatherName.font = UIFont.systemFont(ofSize: 18)
                
                let attributes1 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfPassportNo.attributedPlaceholder = NSAttributedString(string: "Enter Passport Number", attributes:attributes1)
                tfPassportNo.font = UIFont.systemFont(ofSize: 18)
                
                let attributes2 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                   .font : UIFont.systemFont(ofSize: 18)]
                tfPasspotExpiryDate.attributedPlaceholder = NSAttributedString(string: "Enter Passport Expiry Date", attributes:attributes2)
                tfPasspotExpiryDate.font = UIFont.systemFont(ofSize: 18)
                
                let attributes3 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                   .font : UIFont.systemFont(ofSize: 18)]
                tfDOB.attributedPlaceholder = NSAttributedString(string: "Select Date of Birth", attributes:attributes3)
                tfDOB.font = UIFont.systemFont(ofSize: 18)
            }

        }
        
         btnnrc.titleLabel?.font = UIFont(name: appFont, size: appButtonSize) ?? UIFont.systemFont(ofSize: 18.0)
          btnnrc.titleLabel?.font =  fnt
         NRCSET = "1234567890"

        viewSelectIDType.isHidden = false
        viewSelectNRCType.isHidden = false
        btnClearFatherName.isHidden = true
        tfNRCNo.delegate = self
        tfNRCNo.myDelegate = self
        tfDOB.delegate = self
        tfFatherName.delegate = self
        tfPassportNo.delegate = self
        tfPasspotExpiryDate.delegate = self
        self.view.needsUpdateConstraints()
  
        let screenSize: CGRect = UIScreen.main.bounds
        let viewDOBKeyboard = UIView(frame: CGRect(x: 0, y: 0, width:screenSize.width , height: 40))
        viewDOBKeyboard.backgroundColor = UIColor(red: 244/255, green: 199/255, blue: 22/255, alpha: 1)
        
        let viewExpiryDateKeyboard = UIView(frame: CGRect(x: 0, y: 0, width:screenSize.width , height: 40))
        viewExpiryDateKeyboard.backgroundColor = UIColor(red: 244/255, green: 199/255, blue: 22/255, alpha: 1)
        
        let btnCancel  = UIButton(frame: CGRect(x: 10, y: 0, width:100, height: 40))
        btnCancel.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        btnCancel.setTitle("Cancel", for: UIControl.State.normal)
        btnCancel.addTarget(self, action:#selector(btnCancelDOBAction), for: UIControl.Event.touchUpInside)
        btnCancel.addTarget(self, action:#selector(btnCancelPassportExpiryAction), for: UIControl.Event.touchUpInside)
        viewDOBKeyboard.addSubview(btnCancel)
        viewExpiryDateKeyboard.addSubview(btnCancel)
        
        let btnDone  = UIButton(frame: CGRect(x: screenSize.width - 110, y: 0, width:100, height: 40))
        btnDone.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        btnDone.setTitle("Done", for: UIControl.State.normal)
        btnDone.addTarget(self, action:#selector(btnDoneDOBAction), for: UIControl.Event.touchUpInside)
        btnDone.addTarget(self, action:#selector(btnDonePassportExpiryAction), for: UIControl.Event.touchUpInside)
        
        viewDOBKeyboard.addSubview(btnDone)
        viewExpiryDateKeyboard.addSubview(btnDone)
        tfDOB.inputAccessoryView = viewDOBKeyboard
        tfPasspotExpiryDate.inputAccessoryView = viewExpiryDateKeyboard
        self.dHeight = 165.0
        
        self.tfNRCNo.addTarget(self, action: #selector(BasicDetailsFP.textFieldDidChange(_:)),
                               for: UIControl.Event.editingChanged)

        self.btnFU.setTitle("U".localized, for: .normal)
        self.btnFMr.setTitle("Mr".localized, for: .normal)
        self.btnFDr.setTitle("Dr".localized, for: .normal)
        self.btnBFatherFirst.setTitle("U".localized, for: .normal)
        self.btnBFatherSecond.setTitle("Dr".localized, for: .normal)
        self.tfFatherName.placeholder = "Enter Father Name".localized
        self.lblHeader.text = "Select ID Type".localized
        self.lblNRCDetails.text = "Enter NRC Details".localized
        self.lblPassportDetails.text = "Enter Passport Details".localized
        self.btnOR.setTitle("(OR)".localized, for: .normal)
        btnNRCNo.setTitle("Enter NRC Number".localized, for: .normal)
        
        btnContry.setTitle("Select Country of Citizenship".localized, for: .normal)
        tfPassportNo.placeholder = "Enter Passport Number".localized
        tfPasspotExpiryDate.placeholder = "Enter Passport Expiry Date".localized
        self.tfDOB.placeholder = "Select Date of Birth".localized

        viewBFatherInitial.isHidden = true
        viewBFatherInitial.isHidden = true

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    func textFieldDidDelete() {
        if tfNRCNo.text?.count == 0 {
            self.showNRCSuggestion()
        }
    }
    
    @IBAction func btnSelectPassportAction(_ sender: Any) {
        
        self.dHeight = viewCountry.frame.height + viewCountry.frame.origin.y + viewPassport.frame.origin.y
        runGuard()
        
        viewSelectNRCType.isHidden = true
        viewPassport.isHidden = false
        SingletonClass.sharedInstance.setStaticValuesForKey(keyStirng: "SELECT_ID_TYPE", valueString: "PASSPORT")
        ForgotPasswordModel.share.IdType = "04"
    }

    
    
    @IBAction func btnSelectNRCDetailsAction(_ sender: Any) {
       
        guard (self.delegate?.showTownShip() != nil) else {
            return
        }
    }
    
    func setDivison(division: LocationDetail) {
        var divStr = ""
        if ok_default_language == "my"{
            divStr = division.nrcCodeNumberMy + "/"
        }else if ok_default_language == "en"{
            divStr = division.nrcCodeNumber + "/"
        }else {
            divStr = division.nrcCodeNumberUni + "/"
        }
      //  btnNRCNo.setTitle(divStr, for: .normal)
          locationDetails = division
                self.dHeight = viewNRCTF.frame.height + viewNRCTF.frame.origin.y + 40
                runGuard()
                viewSelectNRCType.isHidden = true
                viewNRC.isHidden = false
                SingletonClass.sharedInstance.setStaticValuesForKey(keyStirng: "SELECT_ID_TYPE", valueString: "NRC_NO")
                ForgotPasswordModel.share.IdType = "01"
    }
    
    @IBAction func btnNRCNoAction(_ sender: Any) {
        let sb = UIStoryboard(name: "Registration", bundle: nil)
        let townshipVC2 = sb.instantiateViewController(withIdentifier: "TownshipVC2") as! TownshipVC2
        townshipVC2.selectedDivState = locationDetails
        townshipVC2.townshipVC2Delegate = self
        self.navigationController?.pushViewController(townshipVC2, animated: true)
    }
    
    @IBAction func btnSelectCountryAction(_ sender: Any) {
        guard (self.delegate?.showCountry() != nil) else {
            return
        }
    }
    
    @IBAction func btnSelectFatherPrefixAction(_ sender: UIButton) {
        viewFatherPrefix.isHidden = true
        self.setPrefixName(prefixName: sender.title(for: .normal)!, txtField: tfFatherName)
    }
    
    @IBAction func btnBFatherInitialAction(_ sender: UIButton) {
        viewBFatherInitial.isHidden = true
        self.setPrefixName(prefixName: sender.title(for: .normal)!, txtField: tfFatherName)
    }
    func setPrefixName(prefixName : String, txtField : UITextField) {
        isFatherNameKeyboardHidden = true
        fatherPrefix = prefixName
        let myString = prefixName
        let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: fnt ])
        let lblName = UILabel()
        lblName.frame = CGRect(x: 0, y: 3.0, width:stringSize.width + 10 , height: 52)
        lblName.textAlignment = .left
        lblName.font = fnt
        lblName.text = myString
        let btnView = UIView()
        btnView.frame = CGRect(x: 0, y: 0, width: lblName.frame.width , height: 52)
        btnView.addSubview(lblName)
        txtField.leftView = btnView
        txtField.leftViewMode = UITextField.ViewMode.always
        txtField.becomeFirstResponder()
        txtField.textAlignment = .left
        
    }
    
    @IBAction func btnBasicDetailsResetAction(_ sender: Any) {
         ForgotPasswordModel.share.IdType = ""
         ForgotPasswordModel.share.Nrc = ""
         ForgotPasswordModel.share.PassportCountry = ""
         ForgotPasswordModel.share.PassportNumber = ""
         ForgotPasswordModel.share.PassportExpiryDate = ""
         guard (self.delegate?.resetIDProofDetails() != nil) else {
            return
        }
    }
    
    @IBAction func btnClearFatherNameAction(_ sender: Any) {
        tfFatherName.leftView = nil
        tfFatherName.text = ""
        tfFatherName.resignFirstResponder()
        isFatherNameKeyboardHidden = false
        btnClearFatherName.isHidden = true
        tfFatherName.textAlignment = .center
        
        if UserDefaults.standard.string(forKey: "SecurityQshown") == "YES" {
            if let del = delegate {
                isNextViewHidden = true
                del.hideAllViewIDTYPE(controller: self, isString: false)
            }
        }
    }
    
    
    func NRCValueString(nrcSting : String) {
        self.setNRCDetails(nrcDetails: nrcSting)
    }
    
    private func setNRCDetails(nrcDetails : String) {
        
        
        if nrcDetails == ""{
            ForgotPasswordModel.share.IdType = ""
            ForgotPasswordModel.share.Nrc = ""
            ForgotPasswordModel.share.PassportCountry = ""
            ForgotPasswordModel.share.PassportNumber = ""
            ForgotPasswordModel.share.PassportExpiryDate = ""
            guard (self.delegate?.resetIDProofDetails() != nil) else {
               return
           }
        }else{
            self.viewNRCButton.isHidden = true
            self.viewNRCTF.isHidden = false
            
            let myString = nrcDetails
            let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: fnt])
            btnnrc.frame = CGRect(x: 0, y: -2, width:stringSize.width + 4, height: 52)
            btnnrc.setTitleColor(.black, for: .normal)
            btnnrc.titleLabel?.font =  fnt
            btnnrc.addTarget(self, action: #selector(BtnNrcTypeAction(button:)), for: .touchUpInside)
            btnnrc.setTitle(nrcDetails, for: UIControl.State.normal)
            let btnView = UIView()
            btnView.frame = CGRect(x: 0, y: 0, width: btnnrc.frame.width , height: 52)
            btnView.addSubview(btnnrc)
            tfNRCNo.leftView = btnView
            tfNRCNo.leftViewMode = UITextField.ViewMode.always
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.tfNRCNo.becomeFirstResponder()
            }
        }
        
       
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField == tfFatherName{
            ForgotPasswordModel.share.FatherName = parabaik.uni(toZawgyi: fatherPrefix + "," + tfFatherName.text!.trimmingCharacters(in: .whitespacesAndNewlines))
            if tfFatherName.text!.count > 2{
                if isNextViewHidden {
                    isNextViewHidden = false
                    guard(self.delegate?.showAddress() != nil)else{
                        return false
                    }
                }
            }
            
        }else if textField == tfPassportNo{
                ForgotPasswordModel.share.PassportNumber = tfPassportNo.text!
             tfPassportNo.resignFirstResponder()
        }else if textField == tfNRCNo {
            if tfNRCNo.text == "000000" {
                tfNRCNo.text = ""
                return false
            }
        }
        return true
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == tfDOB{
            
            let date = Calendar.current.date(byAdding: .year, value: -12, to: Date())
            let datePicker = UIDatePicker()
            datePicker.tag = 1
            datePicker.datePickerMode = .date
            datePicker.backgroundColor = UIColor.white
            datePicker.calendar = Calendar(identifier: .gregorian)
            if #available(iOS 13.4, *) {
                           datePicker.preferredDatePickerStyle = .wheels
                       } else {
                           // Fallback on earlier versions
                       }
            if tfDOB.text?.count == 0 {
                datePicker.date = date!
            }else{
                datePicker.date = dateFormatter.date(from: tfDOB.text!)!
            }
            datePicker.maximumDate = date!
            datePicker.addTarget(self, action: #selector(datePickerChanged(sender:)), for: .valueChanged)
            textField.inputView = datePicker
            strDOB = dateFormatter.string(from: datePicker.date)
            let screenSize: CGRect = UIScreen.main.bounds
            let viewDOBKeyboard = UIView(frame: CGRect(x: 0, y: 0, width:screenSize.width , height: 40))
            viewDOBKeyboard.backgroundColor = UIColor(red: 244/255, green: 199/255, blue: 22/255, alpha: 1)
            
            let btnCancel  = UIButton(frame: CGRect(x: 10, y: 0, width:100, height: 40))
            btnCancel.titleLabel?.font = fnt
            btnCancel.setTitle("Cancel".localized, for: UIControl.State.normal)
            
            btnCancel.addTarget(self, action:#selector(btnCancelDOBAction), for: UIControl.Event.touchUpInside)
            viewDOBKeyboard.addSubview(btnCancel)
            
            let btnDone  = UIButton(frame: CGRect(x: screenSize.width - 110, y: 0, width:100, height: 40))
            btnDone.titleLabel?.font = fnt
            btnDone.setTitle("Done".localized, for: UIControl.State.normal)
            btnDone.addTarget(self, action:#selector(btnDoneDOBAction), for: UIControl.Event.touchUpInside)
            
            viewDOBKeyboard.addSubview(btnDone)
            tfDOB.inputAccessoryView = viewDOBKeyboard
            
        }else if textField == tfFatherName{
            if isFatherNameKeyboardHidden {
                viewFatherPrefix.isHidden = true
                viewBFatherInitial.isHidden = true
                return true
            }else{
                if ok_default_language == "my" || ok_default_language == "uni"{
                   viewBFatherInitial.isHidden = false
                }else {
                    viewFatherPrefix.isHidden = false
                }
                return false
            }
        }else if textField == tfPasspotExpiryDate{
            let date = Date()
            let datePicker = UIDatePicker()
            datePicker.tag = 2
            datePicker.datePickerMode = .date
            datePicker.backgroundColor = UIColor.white
            datePicker.calendar = Calendar(identifier: .gregorian)
            if #available(iOS 13.4, *) {
                           datePicker.preferredDatePickerStyle = .wheels
                       } else {
                           // Fallback on earlier versions
                       }
            datePicker.minimumDate = Date(timeInterval: (24 * 60 * 60), since: date)
            let maxDate = Calendar.current.date(byAdding: .year, value: 100, to: Date())
            datePicker.maximumDate = maxDate
            
            if tfPasspotExpiryDate.text?.count == 0 {
                datePicker.date = Date(timeInterval: (24 * 60 * 60), since: date)
            }else{
                datePicker.date = dateFormatter.date(from: tfPasspotExpiryDate.text!)!
            }
            datePicker.addTarget(self, action: #selector(datePickerChanged(sender:)), for: .valueChanged)
            textField.inputView = datePicker
            strExpiryDate = dateFormatter.string(from: datePicker.date)
            let screenSize: CGRect = UIScreen.main.bounds
            let viewExpiryDateKeyboard = UIView(frame: CGRect(x: 0, y: 0, width:screenSize.width , height: 40))
            viewExpiryDateKeyboard.backgroundColor = UIColor(red: 244/255, green: 199/255, blue: 22/255, alpha: 1)
            
            let btnCancel  = UIButton(frame: CGRect(x: 10, y: 0, width:100, height: 40))
            btnCancel.setTitle("Cancel".localized, for: UIControl.State.normal)
            btnCancel.addTarget(self, action:#selector(btnCancelPassportExpiryAction), for: UIControl.Event.touchUpInside)
            viewExpiryDateKeyboard.addSubview(btnCancel)
            
            let btnDone  = UIButton(frame: CGRect(x: screenSize.width - 110, y: 0, width:100, height: 40))
            btnDone.setTitle("Done".localized, for: UIControl.State.normal)
            btnDone.addTarget(self, action:#selector(btnDonePassportExpiryAction), for: UIControl.Event.touchUpInside)
            
            viewExpiryDateKeyboard.addSubview(btnDone)
            tfPasspotExpiryDate.inputAccessoryView = viewExpiryDateKeyboard
        }
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)

        if textField == tfNRCNo {
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: NRCSET).inverted).joined(separator: "")) { return false }
            
            if text == "000000"{
               // tfNRCNo.text = ""
                tfNRCNo.text?.removeLast()
                tfNRCNo.becomeFirstResponder()
                return false
            }
            
            if text.count > 6 {
                tfNRCNo.resignFirstResponder()
                return false
            }
        }else if textField == tfPassportNo {
               if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: PASSWORD_CHAR_SET).inverted).joined(separator: "")) { return false }
            if text.count > 14 {
                return false
            }
            if text.count > 4{
                if isNextViewHidden{
                  if self.dHeight! <= viewPassportExpiry.frame.origin.y + viewPassportExpiry.frame.height + 40.0 {
                    self.dHeight = self.viewPassportExpiry.frame.origin.y + viewPassportExpiry.frame.height + 40.0
                        self.runGuard()
                    }
                }
            }
        }else if textField == tfFatherName {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: NAMESET).inverted).joined(separator: "")) { return false }
            if text.count > 2 {
                if isNextViewHidden {
                self.dHeight = view_FN.frame.origin.y + view_FN.frame.height + viewDOB_FN.frame.origin.y
                    runGuard()
                }
            }
            if UserDefaults.standard.string(forKey: "SecurityQshown") == "YES" {
                if let del = delegate {
                    isNextViewHidden = true
                    del.hideAllViewIDTYPE(controller: self, isString: true)
                }
            }
            
            if text.count > 0 {
                btnClearFatherName.isHidden = false
            }else{
                btnClearFatherName.isHidden = true
            }
            if text.count > 50 { return false }
            if restrictMultipleSpaces(str: string, textField: tfFatherName) {
                return true
            }else {
                if  textField.text?.last == " " {
                    textField.text?.removeLast()
                }
                return false
            }
        }
        return true
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        if tfNRCNo.text!.count >= 6 {
            if isNextViewHidden {
                topConstraintsDOBFN.constant = -114
                tfDOB.becomeFirstResponder()
                self.dHeight = 154.0 //viewDOB.frame.height + viewDOB_FN.frame.origin.y
                runGuard()
            }
         tfNRCNo.resignFirstResponder()
        let helpingFuntion = HelpingFunctionR()
        ForgotPasswordModel.share.Nrc = helpingFuntion.charecterReplace(charecter: "/", replacingWith: "@", mainString: (btnnrc.titleLabel?.text)! + tfNRCNo.text!)
        }
    }
    
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func setDivisionAndTownship(strDivison_township : String){
        self.setNRCDetails(nrcDetails: strDivison_township)
    }
    
    @objc func BtnNrcTypeAction(button : UIButton) {
        self.showNRCSuggestion()
    }
    
    func setNrcTypeName(nrcType : String) {
        btnnrc.setTitle(nrcType, for: .normal)
        let myString = nrcType
        var fnt = UIFont.systemFont(ofSize: 18)
        if ok_default_language == "my" {
            fnt = UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18.0)
        }
        
        let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: fnt])
        btnnrc.frame = CGRect(x: 0, y: -2, width:stringSize.width + 4, height: 52)
        btnnrc.setTitle(nrcType, for: .normal)
        let btnView = UIView()
        btnView.frame = CGRect(x: 0, y: 0, width: btnnrc.frame.width , height: 52)
        btnView.addSubview(btnnrc)
        tfNRCNo.leftView = nil
        tfNRCNo.leftView = btnView
        tfNRCNo.leftViewMode = UITextField.ViewMode.always

    }
    func countryViewController(_ list: CountryViewController, country: Country) {
        btnContry.setTitle(country.name,for: .normal)
        ForgotPasswordModel.share.PassportCountry = country.name
        btnContry.setTitleColor(UIColor.black, for: .normal)
        imgCountry.image = UIImage(named: country.code)
        //viewPassportNo.isHidden = false
        tfPassportNo.text = ""
        tfPassportNo.becomeFirstResponder()
        if self.dHeight! <= viewPassportNo.frame.origin.y + viewPassportNo.frame.height + 40.0 {
            self.dHeight = viewPassportNo.frame.origin.y + viewPassportNo.frame.height + 40.0
            runGuard()
        }
        list.dismiss(animated: false, completion: nil)
    }
    
    func countryViewControllerCloseAction(_ list: CountryViewController) {
        list.dismiss(animated: false, completion: nil)
    }
    
    @objc func datePickerChanged(sender: UIDatePicker) {
        
        sender.calendar = Calendar(identifier: .gregorian)
        if sender.tag == 1 {
            strDOB = dateFormatter.string(from: sender.date)
        }else{
            strExpiryDate = dateFormatter.string(from: sender.date)
        }
    }
    
    @objc func btnCancelDOBAction () {
        tfDOB.resignFirstResponder()
    }
    
    @objc func btnCancelPassportExpiryAction() {
        tfPasspotExpiryDate.resignFirstResponder()
    }
    
    @objc func btnDoneDOBAction () {
        tfDOB.resignFirstResponder()
        tfDOB.text = strDOB
        ForgotPasswordModel.share.Dob = tfDOB.text!
       //tfFatherName.becomeFirstResponder()
        if UserDefaults.standard.string(forKey: "SecurityQshown") == "YES" {
            if let del = delegate {
                isNextViewHidden = true
                 del.hideAllViewIDTYPE(controller: self, isString: false)
            }
        }
        
        self.dHeight = view_FN.frame.origin.y + view_FN.frame.height + viewDOB_FN.frame.origin.y
        runGuard()
    }
    
    @objc func btnDonePassportExpiryAction() {
        tfPasspotExpiryDate.resignFirstResponder()
        tfPasspotExpiryDate.text = strExpiryDate
        topConstraintsDOBFN.constant = 0
        self.dHeight = viewDOB.frame.height + viewDOB_FN.frame.origin.y
        runGuard()  
        ForgotPasswordModel.share.PassportExpiryDate = tfPasspotExpiryDate.text!
    }
    
    private func showNRCSuggestion(){
         tfNRCNo.resignFirstResponder()
        if let del = delegate {
            del.showNRCTypeOption()
        }
    }
    
    func runGuard() {
        guard(self.delegate?.updateMainUIUI() != nil) else {
            return
        }
    }
}

