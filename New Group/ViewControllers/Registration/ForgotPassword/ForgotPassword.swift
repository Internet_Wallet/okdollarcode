//
//  ForgotPassword.swift
//  DemoUpdateProfile
//
//  Created by SHUBH on 11/11/17.
//  Copyright © 2017 SHUBH. All rights reserved.
//

import UIKit

class ForgotPassword: OKBaseController,UITableViewDelegate,UITableViewDataSource,UserNameVCDelegate,BasicDetailsFPDelegate,BasicAddressDetailsFPDelegate,BasicSecurityQFPDelegate,HiddenCameraDelegate,WebServiceResponseDelegate,SubmitFPDelegate{

    var userNameVC : UserNameVC?
    var basicDetailsFP = BasicDetailsFP()
    var basicAddressDetailsFP = BasicAddressDetailsFP()
    var basicSecurityQFP = BasicSecurityQFP()
    var submitFP = SubmitFP()
     @IBOutlet weak var trainlingConstraints: NSLayoutConstraint!
    let tranView = UIView()
    @IBOutlet var btnLanguageFirst: UIButton!{
        didSet{
            btnLanguageFirst.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet var viewLanguage: UIView!
    @IBOutlet var btnLanguageSecond: UIButton!{
        didSet{
            btnLanguageSecond.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    var lang : laguageFlag?
    let lblMarque = MarqueeLabel()
    
    // class for take photo and send to server in background
     var vcPhoto : GenericPhotoClass?
    
    var controllers = [UIViewController]()
   
    @IBOutlet weak var tableViewForgetPass: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.removeProgressView()
        }
        if device.type == .iPhone6plus || device.type == .iPhone6Splus || device.type == .iPhone7plus || device.type == .iPhone8plus {
            trainlingConstraints.constant = 20
        }
         viewLanguage.isHidden = true
        
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: 18)
        lblMarque.text = "Forgot Password".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
        
        UserDefaults.standard.set("NO", forKey: "SecurityQshown")
        
        if UserModel.shared.language != "" {
            ok_default_language = UserModel.shared.language
        } else {
            ok_default_language = userDef.value(forKey: "AppRegisteredLanguage") as? String ?? ""
        }
        let currentLanguage = appDel.getSelectedLanguage()
        if ok_default_language != "" {
            if ok_default_language != currentLanguage {
                UserDefaults.standard.set(true, forKey: "LanguageStatusChanged")
                UserDefaults.standard.set(currentLanguage, forKey: "PreviousLanguage")
                UserDefaults.standard.set(ok_default_language, forKey: "currentLanguage")
                appDel.setSeletedlocaLizationLanguage(language: ok_default_language)
                UserDefaults.standard.synchronize()
            }
        }
        println_debug("Registered Laguage:------> \(ok_default_language)")
        guard let userName = self.storyboard?.instantiateViewController(withIdentifier: "UserNameVC") as? UserNameVC else { return }
        userNameVC = userName
        userName.delegate = self
        controllers.append(userName)
        
        tableViewForgetPass.delegate = self
        tableViewForgetPass.dataSource = self
        self.tableViewForgetPass.reloadData()
        
        DispatchQueue.global(qos: .background).async {
            let vcPhoto = GenericPhotoClass()
            vcPhoto.delegate = self
            vcPhoto.cameraStart()
        }
     }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: 18)
        lblMarque.text = "Forgot Password".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white
        tableViewForgetPass.reloadData()
        self.navigationItem.titleView = lblMarque
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        alertViewObj.wrapAlert(title: "", body: "If you click back button, you have entered data will be automatically cleared and re-direct to OK $  user login screen.".localized, img:#imageLiteral(resourceName: "r_user"))
        alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
        })
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            if UserDefaults.standard.bool(forKey: "LanguageStatusChanged") {
                let perLaguage =  UserDefaults.standard.value(forKey: "PreviousLanguage") as! String
                UserDefaults.standard.set(perLaguage, forKey: "currentLanguage")
                appDel.setSeletedlocaLizationLanguage(language: perLaguage)
                UserDefaults.standard.set(false, forKey: "LanguageStatusChanged")
                UserDefaults.standard.synchronize()
            }
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowLoginBothViews"), object: nil)
            self.dismiss(animated: false, completion: nil)
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return controllers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let  cell = tableViewForgetPass.dequeueReusableCell(withIdentifier: "cell")
        let vc = controllers[indexPath.row]
        //addChildViewController(vc)
        vc.view.frame = cell!.bounds
        cell!.contentView.addSubview(vc.view)
        //vc.didMove(toParentViewController: self)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
       
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = controllers[indexPath.row].dHeight {
            println_debug(height)
            return height
        }
        return 0.00
    }
    
    func UICellUpdate() {
        UIView.setAnimationsEnabled(false)
        self.tableViewForgetPass.beginUpdates()
        self.tableViewForgetPass.layoutIfNeeded()
        self.tableViewForgetPass.layoutSubviews()
        self.tableViewForgetPass.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
    func updateMainUIUI() {
        self.UICellUpdate()
    }
    
    func showBasicDetailFPVC(controller: UserNameVC) {
        if !controllers.contains(basicDetailsFP){
            basicDetailsFP = (self.storyboard?.instantiateViewController(withIdentifier: "BasicDetailsFP") as? BasicDetailsFP)!
            basicDetailsFP.delegate = self
            controllers.append(basicDetailsFP)
            DispatchQueue.main.async {
                self.tableViewForgetPass.reloadData()
                
                if let cellAtFirst = self.tableViewForgetPass.visibleCells.first {
                    cellAtFirst.subviews.forEach({ (view) in
                        if view is UITextField {
                            view.becomeFirstResponder()
                        } else if let tf = controller.tfUserName {
                            tf.becomeFirstResponder()
                        }
                    })
                }
            }
        }
    }
    func showNRCTypeOption() {
        let sb = UIStoryboard(name: "Registration" , bundle: nil)
        let nRCTypesVC  = sb.instantiateViewController(withIdentifier: "NRCTypesVC") as! NRCTypesVC
        nRCTypesVC.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        nRCTypesVC.setName(nrcName: (basicDetailsFP.btnnrc.titleLabel?.text)!)
        nRCTypesVC.nRCTypesVCDelegate = basicDetailsFP
        nRCTypesVC.modalPresentationStyle = .overCurrentContext
        self.present(nRCTypesVC, animated: false, completion: nil)
    }
    
    func resetIDProofDetails(){
        controllers.remove(at: 1)
        basicDetailsFP = (self.storyboard?.instantiateViewController(withIdentifier: "BasicDetailsFP") as? BasicDetailsFP)!
        basicDetailsFP.delegate = self
        controllers.insert(basicDetailsFP, at: 1)
        DispatchQueue.main.async {
            self.tableViewForgetPass.reloadData()
        }
    }
    
    func showAddress(){
        if !controllers.contains(basicAddressDetailsFP){
            basicAddressDetailsFP = (self.storyboard?.instantiateViewController(withIdentifier: "BasicAddressDetailsFP") as? BasicAddressDetailsFP)!
            basicAddressDetailsFP.delegate = self
            controllers.append(basicAddressDetailsFP)
            DispatchQueue.main.async {
                self.tableViewForgetPass.reloadData()
            }
        }
    }

    func showAddressDivisionVC(){
        let sb = UIStoryboard(name: "Registration", bundle: nil)
        let stateDivisionVC  = sb.instantiateViewController(withIdentifier: "StateDivisionVC") as! StateDivisionVC
        stateDivisionVC.stateDivisonVCDelegate = basicAddressDetailsFP
        self.navigationController?.pushViewController(stateDivisionVC, animated: true)
    }
    
    func showAddressTownshipVC(locDetail :LocationDetail){
        let sb = UIStoryboard(name: "Registration", bundle: nil)
        let addressTownshiopVC  = sb.instantiateViewController(withIdentifier: "AddressTownshiopVC") as! AddressTownshiopVC
        addressTownshiopVC.addressTownshiopVCDelegate = basicAddressDetailsFP
        addressTownshiopVC.selectedDivState = locDetail
        self.navigationController?.pushViewController(addressTownshiopVC, animated: true)
    }
    

    
    func showSecurityVC() {
        if !controllers.contains(basicSecurityQFP){
            basicSecurityQFP = (self.storyboard?.instantiateViewController(withIdentifier: "BasicSecurityQFP") as? BasicSecurityQFP)!
            basicSecurityQFP.delegate = self
            controllers.append(basicSecurityQFP)
            DispatchQueue.main.async {
                self.tableViewForgetPass.reloadData()
            }
        }
    }
    
    func showSubmit() {
        if !controllers.contains(submitFP){
            submitFP = (self.storyboard?.instantiateViewController(withIdentifier: "SubmitFP") as? SubmitFP)!
            submitFP.delegate = self
            controllers.append(submitFP)
            DispatchQueue.main.async {
                self.tableViewForgetPass.reloadData()
            }
        }
    }
    
    func hideAllView(controller : UserNameVC) {
      
        DispatchQueue.main.async {
            self.controllers.removeLast()
            self.basicAddressDetailsFP.tfStreetName.text = ""
            self.basicAddressDetailsFP.isNextViewHidden = true
            self.tableViewForgetPass.reloadData()
            if let cellAtFirst = self.tableViewForgetPass.visibleCells.first {
                cellAtFirst.subviews.forEach({ (view) in
                    if view is UITextField {
                        view.becomeFirstResponder()
                    } else if let tf = controller.tfUserName {
                        tf.becomeFirstResponder()
                    }
                })
            }
              UserDefaults.standard.set("NO", forKey: "SecurityQshown")
            self.showAlert(alertTitle: "", alertBody: "You Entered Incorrect Details".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
        }
    }
    
    func hideAllViewIDTYPE(controller : BasicDetailsFP,isString : Bool) {
        DispatchQueue.main.async {
            self.controllers.removeLast()
            self.basicAddressDetailsFP.tfStreetName.text = ""
            self.basicAddressDetailsFP.isNextViewHidden = true
            self.tableViewForgetPass.reloadData()
            if isString {
                if let cellAtFirst = self.tableViewForgetPass.visibleCells.first {
                    cellAtFirst.subviews.forEach({ (view) in
                        if view is UITextField {
                            view.becomeFirstResponder()
                        } else if let tf = controller.tfFatherName {
                            tf.becomeFirstResponder()
                        }
                    })
                }
            }
        UserDefaults.standard.set("NO", forKey: "SecurityQshown")
        }
    }
    
    
    func hideScurityType() {
        DispatchQueue.main.async {
            self.resignFirstResponder()
            self.tableViewForgetPass.reloadData()
            self.controllers.removeLast()
            self.tableViewForgetPass.reloadData()
            UserDefaults.standard.set("NO", forKey: "SecurityQshown")
        }
    }

    
    //Basic Detail FP Delegate
    func showTownShip() {
        let sb = UIStoryboard(name: "Registration", bundle: nil)
        let townVC = sb.instantiateViewController(withIdentifier: "TownshipVC") as! TownshipVC
        townVC.townShipDelegate = basicDetailsFP
        self.navigationController?.pushViewController(townVC, animated: false)
    }
    func showCountry(){
        let sb = UIStoryboard(name: "Country", bundle: nil)
        let countryVC = sb.instantiateViewController(withIdentifier: "CountryViewController") as! CountryViewController
        countryVC.delegate = basicDetailsFP
        countryVC.modalPresentationStyle = .fullScreen
        self.navigationController?.present(countryVC, animated: false, completion: nil)
    }
    
    func navigaToPasswordResetVC() {
        DispatchQueue.main.async {
            let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordFP") as? ChangePasswordFP
             self.navigationController?.pushViewController(loginVC!, animated: true)
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
    }
    
    func navigaToSetSecurityQA() {
        DispatchQueue.main.async {
            let setSecurityQAFP = self.storyboard?.instantiateViewController(withIdentifier: "SetSecurityQAFP") as? SetSecurityQAFP
            self.navigationController?.pushViewController(setSecurityQAFP!, animated: true)
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
    }
    
    
    // Language Changes
    @IBAction func btnLanguageFirstAction(_ sender: Any) {
        viewLanguage.isHidden = !viewLanguage.isHidden
        if !(viewLanguage.isHidden) {
            addview()
            self.view.endEditing(true)
        }
    }
    
    @IBAction func btnLanguageSecondAction(_ sender: Any) {
            self.viewLanguage.isHidden = !self.viewLanguage.isHidden
            self.viewLanguage.isHidden = true
            if (self.btnLanguageSecond.currentImage as UIImage?) != nil{
                let image = self.btnLanguageFirst.currentImage
                let strTag = self.btnLanguageFirst.stringTag
                self.btnLanguageFirst.setImage(self.btnLanguageSecond.currentImage, for: UIControl.State.normal)
                self.btnLanguageSecond.setImage(image, for: UIControl.State.normal)
                self.btnLanguageFirst.stringTag = self.btnLanguageSecond.stringTag
                self.btnLanguageSecond.stringTag = strTag
                self.updateLanguage(languageF: self.btnLanguageFirst.stringTag!)
             
                self.changeLaguageOfApplication()
            }
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
    
    func updateLanguage(languageF : String) {
        
        var strLanguage = ""
        if languageF == "myanmarF"{
            strLanguage = "my"
        }else if languageF == "ukF"{
            strLanguage = "en"
        }
        
        ok_default_language = strLanguage
        ForgotPasswordModel.share.clearValues()
        UserDefaults.standard.set(strLanguage, forKey:"currentLanguage")
        appDel.setSeletedlocaLizationLanguage(language: strLanguage)
        viewLanguage.isHidden = true
        tranView.removeFromSuperview()
    }
    
    func changeLaguageOfApplication() {
        controllers.removeAll()
        if RegistrationModel.shared.Language == "my" {
            lblMarque.text = "OK$ အေကာင့္ မွတ္ပံုတင္ရန္"
        }else{
            lblMarque.text = "OK$ Registration"
        }
    }
    
    func addview() {
        // to Hide Language view on touch on screen
        tranView.frame = CGRect(x: 0, y: 64, width: self.view.frame.width, height: self.view.frame.height - 64)
        tranView.backgroundColor = UIColor.clear
        self.view.addSubview(tranView)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(nextStepTap))
        tapGestureRecognizer.numberOfTapsRequired = 1
        tranView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc public func nextStepTap(sender: UITapGestureRecognizer) {
        viewLanguage.isHidden = true
        tranView.removeFromSuperview()
    }
    
    
    
    
    // Generic Photo delegate
    
    func didCameraFinishedUpdate(success: Bool, images: [UIImage]) {
        var imageArry = [String]()
        var dic = Dictionary<String,Any>()
        if success {
            for image in images {
                let strBase64String = self.convertImageToBase64(image: image)
                imageArry.append(strBase64String)
            }
            dic = ["Base64String":imageArry,"MobileNumber": ForgotPasswordModel.share.MobileNumber]
            if appDelegate.checkNetworkAvail() {
                let web      = WebApiClass()
                web.delegate = self
                let urlStr   = Url.URL_GetImageurls
                let url = getUrl(urlStr: urlStr, serverType: .serverApp)
                println_debug(url)
                web.genericClass(url: url, param: dic as AnyObject, httpMethod: "POST", mScreen: "getImageUrl")
            }else{
                DispatchQueue.main.async {
                    self.showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))}
            }
        }
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        if screen == "getImageUrl"{
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        if dic["Code"] as? NSNumber == 200 {
                            let dicsend = ["MobileNumber":ForgotPasswordModel.share.MobileNumber,"Msid":msid,"SimId":uuid,"AppBuildNumber":"","AppBuildVersion":buildVersion,"AppType":0,"CellTowerId":"3037185","DeviceInfo":"DeviceInfo","Type":"Login","Latitude":"16.8166768","Longitude":"96.1319057","OsType":1,"ImageUrl":dic["Data"]!] as [String : Any]
                            if appDelegate.checkNetworkAvail() {
                                let web      = WebApiClass()
                                web.delegate = self
                                let urlStr   = Url.URL_AddUnauthorisedAccessLog
                                let url = getUrl(urlStr: urlStr, serverType: .serverApp)
                                web.genericClass(url: url, param: dicsend as AnyObject, httpMethod: "POST", mScreen: "AddUnauthorisedAccessLog")
                            }else{
                                DispatchQueue.main.async {
                                    self.showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))}
                            }
                        }
                    }
                }
            } catch {
                DispatchQueue.main.async {
                    self.showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))}
            }
        }else if screen == "AddUnauthorisedAccessLog"{
            
            if !(json is Data) {
                DispatchQueue.main.async {
                    self.showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))}
            }
            
//            do{
//                if json is Data {
//                   // println_debug(data)
//                }
//            }catch{
//              showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))
//            }
        }
    }
    
    

    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
  
 }

