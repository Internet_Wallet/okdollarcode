//
//  ForgotPasswordModel.swift
//  OK
//
//  Created by Subhash Arya on 11/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
     
class ForgotPasswordModel: NSObject {
    static let share = ForgotPasswordModel()
    
    var Name                         : String = ""
    
    var IdType                       : String = ""
    var Nrc                          : String = ""    //"14@AhMaNa(N)123456"
    var PassportCountry              : String = ""
    var PassportExpiryDate           : String = ""
    var PassportNumber               : String = ""

    var Dob                          : String = ""
    var FatherName                   : String = ""
    
    var AddressDetailsRadioSelect    : String = ""
    var AddressOtherDetails          : String = ""
    var State                        : String = "" //Blank
    var DivisionCode                 : String = ""
    var TownshipCode                 : String = ""
    var CityName                     : String = ""
    var Village                      : String = ""
    var Street                       : String = ""
    var HouseNumber                  : String = ""
    var SecurityAnswer               : String = ""
 
    var ResetUserImage               : String = ""
    var OsType                                = 1
    var MerchantNumber               : String = ""
    
    var S_Question                   : String = ""
    var MobileNumber                 : String = ""
    var S_Question_code              : String = ""
    
    var passwordType                 : String = ""
    var ValidateSq                   : String = ""
    
    func warpdata() -> NSDictionary {
        let questionB = VillageManager.shared.data.securityQ["QuestionBurmese"] as? String
        let question = VillageManager.shared.data.securityQ["Question"] as? String
        
        var questionAvailable = ""
        if question == nil || questionB == nil {
            questionAvailable = "0"
        }
        
        var languageType = ""
        if ok_default_language == "my" {
            languageType = "0"
        }else if ok_default_language == "uni" {
            languageType = "1"
        }
        else{
            languageType = "2"
        }
        
//        let dic : NSDictionary =  ["Name":Name,"IdType":IdType,"Nrc":Nrc,"PassportCountry":PassportCountry,"PassportExpiryDate":PassportExpiryDate,"Dob":Dob,"FatherName":FatherName,"AddressDetailsRadioSelect":AddressDetailsRadioSelect,"AddressOtherDetails":AddressOtherDetails,"State":State,"DivisionCode":DivisionCode,"TownshipCode":TownshipCode,"CityName":CityName,"Village":Village,"Street":Street,"HouseNumber":"","SecurityAnswer":SecurityAnswer,"ResetUserImage":"","OsType":OsType,"MerchantNumber":MobileNumber,"PassportNumber":PassportNumber,"ValidateSq": questionAvailable,"TransCellTower": NetworlInfo(),"FontType": languageType]
//
        let dic : NSDictionary =  ["Name":Name,"IdType":IdType,"Nrc":Nrc,"PassportCountry":PassportCountry,"PassportExpiryDate":PassportExpiryDate,"Dob":Dob,"FatherName":FatherName,"AddressDetailsRadioSelect":AddressDetailsRadioSelect,"AddressOtherDetails":AddressOtherDetails,"State":State,"DivisionCode":DivisionCode,"TownshipCode":TownshipCode,"CityName":CityName,"Village":Village,"Street":Street,"HouseNumber":"","SecurityAnswer":SecurityAnswer,"ResetUserImage":"","OsType":OsType,"MerchantNumber":MobileNumber,"PassportNumber":PassportNumber,"ValidateSq": questionAvailable,"TransactionsCellTower": NetworlInfo(),"FontType": languageType,"AppId":buildNumber,"GoogleTokenFor":"","Simid":simid,"Msid":simid]
        
        return dic
    }
    
    func NetworlInfo() -> NSDictionary {
        
        var wifiName = ""
        var ssid = ""
        var mac = ""
        if UitilityClass.isConnectedToWifi(){
            
            let wifiInfo = UitilityClass.getNetworkInfos()[0].components(separatedBy: ",")
            wifiName = wifiInfo[0]
            ssid = wifiInfo[0]
            mac = wifiInfo[1]
        }
        
        let dic : NSDictionary =  ["ConnectedwifiName" : wifiName,"Lac": "","Mac": mac,"Mcc": mccStatus.mcc,"Mnc": mccStatus.mnc,"NetworkType": UitilityClass.getNetworkType(),"SignalStrength": "","Ssid": ssid, "NetworkSpeed": UitilityClass.getSignalStrength()]
        return dic
    }
    
    
    func addSecuritAnswer() -> NSDictionary {
        var nrc_number = ""
        if ForgotPasswordModel.share.IdType == "04" {
           nrc_number = PassportNumber
        }else {
           nrc_number = Nrc
        }
        let dic : NSDictionary =  ["MobileNumber" : MobileNumber,"Dob": Dob,"FatherName": FatherName,"Msid": msid,"Name": Name,"Nrc": nrc_number,"SimId": uuid,"QuestionCode": S_Question_code,"Answer": SecurityAnswer]
        return dic
    }
 /*21 {"DivisionCode":"YANGDIVI","PassportNumber":"m9232671","ResetUserImage":" =\n","SecurityAnswer":"TESTER","OsType":"0","AddressDetailsRadioSelect":"Home","PassportCountry":"India","MerchantNumber":"00959763587691","FatherName":"Mr,GOVIND","TownshipCode":"Pabedan","IdType":"04","CityName":"Yangon","State":"","HouseNumber":"","Street":"fggg","Dob":"13-Jul-1989","AddressOtherDetails":"","Village":"","PassportExpiryDate":"01-Dec-2027","Nrc":"","Name":"Mr,RAVINDRA"}
     */
    func clearValues(){
         Name                          = ""
         IdType                        = ""
         Nrc                           = ""    //"14@AhMaNa(N)123456"
         PassportCountry               = ""
         PassportExpiryDate            = ""
         PassportNumber                = ""
         Dob                           = ""
         FatherName                    = ""
         AddressDetailsRadioSelect     = ""
         AddressOtherDetails           = ""
         State                         = "" //Blank
         DivisionCode                  = ""
         TownshipCode                  = ""
         CityName                      = ""
         Village                       = ""
         Street                        = ""
         HouseNumber                   = ""
         SecurityAnswer                = ""
         ResetUserImage                = ""
         OsType                        = 1
         MerchantNumber                = ""
         S_Question                    = ""
         MobileNumber                  = ""
         S_Question_code               = ""
         passwordType                  = ""
    }
    
    
}
