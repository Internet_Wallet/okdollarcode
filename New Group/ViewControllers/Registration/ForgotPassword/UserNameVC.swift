//
//  UserNameVC.swift
//  OK
//
//  Created by Subhash Arya on 15/02/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol  UserNameVCDelegate : class{
    func showBasicDetailFPVC(controller: UserNameVC)
     func hideAllView(controller : UserNameVC)
}

class UserNameVC: OKBaseController,UITextFieldDelegate {
    
   weak var  delegate : UserNameVCDelegate?
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var tfUserName: UITextField!{
        didSet{
            tfUserName.font = UIFont(name: appFont, size: 18.0)
        }
    }
    @IBOutlet weak var viewMaleFemale: UIView!
    @IBOutlet weak var viewMalePrefix: UIView!
    @IBOutlet weak var viewFemalePrifix: UIView!
    @IBOutlet weak var btnClearUserName: UIButton!
    @IBOutlet weak var topConstant: NSLayoutConstraint!
    
    @IBOutlet weak var btnMale: UIButton!{
        didSet{
            btnMale.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnFemale: UIButton!{
        didSet{
            btnFemale.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnU: UIButton!{
        didSet{
            btnU.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnMg: UIButton!{
        didSet{
            btnMg.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnMr: UIButton!{
        didSet{
            btnMr.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnDr: UIButton!{
        didSet{
            btnDr.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnDaw: UIButton!{
        didSet{
            btnDaw.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnMa: UIButton!{
        didSet{
            btnMa.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnMs: UIButton!{
        didSet{
            btnMs.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnMrs: UIButton!{
        didSet{
            btnMrs.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnMDr: UIButton!{
        didSet{
            btnMDr.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    @IBOutlet weak var btnBFirst: UIButton!{
        didSet{
            btnBFirst.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnBSecond: UIButton!{
        didSet{
            btnBSecond.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnBThired: UIButton!{
        didSet{
            btnBThired.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var viewBMaleFemaleCategory: UIView!
    
    var isYourNameKeyboardHidden = true
    var isNextViewHidden = true
    var namePrefix = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        tfUserName.delegate = self
        self.dHeight = 63.0
        viewBMaleFemaleCategory.isHidden = true
        btnClearUserName.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        
        var font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
            font = UIFont.systemFont(ofSize: 18)
        }
        tfUserName.font = font
        
        if ok_default_language == "en" {
            tfUserName.font = UIFont.systemFont(ofSize: 18)
        }else if ok_default_language == "my" {
            tfUserName.font = UIFont(name: appFont, size: 18)
        }else {
            
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfUserName.attributedPlaceholder = NSAttributedString(string: "Enter Your Name", attributes:attributes as [NSAttributedString.Key : Any])
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfUserName.attributedPlaceholder = NSAttributedString(string: "Enter Your Name", attributes:attributes)
                tfUserName.font = UIFont.systemFont(ofSize: 18)
            }
            tfUserName.font = UIFont(name: appFont, size: 18)
        }
        self.tfUserName.placeholder = "Enter Your Name".localized

        
        self.btnMale.setTitle("Male".localized, for: .normal)
        self.btnFemale.setTitle("Female".localized, for: .normal)
        self.btnU.setTitle("U".localized, for: .normal)
        self.btnMg.setTitle("Mg".localized, for: .normal)
        self.btnMr.setTitle("Mr".localized, for: .normal)
        self.btnDr.setTitle("Dr".localized, for: .normal)
        self.btnDaw.setTitle("Daw".localized, for: .normal)
        self.btnMa.setTitle("Ma".localized, for: .normal)
        self.btnMs.setTitle("Ms".localized, for: .normal)
        self.btnMrs.setTitle("Mrs".localized, for: .normal)
        self.btnMDr.setTitle("Dr".localized, for: .normal)
        self.btnBThired.setTitle("Dr".localized, for: .normal)
        
        //self.tfUserName.placeholder = "Enter Your Name".localized
    }
    
    @IBAction func BtnClearUserNameAction(_ sender: Any) {
        tfUserName.leftView = nil
        tfUserName.text = ""
        tfUserName.resignFirstResponder()
        imgUser.image = UIImage(named: "r_user")
        isYourNameKeyboardHidden = true
        btnClearUserName.isHidden = true
        tfUserName.textAlignment = .center
    }
    
    @IBAction func btnSelectGenderAction(_ sender: UIButton) {
        viewMaleFemale.isHidden = true
        if sender.title(for: .normal) == "Male".localized{
            if ok_default_language == "my" || ok_default_language == "uni"{
                viewMalePrefix.isHidden = true
                viewFemalePrifix.isHidden = true
                viewBMaleFemaleCategory.isHidden = false
                btnBFirst.setTitle("U".localized, for: .normal)
                btnBSecond.setTitle("Mg".localized, for: .normal)
            }else {
                viewMalePrefix.isHidden = false
                viewFemalePrifix.isHidden = true
                viewBMaleFemaleCategory.isHidden = true
            }
            imgUser.image = UIImage(named: "r_user")
        }else {
            if ok_default_language == "my" || ok_default_language == "uni"{
                viewBMaleFemaleCategory.isHidden = false
                viewFemalePrifix.isHidden = true
                viewMalePrefix.isHidden = true
                btnBFirst.setTitle("Daw".localized, for: .normal)
                btnBSecond.setTitle("Ma".localized, for: .normal)
            }else {
                viewFemalePrifix.isHidden = false
                viewMalePrefix.isHidden = true
                viewBMaleFemaleCategory.isHidden = true
            }
            imgUser.image = UIImage(named: "r_female")
        }
    }
    
    @IBAction func btnMalePrefixAction(_ sender: UIButton) {
        setPrefixName(prefixName: sender.title(for: .normal)!, txtField: tfUserName)
        tfUserName.becomeFirstResponder()
    }
    
    @IBAction func btnFeMalePrefixAction(_ sender: UIButton) {
        setPrefixName(prefixName: sender.title(for: .normal)!, txtField: tfUserName)
        tfUserName.becomeFirstResponder()
    }
    
    @IBAction func btnBurmeseUserNameInitialAction(sender : UIButton) {
        setPrefixName(prefixName: sender.title(for: .normal)!, txtField: tfUserName)
        tfUserName.becomeFirstResponder()
    }
    
    func setPrefixName(prefixName : String, txtField : UITextField) {
//        namePrefix = prefixName
//        let prelabel = PreNameView.loadView() as! PreNameView
//        prelabel.setPreName(name : prefixName, alignment : .left)
//        txtField.leftView = prelabel
//        txtField.leftViewMode = UITextField.ViewMode.always
//        txtField.becomeFirstResponder()
//        txtField.textAlignment = .left
//
 btnClearUserName.isHidden = false
//        
        namePrefix = prefixName
        let myString = prefixName
        let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: "Zawgyi-One", size: 18) ?? UIFont.systemFont(ofSize: 18.0) ])
        let lblName = UILabel()
        lblName.frame = CGRect(x: 0, y: 3.0, width:stringSize.width + 10 , height: 52)
        lblName.textAlignment = .left
        lblName.font =  UIFont(name: appFont, size: 18)
        lblName.text = myString
        let btnView = UIView()
        btnView.frame = CGRect(x: 0, y: 0, width: lblName.frame.width , height: 52)
        btnView.addSubview(lblName)
        txtField.leftView = btnView
        txtField.leftViewMode = UITextField.ViewMode.always
        isYourNameKeyboardHidden = false
        viewMalePrefix.isHidden = true
        viewFemalePrifix.isHidden = true
        viewBMaleFemaleCategory.isHidden = true
        txtField.becomeFirstResponder()
        txtField.textAlignment = .left
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if tfUserName.text!.count > 2 {
            if isNextViewHidden{
                guard(self.delegate?.showBasicDetailFPVC(controller: self) != nil)else{
                    return false
                }
                isNextViewHidden = false
            }
            ForgotPasswordModel.share.Name = parabaik.uni(toZawgyi:namePrefix + "," + tfUserName.text!.trimmingCharacters(in: .whitespacesAndNewlines))
        }
        return true
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField ==  tfUserName {
            if !isYourNameKeyboardHidden {
                viewMaleFemale.isHidden = true
                return true
            }else{
                viewMaleFemale.isHidden = false
                return false
            }
        }
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var CHARSET = ""
        if ok_default_language == "my" {
            CHARSET = NAME_CHAR_SET_My
        }else if ok_default_language == "en" {
              CHARSET = NAME_CHAR_SET_En
        }else {
             CHARSET = NAME_CHAR_SET_Uni
        }
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if text.count > 2{
            if isNextViewHidden{
                            isNextViewHidden = false
                            isYourNameKeyboardHidden = false
                            tfUserName.becomeFirstResponder()
                guard(self.delegate?.showBasicDetailFPVC(controller: self) != nil)else{
                    return false
                }
            }
        }
        
//        if text.count > 0 {
//            btnClearUserName.isHidden = false
//        }else{
//            btnClearUserName.isHidden = true
//        }
        
        if UserDefaults.standard.string(forKey: "SecurityQshown") == "YES" {
            if let del = delegate {
                isNextViewHidden = true
                del.hideAllView(controller: self)
            }
            
        }
        if text.count > 50 {
            return false
        }
        if restrictMultipleSpaces(str: string, textField: tfUserName) {
            return true
        }else {
            if  textField.text?.last == " " {
                textField.text?.removeLast()
            }
            return false
        }
    }
    
    
}

