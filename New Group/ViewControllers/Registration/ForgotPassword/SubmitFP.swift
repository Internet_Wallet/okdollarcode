//
//  SubmitFP.swift
//  OK
//
//  Created by Imac on 5/30/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol SubmitFPDelegate {
      func navigaToSetSecurityQA()
}

class SubmitFP: OKBaseController,WebServiceResponseDelegate {
    var delegate: SubmitFPDelegate?
    var failureCount = 0

    @IBOutlet weak var btnSubmit: UIButton! {
        didSet {
            btnSubmit.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnSubmit.setTitle("Submit".localized, for: .normal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        failureCount = 0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dHeight = 50
    }

    @IBAction func onClickSubmit(_ sender: Any) {
        if appDelegate.checkNetworkAvail() {
            let web = WebApiClass()
            web.delegate = self
            let urlStr   = Url.URL_AddForgotPasswordApproval
            let url = getUrl(urlStr: urlStr, serverType: .serverApp)
            let param = ForgotPasswordModel.share.warpdata()
            println_debug(url)
            web.genericClassReg1(url: url, param: param as AnyObject, httpMethod: "POST", mScreen: "AddForgotPasswordApproval")
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
        }else{
            alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img:#imageLiteral(resourceName: "r_user"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        DispatchQueue.main.async {
            self.removeProgressView()
        }
        if screen == "AddForgotPasswordApproval"{
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        if dic["Code"] as! NSInteger == 200 {
                            if let del = self.delegate {
                                del.navigaToSetSecurityQA()
                            }
                        }else {
                            showAlert(alertTitle: "", alertBody:"You entered incorrect details. Please try again with correct details".localized , alertImage: #imageLiteral(resourceName: "alert-icon"))
                            self.failureCount = self.failureCount + 1
                            if self.failureCount == 1 {
                                let failureViewModel = FailureViewModel()
                                let param = ForgotPasswordModel.share.warpdata()
                                failureViewModel.sendDataToFailureApi(request: param as Any, response: json as Any, type: FailureHelper.FailureType.ForgotPassword.rawValue, finished: {
                                })
                            }
                        }
                    }
                }
            } catch {
                showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))
            }
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
}
