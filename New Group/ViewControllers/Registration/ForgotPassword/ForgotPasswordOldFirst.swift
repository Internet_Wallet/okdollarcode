//
//  ForgotPasswordOldFirst.swift
//  OK
//
//  Created by Subhash Arya on 16/02/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import MessageUI
class ForgotPasswordOldFirst: OKBaseController,UITableViewDelegate,UITableViewDataSource,MFMessageComposeViewControllerDelegate{

    var image = [UIImage]()
    var textTitile = [String]()
    var buttonTitle = [String]()
    var butttonBGColor = [UIColor]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.removeProgressView()
        }
        self.navigationItem.title = "Contact"
        let green = UIColor(hex: "479710")
        let sky = UIColor(hex: "42ADE0")
        let valoit = UIColor(hex: "7B519C")
        let darkBlue = UIColor(hex: "395A98")
        let red = UIColor(hex: "EA462B")
        
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: "Zawgyi-One", size: 18)
        lblMarque.text = "Contact Us".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
        
        textTitile = ["Call".localized,"Call".localized,"Call".localized,"Send".localized,"Send".localized,"Open".localized,"Open".localized,"Open".localized,"Send".localized]
        buttonTitle = ["+95-1-377888","+95-9-30000066","+95-9-450144455","+95-9-30000066","+95-9-454994404","+95-9-454994404","+95-9-454994404","https://www.facebook.com/okdollarapp/","customercare@okdollar.com"]
        butttonBGColor = [green,green,green,sky,sky,green,valoit,darkBlue,red]
        image = [#imageLiteral(resourceName: "hcall"),#imageLiteral(resourceName: "hphone"),#imageLiteral(resourceName: "hphone"),#imageLiteral(resourceName: "hmessage"),#imageLiteral(resourceName: "hmessage"),#imageLiteral(resourceName: "hwhatsapp"),#imageLiteral(resourceName: "hviber"),#imageLiteral(resourceName: "hfacebook"),#imageLiteral(resourceName: "hmail")]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    @IBAction func backAction(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowLoginBothViews"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return textTitile.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ForgotPasswordOldFirstCell", for: indexPath) as! ForgotPasswordOldFirstCell
        cell.imgTitle.image = image[indexPath.row]
        cell.lblTitle.text = buttonTitle[indexPath.row]
        cell.btnCallButton.backgroundColor = butttonBGColor[indexPath.row]
        cell.btnCallButton.tag = indexPath.row
        cell.btnCallButton.setTitle(textTitile[indexPath.row], for: .normal)
        cell.btnCallButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        return cell
    }

    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 70
    }

    
    @objc func buttonAction(sender : UIButton) {
        if sender.tag == 0 {
            
            let callURL = URL(string: "telprompt://+95-1-377888")
            UIApplication.shared.open(callURL!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            
        }else if sender.tag == 1 {
            
            let callURL = URL(string: "telprompt://+95-9-30000066")
            UIApplication.shared.open(callURL!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            
        }else if sender.tag == 2 {
            
            let callURL = URL(string: "telprompt://+959450144455")
            UIApplication.shared.open(callURL!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            
        }else if sender.tag == 3 {
            
            sendMessageaction(number: buttonTitle[sender.tag])
            
        }else if sender.tag == 4 {
            
             sendMessageaction(number: buttonTitle[sender.tag])
            
        }else if sender.tag == 5 {
            
            let urlWhats = "whatsapp://send?phone=+959454994404&abid=12354&text=Hello"
            if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
                if let whatsappURL = URL(string: urlString) {
                    if UIApplication.shared.canOpenURL(whatsappURL) {
                        UIApplication.shared.open(whatsappURL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                    } else {
                         showAlert(alertTitle: "", alertBody: "Viber is not install in your Phone, Please install and try agian", alertImage:#imageLiteral(resourceName: "hwhatsapp"))
                    }
                }
            }
            
        }else if sender.tag == 6 {
            
            let urlViber = "viber://chat:<+959454994404>"
            if let urlString = urlViber.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
                if let viberURL = URL(string: urlString) {
                    if UIApplication.shared.canOpenURL(viberURL) {
                        UIApplication.shared.open(viberURL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                    } else {
                        showAlert(alertTitle: "", alertBody: "Viber is not install in your Phone, Please install and try agian", alertImage: #imageLiteral(resourceName: "hviber"))
                    }
                }
            }
            
        }else if sender.tag == 7 {
            
//            if UIApplication.shared.canOpenURL(URL(string: "fb://")!) {
//                let url = URL(string: "fb://profile?app_okdollar")
//                UIApplication.shared.open(url!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
//            } else {
                let url = URL(string: "https://www.facebook.com/okdollarapp/")
                UIApplication.shared.open(url!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            //}
            
        }else if sender.tag == 8 {
            
            let email = "customercare@okdollar.com"
            if let url = URL(string: "mailto:\(email)") {
                UIApplication.shared.open(url)
            }
            else{
                showAlert(alertTitle: "", alertBody:"No Mail Configured" , alertImage:#imageLiteral(resourceName: "r_email"))
            }
        }
        
    }
    
    func sendMessageaction(number : String) {
        
        if !MFMessageComposeViewController.canSendText() {
        }else{
            let composeVC = MFMessageComposeViewController()
            composeVC.messageComposeDelegate = self
            composeVC.recipients = [number]
            composeVC.body = "Welcome To OK Dollar!"
            self.present(composeVC, animated: true, completion: nil)
        }
        
    }
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
}



class ForgotPasswordOldFirstCell: UITableViewCell {
    @IBOutlet weak var imgTitle: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCallButton: UIButton!
    
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
