//
//  SetSecurityQAFP.swift
//  OK
//
//  Created by Imac on 5/30/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class SetSecurityQAFP: OKBaseController,WebServiceResponseDelegate,UITextFieldDelegate,SecurityQuestionRDelegate {
    @IBOutlet weak var lblDescription: UILabel!{
        didSet{
            lblDescription.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblQuestion: MarqueeLabel!{
        didSet{
            lblQuestion.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var tfAnswer: UITextField!{
        didSet{
            tfAnswer.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnSubmit: UIButton! {
        didSet {
            btnSubmit.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
            btnSubmit.setTitle("Submit".localized, for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tfAnswer.delegate = self
        lblQuestion.text = "Select Security Question".localized
        tfAnswer.placeholder = "Enter Answer".localized
        ForgotPasswordModel.share.S_Question_code = ""
        lblDescription.text = "You do not have Security Question & Answer. According to the Central Bank of Myanmar Regulation on Mobile Financial Services, those who open account in OK $, must have Security Questioin & Answer.".localized
    }
    @IBAction func btnSecuritQuestionAction(_ sender: Any) {
        self.view.endEditing(true)
            tfAnswer.text = ""
            tfAnswer.resignFirstResponder()
            let sb = UIStoryboard(name: "Registration" , bundle: nil)
            let securityQuestionR  = sb.instantiateViewController(withIdentifier: "SecurityQuestionR") as! SecurityQuestionR
            securityQuestionR.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            securityQuestionR.securityQuestionRDelegate = self
            securityQuestionR.modalPresentationStyle = .overCurrentContext
            self.present(securityQuestionR, animated: false, completion: nil)
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        if lblQuestion.text == "Select Security Question".localized {
            showAlert(alertTitle: "", alertBody: "Please Select Security Question First".localized, alertImage: #imageLiteral(resourceName: "password"))
        }
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if tfAnswer.text!.count > 0 {
            ForgotPasswordModel.share.SecurityAnswer = parabaik.uni(toZawgyi: tfAnswer.text!.trimmingCharacters(in: .whitespacesAndNewlines) )
        }else {
            ForgotPasswordModel.share.SecurityAnswer = ""
        }
        return true
    }

    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        var CHARSET = ""
        if ok_default_language == "my"{
            CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀/-,:() "
        }else if ok_default_language == "en"{
            CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890/-,:() "
        }else {
           CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀/-,:() "
        }
        
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
        
        if text.count > 50 { return false }
        if restrictMultipleSpaces(str: string, textField: tfAnswer) {
            return true
        }else {
            if  textField.text?.last == " " {
                textField.text?.removeLast()
            }
            return false
        }
    }

    func SelectedQuestion(questionDic: Dictionary<String, AnyObject>) {
        ForgotPasswordModel.share.S_Question_code = questionDic["QuestionCode"] as! String
        if ok_default_language == "my" {
            lblQuestion.text = questionDic["QuestionBurmese"] as? String
        }else if ok_default_language == "en"{
            lblQuestion.text = questionDic["Question"] as? String
        }else {
            lblQuestion.text = questionDic["QuestionBurmese"] as? String
        }
    }
    
    
    
    @IBAction func onClickSubmit(_ sender: Any) {
        if ForgotPasswordModel.share.S_Question_code == "" {
            showAlert(alertTitle: "", alertBody: "Please Select Security Question First".localized, alertImage: #imageLiteral(resourceName: "password"))
        }else if ForgotPasswordModel.share.SecurityAnswer == "" {
            showAlert(alertTitle: "", alertBody: "Please enter Security Answer".localized, alertImage: #imageLiteral(resourceName: "password"))
        }else {
            if appDelegate.checkNetworkAvail() {
                let web = WebApiClass()
                web.delegate = self
                //let urlStr   = Url.URL_AddForgotPasswordApproval
               // let url = getUrl(urlStr: urlStr, serverType: .serverApp)
               let url = URL(string: Url.URL_AddSecurityQuestionAnswer)
                let param = ForgotPasswordModel.share.addSecuritAnswer()
                println_debug(url)
                println_debug(param)
                web.genericClassReg1(url: url!, param: param as AnyObject, httpMethod: "POST", mScreen: "AddForgotPasswordApproval")
                DispatchQueue.main.async {
                    progressViewObj.showProgressView()
                }
            }else{
                alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img:#imageLiteral(resourceName: "r_user"))
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                })
                DispatchQueue.main.async {
                    alertViewObj.showAlert(controller: self)
                }
            }
        }
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        DispatchQueue.main.async {
            self.removeProgressView()
        }
        if screen == "AddForgotPasswordApproval"{
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        if dic["Code"] as! NSInteger == 200 {
                            DispatchQueue.main.async {
                                let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordFP") as? ChangePasswordFP
                                self.navigationController?.pushViewController(loginVC!, animated: true)
                                self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
                            }
                        }else {
                            showAlert(alertTitle: "", alertBody:"You entered incorrect details. Please try again with correct details".localized , alertImage: #imageLiteral(resourceName: "alert-icon"))
                        }
                    }
                }
            } catch {
                showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))
            }
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }

}
