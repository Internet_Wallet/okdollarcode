//
//  ChangePasswordFP.swift
//  OK
//
//  Created by Subhash Arya on 03/03/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ChangePasswordFP: OKBaseController,UITextFieldDelegate {
    @IBOutlet weak var viewOTP: UIView!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var viewConfrimPassword: UIView!
    @IBOutlet weak var viewShowPassword: UIView!
    @IBOutlet weak var viewPattern: UIView!
    
    @IBOutlet weak var tfOTP: UITextField!{
        didSet{
            tfOTP.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var tfPassword: UITextField!{
        didSet{
            tfPassword.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var tfConfrimPassword: UITextField!{
        didSet{
            tfConfrimPassword.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnShowPassword: UIButton!{
        didSet{
            btnShowPassword.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet var patternLock: GesturePatternLock!
    @IBOutlet weak var btnContinue: UIButton!{
        didSet{
            btnContinue.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnCancel: UIButton!{
        didSet{
            btnCancel.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var tfNext: UIButton!{
        didSet{
            tfNext.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var lblShowPass: UILabel!{
        didSet{
            lblShowPass.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnPattern: UIButton!{
        didSet{
            btnPattern.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnSubmit: UIButton!{
        didSet{
            btnPattern.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnPassword: UIButton!{
        didSet{
            btnPassword.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var lblCreatePattern: UILabel!{
        didSet{
            lblCreatePattern.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnCreatePasswordClear: UIButton!{
        didSet{
            btnCreatePasswordClear.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnConfirmPasswordClear: UIButton!{
        didSet{
            btnConfirmPasswordClear.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    @IBOutlet weak var confirmpasswordHeightConstraint: NSLayoutConstraint!
    var strPassword = ""
    var strConfirmPassword = ""
    var passwordString = ""
    var strOTP = ""
    var dicValue : Dictionary<String,Any> = Dictionary<String,Any>()
     let validObj       = PayToValidations()
    var failureCount = 0
    var isComingFromShowPassword = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfOTP.delegate = self
        viewPassword.isHidden = true
        viewPattern.isHidden = true
        tfPassword.delegate = self
        tfConfrimPassword.delegate = self
        btnCreatePasswordClear.isHidden = true
        btnConfirmPasswordClear.isHidden = true
        btnSubmit.isHighlighted = true
        btnSubmit.isUserInteractionEnabled = false
        
        setuplock()
      
        viewConfrimPassword.isHidden = true
        confirmpasswordHeightConstraint.constant = 0.0
        viewShowPassword.isHidden = false
        
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: 18)
        lblMarque.text = "Forgot Password".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
        
        var font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
            font = UIFont.systemFont(ofSize: 18)
        }
        tfOTP.font = font
        tfPassword.font = font
        tfConfrimPassword.font = font
        
        
        if ok_default_language == "en"{
             tfOTP.font = UIFont(name: appFont, size: 18)
        }else if ok_default_language == "en" {
             tfOTP.font = UIFont.systemFont(ofSize: 18)
        }else {
            
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfOTP.attributedPlaceholder = NSAttributedString(string: "Enter your OTP", attributes:attributes as [NSAttributedString.Key : Any])
                
                let attributes1 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfPassword.attributedPlaceholder = NSAttributedString(string: "Enter New Password", attributes:attributes1 as [NSAttributedString.Key : Any])
                
                let attributes2 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                   .font : UIFont(name: appFont, size: 18)]
                tfConfrimPassword.attributedPlaceholder = NSAttributedString(string: "Re-Enter New Password", attributes:attributes2 as [NSAttributedString.Key : Any])
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfOTP.attributedPlaceholder = NSAttributedString(string: "Enter your OTP", attributes:attributes)
                tfOTP.font = UIFont.systemFont(ofSize: 18)
                
                let attributes1 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfPassword.attributedPlaceholder = NSAttributedString(string: "Enter New Password", attributes:attributes1)
                tfPassword.font = UIFont.systemFont(ofSize: 18)
                
                let attributes2 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfConfrimPassword.attributedPlaceholder = NSAttributedString(string: "Re-Enter New Password", attributes:attributes2)
                tfConfrimPassword.font = UIFont.systemFont(ofSize: 18)
            }
            
        }
        
        
    tfOTP.placeholder = "Enter your OTP".localized
    btnCancel.setTitle("Cancel".localized, for: .normal)
    tfNext.setTitle("Next".localized, for: .normal)
    tfPassword.placeholder = "Enter New Password".localized
    tfConfrimPassword.placeholder = "Re-Enter New Password".localized
    lblShowPass.text = "Show Password".localized
    btnPattern.setTitle("Pattern".localized, for: .normal)
    btnSubmit.setTitle("".localized, for: .normal)
    btnPassword.setTitle("Password".localized, for: .normal)
    btnContinue.setTitle("Continue".localized, for: .normal)
    lblCreatePattern.text = "Create Your Pattern".localized
    btnSubmit.setTitle("Submit".localized, for: .normal)
    btnContinueDisable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        failureCount = 0
        tfPassword.clearsOnInsertion = false
    }
    
    func btnContinueDisable() {
        btnContinue.isHighlighted = true
        btnContinue.isUserInteractionEnabled = false
    }
    
    func btnContinueEnable() {
        btnContinue.isHighlighted = false
        btnContinue.isUserInteractionEnabled = true
    }
    
    @IBAction func btnCreatePasswordAction(_ sender: UIButton) {
        btnCreatePasswordClear.isHidden = true
        btnConfirmPasswordClear.isHidden = true
        btnShowPassword.isSelected = false
        tfPassword.text = ""
        tfConfrimPassword.text = ""
        viewConfrimPassword.isHidden = true
        confirmpasswordHeightConstraint.constant = 0.0
        viewShowPassword.isHidden = false
        btnSubmit.isHighlighted = true
        btnSubmit.isUserInteractionEnabled = false
        tfPassword.isSecureTextEntry = true
        tfConfrimPassword.isSecureTextEntry = true
    }
    
    @IBAction func btnConfirmPasswordAction(_ sender: UIButton) {
        btnConfirmPasswordClear.isHidden = true
        tfConfrimPassword.text = ""
        btnSubmit.isHighlighted = true
        btnSubmit.isUserInteractionEnabled = false
    }
    
    
    @IBAction func ShowPasswordAction(_ sender: Any) {
        if isComingFromShowPassword{
            isComingFromShowPassword = false
        }else{
            isComingFromShowPassword = true
        }
        
        btnShowPassword.isSelected = !btnShowPassword.isSelected
        if btnShowPassword.isSelected {
            tfPassword.isSecureTextEntry = false
            tfConfrimPassword.isSecureTextEntry = false
        }else{
            tfPassword.isSecureTextEntry = true
            tfConfrimPassword.isSecureTextEntry = true
        }
    }
    
    @IBAction func PatternAction(_ sender: UIButton) {
        tfPassword.isSecureTextEntry = true
        tfConfrimPassword.isSecureTextEntry = true
        if sender.tag == 1 {
            // Reset Pattern
             tfPassword.text = ""
            if self.btnPassword.titleLabel!.text == "Reset".localized {
                self.patternLock.resetLock()
                lblCreatePattern.text = "Create Your Pattern".localized
                self.btnContinue.setTitle("Continue".localized,for: .normal)
                self.btnPassword.setTitle("Password".localized,for: .normal)
              //  self.btnContinue.isHighlighted = true
              //  self.btnContinue.isUserInteractionEnabled = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                    self?.btnContinueDisable()
                }
               
            }else {
            //show password screen
            tfConfrimPassword.text = ""
            passwordString = ""
            btnShowPassword.isSelected = false
            viewPassword.isHidden = false
             viewPattern.isHidden = true
           // hide these views when password.cout < 6
            viewConfrimPassword.isHidden = true
            confirmpasswordHeightConstraint.constant = 0.0
            viewShowPassword.isHidden = false
            btnCreatePasswordClear.isHidden = true
            }
        }else {
            // call change password api
            if self.btnContinue.titleLabel!.text == "Continue".localized {
                if passwordString.count < 4 {
                    showAlert(alertTitle: "", alertBody: "Please Draw Minimum 4 Dots Pattern".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
                    passwordString = ""
                    self.patternLock.resetLock()
                }else {
                     passwordString = ""
                     lblCreatePattern.text = "Confirm Your Pattern".localized
                    self.btnContinue.setTitle("Confirm".localized ,for: .normal)
                    self.btnPassword.setTitle("Reset".localized,for: .normal)
                    self.patternLock.resetLock()
                }
            }else {
                if passwordString.count == 0 {
                    showAlert(alertTitle: "", alertBody:"Please Draw Minimum 4 Dots Pattern".localized , alertImage: #imageLiteral(resourceName: "password"))
                }else  if !(strPassword == passwordString){
                    showAlert(alertTitle: "", alertBody:"Confirm Pattern does not matched".localized , alertImage: #imageLiteral(resourceName: "password"))
                    passwordString = ""
                    self.patternLock.resetLock()
                    lblCreatePattern.text = "Create Your Pattern".localized
                    self.btnContinue.setTitle("Continue".localized,for: .normal)
                    self.btnPassword.setTitle("Password".localized,for: .normal)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                        self?.btnContinueDisable()
                    }
                }else {
                    let object = PatternValidationClass()
                    let finalString = object.patternEncryption(strPassword, withnumber: ForgotPasswordModel.share.MobileNumber)
                    ForgotPasswordModel.share.passwordType = "1"
                    self.callChangePasswordAPI(pin: finalString!)
                }
            }
        }
    }
    @IBAction func PasswordAction(_ sender: UIButton) {
        if sender.tag == 1 {
             //  Show pattern screen
            viewPassword.isHidden = true
            viewPattern.isHidden = false
            btnContinue.setTitle("Continue".localized, for: .normal)
            passwordString = ""
            self.patternLock.resetLock()
        }else {
            // call change password api
            
            if tfPassword.text == tfConfrimPassword.text {
                
                ForgotPasswordModel.share.passwordType = "0"
                self.callChangePasswordAPI(pin: tfPassword.text!)
            }else {
                showAlert(alertTitle: "", alertBody: "Confirm Pattern does not matched".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
            }
        }
    }
    
    @IBAction func TOPAction(_ sender: UIButton) {
//        self.viewOTP.isHidden = true
//        self.viewPassword.isHidden = false
//        self.strOTP = self.tfOTP.text!
//
        if sender.tag == 2 {
            profileObj.callLoginApi(aCode: ForgotPasswordModel.share.MobileNumber, withPassword: tfOTP.text!, handler: { (success) in
                if success {
                    self.viewOTP.isHidden = true
                    self.viewPassword.isHidden = false
                    self.strOTP = self.tfOTP.text!
                }else {
                    self.showAlert(alertTitle: "", alertBody: "Invalid OTP please enter valid OTP".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
                    self.tfOTP.text = ""
                }
            })
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }

    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)

        if textField == tfPassword {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: PASSWORD_CHAR_SET).inverted).joined(separator: ""))

            { return false }
            
            if range.location == 0 && string == " " {
                return false
            }
            
                if let confirm = tfConfrimPassword.text{
                    if confirm.count>=6{
                        if text.count>confirm.count{
                            tfPassword.text = ""
                            tfConfrimPassword.text = ""
                            viewConfrimPassword.isHidden = true
                            btnCreatePasswordClear.isHidden = true
                            confirmpasswordHeightConstraint.constant = 0.0
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                                self?.btnSubmit.isHighlighted = true
                                self?.btnSubmit.isUserInteractionEnabled = false
                            }
                        }
                    }
                    else{
                        //&& viewConfrimPassword.isHidden
                        if text.count>=6  {
                                viewConfrimPassword.isHidden = false
                                confirmpasswordHeightConstraint.constant = 60.0
                                btnCreatePasswordClear.isHidden = false
                                viewShowPassword.isHidden = false
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                                    self?.btnContinueEnable()
                                }
                        }else{
                            tfConfrimPassword.text = ""
                            viewConfrimPassword.isHidden = true
                            btnCreatePasswordClear.isHidden = true
                            confirmpasswordHeightConstraint.constant = 0.0
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                                self?.btnSubmit.isHighlighted = true
                                self?.btnSubmit.isUserInteractionEnabled = false
                            }
                        }
                    }
                }else{
                    viewConfrimPassword.isHidden = false
                    confirmpasswordHeightConstraint.constant = 60.0
                    btnCreatePasswordClear.isHidden = false
                    viewShowPassword.isHidden = false
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                        self?.btnContinueEnable()
                    }
                }
                
                
            
            
            
            
//            if text.count < 6 {
//                tfConfrimPassword.text = ""
//                viewConfrimPassword.isHidden = true
//                btnCreatePasswordClear.isHidden = true
//                confirmpasswordHeightConstraint.constant = 0.0
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
//                    self?.btnSubmit.isHighlighted = true
//                    self?.btnSubmit.isUserInteractionEnabled = false
//                }
//
//            }else {
//                viewConfrimPassword.isHidden = false
//                confirmpasswordHeightConstraint.constant = 60.0
//
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
//                    self?.btnContinueEnable()
//                }
//
//            }
          //  btnConfirmPasswordClear.isHidden = true
            if text.count > 30 {
                return false
            }
            else{
             //   if let lang = UserDefaults.standard.string(forKey: "currentLanguage"), lang == "en"{
                    let  char = string.cString(using: String.Encoding.utf8)!
                    let isBackSpace = strcmp(char, "\\b")
                    if isBackSpace != -92 {
                        let strTxt : Character = Character(string)
                        if textField.text?.last == strTxt {
                            if textField.text?.count ?? 0 >= 1 {
                                if let index1 = textField.text?.index((textField.text?.startIndex)!, offsetBy: range.location - 1)
                                {//will call succ 2 times
                                    if let lastChar: Character = (textField.text?[index1]) //now we can index!
                                    {
                                        if lastChar == strTxt
                                        {
                                            return false
                                        }
                                    }
                                }
                            }
                        }
                    }else{
                        
                        if let confirm = tfConfrimPassword.text{
                            if confirm.count>=6{
                                if text.count<confirm.count{
                                    tfConfrimPassword.text = ""
                                    viewConfrimPassword.isHidden = false
                                    btnCreatePasswordClear.isHidden = false
                                    if isComingFromShowPassword{
                                        confirmpasswordHeightConstraint.constant = 60.0
                                    }else{
                                        confirmpasswordHeightConstraint.constant = 0.0
                                    }
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                                        self?.btnSubmit.isHighlighted = true
                                        self?.btnSubmit.isUserInteractionEnabled = false
                                    }
                                }else{
                                    
                                    
                                    
                                }
                            }
                            else{
                                //&& viewConfrimPassword.isHidden
                                if text.count<6 {
                                                                            
                                    tfConfrimPassword.text = ""
                                    viewConfrimPassword.isHidden = true
                                    btnCreatePasswordClear.isHidden = true
                                    confirmpasswordHeightConstraint.constant = 0.0
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                                        self?.btnSubmit.isHighlighted = true
                                        self?.btnSubmit.isUserInteractionEnabled = false
                                    }

                                    
                                    
                                }else{
                                    viewConfrimPassword.isHidden = false
                                    confirmpasswordHeightConstraint.constant = 60.0
                                    btnCreatePasswordClear.isHidden = false
                                    viewShowPassword.isHidden = false
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                                        self?.btnContinueEnable()
                                    }

                                }
                            }
                        }else{
                            viewConfrimPassword.isHidden = false
                            confirmpasswordHeightConstraint.constant = 60.0
                            btnCreatePasswordClear.isHidden = false
                            viewShowPassword.isHidden = false
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                                self?.btnContinueEnable()
                            }
                        }
                        
                        
//                        tfConfrimPassword.text = ""
//                        viewConfrimPassword.isHidden = true
//                        btnCreatePasswordClear.isHidden = true
//                        confirmpasswordHeightConstraint.constant = 0.0
//                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
//                            self?.btnSubmit.isHighlighted = true
//                            self?.btnSubmit.isUserInteractionEnabled = false
//                        }

                    }
              //  }
            }
            
        }else if textField == tfConfrimPassword {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: PASSWORD_CHAR_SET).inverted).joined(separator: "")) { return false }
         
          
            if text.count < (tfPassword.text?.count)! {
                btnSubmit.isHighlighted = true
                btnSubmit.isUserInteractionEnabled = false
            }else if text.count > (tfPassword.text?.count)!{
                 tfConfrimPassword.resignFirstResponder()
            }
            
            if validObj.checkMatchingNumber(string: text, withString: self.tfPassword.text!) {
                if text.count > 0 {
                    btnConfirmPasswordClear.isHidden = false
                }else {
                    btnConfirmPasswordClear.isHidden = true
                }
                self.tfConfrimPassword.text = text
                if tfConfrimPassword.text == tfPassword.text {
                    tfConfrimPassword.resignFirstResponder()
                    btnSubmit.isHighlighted = false
                    btnSubmit.isUserInteractionEnabled = true
                }
                return false
            } else {  return false  }
        }
    return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == tfPassword {
            if textField.text == "" || textField.text?.count ?? 0 < 6 {
                tfConfrimPassword.text = ""
                viewConfrimPassword.isHidden = true
                btnCreatePasswordClear.isHidden = true
                confirmpasswordHeightConstraint.constant = 0.0
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                  //  self?.btnSubmit.isHighlighted = false
                  //  self?.btnSubmit.isUserInteractionEnabled = false
                    self?.btnSubmit.isHighlighted = true
                    self?.btnSubmit.isUserInteractionEnabled = false
                   
                }
              
            }
        }
    }
    
    func callChangePasswordAPI(pin: String) {
    var urlString = String.init(format: Url.URL_ChangePassword,ForgotPasswordModel.share.MobileNumber,strOTP,ForgotPasswordModel.share.MobileNumber,pin,UserLogin.shared.token)
        urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let urlS = getUrl(urlStr: urlString, serverType: .serverEstel)
        print(urlS)
        let param = [String:String]() as AnyObject
        TopupWeb.genericClassXML(url: urlS, param: param, httpMethod: "POST", handle: {
            (response , success) in
            if success {
                //navigate to DashBoard
                print(response)
                if let xmlResponse = response as? String {
                    self.parsingFeeLimitKickbackResponse(xml: xmlResponse)
                    if self.dicValue.safeValueForKey("resultdescription") as? String == "Transaction Successful" {
                        userDef.set(ForgotPasswordModel.share.passwordType, forKey: "passwordLogin_local")
                        if ForgotPasswordModel.share.passwordType == "1" {
                            userDef.set(true, forKey: "passwordLoginType")
                        } else {
                            userDef.set(false, forKey: "passwordLoginType")
                        }
                        userDef.synchronize()
                        ok_password         = userDef.string(forKey: "passwordLogin_local")
                        ok_password_type    = userDef.bool(forKey: "passwordLoginType")
                        
                        if ForgotPasswordModel.share.passwordType == "0" {
                            DispatchQueue.main.async {
                                if let pass = self.tfPassword.text {
                                    if pass.isNumeric {
                                        userDef.set(true, forKey: "passKeyBoardType")
                                    }else {
                                        userDef.set(false, forKey: "passKeyBoardType")
                                    }
                                }
                            }
                        }
                        
                        self.showAlert(alertTitle: "", alertBody: "Password changed successfully".localized, alertImage: #imageLiteral(resourceName: "password"))
                  
                        profileObj.callLoginApi(aCode: ForgotPasswordModel.share.MobileNumber, withPassword: pin, handler: { (success) in
                            if success {
                                DispatchQueue.main.async {
                                    profileObj.callUpdateProfileApi(aCode: ForgotPasswordModel.share.MobileNumber, handler: { (successr) in
                                        if successr {                                            
                                            DispatchQueue.main.async {
                                                let rootNav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "navigationMain")
                                                rootNav.modalPresentationStyle = .fullScreen
                                                self.present(rootNav, animated: true, completion: nil)
                                                userDef.set(true, forKey: keyLoginStatus)
                                                userDef.synchronize()
                                            }
                                        }
                                    })
                                }
                            }
                        })
                    }
                }
            }else {
                self.showAlert(alertTitle: "", alertBody: "Password did not reset".localized, alertImage: #imageLiteral(resourceName: "password"))
                self.failureCount = self.failureCount + 1
                if self.failureCount == 1 {
                    let failureViewModel = FailureViewModel()
                    failureViewModel.sendDataToFailureApi(request: param as Any, response: response as Any, type: FailureHelper.FailureType.ForgotPassword.rawValue, finished: {
                    })
                }
            }
            })
    }

    func parsingFeeLimitKickbackResponse(xml: String) {
        let xml = SWXMLHash.parse(xml)
        self.enumerate(indexer: xml)
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            dicValue[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    
    
    func capturePasswordWithDetails(_ code: String) -> (password: String,type: Bool) {
        return (retainingEncryptedPassword(code),false)
    }
    
    private  func retainingEncryptedPassword(_ aCode: String) -> String {
        
        if passwordString == "" {
            self.showAlert(alertTitle: "", alertBody: "Draw the Pattern to Login".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
            self.patternLock.resetLock()
        }
        return passwordString
    }
    
    @objc func gestureComplete(gestureLock: GesturePatternLock) {
        passwordString = ""
        if gestureLock.lockSequence.count <= 3 {
            println_debug("More Than 3 counts needed")
            self.btnPassword.setTitle("Reset".localized,for: .normal)
            return
        }
   
        for element in gestureLock.lockSequence {
            passwordString.append(element.stringValue)
        }
        
        if self.btnContinue.titleLabel!.text == "Continue".localized{
            self.btnPassword.setTitle("Reset".localized,for: .normal)
            strPassword = passwordString
            btnContinueEnable()
            //gestureLock.gestureLockState = .normal
        }else{
//            if !(strPassword == passwordString){
//                showAlert(alertTitle: "", alertBody:"Confirm Password does not matched".localized , alertImage: #imageLiteral(resourceName: "password"))
//                self.patternLock.resetLock()
//                self.btnContinue.setTitle("Continue",for: .normal)
//            }
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
  
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        }
    }
    
    private func setuplock() {
        
        // Set number of sensors
        patternLock.lockSize = (3, 3)
        
        // Sensor grid customisations
        patternLock.edgeInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        
        // Sensor point customisation (normal)
        patternLock.setSensorAppearance(
            type: .inner,
            radius: 5,
            width: 5,
            color: UIColor(displayP3Red: 247.0/255.0, green:199.0/255.0 , blue: 0.0/255.0, alpha: 1.0),
            forState: .normal
        )
        patternLock.setSensorAppearance(
            type: .outer,
            color: .gray,
            forState: .normal
        )
        
        // Sensor point customisation (selected)
        patternLock.setSensorAppearance(
            type: .inner,
            radius: 5,
            width: 5,
            color: UIColor(displayP3Red: 247.0/255.0, green:199.0/255.0 , blue: 0.0/255.0, alpha: 1.0),
            forState: .selected
        )
        patternLock.setSensorAppearance(
            type: .outer,
            radius: 40,
            width: 5,
            color: .gray,
            forState: .selected
        )
        
        // Sensor point customisation (wrong password)
        patternLock.setSensorAppearance(
            type: .inner,
            radius: 03,
            width: 15,
            color: .red,
            forState: .error
        )
        patternLock.setSensorAppearance(
            type: .outer,
            radius: 0,
            width: 1,
            color: .red,
            forState: .error
        )
        
        // Line connecting sensor points (normal/selected)
        [GesturePatternLock.GestureLockState.normal, GesturePatternLock.GestureLockState.selected].forEach { (state) in
            patternLock.setLineAppearance(
                width: 5.5, color: UIColor(red: 241/255, green: 188/255, blue: 20/255, alpha: 1.0), // UIColor.clear UIColor.yellow.withAlphaComponent(0.5)
                forState: state
            )
        }
        
        // Line connection sensor points (wrong password)
        patternLock.setLineAppearance(
            width: 5.5, color: UIColor.red.withAlphaComponent(0.5),
            forState: .error
        )
        
        patternLock.addTarget(
            self, action: #selector(gestureComplete),
            for: .gestureComplete
        )
    }
}

