//
//  BasicSecurityQFP.swift
//  DemoUpdateProfile
//
//  Created by SHUBH on 11/14/17.
//  Copyright © 2017 SHUBH. All rights reserved.
//

import UIKit

protocol BasicSecurityQFPDelegate : class {
    func updateMainUIUI()
    func navigaToPasswordResetVC()
}

class BasicSecurityQFP: OKBaseController,WebServiceResponseDelegate,UITextFieldDelegate {
    @IBOutlet weak var lblQuestion: MarqueeLabel!{
        didSet{
            lblQuestion.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var tfAnswer: UITextField!{
        didSet{
            tfAnswer.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblHeader: NSLayoutConstraint!
    @IBOutlet weak var btnSubmit: UIButton!{
        didSet{
            btnSubmit.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnReset: UIButton!{
        didSet{
            btnReset.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var lblSecurityHeader: MarqueeLabel! {
        didSet {
            lblSecurityHeader.font = UIFont(name: appFont, size: appFontSize)
            lblSecurityHeader.text = "Security Answer".localized
        }
    }
    weak var delegate : BasicSecurityQFPDelegate?
    var fnt = UIFont(name: appFont, size: 18.0)//UIFont.systemFont(ofSize: 18)
    override func viewDidLoad() {
        super.viewDidLoad()
        tfAnswer.delegate = self
        self.dHeight = 154.0
        
        var font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
            font = UIFont(name: appFont, size: 18.0)//UIFont.systemFont(ofSize: 18)
        }
        tfAnswer.font = font
        
        if ok_default_language  == "my" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
            fnt = UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18)
            lblQuestion.font = fnt
            lblQuestion.text = VillageManager.shared.data.securityQ["QuestionBurmese"] as? String
        }else if ok_default_language  == "en"{
            fnt = UIFont.systemFont(ofSize: 18)
            lblQuestion.font = fnt
            btnReset.setBackgroundImage(UIImage(named: "reset_blue"), for: .normal)
            lblQuestion.text = VillageManager.shared.data.securityQ["Question"] as? String
        }else {
            fnt = UIFont.systemFont(ofSize: 18)
            lblQuestion.font = fnt
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
            let attributes1 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                              .font : UIFont.systemFont(ofSize: 18)]
            lblQuestion.attributedText = NSAttributedString(string: VillageManager.shared.data.securityQ["QuestionBurmeseUniCode"] as? String ?? "", attributes:attributes1)
           // lblQuestion.text = VillageManager.shared.data.securityQ["QuestionBurmeseUniCode"] as? String
        
            
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfAnswer.attributedPlaceholder = NSAttributedString(string: "Enter Answer", attributes:attributes as [NSAttributedString.Key : Any])
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfAnswer.attributedPlaceholder = NSAttributedString(string: "Enter Answer", attributes:attributes)
                tfAnswer.font = UIFont.systemFont(ofSize: 18)
            }
        }
     
      //Security Answer
        tfAnswer.placeholder = "Enter Answer".localized
        btnSubmit.setTitle("Submit".localized, for: .normal)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if tfAnswer.text!.count > 0 {
            if ok_default_language == "my" {
                ForgotPasswordModel.share.SecurityAnswer = parabaik.uni(toZawgyi: tfAnswer.text!.trimmingCharacters(in: .whitespacesAndNewlines) )
            }else {
                ForgotPasswordModel.share.SecurityAnswer = tfAnswer.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            }
            self.dHeight = 205.0
            guard(self.delegate?.updateMainUIUI() != nil)else{
                return false
            }
        }else {
            ForgotPasswordModel.share.SecurityAnswer = ""
            self.dHeight = 154.0
            guard(self.delegate?.updateMainUIUI() != nil)else{
                return false
            }
        }
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        var CHARSET = ""
        if ok_default_language == "my"{
            CHARSET = STREETNAME_CHAR_SET_My
        }else if ok_default_language == "en"{
            CHARSET = STREETNAME_CHAR_SET_En
        }else {
             CHARSET = STREETNAME_CHAR_SET_Uni
        }
        
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }

          if text.count > 50 { return false }
        if restrictMultipleSpaces(str: string, textField: tfAnswer) {
            return true
        }else {
            if  textField.text?.last == " " {
                textField.text?.removeLast()
            }
            return false
        }
    }
    @IBAction func btnSubmitAction(_ sender: Any) {
//        if let del = self.delegate {
//            del.navigaToPasswordResetVC()
//        }
        if appDelegate.checkNetworkAvail() {
            let web = WebApiClass()
            web.delegate = self
            let urlStr   = Url.URL_AddForgotPasswordApproval
            let url = getUrl(urlStr: urlStr, serverType: .serverApp)
             let param = ForgotPasswordModel.share.warpdata()
            println_debug(url)
            println_debug(param)
            web.genericClassReg1(url: url, param: param as AnyObject, httpMethod: "POST", mScreen: "AddForgotPasswordApproval")
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
        }else{
         
            DispatchQueue.main.async {
                alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img:#imageLiteral(resourceName: "r_user"))
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                })
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    func webResponse(withJson json: AnyObject, screen: String) {
        DispatchQueue.main.async {
            self.removeProgressView()
        }
        if screen == "AddForgotPasswordApproval"{
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                  
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug(dic)
                        if dic["Code"] as! NSInteger == 200 {
                            if let del = self.delegate {
                                del.navigaToPasswordResetVC()
                            }
                        }else {
                            showAlert(alertTitle: "", alertBody:"You entered incorrect details. Please try again with correct details".localized , alertImage: #imageLiteral(resourceName: "alert-icon"))
                            self.APIFailedWebService(failureFor: "Forgot Password")
                        }
                    }
                }
            } catch {
                showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))
                do {
                    if let data = json as? Data {
                        let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                        if let dic = dict as? Dictionary<String,AnyObject> {
                            println_debug(dic["Msg"] as! String)
                            if dic["Code"] as? NSNumber == 200 {
                                println_debug(dic)
                            }else {
                                showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))
                            }
                        }
                    }else {
                        showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))
                    }
                } catch {
                    showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))
                }
            }
        }else {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug(dic["Msg"] as! String)
                        if dic["Code"] as? NSNumber == 200 {
                            println_debug(dic)
                        }else {
                            showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))
                        }
                    }
                }else {
                    showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))
                }
            } catch {
                showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))
            }
        }
    }
    
    
    func APIFailedWebService(failureFor: String) {
        if appDelegate.checkNetworkAvail() {
            do {
                let web = WebApiClass()
                web.delegate = self
                let url = getUrl(urlStr: Url.URL_FailureTrackInfo, serverType: .serverApp)
                let param = ForgotPasswordModel.share.warpdata()
                let jsonData = try JSONSerialization.data(withJSONObject: param, options:JSONSerialization.WritingOptions(rawValue: 0))
                let dic = ["AppBuildNumber": buildNumber,"AppBuildVersion": buildVersion,"AppType": "1","CellTowerId": "","DeviceInfo": "deviceinfo","FailureStage": failureFor,"HasSimCard": "1","IsRoaming": "0","Latitude": UserModel.shared.lat,
                           "Longitude": UserModel.shared.long,"MobileNumber": ForgotPasswordModel.share.MobileNumber,"OsType": "1","Remarks": String(data: jsonData, encoding: .utf8)]
                println_debug(url)
                println_debug(dic)
                web.genericClassReg(url: url, param: dic as Dictionary<String, Any>, httpMethod: "POST", mScreen: "AddRegistraionFailureTrackInfo", hbData: nil, authToken: nil)
                DispatchQueue.main.async {
                    progressViewObj.showProgressView()
                }
            }catch {
                DispatchQueue.main.async {
                    self.removeProgressView()
                }
            }
        } else {
          
            DispatchQueue.main.async {
                alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img:#imageLiteral(resourceName: "r_user"))
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                })
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {

        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        }
    }
}

