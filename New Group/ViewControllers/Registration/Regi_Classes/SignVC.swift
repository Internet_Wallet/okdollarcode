//
//  SignVC.swift
//  OK
//
//  Created by Subhash Arya on 06/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol  SignVCDelegate{
    func updateMainUIUI()
    func showAdditionalInformationVC()
    func viewEndEditing()
    func presentSignatureVC()
}

class SignVC: OKBaseController,SignatureVCDelegate {
    
    var delegate : SignVCDelegate?
    var Yaxis = Int()
    var isNextViewHidden = true
    
    let gifManager = SwiftyGifManager(memoryLimit:6)

    
    @IBOutlet weak var imgreturn: UIImageView!
    @IBOutlet var imgSign: UIImageView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
     @IBOutlet weak var btnReset: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        if ok_default_language == "en" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue"), for: .normal)
        }else if ok_default_language == "my" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
        }
        lblHeader.attributedText = lblHeader.attributedStringFinal(str1: "Signature".localized)
        lblDescription.text = "Tap here to sign please use your index finger to draw signature".localized
        self.dHeight = 225.0
        loadGIF()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    
    @IBAction func btnSignAction(_ sender: Any) {
        if let del = delegate {
            del.viewEndEditing()
            del.presentSignatureVC()
        }
    }
    
    @IBAction func btnResetAction(_ sender: Any) {
        loadGIF()
        if isNextViewHidden {
           self.dHeight = 225.0
            runGuard()
        }
    }
    
    func setSignatureImage(image: UIImage) {
        imgreturn.isHidden = false
        imgreturn.image = image
        RegistrationModel.shared.signature = OKBaseController.imageTobase64(image: image)
        if isNextViewHidden {
            guard(self.delegate?.showAdditionalInformationVC() != nil)else {
                return
            }
          isNextViewHidden = false
        }
    }
    
    func loadGIF() {
        imgreturn.isHidden = true
        RegistrationModel.shared.signature = ""
        self.imgSign.gifImage = nil
        let gif = UIImage(gifName: "sign_demo")
        self.imgSign.setGifImage(gif, manager: gifManager)
    }
    
    func runGuard() {
        guard(self.delegate?.updateMainUIUI() != nil) else {
            return
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK", style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
}

