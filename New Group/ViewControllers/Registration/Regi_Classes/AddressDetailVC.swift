//
//  AddressDetailVC.swift
//  DemoUpdateProfile
//
//  Created by SHUBH on 11/7/17.
//  Copyright © 2017 SHUBH. All rights reserved.
//

import UIKit
import  Rabbit_Swift

protocol AddressDetailDelegate {
    func updateMainUIUI()
    func showAddressDivisionVC()
    func showAddressTownshipVC(locDetail :LocationDetail)
    func showSecurityVC()
    func showDemo(strURL : String, imgDemo : UIImage, demoTitile:String)
    func viewEndEditing()
}

class AddressDetailVC: OKBaseController ,StateDivisionVCDelegate,AddressTownshiopVCDelegate,UITextFieldDelegate,CurrentLocationVCDelegate{
    @IBOutlet var viewHousingIndustiralZoneName: UIView!
    @IBOutlet weak var viewTypeHere: UIView!
    @IBOutlet weak var viewDivisionState: UIView!
    @IBOutlet weak var viewTownship: UIView!
    @IBOutlet weak var viewCity: UIView!
    @IBOutlet weak var viewVillage: UIView!
    @IBOutlet weak var viewStreet: UIView!
    @IBOutlet weak var viewHouseDetails: UIView!
    @IBOutlet weak var viewHousingZone: UIView!
    @IBOutlet var viewAddressOption: UIView!
    
    @IBOutlet weak var btnHome: UIButton!
    @IBOutlet weak var btnOther: UIButton!
    @IBOutlet weak var btnBusiness: UIButton!
    
    @IBOutlet weak var tfStreetName: UITextField!
    @IBOutlet weak var tfVillageName: UITextField!
    @IBOutlet weak var tfHouseNo: UITextField!
    @IBOutlet weak var tfFloorNo: UITextField!
    @IBOutlet weak var tfRoomNo: UITextField!
    @IBOutlet weak var tfHousingZone: UITextField!
    
    @IBOutlet weak var btnDivison: UIButton!
    @IBOutlet weak var btnTownship: UIButton!
    @IBOutlet weak var btnCity: UIButton!
    
    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var imgBusiness: UIImageView!
    @IBOutlet weak var imgOther: UIImageView!
    @IBOutlet weak var topContraintsTypehere: NSLayoutConstraint!
    @IBOutlet weak var tfTypeHere: UITextField!
    
    @IBOutlet weak var topStreetContraints: NSLayoutConstraint!
    
    @IBOutlet weak var btnTypeHereClear: UIButton!
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet weak var lblHome: UILabel!
    @IBOutlet weak var lblBusiness: MarqueeLabel!
    @IBOutlet weak var lblOther: UILabel!
    @IBOutlet weak var lblHouseNo: UILabel!
    @IBOutlet weak var lblFloorNo: UILabel!
    @IBOutlet weak var lblRoomNo: UILabel!
    @IBOutlet weak var lblHousingZone: UILabel!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var lblDivision: MarqueeLabel! {
        didSet {
          lblDivision.attributedText = lblDivision.scrollMarqueLabel(title: "Select State / Division".localized)
        }
    }
    
    
    var timer = Timer()
    var seconds = 0
    var currentLocationVC : CurrentLocationVC?
    var appDelegate = UIApplication.shared.delegate as? AppDelegate
    var delegate : AddressDetailDelegate?
    var locationDetails: LocationDetail?
    var townShipDetail : TownShipDetail?
    var townshipCode = ""
    
    var isVillageTableCreated =  false
    var myTableView = UITableView()
    var arrFilterStreetData : [Dictionary<String,String>] = []
    var arrFilterVillageData : [Dictionary<String,String>] = []
    var DicTownship : Dictionary<String,String>?
    var isSearch : Bool!
    
    var isNextViewHidden = true
   // var isGooglAPIStart = false
    var addressArray = [addressModel]()
    let country = "Myanmar"
    var kbHeight : CGFloat?
    var CHARSET = ""
    var OTHERCHARSET = ""
    
    var isRoomViewHidden = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblHeader.attributedText = lblHeader.attributedStringFinal(str1: "Address Details".localized)
        lblHome.text = "Home".localized
        lblBusiness.text = "Business".localized
        lblOther.text = "Other".localized
        tfTypeHere.placeholder = "Enter here".localized
        btnTownship.setTitle("Select Township".localized, for: .normal)
        btnCity.setTitle("City/Region".localized, for: .normal)
        tfVillageName.placeholder = "Village Tract".localized
        tfStreetName.placeholder  = "Enter street".localized
        tfHousingZone.placeholder = "Housing / Industrial Zone Name".localized
        lblHouseNo.text = "House No.".localized
        lblFloorNo.text = "Floor No.".localized
        lblRoomNo.text = "Room No.".localized
        lblHousingZone.text = "Housing / Industrial Zone Name".localized
        
        isSearch = false
        self.tfHousingZone.delegate = self
        self.viewTypeHere.isHidden = true
        tfHouseNo.delegate = self
        tfFloorNo.delegate = self
        tfHousingZone.delegate = self
        tfRoomNo.delegate = self
        tfVillageName.delegate = self
        tfStreetName.delegate = self
        tfTypeHere.delegate = self
        self.dHeight = 80.0 // viewAddressOption.frame.origin.y + viewAddressOption.frame.height
        //  self.runGuard()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        //၁၂၃၄၅၆၇၈၉၀
        
        if ok_default_language == "en"{
                  btnReset.setBackgroundImage(UIImage(named: "reset_blue"), for: .normal)
            OTHERCHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789/-,:() "
                CHARSET =  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789/-,:() "
        }else if ok_default_language == "my" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
            OTHERCHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာ၁၂၃၄၅၆၇၈၉၀/-,:() "
            CHARSET =  "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာ၁၂၃၄၅၆၇၈၉၀/-,:() "
        }
 
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    // Mark: Keyboard Height
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            println_debug(keyboardHeight)
            kbHeight = keyboardHeight
        }
    }
    
    @IBAction func btnHomeAction(_ sender: Any) {
    
        if !(imgHome.image == UIImage(named: "select_radio")) {
            self.view.endEditing(true)
            
            imgHome.image = UIImage(named: "select_radio")
            imgBusiness.image = UIImage(named: "r_Unradio")
            imgOther.image = UIImage(named: "r_Unradio")
            self.topContraintsTypehere.constant = -57
            viewTypeHere.isHidden = true
            ShowCurrentLocationVC()
            clearAllField()
            RegistrationModel.shared.AddressType = lblHome.text!
        }
    }
    
    @IBAction func btnBusinessAction(_ sender: Any) {
        if !(imgBusiness.image == UIImage(named: "select_radio")) {
            
            imgHome.image = UIImage(named: "r_Unradio")
            imgBusiness.image = UIImage(named: "select_radio")
            imgOther.image = UIImage(named: "r_Unradio")
            self.topContraintsTypehere.constant = -57
            viewTypeHere.isHidden = true
            ShowCurrentLocationVC()
            clearAllField()
            self.view.endEditing(true)
            RegistrationModel.shared.AddressType = lblBusiness.text!
        }
    }
    
    @IBAction func btnOtherAction(_ sender: Any) {
        if !(imgOther.image == UIImage(named: "select_radio")) {
            self.view.endEditing(true)
            
            imgHome.image = UIImage(named: "r_Unradio")
            imgBusiness.image = UIImage(named: "r_Unradio")
            imgOther.image = UIImage(named: "select_radio")
            self.topContraintsTypehere.constant = 0
            viewTypeHere.isHidden = false
            
            ShowCurrentLocationVC()
            clearAllField()
            RegistrationModel.shared.AddressType = lblOther.text!
        }
    }
    
    @IBAction func btnTypeHereClearAction(_ sender: Any) {
        btnTypeHereClear.isHidden = true
        tfTypeHere.text = ""
    }
    @IBAction func btnSelectDivisionStateAction(_ sender: Any) {
        
        btnTownship.setTitle("Select Township".localized, for: .normal)
        btnCity.setTitle("City/Region".localized, for: .normal)
        tfVillageName.text = ""
        tfStreetName.text = ""
        arrFilterStreetData.removeAll()
        arrFilterVillageData.removeAll()
        tfTypeHere.resignFirstResponder()
        
        guard (self.delegate?.showAddressDivisionVC() != nil) else {
            return
        }
    }
    
    @IBAction func btnSelectTownshipAction(_ sender: Any) {
        tfTypeHere.resignFirstResponder()
        if lblDivision.text == "Select State / Division".localized {
            showAlert(alertTitle: "", alertBody: "Select State / Division".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
        }else {
            guard (self.delegate?.showAddressTownshipVC(locDetail: locationDetails!) != nil) else {
                return
            }
        }
    }
    
    @IBAction func btnCityAction(_ sender: Any) {
    }
    
    func setDivisionStateName(SelectedDic: LocationDetail) {
        RegistrationModel.shared.State = SelectedDic.stateOrDivitionCode
        if ok_default_language == "my" {
            //btnDivison.setTitle(SelectedDic.stateOrDivitionNameMy, for: .normal)
            lblDivision.text = SelectedDic.stateOrDivitionNameMy
        }else {
            lblDivision.text = SelectedDic.stateOrDivitionNameEn
           // btnDivison.setTitle(SelectedDic.stateOrDivitionNameEn, for: .normal)
        }
        RegistrationModel.shared.Township = ""
        locationDetails = SelectedDic
        tfVillageName.text = ""
        tfStreetName.text = ""
        RegistrationModel.shared.VillageName = ""
        RegistrationModel.shared.Address2 = ""
        if self.dHeight! <= viewTownship.frame.origin.y + viewTownship.frame.height{
            self.dHeight = viewTownship.frame.origin.y + viewTownship.frame.height
            runGuard()
        }
    }
    
    func setTownshipCityNameFromDivison(Dic: TownShipDetailForAddress) {
        self.setAddressFromDivisionAndTownship(dic: Dic)
    }
    
    func setTownshipCityName(cityDic : TownShipDetailForAddress) {
        self.setAddressFromDivisionAndTownship(dic: cityDic)
    }
    
    func setAddressFromDivisionAndTownship( dic : TownShipDetailForAddress) {
        tfVillageName.text = ""
        tfStreetName.text = ""
        
        townshipCode = dic.townShipCode
        RegistrationModel.shared.Township = dic.townShipCode
        RegistrationModel.shared.VillageName = ""
        RegistrationModel.shared.Address2 = ""
        
        btnTownship.setTitle("", for: UIControl.State.normal)
        btnCity.setTitle("", for: UIControl.State.normal)
        if ok_default_language == "my" {
            btnTownship.setTitle(dic.townShipNameMY, for: UIControl.State.normal)
            if dic.GroupName == "" {
                btnCity.setTitle(dic.cityNameMY, for: UIControl.State.normal)
            }else {
                if dic.isDefaultCity == "1" {
                    btnCity.setTitle(dic.DefaultCityNameMY, for: UIControl.State.normal)
                    RegistrationModel.shared.Address1 = dic.DefaultCityNameMY
                }else{
                    btnCity.setTitle(dic.cityNameMY, for: UIControl.State.normal)
                    RegistrationModel.shared.Address1 = dic.cityNameMY
                }
            }
        }else {
            btnTownship.setTitle(dic.townShipNameEN, for: UIControl.State.normal)
            if dic.GroupName == "" {
                btnCity.setTitle(dic.cityNameEN, for: UIControl.State.normal)
            }else {
                if dic.isDefaultCity == "1" {
                    btnCity.setTitle(dic.DefaultCityNameEN, for: UIControl.State.normal)
                    RegistrationModel.shared.Address1 = dic.DefaultCityNameEN
                }else {
                    btnCity.setTitle(dic.cityNameEN, for: UIControl.State.normal)
                    RegistrationModel.shared.Address1 = dic.cityNameEN
                }
            }
        }
        
        var isVillageFieldHidden = false
        if dic.GroupName.contains(find: "YCDC") || dic.GroupName.contains(find: "MCDC") {
            isVillageFieldHidden = true
        }
        
        if dic.cityNameEN == "Amarapura" || dic.cityNameEN == "Patheingyi" || dic.townShipNameEN == "PatheinGyi" {
            isVillageFieldHidden = false
        }
        
        DispatchQueue.main.async {
            if isVillageFieldHidden {
                self.topStreetContraints.constant = -57
            }else {
                self.topStreetContraints.constant = 0
            }
            self.runGuard()
            
            if self.dHeight! <= self.viewStreet.frame.origin.y + self.viewStreet.frame.height {
                self.dHeight = self.viewStreet.frame.origin.y + self.viewStreet.frame.height
            }else if self.dHeight! <= self.viewHouseDetails.frame.origin.y + self.viewHouseDetails.frame.height {
                self.dHeight = self.viewHouseDetails.frame.origin.y + self.viewHouseDetails.frame.height
            }else {
                self.dHeight = self.viewHousingIndustiralZoneName.frame.origin.y + self.viewHousingIndustiralZoneName.frame.height
            }
            self.runGuard()
        }

        self.callStreetVillageAPI(townshipcode: dic.townShipCode)
    }
    
    
    
    
    func ShowCurrentLocationVC() {
        currentLocationVC  = self.storyboard?.instantiateViewController(withIdentifier: "CurrentLocationVC") as? CurrentLocationVC
        currentLocationVC?.view.frame = CGRect(x:0 , y:0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        currentLocationVC?.curLocaitonDelegate = self
        currentLocationVC?.modalPresentationStyle = .overCurrentContext
        self.present(currentLocationVC!, animated: false, completion: nil)
        
        
//        if let window = UIApplication.shared.keyWindow {
//            window.rootViewController?.addChild(currentLocationVC!)
//            window.addSubview(currentLocationVC!.view)
//            window.makeKeyAndVisible()
//        }
    }
    
    func SetAddress(Divison: LocationDetail, Township: TownShipDetailForAddress, City: String, Stret: String) {
        RegistrationModel.shared.State = Divison.stateOrDivitionCode
        RegistrationModel.shared.Township = Township.townShipCode
        //SUBHASH
        RegistrationModel.shared.Address2 = parabaik.uni(toZawgyi:Stret)
        RegistrationModel.shared.VillageName = ""
        townshipCode = Township.townShipCode
        locationDetails = Divison
      //  tfStreetName.text = parabaik.uni(toZawgyi: Stret)
         tfStreetName.text = Stret
        
        
        if ok_default_language == "my" {
            lblDivision.text = Divison.stateOrDivitionNameMy
            btnTownship.setTitle(Township.townShipNameMY, for: UIControl.State.normal)
            if Township.GroupName == "" {
                btnCity.setTitle(Township.cityNameMY, for: UIControl.State.normal)
            }else {
                if Township.isDefaultCity == "1" {
                    btnCity.setTitle(Township.DefaultCityNameMY, for: UIControl.State.normal)
                    RegistrationModel.shared.Address1 = Township.DefaultCityNameMY
                }else{
                    btnCity.setTitle(Township.cityNameMY, for: UIControl.State.normal)
                    RegistrationModel.shared.Address1 = Township.cityNameMY
                }
            }
        }else {
            lblDivision.text = Divison.stateOrDivitionNameEn
            btnTownship.setTitle(Township.townShipNameEN, for: UIControl.State.normal)
            if Township.GroupName == "" {
                btnCity.setTitle(Township.cityNameEN, for: UIControl.State.normal)
            }else {
                if Township.isDefaultCity == "1" {
                    btnCity.setTitle(Township.DefaultCityNameEN, for: UIControl.State.normal)
                    RegistrationModel.shared.Address1 = Township.DefaultCityNameEN
                }else {
                    btnCity.setTitle(Township.cityNameEN, for: UIControl.State.normal)
                    RegistrationModel.shared.Address1 = Township.cityNameEN
                }
            }
        }
        
        var isVillageFieldHidden = false
        if Township.GroupName.contains(find: "YCDC") || Township.GroupName.contains(find: "MCDC") {
            isVillageFieldHidden = true
        }
        if Township.cityNameEN == "Amarapura" || Township.cityNameEN == "Patheingyi" || Township.townShipNameEN == "PatheinGyi" {
            isVillageFieldHidden = false
        }
        
        DispatchQueue.main.async {
            if isVillageFieldHidden {
                self.topStreetContraints.constant = -57
            }else {
                self.topStreetContraints.constant = 0
            }
            self.runGuard()
            if Stret == "" {
                if self.isNextViewHidden{
                    self.dHeight = self.viewStreet.frame.origin.y + self.viewStreet.frame.height
                    self.runGuard()
                }
            }else {
                if self.isNextViewHidden{
                    self.isRoomViewHidden = false
                    self.dHeight = self.viewHouseDetails.frame.origin.y + self.viewHouseDetails.frame.height
                    self.runGuard()
                }else {
                    self.dHeight = self.viewHousingIndustiralZoneName.frame.origin.y + self.viewHousingIndustiralZoneName.frame.height
                    self.runGuard()
                }
            }
        }
        
        self.callStreetVillageAPI(townshipcode: Township.townShipCode)
    }
    
    
    func CancelLocationAddress() {
        if isNextViewHidden{
            if self.dHeight! <= viewDivisionState.frame.origin.y + viewDivisionState.frame.height {
                self.dHeight = viewDivisionState.frame.origin.y + viewDivisionState.frame.height
                runGuard()
            }
        }else {
            self.dHeight = viewHousingIndustiralZoneName.frame.origin.y + viewHousingIndustiralZoneName.frame.height
            runGuard()
        }
    }
    
    @IBAction func btnResetAction(_ sender: Any) {
        
        tfTypeHere.resignFirstResponder()
        imgHome.image = #imageLiteral(resourceName: "r_Unradio")
        imgBusiness.image = #imageLiteral(resourceName: "r_Unradio")
        imgOther.image = #imageLiteral(resourceName: "r_Unradio")
        btnTypeHereClear.isHidden = true
        clearAllField()
    }
    
    func clearAllField() {
        RegistrationModel.shared.AddressType = ""
        RegistrationModel.shared.State = ""
        RegistrationModel.shared.Township = ""
        RegistrationModel.shared.Address1 = ""
        RegistrationModel.shared.VillageName = ""
        RegistrationModel.shared.Address2 = ""
        RegistrationModel.shared.HouseBlockNo = ""
        RegistrationModel.shared.FloorNumber = ""
        RegistrationModel.shared.RoomNumber = ""
        RegistrationModel.shared.HouseName = ""
        
        tfTypeHere.text = ""
        lblDivision.text = "Select State / Division".localized
        btnTownship.setTitle("Select Township".localized, for: .normal)
        btnCity.setTitle("City/Region".localized, for: .normal)
        tfVillageName.text = ""
        tfStreetName.text = ""
        tfHouseNo.text = ""
        tfFloorNo.text = ""
        tfRoomNo.text = ""
        tfHousingZone.text = ""
        tfTypeHere.resignFirstResponder()
        self.view.endEditing(true)
        if let del = delegate {
            del.viewEndEditing()
        }
    }
    
    func setVillageName(villageName : String) {
        tfVillageName.text = villageName
       // RegistrationModel.shared.VillageName = parabaik.uni(toZawgyi: villageName)
        RegistrationModel.shared.VillageName = villageName
    }
    
    @IBAction func tfVillageEditChagned(_ sender: UITextField) {
        if (sender.text?.count)! > 0 {
            self.getSearchArrayVillageContains(sender.text!)
        }else{
            myTableView.isHidden = true
            myTableView.allowsMultipleSelection = true
            isSearch = false
        }
    }
    @IBAction func streetEditingChanged(_ sender: UITextField) {
        self.addressArray.removeAll()
        if (tfStreetName.text?.count)! > 0 {
            getAllAddressDetails(searchTextStr: tfStreetName.text!)
        }else {
            myTableView.isHidden = true
        }
    }
    
    
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == tfVillageName {
        }else if textField == tfStreetName {
          //  isGooglAPIStart = true
        }
        
        return true
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField == tfStreetName {
            
          //  isGooglAPIStart = false
            self.myTableView.allowsMultipleSelection = true
            if isNextViewHidden && tfStreetName.text?.count ?? 0 > 0 {
                if isVillageTableCreated {
                    myTableView.isHidden = true
                }
                
                if self.dHeight! <= viewHouseDetails.frame.origin.y + viewHouseDetails.frame.height {
                    // View showing till room No.
                    isRoomViewHidden = false
                    self.dHeight = viewHouseDetails.frame.origin.y + viewHouseDetails.frame.height
                    if UserDefaults.standard.bool(forKey: "reset_to_merchant") {
                    }else {
                         runGuard()
                    }
                }else {
                 UserDefaults.standard.set(false, forKey: "reset_to_merchant")
                }
            }
            removeSpaceAtLast(txtField: tfStreetName)
            setValueInRegistrationModel()
            myTableView.isHidden = true
            return true
        }else if textField == tfVillageName {
            
            if isVillageTableCreated {
                myTableView.isHidden = true
                self.myTableView.allowsMultipleSelection = true
            }
            removeSpaceAtLast(txtField: tfVillageName)
            setVillageName(villageName: tfVillageName.text!)
            return true
        }else if textField == tfRoomNo {
            if tfRoomNo.text!.count > 0 {
                self.dHeight = viewHousingIndustiralZoneName.frame.origin.y + viewHousingIndustiralZoneName.frame.height
                runGuard()
               
                if isNextViewHidden{
                    guard(self.delegate?.showSecurityVC() != nil)else{
                        return false
                    }
                    isNextViewHidden = false
                }
                removeSpaceAtLast(txtField: tfRoomNo)
            }
            setValueInRegistrationModel()
        }else if textField == tfHouseNo {
            if tfHouseNo.text!.count > 0 {
                self.dHeight = viewHousingIndustiralZoneName.frame.origin.y + viewHousingIndustiralZoneName.frame.height
                runGuard()
       
                if isNextViewHidden{
                    guard(self.delegate?.showSecurityVC() != nil)else{
                        return false
                    }
                    isNextViewHidden = false
                }
            removeSpaceAtLast(txtField: tfHouseNo)
            }
        setValueInRegistrationModel()
        }else if textField == tfFloorNo {
            if tfFloorNo.text!.count > 0 {
                self.dHeight = viewHousingIndustiralZoneName.frame.origin.y + viewHousingIndustiralZoneName.frame.height
                runGuard()
         
                if isNextViewHidden {
                    guard(self.delegate?.showSecurityVC() != nil)else{
                        return false
                    }
                    isNextViewHidden = false
                }
            }
             removeSpaceAtLast(txtField: tfFloorNo)
        setValueInRegistrationModel()
        }else if textField == tfHousingZone {
             removeSpaceAtLast(txtField: tfHousingZone)
            setValueInRegistrationModel()
            
        }else if textField == tfTypeHere{
             removeSpaceAtLast(txtField: tfTypeHere)
            if tfTypeHere.text?.count == 0 {
                RegistrationModel.shared.AddressType = lblOther.text!
            }else {
            RegistrationModel.shared.AddressType = self.tfTypeHere.text!
            }
        }
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        
        
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if range.location == 0 && string == " " {
            return false
        }
        if textField == tfHouseNo {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: OTHERCHARSET).inverted).joined(separator: "")) { return false }
            if text.count > 7 {
                return false
            }
        }else if textField == tfFloorNo {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: OTHERCHARSET).inverted).joined(separator: "")) { return false }
            if  text.count > 7  {
                return false
            }
        }else if textField == tfRoomNo {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: OTHERCHARSET).inverted).joined(separator: "")) { return false }
            if  text.count > 7  {
                return false
            }
            
        }else if textField == tfHousingZone {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
            if text.count > 30 {
                return false
            }
            if restrictMultipleSpaces(str: string, textField: tfHousingZone) {
                return true
            }else {
                if  textField.text?.last == " " {
                    textField.text?.removeLast()
                }
                return false
            }
        }else if textField  == tfStreetName {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
            if text.count > 30 {
                return false
            }
        }else if textField == tfVillageName {
            tfStreetName.text = ""
            RegistrationModel.shared.Address2 = ""
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
            if text.count > 30 {
                return false
            }
        }else if textField == tfTypeHere {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: OTHERCHARSET).inverted).joined(separator: "")) { return false }
            if text.count > 0 {
                btnTypeHereClear.isHidden = false
            }else {
                btnTypeHereClear.isHidden = true
            }
            if text.count > 50 {
                return false
            }
            if restrictMultipleSpaces(str: string, textField: tfTypeHere) {
                return true
            }else {
                if  textField.text?.last == " " {
                    textField.text?.removeLast()
                }
                return false
            }
        }
        return true
    }
    
    func getSearchArrayVillageContains(_ text : String) {
        //BurmeseName
        let village = VillageManager.shared.allVillageList
        if ok_default_language == "my" {
            let predicate : NSPredicate = NSPredicate(format: "BurmeseName CONTAINS[c] %@", text.lowercased())
            arrFilterVillageData = (village.villageArray as NSArray).filtered(using: predicate) as! [Dictionary<String, String>]
        }else {
        let predicate : NSPredicate = NSPredicate(format: "self.Name contains[c] %@", text.lowercased())
        arrFilterVillageData.removeAll()
        arrFilterVillageData = (village.villageArray as NSArray).filtered(using: predicate) as! [Dictionary<String, String>]
        }
        
        if arrFilterVillageData.count > 0 {
            isSearch = true
            if !isVillageTableCreated {
                showViewForVillage(tableviewTag: 1)
            }else {
                myTableView.isHidden = false
                myTableView.allowsMultipleSelection = false
            }
            self.myTableView.reloadData()
        }else{
            if isVillageTableCreated {
                myTableView.isHidden = true
                myTableView.allowsMultipleSelection = true
                isSearch = false
            }
        }
        myTableView.tag = 1
    }
    
    func showViewForVillage(tableviewTag : Int){
        if !isVillageTableCreated {
            myTableView = UITableView(frame: CGRect(x:10, y: 64, width: self.view.frame.width - 20, height: UIScreen.main.bounds.height - kbHeight! - 180))
            myTableView.layer.cornerRadius = 5
            myTableView.tableFooterView = UIView(frame: .zero)
            myTableView.register(UINib(nibName: "StreetCell", bundle: nil), forCellReuseIdentifier: "StreetCell")
            myTableView.dataSource = self
            myTableView.delegate = self
            myTableView.allowsMultipleSelection = false
            myTableView.tableFooterView = UIView.init(frame: CGRect.zero)
            
            myTableView.backgroundColor = UIColor.clear
            if let window = UIApplication.shared.keyWindow {
                window.addSubview(myTableView)
                window.makeKeyAndVisible()
            }
            isVillageTableCreated = true
        }
        myTableView.tag = tableviewTag
    }
    
    func getAllAddressDetails(searchTextStr : String) {
        
        var urlString = ""
        if ok_default_language == "my"{
            urlString = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(searchTextStr)&components=country:mm&language=my&key=\(googleAPIKey)"
        } else {
            urlString = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(searchTextStr)&components=country:mm&language=en&key=\(googleAPIKey)"
        }
        let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        let session = URLSession.shared
        if url != nil {
            let task = session.dataTask(with:url!) { (data, response, error) -> Void in
                if let data = data {
                    do {
                        let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                        if let dic = dict as? Dictionary<String,AnyObject> {
                            if let resultArray = dic["predictions"] as? [Dictionary<String,Any>] {
                                self.addressArray.removeAll()
                                println_debug(resultArray)
                                DispatchQueue.main.async {
                                    for dic in resultArray {
                                        self.addressArray.append(addressModel.init(dict: dic))
                                    }
                                    
                                    if self.addressArray.count > 0 {
                                        DispatchQueue.main.async {
                                            if !self.isVillageTableCreated {
                                                self.myTableView.isHidden = false
                                                self.showViewForVillage(tableviewTag: 2)
                                                self.myTableView.allowsMultipleSelection = false
                                                self.myTableView.reloadData()
                                            }else {
                                                self.myTableView.isHidden = false
                                                self.myTableView.allowsMultipleSelection = false
                                                self.myTableView.reloadData()
                                            }
                                        }
                                    }else {
                                        if self.isVillageTableCreated {
                                            DispatchQueue.main.async {
                                                self.myTableView.isHidden = true
                                                self.myTableView.allowsMultipleSelection = true
                                            }
                                        }
                                    }
                                    self.myTableView.tag = 2
                                }
                            }
                        }
                    } catch let error as NSError {
                        println_debug(error.localizedDescription)
                    }
                }else {
                    println_debug("API NOT CALLING")
                }
            }
            task.resume()
        }
    }
    
    
    
    func runGuard() {
        guard (self.delegate?.updateMainUIUI() != nil) else {
            return
        }
    }
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func setValueInRegistrationModel(){
        //ANTONY
        RegistrationModel.shared.Address2 = self.tfStreetName.text!
        RegistrationModel.shared.HouseBlockNo = self.tfHouseNo.text!
        RegistrationModel.shared.FloorNumber = self.tfFloorNo.text!
        RegistrationModel.shared.RoomNumber = self.tfRoomNo.text!
        RegistrationModel.shared.HouseName = self.tfHousingZone.text!
    }
    
    @IBAction func btnShowDemoAction(_ sender: Any) {
        if let del = delegate {
            var strURL = ""
            if ok_default_language == "my"{
                strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/f156ad76-b62a-406b-91fc-f1784d680815ReqDocumentsJuly_02_2018%2001_15_21%20PM.jpg";
            }else {
                strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/8f8ffbb7-d206-4864-a5e4-d94f7e64f619ReqDocumentsJuly_02_2018%2001_34_39%20PM.jpg";
            }
            del.showDemo(strURL:strURL , imgDemo:#imageLiteral(resourceName: "address_type") ,demoTitile: "Address Details".localized)        }
    }
    
    func callStreetVillageAPI(townshipcode : String){
        let urlStr   = Url.URL_VillageList + townshipcode
        let url = getUrl(urlStr: urlStr, serverType: .serverApp)
        let params = Dictionary<String,String>()
        TopupWeb.genericClass(url:url, param: params as AnyObject, httpMethod: "GET", handle: {response, isSuccess in
            if isSuccess {
                if let json = response as? Dictionary<String,Any> {
                    let response = json["Data"] as Any
                    let dic = OKBaseController.convertToDictionary(text: response as! String)
                    if let d = dic {
                        if let arrDic = d["Streets"] as? Array<Dictionary<String,String>> {
                            let streetList = StreetList()
                            streetList.isStreetList = true
                            streetList.streetArray = arrDic
                            VillageManager.shared.allStreetList = streetList
                            
                        }else{
                            let streetList = StreetList()
                            streetList.streetArray.removeAll()
                            streetList.isStreetList = false
                            VillageManager.shared.allStreetList = streetList
                        }
                        if let arrDic1 = d["Villages"] as? Array<Dictionary<String,String>>{
                            DispatchQueue.main.async {
                                let villageList = VillageList()
                                villageList.villageArray = arrDic1
                                VillageManager.shared.allVillageList = villageList
                                villageList.isVillageList = true
                                self.tfVillageName.isUserInteractionEnabled = true
                            }
                        }else{
                            DispatchQueue.main.async {
                                let villageList = VillageList()
                                villageList.villageArray.removeAll()
                                VillageManager.shared.allVillageList = villageList
                                villageList.isVillageList = false
                                self.tfVillageName.isUserInteractionEnabled = false
                            }
                        }
                        println_debug(VillageManager.shared.allVillageList)
                    }
                }
            }else {
                self.showAlert(alertTitle: "", alertBody: "Network error and try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
            }
        })
    }
    
    override var textInputMode: UITextInputMode? {
        let language = ok_default_language
        if language.isEmpty {
            return super.textInputMode
        } else {
            for tim in UITextInputMode.activeInputModes {
                if tim.primaryLanguage!.contains(language) {
                    return tim
                }
            }
            return super.textInputMode
        }
    }
    
    private func removeSpaceAtLast(txtField : UITextField){
        if txtField.text?.last == " " {
            txtField.text?.removeLast()
        }
    }
    

}

extension AddressDetailVC : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView.tag == 1 {
            let village = VillageManager.shared.allVillageList
            if isSearch! {
                return arrFilterVillageData.count
            }else{
                return village.villageArray.count
            }
        }else if tableView.tag == 2 {
                return self.addressArray.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : StreetCell = tableView.dequeueReusableCell(withIdentifier: "StreetCell") as! StreetCell
        cell.backgroundColor = UIColor(red: 236, green: 236, blue: 236, alpha: 1.0)
        if tableView.tag == 1 {
            cell.lblFullAdress.isHidden = true
            cell.lblStreetName.isHidden = true
            cell.lblVillage.isHidden = false
            if isSearch! {
                let dic = arrFilterVillageData[indexPath.row]
                if ok_default_language == "my" {
                    cell.lblVillage.text = dic["BurmeseName"]
                }else {
                    cell.lblVillage.text = dic["Name"]
                }
            }else {
                let village = VillageManager.shared.allVillageList
                let dic = village.villageArray[indexPath.row]
                cell.lblVillage.text = dic["Name"]
            }
        }else {
            cell.lblFullAdress.isHidden = false
            cell.lblStreetName.isHidden = false
            cell.lblVillage.isHidden = true
            if let obj = addressArray[safe: indexPath.row] {
                cell.lblStreetName.text = parabaik.uni(toZawgyi: obj.shortName) ?? ""
                cell.lblFullAdress.text = parabaik.uni(toZawgyi: obj.address) ?? ""
            }
        }
        return cell
            
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if tableView.tag == 1 {
            if isSearch! {
                let dic = arrFilterVillageData[indexPath.row]
                if ok_default_language == "my" {
                    tfVillageName.text = dic["BurmeseName"]
                    if let vName = dic["BurmeseName"] {
                        setVillageName(villageName:vName)
                    }
                }else{
                    tfVillageName.text = dic["Name"]
                    if let vName = dic["Name"] {
                        setVillageName(villageName:vName )
                    }
                }
                isSearch = false
            }else {
                let street = VillageManager.shared.allVillageList
                let dic = street.villageArray[indexPath.row]
                if ok_default_language == "my" {
                    tfVillageName.text = dic["BurmeseName"]
                    if let vName = dic["BurmeseName"] {
                        setVillageName(villageName:vName )
                    }
                }else{
                    tfVillageName.text = dic["Name"]
                    if let vName = dic["Name"] {
                        setVillageName(villageName:vName )
                    }
                }
            }
            tfVillageName.resignFirstResponder()
        }else{
            let dic = addressArray[indexPath.row]
            tfStreetName.text = Rabbit.uni2zg(dic.shortName ?? "")
            tfStreetName.resignFirstResponder()
            //isGooglAPIStart = false
            addressArray.removeAll()
            UserDefaults.standard.set(false, forKey: "shown_to_tfstreet")
        }
        self.myTableView.allowsMultipleSelection = true
        myTableView.isHidden = true
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 1{
            return 50
        }else {
            return 60
        }
    }
}

struct addressModel {
    var shortName : String?
    var address : String?
    init(dict : Dictionary<String, Any>)  {
        if let addressComponents = dict["structured_formatting"] as? Dictionary<String, Any>
        {
            if addressComponents.count > 0
            {
                self.shortName = addressComponents["main_text"] as? String ?? ""
            } else {
                self.shortName = ""
            }
        }
        self.address = dict["description"] as? String ?? ""
    }
}



class StreetCell : UITableViewCell {
    @IBOutlet weak var lblVillage: UILabel!{
        didSet{
            lblVillage.font = UIFont(name: appFont, size: 18.0)
        }
    }
    @IBOutlet weak var lblFullAdress: MarqueeLabel!{
        didSet{
            lblFullAdress.font = UIFont(name: appFont, size: 12.0)
        }
    }
    @IBOutlet weak var lblStreetName: MarqueeLabel!{
        didSet{
            lblStreetName.font = UIFont(name: appFont, size: 18.0)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblVillage.font = UIFont(name: appFont, size: 18)
        lblFullAdress.font = UIFont(name: appFont, size: 12)
        lblStreetName.font = UIFont(name: appFont, size: 18)
    }
}

