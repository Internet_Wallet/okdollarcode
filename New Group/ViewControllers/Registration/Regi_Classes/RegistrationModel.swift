//
//  RegistrationModel.swift
//  OK
//
//  Created by Subhash Arya on 13/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import AVFoundation

class RegistrationModel: NSObject {
    
    static let shared = RegistrationModel()
    var player: AVAudioPlayer?
    //20
    var CountryCode      : String = "" // registration country code
    var MobileNumber    : String = ""// Entered Phone NO
    
    var AccountType     : Int?//Merchent 0 - personal 1
    var Gender          : Bool? // Male- "true" , female - "false"
    var Name             : String = ""//"Mr,Dhruv"
    var DateOfBirth      : String = ""//"15-May-1989"
    var EmailId          : String = "" // ""
    var ProfilePicAwsUrl : String = "" //base 64 string
    //var ProfilePic       : String = ""
    var Father           : String = "" //
    
    var IDType           : String = "" // NRC - 01 , passport- 04
    var NRC              : String = "" // nrc no / passport no
    var IdExpDate        : String = "" // ""
    var IdPhotoAwsUrl    : String = "" //base 64 string
    var IdPhoto          : String = ""
    var IdPhoto1AwsUrl   : String = "" //base 64 string
    var IdPhoto1         : String = ""
    var CountryOfCitizen : String = "" // ""
    
    var BusinessName           : String = "" // ""
    var BusinessType           : String = "" // Sub-Category
    var BusinessCategory       : String = "" //Category
    var BusinessIdPhoto1AwsUrl : String = "" //base 64 string
    var BusinessIdPhoto1       : String = "" //base 64 string
    var BusinessIdPhoto2AwsUrl : String = "" //base 64 string
    var BusinessIdPhoto2       : String = "" //base 64 string
    //19
    var AddressType  : String = "" // Home , Business, Other
    var Address1 : String = "" // CityName
    var Address2 : String = "" // street name  - ""
    var State : String = "" // State Code
    var Township : String = "" //TownshipCode
    var VillageName : String = ""
    var HouseBlockNo : String = "" //Houuse No ""
    var HouseName : String = "" // Housing/ Zone Name
    var FloorNumber : String = "" // floor No
    var RoomNumber : String = "" // Room No
    
    var PasswordType : String = "" // 0 , 1
    var Password : String = ""//ask
    var ConfirmPassword : String = ""//ask
    var SecurityQuestionAnswer : String = ""
    var SecurityQuestionCode : String = "" // code
    
    var signatureAwsUrl : String = "" //base 64 string
    var signature       : String = ""
    
    var FBEmailId : String = "" //Stirng
    var CodeAlternate  : String = "" // country code "+95"
    var Phone : String = ""// alternate Number
    var PhoneNumber : String = "" // This param is to be used for update profile as Phone in Registration
    var CodeRecommended : String = "" //country code "+95"
    var Recommended : String = "" // Mobil No
    
    
    var AlternativeConfirmPhone : String = ""// alternate Number
    var ReferralConfirmPhone : String = ""
    
    var ShopPicOneAwsUrl : String = ""
    var ShopPicTwoAwsUrl : String = ""
    var AgentAuthCode : String = ""// Agent Code
    var IsValidateAgentOtp : String = ""
    //32
    var AppID : String = "" //vinu AppID = "com.cgm.OKDollar";
    var Country : String = "" //"Myanmar"
    var Car : String = ""//null
    var CarType : Int?//null -1
    var CellTowerID : String = ""//null
    var CloseTime : String = ""//null
    // var CountryCode : String = "" // registration country code
    var DeviceId : String = "" // UDID
    var DeviceInfo : Dictionary<String,Any>?//ask
    var Encrypted : String = ""//ask
    var GcmID  : String = ""//ask
    var IMEI : String = "" //null
    var Kickback : Bool? //ask
    var Language : String = "" // en , my
    var Latitude : String = "" //""
    var Longitude : String = "" //""
    var Loyalty : Bool? //ask
    var MSID : String = msid //from stirng file have to fetch telco code
    var Msisdn = [String]() //ask
    var OSType = 1 //0- android, 1- ios
    var OpenTime : String = "" //null
    var OsVersion : String = "" //current os version
    var ParentAccount : String = ""//ask
    var PaymentGateway : Bool? // false
    var PhoneModel : String = "" // current phone model
    var Phonebrand = "" //"iPhone"
    var RegistrationStatus = 1
    var IosBuildNumber = 57
    var SecureToken : String = "" //ask
    var SimID : String = "" //UDID
    var lType : String = "" // MER= 0 , SUBSBR=1
    //total 71
    
    var BankDetails = [String]()
    var ClosedDays = [String]()
    //73
    
    var LoginAppKey : String = ""
    var LoginIdentityType : String = ""
    var countryISO : String = ""
    var operatorName : String = ""
    //OCR Front
    var ocr_F_birth : String = ""
    var ocr_F_bloodGroup : String = ""
    var ocr_F_bloodlinesReligion : String = ""
    var ocr_F_className : String = ""
    var ocr_F_fatherName : String = ""
    var ocr_F_height : String = ""
    var ocr_F_issueDate : String = ""
    var ocr_F_name : String = ""
    var ocr_F_nrcid : String = ""
    var ocr_F_side : String = ""
    //OCR BACK
    var ocr_B_address : String = ""
    var ocr_B_cardID : String = ""
    var ocr_B_className : String = ""
    var ocr_B_nrcIdback : String = ""
    var ocr_B_profession : String = ""
    var ocr_B_side : String = ""
    
    func wrapDataModel() -> Data? {
        let accountType = (AccountType! as NSNumber).stringValue
        
        let carTypeStr = (CarType! as NSNumber).stringValue
        
        let gender = Gender! ? "1" : "0"
        
        let buildNumber = (IosBuildNumber as NSNumber).stringValue
        
        let kickback = "1"
        
        let loyalty = "1"
        
        let osType = "1"
        
        let paymentGateway = "0"
        
        let regStatus = "1"
        
        if lType == "AGENT" {
            IsValidateAgentOtp = "0"
        }else {
            IsValidateAgentOtp = ""
        }
        
        let deviceInfo = RegDeviceInfo.init(bssid: "BluetoothAddress", bluetoothAddress: "02:00:00:00:00:00", bluetoothName: "", connectedNetworkType: "WIFI", deviceSoftwareVersion: UIDevice.current.systemVersion, hiddenSsid: "0", ipAddress: "", linkSpeed: "1", macAddress: "", networkCountryIso: countryISO, networkId: MSID, networkOperator: MSID, networkOperatorName: operatorName, networkSignal: "1", networkType: "4G", phoneType: "GSM", simCountryIso: countryISO, simOperator: MSID, simOperatorName: operatorName, ssid: operatorName, isNetworkRoaming: "0")
        
        let ocrFront =  RegiOcrNrcFrontInfo.init(birth: ocr_F_birth, bloodGroup: ocr_F_bloodGroup, bloodlinesReligion: ocr_F_bloodlinesReligion, className: ocr_F_className, fatherName: ocr_F_fatherName, height: ocr_F_height, issueDate: ocr_F_issueDate, name: ocr_F_name, nrcid: ocr_F_nrcid, side: ocr_F_side)
        
        let ocrBack  = RegiOcrNrcBackInfo.init(address: ocr_B_address, cardID: ocr_B_cardID, className: ocr_B_className, nrcIdback: ocr_B_nrcIdback, profession: ocr_B_profession, side: ocr_B_side)

        // Added encodedString() To four more param : HouseBlockNo,RoomNumber,FloorNumber,SecurityQuestionAnswer and deleted for : State,Township
        let model = RegistrationModelDetail.init(accountType: accountType,
                                                 address1: Address1,
                                                 address2: Address2,
                                                 addressType: AddressType,
                                                 agentAuthCode: AgentAuthCode, appId: AppID, bankDetails: BankDetails, businessCategory: BusinessCategory, businessIdPhoto1AwsUrl: BusinessIdPhoto1, businessIdPhoto2AwsUrl: BusinessIdPhoto2,
                                                 businessName: BusinessName,
                                                 businessType: BusinessType, car: Car, carType: carTypeStr, cellTowerId: CellTowerID, closeTime: CloseTime, closedDays: ClosedDays, codeAlternate: CodeAlternate, codeRecommended: CodeRecommended, country: Country, countryCode: CountryCode, countryOfCitizen: CountryOfCitizen, dateOfBirth: DateOfBirth, deviceId: DeviceId, deviceInfo: deviceInfo, ocrNrcFrontInfo: ocrFront, ocrNrcBackInfo: ocrBack,emailId: EmailId, encrypted: Encrypted, fbEmailId: FBEmailId,
                                                 father: Father,
                                                 floorNumber: FloorNumber,
                                                 gcmId: GcmID, gender: gender,
                                                 houseBlockNo: HouseBlockNo,
                                                 houseName: HouseName,
                                                 idType: IDType, imei: IMEI, idExpDate: IdExpDate, idPhoto1AwsUrl: IdPhoto1, idPhotoAwsUrl: IdPhoto, iosBuildNumber: buildNumber, kickback: kickback, isValidateAgentOtp: IsValidateAgentOtp, language: Language, latitude: Latitude, loginAppKey: LoginAppKey, loginIdentityType: LoginIdentityType, longitude: Longitude, loyalty: loyalty, msid: MSID, mobileNumber: MobileNumber, msisdn: Msisdn,
                                                 nrc: NRC,
                                                 name: Name,
                                                 osType: osType, openTime: OpenTime, osVersion: OsVersion, parentAccount: ParentAccount, password: Password, passwordType: PasswordType, paymentGateway: paymentGateway, phone: Phone, phoneModel: PhoneModel, phonebrand: Phonebrand, profilePicAwsUrl: ProfilePicAwsUrl, recommended: Recommended, registrationStatus: regStatus,
                                                 roomNumber: RoomNumber,
                                                 secureToken: SecureToken, securityQuestionAnswer: SecurityQuestionAnswer,
                                                 securityQuestionCode: SecurityQuestionCode, simId: SimID,
                                                 state: State, shopPicOneAwsUrl: ShopPicOneAwsUrl, shopPicTwoAwsUrl: ShopPicTwoAwsUrl,
                                                 township: Township,
                                                 villageName: VillageName,
                                                 lType: lType, signatureAwsUrl: signatureAwsUrl)
        
        do {
            let data  =  try JSONEncoder().encode(model)
            return data
        } catch {
            println_debug(error)
        }
        
        return Data.init()


    }
    
    func wrapData() -> Dictionary<String, Any> {
        var dict = Dictionary<String, Any>()
         dict  =  ["AccountType":AccountType!,"Address1":Address1,"Address2":Address2,"AddressType":AddressType,"AgentAuthCode":AgentAuthCode,"AppID":AppID,"BankDetails":BankDetails,"BusinessCategory":BusinessCategory,"BusinessIdPhoto1AwsUrl":BusinessIdPhoto1,"BusinessIdPhoto2AwsUrl":BusinessIdPhoto2,"BusinessName":BusinessName,"BusinessType":BusinessType,"Car":Car,"CarType":CarType!,"CellTowerID":CellTowerID,"CloseTime":CloseTime,"ClosedDays":ClosedDays,"CodeAlternate":CodeAlternate,"CodeRecommended":CodeRecommended,"Country":Country,"CountryCode":CountryCode,"CountryOfCitizen":CountryOfCitizen,"DateOfBirth":DateOfBirth,"DeviceId":DeviceId,"DeviceInfo":DeviceInfo!,"EmailId":EmailId,"Encrypted":Encrypted,"FBEmailId":FBEmailId,"Father":Father,"FloorNumber":FloorNumber,"GcmID":GcmID,"Gender":Gender!,"HouseBlockNo":HouseBlockNo,"HouseName":HouseName,"IDType":IDType,"IMEI":IMEI,"IdExpDate":IdExpDate,"IdPhotoAwsUrl":IdPhoto,"IdPhoto1AwsUrl":IdPhoto1,"Kickback":Kickback!,"Language":Language,"Latitude":Latitude,"LoginAppKey":LoginAppKey,"LoginIdentityType":LoginIdentityType,"Longitude":Longitude,"Loyalty":Loyalty!,"MSID":MSID,"MobileNumber":MobileNumber,"Msisdn":Msisdn,"NRC":NRC,"Name":Name,"OSType":OSType,"OpenTime":OpenTime,"OsVersion":OsVersion,"ParentAccount":ParentAccount,"Password":Password,"PasswordType":PasswordType,"PaymentGateway":PaymentGateway!,"Phone":Phone,"PhoneModel":PhoneModel,"Phonebrand":Phonebrand,"ProfilePicAwsUrl":ProfilePicAwsUrl,"Recommended":Recommended,"RegistrationStatus":RegistrationStatus,"IosBuildNumber":IosBuildNumber,"RoomNumber":RoomNumber,"SecureToken":SecureToken,"SecurityQuestionAnswer":SecurityQuestionAnswer,"SecurityQuestionCode":SecurityQuestionCode,"SimID":SimID,"State":State,"Township":Township,"VillageName":VillageName,"lType":lType,"signatureAwsUrl":signatureAwsUrl]
        return dict
    }
    
    func returnDeepCopyString(_ value: String) -> String {
        let keyValue = String.init(format: "%@", value)
        return keyValue
    }
    
    
    func clearValues () {
        AccountType         =        0
        Gender              =        true
        Name                =        ""
        DateOfBirth         =        ""
        EmailId             =        ""
        ProfilePicAwsUrl    =        ""
        //ProfilePic          =        ""
        Father              =        ""
        
        IDType              =        ""
        NRC                 =        ""
        IdExpDate           =        ""
        IdPhotoAwsUrl       =        ""
        IdPhoto             =        ""
        IdPhoto1AwsUrl      =        ""
        IdPhoto1            =        ""
        CountryOfCitizen    =        ""
        
        BusinessName        =        ""
        BusinessType        =        ""
        BusinessCategory    =        ""
        BusinessIdPhoto1AwsUrl    =        ""
        BusinessIdPhoto1          =        ""
        BusinessIdPhoto2AwsUrl    =        ""
        BusinessIdPhoto2          =        ""
        
        AddressType         =        ""
        Address1            =        ""
        Address2            =        ""
        State               =        ""
        Township            =        ""
        VillageName         =        ""
        HouseBlockNo        =        ""
        HouseName           =        ""
        FloorNumber         =        ""
        RoomNumber          =        ""
        
        PasswordType        =        ""
        Password            =        ""
        SecurityQuestionAnswer =     ""
        SecurityQuestionCode   =     ""
        
        signatureAwsUrl     =        ""
        signature           =        ""
        
        FBEmailId           =        ""
        CodeAlternate       =        ""
        Phone               =        ""
        PhoneNumber         =        ""
        CodeRecommended     =        ""
        Recommended         =        ""
    }
    
    func setModelValues() {
       
    
        RegistrationModel.shared.AppID = "com.cgm.OKDollar"
        RegistrationModel.shared.Country = "Myanmar"
        RegistrationModel.shared.Car = ""
        RegistrationModel.shared.CarType = -1
        RegistrationModel.shared.CellTowerID = ""
        RegistrationModel.shared.CloseTime = ""
        
        RegistrationModel.shared.DeviceId = uuid
        RegistrationModel.shared.Encrypted = ""
        RegistrationModel.shared.GcmID = ""
        RegistrationModel.shared.IMEI = ""
        RegistrationModel.shared.Kickback = true
        RegistrationModel.shared.Loyalty = true
        RegistrationModel.shared.MSID = MSID
        
        RegistrationModel.shared.Msisdn = []
        RegistrationModel.shared.ClosedDays = []
        RegistrationModel.shared.BankDetails = []
        RegistrationModel.shared.OSType = 1
        RegistrationModel.shared.OpenTime = ""
        let floatVersion = (UIDevice.current.systemVersion as NSString).floatValue
        let stringFloat =  String(describing: floatVersion)
        RegistrationModel.shared.OsVersion = stringFloat
        RegistrationModel.shared.ParentAccount = ""
        RegistrationModel.shared.PaymentGateway = false
        //        RegistrationModel.shared.Phone = ""
        let deviceModel = String(UIDevice().type.rawValue)
        RegistrationModel.shared.PhoneModel = deviceModel
        RegistrationModel.shared.Phonebrand = "iPhone"
        RegistrationModel.shared.RegistrationStatus = 1
        RegistrationModel.shared.IosBuildNumber = 57
        RegistrationModel.shared.SecureToken = ""
        RegistrationModel.shared.SimID = uuid
        
       // RegistrationModel.shared.AgentAuthCode = ""
        
        RegistrationModel.shared.LoginAppKey = ""
        RegistrationModel.shared.LoginIdentityType = "1"
        
        RegistrationModel.shared.DeviceInfo = ["BSSID":"","BluetoothAddress":"02:00:00:00:00:00","BluetoothName":"",
                                               "ConnectedNetworkType":"WIFI","DeviceSoftwareVersion": UIDevice.current.systemVersion,"HiddenSSID":false,
                                               "IPAddress":"","LinkSpeed":1,"MACAddress":"",
                                               "NetworkCountryIso": countryISO,"NetworkID":MSID,"NetworkOperator":MSID,
                                               "NetworkOperatorName":operatorName,"NetworkSignal":1,"NetworkType":"4G",
                                               "PhoneType":"GSM","SIMCountryIso": countryISO,"SIMOperator":MSID,
                                               "SIMOperatorName":operatorName,"SSID":operatorName,"isNetworkRoaming":false] //as? Dictionary<String, Any>
    }
    
    
    
    func playSound() {
        guard let url = Bundle.main.url(forResource: "wellcome", withExtension: "mp3") else { return }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category(rawValue: convertFromAVAudioSessionCategory(AVAudioSession.Category.playback)), mode: AVAudioSession.Mode.default)
            try AVAudioSession.sharedInstance().setActive(true)
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            guard let player = player else { return }
            player.play()
        } catch let error {
            println_debug(error.localizedDescription)
        }
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
	return input.rawValue
}
