
//
//  AgentImageRequired.swift
//  OK
//
//  Created by Imac on 6/7/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol AgentImageRequiredDelegate {
    func showAgentShopImage()
    func showAddressMerchant()
}


class AgentImageRequired: OKBaseController,UploadImagesUPDelegate {
    var delegate: AgentImageRequiredDelegate?
    @IBOutlet weak var lblAgentImage: MarqueeLabel!
    @IBOutlet weak var lblFirstImage: UILabel!
    @IBOutlet weak var lblSecondImage: UILabel!
    
    @IBOutlet weak var imgAgentFirst: UIImageView!
    @IBOutlet weak var imgAgentSecond: UIImageView!
    var isNextViewshown: Bool?
    override func viewDidLoad() {
        super.viewDidLoad()
         self.isNextViewshown = false
        self.dHeight = 160.0
    }

    @IBAction func onClickImageAction(_ sender: UIButton) {
        if let del = delegate {
            del.showAgentShopImage()
        }
    }
    
    func sendImagesToMainController(images: [String]) {
        RegistrationModel.shared.ShopPicOneAwsUrl = images[0].replacingOccurrences(of: " ", with: "%20")
        RegistrationModel.shared.ShopPicTwoAwsUrl = images[1].replacingOccurrences(of: " ", with: "%20")
        DispatchQueue.main.async {
            if !(self.isNextViewshown ?? false) {
                if (RegistrationModel.shared.ShopPicOneAwsUrl != "") && (RegistrationModel.shared.ShopPicTwoAwsUrl != "") {
                    self.isNextViewshown = true
                    if let del = self.delegate {
                        del.showAddressMerchant()
                    }
                }
            }
            self.imgAgentFirst.sd_setImage(with: URL(string: RegistrationModel.shared.ShopPicOneAwsUrl), placeholderImage: UIImage(named: "nrc"))
            self.imgAgentSecond.sd_setImage(with: URL(string: RegistrationModel.shared.ShopPicTwoAwsUrl), placeholderImage: UIImage(named: "nrc"))
        }
    }
    
}
