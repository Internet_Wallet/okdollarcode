//
//  RegisterationFinalCodableModel.swift
//  OK
//
//  Created by Ashish on 11/20/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

struct RegistrationModelDetail: Codable {
    let accountType, address1, address2, addressType: String?
    let agentAuthCode, appId: String?
    let bankDetails: [String]?
    let businessCategory, businessIdPhoto1AwsUrl, businessIdPhoto2AwsUrl, businessName: String?
    let businessType, car, carType, cellTowerId: String?
    let closeTime: String?
    let closedDays: [String]?
    let codeAlternate, codeRecommended, country, countryCode: String?
    let countryOfCitizen, dateOfBirth, deviceId: String?
    let deviceInfo: RegDeviceInfo?
    let ocrNrcFrontInfo: RegiOcrNrcFrontInfo?
    let ocrNrcBackInfo: RegiOcrNrcBackInfo?
    let emailId, encrypted, fbEmailId, father: String?
    let floorNumber, gcmId, gender, houseBlockNo: String?
    let houseName, idType, imei, idExpDate: String?
    let idPhoto1AwsUrl, idPhotoAwsUrl, iosBuildNumber, kickback, isValidateAgentOtp: String?
    let language, latitude, loginAppKey, loginIdentityType: String?
    let longitude, loyalty, msid, mobileNumber: String?
    let msisdn: [String]?
    let nrc, name, osType, openTime: String?
    let osVersion, parentAccount, password, passwordType: String?
    let paymentGateway, phone, phoneModel, phonebrand: String?
    let profilePicAwsUrl: String?
    let recommended, registrationStatus, roomNumber, secureToken: String?
    let securityQuestionAnswer, securityQuestionCode, simId, state, shopPicOneAwsUrl, shopPicTwoAwsUrl: String?
    let township, villageName, lType: String?
    let signatureAwsUrl: String?
    
    enum CodingKeys: String, CodingKey {
        case accountType = "AccountType"
        case address1 = "Address1"
        case address2 = "Address2"
        case addressType = "AddressType"
        case agentAuthCode = "AgentAuthCode"
        case appId = "AppID"
        case bankDetails = "BankDetails"
        case businessCategory = "BusinessCategory"
        case businessIdPhoto1AwsUrl = "BusinessIdPhoto1AwsUrl"
        case businessIdPhoto2AwsUrl = "BusinessIdPhoto2AwsUrl"
        case businessName = "BusinessName"
        case businessType = "BusinessType"
        case car = "Car"
        case carType = "CarType"
        case cellTowerId = "CellTowerID"
        case closeTime = "CloseTime"
        case closedDays = "ClosedDays"
        case codeAlternate = "CodeAlternate"
        case codeRecommended = "CodeRecommended"
        case country = "Country"
        case countryCode = "CountryCode"
        case countryOfCitizen = "CountryOfCitizen"
        case dateOfBirth = "DateOfBirth"
        case deviceId = "DeviceId"
        case deviceInfo = "DeviceInfo"
        case ocrNrcFrontInfo = "OcrNrcFrontInfo"
        case ocrNrcBackInfo = "OcrNrcBackInfo"
        case emailId = "EmailId"
        case encrypted = "Encrypted"
        case fbEmailId = "FBEmailId"
        case father = "Father"
        case floorNumber = "FloorNumber"
        case gcmId = "GcmID"
        case gender = "Gender"
        case houseBlockNo = "HouseBlockNo"
        case houseName = "HouseName"
        case idType = "IDType"
        case imei = "IMEI"
        case idExpDate = "IdExpDate"
        case idPhoto1AwsUrl = "IdPhoto1AwsUrl"
        case idPhotoAwsUrl = "IdPhotoAwsUrl"
        case iosBuildNumber = "IosBuildNumber"
        case isValidateAgentOtp = "IsValidateAgentOtp"
        case kickback = "Kickback"
        case language = "Language"
        case latitude = "Latitude"
        case loginAppKey = "LoginAppKey"
        case loginIdentityType = "LoginIdentityType"
        case longitude = "Longitude"
        case loyalty = "Loyalty"
        case msid = "MSID"
        case mobileNumber = "MobileNumber"
        case msisdn = "Msisdn"
        case nrc = "NRC"
        case name = "Name"
        case osType = "OSType"
        case openTime = "OpenTime"
        case osVersion = "OsVersion"
        case parentAccount = "ParentAccount"
        case password = "Password"
        case passwordType = "PasswordType"
        case paymentGateway = "PaymentGateway"
        case phone = "Phone"
        case phoneModel = "PhoneModel"
        case phonebrand = "Phonebrand"
        case profilePicAwsUrl = "ProfilePicAwsUrl"
        case recommended = "Recommended"
        case registrationStatus = "RegistrationStatus"
        case roomNumber = "RoomNumber"
        case secureToken = "SecureToken"
        case securityQuestionAnswer = "SecurityQuestionAnswer"
        case securityQuestionCode = "SecurityQuestionCode"
        case simId = "SimID"
        case state = "State"
        case shopPicOneAwsUrl = "ShopPicOneAwsUrl"
        case shopPicTwoAwsUrl = "ShopPicTwoAwsUrl"
        case township = "Township"
        case villageName = "VillageName"
        case lType, signatureAwsUrl
    }
}

struct RegDeviceInfo: Codable {
    let bssid, bluetoothAddress, bluetoothName, connectedNetworkType: String?
    let deviceSoftwareVersion, hiddenSsid, ipAddress, linkSpeed: String?
    let macAddress, networkCountryIso, networkId, networkOperator: String?
    let networkOperatorName, networkSignal, networkType, phoneType: String?
    let simCountryIso, simOperator, simOperatorName, ssid: String?
    let isNetworkRoaming: String?
    
    enum CodingKeys: String, CodingKey {
        case bssid = "BSSID"
        case bluetoothAddress = "BluetoothAddress"
        case bluetoothName = "BluetoothName"
        case connectedNetworkType = "ConnectedNetworkType"
        case deviceSoftwareVersion = "DeviceSoftwareVersion"
        case hiddenSsid = "HiddenSSID"
        case ipAddress = "IPAddress"
        case linkSpeed = "LinkSpeed"
        case macAddress = "MACAddress"
        case networkCountryIso = "NetworkCountryIso"
        case networkId = "NetworkID"
        case networkOperator = "NetworkOperator"
        case networkOperatorName = "NetworkOperatorName"
        case networkSignal = "NetworkSignal"
        case networkType = "NetworkType"
        case phoneType = "PhoneType"
        case simCountryIso = "SIMCountryIso"
        case simOperator = "SIMOperator"
        case simOperatorName = "SIMOperatorName"
        case ssid = "SSID"
        case isNetworkRoaming
    }
}

struct RegiOcrNrcBackInfo: Codable {
    let address, cardID, className, nrcIdback: String
    let profession, side: String
    
    enum CodingKeys: String, CodingKey {
        case address = "Address"
        case cardID = "CardId"
        case className = "ClassName"
        case nrcIdback = "NrcIdback"
        case profession = "Profession"
        case side = "Side"
    }
}

struct RegiOcrNrcFrontInfo: Codable {
    let birth, bloodGroup, bloodlinesReligion, className: String
    let fatherName, height, issueDate, name: String
    let nrcid, side: String
    
    enum CodingKeys: String, CodingKey {
        case birth = "Birth"
        case bloodGroup = "BloodGroup"
        case bloodlinesReligion = "BloodlinesReligion"
        case className = "ClassName"
        case fatherName = "FatherName"
        case height = "Height"
        case issueDate = "IssueDate"
        case name = "Name"
        case nrcid = "Nrcid"
        case side = "Side"
    }
}
