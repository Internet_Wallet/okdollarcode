//
//  BusinessImagesRequired.swift
//  OK
//
//  Created by Imac on 4/8/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol BusinessImagesRequiredDelegate {
      func showAgentImageRequired()
}

class BusinessImagesRequired: OKBaseController,UploadImagesUPDelegate {
    var delegate: BusinessImagesRequiredDelegate?
    
    @IBOutlet weak var imgImage1: UIImageView!
    @IBOutlet weak var imgImage2: UIImageView!
    @IBOutlet weak var lblFront: UILabel!
    @IBOutlet weak var lblRear: UILabel!
    var isNextViewshown: Bool?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dHeight = 120
        self.isNextViewshown = false
        let image1 = RegistrationModel.shared.BusinessIdPhoto1.replacingOccurrences(of: " ", with: "%20")
        let image2 = RegistrationModel.shared.BusinessIdPhoto2.replacingOccurrences(of: " ", with: "%20")
        let urlFront = URL(string: image1)
        imgImage1.sd_setImage(with: urlFront, placeholderImage: UIImage(named: "nrc"))
        let urlRear = URL(string: image2)
        imgImage2.sd_setImage(with: urlRear, placeholderImage: UIImage(named: "nrc"))
        lblFront.text = "Front Page".localized
        lblRear.text = "Back Page".localized
    }
    
    @IBAction func onClickImageAction(_ sender: UIButton){
        DispatchQueue.main.async {
            let image = UIImage()
            var imgList = [UIImage]()
            let url1Exits =  RegistrationModel.shared.BusinessIdPhoto1
            let url2Exits =  RegistrationModel.shared.BusinessIdPhoto2
            if url1Exits != "" {
                if let img = image.convertURLToImage(str: url1Exits) {
                    imgList.append(img)
                }
            }
            if url2Exits != "" {
                if let img = image.convertURLToImage(str: url2Exits) {
                    imgList.append(img)
                }
            }
            let imageVC = UIStoryboard(name: "MyProfile", bundle: Bundle.main).instantiateViewController(withIdentifier: "UploadImagesUP")  as! UploadImagesUP
            if RegistrationModel.shared.IDType == "01" {
                imageVC.setMarqueLabelInNavigation(title: "Business Images".localized)
                imageVC.buttonName = ["Tap to take Photo of your Business's front side".localized, "Tap to take Photo of your Business's back side".localized]
                imageVC.buttonTitleName =  ["Front Page".localized,"Back Page".localized]
            }else {
                imageVC.setMarqueLabelInNavigation(title: "Business Images".localized)
                imageVC.buttonName = ["Tap to take Photo of your Business's front side".localized, "Tap to take Photo of your Business's back side".localized]
                imageVC.buttonTitleName =  ["Front Page".localized,"Back Page".localized]
            }
            imageVC.imageList = imgList
            imageVC.imagesURL = [RegistrationModel.shared.BusinessIdPhoto1,RegistrationModel.shared.BusinessIdPhoto2]
            imageVC.delegate = self
            self.navigationController?.pushViewController(imageVC, animated: true)
        }
    }
    
    func sendImagesToMainController(images: [String]) {
        RegistrationModel.shared.BusinessIdPhoto1 = images[0].replacingOccurrences(of: " ", with: "%20")
        RegistrationModel.shared.BusinessIdPhoto2 = images[1].replacingOccurrences(of: " ", with: "%20")
        DispatchQueue.main.async {
            
            
            if !(self.isNextViewshown ?? false) {
                if (RegistrationModel.shared.BusinessIdPhoto1 != "") && (RegistrationModel.shared.BusinessIdPhoto2 != "") {
                    self.isNextViewshown = true
                    if let del = self.delegate {
                        del.showAgentImageRequired()
                    }
                }
            }
            
            self.imgImage1.sd_setImage(with: URL(string: RegistrationModel.shared.BusinessIdPhoto1), placeholderImage: UIImage(named: "nrc"))
            self.imgImage2.sd_setImage(with: URL(string: RegistrationModel.shared.BusinessIdPhoto2), placeholderImage: UIImage(named: "nrc"))
            
        }
        
        
        
    }

}
