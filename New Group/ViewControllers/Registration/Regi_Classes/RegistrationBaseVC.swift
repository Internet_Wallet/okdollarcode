//
//  RegistrationBaseVC.swift
//  OK
//
//  Created by Subhash Arya on 01/02/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
protocol  RegistrationBaseVCDelegate : class{
    func updateLanguageModel(language : String)
}


class RegistrationBaseVC: OKBaseController,UITableViewDataSource,UITableViewDelegate,ProfilePhotoVCDelegate,AccountDelegate,IDProofDelegate,BusinessDetailDelegate,AddressDetailDelegate,SecurityVCDelegate,SignVCDelegate,ContactInfoPersonalDelegate,ContactInfoMerchantsDelegate,UIWebViewDelegate,OpenGallaryVCDelegate,WebServiceResponseDelegate,PhValidationProtocol,NRCPhotoRequiredDelegate,BusinessImagesRequiredDelegate,ReRegitrationSecurityVCDelegate,AgentImageRequiredDelegate{
 
    

    weak var delegate : RegistrationBaseVCDelegate?
    @IBOutlet weak var viewMyanmar: UIView! {
        
        didSet {
            self.viewMyanmar.backgroundColor = UIColor.lightGray
            self.viewMyanmar.layer.cornerRadius = 15.0
        }
        
    }
    @IBOutlet weak var viewEnglish: UIView!{
        didSet {
            self.viewEnglish.backgroundColor = UIColor.lightGray
            self.viewEnglish.layer.cornerRadius = 15.0
        }
    }
    var screenFrom = "Prelogin"
    @IBOutlet weak var tableViewRagi: UITableView!
    @IBOutlet weak var imgDemo: UIImageView!
    var lang : String?
    @IBOutlet weak var lblDemo: MarqueeLabel!
    @IBOutlet weak var webviewContainer: UIView!
    @IBOutlet weak var webview: UIWebView!
    @IBOutlet weak var lblMyanmar: UILabel!
    
    var controllers = [UIViewController]()
    var profilePhotoVC = ProfilePhotoVC()
    var accountTypeView = AccountTypeView()
     var iDProofVC = IDProofVC()
    var nrcRequired = NRCPhotoRequired()
    var businessDetailVC = BusinessDetailVC()
    var businessRequired = BusinessImagesRequired()
    var addressDetailVC =  AddressDetailVC()
    var securityVC =  SecurityPassQuestionVC()
    var securityQA = ReRegitrationSecurityVC()
    var signVC = SignVC()
   var infoMerchants = ContactInfoMerchants()
    var infoPersonal = ContactInfoPersonal()
    var agentImageRequired = AgentImageRequired()
    let lblMarque = MarqueeLabel()
    var passwordPatternViewControllerR = PasswordPatternViewControllerR()
    let tranView = UIView()
    let appDel = UIApplication.shared.delegate as! AppDelegate
    let strokeTextAttributes = [
        NSAttributedString.Key.font : UIFont.init(name: appFont, size: 18) ?? UIFont.systemFont(ofSize:18)
        ] as [NSAttributedString.Key : Any]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getDataFromApi()
        phNumValidationsFile = getDataFromJSONFile() ?? []
        webviewContainer.isHidden = true
        lblMyanmar.text = "ေဇာ္ဂ်ီ"
        if let language = lang {
            if language == "my" {
                viewMyanmar.backgroundColor = kYellowColor
                viewEnglish.backgroundColor =  UIColor.lightGray
            }else {
                viewEnglish.backgroundColor =  kYellowColor
                viewMyanmar.backgroundColor = UIColor.lightGray
            }
        }
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.bold), NSAttributedString.Key.foregroundColor: UIColor(red:17 , green: 39, blue: 141, alpha: 1.0)]
        self.navigationController?.navigationBar.backItem?.title = " "
        
        profilePhotoVC = (self.storyboard?.instantiateViewController(withIdentifier: "ProfilePhotoVC") as? ProfilePhotoVC)!
        profilePhotoVC.delegate = self
        controllers.append(profilePhotoVC)
        
        tableViewRagi.delegate = self
        tableViewRagi.dataSource = self

            UserDefaults.standard.set("NO", forKey: "ALL_FIELDS_FILLED")
            UserDefaults.standard.set(false, forKey: "PasswordScroll")
            UserDefaults.standard.set(false, forKey: "ProfilePicScroll")
        
        if UserDefaults.standard.bool(forKey: "RE_REGITRATION") {
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            if let language = lang {
                ReRegistrationModel.shared.Language = language
                UserDefaults.standard.set(lang, forKey:"currentLanguage")
                appDel.setSeletedlocaLizationLanguage(language: language)
                ok_default_language = language
                lblMarque.text = "Re-Regitration".localized
            }
        }else {
            lblMarque.text = "OK$ Registration".localized
        }
        self.navigationItem.titleView = nil
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: "Zawgyi-One", size: 18)
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
        if screenFrom == "Login" {
            self.navigationController?.isNavigationBarHidden = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func backAction(_ sender: Any) {
        webviewContainer.isHidden = true
        if  RegistrationModel.shared.Name == "" && RegistrationModel.shared.ProfilePicAwsUrl == "" {
              // Navigate to Help & Support screen
             self.navigationController?.popViewController(animated: true)
        }else {
           // Show alert
       
            alertViewObj.wrapAlert(title: "", body: "If you click back button, you have entered data will be automatically cleared and re-direct to OK $ new user registration screen.".localized, img:#imageLiteral(resourceName: "r_user"))
            alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
            })
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                RegistrationModel.shared.ProfilePicAwsUrl = ""
                self.profilePhotoVC.closeCamera()
            self.navigationController?.popViewController(animated: true)
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    @IBAction func closeWebviewAction(_ sender: Any) {
        webviewContainer.isHidden = true
        webview.loadHTMLString("", baseURL: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return controllers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = tableViewRagi.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
       
        let vc = controllers[indexPath.row]
        addChild(vc)
        vc.view.frame = cell.bounds
        vc.didMove(toParent: self)
        vc.view.layoutSubviews()
        vc.view.layoutIfNeeded()
        cell.addSubview(vc.view)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = controllers[indexPath.row].dHeight {
            return height
        }
        return 0.00
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func UICellUpdate() {
        UIView.setAnimationsEnabled(false)
        self.tableViewRagi.beginUpdates()
        self.tableViewRagi.layoutIfNeeded()
        self.tableViewRagi.layoutSubviews()
        self.tableViewRagi.endUpdates()
        if UserDefaults.standard.string(forKey: "ALL_FIELDS_FILLED") == "NO" {
            if !UserDefaults.standard.bool(forKey: "PasswordScroll") {
            UIView.setAnimationsEnabled(true)
                if !UserDefaults.standard.bool(forKey: "ProfilePicScroll") {
                    UIView.animate(withDuration: 1.5, animations: {
                        _ = Timer.scheduledTimer(withTimeInterval: 0.8, repeats: false) { timer in
                            let indexPath = IndexPath(row: self.controllers.count - 1, section: 0)
                            self.tableViewRagi.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                        }
                    })
                }
             //   UserDefaults.standard.set(false, forKey: "ProfilePicScroll")
                }
  
        }
    }
    
    func updateMainUIUI() {
        self.UICellUpdate()
    }


    func resetAllVC(handler:  @escaping (_ isSuccess: Bool) -> Void) {
        controllers.removeAll()
        RegistrationModel.shared.clearValues()
//        if let profile = profilePhotoVC as? ProfilePhotoVC {
        controllers.append(profilePhotoVC)
//        } else {
//        profilePhotoVC = (self.storyboard?.instantiateViewController(withIdentifier: "ProfilePhotoVC") as! ProfilePhotoVC)
//        profilePhotoVC.delegate = self
//            controllers.append(profilePhotoVC)
//        }
        self.tableViewRagi.reloadData()
        handler(true)
    }
    
    func showAccountTypeVC() {
        if !controllers.contains(accountTypeView){
            accountTypeView = (self.storyboard?.instantiateViewController(withIdentifier: "AccountTypeView") as? AccountTypeView)!
            accountTypeView.delegate = self
            controllers.append(accountTypeView)
            self.reloadTableView()
        }
    }
    
    func showProfilePhotoVC() {
        let sb = UIStoryboard(name: "MyProfile", bundle: nil)
        let cameraVC = sb.instantiateViewController(withIdentifier: "CameraVCUP") as! CameraVCUP
        cameraVC.delegate = profilePhotoVC
        self.navigationController?.pushViewController(cameraVC, animated: true)

    }
    
    func profilePhotoConfirm() {
        accountTypeView.profilePhotoConfirm()
    }
    
    func hideProfilePicOnReset(){
        profilePhotoVC.hideProfilePicOnResetPersonalInfo()
    }
    
   /* func showIDType(controller: AccountTypeView) {
        if !controllers.contains(iDProofVC){
            iDProofVC = (self.storyboard?.instantiateViewController(withIdentifier: "IDProofVC") as? IDProofVC)!
            iDProofVC.delegate = self
            controllers.append(iDProofVC)
            DispatchQueue.main.async {
                self.tableViewRagi.reloadData()
                if let cellAtFirst = self.tableViewRagi.cellForRow(at:IndexPath(row: 1, section: 0)) {
                    self.tableViewRagi.scrollToTop()
                    cellAtFirst.subviews.forEach({ (view) in
                        if view is UITextField {
                            view.becomeFirstResponder()
                        } else if let tf = controller.tfEmailID {
                            if !tf.isFirstResponder {
                                tf.becomeFirstResponder()
                            }
                        }
                    })
                }
            }
        }
    } */
  
    func showIDTyoeOnDone () {
        if !controllers.contains(iDProofVC){
            iDProofVC = (self.storyboard?.instantiateViewController(withIdentifier: "IDProofVC") as? IDProofVC)!
            iDProofVC.delegate = self
            controllers.append(iDProofVC)
            DispatchQueue.main.async {
                self.tableViewRagi.reloadData()
                UIView.setAnimationsEnabled(true)
                UIView.animate(withDuration: 1.5, animations: {
                    _ = Timer.scheduledTimer(withTimeInterval: 0.8, repeats: false) { timer in
                        let indexPath = IndexPath(row: self.controllers.count - 1, section: 0)
                        self.tableViewRagi.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                    }
                })
            }
        }
    }
    
    

    
    
    func resetIDProofDetails() {
        controllers.remove(at: 2)
        iDProofVC = (self.storyboard?.instantiateViewController(withIdentifier: "IDProofVC") as? IDProofVC)!
        iDProofVC.delegate = self
        controllers.insert(iDProofVC, at: 2)
        DispatchQueue.main.async {
            self.tableViewRagi.beginUpdates()
            let index = IndexPath(item: 2, section: 0)
            self.tableViewRagi.reloadRows(at: [index], with: .automatic)
            self.tableViewRagi.endUpdates()
            // self.tableViewRagi.reloadData()
        }
    }
    
    func updateForTypeID() {
        UIView.setAnimationsEnabled(false)
        self.tableViewRagi.beginUpdates()
        self.tableViewRagi.layoutIfNeeded()
        self.tableViewRagi.layoutSubviews()
        self.tableViewRagi.endUpdates()
    }
    
    
    func changeLanguageToEnglish(){
        //ukF.png
        //myanmarF
            self.updateLanguage(languageF: "en")
            if let del = self.delegate {
                del.updateLanguageModel(language: "en")
            }
            self.changeLaguageOfApplication()
    }
    
    func scrollTotop() {
        UIView.setAnimationsEnabled(true)
        UIView.animate(withDuration: 1.5, animations: {
            _ = Timer.scheduledTimer(withTimeInterval: 0.8, repeats: false) { timer in
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableViewRagi.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.top, animated: true)
            }
        })
        
        
    }
    
    func showNRCRequiredImagesVC() {
        if !controllers.contains(nrcRequired){
            nrcRequired = self.storyboard?.instantiateViewController(withIdentifier: "NRCPhotoRequired") as! NRCPhotoRequired
            self.nrcRequired.delegate = self
            self.controllers.append(self.nrcRequired)
            self.reloadTableView()
        }else {
            nrcRequired.dHeight = 120
        }
    UserDefaults.standard.set(true, forKey: "NRC_HIEGHT")
    }
    
    func showBusinessRequiredImages() {
        if !controllers.contains(businessRequired){
            businessRequired = self.storyboard?.instantiateViewController(withIdentifier: "BusinessImagesRequired") as! BusinessImagesRequired
            self.businessRequired.delegate = self
            self.controllers.append(self.businessRequired)
            self.reloadTableView()
        }else {
           // nrcRequired.dHeight = 120
        }
    }
    
    func resetRequiredImages() {
        businessRequired.imgImage1.image =  UIImage(named: "r_attachement")
        businessRequired.imgImage2.image =  UIImage(named: "r_attachement")
        RegistrationModel.shared.BusinessIdPhoto1 = ""
        RegistrationModel.shared.BusinessIdPhoto1 = ""
    }
    
    
    func showBusinessOrAddress() {
        if RegistrationModel.shared.AccountType == 0 {
            if !controllers.contains(businessDetailVC){
                businessDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "BusinessDetailVC") as! BusinessDetailVC
                businessDetailVC.delegate = self
                self.controllers.append(self.businessDetailVC)
                self.reloadTableView()
            }
        }else {
            if !controllers.contains(addressDetailVC){
                addressDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "AddressDetailVC") as! AddressDetailVC
                addressDetailVC.delegate = self
                self.controllers.append(addressDetailVC)
                self.reloadTableView()
            }
        }
    }
    
    func showAgentImageRequired() {
        if RegistrationModel.shared.lType == "AGENT" {
            if !controllers.contains(agentImageRequired){
                agentImageRequired = self.storyboard?.instantiateViewController(withIdentifier: "AgentImageRequired") as! AgentImageRequired
                agentImageRequired.delegate = self
                self.controllers.append(agentImageRequired)
                self.reloadTableView()
            }
        }else {
            showAddressMerchant()
        }

    }
    
    func showAgentShopImage() {
        DispatchQueue.main.async {
            let image = UIImage()
            var imgList = [UIImage]()
            let url1Exits =  RegistrationModel.shared.ShopPicOneAwsUrl
            let url2Exits =  RegistrationModel.shared.ShopPicTwoAwsUrl
            if url1Exits != "" {
                if let img = image.convertURLToImage(str: url1Exits) {
                    imgList.append(img)
                }
            }
            if url2Exits != "" {
                if let img = image.convertURLToImage(str: url2Exits) {
                    imgList.append(img)
                }
            }
            let imageVC = UIStoryboard(name: "MyProfile", bundle: Bundle.main).instantiateViewController(withIdentifier: "UploadImagesUP")  as! UploadImagesUP
            imageVC.setMarqueLabelInNavigation(title: "Agent shop image".localized)
            imageVC.buttonName = ["Tap to take Photo of Agent first shop image".localized, "Tap to take Photo of Agent second shop image".localized]
            imageVC.buttonTitleName =  ["First Image".localized,"Second Image".localized]
            imageVC.imageList = imgList
            imageVC.imagesURL = [RegistrationModel.shared.ShopPicOneAwsUrl,RegistrationModel.shared.ShopPicTwoAwsUrl]
            imageVC.delegate = self.agentImageRequired
            self.navigationController?.pushViewController(imageVC, animated: true)
        }
    }
    

    
    func showAddressMerchant() {
        if !controllers.contains(addressDetailVC){
            addressDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "AddressDetailVC") as! AddressDetailVC
            addressDetailVC.delegate = self
            self.controllers.append(addressDetailVC)
            self.reloadTableView()
        }
    }
    
 
    func showAddressDetailVC() {
        if !controllers.contains(addressDetailVC){
            addressDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "AddressDetailVC") as! AddressDetailVC
            addressDetailVC.delegate = self
            self.controllers.append(addressDetailVC)
            self.reloadTableView()
        }
    }

    fileprivate func reloadTableView()
    {
        DispatchQueue.main.async {
            self.tableViewRagi.reloadData()
            UIView.animate(withDuration: 1.5, animations: {
                _ = Timer.scheduledTimer(withTimeInterval: 0.8, repeats: false) { timer in
                    let indexPath = IndexPath(row: self.controllers.count - 1, section: 0)
                    self.tableViewRagi.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                }
            })
        }
    }
    
    func showSecurityVC() {
        if UserDefaults.standard.bool(forKey: "RE_REGITRATION") {
            if !controllers.contains(securityQA){
                securityQA = self.storyboard?.instantiateViewController(withIdentifier: "ReRegitrationSecurityVC") as! ReRegitrationSecurityVC
                securityQA.delegate = self
                self.controllers.append(securityQA)
                self.reloadTableView()
            }
        }else {
            if !controllers.contains(securityVC){
                securityVC = self.storyboard?.instantiateViewController(withIdentifier: "SecurityPassQuestionVC") as! SecurityPassQuestionVC
                securityVC.delegate = self
                self.controllers.append(securityVC)
                self.reloadTableView()
            }
        }
   
    }
    func showPatternVC() {

        passwordPatternViewControllerR = self.storyboard?.instantiateViewController(withIdentifier: "PasswordPatternViewControllerR") as! PasswordPatternViewControllerR
        passwordPatternViewControllerR.delegate = securityVC
        passwordPatternViewControllerR.mobilNumber = RegistrationModel.shared.MobileNumber
        self.navigationController?.pushViewController(passwordPatternViewControllerR, animated: false)
    }
    
    
    func showSignatureVC() {
        if !controllers.contains(signVC){
            signVC = self.storyboard?.instantiateViewController(withIdentifier: "SignVC") as! SignVC
            self.controllers.append(signVC)
            signVC.delegate = self
            self.reloadTableView()
        }
    }
    
    func showAdditionalInformationVC() {
       if RegistrationModel.shared.AccountType == 0 {
        if !controllers.contains(infoMerchants){
            infoMerchants = self.storyboard?.instantiateViewController(withIdentifier: "ContactInfoMerchants") as! ContactInfoMerchants
            infoMerchants.delegate = self
            self.controllers.append(infoMerchants)
            self.reloadTableView()
        }
       }else {
        if !controllers.contains(infoPersonal){
            infoPersonal = self.storyboard?.instantiateViewController(withIdentifier: "ContactInfoPersonal") as! ContactInfoPersonal
            infoPersonal.delegate = self
            self.controllers.append(infoPersonal)
            self.reloadTableView()
        }
        }
    }
    
    func resetPersonalInfo() {
        controllers.removeLast()
        infoPersonal = (self.storyboard?.instantiateViewController(withIdentifier: "ContactInfoPersonal") as? ContactInfoPersonal)!
        infoPersonal.delegate = self
        controllers.append(infoPersonal)
        self.reloadTableView()
    }
    
    func resetMerchantInfo() {
        controllers.removeLast()
        infoMerchants = (self.storyboard?.instantiateViewController(withIdentifier: "ContactInfoMerchants") as? ContactInfoMerchants)!
        infoMerchants.delegate = self
        controllers.append(infoMerchants)
        self.reloadTableView()
    }
    
    // ID Proof Delegtes
    func showTownShip() {
        accountTypeView.view.endEditing(true)
        let townVC = self.storyboard?.instantiateViewController(withIdentifier: "TownshipVC") as! TownshipVC
        townVC.townShipDelegate = iDProofVC
        self.navigationController?.pushViewController(townVC, animated: false)
        
    }
    func showCountry(){
         accountTypeView.view.endEditing(true)
        let sb = UIStoryboard(name: "Country", bundle: nil)
        let countryVC = sb.instantiateViewController(withIdentifier: "CountryViewController") as! CountryViewController
         countryVC.delegate = iDProofVC
        countryVC.regiCheck = "removeMyanmarCountry"
        countryVC.modalPresentationStyle = .fullScreen
        self.navigationController?.present(countryVC, animated: false, completion: nil)
    }
    // Business Detail Delegate
    
    func showBusinessCategory() {
        let businessCategoryVC  = self.storyboard?.instantiateViewController(withIdentifier: "BusinessCategoryVC") as! BusinessCategoryVC
        businessCategoryVC.bCatDelegate = businessDetailVC
        self.navigationController?.pushViewController(businessCategoryVC, animated: true)
    }
    
    func removeNRCRequriedOnClickReset() {
        nrcRequired.dHeight = 0
        nrcRequired.isNextViewshown = false
        nrcRequired.imgImage1.image = UIImage(named: "nrc")
        nrcRequired.imgImage2.image = UIImage(named: "nrc")
        UserDefaults.standard.set(false, forKey: "NRC_HIEGHT")
    }
    
    
    func showBusinessSubCategory(categories: CategoryDetail) {
        let businessSubCategoryVC  = self.storyboard?.instantiateViewController(withIdentifier: "BusinessSubCategoryVC") as! BusinessSubCategoryVC
        businessSubCategoryVC.businessSubCategoryVCDelegate = businessDetailVC
        businessSubCategoryVC.dicName = categories
        self.navigationController?.pushViewController(businessSubCategoryVC, animated: true)
    }
    
    //Address Detail Delegate
    
    func showAddressDivisionVC(){
        let stateDivisionVC  = self.storyboard?.instantiateViewController(withIdentifier: "StateDivisionVC") as! StateDivisionVC
        stateDivisionVC.stateDivisonVCDelegate = addressDetailVC
        self.navigationController?.pushViewController(stateDivisionVC, animated: true)
        }
    
    func showAddressTownshipVC(locDetail :LocationDetail){
        let addressTownshiopVC  = self.storyboard?.instantiateViewController(withIdentifier: "AddressTownshiopVC") as! AddressTownshiopVC
        addressTownshiopVC.addressTownshiopVCDelegate = addressDetailVC
        addressTownshiopVC.selectedDivState = locDetail
        self.navigationController?.pushViewController(addressTownshiopVC, animated: true)
    }
    
    // ContactInfoDelegates
    
    func showContactPickerView(viewConroller : UIViewController) {
        let nav = UitilityClass.openContact(multiSelection: false, viewConroller as? ContactPickerDelegate, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
        nav.modalPresentationStyle = .fullScreen
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    
    func navigateToLoginVC() {
        
        alertViewObj.wrapAlert(title: "", body:"You have successfully registered with OK$".localized , img: #imageLiteral(resourceName: "user"))
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            DispatchQueue.main.async {
                   let sb = UIStoryboard(name: "Login", bundle: nil)
                let loginVC = sb.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
//                loginVC?.agentCode = RegistrationModel.shared.MobileNumber
                println_debug(RegistrationModel.shared.MobileNumber)
                preLoginModel.agentCode = RegistrationModel.shared.MobileNumber
                loginVC?.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(loginVC!, animated: true)
                self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            }
        })
       
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
        }
    }
    
    // Security Detail Delegate

// Take Photo

    func takeImageFromCameraAndGallary(tagNo : Int) {
  
        if tagNo == 1 {
            alertViewObj.wrapAlert(title: "", body: "Pick the Image of Front side NRC".localized, img:#imageLiteral(resourceName: "camera"))
            alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
            })
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                self.showPhotoSelectionVC(tagNumber: 1, viewCon: self.iDProofVC)
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }else if tagNo == 2{
            alertViewObj.wrapAlert(title: "", body: "Pick the Image of Back side NRC".localized, img:#imageLiteral(resourceName: "camera"))
            alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
            })
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                self.showPhotoSelectionVC(tagNumber: 2, viewCon: self.iDProofVC)
            })
          
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }else if tagNo == 3 {
            self.showPhotoSelectionVC(tagNumber: 3, viewCon: iDProofVC)
        }
    }
    
    private func showPhotoSelectionVC(tagNumber : Int, viewCon: UIViewController) {
        let sb = UIStoryboard(name: "Registration" , bundle: nil)
        let open  = sb.instantiateViewController(withIdentifier: "OpenGallaryVC") as! OpenGallaryVC
        if let window = UIApplication.shared.keyWindow {
            window.rootViewController?.addChild(open)
            open.view.frame = .init(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
            open.delegate = self
            open.setTag(tg: tagNumber, vc: viewCon)
            window.addSubview(open.view)
            window.makeKeyAndVisible()
        }
    }
    func openGalleryOptioon(str: String, tagnumber: Int, viewController: UIViewController) {
        if str == "Gallery" {
            let imagePicker =  UIImagePickerController()
            imagePicker.delegate = viewController as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.sourceType = .photoLibrary
            imagePicker.view.tag = tagnumber
            self.present(imagePicker, animated: true, completion: nil)
        }else {
            let imagePicker =  UIImagePickerController()
            imagePicker.delegate = viewController as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.sourceType = .camera
            imagePicker.view.tag = tagnumber
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    
    
    
    func dismissPicker (){
        self.dismiss(animated: true, completion: {
            let indexPath = IndexPath(row: self.controllers.count - 1, section: 0)
            self.tableViewRagi.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
        })
    }
    
    func takePhoto(tagNo : Int){
        self.showPhotoSelectionVC(tagNumber: tagNo, viewCon: self.businessDetailVC)
//        let actionSheetController = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
//        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
//        }
//        let titFont = [NSAttributedStringKey.font: UIFont(name: "Zawgyi-One", size: 18.0)!]
//        let titAttrStringAction = NSMutableAttributedString(string: "Select Photo".localized, attributes: titFont)
//        actionSheetController.setValue(titAttrStringAction, forKey: "attributedTitle")
//
//        actionSheetController.addAction(cancelActionButton)
//        let cameraButton = UIAlertAction(title: "Camera", style: .default) { action -> Void in
//            let imagePicker =  UIImagePickerController()
//            imagePicker.delegate = self.businessDetailVC
//            imagePicker.sourceType = .camera
//            imagePicker.view.tag = tagNo
//            self.present(imagePicker, animated: true, completion: nil)
//        }
//        let galleryButton = UIAlertAction(title: "Gallery", style: .default) { action -> Void in
//            let imagePicker =  UIImagePickerController()
//            imagePicker.delegate = self.businessDetailVC
//            imagePicker.sourceType = .photoLibrary
//            imagePicker.view.tag = tagNo
//            self.present(imagePicker, animated: true, completion: nil)
//        }
//
//        actionSheetController.addAction(galleryButton)
//        actionSheetController.addAction(cameraButton)
//        self.present(actionSheetController, animated: true, completion: nil)
    }
    
     func selectCountry() {
        let sb = UIStoryboard(name: "Country", bundle: nil)
        let countryVC = sb.instantiateViewController(withIdentifier: "CountryViewController") as! CountryViewController
        countryVC.delegate = infoPersonal
        countryVC.modalPresentationStyle = .fullScreen
        self.navigationController?.present(countryVC, animated: false, completion: nil)
    }
    
    func  selectCountryForMer() {
    let sb = UIStoryboard(name: "Country", bundle: nil)
    let countryVC = sb.instantiateViewController(withIdentifier: "CountryViewController") as! CountryViewController
    countryVC.delegate = infoMerchants
    countryVC.modalPresentationStyle = .fullScreen
    self.navigationController?.present(countryVC, animated: false, completion: nil)
    }
    
    func viewEndEditing() {
        self.view.endEditing(true)
    }
    
    func presentSignatureVC(){
            let sb = UIStoryboard(name: "Signature", bundle: nil)
        let signatureVC  = sb.instantiateViewController(withIdentifier: "SignatureVC") as! SignatureVC
        signatureVC.delegate = self.signVC
        signatureVC.modalPresentationStyle = .fullScreen
       self.present(signatureVC, animated:false , completion: nil)
    }
    
    
// Language Changes
    @IBAction func btnLanguageFirstAction(_ sender: Any) {
          self.changeCurrentLanguage(language: "en")
        
    }
    
    @IBAction func btnLanguageSecondAction(_ sender: Any) {
        if controllers.count > 1 {
            alertViewObj.wrapAlert(title: "", body: "If you change Myanmar language all entered information will be deleted and you have to fill up all information again in Myanmar language only.".localized, img:#imageLiteral(resourceName: "r_user"))
            alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
            })
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                 self.changeCurrentLanguage(language: "my")
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }else {
            self.changeCurrentLanguage(language: "my")
        }
        

 
    }

    private func changeCurrentLanguage(language: String) {
           webviewContainer.isHidden = true
        if language == "my" {
            viewMyanmar.backgroundColor = kYellowColor
            viewEnglish.backgroundColor =  UIColor.lightGray
        }else {
            viewEnglish.backgroundColor =  kYellowColor
            viewMyanmar.backgroundColor = UIColor.lightGray
        }
        
        if let del = self.delegate {
            del.updateLanguageModel(language: language)
        }
        self.changeLaguageOfApplication()
        updateLanguage(languageF: language)
    }

    func updateLanguage(languageF : String) {
        RegistrationModel.shared.Language = languageF
        ok_default_language = languageF
        RegistrationModel.shared.clearValues()
        UserDefaults.standard.set(languageF, forKey:"currentLanguage")
        appDel.setSeletedlocaLizationLanguage(language: languageF)
        tranView.removeFromSuperview()
    }
    
    func changeLaguageOfApplication() {
        controllers.removeAll()
        if RegistrationModel.shared.Language == "my" {
            lblMarque.text = "OK$ အေကာင့္ မွတ္ပံုတင္ရန္"
        }else{
            lblMarque.text = "OK$ Registration"
        }
        profilePhotoVC = (self.storyboard?.instantiateViewController(withIdentifier: "ProfilePhotoVC") as? ProfilePhotoVC)!
        profilePhotoVC.delegate = self
        controllers.append(profilePhotoVC)
        UserDefaults.standard.set("NO", forKey: "ALL_FIELDS_FILLED")
        self.tableViewRagi.reloadData()
    }
    
    func addview() {
        // to Hide Language view on touch on screen
        tranView.frame = CGRect(x: 0, y: 124, width: self.view.frame.width, height: self.view.frame.height - 124)
        tranView.backgroundColor = UIColor.clear
        self.view.addSubview(tranView)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(nextStepTap))
        tapGestureRecognizer.numberOfTapsRequired = 1
        tranView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc public func nextStepTap(sender: UITapGestureRecognizer) {
    tranView.removeFromSuperview()
    }
    
    func showDemo(strURL : String, imgDemo : UIImage, demoTitile:String){
        self.view.endEditing(true)
        webviewContainer.isHidden = false
        webview.scalesPageToFit = true
        webview.delegate = self
        lblDemo.text = demoTitile.localized
        webview.loadRequest(URLRequest(url: URL(string: strURL)!))
    }
    
    public func webViewDidStartLoad(_ webView: UIWebView) {
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
        }
    }
    
    public func webViewDidFinishLoad(_ webView: UIWebView) {
        DispatchQueue.main.async {
            progressViewObj.removeProgressView()
        }
    }
    
    // MARK: Number Validation API Call
    func getDataFromApi() {
        phNumValidationsApi = []
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let floatVersion = (UIDevice.current.systemVersion as NSString).floatValue
            let stringFloat =  String(describing: floatVersion)
            let urlStr = String.init(format: Url.numberValidation, stringFloat, RegistrationModel.shared.MobileNumber, simid, appVersion, msid, "1")
            let getPhNumValidtnUrl = getUrl(urlStr: urlStr, serverType: .serverApp)
            let params = [String: String]()
            println_debug("REQUEST URL: \(getPhNumValidtnUrl)")
            web.genericClass(url: getPhNumValidtnUrl, param: params as AnyObject, httpMethod: "GET", mScreen: "PhNoValidation")
        } else {
            println_debug("No Internet")
        }
    }
        
    func webResponse(withJson json: AnyObject, screen: String) {
            if screen == "PhNoValidation" {
                guard let castedData = json as? Data else {
                    PTLoader.shared.hide()
                    println_debug("Data error")
                    return
                }
                do {
                    let decoder = JSONDecoder()
                    let response = try decoder.decode(MobNumValidationResponse.self, from: castedData)
                    if let responseStr = response.data {
                        println_debug(responseStr)
                        if responseStr == "null" {
                            println_debug("Null string from server")
                        } else {
                            guard let strData = responseStr.data(using: .utf8) else { return }
                            let decRes = try decoder.decode(MobNumDataResponse.self, from: strData)
                            if let version = decRes.version, version.count > 0 {
                                if let versionStr = version[0].validationJSON, versionStr != "null"  {
                                    guard let jsonData = versionStr.data(using: .utf8) else { return }
                                    let finalResponse = try decoder.decode(ValidationListResponse.self, from: jsonData)
                                    if let code = finalResponse.code, code == 200 {
                                        if let validations = finalResponse.validations, validations.count > 0 {
                                            phNumValidationsApi = validations
                                        }
                                    }
                                }
                            } else {
                                println_debug("No data from server")
                            }
                        }
                    } else {
                        PTLoader.shared.hide()
                        println_debug("No specified key from server")
                    }
                } catch _ {
                    println_debug("Try error")
                    PTLoader.shared.hide()
                }
            }
        }
    
}

