//
//  SecurityQuestionR.swift
//  OK
//
//  Created by SHUBH on 11/29/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol SecurityQuestionRDelegate{
    func SelectedQuestion(questionDic : Dictionary<String,AnyObject>)
}

class SecurityQuestionR: OKBaseController,WebServiceResponseDelegate,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet  weak var securityQuestionLabel: MarqueeLabel!
    var securityQuestionRDelegate : SecurityQuestionRDelegate?
    
    @IBOutlet weak var tableview: UITableView!
    var Question_arr = Array<Dictionary<String,AnyObject>>()
    override func viewDidLoad() {
        super.viewDidLoad()
        securityQuestionLabel.text = "Select Security Question".localized
        btnCancel.setTitle("Cancel".localized, for: .normal)
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self
            let urlStr   = Url.URL_Security_Question
            let url = getUrl(urlStr: urlStr, serverType: .serverApp)
            println_debug(url)
            let param = [String:String]() as AnyObject
            web.genericClass(url: url, param: param, httpMethod: "GET", mScreen: "mLogin")
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
        }else{
            DispatchQueue.main.async {
                progressViewObj.removeProgressView()
            }
            alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img:#imageLiteral(resourceName: "r_user"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        appDel.floatingButtonControl?.window.isHidden = true
//        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
//    }

    @IBAction func btnCancel(_ sender: Any) {
        self.view.removeFromSuperview()
        self.dismiss(animated: false, completion: nil)
    }
    
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return Question_arr.count
        
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:SecurityQuestionCell = tableView.dequeueReusableCell(withIdentifier:"SecurityQuestionCell") as! SecurityQuestionCell
        let dic = Question_arr[indexPath.row]
        if ok_default_language == "my"{
           cell.setSecurityQuestion(securityQuestion: dic["QuestionBurmese"] as! String)
        }else if ok_default_language == "en"{
           cell.setSecurityQuestion(securityQuestion: dic["Question"] as! String)
        }else {
           cell.setSecurityQuestion(securityQuestion: dic["QuestionBurmeseUniCode"] as! String)
        }
      
        return cell
    }

     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableview.deselectRow(at: indexPath, animated: true)
        guard (self.securityQuestionRDelegate?.SelectedQuestion(questionDic: Question_arr[indexPath.row]) != nil) else {
            return
        }
       // self.view.removeFromSuperview()
        self.dismiss(animated: false, completion: nil)
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        DispatchQueue.main.async {
            progressViewObj.removeProgressView()
        }
        do {
            if let data = json as? Data {
                let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let dic = dict as? Dictionary<String,AnyObject> {
                    println_debug(dic)
                    println_debug(dic["Msg"] as! String)
    
                    if dic["Msg"] as? String == "Success" {
                        if let Resopnse = dic["Data"] as? String {
                            let dic = OKBaseController.convertToArrDictionary(text: Resopnse)
                            if let log = dic as? Array<Dictionary<String,AnyObject>> {
                                    Question_arr = log
                            }
                                 DispatchQueue.main.async {
                                    self.tableview.delegate = self
                                    self.tableview.dataSource = self
                                    self.tableview.reloadData()
                            }
                        }
                    }
                    println_debug(Question_arr)
                }
            }
        } catch {
            alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img:#imageLiteral(resourceName: "r_user"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
}
