//
//  TermsConditionsRVC.swift
//  OK
//
//  Created by Subhash Arya on 20/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol  TermsConditionsRVCDelegate{
    func removeFromSuperVC(vc : UIViewController)
}


class TermsConditionsRVC: OKBaseController,WebServiceResponseDelegate {
var str = ""
    var delegate : TermsConditionsRVCDelegate?
    @IBOutlet var viewcard: CardDesignView!
    @IBOutlet var webView: UIWebView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    lblHeader.text = "Terms and Conditions".localized
    btnAccept.setTitle("Accept".localized, for: .normal)
    btnReject.setTitle("Reject".localized, for: .normal)
        
        if RegistrationModel.shared.AccountType == 1 {
            if ok_default_language == "my" {
               str = "okdollar_terms_personal_burmese"
            }else {
                str = "okdollar_terms_personal"
            }
        }else {
            if ok_default_language == "my" {
                  str = "okdollar_terms_merchant_burmese"
            }else{
                str = "okdollar_terms_merchant"
            }
        }
        do {
            guard let filePath = Bundle.main.path(forResource: str, ofType: "html")
                else {
                    return
            }
            let contents =  try String(contentsOfFile: filePath, encoding: .utf8)
            let baseUrl = URL(fileURLWithPath: filePath)
            //webView.stringByEvaluatingJavaScript(from: "document.getElementsByTagName('body')[0].style.fontFamily ='Zawgyi-One'")
            self.webView.loadHTMLString(contents as String, baseURL: baseUrl)
        }
        catch {
            print ("File HTML error")
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func btnRejectAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        //self.view.removeFromSuperview()
    }
    
    @IBAction func btnAcceptAction(_ sender: Any) {
        guard (self.delegate?.removeFromSuperVC(vc: self) != nil) else {
            return
        }
        self.dismiss(animated: false, completion: nil)
    }
    func webResponse(withJson json: AnyObject, screen: String) {
        DispatchQueue.main.async {
            progressViewObj.removeProgressView()
        }
        do {
            if let data = json as? Data {
                let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let dic = dict as? Dictionary<String,AnyObject> {
                    println_debug(dic)
                    println_debug(dic["Msg"] as! String)
                    if dic["Code"] as? NSNumber == 200 {
                        let sb = UIStoryboard(name: "Login", bundle: nil)
                        alertViewObj.wrapAlert(title: "", body:"You have successfully registered with OK$" , img:#imageLiteral(resourceName: "r_user"))
                        alertViewObj.addAction(title: "OK", style: .target , action: {
                            let loginVC = sb.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                            loginVC?.agentCode = RegistrationModel.shared.MobileNumber
                            self.navigationController?.pushViewController(loginVC!, animated: true)
                            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
                        })
                       
                        // Navigate to DashBoard
                    }else {
                        alertViewObj.wrapAlert(title: "", body:dic["Msg"] as! String , img:#imageLiteral(resourceName: "r_user"))
                        alertViewObj.addAction(title: "OK", style: .target , action: {
                        })
                        DispatchQueue.main.async {
                            alertViewObj.showAlert(controller: self)
                        }
                    }
                }
            }
        } catch {
            alertViewObj.wrapAlert(title: "", body: "Network error and try again", img:#imageLiteral(resourceName: "r_user"))
            alertViewObj.addAction(title: "OK", style: .target , action: {
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
}

