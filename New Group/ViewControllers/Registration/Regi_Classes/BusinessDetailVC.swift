//
//  BusinessDetailVC.swift
//  DemoUpdateProfile
//
//  Created by SHUBH on 11/7/17.
//  Copyright © 2017 SHUBH. All rights reserved.
//

import UIKit

protocol BusinessDetailDelegate {
    
    func showBusinessCategory()
    func showBusinessSubCategory(categories : CategoryDetail)
    func showAddressDetailVC()
    func showBusinessRequiredImages()
    func updateMainUIUI()
    func takePhoto(tagNo : Int)
    func dismissPicker()
    func showDemo(strURL : String, imgDemo : UIImage, demoTitile:String)
    func viewEndEditing()
    func resetRequiredImages()
}

class BusinessDetailVC: OKBaseController,UITextFieldDelegate,BusinessCategoryVCDelegate,BusinessSubCategoryVCDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
  
    @IBOutlet var btnDeletePhotoFirst: UIButton!
    @IBOutlet var btnAddBusinessPhotoSecond: UIButton!
    @IBOutlet weak var topBussi_Constraint: NSLayoutConstraint!
    @IBOutlet weak var btnBusinessCategory: UIButton!
    @IBOutlet weak var btnSelectBusinessSubCategory: UIButton!
    @IBOutlet weak var tfBusinessName: UITextField!
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var imgSubCategory: UIImageView!
    @IBOutlet weak var lblCatImg: UILabel!
    @IBOutlet weak var lblSubCatImg: UILabel!
    @IBOutlet var btnAttachLicensePhoto1: UIButton!
    @IBOutlet var btnAttachLicensePhoto2: UIButton!
    @IBOutlet var viewBusinessName: UIView!
    @IBOutlet var viewBusinessCategory: UIView!
    @IBOutlet var viewBusinessSubCategory: UIView!
    @IBOutlet weak var imgBusiness_L_Photo1: UIImageView!
    @IBOutlet weak var imgBusiness_L_Photo2: UIImageView!
    @IBOutlet var viewAttachBLicense1: UIView!
    @IBOutlet var viewAttachBLicense2: UIView!
    
    @IBOutlet weak var lblAttachBusinessL2: MarqueeLabel!
    @IBOutlet weak var btnDeleteBRI2: UIButton!
    
    var businessName : Dictionary<AnyHashable, AnyObject>?
    var businessImage : Dictionary<AnyHashable, AnyObject>?
    var delegate : BusinessDetailDelegate?
    var categoryDetail : CategoryDetail?
    
    @IBOutlet var lblSubBusinessCategory: MarqueeLabel!
    @IBOutlet var lblBusinessCategory: MarqueeLabel!
    @IBOutlet var lblBusinessLicensePhotoFirst: MarqueeLabel!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblAttachB1: MarqueeLabel!
     @IBOutlet weak var btnReset: UIButton!
    var isNextviewShow = false
    var BUSINESSNAMECHARSET = ""
    
    var isAttachBRI2 = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblHeader.attributedText = lblHeader.attributedStringFinal(str1: "Business Details".localized)
        tfBusinessName.placeholder = "Enter Business Name".localized
        lblBusinessCategory.text = "Select Business Category".localized
        lblSubBusinessCategory.text = "Select Business Sub-Category".localized
        lblAttachB1.text = "Attach Business Registration Image".localized
        lblAttachBusinessL2.text = "Attach Business Registration Image".localized
        
        self.tfBusinessName.delegate = self
        topBussi_Constraint.constant = -57
        btnDeletePhotoFirst.isHidden = true
        let tapLblBusinessCategory = UITapGestureRecognizer(target: self, action: #selector(BusinessDetailVC.BusinessCategoryAction))
        lblBusinessCategory.addGestureRecognizer(tapLblBusinessCategory)
        let tapLblSubBusinessCategory = UITapGestureRecognizer(target: self, action: #selector(BusinessDetailVC.SubBusinessCategoryAction))
        lblSubBusinessCategory.addGestureRecognizer(tapLblSubBusinessCategory)
        let idPicFirst = UITapGestureRecognizer(target: self, action: #selector(BusinessDetailVC.IdPhotoAction))
        lblBusinessLicensePhotoFirst.addGestureRecognizer(idPicFirst)
        self.dHeight = viewBusinessName.frame.height + viewBusinessName.frame.origin.y
        
        
        if ok_default_language == "en" {
             btnReset.setBackgroundImage(UIImage(named: "reset_blue"), for: .normal)
            BUSINESSNAMECHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
        }else if ok_default_language == "my" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
            BUSINESSNAMECHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာ "
        }
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BusinessDetailVC.imageTapped))
        imgBusiness_L_Photo1.isUserInteractionEnabled = true
        imgBusiness_L_Photo1.addGestureRecognizer(tapGestureRecognizer)
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(BusinessDetailVC.imageTapped2))
        imgBusiness_L_Photo2.isUserInteractionEnabled = true
        imgBusiness_L_Photo2.addGestureRecognizer(tapGestureRecognizer2)
        
        btnDeleteBRI2.isHidden = true
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    @objc func imageTapped() {
        if imgBusiness_L_Photo1.image != #imageLiteral(resourceName: "r_attachement") {
            self.showImageZoomVC(idImage: imgBusiness_L_Photo1.image!)
        }
    }
    @objc  func imageTapped2() {
        if imgBusiness_L_Photo2.image != #imageLiteral(resourceName: "r_attachement") {
            self.showImageZoomVC(idImage: imgBusiness_L_Photo2.image!)
        }
    }
    
    private func showImageZoomVC (idImage : UIImage) {
        if let del = self.delegate {
            del.viewEndEditing()
        }
        let sb = UIStoryboard(name: "Registration" , bundle: nil)
        let zoomImageVC  = sb.instantiateViewController(withIdentifier: "ZoomImageVC") as! ZoomImageVC
        if let window = UIApplication.shared.keyWindow {
            window.rootViewController?.addChild(zoomImageVC)
            zoomImageVC.view.frame = .init(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
            zoomImageVC.setImageToZoom(myImage: idImage)
            window.addSubview(zoomImageVC.view)
            window.makeKeyAndVisible()
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == tfBusinessName{
        // RegistrationModel.shared.BusinessName = parabaik.uni(toZawgyi:  tfBusinessName.text!)
        RegistrationModel.shared.BusinessName = tfBusinessName.text!
        }
        removeSpaceAtLast(txtField:tfBusinessName )
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
  if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: BUSINESSNAMECHARSET).inverted).joined(separator: "")) { return false }
        if range.location == 0 && string == " " {
            return false
        }
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
          if textField == tfBusinessName{
              //textField.text = text.uppercased()
            if text.count > 2 {
                if !isNextviewShow{
                    if self.dHeight! <= viewBusinessCategory.frame.height + viewBusinessCategory.frame.origin.y{
                        self.dHeight = viewBusinessCategory.frame.height + viewBusinessCategory.frame.origin.y
                        runGuard()
                    }
                }
            }
            if text.count > 50 {
                return false
            }
            if restrictMultipleSpaces(str: string, textField: tfBusinessName) {
                return true
            }else {
                if  textField.text?.last == " " {
                    textField.text?.removeLast()
                }
                return false
            }
        }
        return true
    }
    
    @IBAction func btnResetAction(_ sender: Any) {
        RegistrationModel.shared.BusinessName = ""
        RegistrationModel.shared.BusinessType = ""
        RegistrationModel.shared.BusinessCategory = ""
        RegistrationModel.shared.BusinessIdPhoto1 = ""
        RegistrationModel.shared.BusinessIdPhoto2 = ""
        RegistrationModel.shared.BusinessIdPhoto1AwsUrl = ""
        RegistrationModel.shared.BusinessIdPhoto2AwsUrl = ""
        tfBusinessName.resignFirstResponder()
        tfBusinessName.text = ""
        
        imgCategory.image = #imageLiteral(resourceName: "sub_categories")
        lblBusinessCategory.text = "Select Business Category".localized
        imgSubCategory.isHidden = false
        imgSubCategory.image = #imageLiteral(resourceName: "r_merchant")
        
        lblSubCatImg.isHidden = true
        lblSubCatImg.text = ""
        lblSubBusinessCategory.text = "Select Business Sub-Category".localized

        imgBusiness_L_Photo1.image = #imageLiteral(resourceName: "r_attachement")
        lblBusinessLicensePhotoFirst.text = "Attach Business Registration Image".localized
        btnAddBusinessPhotoSecond.tag = 1

        btnDeletePhotoFirst.isHidden = true
        btnAddBusinessPhotoSecond.isHidden = true
        btnAddBusinessPhotoSecond.setImage(#imageLiteral(resourceName: "r_add"), for: .normal)

        imgBusiness_L_Photo2.image = #imageLiteral(resourceName: "r_attachement")
        lblAttachBusinessL2.text = "Attach Business Registration Image".localized
        
        if let del = delegate {
            del.resetRequiredImages()
        }
        
        if isAttachBRI2 {
            self.dHeight = 268
            isAttachBRI2 = false
            runGuard()
        }
    }
    
    
    @IBAction func btnBusinessCatAction(_ sender: Any) {
    }
    @IBAction func btnBusinessSubCatAction(_ sender: Any) {
    }

    @objc func BusinessCategoryAction(sender:UITapGestureRecognizer) {
        tfBusinessName.resignFirstResponder()
        guard (self.delegate?.showBusinessCategory() != nil) else {
            return
        }

    }
    @objc func SubBusinessCategoryAction(sender:UITapGestureRecognizer) {
        tfBusinessName.resignFirstResponder()
        if lblBusinessCategory.text == "Select Business Category".localized {
            showAlert(alertTitle: "", alertBody: "Select Business Category".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
        }else{
            
            guard (self.delegate?.showBusinessSubCategory(categories: categoryDetail!) != nil) else {
                return
            }
        }
    }
    
    func setBusinessCategoryNameImage(Dicti : CategoryDetail) {
       
        RegistrationModel.shared.BusinessCategory = Dicti.categoryCode
        categoryDetail = Dicti
        viewBusinessSubCategory.isHidden = false
        viewAttachBLicense1.isHidden = false
        
        if RegistrationModel.shared.Language == "my" {
                lblBusinessCategory.text = Dicti.categoryBurmeseName
        }else {
              lblBusinessCategory.text = Dicti.categoryName
        }
        
    
        imgCategory.image = UIImage(named: Dicti.category_image)
        if !isNextviewShow{
            if self.dHeight! <= viewBusinessSubCategory.frame.origin.y + viewBusinessSubCategory.frame.height {
                self.dHeight = viewBusinessSubCategory.frame.origin.y + viewBusinessSubCategory.frame.height
                runGuard()
            }
        }

        lblSubBusinessCategory.text = "Select Business Sub-Category".localized
        RegistrationModel.shared.BusinessType = ""
        imgSubCategory.isHidden = false
        lblSubCatImg.isHidden = true
    }
    
    func setBusinessSubcategoryNameImage(sub_dictionary :SubCategoryDetail){
        RegistrationModel.shared.BusinessType = replaceSubCat(sbCat: sub_dictionary.subCategoryCode)
        if RegistrationModel.shared.Language == "my"{
            lblSubBusinessCategory.text = sub_dictionary.subCategoryBurmeseName
        }else {
            lblSubBusinessCategory.text = sub_dictionary.subCategoryName
        }
        lblSubCatImg.text = sub_dictionary.subCategoryEmoji
        lblSubCatImg.isHidden = false
        imgSubCategory.isHidden = true
        
        self.dHeight = viewAttachBLicense2.frame.origin.y + viewAttachBLicense2.frame.height
        runGuard()
        
        guard (self.delegate?.showBusinessRequiredImages() != nil) else {
            return
        }
        
        //        guard (self.delegate?.showAddressDetailVC() != nil) else {
        //            return
        //        }
    }
    
    func backFromBusinessSubCategoryVCWithNameImage(dicSubCate: SubCategoryDetail) {
        RegistrationModel.shared.BusinessType = replaceSubCat(sbCat: dicSubCate.subCategoryCode)
        if RegistrationModel.shared.Language == "my"{
             lblSubBusinessCategory.text = dicSubCate.subCategoryBurmeseName
        }else {
             lblSubBusinessCategory.text = dicSubCate.subCategoryName
        }
       
        lblSubCatImg.text = dicSubCate.subCategoryEmoji
        lblSubCatImg.isHidden = false
        imgSubCategory.isHidden = true
        viewAttachBLicense1.isHidden = false
        self.dHeight = viewAttachBLicense2.frame.origin.y + viewAttachBLicense2.frame.height
        runGuard()
        guard (self.delegate?.showBusinessRequiredImages() != nil) else {
            return
        }
//        guard (self.delegate?.showAddressDetailVC() != nil) else {
//            return
//        }
    }
    
    @IBAction func btnbtnBusinessLicense1Action(_ sender: Any) {
    }
    
    @objc func IdPhotoAction(sender:UITapGestureRecognizer) {
        guard (self.delegate?.takePhoto(tagNo : 1) != nil) else {
            return
        }
    }
    
    @IBAction func btnbtnBusinessLicense2Action(_ sender: Any) {
        guard (self.delegate?.takePhoto(tagNo : 2) != nil) else {
            return
        }
    }
    @IBAction func btnDeleteBusinessLicense1(_ sender: Any) {
        
        btnAddBusinessPhotoSecond.tag = 1
        if imgBusiness_L_Photo2.image == UIImage(named: "r_attachement"){ // blank
            imgBusiness_L_Photo1.image = UIImage(named: "r_attachement")
            lblBusinessLicensePhotoFirst.text = "Attach Business Registration Image".localized
            btnDeletePhotoFirst.isHidden = true
            btnAddBusinessPhotoSecond.isHidden = true
            RegistrationModel.shared.BusinessIdPhoto1 = ""
            RegistrationModel.shared.BusinessIdPhoto1AwsUrl = ""

        }else { // not blanck
            imgBusiness_L_Photo1.image = imgBusiness_L_Photo2.image
            lblBusinessLicensePhotoFirst.text = "Business Registration Image".localized
            RegistrationModel.shared.BusinessIdPhoto1 = RegistrationModel.shared.BusinessIdPhoto2
            imgBusiness_L_Photo2.image = UIImage(named: "r_attachement")
            lblAttachBusinessL2.text = "Attach Business Registration Image".localized
        }
        isAttachBRI2 = false
        self.dHeight = 268
        runGuard()
    }
    
    @IBAction func btnAddBusinessLicense1(_ sender: UIButton) {
        if sender.tag == 1 {
            if isAttachBRI2 {
                imgBusiness_L_Photo1.image = UIImage(named: "r_attachement")
                lblBusinessLicensePhotoFirst.text = "Attach Business Registration Image".localized
                btnAddBusinessPhotoSecond.isHidden = true
                btnDeletePhotoFirst.setImage(#imageLiteral(resourceName: "delete_red"), for: .normal)
                btnAddBusinessPhotoSecond.setImage(#imageLiteral(resourceName: "r_add"), for: .normal)
                RegistrationModel.shared.BusinessIdPhoto1 = ""
                RegistrationModel.shared.BusinessIdPhoto1AwsUrl = ""
               // topBussi_Constraint.constant = -57
                isAttachBRI2 = false
                self.dHeight = 268
            }else {
                btnDeleteBRI2.isHidden = true
                btnAddBusinessPhotoSecond.setImage(#imageLiteral(resourceName: "delete_red"), for: .normal)
                topBussi_Constraint.constant = 0
                viewAttachBLicense2.isHidden = false
                self.dHeight = 325
                isAttachBRI2 = true
            }
            btnDeletePhotoFirst.isHidden = true
            runGuard()
        }else if sender.tag == 2 {
            btnAddBusinessPhotoSecond.setImage(#imageLiteral(resourceName: "r_add"), for: .normal)
            btnAddBusinessPhotoSecond.tag = 1
            if imgBusiness_L_Photo2.image == UIImage(named: "r_attachement"){ // blank
                imgBusiness_L_Photo1.image = UIImage(named: "r_attachement")
                lblBusinessLicensePhotoFirst.text = "Attach Business Registration Image".localized
                btnDeletePhotoFirst.isHidden = true
                btnAddBusinessPhotoSecond.isHidden = true
                RegistrationModel.shared.BusinessIdPhoto1 = ""
                RegistrationModel.shared.BusinessIdPhoto1AwsUrl = ""
                
            }else { // not blanck
                imgBusiness_L_Photo1.image = imgBusiness_L_Photo2.image
                lblBusinessLicensePhotoFirst.text = "Business Registration Image".localized
                RegistrationModel.shared.BusinessIdPhoto1 = RegistrationModel.shared.BusinessIdPhoto2
                imgBusiness_L_Photo2.image = UIImage(named: "r_attachement")
                lblAttachBusinessL2.text = "Attach Business Registration Image".localized
                btnDeletePhotoFirst.isHidden = false
                btnDeletePhotoFirst.setImage(#imageLiteral(resourceName: "delete_red"), for: .normal)
                btnAddBusinessPhotoSecond.setImage(#imageLiteral(resourceName: "r_add"), for: .normal)
                btnDeleteBRI2.isHidden = true
            }
            isAttachBRI2 = false
            self.dHeight = 268
            runGuard()
            }
    }
    
    @IBAction func btnDeleteBusinessLicense2(_ sender: Any) {
        btnAddBusinessPhotoSecond.tag = 1
        topBussi_Constraint.constant = -57
        viewAttachBLicense2.isHidden = true
        imgBusiness_L_Photo2.image = UIImage(named: "r_attachement")
        lblAttachBusinessL2.text = "Attach Business Registration Image".localized
        RegistrationModel.shared.BusinessIdPhoto2 = ""
        btnAddBusinessPhotoSecond.setImage(#imageLiteral(resourceName: "r_add"), for: .normal)
        btnDeletePhotoFirst.isHidden = false
        btnDeleteBRI2.isHidden = true
        isAttachBRI2 = false
        self.dHeight = 268
        //viewAttachBLicense1.frame.origin.y + viewAttachBLicense1.frame.height
        runGuard()
    }
  
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        guard (self.delegate?.dismissPicker() != nil) else {
            return
        }
        println_debug(info)
        if picker.view.tag == 1{
            //Business License Photo 1
            btnDeletePhotoFirst.isHidden = false
            btnAddBusinessPhotoSecond.isHidden = false
            
            lblBusinessLicensePhotoFirst.text = "Business Registration Image".localized//getTimeStepString()
            self.imgBusiness_L_Photo1.image = (info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage)!
            RegistrationModel.shared.BusinessIdPhoto1 = OKBaseController.imageTobase64(image: imgBusiness_L_Photo1.image!)

        }else if picker.view.tag == 2{
            //Business License Photo 2
            btnAddBusinessPhotoSecond.tag = 2
            btnDeleteBRI2.isHidden = false
            lblAttachBusinessL2.text = "Business Registration Image".localized
            self.imgBusiness_L_Photo2.image = (info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage)!
            RegistrationModel.shared.BusinessIdPhoto2 = OKBaseController.imageTobase64(image: imgBusiness_L_Photo2.image!)
        }
        
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        guard (self.delegate?.dismissPicker() != nil) else {
            return
        }
    }
    
    func runGuard(){
        guard (self.delegate?.updateMainUIUI() != nil) else {
            return
        }
    }
    
    func replaceSubCat(sbCat : String) ->String {
        var st = sbCat
        if st.count == 1 {
            st = "0" + st
        }
        return st
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func getTimeStepString() -> String {
        let currentDate = Date()
        let since1970 = currentDate.timeIntervalSince1970
        return String(since1970)
    }
    
    override var textInputMode: UITextInputMode? {
        let language = ok_default_language
        if language.isEmpty {
            return super.textInputMode
        } else {
            for tim in UITextInputMode.activeInputModes {
                if tim.primaryLanguage!.contains(language) {
                    return tim
                }
            }
            return super.textInputMode
        }
    }

    @IBAction func btnShowDemoAction(_ sender: Any) {
        if let del = delegate {
            var strURL = ""
            if ok_default_language == "my"{
                strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/831520e3-ca48-4202-9347-6d76c1f32c6aReqDocumentsJuly_02_2018%2001_20_09%20PM.jpg";
            }else {
                strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/ccebdb05-bb2e-43da-8af4-293147597627ReqDocumentsJuly_02_2018%2001_33_23%20PM.jpg";
            }
            del.showDemo(strURL:strURL , imgDemo:#imageLiteral(resourceName: "business") ,demoTitile: "Business Details".localized)
    }
    }
  
    private func removeSpaceAtLast(txtField : UITextField){
        if txtField.text?.last == " " {
            txtField.text?.removeLast()
        }
    }
    
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
