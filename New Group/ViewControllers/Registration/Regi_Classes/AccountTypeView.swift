//
//  AccountTypeView.swift
//  DemoUpdateProfile
//
//  Created by SHUBH on 8/5/17.
//  Copyright © 2017 SHUBH. All rights reserved.
//

import UIKit
import AVFoundation

protocol AccountDelegate {
    func updateMainUIUI()
    //func showIDType(controller: AccountTypeView)
    func showProfilePhotoVC()
    func showIDTyoeOnDone ()
    func showDemo(strURL : String, imgDemo : UIImage, demoTitile:String)
    func viewEndEditing()
    func hideProfilePicOnReset()
}

class AccountTypeView: OKBaseController,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    let appDel = UIApplication.shared.delegate as! AppDelegate

    @IBOutlet var lbPersonalInfo: UILabel!
    @IBOutlet weak var viewPersonalInfo: UIView!
    @IBOutlet weak var viewYourName: UIView!
    @IBOutlet weak var viewDOB: UIView!
    @IBOutlet weak var viewTakeSelfie: UIView!
    @IBOutlet weak var viewFatherName: UIView!
    @IBOutlet weak var viewEmailID: UIView!
    @IBOutlet weak var viewMaleFemale: UIView!
    @IBOutlet weak var viewMaleCategory: UIView!
    @IBOutlet weak var viewFemaleCategory: UIView!
    @IBOutlet weak var viewFatherInitial: UIView!

    @IBOutlet weak var imgMaleFemale: UIImageView!

    @IBOutlet weak var btnTakeSalfie: UIButton!
    @IBOutlet weak var tfYourName: UITextField!
    @IBOutlet weak var tfDOB: UITextField!
    @IBOutlet weak var tfFatherName: UITextField!
    @IBOutlet weak var tfEmailID: UITextField!
    
    @IBOutlet weak var btnUserNameReset: UIButton!
    @IBOutlet weak var btnFatherNameReset: UIButton!
    
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnU: UIButton!
    @IBOutlet weak var btnMg: UIButton!
    @IBOutlet weak var btnMr: UIButton!
    @IBOutlet weak var btnDr: UIButton!
    @IBOutlet weak var btnDaw: UIButton!
    @IBOutlet weak var btnMa: UIButton!
    @IBOutlet weak var btnMs: UIButton!
    @IBOutlet weak var btnMrs: UIButton!
    @IBOutlet weak var btnMDr: UIButton!
    @IBOutlet weak var btnFU: UIButton!
    @IBOutlet weak var btnFMr: UIButton!
    @IBOutlet weak var btnFDr: UIButton!
    @IBOutlet weak var btnBFirst: UIButton!
    @IBOutlet weak var btnBSecond: UIButton!
    @IBOutlet weak var btnBThired: UIButton!
    @IBOutlet weak var viewBMaleFemaleCategory: UIView!
    @IBOutlet weak var viewBFatherInitial: UIView!
    @IBOutlet weak var btnBFatherFirst: UIButton!
    @IBOutlet weak var btnBFatherSecond: UIButton!
    @IBOutlet weak var btnMailIDClear: UIButton!
    @IBOutlet weak var topContraintsFatherView: NSLayoutConstraint!
    @IBOutlet weak var btnReset: UIButton!
    
    var delegate : AccountDelegate?
    var strDOB = ""
    var isYourNameKeyboardHidden = true
    var isFatherNameKeyboardHidden = true
    var isEmailShown = false
    var isNextViewHidden = true
    
    var preUserName = ""
    var preFatherName = ""
    var isProfileCameraShown = false
    var NAMECHARSET = ""
    var EMAILCHARSET = ""
    let dateFormatter = DateFormatter()
    override func viewDidLoad() {
        super.viewDidLoad()
        lbPersonalInfo.attributedText = lbPersonalInfo.attributedStringFinal(str1: "Personal Information".localized)
        
        if ok_default_language == "en" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue"), for: .normal)
        }else if ok_default_language == "my" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
        }
        self.tfYourName.placeholder = "Enter Your Name".localized
        self.tfDOB.placeholder = "Select Date of Birth".localized
        self.btnTakeSalfie.setTitle("Take Your Photo (Selfie)".localized, for: .normal)
        self.tfFatherName.placeholder = "Enter Father Name".localized
        self.tfEmailID.placeholder = "Enter E-Mail ID".localized
        self.btnMale.setTitle("Male".localized, for: .normal)
        self.btnFemale.setTitle("Female".localized, for: .normal)
        self.btnU.setTitle("U".localized, for: .normal)
        self.btnMg.setTitle("Mg".localized, for: .normal)
        self.btnMr.setTitle("Mr".localized, for: .normal)
        self.btnDr.setTitle("Dr".localized, for: .normal)
        self.btnDaw.setTitle("Daw".localized, for: .normal)
        self.btnMa.setTitle("Ma".localized, for: .normal)
        self.btnMs.setTitle("Ms".localized, for: .normal)
        self.btnMrs.setTitle("Mrs".localized, for: .normal)
        self.btnMDr.setTitle("Dr".localized, for: .normal)
        self.btnFU.setTitle("U".localized, for: .normal)
        self.btnFMr.setTitle("Mr".localized, for: .normal)
        self.btnFDr.setTitle("Dr".localized, for: .normal)
        self.btnBThired.setTitle("Dr".localized, for: .normal)
        self.btnBFatherFirst.setTitle("U".localized, for: .normal)
        self.btnBFatherSecond.setTitle("Dr".localized, for: .normal)
        
        if RegistrationModel.shared.ProfilePicAwsUrl == "" {
          //  self.viewFatherName.isHidden = true
          //  self.viewTakeSelfie.isHidden = true
        }else {
         //   self.viewTakeSelfie.isHidden = true
        }
        self.viewBMaleFemaleCategory.isHidden = true
        self.viewBFatherInitial.isHidden  = true
        self.tfYourName.delegate = self
        self.tfDOB.delegate = self
        self.tfFatherName.delegate = self
        self.tfEmailID.delegate = self
        btnUserNameReset.isHidden = true
        btnFatherNameReset.isHidden = true
      //  tfDOB.textAlignment = .center
       // tfEmailID.textAlignment = .center
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        
        let screenSize: CGRect = UIScreen.main.bounds
        let viewDOBKeyboard = UIView(frame: CGRect(x: 0, y: 0, width:screenSize.width , height: 40))
        viewDOBKeyboard.backgroundColor = UIColor(red: 244/255, green: 199/255, blue: 22/255, alpha: 1)
        
        let btnCancel  = UIButton(frame: CGRect(x: 10, y: 0, width:100, height: 40))
        btnCancel.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
        btnCancel.setTitle("Cancel".localized, for: UIControl.State.normal)
        btnCancel.addTarget(self, action:#selector(btnCancelDOBAction), for: UIControl.Event.touchUpInside)
        viewDOBKeyboard.addSubview(btnCancel)
        
        let btnDone  = UIButton(frame: CGRect(x: screenSize.width - 110, y: 0, width:100, height: 40))
        btnDone.setTitle("Done".localized, for: UIControl.State.normal)
        btnDone.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
        btnDone.addTarget(self, action:#selector(btnDoneDOBAction), for: UIControl.Event.touchUpInside)
        viewDOBKeyboard.addSubview(btnDone)
        tfDOB.inputAccessoryView = viewDOBKeyboard
        self.dHeight = viewYourName.frame.origin.y + viewYourName.frame.height
        tfYourName.autocapitalizationType = .allCharacters
        
        if ok_default_language == "en" {
            NAMECHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
        }else if ok_default_language == "my" {
            NAMECHARSET =  "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာ "
        }
        EMAILCHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890.@_"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }

    @IBAction func btnTakeSelfieAction(_ sender: Any) {
       //viewTakeSelfie.isUserInteractionEnabled = false
        guard (self.delegate?.showProfilePhotoVC() != nil) else {
            return
        }
        isProfileCameraShown = true
    }
    
    func profilePhotoConfirm() {
      //  viewFatherName.isHidden = false
        viewTakeSelfie.isUserInteractionEnabled = false
            self.topContraintsFatherView.constant = -57
            if self.isEmailShown {
               self.dHeight = 268
            }else{
                self.dHeight = 211
            }
    //  if device.type == .iPhone5 || device.type == .iPhone5S || device.type == .iPhoneSE || device.type == .iPhone5C{ }
        UserDefaults.standard.set(true, forKey: "ProfilePicScroll")
        self.runGuard()
        
//            if self.dHeight! <= self.viewFatherName.frame.origin.y + self.viewFatherName.frame.height{
//                self.dHeight = self.viewFatherName.frame.origin.y + self.viewFatherName.frame.height
//                self.runGuard()
//            }
        UserDefaults.standard.set(false, forKey: "ProfilePicScroll")
        viewTakeSelfie.isUserInteractionEnabled = true
    }
    
    @IBAction func btnUserNameResetAction(_ sender: UIButton) {
        tfYourName.text = ""
        viewMaleFemale.isHidden = true
        viewMaleCategory.isHidden = true
        viewFemaleCategory.isHidden = true
        viewBMaleFemaleCategory.isHidden = true
        isYourNameKeyboardHidden = true
        tfYourName.leftView = nil
        imgMaleFemale.image = UIImage(named : "r_user")
        tfYourName.resignFirstResponder()
        btnUserNameReset.isHidden = true
       // tfYourName.textAlignment = .center
        RegistrationModel.shared.Name = ""
    }
    
    @IBAction func btnFatherNameResetAction(_ sender: UIButton) {
        tfFatherName.leftView = nil
        isFatherNameKeyboardHidden  = true
        viewFatherInitial.isHidden = true
        tfFatherName.text = ""
        tfFatherName.resignFirstResponder()
        btnFatherNameReset.isHidden = true
       // tfFatherName.textAlignment = .center
        viewBFatherInitial.isHidden = true
        RegistrationModel.shared.Father = ""
    }

    
    @IBAction func btnResetAction(_ sender: UIButton) {
        preUserName = ""
        preFatherName = ""
      //  UserDefaults.standard.set(true, forKey: "ProfilePicScroll")
        self.btnUserNameResetAction(btnUserNameReset)
        strDOB = ""
        tfDOB.text = ""
       // tfDOB.textAlignment = .center
        self.btnFatherNameResetAction(btnFatherNameReset)
        self.mailIDClearAction(btnMailIDClear)
    
        RegistrationModel.shared.Name = ""
        RegistrationModel.shared.DateOfBirth = ""
        RegistrationModel.shared.Father = ""
        RegistrationModel.shared.EmailId = ""
        if let del = self.delegate {
            del.viewEndEditing()
            del.hideProfilePicOnReset()
        }
        if isProfileCameraShown {
            viewTakeSelfie.isHidden = false
           // viewFatherName.isHidden = false
            isProfileCameraShown = false
            self.topContraintsFatherView.constant = 0
            if isEmailShown {
                self.dHeight = 325
                self.runGuard()
            }else {
                self.dHeight = 268
                self.runGuard()
            }
            
//            DispatchQueue.main.async {

//                //                self.dHeight = self.viewFatherName.frame.origin.y + self.viewFatherName.frame.height
//                //                self.runGuard()
//                if self.dHeight! <= self.viewTakeSelfie.frame.origin.y + self.viewTakeSelfie.frame.height{
//                    self.dHeight = self.viewTakeSelfie.frame.origin.y + self.viewTakeSelfie.frame.height
//                    self.runGuard()
//                }
//            }
        }
    }
    
    @IBAction func btnMaleAction(_ sender: Any) {
        viewMaleFemale.isHidden = true
        imgMaleFemale.image = UIImage(named: "r_user")
        RegistrationModel.shared.Gender = true
        
        if RegistrationModel.shared.Language == "my" {
            btnBFirst.setTitle("U".localized, for: .normal)
            btnBSecond.setTitle("Mg".localized, for: .normal)
            self.viewBMaleFemaleCategory.isHidden = false
        }else {
            self.viewMaleCategory.isHidden = false
        }
    }
    
    @IBAction func btnFemaleAction(_ sender: Any) {
        viewMaleFemale.isHidden = true
        imgMaleFemale.image = UIImage(named: "r_female")
        RegistrationModel.shared.Gender = false
  
        if RegistrationModel.shared.Language == "my" {
            btnBFirst.setTitle("Daw".localized, for: .normal)
            btnBSecond.setTitle("Ma".localized, for: .normal)
            self.viewBMaleFemaleCategory.isHidden = false
        }else {
           viewFemaleCategory.isHidden = false
        }
       
    }
    @IBAction func btnMaleCatAction(_ sender: UIButton) {
        viewMaleCategory.isHidden = true
        viewMaleFemale.isHidden = true
        isYourNameKeyboardHidden = false
        btnUserNameReset.isHidden = false
        setPrefixName(prefixName: sender.title(for: .normal)!, txtField: tfYourName)
    }
    
    @IBAction func btnFemaleCatAction(_ sender: UIButton) {
        viewFemaleCategory.isHidden = true
        viewMaleFemale.isHidden = true
        isYourNameKeyboardHidden = false
        btnUserNameReset.isHidden = false
        setPrefixName(prefixName: sender.title(for: .normal)!, txtField: tfYourName)
    }
    
    @IBAction func btnBurmeseUserNameInitialAction(sender : UIButton) {
        viewFemaleCategory.isHidden = true
        viewMaleFemale.isHidden = true
        viewBMaleFemaleCategory.isHidden = true
        isYourNameKeyboardHidden = false
        btnUserNameReset.isHidden = false
        setPrefixName(prefixName: sender.title(for: .normal)!, txtField: tfYourName)
    }

    @IBAction func btnFatherInitialAction(_ sender: UIButton) {
        viewFatherInitial.isHidden = true
        viewBFatherInitial.isHidden = true
        isFatherNameKeyboardHidden = false
        btnFatherNameReset.isHidden = false
        setPrefixFatherName(prefixName: sender.title(for: .normal)!, txtField: tfFatherName)
    }
    
    @IBAction func btnBFatherInitialAction(_ sender: UIButton) {
        viewFatherInitial.isHidden = true
        viewBFatherInitial.isHidden = true
        isFatherNameKeyboardHidden = false
        btnFatherNameReset.isHidden = false
        setPrefixFatherName(prefixName: sender.title(for: .normal)!, txtField: tfFatherName)
    }
    
    func setPrefixName(prefixName : String, txtField : UITextField) {
        preUserName = prefixName
        let myString = prefixName
        let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: "Zawgyi-One", size: 18) ?? UIFont.systemFont(ofSize: 18.0) ])
        let lblName = UILabel()
        lblName.frame = CGRect(x: 0, y: -1, width:stringSize.width + 10 , height: 52)
        lblName.textAlignment = .left
        lblName.font =  UIFont(name: "Zawgyi-One", size: 18)
        lblName.text = myString
        let btnView = UIView()
        btnView.frame = CGRect(x: 0, y: 0, width: lblName.frame.width , height: 52)
        btnView.addSubview(lblName)
        txtField.leftView = btnView
        txtField.leftViewMode = UITextField.ViewMode.always
        txtField.becomeFirstResponder()
    }
    
    func setPrefixFatherName(prefixName : String, txtField : UITextField) {
        preFatherName = prefixName
        let myString = prefixName
        let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: "Zawgyi-One", size: 18) ?? UIFont.systemFont(ofSize: 18.0) ])
        let lblName = UILabel()
        lblName.frame = CGRect(x: 0, y: -1, width:stringSize.width + 10 , height: 52)
        lblName.textAlignment = .left
        lblName.font =  UIFont(name: "Zawgyi-One", size: 18)
        lblName.text = myString
        let btnView = UIView()
        btnView.frame = CGRect(x: 0, y: 0, width: lblName.frame.width , height: 52)
        btnView.addSubview(lblName)
        txtField.leftView = btnView
        txtField.leftViewMode = UITextField.ViewMode.always
        txtField.becomeFirstResponder()
    }
    
    
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        //ANTONY
        if textField == tfYourName {
            if tfYourName.text!.count > 0 {
                RegistrationModel.shared.Name = preUserName + "," + tfYourName.text!
            } else {
                RegistrationModel.shared.Name = ""
            }
            removeSpaceAtLast(txtField: tfYourName)
        } else if textField == tfFatherName {
            if tfFatherName.text!.count > 0 {
                RegistrationModel.shared.Father = preFatherName + "," +  tfFatherName.text!
            } else {
                RegistrationModel.shared.Father = ""
            }
            removeSpaceAtLast(txtField: tfFatherName)
        }
        else if textField == tfEmailID {
            if isNextViewHidden {
                guard(self.delegate?.showIDTyoeOnDone() != nil) else {
                    return false
                }
                isNextViewHidden = false
            }
            removeSpaceAtLast(txtField: tfEmailID)
            RegistrationModel.shared.EmailId = tfEmailID.text!
            let currentLanguage = appDel.getSelectedLanguage()
            if currentLanguage == "my" {
                ok_default_language = "my"
            }
        }
        return true
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == self.tfDOB {
            let date = Calendar.current.date(byAdding: .year, value: -12, to: Date())
            let minDate = Calendar.current.date(byAdding: .year, value: -117, to: Date())
            
            let datePicker = UIDatePicker()
            datePicker.datePickerMode = .date
            datePicker.backgroundColor = UIColor.white
            datePicker.calendar = Calendar(identifier: .gregorian)
            
            datePicker.maximumDate = date!
            datePicker.minimumDate = minDate
            
            if #available(iOS 13.4, *) {
                datePicker.preferredDatePickerStyle = .wheels
            } else {
                // Fallback on earlier versions
            }
            tfDOB.inputView = datePicker
          
            if tfDOB.text?.count == 0 {
                datePicker.date = date!
            }else{
                datePicker.date = dateFormatter.date(from: tfDOB.text!)!
            }
            strDOB = dateFormatter.string(from: datePicker.date)
            datePicker.addTarget(self, action: #selector(datePickerChanged(sender:)), for: .valueChanged)
        }else if textField == tfYourName {
            if !isYourNameKeyboardHidden {
                viewMaleFemale.isHidden = true
              //  tfYourName.textAlignment = .left
               // tfDOB.textAlignment = .left
              //  btnTakeSalfie.titleLabel?.textAlignment = .left
            }else{
                viewMaleFemale.isHidden = false
                return false
            }
        }else if textField == tfFatherName {
           // tfFatherName.textAlignment = .left
            if !isFatherNameKeyboardHidden {
                if RegistrationModel.shared.Language == "my"{
                    viewBFatherInitial.isHidden = true
                }else {
                    viewFatherInitial.isHidden = true
                }
            }else{
                if RegistrationModel.shared.Language == "my"{
                    viewBFatherInitial.isHidden = false
                }else {
                     viewFatherInitial.isHidden = false
                }
                return false
            }
        }else if textField == tfEmailID {
            if ok_default_language == "my" {
                ok_default_language = "en"
                tfEmailID.keyboardType = .emailAddress
            }
        }
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)

        if range.location == 0 && string == " " {
            return false
        }
        if textField == self.tfYourName {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: NAMECHARSET).inverted).joined(separator: "")) { return false }
            if text.count > 40 { return false }
            if text.count > 2 {
                if isNextViewHidden{
                    if self.dHeight! <= viewDOB.frame.origin.y + viewDOB.frame.height {
                         self.dHeight = viewDOB.frame.origin.y + viewDOB.frame.height
                        runGuard()
                    }
                }
            }
            if restrictMultipleSpaces(str: string, textField: tfYourName) {
                return true
            }else {
                if  textField.text?.last == " " {
                    textField.text?.removeLast()
                }
              return false
            }
        }else if textField == self.tfFatherName {
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: NAMECHARSET).inverted).joined(separator: "")) { return false }
            if text.count > 40 { return false }
            if text.count > 2 {
                if isNextViewHidden {
                    if self.dHeight! < viewEmailID.frame.origin.y + viewEmailID.frame.height {
                        self.dHeight = viewEmailID.frame.origin.y + viewEmailID.frame.height
                        isEmailShown = true
                        runGuard()
                    }
                    }
            }
            if restrictMultipleSpaces(str: string, textField: tfFatherName) {
                return true
            }else {
                if  textField.text?.last == " " {
                    textField.text?.removeLast()
                }
                return false
            }
        }else if textField == tfEmailID {
           if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: EMAILCHARSET).inverted).joined(separator: "")) { return false }
            if text.count > 2 {
                if isNextViewHidden {
                }
                btnMailIDClear.isHidden = false
            }else {
              btnMailIDClear.isHidden = true
            }
       return restrictMultipleSpaces(str: string, textField: tfEmailID)
        }
        return true
    }
    
    func runGuard() {
        guard (self.delegate?.updateMainUIUI() != nil) else {
            return
        }
    }
    
    @objc func datePickerChanged(sender: UIDatePicker) {
        sender.calendar = Calendar(identifier: .gregorian)
        strDOB = dateFormatter.string(from: sender.date)
    }
    
    @objc func btnCancelDOBAction () {
        self.tfDOB.becomeFirstResponder()
        tfDOB.resignFirstResponder()
    }
    
    @objc func btnDoneDOBAction () {
        self.tfDOB.becomeFirstResponder()
        self.tfDOB.resignFirstResponder()
        tfDOB.text = strDOB
        //tfDOB.textAlignment = .left
        if tfDOB.text!.count > 0 {
            tfDOB.text = strDOB
            RegistrationModel.shared.DateOfBirth = tfDOB.text!
            if !isProfileCameraShown {
              //  viewTakeSelfie.isHidden = false
                if isNextViewHidden {
                    if self.dHeight! <= viewTakeSelfie.frame.origin.y + viewTakeSelfie.frame.height {
                        self.dHeight = viewTakeSelfie.frame.origin.y + viewTakeSelfie.frame.height
                        runGuard()
                    }
                }
            }
//(),-၁၂၃၄၅၆၇၈၉၀ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ်   ိ  ္ ့  ံ ျ     ု  ဳ   ူ  ဴ  း  ၚ ွ  ီ ြ  ၤ   ဲ    ွ်   ၽႊ  ႏ  ၽြ    ႊ  ႈ  ဥ  ဧ ၿ   ၾ      ၌ ဋ ႑ ဍ   ၨ    ၳ  ၡ    ႅ   ၻ  ဉ    ဎ  ၺ  ႎ   ႍ `ါ  ႄ      ၶ    ၦ    ၱ   ၷ  ၼ  ဤ   ၸ    ၠ ၍ ႆ  ၥ ၮ   ၎ ဩ   ႀ      ဦ   ၢ႐ ဪ  ႁ    ႂ     ၯ  ၩ ၏
        }
    }

    @IBAction func mailIDClearAction(_ sender: UIButton) {
        tfEmailID.text = ""
        btnMailIDClear.isHidden = true
      //  tfEmailID.textAlignment = .center
        RegistrationModel.shared.EmailId = ""
    }

    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    @IBAction func btnShowDemoAction(_ sender: Any) {
        if let del = delegate {
            var strURL = ""
            if ok_default_language == "my"{
                if RegistrationModel.shared.AccountType == 1 {
                  strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/e18055fe-3137-447f-bfb5-124fcaeee3fdReqDocumentsJuly_03_2018%2003_15_43%20AM.jpg"
                    del.showDemo(strURL: strURL, imgDemo: #imageLiteral(resourceName: "personal") , demoTitile: "Personal".localized)
                }else {
                        strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/95055398-2b8b-4fe2-9198-deefbaaff14cReqDocumentsJuly_02_2018%2001_22_59%20PM.jpg";
                    del.showDemo(strURL: strURL, imgDemo: #imageLiteral(resourceName: "personal") , demoTitile: "Merchant".localized)
                }
            }else {
                if RegistrationModel.shared.AccountType == 1 {
                  strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/655446d1-1bbd-4c00-8685-729b7cef201eReqDocumentsJuly_03_2018%2003_13_17%20AM.jpg"
                    del.showDemo(strURL: strURL, imgDemo: #imageLiteral(resourceName: "merchant") , demoTitile: "Personal".localized)
                }else {
                        strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/9fd41fe5-297f-4f79-abdb-eef5efbb3d0eReqDocumentsJuly_02_2018%2001_27_55%20PM.jpg";
                    del.showDemo(strURL: strURL, imgDemo: #imageLiteral(resourceName: "merchant") , demoTitile: "Merchant".localized)
                }
            }
        }
    }
    
    override var textInputMode: UITextInputMode? {
         let language = ok_default_language
        if language.isEmpty {
            return super.textInputMode
        } else {
            for tim in UITextInputMode.activeInputModes {
                if tim.primaryLanguage!.contains(language) {
                    return tim
                }
            }
            return super.textInputMode
        }
    }
    
    private func removeSpaceAtLast(txtField : UITextField){
                    if txtField.text?.last == " " {
                        txtField.text?.removeLast()
                    }
    }
    
}

extension UILabel {
    func attributedStringFinal(str1 : String) -> NSAttributedString{
        let attrs1 = [NSAttributedString.Key.foregroundColor : UIColor.init(red: 22/255.0, green: 45/255.0, blue: 159/255.0, alpha: 1)]
        let attrs2 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 20), NSAttributedString.Key.foregroundColor : UIColor.red]
        let attributedString1 = NSMutableAttributedString(string:str1, attributes:attrs1)
        let attributedString2 = NSMutableAttributedString(string:" *", attributes:attrs2)
        attributedString1.append(attributedString2)
        return attributedString1
    }
    
    func scrollMarqueLabel(title : String) -> NSAttributedString {
        let attrs1 = [NSAttributedString.Key.foregroundColor : UIColor.init(red: 22/255.0, green: 45/255.0, blue: 159/255.0, alpha: 1)]
        let attributedString1 = NSMutableAttributedString(string:title, attributes:attrs1)
        attributedString1.append(attributedString1)
        return attributedString1
    }
}


