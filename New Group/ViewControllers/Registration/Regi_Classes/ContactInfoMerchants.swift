//
//  ContactInfoMerchants.swift
//  OK
//
//  Created by Subhash Arya on 07/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol  ContactInfoMerchantsDelegate{
    func updateMainUIUI()
    func resetMerchantInfo()
    func showContactPickerView(viewConroller : UIViewController)
    func navigateToLoginVC()
    func selectCountryForMer()
    func viewEndEditing()
}

class ContactInfoMerchants:OKBaseController,ContactPickerDelegate,UITextFieldDelegate,TermsConditionsRVCDelegate,WebServiceResponseDelegate,AgenTermConditionDelegate,ReferralVCDelegate,CountryViewControllerDelegate,BioMetricLoginDelegate,UploadImagesUPDelegate {
    
    
    var delegate : ContactInfoMerchantsDelegate?
    let validObj = PayToValidations()
    
    @IBOutlet weak var btnRNImage: UIImageView!
    @IBOutlet var imgRN: UIImageView!
    @IBOutlet var tfContactNo: UITextField!
    @IBOutlet var tfConfirmContactNo: UITextField!
    
    @IBOutlet var btnClearContact: UIButton!
    @IBOutlet var btnClearConfirmContact: UIButton!
    
    @IBOutlet var imgAgentCode: UIImageView!
    @IBOutlet var imgAddtionalInformation: UIImageView!
    
    @IBOutlet var topAddiInfo: NSLayoutConstraint!
    @IBOutlet var tfAgentCode: UITextField!
    
    @IBOutlet var viewConfirmContactNo: UIView!
    @IBOutlet weak var viewTermConditions: UIView!
    @IBOutlet weak var tfFacebook: UITextField!
    
    @IBOutlet weak var topTermsConditions: NSLayoutConstraint!
    @IBOutlet var lblreferral: MarqueeLabel!
    @IBOutlet var lblAgentCode: MarqueeLabel!
    
    @IBOutlet weak var topAgentCons: NSLayoutConstraint!
    @IBOutlet weak var fbClear: UIButton!
    
    @IBOutlet weak var viewAgentImage: UIView!
    @IBOutlet weak var lblAgentImage: MarqueeLabel!
    @IBOutlet weak var lblFirstImage: UILabel!
    @IBOutlet weak var lblSecondImage: UILabel!
    
    @IBOutlet weak var imgAgentFirst: UIImageView!
    @IBOutlet weak var imgAgentSecond: UIImageView!
    
    @IBOutlet weak var agentClearBtn: UIButton!
    @IBOutlet weak var viewReferralNo: UIView!
    @IBOutlet weak var btnAddiinfo: UIButton!
    @IBOutlet weak var lbladdiContact: UILabel!
    @IBOutlet weak var contactNoContainerView: UIView!
    @IBOutlet weak var viewFacebook: UIView!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var lblAddiInfo: MarqueeLabel!
    var isAddtionalInformationHidden = Bool()
    var isAgentCodeHidden = true
    var isConfrimContactHidden = true
    var isContactSelected = false
    
    
    
    var arrCountryObj = NSArray()
    var dicValue : Dictionary<String,Any> = Dictionary<String,Any>()
    
    @IBOutlet weak var btnReferralSelectNo: UIButton!
    @IBOutlet weak var btnShowContactVC: UIButton!
    @IBOutlet weak var btnAccept: UIButton!
    var countryCodeString = ""
    var MobilNumber = ""
    var CONTACTCHATSET = "0123456789"
    var FACEBOOKCHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890.@_ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာ၁၂၃၄၅၆၇၈၉၀ "
    var AGENTCODESET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890/-,:()"
    override func viewDidLoad() {
        super.viewDidLoad()
        lblreferral.attributedText = lblreferral.attributedStringFinal(str1: "Referral Number (Enter OK$ Account Number)".localized)
        tfContactNo.placeholder = "Enter Referral Number".localized
        lblAgentCode.text = "You have Agent code please click here".localized
        tfAgentCode.placeholder = "Enter Your Agent Code".localized
        lblAddiInfo.text = "Additional Information".localized
        lbladdiContact.text = "Additional Contact Information".localized
        tfFacebook.placeholder = "Enter Facebook ID".localized
        btnAccept.setTitle("ACCEPT & REGISTER".localized, for: .normal)
        lblAgentImage.text = "Attach Agent Images".localized
        btnRNImage.layer.borderWidth = 1.0
        btnRNImage.layer.borderColor = UIColor.black.cgColor
        imgRN.layer.borderWidth = 1.0
        imgRN.layer.borderColor = UIColor.black.cgColor
        
        topAgentCons.constant = -57
        topAddiInfo.constant = -57 - 160
        topTermsConditions.constant = -97
        viewConfirmContactNo.isHidden = true
        isAddtionalInformationHidden = true
        tfContactNo.delegate = self
        tfConfirmContactNo.delegate = self
        tfAgentCode.delegate = self
        tfFacebook.delegate = self
        btnClearContact.isHidden = true
        viewFacebook.isHidden = true
        self.dHeight = 259.0
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(ContactInfoPersonal.onClickAlternetImage))
        btnRNImage.addGestureRecognizer(tap1)
        
        if !(RegistrationModel.shared.MobileNumber == ""){
            let (mobNo,CountryCode) = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: RegistrationModel.shared.MobileNumber)
            countryCodeString = CountryCode
            MobilNumber = mobNo
            let countryData = identifyCountryByCode(withPhoneNumber: CountryCode)
            if let safeImage = UIImage(named: countryData.countryFlag) {
                DispatchQueue.main.async {
                    self.btnRNImage.image = safeImage
                    self.imgRN.image = safeImage
                    self.imgRN.layer.borderColor = UIColor.black.cgColor
                    self.setLeftViewInTextField(txtField: self.tfContactNo, codeCountry: CountryCode)
                    self.setLeftViewInTextField(txtField: self.tfConfirmContactNo, codeCountry: CountryCode)
                }
            }
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(ContactInfoMerchants.tapFunction))
        lblAgentCode.addGestureRecognizer(tap)
        
        UserDefaults.standard.set("YES", forKey: "ALL_FIELDS_FILLED")
        if ok_default_language == "en" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue"), for: .normal)
        }else if ok_default_language == "my" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {() -> Void in
            self.lblreferral.resetLabel()
            self.lblAgentCode.restartLabel()
        })
    }
    
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        self.btnAgentCodeAction(UIButton())
    }
    
    @IBAction func btnReferralNoSelectAction(_ sender: Any) {
        
        let selectVC  = self.storyboard?.instantiateViewController(withIdentifier: "selectReferralNumberVc") as! selectReferralNumberVc
        selectVC.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        selectVC.delegate = self
        selectVC.modalPresentationStyle = .overCurrentContext
        self.present(selectVC, animated: false, completion: nil)
        
//        addChild(selectVC)
//        if let window = UIApplication.shared.keyWindow {
//            window.rootViewController?.addChild(selectVC)
//            selectVC.view.frame = .init(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
//            selectVC.delegate = self
//            window.addSubview(selectVC.view)
//            window.makeKeyAndVisible()
//        }
    }
    
    func dismissWithDetail(selectedOption: String, selfView: UIViewController) {
        //selfView.view.removeFromSuperview()
        if selectedOption == "MySelf".localized {
            btnReferralSelectNo.isUserInteractionEnabled = false
            let countryData = identifyCountryByCode(withPhoneNumber: RegistrationModel.shared.CountryCode)
            if let safeImage = UIImage(named: countryData.countryFlag) {
                self.btnRNImage.image = safeImage
                self.setLeftViewInTextField(txtField: tfContactNo, codeCountry: countryData.countryCode)
                self.tfContactNo.text = MobilNumber
                self.tfConfirmContactNo.text = MobilNumber
                self.tfContactNo.textColor = UIColor.gray
                RegistrationModel.shared.Recommended = RegistrationModel.shared.MobileNumber
                RegistrationModel.shared.CodeRecommended = RegistrationModel.shared.CountryCode
            }
        }else {
            contactNoContainerView.isHidden = true
            tfContactNo.text = ""
            tfContactNo.textColor = UIColor.black
            tfContactNo.becomeFirstResponder()
            btnShowContactVC.isHidden = false
        }
    }
    
    @IBAction func btnResetAction(_ sender: Any) {
        RegistrationModel.shared.Recommended = ""
        RegistrationModel.shared.CodeRecommended = ""
        RegistrationModel.shared.AgentAuthCode = ""
        RegistrationModel.shared.FBEmailId = ""
        isContactSelected = false
        btnReferralSelectNo.isUserInteractionEnabled = true
        contactNoContainerView.isHidden = false
        guard(self.delegate?.resetMerchantInfo() != nil) else {
            return
        }
    }
    
    @IBAction func btnAddContactNoAction(_ sender: Any) {
        guard (self.delegate?.showContactPickerView(viewConroller: self) != nil) else {
            return
        }
    }
    
    @IBAction func btnClearContactNoAction(_ sender: UIButton) {
        tfConfirmContactNo.textAlignment = .left
        tfContactNo.text = ""
        tfContactNo.resignFirstResponder()
        
        tfConfirmContactNo.text = ""
        tfConfirmContactNo.resignFirstResponder()
        
        btnClearContact.isHidden = true
        btnClearConfirmContact.isHidden = true
        viewConfirmContactNo.isHidden = true
        isContactSelected = false
        if !isConfrimContactHidden {
            DispatchQueue.main.async {
                self.topAgentCons.constant = -57
                self.dHeight = self.viewTermConditions.frame.origin.y + self.viewTermConditions.frame.height - 57
                self.runGuard()
            }
        }
        RegistrationModel.shared.Recommended = ""
        RegistrationModel.shared.CodeRecommended = ""
    }
    
    @IBAction func btnClearConfirmContactNoAction(_ sender: Any) {
        tfConfirmContactNo.resignFirstResponder()
        tfConfirmContactNo.text = ""
        btnClearConfirmContact.isHidden = true
    }
    
    @IBAction func btnAgentCodeAction(_ sender: UIButton) {
        
        if let del = delegate {
            del.viewEndEditing()
        }
        tfAgentCode.text = ""
        RegistrationModel.shared.AgentAuthCode = ""
        if imgAgentCode.image == #imageLiteral(resourceName: "radio_select") {
            isAgentCodeHidden = false
            self.AgentCodeFuntion()
        }else {
            let sb = UIStoryboard(name: "Registration" , bundle: nil)
            let agentTCVC  = sb.instantiateViewController(withIdentifier: "AgenTermCondition") as! AgenTermCondition
            agentTCVC.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            agentTCVC.delegate = self
            agentTCVC.modalPresentationStyle = .overCurrentContext
            self.present(agentTCVC, animated: false, completion: nil)
            
//            addChild(agentTCVC)
//            if let window = UIApplication.shared.keyWindow {
//                window.rootViewController?.addChild(agentTCVC)
//                agentTCVC.view.frame = .init(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
//                agentTCVC.delegate = self
//                window.addSubview(agentTCVC.view)
//                window.makeKeyAndVisible()
//            }
        }
    }
    
    
    @objc func onClickAlternetImage() {
        if let dl = self.delegate {
            dl.selectCountryForMer()
        }
    }
    
    func countryViewController(_ list: CountryViewController, country: Country) {
        RegistrationModel.shared.CodeRecommended = ""
        RegistrationModel.shared.Recommended = ""
        tfContactNo.text = ""
        tfConfirmContactNo.text = ""
        btnClearContact.isHidden = true
        countryCodeString = country.dialCode
        self.hideConfirmReferralNumberField()
        self.btnRNImage.image = UIImage(named: country.code)
        self.setLeftViewInTextField(txtField: tfContactNo, codeCountry: country.dialCode)
        list.dismiss(animated: false, completion: nil)
    }
    
    func countryViewControllerCloseAction(_ list: CountryViewController) {
        list.dismiss(animated: false, completion: nil)
    }
    
    func removeVC(vc : UIViewController) {
       // vc.view.removeFromSuperview()
        AgentCodeFuntion()
    }
    
    func AgentCodeFuntion(){
        agentClearBtn.isHidden = true
        if isAgentCodeHidden {
            topAddiInfo.constant = 0
            isAgentCodeHidden = false
            viewAgentImage.isHidden = false
            imgAgentCode.image = UIImage(named: "radio_select")
            self.dHeight = viewTermConditions.frame.origin.y + viewTermConditions.frame.height + 57.0 + 160.0
            runGuard()
        }else{
            topAddiInfo.constant = -57 - 160
            isAgentCodeHidden = true
            viewAgentImage.isHidden = true
            imgAgentCode.image = UIImage(named: "radio")
            self.dHeight = viewTermConditions.frame.origin.y + viewTermConditions.frame.height - 57.0 - 160.0
            runGuard()
        }
    }
    
    @IBAction func onClickImageAction(_ sender: UIButton){
        DispatchQueue.main.async {
            let image = UIImage()
            var imgList = [UIImage]()
            let url1Exits =  RegistrationModel.shared.ShopPicOneAwsUrl
            let url2Exits =  RegistrationModel.shared.ShopPicTwoAwsUrl
            if url1Exits != "" {
                if let img = image.convertURLToImage(str: url1Exits) {
                    imgList.append(img)
                }
            }
            if url2Exits != "" {
                if let img = image.convertURLToImage(str: url2Exits) {
                    imgList.append(img)
                }
            }
            let imageVC = UIStoryboard(name: "MyProfile", bundle: Bundle.main).instantiateViewController(withIdentifier: "UploadImagesUP")  as! UploadImagesUP
                imageVC.setMarqueLabelInNavigation(title: "Agent shop image".localized)
                imageVC.buttonName = ["Tap to take Photo of Agent first shop image".localized, "Tap to take Photo of Agent second shop image".localized]
                imageVC.buttonTitleName =  ["First Image".localized,"Second Image".localized]
            imageVC.imageList = imgList
            imageVC.imagesURL = [RegistrationModel.shared.ShopPicOneAwsUrl,RegistrationModel.shared.ShopPicTwoAwsUrl]
            imageVC.delegate = self
            self.navigationController?.pushViewController(imageVC, animated: true)
        }
    }
    
    func sendImagesToMainController(images: [String]) {
        RegistrationModel.shared.ShopPicOneAwsUrl = images[0].replacingOccurrences(of: " ", with: "%20")
        RegistrationModel.shared.ShopPicTwoAwsUrl = images[1].replacingOccurrences(of: " ", with: "%20")
        DispatchQueue.main.async {
            self.imgAgentFirst.sd_setImage(with: URL(string: RegistrationModel.shared.ShopPicOneAwsUrl), placeholderImage: UIImage(named: "nrc"))
            self.imgAgentSecond.sd_setImage(with: URL(string: RegistrationModel.shared.ShopPicTwoAwsUrl), placeholderImage: UIImage(named: "nrc"))
        }
    }
    
    @IBAction func btnClearAgentCode(_ sender: Any) {
        tfAgentCode.text = ""
        agentClearBtn.isHidden = true
    }
    
    @IBAction func btnClearFBidAction(_ sender: Any) {
        tfFacebook.text = ""
        fbClear.isHidden = true
    }
    
    @IBAction func btnAdditionalInfoAction(_ sender: Any) {
        
        
        if isAddtionalInformationHidden {
            topTermsConditions.constant = 0
            viewFacebook.isHidden = false
            imgAddtionalInformation.image = UIImage(named: "radio_select")
            isAddtionalInformationHidden = false
            self.dHeight = viewTermConditions.frame.origin.y + viewTermConditions.frame.height + 97.0
            runGuard()
        }else {
            topTermsConditions.constant = -97
            viewFacebook.isHidden = true
            imgAddtionalInformation.image = UIImage(named: "radio")
            isAddtionalInformationHidden = true
            self.dHeight = viewTermConditions.frame.origin.y + viewTermConditions.frame.height - 97.0
            runGuard()
        }
    }
    
    
    @IBAction func referralNumberEditChanged(_ sender: UITextField) {
        var tempNo = RegistrationModel.shared.MobileNumber
        tempNo.removeFirst()
        tempNo.removeFirst()
        tempNo.removeFirst()
        tempNo.removeFirst()
        
        if tfContactNo.text == "0" + tempNo {
            showAlert(alertTitle: "", alertBody: "Please provide another number".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
            tfContactNo.text?.removeLast()
        }else {
            if btnRNImage.image == UIImage(named: "myanmar") {
                let mbLength = (validObj.getNumberRangeValidation(sender.text!))
                if sender.text?.count == mbLength.min {
                    self.showConfirmReferralNumberField()
                    self.dHeight = self.viewTermConditions.frame.origin.y + self.viewTermConditions.frame.height + 57
                    self.runGuard()
                }else if (sender.text?.count)! < mbLength.min {
                    hideConfirmReferralNumberField()
                    self.runGuard()
                }else if (sender.text?.count)! <= mbLength.max && (sender.text?.count)! > mbLength.min {
                    if isContactSelected {
                        self.showConfirmReferralNumberField()
                        isContactSelected = false
                    }
                }
                
                if sender.text?.count == mbLength.max {
                    tfContactNo.resignFirstResponder()
                    tfConfirmContactNo.becomeFirstResponder()
                }
            }else {
                if (sender.text?.count)! > 3{
                    if viewConfirmContactNo.isHidden == true {
                        showConfirmReferralNumberField()
                        self.runGuard()
                    }else {
                        self.tfConfirmContactNo.isUserInteractionEnabled = true
                        self.imgRN.image = self.btnRNImage.image
                        self.imgRN.layer.borderColor = UIColor.black.cgColor
                        self.setLeftViewInTextField(txtField: tfConfirmContactNo, codeCountry: countryCodeString)
                    }
                }else if (sender.text?.count)! < 4{
                    if viewConfirmContactNo.isHidden == false {
                        hideConfirmReferralNumberField()
                        self.runGuard()
                    }
                }
                if (sender.text?.count) == 13 {
                    tfContactNo.resignFirstResponder()
                    tfConfirmContactNo.becomeFirstResponder()
                }
            }
        }
    }
    
    func hideConfirmReferralNumberField() {
        tfConfirmContactNo.text = ""
        self.tfConfirmContactNo.leftView = nil
        self.isConfrimContactHidden = true
        self.viewConfirmContactNo.isHidden = true
        self.topAgentCons.constant = -57
    }
    
    func showConfirmReferralNumberField(){
        tfConfirmContactNo.text = ""
        self.isConfrimContactHidden = false
        self.viewConfirmContactNo.isHidden = false
        self.tfConfirmContactNo.textAlignment = .left
        self.tfConfirmContactNo.isUserInteractionEnabled = true
        self.imgRN.image = self.btnRNImage.image
        self.imgRN.layer.borderColor = UIColor.black.cgColor
        self.topAgentCons.constant = 0
        self.setLeftViewInTextField(txtField: tfConfirmContactNo, codeCountry: countryCodeString)
    }
    
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == tfContactNo{
            
            if btnRNImage.image == UIImage(named: "myanmar") {
                if tfContactNo.text == "" {
                    tfContactNo.text = "09"
                }
            }
        }else if textField == tfConfirmContactNo{
            if btnRNImage.image == UIImage(named: "myanmar") {
                if  tfConfirmContactNo.text == "" {
                    tfConfirmContactNo.text = "09"
                }
            }
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == tfFacebook {
            removeSpaceAtLast(txtField: tfFacebook)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if range.location == 0 && string == " " {
            return false
        }
        
        let chars = textField.text! + string;
        let mbLength = validObj.getNumberRangeValidation(chars).max
        let isRejected = validObj.getNumberRangeValidation(chars).isRejected
        
        if textField == tfContactNo{
            
            if chars.count > 2 {
                btnClearContact.isHidden = false
            }else{
                btnClearContact.isHidden = true
            }
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CONTACTCHATSET).inverted).joined(separator: "")) { return false }
            
            if btnRNImage.image == UIImage(named: "myanmar") {
                
                if (range.location == 0 && range.length == 0) || (range.location == 1 && range.length == 0) {
                    return false
                }
                
                if range.location == 0 && range.length > 1 {
                    textField.text = "09"
                    btnClearContact.isHidden = true
                    return false
                }
                
                if text.count < 2 { return false }
                if chars.count > 3 {
                    btnClearContact.isHidden = false
                }else {
                    btnClearContact.isHidden = true
                }
                if isRejected {
                    showAlert(alertTitle: "", alertBody: "Rejected Number".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
                    tfContactNo.text = commonPrefixMobileNumber
                    btnClearContact.isHidden = true
                    return false
                }
                
                if chars.count > mbLength {
                    tfContactNo.resignFirstResponder()
                    return false
                }
            }else {
                if text.count > 0 {
                    btnClearContact.isHidden = false
                }else {
                    btnClearContact.isHidden = true
                }
                tfConfirmContactNo.text = ""
                RegistrationModel.shared.CodeRecommended = ""
                RegistrationModel.shared.Recommended = ""
                if text.count > 13 {
                    return false
                }
            }
        }else if textField == tfConfirmContactNo {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CONTACTCHATSET).inverted).joined(separator: "")) { return false }
            if text.count == tfContactNo.text!.count{
                tfConfirmContactNo.resignFirstResponder()
                RegistrationModel.shared.CodeRecommended =  "+" + countryCodeString
                MobilNumber = ""
                
                if btnRNImage.image == UIImage(named: "myanmar") {
                    if text.count < 2 { return false }
                    let mb = tfContactNo.text?.dropFirst()
                    MobilNumber = "0095" + mb!
                }else {
                    MobilNumber = "00" + countryCodeString + tfContactNo.text!
                }
                RegistrationModel.shared.Recommended = MobilNumber
            }else {
                RegistrationModel.shared.CodeRecommended = ""
                RegistrationModel.shared.Recommended = ""
            }
            
            if chars.count > 2 {
                btnClearConfirmContact.isHidden = false
            }else{
                btnClearConfirmContact.isHidden = true
            }
            if validObj.checkMatchingNumber(string: text, withString: self.tfContactNo.text!) {
                self.tfConfirmContactNo.text = text
                if btnRNImage.image == UIImage(named: "myanmar") {
                    if chars.count - 1 == commonPrefixMobileNumber.count {
                        btnClearConfirmContact.isHidden = true
                    }
                }else {
                    if chars.count - 1 == 0 {
                        btnClearConfirmContact.isHidden = true
                    }
                }
                return false
            } else {
                if btnRNImage.image == UIImage(named: "myanmar") {
                    if chars.count - 1 == commonPrefixMobileNumber.count {
                        btnClearConfirmContact.isHidden = true
                    }
                }else {
                    if chars.count - 1 == 0 {
                        btnClearConfirmContact.isHidden = true
                    }
                }
                return false
            }
        }else if textField == tfFacebook {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: FACEBOOKCHARSET).inverted).joined(separator: "")) { return false }
            if text.count > 0 {
                fbClear.isHidden = false
            }else {
                fbClear.isHidden = true
            }
        }else if textField == tfAgentCode {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: AGENTCODESET).inverted).joined(separator: "")) { return false }
            if text.count > 0 {
                agentClearBtn.isHidden = false
            }else {
                agentClearBtn.isHidden = true
            }
        }
        
        return true
    }
    
    func contact(_: ContactPickersPicker, didContactFetchFailed error: NSError){}
    func contact(_: ContactPickersPicker, didCancel error: NSError){}
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker){ decodeContact(contact: contact)}
    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]){}
    func decodeContact(contact: ContactPicker) {
        var countryCode = ""
        var countryFlag = ""
        var tempContryCode = ""
        var tempPhoneNumber = ""
        let phoneNumberCount = contact.phoneNumbers.count
        var phoneNumber = ""
        
        if phoneNumberCount == 1  {
            phoneNumber = "\(contact.phoneNumbers[0].phoneNumber)"
        }
        else if phoneNumberCount > 1 {
            phoneNumber = "\(contact.phoneNumbers[0].phoneNumber)"
        }else {}
        
        if phoneNumber != "" {
            phoneNumber = phoneNumber.replacingOccurrences(of: ")", with: "")
            phoneNumber = phoneNumber.replacingOccurrences(of: "(", with: "")
            phoneNumber = phoneNumber.trimmingCharacters(in: .whitespaces)
        }
        
        let cDetails = identifyCountry(withPhoneNumber: phoneNumber)
        if cDetails.0 == "" || cDetails.1 == "" {
            countryFlag = "myanmar"
            countryCode = "+95"
            tempContryCode = countryCode
        } else {
            countryFlag = cDetails.1
            countryCode = cDetails.0
            tempContryCode = countryCode
        }
        println_debug(tempContryCode)
        let phone = phoneNumber.replacingOccurrences(of: cDetails.0, with: "0")
        if phone.hasPrefix("0") {
            if countryFlag == "myanmar" {
                tempPhoneNumber = phone
            } else {
                tempPhoneNumber = phoneNumber.replacingOccurrences(of: cDetails.0, with: "")
            }
        } else {
            if countryFlag == "myanmar" {
                tempPhoneNumber =   "0" + phone
            } else {
                tempPhoneNumber =  phone
            }
        }
        
        countryCode.remove(at: countryCode.startIndex)
        if tempPhoneNumber.hasPrefix("0"){
            tempPhoneNumber.remove(at: tempPhoneNumber.startIndex)
        }
        let finalMob =  "00" + countryCode + tempPhoneNumber
        let noToCheck = "0" + tempPhoneNumber
        if finalMob.count > 8 {
            if countryCode == "95" {
                let mbLength = (validObj.getNumberRangeValidation(noToCheck))
                if !mbLength.isRejected {
                    DispatchQueue.main.async {
                        self.tfContactNo.text = phone
                        self.tfConfirmContactNo.text = contact.displayName()
                        RegistrationModel.shared.CodeRecommended =  "+" + countryCode
                        RegistrationModel.shared.Recommended = finalMob
                        self.isContactSelected = true
                        self.valideReferralNumber()
                    }
                }else{
                    DispatchQueue.main.async {
                        self.btnClearContactNoAction(UIButton())
                        self.showAlert(alertTitle: "", alertBody: "You selected invalid number, Please try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
                    }
                }
            }else{
                DispatchQueue.main.async {
                    self.tfContactNo.text = phone
                    self.tfConfirmContactNo.text = contact.displayName()
                    RegistrationModel.shared.CodeRecommended =  "+" + countryCode
                    RegistrationModel.shared.Recommended = finalMob
                    self.valideReferralNumber()
                }
            }
            countryCodeString = "+" + countryCode
        }else{
            DispatchQueue.main.async {
                self.btnClearContactNoAction(UIButton())
                self.showAlert(alertTitle: "", alertBody: "You selected invalid number, Please try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
            }
        }
    }
    
    private func valideReferralNumber() {
        let countryData = identifyCountryByCode(withPhoneNumber: RegistrationModel.shared.CodeRecommended)
        if let safeImage = UIImage(named: countryData.countryFlag) {
            DispatchQueue.main.async {
                self.btnRNImage.image = safeImage
                self.setLeftViewInTextField(txtField:self.tfContactNo , codeCountry: countryData.countryCode)
                self.imgRN.image = UIImage(named: "sim_R")
                self.imgRN.layer.borderColor = UIColor.white.cgColor
            }
        }
        self.tfConfirmContactNo.isUserInteractionEnabled = false
        self.btnClearContact.isHidden = false
        self.btnClearConfirmContact.isHidden = true
        self.tfConfirmContactNo.leftView = nil
        self.isConfrimContactHidden = false
        self.viewConfirmContactNo.isHidden = false
        self.topAgentCons.constant = 0
        self.dHeight = self.viewTermConditions.frame.origin.y + self.viewTermConditions.frame.height + 57
        self.runGuard()
    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful {
            self.uploadProfileAndSingnatureImages(handler: {(success) in
                if success {
                    DispatchQueue.main.async {
             //           if let window = UIApplication.shared.keyWindow {
                            let sb = UIStoryboard(name: "Registration" , bundle: nil)
                            let termsConditionsRVC  = sb.instantiateViewController(withIdentifier: "TermsConditionsRVC") as! TermsConditionsRVC
                              termsConditionsRVC.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                            termsConditionsRVC.modalPresentationStyle = .overCurrentContext
                            termsConditionsRVC.delegate = self
                            self.present(termsConditionsRVC, animated: false, completion: nil)
                            
//                            self.addChild(termsConditionsRVC)
//                            window.rootViewController?.addChild(termsConditionsRVC)
//                            termsConditionsRVC.view.frame = .init(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
//                            termsConditionsRVC.delegate = self
//                            window.addSubview(termsConditionsRVC.view)
//                            window.makeKeyAndVisible()
                  //      }
                    }
                }else {
                    self.showAlert(alertTitle: "", alertBody: "Image uploading failed. Please press again on Accept and Register button".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
                }
            })
        }else{
            
        }
    }
    
    @IBAction func btnAcceotAndRegisterAction(_ sender: Any) {
        if let del = delegate {
            del.viewEndEditing()
        }
        RegistrationModel.shared.AgentAuthCode = tfAgentCode.text!
        RegistrationModel.shared.FBEmailId = tfFacebook.text!
        self.callRegistrationAPI()
    }
    
    func removeFromSuperVC(vc : UIViewController) {
        //ANTONY
        //vc.view.removeFromSuperview()
        RegistrationModel.shared.setModelValues()
        // as? Dictionary<String, Any>

        if appDelegate.checkNetworkAvail() {
            let web = WebApiClass()
            web.delegate = self
            let urlStr   = Url.URL_Registration
            let url = getUrl(urlStr: urlStr, serverType: .serverApp)
            println_debug(url)
            let dic = RegistrationModel.shared.wrapDataModel()
            web.genericClassReg(url: url, param: Dictionary<String,Any>(), httpMethod: "POST", mScreen: "mLogin", hbData: dic, authToken: nil)
           // web.genericClassReg1(url: url, param: param as AnyObject, httpMethod: "POST", mScreen: "registration")
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
        } else {
            alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img:#imageLiteral(resourceName: "r_user"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        DispatchQueue.main.async {
            progressViewObj.removeProgressView()
        }
        do {
            if let data = json as? Data {
                let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let dic = dict as? Dictionary<String,AnyObject> {
                    println_debug(dic["Msg"] as! String)
                    if dic["Code"] as? NSNumber == 200 {
                        RegistrationModel.shared.playSound()
                        userDef.set(RegistrationModel.shared.Password, forKey: "passwordLogin_local")
                        if RegistrationModel.shared.PasswordType == "0" {
                            userDef.set(false, forKey: "passwordLoginType")
                            ok_password_type = false
                        } else {
                            userDef.set(true, forKey: "passwordLoginType")
                            ok_password_type = true
                        }
                        ok_password         = RegistrationModel.shared.Password
                        UserDefaults.standard.set(0, forKey: "App_Password_Type")
                        
                        guard(self.delegate?.navigateToLoginVC() != nil) else {
                            return
                        }
                    }else {
                        showAlert(alertTitle: "", alertBody: dic["Msg"] as! String , alertImage: #imageLiteral(resourceName: "r_user"))
                    }
                }
            }
        } catch {
            showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))
        }
    }
    
    private func callRegistrationAPI() {
        
        RegistrationModel.shared.FBEmailId = tfFacebook.text!
        checkEmptyField(handler: { (success , emptyField) in
            if success {
                OKPayment.main.authenticateForRegistration(screenName: "Login", delegate: self)
            }else{
                showAlert(alertTitle: "Please Fill All Mandatory Fields".localized, alertBody: emptyField , alertImage: #imageLiteral(resourceName: "alert-icon"))
            }
        })
    }
    
    
    
    private func uploadProfileAndSingnatureImages(handler : @escaping (_ isSuccess : Bool) -> Void) {
        
        let paramString : [String:Any] = [
            "MobileNumber":RegistrationModel.shared.MobileNumber,
            "Base64String":[RegistrationModel.shared.signature,"","","",""]
        ]
        
        VillageManager.shared.uploadImageToS3(imageBase64String :paramString, handler: {(imageURL,success) in
            if success {
                RegistrationModel.shared.signatureAwsUrl = imageURL[0].replacingOccurrences(of: " ", with: "%20")
                  handler(true)
//                self.uploadOptionalIamges(handler: {(success) in
//                    if success {
//                        handler(true)
//                    }else {
//                        handler(false)
//                    }
//                })
            }else {
                handler(false)
            }
        })
    }
    
    private func uploadOptionalIamges(handler : @escaping (_ isSuccess : Bool) -> Void) {
        
        let paramString : [String:Any] = [
            "MobileNumber":RegistrationModel.shared.MobileNumber,
            "Base64String":[ RegistrationModel.shared.IdPhoto,RegistrationModel.shared.IdPhoto1,RegistrationModel.shared.BusinessIdPhoto1,RegistrationModel.shared.BusinessIdPhoto2,""]
        ]
        
        VillageManager.shared.uploadImageToS3(imageBase64String :paramString, handler: {(imageURL,success) in
            if success {
                if RegistrationModel.shared.IdPhoto != "" {
                    RegistrationModel.shared.IdPhotoAwsUrl = imageURL[0].replacingOccurrences(of: " ", with: "%20")
                }
                if RegistrationModel.shared.IdPhoto1 != "" {
                    RegistrationModel.shared.IdPhoto1AwsUrl = imageURL[1].replacingOccurrences(of: " ", with: "%20")
                }
                if RegistrationModel.shared.BusinessIdPhoto1 != "" {
                    RegistrationModel.shared.BusinessIdPhoto1AwsUrl = imageURL[2].replacingOccurrences(of: " ", with: "%20")
                }
                if RegistrationModel.shared.BusinessIdPhoto2 != "" {
                    RegistrationModel.shared.BusinessIdPhoto2AwsUrl = imageURL[3].replacingOccurrences(of: " ", with: "%20")
                }
                handler(true)
            }else {
                handler(false)
            }
        })
    }
    
    func runGuard(){
        guard (self.delegate?.updateMainUIUI() != nil) else {
            return
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    func formattedMobileNo(mobilNumber mNo : String ,countryCode code: String)-> String {
        var mNumb = mNo
        if code == "myanmar" {
            mNumb.remove(at: mNumb.startIndex)
            let finalMobNum = "00" + "95" + mNumb
            return finalMobNum
        }
        return mNumb
    }
    
    
    
    func parsingFeeLimitKickbackResponse(xml: String) {
        let xml = SWXMLHash.parse(xml)
        self.enumerate(indexer: xml)
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            dicValue[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    func checkEmptyField(handler : (_ success : Bool,_ emptyField : String) -> Void){
        var emptyFields = ""
        
        if RegistrationModel.shared.Name == "" {
            emptyFields = "User Name".localized
        }else if RegistrationModel.shared.Name.contains(find: ",") {
            let token = RegistrationModel.shared.Name.components(separatedBy: ",")
            if token[1].count < 3 {
                emptyFields = emptyFields + "\n" + "User Name must be more than 2 characters".localized
            }
        }
        
        if RegistrationModel.shared.DateOfBirth == "" {
            emptyFields = emptyFields + " " + "Date of Birth".localized
        }
        
        if RegistrationModel.shared.ProfilePicAwsUrl == "" {
            emptyFields = emptyFields + " " + "Profile Image".localized
        }
        
        if RegistrationModel.shared.Father == "" {
            emptyFields = emptyFields + " " + "Father Name".localized
        }else if RegistrationModel.shared.Father.contains(find: ",") {
            let token = RegistrationModel.shared.Father.components(separatedBy: ",")
            if token[1].count < 3 {
                emptyFields = emptyFields + "\n" + "Father Name must be more than 2 characters".localized
            }
        }
        
        if RegistrationModel.shared.EmailId == "" {
            emptyFields = emptyFields + "\n" + "Email ID".localized
        }else {
            if isValidEmail(testStr: RegistrationModel.shared.EmailId) {
                
            }else {
                emptyFields = emptyFields + "\n" + "Invalid email format".localized
            }
        }
        
        if RegistrationModel.shared.IDType == "01" {
            if RegistrationModel.shared.NRC == "" {
                emptyFields = emptyFields + "\n" + "NRC number".localized
            }else if RegistrationModel.shared.NRC.contains(find: "@") {
                let token = RegistrationModel.shared.NRC.components(separatedBy: ")")
                if token[1].count < 6 {
                    emptyFields = emptyFields + "\n" + "NRC number must be 6 characters".localized
                }
            }
            
            
            if RegistrationModel.shared.IdPhoto == "" {
                emptyFields = emptyFields + "\n" + "NRC Front Image Missing".localized
            }
            if RegistrationModel.shared.IdPhoto1 == "" {
                emptyFields = emptyFields + "\n" + "NRC Back Image Missing".localized
            }
            
        }else if RegistrationModel.shared.IDType == "04" {
            if RegistrationModel.shared.CountryOfCitizen == "" {
                emptyFields = emptyFields + "\n" + "Country of Citizen".localized
            }
            
            if RegistrationModel.shared.NRC == "" {
                emptyFields = emptyFields + "\n" + "Passport Number".localized
            }else if RegistrationModel.shared.NRC.count < 5 {
                emptyFields = emptyFields + "\n" + "Passport Number must be more than 4 characters".localized
            }
            
            if RegistrationModel.shared.IdExpDate == "" {
                emptyFields = emptyFields + "\n" + "Passport Expiry Date".localized
            }
            
            if RegistrationModel.shared.IdPhoto == "" {
                emptyFields = emptyFields + "\n" + "Passport Front Image Missing".localized
            }
            if RegistrationModel.shared.IdPhoto1 == "" {
                emptyFields = emptyFields + "\n" + "Passport Back Image Missing".localized
            }
        }else{
            emptyFields = emptyFields + "\n" + "ID Type".localized
        }
     
        
        
        if RegistrationModel.shared.BusinessName == "" {
            emptyFields = emptyFields + "\n" + "Business Name".localized
        }else if RegistrationModel.shared.BusinessName.count < 3 {
            emptyFields = emptyFields + "\n" + "Business Name must be more than 2 characters".localized
        }
        
        if RegistrationModel.shared.BusinessCategory == "" {
            emptyFields = emptyFields + "\n" + "Business Category".localized
        }
        
        if RegistrationModel.shared.BusinessType == "" {
            emptyFields = emptyFields + "\n" + "Business Sub-Category".localized
        }
        
        if RegistrationModel.shared.BusinessIdPhoto1 == "" {
            emptyFields = emptyFields + "\n" + "Business License Image 1 Missing".localized
        }
        if RegistrationModel.shared.BusinessIdPhoto1 == "" {
            emptyFields = emptyFields + "\n" + "Business License Image 2 Missing".localized
        }
        
        if RegistrationModel.shared.AddressType == "" {
            emptyFields = emptyFields + "\n" + "Address Type".localized
        }else {
            if RegistrationModel.shared.State == "" {
                emptyFields = emptyFields + "\n" + "State/Division Name".localized
            }
            if RegistrationModel.shared.Township == "" {
                emptyFields = emptyFields + "\n" + "Township Name".localized
            }
            
            if RegistrationModel.shared.Address2 == "" {
                emptyFields = emptyFields + "\n" + "Street Name".localized
            }
            
            if   RegistrationModel.shared.HouseBlockNo == "" && RegistrationModel.shared.FloorNumber ==  "" && RegistrationModel.shared.RoomNumber == "" {
                emptyFields = emptyFields + "\n" + "House No/Floor No/Room No".localized
            }
        }
        
        
        if RegistrationModel.shared.PasswordType == "" {
            emptyFields = emptyFields + "\n" + "Security Type".localized
        }
        
        if RegistrationModel.shared.Password == "" {
            emptyFields = emptyFields + "\n" + "Enter Password".localized
        }else if RegistrationModel.shared.Password.count < 6{
            emptyFields = emptyFields + "\n" + "Password must be more than 5 digits".localized
        }
        
        if RegistrationModel.shared.PasswordType == "0" {
            if RegistrationModel.shared.ConfirmPassword == "" {
                emptyFields = emptyFields + "\n" + "Enter Confirm Password".localized
            }else if RegistrationModel.shared.Password != RegistrationModel.shared.ConfirmPassword {
                emptyFields = emptyFields + "\n" + "Password and Confirm Password does not matched".localized
            }
        }
        
        if RegistrationModel.shared.SecurityQuestionAnswer == "" {
            emptyFields = emptyFields + "\n" + "Security Answer".localized
        }
        
        if RegistrationModel.shared.signature == "" {
            emptyFields = emptyFields + "\n" + "User Signature".localized
        }
//        if imgAgentCode.image == #imageLiteral(resourceName: "radio_select") {
//            if tfAgentCode.text == "" {
//                emptyFields = emptyFields + "\n" + "Agent Code".localized
//            }
//        }
        
        
        if tfConfirmContactNo.isUserInteractionEnabled {
            if countryCodeString == "+95" {
                if (tfContactNo.text?.count)! < 3 {
                    emptyFields = emptyFields + "\n" + "Refferal number missing".localized
                }else if (tfContactNo.text?.count)! < validObj.getNumberRangeValidation(tfContactNo.text!).min || (tfContactNo.text?.count)! > validObj.getNumberRangeValidation(tfContactNo.text!).max {
                    emptyFields = emptyFields + "\n" + "Refferal number is invalid number".localized
                }else if (tfConfirmContactNo.text?.count)! < 3 {
                    emptyFields = emptyFields + "\n" + "Confirm refferal number missing".localized
                }else if tfContactNo.text != tfConfirmContactNo.text {
                    emptyFields = emptyFields + "\n" + "Refferal and confirm refferal number mismaching".localized
                }else if tfContactNo.text == tfConfirmContactNo.text {
                    if RegistrationModel.shared.Recommended == "" {
                        emptyFields = emptyFields + "\n" + "Refferal number missing".localized
                    }
                }
            }else {
                if (tfContactNo.text?.count)! == 0 {
                    emptyFields = emptyFields + "\n" + "Refferal number missing".localized
                }else if (tfContactNo.text?.count)! < 3 {
                    emptyFields = emptyFields + "\n" + "Refferal number is invalid number".localized
                }else if (tfConfirmContactNo.text?.count)! == 0 {
                    emptyFields = emptyFields + "\n" + "Confirm Refferal number missing".localized
                }else if tfContactNo.text != tfConfirmContactNo.text {
                    emptyFields = emptyFields + "\n" + "Refferal and confirm refferal number mismaching".localized
                }else if tfContactNo.text == tfConfirmContactNo.text {
                    if RegistrationModel.shared.Recommended == "" {
                        emptyFields = emptyFields + "\n" + "Refferal number missing".localized
                    }
                }
            }
        }else {
            if RegistrationModel.shared.Recommended == "" {
                emptyFields = emptyFields + "\n" + "Referral number missing".localized
            }
        }
        
        if RegistrationModel.shared.EmailId == "" {
            emptyFields = emptyFields + "\n" + "Email ID missing".localized
        }
        
        if RegistrationModel.shared.AccountType == 0 {
            if isAgentCodeHidden == false {
                if RegistrationModel.shared.AgentAuthCode == "" {
                    emptyFields = emptyFields + "\n" + "Agent code missing".localized
                }
                if RegistrationModel.shared.ShopPicOneAwsUrl == "" {
                    emptyFields = emptyFields + "\n" + "Agent first shop image missing".localized
                }
                if RegistrationModel.shared.ShopPicTwoAwsUrl == "" {
                    emptyFields = emptyFields + "\n" + "Agent second shop image missing".localized
                }
                RegistrationModel.shared.lType = "AGENT"
            }else {
                RegistrationModel.shared.lType = "MER"
            }
        }
        
   
        
        if emptyFields == "" {
            handler (true, "")
        }else {
            handler (false, emptyFields)
        }
    }
    
    
    func setLeftViewInTextField(txtField : UITextField,codeCountry:String) {
        txtField.leftView = nil
        let stringSize: CGSize = codeCountry.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: "Zawgyi-One", size: 18) ?? UIFont.systemFont(ofSize: 18.0) ])
        let leftView = PreNameView.loadView() as! PreNameView
        leftView.frame = CGRect(x: 0, y: 0, width: stringSize.width + 15 , height: 52)
        leftView.setPreName(name: "(" + codeCountry + ")",alignment: .left)
        txtField.leftView = leftView
        txtField.leftViewMode = UITextField.ViewMode.always
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    private func removeSpaceAtLast(txtField : UITextField){
        if txtField.text?.last == " " {
            txtField.text?.removeLast()
        }
    }
}

