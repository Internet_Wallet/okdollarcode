//
//  NRCPhotoRequired.swift
//  OK
//
//  Created by Subhash Arya on 01/04/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit




protocol NRCPhotoRequiredDelegate {
    func showBusinessOrAddress()
}

class NRCPhotoRequired: OKBaseController,NRCPhtotoUploadwithScanDelegate {
    
    var delegate: NRCPhotoRequiredDelegate?
 
    @IBOutlet weak var imgImage1: UIImageView!
    @IBOutlet weak var imgImage2: UIImageView!
    @IBOutlet weak var lblFront: UILabel!
    @IBOutlet weak var lblRear: UILabel!
    var isNextViewshown: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dHeight = 120
        self.isNextViewshown = false
        let image1 = RegistrationModel.shared.IdPhoto.replacingOccurrences(of: " ", with: "%20")
        let image2 = RegistrationModel.shared.IdPhoto1.replacingOccurrences(of: " ", with: "%20")
        let urlFront = URL(string: image1)
        imgImage1.sd_setImage(with: urlFront, placeholderImage: UIImage(named: "nrc"))
        let urlRear = URL(string: image2)
        imgImage2.sd_setImage(with: urlRear, placeholderImage: UIImage(named: "nrc"))
       // UserDefaults.standard.set(true, forKey: "NRC_HIEGHT")
    }
    
    
    @IBAction func onClickImageAction(_ sender: UIButton){
        DispatchQueue.main.async {
            let image = UIImage()
            var imgList = [UIImage]()
            let url1Exits =  RegistrationModel.shared.IdPhoto
            let url2Exits =  RegistrationModel.shared.IdPhoto1
            if url1Exits != "" {
                if let img = image.convertURLToImage(str: url1Exits) {
                    imgList.append(img)
                }
            }
            if url2Exits != "" {
                if let img = image.convertURLToImage(str: url2Exits) {
                    imgList.append(img)
                }
            }
            let imageVC = UIStoryboard(name: "Registration", bundle: Bundle.main).instantiateViewController(withIdentifier: "NRCPhtotoUploadwithScan")  as! NRCPhtotoUploadwithScan
            if RegistrationModel.shared.IDType == "01" {
                imageVC.setMarqueLabelInNavigation(title: "NRC Images".localized)
                imageVC.buttonName = ["Tap to take Photo of your NRC's front side".localized, "Tap to take Photo of your NRC's back side".localized]
                imageVC.buttonTitleName =  ["Front side of your NRC".localized,"Back side of your NRC".localized]
            }else {
                imageVC.setMarqueLabelInNavigation(title: "Passport Images".localized)
                imageVC.buttonName = ["Tap to take Photo of your Passport's front side".localized, "Tap to take Photo of your Passport's back side".localized]
                imageVC.buttonTitleName =  ["Front side of your Passport".localized,"Back side of your Passport".localized]
            }
            imageVC.imageList = imgList
            imageVC.imagesURL = [RegistrationModel.shared.IdPhoto,RegistrationModel.shared.IdPhoto1]
            imageVC.delegate = self
            self.navigationController?.pushViewController(imageVC, animated: true)
        }
    }
    func sendImagesToMainController(images: [String],screen: String, imagArr: [UIImage]) {
        RegistrationModel.shared.IdPhoto = images[0].replacingOccurrences(of: " ", with: "%20")
        RegistrationModel.shared.IdPhoto1 = images[1].replacingOccurrences(of: " ", with: "%20")
            DispatchQueue.main.async {
                
                
                if !(self.isNextViewshown ?? false) {
                    if (RegistrationModel.shared.IdPhoto != "") && (RegistrationModel.shared.IdPhoto1 != "") {
                        self.isNextViewshown = true
                        if let del = self.delegate {
                            del.showBusinessOrAddress()
                        }
                    }
                }
            
                self.imgImage1.sd_setImage(with: URL(string: RegistrationModel.shared.IdPhoto), placeholderImage: UIImage(named: "nrc"))
                self.imgImage2.sd_setImage(with: URL(string: RegistrationModel.shared.IdPhoto1), placeholderImage: UIImage(named: "nrc"))
                //self.getTokenForNRCScan()
            }
    }

}
