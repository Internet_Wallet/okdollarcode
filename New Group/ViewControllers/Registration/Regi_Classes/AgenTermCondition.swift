//
//  AgenTermCondition.swift
//  OK
//
//  Created by Subhash Arya on 11/03/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol  AgenTermConditionDelegate{
    func removeVC(vc : UIViewController)
}

class AgenTermCondition: UIViewController {
    @IBOutlet weak var webView: UIWebView!
    var delegate : AgenTermConditionDelegate?
    
    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        lblHeader.text = "Terms and Conditions".localized
        btnAccept.setTitle("Accept".localized, for: .normal)
        btnReject.setTitle("Reject".localized, for: .normal)
        
        
        do {
            var filename = ""
            if ok_default_language == "my"{
                filename = "agentTerm_Burmese"
            }else {
                filename = "agentTerm_Englsih"
            }
            guard let filePath = Bundle.main.path(forResource: filename, ofType: "html") else { return }
            
            let contents =  try String(contentsOfFile: filePath, encoding: .utf8)
            let baseUrl = URL(fileURLWithPath: filePath)
            self.webView.loadHTMLString(contents as String, baseURL: baseUrl)
        }catch { print ("File HTML error") }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func btnRejectAccept(_ sender: UIButton) {
        if sender.tag == 1 {
            //self.view.removeFromSuperview()
            self.dismiss(animated: false, completion: nil)
        }else {
            if let dg = self.delegate{
                dg.removeVC(vc: self)
            }
            self.dismiss(animated: false, completion: nil)
        }
    }
}
