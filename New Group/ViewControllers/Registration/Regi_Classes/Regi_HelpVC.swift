//
//  HelpSupportVC.swift
//  OK
//
//  Created by Subhash Arya on 16/03/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

struct laguageFlag {
    var flag1 = ""
    var falg2 = ""
    init(f1 : String, f2 : String) {
        self.flag1 = f1
        self.falg2 = f2
    }
}

import UIKit

class Regi_HelpVC: OKBaseController,RegistrationBaseVCDelegate,ReRegistrationVCVCDelegate,WebServiceResponseDelegate {
   
    let appDel = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblHelpSupport: UILabel!{
        didSet{
            lblHelpSupport.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblRegi: UILabel!{
        didSet{
            lblRegi.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgMyFlag: UIImageView!
    @IBOutlet weak var imgEngFlag: UIImageView!
    @IBOutlet weak var imgUniCode: UIImageView!
    @IBOutlet weak var lblUniCode: UILabel!{
        didSet {
            lblUniCode.text = "ယူနစ္ကုဒ္"
        }
    }
    var langModel : laguageFlag?

    @IBOutlet weak var viewEnglish: UIView! {
        didSet {
            self.viewEnglish.backgroundColor = UIColor.lightGray
            self.viewEnglish.layer.cornerRadius = 15.0
        }
    }
    @IBOutlet weak var lblSwitchAccount: UILabel!{
        didSet{
            lblSwitchAccount.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var viewUnicode: UIView!{
        didSet {
            self.viewUnicode.backgroundColor = UIColor.lightGray
            self.viewUnicode.layer.cornerRadius = 15.0
        }
    }
    
    @IBOutlet weak var viewMyanmar: UIView! {
        didSet {
            self.viewMyanmar.backgroundColor = UIColor.lightGray
            self.viewMyanmar.layer.cornerRadius = 15.0

        }
    }
    @IBOutlet weak var lblEnglish: UILabel! {
        didSet {
            lblEnglish.text = "English"
        }
    }
    @IBOutlet weak var lblMyanmar: UILabel! {
        didSet {
           lblMyanmar.text = "ေဇာ္ဂ်ီ"
        }
    }
    
    let lblMarque = MarqueeLabel()
    var initialScreenFrom = false
    var englishLanguage = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bgView.isHidden = true
        UserDefaults.standard.set(false, forKey: "RigiRedirect")
        imgMyFlag.layer.borderWidth = 1.0
        imgMyFlag.layer.borderColor = UIColor.white.cgColor
        imgEngFlag.layer.borderWidth = 1.0
        imgEngFlag.layer.borderColor = UIColor.white.cgColor
        imgUniCode.layer.borderWidth = 1.0
        imgUniCode.layer.borderColor = UIColor.white.cgColor
        
        if initialScreenFrom {
            if let object = userDef.value(forKey: "PreloginSuccess") as? [Any], let numberString = object.first as? String {
                let numberArray = numberString.components(separatedBy: ",")
                if numberArray.count > 1 {
                    let agentCode  = getAgentCode(numberWithPrefix: numberArray.first ?? "", havingCountryPrefix: numberArray[1])
                    ReRegistrationModel.shared.MobileNumber = agentCode
                    
                }
            }
        }
        navigationTitleViewSetup()
        getDataFromApi()
    }
   
    
    override func viewDidDisappear(_ animated: Bool) {
        if !UserDefaults.standard.bool(forKey: "RigiRedirect") {
           setupTitleNavigation()
        }else {
            
        }
       
    }

    override func viewWillAppear(_ animated: Bool) {
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        UserDefaults.standard.set(ReRegistrationModel.shared.Language, forKey:"currentLanguage")
        appDel.setSeletedlocaLizationLanguage(language: ReRegistrationModel.shared.Language)
        lblHelpSupport.font = UIFont(name: appFont, size: appFontSize)
        lblRegi.font = UIFont(name: appFont, size: appFontSize)
        lblSwitchAccount.font = UIFont(name: appFont, size: appFontSize)
        lblHelpSupport.text = "Help & Support".localized
        lblRegi.text = "New User Registration".localized
        lblSwitchAccount.text = "Switch Account".localized
        if ReRegistrationModel.shared.Language.localizedLowercase == "my" {
            viewMyanmar.backgroundColor = kYellowColor
            viewEnglish.backgroundColor =  UIColor.lightGray
            viewUnicode.backgroundColor = UIColor.lightGray
        }else if ReRegistrationModel.shared.Language.localizedLowercase == "en" {
            viewEnglish.backgroundColor =  kYellowColor
            viewMyanmar.backgroundColor = UIColor.lightGray
            viewUnicode.backgroundColor = UIColor.lightGray
        }else {
            viewEnglish.backgroundColor = UIColor.lightGray
            viewMyanmar.backgroundColor = UIColor.lightGray
            viewUnicode.backgroundColor = kYellowColor
        }
        
        if !UserDefaults.standard.bool(forKey: "RigiRedirect") {
             self.setupImageInNavigation()
        }
        
    }
    
    func updateLanguageModel(language: String) {
        if language == "my" {
            viewMyanmar.backgroundColor = kYellowColor
            viewEnglish.backgroundColor =  UIColor.lightGray
            viewUnicode.backgroundColor = UIColor.lightGray
        }else if language == "en" {
            viewEnglish.backgroundColor =  kYellowColor
            viewMyanmar.backgroundColor = UIColor.lightGray
            viewUnicode.backgroundColor = UIColor.lightGray
        }else {
            viewEnglish.backgroundColor = UIColor.lightGray
            viewMyanmar.backgroundColor = UIColor.lightGray
            viewUnicode.backgroundColor = kYellowColor
        }
        updateLanguage(language: language)
    }
    
    
    func navigationTitleViewSetup() {
        self.navigationItem.hidesBackButton = true
        self.setupImageInNavigation()
        let currentLan =  UserDefaults.standard.value(forKey:"currentLanguage") as! String
        ReRegistrationModel.shared.Language = currentLan
        appDel.setSeletedlocaLizationLanguage(language: currentLan)
        lblHelpSupport.text = "Help & Support".localized
        lblRegi.text = "New User Registration".localized
        lblSwitchAccount.text = "Switch Account".localized

        if ReRegistrationModel.shared.Language.localizedLowercase == "my" {
            viewMyanmar.backgroundColor = kYellowColor
            viewEnglish.backgroundColor =  UIColor.lightGray
            viewUnicode.backgroundColor = UIColor.lightGray
        }else if ReRegistrationModel.shared.Language.localizedLowercase == "en" {
            viewEnglish.backgroundColor =  kYellowColor
            viewMyanmar.backgroundColor = UIColor.lightGray
            viewUnicode.backgroundColor = UIColor.lightGray
        }else {
            viewEnglish.backgroundColor = UIColor.lightGray
            viewMyanmar.backgroundColor = UIColor.lightGray
            viewUnicode.backgroundColor = kYellowColor
        }
    }
    func setupImageInNavigation() {
        bgView.isHidden = true
       // setImage.image = nil
        self.navigationItem.titleView = nil
        let imageV = UIImageView.init(frame: .init(x: 0, y: 0, width: 30, height: 30))
        imageV.contentMode = .scaleAspectFit
        imageV.image = #imageLiteral(resourceName: "ok")
        self.navigationItem.titleView = imageV
    }
    
    func setupTitleNavigation() {
        bgView.isHidden = false
//        if let img = bgImage as? UIImage {
//         setImage.image = img
//        }
        self.navigationItem.titleView = nil
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: 18)
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white
        if UserDefaults.standard.bool(forKey: "RE_REGITRATION") {
            lblMarque.text = "Re-Regitration".localized
        }else {
            lblMarque.text = "OK$ Registration".localized
        }
         self.navigationItem.titleView = lblMarque
    }
    
    
    
    @IBAction func onClickSwitchAction(_ sender: Any) {
        userDef.removeObject(forKey: "PreloginSuccess")
        appDel.deleteAllObjects()
        appDel.loadSplashScreen(show: false)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    @IBAction func registrationAction(_ sender: Any) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("BACK_TO_LOGIN"), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name("CHANGELAGUAGE"), object: nil)
        DispatchQueue.main.async(execute: {

            self.navigationTitleViewSetup()
         
            let sb1 = UIStoryboard(name: "ReRegistration", bundle: nil)
            let VC  = sb1.instantiateViewController(withIdentifier: "ReRegistrationNav");            VC.modalPresentationStyle = .fullScreen
           // VC.englishlanguage = self.englishLanguage
            if var topController = UIApplication.shared.keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }
                UserDefaults.standard.set(false, forKey: "RE_REGITRATION")
                ReRegistrationModel.shared.screenFrom = "Regi_HelpVC"
                topController.present(VC, animated: false, completion: nil)
            }
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(self.navigateToLoginPage),
                name: NSNotification.Name(rawValue: "BACK_TO_LOGIN"),
                object: nil)
            
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(self.ChangeLanguage),
                name: NSNotification.Name(rawValue: "CHANGELAGUAGE"),
                object: nil)
        })
        
    }
    
    @IBAction func helpsupportAction(_ sender: Any) {
        let viewController = UIStoryboard(name: "HelpSupport", bundle: Bundle.main).instantiateViewController(withIdentifier: "HelpNavigation")
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: true, completion: nil)
    }

    // Language Changes
    @IBAction func btnLanguageFirstAction(_ sender: Any) {
        self.updateLanguage(language: "my")
        viewEnglish.backgroundColor = UIColor.lightGray
        viewMyanmar.backgroundColor = kYellowColor
        viewUnicode.backgroundColor = UIColor.lightGray
        englishLanguage = "my"
    }
    @IBAction func btnLanguageSecondAction(_ sender: Any) {
        self.updateLanguage(language: "uni")
        viewEnglish.backgroundColor = UIColor.lightGray
        viewMyanmar.backgroundColor = UIColor.lightGray
        viewUnicode.backgroundColor = kYellowColor
        englishLanguage = "uni"
    }

    @IBAction func btnLanguageThiredAction(_ sender: Any) {
        self.updateLanguage(language: "en")
        viewEnglish.backgroundColor =  kYellowColor
        viewMyanmar.backgroundColor = UIColor.lightGray
        viewUnicode.backgroundColor = UIColor.lightGray
        englishLanguage = "en"
    }
    
    func updateLanguage(language : String) {
        ReRegistrationModel.shared.Language = language
        UserDefaults.standard.set(language, forKey:"currentLanguage")
        appDel.setSeletedlocaLizationLanguage(language: language)
        lblHelpSupport.font = UIFont(name: appFont, size: appFontSize)
        lblRegi.font = UIFont(name: appFont, size: appFontSize)
        lblSwitchAccount.font = UIFont(name: appFont, size: appFontSize)
        lblHelpSupport.text = "Help & Support".localized
        lblRegi.text = "New User Registration".localized
        lblSwitchAccount.text = "Switch Account".localized
    }
    
    
    @objc private func navigateToLoginPage(notification: NSNotification){
        self.dismiss(animated: false, completion: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name("BACK_TO_LOGIN"), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name("CHANGELAGUAGE"), object: nil)
    }
    
    @objc private func ChangeLanguage(notification: NSNotification) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("BACK_TO_LOGIN"), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name("CHANGELAGUAGE"), object: nil)
        DispatchQueue.main.async(execute: {
            let sb1 = UIStoryboard(name: "ReRegistration", bundle: nil)
            let VC  = sb1.instantiateViewController(withIdentifier: "ReRegistrationNav")
            VC.modalPresentationStyle = .fullScreen
            if var topController = UIApplication.shared.keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }
                ReRegistrationModel.shared.screenFrom = "Regi_HelpVC"
                UserDefaults.standard.set(false, forKey: "RE_REGITRATION")
                topController.present(VC, animated: false, completion: nil)
            }
            //self.navigationController?.pushViewController(VC!, animated: false)
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(self.navigateToLoginPage),
                name: NSNotification.Name(rawValue: "BACK_TO_LOGIN"),
                object: nil)
            
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(self.ChangeLanguage),
                name: NSNotification.Name(rawValue: "CHANGELAGUAGE"),
                object: nil)
        })
        
    }
    
    func getDataFromApi() {
        phNumValidationsApi = []
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let floatVersion = (UIDevice.current.systemVersion as NSString).floatValue
            let stringFloat =  String(describing: floatVersion)
            let urlStr = String.init(format: Url.numberValidation, stringFloat, ReRegistrationModel.shared.MobileNumber, simid, appVersion, msid, "1")
            let getPhNumValidtnUrl = getUrl(urlStr: urlStr, serverType: .serverApp)
            let params = [String: String]()
            println_debug("REQUEST URL: \(getPhNumValidtnUrl)")
            web.genericClass(url: getPhNumValidtnUrl, param: params as AnyObject, httpMethod: "GET", mScreen: "PhNoValidation")
        } else {
            println_debug("No Internet")
        }
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        if screen == "PhNoValidation" {
            guard let castedData = json as? Data else {
                PTLoader.shared.hide()
                println_debug("Data error")
                return
            }
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode(MobNumValidationResponse.self, from: castedData)
                if let responseStr = response.data {
                    println_debug(responseStr)
                    if responseStr == "null" {
                        println_debug("Null string from server")
                    } else {
                        guard let strData = responseStr.data(using: .utf8) else { return }
                        let decRes = try decoder.decode(MobNumDataResponse.self, from: strData)
                        if let version = decRes.version, version.count > 0 {
                            if let versionStr = version[0].validationJSON, versionStr != "null"  {
                                guard let jsonData = versionStr.data(using: .utf8) else { return }
                                let finalResponse = try decoder.decode(ValidationListResponse.self, from: jsonData)
                                if let code = finalResponse.code, code == 200 {
                                    if let validations = finalResponse.validations, validations.count > 0 {
                                        phNumValidationsApi = validations
                                    }
                                }
                            }
                        } else {
                            println_debug("No data from server")
                        }
                    }
                } else {
                    PTLoader.shared.hide()
                    println_debug("No specified key from server")
                }
            } catch _ {
                println_debug("Try error")
                PTLoader.shared.hide()
            }
        }
    }
    
    
    
}
