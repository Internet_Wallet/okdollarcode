//
//  ContactInfoPersonal.swift
//  OK
//
//  Created by Subhash Arya on 07/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol  ContactInfoPersonalDelegate{
    func updateMainUIUI()
    func resetPersonalInfo()
    func showContactPickerView(viewConroller : UIViewController)
    func navigateToLoginVC()
    func selectCountry()
    func viewEndEditing()
}

class ContactInfoPersonal: OKBaseController,ContactPickerDelegate,UITextFieldDelegate,TermsConditionsRVCDelegate,WebServiceResponseDelegate,CountryViewControllerDelegate,ReferralVCDelegate,BioMetricLoginDelegate{
    
    var arrCountryObj = NSArray()
    var countryCode = ""
    var countryFlag = ""
    var btnContactTag = Int()
    var countryTag = Int()
    var emptyArray = [String.Element]()
    var emptyArray2 = [String.Element]()
    var delegate : ContactInfoPersonalDelegate?
    let validObj = PayToValidations()
    
    @IBOutlet weak var viewReferralNo: UIView!
    @IBOutlet var btnClearContactNo: UIButton!
    @IBOutlet var btnClearConfirmReferral: UIButton!
    @IBOutlet var btnClearConfirmContactNo: UIButton!
    @IBOutlet weak var tfContactNo: UITextField!
    @IBOutlet weak var viewConfirmContactNo: UIView!
    @IBOutlet weak var subViewConfirmContactNo: UIView!
    @IBOutlet weak var imgCountryConfirmNo: UIImageView!
    @IBOutlet weak var tfConfirmContactNo: UITextField!
    @IBOutlet var topConstraints: NSLayoutConstraint!
    @IBOutlet var tfANContactNo: UITextField!
    @IBOutlet var viewANConnfirmContactNO: UIView!
    @IBOutlet var viewANSubViewConfirmContactNo: UIView!
    @IBOutlet var imgANConfirmCountry: UIImageView!
    @IBOutlet var tfANConfirmContactNo: UITextField!
    @IBOutlet var viewReferalNo: UIView!
    @IBOutlet weak var topTermsConditions: NSLayoutConstraint!
    
    @IBOutlet weak var imgAdditionalInfo: UIImageView!
    @IBOutlet weak var viewAdditionalContactInfo: UIView!
    @IBOutlet weak var tfFacebook: UITextField!
    
    @IBOutlet weak var btnAN: UIImageView!
    @IBOutlet weak var btnRN: UIImageView!
    
    
    @IBOutlet weak var lblReferralHeader: MarqueeLabel!
    @IBOutlet weak var btnClearReferral: UIButton!
    
    var dicValue : Dictionary<String,Any> = Dictionary<String,Any>()
    
    @IBOutlet weak var lblHeader: MarqueeLabel!
    @IBOutlet var lblReferralNo: MarqueeLabel!
    
    @IBOutlet weak var fbClear: UIButton!
    @IBOutlet weak var btnSelectReferralNo: UIButton!
    @IBOutlet weak var btnAccept: UIButton!
    
    @IBOutlet var viewTermsConditions: UIView!
    
    @IBOutlet weak var btnALShowContact: UIButton!
    @IBOutlet weak var btnAdditionInfo: UIButton!
    @IBOutlet weak var lblAddinfo: UILabel!
    @IBOutlet weak var referralTopCons: NSLayoutConstraint!
    @IBOutlet weak var btnReset: UIButton!
     @IBOutlet weak var btnResetRefferal: UIButton!
    @IBOutlet weak var lblAddtionalMarque: MarqueeLabel!
    
    
    var countryCodeString = ""
    var countryCodeStringReferral = ""
    var isAddtionalInformationHidden = true
    var  isRNConrfirm = false
    var isANConfirmVisible = false
    var CONTACTCHATSET = ""
    var FACEBOOKCHARSET = ""
    var isAlternetNumberSelectedFromContact = false
    var isReferralNumberSelectedFromContact = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let taplbl = UITapGestureRecognizer(target: self, action: #selector(ContactInfoPersonal.addtionalInfoClicked))
        lblAddtionalMarque.addGestureRecognizer(taplbl)
        
        
        if ok_default_language == "en" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue"), for: .normal)
            btnResetRefferal.setBackgroundImage(UIImage(named: "reset_blue"), for: .normal)
        }else if ok_default_language == "my" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
            btnResetRefferal.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
        }
        lblHeader.attributedText = lblHeader.attributedStringFinal(str1: "Please enter alternative number for transfer account balance in case of sim lost".localized)
        tfContactNo.placeholder = "" //"Alternative Number".localized
        tfConfirmContactNo.placeholder = "" //"Confirm Alternative No".localized
        lblReferralHeader.attributedText = lblReferralHeader.attributedStringFinal(str1: "Referral Number (Enter OK$ Account Number)".localized)
        tfANContactNo.placeholder = "Enter Referral Number".localized
        lblAddtionalMarque.text = "Additional Information".localized
        lblAddinfo.text = "Additional Contact Information".localized
        tfFacebook.placeholder = "Enter Facebook ID".localized
        btnAccept.setTitle("ACCEPT & REGISTER".localized,for:.normal)
        fbClear.isHidden = true
        btnAN.layer.borderWidth = 1.0
        btnAN.layer.borderColor = UIColor.black.cgColor
        btnRN.layer.borderWidth = 1.0
        btnRN.layer.borderColor = UIColor.black.cgColor
        imgCountryConfirmNo.layer.borderWidth = 1.0
        imgCountryConfirmNo.layer.borderColor = UIColor.black.cgColor
        imgANConfirmCountry.layer.borderWidth = 1.0
        imgANConfirmCountry.layer.borderColor = UIColor.black.cgColor
        
        
        lblHeader.type = .continuous
        lblHeader.speed = .duration(20)
        lblHeader.fadeLength = 10.0
        lblHeader.trailingBuffer = 30.0
        lblHeader.resetLabel()
        
        lblReferralHeader.type = .continuous
        lblReferralHeader.speed = .duration(20)
        lblReferralHeader.fadeLength = 10.0
        lblReferralHeader.trailingBuffer = 30.0
        lblReferralHeader.resetLabel()
        
        btnClearContactNo.isHidden = true
        btnClearConfirmContactNo.isHidden  = true
        tfContactNo.delegate = self
        tfConfirmContactNo.delegate = self
        tfANConfirmContactNo.delegate = self
        tfANContactNo.delegate = self
        tfFacebook.delegate = self
        
        referralTopCons.constant = -57
        topConstraints.constant = -57
        topTermsConditions.constant = -97
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(ContactInfoPersonal.onClickAlternetImage))
        btnAN.addGestureRecognizer(tap)
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(ContactInfoPersonal.onClickrefferalImage))
        btnRN.addGestureRecognizer(tap1)
        self.dHeight = 97
        
        let (_,CountryCode) = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: RegistrationModel.shared.MobileNumber)
        countryCodeString = CountryCode
        countryCodeStringReferral = CountryCode
        
        let countryData = identifyCountryByCode(withPhoneNumber: RegistrationModel.shared.CountryCode)
        if let safeImage = UIImage(named: countryData.countryFlag) {
            DispatchQueue.main.async {
                self.btnAN.image = safeImage
                self.btnRN.image = safeImage
                self.tfContactNo.setPrefixNameInLeftView(prefixName: "(" + CountryCode + ")", txtField: self.tfContactNo)
                self.tfConfirmContactNo.setPrefixNameInLeftView(prefixName: "(" + CountryCode + ")", txtField: self.tfConfirmContactNo)
                self.tfANContactNo.setPrefixNameInLeftView(prefixName: "(" + CountryCode + ")", txtField: self.tfANContactNo)
                self.tfANConfirmContactNo.setPrefixNameInLeftView(prefixName: "(" + CountryCode + ")", txtField: self.tfANConfirmContactNo)
            }
        }
        
        
        FACEBOOKCHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890.@_ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာ၁၂၃၄၅၆၇၈၉၀ "
        CONTACTCHATSET = "0123456789"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {() -> Void in
            
            self.lblHeader.resetLabel()
            self.lblHeader.restartLabel()
            self.lblReferralHeader.resetLabel()
            self.lblReferralHeader.restartLabel()
        })
    }
    
    
    func updateViewHeight() {
        
        self.dHeight = self.viewTermsConditions.frame.origin.y + self.viewTermsConditions.frame.height
        self.runGuard()
        self.dHeight = self.viewTermsConditions.frame.origin.y + self.viewTermsConditions.frame.height
        self.runGuard()
        
        
    }
    
    @objc func onClickAlternetImage() {
        if let dg = self.delegate {
            countryTag = 1
            dg.selectCountry()
        }
    }
    
    @objc func onClickrefferalImage() {
        if let dg = self.delegate {
            countryTag = 2
            dg.selectCountry()
        }
    }
    
    @IBAction func selectRefferalNumber(_ sender: Any) {
        let selectRefVC = self.storyboard?.instantiateViewController(withIdentifier: "selectReferralNumberVc") as! selectReferralNumberVc
        selectRefVC.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        selectRefVC.delegate = self
        selectRefVC.modalPresentationStyle = .overCurrentContext
        self.present(selectRefVC, animated: false, completion: nil)
        
//        if let window = UIApplication.shared.keyWindow {
//            window.rootViewController?.addChild(selectRefVC)
//            selectRefVC.view.frame = .init(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
//            selectRefVC.delegate = self
//            window.addSubview(selectRefVC.view)
//            window.makeKeyAndVisible()
//        }
        
    }
    
    func dismissWithDetail(selectedOption : String, selfView : UIViewController) {
        btnSelectReferralNo.isHidden = true
       // selfView.view.removeFromSuperview()
        if selectedOption == "MySelf".localized {
            viewReferralNo.isUserInteractionEnabled = false
            let countryData = identifyCountryByCode(withPhoneNumber: RegistrationModel.shared.CountryCode)
            if let safeImage = UIImage(named: countryData.countryFlag) {
                self.btnRN.image = safeImage
                self.tfANContactNo.setPrefixNameInLeftView(prefixName: "(" + countryData.countryCode + ")", txtField: self.tfANContactNo)
                self.tfANContactNo.textColor = UIColor.gray
                if safeImage == #imageLiteral(resourceName: "myanmar") {
                    self.tfANContactNo.text = formattedMobileNo(mobilNumber: RegistrationModel.shared.MobileNumber, countryCode: countryData.countryFlag)
                }else {
                    self.tfANContactNo.text = formattedMobileNo(mobilNumber: RegistrationModel.shared.MobileNumber, countryCode: countryData.countryFlag)
                }
                self.tfANConfirmContactNo.text = formattedMobileNo(mobilNumber: RegistrationModel.shared.MobileNumber, countryCode: countryData.countryFlag)
                RegistrationModel.shared.Recommended =  RegistrationModel.shared.MobileNumber
                RegistrationModel.shared.CodeRecommended = RegistrationModel.shared.CountryCode
            }
        }else {
            tfANContactNo.becomeFirstResponder()
            btnALShowContact.isHidden = false
        }
    }
    
    func countryViewController(_ list: CountryViewController, country: Country) {
        countryCodeString = ""
        countryCodeStringReferral = ""
        
        if self.countryTag == 1{
            let btnTemp = UIButton()
            btnTemp.tag = 100
            self.btnClearContactNoAction(btnTemp)
            
            RegistrationModel.shared.Phone = ""
            RegistrationModel.shared.CodeAlternate = ""
            self.tfContactNo.text = ""
            self.tfContactNo.becomeFirstResponder()
            self.tfConfirmContactNo.text = ""
            self.btnClearContactNo.isHidden = true
            self.countryCodeString = country.dialCode
            self.btnAN.image = UIImage(named: country.code)
            self.tfContactNo.setPrefixNameInLeftView(prefixName: "(" + country.dialCode + ")", txtField: tfContactNo)
            self.tfContactNo.text = ""
            self.imgCountryConfirmNo.image = UIImage(named: country.code)
            self.imgCountryConfirmNo.layer.borderColor = UIColor.black.cgColor
            self.tfConfirmContactNo.setPrefixNameInLeftView(prefixName: "(" + country.dialCode + ")", txtField: tfConfirmContactNo)
            self.tfConfirmContactNo.text = ""
        }else {
            resetReferral(mode: "clear")
            RegistrationModel.shared.CodeRecommended = ""
            RegistrationModel.shared.Recommended = ""
            self.countryCodeStringReferral = country.dialCode
            self.btnRN.image = UIImage(named: country.code)
            self.tfANContactNo.setPrefixNameInLeftView(prefixName: "(" + country.dialCode + ")", txtField: tfANContactNo)
            self.tfANContactNo.text = ""
            self.tfANContactNo.becomeFirstResponder()
            self.imgANConfirmCountry.image = UIImage(named: country.code)
            self.imgANConfirmCountry.layer.borderColor = UIColor.black.cgColor
            self.tfANConfirmContactNo.setPrefixNameInLeftView(prefixName: "(" + country.dialCode + ")", txtField: tfANConfirmContactNo)
            self.tfANConfirmContactNo.text = ""
            
        }
        list.dismiss(animated: false, completion: nil)
    }
    
    func countryViewControllerCloseAction(_ list: CountryViewController) {
        list.dismiss(animated: false, completion: nil)
    }
    
    
    @IBAction func fbClearAction(_ sender: Any) {
        fbClear.isHidden = true
        tfFacebook.text = ""
    }
    
    @IBAction func btnClearContactNoAction(_ sender: UIButton) {
        self.tfContactNo.resignFirstResponder()
        self.tfConfirmContactNo.resignFirstResponder()
        if isAlternetNumberSelectedFromContact {
            isAlternetNumberSelectedFromContact = false
            self.imgCountryConfirmNo.image = self.btnAN.image
            self.imgCountryConfirmNo.layer.borderColor = UIColor.black.cgColor
            self.tfConfirmContactNo.setPrefixNameInLeftView(prefixName: "(" + countryCodeString + ")", txtField: tfConfirmContactNo)
        }
        self.tfContactNo.text = ""
        self.tfConfirmContactNo.text = ""
        self.btnClearContactNo.isHidden = true
        self.btnClearConfirmContactNo.isHidden = true
        self.btnAN.isUserInteractionEnabled = true
        self.tfConfirmContactNo.isUserInteractionEnabled = true
        RegistrationModel.shared.Phone = ""
        RegistrationModel.shared.CodeAlternate = ""
        
        if sender.tag == 100 {
            let countryData = identifyCountryByCode(withPhoneNumber: RegistrationModel.shared.CountryCode)
            self.btnAN.image = UIImage(named: countryData.countryFlag)
            self.tfContactNo.setPrefixNameInLeftView(prefixName: "(" + countryData.countryCode + ")", txtField: tfContactNo)
            self.imgCountryConfirmNo.image = UIImage(named: countryData.countryFlag)
            self.imgCountryConfirmNo.layer.borderColor = UIColor.black.cgColor
            self.tfConfirmContactNo.setPrefixNameInLeftView(prefixName: "(" + countryData.countryCode + ")", txtField: tfConfirmContactNo)
        }
        
        
        self.referralTopCons.constant = -57
        self.viewTermsConditions.updateConstraintsIfNeeded()
        self.viewTermsConditions.layoutIfNeeded()
        self.isANConfirmVisible = false
        if self.dHeight! <= self.viewConfirmContactNo.frame.origin.y  || self.dHeight! <= self.viewConfirmContactNo.frame.origin.y + self.viewConfirmContactNo.frame.height{
            self.dHeight = self.viewConfirmContactNo.frame.origin.y
        }else {
            self.updateViewHeight()
        }
        self.runGuard()
    }
    
    @IBAction func btnClearConfirmNoAction(_ sender: Any) {
        self.btnClearConfirmContactNo.isHidden = true
        self.tfConfirmContactNo.text = ""
        RegistrationModel.shared.Phone = ""
        tfConfirmContactNo.resignFirstResponder()
    }
    
    @IBAction func btnPhoneContactAction(_ sender: Any) {
        btnContactTag = 1
        //tfContactNo.text = ""
        guard (self.delegate?.showContactPickerView(viewConroller : self) != nil) else {
            return
        }
    }
    
    
    @IBAction func alternateNumberEditChanged(_ sender: UITextField) {
        var tempNo = RegistrationModel.shared.MobileNumber
        tempNo.removeFirst()
        tempNo.removeFirst()
        tempNo.removeFirst()
        tempNo.removeFirst()
        
        if tfContactNo.text == "0" + tempNo {
            showAlert(alertTitle: "", alertBody: "Please provide another number", alertImage: #imageLiteral(resourceName: "alert-icon"))
            tfContactNo.text?.removeLast()
        }else {
            let mbLength = (validObj.getNumberRangeValidation(sender.text!))
            if btnAN.image == UIImage(named: "myanmar") {
                if sender.text?.count == mbLength.min {
                    self.showAlternateConfirmContactTextField()
                }else if (sender.text?.count)! < mbLength.min {
                    if isANConfirmVisible {
                        self.hideAlternateConfirmContactTextField()
                    }
                }else if (sender.text?.count)! <= mbLength.max && (sender.text?.count)! > mbLength.min {
                    if isAlternetNumberSelectedFromContact {
                        self.showAlternateConfirmContactTextField()
                        isAlternetNumberSelectedFromContact = false
                    }
                }
                
                if sender.text?.count == mbLength.max {
                    tfContactNo.resignFirstResponder()
                    tfConfirmContactNo.becomeFirstResponder()
                }
            }else {
                if sender.text?.count == 13 {
                    tfContactNo.resignFirstResponder()
                    tfConfirmContactNo.becomeFirstResponder()
                }
            }
        }
    }
    
    @IBAction func referralNumberEditChanged(_ sender: UITextField) {
        
        var tempNo = RegistrationModel.shared.MobileNumber
        tempNo.removeFirst()
        tempNo.removeFirst()
        tempNo.removeFirst()
        tempNo.removeFirst()
        
        if tfANContactNo.text == "0" + tempNo {
            showAlert(alertTitle: "", alertBody: "Please provide another number".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
            tfANContactNo.text?.removeLast()
        }else {
            if btnRN.image == UIImage(named: "myanmar") {
                let mbLength = (validObj.getNumberRangeValidation(sender.text!))
                if sender.text?.count == mbLength.min {
                    showReferralConfirmNumber()
                }else if (sender.text?.count)! < mbLength.min {
                    if self.topConstraints.constant == 0 {
                        self.HideReferralConfirmNumber()
                    }
                }else if (sender.text?.count)! <= mbLength.max && (sender.text?.count)! > mbLength.min {
                    if isReferralNumberSelectedFromContact {
                        showReferralConfirmNumber()
                        isReferralNumberSelectedFromContact = false
                    }
                }
                if sender.text?.count == mbLength.max {
                    tfANContactNo.resignFirstResponder()
                    tfANConfirmContactNo.becomeFirstResponder()
                }
            }else {
                if (sender.text?.count)! > 3{
                    if !isReferralNumberSelectedFromContact {
                        if !isRNConrfirm {
                            isRNConrfirm = true
                            self.tfANConfirmContactNo.textAlignment = .left
                            self.imgANConfirmCountry.image = self.btnRN.image
                            self.imgANConfirmCountry.layer.borderColor = UIColor.black.cgColor
                            self.topConstraints.constant = 0
                        }
                    }else {
                        if isReferralNumberSelectedFromContact {
                            isReferralNumberSelectedFromContact = false
                            self.tfANConfirmContactNo.text = ""
                            self.imgANConfirmCountry.image = self.btnRN.image
                            self.imgANConfirmCountry.layer.borderColor = UIColor.black.cgColor
                            self.tfANConfirmContactNo.isUserInteractionEnabled = true
                            self.tfANConfirmContactNo.setPrefixNameInLeftView(prefixName: "(" + self.countryCodeStringReferral + ")", txtField: tfANConfirmContactNo)
                            RegistrationModel.shared.CodeRecommended = ""
                            RegistrationModel.shared.Recommended = ""
                        }else {
                            self.topConstraints.constant = -57
                            isRNConrfirm = false
                            self.HideReferralConfirmNumber()
                        }
                    }
                }else if (sender.text?.count)! < 4{
                    isRNConrfirm = false
                    self.topConstraints.constant = -57
                }
                if (sender.text?.count)! == 13 {
                    tfANContactNo.resignFirstResponder()
                    tfANConfirmContactNo.becomeFirstResponder()
                }
            }
        }
    }
    
    
    
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == tfContactNo{
            if btnAN.image == UIImage(named: "myanmar") {
                if tfContactNo.text == "" {
                    tfContactNo.text = commonPrefixMobileNumber
                }
            }
            
        btnClearConfirmContactNo.isHidden = true
        }else if textField == tfConfirmContactNo{
            tfConfirmContactNo.textAlignment = .left
            if btnAN.image == UIImage(named: "myanmar") {
                if tfConfirmContactNo.text == "" {
                    tfConfirmContactNo.text = commonPrefixMobileNumber
                }
            }
        }else if textField == tfANContactNo{
            if btnRN.image == UIImage(named: "myanmar") {
                if tfANContactNo.text == "" {
                    tfANContactNo.text = commonPrefixMobileNumber
                }
            }
            // tfANConfirmContactNo.text = ""
            btnClearConfirmReferral.isHidden = true
        }else if textField == tfANConfirmContactNo {
            if btnRN.image == UIImage(named: "myanmar") {
                if tfANConfirmContactNo.text == "" {
                    tfANConfirmContactNo.text = commonPrefixMobileNumber
                }
            }
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField == tfANContactNo {
            self.updateViewHeight()
        }else if textField == tfANConfirmContactNo {
            self.updateViewHeight()
        }else if textField == tfFacebook {
            removeSpaceAtLast(txtField: tfFacebook)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
   
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let chars = textField.text! + string;
        let mbLength = validObj.getNumberRangeValidation(chars).max
        let isRejected = validObj.getNumberRangeValidation(chars).isRejected
        if range.location == 0 && string == " " { return false }
        
        if textField == tfContactNo{
            RegistrationModel.shared.Phone = ""
            //New Changes
            if !isAlternetNumberSelectedFromContact {
                 tfConfirmContactNo.text = ""
            }
          
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CONTACTCHATSET).inverted).joined(separator: "")) { return false }
            
            if btnAN.image == UIImage(named: "myanmar") {
                if text.count < 2 { return false }
               
                
                if (range.location == 0 && range.length == 0) || (range.location == 1 && range.length == 0)
                {
                    return false
                }
                
                if range.location == 0 && range.length > 1 {
                    textField.text = "09"
                    btnClearContactNo.isHidden = true
                    return false
                }
                
                if isRejected {
                    textField.text = commonPrefixMobileNumber
                    self.btnClearContactNo.isHidden = true
                    return false
                }
                
                if chars.count > mbLength {
                    tfContactNo.resignFirstResponder()
                    return false
                }else {
                  
                }
                if text == commonPrefixMobileNumber {
                    self.btnClearContactNo.isHidden = true
                } else {
                    self.btnClearContactNo.isHidden = false
                }
            }else {
                
                if text.count > 13 {
                    tfContactNo.resignFirstResponder()
                    return false
                }else if text.count > 3{
                    if !isAlternetNumberSelectedFromContact {
                        self.showAlternateConfirmContactTextField()
                    }else {
                        if isAlternetNumberSelectedFromContact {
                            isAlternetNumberSelectedFromContact = false
                            
                            let countryData = identifyCountryByCode(withPhoneNumber: self.countryCodeString)
                            self.imgCountryConfirmNo.image = nil
                            self.imgCountryConfirmNo.image = UIImage(named: countryData.countryFlag)
                            self.imgCountryConfirmNo.layer.borderColor = UIColor.black.cgColor
                            self.tfConfirmContactNo.setPrefixNameInLeftView(prefixName: "(" + countryData.countryCode + ")", txtField: tfConfirmContactNo)
                            self.tfConfirmContactNo.isUserInteractionEnabled = true
                        }else {
                            self.hideAlternateConfirmContactTextField()
                        }
                    }
                }else {
                    if isANConfirmVisible {
                        self.hideAlternateConfirmContactTextField()
                    }
                }
                
                if text.count > 0 {
                    self.btnClearContactNo.isHidden = false
                } else {
                    self.btnClearContactNo.isHidden = true
                }
            }
        }else if textField == tfConfirmContactNo{
            
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CONTACTCHATSET).inverted).joined(separator: "")) { return false }
            
            if btnAN.image == UIImage(named: "myanmar") {
                if text.count < 2 { return false }
            }
            if text == commonPrefixMobileNumber {btnClearConfirmContactNo.isHidden = true }else {btnClearConfirmContactNo.isHidden = false}
            
            if isRejected {
                textField.text = commonPrefixMobileNumber
                btnClearConfirmContactNo.isHidden = true
                return false
            }
            
            if text == tfContactNo.text! {
                
                var mobN = ""
                if btnAN.image == UIImage(named: "myanmar"){
                    mobN = "00" + countryCodeString.dropFirst() + tfContactNo.text!.dropFirst()
                }else {
                    mobN = "00" + countryCodeString.dropFirst() + tfContactNo.text!
                }
                
                RegistrationModel.shared.Phone = mobN
                RegistrationModel.shared.CodeAlternate =  countryCodeString
                tfConfirmContactNo.resignFirstResponder()
                self.updateViewHeight()
                UserDefaults.standard.set("YES", forKey: "ALL_FIELDS_FILLED")
            }else {
                RegistrationModel.shared.Phone = ""
                RegistrationModel.shared.CodeAlternate =  ""
                
            }
            if validObj.checkMatchingNumber(string: text, withString: self.tfContactNo.text!) {
                self.tfConfirmContactNo.text = text
                if btnAN.image == UIImage(named: "myanmar") {
                    if chars.count - 1 == commonPrefixMobileNumber.count {
                        btnClearConfirmContactNo.isHidden = true
                    }
                }else {
                    if chars.count - 1 == 0 {
                        btnClearConfirmContactNo.isHidden = true
                    }
                }
                
                return false
            } else {
                if btnAN.image == UIImage(named: "myanmar") {
                    if chars.count - 1 == commonPrefixMobileNumber.count {
                        btnClearConfirmContactNo.isHidden = true
                    }
                }else {
                    if chars.count - 1 == 0 {
                        btnClearConfirmContactNo.isHidden = true
                    }
                }
                return false  }
        }else if textField == tfANContactNo{
            
            RegistrationModel.shared.Recommended = ""
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CONTACTCHATSET).inverted).joined(separator: "")) { return false }
            if btnRN.image == UIImage(named: "myanmar") {
                if text.count < 2 { return false }
                
                if (range.location == 0 && range.length == 0) || (range.location == 1 && range.length == 0)
                {
                    return false
                }
                
                if range.location == 0 && range.length > 1 {
                    textField.text = "09"
                    btnClearReferral.isHidden = true
                    return false
                }
                
                if isRejected {
                    textField.text = commonPrefixMobileNumber
                    btnClearReferral.isHidden = true
                    return false
                }
                
                if chars.count > mbLength {
                    tfANContactNo.resignFirstResponder()
                    return false
                }
                if text == commonPrefixMobileNumber {
                    self.btnClearReferral.isHidden = true
                } else {
                    self.btnClearReferral.isHidden = false
                }
            } else {
                if text.count > 13 {
                    tfANContactNo.resignFirstResponder()
                    return false
                }
                if text.count > 0 {
                    self.btnClearReferral.isHidden = false
                } else {
                    self.btnClearReferral.isHidden = true
                }
            }
        }else if textField == tfANConfirmContactNo {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CONTACTCHATSET).inverted).joined(separator: "")) { return false }
            if btnRN.image == UIImage(named: "myanmar") {
                if text.count < 2 {
                    btnClearConfirmReferral.isHidden = true
                    return false
                } else {
                    btnClearConfirmReferral.isHidden = false
                }
            } else {
                if text.count > 0 {
                    btnClearConfirmReferral.isHidden = false
                } else {
                    btnClearConfirmReferral.isHidden = true
                }
            }
            if text == tfANContactNo.text! {
                RegistrationModel.shared.CodeRecommended = countryCodeStringReferral
                var mobN = ""
                if btnAN.image == UIImage(named: "myanmar"){
                    mobN = "00" + countryCodeStringReferral.dropFirst() + tfANContactNo.text!.dropFirst()
                }else {
                    mobN = "00" + countryCodeStringReferral.dropFirst() + tfANContactNo.text!
                }
                RegistrationModel.shared.Recommended = mobN
                tfANConfirmContactNo.resignFirstResponder()
                isRNConrfirm = true
            }else {
                RegistrationModel.shared.CodeRecommended = ""
                RegistrationModel.shared.Recommended = ""
            }
            
            if validObj.checkMatchingNumber(string: text, withString: self.tfANContactNo.text!) {
                self.tfANConfirmContactNo.text = text
                if btnRN.image == UIImage(named: "myanmar") {
                    if chars.count - 1 == commonPrefixMobileNumber.count {
                        btnClearConfirmReferral.isHidden = true
                    }
                }else {
                    if chars.count - 1 == 0 {
                        btnClearConfirmReferral.isHidden = true
                    }
                }
                return false
            } else {
                if btnRN.image == UIImage(named: "myanmar") {
                    if chars.count - 1 == commonPrefixMobileNumber.count {
                        btnClearConfirmReferral.isHidden = true
                    }
                }else {
                    if chars.count - 1 == 0 {
                        btnClearConfirmReferral.isHidden = true
                    }
                }
                return false
            }
            
        }else if textField == tfFacebook {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: FACEBOOKCHARSET).inverted).joined(separator: "")) { return false }
            if text.count > 0{
                fbClear.isHidden = false
            }else {
                fbClear.isHidden = true
            }
            
            if text.count > 50 { return false }
            return restrictMultipleSpaces(str: string, textField: tfFacebook)
        }
        return true
    }
    func showAlternateConfirmContactTextField () {
        self.tfConfirmContactNo.text = ""
        self.tfConfirmContactNo.isUserInteractionEnabled = true
        self.imgCountryConfirmNo.image = btnAN.image
        self.imgCountryConfirmNo.layer.borderColor = UIColor.black.cgColor
        self.tfConfirmContactNo.setPrefixNameInLeftView(prefixName: "(" + self.countryCodeString + ")", txtField: tfConfirmContactNo)
        
        referralTopCons.constant = 0
        isANConfirmVisible = true
        if self.dHeight! <= viewConfirmContactNo.frame.origin.y + viewConfirmContactNo.frame.height{
            self.dHeight = viewConfirmContactNo.frame.origin.y + viewConfirmContactNo.frame.height
        }else {
            self.viewTermsConditions.updateConstraintsIfNeeded()
            self.viewTermsConditions.layoutIfNeeded()
            self.dHeight = self.viewTermsConditions.frame.origin.y + self.viewTermsConditions.frame.height
            self.runGuard()
            self.dHeight = self.viewTermsConditions.frame.origin.y + self.viewTermsConditions.frame.height
        }
        self.runGuard()
    }
    
    func hideAlternateConfirmContactTextField () {
        
        self.viewTermsConditions.updateConstraintsIfNeeded()
        self.viewTermsConditions.layoutIfNeeded()
        self.referralTopCons.constant = -57
        self.isANConfirmVisible = false
        self.tfConfirmContactNo.text = ""
        self.btnClearConfirmContactNo.isHidden = true
        if self.dHeight == self.viewConfirmContactNo.frame.origin.y + self.viewConfirmContactNo.frame.height {
            self.dHeight = 97
        }else{
            self.updateViewHeight()
        }
        self.runGuard()
        self.tfConfirmContactNo.text = ""
        self.btnClearConfirmContactNo.isHidden = true
        self.tfConfirmContactNo.isUserInteractionEnabled = true
        
        let countryData = identifyCountryByCode(withPhoneNumber: self.countryCodeString)
        self.imgCountryConfirmNo.image = nil
        self.imgCountryConfirmNo.image = UIImage(named: countryData.countryFlag)
        self.imgCountryConfirmNo.layer.borderColor = UIColor.black.cgColor
        self.tfConfirmContactNo.setPrefixNameInLeftView(prefixName: "(" + countryData.countryCode + ")", txtField: tfConfirmContactNo)
    }
    
    func showReferralConfirmNumber() {
        tfANConfirmContactNo.text = ""
        self.tfANConfirmContactNo.textAlignment = .left
        self.imgANConfirmCountry.image = self.btnRN.image
        self.imgANConfirmCountry.layer.borderColor = UIColor.black.cgColor
        self.topConstraints.constant = 0
        self.tfANConfirmContactNo.isUserInteractionEnabled = true
        self.tfANConfirmContactNo.setPrefixNameInLeftView(prefixName: "(" + self.countryCodeStringReferral + ")", txtField: tfANConfirmContactNo)
        RegistrationModel.shared.CodeRecommended = ""
        RegistrationModel.shared.Recommended = ""
    }
    
    
    func HideReferralConfirmNumber() {
        self.tfANConfirmContactNo.text = ""
        self.tfANConfirmContactNo.textAlignment = .left
        self.imgANConfirmCountry.image = self.btnRN.image
        self.imgANConfirmCountry.layer.borderColor = UIColor.black.cgColor
        self.tfANConfirmContactNo.isUserInteractionEnabled = true
        
        self.tfANConfirmContactNo.setPrefixNameInLeftView(prefixName: "(" + self.countryCodeStringReferral + ")", txtField: tfANConfirmContactNo)
        
        RegistrationModel.shared.CodeRecommended = ""
        RegistrationModel.shared.Recommended = ""
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.topConstraints.constant = -57
            self.runGuard()
        }
    }
    
    
    
    func contact(_: ContactPickersPicker, didContactFetchFailed error: NSError){ }
    func contact(_ control: ContactPickersPicker, didCancel error: NSError){ }
    func contact(_ control: ContactPickersPicker, didSelectContact contact: ContactPicker){ decodeContact(contact: contact)}
    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]){ }
    func decodeContact(contact: ContactPicker) {
        
        let phoneNumberCount = contact.phoneNumbers.count
        var phoneNumber = ""
        
        if phoneNumberCount == 1  {
            phoneNumber = "\(contact.phoneNumbers[0].phoneNumber)"
        }
        else if phoneNumberCount > 1 {
            phoneNumber = "\(contact.phoneNumbers[0].phoneNumber)"
        }else {}
        
        if phoneNumber != "" {
            phoneNumber = phoneNumber.replacingOccurrences(of: ")", with: "")
            phoneNumber = phoneNumber.replacingOccurrences(of: "(", with: "")
            phoneNumber = phoneNumber.trimmingCharacters(in: .whitespaces)
        }
        let cDetails =  identifyCountry(withPhoneNumber: phoneNumber)
        
        if cDetails.0 == "" || cDetails.1 == "" {
            countryFlag = "myanmar"
            countryCode = "+95"
            if btnContactTag == 1 {
                btnAN.image = UIImage(named: countryFlag)
                self.tfContactNo.setPrefixNameInLeftView(prefixName: "(" + countryCode + ")", txtField: tfContactNo)
            }else if btnContactTag == 2{
                btnRN.image = UIImage(named: countryFlag)
                tfANContactNo.setPrefixNameInLeftView(prefixName: "(" + countryCode + ")", txtField: tfANContactNo)
            }
        } else {
            countryFlag = cDetails.1
            countryCode = cDetails.0
            
            if btnContactTag == 1 {
                btnAN.image = UIImage(named: countryFlag)
                tfContactNo.setPrefixNameInLeftView(prefixName: "(" + countryCode + ")", txtField: tfContactNo)
            }else if btnContactTag == 2{
                btnRN.image = UIImage(named: countryFlag)
                tfANContactNo.setPrefixNameInLeftView(prefixName: "(" + countryCode + ")", txtField: tfANContactNo)
            }
        }
        let phone = phoneNumber.replacingOccurrences(of: cDetails.0, with: "0")
        
        if phone.hasPrefix("0") {
            if countryFlag == "myanmar" {
                if btnContactTag == 1 {
                    self.tfContactNo.text = phone
                }else if btnContactTag == 2 {
                    self.tfANContactNo.text = phone
                }
            } else {
                if btnContactTag == 1 {
                    self.tfContactNo.text = phoneNumber.replacingOccurrences(of: cDetails.0, with: "")
                }else if btnContactTag == 2 {
                    self.tfANContactNo.text = phoneNumber.replacingOccurrences(of: cDetails.0, with: "")
                }
            }
        } else {
            if countryFlag == "myanmar" {
                if btnContactTag == 1 {
                    self.tfContactNo.text =   "0" + phone
                }else if btnContactTag == 2 {
                    self.tfANContactNo.text =   "0" + phone
                }
            } else {
                if btnContactTag == 1 {
                    self.tfContactNo.text =   phone
                }else if btnContactTag == 2 {
                    self.tfANContactNo.text =   phone
                }
            }
        }
        
        if btnContactTag == 1 {
            
            countryCode.remove(at: countryCode.startIndex)
            var mNumber = tfContactNo.text!
            if mNumber.hasPrefix("0"){
                mNumber.remove(at: mNumber.startIndex)
            }
            let finalMob =  "00" + countryCode + mNumber
            let noToCheck = "0" + mNumber
            if finalMob.count > 8 {
                if finalMob ==  RegistrationModel.shared.MobileNumber {
                    self.hideUIWhenNumberInvalidAlternate()
                }else {
                    if countryCode == "95" {
                         let mbLength = (validObj.getNumberRangeValidation(noToCheck))
                        if !mbLength.isRejected  {
                            self.tfConfirmContactNo.text = contact.displayName()
                            RegistrationModel.shared.Phone = finalMob
                            RegistrationModel.shared.CodeAlternate = "+" + self.countryCode
                            countryCodeString = "+" + self.countryCode
                            self.tfConfirmContactNo.isUserInteractionEnabled = false
                            self.alternateNumberValide()
                        }else {
                            self.hideUIWhenNumberInvalidAlternate()
                        }
                    }else{
                        self.tfConfirmContactNo.text = contact.displayName()
                        RegistrationModel.shared.Phone = finalMob
                        RegistrationModel.shared.CodeAlternate = "+" + self.countryCode
                        countryCodeString = "+" + self.countryCode
                        self.tfConfirmContactNo.isUserInteractionEnabled = false
                        self.alternateNumberValide()
                    }
                }
            }else{
                self.hideUIWhenNumberInvalidAlternate()
            }
        }else if btnContactTag == 2 {
            
            countryCode.remove(at: countryCode.startIndex)
            var mNumber = tfANContactNo.text!
            if mNumber.hasPrefix("0"){
                mNumber.remove(at: mNumber.startIndex)
            }
            let finalMob =  "00" + countryCode + mNumber
            let noToCheck = "0" + mNumber
            if finalMob.count > 8 {
                if countryCode == "95"{
                    let mbLength = (validObj.getNumberRangeValidation(noToCheck))
                    if !mbLength.isRejected  {
                        self.tfANConfirmContactNo.text = contact.displayName()
                        RegistrationModel.shared.CodeRecommended =  "+" + self.countryCode
                        countryCodeStringReferral = "+" + self.countryCode
                        RegistrationModel.shared.Recommended = finalMob
                        self.refferalNumberValide()
                    }else {
                        self.hideUIWhenNumberInvalidReferral()
                    }
                }else{
                    self.tfANConfirmContactNo.text = contact.displayName()
                    RegistrationModel.shared.CodeRecommended =  "+" + self.countryCode
                    countryCodeStringReferral = "+" + self.countryCode
                    RegistrationModel.shared.Recommended = finalMob
                    self.refferalNumberValide()
                }
            }else {
                self.hideUIWhenNumberInvalidReferral()
            }
        }
    }
    
    private func hideUIWhenNumberInvalidReferral() {
        let countryData = identifyCountryByCode(withPhoneNumber: RegistrationModel.shared.CountryCode)
        if let safeImage = UIImage(named: countryData.countryFlag) {
            self.btnRN.image = safeImage
            self.tfANContactNo.setPrefixNameInLeftView(prefixName: "(" + countryData.countryCode + ")", txtField: tfANContactNo)
        }
        RegistrationModel.shared.CodeRecommended = ""
        RegistrationModel.shared.Recommended = ""
        DispatchQueue.main.async {
            self.tfANContactNo.text = ""
            self.tfANConfirmContactNo.text = ""
            if self.isRNConrfirm {
                self.topConstraints.constant = -57
                self.isRNConrfirm = false
                self.updateViewHeight()
                self.runGuard()
            }
        }
        self.showAlert(alertTitle: "", alertBody: "You selected invalid number, Please try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
    }
    
    private func hideUIWhenNumberInvalidAlternate() {
        if self.isANConfirmVisible{
            self.isANConfirmVisible = false
            self.referralTopCons.constant = -57
            self.updateViewHeight()
        }
        tfConfirmContactNo.text = ""
        tfContactNo.text = ""
        self.btnClearContactNo.isHidden = true
        self.btnClearConfirmContactNo.isHidden = true
        self.showAlert(alertTitle: "", alertBody: "You selected invalid number, Please try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
    }
    private func refferalNumberValide() {
        self.isRNConrfirm = true
        self.tfANConfirmContactNo.leftView = nil
        self.isReferralNumberSelectedFromContact = true
        self.btnClearConfirmReferral.isHidden = true
        self.tfANConfirmContactNo.isUserInteractionEnabled = false
        self.btnClearReferral.isHidden = false
        self.imgANConfirmCountry.image = UIImage(named: "sim_R")
        self.imgANConfirmCountry.layer.borderColor = UIColor.white.cgColor
        self.topConstraints.constant = 0
        self.updateViewHeight()
    }
    
    private func alternateNumberValide() {
        self.tfConfirmContactNo.leftView = nil
        self.btnClearContactNo.isHidden = false
        self.btnClearConfirmContactNo.isHidden = true
        self.imgCountryConfirmNo.image = UIImage(named: "sim_R")
        self.imgCountryConfirmNo.layer.borderColor = UIColor.white.cgColor
        if !(self.isANConfirmVisible){
            self.isANConfirmVisible = true
            self.referralTopCons.constant = 0
        }
        self.updateViewHeight()
        isAlternetNumberSelectedFromContact  = true
        
    }
    
    @IBAction func btnANClearContactNO(_ sender: Any) {
        tfANContactNo.resignFirstResponder()
        tfANContactNo.isUserInteractionEnabled = true
        if isReferralNumberSelectedFromContact {
            isReferralNumberSelectedFromContact = false
        }
        resetReferral(mode: "clear")
    }
    
    @IBAction func btnANConfirmClearContactAction(_ sender: Any) {
        tfANConfirmContactNo.resignFirstResponder()
        tfANConfirmContactNo.text = ""
        btnClearConfirmReferral.isHidden = true
        RegistrationModel.shared.CodeRecommended = ""
        RegistrationModel.shared.Recommended = ""
    }
    
    @IBAction func btnANPhoneContactNOACtion(_ sender: Any) {
        btnContactTag = 2
        guard (self.delegate?.showContactPickerView(viewConroller : self) != nil) else {
            return
        }
    }
    
    @IBAction func btnResetRefferalNoAction(_ sender: Any) {
        tfANContactNo.isUserInteractionEnabled = true
        self.isReferralNumberSelectedFromContact = false
        resetReferral(mode: "reset")
    }
    
    @IBAction func btnResetAction(_ sender: Any) {
        DispatchQueue.main.async{
            self.tfANContactNo.isUserInteractionEnabled = true
            self.isAlternetNumberSelectedFromContact = false
            self.tfContactNo.text = ""
            self.tfConfirmContactNo.text = ""
            self.btnClearContactNo.isHidden = true
            self.btnAN.isUserInteractionEnabled = true
            self.btnClearConfirmContactNo.isHidden = true
            self.tfConfirmContactNo.isUserInteractionEnabled = true
            self.tfContactNo.resignFirstResponder()
            self.tfConfirmContactNo.resignFirstResponder()
            self.tfANContactNo.resignFirstResponder()
            self.tfANConfirmContactNo.resignFirstResponder()
            
            RegistrationModel.shared.Phone = ""
            RegistrationModel.shared.CodeAlternate = ""
            
            let countryData = identifyCountryByCode(withPhoneNumber: RegistrationModel.shared.CountryCode)
            self.countryCodeString = countryData.countryCode
            self.btnAN.image = UIImage(named: countryData.countryFlag)
            self.tfContactNo.setPrefixNameInLeftView(prefixName: "(" + countryData.countryCode + ")", txtField: self.tfContactNo)
            
            self.imgCountryConfirmNo.image = UIImage(named: countryData.countryFlag)
            self.imgCountryConfirmNo.layer.borderColor = UIColor.black.cgColor
            self.tfConfirmContactNo.setPrefixNameInLeftView(prefixName: "(" + countryData.countryCode + ")", txtField: self.tfConfirmContactNo)
            
            self.isANConfirmVisible = false
            self.referralTopCons.constant = -57
            if self.dHeight! <= self.viewConfirmContactNo.frame.origin.y  || self.dHeight! <= self.viewConfirmContactNo.frame.origin.y + self.viewConfirmContactNo.frame.height{
                self.dHeight = self.viewConfirmContactNo.frame.origin.y
            }else {
                self.updateViewHeight()
            }
            self.runGuard()
        }
        
        
    }
    
    @IBAction func btnAdditionalInfoAction(_ sender: Any) {
        
        if isAddtionalInformationHidden {
            topTermsConditions.constant = 0
            imgAdditionalInfo.image = UIImage(named: "radio_select")
            isAddtionalInformationHidden = false
            self.updateViewHeight()
        }else {
            topTermsConditions.constant = -97
            imgAdditionalInfo.image = UIImage(named: "radio")
            isAddtionalInformationHidden = true
            self.updateViewHeight()
        }
    }
    
    @objc
    func addtionalInfoClicked(sender:UITapGestureRecognizer) {
        println_debug("tap working")
        if isAddtionalInformationHidden {
            topTermsConditions.constant = 0
            imgAdditionalInfo.image = UIImage(named: "radio_select")
            isAddtionalInformationHidden = false
            self.updateViewHeight()
        }else {
            topTermsConditions.constant = -97
            imgAdditionalInfo.image = UIImage(named: "radio")
            isAddtionalInformationHidden = true
            self.updateViewHeight()
        }
    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful {
            self.uploadProfileAndSingnatureImages(handler: {(success) in
                if success {
                    DispatchQueue.main.async {
                        let sb = UIStoryboard(name: "Registration" , bundle: nil)
                        let termsConditionsRVC  = sb.instantiateViewController(withIdentifier: "TermsConditionsRVC") as! TermsConditionsRVC
                        termsConditionsRVC.modalPresentationStyle = .overCurrentContext
                        termsConditionsRVC.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                        termsConditionsRVC.delegate = self
                        self.present(termsConditionsRVC, animated: false, completion: nil)
                        
                        //self.addChild(termsConditionsRVC)
//                        if let window = UIApplication.shared.keyWindow {
//                            window.rootViewController?.addChild(termsConditionsRVC)
//                            termsConditionsRVC.view.frame = .init(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
//                            termsConditionsRVC.delegate = self
//                            window.addSubview(termsConditionsRVC.view)
//                            window.makeKeyAndVisible()
//                        }
                    }
                }else {
                    
                    self.showAlert(alertTitle: "", alertBody:"Image uploading failed. Please press again on Accept and Register button".localized , alertImage: #imageLiteral(resourceName: "alert-icon"))
                }
            })
        }else{
            
        }
    }
    
    
    @IBAction func btnAcceotAndRegisterAction(_ sender: Any) {
        if let del = delegate {
            del.viewEndEditing()
        }
        self.callRegistrationAPI()
        
    }
    
    private func callRegistrationAPI() {
        RegistrationModel.shared.FBEmailId = tfFacebook.text!
        checkEmptyField(handler: { (success,emptField) in
            if success {
                OKPayment.main.authenticateForRegistration(screenName: "Login", delegate: self)
            }else{
                showAlert(alertTitle: "Please Fill All Mandatory Fields".localized, alertBody: emptField , alertImage: #imageLiteral(resourceName: "alert-icon"))
            }
        })
    }
    
    func removeFromSuperVC(vc : UIViewController) {
        //ANTONY
       // vc.view.removeFromSuperview()
        RegistrationModel.shared.setModelValues()
        let dic = RegistrationModel.shared.wrapDataModel() // as? Dictionary<String, Any>
        println_debug(dic)
        
        if appDelegate.checkNetworkAvail() {
            let web = WebApiClass()
            web.delegate = self
            let urlStr   = Url.URL_Registration
            let url = getUrl(urlStr: urlStr, serverType: .serverApp)
            println_debug(url)
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
            web.genericClassReg(url: url, param: Dictionary<String,Any>(), httpMethod: "POST", mScreen: "mLogin", hbData: dic, authToken: nil)
           // web.genericClassReg1(url: url, param: param as AnyObject, httpMethod: "POST", mScreen: "registration")
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
        } else {
            alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img:#imageLiteral(resourceName: "r_user"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        DispatchQueue.main.async{
            progressViewObj.removeProgressView()
        }
        do {
            if let data = json as? Data {
                let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let dic = dict as? Dictionary<String,AnyObject> {
                    println_debug(dic["Msg"] as! String)
                    if dic["Code"] as? NSNumber == 200 {
                        RegistrationModel.shared.playSound()
                        
                        userDef.set(RegistrationModel.shared.Password, forKey: "passwordLogin_local")
                        if RegistrationModel.shared.PasswordType == "0" {
                            userDef.set(false, forKey: "passwordLoginType")
                            ok_password_type = false
                        } else {
                            userDef.set(true, forKey: "passwordLoginType")
                            ok_password_type = true
                        }
                        ok_password = RegistrationModel.shared.Password
                        UserDefaults.standard.set(0, forKey: "App_Password_Type")
                        guard(self.delegate?.navigateToLoginVC() != nil) else {
                            return
                        }
                    }else {
                        showAlert(alertTitle: "", alertBody: dic["Msg"] as! String , alertImage: #imageLiteral(resourceName: "r_user"))
                    }
                }
            }
        } catch {
            showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))
        }
    }
    
    func resetReferral(mode : NSString) {
        
        isRNConrfirm = true
        RegistrationModel.shared.CodeRecommended = ""
        RegistrationModel.shared.Recommended = ""
        tfANContactNo.resignFirstResponder()
        tfANConfirmContactNo.resignFirstResponder()
        if mode == "reset" {
            let countryData = identifyCountryByCode(withPhoneNumber: RegistrationModel.shared.CountryCode)
            if let safeImage = UIImage(named: countryData.countryFlag) {
                DispatchQueue.main.async{
                    self.btnRN.image = safeImage
                    self.tfANContactNo.setPrefixNameInLeftView(prefixName: "(" + countryData.countryCode + ")", txtField: self.tfANContactNo)
                    self.btnSelectReferralNo.isHidden = false
                }
            }
            viewReferralNo.isUserInteractionEnabled = true
            btnALShowContact.isHidden = true
        } else {
            
        }
        
        self.tfANContactNo.textColor = UIColor.black
        self.tfANContactNo.text = ""
        self.tfANConfirmContactNo.text = ""
        self.btnClearReferral.isHidden = true
        self.btnClearConfirmReferral.isHidden = true
        self.tfANConfirmContactNo.isUserInteractionEnabled = true
        imgANConfirmCountry.image = btnAN.image
        self.imgANConfirmCountry.layer.borderColor = UIColor.black.cgColor
        self.tfANConfirmContactNo.setPrefixNameInLeftView(prefixName: "(" + "+95" + ")", txtField: tfANConfirmContactNo)
        
        if isRNConrfirm {
            isRNConrfirm = false
            DispatchQueue.main.async{
                self.topConstraints.constant = -57
                self.updateViewHeight()
            }
        }
    }
    
    
    func runGuard(){
        guard (self.delegate?.updateMainUIUI() != nil) else {
            return
        }
    }
    
    func parsingFeeLimitKickbackResponse(xml: String) {
        let xml = SWXMLHash.parse(xml)
        self.enumerate(indexer: xml)
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            dicValue[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
    func formattedMobileNo(mobilNumber mNo : String ,countryCode code: String)-> String {
        var mNumb = mNo
        mNumb.remove(at: mNumb.startIndex)
        mNumb.remove(at: mNumb.startIndex)
        mNumb.remove(at: mNumb.startIndex)
        mNumb.remove(at: mNumb.startIndex)
        if code == "myanmar" {
            let finalMobNum = "0" + mNumb
            return finalMobNum
        }
        return mNumb
    }
    
    private func uploadProfileAndSingnatureImages(handler : @escaping (_ isSuccess : Bool) -> Void) {
        
        let paramString : [String:Any] = [
            "MobileNumber":RegistrationModel.shared.MobileNumber,
            "Base64String":[RegistrationModel.shared.signature,"","","",""]
        ]
        
        VillageManager.shared.uploadImageToS3(imageBase64String :paramString, handler: {(imageURL,success) in
            if success {
                RegistrationModel.shared.signatureAwsUrl = imageURL[0].replacingOccurrences(of: " ", with: "%20")
                   handler(true)
//                self.uploadOptionalIamges(handler: {(success) in
//                    if success {
//                        handler(true)
//                    }else {
//                        handler(false)
//                    }
//                })
            }else {
                handler(false)
            }
        })
    }
    
    private func uploadOptionalIamges(handler : @escaping (_ isSuccess : Bool) -> Void) {
        let paramString : [String:Any] = [
            "MobileNumber":RegistrationModel.shared.MobileNumber,
            "Base64String":[ RegistrationModel.shared.IdPhoto,RegistrationModel.shared.IdPhoto1,"","",""]
        ]
        VillageManager.shared.uploadImageToS3(imageBase64String :paramString, handler: {(imageURL,success) in
            if success {
                if RegistrationModel.shared.IdPhoto != "" {
                    RegistrationModel.shared.IdPhotoAwsUrl = imageURL[0].replacingOccurrences(of: " ", with: "%20")
                }
                if RegistrationModel.shared.IdPhoto1 != "" {
                    RegistrationModel.shared.IdPhoto1AwsUrl = imageURL[1].replacingOccurrences(of: " ", with: "%20")
                }
                handler(true)
            }else {
                handler(false)
            }
        })
    }
    
    func checkEmptyField(handler : (_ success : Bool ,_ emptyField : String) -> Void){
        var emptyFields = ""
        
        if RegistrationModel.shared.Name == "" {
            emptyFields =  "User Name".localized
        }else if RegistrationModel.shared.Name.contains(find: ",") {
            let token = RegistrationModel.shared.Name.components(separatedBy: ",")
            if token[1].count < 3 {
                emptyFields = emptyFields + "\n" + "User Name must be more than 2 characters".localized
            }
        }
        
        if RegistrationModel.shared.DateOfBirth == "" {
            emptyFields = emptyFields + "\n" + "Date of Birth".localized
        }
        
        if RegistrationModel.shared.ProfilePicAwsUrl == "" {
            emptyFields = emptyFields + "\n" + "Profile Image".localized
        }
        
        if RegistrationModel.shared.Father == "" {
            emptyFields = emptyFields + "\n" + "Father Name".localized
        }else if RegistrationModel.shared.Father.contains(find: ",") {
            let token = RegistrationModel.shared.Father.components(separatedBy: ",")
            if token[1].count < 3 {
                emptyFields = emptyFields + "\n" + "Father Name must be more than 2 characters".localized
            }
        }
        
        if RegistrationModel.shared.EmailId == "" {
            emptyFields = emptyFields + "\n" + "Email ID".localized
        }else {
            if isValidEmail(testStr: RegistrationModel.shared.EmailId) {
                
            }else {
                emptyFields = emptyFields + "\n" + "Invalid email format".localized
            }
        }
        
        if RegistrationModel.shared.IDType == "01" {
            if RegistrationModel.shared.NRC == "" {
                emptyFields = emptyFields + "\n" + "NRC number".localized
            }else if RegistrationModel.shared.NRC.contains(find: "@") {
                let token = RegistrationModel.shared.NRC.components(separatedBy: ")")
                if token[1].count < 6 {
                    emptyFields = emptyFields + "\n" + "NRC number must be 6 characters".localized
                }
            }
            
            if RegistrationModel.shared.IdPhoto == "" {
                emptyFields = emptyFields + "\n" + "NRC Front Image Missing".localized
            }
            if RegistrationModel.shared.IdPhoto1 == "" {
                emptyFields = emptyFields + "\n" + "NRC Back Image Missing".localized
            }
        }else if RegistrationModel.shared.IDType == "04" {
            if RegistrationModel.shared.CountryOfCitizen == "" {
                emptyFields = emptyFields + "\n" + "Country of Citizen".localized
            }
            
            if RegistrationModel.shared.NRC == "" {
                emptyFields = emptyFields + "\n" + "Passport Number".localized
            }else if RegistrationModel.shared.NRC.count < 5 {
                emptyFields = emptyFields + "\n" + "Passport Number must be more than 4 characters".localized
            }
            
            if RegistrationModel.shared.IdExpDate == "" {
                emptyFields = emptyFields + "\n" + "Passport Expiry Date".localized
            }
            
            if RegistrationModel.shared.IdPhoto == "" {
                emptyFields = emptyFields + "\n" + "Passport Front Image Missing".localized
            }
            if RegistrationModel.shared.IdPhoto1 == "" {
                emptyFields = emptyFields + "\n" + "Passport Back Image Missing".localized
            }
        }else{
            emptyFields = emptyFields + "\n" + "ID Type".localized
        }
        
        if RegistrationModel.shared.AddressType == "" {
            emptyFields = emptyFields + "\n" + "Address Type".localized
        }else {
            if RegistrationModel.shared.State == "" {
                emptyFields = emptyFields + "\n" + "State/Division Name".localized
            }
            if RegistrationModel.shared.Township == "" {
                emptyFields = emptyFields + "\n" + "Township Name".localized
            }
            
            if RegistrationModel.shared.Address2 == "" {
                emptyFields = emptyFields + "\n" + "Street Name".localized
            }
            
            if  RegistrationModel.shared.HouseBlockNo == "" && RegistrationModel.shared.FloorNumber ==  "" && RegistrationModel.shared.RoomNumber == "" {
                emptyFields = emptyFields + "\n" + "House No/Floor No/Room No".localized
            }
        }
        
        if RegistrationModel.shared.PasswordType == "" {
            emptyFields = emptyFields + "\n" + "Security Type".localized
        }
        
        
        if RegistrationModel.shared.Password == "" {
            emptyFields = emptyFields + "\n" + "Enter Password".localized
        }else if RegistrationModel.shared.Password.count < 6{
            emptyFields = emptyFields + "\n" + "Password must be more than 5 digits".localized
        }
        
        if RegistrationModel.shared.PasswordType == "0" {
            if RegistrationModel.shared.ConfirmPassword == "" {
                emptyFields = emptyFields + "\n" + "Enter Confirm Password".localized
            }else if RegistrationModel.shared.Password != RegistrationModel.shared.ConfirmPassword {
                emptyFields = emptyFields + "\n" + "Password and Confirm Password does not matched".localized
            }
        }
        
        if RegistrationModel.shared.SecurityQuestionAnswer == "" {
            emptyFields = emptyFields + "\n" + "Security Answer".localized
        }
        
        if RegistrationModel.shared.signature == "" {
            emptyFields = emptyFields + "\n" + "User Signature".localized
        }
        
        if !isAlternetNumberSelectedFromContact {
            if countryCodeString == "+95" {
                if (tfContactNo.text?.count)! < 3 {
                    emptyFields = emptyFields + "\n" + "Alternate number missing".localized
                }else if (tfContactNo.text?.count)! < validObj.getNumberRangeValidation(tfContactNo.text!).min || (tfContactNo.text?.count)! > validObj.getNumberRangeValidation(tfContactNo.text!).max {
                    emptyFields = emptyFields + "\n" + "Alternate number is invalid number".localized
                }else if (tfConfirmContactNo.text?.count)! > 2 {
                    if tfContactNo.text == tfConfirmContactNo.text{
                        if RegistrationModel.shared.Phone == "" {
                            emptyFields = emptyFields + "\n" + "Alternate number missing".localized
                        }
                    }else {
                        emptyFields = emptyFields + "\n" + "Alternate and confirm alternate number mismatching".localized
                    }
                }else {
                    emptyFields = emptyFields + "\n" + "Confirm alternate number missing".localized
                }
            }else {
                if (tfContactNo.text?.count)! == 0 {
                    emptyFields = emptyFields + "\n" + "Alternate number missing".localized
                }else if (tfContactNo.text?.count)! < 3 {
                    emptyFields = emptyFields + "\n" + "Alternate number is invalid number".localized
                }else if (tfConfirmContactNo.text?.count)! == 0 {
                   emptyFields = emptyFields + "\n" + "Confirm alternate number missing".localized
                }else if tfContactNo.text != tfConfirmContactNo.text {
                    emptyFields = emptyFields + "\n" + "Alternate and confirm alternate number mismatching".localized
                }else if tfContactNo.text == tfConfirmContactNo.text {
                    if RegistrationModel.shared.Phone == "" {
                       emptyFields = emptyFields + "\n" + "Alternate number missing".localized
                    }
                }
            }
        }else {
            if RegistrationModel.shared.Phone == "" {
                emptyFields = emptyFields + "\n" + "Alternate number missing".localized
            }
        }
        
        if !isReferralNumberSelectedFromContact {
            if countryCodeStringReferral == "+95" {
                if (tfANContactNo.text?.count)! < 3 {
                    emptyFields = emptyFields + "\n" + "Referral number missing".localized
                }else if (tfANContactNo.text?.count)! < validObj.getNumberRangeValidation(tfANContactNo.text!).min || (tfANContactNo.text?.count)! > validObj.getNumberRangeValidation(tfANContactNo.text!).max {
                    emptyFields = emptyFields + "\n" + "Referral number is invalid number".localized
                }else if (tfANConfirmContactNo.text?.count)! < 3 {
                    emptyFields = emptyFields + "\n" + "Confirm referral number missing".localized
                }else if tfANContactNo.text != tfANConfirmContactNo.text {
                    emptyFields = emptyFields + "\n" + "Referral and confirm referral number mismatching".localized
                }else if tfANContactNo.text == tfANConfirmContactNo.text {
                    if RegistrationModel.shared.Recommended == "" {
                        emptyFields = emptyFields + "\n" + "Referral number missing".localized
                    }
                }
            }else {
                if (tfANContactNo.text?.count)! == 0 {
                    emptyFields = emptyFields + "\n" + "Referral number missing".localized
                }else if (tfANContactNo.text?.count)! < 3 {
                    emptyFields = emptyFields + "\n" + "Referral number is invalid number".localized
                }else if (tfANConfirmContactNo.text?.count)! == 0 {
                    emptyFields = emptyFields + "\n" + "Confirm referral number missing".localized
                }else if tfANContactNo.text != tfANConfirmContactNo.text {
                    emptyFields = emptyFields + "\n" + "Referral and confirm referral number mismatching".localized
                }else if tfANContactNo.text == tfANConfirmContactNo.text {
                    if RegistrationModel.shared.Recommended == "" {
                        emptyFields = emptyFields + "\n" + "Referral number missing".localized
                    }
                }
            }
        }else {
            if RegistrationModel.shared.Recommended == "" {
                emptyFields = emptyFields + "\n" + "Referral number missing".localized
            }
        }
        
        if RegistrationModel.shared.EmailId == "" {
            emptyFields = emptyFields + "\n" + "Email ID missing".localized
        }
        
        if emptyFields == "" {
            handler (true, "")
        }else {
            handler (false, emptyFields)
        }
    }
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    private func removeSpaceAtLast(txtField : UITextField){
        if txtField.text?.last == " " {
            txtField.text?.removeLast()
        }
    }
    
}

extension UITextField {
    
    func setPrefixNameInLeftView(prefixName : String, txtField : UITextField) {
        txtField.leftView = nil
        let myString = prefixName
        let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: "Zawgyi-One", size: 18) ?? UIFont.systemFont(ofSize: 18.0) ])
        let lblName = UILabel()
        lblName.frame = CGRect(x: 0, y: -1, width:stringSize.width + 10 , height: 55)
        lblName.textAlignment = .left
        lblName.font =  UIFont(name: "Zawgyi-One", size: 16)
        lblName.text = myString
        let btnView = UIView()
        btnView.frame = CGRect(x: 0, y: 0, width: lblName.frame.width , height: 55)
        btnView.addSubview(lblName)
        txtField.leftView = btnView
        txtField.leftViewMode = UITextField.ViewMode.always
    }
}
