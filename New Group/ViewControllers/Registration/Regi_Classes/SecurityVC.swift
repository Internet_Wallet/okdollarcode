//
//  SecurityVC.swift
//  OK
//
//  Created by Subhash Arya on 29/03/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

let pass_rigix = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!@#$%^&*"
let ans_rigix = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 "
protocol SecurityVCDelegate1{
    func updateMainUIUI()
    func showSignatureVC()
    func resetSecurityLock()
    func showPatternVC()
}

class SecurityVC: OKBaseController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    var delegate : SecurityVCDelegate1?
    var arrindex = [String]()
    var arrIndexSection1 = [String]()
    var noOfSection  = 0
    @IBOutlet weak var securityTable: UITableView!
    let validObj = PayToValidations()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dHeight = 80
        securityTable.delegate = self
        securityTable.dataSource = self
        arrindex = ["0"]
        arrIndexSection1 = []
    }

    func runGuard() {
        guard(self.delegate?.updateMainUIUI() != nil) else {
            return
        }
    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return arrindex.count
        }else {
            return arrIndexSection1.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell : PasswordheaderCell = (tableView.dequeueReusableCell(withIdentifier: "PasswordheaderCell") as? PasswordheaderCell)!
                cell.delegate = self
                return cell
            }else if indexPath.row == 1 || indexPath.row == 2 {
                let cell : PasswordCell = (tableView.dequeueReusableCell(withIdentifier: "PasswordCell") as? PasswordCell)!
                cell.delegate = self
                if indexPath.row == 1{
                    cell.tfpassword.tag = 1
                }else if indexPath.row == 2 {
                    cell.tfpassword.placeholder = "Confirm Password"
                    cell.tfpassword.tag = 2
                    cell.imgEye.isHidden = true
                }
                cell.tfpassword.text = ""
                return cell
            }
        }else {
            if indexPath.row == 0 {
                let cell : SecurityQheaderCell = (tableView.dequeueReusableCell(withIdentifier: "SecurityQheaderCell") as? SecurityQheaderCell)!
                cell.delegate = self
                return cell
            }else {
                let cell : PasswordCell = (tableView.dequeueReusableCell(withIdentifier: "PasswordCell") as? PasswordCell)!
                cell.delegate = self
                cell.tfpassword.tag = 3
                cell.tfpassword.isSecureTextEntry = false
                cell.tfpassword.placeholder = "Enter Answer"
                cell.tfpassword.text = ""
                cell.imgEye.isHidden = true
                cell.imgLeft.image = #imageLiteral(resourceName: "r_answer")
                cell.tfpassword.autocapitalizationType = .allCharacters
                return cell
            }
        }
        return (tableView.dequeueReusableCell(withIdentifier: "cell"))!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                return 80
            }else{
                return 57
            }
        }else if indexPath.section == 1 {
            if indexPath.row == 0 {
                return 97
            }else{
                return 57
            }
        }else {
            return 0
        }
    }
}





protocol passwordDelegates : class {
    func inserRowInTable(field : Int , section : Int)
    func deleteRowInTable(mode : String)
    func resetPassword()
    func updateMainUIUI()
    func showSignatureVC()
    func showPatternVC()
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage)
    
}

extension SecurityVC : passwordDelegates {

    func updateMainUIUI() {

    }
    
    func showSignatureVC() {
    }
    
    func showPatternVC() {
    
    }
    
    func resetPassword() {
        deleteRowInTable(mode:"password")
        deleteRowInTable(mode:"pattern")
        
    }
    
    func inserRowInTable(field: Int, section: Int) {
        let indexPath = IndexPath(row: field, section: section)
        if section == 0 {
            if !arrindex.contains(String(field)) {
                arrindex.insert(String(field), at: field)
                self.securityTable.insertRows(at: [indexPath], with: .none)
            }
        }else {
            if !arrIndexSection1.contains(String(field)) {
                arrIndexSection1.insert(String(field), at: field)
                self.securityTable.insertRows(at: [indexPath], with: .none)
            }
        }
    }
    
    func deleteRowInTable(mode : String) {
        println_debug(arrindex)
        println_debug(arrIndexSection1)
        if mode == "password" {
            if arrindex.count == 3 {
                arrindex.remove(at: 2)
                let indexPath = IndexPath(row: 2, section: 0)
                securityTable.deleteRows(at:[indexPath] , with: UITableView.RowAnimation.fade)
                arrindex.remove(at: 1)
                let indexPath1 = IndexPath(row: 1 , section: 0)
                securityTable.deleteRows(at:[indexPath1] , with: UITableView.RowAnimation.fade)
            }else if arrindex.count == 2 {
                arrindex.remove(at: 1)
                let indexPath = IndexPath(row: 1, section: 0)
                securityTable.deleteRows(at:[indexPath] , with: UITableView.RowAnimation.fade)
            }
        }else {
            if arrIndexSection1.count == 2 {
                arrIndexSection1.remove(at: 1)
                let indexPath = IndexPath(row: 1, section: 1)
                securityTable.deleteRows(at:[indexPath] , with: UITableView.RowAnimation.fade)
                arrIndexSection1.remove(at: 0)
                let indexPath1 = IndexPath(row: 0, section: 1)
                securityTable.deleteRows(at:[indexPath1] , with: UITableView.RowAnimation.fade)
            }else {
                arrIndexSection1.remove(at: 0)
                let indexPath = IndexPath(row: 0, section: 1)
                securityTable.deleteRows(at:[indexPath] , with: UITableView.RowAnimation.fade)
                
            }
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK", style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
}

func allowCharecter(txt : String ,regix : String) -> Bool {
    let cs = NSCharacterSet(charactersIn: regix).inverted
    let filtered = txt.components(separatedBy: cs).joined(separator: "")
    if !(txt == filtered) {
        return false
    }else{
        return true
    }
}


class PasswordheaderCell : UITableViewCell {
    
    var delegate :passwordDelegates?
    @IBOutlet weak var btnPattern: UIButton!
    @IBOutlet weak var btnPassword: UIButton!
    @IBOutlet weak var imgPassword: UIImageView!
    @IBOutlet weak var imgPattern: UIImageView!
    
    
    override func awakeFromNib() {
    }
    @IBAction func btnResetAction(_ sender: Any) {
        if let del = self.delegate {
            del.resetPassword()
            imgPattern.image = #imageLiteral(resourceName: "r_radio_btn")
            imgPassword.image = #imageLiteral(resourceName: "r_radio_btn")
        }
    }
    
    @IBAction func btnPatternAction(_ sender: Any) {
        if  !(imgPattern.image == #imageLiteral(resourceName: "select_radio")){
            imgPattern.image = #imageLiteral(resourceName: "select_radio")
            if  imgPassword.image == #imageLiteral(resourceName: "select_radio") {
                if let del = self.delegate {
                    imgPassword.image = #imageLiteral(resourceName: "r_radio_btn")
                    del.deleteRowInTable(mode: "password")                }
            }
            if let del = self.delegate{
                del.inserRowInTable(field: 0, section: 1)
            }
        }
    }
    
    @IBAction func btnPassword(_ sender: Any) {
        if !(imgPassword.image == #imageLiteral(resourceName: "select_radio")){
            imgPassword.image = #imageLiteral(resourceName: "select_radio")
            imgPattern.image = #imageLiteral(resourceName: "r_radio_btn")
            
            RegistrationModel.shared.PasswordType = "1"

            if let del = self.delegate {
                del.showAlert(alertTitle :"" , alertBody:"Password must be atleast 6 digits. It should not be same with your mobile number or name and also case-sensitive", alertImage: #imageLiteral(resourceName: "password") )
            }
            
            if let del = self.delegate{
                del.inserRowInTable(field: 1, section: 0)
            }
        }
    }
}
class SecurityQheaderCell : UITableViewCell {
    var delegate :passwordDelegates?
  
    @IBOutlet weak var labQuestion: MarqueeLabel!
    override func awakeFromNib() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self, action: #selector(didTapLabelDemo))
        labQuestion.addGestureRecognizer(tap)
    }
    
    @objc func didTapLabelDemo(sender: UITapGestureRecognizer) {
        if let del = self.delegate {
            del.inserRowInTable(field: 1, section: 1)
        }
    }
    
}
class PasswordCell : UITableViewCell,UITextFieldDelegate {
    var delegate : passwordDelegates?
    @IBOutlet weak var tfpassword: UITextField!
    @IBOutlet weak var imgLeft: UIImageView!
    @IBOutlet weak var imgEye: UIImageView!
    var createPassString = ""
    var isNextViewHidden = true
    override func awakeFromNib() {
        tfpassword.delegate = self
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self, action: #selector(didTapLabelEye))
        imgEye.addGestureRecognizer(tap)
    }
    
    func runGuard() {
        if let del = self.delegate {
            del.updateMainUIUI()
        }
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if range.location == 0 && string == " " {
            return false
        }
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if textField.tag == 1 {
            let str = RegistrationModel.shared.MobileNumber
            let startIndex = str.index(str.startIndex, offsetBy: 4)
            if text == String(str[startIndex...]) {
                if let del = self.delegate {
                del.showAlert(alertTitle :"" , alertBody:"Please enter valid password. Password like 111111, 112233 and your mobile number is not allowed for security reasons".localized, alertImage: #imageLiteral(resourceName: "password") )
                return false
                }
            }
            if text.count > 15 {
                return false
            }else if text.count > 5 {
                if isNextViewHidden {
                   // self.dHeight =
//viewlockTypeSelect.frame.height + viewConfirmPassword.frame.height + viewConfirmPassword.frame.origin.y
                }
            }else {
                if isNextViewHidden {
                   // self.dHeight =
    //self.viewlockTypeSelect.frame.height + self.viewCreatePassword.frame.height
                    self.runGuard()
                }
            }
            
            
            
            if text.count > 2 {
                if let del = self.delegate {
                    del.inserRowInTable(field: 2, section: 0)
                }
            }
            return allowCharecter(txt: text, regix: pass_rigix)
        }else if textField.tag == 2 {
            if text.count > 2 {
                if let del = self.delegate {
                    del.inserRowInTable(field: 0, section: 1)
                }
            }
            return allowCharecter(txt: text, regix: pass_rigix)
        }else if textField.tag == 3 {
            return allowCharecter(txt: text, regix: ans_rigix)
        }
        
        
        
        
        
        
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 2 {
           createPassString = ""
           createPassString = tfpassword.text!
            println_debug("createPass : \(createPassString)")
        }else if textField.tag == 3 {
            if !(tfpassword.text! == createPassString) {
                println_debug("Password does not matched")
            }
        }
        return true
    }
    
    

    @objc func didTapLabelEye(sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            tfpassword.isSecureTextEntry = true
        }
        else if sender.state == .began {
            tfpassword.isSecureTextEntry = false
        }
    }
}
