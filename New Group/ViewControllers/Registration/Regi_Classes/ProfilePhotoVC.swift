//
//  ProfilePhotoVC.swift
//  OK
//
//  Created by Subhash Arya on 01/02/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import AVFoundation
import Rabbit_Swift

protocol ProfilePhotoVCDelegate {
    func profilePhotoConfirm()
    func resetAllVC(handler: @escaping (_ isSuccess: Bool) -> Void)
    func updateMainUIUI()
    func showAccountTypeVC()
    func showDemo(strURL : String, imgDemo : UIImage, demoTitile:String)
    func scrollTotop()
    func showProfilePhotoVC()
}

class ProfilePhotoVC: OKBaseController,CameraVCUPDelegate {
    
    let appDel = UIApplication.shared.delegate as! AppDelegate
    var session: AVCaptureSession?
    var stillImageOutput: AVCaptureStillImageOutput?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    
    @IBOutlet weak var lblStaus: UILabel!
    var delegate : ProfilePhotoVCDelegate?
    @IBOutlet weak var imgCamera: UIImageView!
   
    var seconds = 5
    var timer = Timer()
    
    @IBOutlet var previewView: UIView!
    @IBOutlet var capturedImage: UIImageView!
    
    @IBOutlet var imgCountryFlag: UIImageView!
    @IBOutlet var lblCountryCode: UILabel!
    @IBOutlet var lblMobilNo: UILabel!
    @IBOutlet var lblOperatorName: UILabel!
    @IBOutlet var lbAccountMobileNo: UILabel!
    @IBOutlet var lbSelectAccountType: UILabel!
    @IBOutlet var lblMerchant: UILabel!
    @IBOutlet var lblPersonal: UILabel!
    @IBOutlet weak var lblAgent: UILabel!
    
    @IBOutlet weak var imgAgent: UIImageView!
    @IBOutlet weak var imgRadioBtnMerchant: UIImageView!
    @IBOutlet weak var imgRadioBtnPersonal: UIImageView!
    @IBOutlet weak var btnMerchant: UIButton!
    @IBOutlet weak var btnPersonal: UIButton!
    @IBOutlet weak var topContraints: NSLayoutConstraint!
    @IBOutlet weak var lblRequired: UILabel! {
        didSet {
        lblRequired.text = "Required Fields".localized + " *"
        }
    }
    @IBOutlet weak var btnTakeImage: UIButton!
    var isAccountTypeSelected = false
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        lblStaus.text = ""
        //lbAccountMobileNo.font = UIFont(name: "Zawgyi-One", size: 17)
        lbAccountMobileNo.text = "OK$ Account Open Number".localized
        lbSelectAccountType.text = "Select Account Type".localized
        lblMerchant.text = "Merchant".localized
        lblPersonal.text = "Personal".localized
        lblAgent.text = "Agent".localized
        previewView.isHidden = true
        previewView.layer.borderWidth = 1.5
        previewView.layer.borderColor = UIColor.white.cgColor
        capturedImage.isHidden = false
        btnTakeImage.isUserInteractionEnabled = true
        
        self.capturedImage.layer.cornerRadius = 74.0
        self.capturedImage.clipsToBounds = true
        self.capturedImage.layer.borderColor = UIColor.init(hex: "#F2CF46").cgColor
        self.capturedImage.layer.borderWidth = 1.0
        previewView.layer.cornerRadius = 75
        previewView.layer.masksToBounds = true
        
        self.btnTakeImage.layer.cornerRadius = 74.0
        self.btnTakeImage.clipsToBounds = true
        self.imgCamera.isHidden = false
        let (mobileNumber,CountryCode) = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: RegistrationModel.shared.MobileNumber)
        lblMobilNo.text = mobileNumber
         let countryData = identifyCountry(withPhoneNumber: CountryCode)
        lblCountryCode.text = "(" + countryData.countryCode + ")"
        RegistrationModel.shared.CountryCode = countryData.countryCode
        if let safeImage = UIImage(named: countryData.countryFlag) {
            DispatchQueue.main.async {
                self.imgCountryFlag.image = safeImage
            }
        }
        
        if let country = preCodeCountry?.country {
            RegistrationModel.shared.countryISO = country
        }else {
            RegistrationModel.shared.countryISO = "Myanmar"
        }
        
        
        if let operatorName = preNetInfo?.operatorName {
            lblOperatorName.text = operatorName
            RegistrationModel.shared.operatorName = operatorName
        }
        topContraints.constant = -200
        self.dHeight = 240.00
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    func setImage(userImgae: UIImage) {
        capturedImage.image = userImgae
        self.uploadImageToServer(u_image: userImgae)
        //RegistrationModel.shared.ProfilePicAwsUrl = OKBaseController.imageTobase64(image: userImgae)
        topContraints.constant = 0
        self.dHeight = 440
        runGuard()
    }
    
    func UpdateUICellHeight () {
        
        topContraints.constant = 0
        self.dHeight = 440
        runGuard()
      //  if device.type == .iPhone5 || device.type == .iPhone5S || device.type == .iPhoneSE || device.type == .iPhone5C{ }
        if let del = self.delegate {
            del.scrollTotop()
        }
        //setupCamera()
//        let btn = UIButton()
//       self.btnTakePhoto(btn)
    }
   
    func hideProfilePicOnResetPersonalInfo() {
        topContraints.constant = -200
        self.dHeight = 240
        runGuard()
    }
    
    
    @IBAction func btnMerchantAction(_ sender: UIButton) {
        if !(imgRadioBtnMerchant.image == UIImage(named: "select_radio")) {
            if !isAccountTypeSelected{
                setupUIMerchant()
                guard(self.delegate?.showAccountTypeVC() != nil)else {
                    return
                }
            }else{
                UserDefaults.standard.setValue(true, forKey: "reset_to_merchant")
                self.resetMarchant()
            }
        }
    }
    
    func setupUIMerchant(){
        isAccountTypeSelected = true
        RegistrationModel.shared.AccountType = 0
        RegistrationModel.shared.lType = "MER"
        self.imgRadioBtnMerchant.image = UIImage(named: "select_radio")
        self.imgRadioBtnPersonal.image = UIImage(named: "r_Unradio")
        self.imgAgent.image = UIImage(named: "r_Unradio")
    }
    
    @IBAction func btnPersonalAction(_ sender: UIButton) {
        
        if !(imgRadioBtnPersonal.image == UIImage(named: "select_radio")) {
            
            if !isAccountTypeSelected{
                showAlert(alertTitle: "", alertBody:"If you open Personal account there is a daily transfer limit  500,000 MMK.".localized, alertImage: #imageLiteral(resourceName: "personal"))
                setupUIPersonal()
                guard(self.delegate?.showAccountTypeVC() != nil)else {
                    return
                }
            }else{
                    alertViewObj.wrapAlert(title:"", body:"If you open Personal account there is a daily transfer limit  500,000 MMK.".localized, img:#imageLiteral(resourceName: "r_personal"))
                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                        self.resetPersonal()
                    })
                    DispatchQueue.main.async {
                        alertViewObj.showAlert(controller: self)
                    }
            }
        }
    }
    
    @IBAction func btnAgentAction(_ sender: UIButton) {
        if !(imgAgent.image == UIImage(named: "select_radio")) {
            if !isAccountTypeSelected{
                setupUIAgent()
                guard(self.delegate?.showAccountTypeVC() != nil)else {
                    return
                }
            }else{
                UserDefaults.standard.setValue(true, forKey: "reset_to_merchant")
                self.resetAgent()
            }
        }
    }
    
    func setupUIAgent() {
        isAccountTypeSelected = true
        RegistrationModel.shared.AccountType = 0
        RegistrationModel.shared.lType = "AGENT"
        self.imgAgent.image = UIImage(named: "select_radio")
        self.imgRadioBtnMerchant.image = UIImage(named: "r_Unradio")
        self.imgRadioBtnPersonal.image = UIImage(named: "r_Unradio")
    }
    
    
    private func resetMarchant() {
        
        self.resetAll(handler: { (success) in
            if success {
                UserDefaults.standard.set("NO", forKey: "ALL_FIELDS_FILLED")
                UserDefaults.standard.set(false, forKey: "PasswordScroll")
                self.setupUIMerchant()
                RegistrationModel.shared.ProfilePicAwsUrl = ""
               self.capturedImage.image = nil
                DispatchQueue.main.async {
                    self.topContraints.constant = -200
                    self.dHeight = 240.00
                }
                UserDefaults.standard.setValue(false, forKey: "reset_to_merchant")
                guard(self.delegate?.showAccountTypeVC() != nil)else {
                    return
                }
            }
        })
        
    }
    private func resetPersonal() {
        self.resetAll(handler: { (success) in
            if success {
                 UserDefaults.standard.set("NO", forKey: "ALL_FIELDS_FILLED")
                UserDefaults.standard.set(false, forKey: "PasswordScroll")
                RegistrationModel.shared.ProfilePicAwsUrl = ""
                 self.capturedImage.image = nil
                self.setupUIPersonal()
                DispatchQueue.main.async {
                    self.topContraints.constant = -200
                    self.dHeight = 240.00
                }
                guard(self.delegate?.showAccountTypeVC() != nil)else {
                    return
                }
            }
        })
    }
    private func resetAgent() {
        self.resetAll(handler: { (success) in
            if success {
                UserDefaults.standard.set("NO", forKey: "ALL_FIELDS_FILLED")
                UserDefaults.standard.set(false, forKey: "PasswordScroll")
                self.setupUIAgent()
                RegistrationModel.shared.ProfilePicAwsUrl = ""
                self.capturedImage.image = nil
                DispatchQueue.main.async {
                    self.topContraints.constant = -200
                    self.dHeight = 240.00
                }
                UserDefaults.standard.setValue(false, forKey: "reset_to_merchant")
                guard(self.delegate?.showAccountTypeVC() != nil)else {
                    return
                }
            }
        })
        
    }
    
    func setupUIPersonal() {
        isAccountTypeSelected = true
        RegistrationModel.shared.AccountType = 1
        RegistrationModel.shared.lType = "SUBSBR"
        self.imgRadioBtnPersonal.image = UIImage(named: "select_radio")
        self.imgRadioBtnMerchant.image = UIImage(named: "r_Unradio")
        self.imgAgent.image = UIImage(named: "r_Unradio")
    }
    
    
    func resetAll(handler: @escaping (_ success: Bool) -> Void) {
        if let delegate = self.delegate {
            delegate.resetAllVC(handler: { (success) in
                handler(success)
            })
        }
    }
    func updateProflePic(imageUrl: String, realImage: UIImage) {
        RegistrationModel.shared.ProfilePicAwsUrl = imageUrl
        capturedImage.image = realImage
        UserDefaults.standard.set("NO", forKey: "ALL_FIELDS_FILLED")
        self.imgCamera.isHidden = false
        guard(self.delegate?.profilePhotoConfirm() != nil)else{
        return
        }
        self.UpdateUICellHeight()
    }
    
    @IBAction func btnTakePhoto(_ sender: UIButton) {
        if let del = delegate {
            del.showProfilePhotoVC()
        }
        
        
//        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
//          //  self.openCamera()
//        } else {
//            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
//                if granted {
//                    println_debug("access allowed")
//                    //self.openCamera()
//                } else {
//                    DispatchQueue.main.async {
//                       // self.lblStaus.font = UIFont(name: "Zawgyi-One", size: 14)
//                       // self.lblStaus.text = "Click here for Image"
//                    }
//                    println_debug("access denied")
//                    alertViewObj.wrapAlert(title:"", body:"Please Allow Camera Access".localized, img:#imageLiteral(resourceName: "alert-icon"))
//                    alertViewObj.addAction(title: "Setting".localized, style: .target , action: {
//
//                        if #available(iOS 10.0, *) {
//                            UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
//                        } else {
//                            UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
//                        }
//
//                    })
//                    DispatchQueue.main.async {
//                        alertViewObj.showAlert(controller: self)
//                    }
//
//                }
//            })
//        }
    }
    
    
    func openCamera() {
        DispatchQueue.main.async {
           // self.btnTakeImage.isUserInteractionEnabled = false
            RegistrationModel.shared.ProfilePicAwsUrl = ""
           //self.seconds = 5
//            if !self.capturedImage.isHidden {
//                self.capturedImage.isHidden = true
//            }
            //self.session?.startRunning()
           // self.runTimer()
        }
    }
 
    
    func runTimer() {
        DispatchQueue.main.async {
       // self.lblStaus.font = UIFont(name: "Zawgyi-One", size: 15)
       // self.lblStaus.text = "Smile"
       // self.timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(ProfilePhotoVC.updateTimer)), userInfo: nil, repeats: true)
        }
    }
    
    @objc func updateTimer() {
        //seconds -= 1
        //lblStaus.text = String(seconds)
        //lblStaus.font = UIFont(name: "Zawgyi-One", size: 40)
       // if seconds == 0{
           // timer.invalidate()
           // lblStaus.text = ""
            takePhoto()
       // }
    }
    
    func closeCamera() {
        //timer.invalidate()
        session?.stopRunning()
    }
    
    func setupCamera() {
        session = AVCaptureSession()
        session!.sessionPreset = AVCaptureSession.Preset.photo
        let backCamera = AVCaptureDevice.default(.builtInWideAngleCamera,
                                                 for: AVMediaType.video,
                                                 position: .front)
        var error: NSError?
        var input: AVCaptureDeviceInput!
        do {
            input = try AVCaptureDeviceInput(device: backCamera!)
        } catch let error1 as NSError {
            error = error1
            input = nil
            println_debug(error!.localizedDescription)
        }
        if error == nil && session!.canAddInput(input) {
            session!.addInput(input)
            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput?.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
            if session!.canAddOutput(stillImageOutput!) {
                session!.addOutput(stillImageOutput!)
                videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session!)
                videoPreviewLayer!.frame = self.previewView.layer.bounds
                videoPreviewLayer!.videoGravity = AVLayerVideoGravity.resizeAspectFill
                videoPreviewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
                previewView.layer.addSublayer(videoPreviewLayer!)
                session!.startRunning()
            }
        }
    }
    
    func takePhoto() {
        let videoConnection = stillImageOutput!.connection(with: AVMediaType.video)
        if(videoConnection != nil) {
            stillImageOutput?.captureStillImageAsynchronously(from: videoConnection!, completionHandler: { (sampleBuffer, error) -> Void in
                if sampleBuffer != nil {
                    let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer!)
                    let dataProvider = CGDataProvider(data: imageData! as CFData)
                    let cgImageRef = CGImage.init(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: CGColorRenderingIntent.relativeColorimetric)
                    let image = UIImage(cgImage: cgImageRef!, scale: 1.0, orientation: UIImage.Orientation.right)
                    self.detect(userImage: image)
                }
            })
        }
    }
    
    func detect(userImage : UIImage) {
        let imageOptions =  NSDictionary(object: NSNumber(value: 5) as NSNumber, forKey: CIDetectorImageOrientation as NSString)
        let personciImage = CIImage.init(cgImage: userImage.cgImage!)
        let accuracy = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
        let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: accuracy)
        let faces = faceDetector?.features(in: personciImage, options: imageOptions as? [String : AnyObject])
        
        if let faces = faces {
            if faces.count > 1 {
                showAlert(alertTitle:"", alertBody: "More Than One face was detected. Please try again".localized, alertImage: #imageLiteral(resourceName: "user"))
                btnTakeImage.isUserInteractionEnabled = true
            } else {
                if let face = faces.first as? CIFaceFeature {
                    println_debug("found bounds are \(face.bounds)")
                    if face.hasMouthPosition && face.hasLeftEyePosition &&  face.hasRightEyePosition {
                        session?.stopRunning()
                        self.capturedImage.isHidden = false
                        self.capturedImage.image = userImage
                        //lblStaus.font = UIFont(name: "Zawgyi-One", size: 20)
                        //seconds = 2
                       // timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(ProfilePhotoVC.updateTimerForMesasge)), userInfo: nil, repeats: true)
                        self.uploadImageToServer(u_image: userImage)
                    }else {
                        btnTakeImage.isUserInteractionEnabled = true
                        self.imgCamera.isHidden = false
                        alertViewObj.wrapAlert(title:"No Face".localized, body:"No face was detected. Please try again".localized, img:#imageLiteral(resourceName: "alert-icon"))
                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                           // self.timer.invalidate()
                           // self.seconds = 0
                           // self.openCamera()
                        })
                        DispatchQueue.main.async {
                            alertViewObj.showAlert(controller: self)
                        }
                    }
                } else {
                    btnTakeImage.isUserInteractionEnabled = true
                    self.imgCamera.isHidden = false
                    alertViewObj.wrapAlert(title:"No Face".localized, body:"No face was detected. Please try again".localized, img:#imageLiteral(resourceName: "alert-icon"))
                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                        //self.timer.invalidate()
                       // self.seconds = 0
                       // self.openCamera()
                    })
                    DispatchQueue.main.async {
                        alertViewObj.showAlert(controller: self)
                    }
                    
                }
            }
        }
    }
    
    @objc func updateTimerForMesasge() {
       // seconds -= 1
       // lblStaus.font = UIFont(name: "Zawgyi-One", size: 15)
      //  lblStaus.text = "You look great".localized
        // Do not scroll up in iPhone5S,SE
        UserDefaults.standard.set("NO", forKey: "ALL_FIELDS_FILLED")
        self.imgCamera.isHidden = false
       // lblStaus.font = UIFont(name: "Zawgyi-One", size: 20)
//        if seconds == 0{
//            btnTakeImage.isUserInteractionEnabled = true
//            timer.invalidate()
//            lblStaus.text = ""
//            if delegate != nil{
//                guard(self.delegate?.profilePhotoConfirm() != nil)else{
//                    return
//                }
//            }
//        }
    }
    
    func uploadImageToServer(u_image: UIImage) {
        RegistrationModel.shared.ProfilePicAwsUrl = OKBaseController.imageTobase64(image: u_image)
    }
    
    
    
    @IBAction func btnShowDemoAction(_ sender: Any) {
        if let del = delegate {
            var strURL = ""
            if ok_default_language == "my"{
                strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/3f7b2c1e-a2e4-4563-8693-afb7f58c70daReqDocumentsJuly_02_2018%2001_38_59%20PM.jpg";
            }else {
                strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/32e4f212-896a-4517-831b-88f96be0c855ReqDocumentsJuly_02_2018%2001_43_13%20PM.jpg";
            }
            del.showDemo(strURL: strURL, imgDemo: #imageLiteral(resourceName: "user") , demoTitile: "Registration".localized)
        }
    }
    
    
    
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func runGuard() {
        guard (self.delegate?.updateMainUIUI() != nil) else {
            return
        }
    }


}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
