//
//  IDProofVC.swift
//  DemoUpdateProfile
//
//  Created by Subhash Arya on 02/11/17.
//  Copyright © 2017 SHUBH. All rights reserved.
//

import UIKit


protocol IDProofDelegate {
    func updateMainUIUI()
    func showBusinessOrAddress()
    func showNRCRequiredImagesVC()
    func showTownShip()
    func resetIDProofDetails()
    func showCountry()
    func takeImageFromCameraAndGallary(tagNo : Int)
    func dismissPicker()
    func showDemo(strURL : String, imgDemo : UIImage, demoTitile:String)
    func viewEndEditing()
    func changeLanguageToEnglish()
    func updateForTypeID()
    func removeNRCRequriedOnClickReset()
}

class IDProofVC: OKBaseController,TownShipVcDeletage,NRCTypesVCDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CountryViewControllerDelegate,TownshipVC2Delegate,MyTextFieldDelegate {
    func textFieldDidDelete() {
        if tfNRC.text?.count == 0 {
            self.showNRCSuggestion()
        }
    }
    
    @IBOutlet weak var lblAttach: MarqueeLabel!
    @IBOutlet var viewNrcHeightConstraints: NSLayoutConstraint!
    @IBOutlet var viewNRCTypeHeightConstant: NSLayoutConstraint!
    
    @IBOutlet var viewNRCtypeSelect: UIView!
    @IBOutlet var imgFront: UIImageView!
    @IBOutlet var imgIDrear: UIImageView!
    
    @IBOutlet var btnAddNRCID: UIButton!
    @IBOutlet var tfNRC: MyTextField!
    @IBOutlet var btnNRC: UIButton!
    @IBOutlet var viewNRCTF: UIView!
    @IBOutlet var viewNRCButton: UIView!
    @IBOutlet var viewNRC: UIView!
    @IBOutlet weak var viewNRCID: UIView!
    
    @IBOutlet weak var viewPassport: UIView!
    
    @IBOutlet weak var viewCountry: UIView!
    @IBOutlet weak var viewPassportNo: UIView!
    @IBOutlet weak var viewDate_of_Expiry: UIView!
    @IBOutlet weak var viewPassport_ID_Photo: UIView!
    @IBOutlet weak var btnSelectCountry: UIButton!
    
    @IBOutlet weak var btnPassportId: UIButton!
    @IBOutlet weak var tfPassportNo: UITextField!
    @IBOutlet weak var imgCountry: UIImageView!
    @IBOutlet weak var tfDate_Of_Expiry: UITextField!

    @IBOutlet weak var lblNRC: MarqueeLabel!
    @IBOutlet weak var lblPassport: MarqueeLabel!
    
    @IBOutlet weak var lblHeader: MarqueeLabel!
    @IBOutlet weak var btnEnterNRC: UIButton!
    @IBOutlet weak var btnOR: UIButton!
    @IBOutlet weak var btnEnterPassport: UIButton!
    @IBOutlet weak var lblAttachPassport: MarqueeLabel!
    @IBOutlet weak var imgPassport: UIImageView!
    
     @IBOutlet weak var btnReset: UIButton!
    var imgProfle = UIImage()
    var strDOB = ""
    var delegate : IDProofDelegate?
    let btnnrc = UIButton()
    var isNextViewShown = false
    var locationDetails: LocationDetail?
    let dateFormatter = DateFormatter()
    var NRCCHARSET = ""
    var PASSPORTCHARSET = "(),_-1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
   // var accountVC = AccountTypeView()
    var isNRCImageIDFieldHidden = true
    override func viewDidLoad() {
        super.viewDidLoad()
    self.lblHeader.attributedText = lblHeader.attributedStringFinal(str1: "Select ID Type".localized)
    lblNRC.textColor = UIColor.init(red: 22/255.0, green: 45/255.0, blue: 159/255.0, alpha: 1)
     lblNRC.text = "Enter NRC Details".localized //lblNRC.scrollMarqueLabel(title: "Enter NRC Details".localized)
        
      //UIColor.init(red: 22/255.0, green: 45/255.0, blue: 159/255.0, alpha: 1)
    lblPassport.attributedText = lblPassport.scrollMarqueLabel(title: "Enter Passport Details".localized)
    self.btnOR.setTitle("(OR)".localized, for: .normal)
    btnNRC.setTitle("Enter NRC Number".localized, for: .normal)
    lblAttach.text = "Attach NRC ID Photo".localized
    btnSelectCountry.setTitle("Select Country of Citizenship".localized, for: .normal)
    tfPassportNo.placeholder = "Enter Passport Number".localized
    tfDate_Of_Expiry.placeholder = "Enter Passport Expiry Date".localized
    lblAttachPassport.text = "Attach Passport".localized
        
        self.tfNRC.delegate = self
        self.tfPassportNo.delegate = self
        self.tfDate_Of_Expiry.delegate = self
        let screenSize: CGRect = UIScreen.main.bounds
        let viewDOBKeyboard = UIView(frame: CGRect(x: 0, y: 0, width:screenSize.width , height: 40))
        viewDOBKeyboard.backgroundColor = UIColor(red: 244/255, green: 199/255, blue: 22/255, alpha: 1)
        
        let btnCancel  = UIButton(frame: CGRect(x: 10, y: 0, width:100, height: 40))
          btnCancel.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
        btnCancel.setTitle("Cancel".localized, for: UIControl.State.normal)
        btnCancel.addTarget(self, action:#selector(btnCancelDOBAction), for: UIControl.Event.touchUpInside)
        viewDOBKeyboard.addSubview(btnCancel)
        
        let btnDone  = UIButton(frame: CGRect(x: screenSize.width - 110, y: 0, width:100, height: 40))
        btnDone.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
        btnDone.setTitle("Done".localized, for: UIControl.State.normal)
        btnDone.addTarget(self, action:#selector(btnDoneDOBAction), for: UIControl.Event.touchUpInside)
        viewDOBKeyboard.addSubview(btnDone)
        tfDate_Of_Expiry.inputAccessoryView = viewDOBKeyboard
        self.dHeight = 175.00
        self.tfNRC.addTarget(self, action: #selector(IDProofVC.textFieldDidChange(_:)),
                               for: UIControl.Event.editingChanged)
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        self.tfNRC.myDelegate = self
        
        if ok_default_language == "en" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue"), for: .normal)
            NRCCHARSET = "1234567890"
        }else if ok_default_language == "my" {
            btnReset.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
            NRCCHARSET = "1234567890"
        }
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(IDProofVC.imageTapped))
        imgFront.isUserInteractionEnabled = true
        imgFront.addGestureRecognizer(tapGestureRecognizer)
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(IDProofVC.imageTapped2))
        imgIDrear.isUserInteractionEnabled = true
        imgIDrear.addGestureRecognizer(tapGestureRecognizer2)
        
        let tapGestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(IDProofVC.imageTapped3))
        imgPassport.isUserInteractionEnabled = true
        imgPassport.addGestureRecognizer(tapGestureRecognizer3)
        NotificationCenter.default.addObserver(self, selector: #selector(IDProofVC.setNRCNumber), name: NSNotification.Name(rawValue: "NRC_NUMBER_CARD"), object: nil)
    }
    
    @objc func setNRCNumber() {
        
        if RegistrationModel.shared.ocr_F_nrcid.contains(find: ")") {
            let nrcNum = RegistrationModel.shared.ocr_F_nrcid.components(separatedBy: ")")
            let myString = nrcNum[0] + ")"
            let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: "Zawgyi-One", size: 18) ?? UIFont.systemFont(ofSize: 18.0) ])
            btnnrc.frame = CGRect(x: 0, y: -2, width:stringSize.width, height: 52)
            btnnrc.setTitleColor(.black, for: .normal)
            btnnrc.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
            btnnrc.addTarget(self, action: #selector(BtnNrcTypeAction(button:)), for: .touchUpInside)
            btnnrc.setTitle(myString, for: UIControl.State.normal)
            let btnView = UIView()
            btnView.frame = CGRect(x: 0, y: 0, width: btnnrc.frame.width , height: 52)
            btnView.addSubview(btnnrc)
            tfNRC.leftView = nil
            tfNRC.leftView = btnView
            tfNRC.leftViewMode = UITextField.ViewMode.always
            
            //Mapping burmese Number: ၁၂၃၄၅၆၇၈၉၀
            var finalEnglishNRC = ""
            for char in nrcNum[1].map(String.init) {
                if char == "၁" {
                finalEnglishNRC = finalEnglishNRC + "1"
                }else if char == "၂" {
                 finalEnglishNRC = finalEnglishNRC + "2"
                }else if char == "၃" {
                  finalEnglishNRC = finalEnglishNRC + "3"
                }else if char == "၄" {
                  finalEnglishNRC = finalEnglishNRC + "4"
                }else if char == "၅" {
                  finalEnglishNRC = finalEnglishNRC + "5"
                }else if char == "၆" {
                  finalEnglishNRC = finalEnglishNRC + "6"
                }else if char == "၇" {
                  finalEnglishNRC = finalEnglishNRC + "7"
                }else if char == "၈" {
                  finalEnglishNRC = finalEnglishNRC + "8"
                }else if char == "၉" {
                  finalEnglishNRC = finalEnglishNRC + "9"
                }else if char == "၀" {
                  finalEnglishNRC = finalEnglishNRC + "0"
                }
            }
            tfNRC.text = finalEnglishNRC
            let helpingFuntion = HelpingFunctionR()
            RegistrationModel.shared.NRC = helpingFuntion.charecterReplace(charecter: "/", replacingWith: "@", mainString: btnnrc.title(for: .normal)! + tfNRC.text!)
            RegistrationModel.shared.ocr_F_nrcid = RegistrationModel.shared.NRC
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "NRC_NUMBER_CARD"), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    
    @objc func imageTapped() {
        if imgFront.image != nil {
            self.showImageZoomVC(idImage: imgFront.image!)
        }
    }
   @objc  func imageTapped2() {
    if imgIDrear.image != nil {
        self.showImageZoomVC(idImage: imgIDrear.image!)
    }
    }
  @objc  func imageTapped3() {
    if imgPassport.image != nil {
        self.showImageZoomVC(idImage: imgPassport.image!)
    }
    }
    
    private func showImageZoomVC (idImage : UIImage) {
        if let del = self.delegate {
            del.viewEndEditing()
        }
        let sb = UIStoryboard(name: "Registration" , bundle: nil)
        let zoomImageVC  = sb.instantiateViewController(withIdentifier: "ZoomImageVC") as! ZoomImageVC
        if let window = UIApplication.shared.keyWindow {
            window.rootViewController?.addChild(zoomImageVC)
            if device.type == .iPhoneXS || device.type == .iPhoneXSMax || device.type == .iPhoneXR || device.type == .iPhoneX {
             zoomImageVC.view.frame = .init(x: 0, y: 24, width: window.frame.width , height: window.frame.height - 24)
            }else{
            zoomImageVC.view.frame = .init(x: 0, y: 0, width: window.frame.width , height: window.frame.height)
            }

            
            zoomImageVC.setImageToZoom(myImage: idImage)
            window.addSubview(zoomImageVC.view)
            window.makeKeyAndVisible()
        }
    }

    
    @objc func btnCancelDOBAction () {
        tfDate_Of_Expiry.resignFirstResponder()
    }
    
    @objc func btnDoneDOBAction () {
        tfDate_Of_Expiry.resignFirstResponder()
       
        if (strDOB.count) > 0 {
             tfDate_Of_Expiry.text = strDOB
             RegistrationModel.shared.IdExpDate = tfDate_Of_Expiry.text!
            self.viewPassport_ID_Photo.isHidden = false
            self.dHeight = viewPassport_ID_Photo.frame.origin.y + viewPassport_ID_Photo.frame.height + viewPassport.frame.origin.y
            if isNextViewShown {
                self.SelfRun()
            }else {
                runGuard()
            }
            if !isNextViewShown{
                if let del = delegate {
                    del.showNRCRequiredImagesVC()
                }
//                guard(self.delegate?.showBusinessOrAddress() != nil)else {return}
                isNextViewShown = true
            }else{
                let value = UserDefaults.standard.bool(forKey: "NRC_HIEGHT")
                if !value {
                    if let del = delegate {
                        del.showNRCRequiredImagesVC()
                    }
                    isNextViewShown = true
                }
            }
        }else {
            self.viewPassport_ID_Photo.isHidden = true
        }
    }
    
    @objc func datePickerChanged(sender: UIDatePicker) {
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        strDOB = dateFormatter.string(from: sender.date)
    }
    
    @IBAction func btnPassportShowAction(_ sender: Any) {
        if ok_default_language  == "my" {
            alertViewObj.wrapAlert(title: "", body: "If you want to Register with passport details than you need to change language".localized, img:#imageLiteral(resourceName: "r_user"))
            alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
            })
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                if let del = self.delegate {
                    del.changeLanguageToEnglish()
                }
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }else {
            self.dHeight = viewCountry.frame.height + viewCountry.frame.origin.y + viewPassport.frame.origin.y
            if isNextViewShown {
                self.SelfRun()
            }else {
                runGuard()
            }
            self.viewNRCtypeSelect.isHidden = true
            self.viewNRC.isHidden = true
            self.viewPassport.isHidden = false
            RegistrationModel.shared.IDType = "04"
        }
    }
    @IBAction func btnNRCAction(_ sender: Any) {
        let sb = UIStoryboard(name: "Registration", bundle: nil)
        let townshipVC2 = sb.instantiateViewController(withIdentifier: "TownshipVC2") as! TownshipVC2
        townshipVC2.selectedDivState = locationDetails
        townshipVC2.townshipVC2Delegate = self
        self.navigationController?.pushViewController(townshipVC2, animated: true)
    }
    
    @IBAction func btnNRCShowAction(_ sender: Any) {
        // Show TownshipVC1
        guard (self.delegate?.showTownShip() != nil) else {
            return
        }
    }
    
    @IBAction func btnResetIDProofAction(_ sender: Any) {
        if viewNRCtypeSelect.isHidden {
            RegistrationModel.shared.IDType = ""
            RegistrationModel.shared.NRC = ""
            RegistrationModel.shared.CountryOfCitizen = ""
            RegistrationModel.shared.IdExpDate = ""
            RegistrationModel.shared.IdPhoto = ""
            RegistrationModel.shared.IdPhotoAwsUrl = ""
            RegistrationModel.shared.IdPhoto1 = ""
            RegistrationModel.shared.IdPhoto1AwsUrl = ""
            //NRC
            imgIDrear.image = nil
            imgFront.image = nil
            tfNRC.text = ""
            tfNRC.leftView = nil
            isNRCImageIDFieldHidden = true
            self.viewNRCButton.isHidden = false
            self.viewNRCTF.isHidden = true
            self.viewNRCtypeSelect.isHidden = false

            //Passport
            imgCountry.image = UIImage(named: "r_country")
            btnSelectCountry.setTitle("Select Country of Citizenship".localized, for: .normal)
            tfPassportNo.text = ""
            tfPassportNo.placeholder = "Enter Passport Number".localized
            tfDate_Of_Expiry.text = ""
            viewDate_of_Expiry.isHidden = true
            strDOB = ""
            tfDate_Of_Expiry.placeholder = "Enter Passport Expiry Date".localized
            viewPassport_ID_Photo.isHidden = true
            lblAttachPassport.text = "Attach Passport".localized
            imgPassport.image = nil

            guard (self.delegate?.removeNRCRequriedOnClickReset() != nil) else {
                return
            }
            
            DispatchQueue.main.async {
                self.dHeight = 175.00
                if self.isNextViewShown {
                    self.SelfRun()
                }else {
                   self.runGuard()
                }
            }
 
        }
    }
    
    @IBAction func btnAddNRCIDAction(_ sender: Any) {
        if RegistrationModel.shared.IdPhoto == "" {
            guard (self.delegate?.takeImageFromCameraAndGallary(tagNo: 1) != nil) else {
                return
            }
        }else if RegistrationModel.shared.IdPhoto1 == ""{
            guard (self.delegate?.takeImageFromCameraAndGallary(tagNo: 2) != nil) else {
                return
            }
        }else {
            guard (self.delegate?.takeImageFromCameraAndGallary(tagNo: 1) != nil) else {
                return
            }
        }
    }
    
    func setDivison(division: LocationDetail) {
        // After Selecting NRC Divison
        locationDetails = division
        var divStr = ""
        if ok_default_language == "my"{
            divStr = division.nrcCodeNumberMy + "/"
        }else {
            divStr = division.nrcCodeNumber + "/"
        }
     
        self.viewNRCtypeSelect.isHidden = true
        self.viewPassport.isHidden = true
        self.viewNRC.isHidden = false
        self.dHeight = viewNRCTF.frame.height + viewNRCTF.frame.origin.y + 40
        if isNextViewShown {
        self.SelfRun()
        }else {
         runGuard()
        }
        
        btnNRC.setTitle(divStr, for: .normal)
        RegistrationModel.shared.IDType = "01"
    }
    
    func setDivisionAndTownship(strDivison_township: String) {
        self.setNRCDetails(nrcDetails: strDivison_township)
    }
    
    func NRCValueString(nrcSting : String) {
        self.setNRCDetails(nrcDetails: nrcSting)
    }
    
    private func setNRCDetails(nrcDetails : String) {
        self.viewNRCButton.isHidden = true
        self.viewNRCTF.isHidden = false
        let myString = nrcDetails
        let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: "Zawgyi-One", size: 18) ?? UIFont.systemFont(ofSize: 18.0) ])
        btnnrc.frame = CGRect(x: 0, y: -2, width:stringSize.width, height: 52)
        btnnrc.setTitleColor(.black, for: .normal)
        btnnrc.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
        btnnrc.addTarget(self, action: #selector(BtnNrcTypeAction(button:)), for: .touchUpInside)
        btnnrc.setTitle(nrcDetails, for: UIControl.State.normal)
        let btnView = UIView()
        btnView.frame = CGRect(x: 0, y: 0, width: btnnrc.frame.width , height: 52)
        btnView.addSubview(btnnrc)
        tfNRC.leftView = btnView
        tfNRC.leftViewMode = UITextField.ViewMode.always
        self.showNRCSuggestion()
    }
  
    @objc func BtnNrcTypeAction(button : UIButton) {
    self.showNRCSuggestion()
    }
    
    func setNrcTypeName(nrcType : String) {
        //Delegate Method after select popup value
        btnnrc.setTitle(nrcType, for: .normal)
        if (tfNRC.text?.length)! < 6 {
           tfNRC.becomeFirstResponder()
        }
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == tfDate_Of_Expiry {
            let date = Date()
            let datePicker = UIDatePicker()
            datePicker.datePickerMode = .date
            datePicker.backgroundColor = UIColor.white
            datePicker.calendar = Calendar(identifier: .gregorian)
            if #available(iOS 13.4, *) {
                           datePicker.preferredDatePickerStyle = .wheels
                       } else {
                           // Fallback on earlier versions
                       }
            if strDOB.count > 0 {
                datePicker.date = dateFormatter.date(from: strDOB)!
            }else {
                datePicker.date = date
            }
            datePicker.minimumDate = Date(timeInterval: (24 * 60 * 60), since: date)
            let maxDate = Calendar.current.date(byAdding: .year, value: 100, to: Date())
            datePicker.maximumDate = maxDate
            strDOB = dateFormatter.string(from: datePicker.date)
            datePicker.addTarget(self, action: #selector(datePickerChanged(sender:)), for: .valueChanged)
            tfDate_Of_Expiry.inputView = datePicker
        }
        return true
    }
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == tfPassportNo {
            RegistrationModel.shared.NRC = self.tfPassportNo.text!
            removeSpaceAtLast(txtField:tfPassportNo)
        }else if textField == tfNRC {
            tfNRC.keyboardType = .numberPad
            tfNRC.becomeFirstResponder()
            if tfNRC.text == "00000" {
                tfNRC.text = ""
                RegistrationModel.shared.NRC = ""
                return false
            }
        }
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let length = textField.text!.count + string.count - range.length
        
        if range.location == 0 && string == " " {
            return false
        }
        if textField == self.tfNRC {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: NRCCHARSET).inverted).joined(separator: "")) { return false }
            if length > 6 {
                tfNRC.resignFirstResponder()
                return false
            }


        }else if textField == self.tfPassportNo{
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: PASSPORTCHARSET).inverted).joined(separator: "")) { return false }
            if text.count > 4{
                self.viewDate_of_Expiry.isHidden = false
                if self.dHeight! <= viewDate_of_Expiry.frame.origin.y + viewDate_of_Expiry.frame.height + viewPassport.frame.origin.y {
                    self.dHeight = viewDate_of_Expiry.frame.origin.y + viewDate_of_Expiry.frame.height + viewPassport.frame.origin.y
                    if isNextViewShown {
                        self.SelfRun()
                    }else {
                        runGuard()
                    }
                }
                return text.count <= 14
            }
        }else if textField == self.tfDate_Of_Expiry{
            return false
        }
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if tfNRC.text == "000000" {
            tfNRC.text?.removeLast()
        }else if tfNRC.text!.count == 0 {
            RegistrationModel.shared.NRC = ""
        }else if tfNRC.text!.count == 6 {
            tfNRC.resignFirstResponder()
            if isNRCImageIDFieldHidden {
                isNRCImageIDFieldHidden = false
                self.viewNRCID.isHidden = false
                self.view.endEditing(true)
                self.dHeight = viewNRC.frame.height + viewNRC.frame.origin.y
                if isNextViewShown {
                    self.SelfRun()
                }else {
                    runGuard()
                }
            }
            
//            if !isNextViewShown {
            //                guard (self.delegate?.takeImageFromCameraAndGallary(tagNo: 1) != nil) else {return}}
            if !isNextViewShown {
//                guard(self.delegate?.showBusinessOrAddress() != nil)else {return}
                
                if let del = delegate {
                    del.showNRCRequiredImagesVC()
                }
                isNextViewShown = true
            }else {
                let value = UserDefaults.standard.bool(forKey: "NRC_HIEGHT")
                if !value {
                    if let del = delegate {
                        del.showNRCRequiredImagesVC()
                    }
                    isNextViewShown = true
                }
            }
            let helpingFuntion = HelpingFunctionR()
            RegistrationModel.shared.NRC = helpingFuntion.charecterReplace(charecter: "/", replacingWith: "@", mainString: btnnrc.title(for: .normal)! + tfNRC.text!)
        }else {
            let helpingFuntion = HelpingFunctionR()
            RegistrationModel.shared.NRC = helpingFuntion.charecterReplace(charecter: "/", replacingWith: "@", mainString: btnnrc.title(for: .normal)! + tfNRC.text!)
        }
    }
    
    // MARK:
    // MARK:  UIIMAGE PICKER CONTROLEER DELEGATES
    // MARK:
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        guard (self.delegate?.dismissPicker() != nil) else {
            return
        }
        if picker.view.tag == 1{
            //Front
            self.imgFront.image = (info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage)!
            RegistrationModel.shared.IdPhoto = OKBaseController.imageTobase64(image: imgFront.image!)
            RegistrationModel.shared.IdPhotoAwsUrl = ""
            guard (self.delegate?.takeImageFromCameraAndGallary(tagNo: 2) != nil) else {
                return
            }
        
        }else if picker.view.tag == 2{
            //Rear
            self.imgIDrear.image = (info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage)!
            RegistrationModel.shared.IdPhoto1 = OKBaseController.imageTobase64(image: imgIDrear.image!)
            RegistrationModel.shared.IdPhoto1AwsUrl = ""
        }else if picker.view.tag == 3 {
            self.imgPassport.image = (info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage)!
                  RegistrationModel.shared.IdPhoto = OKBaseController.imageTobase64(image: imgPassport.image!)
            RegistrationModel.shared.IdPhotoAwsUrl = ""
        }
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
     
        guard (self.delegate?.dismissPicker() != nil) else {
            return
        }
    }
    
    @IBAction func SelectCountryAction(_ sender: Any) {
        //Click to navigate country VC
        guard (self.delegate?.showCountry() != nil) else {
            return
        }
        tfPassportNo.text = ""
    }
    
    
    func countryViewController(_ list: CountryViewController, country: Country) {

        RegistrationModel.shared.CountryOfCitizen = country.name
        btnSelectCountry.setTitle(country.name,for: .normal)
        btnSelectCountry.setTitleColor(UIColor.black, for: .normal)
        imgCountry.image = UIImage(named: country.code)
        self.viewPassportNo.isHidden = false
        
        if self.dHeight! <= viewPassportNo.frame.origin.y + viewPassportNo.frame.height + viewPassport.frame.origin.y{
           self.dHeight = viewPassportNo.frame.origin.y + viewPassportNo.frame.height + viewPassport.frame.origin.y
            if isNextViewShown {
                self.SelfRun()
            }else {
                runGuard()
            }
        }
   
        list.dismiss(animated: false, completion: nil)
        tfPassportNo.becomeFirstResponder()
    }
    
    func countryViewControllerCloseAction(_ list: CountryViewController) {
        list.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func addPassportIDphotoAction(_ sender: UIButton) {
        guard (self.delegate?.takeImageFromCameraAndGallary(tagNo: 3) != nil) else {
            return
        }
    }

    
    func runGuard() {
        guard (self.delegate?.updateMainUIUI() != nil) else {
            return
        }
    }
    func SelfRun() {
        if let del = self.delegate {
            del.updateForTypeID()
        }
    }
    
    private func showNRCSuggestion(){
        if let del = delegate {
            del.viewEndEditing()
        }
        let sb = UIStoryboard(name: "Registration" , bundle: nil)
        let nRCTypesVC  = sb.instantiateViewController(withIdentifier: "NRCTypesVC") as! NRCTypesVC
        nRCTypesVC.modalPresentationStyle = .overCurrentContext
        nRCTypesVC.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        nRCTypesVC.setName(nrcName: (btnnrc.titleLabel?.text)!)
        nRCTypesVC.nRCTypesVCDelegate = self
        self.present(nRCTypesVC, animated: false, completion: nil)
        tfNRC.resignFirstResponder()
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    @IBAction func btnShowDemoAction(_ sender: Any) {
        if let del = delegate {
            var strURL = ""
            if ok_default_language == "my"{
                    strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/1c40c13b-57c4-4c91-8f94-a79fe4badc8fReqDocumentsJuly_02_2018%2001_24_29%20PM.jpg";
            }else {
                    strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/813d381e-40da-4917-8539-7774225e6f82ReqDocumentsJuly_02_2018%2001_32_50%20PM.jpg";
            }
            del.showDemo(strURL:strURL , imgDemo:#imageLiteral(resourceName: "nrc") ,demoTitile: "Select ID Type".localized)
        }
    }

    private func removeSpaceAtLast(txtField : UITextField){
        if txtField.text?.last == " " {
            txtField.text?.removeLast()
        }
    }
    
    
}

extension UITextField {
    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
            return false
    }
    
    func canBecomeFirstResponder() -> Bool {
        return false
    }
    
    override open func addGestureRecognizer(_ gestureRecognizer: UIGestureRecognizer) {
        
        if gestureRecognizer.isKind(of: UILongPressGestureRecognizer.self) {
            gestureRecognizer.isEnabled = false
        }
        return super.addGestureRecognizer(gestureRecognizer)
    }
}



// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
