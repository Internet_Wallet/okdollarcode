//
//  SecurityPassQuestionVC.swift
//  DemoUpdateProfile
//
//  Created by SHUBH on 11/8/17.
//  Copyright © 2017 SHUBH. All rights reserved.
//

import UIKit

protocol SecurityVCDelegate{
    func updateMainUIUI()
    func showSignatureVC()
    func showPatternVC()
    func showDemo(strURL : String, imgDemo : UIImage, demoTitile:String)
    func viewEndEditing()
    func showAdditionalInformationVC()
    
}

class SecurityPassQuestionVC: OKBaseController,UITextFieldDelegate,SecurityQuestionRDelegate,PasswordPatternViewControllerDelegate{
   
     let validObj = PayToValidations()
    var delegate : SecurityVCDelegate?
    let appDel = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var viewPassword: UIView!

    @IBOutlet weak var tfPassword: UITextField!
    
    @IBOutlet weak var tfConfirmPassword: UITextField!
    @IBOutlet weak var tfAnswer: UITextField!
    @IBOutlet weak var imgPattern: UIImageView!
    @IBOutlet weak var imgPassword: UIImageView!
    @IBOutlet weak var btnSecuritQuestion: UIButton!
    @IBOutlet var viewlockTypeSelect: UIView!
    @IBOutlet weak var btnPattern: UIButton!
    @IBOutlet weak var btnPassword: UIButton!
    @IBOutlet weak var viewCreatePassword: UIView!
    @IBOutlet weak var viewConfirmPassword: UIView!
    @IBOutlet weak var veiwSecurityQuestion: UIView!
    @IBOutlet weak var veiwAnswer: UIView!
    @IBOutlet weak var viewSelectSecurityQuestion: UIView!
    @IBOutlet weak var lblQuestion: MarqueeLabel!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var topConstraintsSSQues: NSLayoutConstraint!
    
    @IBOutlet weak var lblHeader: MarqueeLabel!
    @IBOutlet weak var lblPattern: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var lblSecurityHeader: UILabel!
    @IBOutlet weak var btnEyes: UIButton!
    @IBOutlet weak var btnConfirmEye: UIButton!
    @IBOutlet weak var lblPasswordMarque: MarqueeLabel! {
        didSet {
            lblPasswordMarque.attributedText = lblPasswordMarque.scrollMarqueLabel(title: "Create Password(Min 6 digits)".localized)
        }
    }
    
    
    var isSecurityQShown = false
    var isAnswerShown = false
    var  isNextViewHidden = true
    var PASSWORDCHARSET = ""
    var ANSWERCHARSET = ""
    var VALIDCHAR = "1234567890"
    override func viewDidLoad() {
        super.viewDidLoad()
        lblHeader.attributedText = lblHeader.attributedStringFinal(str1: "Select Security Type".localized)
        lblPattern.text = "Pattern".localized
        lblPassword.text = "Password".localized
      //  tfPassword.placeholder = "Create Password(Min 6 digits)".localized
        tfConfirmPassword.placeholder = "Confirm Password".localized
        lblSecurityHeader.attributedText = lblSecurityHeader.attributedStringFinal(str1: "Select Security Question ".localized)
        lblQuestion.attributedText =  lblQuestion.attributedStringFinal(str1: "Select Security Question".localized)
        tfAnswer.placeholder = "Enter Answer".localized
        
        tfPassword.delegate = self
        tfConfirmPassword.delegate = self
        tfAnswer.delegate = self
        self.dHeight = viewlockTypeSelect.frame.origin.y + viewlockTypeSelect.frame.height
      //  btnEyes.isHidden = true
      //  btnConfirmEye.isHidden = true
           PASSWORDCHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"
        if ok_default_language == "en"{
            resetButton.setBackgroundImage(UIImage(named: "reset_blue"), for: .normal)
            ANSWERCHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890/-,() "
        }else if ok_default_language == "my" {
             resetButton.setBackgroundImage(UIImage(named: "reset_blue_my"), for: .normal)
           // PASSWORDCHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာ၁၂၃၄၅၆၇၈၉၀"
            ANSWERCHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာ၁၂၃၄၅၆၇၈၉၀/-,() "
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
 
    @IBAction func btnResetSecurityLockAction(_ sender: Any) {
        RegistrationModel.shared.PasswordType = ""
        RegistrationModel.shared.Password = ""
        RegistrationModel.shared.SecurityQuestionCode = ""
        RegistrationModel.shared.SecurityQuestionAnswer = ""
        RegistrationModel.shared.ConfirmPassword = ""
        imgPattern.image = UIImage(named:"r_Unradio")
        imgPassword.image = UIImage(named:"r_Unradio")
        tfPassword.text = ""
        lblPasswordMarque.text = "Create Password(Min 6 digits)".localized
        tfConfirmPassword.text = ""
        lblQuestion.text = "Select Security Question".localized
        tfAnswer.text = ""

        self.topConstraintsSSQues.constant = -114
        
        if isSecurityQShown {
            if isAnswerShown {
                self.dHeight = 80 + 97 + 57
            }else {
                self.dHeight = 80 + 97
            }
        }else {
           self.dHeight = viewlockTypeSelect.frame.origin.y + viewlockTypeSelect.frame.height
        }
        DispatchQueue.main.async {
            self.runGuard()
        }
    }
    
    
    @IBAction func btnPasswordAction(_ sender: Any) {
        if !(imgPassword.image == UIImage(named:"select_radio")) {

            tfPassword.text = ""
            lblPasswordMarque.text = "Create Password(Min 6 digits)".localized
            tfConfirmPassword.text = ""
             RegistrationModel.shared.Password = ""
            RegistrationModel.shared.PasswordType = "0"
            
            alertViewObj.wrapAlert(title:"", body:"Password must be atleast 6 digits. It should not be same with your mobile number or name and also case-sensitive".localized, img:#imageLiteral(resourceName: "password"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                self.tfPassword.becomeFirstResponder()
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
            
            imgPassword.image = UIImage(named:"select_radio")
            imgPattern.image = UIImage(named:"r_Unradio")
         //   self.topConstraintsSSQues.constant = 0
            
            if isSecurityQShown{
                    if isAnswerShown {
                          DispatchQueue.main.async {
                        self.topConstraintsSSQues.constant = -57
                             self.dHeight = 80 + 57 + 154
                             self.runGuard()
                        }
                    }else {
                        DispatchQueue.main.async {
                            self.topConstraintsSSQues.constant = -57
                            self.dHeight = 80 + 57 + 97
                             self.runGuard()
                        }
                    }
                }else {
                  DispatchQueue.main.async {
                    self.dHeight = self.viewlockTypeSelect.frame.height + self.viewCreatePassword.frame.height
                    self.topConstraintsSSQues.constant = -57
                      self.runGuard()
                }
            }
         
        }
    }
    
    @IBAction func btnPatternAction(_ sender: Any) {
        
        if !(imgPattern.image == UIImage(named:"select_radio")) || !(imgPattern.image == UIImage(named:"r_Unradio")){
            imgPassword.image = UIImage(named:"r_Unradio")
            tfPassword.text = ""
            lblPasswordMarque.text = "Create Password(Min 6 digits)".localized
            tfConfirmPassword.text = ""
             RegistrationModel.shared.Password = ""
            if let dl = self.delegate {
                dl.showPatternVC()
            }
    }
    }
    
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == tfPassword {
            if imgPattern.image == #imageLiteral(resourceName: "r_Unradio") && imgPassword.image == #imageLiteral(resourceName: "r_Unradio") {
                showAlert(alertTitle: "", alertBody: "Select Security Type".localized, alertImage: #imageLiteral(resourceName: "password"))
               return false
            }
        }else if textField == tfAnswer {
          //  tfAnswer.autocorrectionType = .no
            tfAnswer.isSecureTextEntry = false
            if lblQuestion.text ==  "Select Security Question".localized {
            showAlert(alertTitle: "", alertBody: "Please Select Security Question First".localized, alertImage: #imageLiteral(resourceName: "password"))
              return false
            }
        }
        return true
    }
    
    
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == tfPassword {
            RegistrationModel.shared.Password = tfPassword.text!
            if tfPassword.text!.count < 6 && tfPassword.text!.count > 0 {
      //      showAlert(alertTitle: "", alertBody: "Password must be at least 6 characters".localized, alertImage: #imageLiteral(resourceName: "password"))
            }
        } else if textField == tfConfirmPassword {
            RegistrationModel.shared.ConfirmPassword = tfConfirmPassword.text!
            if tfConfirmPassword.text!.count < tfPassword.text!.count {
        //    showAlert(alertTitle: "", alertBody: "Password and Confirm Password does not matched".localized, alertImage: #imageLiteral(resourceName: "password"))
            }
        }else if textField == tfAnswer {
            if tfAnswer.text!.count > 0 {
                    if self.dHeight! <= viewSelectSecurityQuestion.frame.origin.y + viewSelectSecurityQuestion.frame.height{
                    self.dHeight = viewSelectSecurityQuestion.frame.origin.y + viewSelectSecurityQuestion.frame.height
                    runGuard()
                    }

                  if isNextViewHidden{
                    isNextViewHidden = false
                    guard(self.delegate?.showSignatureVC() != nil)else { return false }
//                    if let del = self.delegate {
//                        del.showAdditionalInformationVC()
//                    }
                }
               
            }
            removeSpaceAtLast(txtField:tfAnswer)
            //ANTONY
        RegistrationModel.shared.SecurityQuestionAnswer = tfAnswer.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        return true
    }

    @IBAction func didEditChanged(_ sender: UITextField) {
        if tfPassword.text?.count == 0 {
        lblPasswordMarque.text = "Create Password(Min 6 digits)".localized
        }else {
        lblPasswordMarque.text = ""
        }
    }
    
    
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if range.location == 0 && string == " " {
            return false
        }
        
        if textField == tfPassword {
          
            tfConfirmPassword.text = ""
             RegistrationModel.shared.ConfirmPassword = ""
            RegistrationModel.shared.Password = ""
          if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: PASSWORDCHARSET).inverted).joined(separator: "")) { return false }
            
            if text.count > 1 {
               if (string == string.components(separatedBy: NSCharacterSet(charactersIn: VALIDCHAR).inverted).joined(separator: "")) {
                let last = text.index(text.endIndex, offsetBy: -1)
                let secondLast = text.index(text.endIndex, offsetBy: -2)
                if text[last] == text[secondLast] {
                    if string != "" {
                        tfPassword.text = ""
                        lblPasswordMarque.text = "Create Password(Min 6 digits)".localized
                        showAlert(alertTitle :"" , alertBody:"Please enter valid password. Password like 111111, 112233 and your mobile number is not allowed for security reasons".localized, alertImage: #imageLiteral(resourceName: "password") )
                        return false
                    }
                }
                }
            }
           // if text.count > 0 { btnEyes.isHidden = false }else{ btnEyes.isHidden = true }
            let str = RegistrationModel.shared.MobileNumber
            let startIndex = str.index(str.startIndex, offsetBy: 4)
            if text == String(str[startIndex...]) {
                    tfPassword.text = ""
                lblPasswordMarque.text = "Create Password(Min 6 digits)".localized
                    showAlert(alertTitle :"" , alertBody:"Please enter valid password. Password like 111111, 112233 and your mobile number is not allowed for security reasons".localized, alertImage: #imageLiteral(resourceName: "password") )
                    return false
            }
            
            if text.count > 30 {
                return false
            }else if text.count > 5 {
                    if self.dHeight! <= viewlockTypeSelect.frame.height + viewConfirmPassword.frame.height + viewConfirmPassword.frame.origin.y {
                        self.dHeight = viewlockTypeSelect.frame.height + viewConfirmPassword.frame.height + viewConfirmPassword.frame.origin.y
                          self.topConstraintsSSQues.constant = 0
                          runGuard()
                    }else {
                        if isSecurityQShown{
                            if isAnswerShown {
                                DispatchQueue.main.async {
                                     UserDefaults.standard.set(true, forKey: "PasswordScroll")
                                    self.topConstraintsSSQues.constant = 0
                                    self.dHeight = 80 + 114 + 154
                                    self.runGuard()
                                }
                            }else {
                                DispatchQueue.main.async {
                                     UserDefaults.standard.set(true, forKey: "PasswordScroll")
                                    self.topConstraintsSSQues.constant = 0
                                    self.dHeight = 80 + 144 + 97
                                    self.runGuard()
                                }
                            }
                        }
                }
            }
        }else if textField == tfConfirmPassword {
          //  if text.count > 0 { btnConfirmEye.isHidden = false }else{ btnConfirmEye.isHidden = true }
            
            if validObj.checkMatchingNumber(string: text, withString: self.tfPassword.text!) {
                self.tfConfirmPassword.text = text
                if text == tfPassword.text! {
                    RegistrationModel.shared.Password = tfPassword.text!
                    isSecurityQShown = true
                    tfConfirmPassword.resignFirstResponder()
                    
                    if self.dHeight! <= viewSelectSecurityQuestion.frame.origin.y + viewSelectSecurityQuestion.frame.height - 57{
                        self.dHeight! = viewSelectSecurityQuestion.frame.origin.y + viewSelectSecurityQuestion.frame.height - 57
                        self.topConstraintsSSQues.constant = 0
                        runGuard()
                    }
                    if isNextViewHidden{ }
                }
                return false
            } else {  return false  }
        }else if textField == tfAnswer{
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: ANSWERCHARSET).inverted).joined(separator: "")) { return false }
            if text.count > 50 {
                return false
            }
            if restrictMultipleSpaces(str: string, textField: tfAnswer) {
                return true
            }else {
                if  textField.text?.last == " " {
                    textField.text?.removeLast()
                }
                return false
            }
        }
        return true
    }
    
    @IBAction func btnSecuritQuestionAction(_ sender: Any) {
        if let del = delegate {
            del.viewEndEditing()
        }
        
        if imgPattern.image == #imageLiteral(resourceName: "r_Unradio") && imgPassword.image == #imageLiteral(resourceName: "r_Unradio")  {
            showAlert(alertTitle: "", alertBody: "Please Select Password Type First".localized, alertImage: #imageLiteral(resourceName: "password"))
        }else {
        tfAnswer.text = ""
        tfAnswer.resignFirstResponder()
        tfPassword.resignFirstResponder()
        tfConfirmPassword.resignFirstResponder() 
        let sb = UIStoryboard(name: "Registration" , bundle: nil)
        let securityQuestionR  = sb.instantiateViewController(withIdentifier: "SecurityQuestionR") as! SecurityQuestionR
            securityQuestionR.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            securityQuestionR.securityQuestionRDelegate = self
            securityQuestionR.modalPresentationStyle = .overCurrentContext
            self.present(securityQuestionR, animated: false, completion: nil)
        }
    }
    
    
    func SelectedQuestion(questionDic: Dictionary<String, AnyObject>) {
        
        RegistrationModel.shared.SecurityQuestionCode = questionDic["QuestionCode"] as! String
        if ok_default_language == "my" {
            lblQuestion.text = questionDic["QuestionBurmese"] as? String
        }else {
              lblQuestion.text = questionDic["Question"] as? String
        }
   
        if isNextViewHidden {
            if self.dHeight! <= viewSelectSecurityQuestion.frame.origin.y + veiwAnswer.frame.height + veiwAnswer.frame.origin.y {
            self.dHeight = viewSelectSecurityQuestion.frame.origin.y + veiwAnswer.frame.height + veiwAnswer.frame.origin.y
                isAnswerShown = true
            runGuard()
            }
        }
       // tfAnswer.becomeFirstResponder()
    }
    
    func CancelPatternPasswordVC(PasswordPatternViewControllerR : UIViewController){
        PasswordPatternViewControllerR.navigationController?.popViewController(animated: false)
//        if !isSecurityQShown {
//             self.topConstraintsSSQues.constant = 0
//        }
        self.topConstraintsSSQues.constant = -114
        if isSecurityQShown {
            if isAnswerShown {
                self.dHeight = 80 + 97 + 57
            }else {
                self.dHeight = 80 + 97
            }
        }else {
            self.dHeight = viewlockTypeSelect.frame.origin.y + viewlockTypeSelect.frame.height
        }
        DispatchQueue.main.async {
            self.runGuard()
        }
    }
    
     func ShowPatternPasswordViewAndPasswordString(strPassword : String,PasswordPatternViewControllerR : UIViewController){
        PasswordPatternViewControllerR.navigationController?.popViewController(animated: false)
        isSecurityQShown = true
        RegistrationModel.shared.PasswordType = "1"
        RegistrationModel.shared.Password = strPassword
        self.topConstraintsSSQues.constant = -114
        imgPattern.image = UIImage(named:"select_radio")
        imgPassword.image = UIImage(named:"r_Unradio")
        tfAnswer.text = ""
        
        if self.dHeight! <= 177 {
            self.dHeight = 177
        }else if self.dHeight! <= 177 + 57 {
            self.dHeight = 177 + 57
        }else {
          self.dHeight = 234
        }
        DispatchQueue.main.async {
            self.runGuard()
        }
    }

    @IBAction func touchDown(_ sender: Any) {
        // tfPassword.isSecureTextEntry = false
       //  btnEyes.setImage(#imageLiteral(resourceName: "r_hide"), for: .normal)
    }
    
    @IBAction func btnShowPassword(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            tfPassword.isSecureTextEntry = false
            btnEyes.setImage(#imageLiteral(resourceName: "r_hide"), for: .normal)
        }else {
            tfPassword.isSecureTextEntry = true
            btnEyes.setImage(#imageLiteral(resourceName: "r_show"), for: .normal)
        }

    }
    
    @IBAction func btnShowConfirmPassword(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            tfConfirmPassword.isSecureTextEntry = false
            btnConfirmEye.setImage(#imageLiteral(resourceName: "r_hide"), for: .normal)
        }else {
            tfConfirmPassword.isSecureTextEntry = true
            btnConfirmEye.setImage(#imageLiteral(resourceName: "r_show"), for: .normal)
        }
    }
    
    @IBAction func touchDownConfirm(_ sender: UIButton) {
     //   tfConfirmPassword.isSecureTextEntry = false
     //   btnConfirmEye.setImage(#imageLiteral(resourceName: "r_hide"), for: .normal)
    }
    
    
    func runGuard() {
        guard(self.delegate?.updateMainUIUI() != nil) else {
            return
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    @IBAction func btnShowDemoAction(_ sender: Any) {
        if let del = delegate {
            var strURL = ""
            if ok_default_language == "my"{
                strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/d9ded764-c099-4405-b56c-a1a41683748aReqDocumentsJuly_02_2018%2001_25_35%20PM.jpg";
            }else {
                strURL = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959897241397/7c810532-0aad-4a05-8b65-2234d8104578ReqDocumentsJuly_02_2018%2001_35_58%20PM.jpg";
            }
           del.showDemo(strURL:strURL , imgDemo:#imageLiteral(resourceName: "password") ,demoTitile: "Select Security Type".localized)
        }
    }
    
    private func removeSpaceAtLast(txtField : UITextField){
        if txtField.text?.last == " " {
            txtField.text?.removeLast()
        }
    }

    override var textInputMode: UITextInputMode? {
        let language = ok_default_language
        if language.isEmpty {
            return super.textInputMode
        } else {
            for tim in UITextInputMode.activeInputModes {
                if tim.primaryLanguage!.contains(language) {
                    return tim
                }
            }
            return super.textInputMode
        }
    }
}

