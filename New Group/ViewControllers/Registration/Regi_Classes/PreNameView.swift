//
//  PreNameView.swift
//  OK
//
//  Created by Subhash Arya on 08/02/18.
//  Copyright © 2018 Subhash Arya. All rights reserved.
//

import UIKit

public class PreNameView: UIView {
    
    @IBOutlet weak var lblPreName: MarqueeLabel!
    public class func loadView () -> UIView{
        let nib =  UINib(nibName: "PreNameView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[0] as! PreNameView
        nib.frame = .init(x: 0, y: 0, width: 50, height: 52)
        return nib
    }
    
    func setPreName(name : String , alignment : NSTextAlignment) {
        self.lblPreName.textAlignment = alignment
        self.lblPreName.text = name
    }
}

