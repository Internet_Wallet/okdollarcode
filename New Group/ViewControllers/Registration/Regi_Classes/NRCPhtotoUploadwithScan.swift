//
//  NRCPhtotoUploadwithScan.swift
//  OK
//
//  Created by Imac on 5/2/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import Rabbit_Swift

protocol NRCPhtotoUploadwithScanDelegate {
    func sendImagesToMainController(images: [String],screen: String, imagArr: [UIImage])
}


class NRCPhtotoUploadwithScan: OKBaseController {

    @IBOutlet weak var btnUpload: UIButton! {
        didSet {
            btnUpload.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnUpload.setTitle("Save".localized, for: .normal)
        }
    }
    
    var delegate: NRCPhtotoUploadwithScanDelegate?
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var tbImages: UITableView!
    var imagePicker : UIImagePickerController?
    var imageList: [UIImage]?
    var selectedIndex = 0
    var optionRowHeight = 0
    var buttonName: [String]?
    var buttonTitleName: [String]?
    var checkImageHave = false
    var imagesURL: [String]?
    var imagesChanged = false
    var imgToUpload: UIImage?
    var screenTitle = ""
    var uploadedIamge = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if imageList?.count == 0 {
            imageList?.append(UIImage(named: "UploadImagebg")!)
        }else if imageList?.count == 1  {
            imageList?.append(UIImage(named: "UploadImagebg")!)
        }
        
        if let imgs = imageList {
            uploadedIamge = imgs
        }
        
        stackView.isHidden = true
    }
    
    func setMarqueLabelInNavigation(title: String) {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: 16)
        lblMarque.text = title
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
        screenTitle = title
    }
    
    @objc func onClickImageAction(_ sender: UIButton) {
        selectedIndex = sender.tag
        optionRowHeight = 200
        self.stackView.isHidden = true
        self.reloadSection(index: 1)
        self.scrollToRow(indexRow: 0, indexSection: 1)
    }
    
    func reloadSection(index: Int) {
        let indexSet: IndexSet = [index]
        self.tbImages.beginUpdates()
        self.tbImages.reloadSections(indexSet, with: .automatic)
        self.tbImages.endUpdates()
    }
    
    func reloadRows(index: Int, withSeciton sec: Int) {
        UIView.setAnimationsEnabled(false)
        self.tbImages.beginUpdates()
        let indexPosition = IndexPath(row: index, section: sec)
        self.tbImages.reloadRows(at: [indexPosition], with: .none)
        self.tbImages.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
    @IBAction func onClickBackAction(_ sender: Any) {
        if imagesChanged {
            if let del = delegate {
                del.sendImagesToMainController(images: imagesURL ?? [""],screen: screenTitle, imagArr: uploadedIamge)
            }
            if ReRegistrationModel.shared.ocr_F_nrcid != "" {
               NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NRC_NUMBER_CARD"), object: nil)
            }
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc func onClickCamera(_ sender: UIButton) {
        DispatchQueue.main.async {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let picker = UIImagePickerController()
                picker.allowsEditing = false
                picker.delegate = self
                picker.title = "Camera".localized
                picker.sourceType = UIImagePickerController.SourceType.camera
                picker.cameraCaptureMode = .photo
                picker.modalPresentationStyle = .fullScreen
                self.present(picker,animated: true,completion: nil)
            }
        }
        
    }
    
    @objc func onClickGallery(_ sender: UIButton) {
        DispatchQueue.main.async {
            let picker = UIImagePickerController()
            picker.allowsEditing = false
            picker.delegate = self
            picker.title = "Gallery".localized
            picker.sourceType = .photoLibrary
            picker.modalPresentationStyle = .popover
            self.present(picker, animated: true, completion: nil)
        }
    }
    @objc func onClickCancel(_ sender: UIButton) {
        if checkImageHave {
            self.stackView.isHidden = false
        }else {
            self.stackView.isHidden = true
        }
        
        optionRowHeight = 0
        self.reloadSection(index: 1)
        scrollToRow(indexRow: 0, indexSection: 0)
        
    }
    func scrollToRow(indexRow: Int, indexSection: Int) {
        let indexPath = IndexPath(row: indexRow, section: indexSection)
        self.tbImages.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    
    
    func getTokenForNRCScan(imgSixtyFour: String) {
        if appDelegate.checkNetworkAvail() {
            var arr = [String]()
            arr.append("password")
            let password = arr
            let user = ["name":"testing","password":"5stkbc2V7N", "domain":["name": "OkdollarTest"]] as [String : Any]
            let passwordUser = ["user": user]
            let identity = ["methods": password,"password": passwordUser] as [String : Any]
            let project =  ["name":"ap-southeast-1"]
            let scop = ["project": project]
            let finaldic = ["identity": identity,"scope": scop]
            let dic = ["auth" : finaldic]
            
            let url = URL(string: "https://iam.ap-southeast-1.myhwclouds.com/v3/auth/tokens")
            
            TopupWeb.genericClassWithHeaderInfo(url:url!, param: dic as AnyObject, httpMethod: "POST", handle: {response, isSuccess in
                if isSuccess {
                    if let headerRes = response as? HTTPURLResponse, headerRes.statusCode == 201 {
                        let token = headerRes.allHeaderFields["X-Subject-Token"] as? String ?? ""
                        println_debug(token)
                        self.getDataFromNRCPhoto(tokenString: token, imgString: imgSixtyFour)
                    }
                }else {
                    DispatchQueue.main.async {
                        self.showAlert(alertTitle: "", alertBody: "Network error and try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))}
                }
            })
        }else {
            alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img:#imageLiteral(resourceName: "r_user"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    func getDataFromNRCPhoto(tokenString: String, imgString: String) {
        if appDelegate.checkNetworkAvail() {
            let dic = ["image": imgString]
            let url = URL(string: "https://ocr.ap-southeast-1.myhuaweicloud.com/v1.0/myanmar-id-card")
            println_debug("url \(String(describing: url))")
            TopupWeb.genericClassWithHeader(url:url!, param: dic as AnyObject, httpMethod: "POST", httpHeader: tokenString, handle: {response, isSuccess in
                if isSuccess {
                    if let dic = response as? Dictionary<String,AnyObject> {
                            print(dic)
                        if let info = dic["result"] {
                            let side = info["side"] as? String ?? ""
                            if side == "front" {
                                ReRegistrationModel.shared.ocr_F_birth =  Rabbit.uni2zg(info["birth"] as? String ?? "")
                                ReRegistrationModel.shared.ocr_F_bloodGroup = Rabbit.uni2zg(info["blood_group"] as? String ?? "")
                                ReRegistrationModel.shared.ocr_F_bloodlinesReligion = Rabbit.uni2zg(info["bloodlines_religion"] as? String ?? "")
                                ReRegistrationModel.shared.ocr_F_className = Rabbit.uni2zg(info["class"] as? String ?? "")
                                ReRegistrationModel.shared.ocr_F_fatherName = Rabbit.uni2zg(info["father_name"] as? String ?? "")
                                ReRegistrationModel.shared.ocr_F_height = Rabbit.uni2zg(info["height"] as? String ?? "")
                                ReRegistrationModel.shared.ocr_F_issueDate = Rabbit.uni2zg(info["issue_date"] as? String ?? "")
                                ReRegistrationModel.shared.ocr_F_name = Rabbit.uni2zg(info["name"] as? String ?? "")
                                ReRegistrationModel.shared.ocr_F_nrcid = Rabbit.uni2zg(info["nrc_id"] as? String ?? "")
                                ReRegistrationModel.shared.ocr_F_side = Rabbit.uni2zg(info["side"] as? String ?? "")
                            }else if side == "back" {
                                ReRegistrationModel.shared.ocr_B_address = Rabbit.uni2zg(info["address"] as? String ?? "")
                                ReRegistrationModel.shared.ocr_B_cardID  = Rabbit.uni2zg(info["card_id"] as? String ?? "")
                                ReRegistrationModel.shared.ocr_B_className = Rabbit.uni2zg(info["class"] as? String ?? "")
                                ReRegistrationModel.shared.ocr_B_nrcIdback = Rabbit.uni2zg(info["nrc_id_back"] as? String ?? "")
                                ReRegistrationModel.shared.ocr_B_profession = Rabbit.uni2zg(info["profession"] as? String ?? "")
                                ReRegistrationModel.shared.ocr_B_side = Rabbit.uni2zg(info["side"] as? String ?? "")
                            }
                            self.imageUploadOnServer(imageFile: imgString)
                        }else {
                            ReRegistrationModel.shared.ocr_F_nrcid = ""
                            self.imageUploadOnServer(imageFile: imgString)
                        }
                    }
                    
                }else {
                    self.showAlert(alertTitle: "", alertBody: "API Request failed, Please try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
                }
            })
        }else {
            alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img:#imageLiteral(resourceName: "r_user"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
    
}



extension NRCPhtotoUploadwithScan: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return imageList?.count ?? 0
        }else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ImageUpCellRegi") as! ImageUpCellRegi
            cell.selectionStyle = .none
            cell.btnTakeImage.tag = indexPath.row
            cell.btnTakeImage.addTarget(self, action: #selector(onClickImageAction(_:)) , for: .touchUpInside)
            cell.lblTop.text = buttonTitleName?[indexPath.row] ?? ""
            if let img = imageList?[indexPath.row], imageList?[indexPath.row] != UIImage(named: "UploadImagebg") {
                cell.imgActual.image = img
                cell.imgActual.layer.cornerRadius = 15
                cell.imgActual.layer.masksToBounds = true
                cell.btnTakeImage.setTitle("", for: .normal)
            }else {
                cell.imgActual.image = nil
                cell.btnTakeImage.setTitle(buttonName?[indexPath.row] ?? "", for: .normal)
            }
            cell.imgBg.image = UIImage(named: "UploadImagebg")
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ImageOptionUPCellRegi") as! ImageOptionUPCellRegi
            if optionRowHeight == 0 {
                cell.isHidden = true
            }else {
                cell.isHidden = false
            }
            cell.selectionStyle = .none
            cell.btnCamera.addTarget(self, action: #selector(onClickCamera(_:)), for: .touchUpInside)
            cell.btnGallery.addTarget(self, action: #selector(onClickGallery(_:)), for: .touchUpInside)
            cell.btnCancel.addTarget(self, action: #selector(onClickCancel(_:)), for: .touchUpInside)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 250
        }else {
            if optionRowHeight == 0 {
                return 0
            }else {
                return 200
            }
        }
    }
    
}

extension NRCPhtotoUploadwithScan: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let info = convertFromUIImagePickerControllerInfoKeyDictionary2(info)
        if let image = info[convertFromUIImagePickerControllerInfoKey2(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            if selectedIndex == 0 {
                self.imageList?[0] = image
                imgToUpload = image
                let convertedStr = imgToUpload?.base64(format: .jpeg(0.5))
                if self.screenTitle == "NRC Images" {
                    if let str = convertedStr {
                        getTokenForNRCScan(imgSixtyFour: str)
                    }
                }else {
                    self.imageUploadOnServer(imageFile: convertedStr ?? "")
                }
            }else if selectedIndex == 1 {
                self.imageList?[1] = image
                imgToUpload = image
                let  convertedStr = imgToUpload?.base64(format: .jpeg(0.5))
                
                if self.screenTitle == "NRC Images" {
                    if let str = convertedStr {
                        getTokenForNRCScan(imgSixtyFour: str)
                    }
                }else {
                   self.imageUploadOnServer(imageFile: convertedStr ?? "")
                }
            }
        }
        dismiss(animated: true, completion:  {
            self.imagePicker = nil
        })
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[convertFromUIImagePickerControllerInfoKey2(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            if selectedIndex == 0 {
                self.imageList?[0] = pickedImage
                 imgToUpload = pickedImage
                let  convertedStr = imgToUpload?.base64(format: .jpeg(0.5))
                if self.screenTitle == "NRC Images" {
                    if let str = convertedStr {
                        getTokenForNRCScan(imgSixtyFour: str)
                    }
                }else {
                    self.imageUploadOnServer(imageFile: convertedStr ?? "")
                }
            }else if selectedIndex == 1 {
                self.imageList?[1] = pickedImage
                imgToUpload = pickedImage
                let  convertedStr = imgToUpload?.base64(format: .jpeg(0.5))
                if self.screenTitle == "NRC Images" {
                    if let str = convertedStr {
                        getTokenForNRCScan(imgSixtyFour: str)
                    }
                }else {
                    self.imageUploadOnServer(imageFile: convertedStr ?? "")
                }
            }
        }
        
        dismiss(animated: true, completion:  {
            self.imagePicker = nil
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: {
            self.imagePicker = nil
        })
    }
    
}

extension NRCPhtotoUploadwithScan: WebServiceResponseDelegate {
    func webResponse(withJson json: AnyObject, screen: String) {
        self.removeProgressView()
        do {
            if let data = json as? Data {
                let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let dic = dict as? Dictionary<String,AnyObject> {
                    println_debug(dic)
                    if dic["Code"] as? NSNumber == 200 {
                        let url = dic["Data"] as! String
                        self.imagesURL?[self.selectedIndex] = url
                        var mesage = ""
                        if self.screenTitle == "NRC Images" {
                            if selectedIndex == 0 {
                               mesage = "Front side NRC image uploaded successfully".localized
                            }else {
                               mesage = "Back side NRC image uploaded successfully".localized
                            }
                        }else {
                            if selectedIndex == 0 {
                                mesage = "Main page of passport image uploaded successfully".localized
                            }else {
                                mesage = "Visa or Residence page of passport image uploaded successfully".localized
                            }
                        }
                       
                        DispatchQueue.main.async {
                            alertViewObj.wrapAlert(title: nil, body: mesage, img: UIImage(named: "bank_success")!)
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                self.reloadRows(index: self.selectedIndex, withSeciton: 0)
                               
                                self.checkImageHave = true
                                self.optionRowHeight = 0
                                self.reloadSection(index: 1)
                                self.imagesChanged = true
                                if self.imageList?.count == 1 {
                                    self.imageList?.append(UIImage(named: "UploadImagebg")!)
                                    self.reloadSection(index: 0)
                                }
                                if self.selectedIndex == 0 {
                                    if let img = self.imgToUpload {
                                        if self.uploadedIamge.count > 0 {
                                             self.uploadedIamge.remove(at: 0)
                                        }
                                        self.uploadedIamge.insert(img, at: 0)
                                    }
                                }else {
                                    self.stackView.isHidden = false
                                    if let img = self.imgToUpload {
                                        if self.uploadedIamge.count > 1 {
                                            self.uploadedIamge.remove(at: 1)
                                        }
                                        self.uploadedIamge.insert(img, at: 1)
                                    }
                                }
                            }
                            alertViewObj.showAlert(controller: self)
                        }
                    }else {
                        DispatchQueue.main.async {
                            alertViewObj.wrapAlert(title: nil, body: "Image uploading failed. Please try again".localized, img: #imageLiteral(resourceName: "info_one"))
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertViewObj.showAlert(controller: self)
                        }
                    }
                }
            }else {
                DispatchQueue.main.async {
                    alertViewObj.wrapAlert(title: nil, body: "Image uploading failed. Please try again".localized, img: #imageLiteral(resourceName: "info_one"))
                    alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                        self.dismiss(animated: true, completion: nil)
                    }
                    alertViewObj.showAlert(controller: self)
                }
            }
        } catch {}
    }
    
    func imageUploadOnServer(imageFile: String) {
        showProgressView()
        if let img = imgToUpload {
            let  convertedStr = img.base64(format: .jpeg(0.1))
            DispatchQueue.main.async {
                let paramString : [String:Any] = [
                    "MobileNumber": RegistrationModel.shared.MobileNumber,
                    "Base64String": convertedStr as! String
                ]
                let web      = WebApiClass()
                web.delegate = self
                let ur = URL(string: "https://www.okdollar.co/RestService.svc/GetImageUrlByBase64String")//getUrl(urlStr: urlStr, serverType: .imageUpload)
                println_debug(ur)
                web.genericClass(url: ur!, param: paramString as AnyObject, httpMethod: "POST", mScreen: "mImageUpload")
            }
        }
    }
}




class ImageUpCellRegi: UITableViewCell {
    @IBOutlet weak var lblTop: UILabel!{
        didSet{
            lblTop.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblBottom: UILabel!{
        didSet {
            lblBottom.font = UIFont(name: appFont, size: appFontSize)
            lblBottom.text = "Tap to Change".localized
        }
    }
    @IBOutlet weak var btnTakeImage: UIButton!{
        didSet {
            btnTakeImage.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var imgActual: UIImageView!
    
}


class ImageOptionUPCellRegi: UITableViewCell {
    @IBOutlet weak var btnCamera: UIButton! {
        didSet {
            btnCamera.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnCamera.setTitle("Camera".localized, for: .normal)
        }
    }
    @IBOutlet weak var btnCancel: UIButton! {
        didSet {
            btnCancel.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnCancel.setTitle("Cancel".localized, for: .normal)
        }
    }
    @IBOutlet weak var btnGallery: UIButton! {
        didSet {
            btnGallery.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnGallery.setTitle("Gallery".localized, for: .normal)
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary2(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey2(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}

extension UIImage {
func resizedImage(withPercentage percentage: CGFloat) -> UIImage? {
    let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
    UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
    defer { UIGraphicsEndImageContext() }
    draw(in: CGRect(origin: .zero, size: canvasSize))
    return UIGraphicsGetImageFromCurrentImageContext()
}
}
