//
//  MarqueeButton.swift
//  OK
//
//  Created by PC on 1/25/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//


import UIKit

open class MarqueeButton: UIView {
    
}

extension UIButton {
    func startButtonAnimation() {
        
        UIView.animate(withDuration: 12.0, delay: 1, options: ([.curveLinear, .repeat]), animations: {() -> Void in
            self.titleLabel?.center = CGPoint(x:0 - (self.titleLabel?.bounds.size.width)! / 2, y:(self.titleLabel?.center.y)!)
        }, completion:  { _ in })
        
    }
}
