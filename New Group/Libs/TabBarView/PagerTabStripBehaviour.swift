
//
//  PagerTabStripBehaviour.swift
//  OK
//
//  Created by palnar on 29/11/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//
import Foundation

public enum PagerTabStripBehaviour {
    
    case Common(skipIntermediateViewControllers: Bool)
    case Progressive(skipIntermediateViewControllers: Bool, elasticIndicatorLimit: Bool)
    
    public var skipIntermediateViewControllers: Bool {
        switch self {
        case .Common(let skipIntermediateViewControllers):
            return skipIntermediateViewControllers
        case .Progressive(let skipIntermediateViewControllers, _):
            return skipIntermediateViewControllers
        }
    }
    
    
    public var isProgressiveIndicator: Bool {
        switch self {
        case .Common(_):
            return false
        case .Progressive(_, _):
            return true
        }
    }
    
    public var isElasticIndicatorLimit: Bool {
        switch self {
        case .Common(_):
            return false
        case .Progressive(_, let elasticIndicatorLimit):
            return elasticIndicatorLimit
        }
    }
}




