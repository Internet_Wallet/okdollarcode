//
//  SwipeDirection.swift
//  OK
//
//  Created by palnar on 29/11/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//
import Foundation

public enum SwipeDirection {
    case Left
    case Right
    case None
}
