
//
//  ButtonBarViewCell.swift
//  OK
//
//  Created by palnar on 29/11/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//
import Foundation
import UIKit

public class ButtonBarViewCell: UICollectionViewCell {
    
    @IBOutlet public var imageView: UIImageView!
    @IBOutlet public lazy var label: UILabel! = { [unowned self] in
        let label = UILabel(frame: self.contentView.bounds)
        label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        label.textAlignment = .center
        label.font = UIFont.init(name: appFont, size: 20.00)
        return label
    }()
    
    public override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        
        if label.superview != nil {
            contentView.addSubview(label)
        }
    }
}
