
//
//  PagerTabStripError.swift
//  OK
//
//  Created by palnar on 29/11/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//
import Foundation

public enum PagerTabStripError: Error {
    case ViewControllerNotContainedInPagerTabStrip
}
