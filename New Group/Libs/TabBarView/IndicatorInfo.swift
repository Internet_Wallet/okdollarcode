//
//  IndicatorInfo.swift
//  OK
//
//  Created by palnar on 29/11/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//


import Foundation
import UIKit

public struct IndicatorInfo {
    
    public var title: String
    public var image: UIImage?
    public var highlightedImage: UIImage?
    
    public init(title: String) {
        self.title = title
    }
    
    public init(title: String, image: UIImage?) {
        self.init(title: title)
        self.image = image
    }
    
    public init(title: String, image: UIImage?, highlightedImage: UIImage?) {
        self.init(title: title, image: image)
        self.highlightedImage = highlightedImage
    }
}


extension IndicatorInfo : ExpressibleByStringLiteral {
    
    public init(stringLiteral value: String){
        title = value
    }
    
    public init(extendedGraphemeClusterLiteral value: String){
        title = value
    }
    
    public init(unicodeScalarLiteral value: String){
        title = value
    }
}
