
//
//  BarView.swift
//  OK
//
//  Created by palnar on 29/11/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//
import Foundation
import UIKit

public class BarView: UIView {
    
    public lazy var selectedBar: UIView = { [unowned self] in
        let selectedBar = UIView(frame: CGRect(origin : CGPoint(x:0, y:0), size:CGSize(width:self.frame.size.width, height:self.frame.size.height)))
        
       
        return selectedBar
    }()
    
    var optionsCount = 1 {
        willSet(newOptionsCount) {
            if newOptionsCount <= selectedIndex {
                selectedIndex = optionsCount - 1
            }
        }
    }
    var selectedIndex = 0
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addSubview(selectedBar)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(selectedBar)
    }
    
    
    // MARK: - Helpers
    
    private func updateSelectedBarPositionWithAnimation(animation: Bool) {
        var frame = selectedBar.frame
        frame.size.width = self.frame.size.width / CGFloat(optionsCount)
        frame.origin.x = frame.size.width * CGFloat(selectedIndex)
        if animation {
            
            UIView.animate(withDuration :0.3, animations: { [weak self] in
                self?.selectedBar.frame = frame
            })
        }
        else{
            selectedBar.frame = frame
        }
    }
    
    public func moveToIndex( index: Int, animated: Bool) {
        selectedIndex = index
        updateSelectedBarPositionWithAnimation(animation :animated)
    }
    
    public func moveToIndex( fromIndex: Int, toIndex: Int, progressPercentage: CGFloat) {
        selectedIndex = (progressPercentage > 0.5) ? toIndex : fromIndex
        
        var newFrame = selectedBar.frame
        newFrame.size.width = frame.size.width / CGFloat(optionsCount)
        var fromFrame = newFrame
        fromFrame.origin.x = newFrame.size.width * CGFloat(fromIndex)
        var toFrame = newFrame
        toFrame.origin.x = toFrame.size.width * CGFloat(toIndex)
        var targetFrame = fromFrame
        targetFrame.origin.x += (toFrame.origin.x - targetFrame.origin.x) * CGFloat(progressPercentage)
        selectedBar.frame = targetFrame
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        updateSelectedBarPositionWithAnimation(animation : false)
    }
}
