//
//  CustomAlert.m
//  CustomAlert
//
//  Created by Mariya Kholod on 4/23/13.
//  Copyright (c) 2013 Mariya Kholod. All rights reserved.
//

#import "CustomAlert.h"
#define MAX_ALERT_HEIGHT 300.0

@implementation CustomAlert

@synthesize delegate;

- (id)initWithTitle:(NSString *)title message:(NSString *)message delegate:(id)AlertDelegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitle:(NSString *)otherButtonTitle
{
  /*  CGRect frame;
    
    if (appDelegate.interfaceeOrientation == UIInterfaceOrientationPortrait){
        NSLog(@"appDelegate.isMSIDNVew %@",appDelegate.isMSIDNVew);

           frame = CGRectMake(0.0, 0.0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
        
    }
    else{
        frame = CGRectMake(0.0, 0.0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
    }
    
    self = [super initWithFrame:frame];
    
    NSString *fontname = [appDelegate getlocaLizationLanguage:@"fontname"];

    
    if (self) {
        self.delegate = AlertDelegate;
        //self.alpha = 0.95;
        self.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.6];
        
        self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        
        UIImage *AlertBg;
        UIImageView *alertIconImgView;
        if ([title isEqualToString:@"Alert"] || [title isEqualToString:@"အသိေပး"]) {
            AlertBg = [UIImage imageNamed:@"CustomAlertBG1.png"];
            alertIconImgView =  [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"alert-icon.png"]];
        }else{
            AlertBg = [UIImage imageNamed:@"CustomAlertBG.png"];
        }
        
        
        
        UIImage *CancelBtnImg = [UIImage imageNamed:@"cancel_btn.png"];
        UIImage *OtherBtnImg = [UIImage imageNamed:@"other_btn.png"];
        
        UIImageView *AlertImgView = [[UIImageView alloc] initWithImage:AlertBg];
        
        float alert_width = [[UIScreen mainScreen] bounds].size.width - 60;//AlertImgView.frame.size.width;
        float alert_height = 200;
//add text
        UILabel *TitleLbl;
        UILabel *separatorLineLbl;
        UIScrollView *MsgScrollView;
        UILabel *MessageLbl;
       
        if (title)
        {
            
            if ([title isEqualToString:@"Alert"] || [title isEqualToString:@"အသိေပး"]) {
                
                TitleLbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, alert_width, 40.0)];

            }else{
                
                TitleLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0, alert_width, 40.0)];

            }
            
            
            TitleLbl.adjustsFontSizeToFitWidth = YES;
            TitleLbl.font = [UIFont fontWithName:fontname size: 19.0];
            TitleLbl.textAlignment = NSTextAlignmentCenter;
            TitleLbl.minimumFontSize = 12.0;
            TitleLbl.backgroundColor = [UIColor clearColor];
            
            if ([title isEqualToString:@"Alert"] || [title isEqualToString:@"အသိေပး"]) {
                if ([title isEqualToString:@"Alert"]) {
                    
                    TitleLbl.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];

                }else{
                    TitleLbl.font = [UIFont fontWithName:@"Zawgyi-One" size:14];

                }

                TitleLbl.textColor = [UIColor colorWithRed:0.0 green:0.3373 blue:0.7294 alpha:1.0];
            }
            else{
               TitleLbl.textColor = [UIColor blackColor];
            }
            
            if ([title isEqualToString:@"Alert"] || [title isEqualToString:@"အသိေပး"]) {
                TitleLbl.text = @"";
            }else{
                TitleLbl.text = title;
            }
            TitleLbl.numberOfLines = 4;
            
        }
        else
        {
            
        }
        
        separatorLineLbl  = [[UILabel alloc]initWithFrame:CGRectMake(10, 50,alert_width-20,0.7)];
        separatorLineLbl.backgroundColor = [UIColor lightGrayColor];
        
        
        if (message)
        {
            
            MessageLbl = [[UILabel alloc] initWithFrame:CGRectMake(5.0, 50.0, alert_width - 10, alert_height - 100)];
            
            MessageLbl.numberOfLines = 10;
            MessageLbl.font = [UIFont fontWithName:fontname size: 18.0];
            MessageLbl.textAlignment = NSTextAlignmentCenter;
            MessageLbl.backgroundColor = [UIColor clearColor];
            MessageLbl.textColor = [UIColor blackColor];
            MessageLbl.text = message;
            
        }
        else{
           
        }
        
        
        //add buttons
        UIButton *CancelBtn;
        UIButton *OtherBtn;
        UILabel *lineLab;
        UILabel *cancelBtntextlable;
        UILabel *otherBtntextlable;
        
        if (cancelButtonTitle && otherButtonTitle)
        {
            float x_displ = (int)((alert_width-CancelBtnImg.size.width*2)/3.0);
            
            CancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(x_displ, alert_height -43, CancelBtnImg.size.width, CancelBtnImg.size.height )];
            cancelBtntextlable = [[UILabel alloc] initWithFrame:CGRectMake(x_displ, alert_height -43, CancelBtnImg.size.width, CancelBtnImg.size.height )];
            
            [CancelBtn setTag:1000];
           // [CancelBtn setTitle:cancelButtonTitle forState:UIControlStateNormal];
            [CancelBtn addTarget:self action:@selector(onBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            [CancelBtn.titleLabel setFont:[UIFont boldSystemFontOfSize: 15.0]];
            //cancelBtntextlable.backgroundColor = [UIColor redColor];
            cancelBtntextlable.font = [UIFont fontWithName:fontname size: 18.0];
            cancelBtntextlable.text = cancelButtonTitle;
            cancelBtntextlable.textColor = AppBlue;
            cancelBtntextlable.textAlignment = NSTextAlignmentCenter;
            
            OtherBtn = [[UIButton alloc] initWithFrame:CGRectMake(x_displ*2+CancelBtnImg.size.width, alert_height -43, OtherBtnImg.size.width, OtherBtnImg.size.height)];
            otherBtntextlable = [[UILabel alloc] initWithFrame:CGRectMake(x_displ*2+CancelBtnImg.size.width, alert_height - 43, OtherBtnImg.size.width, OtherBtnImg.size.height)];
            
            [OtherBtn setTag:1001];
           // [OtherBtn setTitle:otherButtonTitle forState:UIControlStateNormal];
            [OtherBtn.titleLabel setFont:[UIFont fontWithName:fontname size: 18.0]];
            //[OtherBtn setBackgroundImage:OtherBtnImg forState:UIControlStateNormal];
            [OtherBtn addTarget:self action:@selector(onBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            otherBtntextlable.font = [UIFont fontWithName:fontname size: 18.0];
            otherBtntextlable.text = otherButtonTitle;
            otherBtntextlable.textColor =AppBlue;
            otherBtntextlable.textAlignment = NSTextAlignmentCenter;
            
            lineLab = [[UILabel alloc] initWithFrame:CGRectMake(((alert_width - 2)/2), alert_height-42 , 2,42)];
            lineLab.backgroundColor = [UIColor clearColor];
            
        }
        else if (cancelButtonTitle)
        {
            CancelBtn = [[UIButton alloc] initWithFrame:CGRectMake((int)((alert_width-CancelBtnImg.size.width)/2.0), alert_height -43, CancelBtnImg.size.width, 40)];
            [CancelBtn setTag:1000];
            [CancelBtn setTitle:cancelButtonTitle forState:UIControlStateNormal];
 
            CancelBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
            [CancelBtn.titleLabel setFont:[UIFont fontWithName:fontname size: 18.0]];

            [CancelBtn setTitleColor:AppBlue forState:UIControlStateNormal];
            [CancelBtn addTarget:self action:@selector(onBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        }
        else if (otherButtonTitle)
        {
            OtherBtn = [[UIButton alloc] initWithFrame:CGRectMake((int)((alert_width-CancelBtnImg.size.width)/2.0), alert_height -43, CancelBtnImg.size.width, 40)];
            [OtherBtn setTag:1001];
            [OtherBtn setTitle:otherButtonTitle forState:UIControlStateNormal];
            [OtherBtn.titleLabel setFont:[UIFont fontWithName:fontname size: 18.0]];
             OtherBtn.titleLabel.textColor =AppBlue;
            [OtherBtn addTarget:self action:@selector(onBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        }
        else{
            
        }
        
       
        
        AlertView = [[UIView alloc] initWithFrame:CGRectMake((int)((self.frame.size.width-alert_width)/2.0 ), (int)((self.frame.size.height-alert_height)/2.0) , alert_width,alert_height)];
        
        AlertImgView.frame = AlertView.bounds;
        [AlertView addSubview:AlertImgView];
        
        [self addSubview:AlertView];
        
        if (cancelBtntextlable){
            [AlertView addSubview:cancelBtntextlable];
            //cancelBtntextlable.backgroundColor = [UIColor redColor];
        }
        
        if (otherBtntextlable) {
             [AlertView addSubview:otherBtntextlable];
           //  otherBtntextlable.backgroundColor = [UIColor redColor];
        }
        
        
        if (TitleLbl){
             [AlertView addSubview:TitleLbl];
           //  TitleLbl.backgroundColor = [UIColor redColor];
        }
        
        
        if (MessageLbl){
             [AlertView addSubview:MessageLbl];
             //MessageLbl.backgroundColor = [UIColor yellowColor];
        }
        
        
        if (CancelBtn){
             [AlertView addSubview:CancelBtn];
           // CancelBtn.backgroundColor = [UIColor brownColor];
        }
        
        
        if (OtherBtn){
             [AlertView addSubview:OtherBtn];
            // OtherBtn.backgroundColor = [UIColor brownColor];
        }
        
        if (lineLab){
            [AlertView addSubview:lineLab];
            // OtherBtn.backgroundColor = [UIColor brownColor];
        }
        
        if (separatorLineLbl) {
            [AlertView addSubview:separatorLineLbl];
        }
        
        if (alertIconImgView) {
//            alertIconImgView.frame = CGRectMake(((alert_width - 2)/2) - 45, 5, 30, 30);
            alertIconImgView.frame = CGRectMake(((alert_width - 2)/2) - 15 , 5, 30, 30);
            [AlertView addSubview:alertIconImgView];
        }
        
    }
    */
    return self;
}

- (void)onBtnPressed:(id)sender
{
    UIButton *button = (UIButton *)sender;
    
    long button_index = button.tag-1000;
    
    if ([delegate respondsToSelector:@selector(customAlertView:clickedButtonAtIndex:)])
        [delegate customAlertView:self clickedButtonAtIndex:button_index];
    
    [self animateHide];
}

//- (void)showInView:(UIView*)view
//{
//    if ([view isKindOfClass:[UIView class]])
//    {
//        // [self removeFromSuperview];
//        //[view removeFromSuperview];
//        
//        
//        [view addSubview:self];
//        [self animateShow];
//    }
//}


- (void)showInView:(UIView*)view
{
    if ([view isKindOfClass:[UIView class]])
    {
        // [self removeFromSuperview];
        //[view removeFromSuperview];
        
        self.frame = view.bounds;
        
        float alert_width = [[UIScreen mainScreen] bounds].size.width - 60;//AlertImgView.frame.size.width;
        float alert_height = 200;
        
        AlertView.frame = CGRectMake((int)((self.frame.size.width-alert_width)/2.0 ), (int)((self.frame.size.height-alert_height)/2.0) , alert_width,alert_height);
        [view addSubview:self];
        [self animateShow];
    }
}

-(void)showInWindow:(UIView *)view{
    
    if([view isKindOfClass:[UIView class]]){
        
        [[UIApplication sharedApplication].keyWindow addSubview:self];
        [self animateShow];
    }
}

- (void)animateShow
{
    CAKeyframeAnimation *animation = [CAKeyframeAnimation
                                      animationWithKeyPath:@"transform"];
    
    CATransform3D scale1 = CATransform3DMakeScale(0.5, 0.5, 1);
    CATransform3D scale2 = CATransform3DMakeScale(1.2, 1.2, 1);
    CATransform3D scale3 = CATransform3DMakeScale(0.9, 0.9, 1);
    CATransform3D scale4 = CATransform3DMakeScale(1.0, 1.0, 1);
    
    NSArray *frameValues = [NSArray arrayWithObjects:
                            [NSValue valueWithCATransform3D:scale1],
                            [NSValue valueWithCATransform3D:scale2],
                            [NSValue valueWithCATransform3D:scale3],
                            [NSValue valueWithCATransform3D:scale4],
                            nil];
    [animation setValues:frameValues];
    
    NSArray *frameTimes = [NSArray arrayWithObjects:
                           [NSNumber numberWithFloat:0.0],
                           [NSNumber numberWithFloat:0.5],
                           [NSNumber numberWithFloat:0.9],
                           [NSNumber numberWithFloat:1.0],
                           nil];
    [animation setKeyTimes:frameTimes];
    
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    animation.duration = 0.2;
    
    [AlertView.layer addAnimation:animation forKey:@"show"];
}

- (void)animateHide
{
    CAKeyframeAnimation *animation = [CAKeyframeAnimation
                                      animationWithKeyPath:@"transform"];
    
    CATransform3D scale1 = CATransform3DMakeScale(1.0, 1.0, 1);
    CATransform3D scale2 = CATransform3DMakeScale(0.5, 0.5, 1);
    CATransform3D scale3 = CATransform3DMakeScale(0.0, 0.0, 1);
    
    NSArray *frameValues = [NSArray arrayWithObjects:
                            [NSValue valueWithCATransform3D:scale1],
                            [NSValue valueWithCATransform3D:scale2],
                            [NSValue valueWithCATransform3D:scale3],
                            nil];
    [animation setValues:frameValues];
    
    NSArray *frameTimes = [NSArray arrayWithObjects:
                           [NSNumber numberWithFloat:0.0],
                           [NSNumber numberWithFloat:0.5],
                           [NSNumber numberWithFloat:0.9],
                           nil];
    [animation setKeyTimes:frameTimes];
    
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    animation.duration = 0.1;
    
    [AlertView.layer addAnimation:animation forKey:@"hide"];
    
    [self performSelector:@selector(removeFromSuperview) withObject:self afterDelay:0.105];
}


-(id)initWithTitle2:(NSString *)title message:(NSMutableAttributedString *)attributedStr delegate:(id)AlertDelegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitle:(NSString *)otherButtonTitle
{
 /*   CGRect frame;
    
    if (appDelegate.interfaceeOrientation == UIInterfaceOrientationPortrait){
        NSLog(@"appDelegate.isMSIDNVew %@",appDelegate.isMSIDNVew);
        
        frame = CGRectMake(0.0, 0.0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
    }
    else{
        frame = CGRectMake(0.0, 0.0, [[UIScreen mainScreen] bounds].size.height, [[UIScreen mainScreen] bounds].size.width);
    }
    self = [super initWithFrame:frame];
    
    NSString *fontname = [appDelegate getlocaLizationLanguage:@"fontname"];
    
    
    if (self) {
        self.delegate = AlertDelegate;
        self.alpha = 0.95;
        self.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.6];
        
        self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        
        UIImage *AlertBg = [UIImage imageNamed:@"ttt.png"];
        
        if ([AlertBg respondsToSelector:@selector(resizableImageWithCapInsets:)])
            AlertBg = [[UIImage imageNamed: @"ttt.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(40.0, 25.0, 25.0, 25.0)];
        else
            AlertBg = [[UIImage imageNamed: @"ttt.png"] stretchableImageWithLeftCapWidth: 25 topCapHeight: 40];
        
        
        UIImage *CancelBtnImg = [UIImage imageNamed:@"cancel_btn.png"];
        UIImage *OtherBtnImg = [UIImage imageNamed:@"other_btn.png"];
        
        UIImageView *AlertImgView = [[UIImageView alloc] initWithImage:AlertBg];
        
        float alert_width = AlertImgView.frame.size.width;
        float alert_height = 5.0;
        
        //add text
        UILabel *TitleLbl;
        UIScrollView *MsgScrollView;
        
        if (title)
        {
            
            TitleLbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, alert_height, alert_width-20.0, 30.0)];
            TitleLbl.adjustsFontSizeToFitWidth = YES;
            TitleLbl.font = [UIFont fontWithName:fontname size: 18.0];
            TitleLbl.textAlignment = NSTextAlignmentCenter;
            TitleLbl.minimumFontSize = 12.0;
            TitleLbl.backgroundColor = [UIColor clearColor];
            TitleLbl.textColor = [UIColor blackColor];
            TitleLbl.text = title;
            
            alert_height += TitleLbl.frame.size.height + 5.0;
        }
        else
            alert_height += 15.0;
        
        if (attributedStr)
        {
            float max_msg_height = MAX_ALERT_HEIGHT - alert_height - ((cancelButtonTitle || otherButtonTitle)?(CancelBtnImg.size.height+30.0):30.0);
            
            UILabel *MessageLbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0, alert_width-40.0, 0.0)];
            MessageLbl.numberOfLines = 0;
            MessageLbl.font = [UIFont fontWithName:fontname size: 16.0];
            MessageLbl.textAlignment = NSTextAlignmentCenter;
            MessageLbl.backgroundColor = [UIColor clearColor];
            MessageLbl.textColor = [UIColor blackColor];
            MessageLbl.attributedText = attributedStr;
            
            [MessageLbl sizeToFit];
            MessageLbl.frame = CGRectMake(10.0, 0.0, alert_width-40.0, MessageLbl.frame.size.height);
            
            while (MessageLbl.frame.size.height>max_msg_height && MessageLbl.font.pointSize>12) {
                MessageLbl.font = [UIFont systemFontOfSize:MessageLbl.font.pointSize-1];
                [MessageLbl sizeToFit];
                MessageLbl.frame = CGRectMake(10.0, 0.0, alert_width-40.0, MessageLbl.frame.size.height);
            }
            
            MsgScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(10.0, alert_height, alert_width-20.0, (MessageLbl.frame.size.height>max_msg_height)?max_msg_height:MessageLbl.frame.size.height)];
            MsgScrollView.contentSize = MessageLbl.frame.size;
            [MsgScrollView addSubview:MessageLbl];
            
            alert_height += MsgScrollView.frame.size.height + 15.0;
        }
        else
            alert_height += 15.0;
        
        //add buttons
        UIButton *CancelBtn;
        UIButton *OtherBtn;
        UILabel *lineLab;
        UILabel *cancelBtntextlable;
        UILabel *otherBtntextlable;
        
        UILabel *lineLab1  = [[UILabel alloc]initWithFrame:CGRectMake(0, alert_height + 10,alert_width,1.5)];
        lineLab1.backgroundColor = [UIColor lightGrayColor];
        
        if (cancelButtonTitle && otherButtonTitle)
        {
            float x_displ = (int)((alert_width-CancelBtnImg.size.width*2)/3.0);
            CancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(x_displ, alert_height + 13, CancelBtnImg.size.width, CancelBtnImg.size.height )];
            cancelBtntextlable = [[UILabel alloc] initWithFrame:CGRectMake(x_displ, alert_height + 13, CancelBtnImg.size.width, CancelBtnImg.size.height )];
            
            [CancelBtn setTag:1000];
            // [CancelBtn setTitle:cancelButtonTitle forState:UIControlStateNormal];
            [CancelBtn addTarget:self action:@selector(onBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            [CancelBtn.titleLabel setFont:[UIFont boldSystemFontOfSize: 15.0]];
            //cancelBtntextlable.backgroundColor = [UIColor redColor];
            cancelBtntextlable.font = [UIFont fontWithName:fontname size: 18.0];
            cancelBtntextlable.text = cancelButtonTitle;
            cancelBtntextlable.textColor = [UIColor colorWithRed:0.0078 green:0.4706 blue:1 alpha:1];
            cancelBtntextlable.textAlignment = NSTextAlignmentCenter;
            
            
            
            OtherBtn = [[UIButton alloc] initWithFrame:CGRectMake(x_displ*2+CancelBtnImg.size.width, alert_height + 13, OtherBtnImg.size.width, OtherBtnImg.size.height)];
            otherBtntextlable = [[UILabel alloc] initWithFrame:CGRectMake(x_displ*2+CancelBtnImg.size.width, alert_height + 13, OtherBtnImg.size.width, OtherBtnImg.size.height)];
            
            [OtherBtn setTag:1001];
            // [OtherBtn setTitle:otherButtonTitle forState:UIControlStateNormal];
            [OtherBtn.titleLabel setFont:[UIFont fontWithName:fontname size: 18.0]];
            //[OtherBtn setBackgroundImage:OtherBtnImg forState:UIControlStateNormal];
            [OtherBtn addTarget:self action:@selector(onBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            // OtherBtn.backgroundColor = [UIColor redColor];
            
            //otherBtntextlable.backgroundColor = [UIColor redColor];
            otherBtntextlable.font = [UIFont boldSystemFontOfSize: 20.0];
            otherBtntextlable.text = otherButtonTitle;
            otherBtntextlable.textColor =[UIColor colorWithRed:0.0078 green:0.4706 blue:1 alpha:1];
            otherBtntextlable.textAlignment = NSTextAlignmentCenter;
            
            if (IS_IPHONE_5) {
                lineLab = [[UILabel alloc]initWithFrame:CGRectMake(OtherBtn.frame.origin.x - 4, OtherBtn.frame.origin.y - 2, 1.5, OtherBtn.frame.size.height + 4)];
            }
            else{
                lineLab = [[UILabel alloc]initWithFrame:CGRectMake(OtherBtn.frame.origin.x - 4, OtherBtn.frame.origin.y, 1.5, OtherBtn.frame.size.height + 5)];
            }
            
            lineLab.backgroundColor = [UIColor lightGrayColor];
            
            alert_height += CancelBtn.frame.size.height + 15.0;
        }
        else if (cancelButtonTitle)
        {
            CancelBtn = [[UIButton alloc] initWithFrame:CGRectMake((int)((alert_width-CancelBtnImg.size.width)/2.0), alert_height + 13, CancelBtnImg.size.width, CancelBtnImg.size.height)];
            [CancelBtn setTag:1000];
            [CancelBtn setTitle:cancelButtonTitle forState:UIControlStateNormal];
            [CancelBtn.titleLabel setFont:[UIFont boldSystemFontOfSize: 20.0]];
            // [CancelBtn setBackgroundImage:CancelBtnImg forState:UIControlStateNormal];
            // CancelBtn.backgroundColor = [UIColor redColor];
            [CancelBtn addTarget:self action:@selector(onBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            alert_height += CancelBtn.frame.size.height + 15.0;
            
            
        }
        else if (otherButtonTitle)
        {
            OtherBtn = [[UIButton alloc] initWithFrame:CGRectMake((int)((alert_width-OtherBtnImg.size.width)/2.0), alert_height +13, OtherBtnImg.size.width, OtherBtnImg.size.height)];
            [OtherBtn setTag:1001];
            [OtherBtn setTitle:otherButtonTitle forState:UIControlStateNormal];
            [OtherBtn.titleLabel setFont:[UIFont boldSystemFontOfSize: 20.0]];
            //[OtherBtn setBackgroundImage:OtherBtnImg forState:UIControlStateNormal];
            // OtherBtn.backgroundColor = [UIColor redColor];
            [OtherBtn addTarget:self action:@selector(onBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            alert_height += OtherBtn.frame.size.height + 15.0;
        }
        else
            alert_height += 15.0;
        
        //add background
        if (CancelBtn) {
            [CancelBtn setTitleColor:[UIColor colorWithRed:0.0078 green:0.4706 blue:1.0 alpha:1] forState:UIControlStateNormal];
        }
        if (OtherBtn) {
            [OtherBtn setTitleColor:[UIColor colorWithRed:0.0078 green:0.4706 blue:1.0 alpha:1] forState:UIControlStateNormal];
        }
        
        
        
        if ([appDelegate.isMSIDNVew isEqualToString:@"0"]) {
            AlertView = [[UIView alloc] initWithFrame:CGRectMake((int)((self.frame.size.width-alert_width)/2.0), (int)((self.frame.size.height-alert_height)/2.0), alert_width, alert_height)];
            AlertView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        }
        else{
            AlertView = [[UIView alloc] initWithFrame:CGRectMake((int)((self.frame.size.width-alert_width)/2.0 ), (int)((self.frame.size.height-alert_height)/2.0) , alert_width, alert_height)];
        }
        
        AlertImgView.frame = AlertView.bounds;
        [AlertView addSubview:AlertImgView];
        
        [self addSubview:AlertView];
        
        if (cancelBtntextlable)
            [AlertView addSubview:cancelBtntextlable];
        if (otherBtntextlable)
            [AlertView addSubview:otherBtntextlable];
        
        if (lineLab1)
            [AlertView addSubview:lineLab1];
        
        if (TitleLbl)
            [AlertView addSubview:TitleLbl];
        
        if (MsgScrollView)
            [AlertView addSubview:MsgScrollView];
        
        if (CancelBtn)
            [AlertView addSubview:CancelBtn];
        
        if (OtherBtn)
            [AlertView addSubview:OtherBtn];
        if(lineLab)
            [AlertView addSubview:lineLab];
    }
    */
    return self;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
