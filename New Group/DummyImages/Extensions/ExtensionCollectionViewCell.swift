//
//  ExtensionCollectionViewCell.swift
//  OK
//
//  Created by Ashish on 2/26/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

extension UICollectionViewCell {
    private struct AssociatedKey {
        static var dynamicText = "we can store any kind of string matching."
    }
    
    var matchingText : String? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKey.dynamicText) as? String
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(self, &AssociatedKey.dynamicText, newValue as String?, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
}
