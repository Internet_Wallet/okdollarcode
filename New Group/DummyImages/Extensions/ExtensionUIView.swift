//
//  ExtensionUIView.swift
//  OK
//
//  Created by Ashish Kumar Singh on 10/13/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

 @IBDesignable
class CardDesignView: UIView {
        
        @IBInspectable var cornerRadius: CGFloat = 2
        
        @IBInspectable var shadowOffsetWidth: Int = 0
        @IBInspectable var shadowOffsetHeight: Int = 3
        @IBInspectable var shadowColor: UIColor? = UIColor.black
        @IBInspectable var shadowOpacity: Float = 0.5
        @IBInspectable var maskBound: Bool = false
        @IBInspectable var borderColor: UIColor = UIColor.white
        @IBInspectable var borderWidth: CGFloat = 0.00

    override func layoutSubviews() {
            layer.cornerRadius = cornerRadius
            let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
            
            layer.borderColor = borderColor.cgColor
            layer.borderWidth = borderWidth

            layer.masksToBounds = maskBound
            layer.shadowColor = shadowColor?.cgColor
            layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
            layer.shadowOpacity = shadowOpacity
            layer.shadowPath = shadowPath.cgPath
        }
     
}

extension CGFloat {
    var half: CGFloat { return self / 2 }
    var double: CGFloat { return self * 2 }
}

extension UIView {
    
        var ending: CGPoint { return CGPoint(x: frame.origin.x + frame.width, y: frame.origin.y + frame.height) }
        var viewWidth: CGFloat { return frame.width }
        var viewHeight: CGFloat { return frame.height }
    

    
    func drawShadowLayer(radius: CGFloat,opacity: Float) {
        self.layer.shadowColor    = UIColor.gray.cgColor
        self.layer.shadowOpacity  = opacity
        self.layer.shadowOffset   = CGSize.zero
        self.layer.shadowRadius   = radius
    }
    
    func layerShadow(radius: CGFloat = 4, offsetValue: CGSize = CGSize.zero, opacity: Float = 1) {
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offsetValue
        self.layer.shadowRadius = radius
    }
    
    func toImage() -> UIImage {
        
        var image :UIImage? = nil
        let bounds = self.bounds
        let size = bounds.size
        let scale = UIScreen.main.scale
        
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        
        drawHierarchy(in: bounds, afterScreenUpdates: true)
        image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
    
    func cornerEdge(radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func setBottomLine(borderColor: UIColor) {
        
        //   self.borderStyle = UITextBorderStyle.none
        self.backgroundColor = UIColor.clear
        
        let borderLine = UIView()
        let height = 1.0
        borderLine.frame = CGRect(x: 0, y: Double(self.frame.height) - height, width: Double(self.frame.width), height: height)
        
        borderLine.backgroundColor = borderColor
        self.addSubview(borderLine)
    }
    
    func makeCircle() {
        self.layer.cornerRadius = self.frame.size.width/2
        self.clipsToBounds = true
    }
    
    func setShadowToLabel(thisView: UILabel) {
        thisView.layer.shadowColor = UIColor.lightGray.cgColor
        thisView.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        thisView.layer.shadowOpacity = 1.0
        thisView.layer.shadowRadius = 4
        thisView.layer.masksToBounds = true
        thisView.layer.cornerRadius = 2.0
    }
    
    func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2) {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        
        animation.toValue = toValue
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = kCAFillModeForwards
        
        self.layer.add(animation, forKey: nil)
    }
    
    func setShadowToView(view: UIView) {
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 4
    }
    
    func setShadowToView(view: UIView, color: UIColor) {
        view.layer.shadowColor = color.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 4
    }
    
    func setShadowToButton(view: UIButton, color: UIColor) {
        view.layer.shadowColor = color.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 4
    }
    
    func layerToImage(layer: CALayer) -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: layer.frame.size)
        let imageLocal = renderer.image(actions: { context in
            layer.render(in: context.cgContext)
        })
        
        //        UIGraphicsBeginImageContext(bounds.size)
        //        if let context = UIGraphicsGetCurrentContext() {
        //            self.layer.render(in: context)
        //            image = UIGraphicsGetImageFromCurrentImageContext()
        //        }
        UIGraphicsEndImageContext()
        return imageLocal
    }
    
    var snapshot : UIImage? {
        //var image: UIImage? = nil
        guard let layer = UIApplication.shared.keyWindow?.layer else { return nil }
        return layerToImage(layer: layer)
    }

    var snapshotOfCustomeView: UIImage {
        return layerToImage(layer: self.layer)
    }
}

public extension UIView {
    
    public static func newInstance(xibName :String, bundle :Bundle? = nil) -> Self? {
        return _newInstance(xibName: xibName, bundle: bundle ?? Bundle.main)
    }
    
    private static func _newInstance<T>(xibName :String, bundle :Bundle) -> T? {
        //
        return bundle.loadNibNamed(xibName, owner: self, options: nil)?.last as? T
    }
}

