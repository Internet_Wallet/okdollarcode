//
//  Extensions.swift
//  OK
//
//  Created by Badru on 10/31/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//
import Foundation
import UIKit

extension UIView {
     
    
    func dropShadow(scale: Bool = true) {
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 1
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }

}

extension UIFont {
    convenience init(setLabelFont: CGFloat) {
        self.init(name: "Zawgyi-One", size: setLabelFont)!
    }
}

