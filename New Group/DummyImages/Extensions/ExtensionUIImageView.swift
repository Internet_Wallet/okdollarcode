//
//  ExtensionUIImageView.swift
//  OK
//
//  Created by palnar on 27/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import Foundation


extension UIImageView {
    func setRounded() {
        self.layer.borderWidth = 1
        self.layer.masksToBounds = false
        self.layer.borderColor = UIColor.init(red: 146/255, green: 146/255, blue: 146/255, alpha: 1.0) .cgColor
        self.layer.cornerRadius = self.frame.height/2
        self.clipsToBounds = true
    }
}
