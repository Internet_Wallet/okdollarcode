//
//  UserProfileModel.swift
//  OK
//
//  Created by Vinod's MacBookPro on 10/22/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreData


public enum AgentType {
    case user
    case merchant
    case agent
    case advancemerchant
    case dummymerchant
    case oneStopMart
    
    var intValue: Int {
        switch self {
        case .user:
            return 6
        case .merchant:
            return 2
        case .agent:
            return 1
        case .advancemerchant:
            return 4
        case .dummymerchant:
            return 5
        case .oneStopMart:
            return 8
        }
    }
    //{ SuperAdmin = 0, Agent, Customer, Admin, AdvMer, Dummy, Subscriber, OkDollarBranch, OneStopMart }
}

final class User {
    
    private init() { }
    
    static let shared = User()
    
    // MARK: Local Variable
    var profile            = Dictionary<String,Any>()
    var bankDetails        = Dictionary<String,Any>()
    var closeDay           = Array<Dictionary<String,Any>>()
    var questionCode       = Dictionary<String,Any>()
    //    var msnidDetails       = [Dictionary<String,Any>]()
    var notificationDetail = Dictionary<String,Any>()
    
    var msnidDetails       = [MsnIdDetail]()
    var msnidDetailsModel:MsnIdDetailViewModel!

    func wrapModel(dic: Dictionary<String,Any>) {
        
        if let bank = dic["BankDetails"] as? Array<Dictionary<String,Any>>, bank.count > 0 {
            if bank.indices.contains(0){
                self.bankDetails = bank.first!
            }
        }
        
        if let close = dic["ClosedDays"] as? Array<Dictionary<String,Any>> , close.count > 0{
            for day in close {
                self.closeDay.append(day)
            }
            var dayStr = ""
            for dic in closeDay {
                dayStr = "\(dayStr)\(dic["ClosedDays"] as? String ?? ""),"
            }
            dayStr.removeLast()
          closeDayModel.share.wrapUserData(str: dayStr)
        }
        
        if let question = dic["QuestionCode"] as? Array<Dictionary<String,Any>>, question.count > 0 {
            self.questionCode = question.first!
            UserModel.wrapSecurityQuestionData(dict: self.questionCode)
        }
        if let msn = dic["MsisdnDetails"] as? Array<Dictionary<String,Any>>, msn.count > 0 {
            //            self.msnidDetails = msn
            //            self.msnidDetails.removeAll()
            //            self.msnidDetails = MsnIdDetail.wrapMsnIdDetails(array: msn)
            // archiveObjectWithArray(array: self.msnidDetails, withKey: "msnidDetails")
            self.msnidDetailsModel = MsnIdDetailViewModel(data: msn)
            self.msnidDetails = self.msnidDetailsModel.getMsnIDDetailData
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: self.msnidDetails)
            userDef.set(encodedData, forKey: "msnidDetails")
        }
        if let notify = dic["NotificationDetails"] as? Array<Dictionary<String,Any>> , notify.count > 0{
            if notify.indices.contains(0){
                self.notificationDetail = notify.first!
            }
            
        }
        
        if let profileDetail = dic["ProfileDetails"] as? Array<Dictionary<String,Any>>, profileDetail.count > 0 {
            if profileDetail.indices.contains(0){
                self.profile = profileDetail.first!
            }
            
            UserModel.wrapUserData(dict: self.profile)
        }
    }
    
}

class MsnIdDetail : NSObject, NSCoding {
    var countryCode:String = ""
    var mobileNumber:String = ""
    
    init(countryCode: String, mobileNumber: String) {
        self.countryCode = countryCode
        self.mobileNumber = mobileNumber
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(countryCode, forKey: "Msisdn")
        aCoder.encode(mobileNumber, forKey: "CountryCode")
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let countryCode = aDecoder.decodeObject(forKey: "Msisdn") as! String
        let mobileNumber = aDecoder.decodeObject(forKey: "CountryCode") as! String
        self.init(countryCode: countryCode, mobileNumber: mobileNumber)
    }
    
}

class MsnIdDetailViewModel {
    var msnIdDetailObj = [MsnIdDetail]()
    //    static let shared = MsnIdDetail()
    
    init(data:[Dictionary<String, Any>]) {
        let arrayData = data as Array<Dictionary<String,Any>>
        msnIdDetailObj =  arrayData.map({(value: Dictionary<String, Any>) -> MsnIdDetail in
            return  MsnIdDetail(countryCode: (value["Msisdn"] as? String)!, mobileNumber: (value["Msisdn"] as? String)!)
        })
    }
    
    var getMsnIDDetailData:Array<MsnIdDetail>{
        return msnIdDetailObj
    }
    
}


//class MsnIdDetail {
//
//    var countryCode   =  ""
//    var mobileNumber  =  ""
//    static let shared = MsnIdDetail()
//
//    class func wrapMsnIdDetails(array: [Dictionary<String, Any>]) -> [MsnIdDetail]{
//        var myData = [MsnIdDetail]()
//        var i = 0
//        for dict in array {
//            if let title = dict["Msisdn"] as? String {
//                self.shared.mobileNumber = title
//            }
//            if let title = dict["CountryCode"] as? String {
//                self.shared.countryCode  = title
//            }
//            myData.append(self.shared)
//        }
//        return myData
//        // archiveObjectWithArray(array: myData, withKey: keyMsnIdModel)
//    }
//
//}


class closeDayModel : NSObject {
 
    var closeDays = ""
    static let share = closeDayModel()
    func wrapUserData(str: String) {
        UserLoginDataManager.sharedInstance.insertCloseDaysIntoDB(closeStr: str)
    }
    
  class func wrapFromDB() {
        if let unwrappedProfileDay = UserLoginDataManager.sharedInstance.fetchUProfileDaysRecord() {
         self.share.closeDays = unwrappedProfileDay.closedDays.safelyWrappingString()
        }
    }
}


class UserModel : NSObject {
    
    var accountID = ""
    var agentID   = ""
    
    var mobileNo     = ""
    var name         = ""
    var simID        = ""
    var agentType  : AgentType   = .user
    var fatherName   = ""
    var dob          = ""
    var nrc          = ""
    var idType       = ""
    var pass         = ""
    
    var ageNow       = ""
    
    var businessType = ""
    var businessCate = ""
    var businessSubCate = ""
    var businessOutlet = ""
    var lat          = geoLocManager.currentLongitude ?? "0.0"
    var long         = geoLocManager.currentLatitude ?? "0.0"
    var proPic       = ""
    var signPic      = ""
    var email        = ""
    var addressType  = ""
    var businessName = ""
    var country      = ""
    var countryCode  = ""
    var deviceId     = ""
    
    //    var isAgent : Bool = false
    
    var devicePatternORPass = 0
    
    var pin = ""
    var securityQuestionCode = ""
    var securityAnswer = ""
    var securityQuestion = Dictionary<String,AnyObject>()
    
    //Vinod added
    
    var accountType   = ""
    var address1      = ""
    var address2      = ""
    var appID         = ""
    var authCode      = ""
    var authCodeStatus = ""
    var businessIdPhoto1 = ""
    var businessIdPhoto2 = ""
    var car           = ""
    var carType       = ""
    var cellTowerID   = ""
    var closeTime     = ""
    var codeAlternate = ""
    var codeRecommended = ""
    var countryOfCitizen = ""
    var createdDate   = ""
    var encryptedKey  = ""
    var fbEmailId     = ""
    var website     = ""
    var floorNumber   = ""
    var gcmId         = ""
    var gender        = ""
    var houseBlockNo  = ""
    var houseName     = ""
    var imei          = ""
    var idExpDate     = ""
    var idPhoto       = ""
    var idPhoto1      = ""
    var isNewStreet   = ""
    var street   = ""
    var language      = ""
    var msid          = ""
    var osType        = ""
    var openTime      = ""
    var osVersion     = ""
    var parentAccount = ""
    var passwordType  = ""
    var phoneModel    = ""
    var phoneNumber   = ""
    var phonebrand    = ""
    var recommendedBy = ""
    var registrationStatus = ""
    var roomNumber    = ""
    var state         = ""
    var township      = ""
    var villageName   = ""
    var paymentNumber_AdvanceMerchant = ""
    var formattedNumber = ""
    //------------------------------
    
    var b_AddressType = ""
    var b_Division = ""
    var b_Township = ""
    var b_City = ""
    var b_Village = ""
    var b_Street = ""
    var b_HouseNumber = ""
    var b_FloorNumber = ""
    var b_RoomNumber = ""
    var b_HouseBlockNumber = ""
    var ownershipType = ""
    var logoImages = ""
    var kickBackNumber = ""
    
    static let shared = UserModel()
    
    private struct agentTypeStringStructure {
        let subscriber = "SUBSBR"
        let merchant = "MER"
        let agent = "AGENT"
        let advanceMerchant = "ADVMER"
        let dummy = "DUMMY"
        let oneStop = "ONESTOP"
    }

    
    class func wrapSecurityQuestionData(dict: Dictionary<String,Any>) {
        let obj = dict
        
        self.shared.securityQuestionCode     = obj["QuestionCode"] as! String
        self.shared.securityQuestion       = VillageManager.shared.data.securityQ
        self.shared.securityAnswer      = obj["SecurityAnswer"] as! String
        archiveObjectWith(dict: dict, withKey: keySectQuestionModel)
        
    }
    
    class func wrapPin(pin: String) {
        self.shared.pin = ""
    }
    
    class func wrapUserData(dict: Dictionary<String,Any>) {
        //Save record into core data
        UserLoginDataManager.sharedInstance.insertUserProfileDetailsIntoDB(dict: dict)
    }
    
    class func clearAll() {
        self.shared.accountID    = ""
        self.shared.agentID      = ""
        self.shared.mobileNo     = ""
        self.shared.name         = ""
        self.shared.simID        = ""
        self.shared.fatherName   = ""
        self.shared.dob          = ""
        self.shared.nrc          = ""
        self.shared.idType       = ""
        self.shared.businessType = ""
        self.shared.businessCate = ""
        
        self.shared.lat          = ""
        self.shared.long         = ""
        self.shared.proPic       = ""
        
        self.shared.signPic      = ""
        self.shared.email        = ""
        self.shared.addressType  = ""
        
          self.shared.businessName = ""
       
        self.shared.businessOutlet = ""
        self.shared.country      = ""
        self.shared.countryCode  = ""

        
        self.shared.codeRecommended  = ""
        self.shared.deviceId         = ""
        self.shared.accountType      = ""
        self.shared.address1         = ""
        self.shared.address2         = ""
        self.shared.appID            = ""
        self.shared.authCode         = ""
        self.shared.authCodeStatus   = ""
        self.shared.businessIdPhoto1 = ""
        self.shared.businessIdPhoto2 = ""
        self.shared.car              = ""
        self.shared.carType          = ""
        self.shared.cellTowerID      = ""
        self.shared.closeTime        = ""
        self.shared.codeAlternate    = ""
        self.shared.codeRecommended  = ""
        self.shared.countryOfCitizen = ""
        self.shared.createdDate      = ""
        self.shared.encryptedKey     = ""
        self.shared.fbEmailId        = ""
        self.shared.floorNumber      = ""
        self.shared.gcmId            = ""
        self.shared.gender           = ""
        
        self.shared.houseBlockNo     = ""
        self.shared.houseName        = ""
        self.shared.imei             = ""
        self.shared.idExpDate        = ""
        
        self.shared.idPhoto          = ""
        self.shared.idPhoto1         = ""
        
        self.shared.isNewStreet      = ""
        
        self.shared.language         = ""
    
        self.shared.msid             = ""
        
        self.shared.osType           = ""
        
        self.shared.openTime         = ""
        self.shared.osVersion        = ""
        self.shared.parentAccount    = ""
        
        self.shared.passwordType     = ""
        self.shared.phoneModel       = ""
        self.shared.phoneNumber      = ""
        self.shared.phonebrand       = ""
        self.shared.recommendedBy    = ""
        self.shared.registrationStatus = ""
        self.shared.roomNumber       = ""
        self.shared.state            = ""
        self.shared.township         = ""
        self.shared.villageName      = ""
        self.shared.ageNow  = ""
        self.shared.checkAgentORNot(agenttype: "")
        
        self.shared.b_AddressType = ""
        self.shared.b_Division = ""
        self.shared.b_Township = ""
        self.shared.b_City = ""
        self.shared.b_Village = ""
        self.shared.b_Street = ""
        self.shared.b_HouseNumber = ""
        self.shared.b_FloorNumber = ""
        self.shared.b_RoomNumber = ""
        self.shared.b_HouseBlockNumber = ""
        self.shared.ownershipType = ""
        self.shared.logoImages = ""
        self.shared.kickBackNumber = ""
    }
    
    class  func wrapFromDatabase() {
        
        if let unwrappedProfileList = UserLoginDataManager.sharedInstance.fetchUserProfileDetailsRecord() {
            self.shared.accountID    = unwrappedProfileList.accountID.safelyWrappingString()
            self.shared.agentID      = self.shared.accountID
            self.shared.mobileNo     = unwrappedProfileList.mobileNo.safelyWrappingString()
            self.shared.name         = unwrappedProfileList.name.safelyWrappingString()
            self.shared.simID        = unwrappedProfileList.simID.safelyWrappingString()
            self.shared.fatherName   = unwrappedProfileList.fatherName.safelyWrappingString()
            self.shared.dob          = unwrappedProfileList.dob.safelyWrappingString()
            self.shared.nrc          = unwrappedProfileList.nrc.safelyWrappingString()
            self.shared.idType       = unwrappedProfileList.idType.safelyWrappingString()
            self.shared.businessType = unwrappedProfileList.businessType.safelyWrappingString()
            self.shared.businessCate = unwrappedProfileList.businessCategory.safelyWrappingString()
            
            self.shared.lat          = unwrappedProfileList.latitude.safelyWrappingString()
            self.shared.long         = unwrappedProfileList.longitude.safelyWrappingString()
            self.shared.proPic       = unwrappedProfileList.profilePic.safelyWrappingString()
            
            self.shared.signPic      = unwrappedProfileList.signaturePic.safelyWrappingString()
            self.shared.email        = unwrappedProfileList.emailId.safelyWrappingString()
            self.shared.addressType  = unwrappedProfileList.addressType.safelyWrappingString()
            
            if unwrappedProfileList.agentType.safelyWrappingString() == "SUBSBR" {
              self.shared.businessName = ""
            }else {
                 self.shared.businessName = unwrappedProfileList.businessName.safelyWrappingString()
            }
           
            self.shared.businessOutlet = unwrappedProfileList.businessOutlet.safelyWrappingString()
            self.shared.country      = unwrappedProfileList.country.safelyWrappingString()
            self.shared.countryCode  = unwrappedProfileList.countryCode.safelyWrappingString()

            
            self.shared.codeRecommended  = unwrappedProfileList.codeRecommended.safelyWrappingString()
            self.shared.deviceId         = unwrappedProfileList.deviceId.safelyWrappingString()
            self.shared.accountType      = unwrappedProfileList.accountType.safelyWrappingString()//(unwrappedProfileList("AccountType") as! NSNumber).stringValue
            self.shared.address1         = unwrappedProfileList.address1.safelyWrappingString()
            self.shared.address2         = unwrappedProfileList.address2.safelyWrappingString()
            self.shared.appID            = unwrappedProfileList.appID.safelyWrappingString()
            self.shared.authCode         = unwrappedProfileList.authCode.safelyWrappingString()
            self.shared.authCodeStatus   = unwrappedProfileList.authCodeStatus.safelyWrappingString()//self.covertBoolToString(value: unwrappedProfileList("AuthCodeStatus") as! Bool)
            self.shared.businessIdPhoto1 = unwrappedProfileList.businessIdPhoto1.safelyWrappingString()
            self.shared.businessIdPhoto2 = unwrappedProfileList.businessIdPhoto2.safelyWrappingString()
            self.shared.car              = unwrappedProfileList.car.safelyWrappingString()
            self.shared.carType          = unwrappedProfileList.carType.safelyWrappingString()//(unwrappedProfileList("CarType") as! NSNumber).stringValue
            self.shared.cellTowerID      = unwrappedProfileList.cellTowerID.safelyWrappingString()
            self.shared.closeTime        = unwrappedProfileList.closeTime.safelyWrappingString()
            self.shared.codeAlternate    = unwrappedProfileList.codeAlternate.safelyWrappingString()
            self.shared.codeRecommended  = unwrappedProfileList.codeRecommended.safelyWrappingString()
            self.shared.countryOfCitizen = unwrappedProfileList.countryOfCitizen.safelyWrappingString()
            self.shared.createdDate      = unwrappedProfileList.createdDate.safelyWrappingString()
            self.shared.encryptedKey     = unwrappedProfileList.encryptedKey.safelyWrappingString()
            self.shared.fbEmailId        = unwrappedProfileList.fbEmailId.safelyWrappingString()
            self.shared.floorNumber      = unwrappedProfileList.floorNumber.safelyWrappingString()
            self.shared.gcmId            = unwrappedProfileList.gcMID.safelyWrappingString()
            self.shared.gender           = unwrappedProfileList.gender.safelyWrappingString()//self.covertBoolToString(value: unwrappedProfileList("Gender") as! Bool)
            
            self.shared.houseBlockNo     = unwrappedProfileList.houseBlockNo.safelyWrappingString()
            self.shared.houseName        = unwrappedProfileList.houseName.safelyWrappingString()
            self.shared.imei             = unwrappedProfileList.imei.safelyWrappingString()
            self.shared.idExpDate        = unwrappedProfileList.idExpDate.safelyWrappingString()
            
            self.shared.idPhoto          = unwrappedProfileList.idPhoto.safelyWrappingString()
            self.shared.idPhoto1         = unwrappedProfileList.idPhoto1.safelyWrappingString()
            
            self.shared.isNewStreet      = unwrappedProfileList.isNewStreet.safelyWrappingString()//self.covertBoolToString(value: unwrappedProfileList("IsNewStreet") as! Bool)
            
            if unwrappedProfileList.language.safelyWrappingString() == "" {
                self.shared.language         = "en"
            }else {
                self.shared.language         = unwrappedProfileList.language.safelyWrappingString().lowercased()
            }
      
            self.shared.msid             = unwrappedProfileList.msid.safelyWrappingString()
            
            self.shared.osType           = unwrappedProfileList.osType.safelyWrappingString()//(unwrappedProfileList("OSType") as! NSNumber).stringValue
            
            self.shared.openTime         = unwrappedProfileList.openTime.safelyWrappingString()
            self.shared.osVersion        = unwrappedProfileList.osVersion.safelyWrappingString()
            self.shared.parentAccount    = unwrappedProfileList.parentAccount.safelyWrappingString()
            
            self.shared.passwordType     = unwrappedProfileList.passwordType.safelyWrappingString()//(unwrappedProfileList("PasswordType") as! NSNumber).stringValue
            
            self.shared.phoneModel       = unwrappedProfileList.phoneModel.safelyWrappingString()
            self.shared.phoneNumber      = unwrappedProfileList.phoneNumber.safelyWrappingString()
            self.shared.phonebrand       = unwrappedProfileList.phoneBrand.safelyWrappingString()
            self.shared.recommendedBy    = unwrappedProfileList.recommendedBy.safelyWrappingString()
            
            self.shared.registrationStatus = unwrappedProfileList.registrationStatus.safelyWrappingString()//(unwrappedProfileList("RegistrationStatus") as! NSNumber).stringValue
            self.shared.roomNumber       = unwrappedProfileList.roomNumber.safelyWrappingString()//unwrappedProfileList("RoomNumber") as! String
            self.shared.state            = unwrappedProfileList.state.safelyWrappingString()//unwrappedProfileList("State") as! String
            self.shared.township         = unwrappedProfileList.township.safelyWrappingString()//unwrappedProfileList("Township") as! String
            self.shared.villageName      = unwrappedProfileList.villageName.safelyWrappingString()//unwrappedProfileList("VillageName") as! String
            
            self.shared.ageNow  = self.shared.calculateCurrentAge(self.shared.dob).safelyWrappingString()
            
            self.shared.checkAgentORNot(agenttype: unwrappedProfileList.agentType.safelyWrappingString())
            
            self.shared.formattedNumber = self.shared.wrapFormattedNumber()
            
            self.shared.updatePasswordType(passType: self.shared.passwordType)
            self.shared.b_AddressType = unwrappedProfileList.b_AddressType.safelyWrappingString()
            self.shared.b_Division = unwrappedProfileList.b_Division.safelyWrappingString()
            self.shared.b_Township = unwrappedProfileList.b_Township.safelyWrappingString()
            self.shared.b_City = unwrappedProfileList.b_City.safelyWrappingString()
            self.shared.b_Village = unwrappedProfileList.b_Village.safelyWrappingString()
            self.shared.b_Street = unwrappedProfileList.b_Street.safelyWrappingString()
            self.shared.b_HouseNumber = unwrappedProfileList.b_HouseNumber.safelyWrappingString()
            self.shared.b_FloorNumber = unwrappedProfileList.b_FloorNumber.safelyWrappingString()
            self.shared.b_RoomNumber = unwrappedProfileList.b_RoomNumber.safelyWrappingString()
            self.shared.b_HouseBlockNumber = unwrappedProfileList.b_HouseBlockNumber.safelyWrappingString()
            self.shared.ownershipType = unwrappedProfileList.ownershipType.safelyWrappingString()
            self.shared.logoImages = unwrappedProfileList.logoImages.safelyWrappingString()
            self.shared.kickBackNumber = unwrappedProfileList.kickBackNumber.safelyWrappingString()
        }
        
        
        
        // msid,udid, otp, cellid, simid
        
    }
    
    
    
    
    
    
    func updatePasswordType(passType: String) {
        if passType == "-1" {
            userDef.setValue(true, forKey: "passwordLoginType")
        } else if passType == "0" {
            userDef.setValue(false, forKey: "passwordLoginType")
        } else if passType == "1" {
            userDef.setValue(true, forKey: "passwordLoginType")
        } else {
            userDef.setValue(false, forKey: "passwordLoginType")
        }
        userDef.synchronize()
        
        // 1 -> pattern
        // 0 -> password
    }
    
    
    
    
    
    
    func wrapFormattedNumber() -> String {
        var agentCode = UserModel.shared.mobileNo
        if agentCode.count > 2 {
            agentCode.remove(at: String.Index.init(encodedOffset: 0))
            agentCode.remove(at: String.Index.init(encodedOffset: 0))
            agentCode.insert("+", at: String.Index.init(encodedOffset: 0))
            
            let countryDetails = identifyCountry(withPhoneNumber: agentCode)
            
            if agentCode.hasPrefix(countryDetails.countryCode) {
                
                agentCode = agentCode.replacingOccurrences(of: countryDetails.countryCode, with: "0")
                return agentCode
                
            } else {
                return ""
            }
            
        } else {
            return ""
        }
    }
    
    func formattingDateAndTime() -> (date: String, time: String) {
        let login = UserLogin.shared.lastLogin
        var date  = login.components(separatedBy: " ").first ?? ""
        let time  = login.components(separatedBy: " ").last ?? ""
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.calendar = Calendar(identifier: .gregorian)
        dateFormatterGet.dateFormat = "MM-dd-yyyy HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.calendar = Calendar(identifier: .gregorian)
        dateFormatterPrint.dateFormat = "EEEE, MMM d, yyyy"
        
        let lDate = dateFormatterGet.date(from: login)
        date = dateFormatterPrint.string(from: lDate ?? Date.init())
        
        return (date,time)
    }
    
    func checkAgentORNot(agenttype: String) {
       
        if agenttype == "SUBSBR" {
            self.agentType = .user
        }else if agenttype == "MER" {
            self.agentType = .merchant
        }else if agenttype == "AGENT" {
            self.agentType = .agent
        }else if agenttype == "ADVMER" {
            self.agentType = .advancemerchant
        }else if agenttype == "DUMMY" {
            self.agentType = .dummymerchant
        } else {
            self.agentType = .user
        }
    }
    
    func agentServiceTypeString() -> String {
        
        let string = agentTypeStringStructure()
        
        switch self.agentType {
        case .user:
            return string.subscriber
        case .merchant:
            return string.merchant
        case .agent:
            return string.agent
        case .advancemerchant:
            return string.advanceMerchant
        case .dummymerchant:
            return string.dummy
        case .oneStopMart:
            return string.oneStop
        }
    }
    
    func checkPatternORPass(_key: Int) {
        self.devicePatternORPass = _key
        
    }
    
    func covertBoolToString(value: Bool) -> String {
        return value ? "1" : "0"
    }
    
    func covertGenderBoolToString(value: Bool) -> String {
        return value ? "M" : "F"
    }
    
    func calculateCurrentAge(_ dob: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "dd-MMM-yyyy" //Your date format
        let date = dateFormatter.date(from: dob) //according to date format your date string
        let ageValue = date?.age
        let stringAgeValue : String? = "\(ageValue ?? 0)"
        return stringAgeValue
    }

    
    func wrap_AdvanceMerchantPaymentNumber(value: String) {
        self.paymentNumber_AdvanceMerchant = value
    }
}

class UserLogin {
    
    var transId = ""
    var token   = ""
    var aName   = ""
    var atype   = ""
    var aLevel  = ""
    
    var lastLogin = ""
    
    var walletBal = ""
    var telco     = ""
    //New Data
    var authCode = ""
    var transactionTime = ""
    var resultDescription     = ""
    var needToUpdateProfile     = ""
    var resultCode     = ""
    var buinessCategory     = ""
    var bonusPoint     = ""
    var kickBack     = ""
    var authCodeStatus     = ""
    var registrationStatus = ""
    
    var loginSessionExpired : Bool = true {
        didSet {
            if loginSessionExpired {
                NotificationCenter.default.post(name: .loginTimeOut,
                                                object: nil)
                UserLogin.shared.seconds = 0
                UserLogin.shared.sessionTimer.invalidate()
                UserDefaults.standard.set(true, forKey: "isLogOut")
            }
        }
    }
    
    private var sessionTimer : Timer = Timer()
    private var seconds : Int = 0

    static let shared = UserLogin()
    
    class func wrapUserData(dict: Dictionary<String,Any>) {
        //Add records into database
        UserLoginDataManager.sharedInstance.insertUserLoginDetailsIntoDB(dict: dict)
        UserLogin.shared.loginSessionExpired = false
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateTBMoreProfile"), object: nil)
    }
    
    class func wrapLoginFromDB() {
        if let unwrappedLoginDetails = UserLoginDataManager.sharedInstance.fetchUserLoginDetailsRecord() {
            // login parameters
            self.shared.transId = unwrappedLoginDetails.transactionId.safelyWrappingString()
            self.shared.token   = unwrappedLoginDetails.secureToken.safelyWrappingString()
            self.shared.aName   = unwrappedLoginDetails.agentName.safelyWrappingString()
            self.shared.atype   = unwrappedLoginDetails.agentType.safelyWrappingString()
            self.shared.aLevel  = unwrappedLoginDetails.agentLevel.safelyWrappingString()
            
            self.shared.lastLogin = unwrappedLoginDetails.lastLogin.safelyWrappingString()
            self.shared.walletBal = unwrappedLoginDetails.walletBalance.safelyWrappingString()
            self.shared.telco     = unwrappedLoginDetails.telecoNumber.safelyWrappingString()
            self.shared.authCode  = unwrappedLoginDetails.authCode.safelyWrappingString()
            
            self.shared.transactionTime     = unwrappedLoginDetails.transactionTime.safelyWrappingString()
            self.shared.resultDescription   = unwrappedLoginDetails.resultDescription.safelyWrappingString()
            self.shared.needToUpdateProfile = unwrappedLoginDetails.needToUpdateProfile ? "1" : "0"
            self.shared.resultCode          = unwrappedLoginDetails.resultCode ? "1" : "0"
            self.shared.buinessCategory     = unwrappedLoginDetails.buinessCategory.safelyWrappingString()
            self.shared.bonusPoint          = unwrappedLoginDetails.bonusPoint.safelyWrappingString()
            self.shared.kickBack            = unwrappedLoginDetails.kickBack ? "1" : "0"
            self.shared.authCodeStatus      = unwrappedLoginDetails.authCodeStatus ? "1" : "0"
            self.shared.registrationStatus  = unwrappedLoginDetails.registrationStatus ? "1" : "0"
        }
    }
    
    func sessionTimerStart() {
        println_debug("**************** ---  Login Session Started --- *******************************")
        UserLogin.shared.sessionTimer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        UserLogin.shared.seconds = UserLogin.shared.seconds + 1
        if UserLogin.shared.seconds == 300 {
            println_debug("**************** ---  Login Session Expired --- *******************************")
            UserLogin.shared.loginSessionExpired = true
            UserLogin.shared.seconds = 0
            UserLogin.shared.sessionTimer.invalidate()
        }

    }

    
    class func updateWalletBalanceInDB(_ balance: String) {
        
    }
}


func archiveObjectWith(dict: Dictionary<String,Any>,withKey key: String){
    let encode = NSKeyedArchiver.archivedData(withRootObject: dict)
    userDef.set(encode, forKey: key)
    userDef.synchronize()
}

func unArchiveObjectWith(withKey key: String) -> Dictionary<String,Any>? {
    if let data = userDef.object(forKey: key) as? Data {
        let decoder = NSKeyedUnarchiver.unarchiveObject(with: data)
        if let dict = decoder as? Dictionary<String,Any> {
            return dict
        }else {
            return nil
        }
    } else {
        return nil
    }
    
}

func unArchiveObjectWithArray(withKey key: String) -> [Any]? {
    if let data = userDef.object(forKey: key) as? Data {
        let decoder = NSKeyedUnarchiver.unarchiveObject(with: data)
        if let array =  decoder as? [Any] {
            if let msnIdArr = array as? [MsnIdDetail] {
                return msnIdArr
            }
        }
    }
    
    return nil
}

protocol timerDelegate {
    
    func timerIntrupCall()
}

class TimerModel: NSObject {
    
    var timerDelegate :timerDelegate?
    static let sharedTimer: TimerModel = {
       let timer = TimerModel()
        return timer
    }()

    var internalTimer: Timer?

    func startTimer(withInterval interval: Double) {
        if internalTimer != nil {
            internalTimer?.invalidate()
        }
        internalTimer = Timer.scheduledTimer(timeInterval: interval, target: self, selector: #selector(doJob), userInfo: nil, repeats: true)
    }

    func pauseTimer() {
        guard internalTimer != nil else {
            print("No timer active, start the timer before you stop it.")
            return
        }
        internalTimer?.invalidate()
    }

    func stopTimer() {
        guard internalTimer != nil else {
            print("No timer active, start the timer before you stop it.")
            return
        }
        internalTimer?.invalidate()
    }

    @objc func doJob() {
        self.timerDelegate?.timerIntrupCall()
    }
    
}
