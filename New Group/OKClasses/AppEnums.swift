//
//  AppEnums.swift
//  OK
//
//  Created by Uma Rajendran on 10/30/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import Foundation
import UIKit

//////////////////////////// -------- Map Related Enums ------------- ////////////////////////////////


public enum NearByView {
    case merchantView
    case okAgentsView
    case okOfficesView
    case okAllView

}

public enum FromView {
    case promotion
    case cashInOut
}

public enum UIType {
    case mapView
    case listView
}

public enum Location: Int {
    case byDivision = 0
    case byTownship
    case byCurrentLocation
    case bySearch
    case byCategory
}

public enum CashInOutLocation: Int {
    case byTownship
    case byCurLocation
}


public enum CashInOutType: Int {
    case cashin_alllist = 0
    case cashin_allbanks
    case cashin_allagents
    case cashin_alloffices
    case cashin_allmyanmarpost
    case cashout_alllist
    case cashout_allbanks
    case cashout_allagents
    case cashout_alloffices
    case cashout_allmyanmarpost
}

public enum CashType: Int {
    case cashin = 0
    case cashout
}

public enum CashUIType {
    case cashAgentView
    case cashOfficeView
}

//////////////////////////// -------- Map Related Enums ------------- ////////////////////////////////




//////////////////////////// -------- Send Money to Bank ------------- ////////////////////////////////

public enum IdProofType: Int {
    case id_nrc = 0
    case id_passport
}

public enum AccountView {
    case myacc
    case otheracc
}

public enum MyAccountView {
    case myacc_addbank
    case myacc_existingbank
}

public enum OtherAccountView {
    case otheracc_addbank
    case otheracc_existingbank
}

public enum BankItems : Int {
    case bank_accountholdername = 0
    case bank_accountidproof
    case bank_amount
    case bank_bankname
    case bank_accounttype
    case bank_accountnumber
    case bank_contactnumber
    case bank_remarks
    case bank_emailid
}

public enum CharacterType {
    case numeric
    case alpha
    case hifen
}


//////////////////////////// -------- Send Money to Bank ------------- ////////////////////////////////



/////////////////////////// --------- Toast ---------------- ////////////////////////////////////

public enum ToastAlignment {
    case top
    case center
    case bottom
}


/////////////////////////// --------- Language Change ---------------- ////////////////////////////////////

public enum SelectedLang {
    case english, burmese, chinese, thai
}


/////////////////////////// --------- Add Vehicle ---------------- ////////////////////////////////////

public enum VehicleList {
    case vehicle_name, vehicle_brand, vehicle_subbrand, vehicle_color, vehicle_number, vehicle_division, vehicle_licensenumber
}



