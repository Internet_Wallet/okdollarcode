//
//  Constants.swift
//  OK
//
//  Created by Ashish Kumar Singh on 10/14/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import AdSupport
import Contacts
import CoreTelephony

// common for all classes
let screenWidth  = UIScreen.main.bounds.width
let screenHeight = UIScreen.main.bounds.height
let appDelegate  = UIApplication.shared.delegate as! AppDelegate

//For AppStore Testing
var numberToMatch = "09782763424"
var appFont = ""
var appFontSize = CGFloat()
var appFontSizeReport = CGFloat()

//this will be for bottom button like submit,save,pay
var appButtonSize = CGFloat()

//this will be for the title we used in button
var appTitleButton = CGFloat()


//"519DE24D-733B-4DC4-BB86-963A525FF0C2"
//"D21417F1-7666-4D55-BF5E-068F92AE3D05"
//"EFF610FE-B582-476C-9F3F-0A2E222EC70F"

//var uuid         =   "D21417F1-7666-4D55-BF5E-068F92AE3D05"
//var otp          =   "D21417F1-7666-4D55-BF5E-068F92AE3D05"
//var simid        =   "D21417F1-7666-4D55-BF5E-068F92AE3D05"




var uuid =   UitilityClass.getUUID()
var otp  =   UitilityClass.getUUID()
var simid  = UitilityClass.getUUID()



//ASIdentifierManager.shared().advertisingIdentifier.uuidString""
//For AppStore Testing
var number : String = "" {
    
    //First this
    willSet {
        println_debug(newValue)
        print("Value old")
    }
    
    didSet {
        if numberToMatch == number {
            uuid         =  "85C44C63-0E15-4400-A773-1312CC33BC2A"
            otp          =  "85C44C63-0E15-4400-A773-1312CC33BC2A"
            simid        =  "85C44C63-0E15-4400-A773-1312CC33BC2A"
        }else{
             uuid         = UitilityClass.getUUID()
             otp          = UitilityClass.getUUID()
             simid        = UitilityClass.getUUID()
        }
        
        print("Value new")
    }
}

let userDef      = UserDefaults.standard
let notfDef      = NotificationCenter.default

var ok_password         = userDef.string(forKey: "passwordLogin_local")
var ok_password_type    = userDef.bool(forKey: "passwordLoginType")
var ok_password_Keyboard = userDef.bool(forKey: "passKeyBoardType")
var shop_Name = userDef.string(forKey: "shop_name")
var changeType = userDef.string(forKey: "changeType")
var isShowGiftCard = true
var refRecharge : RechargeBillPayViewController?
var refLoyality: LoyalityPromotionHomeViewController?
var refBoolOKDollar: BookOnOKDollarViewController?
var dashboard: DashboardVC?

var isOverseasShow = false
var isDTHShow = false
var isMarchantPaymentShow = false
var isBillSplitterShow = false
var isInstantPayShow = false
var isMobileElectronicsShow = false
var isBuySaleBonusPointShow = false
var isLuckyDrawShow = false
var isEarningPointShow = false
var isVotingshow = false
var isOfferShow = false
var isLotteryShow = false

var ok_default_language = "my" 
var registered_Language = "en"
var mccStatus  = NetworkMCCMNC.init("", "")

let profileObj       = GetProfile()
let alertViewObj     = AlertView()
let loyaltyAlertView = LoyaltyAlertView()
let progressViewObj  = ProgressView()
let geoLocManager    = GeoLocationManager.shared

let favoriteManager = FavoriteDBManager()

let advImagesUrl = AdvertisementImages()

let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
let buildNumber = "68"
let buildVersion = "1.25.9"
let EMAILCHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890.@_-"
let SEARCHCHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890.@_ -"

let SEARCH_CHAR_SET_En = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890.@_ -"
let SEARCH_CHAR_SET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890.@_-ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ "
let SEARCH_CHAR_SET_My = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀.@_ - "

let SEARCH_CHAR_SET_Uni = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀.@_ - "

let SEARCHCHARSETOffer = "ABCDEFGHIJKLMNOPQR​ေSTUVWXYျZေaြbcdefghijklmnopqrstuvwxyz1234567890.@_ -ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာ၁၂၃၄၅၆၇၈၉၀/-,:()ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀"
let PASSWORD_CHAR_SET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

let NAME_CHAR_SET_En = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "

let NAME_CHAR_SET_My = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာ "

let NAME_CHAR_SET_Uni = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ "

let PASSPORT_CHAR_SET = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz(),_-"

let BUSINESSNAME_CHAR_SET_En = "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 "

let BUSINESSNAME_CHAR_SET_My = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ "
let BUSINESSNAME_CHAR_SET_Uni = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ "

let STREETNAME_CHAR_SET_En = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789/-,:() "

let STREETNAME_CHAR_SET_My = " (),-၁၂၃၄၅၆၇၈၉၀ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ်   ိ  ္ ့  ံ ျ     ု  ဳ   ူ  ဴ  း  ၚ ွ  ီ ြ  ၤ   ဲ    ွ်   ၽႊ  ႏ  ၽြ    ႊ  ႈ  ဥ  ဧ ၿ   ၾ      ၌ ဋ ႑ ဍ   ၨ    ၳ  ၡ    ႅ   ၻ  ဉ    ဎ  ၺ  ႎ   ႍ `ါ  ႄ      ၶ    ၦ    ၱ   ၷ  ၼ  ဤ   ၸ    ၠ ၍ ႆ  ၥ ၮ   ၎ ဩ   ႀ      ဦ   ၢ႐ ဪ  ႁ    ႂ     ၯ  ၩ ၏ ာ";

let STREETNAME_CHAR_SET_Uni = " (),-၁၂၃၄၅၆၇၈၉၀ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ   ိ  ့်  ံ ြ     ု  ု   ူ  ူ  း  ါ် ှ  ီ ွ  ၤ   ဲ    ျှ   ျွှ  န  ျွ    ွှ  ှု  ဥ  ဧ ြ   ြ      ၌ ဋ ဏ္ဍ ဍ   ္ဇ    ္ထ  ္ခ    ္လ   ္ဘ  ဉ    ဎ  ္ဗ  ိံ   ႍ `ါ  ြ      ္ဓ    ္ဆ    ္တ   ္န  ္မ  ဤ   ္ပ    ္က ၍ ဿ  ္စ ဍ္ဍ   ၎င်း ဩ   ြ      ဦ   ္ဂရ ဪ  ြ    ြ     ဍ္ဎ  ္ဈ ၏ ာ ေ"

let SearchEnglishUnicode = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890.@_ - ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀.@_ -"
let SearchEnglishZwagi = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890.@_ - ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀.@_ - "

let BackSpaceValue = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890.@_ - ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀.@_ - ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀.@_ -"

//Recent seraches
var locationArr = [addressModel]()

//Offers Data Management
var freeQty = 0
var buyQty = 0
var discountAmount = 0
var isPayToDashboard: Bool? = false

let screenArea = UIScreen.main.bounds
var okContacts = Array<CNContact>()
var okDollarALLContacts = [String]()
let device = UIDevice()

let kSelectedTextColor = UIColor.init(red: 238.0/255.0, green: 135.0/255.0, blue: 25.0/255.0, alpha: 1.0)
let kYellowColor = UIColor.init(red: 233.0/255.0, green: 197.0/255.0, blue: 20.0/255.0, alpha: 1.0)
//let kBlueColor = UIColor.init(red: 22.0/255.0, green: 45.0/255.0, blue: 159.0/255.0, alpha: 1.0)
let kBlueColor = UIColor.init(hex: "#162d9f")
//let kBlueColor = UIColor.init(red: 22.0, green: 45.0, blue: 159.0, alpha: 1.0)
let kGradientGreyColor = UIColor.init(red: 38.0/255.0, green: 38.0/255.0, blue: 38.0/255.0, alpha: 0.5)
let kBackGroundGreyColor = UIColor.init(red: 230.0/255.0, green: 231.0/255.0, blue: 232.0/255.0, alpha: 1.0)

let kDarkBlueColor = UIColor.init(red: 22/255, green: 45/255, blue: 159/255, alpha: 1)

let kJSONData = "Data"
let kOfferCell = "offerCell"
let kAgent_code = "agentCode"
let kCurrentDeviceTelecoCode = "currentDeviceTelecoCode"
let kFavouriteList = "ProList"
let kSecuretoken = "securetoken"
let kBalance = "balance"
let kPin = "pin"
let kContentType_Json = "application/json"
let kContentType_urlencoded = "application/x-www-form-urlencoded"
let kMethod_Get = "GET"
let kMethod_Post = "POST"

// Anaylitics Constants
let k_Payment = "Topup"
let k_MPT = "MPT"
let k_ORIDO = "OREDOO"
let k_TELENOR = "TELENOR"
let k_MECTEL = "MECTEL"
let k_MYTEL = "MYTEL"
let k_OVERSEASE = "OVERSEAS"

let k_TransactionSuccess = "Success"
let k_TransactionFailure = "Failure"


//let msid = userDef.value(forKey: kCurrentDeviceTelecoCode) as! String
//let msid = "01" //getMsid()
let msid = getMsid()

let cellid = "false"

let googleAPIKey = "AIzaSyB4wTaf3qx48KN7wQrH7Pj9vafdMW-dIwE" //New key-Jan28
//let googleAPIKey = "AIzaSyBj3-AcJhgEf7dQ91Zrwc5UjX-mpAPzQD0" //old key
//let googleAPIKey = "AIzaSyD_vWXzmvka5eDZdLEEj-dvKDsnGwpdIYs" //New key
//let googleAPIKey = "AIzaSyAKewL4eZY6aohfdKJeMaropcfivkDsqIM" //Firebase key




let kNetworkError = "Network Not Available"
let queueBackground = DispatchQoS.QoSClass.background
let kNearByDistance = "3281"


let  Xml_Notification = "xmlParserNotification"

let ACCEPTABLE_CHARACTERS = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_@.,"

//Request Money

let kRemindRequest = "RemindRequest"
let kChangeStatusOfRequest = "ChangeStatusOfRequest"
let kRequestMoney = "RequestMoney"
let kBlockOrUnblock = "BlockOrUnblock"

let commonPrefixMobileNumber = "09"
let k_isContactUploaded = "contactUploaded"
let k_contactCount = "contactCount"

let AddMoney_GetData = "https://addmoneyapi.okdollar.org/counterdeposit/History/phonenumber/%@/limit/%@"

let AddmoneyMPU = "https://mobileview.okdollar.org/netbanking/Mpu/Index"

let AddmoneyKBZ = "https://mobileview.okdollar.org/netbanking/KbzBank/Index"

let AddmoneyCB  = "https://mobileview.okdollar.org/Netbanking/CbBank/Index"

let  AddMoneyGetCharges  = "https://addmoneyapi.okdollar.org/banks/charges/all"


let apiType = "api"
let requestType = "request"
let responseType = "response"


//Declare all the static constants here
struct GlobalConstants {
    static var timerValue:Int = 0
    
    //MARK: String Constants
    struct Strings {
        static let birthdayDateFormat = "MMM d"
        static let contactsTitle = "Contacts"
        static let phoneNumberNotAvaialable = "No phone numbers available"
        static let emailNotAvaialable = "No emails available"
        static let bundleIdentifier = "OKContactsPicker"
        static let cellNibIdentifier = "OKContactCell"
        static let defaultDateFormat    = "MM/dd/yyyy hh:mm:ss a"
        static let defaultAPIDateFormat =  "yyyy-MM-dd'T'hh:mm:ss.SSS"
        static let filterDateFormat  =     "MM/dd/yyyy"
        static let mmk = " MMK"
        static let qrCodeDateFormat = "yyyyMMdd_HHmmss"
        
        static let schduledTimeDateFormat = "dd/MM/yyyy HH:mm:ss a"
    }
    
    //MARK: Color Constants
    struct Colors {
        static let emerald = UIColor(red: (46/255), green: (204/255), blue: (113/255), alpha: 1.0)
        static let sunflower = UIColor(red: (241/255), green: (196/255), blue: (15/255), alpha: 1.0)
        static let pumpkin = UIColor(red: (211/255), green: (84/255), blue: (0/255), alpha: 1.0)
        static let asbestos = UIColor(red: (127/255), green: (140/255), blue: (141/255), alpha: 1.0)
        static let amethyst = UIColor(red: (155/255), green: (89/255), blue: (182/255), alpha: 1.0)
        static let peterRiver = UIColor(red: (52/255), green: (152/255), blue: (219/255), alpha: 1.0)
        static let pomegranate = UIColor(red: (192/255), green: (57/255), blue: (43/255), alpha: 1.0)
        
        static let all = [emerald, sunflower, pumpkin, asbestos, amethyst, peterRiver, pomegranate]
    }
    
    //MARK: Array Constants
    struct Arrays {
        static let alphabets = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","#"] //# indicates the names with numbers and blank spaces
    }
    
}

func getMsid() -> String {
    

    if #available(iOS 13.0, *){
        var primaryNetworkCode = ""
        if let data = CTTelephonyNetworkInfo.init().serviceSubscriberCellularProviders{

            if data.count == 1{
                if data["0000000100000001"]?.mobileNetworkCode ?? "" == "" {

                }else {
                    primaryNetworkCode = data["0000000100000001"]?.mobileNetworkCode ?? ""


                }
            }else{
                if data["0000000100000001"]?.mobileNetworkCode ?? "" == "" && data["0000000100000002"]?.mobileNetworkCode ?? "" == ""{
                }else {
                    primaryNetworkCode = data["0000000100000001"]?.mobileNetworkCode ?? ""
                }
            }

            userDef.set(primaryNetworkCode, forKey: kCurrentDeviceTelecoCode)
            userDef.synchronize()
            return userDef.value(forKey: kCurrentDeviceTelecoCode) as? String ?? ""
        }
    }else{
        let networkInfo = CTTelephonyNetworkInfo.init()
        let carrier     = networkInfo.subscriberCellularProvider
        if let mnc = carrier?.mobileNetworkCode {
            userDef.set(mnc, forKey: kCurrentDeviceTelecoCode)
            userDef.synchronize()
            return userDef.value(forKey: kCurrentDeviceTelecoCode) as? String ?? ""
        }
        
   }
   
    return ""
    
    
    
}

func getAllContacts()  {
    let storyBoard = UIStoryboard.init(name: "PayTo", bundle: nil)
    let contact = storyBoard.instantiateViewController(withIdentifier: "ContactPickersPicker") as! ContactPickersPicker
    contact.getContacts { (contacts, error) in
        DispatchQueue.main.async {
            okContacts = contacts
            contact.contactAll = contacts
            contact.getOkDollarContacts({ (okcontacts , error2) in
                DispatchQueue.main.async {
                    okDollarALLContacts = okcontacts
                    let defaults = UserDefaults.standard
                    let isContactUploadedOnServer = defaults.bool(forKey: k_isContactUploaded) as? Bool
                    if  ( isContactUploadedOnServer == false) {
                        uploadContactCSVToServer()
                    }else {
                        let count : Int = defaults.integer(forKey: k_contactCount)
                        if count < okContacts.count {
                            uploadContactCSVToServer()
                        }
                    }
                }
            })
        }
    }
}


func uploadContactCSVToServer () {
    
    let urlString = String.init(format: "%@",PTHelper.getContactPresigned)
    let url = URL.init(string: urlString)
    DispatchQueue.main.async {
        let task = URLSession.shared.dataTask(with: url!) {(data, response, error) in
            if((error) != nil) {
                print(error!.localizedDescription)
            }else {
                do {
                    guard let dataResponse = data else { return }
                    let json = try JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments)
                    if let jsonResponse = json as? Dictionary<String,Any> {
                        if let code = jsonResponse["Code"] as? NSNumber, code == 200 {
                            if let data = jsonResponse["Data"] as? String {
                                DispatchQueue.main.async {
                                    let path = createCSVFile()
                                    uploadFile(path, And: data)
                                }
                            }
                        } else {
                            if let msg = jsonResponse["Msg"] as? String {
                                print(msg)
                            }
                        }
                    }
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
        task.resume()
    }
}


func uploadFile(_ urlPath: URL ,And UploadURL : String ){
    
    if let url = URL(string: UploadURL){
        var request = URLRequest(url: url)
        let boundary:String = "Boundary-\(UUID().uuidString)"
        request.httpMethod = "POST"
        request.timeoutInterval = 10
        request.allHTTPHeaderFields = ["Content-Type": "multipart/form-data; boundary=----\(boundary)"]
        do{
            var data2: Data = Data()
            var data: Data = Data()
            data2 = try NSData.init(contentsOf: URL.init(fileURLWithPath: urlPath.absoluteString, isDirectory: true)) as Data
            data.append("------\(boundary)\r\n")
            //Here you have to change the Content-Type
            data.append("Content-Disposition: form-data; name=\"picture\";filename=\"OK_Dollar\(UserModel.shared.mobileNo).csv\"\r\n")
            data.append("Content-Type: text/csv\r\n\r\n")
            data.append(data2)
            data.append("\r\n")
            data.append("------\(boundary)--")
            
            request.httpBody = data
        }catch let e{
            print(e)
        }
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).sync {
            let session = URLSession.shared
            let task = session.dataTask(with: request, completionHandler: { (dataS, aResponse, error) in
                if let erros = error{
                    print(erros)
                }else{
                    do{
                        let responseObj = try JSONSerialization.jsonObject(with: dataS!, options: JSONSerialization.ReadingOptions(rawValue:0)) as! [String:Any]
                        print(responseObj)
                        let defaults = UserDefaults.standard
                        defaults.set(true,forKey:k_isContactUploaded)
                        defaults.set(okContacts.count , forKey: k_contactCount)
                        print("successfuly uploaded csv file")
                        // changes for the csv File (Avaneesh )
                    }catch let e{
                        print("after response Error :  %@",e)
                    }
                }
            }).resume()
        }
    }
}

extension Data{
    mutating func append(_ string: String, using encoding: String.Encoding = .utf8) {
        if let data = string.data(using: encoding) {
            append(data)
        }
    }
}

//func uploadMedia(uploadURL : String , FilePath file:URL) {
//    progressViewObj.showProgressView()
//    guard let url = URL(string: uploadURL) else {
//        return
//    }
//    var request = URLRequest(url: url)
//    request.httpMethod = "POST";
//    let boundary = "Boundary--\(uuid)"
//    request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
//    var user_ID = ""
//    var profile_Pic = ""
//    if let userID = self.depositorDic?["UserId"] as? String {
//        user_ID = userID
//    }
//    if let prifilePic = self.depositorDic?["ProfileImg"] as? String {
//        profile_Pic = prifilePic
//    }
//    if profile_Pic == "" && user_ID == "" {
//        return
//    }
//
//
//    let parameters: [String: Any] = [
//        "UserId": user_ID,
//        "DepositorURL": profile_Pic,
//        "Token": token
//    ]
//    request.timeoutInterval = 90
//    request.httpBody = createBodyWithParameters(parameters: parameters as? [String : String], KeyName: "file", filePathKey: "\(file)" , imageDataKey: movieData , boundary: boundary) as Data
//    let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
//        self.removeProgressView()
//
//        if error != nil {
//            println_debug("Erroe \(String(describing: error))")
//            alertViewObj.wrapAlert(title: nil, body: "Network error and try again", img: UIImage(named: "alert-icon"))
//            alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
//            }
//            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
//                self.openCamera()
//            }
//            alertViewObj.showAlert(controller: self)
//            return
//        }
//        println_debug("Erroe \(String(describing: response))")
//        do {
//            if let data = data {
//                let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
//                if let dic = dict as? Dictionary<String,Any> {
//                    println_debug(dic)
//                    if dic["code"] as? Int == 400 {
//                        DispatchQueue.main.async {
//                            self.navigateToPreviouVCAterFaceMatched()
//                        }
//                    }else if dic["code"] as? Int == 400 {
//                        alertViewObj.wrapAlert(title: nil, body: "Face not matched".localized, img: UIImage(named: "face_no_match"))
//                        alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
//                            if let del = self.delegate {
//                                del.faceRecongnitionStatus(status: false)
//                                self.navigationController?.popViewController(animated: false)
//                            }
//                        }
//                        alertViewObj.addAction(title: "Retake".localized, style: .cancel) {
//                            self.openCamera()
//                        }
//                        alertViewObj.showAlert(controller: self)
//                    }
//                }
//            }
//        }catch {
//            alertViewObj.wrapAlert(title: nil, body: "Network error and try again".localized, img: UIImage(named: "alert-icon"))
//            alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
//
//            }
//            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
//                self.openCamera()
//            }
//            alertViewObj.showAlert(controller: self)
//        }
//    }
//    task.resume()
//
//}

func printData( requestString : String , type : String) {
    switch (type) {
    case apiType:
        print(" ************* API  *************")
        println_debug(requestString)
        print(" ******** END ********")
    case requestType:
        print(" ************* REQUEST DATA *************")
        println_debug(requestString)
        print(" ******** END ********")
    case responseType:
        print(" ************* RESPONSE DATA *************")
        println_debug(requestString)
        print(" ******** END ********")
    default:
        return
    }
}


func createCSVFile() -> URL {
    
    let allContacts = okContacts
    let fileName = "contacts.csv"
    let path = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName)!
    
    var csvText = "Name,Number,Email,ProfilePic\n\n"
    //allContacts.sortInPlace({ $0.date.compare($1.date) == .OrderedDescending })
    
    if allContacts.count > 0 {
        for contact in allContacts {
            if let tempContact = contact as? CNContact {
                let name = tempContact.givenName
                var numbers = ""
                if let countX = tempContact.phoneNumbers.count as? Int,countX > 1 {
                    for lableName in tempContact.phoneNumbers {
                        numbers.append("\(lableName.value.stringValue.getModifiedContact())")
                        numbers.append(",")
                    }
                }
                else {
                    if let phoneLbl = tempContact.phoneNumbers.last {
                        numbers.append("\(phoneLbl.value.stringValue.getModifiedContact())")
                    }
                }
                var emails = ""
                if let countX = tempContact.emailAddresses.count as? Int, countX > 1 {
                    for email in tempContact.emailAddresses {
                        emails.append("\(String(describing: email.label))")
                        emails.append(",")
                    }
                }
                else {
                    let email = tempContact.emailAddresses.last
                    emails.append("\(String(describing: email?.label))")
                    
                }
                var image = ""
                if tempContact.imageData != nil {
                    // image = String(data: profilePic , encoding: .utf8)!
                    image = "duumyImage.jpeg"
                }
                let newLine = "\(name),\(numbers),\(emails),\(image))\n"
                csvText.append(contentsOf: newLine)
            }
        }
        do {
            try csvText.write(to: path, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            print("Failed to create file")
            print("\(error)")
        }
    } else {
        // showErrorAlert("Error", msg: "There is no data to export")
    }
    return path
}


func getDivisionAndTownShip(divisionCode: String, townshipCode: String) -> String {
    var divisionTownshipName = ""
    if let townshipPath = Bundle.main.path(forResource: "townshipJson", ofType: "json")  {
        do {
            let townshipData = try Data(contentsOf: URL(fileURLWithPath: townshipPath), options: .mappedIfSafe)
            let townshipJson = try JSONSerialization.jsonObject(with: townshipData, options: .mutableLeaves)
            if let townshipJson = townshipJson as? Dictionary<String, AnyObject>, let allTownshipList = townshipJson["data"] as? [AnyObject] {
                
                let predicate = NSPredicate(format: "TownshipCode == %@", townshipCode)
                let filterTownShipList = allTownshipList.filter { predicate.evaluate(with: $0) } as [AnyObject]
                
                if filterTownShipList.count > 0 {
                    for (_, townshipValue) in filterTownShipList.enumerated() {
                        // here create new township instance for that selected division
                        if let dic = townshipValue as? Dictionary<String, AnyObject> {
                            let eachTownship = TownShipDetail()
                            
                            eachTownship.townShipNameEN = dic["TownshipName"]   as! String
                            eachTownship.townShipNameMY = dic["TownshipBName"]  as! String
                            eachTownship.stateName   = dic["StateName"]   as! String
                            
                            if appDel.currentLanguage == "my" {
                                divisionTownshipName = eachTownship.townShipNameMY + "," + eachTownship.stateName
                            } else {
                                divisionTownshipName = eachTownship.townShipNameEN + "," + eachTownship.stateName
                            }
                            
                        }
                    }
                }
            }
        } catch {
            println_debug("something went wrong when extract data from township json file")
        }
    }
    return divisionTownshipName
}

func getBurmeseAgentType() -> String {
    switch UserModel.shared.agentType {
    case .user:
        return "ကိုယ္ပိုင္"
    case .agent:
        return "ေအးဂ်င့္"
    case .advancemerchant:
        return "ႀကိဳတင္ေငြေပးမည့္ ကုန္သည္"
    case .merchant:
        return "ကုန္သည္"
    case .dummymerchant:
        return "အေရာင္းစာရင္းေငြကိုင္"
    case .oneStopMart:
        return "One Stop Mart"
    }
}

func getDivisionAndTownShipForReport(divisionCode: String, townshipCode: String) -> String {
    var divisionTownshipName = ""
    if let townshipPath = Bundle.main.path(forResource: "townshipJson", ofType: "json")  {
        do {
            let townshipData = try Data(contentsOf: URL(fileURLWithPath: townshipPath), options: .mappedIfSafe)
            let townshipJson = try JSONSerialization.jsonObject(with: townshipData, options: .mutableLeaves)
            if let townshipJson = townshipJson as? Dictionary<String, AnyObject>, let allTownshipList = townshipJson["data"] as? [AnyObject] {
                
                
                if let statePath = Bundle.main.path(forResource: "city_list_json", ofType: "json")  {
                    do {
                        let stateData = try Data(contentsOf: URL(fileURLWithPath: statePath), options: .mappedIfSafe)
                        let stateJson = try JSONSerialization.jsonObject(with: stateData, options: .mutableLeaves)
                        if let stateJson = stateJson as? Dictionary<String, AnyObject>, let allStateList = stateJson["data"] as? [AnyObject] {
                            
                            let filterStateList = allStateList.filter { (state) -> Bool in
                                if let stateObj = state as? Dictionary<String, AnyObject> {
                                    return (stateObj["State Code"] as? String ?? "") == divisionCode
                                } else {
                                    return false
                                }
                            }
                            let predicate = NSPredicate(format: "TownshipCode == %@", townshipCode )
                            let filterTownShipList = allTownshipList.filter { predicate.evaluate(with: $0) } as [AnyObject]
                            var stateName = ""
                            var stateBName = ""
                            
                            if filterTownShipList.count > 0, filterStateList.count > 0 {
                                for (_, townshipValue) in filterTownShipList.enumerated() {
                                    // here create new township instance for that selected division
                                    if let dic = townshipValue as? Dictionary<String, AnyObject> {
                                        let eachTownship = TownShipDetail()
                                        
                                        eachTownship.townShipNameEN = dic["TownshipName"] as! String
                                        eachTownship.townShipNameMY = dic["TownshipBName"] as! String
                                        if let stateDic = filterStateList.first as? Dictionary<String, AnyObject> {
                                            stateName   = stateDic["State Name"] as! String
                                            stateBName   = stateDic["State BName"] as! String
                                        }
                                        
                                        if UserModel.shared.language != "en"  {
                                            divisionTownshipName = eachTownship.townShipNameMY + "," + stateBName
                                        } else {
                                            divisionTownshipName = eachTownship.townShipNameEN + "," + stateName
                                        }
                                        
                                    }
                                }
                            }
                        }
                    } catch {
                        
                    }
                }
                
            }
        } catch {
            println_debug("something went wrong when extract data from township json file")
        }
    }
    return divisionTownshipName
}

var kHeaderHeight: CGFloat {
    if device.type == .iPhoneX {
        return 84.0
    }
    return 64.0
}

func setLocStr(_ str: String) -> String {
    return appDelegate.getlocaLizationLanguage(key: str)
}

var phNumValidationsApi: [PhNumValidationList] = []
var phNumValidationsFile: [PhNumValidationList] = []
var kycAppControlInfo: [KycAppControlInfo] = []
var bankRoutingapiversion: String?
var serviceControlAppInfo: [ServiceControlAppInfo] = []
var telecoTopupSubscription : [TelecoTopupSubscriptionValue]? 


extension String {
    func isNumeric(with spaceAllowed: Bool = false) -> Bool {
        if spaceAllowed {
            return self.range(of: "[^0-9 ]", options: .regularExpression) == nil && self != ""
        } else {
            return self.range(of: "[^0-9]", options: .regularExpression) == nil && self != ""
        }
    }
    
    func isAlphanumeric() -> Bool {
        return self.rangeOfCharacter(from: CharacterSet.alphanumerics.inverted) == nil && self != ""
    }
    
    func isCouponCharacters() -> Bool {
        return self.range(of: "[^a-zA-Z0-9-]", options: .regularExpression) == nil && self != ""
    }
    
}



