//
//  Utilities.swift
//  OK
//
//  Created by Ashish Kumar Singh on 10/13/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreTelephony
import Contacts
import CoreLocation

extension OKBaseController {
    
  func showHeaderViewWithRequiredTitle(strHeaderTitle: String)-> UIView
  {
    let view = UIView(frame: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 25))
    let label = UILabel(frame: CGRect(x: 15, y: 0, width: self.view.frame.width - 15, height:25))
    label.text = strHeaderTitle.localized
    label.textAlignment = NSTextAlignment.left
    label.font = UIFont(name: appFont, size: 14)
    label.textColor = UIColor.darkGray
    label.backgroundColor = UIColor.clear
    view.addSubview(label)
    
    let labelRequired = UILabel(frame: CGRect(x: 15, y: 0, width: self.view.frame.width - 30, height: 25))
    labelRequired.text = "Required Field *".localized
    labelRequired.backgroundColor = UIColor.clear
    labelRequired.textAlignment = NSTextAlignment.right
    labelRequired.font = UIFont(name: appFont, size: 14)
    labelRequired.textColor = UIColor.red
    view.addSubview(labelRequired)
    
    view.backgroundColor = UIColor.init(red: 223/255, green: 223/255, blue: 223/255, alpha: 1)
    return view
  }
  
  func showHeaderViewWithTitle(strHeaderTitle: String)-> UIView
  {
    let view = UIView(frame: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 25))
    let label = UILabel(frame: CGRect(x: 15, y: 0, width: self.view.frame.width - 15, height:25))
    label.text = strHeaderTitle.localized
    label.textAlignment = NSTextAlignment.left
    label.font = UIFont(name: appFont, size: 14)
    label.textColor = UIColor.darkGray
    
    label.backgroundColor = UIColor.clear
    view.addSubview(label)
    view.backgroundColor = UIColor.init(red: 223/255, green: 223/255, blue: 223/255, alpha: 1)
    return view
  }

    func nullToNil(value : AnyObject?) -> AnyObject? {
        if value is NSNull {
            return nil
        } else {
            return value
        }
    }
    
    func convertImageToBase64(image: UIImage) -> String {
        
        //let imageData:NSData = UIImagePNGRepresentation(image)! as NSData
        
        let imageData:NSData =  image.jpegData(compressionQuality: 0.0)! as NSData
        let dataImage = imageData.base64EncodedString(options: .lineLength64Characters)
        return dataImage
        
    }// end convertImageToBase64
    
    func countryViewController(delegate: CountryViewControllerDelegate?) -> CountryViewController {
        
        let story    = UIStoryboard.init(name: "Country", bundle: nil)
        let vc       = story.instantiateViewController(withIdentifier: "CountryViewController") as? CountryViewController
        vc?.modalPresentationStyle = .fullScreen
        if delegate != nil {
            vc?.delegate = delegate
        }
        return vc!
        
    }
    
    func gettingCountryCode() {
        
        let networkInfo = CTTelephonyNetworkInfo.init()
        let carrier     = networkInfo.subscriberCellularProvider
        if #available(iOS 12.0, *) {
            if let ct = networkInfo.serviceSubscriberCellularProviders {
                if ct.count == 1 {
                    UserDefaults.standard.set(false, forKey: "DUAL_SIM")
                }else {
                    UserDefaults.standard.set(true, forKey: "DUAL_SIM")
                }
            }
        }else {
            UserDefaults.standard.set(false, forKey: "DUAL_SIM")
        }

        loadCountry { (result) in
            
            if  let mcc = carrier?.mobileCountryCode {
                for data in result {
                    if let dic = data as? Dictionary<String,AnyObject> {
                        if dic["MCC"] as! String == mcc {
                            println_debug(dic)
                            preCodeCountry = CodePreLogin.init(cName: dic["CountryName"] as! String, codes: dic["CountryCode"] as! String, flag: "CountryFlagCode")
                            if  let mnc =  carrier?.mobileNetworkCode {
                                if let network = dic["networks"] as? [Dictionary<String,String>] {
                                    for net in network {
                                        if net["MNC"] == mnc {
                                            preNetInfo = NetworkPreLogin.init(status: net["Status"]!, netName: net["NetworkName"]!, opeName: net["OperatorName"]!)
                                            println_debug(preNetInfo)
                                            if let mncVal = net["MNC"], let mccVal = dic["MCC"] as? String {
                                                mccStatus = NetworkMCCMNC.init(mncVal, mccVal)
                                            } else {
                                                println_debug("NetworkMCCMNC initializa failed")
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func loadCountry(completion: (_ result: NSArray)->()) {
        do {
            if let file = Bundle.main.path(forResource: "mcc_mnc", ofType: "txt") {
                let data = try Data.init(contentsOf: URL.init(fileURLWithPath: file))
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let object = json as? NSDictionary {
                    if let arr = object.object(forKey: "data") as? NSArray {
                        completion(arr)
                    }
                } else if let object = json as? [Any] {
                    println_debug(object)
                } else {
                    println_debug("JSON is invalid")
                }
            } else {
                println_debug("no file")
            }
        } catch {
            println_debug(error.localizedDescription)
        }
    }
    
    
    func noInternetAlert() {
        DispatchQueue.main.async {
            loyaltyAlertView.wrapAlert(title: "No Internet Connection".localized, body:"Please check your internet connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            loyaltyAlertView.addAction(title: "OK".localized, style: .cancel, action: {
                println_debug("Cancel")
            })
            loyaltyAlertView.showAlert(controller: self)
        }
        
    }
    
    // MARK: Getting Operator Name
    
    func getOparatorName(_ mobileNumber: String) -> String {
        
        let operatorArray = ["Select Operator", "Telenor", "Ooredoo", "MPT", "MecTel", "MyTel", "MecTel CDMA", "Mpt CDMA(800)", "Mpt CDMA(450)"]
        
        var oparatorName: String
        
        if mobileNumber.hasPrefix("979") || mobileNumber.hasPrefix("0979") || mobileNumber.hasPrefix("0095979") || mobileNumber.hasPrefix("00950979") || mobileNumber.hasPrefix("978") || mobileNumber.hasPrefix("0978") || mobileNumber.hasPrefix("0095978") || mobileNumber.hasPrefix("00950978") || mobileNumber.hasPrefix("0976") || mobileNumber.hasPrefix("976") || mobileNumber.hasPrefix("00950976") || mobileNumber.hasPrefix("0095976") || mobileNumber.hasPrefix("977") || mobileNumber.hasPrefix("0977") || mobileNumber.hasPrefix("00950977") || mobileNumber.hasPrefix("0095977") {
            oparatorName = operatorArray[1]
            //oparatorName =@"Telenor";
            
        }else if mobileNumber.hasPrefix("0997") || mobileNumber.hasPrefix("997") || mobileNumber.hasPrefix("00950997") || mobileNumber.hasPrefix("0095997") || mobileNumber.hasPrefix("99") || mobileNumber.hasPrefix("099") || mobileNumber.hasPrefix("0095099") || mobileNumber.hasPrefix("009599") {
            
            oparatorName = operatorArray[2]
            //oparatorName =@"Oreedoo";

            if mobileNumber.hasPrefix("0095991") || mobileNumber.hasPrefix("0991") || mobileNumber.hasPrefix("991") {
                oparatorName = operatorArray[3]
                //oparatorName =@"mpt";
            }
            
            
        }else if mobileNumber.hasPrefix("093") || mobileNumber.hasPrefix("93") || mobileNumber.hasPrefix("0095093") || mobileNumber.hasPrefix("009593") {
            oparatorName = operatorArray[4]
            //oparatorName =@"Mectel";
            
        }else if mobileNumber.hasPrefix("0931") || mobileNumber.hasPrefix("931") || mobileNumber.hasPrefix("0933") || mobileNumber.hasPrefix("933") || mobileNumber.hasPrefix("00950931") || mobileNumber.hasPrefix("0095931") || mobileNumber.hasPrefix("00950933") || mobileNumber.hasPrefix("0095933") {
            oparatorName = operatorArray[5]
            //oparatorName =@"Mectel CDMA";
            
        } else if mobileNumber.hasPrefix("0973") || mobileNumber.hasPrefix("973") || mobileNumber.hasPrefix("00950973") || mobileNumber.hasPrefix("0095973") || mobileNumber.hasPrefix("980") || mobileNumber.hasPrefix("0980") || mobileNumber.hasPrefix("00950980") || mobileNumber.hasPrefix("0095980") {
            oparatorName = operatorArray[6]
            //oparatorName =@"MPT 800";
            
            
        }else if mobileNumber.hasPrefix("0949") || mobileNumber.hasPrefix("949") || mobileNumber.hasPrefix("00950949") || mobileNumber.hasPrefix("0095949") || mobileNumber.hasPrefix("0947") || mobileNumber.hasPrefix("947") || mobileNumber.hasPrefix("0095947") || mobileNumber.hasPrefix("00950947") || mobileNumber.hasPrefix("0986") || mobileNumber.hasPrefix("986") || mobileNumber.hasPrefix("00950986") || mobileNumber.hasPrefix("0095986") {
            oparatorName = operatorArray[7]
            //oparatorName =@"MPT 400";
            
        }
        
        
        else if mobileNumber.hasPrefix("0969") || mobileNumber.hasPrefix("969") || mobileNumber.hasPrefix("00950969") || mobileNumber.hasPrefix("0095969") {
            oparatorName = operatorArray[5]
        }
            else {
            oparatorName = operatorArray[3]
            //oparatorName =@"MPT ";
            
        }
        
        return oparatorName
    }
    
    // MARK: Get Agent Type
    func agentTypeString() -> String {
        let type = UserModel.shared.agentType
        switch type {
        case .user:
            return "Personal"
        case .merchant:
            return "Merchant"
        case .agent:
            return "Agent"
        case .advancemerchant:
            return "Advance Merchant"
        case .dummymerchant:
            return "Safety Cashier"
        case .oneStopMart:
            return "One Stop Mart"
        }
    }
    
    // MARK: Duration from lat&long
    func durationFromLatLong(lat:Double?,long:Double?) -> String {
        var strDuration = ""
        let coordinate0 = CLLocation(latitude: lat ?? 0.0, longitude: long ?? 0.0)
        let coordinate1 = CLLocation(latitude: Double(geoLocManager.currentLatitude ?? "0.0")!, longitude: Double(geoLocManager.currentLongitude ?? "0.0")!)
        let distance = coordinate0.distance(from: coordinate1) // result is in meters
        let time = (distance / 500).rounded()
        if time < 1 {
            strDuration = "1 Min"
        } else {
            if time > 60 {
                let tmpTime = Int(time) % 60
                strDuration = "\(Int(tmpTime)) Hr"
            } else {
            strDuration = "\(Int(time)) Mins"
            }
        }
        return strDuration
    }
    // MARK: Round off amount
    
    func roundOffMoney(_ Amount: String) -> String {
        var ChangedAmount: String
        let cset = CharacterSet(charactersIn: ".")
        let range: NSRange = (Amount as NSString).rangeOfCharacter(from: cset)
        if range.location == NSNotFound {
            // not in the string
            ChangedAmount = Amount
            
        }else {
            // char are present
            var roundedup: Float = Float(Amount) ?? 0.0
            let intpart: Float = Float(roundedup)
            let decpart: Float = roundedup - intpart
            if decpart == 0.0 {
                //Contains no decimals
                let result = Int(roundf(roundedup))
                ChangedAmount = "\(result)"
                
            } else {
                
                //Number contains decimals
                roundedup = roundf(roundedup * 100.0) / 100.0
                ChangedAmount = String(format: "%.2f", roundedup)
            }
        }
        
        return ChangedAmount
    }
    
    
    func getNumberFormat(_ str: String) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        //  let groupingSeparator = NSLocale.current[.groupingSeparator] as? String
        
        let groupingSeparator = NSLocale.current.groupingSeparator
        formatter.groupingSeparator = groupingSeparator ?? ""
        formatter.groupingSize = 3
        formatter.alwaysShowsDecimalSeparator = false
        formatter.usesGroupingSeparator = true
        if str.contains(".") {
            formatter.minimumFractionDigits = 2
        }
        let amount: String? = formatter.string(from: Double(str)! as NSNumber)
        return amount ?? ""
    }
    
    func getAgentCode(numberWithPrefix num: String, havingCountryPrefix preCode: String) -> String {
        var finalAgentCode = ""
        var agentCodePrefic = preCode
        agentCodePrefic = agentCodePrefic.replacingOccurrences(of: "+", with: "00")
        if agentCodePrefic == "0095" {
            if num.hasPrefix("0") {
                let firstChar = num.dropFirst()
                finalAgentCode = String(firstChar)
            } else {
                finalAgentCode = num
            }
        } else {
            finalAgentCode = num
        }

        finalAgentCode = agentCodePrefic + finalAgentCode
        println_debug(finalAgentCode)
        return finalAgentCode
    }
    
    func removeCommaFromDigit(_ digit: String) -> String {
        
        var formateStr: String
  
        formateStr = digit.replacingOccurrences(of: ",", with: "")
        return formateStr

    }
    
    //Server Date Format
    func getServerDate(_ digit: String) -> String {
        
        if(digit.count > 0)
        {
        var FormateStr: String
        
        //calculate duration since join
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)

        // initially set the format based on your datepicker date
        formatter.dateFormat = "dd-MM-yyyy"
        
        // convert your string to date
        let yourDate = formatter.date(from: digit)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MMM-yyyy"
        // again convert your date to string
        FormateStr = formatter.string(from: yourDate!)
        
        return FormateStr
        }
        else
        {
            return ""
        }
    }
    
    //Date Of Birth Year
    func getDOBYear(_ digit: String) -> String {
        var FormateStr: String
        
        //calculate duration since join
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)

        // initially set the format based on your datepicker date
        formatter.dateFormat = "dd-MM-yyyy"
        //formatter.timeZone = TimeZone(abbreviation: "UTC");

        // convert your string to date
        let yourDate = formatter.date(from: digit)
        
        let form = DateComponentsFormatter()
        form.maximumUnitCount = 2
        form.unitsStyle = .full
        form.allowedUnits = [.year]
        
        let date : Date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "dd-MM-yyyy"
        //dateFormatter.timeZone = TimeZone(abbreviation: "UTC");

        let todaysDate = dateFormatter.string(from: date)
        
        let yearValue = form.string(from: yourDate!, to: formatter.date(from: todaysDate)!)
        println_debug(yearValue as Any)
        let fullNameArr = yearValue?.components(separatedBy: " ")
        FormateStr = (fullNameArr![0] as NSString) as String

        return FormateStr
    }
    
    func get12FormatTime(timeStr: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "HH:mm:ss"
        //dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        var hour = ""
        if let time = dateFormatter.date(from: timeStr) {
            dateFormatter.dateFormat = "hh:mm a"
            hour = dateFormatter.string(from: time)
        }
        return hour
    }
    
    //Get Date & Time
    func getDateAndTime(_ date: String) -> String {
        var formateDate: String
        var formateTime: String
        var strFinal: String
        
        //calculate duration since join
        let formatterDate = DateFormatter()
        formatterDate.calendar = Calendar(identifier: .gregorian)

        // initially set the format based on your datepicker date
        formatterDate.dateFormat = "yyyy-MM-dd'T'hh:mm:ss.SS"
        
        // convert your string to date
        let yourDate = formatterDate.date(from: date)
        //then again set the date format whhich type of output you need
        formatterDate.dateFormat = "dd-MMM-yyyy"
        // again convert your date to string
        formateDate = formatterDate.string(from: yourDate!)
        
        //then again set the date format whhich type of output you need
        formatterDate.dateFormat = "hh:mm a"
        // again convert your date to string
        formateTime = formatterDate.string(from: yourDate!)
        
        strFinal = formateDate + "&" + formateTime
        return strFinal
    }
    
    //"OkDollarJoiningDate":"12/12/2017 3:11:18 PM"
    func getSubmitJoiningDate(_ digit: String) -> String {
        var FormateStr: String
        
        //calculate duration since join
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        
        // initially set the format based on your datepicker date
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
        
        // convert your string to date
        let yourDate = formatter.date(from: digit)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd/MM/yyyy hh:mm:ss a"
        // again convert your date to string
        FormateStr = formatter.string(from: yourDate!)
        
        return FormateStr
    }
    
    //Joining Date
    func getJoiningDate(_ digit: String) -> String {
        var FormateStr: String
        
        //calculate duration since join
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)

        // initially set the format based on your datepicker date
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        
        // convert your string to date
        let yourDate = formatter.date(from: digit)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MMM-yyyy"
        // again convert your date to string
        FormateStr = formatter.string(from: yourDate!)
        
        return FormateStr
    }
    
    //Joining Year
    func getJoiningYear(_ digit: String) -> String {
        var FormateStr = ""
        
        //calculate duration since join
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)

        // initially set the format based on your datepicker date
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
        
        // convert your string to date
        let yourDate = formatter.date(from: digit)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MMM-yyyy"
        // again convert your date to string
        let form = DateComponentsFormatter()
        form.maximumUnitCount = 2
        form.unitsStyle = .full
        form.allowedUnits = [.year]
        FormateStr = form.string(from: yourDate!, to: Date())!
        /*
        let yearValue = form.string(from: yourDate!, to: Date())
        //println_debug(yearValue as Any)
        let fullNameArr = yearValue?.components(separatedBy: " ")
        let firstName = (fullNameArr![0] as NSString).integerValue

        if(firstName > 1)
        {
            FormateStr = String(firstName) + " Years"
        }
        else  if(firstName == 1)
        {
            FormateStr = String(firstName) + " Year"
        }
        */
        return FormateStr
    }
    
    
    
    func getDigitDisplay(_ digit: String) -> String {
        var FormateStr: String
        let mystring = digit.replacingOccurrences(of: ",", with: "")
        let number = NSDecimalNumber(string: mystring)
        
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        FormateStr = formatter.string(from: number)!
        if FormateStr == "NaN"{
            FormateStr = ""
        }
        
        return FormateStr
    }
    
    //Get URL from String
    func getImageURLFromString(_ strURL: String) -> URL {
        
        var strURLData: URL
        
        if(strURL.count == 0)
        {
            strURLData = URL.init(string: "http://www.google.com")!
        }
        else
        {
        strURLData = URL.init(string: strURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)!
        }
        return strURLData
    }
    
    //Get Emoji Icon from string
    func getEmojiIconFromString(_ strImg: String) -> UIImage {
        var FormateStr: UIImage
        
        let size = CGSize(width: 30, height: 35)
        UIGraphicsBeginImageContextWithOptions(size, false, 0);
        UIColor.white.set()
        let rect = CGRect(origin: CGPoint(), size: size)
        UIRectFill(CGRect(origin: CGPoint(), size: size))
        strImg.draw(in: rect, withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 30) ?? UIFont.systemFont(ofSize: 30)])
        FormateStr = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return FormateStr
    }
    
    //Get VerfiyOnTill from String
    func getVerfiyOnTillFormat(_ date: String) -> String {
        var FormateStr: String = ""
        if date.count > 0
        {
        let dateString = date
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "MM/dd/yyyyy hh:mm:ss a"
        let date: Date? = dateFormatter.date(from: dateString)
        // Convert date object into desired format
        dateFormatter.dateFormat = "dd-MM-yyyy"
        FormateStr = dateFormatter.string(from: date!)
        }
        return FormateStr
    }
    
    func getExpireDays(_ date: String) -> Int
    {
        //calculate duration since join
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        // initially set the format based on your datepicker date
        formatter.dateFormat = "MM/dd/yyyyy hh:mm:ss a"//"dd-MMM-yyyy"
        
        let myString = date
        println_debug(myString)
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MMM-yyyy"
        // again convert your date to string
        
        let form = DateComponentsFormatter()
        form.maximumUnitCount = 2
        form.unitsStyle = .full
        form.allowedUnits = [.day]
        let yearValue = form.string(from: Date(), to: yourDate!)
        println_debug(yearValue as Any)
        let fullNameArr = yearValue?.components(separatedBy: " ")
        let firstName = (fullNameArr![0] as NSString).integerValue
        
        return firstName
    }
    
    //Get DOB from String
    func getDOBDateFormat(_ date: String) -> String {
        var FormateStr: String
        
        let dateString = date
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let date: Date? = dateFormatter.date(from: dateString)
        // Convert date object into desired format
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        FormateStr = dateFormatter.string(from: date!)
        
        return FormateStr
    }
    //Get DOB from String
    func getDOBDateFormatforEcommerce(_ date: String) -> String {
        var FormateStr: String = ""
        
        let dateString = date
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        let date: Date? = dateFormatter.date(from: dateString)
        // Convert date object into desired format
        dateFormatter.dateFormat = "dd-MM-yyyy"
        if let dateFinal = date {
            FormateStr = dateFormatter.string(from: dateFinal)
        }
        return FormateStr
    }

    //Convert 0 to 00
    func replaceStringWithZero(str : String) -> String {
        if str.count == 2 {
            let index = str.index(str.startIndex, offsetBy: 0)
            if str == "00" {
                return "0"
            }else {
                if str[index] == "0"{
                    let string = str.replacingOccurrences(of: "0", with: "")
                    return string
                }
            }
        }
        return str
    }
    
    //Convert server date to Display date
    func convertServerDateToDisplayDate(_ date: String) -> String {
        
        let dateString = date
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"//"yyyy-MM-dd'T'HH:mm:ss"
        //dateFormatter.timeZone = TimeZone(abbreviation: "UTC");

        if let dateStr = dateFormatter.date(from: dateString)
        {
            // Convert date object into desired format
            let dateFormatterNew = DateFormatter()
            dateFormatterNew.calendar = Calendar(identifier: .gregorian)
            dateFormatterNew.dateFormat = "dd-MMM-yyyy"
            return dateFormatterNew.string(from: dateStr)
        }
        else
        {
            return ""
        }
    }
    
    //Convert server date to Display date
    func convertServerDateToMMDate(_ date: String) -> String {
        
        let dateString = date
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"//"yyyy-MM-dd'T'HH:mm:ss"
        //dateFormatter.timeZone = TimeZone(abbreviation: "UTC");
        
        if let dateStr = dateFormatter.date(from: dateString)
        {
            // Convert date object into desired format
            let dateFormatterNew = DateFormatter()
            dateFormatterNew.calendar = Calendar(identifier: .gregorian)
            dateFormatterNew.dateFormat = "dd-MMM-yyyy"
            return dateFormatterNew.string(from: dateStr)
        }
        else
        {
            return ""
        }
    }
    
    //00 Formate
    func getDisplayDate(_ date: String) -> String {
        
        var FormateStr: String
        //let dateString = date
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
        let date123 = dateFormatter.date(from: date)
        // Convert date object into desired format
        dateFormatter.dateFormat = "EE, dd-MMM-yyyy hh:mm a"
        //dateFormatter.locale = Locale.current
        
        if let dateStr = date123 {
           FormateStr = dateFormatter.string(from: dateStr)
        } else {
            FormateStr = date
        }
        
        return FormateStr
        
    }
    
    // Check Myanmar Number
    func isMyanmarNumber(_ phoneNum: String, withCountryCode countryCode: String) -> Bool {
    
        if phoneNum.hasPrefix("9") {
                return true
            }
            else if phoneNum.hasPrefix("0") {
                return true
            }
        else if phoneNum.hasPrefix("+95") {
            return true
        }
        else if phoneNum.hasPrefix("0095") {
            return true
        }
        else if phoneNum.hasPrefix("95") {
            return true
        }
        else if phoneNum.hasPrefix("09") {
            return true
        }
        return false
    }
    
    
    //00 Formate
    func getConatctNum(_ phoneNum: String, withCountryCode countryCode: String) -> String {
        var FormateStr: String
        if phoneNum.hasPrefix("0") {
            _ = (phoneNum).substring(from: 1)
        }
        let notAllowedChars = CharacterSet(charactersIn: "+()")
        let resultString: String = countryCode.components(separatedBy: notAllowedChars).joined(separator: "")
        if (countryCode == "+95") || (countryCode == "(+95)") {
            
            if phoneNum.hasPrefix("+")  {
                FormateStr = "00\((phoneNum).substring(from: 1))"
            }
            else if phoneNum.hasPrefix("0")
            {
                FormateStr = "0095\((phoneNum).substring(from: 1))"
            }
            else {
                FormateStr = "0095\(phoneNum)"
            }
            
        }
        else {
            FormateStr = "00\(resultString)\(phoneNum)"
        }
        return FormateStr
    }
    
    //00 Formate
    func getActualNum(_ phoneNum: String, withCountryCode countryCode: String) -> String {
        var FormateStr: String
        if phoneNum.hasPrefix("0") {
            _ = (phoneNum).substring(from: 1)
        }
        let notAllowedChars = CharacterSet(charactersIn: "+()")
        let resultString: String = countryCode.components(separatedBy: notAllowedChars).joined(separator: "")
        if (countryCode == "+95") || (countryCode == "(+95)") {
            if phoneNum.hasPrefix("0095") {
                FormateStr = "0\((phoneNum).substring(from: 4))"
            }
            else if phoneNum.hasPrefix("0091") {
                FormateStr = "0\((phoneNum).substring(from: 4))"
            }
            else if phoneNum.hasPrefix("95") {
                FormateStr = "0\((phoneNum).substring(from: 2))"
            }
            else {
                FormateStr = "\(phoneNum)"
            }
            
        }
        else {
            FormateStr = "00\(resultString)\(phoneNum)"
        }
        return FormateStr
    }
    
    //+95 Formate
    func getPlusWithDestinationNum(_ phoneNum: String, withCountryCode countryCode: String) -> String {
        var FormateStr: String
        if phoneNum.hasPrefix("0") {
            _ = (phoneNum).substring(from: 1)
            println_debug(phoneNum)
        }
        let notAllowedChars = CharacterSet(charactersIn: "+()")
        let resultString: String = countryCode.components(separatedBy: notAllowedChars).joined(separator: "")
        if (countryCode == "+95") || (countryCode == "(+95)") {
            if phoneNum.hasPrefix("9") {
                FormateStr = "00\(resultString)\(phoneNum)"
            }
            else if phoneNum.hasPrefix("0") {
                FormateStr = "+\((phoneNum).substring(from: 2))"
            }
            else {
                FormateStr = "00\(resultString)9\(phoneNum)"
            }
        }
        else {
            FormateStr = "00\(resultString)\((phoneNum).substring(from: 1))"
        }
        return FormateStr.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    //0095 Formate
    func getDestinationNum(_ phoneNum: String, withCountryCode countryCode: String) -> String {
        var FormateStr: String
        if phoneNum.hasPrefix("0") {
            _ = (phoneNum).substring(from: 1)
            println_debug(phoneNum)
        }
        let notAllowedChars = CharacterSet(charactersIn: "+()")
        let resultString: String = countryCode.components(separatedBy: notAllowedChars).joined(separator: "")
        if (countryCode == "+95") || (countryCode == "(+95)") {
            if phoneNum.hasPrefix("9") {
                FormateStr = "00\(resultString)\(phoneNum)"
            }
            else if phoneNum.hasPrefix("0") {
                FormateStr = "00\(resultString)\((phoneNum).substring(from: 1))"
            }
            else {
                FormateStr = "00\(resultString)9\(phoneNum)"
            }
        }
        else {
            FormateStr = "00\(resultString)\((phoneNum))"
        }
        return FormateStr.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    func todayDateWithOutputFormat(outputFormat: String ) -> String {
        let date = Date.init()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = outputFormat
        return dateFormatter.string(from: date)
    }
    
    //Web Module-TransactionsCellTower
    func transactionsCellTowerDictionary() ->NSMutableDictionary {
        
        let dictTransactionsCellTower = NSMutableDictionary()
        dictTransactionsCellTower["Lac"] = ""
        dictTransactionsCellTower["Mcc"] = mccStatus.mcc
        dictTransactionsCellTower["Mnc"] = mccStatus.mnc
        dictTransactionsCellTower["SignalStrength"] = ""
        dictTransactionsCellTower["NetworkType"] = UitilityClass.getNetworkType()
        
        if UitilityClass.isConnectedToWifi(){
            let wifiInfo = UitilityClass.getNetworkInfos()[0].components(separatedBy: ",")
            dictTransactionsCellTower["ConnectedwifiName"] = wifiInfo[0]
            dictTransactionsCellTower["Ssid"] = wifiInfo[0]
            dictTransactionsCellTower["Mac"] = wifiInfo[1]
        }else{
            dictTransactionsCellTower["ConnectedwifiName"] = ""
            dictTransactionsCellTower["Ssid"] = ""
            dictTransactionsCellTower["Mac"] = ""
        }
        
        return dictTransactionsCellTower
    }
    //Web Module-lGeoLocation
    func lGeoLocationDictionary() ->NSMutableDictionary {
        
        let dictlGeoLocation = NSMutableDictionary()
        dictlGeoLocation["CellID"] = ""
        dictlGeoLocation["Latitude"] = GeoLocationManager.shared.currentLatitude ?? "0.0"
        dictlGeoLocation["Longitude"] = GeoLocationManager.shared.currentLongitude ?? "0.0"
        
        return dictlGeoLocation
    }
    
  //Return Login Dictionary
  func loginInfoDictionary() ->NSMutableDictionary {
    
    let dictLoginUserInfo = NSMutableDictionary()
    dictLoginUserInfo["AppId"] = UserModel.shared.appID
    dictLoginUserInfo["Limit"] = 0
    dictLoginUserInfo["MobileNumber"] = UserModel.shared.mobileNo
    dictLoginUserInfo["Msid"] = msid
    dictLoginUserInfo["Offset"] = 0
    dictLoginUserInfo["Ostype"] = 1
    dictLoginUserInfo["Simid"] = UserModel.shared.simID
    
    return dictLoginUserInfo
  }
    func loginInfoDictionaryWithOTP() ->NSMutableDictionary {
        let dictLoginUserInfo = NSMutableDictionary()
        dictLoginUserInfo["AppId"] = UserModel.shared.appID
        dictLoginUserInfo["Limit"] = 0
        dictLoginUserInfo["MobileNumber"] = UserModel.shared.mobileNo
        dictLoginUserInfo["Msid"] = msid
        dictLoginUserInfo["Offset"] = 0
        dictLoginUserInfo["Ostype"] = 1
        dictLoginUserInfo["Simid"] = UserModel.shared.simID
        dictLoginUserInfo["Otp"] = UserModel.shared.simID
        return dictLoginUserInfo
    }
    
  
    //#MARK:- Alert View Methods
    func showProgressView()  {
        //DispatchQueue.main.async {
        PTLoader.shared.show()

    }
    
    func removeProgressView() {
        PTLoader.shared.hide()
    }
    //#MARK:- End of Alert View Methods
    
    class func getIPAddress() -> String? {
        var address: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next }
                
                let interface = ptr?.pointee
                let addrFamily = interface?.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                    if let ifaName = interface?.ifa_name {
                        if String(cString: ifaName) == "en0" {
                            var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                            getnameinfo(interface?.ifa_addr, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                            address = String(cString: hostname)
                        }
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        return address
    }
    
    
    // MARK :- Contacts Suggesstion
    
    func contactSuggesstionCompletion( number:String,  completionHandler : @escaping ([Dictionary<String, Any>]) -> Void)  {
        var contactNumbers = [Dictionary<String, Any>]()
        
        if okContacts.count > 0 {
            for contact in okContacts {
                
                for phoneNumber in contact.phoneNumbers {
                    let phoneNumberStruct = phoneNumber.value
                    let phoneNumberString = phoneNumberStruct.stringValue
                    //println_debug("print phone number: \(phoneNumberString)")
                    
                    let strMobNo = phoneNumberString
                    //println_debug(strMobNo)
                    let trimmed = String(strMobNo.filter { !" ".contains($0) })
                    //println_debug(trimmed)
                    
                    var prefixString : String = number
                    
                    if prefixString.hasPrefix("0") {
                        _ = prefixString.remove(at: String.Index.init(encodedOffset: 0))
                    }
                    
                    if trimmed.lowercased().range(of: prefixString.lowercased()) != nil {
                        let dic = [ContactSuggessionKeys.contactname            : contact.givenName,
                                   ContactSuggessionKeys.phonenumber_backend    : trimmed.digitsPhone,
                                   ContactSuggessionKeys.phonenumber_onscreen   : phoneNumberString.digitsPhone,
                                   ContactSuggessionKeys.operatorname           : PayToValidations().getNumberRangeValidation(number).operator]
                        if PayToValidations().getNumberRangeValidation(trimmed).isRejected == false {
                            contactNumbers.append(dic)
                        }
                    }
                }
            }
        }
        
        completionHandler(contactNumbers)
        
    }

    
    func contactSuggesstion(_ number:String)  -> [Dictionary<String, Any>] {
        var contactNumbers = [Dictionary<String, Any>]()
        
        if okContacts.count > 0 {
            for i in 0...okContacts.count - 1 {

                for phoneNumber in okContacts[i].phoneNumbers {
                    let phoneNumberStruct = phoneNumber.value
                    let phoneNumberString = phoneNumberStruct.stringValue
                    //println_debug("print phone number: \(phoneNumberString)")

                    let strMobNo = phoneNumberString
                    //println_debug(strMobNo)
                    let trimmed = String(strMobNo.filter { !" ".contains($0) })
                    //println_debug(trimmed)

                    let strContactNumber = self.getConatctNum(trimmed,withCountryCode: "+95")
                    //println_debug(strContactNumber)

                    var validNumber = ""
                    if strContactNumber.hasPrefix("0095") {
                        validNumber = "0\(strContactNumber.substring(from: 4))"
                        let operatorName = self.getOparatorName(validNumber)
                        if validNumber.contains(find: number) {
                            let dic = [ContactSuggessionKeys.contactname            : okContacts[i].givenName,
                                       ContactSuggessionKeys.phonenumber_backend    : validNumber,
                                       ContactSuggessionKeys.phonenumber_onscreen   : phoneNumberString,
                                       ContactSuggessionKeys.operatorname           : operatorName]

                            contactNumbers.append(dic)
                        }
                    }else {

                    }
                }
            }
        }
        return contactNumbers
        
    }
}

func getDestinationNumber(_ phoneNum: String, withCountryCode countryCode: String) -> String {
    var FormateStr: String
    if phoneNum.hasPrefix("0") {
        _ = (phoneNum).substring(from: 1)
        println_debug(phoneNum)
    }
    let notAllowedChars = CharacterSet(charactersIn: "+()")
    let resultString: String = countryCode.components(separatedBy: notAllowedChars).joined(separator: "")
    if (countryCode == "+95") || (countryCode == "(+95)") {
        if phoneNum.hasPrefix("9") {
            FormateStr = "00\(resultString)\(phoneNum)"
        }
        else if phoneNum.hasPrefix("0") {
            FormateStr = "00\(resultString)\((phoneNum).substring(from: 1))"
        }
        else {
            FormateStr = "00\(resultString)9\(phoneNum)"
        }
    }
    else {
        FormateStr = "00\(resultString)\((phoneNum))"
    }
    return FormateStr.trimmingCharacters(in: .whitespacesAndNewlines)
}

public func wrapAmountWithMMK(bal: String) -> NSAttributedString {
    let denomAttr = [NSAttributedString.Key.font: UIFont(name: appFont, size: 17.0) ?? UIFont.systemFont(ofSize: 17.0)]
    let amntAttr = [NSAttributedString.Key.font: UIFont(name: appFont, size: 11.0) ?? UIFont.systemFont(ofSize: 11.0)]
    let denomStr = NSMutableAttributedString(string: bal, attributes: denomAttr)
    let amountStr    = NSMutableAttributedString.init(string: " MMK", attributes: amntAttr)
    
    let amountAttr = NSMutableAttributedString()
    amountAttr.append(denomStr)
    amountAttr.append(amountStr)
    return amountAttr
}


