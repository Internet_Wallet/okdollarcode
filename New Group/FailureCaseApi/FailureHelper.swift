//
//  FailureHelper.swift
//  OK
//
//  Created by Tushar Lama on 30/09/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation

struct FailureHelper{
    
static let failureAPI = "http://13.229.4.171/api/OkdollarLog"
    enum FailureType: String {
        case REVERIFY = "REVERIFY"
        case LOGIN = "LOGIN"
        case TOPUP = "TOPUP"
        case PAYTO = "PAYTO"
        case FACEPAY = "FACEPAY"
        case ForgotPassword = "ForgotPassword"
        case Registration = "Registration"
        case Verification = "Verification"
    }
}



struct FailureBlockParam {
    
    static func getFailureBlockParam(request: Any,response: Any,type: String) -> Dictionary<String,Any>{
    var failureParam = Dictionary<String, Any>()
        failureParam["Fcm"] = "f1PnUsEqCvc:APA91bHVuVwJQxVHmxpOLN4g8Fjxe"
        failureParam["Fcm"] = UserDefaults.standard.string(forKey: "FCMToken") ?? ""
        failureParam["MobileNumber"] = UserModel.shared.mobileNo
        
        failureParam["Request"] = FailureBlockParam.getStringFromDic(request: request)
        failureParam["Response"] = FailureBlockParam.getStringFromDic(request: response)
        failureParam["Type"] = type
        failureParam["VesionName"] = buildVersion
        failureParam["VesionNo"] = String.init(format: "Build (\(buildNumber))")
        failureParam["OsType"] = 1
        return failureParam
    }
    
    static func getStringFromDic(request: Any) -> String{
        let jsonData = try! JSONSerialization.data(withJSONObject: request, options: [])
        let decoded = String(data: jsonData, encoding: .utf8)!
        
        return decoded
    }
}




