//
//  FailureViewModel.swift
//  OK
//
//  Created by Tushar Lama on 30/09/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation



class FailureViewModel{
    var createConnectionObj = NearByConnection()
    
    func sendDataToFailureApi(request: Any,response: Any,type: String,finished:@escaping () -> Void){
        createConnectionObj.sendfailureBlockToApi(request: request, response: response, type: type, completion: { [weak self]
            (error) in
            //not handling completion because its just for our internal reference when the service fails
            finished()
           
//            if let ifThereIsError = error{
//                //doesnt matter if error is there or not
//                if !ifThereIsError{
//                    finished()
//                }else{
//                    finished()
//                }
//            }
        })
    }
    
}

//var failureViewModelObj = FailureViewModel()
//if appDelegate.checkNetworkAvail() {
//    progressViewObj.showProgressView()
//    failureViewModelObj.sendDataToFailureApi(request: request,response: response,type: type,finished:{
//        progressViewObj.removeProgressView()
//        //checking if data is available
//    })
//} else {
//    self.noInternetAlert()
//    progressViewObj.removeProgressView()
//}
