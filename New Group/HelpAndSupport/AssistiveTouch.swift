//
//  AssistiveTouch.swift
//  AssistiveDemo
//
//  Created by DS on 29/12/16.
//  Copyright © 2016 DS. All rights reserved.
//

import UIKit

class AssistiveTouch: UIWindow {
    
    static var instance:AssistiveTouch = AssistiveTouch(frame: UIScreen.main.bounds)
    var canDrag:Bool = true {
        didSet{
            self.panGesture.isEnabled = canDrag
        }
    }
    var isFullScreen:Bool = false;
    let kScreenSize = UIScreen.main.bounds.size;
    var smallSize:CGRect = CGRect(x: 35, y: 20, width: 60, height: 60)
    var touchView:UIView!
    var touchButton:UIButton!
    var panGesture:UIPanGestureRecognizer!
    // MARK: init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInIt()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInIt()
    }
    
    // MARK: init touch view
    
    func commonInIt(){
        let width = 50 * (kScreenSize.width / 320.0)
        smallSize = CGRect(x: 35, y: 20, width: width, height: width)
        self.touchView = UIView(frame: smallSize)
        self.touchButton = UIButton(frame:touchView.bounds)
        self.touchButton.clipsToBounds = true
        self.touchButton.backgroundColor = .clear
        self.touchView.backgroundColor = .clear
        self.touchView.layer.cornerRadius = 10.0
        self.touchView.clipsToBounds = true
        self.touchButton.setTitle("Help", for: .normal)
        self.touchButton.imageView?.contentMode = .scaleAspectFit
        self.touchButton.setTitleColor(UIColor.darkText, for: .normal)
        self.touchButton.setImage(UIImage.init(named: "personal.png"), for: .normal)
        self.touchButton.alignVertical(spacing: 2.0)
        self.touchButton.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
        self.touchButton.addTarget(self, action: #selector(touchButtonClick(sender:)), for: .touchUpInside)
        self.touchView.addSubview(self.touchButton)
        self.touchView.isUserInteractionEnabled = true
        self.panGesture = UIPanGestureRecognizer()
        self.panGesture.addTarget(self, action: #selector(panGestureHandle(panGesture:)))
        self.touchView.addGestureRecognizer(self.panGesture)
        self.canDrag = true
        self.addSubview(self.touchView)
    }
    
    @objc func touchButtonClick(sender:UIButton){
        
//        var frame = smallSize
        
//        if isFullScreen {
//            isFullScreen = false
//            self.canDrag = true
//        } else{
//            isFullScreen = true
//            smallSize = touchView.frame
//            let tviewSize = CGSize(width:kScreenSize.width-20, height: kScreenSize.width-20)
//            frame = CGRect(x: (kScreenSize.width - tviewSize.width)/2, y: (kScreenSize.height - tviewSize.width)/2, width: tviewSize.width, height: tviewSize.height)
//            self.canDrag = false
//        }
//        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
//            self.touchView.frame = frame
//            },completion: { (complete) in
//                self.touchButton.frame = self.touchView.bounds
//            })
        self.isHidden = true
    }
    
    func showAssistiveTouch(){
        self.isHidden = false
    }
    
    func hideAssistiveTouch(){
        self.isHidden = true
    }
    
    // MARK: Pangesture view
    
    @objc func panGestureHandle(panGesture:UIPanGestureRecognizer){
        switch panGesture.state {
        case .changed:
            let contentViewH = self.touchView.bounds.size.height / 2
            var newCenter = self.touchView.center
            newCenter.y += panGesture.translation(in: self).y
            newCenter.x += panGesture.translation(in: self).x
            panGesture.setTranslation(CGPoint.zero, in: self)
            
            if(newCenter.y > contentViewH && newCenter.y < (kScreenSize.height - contentViewH)){
                self.touchView.center = newCenter
            }
            break;
        
        case .cancelled, .ended:
            var center = self.touchView.center;
            if (self.touchView.center.x > (kScreenSize.width/2)){
                center.x = kScreenSize.width - self.touchView.frame.size.width/2 - 2.5;
            }else{
                center.x = self.touchView.frame.size.width/2 + 2.5;
            }
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
                self.touchView.center = center;
            }, completion: nil)
            break;
            
        default:
            break
        }
    }
    
    
    // MARK change properties
    override var isHidden: Bool{
        didSet{
            self.backgroundColor = .clear
        }
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        var allowTouch:Bool = false;
        if self.touchView.frame.contains(point) {
            allowTouch = true;
        } else if(isFullScreen){
            allowTouch = true;
            self.touchButtonClick(sender: self.touchButton)
        }
        return allowTouch
    }
}

extension UIButton {
    func alignVertical(spacing: CGFloat = 6.0) {
        guard let imageSize = self.imageView?.image?.size,
            let text = self.titleLabel?.text,
            let font = self.titleLabel?.font
            else { return }
        self.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -imageSize.width, bottom: -(imageSize.height + spacing), right: 0.0)
        let labelString = NSString(string: text)
        let titleSize = labelString.size(withAttributes: [NSAttributedString.Key.font: font])
        self.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + spacing), left: 60 - (imageSize.width / 2.0), bottom: 0.0, right: 60 - (imageSize.width / 2.0))
        let edgeOffset = abs(titleSize.height - imageSize.height) / 2.0;
        self.contentEdgeInsets = UIEdgeInsets(top: edgeOffset, left: 0.0, bottom: edgeOffset, right: 0.0)
    }
}

class FloatingButtonController: UIViewController {
    
    private(set) var button: UIButton!
    private(set) var closeButton: UIButton!
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        window.windowLevel = UIWindow.Level(rawValue: CGFloat.greatestFiniteMagnitude)
        window.isHidden = false
        window.rootViewController = self
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow(note:)), name: UIResponder.keyboardDidShowNotification, object: nil)
    }
    
     let window = FloatingButtonWindow()
    
    override func loadView() {
        let view = UIView()
        let touchButton = UIButton()
        if appDel.currentLanguage == "en"{
            touchButton.frame = CGRect(x: 55, y: 45, width: 120, height: 70)
        }else{
            touchButton.frame = CGRect(x: 55, y: 45, width: 150, height: 70)
        }
        touchButton.frame = CGRect(x: 55, y: 45, width: 120, height: 70)
        touchButton.clipsToBounds = true
        touchButton.backgroundColor = .clear
        let title = "Help".localized
        touchButton.setTitle(title, for: .normal)
        touchButton.imageView?.contentMode = .scaleAspectFit
        touchButton.contentMode = .center
        touchButton.setTitleColor(UIColor.init(hex: "0321AA"), for: .normal)
//        touchButton.setImage(#imageLiteral(resourceName: "helpSupportNew.png"), for: .normal)
        touchButton.setImage(UIImage(named: "helpSpt_new1"), for: .normal)
        touchButton.alignVertical(spacing: 2.0)
        touchButton.titleLabel?.font =  UIFont(name: "Zawgyi-One", size: appFontSize)
        touchButton.autoresizingMask = []
        touchButton.titleLabel?.textAlignment = .center
        view.addSubview(touchButton)
        
        let closeBtn = UIButton()
        closeBtn.frame = CGRect(x: (self.window.frame.width / 2.0 - 30.0), y: (self.window.frame.height - 150.0), width: 80, height: 80)
        closeBtn.clipsToBounds = true
        closeBtn.backgroundColor = UIColor.clear
        closeBtn.layer.cornerRadius = 30
        closeBtn.layer.masksToBounds = true
        closeBtn.setImage(UIImage.init(named: "close"), for: .normal)
        closeBtn.autoresizingMask = []
        view.addSubview(closeBtn)
        
        self.view = view
        
        self.button = touchButton
        self.closeButton = closeBtn

        window.button = touchButton
        window.closeButton = closeBtn
        
        let panner = UIPanGestureRecognizer()
        panner.addTarget(self, action: #selector(panDidFire(panner:)))
        button.addGestureRecognizer(panner)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        snapButtonToSocket()
    }
    
    @objc func panDidFire(panner: UIPanGestureRecognizer) {
        let offset = panner.translation(in: view)
        panner.setTranslation(CGPoint.zero, in: view)
        var center = button.center
        center.x += offset.x
        center.y += offset.y
        button.center = center
        
        if panner.state == .ended || panner.state == .cancelled {
            UIView.animate(withDuration: 0.3) {
                self.window.closeButton?.isHidden = true
                if self.button.center.x < 10
                {
                    self.button.center.x = self.button.center.x + 30.0
                }
                if self.button.center.x >= (self.window.bounds.maxX - 10.0)
                {
                    self.button.center.x = self.button.center.x - 30.0
                }
                if self.button.center.y < 10  {
                    self.button.center.y = self.button.center.y + 30.0
                }
                if self.button.center.y >= (self.window.bounds.maxX - 10.0)
                {
                    self.button.center.y = self.button.center.y - 30.0
                }
                
                if (((self.window.frame.width / 2.0 - 40.0) ... (self.window.frame.width / 2.0 + 40.0)) ~= self.button.center.x) && (((self.window.frame.height - 150.0) ... (self.window.frame.height - 70.0)) ~= self.button.center.y) {
                    self.window.isHidden = true
                    UserDefaults.standard.set(true, forKey: "HelpButtonHideShow")
                } else {
                    self.window.isHidden = false
                    UserDefaults.standard.set(false, forKey: "HelpButtonHideShow")
                }
            }
        } else {
            window.closeButton?.isHidden = false
        }
    }
    
    @objc func keyboardDidShow(note: NSNotification) {
        window.windowLevel = UIWindow.Level(rawValue: 0)
        window.windowLevel = UIWindow.Level(rawValue: CGFloat.greatestFiniteMagnitude)
    }
    
    private func snapButtonToSocket() {
        var bestSocket = CGPoint.zero
        var distanceToBestSocket = CGFloat.infinity
        let center = button.center
        for socket in sockets {
            let distance = hypot(center.x - socket.x, center.y - socket.y)
            if distance < distanceToBestSocket {
                distanceToBestSocket = distance
                bestSocket = socket
            }
        }
        button.center =  bestSocket
    }
    
    private var sockets: [CGPoint] {
        let buttonSize = button.bounds.size
        let rect = view.bounds.insetBy(dx: 50 + buttonSize.width / 2, dy: 30 + buttonSize.height / 2)
        let sockets: [CGPoint] = [
            CGPoint(x: rect.minX, y: rect.minY),
//            CGPoint(x: rect.minX, y: rect.maxY),
//            CGPoint(x: rect.maxX, y: rect.minY),
//            CGPoint(x: rect.maxX, y: rect.maxY),
//            CGPoint(x: rect.midX, y: rect.midY),
//            CGPoint(x: 0, y: rect.minY),
//            CGPoint(x: 0, y: rect.maxY),
//            CGPoint(x: rect.minX, y: 0),
//            CGPoint(x: rect.maxX, y: 0)
            ]
        return sockets
    }
    
}

 class FloatingButtonWindow: UIWindow {
    
    var button: UIButton?
    var closeButton : UIButton?
    {
        didSet {
            self.closeButton?.isHidden = true
        }
    }
    
    init() {
        super.init(frame: UIScreen.main.bounds)
        backgroundColor = nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
     override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        guard let button = button else { return false }
        let buttonPoint = convert(point, to: button)
        return button.point(inside: buttonPoint, with: event)
    }
    
}

