//
//  CallBackViewController.swift
//  OK
//
//  Created by Vinod's MacBookPro on 12/6/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class CallBackViewController: HelpandSupportBaseViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UITextViewDelegate{
   
    @IBOutlet weak var commentTextView: UITextView! {
        didSet {
                self.commentTextView.text = "Comments (Max 160 Char)".localized
        }
    }
    @IBOutlet weak var commentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var commentView: UIView! {
        didSet {
            self.commentView.isHidden = true
        }
    }
    
    @IBOutlet weak var transIdTxt : FloatLabelTextField! {
        didSet {
            self.transIdTxt.placeholder = "Enter Transaction ID".localized
            if self.transIdTxt.isFirstResponder != true {
                self.transIdTxt.titleTextColour = UIColor.blue
            }
        }
    }
    @IBOutlet weak var transIdView : UIView! {
        didSet {
            self.transIdView.isHidden = true
        }
    }
    
    @IBOutlet weak var submitButton: UIButton! {
        didSet {
            self.submitButton.setTitle("Submit".localized, for: .normal)
            self.submitButton.isHidden = true
        }
    }
    
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var templateBtnOutlet: UIButton! {
        didSet {
                self.templateBtnOutlet.setTitle("Select Issue template".localized, for: .normal)
        }
    }
    
    @IBOutlet weak var tableParentView: UIView! {
        didSet {
            self.tableParentView.isHidden = true
        }
    }
    
    @IBOutlet weak var transIdViewHeight: NSLayoutConstraint!
    
    var issueEnglishName = ""
    var arrayId = [String]()
    var subCategory : callBackModel?
    var navFrom = ""
    
    var submitView: UIView {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 50))
        let btn = UIButton.init(frame: view.frame)
        btn.backgroundColor = kYellowColor
        btn.setTitle("Submit".localized, for: .normal)
        if let myFont = UIFont(name: appFont, size: 18) {
            btn.titleLabel?.font =  myFont
        }
        btn.addTarget(self, action: #selector(submitAction(_:)), for: UIControl.Event.touchUpInside)
        view.addSubview(btn)
        return view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isHidden = true
        commentTextView.delegate = self
        commentTextView.textColor = UIColor.lightGray
        self.setNavigation()
        
        self.transIdTxt.addTarget(self, action: #selector(CallBackViewController.textFieldDidChange(_:)),
                                              for: UIControl.Event.editingChanged)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]

        let viewBottom = UIView()
        viewBottom.frame = CGRect(x : 0, y : tableView.frame.size.height + tableView.frame.origin.y, width : tableView.frame.size.width, height : 2.0)
        viewBottom.backgroundColor = UIColor.init(hex: "E6E6E6")
        tableView.tableFooterView = viewBottom
        self.setUpAccessoryView()
    }
    
    
    func setNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "tabBarBack").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(backActionCallBack), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        if appDel.getSelectedLanguage() == "my" {
            label.text = subCategory?.issueTypeInBurmese ?? "CALL BACK".localized + " "
        }
        else if appDel.getSelectedLanguage() == "uni" {
            label.text = subCategory?.issueTypeInUniCode ?? "CALL BACK".localized + " "
        }
        else {
            label.text = subCategory?.issueType ?? "CALL BACK".localized + " "
        }
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
    
    @objc func backActionCallBack() {
        if navFrom == "DashBoardVC" {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = subCategory?.issueSubCategoryType.count {
            return count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath) as! TemplateCell
        if appDel.currentLanguage == "my" {
            cell.loadCell(questionName: (subCategory?.issueSubCategoryTypeInBurmese[indexPath.row])!)
        }
        else if appDel.currentLanguage == "uni" {
            cell.loadCell(questionName: (subCategory?.issueSubCategoryTypeInUniCode[indexPath.row])!)
        }
        else {
            cell.loadCell(questionName: (subCategory?.issueSubCategoryType[indexPath.row])!)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if appDel.currentLanguage == "my" {
            let issueName = (subCategory?.issueSubCategoryTypeInBurmese[indexPath.row])!
            templateBtnOutlet.setTitle(issueName, for: .normal)
        }
        else if appDel.currentLanguage == "uni" {
            let issueName = (subCategory?.issueSubCategoryTypeInUniCode[indexPath.row])!
            templateBtnOutlet.setTitle(issueName, for: .normal)
        }
        else {
             let issueName = (subCategory?.issueSubCategoryType[indexPath.row])!
                           templateBtnOutlet.setTitle(issueName, for: .normal)
        }

        issueEnglishName = (subCategory?.issueSubCategoryType[indexPath.row])!
        self.view.backgroundColor = UIColor.init(hex: "E6E6E6")
        self.tableParentView.isHidden = true
        tableView.isHidden = true
        if (subCategory?.issueSubCategoryTypeTransId[indexPath.row])! == true {
            self.transIdViewHeight.constant = 60.0
            transIdView.isHidden = false
            self.commentView.isHidden = true
        } else {
            transIdView.isHidden = true
            self.transIdViewHeight.constant = 0
            self.commentView.isHidden = false
        }
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }

    }

    @IBAction func templateBtnAction(_ sender: Any) {
        if let count = subCategory?.issueSubCategoryType.count {
            println_debug(count)
            if tableView.isHidden == true {
                self.tableParentView.isHidden = false
                tableView.isHidden = false
            } else {
                self.tableParentView.isHidden = true
                tableView.isHidden = true
            }
        } else {
            self.showErrorAlert(errMessage: "No listed issues found".localized)
        }
        self.commentTextView.text = nil
        self.transIdTxt.text = ""
        self.transIdViewHeight.constant = 0
        self.commentView.isHidden = true
        self.commentTextView.text = "Comments (Max 160 Char)".localized
        self.commentTextView.textColor = UIColor.lightGray
        self.commentTextView.resignFirstResponder()
        self.submitButton.isHidden = true
    }
    
    @IBAction func submitAction(_ sender: Any) {
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            let url = getUrl(urlStr: Url.submitQueryApi, serverType: .helpandSupportUrl)
            //println_debug(url)
            
            let request = NSMutableURLRequest(url:url as URL);
            request.httpMethod = "POST";
            
           // let comment = "Transaction Id = \(transIdTxt.text!), \(commentTextView.text!)"
            let comment =  "\(commentTextView.text!)"
            let transID = transIdTxt.text
            
            var ticketRequest = [String:Any]()
            ticketRequest["CustomerPhone"] = UserModel.shared.mobileNo
            ticketRequest["CustomerDeviceId"] = uuid
            ticketRequest["CustomerName"] = UserModel.shared.name
            ticketRequest["IssueCategory"] =  (subCategory?.issueType)
            ticketRequest["IssueSubCategory" ] =  issueEnglishName
            ticketRequest["OkHelpTransactionId" ] =  transID
            ticketRequest["Comments"] =  comment
            
            let param1 = self.JSONStringFromAnyObject(value: ticketRequest as AnyObject)
            let dict = ["TicketRequest" : param1]
            
            let boundary = "Boundary-\(uuid)"
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            let imageName = self.getImagePathFromDocDirectory()
            var imageData = Data()
            if imageName != "" {
                let img = UIImage(contentsOfFile: imageName)
                imageData = img!.jpegData(compressionQuality: 1)!
            }
            
            request.httpBody = createBodyWithParameters(parameters: dict, filePathKey: imageName, imageDataKey: imageData as NSData, boundary: boundary) as Data
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                (data, response, error) in
                
                if error != nil {
                    return
                }
                do {
                    if let data = data {
                        let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                        if let dic = dict as? Dictionary<String,AnyObject> {
                            //println_debug(dic)
                            
                            if let statusCode = dic["StatusCode"] as? Int {
                                if statusCode == 200 {
                                    self.showAlertAndDismiss(statusCode : statusCode)
                                } else {
                                    self.showAlertAndDismiss(statusCode : statusCode)
                                }
                            }
                        }
                    }
                    DispatchQueue.main.async {
                        progressViewObj.removeProgressView()
                    }
                } catch {
                    DispatchQueue.main.async {
                        progressViewObj.removeProgressView()
                    }
                    //println_debug(error)
                }
            }
            task.resume()
        } else {
            self.noInternetAlert()
        }
    }
    
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        var body = Data()
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.append(Data("--\(boundary)\r\n".utf8))
                body.append(Data("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".utf8))
                body.append(Data("\(value)\r\n".utf8))
            }
        }
        
        var filename = ""
        if filePathKey != "" {
            filename = "screenShot.jpg"
        }
        
        let mimetype = "image/jpg"
        
        body.append(Data("--\(boundary)\r\n".utf8))
        body.append(Data("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n".utf8))
        body.append(Data("Content-Type: \(mimetype)\r\n\r\n".utf8))
        body.append(imageDataKey as Data)
        body.append(Data("\r\n".utf8))
        body.append(Data("--\(boundary)--\r\n".utf8))
        return body as NSData
    }
    
    func showAlertAndDismiss(statusCode : Int) {
        DispatchQueue.main.async {
            if statusCode == 200 {
                if let alertView = UIStoryboard(name: "HelpSupport", bundle: nil).instantiateViewController(withIdentifier: "SuccessStatusAlertView_ID") as? SuccessStatusAlertView {
                    alertView.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    alertView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    alertView.delegate = self
                    self.present(alertView, animated: true, completion: nil)
                }
            } else {
                self.showErrorAlert(errMessage: appDel.getlocaLizationLanguage(key: "Something went wrong. Please try again!"))
            }
        }
    }
    
    fileprivate func setUpAccessoryView() {
        commentTextView.inputAccessoryView = self.submitView
        commentTextView.inputAccessoryView?.isHidden = true
    }
    
    
    fileprivate func showAccessoryView(status : Bool) {
        if status {
            commentTextView.inputAccessoryView?.isHidden = false
        } else {
            commentTextView.inputAccessoryView?.isHidden = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.length > 0 {
            self.showAccessoryView(status : true)
            self.submitButton.isHidden = false
        } else {
            self.showAccessoryView(status : false)
            self.submitButton.isHidden = true
        }
    }
    
    internal func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            self.commentTextView.text = "Comments (Max 160 Char)".localized
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
        }
        return true
    }
    
    //TextFieldDelegate
    @objc private func textFieldDidChange(_ textField: UITextField) {
        //fixed text field with two prefix 09
        if(textField.text!.count > 0) {
            textField.placeholder = "Transaction ID".localized
        } else {
            textField.placeholder = "Enter Transaction ID".localized
        }
        if(textField.text!.count > 4) {
            self.commentView.isHidden = false
        } else {
            self.commentView.isHidden = true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        transIdTxt.titleTextColour = UIColor.init(hex: "0F70FF")
    }
    
   fileprivate func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    fileprivate func getImagePathFromDocDirectory() -> String {
        let fileManager = FileManager.default
        let imagePAth = (self.getDirectoryPath() as NSString).appendingPathComponent("screenShot.jpg")
        if fileManager.fileExists(atPath: imagePAth){
            return imagePAth
        } else {
           return ""
        }
    }
    fileprivate func deleteDirectory() {
        let fileManager = FileManager.default
        let paths = (self.getDirectoryPath() as NSString).appendingPathComponent("screenShot.jpg")
        if fileManager.fileExists(atPath: paths){
            try! fileManager.removeItem(atPath: paths)
        } else {
            println_debug("Something wrong.")
        }
    }
}

class TemplateCell : UITableViewCell {
    
    @IBOutlet weak var questionLabel : UILabel!
    override func awakeFromNib() {
        
    }
    
    let appDel = UIApplication.shared.delegate as! AppDelegate
    func loadCell(questionName : String) {
        questionLabel.text = questionName
        questionLabel.font = UIFont(name: appFont, size: 16.0)
    }
}

extension CallBackViewController : TicketStatusProtocol
{
    func dismissCreateViewController() {
        self.deleteDirectory()
        self.backActionCallBack()
    }
}
