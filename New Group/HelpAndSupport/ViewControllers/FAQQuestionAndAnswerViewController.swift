//
//  FAQQuestionAndAnswerViewController.swift
//  OK
//
//  Created by Mohit on 3/12/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

struct FAQQuesAndAnswerModel {
    var question : String?
    var questionInBurmeese : String?
    var questionInUniCode : String?
    var answer : String?
    var answerInBurmeese : String?
    var answerInUniCode : String?
    var isDeleted : Bool?
    init(dict : Dictionary<String , Any>) {
        self.question = dict["Question"] as? String ?? ""
        self.answer = dict["Answer"] as? String ?? ""
        self.questionInBurmeese = dict["QuestionInBurmese"] as? String ?? ""
        self.answerInBurmeese = dict["AnswerInBurmese"] as? String ?? ""
        self.questionInUniCode = dict["QuestionInUniCode"] as? String ?? ""
        self.answerInUniCode = dict["AnswerInUniCode"] as? String ?? ""
        self.isDeleted = dict["IsDeleted"] as? Bool ?? false
    }
}

class FAQQuestionAndAnswerViewController: HelpandSupportBaseViewController {
    
    @IBOutlet weak var answerTableView : UITableView!
    @IBOutlet weak var noListedLbl : UILabel! {
        didSet {
            self.noListedLbl.isHidden = true
        }
    }
    var isCellClicked : [Bool] = [false]
    var quesAnswerModelArray = [FAQQuesAndAnswerModel]()
    var heightArray = [CGFloat]()
    var selectedFAQ = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "FAQ".localized
        self.loadBackHelpAndSupportButton()
        self.loadNavigationBarSettings()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        answerTableView.tableFooterView = UIView()
        getQuestionAndAnswer()
        answerTableView.translatesAutoresizingMaskIntoConstraints = false
        noListedLbl.text = "No records found!".localized
    }
   
    func getQuestionAndAnswer() {
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            let url = getUrl(urlStr: Url.getQuestionAnswerByTagApi, serverType: .helpandSupportUrl)
            
            self.getResponse(param : selectedFAQ, url : url, handle: { (response, success) in
                if success {
                    do {
                        if let data = response as? Data {
                            let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                            if let dic = dict as? Dictionary<String,AnyObject> {
                                if let questionArray = dic["Content"] as? [Dictionary<String,Any>] {
                                    for question in questionArray {
                                        self.quesAnswerModelArray.append(FAQQuesAndAnswerModel.init(dict: question))
                                    }
                                    if self.quesAnswerModelArray.count > 0 {
                                        DispatchQueue.main.async {
                                            if appDel.currentLanguage != "my" {
                                                for answer in self.quesAnswerModelArray {
                                                    self.calculateHeight(answer: answer.answer!)
                                                    self.isCellClicked.append(false)
                                                }
                                            } else {
                                                for answer in self.quesAnswerModelArray {
                                                    self.calculateHeight(answer: answer.answerInBurmeese!)
                                                    self.isCellClicked.append(false)
                                                }
                                            }
                                            progressViewObj.removeProgressView()
                                            self.answerTableView.reloadData()
                                        }
                                    }
                                }
                            }
                        }
                        DispatchQueue.main.async {
                            if self.quesAnswerModelArray.count > 0 {
                                self.noListedLbl.isHidden = true
                            } else {
                                self.noListedLbl.isHidden = false
                            }
                            progressViewObj.removeProgressView()
                        }
                    } catch {
                        DispatchQueue.main.async {
                            progressViewObj.removeProgressView()
                        }
                        //println_debug(error)
                    }
                } else {
                    DispatchQueue.main.async {
                        progressViewObj.removeProgressView()
                    }
                }
            })
        } else {
            self.noInternetAlert()
        }
    }
    
    func calculateHeight(answer : String) {
        var fontSize : CGFloat = 16.0
        let width = UIScreen.main.bounds.width
        if width > 325 {
            fontSize = 16.0
        } else {
            fontSize = 13.0
        }
        DispatchQueue.main.async {
            self.heightArray.append(answer.heightWithConstrainedWidth(width: self.answerTableView.frame.size.width - 30, font: UIFont(name: appFont, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)))
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadTableCell(indexpath : IndexPath, style : String) {
        DispatchQueue.main.async {
            self.answerTableView.beginUpdates()
            if style == "top" {
                self.answerTableView.reloadRows(at: [indexpath], with: UITableView.RowAnimation.bottom)
            } else {
                self.answerTableView.reloadRows(at: [indexpath], with: UITableView.RowAnimation.top)
            }
            self.answerTableView.endUpdates()
        }
    }
}
    
extension FAQQuestionAndAnswerViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        if isCellClicked.count > indexPath.row {
            if isCellClicked[indexPath.row] == true {
                isCellClicked[indexPath.row] = false
                reloadTableCell(indexpath: indexPath, style: "bottom")
            } else {
                isCellClicked[indexPath.row] = true
                reloadTableCell(indexpath: indexPath, style: "top")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isCellClicked.count > indexPath.row {
            if isCellClicked[indexPath.row] == false {
                return 60
            } else {
                return heightArray[indexPath.row] + 85
            }
        } else {
            return 60
        }
    }
}
    
extension FAQQuestionAndAnswerViewController : UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return quesAnswerModelArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "answerCell", for: indexPath) as! QuestAndAnswerTableViewCell
        cell.arrowImage.tintColor = UIColor.lightGray
        if isCellClicked.count > indexPath.row {
            if isCellClicked[indexPath.row] == true {
                cell.arrowImage.image = UIImage(named: "ic_keyboard_arrow_up.png")!.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
            } else {
                cell.arrowImage.image = UIImage(named: "ic_keyboard_arrow_down.png")!.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
            }
            cell.loadCell(answerModel: quesAnswerModelArray[indexPath.row])
            return cell
        }
        return cell
    }
}

class QuestAndAnswerTableViewCell : UITableViewCell {
    @IBOutlet weak var questionLabel : UILabel!
    @IBOutlet weak var answerTextView : UITextView!
    @IBOutlet weak var arrowImage : UIImageView!
    
    override func awakeFromNib() {
        let width = UIScreen.main.bounds.width
        if width > 325 {
            questionLabel.font = UIFont(name: appFont, size: 16.0)
            answerTextView.font = UIFont(name: appFont, size: 16.0)
        } else {
            questionLabel.font = UIFont(name: appFont, size: 13.0)
            answerTextView.font = UIFont(name: appFont, size: 13.0)
        }
    }
    
    func loadCell(answerModel : FAQQuesAndAnswerModel) {
      
        DispatchQueue.main.async {
            let width = UIScreen.main.bounds.width
                        if width > 325 {
                            self.questionLabel.font = UIFont(name: appFont, size: 16.0)
                            self.answerTextView.font = UIFont(name: appFont, size: 16.0)
                        } else {
                            self.questionLabel.font = UIFont(name: appFont, size: 13.0)
                            self.answerTextView.font = UIFont(name: appFont, size: 13.0)
                        }
            
            if appDel.currentLanguage == "my" {
                self.questionLabel.text = answerModel.questionInBurmeese!
                self.answerTextView.text = answerModel.answerInBurmeese!
            } else if appDel.currentLanguage == "uni" {
                self.questionLabel.text = answerModel.questionInUniCode!
                self.answerTextView.text = answerModel.answerInUniCode!
            }
            else {
                self.questionLabel.text = answerModel.question!
                self.answerTextView.text = answerModel.answer!
            }
        }
    }
}

extension String {
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: font], context: nil)
        return boundingBox.height
    }
}

