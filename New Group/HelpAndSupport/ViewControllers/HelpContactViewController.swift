//
//  HelpContactViewController.swift
//  OK
//
//  Created by Vinod's MacBookPro on 12/6/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import MapKit
import MessageUI
import Rabbit_Swift

class HelpContactViewController: HelpandSupportBaseViewController, MFMessageComposeViewControllerDelegate {
    
    struct locationDetails {
        var branchName : String?
        var latitude : Double?
        var longitude : Double?
        var locationName : String?
        init(dict : Dictionary<String, Any>) {
            if let branch = dict["Source"] as? String {
                self.branchName = branch
            } else {
                self.branchName = ""
            }
            if let lat = dict["Latitude"] as? Double {
                self.latitude = lat
            } else {
                self.latitude = 0
            }
            if let longi = dict["Longitude"] as? Double {
                self.longitude = longi
            } else {
                self.longitude = 0
            }
            if let nam = dict["Name"] as? String {
                self.locationName = nam
            } else {
                self.locationName = ""
            }
        }
    }
    
    @IBOutlet weak var headOfficeLabel : UILabel!
    @IBOutlet weak var addressLabel : UILabel!
    
    @IBOutlet weak var callUsLabel : UILabel!
    @IBOutlet weak var callUsBtn1: UIButton!
    @IBOutlet weak var callUsBtn2: UIButton!
    
    @IBOutlet weak var smsUsLabel : UILabel!
    @IBOutlet weak var smsUsBtn1: UIButton!

    @IBOutlet weak var whatsAppLabel : UILabel!
    @IBOutlet weak var whatsAppBtn1: UIButton!
    
    @IBOutlet weak var mailLabel : UILabel!
    @IBOutlet weak var mailUsBtn1: UIButton!
    
    @IBOutlet weak var faceBookLabel : UILabel!
    @IBOutlet weak var facebookIdBtn1: UIButton!
    
    @IBOutlet weak var kycNumberLabel : UILabel!
    @IBOutlet weak var kycNumberBtn1: UIButton!
    @IBOutlet weak var kycNumberBtn2: UIButton!
    @IBOutlet weak var kycNumberBtn3: UIButton!
    
    @IBOutlet weak var amlNumberLabel : UILabel!
    @IBOutlet weak var amlNumberBtn1: UIButton!
    @IBOutlet weak var amlNumberBtn2: UIButton!
    @IBOutlet weak var amlNumberBtn3: UIButton!
    
    @IBOutlet weak var callBackButton : UIButton!
    @IBOutlet weak var helpFaqButton : UIButton!
    @IBOutlet weak var myQueryButton : UIButton!
    
    @IBOutlet weak var officeAddressLabel : UILabel!
    
    @IBOutlet weak var callBackLabel : UILabel!
    @IBOutlet weak var helpFaqLabel : UILabel!
    @IBOutlet weak var myQuerylabel : UILabel!
    
    let appDel = UIApplication.shared.delegate as! AppDelegate
    let geoLocManager    = GeoLocationManager.shared
    @IBOutlet weak var helpSupportMapView : MKMapView!
    var locationManager: CLLocationManager!
    let clusteringManager = FBClusteringManager()
    let regionRadius: CLLocationDistance = 5000
    @IBOutlet weak var currentAddressLbl : UILabel!
    var names = [String]()
    fileprivate var currentAddressName = ""
    fileprivate var currentLat = "0.0"
    fileprivate var CurrentLong = "0.0"
    @IBOutlet weak var mapViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollViewHeight: NSLayoutConstraint!
    
    var zoomState = false
    var searchLocationArray : [locationDetails] = []
    
    var annotations : Array = [Artwork]()
    
    @IBOutlet weak var mapZoomButton : UIButton! {
        didSet {
            mapZoomButton.layer.masksToBounds = true
            self.mapZoomButton.backgroundColor = UIColor.clear
            mapZoomButton.layer.cornerRadius = self.mapZoomButton.frame.size.width / 2.0
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Help & Support".localized
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "tabBarBack"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(dismissBackAction), for: .touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 50, height: 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        if #available(iOS 11.0, *) {
          //  helpSupportMapView.register(ArtworkMarkerView.self,
                                     //   forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        } else {
            // Fallback on earlier versions
        }
        
        self.loadNavigationBarSettings()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20.0)]
        helpSupportMapView.delegate = self
//        helpSupportMapView.showsUserLocation = true
        geoLocManager.startUpdateLocation()
        progressViewObj.showProgressView()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.startGeoLocation()
            self.getContactDetails()
        }
        zoomState = false
        setUpLocalization()
        setFont()
        
        addMapTrackingButton()
    }
    
    fileprivate func setUpLocalization() {
        officeAddressLabel.text = "Branch Office".localized + " :- "
        callBackLabel.text  = "CALL BACK".localized
        helpFaqLabel.text  = "Help & FAQ".localized
        myQuerylabel.text  = "My Query".localized
    }
    
    @objc func dismissBackAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func setFont() {
       let width = UIScreen.main.bounds.width
        if width > 325 {
            addressLabel.font = UIFont(name: appFont, size: 16.0)
            officeAddressLabel.font = UIFont(name: appFont, size: 16.0)
            callBackLabel.font = UIFont(name: appFont, size: 16.0)
            helpFaqLabel.font = UIFont(name: appFont, size: 16.0)
            myQuerylabel.font = UIFont(name: appFont, size: 16.0)
            facebookIdBtn1.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
            //Font update-Gauri-Aug07
            headOfficeLabel.font = UIFont(name: appFont, size: 16.0)
            callUsLabel.font = UIFont(name: appFont, size: 16.0)
            smsUsLabel.font = UIFont(name: appFont, size: 16.0)
            whatsAppLabel.font = UIFont(name: appFont, size: 16.0)
            mailLabel.font = UIFont(name: appFont, size: 16.0)
            faceBookLabel.font = UIFont(name: appFont, size: 16.0)
            kycNumberLabel.font = UIFont(name: appFont, size: 16.0)
            amlNumberLabel.font = UIFont(name: appFont, size: 16.0)
        } else {
            addressLabel.font = UIFont(name: appFont, size: 14.0)
            officeAddressLabel.font = UIFont(name: appFont, size: 14.0)
            callBackLabel.font = UIFont(name: appFont, size: 14.0)
            helpFaqLabel.font = UIFont(name: appFont, size: 14.0)
            myQuerylabel.font = UIFont(name: appFont, size: 14.0)
            facebookIdBtn1.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
            //Font update-Gauri-Aug07
            headOfficeLabel.font = UIFont(name: appFont, size: appButtonSize)
            callUsLabel.font = UIFont(name: appFont, size: 14.0)
            smsUsLabel.font = UIFont(name: appFont, size: 14.0)
            whatsAppLabel.font = UIFont(name: appFont, size: 14.0)
            mailLabel.font = UIFont(name: appFont, size: 14.0)
            faceBookLabel.font = UIFont(name: appFont, size: 14.0)
            kycNumberLabel.font = UIFont(name: appFont, size: 14.0)
            amlNumberLabel.font = UIFont(name: appFont, size: 14.0)
        }
    }
    
    //map locations
    func startGeoLocation() {
        CurrentLong = geoLocManager.currentLongitude ?? "0.0"
        currentLat = geoLocManager.currentLatitude ?? "0.0"
        let initialLocation = CLLocation(latitude: Double(currentLat)!, longitude: Double(CurrentLong)!)
        centerMapOnLocation(location: initialLocation)
        
        if let lat = geoLocManager.currentLatitude, let long = geoLocManager.currentLongitude {
            geoLocManager.getAddressFrom(lattitude: lat, longitude: long, language: appDel.currentLanguage, completionHandler: { (success, address) in
                guard success else {
                    println_debug("something went wrong when get current address from google api")
                    DispatchQueue.main.async {
                        progressViewObj.removeProgressView()
                    }
                    return
                }
                DispatchQueue.main.async {
                    if let adrs = address {
                        if let dictionary = adrs as? Dictionary<String,Any> {
                            let street = dictionary.safeValueForKey("street") as? String ?? ""
                            let township = dictionary.safeValueForKey("township") as? String ?? ""
                            let city = dictionary.safeValueForKey("city") as? String ?? ""
                            let region = dictionary.safeValueForKey("region") as? String ?? ""
                            var curAddress = ""
                            if self.appDel.currentLanguage == "uni" {
                                curAddress = String.init(format: "%@, %@, %@, %@", street, township, city, region)
                            } else {
                             curAddress = Rabbit.uni2zg(String.init(format: "%@, %@, %@, %@", street, township, city, region))
                            }
                            self.currentAddressLbl.text = curAddress
                            self.currentAddressName = curAddress
                            self.officeAddressLabel.text = "Branch Office".localized + " :- " + curAddress
                            let artWork = Artwork(title: " ",
                                                  locationName: curAddress,
                                                  discipline: "",
                                                  coordinate: CLLocationCoordinate2D(latitude: Double(self.currentLat)!, longitude: Double(self.CurrentLong)!))
                            self.annotations.append(artWork)
                        }
                    } else {
                        self.currentAddressLbl.text = "Loading location.....".localized
                    }
                    progressViewObj.removeProgressView()
                }
            })
        } else {
            progressViewObj.removeProgressView()
        }
        self.getLocationDetails()
    }
    
    @IBAction func mapZoomButtonAction(_ sender : UIButton) {
     
        if zoomState == false {
            zoomState = true
            sender.setImage(UIImage(named: "zoom_out"), for: .normal)
           if UIScreen.main.bounds.size.height >= 812{
            self.mapViewHeight.constant = self.view.frame.size.height - 200
           }else{
            self.mapViewHeight.constant = self.view.frame.size.height - 160
            }
            
            
            self.scrollViewHeight.constant = 920 + self.view.frame.size.height
//            UIView.animate(withDuration: 0.5) {
//                self.view.layoutIfNeeded()
//            }
        } else {
            zoomState = false
            sender.setImage(UIImage(named: "zoom_in"), for: .normal)
            self.mapViewHeight.constant = 250
            self.scrollViewHeight.constant = 1340
//            UIView.animate(withDuration: 0.5) {
//                self.view.layoutIfNeeded()
//            }
        }
    }
    
    private func getLocationDetails() {
        self.searchLocationArray.removeAll()
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self
            let url = getUrl(urlStr: Url.searchLocations, serverType: .businessLocationUrl)
            //println_debug(url)
            progressViewObj.showProgressView()
            var params = Dictionary<String,String>()
            var lang = "en"
            if appDel.currentLanguage == "my" {
                lang = "mm"
            }
            else if appDel.currentLanguage == "uni" {
                lang = "uni"
            }
            else {
                lang = "en"
            }
            params = ["Latitude": currentLat , "Longitude": CurrentLong, "Language" : lang]
            web.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", mScreen: "mHelpContact")
        }
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion.init(center: location.coordinate,
                                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        helpSupportMapView.setRegion(coordinateRegion, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func callUsAction(_ sender: UIButton) {
        let callURL = URL(string: "telprompt://\(sender.titleLabel?.text ?? "")")
        UIApplication.shared.open(callURL!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
    }
    
    @IBAction func smsAction(_ sender: UIButton) {
        let phNo = sender.titleLabel?.text?.replacingOccurrences(of: "-", with: "") ?? ""
        if !MFMessageComposeViewController.canSendText() {
            println_debug("SMS services are not available")
        }
        let composeVC = MFMessageComposeViewController()
        composeVC.messageComposeDelegate = self
        composeVC.recipients = [phNo]
        composeVC.body = "Welcome To OK Dollar!"
        self.present(composeVC, animated: true, completion: nil)
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func whatsAppAction(_ sender: UIButton) {
        let phNo = sender.titleLabel?.text?.replacingOccurrences(of: "-", with: "") ?? ""
        let urlWhats = "whatsapp://send?phone=\(phNo)&abid=12354&text=Hello"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL) {
                    UIApplication.shared.open(whatsappURL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                } else {
                    alertViewObj.wrapAlert(title: "", body: "Whatsapp is not installed on your device".localized, img: nil)
                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                    })
                    alertViewObj.showAlert(controller: self)
                    //println_debug("Install Whatsapp")
                }
            }
        }
    }
    
    @IBAction func mailAction(_ sender: UIButton) {
        let email = sender.titleLabel?.text ?? ""
        if let url = URL(string: "mailto:\(email)") {
            UIApplication.shared.open(url)
        } else {
            alertViewObj.wrapAlert(title: "", body: "No Mail Configured".localized, img: nil)
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                
            })
            alertViewObj.showAlert(controller: self)
            //println_debug("No Mail Configured")
        }
    }
    
    @IBAction func faceBookAction(_ sender: UIButton) {
        let url = URL(string: sender.titleLabel?.text ?? "")
        UIApplication.shared.open(url!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
    }
    
    @IBAction func helpnFaqAction(_ sender: Any) {
        let viewController = UIStoryboard(name: "HelpSupport", bundle: Bundle.main).instantiateViewController(withIdentifier: "HelpFAQViewController") as? HelpFAQViewController
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
    @IBAction func myQueryAction(_ sender: Any) {
        
    }
    
    func addAnnotationsInMap() {
        for annotation in searchLocationArray {
            //let title = annotation.branchName ?? ""
            let long = annotation.longitude ?? 0
            let lat = annotation.latitude ?? 0
            let userData = annotation.locationName ?? ""
            let annotation = MKPointAnnotation()
            annotation.title = userData
            annotation.coordinate = CLLocationCoordinate2D(latitude:  lat, longitude: long)
            let artwork = Artwork(title: " ",
                                  locationName: userData,
                                  discipline: "Monument",
                                  coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long))
            annotations.append(artwork)
        }
        reloadMap()
    }
    
    func reloadMap() {
        DispatchQueue.main.async {
            self.helpSupportMapView.addAnnotations(self.annotations)
        }
    }
   
    deinit {
        geoLocManager.stopUpdateLocation()
    }
    
    fileprivate func updateOfficeDetails(dictionary: Dictionary<String, Any>) {
        if let headerDetails = dictionary["HeadOfficeDetails"] as? Dictionary<String, Any> {
            self.headOfficeLabel.text = "Head Office".localized
            self.addressLabel.text = (headerDetails["Location"] as? String ?? "").localized
        }
        
        if let contentDetailsArray = dictionary["ContactDetails"] as? [Dictionary<String, Any>] {
            if contentDetailsArray.count > 0 {
             let callusDict = contentDetailsArray[0]
                self.callUsLabel.text = (callusDict["Type"] as? String ?? "").localized
                if let phNumArray = callusDict["Values"] as? [String] {
                    if phNumArray.count > 0 {
                        self.callUsBtn1.setTitle(phNumArray[0], for: .normal)
                    }
                    if phNumArray.count > 1 {
                        self.callUsBtn2.setTitle(phNumArray[1], for: .normal)
                    }
                }
            }
            
            if contentDetailsArray.count > 1 {
                let callusDict = contentDetailsArray[1]
                self.smsUsLabel.text = (callusDict["Type"] as? String ?? "").localized
                if let phNumArray = callusDict["Values"] as? [String] {
                    if phNumArray.count > 0 {
                        self.smsUsBtn1.setTitle(phNumArray[0], for: .normal)
                    }
                }
            }

            if contentDetailsArray.count > 2 {
                let callusDict = contentDetailsArray[2]
                self.whatsAppLabel.text = (callusDict["Type"] as? String ?? "").localized
                if let phNumArray = callusDict["Values"] as? [String] {
                    if phNumArray.count > 0 {
                        self.whatsAppBtn1.setTitle(phNumArray[0], for: .normal)
                    }
                }
            }

            if contentDetailsArray.count > 3 {
                let callusDict = contentDetailsArray[3]
                self.mailLabel.text = (callusDict["Type"] as? String ?? "").localized
                if let phNumArray = callusDict["Values"] as? [String] {
                    if phNumArray.count > 0 {
                        self.mailUsBtn1.setTitle(phNumArray[0], for: .normal)
                    }
                }
            }

            if contentDetailsArray.count > 4 {
                let callusDict = contentDetailsArray[4]
                self.faceBookLabel.text = (callusDict["Type"] as? String ?? "").localized
                if let _ = callusDict["Values"] as? [String] {
                    //self.facebookIdBtn1.setTitle("https://www.facebook.com/okdollarapp", for: .normal)
                }
            }

            if contentDetailsArray.count > 5 {
                let callusDict = contentDetailsArray[5]
                self.kycNumberLabel.text = (callusDict["Type"] as? String ?? "").localized
                if let phNumArray = callusDict["Values"] as? [String] {
                    if phNumArray.count > 0 {
                        self.kycNumberBtn1.setTitle(phNumArray[0], for: .normal)
                    }
                    if phNumArray.count > 1 {
                        self.kycNumberBtn2.setTitle(phNumArray[1], for: .normal)
                    }
                    if phNumArray.count > 2 {
                        self.kycNumberBtn3.setTitle(phNumArray[2], for: .normal)
                    }
                }
            }

            if contentDetailsArray.count > 6 {
                let callusDict = contentDetailsArray[6]
                self.amlNumberLabel.text = (callusDict["Type"] as? String ?? "").localized
                if let phNumArray = callusDict["Values"] as? [String] {
                    if phNumArray.count > 0 {
                        self.amlNumberBtn1.setTitle(phNumArray[0], for: .normal)
                    }
                    if phNumArray.count > 1 {
                        self.amlNumberBtn2.setTitle(phNumArray[1], for: .normal)
                    }
                    if phNumArray.count > 2 {
                        self.amlNumberBtn3.setTitle(phNumArray[2], for: .normal)
                    }
                }
            }
        }
        progressViewObj.removeProgressView()
    }
    
 
}

extension HelpContactViewController : UIScrollViewDelegate {
    /*
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let scrollOffsetY = scrollView.contentOffset.y;
        
        if(scrollOffsetY < -24)
        {
            self.mapZoomButton.setTitle("-", for: .normal)
            self.mapViewHeight.constant = self.view.frame.size.height - 120
            self.scrollViewHeight.constant = 670 + self.view.frame.size.height
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
        else if(scrollOffsetY > 25)
        {
            self.mapZoomButton.setTitle("+", for: .normal)
            self.mapViewHeight.constant = 250
            self.scrollViewHeight.constant = 1040
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
        else if (scrollOffsetY == -24)
        {
            self.mapZoomButton.setTitle("+", for: .normal)
            self.mapViewHeight.constant = 250
            self.scrollViewHeight.constant = 1040
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        } else {
            
        }
    }
 */
}

extension HelpContactViewController {
    fileprivate func getContactDetails() {
        if appDel.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self
            let url = getUrl(urlStr: "/BussinessLocationService/SearchForLocations", serverType: .cashInCashOutUrl)
            
            var lang = "mm"//(appDel.currentLanguage == "my") ?  "mm" : "en"
            
            if appDel.currentLanguage == "my" {
                lang = "mm"
            } else if appDel.currentLanguage == "uni" {
                lang = "uni"
            }
            else {
                lang = "en"
            }
            
            let paramsDict = ["Latitude" : currentLat, "Longitude": CurrentLong, "Language" : lang]
            
            web.genericClass(url: url, param: paramsDict as AnyObject , httpMethod: "POST", mScreen: "HelpSupport")
            
        } else {
            PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.noInternet.localized)
        }
    }
    
    func handleResponse(data: Any) {
        guard let successData = data as? Data else {
            progressViewObj.removeProgressView()
            return }
        do {
            guard let dictionary = try JSONSerialization.jsonObject(with: successData, options: .allowFragments) as? Dictionary<String, Any> else {
                progressViewObj.removeProgressView()
                return }
            if let statusCode = dictionary["StatusCode"] as? Int, statusCode == 200 {
                guard let contentArray = dictionary["Content"] as? Array<Any>, let officeDetails = contentArray.first as? Dictionary<String, Any> else { progressViewObj.removeProgressView()
                    return
                }
                DispatchQueue.main.async {
                    self.updateOfficeDetails(dictionary: officeDetails)
                }
            } else {
                progressViewObj.removeProgressView()
            }
        } catch _ {
            progressViewObj.removeProgressView()
        }
    }
}

extension HelpContactViewController: WebServiceResponseDelegate {
    func webResponse(withJson json: AnyObject, screen: String) {
        if screen == "HelpSupport" {
            handleResponse(data: json)
        } else if  screen == "mHelpContact" {
            do {
                if let data = json as? Data,
                    let json1 = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                    let contentArray = json1["Content"] as? [[String: Any]] {
                    for content in contentArray {
                        self.searchLocationArray.append(locationDetails.init(dict: content["Location"] as! Dictionary<String, Any>))
                    }
                }
                self.addAnnotationsInMap()
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                }
            } catch {
                println_debug("Error deserializing JSON: \(error)")
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                }
            }
        }
    }
    
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        DispatchQueue.main.async {
            progressViewObj.removeProgressView()
        }
    }
}

extension HelpContactViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        reloadMap()
    }
}

class Artwork: NSObject, MKAnnotation {
    let title: String?
    let locationName: String
    let discipline: String
    let coordinate: CLLocationCoordinate2D
    
    var markerTintColor: UIColor  {
        switch discipline {
        case "Monument":
            return .red
        default:
            return .green
        }
    }
    
    init(title: String, locationName: String, discipline: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.locationName = locationName
        self.discipline = discipline
        self.coordinate = coordinate
        super.init()
    }
    
    var subtitle: String? {
        return locationName
    }
}

@available(iOS 11.0, *)
class ArtworkMarkerView: MKMarkerAnnotationView {
    override var annotation: MKAnnotation? {
        willSet {
            guard let artwork = newValue as? Artwork else { return }
            canShowCallout = true
            markerTintColor = artwork.markerTintColor
            let detailLabel = UILabel()
            detailLabel.numberOfLines = 0
            detailLabel.font = UIFont(name: appFont, size: 13.0)
            detailLabel.text = artwork.subtitle
            detailCalloutAccessoryView = detailLabel
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}


// MARK: Map Center Button
extension HelpContactViewController {
    
    func addMapTrackingButton(){
        let mapCenterButton = UIButton.init(type: .custom)
        mapCenterButton.frame = CGRect(origin: CGPoint(x: UIScreen.main.bounds.width - 50 , y: 25), size: CGSize(width: 35, height: 35))
        mapCenterButton.backgroundColor = .white
        mapCenterButton.roundedButton()
        mapCenterButton.setImage(UIImage.init(named: "mapCenter.png"), for: UIControl.State.normal)
        mapCenterButton.addTarget(self, action: #selector(self.centerMapOnUserButtonClicked), for:.touchUpInside)
        helpSupportMapView.addSubview(mapCenterButton)
    }
    
    @objc func centerMapOnUserButtonClicked() {
        helpSupportMapView.setUserTrackingMode(MKUserTrackingMode.follow, animated: true)
    }
    
    
}
