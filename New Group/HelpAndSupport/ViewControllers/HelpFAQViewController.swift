//
//  HelpFAQViewController.swift
//  OK
//
//  Created by Vinod's MacBookPro on 12/7/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

struct fAQQuestionModel {
    var tagName : String?
    var tagNameInBurmese : String?
    var tagNameInUniCode : String?
    var isDeleted : Bool?
    var createdDate : String?
    var modifiedDate : String?
    var createdBy : String?
    var id : String?
    var transactionId : String?
    
    init(dict : Dictionary<String, Any>) {
        self.tagName = dict["TagName"] as? String ?? ""
        self.tagNameInBurmese = dict["TagNameInBurmese"] as? String ?? ""
        self.tagNameInUniCode = dict["TagNameInUniCode"] as? String ?? ""
        self.isDeleted = dict["IsDeleted"] as? Bool ?? false
        self.createdDate = dict["CreatedDate"] as? String ?? ""
        self.modifiedDate = dict["ModifiedDate"] as? String ?? ""
        self.createdBy = dict["CreatedBy"] as? String ?? ""
        self.id = dict["Id"] as? String ?? ""
        self.transactionId = dict["TransactionId"] as? String ?? ""
    }
}

class HelpFAQViewController: HelpandSupportBaseViewController  {
    @IBOutlet weak var FAQTableView: UITableView!
    @IBOutlet weak var noRecordsView : UIView! {
        didSet {
            self.noRecordsView.isHidden = true
        }
    }
    @IBOutlet weak var noRecordsLabel : UILabel!
    
    fileprivate var faqQuestionArray = [fAQQuestionModel]()
    var shownIndexes : [IndexPath] = []
    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigation()
        FAQTableView.tableFooterView = UIView()
        getHelpAndFAQDetails()
        FAQTableView.delegate = self
        FAQTableView.dataSource = self
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            FAQTableView.refreshControl = refreshControl
        } else {
            FAQTableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshgetHelpAndFAQDetails(_:)), for: .valueChanged)
        noRecordsLabel.text = "Try Again".localized
    }
    
    func setNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "tabBarBack").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(backActionCurrent), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  "FAQ".localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
    
    @objc private func refreshgetHelpAndFAQDetails(_ sender: Any) {
        getHelpAndFAQDetails()
    }
    
    func getHelpAndFAQDetails() {
        noRecordsView.isHidden = true
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            let url = getUrl(urlStr: Url.getAllFAQQuestions, serverType: .helpandSupportUrl)
            //println_debug(url)
            
            let params = Dictionary<String,String>()
            self.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", handle: { (response, success) in
                if success {
                    do {
                        if let data = response as? Data {
                            let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                            if let dic = dict as? Dictionary<String,AnyObject> {
                                if let questionArray = dic["Content"] as? [Dictionary<String,AnyObject>] {
                                    self.faqQuestionArray.removeAll()
                                    for question in questionArray {
                                        self.faqQuestionArray.append(fAQQuestionModel.init(dict: question))
                                    }
                                    DispatchQueue.main.async {
                                        progressViewObj.removeProgressView()
                                        self.refreshControl.endRefreshing()
                                        self.FAQTableView.reloadData()
                                    }
                                }
                            }
                        }
                        DispatchQueue.main.async {
                            if self.faqQuestionArray.count == 0 {
                                self.noRecordsView.isHidden = false
                            }
                            self.refreshControl.endRefreshing()
                            progressViewObj.removeProgressView()
                        }
                    } catch {
                        DispatchQueue.main.async {
                            self.refreshControl.endRefreshing()
                            progressViewObj.removeProgressView()
                        }
                        //println_debug(error)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.refreshControl.endRefreshing()
                        self.noRecordsView.isHidden = false
                        progressViewObj.removeProgressView()
                    }
                }
            })
        } else {
            self.noInternetAlert()
            DispatchQueue.main.async {
                self.noRecordsView.isHidden = false
                self.refreshControl.endRefreshing()
                progressViewObj.removeProgressView()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension HelpFAQViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        UIView.animate(withDuration: 1.0) {
            if let viewController = UIStoryboard(name: "HelpSupport", bundle: Bundle.main).instantiateViewController(withIdentifier: "FAQQuestionAndAnswerViewController_ID") as? FAQQuestionAndAnswerViewController {
                viewController.selectedFAQ = self.faqQuestionArray[indexPath.row].tagName!
                self.navigationController?.pushViewController(viewController,animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
////        if (shownIndexes.contains(indexPath) == false) {
////            shownIndexes.append(indexPath)
////
////            cell.transform = CGAffineTransform(translationX: 0.0, y: 60.0)
////          //  cell.layer.shadowColor = UIColor.black.cgColor
////            cell.layer.shadowOffset = CGSize(width: 10, height: 10)
////            cell.alpha = 0
////
////            UIView.beginAnimations("rotation", context: nil)
////            UIView.setAnimationDuration(0.5)
////            cell.transform = CGAffineTransform(translationX: 0, y: 0)
////            cell.alpha = 1
////            cell.layer.shadowOffset = CGSize(width: 0, height: 0)
////            UIView.commitAnimations()
////        }
//    }
}

extension HelpFAQViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : FAQTableCell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath) as! FAQTableCell
        if indexPath.row <= self.faqQuestionArray.count {
            cell.loadCell(modelValue: faqQuestionArray[indexPath.row])
        }
        cell.selectionStyle = .none
        cell.accessoryType = .disclosureIndicator
        cell.tintColor = UIColor.black
        cell.accessoryView = UitilityClass.createImageViewWithImage(imageName: "rightArrow", x: 0, y: 0, width: 30, height: 30)
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return faqQuestionArray.count
    }
}

class FAQTableCell : UITableViewCell {
    let appDel = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var mainCategoryLabel: UILabel!
    
    override func awakeFromNib() {
        let width = UIScreen.main.bounds.width
        if width > 325 {
            mainCategoryLabel.font = UIFont(name: appFont, size: 16.0)
        } else {
            mainCategoryLabel.font = UIFont(name: appFont, size: 13.0)
        }
    }
    
    func loadCell(modelValue : fAQQuestionModel) {
        let width = UIScreen.main.bounds.width
        if width > 325 {
            mainCategoryLabel.font = UIFont(name: appFont, size: 16.0)
        } else {
            mainCategoryLabel.font = UIFont(name: appFont, size: 13.0)
        }
        categoryImage.image = UIImage(named: "ok.png")
        if appDel.currentLanguage == "my" {
            if let title = modelValue.tagNameInBurmese {
                mainCategoryLabel.text = title
            }
        } else if appDel.currentLanguage == "uni" {
            if let title = modelValue.tagNameInUniCode {
                mainCategoryLabel.text = title
            }
        }
        else {
            if let title = modelValue.tagName {
                mainCategoryLabel.text = title
            }
        }
    }
}
