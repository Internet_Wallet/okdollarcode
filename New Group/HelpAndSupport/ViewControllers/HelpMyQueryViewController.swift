//
//  HelpMyQueryViewController.swift
//  OK
//
//  Created by Vinod's MacBookPro on 12/7/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

struct customerTicketListModel {
    var issueName : String?
    var issueNameInBurmeese : String?
    var issueSubName : String?
    var issueSubNameInBurmeese : String?
    var ticketNo : String?
    var issueStatus : Int?
    var assignedTo : String?
    var createdDate : String?
    
    init(dict : Dictionary<String, Any>) {
        self.issueName = dict["IssueCategory"] as? String ?? ""
        self.issueNameInBurmeese = dict["IssueCategoryInBurmeese"] as? String ?? ""
        self.issueSubName = dict["IssueSubCategory"] as? String ?? ""
        self.issueSubNameInBurmeese = dict["IssueSubCategoryInBurmeese"] as? String ?? ""
        self.ticketNo = dict["TicketNumber"] as? String ?? ""
        self.issueStatus = dict["Status"] as? Int ?? 0
        self.assignedTo = dict["AssignedUserName"] as? String ?? ""
        self.createdDate = dict["CreatedDate"] as? String ?? ""
    }
}

class HelpMyQueryViewController: HelpandSupportBaseViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var queryTableView: UITableView!
    @IBOutlet weak var noRecordsView : UIView! {
        didSet {
            self.noRecordsView.isHidden = true
        }
    }
    @IBOutlet weak var noRecordsLabel : UILabel!
    
    fileprivate var customerTicketModeArray = [customerTicketListModel]()
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        queryTableView.delegate = self
        queryTableView.dataSource = self
        
        getMyQueriesList()
        queryTableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
        
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            queryTableView.refreshControl = refreshControl
        } else {
            queryTableView.addSubview(refreshControl)
        }
        
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshgetMyQueriesList(_:)), for: .valueChanged)
        noRecordsLabel.text = "Try Again".localized
        setNavigation()
    }
    
    
    func setNavigation() {
        
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "tabBarBack").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(backActionCurrent), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  "My Query".localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
    
    @objc private func refreshgetMyQueriesList(_ sender: Any) {
        customerTicketModeArray.removeAll()
        getMyQueriesList()
    }
    
    func getMyQueriesList() {
        
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            let url = getUrl(urlStr: Url.myQueryUrlApi, serverType: .helpandSupportUrl)
            var number = UserModel.shared.mobileNo
            if number == "" {
                number = ReRegistrationModel.shared.MobileNumber
            }
            self.getResponse(param : number, url : url, handle: { (response, success) in
                if success {
                    do {
                        if let data = response as? Data {
                            let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                            if let dic = dict as? Dictionary<String,AnyObject> {
                                if let questionArray = dic["Content"] as? [Dictionary<String,Any>] {
                                    DispatchQueue.main.async {
                                        self.customerTicketModeArray.removeAll()
                                        for question in questionArray {
                                            self.customerTicketModeArray.append(customerTicketListModel.init(dict: question))
                                        }
                                        self.queryTableView.reloadData()
                                        self.refreshControl.endRefreshing()
                                        progressViewObj.removeProgressView()
                                    }
                                }
                            }
                        }
                        DispatchQueue.main.async {
                            if self.customerTicketModeArray.count == 0 {
                                self.noRecordsView.isHidden = false
                                self.noRecordsLabel.text = "No Records Found!".localized
                            }
                            self.refreshControl.endRefreshing()
                            progressViewObj.removeProgressView()
                        }
                    } catch {
                        self.noRecordsViewShow()
                    }
                } else {
                    self.noRecordsViewShow()
                }
            })
        } else {
            self.noInternetAlert()
            self.noRecordsViewShow()
        }
    }
    
    fileprivate func noRecordsViewShow() {
        DispatchQueue.main.async {
            self.noRecordsView.isHidden = false
            self.noRecordsLabel.text = "Try Again".localized
            self.refreshControl.endRefreshing()
            progressViewObj.removeProgressView()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.customerTicketModeArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath) as! QueryTableCell
        cell.selectionStyle = .none
        if indexPath.row <= self.customerTicketModeArray.count {
            cell.loadCell(statusModel: self.customerTicketModeArray[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let alertView = UIStoryboard(name: "HelpSupport", bundle: nil).instantiateViewController(withIdentifier: "CustomerTicketStatusAlertView_ID") as? CustomerTicketStatusAlertView {
            alertView.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            alertView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            alertView.currentAlertModalValue  = self.customerTicketModeArray[indexPath.row]
            self.present(alertView, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

class QueryTableCell : UITableViewCell {
    
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var categoryImg: UIImageView!
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var statusImg: UIImageView! {
        didSet {
            self.statusImg.layer.masksToBounds = false
            self.statusImg.layer.cornerRadius = self.statusImg.frame.size.width / 2.0
            self.statusImg.clipsToBounds = true
        }
    }
    
    override func awakeFromNib() {
        let width = UIScreen.main.bounds.width
        if width > 325 {
            categoryName.font = UIFont(name: appFont, size: 16.0)
            statusLabel.font = UIFont(name: appFont, size: 16.0)
        } else {
            categoryName.font = UIFont(name: appFont, size: 13.0)
            statusLabel.font = UIFont(name: appFont, size: 13.0)
        }
    }
    
    func loadCell(statusModel : customerTicketListModel) {
        if appDel.currentLanguage == "my" {
            self.categoryName.text = statusModel.issueNameInBurmeese
        } else {
            self.categoryName.text = statusModel.issueName
        }
        if statusModel.issueStatus != 3 {
           self.statusLabel.text = appDel.getlocaLizationLanguage(key: "Pending")
            statusImg.image = UIImage(named: "help_pending.png")
        } else {
            self.statusLabel.text = appDel.getlocaLizationLanguage(key: "Closed")
            statusImg.image = UIImage(named: "act_checkbox.png")
        }
    }
}
