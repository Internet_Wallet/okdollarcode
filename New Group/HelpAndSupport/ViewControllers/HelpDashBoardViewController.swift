//
//  HelpDashBoardViewController.swift
//  OK
//
//  Created by Vinod's MacBookPro on 12/6/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

struct callBackModel {
    var issueType : String?
    var issueTypeInBurmese : String?
    var issueTypeInUniCode : String?
    var imageUrl : String?
    var issueSubCategoryType = [String]()
    var issueSubCategoryTypeInBurmese = [String]()
    var issueSubCategoryTypeInUniCode = [String]()
    var issueSubCategoryTypeTransId = [Bool]()
    
    init(dict : Dictionary<String, Any>) {
        self.issueType = dict["IssueType"] as? String ?? ""
        self.issueTypeInBurmese = dict["IssueTypeInBurmeese"] as? String ?? ""
        self.issueTypeInUniCode = dict["IssueTypeInUniCode"] as? String ?? ""
        self.imageUrl  = dict["ImageUrl"] as? String ?? ""
        
        if let subCategoryAray = dict["SubCategories"] as? [Dictionary<String, Any>] {
            for subCat in subCategoryAray {
                self.issueSubCategoryType.append(subCat["Name"] as? String ?? "")
                self.issueSubCategoryTypeInBurmese.append(subCat["NameInBurmeese"] as? String ?? "")
                self.issueSubCategoryTypeInUniCode.append(subCat["NameInUniCode"] as? String ?? "")
                self.issueSubCategoryTypeTransId.append(subCat["TransactionIdRequired"] as? Bool ?? false)
            }
        }
    }
    
}

class HelpDashBoardViewController: HelpandSupportBaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var collectionV: UICollectionView!
    @IBOutlet weak var noRecordsView : UIView! {
        didSet {
            self.noRecordsView.isHidden = true
        }
    }
    @IBOutlet weak var noRecordsLabel : UILabel!
    
    fileprivate var callBackModelArray = [callBackModel]()
    var navFrom = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getAllCallBack()
        self.title = "CALL BACK".localized
        self.loadBackHelpAndSupportDashButton()
        self.loadNavigationBarSettings()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        noRecordsLabel.text = "Try Again".localized
    }
    
    func loadBackHelpAndSupportDashButton() {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "tabBarBack"), for: UIControl.State.normal)
        button.addTarget(self, action:#selector(backActionDashBoard), for:.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 50, height: 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func backActionDashBoard() {
//        if navFrom == "DashBoardVC"
//        {
//            self.dismiss(animated: true, completion: nil)
//        } else {
            self.navigationController?.popViewController(animated: true)
//        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func getAllCallBack() {
        noRecordsView.isHidden = true
        if appDelegate.checkNetworkAvail() {
             progressViewObj.showProgressView()
            let url = getUrl(urlStr: Url.getAllCallBack, serverType: .helpandSupportUrl)
            //println_debug(url)
            
            let params = Dictionary<String,String>()
            self.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", handle: { (response, success) in
                if success {
                    do {
                        if let data = response as? Data {
                            let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                            if let dic = dict as? Dictionary<String,AnyObject> {
                                if let callBackArray = dic["Content"] as? [Dictionary<String,Any>] {
                                    for callBack in callBackArray {
                                        self.callBackModelArray.append(callBackModel.init(dict: callBack))
                                    }
                                    DispatchQueue.main.async {
                                        self.collectionV.reloadData()
                                        progressViewObj.removeProgressView()
                                    }
                                }
                            }
                            
                        }
                        DispatchQueue.main.async {
                            if self.callBackModelArray.count == 0 {
                                self.noRecordsView.isHidden = false
                            }
                            progressViewObj.removeProgressView()
                        }
                    } catch {
                        DispatchQueue.main.async {
                            progressViewObj.removeProgressView()
                        }
                        //println_debug(error)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.noRecordsView.isHidden = false
                        progressViewObj.removeProgressView()
                    }
                }
            })
        } else {
            self.noInternetAlert()
            DispatchQueue.main.async {
                self.noRecordsView.isHidden = false
                progressViewObj.removeProgressView()
            }
        }
    }
    
    //UICollectionViewDelegateFlowLayout methods
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4;
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1;
    }
    
    //UICollectionViewDatasource methods
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return callBackModelArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIdentifier", for: indexPath) as! HelpSupportCollectionCell
        let titleArray = callBackModelArray[indexPath.row]
        if appDel.currentLanguage == "my" {
            cell.loadCell(title: titleArray.issueTypeInBurmese! , imgUrl: titleArray.imageUrl! )
        } else if appDel.currentLanguage == "uni" {
            cell.loadCell(title: titleArray.issueTypeInUniCode! , imgUrl: titleArray.imageUrl! )
        }
        else {
            cell.loadCell(title: titleArray.issueType! , imgUrl: titleArray.imageUrl! )
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize{
        let cellWidth  = screenWidth / 3.3
        
        //This check is made because the reset password account blocked text was not coming fully 
        if indexPath.row == 20 {
            return CGSize(width: cellWidth, height: cellWidth + 55)
        }else{
             return CGSize(width: cellWidth, height: cellWidth + 30)
        }
        
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let viewController = UIStoryboard(name: "HelpSupport", bundle: Bundle.main).instantiateViewController(withIdentifier: "CallBackViewController") as? CallBackViewController
        viewController?.subCategory = callBackModelArray[indexPath.row]
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
}

class HelpSupportCollectionCell : UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    let imageCache = NSCache<NSString, UIImage>()
    
    override func awakeFromNib() {
        
            nameLabel.font = UIFont(name: appFont, size: 13.0)
        
    }
    
    func loadCell(title: String, imgUrl : String) {
        
        self.nameLabel.text = title
        nameLabel.font = UIFont(name: appFont, size: 13.0)
        
        if let cachedImage = imageCache.object(forKey: imgUrl as NSString) {
            DispatchQueue.main.async(execute: { () -> Void in
                self.imageView.image = cachedImage
            })
        } else {
            URLSession.shared.dataTask(with: NSURL(string: imgUrl)! as URL, completionHandler: { (data, response, error) -> Void in
                if error != nil {
//                    println_debug(error ?? "No Error")
                    return
                } else {
                    if let data = data, let image = UIImage(data: data) {
                        self.imageCache.setObject(image, forKey: imgUrl as NSString)
                        DispatchQueue.main.async(execute: { () -> Void in
                            let image = UIImage(data: data)
                            self.imageView.image = image
                        })
                    }
                }
            }).resume()
        }
    }
}
