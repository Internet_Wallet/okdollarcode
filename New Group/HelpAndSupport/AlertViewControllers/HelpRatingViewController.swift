//
//  HelpRatingViewController.swift
//  OK
//
//  Created by Vinod's MacBookPro on 12/8/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class HelpRatingViewController: HelpandSupportBaseViewController,UITextViewDelegate {
    
    @IBOutlet weak var ratingPopupView: UIView!
    @IBOutlet weak var ratingTypeLbl: UILabel!
    @IBOutlet weak var ratingTypeImg: UIImageView!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var ratingResolvedByLbl : UILabel!
    @IBOutlet weak var submitBtn: UIButton!
    var nameText = ""
    var ticketId = ""
    
    var selectedStarCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ratingPopupView.layer.cornerRadius = 15
        ratingPopupView.layer.borderWidth = 1
        ratingPopupView.layer.borderColor = UIColor.white.cgColor
        
        commentTextView.layer.cornerRadius = 5
        commentTextView.layer.borderWidth = 1
        commentTextView.layer.borderColor = UIColor.lightGray.cgColor
        commentTextView.delegate = self
        commentTextView.text = "Add Comment".localized
        commentTextView.textColor = UIColor.lightGray
   
        submitBtn.layer.cornerRadius = 10
        submitBtn.layer.borderWidth = 1
        submitBtn.layer.borderColor = UIColor.clear.cgColor
        self.view?.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        ratingTypeLbl.text = ratingTypeLbl.text?.localized
        submitBtn.setTitle(submitBtn.titleLabel?.text?.localized, for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
       ratingResolvedByLbl.text = "Please rate ".localized + nameText + " for resolving your issue. Your rating will help us to improve our services.".localized
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.white
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Add Comment".localized
            textView.textColor = UIColor.lightGray
        }
    }
    
    fileprivate func getParams() -> String
    {
        var ticketRequest = [String:Any]()
        ticketRequest["Rate"] = String(selectedStarCount)
        ticketRequest["Comments"] = commentTextView.text
        let param1 = self.JSONStringFromAnyObject(value: ticketRequest as AnyObject)
        let dict = ["Rating" : param1,
                    "TicketId" : ticketId,
                    "CustomerPhone" : UserModel.shared.mobileNo]
        
        let finalParam = self.JSONStringFromAnyObject(value: dict as AnyObject)
        return finalParam
    }
    
    fileprivate func submitQuery() {
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            let url = getUrl(urlStr: Url.rateOkDollarHelpSupportApi, serverType: .helpandSupportUrl)
            
            
            let params = self.getParams()            
            self.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", handle: { (response, success) in
                if success {
                    do {
                        if let data = response as? Data {
                            let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                            if let dic = dict as? Dictionary<String,AnyObject> {
                                if let statusCode = dic["Statuscode"] as? Int {
                                    if statusCode == 200 {
                                        DispatchQueue.main.async {
                                            progressViewObj.removeProgressView()
                                            self.showAlert()
                                        }
                                    }
                                }
                            }
                        }
                        DispatchQueue.main.async {
                            progressViewObj.removeProgressView()
                        }
                    } catch {
                        DispatchQueue.main.async {
                            progressViewObj.removeProgressView()
                        }
                        //println_debug(error)
                    }
                } else {
                    DispatchQueue.main.async {
                        progressViewObj.removeProgressView()
                    }
                }
            })
        }
    }
    
    
    fileprivate func showAlert() {
        self.ratingPopupView.isHidden = true
    
        alertViewObj.wrapAlert(title: "", body: "Thank you for your valuable feedback!".localized, img: #imageLiteral(resourceName: "help"))
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            self.dismiss(animated: true, completion: nil)
        })
        
        alertViewObj.showAlert(controller: self)
    }
    
    @IBAction func submitBtnAction(_ sender: Any) {
        if selectedStarCount > 0 {
           submitQuery()
        } else {
            self.showErrorAlert(errMessage: "Please select the star!".localized)
        }
    }
    
    @IBAction func ratingButtonAction(_ sender : UIButton) {
        let tag = sender.tag
        selectedStarCount = tag
        switch tag {
        case 1, 2 :
            ratingTypeLbl.text = "POOR".localized
        case 3:
            ratingTypeLbl.text = "AVERAGE".localized
        case 4:
            ratingTypeLbl.text = "GOOD".localized
        case 5:
            ratingTypeLbl.text = "EXCELLENT".localized
        default:
            ratingTypeLbl.text = "GOOD".localized
        }
        for i in 1...tag {
            let view = self.ratingPopupView.viewWithTag(i)
            if let button = view as? UIButton {
                button.setImage(UIImage(named: "receiptStarFilled.png"), for: .normal)
            }
        }
        if tag != 5 {
            for i in tag+1...5 {
                let view = self.ratingPopupView.viewWithTag(i)
                if let button = view as? UIButton {
                    button.setImage(UIImage(named: "receiptStarUnfilled.png"), for: .normal)
                }
            }
        }
    }
}
