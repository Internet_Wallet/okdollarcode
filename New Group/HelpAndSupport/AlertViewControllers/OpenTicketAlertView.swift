//
//  OpenTicketAlertView.swift
//  OK
//
//  Created by Mohit on 3/14/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class OpenTicketAlertView: HelpandSupportBaseViewController {
    
    @IBOutlet weak var commentTextView : UITextView!
    @IBOutlet weak var popUpview : UIView!
    
    var ticketId = ""
    
    @IBOutlet weak var submitButton : UIButton! {
        didSet {
            self.submitButton.layer.cornerRadius = 0.2
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view?.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        // Do any additional setup after loading the view.
        commentTextView.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func getParams() -> String {
        var ticketRequest = [String:Any]()
        ticketRequest["IsClosed"] = String(false)
        ticketRequest["Comments"] = commentTextView.text
        ticketRequest["TicketId"] = ticketId
        ticketRequest["PhoneNumber"] = UserModel.shared.mobileNo
        
        let finalParam = self.JSONStringFromAnyObject(value: ticketRequest as AnyObject)
        return finalParam
    }
    
    fileprivate func submitQuery()
    {
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            //Updated url on Jan5,2021-Gauri
            let url = getUrl(urlStr: Url.ConfirmClosedTicket, serverType: .helpandSupportUrl)
            //println_debug(url)
            
            let params = self.getParams()
            self.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", handle: { (response, success) in
                if success {
                    do {
                        if let data = response as? Data {
                            let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                            if let dic = dict as? Dictionary<String,AnyObject> {
                                if let statusCode = dic["Statuscode"] as? Int {
                                    if statusCode == 200 {
                                        DispatchQueue.main.async {
                                            progressViewObj.removeProgressView()
                                            self.showAlert()
                                        }
                                    }
                                }
                            }
                        }
                        DispatchQueue.main.async {
                            progressViewObj.removeProgressView()
                        }
                    } catch {
                        DispatchQueue.main.async {
                            progressViewObj.removeProgressView()
                        }
                        //println_debug(error)
                    }
                } else {
                    DispatchQueue.main.async {
                        progressViewObj.removeProgressView()
                    }
                }
            })
        }
    }
    
    fileprivate func showAlert() {
        self.popUpview.isHidden = true
        
        alertViewObj.wrapAlert(title: "", body: "Managers Get alert with details. We personally arrange a call back and resolve issue asap.".localized, img: #imageLiteral(resourceName: "help"))
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            self.dismiss(animated: true, completion: nil)
        })
        
        alertViewObj.showAlert(controller: self)
    }
    
    @IBAction func submitQueryAction(_ sender : UIButton) {
       // self.showAlert()
        self.submitQuery()
    }
}
