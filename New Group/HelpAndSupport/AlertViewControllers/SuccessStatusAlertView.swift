//
//  SuccessStatusAlertView.swift
//  OK
//
//  Created by Mohit on 3/14/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol TicketStatusProtocol : class {
    func dismissCreateViewController()
}

class SuccessStatusAlertView: HelpandSupportBaseViewController {
    
    @IBOutlet weak var successLbl : UILabel!
    @IBOutlet weak var okButton : UIButton!
    
    weak var delegate : TicketStatusProtocol?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view?.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        // Do any additional setup after loading the view.
        successLbl.text = "Successfully sent your request".localized + "!"
        okButton.setTitle("OK".localized, for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismissView(_ sender : UIButton) {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.dismissCreateViewController()
    }
}
