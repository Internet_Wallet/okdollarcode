//
//  CustomerTicketStatusAlertView.swift
//  OK
//
//  Created by Mohit on 3/13/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class CustomerTicketStatusAlertView: HelpandSupportBaseViewController {
    
    @IBOutlet weak var ticketNoLocLbl : UILabel!
    @IBOutlet weak var subCategoryLocLbl : UILabel!
    @IBOutlet weak var assignedToLocLbl : UILabel!
    @IBOutlet weak var dateLocLbl : UILabel!
    @IBOutlet weak var statusLocLbl : UILabel!
    @IBOutlet weak var okButton : UIButton!
    
    @IBOutlet weak var mainCategoryLbl : UILabel!
    @IBOutlet weak var ticketNoLbl : UILabel!
    @IBOutlet weak var subCategoryLbl : UILabel!
    @IBOutlet weak var assignedToLbl : UILabel!
    @IBOutlet weak var dateLbl : UILabel!
    @IBOutlet weak var statusLbl : UILabel!
    
    var currentAlertModalValue : customerTicketListModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view?.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        displayAlert()
        setUpLocalization()
    }
    
    fileprivate func setUpLocalization() {
        ticketNoLocLbl.text = "Your ticket number".localized
        subCategoryLocLbl.text = "Subcategory".localized
        assignedToLocLbl.text = "Assigned to".localized
        dateLocLbl.text = "Date".localized
        statusLocLbl.text = "Status".localized
        okButton.setTitle("OK".localized, for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func displayAlert() {
        if appDel.currentLanguage == "my" {
            mainCategoryLbl.text = currentAlertModalValue?.issueNameInBurmeese
            subCategoryLbl.text = currentAlertModalValue?.issueSubNameInBurmeese
        } else {
            mainCategoryLbl.text = currentAlertModalValue?.issueName
            subCategoryLbl.text = currentAlertModalValue?.issueSubName
        }
        ticketNoLbl.text = currentAlertModalValue?.ticketNo
        assignedToLbl.text = currentAlertModalValue?.assignedTo
        dateLbl.text = self.convertDateFormatter(date: (currentAlertModalValue?.createdDate)!)
        if currentAlertModalValue?.issueStatus != 3 {
            statusLbl.text = "Pending".localized
        } else {
            statusLbl.text = "Resolved".localized
        }
    }
    
    @IBAction func dismissAlert(_ sender : UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func convertDateFormatter(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "EEE,dd-MM-yyyy HH:mm:ss"///this is what you want to convert format
        let timeStamp = dateFormatter.string(from: date!)
        return timeStamp
    }

}
