//
//  TaxPaymentInvoiceCell.swift
//  OK
//
//  Created by Tushar Lama on 03/06/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit


protocol TaxInvoiceDelegate {
    func sendDataOfParticularCell(index: Int)
}

class TaxPaymentInvoiceCell: UITableViewCell {
    @IBOutlet var okNumberLabel: UILabel!
    @IBOutlet var okNumberValue: UILabel!
    @IBOutlet var okTrasactionIdLabel: UILabel!
    @IBOutlet var okTransactionValue: UILabel!
    @IBOutlet var taxPayerIdentificationNumberLabel: UILabel!
    @IBOutlet var taxPayerIdentificationValue: UILabel!
    @IBOutlet var irdTrascationCodeLabel: UILabel!
    @IBOutlet var irdTransactionCodeValue: UILabel!
    @IBOutlet var walletBalanceLabel: UILabel!
    @IBOutlet var walletBalanceValue: UILabel!
    @IBOutlet var invoiceButton: UIButton!
    @IBOutlet var headerDate: UILabel!
    var delegate: TaxInvoiceDelegate?
    
    @IBOutlet var taxPaymentLabelHeader: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBAction func onClickInvoice(_ sender: Any) {
        delegate?.sendDataOfParticularCell(index: invoiceButton.tag)
    }
    
    func setDataOnCell(data :TaxInvoiceDetail){
        
        okNumberLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
        okNumberLabel.text = "OK$ Number".localized
        okTrasactionIdLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
        okTrasactionIdLabel.text = "OK$ Transaction ID".localized
        taxPayerIdentificationNumberLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
        taxPayerIdentificationNumberLabel.text = "Taxpayer Identification Number".localized
        irdTrascationCodeLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
        irdTrascationCodeLabel.text = "Transaction Code".localized
        walletBalanceLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
        walletBalanceLabel.text = "Paid Amount".localized
        
        okNumberValue.text = UserModel.shared.mobileNo.replacingOccurrences(of: "0095", with: "(+95) 0")
        okTransactionValue.text = data.transactionID ?? ""
        taxPayerIdentificationValue.text = data.payerRegNumber ?? ""
           //data.companyOrCitizenNumber ?? "".replacingOccurrences(of: "-", with: "/")
        irdTransactionCodeValue.text = data.transactionCode
        walletBalanceValue.text =   data.taxAmount 
        taxPaymentLabelHeader.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
        taxPaymentLabelHeader.text = "Tax Payment".localized
        
    }
}
