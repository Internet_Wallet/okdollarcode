//
//  NearByScanQRDetailsCell.swift
//  OK
//
//  Created by Tushar Lama on 29/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class NearByScanQRDetailsCell: UITableViewCell {
    @IBOutlet var details: UILabel!
    @IBOutlet var title: UILabel!
    @IBOutlet var detailsHeight: NSLayoutConstraint!
    @IBOutlet var titleHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
