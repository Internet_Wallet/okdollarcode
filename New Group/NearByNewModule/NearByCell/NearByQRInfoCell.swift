//
//  NearByQRInfoCell.swift
//  OK
//
//  Created by Tushar Lama on 16/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class NearByQRInfoCell: UITableViewCell {

    @IBOutlet var imageViewLeading: NSLayoutConstraint!
    @IBOutlet var merchantInfoImageView: UIImageView!
    @IBOutlet var merchantInfoDetailLabelHeight: NSLayoutConstraint!
    @IBOutlet var merchantInfoDetailLabel: UILabel!
    @IBOutlet var merchantInfoTitleLabel: UILabel!
    @IBOutlet var imageViewWidth: NSLayoutConstraint!
    @IBOutlet var seperatorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func setDataOnCell(obj: NewScanQRModel){
        
        
        
    }
}
