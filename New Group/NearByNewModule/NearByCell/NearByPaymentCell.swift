//
//  NearByPaymentCell.swift
//  OK
//
//  Created by Tushar Lama on 16/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class NearByPaymentCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var merchantValueLabel: UILabel!
    @IBOutlet var merchantLabelHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
