//
//  TaxReceiptCell.swift
//  OK
//
//  Created by Tushar Lama on 03/06/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class TaxReceiptCell: UITableViewCell {

    @IBOutlet var qrImageView: UIImageView!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let myString = formatter.string(from: Date())
        let yourDate = formatter.date(from: myString)
        formatter.dateFormat = "EEE, dd-MMM-yyyy"
        
        if let isValidDate = yourDate{
            let date = formatter.string(from: isValidDate)
            let timeFormatterPrint = DateFormatter()
            timeFormatterPrint.dateFormat = "HH:mm:ss"
            timeFormatterPrint.calendar = Calendar(identifier: .gregorian)
            let time = timeFormatterPrint.string(from: isValidDate)
            dateLabel.text = date
            timeLabel.text = time
        }else{
            dateLabel.text = ""
            timeLabel.text = ""
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
  
    
    
  

}
