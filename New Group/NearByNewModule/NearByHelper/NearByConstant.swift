//
//  NearByConstant.swift
//  OK
//
//  Created by Tushar Lama on 16/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation

struct NearByConstant{
    
    static let kNearByController = "NearByController"
    static let kNearByQRInfoController = "NearByQRInfoController"
    static let kNearByQRInfoCell = "NearByQRInfoCell"
    static let kNearByPaymentCell = "NearByPaymentCell"
    static let kNearByReceiptController = "NearByReceiptController"
    static let kNearByCheckPaymentDetails = "NearByCheckPaymentDetails"
    static let kNearByScanDetails = "NearByScanDetails"
    static let kNearByScanQRDetailsCell = "NearByScanQRDetailsCell"
    static let kNearByTaxReceiptController = "NearByTaxReceiptController"
    static let kTaxReceiptCell = "TaxReceiptCell"
    static let kTaxReceiptFinalController = "TaxReceiptFinalController"
    static let kTaxPaymentInvoiceCell = "TaxPaymentInvoiceCell"
    static let kTaxInvoiceViewController = "TaxInvoiceViewController"
    static let kGeneriCustomAlertViewController = "GeneriCustomAlertViewController"
    
    static let nearByMerchantArray = ["Merchant Name".localized,"Merchant Description".localized,"Amount".localized,"Remarks".localized]
    static let dummyData = ["Zin Mar","+95 9715487628","1000 MMK","Lorem lpsu is simply dummy text of the printing and tysetting industry.Lorem lpsum has been the industry's standard dummy"]
    static let nearbyMerchantImages = ["merchantIcon.png","merchantMobile.png","merchantAmount.png","merchantRemarks.png","merchantIcon.png","merchantIcon.png"]
       
     static let nearByBaseURLTest = "http://69.160.4.151:8001/RestService.svc/"
     static let nearByBaseURL = "https://www.okdollar.co/RestService.svc/"
     static let baseURL = nearByBaseURL
    static let taxPaymentBaseURLTest = "http://69.160.4.151:8001/RestService.svc/"
    static let nearByServiceTypePost = "POST"
    static let nearByServiceTypeGet = "GET"
    
    static func getURLForNearByScanQR() -> String{
        return baseURL + "ScanQrandPayByUser"
    }
    
    static func getURLForNearByGetQR() -> String{
       // return baseURL + "GetQrPayStatusById"
        return baseURL + "GetTransInfoByQrId"
    }
    
    static func getURLForNearByGetDataForQR() -> String{
        return baseURL + "GetDataForScanQrPay"
    }
    
    static func getURLToPayTax() -> String{
        return baseURL + "ProcessTaxPayment"
    }
    
    static func getURLToSeeTaxHistory() -> String{
        return baseURL + "GetTaxPaymentHistory"
    }
    
    static func getURLToFetchQRTimeAndMsg() -> String{
        return baseURL + "GetCustomMessageforQrModule"
    }
    
}



