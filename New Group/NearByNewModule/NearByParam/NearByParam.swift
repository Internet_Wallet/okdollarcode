//
//  NearByParam.swift
//  OK
//
//  Created by Tushar Lama on 18/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation

struct NearByParam {
    
    //this param we use when we want to generate QR Code
    static func nearByGetParamForQR() -> NSMutableDictionary{
        
        //Login Data
        let loginDic = NSMutableDictionary()
        loginDic.setValue(UserModel.shared.mobileNo, forKey: "MobileNumber")
        loginDic.setValue(uuid, forKey: "Simid")
        loginDic.setValue(msid, forKey: "Msid")
        loginDic.setValue(1, forKey: "Ostype")
        loginDic.setValue("", forKey: "Otp")
        loginDic.setValue(UserModel.shared.appID, forKey: "AppId")
        loginDic.setValue(0, forKey: "Limit")
        loginDic.setValue(0, forKey: "Offset")
        
        let qrDic = NSMutableDictionary()
        qrDic.setValue(UserModel.shared.mobileNo, forKey: "OkAccNumber")
        qrDic.setValue(ok_password ?? "", forKey: "Password")
        
        
        let mainDic = NSMutableDictionary()
        mainDic.setValue(loginDic, forKey: "Login")
        mainDic.setValue(qrDic, forKey: "QrPayData")
        
        return mainDic
    }
    
    //This param we user when we want to show user the current transaction made by him
    static func nearByGetParamForTransactionQR(qrID: String,spid: String,destinationNumber: String) -> NSMutableDictionary{
        let mainDic = NSMutableDictionary()
        mainDic.setValue(qrID, forKey: "QrId")
        mainDic.setValue(getParamToGenerateInvoice(), forKey: "Login")
        print(mainDic)
        return mainDic
    }
    
    
    static func nearByGetTimeAndInfoString() -> NSMutableDictionary{
        let mainDic = NSMutableDictionary()
        mainDic.setValue(getParamToGenerateInvoice(), forKey: "Login")
        return mainDic
    }
    
    //This param we use when we scan the QR of merchant and pass the data to the server
    static func getParamForMerchantTransaction(merTransInfo: String) -> Dictionary<String, Any> {
        var dict = Dictionary<String, Any>()
        dict["MerTransInfo"] = merTransInfo
        dict["UserOkAccNumber"] = UserModel.shared.mobileNo
        dict["Password"] = ok_password ?? ""
        dict["SecureToken"] = UserLogin.shared.token
        return dict
    }
    
    //this will return param to post request while paying text
    static func getParamToPayTax(transcationCode: String,payerName: String,payerRegNum: String,companyCitizenNumber: String,address: String,taxType: String,incomeYear: String,bankAccountNum: String,taxOffice: String,supplier: String,payAmount: String,taxAmount: String) -> NSMutableDictionary{
        
        //Login Data
        let loginDic = NSMutableDictionary()
        loginDic.setValue(UserModel.shared.mobileNo, forKey: "MobileNumber")
        loginDic.setValue(uuid, forKey: "Simid")
        loginDic.setValue(msid, forKey: "Msid")
        loginDic.setValue(1, forKey: "Ostype")
        loginDic.setValue("", forKey: "Otp")
        loginDic.setValue(UserModel.shared.appID, forKey: "AppId")
        loginDic.setValue(0, forKey: "Limit")
        loginDic.setValue(0, forKey: "Offset")
        
        //TaxPayeeInfo
        let taxPayInfoDic = NSMutableDictionary()
        
        
      // taxPayInfoDic.setValue(String(Date().toMillis() ?? 232333333323223), forKey: "TransactionCode")
        taxPayInfoDic.setValue(transcationCode, forKey: "TransactionCode")
        taxPayInfoDic.setValue(payerName, forKey: "PayerName")
        taxPayInfoDic.setValue(payerRegNum, forKey: "PayerRegNum")
        taxPayInfoDic.setValue(companyCitizenNumber, forKey: "CompanyOrCitizenIdNumber")
        taxPayInfoDic.setValue(address, forKey: "Address")
        taxPayInfoDic.setValue(taxType, forKey: "TaxType")
        taxPayInfoDic.setValue(incomeYear, forKey: "IncomeYear")
        taxPayInfoDic.setValue(bankAccountNum, forKey: "BankAcNum")
        taxPayInfoDic.setValue(taxOffice, forKey: "TaxOffice")
        taxPayInfoDic.setValue(supplier, forKey: "Supplier")
        taxPayInfoDic.setValue(payAmount, forKey: "PayAmount")
        taxPayInfoDic.setValue(UserModel.shared.name, forKey: "AgentName")
       // taxPayInfoDic.setValue(UserModel.shared.agentType, forKey: "AgentType")
        taxPayInfoDic.setValue(UitilityClass.returnAccountNameFromType(type: "\(UserModel.shared.agentType)"), forKey: "AgentType")
        
        
        //PaymentInfo
         let sourceDic = NSMutableDictionary()
         sourceDic.setValue(UserModel.shared.mobileNo, forKey: "Source")
         sourceDic.setValue(UserLogin.shared.token, forKey: "SecureToken")
         sourceDic.setValue(ok_password ?? "", forKey: "Password")
         sourceDic.setValue(taxAmount, forKey: "TaxAmount")
        
        let taxDic = NSMutableDictionary()
        taxDic.setValue(taxPayInfoDic, forKey: "TaxPayeeInfo")
        taxDic.setValue(sourceDic, forKey: "PaymentInfo")
        
        let mainDic = NSMutableDictionary()
        mainDic.setValue(loginDic, forKey: "Login")
        mainDic.setValue(taxDic, forKey: "TaxPayInfo")
        
        return mainDic
        
    }
    
    //this will help us to generate invoice after paying tax
    static func getParamToGenerateInvoice() -> NSMutableDictionary{
        let loginDic = NSMutableDictionary()
        loginDic.setValue(UserModel.shared.mobileNo, forKey: "MobileNumber")
        loginDic.setValue(uuid, forKey: "Simid")
        loginDic.setValue(msid, forKey: "Msid")
        loginDic.setValue(1, forKey: "Ostype")
        loginDic.setValue("", forKey: "Otp")
        loginDic.setValue(UserModel.shared.appID, forKey: "AppId")
        loginDic.setValue(0, forKey: "Limit")
        loginDic.setValue(0, forKey: "Offset")
        return loginDic
    }
    
    //This will return the transaction history
    
    // {"MobileNumber":null,"Simid":null,"Msid":null,"Ostype":0,"Otp":null,"AppId":null,"Limit":0,"Offset":0}  
}


