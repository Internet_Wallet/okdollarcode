//
//  TaxInvoiceModel.swift
//  OK
//
//  Created by Tushar Lama on 04/06/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation

open class TaxInvoiceModel{
    var msg: String?
    var data: String?
    var isTransactionSuccess = false
    var taxHistoryDetails = [TaxInvoiceDetail]()
    
    
    public init(data: NSDictionary?){
        if let response = data{
            if response.count>0{
                let code = response.value(forKey: "Code") as? Int
                self.msg = response.value(forKey: "Msg") as? String
                if let isValidCode = code{
                    if isValidCode == 200{
                        self.isTransactionSuccess = true
                        let data = response.value(forKey: "Data") as? String
                        if let validObj = data{
                            let dic =   validObj.replacingOccurrences(of: "\\", with: "")
                            if let list = UitilityClass.convertStringToArrayOfDictionary(text: dic) as? [AnyObject] {
                                taxHistoryDetails = list.compactMap({TaxInvoiceDetail(responseData: $0 as! NSDictionary)})
                            }
                        }
                    }else{
                        self.isTransactionSuccess = false
                    }
                }
            }
        }
    }
    
}



open class TaxInvoiceDetail{
    var taxAmount = ""
    var transactionID: String?
    var transactionTime: String?
    var transactionCode = ""
    var payerName: String?
    var payerRegNumber: String?
    var companyOrCitizenNumber: String?
    var address: String?
    var taxType: String?
    var incomeYear: String?
    var bankAcNumber: String?
    var taxOffice: String?
    var supplier: String?
    var payAmount = 0.0
    
    public init(responseData: NSDictionary){
        if responseData.count>0{
            self.taxAmount =  wrapAmountWithCommaDecimal(key: responseData.value(forKey: "TransAmount") as? String ?? "") + " " + "MMK"     
            self.transactionID = responseData.value(forKey: "TransactionId") as? String ?? ""
            self.transactionTime = responseData.value(forKey: "TransactionTime") as? String ?? ""
            self.transactionCode = responseData.value(forKey: "TransactionCode") as? String ?? ""
            self.payerName = responseData.value(forKey: "PayerName") as? String ?? ""
            self.payerRegNumber = responseData.value(forKey: "PayerRegNum") as? String ?? ""
            self.companyOrCitizenNumber = responseData.value(forKey: "CompanyOrCitizenIdNumber") as? String ?? ""
            self.address = responseData.value(forKey: "Address") as? String ?? ""
            self.taxType = responseData.value(forKey: "TaxType") as? String ?? ""
            self.incomeYear = responseData.value(forKey: "IncomeYear") as? String ?? ""
            self.bankAcNumber = responseData.value(forKey: "BankAcNum") as? String ?? ""
            self.taxOffice = responseData.value(forKey: "TaxOffice") as? String ?? ""
            self.supplier = responseData.value(forKey: "Supplier") as? String ?? ""
            self.payAmount = responseData.value(forKey: "PayAmount") as? Double ?? 0.0
        }
    }
}




