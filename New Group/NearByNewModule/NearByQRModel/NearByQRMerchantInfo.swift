//
//  NearByQRMerchantInfo.swift
//  OK
//
//  Created by Tushar Lama on 18/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation

open class NearByQRMerchantInfo{
    
   // var trasactionDetailsObj:
    var isTransactionSuccess = false
    var msg: String?
    public init(response: NSDictionary?){
        if let isValidResponse = response{
            if isValidResponse.count>0{
                let code = isValidResponse.value(forKey: "Code") as? Int
                self.msg = isValidResponse.value(forKey: "Msg") as? String
                if let isValidCode = code{
                    if isValidCode == 200 || msg == "Success" || msg == "success"{
                        self.isTransactionSuccess = true
                    }
                }
            }
        }
    }
}
