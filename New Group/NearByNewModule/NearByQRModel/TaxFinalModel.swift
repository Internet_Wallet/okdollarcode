//
//  TaxFinalModel.swift
//  OK
//
//  Created by Tushar Lama on 03/06/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation

open class TaxFinalModel{
   
    var isTransactionSuccess = false
    var msg: String?
    var transactionId: String?
    var walletBalance: String?
    var responseDataTime: String?
    var oprWallet: String?
    var merchantName: String?
    var comment: String?
    var code: Int?
    public init(data: NSDictionary?){
        
        if let response = data{
            if response.count>0{
                self.code = response.value(forKey: "Code") as? Int
                self.msg = response.value(forKey: "Msg") as? String
                
                if let isValidCode = code{
                    if isValidCode == 200{
                        self.isTransactionSuccess = true
                        let data = response.value(forKey: "Data") as? String
                        if let validObj = data{
                            let dic =   validObj.replacingOccurrences(of: "\\", with: "")
                            if let list = UitilityClass.convertToDictionary(text: dic){
                                self.transactionId = list["Transid"] as? String ?? ""
                                self.responseDataTime = list["Responsects"] as? String ?? ""
                            }
                        }
                    }else{
                        self.isTransactionSuccess = false
                    }
                }
            }
        }
        
        
        
        
        
     //   Amount , TransactionID,Transactiontime,TransactionCode



        
        
//        if data.count>0{
//            self.transactionId = data.value(forKey: "Transid") as? String
//            self.walletBalance = data.value(forKey: "Walletbalance") as? String
//            self.responseDataTime = data.value(forKey: "Responsects") as? String
//            self.oprWallet = data.value(forKey: "Oprwallet") as? String
//            self.merchantName = data.value(forKey: "Merchantnamesms") as? String
//            self.comment = data.value(forKey: "Comments") as? String
//        }
    }
}

//
////{
////    Code = 200;
////    Data = "{\"ResultCode\":0,
//\"ResultDescription\":\"Transaction Successful\",
//\"Transid\":\"2009444144\",
//\"Walletbalance\":\"1212641\",
//\"Responsects\":\"03-Jun-2020 15:48:50\",
//\"Oprwallet\":\"279525.0\",
//\"Loyaltypoints\":\"0\",
//\"Kickvalue\":\"\",
//\"Merchantnamesms\":\"FGBBHH\",
//\"Comments\":\"#Tax Code :TX100000012#\"}";
//    Msg = Success;
//}
