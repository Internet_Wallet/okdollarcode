//
//  NearByQRModel.swift
//  OK
//
//  Created by Tushar Lama on 18/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation

open class NearByQRModel{
    
    var data: String?
    var msg: String?
    
    
    public init(response: NSDictionary?){
        if let isValidResponse = response{
            if isValidResponse.count>0{
                let code = isValidResponse.value(forKey: "Code") as? Int
                self.msg = isValidResponse.value(forKey: "Msg") as? String
                if let isValidCode = code{
                    if isValidCode == 200{
                        self.data = isValidResponse.value(forKey: "Data") as? String
                    }
                }
            }
        }
    }
}
