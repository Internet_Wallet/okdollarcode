//
//  NearByQRPayModel.swift
//  OK
//
//  Created by Tushar Lama on 18/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation

open class NearByQRPayModel{
    
    var paymentTransId: String?
    var merRefNumber: String?
    var transactionTime: String?
    var paymentStatus: String?
    var amount: String?
    var merchantName: String?
    var PaymentType: String?
    var code: Int?
    var msg: String?
    
    public init(data: [String: Any]?,code:Int?,msg: String?){
        let codeData = code
        if let isValidCode = codeData{
            self.code = isValidCode
        }
        
        if let isValidMsg = msg{
            self.msg = isValidMsg
        }
        
        if let isValidObj = data{
            if isValidObj.count>0{
                self.paymentTransId = isValidObj["PaymentTransId"] as? String ?? ""
                self.merRefNumber = isValidObj["MerRefNumber"] as? String ?? ""
                self.transactionTime = isValidObj["TransactionTime"] as? String ?? ""
                self.paymentStatus = isValidObj["PaymentStatus"] as? String ?? ""
                self.amount = isValidObj[ "Amount"] as? String ?? ""
                self.merchantName = isValidObj["MerchantName"] as? String ?? ""
                self.PaymentType = isValidObj["PaymentType"] as? String ?? ""
            }
        }
    }
}


open class NearMeTimeStringModel{
    var time: String?
    var showAlertCustomMsgEng: String?
    var showAlertCustomMsgMyUni: String?
    var showAlertCustomMsgMyZaw: String?
    var msg: String?
    var code: Int?
    
    public init(data: NSDictionary){
        
            if data.count>0{
                self.code = data.value(forKey: "Code") as? Int
                self.msg = data.value(forKey: "Msg") as? String
                let newData = data.value(forKey: "Data") as? String
                if let isValidData = newData{
                    let value = UitilityClass.convertToDictionary(text: isValidData)
                    if let hasValidData = value{
                        self.time = hasValidData["ShowExpirtytime"] as? String
                        self.showAlertCustomMsgEng = hasValidData["ShowAlertCustomMsgEng"] as? String
                        self.showAlertCustomMsgMyUni = hasValidData["ShowAlertCustomMsgMyUni"] as? String
                        self.showAlertCustomMsgMyZaw = hasValidData["ShowAlertCustomMsgMyZaw"] as? String
                    }
                    
                }
                
                println_debug(data)
            }
        
    }

    
    
}

