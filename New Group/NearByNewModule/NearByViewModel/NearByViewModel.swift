//
//  NearByViewModel.swift
//  OK
//
//  Created by Tushar Lama on 18/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation

class NearByViewModel{
    var createConnectionObj = NearByConnection()
    var nearByQRModelObj: NearByQRModel?
    var nearByQRPayModelObj:NearByQRPayModel?
    var nearByQRMerchantInfoModelObj: NearByQRMerchantInfo?
    var nearByQRTimeMSgModel: NearMeTimeStringModel?
    
   //this function will help us get the QR for user
    func getDataForQRScan(finished:@escaping () -> Void){
        nearByQRModelObj = nil
        
        createConnectionObj.requestNearByQR(completion: { [weak self]
            (response,error) in
            if let ifThereIsError = error{
                //no error
                if !ifThereIsError{
                    self?.nearByQRModelObj = response
                    finished()
                }else{
                    finished()
                }
            }
        })
    }
    
    //this function will help user to get the current transaction Details
    func getNearByCurrentTransactionDetails(qrData: String,finished:@escaping () -> Void){
        nearByQRPayModelObj = nil
        createConnectionObj.requestNearByCurrentTransaction(qrID: qrData,completion: { [weak self]
            (response,error) in
            if let ifThereIsError = error{
                //no error
                if !ifThereIsError{
                    self?.nearByQRPayModelObj = response
                    finished()
                }else{
                    finished()
                }
            }
        })
    }
    
    //this function will be used to pass the merchant information after we scan the qr
    func sendMerchantInformationToTheServer(merchantInfo: String,finished:@escaping () -> Void){
        nearByQRMerchantInfoModelObj = nil
        createConnectionObj.sendMerchantInfo(merchantInfo: merchantInfo,completion: { [weak self]
            (response,error) in
            if let ifThereIsError = error{
                //no error
                if !ifThereIsError{
                    self?.nearByQRMerchantInfoModelObj = response
                    finished()
                }else{
                    finished()
                }
            }
        })
    }
    
    //This function will give you details of time for QR expiry and msg to display customer
    func getQRTimeMsg(finished:@escaping () -> Void){
           nearByQRTimeMSgModel = nil
           createConnectionObj.getQRTimeMsg(completion: { [weak self]
               (response,error) in
               if let ifThereIsError = error{
                   //no error
                   if !ifThereIsError{
                    self?.nearByQRTimeMSgModel = response
                       finished()
                   }else{
                       finished()
                   }
               }
           })
       }
}
