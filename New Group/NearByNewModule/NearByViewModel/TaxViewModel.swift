//
//  TaxViewModel.swift
//  OK
//
//  Created by Tushar Lama on 03/06/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation

class TaxViewModel{
   
    var createConnectionObj = NearByConnection()
    var paymentConfirmationModel: TaxFinalModel?
    var trasactionHistory: TaxInvoiceModel?
    
    //this function will be used to pass user information
       func sendMerchantInformationToTheServer(merchantInfo: [String],finished:@escaping () -> Void){
           paymentConfirmationModel = nil
           createConnectionObj.sendMerchantTaxInfo(merchantInfo: merchantInfo,completion: { [weak self]
               (response,error) in
               if let ifThereIsError = error{
                   //no error
                   if !ifThereIsError{
                       self?.paymentConfirmationModel = response
                       finished()
                   }else{
                       finished()
                   }
               }
           })
       }
    
    //this function will return all the transaction related to tax payment history
    func getTaxTransactionhistory(finished:@escaping () -> Void){
        trasactionHistory = nil
        createConnectionObj.getTaxTransactionhistory(completion: { [weak self]
            (response,error) in
            if let ifThereIsError = error{
                //no error
                if !ifThereIsError{
                    self?.trasactionHistory = response
                    finished()
                }else{
                    finished()
                }
            }
        })
    }
    
  
    

    
}
