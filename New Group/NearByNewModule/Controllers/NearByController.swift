//
//  NearByController.swift
//  OK
//
//  Created by Tushar Lama on 16/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit
import AVFoundation

class NearByController: OKBaseController {
    @IBOutlet var scanQRButton: UIButton!{
        didSet{
            scanQRButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)        }
    }
    @IBOutlet var showQRButton: UIButton!{
        didSet{
            showQRButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)        }
    }
    @IBOutlet var scanQRView: UIView!
    @IBOutlet var showQRView: UIView!
    @IBOutlet var qrImageView: UIImageView!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var showQRButtonWidth: NSLayoutConstraint!
    @IBOutlet var flashView: UIView!
    @IBOutlet var flashImageView: UIImageView!
    @IBOutlet var ReportView: UIView!
    
    @IBOutlet var generateReceiptButton: DesignableButton!{
        didSet{
            generateReceiptButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            generateReceiptButton.setTitle(generateReceiptButton.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet var qrCodeView: UIView!
    var nearByViewModel = NearByViewModel()
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var minLabel: UILabel!
   
    var timerQR: Timer?
    var totalTime = 120
    var timeAddedFromServer = 0
    var timeStart = false
    var isQrGeneratedSuccess = false
    var qrInfoTitle = [String]()
    var hasFinishedQRCall  = false
    var qrImage: UIImage?
    var qrInfoDetails = [String]()
    var isComingFromRecent = false
    
    //this variable will be used to manage screen between near by and tax payment
    var isComingFromTaxPayment = false
    
    var previousTime = Date()
    var currentTime = Date()
    var previoushour = 0
    var previousMinute = 0
    var previousSecond = 0
    var currentHour = 0
    var currentMinute = 0
    var currentSecond = 0
    var wentBackGround = false
    var formatter = DateFormatter()
    var qrData:  String?
    var qrInfoLocalTitle = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      self.navigationController?.navigationBar.topItem?.title = ""
        
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "hh:mm:ss"
        NotificationCenter.default.addObserver(self, selector: #selector(moveToBackGround), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(enterForgound), name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.appWillResignActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    @objc func appWillResignActive() {
       
        self.flashImageView.isHighlighted = false
        if let device = AVCaptureDevice.default(for: AVMediaType.video) {
            if (device.hasTorch) {
                do {
                    try device.lockForConfiguration()
                    if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                        self.flashImageView.isHighlighted = false
                        device.torchMode = AVCaptureDevice.TorchMode.off
                    }
//                    else {
//                        self.flashImageView.isHighlighted = true
//                        do {
//                            try device.setTorchModeOn(level: 1.0)
//                        } catch {
//                            println_debug(error)
//                        }
//                    }
                    device.unlockForConfiguration()
                } catch {
                    println_debug(error)
                }
            }
        }
        
        
    }
    
    @objc func moveToBackGround() {
        let date = Date()
        previousTime = date
        let str = formatter.string(from: previousTime)
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: formatter.date(from: str) ?? Date())
        let minutes = calendar.component(.minute, from: formatter.date(from: str) ?? Date())
        let second = calendar.component(.second, from: formatter.date(from: str) ?? Date())
        previoushour = hour
        previousMinute = minutes
        previousSecond = second
        wentBackGround = true
    }
    
    @objc func enterForgound() {
        if wentBackGround {
            diffTime()
            wentBackGround = false
        }
    }
    
    func diffTime() {
        let str = formatter.string(from: Date())
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: formatter.date(from: str) ?? Date())
        let minutes = calendar.component(.minute, from: formatter.date(from: str) ?? Date())
        let second = calendar.component(.second, from: formatter.date(from: str) ?? Date())
        currentHour = hour
        currentMinute = minutes
        currentSecond = second
        
        if currentHour < previoushour {
            currentMinute = currentMinute + 60
        }
        if currentMinute < previousMinute {
            currentSecond = currentSecond + 60
        }
        let finalMin = currentMinute - previousMinute
        let finalSecond = currentSecond - previousSecond
        totalTime =  totalTime - (finalMin * 60 + finalSecond)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUI()
        navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        //loading all the content to scan
        // loadCamera()
        //        if (captureSession?.isRunning == false) {
        //            captureSession.startRunning()
        //        }
    }
    
    //checking the camera session and invalidating the timer when the view gets disappers
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.flashImageView.isHighlighted = false
        if let timer = self.timerQR {
            timer.invalidate()
        }
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //this will work for scanner only in one mode
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    //call the invoice transaction class
    @IBAction func onClickReportButton(_ sender: Any) {
        if #available(iOS 13.0, *) {
            
            
            let obj = self.storyboard?.instantiateViewController(identifier: NearByConstant.kTaxInvoiceViewController) as? TaxInvoiceViewController
            if let isValid = obj{
                self.navigationController?.pushViewController(isValid, animated: true)
            }
        } else {
            if let obj = self.storyboard?.instantiateViewController(withIdentifier: NearByConstant.kTaxInvoiceViewController) as? TaxInvoiceViewController{
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }
    }
    
    @IBAction func onClickGenerateReceipt(_ sender: Any) {
        self.getTransactionDetail(qrData: qrData ?? "")
        
    }
    
    //invoke the flash button
    @IBAction func onClickFlashButtonm(_ sender: Any) {
        if let device = AVCaptureDevice.default(for: AVMediaType.video) {
            if (device.hasTorch) {
                do {
                    try device.lockForConfiguration()
                    if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                        self.flashImageView.isHighlighted = false
                        device.torchMode = AVCaptureDevice.TorchMode.off
                    } else {
                        self.flashImageView.isHighlighted = true
                        do {
                            try device.setTorchModeOn(level: 1.0)
                        } catch {
                            println_debug(error)
                        }
                    }
                    device.unlockForConfiguration()
                } catch {
                    println_debug(error)
                }
            }
        }
    }
    
    //This will work for buttom back action on controller
    @IBAction func onClickBackButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        ////        if backButton.currentTitle == "Check Transaction".localized{
        ////            if let isValidObj = self.nearByViewModel.nearByQRModelObj{
        //////                if let qrData = isValidObj.data{
        //////                  //  self.getTransactionDetail(qrData: qrData)
        //////                }
        ////            }
        ////        }else{
        //            if #available(iOS 13.0, *) {
        //                let obj = self.storyboard?.instantiateViewController(identifier: NearByConstant.kNearByQRInfoController) as? NearByQRInfoController
        //                if let isValid = obj{
        //                    self.navigationController?.pushViewController(isValid, animated: true)
        //                }
        //            } else {
        //                // Fallback on earlier versions
        //               if let ViewController = self.storyboard?.instantiateViewController(withIdentifier: NearByConstant.kNearByQRInfoController) as? NearByQRInfoController{
        //                          self.navigationController?.pushViewController(ViewController, animated: true)
        //                      }
        //            }
        // }
    }
    
    
    //commented now will be used for future when scanning will also be done
    //This will be called when the user press scan qr
    @IBAction func onClickScanQR(_ sender: Any) {
        //        scanQRView.backgroundColor = .white
        //        showQRView.backgroundColor = kDarkBlueColor
        //        if let timer = self.timerQR {
        //            timer.invalidate()
        //        }
        //
        //        if isQrGeneratedSuccess{
        //            isQrGeneratedSuccess = false
        //            DispatchQueue.main.async {
        //                self.loadCamera()
        //            }
        //        }
    }
    
    func stopSession() {
        if captureSession.isRunning {
            DispatchQueue.global().async {
                self.captureSession.stopRunning()
                self.previewLayer.removeFromSuperlayer()
                self.loadCamera()
            }
        }
    }
    
    
    //This will be called when the user press show qr
    @IBAction func onClickShowQR(_ sender: Any) {
        scanQRView.backgroundColor = kDarkBlueColor
        showQRView.backgroundColor = .white
        OKPayment.main.authenticate(screenName: "Tushar", delegate: self)
    }
    
    //this function will be called to generate QRCode
    fileprivate func getQR(){
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            nearByViewModel.getDataForQRScan( finished:{
                progressViewObj.removeProgressView()
                //checking if data is available
                if let isValidObj = self.nearByViewModel.nearByQRModelObj{
                    if let qrData = isValidObj.data{
                        DispatchQueue.main.async {
                            self.setTimer()
                            self.qrData = qrData
                            self.qrImageView.image = UitilityClass.getQRImageNew(stringQR:qrData,withSize: 10,logoName: "NearMeNew")
                            //                            if (self.captureSession?.isRunning == true) {
                            //                                self.captureSession.stopRunning()
                            //                                self.previewLayer.removeFromSuperlayer()
                            //                                self.view.backgroundColor = UIColor(red: 199.0/255.0, green: 199.0/255.0, blue: 204.0/255.0, alpha: 1.0)
                            //                                self.setTimer()
                            //                                self.isQrGeneratedSuccess = true
                            //                                self.qrImageView.image = UitilityClass.getQRImage(stringQR:qrData,withSize: 10,logoName: "NearMeNew")
                            //                            }
                        }
                    }else{
                        self.showErrorAlert(errMessage: isValidObj.msg ?? "")
                    }
                }else{
                    self.showErrorAlert(errMessage: "Some error happen please try again".localized)
                }
            })
        } else {
            self.noInternetAlert()
            progressViewObj.removeProgressView()
        }
    }
    //"cef4d8dc-b056-487b-a777-dde3efb584b0-202087"
    //this function will give details of the user transaction
    fileprivate func getTransactionDetail(qrData: String) {
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            nearByViewModel.getNearByCurrentTransactionDetails(qrData: qrData ,finished:{
                progressViewObj.removeProgressView()
                //checking if data is available
                if let obj = self.nearByViewModel.nearByQRPayModelObj{
                    DispatchQueue.main.async { [weak self] in
                        //if let timer = self?.timerQR {
                           // timer.invalidate()
                           // self?.timeLabel.isHidden = true
                           // self?.minLabel.isHidden = true
                            if obj.code ?? 0 == 200{
                                DispatchQueue.main.async {
                                    self?.navigateToReceipt()
                                }
                            }else{
                                DispatchQueue.main.async {
                                     alertViewObj.wrapAlert(title: "", body: obj.msg ?? "", img: nil)
                                                   alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                                   })
                                                   alertViewObj.showAlert(controller: self)
                                }
                               // self?.showErrorAlert(errMessage: obj.msg ?? "")
                            }
                       // }
                    }
                }else{
                    //some problem
                }
            })
        } else {
            self.noInternetAlert()
            progressViewObj.removeProgressView()
        }
    }
    
    
    func navigateToReceipt(){
        if let obj = self.storyboard?.instantiateViewController(withIdentifier: "NearMeQrPayment") as? NearMeQrPaymentViewController{
            if let isValidObj = self.nearByViewModel.nearByQRPayModelObj{
                obj.nearByQRPayModelObj = isValidObj
            }
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
    
    //    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    //        if segue.identifier == "NearMeQrPaymentSegue" {
    //            if let vc = segue.destination as? NearMeQrPaymentViewController {
    //                if let isValidObj = self.nearByViewModel.nearByQRPayModelObj{
    //                      vc.nearByQRPayModelObj = isValidObj
    ////                    vc.recordArray.append(["Merchant Name": isValidObj.merchantName ?? ""])
    ////                    vc.recordArray.append(["Merchant ID": isValidObj.paymentTransId ?? ""])
    ////                    vc.recordArray.append(["Near Me Reference Number": isValidObj.merRefNumber ?? ""])
    ////                    vc.recordArray.append(["Amount": isValidObj.amount ?? ""])
    ////                    vc.recordArray.append(["OK$ transaction ID": isValidObj.paymentTransId ?? ""])
    ////                    vc.recordArray.append(["Transaction Date and Time": isValidObj.transactionTime ?? ""])
    ////                    vc.recordArray.append(["Transaction Type ( Near Me Payment)": isValidObj.paymentStatus ?? ""])
    //                }
    //            }
    //        }
    //    }
    
    
    //this function will post merchant info to the server
    fileprivate func postMerchantInfoToServer(merchantInfo: String){
        if appDelegate.checkNetworkAvail() {
            hasFinishedQRCall = true
            progressViewObj.showProgressView()
            nearByViewModel.sendMerchantInformationToTheServer(merchantInfo: merchantInfo,finished:{
                progressViewObj.removeProgressView()
                //checking if data is available
                if let isValidObj = self.nearByViewModel.nearByQRMerchantInfoModelObj{
                    self.hasFinishedQRCall = false
                    if isValidObj.isTransactionSuccess{
                        //success
                        if (self.captureSession?.isRunning == false) {
                            self.captureSession.startRunning()
                        }
                        self.showErrorAlert(errMessage: "success")
                    }else{
                        //failed
                        self.showErrorAlert(errMessage: isValidObj.msg ?? "")
                    }
                }else{
                    self.hasFinishedQRCall = false
                    self.showErrorAlert(errMessage: "Some error happen please try again".localized)
                }
            })
        } else {
            self.noInternetAlert()
            progressViewObj.removeProgressView()
        }
    }
    
    
    //get the msg and time to show QR
    //this function will post merchant info to the server
    fileprivate func getDataForTimeMsg(){
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            nearByViewModel.getQRTimeMsg(finished:{
                progressViewObj.removeProgressView()
                //checking if data is available
                if let isValidData = self.nearByViewModel.nearByQRTimeMSgModel{
                    if isValidData.code == 200{
                        if let timeValue = isValidData.time{
                            self.totalTime = Int(timeValue) ?? 0
                            self.timeAddedFromServer = Int(timeValue) ?? 0
                        }else{
                            self.totalTime = 120
                        }
                        
                        if appDel.currentLanguage == "my"{
                            self.showMsgAlert(msg: isValidData.showAlertCustomMsgMyZaw ?? "")
                        }else if appDel.currentLanguage == "uni"{
                            self.showMsgAlert(msg: isValidData.showAlertCustomMsgMyUni ?? "")
                        }else{
                             self.showMsgAlert(msg: isValidData.showAlertCustomMsgEng ?? "")
                        }
                    }else{
                        self.showErrorAlert(errMessage: isValidData.msg ?? "")
                    }
                }else{
                    self.showErrorAlert(errMessage: "Some error happen please try again".localized)
                }
            })
        } else {
            self.noInternetAlert()
            progressViewObj.removeProgressView()
        }
    }
    
    fileprivate func showMsgAlert(msg: String){
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: "", body: msg, img: #imageLiteral(resourceName: "NearMeNew"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                self.getQR()
            })
            alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
                self.navigationController?.popToRootViewController(animated: false)
            })
            alertViewObj.showAlert(controller: self)
        }
    }
    
}


extension NearByController: BioMetricLoginDelegate{
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String){
        if isSuccessful{
            getQR()
        }else{
            //login not successful so cannot generate QR
            //self.showErrorAlert(errMessage: "login not successful so cannot generate QR")
        }
    }
}

extension NearByController{
    
    fileprivate func setUI(){
        //setting all the ui update here
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
       // navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
        
        if isComingFromTaxPayment{
            generateReceiptButton.isHidden = true
            // showQRButtonWidth.constant = self.view.frame.size.width
            
            //loading all the content to scan
            loadCamera()
            if (captureSession?.isRunning == false) {
                captureSession.startRunning()
            }
            scanQRButton.titleLabel?.font = UitilityClass.getZwagiFontWithSize(size: appButtonSize)
            scanQRButton.setTitle("Scan QR".localized, for: .normal)
              self.navigationItem.title = "Tax Payment".localized
          //  self.title = "Tax Payment".localized
            flashView.isHidden = false
            ReportView.isHidden = false
        }else{
            generateReceiptButton.isHidden = false
            // showQRButtonWidth.constant = self.view.frame.size.width/2
            scanQRButton.setTitle("Show QR".localized, for: .normal)
            self.title = "Near me Payment".localized
            flashView.isHidden = true
            ReportView.isHidden = true
            getDataForTimeMsg()
           // self.setTimer()
            if let isValidImage = qrImage{
                self.qrImageView.image = isValidImage
            }
        }
        
        // showQRButtonWidth.constant = self.view.frame.size.width/2
        scanQRView.backgroundColor = .white
        //  showQRView.backgroundColor = kDarkBlueColor
        backButton.titleLabel?.font = UitilityClass.getZwagiFontWithSize(size: 17.0)
        backButton.setTitle("Back".localized, for: .normal)
        //  scanQRButton.setTitle("Scan QR".localized, for: .normal)
        // showQRButton.setTitle("Show QR".localized, for: .normal)
        
        timeLabel.layer.cornerRadius = 8.0
        timeLabel.layer.masksToBounds = true
        timeLabel.layer.borderColor = kDarkBlueColor.cgColor
        timeLabel.layer.borderWidth = 3.0
        backButton.isHidden = false
        navigationItem.hidesBackButton = false
    }
    
    //setting timer to start from 5 min
    fileprivate func setTimer(){
        let value = totalTime/60
        
        if value<10{
            timeLabel.text = "\(0)\(value):00"
        }else{
            timeLabel.text = "\(value):00"
        }
        
       // timeLabel.text  = "05:00"
        
        self.startOtpTimer(time: totalTime - 1)
    }
    
    private func startOtpTimer(time: Int) {
        //5 min 300 sec
        self.totalTime = time
        self.timerQR = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    //once 5 min is over updating the timer here
    @objc func updateTimer() {
        self.timeLabel.text = self.timeFormatted(self.totalTime)
        
        if totalTime > 0 {
            totalTime -= 1
        }else {
            if let timer = self.timerQR {
                timer.invalidate()
                alertViewObj.wrapAlert(title: "", body: "QR Code Expired . Do You Want To Refresh??".localized, img: #imageLiteral(resourceName: "NearMeNew"))
                alertViewObj.addAction(title: "YES".localized, style: .target , action: {
                    self.totalTime = self.timeAddedFromServer
                    self.getQR()
                })
                alertViewObj.addAction(title: "NO".localized, style: .target , action: {
                    //                    self.scanQRView.backgroundColor = .white
                    //                    self.showQRView.backgroundColor = kDarkBlueColor
                    //                    if let timer = self.timerQR {
                    //                        timer.invalidate()
                    //                    }
                    //                    if self.isQrGeneratedSuccess{
                    //                        self.isQrGeneratedSuccess = false
                    //                        DispatchQueue.main.async {
                    //                            self.loadCamera()
                    //                        }
                    //                    }
                    self.navigationController?.popToRootViewController(animated: false)
                })
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%01d:%02d", minutes, seconds)
    }
    
    fileprivate func loadCamera(){
        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = CGRect(x: 0, y: 35, width: self.view.frame.size.width, height: self.view.frame.size.height - 80)
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)
        view.addSubview(flashView)
        view.addSubview(backButton)
        view.addSubview(ReportView)
        view.bringSubviewToFront(flashView)
        view.bringSubviewToFront(backButton)
        view.bringSubviewToFront(ReportView)
        captureSession.startRunning()
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported".localized, message: "Your device does not support scanning a code from an item. Please use a device with a camera.".localized, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    func parseQRData(code: String){
        let data = code
        let values = data.components(separatedBy: ";")
        qrInfoLocalTitle.removeAll()
        for i in 0..<values.count{
            let data = values[i].components(separatedBy: "=")
            if data.indices.contains(0){
           
                if data[0].replacingOccurrences(of: " ", with: "").contains(find: "TransactionID") || data[0].replacingOccurrences(of: " ", with: "").contains(find: "Transaction ID"){
                        print("TransactionID".localized)
                       qrInfoLocalTitle.append("TransactionID".localized)
                    }else if data[0].replacingOccurrences(of: " ", with: "").contains(find: "TransactionCode"){
                       qrInfoLocalTitle.append("Transaction Code".localized)
                    }else{
                       qrInfoLocalTitle.append(data[0].replacingOccurrences(of: "\n", with: ""))
                    }
                
                
              
            }
        }
        
    }
    
    func callInvalidQR(){
        DispatchQueue.main.async {
             alertViewObj.wrapAlert(title: "", body: "Please scan correct QR Code".localized, img: nil)
                   alertViewObj.addAction(title: "YES".localized, style: .target , action: {
                       self.loadCamera()
                       if (self.captureSession?.isRunning == false) {
                           self.captureSession.startRunning()
                       }
                   })
                   alertViewObj.showAlert(controller: self)
        }
       
    }
    
    
    func found(code: String) {
        //this work for tax Payment
       // print(code)
        if isComingFromTaxPayment{
            if code.count>0{
                let encodedValue = AESCrypt.decrypt(code, password: "m2n1shlko@$p##d")
                if encodedValue != ""{
                    parseQRData(code: encodedValue ?? "")
                    let valueDAta = encodedValue
                    if let  isValidData = valueDAta{
                        let data = isValidData.components(separatedBy: "=")
                        if data.indices.contains(1){
                            let dataNew = data[1].components(separatedBy: ";")
                            if dataNew.indices.contains(0){
                                let newValue = dataNew[0]
                                //then only show data
                                print(newValue)
                                if newValue != UserModel.shared.mobileNo{
                                    if !qrInfoLocalTitle.contains("Transaction Time") {
                                        self.callInvalidQR()
                                    }else{
                                        DispatchQueue.main.async {
                                            if #available(iOS 13.0, *) {
                                                if let obj = self.storyboard?.instantiateViewController(identifier: NearByConstant.kGeneriCustomAlertViewController) as? GeneriCustomAlertViewController{
                                                    obj.delegate = self
                                                    obj.responseCode = encodedValue
                                                    self.navigationController?.present(obj, animated: true, completion: nil)
                                                }
                                            } else {
                                                if let ViewController = self.storyboard?.instantiateViewController(withIdentifier: NearByConstant.kGeneriCustomAlertViewController) as? GeneriCustomAlertViewController{
                                                    ViewController.delegate = self
                                                    ViewController.responseCode = encodedValue
                                                    self.navigationController?.present(ViewController, animated: true, completion: nil)
                                                }
                                            }
                                        }
                                    }
                                    
                                }else{
                                    self.callInvalidQR()
                                }
                            }
                        }
                                            
                        
                    }else{
                        self.callInvalidQR()
                    }
                    
                }else{
                    parseQRData(code: code)
                    if !qrInfoLocalTitle.contains("Transaction Code".localized)  && !qrInfoLocalTitle.contains("TransactionID".localized){
                        self.callInvalidQR()
                    }else{
                        DispatchQueue.main.async {
                            if #available(iOS 13.0, *) {
                                let obj = self.storyboard?.instantiateViewController(identifier: NearByConstant.kNearByQRInfoController) as? NearByQRInfoController
                                if let isValid = obj{
                                    isValid.isComingFromTaxPayment = true
                                    isValid.taxInfoFromQR = code
                                    isValid.isComingFromRecent =  self.isComingFromRecent
                                    self.navigationController?.pushViewController(isValid, animated: true)
                                }
                            } else {
                                // Fallback on earlier versions
                                if let ViewController = self.storyboard?.instantiateViewController(withIdentifier: NearByConstant.kNearByQRInfoController) as? NearByQRInfoController{
                                    ViewController.isComingFromTaxPayment = true
                                    ViewController.taxInfoFromQR = code
                                    ViewController.isComingFromRecent =  self.isComingFromRecent
                                    self.navigationController?.pushViewController(ViewController, animated: true)
                                }
                            }
                        }
                    }
                }
            }
        }else{
            if !hasFinishedQRCall{
                //this will completely work for near by
                postMerchantInfoToServer(merchantInfo: code)
            }
        }
    }
    
}

extension NearByController: GeneriCustomAlertViewDelegate{
   
    func closeClick() {
        loadCamera()
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    
}


extension NearByController: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
    }
}

extension NearByController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return qrInfoTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NearByConstant.kNearByScanQRDetailsCell) as? NearByScanQRDetailsCell else{
            return UITableViewCell()
        }
        cell.selectionStyle = .none
        let height =  heightForView(text:qrInfoDetails[indexPath.row] , font: UitilityClass.getZwagiFontWithSize(size: 14.0), width: screenWidth)
        if height <= 22.0{
            cell.detailsHeight.constant = 22.0
        }else{
            cell.detailsHeight.constant = height
        }
        
        cell.title.text = qrInfoTitle[indexPath.row]
        cell.details.text = qrInfoDetails[indexPath.row]
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height =  heightForView(text: qrInfoDetails[indexPath.row], font: UitilityClass.getZwagiFontWithSize(size: 14.0), width: screenWidth)
        if height <= 22.0{
            return 70.0
        }else{
            let newHeight = height - 22.0
            return 70.0 + newHeight
        }
        
    }
}
