//
//  TaxReceiptFinalController.swift
//  OK
//
//  Created by Tushar Lama on 03/06/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit
import PDFKit



class TaxReceiptFinalController: OKBaseController {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var buttonHeight: NSLayoutConstraint!
    @IBOutlet var shareButton: UIButton!
    @IBOutlet var finalReceiptTable: UITableView!
    var qrTitle = [String]()
    var qrInfo = [String]()
    var qrCodeString: String?
    var qrImage: UIImage?
    var trasactionHistory: TaxInvoiceDetail?
    @IBOutlet var PDFViewContainer:PDFView?
    var newTusharPDf = UIView()
    var dateTimeStr: String?
    var isComingFromInbox = false
    var pdfName:  URL?
    var pdfDocument: PDFDocument?
    var iscomingFromShare = false
     var isComingFromRecent = false
    var transactionDate = ""
    var transactionTime = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        makePDF()
        if iscomingFromShare{
            openPDFToShare()
            shareButton.isHidden = true
            buttonHeight.constant = 0.0
        }else{
            shareButton.isHidden = false
            buttonHeight.constant = 40.0
        }
        
    }
    
    
    func openPDFToShare(){
        self.PDFViewContainer?.autoScales = true
        self.PDFViewContainer?.backgroundColor = UIColor.lightGray
        
        if let url = self.pdfName{
            let message = MessageWithSubject(subject: "OK$ Tax Receipt", message: "This is OK$ Tax Receipt.")
            var activityItems = [Any]()
            activityItems.append(message)
            activityItems.append(url)
           
            let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
            activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.copyToPasteboard,UIActivity.ActivityType.assignToContact, UIActivity.ActivityType.addToReadingList,UIActivity.ActivityType.postToFlickr, UIActivity.ActivityType.postToVimeo]
            activityViewController.popoverPresentationController?.sourceView = self.view
            DispatchQueue.main.async {
                self.present(activityViewController, animated: true, completion: {
                })
            }
            
//            //this code will work when i will press the cross button of activity view controller
//            activityViewController.completionWithItemsHandler = { (activityType, completed:Bool, returnedItems:[Any]?, error: Error?) in
//                if !completed {
//                    DispatchQueue.main.async {
//                        self.navigationController?.popViewController(animated: false)
//                    }
//                }
//            }
        }
        
    }
    
     func getFormattedDate(date: Date, format: String) -> String {
        let dateformat = DateFormatter()
        dateformat.dateFormat = format
        return dateformat.string(from: date)
    }
    
    func setDateformat(dateString: String) ->String{
        let localDate = dateString
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "dd-MMM-yyyy"//this your string date format
        let date = dateFormatter.date(from: localDate)
        //dateFormatter.dateFormat = "EEE, dd-MM-yyyy HH:mm:ss"///this is what you want to convert format
        dateFormatter.dateFormat = "EEE, dd-MMM-yyyy"
        if let data = date{
            let timeStamp = dateFormatter.string(from: data)
            return timeStamp
        }else{
            return dateString
        }
    }
    
    
    func createPdfFromView(aView: UIView, saveToDocumentsWithFileName pdfFileName: String) -> URL? {
        let render = UIPrintPageRenderer()
        let page = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 1200) // A4, 72 dpi
        let printable = page.insetBy(dx: 0, dy: 0)
        render.setValue(NSValue(cgRect: page), forKey: "paperRect")
        render.setValue(NSValue(cgRect: printable), forKey: "printableRect")
        let priorBounds = aView.bounds
        let fittedSize = aView.sizeThatFits(CGSize(width:priorBounds.size.width, height:1200))
        aView.bounds = CGRect(x:0, y:0, width:fittedSize.width, height:1200)
        let pdfPageBounds = CGRect(x:0, y:0, width:aView.frame.width, height:1200)
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds,nil)
        let pageOriginY: CGFloat = 0
        UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
        UIGraphicsGetCurrentContext()!.saveGState()
        UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: -pageOriginY)
        aView.layer.render(in: UIGraphicsGetCurrentContext()!)
        UIGraphicsGetCurrentContext()!.restoreGState()
        UIGraphicsEndPDFContext()
        aView.bounds = priorBounds
        guard let documentDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else  { return nil}
        let properPDFFileName = pdfFileName.replacingOccurrences(of: ".pdf", with: "") + ".pdf"
        let pdfUrl = documentDirectories + "/" + properPDFFileName
        //Delete file if alreay exists
        do {
            try FileManager.default.removeItem(atPath: pdfUrl)
        } catch let error as NSError {
            println_debug("Error: \(error.domain)")
        }
        //Write file
        if pdfData.write(toFile: pdfUrl, atomically: true) {
            println_debug("Successfully wrote to \(pdfUrl)")
            let pathUrl = URL(fileURLWithPath: documentDirectories)
            let fileUrl = pathUrl.appendingPathComponent(properPDFFileName)
            return fileUrl
        }
        return nil
    }
    
    
    func makePDF(){
        newTusharPDf.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 1200)
        let logoImage = UIImageView()
        logoImage.frame = CGRect(x: 20, y: 10, width: 50, height: 50)
        logoImage.image = UIImage(named: "tax_Logo")
        newTusharPDf.addSubview(logoImage)
        let taxLabel = UILabel()
        taxLabel.frame = CGRect(x: 90.0, y: 0, width: 300.0, height: 70.0)
        taxLabel.font =  UitilityClass.getZwagiFontWithSize(size: 13.0)
        taxLabel.numberOfLines = 5
        taxLabel.text = "The Republic of the Union of Myanmar Ministry of Planning,Finance and Industry. Internal Revenue Department Tax Reference".localized
        newTusharPDf.addSubview(taxLabel)
        newTusharPDf.bringSubviewToFront(taxLabel)
        
        let oKDollarLogo = UIImageView(frame: CGRect(x: newTusharPDf.frame.size.width/2.5, y:  taxLabel.frame.size.height + taxLabel.frame.origin.y + 10, width: 50.0, height: 50.0))
        oKDollarLogo.image = UIImage(named: "appIcon_Ok")
            
        let okDollarReceiptName = UILabel(frame: CGRect(x: newTusharPDf.frame.size.width/3, y:  oKDollarLogo.frame.size.height + oKDollarLogo.frame.origin.y + 10, width: 150.0, height: 30.0))
        okDollarReceiptName.font =  UIFont.init(name: appFont, size: 13)
        okDollarReceiptName.text = "Transaction Receipt".localized
        
        newTusharPDf.addSubview(oKDollarLogo)
        newTusharPDf.bringSubviewToFront(oKDollarLogo)
        newTusharPDf.addSubview(okDollarReceiptName)
        newTusharPDf.bringSubviewToFront(okDollarReceiptName)
        
        print(qrTitle)
        print(qrInfo)
        
        
        for i in 0..<qrTitle.count{
            let titleLabel = UILabel(frame: CGRect(x: 20, y: CGFloat(i) * 40 + 230, width: 160.0, height: 50))
            let valueLabel = UILabel(frame: CGRect(x: 170.0, y: CGFloat(i) * 40 + 230, width: 200.0, height: 50))
            //let borderView = UIView(frame: CGRect(x: 0.0, y: CGFloat(i) * 40 + 120, width: self.view.frame.size.width, height: 2))
           // borderView.backgroundColor = .white
            titleLabel.font =  UitilityClass.getZwagiFontWithSize(size: 12.0)
            valueLabel.font =  UitilityClass.getZwagiFontWithSize(size: 12.0)
           // titleLabel.frame = CGRect(x: 20, y: Double(80 + i * 2), width: 160.0, height: 40.0)
            titleLabel.numberOfLines = 3
            valueLabel.numberOfLines = 3
            
            if qrTitle.indices.contains(i){
                print(qrTitle[i])
//                if qrTitle[i].localized == "TransactionId".localized || qrTitle[i].localized == "TransactionId"  || qrTitle[i].localized == "Transaction ID"{
//                    titleLabel.text = "Transaction Code".localized
//                }else{
//                     titleLabel.text = qrTitle[i]
//                }
//
                
                if qrTitle[i] == "TransactionId".localized || qrTitle[i] == "Transaction Id" || qrTitle[i] == "Transaction ID"{
                    titleLabel.text = "Transaction Code".localized
                }else if qrTitle[i] == "Income Year"{
                     titleLabel.text = "Fiscal Year".localized
                }else if qrTitle[i] == "Id Number".localized{
                   titleLabel.text = "NRC".localized
                }else if qrTitle[i] == "Paid Amount".localized{
                   titleLabel.text = "Bill Amount".localized
                }else{
                    titleLabel.text = qrTitle[i]
                }
                
                
            }
            
            
            if qrInfo.indices.contains(i){
                
                if qrTitle[i] == "Paid Amount".localized || qrTitle[i] == "Bill Amount".localized{
                    
                    if qrInfo[i].contains(find: "MMK"){
                        valueLabel.text =  " : " + wrapAmountWithCommaDecimal(key: qrInfo[i])
                    }else{
                       valueLabel.text =  " : " + wrapAmountWithCommaDecimal(key: qrInfo[i]) + " MMK"
                    }
                }else{
                    valueLabel.text =  " : " + qrInfo[i]
                }
                
                
            }
            
            //for  last index
            if i == qrTitle.count - 1{
                let dateImage = UIImageView(frame: CGRect(x: 20, y:  valueLabel.frame.size.height + valueLabel.frame.origin.y + 20, width: 15.0, height: 15.0))
                let timeImage = UIImageView(frame: CGRect(x: 265.0, y:  valueLabel.frame.size.height + valueLabel.frame.origin.y + 20, width: 15.0, height: 15.0))
                let dateTime = UILabel(frame: CGRect(x: 55.0, y:  valueLabel.frame.size.height + valueLabel.frame.origin.y + 18, width: 160.0, height: 20))
                let timeLabel = UILabel(frame: CGRect(x: 290.0, y:  valueLabel.frame.size.height + valueLabel.frame.origin.y + 17.5 , width: 150.0, height: 20))
                timeLabel.textAlignment = .center
                dateImage.image = UIImage(named: "merchantCalendar")
                timeImage.image = UIImage(named: "merchantClock")
                
            //  dateTime.backgroundColor = .yellow
                dateTime.font =  UIFont.init(name: appFont, size: 13)
                timeLabel.font = UIFont.init(name: appFont, size: 13)
                
                timeLabel.text = transactionTime
                dateTime.text = transactionDate

                let imageViewQR = UIImageView(frame: CGRect(x: self.view.frame.size.width/3, y:  dateTime.frame.size.height + dateTime.frame.origin.y + 40.0, width: 100.0, height: 100.0))
                if let value = qrImage{
                    imageViewQR.image = value
                }
                
                self.newTusharPDf.addSubview(dateImage)
                self.newTusharPDf.addSubview(timeImage)
                self.newTusharPDf.addSubview(timeLabel)
                self.newTusharPDf.addSubview(imageViewQR)
                self.newTusharPDf.addSubview(dateTime)
            }
            self.newTusharPDf.addSubview(titleLabel)
            self.newTusharPDf.addSubview(valueLabel)
           // self.newTusharPDf.addSubview(borderView)
        }
        
        
        let formatingDate = self.getFormattedDate(date: Date(), format: "yyyy-MM-dd'T'HH:mm:ssZZZZZ")
         print(formatingDate)

         let strPdfName = "OK$-TAX_PAYMENT_RECEIPT" + "\(Date().toMillis() ?? 232333333323223)" + "-" + formatingDate


        // let pdfUrl = self.pdfDataWithTableView(tableView: self.newTusharPDf, saveToDocumentsWithFileName: strPdfName)

         let pdfUrl = self.createPdfFromView(aView: self.newTusharPDf, saveToDocumentsWithFileName: strPdfName)
         

         if let document = PDFDocument(url: pdfUrl!) {
             document.delegate = self
             pdfDocument = document
         }

         guard let data = pdfDocument!.dataRepresentation() else { return }

         let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

         let docURL = documentDirectory.appendingPathComponent(strPdfName + ".pdf")

         do{

             try data.write(to: docURL)

         }catch(let error){
             print("error is \(error.localizedDescription)")
         }

         pdfName =  documentDirectory.appendingPathComponent(strPdfName + ".pdf")
        
    }
    
    
    @IBAction func shareAction(_ sender: UIButton) {
        openPDFToShare()
    }
}

extension TaxReceiptFinalController: UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
            return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if section == 1{
                return 1
            }else{
                return qrTitle.count
            }
    }
      
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: NearByConstant.kTaxReceiptCell) as? TaxReceiptCell else{
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            if let value = qrImage{
                cell.qrImageView.image = value
//                if let isValidDate = dateTimeStr{
//                   cell.dateLabel.text = isValidDate.components(separatedBy: " ")[0]
//                 cell.timeLabel.text = isValidDate.components(separatedBy: " ")[1]
//                }
                cell.dateLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
                cell.dateLabel.text = setDateformat(dateString: transactionDate)
                cell.timeLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
                cell.timeLabel.text = transactionTime
            }
            return cell
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: NearByConstant.kNearByPaymentCell) as? NearByPaymentCell else{
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            cell.backgroundColor = UIColor(red: 221.0/255.0, green: 221.0/255.0, blue: 221.0/255.0, alpha: 1.0)
            
            if qrInfo.indices.contains(indexPath.row){
                let height =  heightForView(text:qrInfo[indexPath.row] , font: UitilityClass.getZwagiFontWithSize(size: 13.0), width: screenWidth)
                
                if qrTitle.indices.contains(indexPath.row){
                    let titleHeight = heightForView(text:qrTitle[indexPath.row] , font: UitilityClass.getZwagiFontWithSize(size: 13.0), width: screenWidth)
                    
                    if height>titleHeight{
                        if height<=19.0{
                            cell.merchantLabelHeight.constant = 20.0
                        }else{
                            cell.merchantLabelHeight.constant = height + 10
                        }
                    }else{
                        if titleHeight<=20.0{
                            cell.merchantLabelHeight.constant = 20.0
                        }else{
                            cell.merchantLabelHeight.constant = titleHeight
                        }
                    }
                    
                    cell.titleLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
                    if qrTitle[indexPath.row] == "TransactionId".localized || qrTitle[indexPath.row] == "Transaction Id" || qrTitle[indexPath.row] == "Transaction ID"{
                        cell.titleLabel.text = "Transaction Code".localized
                    }else if qrTitle[indexPath.row] == "Income Year"{
                        cell.titleLabel.text = "Fiscal Year".localized
                    }else if qrTitle[indexPath.row] == "Id Number".localized{
                      cell.titleLabel.text = "NRC".localized
                    }else if qrTitle[indexPath.row] == "Paid Amount".localized{
                        cell.titleLabel.text = "Bill Amount".localized
                    }else{
                        cell.titleLabel.text = qrTitle[indexPath.row]
                    }
                    
                }
                
                cell.merchantValueLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
                if qrTitle[indexPath.row] == "Paid Amount".localized{
                    cell.merchantValueLabel.text = wrapAmountWithCommaDecimal(key: qrInfo[indexPath.row]) + " " + "MMK"
                }else{
                    cell.merchantValueLabel.text = qrInfo[indexPath.row]
                }
                
                    
            }
            
            return cell
        }
        
    }
      
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1{
            return 190.0
        }else{
            
            if qrInfo.indices.contains(indexPath.row){
                let height =  heightForView(text:qrInfo[indexPath.row] , font: UitilityClass.getZwagiFontWithSize(size: 16.0), width: screenWidth)
                if qrInfo.indices.contains(indexPath.row){
                    let infoHeight = heightForView(text:qrTitle[indexPath.row] , font: UitilityClass.getZwagiFontWithSize(size: 16.0), width: screenWidth)
                    if infoHeight>height{
                        if infoHeight<=19.0{
                            return 40.0
                        }else{
                            let newHeight = infoHeight - 10.0
                            return 40.0 + newHeight
                        }
                    }else{
                        if height<=19.0{
                            return 40.0
                        }else{
                            let newHeight = height - 10.0
                            return 40.0 + newHeight
                        }
                    }
                }
            }
            return 0.0
        }
        
    }
}


class MessageWithSubject: NSObject, UIActivityItemSource {
       
       let subject:String
       let message:String
       
       init(subject: String, message: String) {
           self.subject = subject
           self.message = message
           
           super.init()
       }
       
       func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
           return message
       }
       
       func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
           return message
       }
       
       func activityViewController(_ activityViewController: UIActivityViewController,
                                   subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
           return subject
       }
   }


extension Date {
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}

@available(iOS 11.0, *)
extension TaxReceiptFinalController : PDFDocumentDelegate {
    
}


extension TaxReceiptFinalController{
    
    fileprivate func setUI(){
        titleLabel.font = UitilityClass.getZwagiFontWithSize(size: 16.0)
        titleLabel.text = "The Republic of the Union of Myanmar Ministry of Planning,Finance and Industry. Internal Revenue Department Tax Reference".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationItem.title = "Tax Payment".localized
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        finalReceiptTable.layer.borderWidth = 1.0
        finalReceiptTable.layer.borderColor = UIColor(red: 221.0/255.0, green: 221.0/255.0, blue: 221.0/255.0, alpha: 1.0).cgColor
        self.finalReceiptTable.register(UINib(nibName: NearByConstant.kNearByPaymentCell, bundle: nil), forCellReuseIdentifier: NearByConstant.kNearByPaymentCell)
        self.finalReceiptTable.register(UINib(nibName: NearByConstant.kTaxReceiptCell, bundle: nil), forCellReuseIdentifier: NearByConstant.kTaxReceiptCell)
        self.finalReceiptTable.register(UINib(nibName: NearByConstant.kTaxPaymentInvoiceCell, bundle: nil), forCellReuseIdentifier: NearByConstant.kTaxPaymentInvoiceCell)
        self.shareButton.titleLabel?.font = UitilityClass.getZwagiFontWithSize(size: appButtonSize)
        self.shareButton.setTitle("Share".localized, for: .normal)
        self.finalReceiptTable.delegate = self
        self.finalReceiptTable.dataSource  = self
        self.finalReceiptTable.reloadData()
        
    }
    
}
