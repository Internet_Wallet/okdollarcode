//
//  NearByQRInfoController.swift
//  OK
//
//  Created by Tushar Lama on 16/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit




class NearByQRInfoController: OKBaseController, BioMetricLoginDelegate {
    @IBOutlet var qrInfoTableView: UITableView!
    @IBOutlet var nextPayButton: UIButton!
    //If its coming from login means the bottom button title will change to pay
    var isComingAfterLogin = false
    //if its coming from tax payment module i will change the ui
    var isComingFromTaxPayment = false
    var taxInfoFromQR: String?
    var isComingFromRecent = false
    var qrInfoTitle = [String]()
    var qrInfoDetails = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }    
    
    @IBAction func onClickNextPay(_ sender: Any) {
        OKPayment.main.authenticate(screenName: "Tushar", delegate: self)
    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String){
        if isSuccessful{
            DispatchQueue.main.async {
                if #available(iOS 13.0, *) {
                    let obj = self.storyboard?.instantiateViewController(identifier: NearByConstant.kNearByCheckPaymentDetails) as? NearByCheckPaymentDetails
                    if let isValid = obj{
                        isValid.isComingfromTaxModule = self.isComingFromTaxPayment
                        if self.isComingFromTaxPayment{
                            isValid.qrInfo = self.qrInfoDetails
                            isValid.qrTitle = self.qrInfoTitle
                        }
                        isValid.isComingFromRecent =  self.isComingFromRecent
                        self.navigationController?.pushViewController(isValid, animated: true)
                    }
                } else {
                    // Fallback on earlier versions
                    if let obj = self.storyboard?.instantiateViewController(withIdentifier: NearByConstant.kNearByCheckPaymentDetails) as? NearByCheckPaymentDetails{
                        obj.isComingfromTaxModule = self.isComingFromTaxPayment
                        if self.isComingFromTaxPayment{
                            obj.qrInfo = self.qrInfoDetails
                            obj.qrTitle = self.qrInfoTitle
                        }
                        obj.isComingFromRecent =  self.isComingFromRecent
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                }
            }
        }
    }
}


extension NearByQRInfoController{
    
    fileprivate func setUI(){
        //performing all the ui update here when the view loads
        //this will be use to pase the data
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
        if isComingFromTaxPayment{
            self.navigationItem.title = "Tax Payment".localized
            nextPayButton.titleLabel?.font = UitilityClass.getZwagiFontWithSize(size: appButtonSize)
            nextPayButton.setTitle("Make Payment".localized, for: .normal)
            parseTaxQRInfo()
        }else{
            self.title = "Near me Payment".localized
            nextPayButton.setTitle("Next".localized, for: .normal)
            self.qrInfoTableView.separatorColor = .clear
        }
        
        self.qrInfoTableView.backgroundColor = .lightGray
        self.qrInfoTableView.register(UINib(nibName: NearByConstant.kNearByQRInfoCell, bundle: nil), forCellReuseIdentifier: NearByConstant.kNearByQRInfoCell)
        self.qrInfoTableView.register(UINib(nibName: NearByConstant.kNearByScanDetails, bundle: nil), forCellReuseIdentifier: NearByConstant.kNearByScanQRDetailsCell)
        self.qrInfoTableView.delegate = self
        self.qrInfoTableView.dataSource  = self
        self.qrInfoTableView.reloadData()
    }
    
    fileprivate func parseTaxQRInfo(){
        if let isValueAvailable = taxInfoFromQR{
            //parse data here
            let values = isValueAvailable.components(separatedBy: ";")
            print(values)
            if appDel.currentLanguage == "uni"{
                qrInfoTitle = ["Tax Payer".localized,"Taxpayer Identification Number".localized,"NRC".localized, "Tax Office".localized,
                               "Tax Type".localized,"Income Year".localized,"Bank Acc Number".localized,"Address".localized,"Supplier".localized,"BillAmountTax".localized,"TransactionID"]
            }else{
                qrInfoTitle = ["အခြန္ထမ္းအမည္","အခြန္ထမ္းမွတ္ပံုတင္အမွတ္","နုိင္ငံသား စိစစ္ေရးကတ္ျပားအမွတ္", "အခြန္ရံုး",
                               "အခြန္အမ်ဳိးအစား","ဝင္ေငြႏွစ္","ဘဏ္စာရင္းအမွတ္", "လိပ္စာ" ,"ေပးသြင္းသည့္ဘဏ္","ေပးေဆာင္သည့္ပမာဏ","TransactionID"]
            }
            
            
            
            //print(titleData)
            
            
            let title = values.compactMap({$0.components(separatedBy: "=")})
            
            
            
            let newTitle = title.compactMap({$0[0].replacingOccurrences(of: "\n", with: "")})
            let stringArray = newTitle.compactMap({$0.replacingOccurrences(of: " ", with: "")})
            
            
            print(stringArray)
            var indexNumber: Int?
            
            if stringArray.contains("TransactionID") || stringArray.contains("Transaction ID"){
                indexNumber = 2
            }else if stringArray.contains("Transaction Code") || stringArray.contains("TransactionCode"){
                indexNumber = 2
            }else{
                
            }
            
            for i in 0..<values.count{
                let data = values[i].components(separatedBy: "=")
                
                //                if data.indices.contains(0){
                //                    qrInfoTitle.append(data[0].replacingOccurrences(of: "\n", with: ""))
                //                }
                if data.indices.contains(1){
                    
                    if let isValid = indexNumber{
                        if i  == values.count - isValid{
                            qrInfoDetails.append(wrapAmountWithCommaDecimal(key: data[1].replacingOccurrences(of: "\n", with: "")) + " " + "MMK")
                        }else if i == 2{
                            qrInfoDetails.append(data[1].replacingOccurrences(of: "\n", with: "").replacingOccurrences(of: "-", with: "/"))
                        }else{
                            qrInfoDetails.append(data[1].replacingOccurrences(of: "\n", with: ""))
                        }
                    }
                    
                }
                
            }
            print(qrInfoTitle)
            print(qrInfoDetails)
            if qrInfoTitle.count>0 && qrInfoDetails.count>0{
                qrInfoTableView.reloadData()
            }
        }
    }
}


extension NearByQRInfoController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isComingFromTaxPayment{
            return qrInfoTitle.count
        }else{
            return 4
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isComingFromTaxPayment{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: NearByConstant.kNearByScanQRDetailsCell) as? NearByScanQRDetailsCell else{
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            
            if qrInfoDetails.indices.contains(indexPath.row){
                let height =  heightForView(text:qrInfoDetails[indexPath.row] , font: UitilityClass.getZwagiFontWithSize(size: 14.0), width: screenWidth)
                if height <= 22.0{
                    cell.detailsHeight.constant = 22.0
                }else{
                    cell.detailsHeight.constant = height
                }
                
                cell.details.font = UitilityClass.getZwagiFontWithSize(size: 14.0)
                cell.title.font = UitilityClass.getZwagiFontWithSize(size: 14.0)
                
                
                cell.details.text = qrInfoDetails[indexPath.row]
                
                
                if qrInfoTitle.indices.contains(indexPath.row){
                    cell.title.text = qrInfoTitle[indexPath.row]
                }
                
            }
            
            
            return cell
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: NearByConstant.kNearByQRInfoCell) as? NearByQRInfoCell else{
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            cell.merchantInfoImageView.image = UIImage(named: NearByConstant.nearbyMerchantImages[indexPath.row])
            cell.imageViewWidth.constant = 30.0
            cell.imageViewLeading.constant = 15.0
            let height =  heightForView(text:NearByConstant.dummyData[indexPath.row] , font: UitilityClass.getZwagiFontWithSize(size: 14.0), width: screenWidth)
            if height <= 22.0{
                cell.merchantInfoDetailLabelHeight.constant = 22.0
            }else{
                cell.merchantInfoDetailLabelHeight.constant = height
            }
            cell.merchantInfoTitleLabel.textColor = kDarkBlueColor
            cell.merchantInfoTitleLabel.text = NearByConstant.nearByMerchantArray[indexPath.row]
            cell.merchantInfoDetailLabel.text = NearByConstant.dummyData[indexPath.row]
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if isComingFromTaxPayment{
            
            if qrInfoDetails.indices.contains(indexPath.row){
                let height =  heightForView(text: qrInfoDetails[indexPath.row], font: UitilityClass.getZwagiFontWithSize(size: 16.0), width: screenWidth)
                if height <= 22.0{
                    return 80.0
                }else{
                    let newHeight = height - 22.0
                    return 80.0 + newHeight
                }
            }
            return 0
            
        }else{
            if indexPath.row == 3{
                let height =  heightForView(text: NearByConstant.dummyData[indexPath.row], font: UitilityClass.getZwagiFontWithSize(size: 16.0), width: screenWidth)
                if height <= 22.0{
                    return 60.0
                }else{
                    let newHeight = height - 22.0
                    return 60.0 + newHeight
                }
            }else{
                return  60
            }
            
        }
        
    }
}
