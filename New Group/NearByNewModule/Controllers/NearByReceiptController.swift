//
//  NearByReceiptController.swift
//  OK
//
//  Created by Tushar Lama on 16/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class NearByReceiptController: OKBaseController {

    @IBOutlet var doneButton: UIButton!
    @IBOutlet var paymentSuccessFulLabel: UILabel!
    @IBOutlet var receiptTableVidw: UITableView!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var easyFastSecureLabel: UILabel!
    @IBOutlet var receiptTableHeight: NSLayoutConstraint!
     var isComingFromRecent = false
    override func viewDidLoad() {
        super.viewDidLoad()
          setUI()
    }
    
    @IBAction func onClickDoneButton(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }

}


extension NearByReceiptController{
    fileprivate func setUI(){
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        paymentSuccessFulLabel.font = UitilityClass.getZwagiFontWithSize(size: 20.0)
        doneButton.setTitle("Done".localized, for: .normal)
        paymentSuccessFulLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
        paymentSuccessFulLabel.text = "Payment Successful".localized
        easyFastSecureLabel.text = "Easy - Fast - Secure".localized
        self.receiptTableVidw.register(UINib(nibName: NearByConstant.kNearByPaymentCell, bundle: nil), forCellReuseIdentifier: NearByConstant.kNearByPaymentCell)
        self.receiptTableVidw.register(UINib(nibName: NearByConstant.kNearByQRInfoCell, bundle: nil), forCellReuseIdentifier: NearByConstant.kNearByQRInfoCell)
        receiptTableHeight.constant = getTableHeight()
        self.receiptTableVidw.separatorStyle = .none
        self.receiptTableVidw.delegate = self
        self.receiptTableVidw.dataSource  = self
        self.receiptTableVidw.reloadData()
    }
    
    fileprivate func getTableHeight() -> CGFloat{
        let height =  heightForView(text:NearByConstant.dummyData[3] , font: UitilityClass.getZwagiFontWithSize(size: 14.0), width: screenWidth)
        if height <= 22.0{
            return  22.0 + 160.0
        }else{
            return height + 160.0
        }
    }
    
}


extension NearByReceiptController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0,1,2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: NearByConstant.kNearByPaymentCell) as? NearByPaymentCell else{
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            cell.titleLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
            cell.titleLabel.text = NearByConstant.nearByMerchantArray[indexPath.row]
            cell.merchantValueLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
            cell.merchantValueLabel.text = NearByConstant.dummyData[indexPath.row]
            return cell
            
        case 3:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: NearByConstant.kNearByQRInfoCell) as? NearByQRInfoCell else{
                return UITableViewCell()
            }
            let height =  heightForView(text:NearByConstant.dummyData[indexPath.row] , font: UitilityClass.getZwagiFontWithSize(size: 13.0), width: screenWidth)
            if height <= 22.0{
                cell.merchantInfoDetailLabelHeight.constant = 22.0
            }else{
                cell.merchantInfoDetailLabelHeight.constant = height
            }
            cell.selectionStyle = .none
            cell.merchantInfoDetailLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
            cell.seperatorView.isHidden = true
            cell.merchantInfoTitleLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
            cell.merchantInfoTitleLabel.textColor = .black
            cell.imageViewWidth.constant = 0.0
            cell.imageViewLeading.constant = 0.0
            cell.merchantInfoTitleLabel.text = NearByConstant.nearByMerchantArray[indexPath.row]
            cell.merchantInfoDetailLabel.text = NearByConstant.dummyData[indexPath.row]
            return cell
            
        default:
            break
        }
        
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 3{
            let height =  heightForView(text: NearByConstant.dummyData[indexPath.row], font: UitilityClass.getZwagiFontWithSize(size: 16.0), width: screenWidth)
            if height <= 22.0{
                return 60.0
            }else{
                let newHeight = height - 22.0
                return 60.0 + newHeight
            }
        }else{
            return  40.0
        }
    }
}



