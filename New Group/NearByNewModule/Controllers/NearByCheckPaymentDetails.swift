//
//  NearByCheckPaymentDetails.swift
//  OK
//
//  Created by Tushar Lama on 17/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class NearByCheckPaymentDetails: OKBaseController {

    @IBOutlet var checkPaymentLabel: UILabel!
    @IBOutlet var payButton: UIButton!
    @IBOutlet var paymentDetailsTableView: UITableView!
    
    var isComingfromTaxModule = false
    var qrTitle = [String]()
    var qrInfo = [String]()
    var taxViewModel = TaxViewModel()
    var isButtonTapped = false
     var isComingFromRecent = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
         setUI()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickPayButton(_ sender: Any) {
        if !isButtonTapped {
            //checking the TAx amount with main balance
            
            let balanceAmount = (UserLogin.shared.walletBal as NSString).floatValue
            let billAmountIntValue = (self.qrInfo[self.qrInfo.count - 2] as NSString).floatValue
            
            if balanceAmount>=billAmountIntValue{
                //then only deduct the money
                if appDelegate.checkNetworkAvail() {
                    progressViewObj.showProgressView()
                    self.isButtonTapped = true
                    taxViewModel.sendMerchantInformationToTheServer(merchantInfo: qrInfo, finished:{
                        progressViewObj.removeProgressView()
                        //checking if data is available
                        if let isValidObj = self.taxViewModel.paymentConfirmationModel{
                            if isValidObj.isTransactionSuccess{
                                //success
                                self.isButtonTapped = false
                                DispatchQueue.main.async {
                                    self.navigate(qrString: "")
                                }
                            }else{
                                self.isButtonTapped = false
                                //failed
                                if let code = isValidObj.code{
                                    if code == 303{
                                        self.showErrorAlert(errMessage: "TaxCode already processed".localized)
                                    }else{
                                        self.showErrorAlert(errMessage: isValidObj.msg ?? "")
                                    }
                                }else{
                                    self.showErrorAlert(errMessage: isValidObj.msg ?? "")
                                }
                                
                            }
                        }else{
                            self.isButtonTapped = false
                            self.showErrorAlert(errMessage: "Some error happen please try again".localized)
                        }
                    })
                } else {
                    self.isButtonTapped = false
                    self.noInternetAlert()
                    progressViewObj.removeProgressView()
                }
           }else{
                //show alert
                self.showErrorAlert(errMessage: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized)
            }
        }
    }
    
    fileprivate func navigate(qrString: String){
        if #available(iOS 13.0, *) {
        
            if isComingfromTaxModule{
                let obj = self.storyboard?.instantiateViewController(identifier: NearByConstant.kNearByTaxReceiptController) as? NearByTaxReceiptController
                if let isValid = obj{
                    if let isValidNewObj = self.taxViewModel.paymentConfirmationModel{
                        isValid.taxModel = isValidNewObj
                    }
                    
                    isValid.qrTitle = self.qrTitle
                    isValid.qrInfo = self.qrInfo
                    isValid.qrInfoString = qrString
                    isValid.isComingFromRecent =  self.isComingFromRecent
                    self.navigationController?.pushViewController(isValid, animated: true)
                }
            }else{
                let obj = self.storyboard?.instantiateViewController(identifier: NearByConstant.kNearByReceiptController) as? NearByReceiptController
                if let isValid = obj{
                    self.navigationController?.pushViewController(isValid, animated: true)
                }
            }
        } else {
            // Fallback on earlier versions
            if isComingfromTaxModule{
                if let obj = self.storyboard?.instantiateViewController(withIdentifier: NearByConstant.kNearByTaxReceiptController) as? NearByTaxReceiptController{
                    if let isValidNewObj = self.taxViewModel.paymentConfirmationModel{
                        obj.taxModel = isValidNewObj
                    }
                    
                    obj.qrTitle = self.qrTitle
                    obj.qrInfo = self.qrInfo
                    obj.qrInfoString = qrString
                    self.navigationController?.pushViewController(obj, animated: true)
                }
            }else{
                if let ViewController = self.storyboard?.instantiateViewController(withIdentifier: NearByConstant.kNearByReceiptController) as? NearByReceiptController{
                    self.navigationController?.pushViewController(ViewController, animated: true)
                }
            }
        }
    }

}

extension NearByCheckPaymentDetails{
    fileprivate func setUI(){
        checkPaymentLabel.font = UitilityClass.getZwagiFontWithSize(size: 16.0)
      checkPaymentLabel.text = "Check Payment Details".localized
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        if isComingfromTaxModule{
            let info = qrInfo
            let title = qrTitle
            qrInfo.removeAll()
            qrTitle.removeAll()
            
            
            if appDel.currentLanguage == "uni"{
                qrTitle.append("OK$ Name".localized)
                qrTitle.append("OK$ Number".localized)
            }else{
                qrTitle.append("OK$ အေကာင့္အမည္".localized)
                qrTitle.append("OK$ အေကာင့္ ဖုန္းနံပါတ္".localized)
            }
            
            qrInfo.append(UserModel.shared.name)
            qrInfo.append(UserModel.shared.mobileNo.replacingOccurrences(of: "0095", with: "(+95) 0"))
            for i in 0..<info.count{
                qrInfo.append(info[i])
            }
           
            for j in 0..<title.count{
                qrTitle.append(title[j])
            }
            self.navigationItem.title = "Tax Payment".localized
        }else{
            self.title = "Near By Payment".localized
        }
        payButton.titleLabel?.font = UitilityClass.getZwagiFontWithSize(size: appButtonSize)
        payButton.setTitle("Pay".localized, for: .normal)
        self.paymentDetailsTableView.backgroundColor = .white
        self.paymentDetailsTableView.register(UINib(nibName: NearByConstant.kNearByPaymentCell, bundle: nil), forCellReuseIdentifier: NearByConstant.kNearByPaymentCell)
        self.paymentDetailsTableView.register(UINib(nibName: NearByConstant.kNearByQRInfoCell, bundle: nil), forCellReuseIdentifier: NearByConstant.kNearByQRInfoCell)
        self.paymentDetailsTableView.separatorColor = .clear
        self.paymentDetailsTableView.delegate = self
        self.paymentDetailsTableView.dataSource  = self
        self.paymentDetailsTableView.reloadData()
    }
}


extension NearByCheckPaymentDetails: UITableViewDelegate,UITableViewDataSource{
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isComingfromTaxModule{
            return qrTitle.count
        }else{
            return 4
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isComingfromTaxModule{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: NearByConstant.kNearByPaymentCell) as? NearByPaymentCell else{
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            
            
            
            if qrInfo.indices.contains(indexPath.row){
                let height =  heightForView(text:qrInfo[indexPath.row] , font: UitilityClass.getZwagiFontWithSize(size: 13.0), width: screenWidth)
                if  qrTitle.indices.contains(indexPath.row){
                    let titleHeight = heightForView(text:qrTitle[indexPath.row] , font: UitilityClass.getZwagiFontWithSize(size: 13.0), width: screenWidth)
                    if height>titleHeight{
                        if height<=19.0{
                            cell.merchantLabelHeight.constant = 20.0
                        }else{
                            cell.merchantLabelHeight.constant = height + 10
                        }
                    }else{
                        if titleHeight<=20.0{
                            cell.merchantLabelHeight.constant = 20.0
                        }else{
                            cell.merchantLabelHeight.constant = titleHeight
                        }
                    }
                    cell.titleLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
                    cell.titleLabel.text = qrTitle[indexPath.row]
                }
                cell.merchantValueLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
                cell.merchantValueLabel.text = qrInfo[indexPath.row]
            }
            return cell

        }else{
            
          switch indexPath.row {
            case 0,1,2:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: NearByConstant.kNearByPaymentCell) as? NearByPaymentCell else{
                    return UITableViewCell()
                }
                cell.selectionStyle = .none
                cell.titleLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
                cell.merchantValueLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
                cell.titleLabel.text = NearByConstant.nearByMerchantArray[indexPath.row]
                cell.merchantValueLabel.text = NearByConstant.dummyData[indexPath.row]
                return cell
                
            case 3:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: NearByConstant.kNearByQRInfoCell) as? NearByQRInfoCell else{
                    return UITableViewCell()
                }
                cell.selectionStyle = .none
                let height =  heightForView(text:NearByConstant.dummyData[indexPath.row] , font: UitilityClass.getZwagiFontWithSize(size: 15.0), width: screenWidth)
                if height <= 22.0{
                    cell.merchantInfoDetailLabelHeight.constant = 22.0
                }else{
                    cell.merchantInfoDetailLabelHeight.constant = height
                }
                cell.merchantInfoDetailLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
                cell.merchantInfoTitleLabel.textColor = .black
                cell.imageViewWidth.constant = 0.0
                cell.imageViewLeading.constant = 0.0
                cell.merchantInfoTitleLabel.text = NearByConstant.nearByMerchantArray[indexPath.row]
                cell.merchantInfoDetailLabel.text = NearByConstant.dummyData[indexPath.row]
                return cell
            
            default:
                break
            }
            
        }
           
            
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isComingfromTaxModule{
            
            if qrInfo.indices.contains(indexPath.row){
                let height =  heightForView(text:qrInfo[indexPath.row] , font: UitilityClass.getZwagiFontWithSize(size: 16.0), width: screenWidth)
                if qrTitle.indices.contains(indexPath.row){
                    let infoHeight = heightForView(text:qrTitle[indexPath.row] , font:UitilityClass.getZwagiFontWithSize(size: 16.0), width: screenWidth)
                    
                    if infoHeight>height{
                        if infoHeight<=19.0{
                            return 40.0
                        }else{
                            let newHeight = infoHeight - 10.0
                            return 40.0 + newHeight
                        }
                    }else{
                        if height<=19.0{
                            return 40.0
                        }else{
                            let newHeight = height - 10.0
                            return 40.0 + newHeight
                        }
                    }
                }
            }else{
                return 0.0
            }
        }else{
            if indexPath.row == 3{
                let height =  heightForView(text: NearByConstant.dummyData[indexPath.row], font: UitilityClass.getZwagiFontWithSize(size: 16.0), width: screenWidth)
                if height <= 22.0{
                    return 60.0
                }else{
                    let newHeight = height - 22.0
                    return 60.0 + newHeight
                }
            }else{
                return  40.0
            }
        }
        
        return 0.0
    }
}
