//
//  NearByTaxReceiptController.swift
//  OK
//
//  Created by Tushar Lama on 01/06/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit
import PDFKit

class NearByTaxReceiptController: OKBaseController {

    @IBOutlet var inVoiceButton: UIButton!
    @IBOutlet var repeatPaymentButton: UIButton!
    @IBOutlet var cardView: CardDesignView!
    @IBOutlet var shareView: UIView!
    @IBOutlet var invoiceView: UIView!
    @IBOutlet var repeatPaymentView: UIView!
    @IBOutlet var shareMainView: UIView!
    @IBOutlet var shareButton: UIButton!
    @IBOutlet var homeButton: UIButton!
    @IBOutlet var taxReceiptTableView: UITableView!
    @IBOutlet var taxPaymentLabel: UILabel!
    @IBOutlet var govtTextLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    var qrTitle = [String]()
    var qrInfo = [String]()
    var qrInfoString: String?
    var qrImage: UIImage?
    var taxModel: TaxFinalModel?
    var pdfName:  URL?
    var pdfDocument: PDFDocument?
    @IBOutlet var PDFViewContainer:PDFView?
     var newTusharPDf = UIView()
     var isComingFromRecent = false
   
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    //these all are last minute changes
    func getFormattedDate(date: Date, format: String) -> String {
        let dateformat = DateFormatter()
        dateformat.dateFormat = format
        return dateformat.string(from: date)
    }
    
     func makePDF(){
            newTusharPDf.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 1200)
            let logoImage = UIImageView()
            logoImage.frame = CGRect(x: 20, y: 10, width: 50, height: 50)
            logoImage.image = UIImage(named: "tax_Logo")
            newTusharPDf.addSubview(logoImage)
            let taxLabel = UILabel()
            taxLabel.frame = CGRect(x: 90.0, y: 0, width: 300.0, height: 70.0)
            taxLabel.font =  UitilityClass.getZwagiFontWithSize(size: 13.0)
            taxLabel.numberOfLines = 5
            taxLabel.text = "The Republic of the Union of Myanmar Ministry of Planning,Finance and Industry. Internal Revenue Department Tax Reference".localized
            newTusharPDf.addSubview(taxLabel)
            newTusharPDf.bringSubviewToFront(taxLabel)
            
            let oKDollarLogo = UIImageView(frame: CGRect(x: newTusharPDf.frame.size.width/2.5, y:  taxLabel.frame.size.height + taxLabel.frame.origin.y + 10, width: 50.0, height: 50.0))
            oKDollarLogo.image = UIImage(named: "appIcon_Ok")
                
            let okDollarReceiptName = UILabel(frame: CGRect(x: newTusharPDf.frame.size.width/3, y:  oKDollarLogo.frame.size.height + oKDollarLogo.frame.origin.y + 10, width: 150.0, height: 30.0))
            okDollarReceiptName.font =  UIFont.init(name: appFont, size: 13)
            okDollarReceiptName.text = "Transaction Receipt".localized
            
            newTusharPDf.addSubview(oKDollarLogo)
            newTusharPDf.bringSubviewToFront(oKDollarLogo)
            newTusharPDf.addSubview(okDollarReceiptName)
            newTusharPDf.bringSubviewToFront(okDollarReceiptName)
            
            for i in 0..<qrTitle.count{
                let titleLabel = UILabel(frame: CGRect(x: 20, y: CGFloat(i) * 40 + 230, width: 160.0, height: 50))
                let valueLabel = UILabel(frame: CGRect(x: 170.0, y: CGFloat(i) * 40 + 230, width: 200.0, height: 50))
                //let borderView = UIView(frame: CGRect(x: 0.0, y: CGFloat(i) * 40 + 120, width: self.view.frame.size.width, height: 2))
               // borderView.backgroundColor = .white
                titleLabel.font =  UitilityClass.getZwagiFontWithSize(size: 12.0)
                valueLabel.font =  UitilityClass.getZwagiFontWithSize(size: 12.0)
               // titleLabel.frame = CGRect(x: 20, y: Double(80 + i * 2), width: 160.0, height: 40.0)
                titleLabel.numberOfLines = 3
                valueLabel.numberOfLines = 3
                
                if qrTitle.indices.contains(i){
                    print(qrTitle[i])
                    
                    if qrTitle[i] == "TransactionId".localized || qrTitle[i] == "Transaction Id" || qrTitle[i] == "Transaction ID"{
                        titleLabel.text = "Transaction Code".localized
                    }else if qrTitle[i] == "Income Year"{
                         titleLabel.text = "Fiscal Year".localized
                    }else if qrTitle[i] == "Id Number".localized{
                       titleLabel.text = "NRC".localized
                    }else if qrTitle[i] == "Paid Amount".localized{
                       titleLabel.text = "Bill Amount".localized
                    }else{
                        titleLabel.text = qrTitle[i]
                    }
                    
                    
                }
                
                
                if qrInfo.indices.contains(i){
                    if qrTitle[i] == "Paid Amount".localized || qrTitle[i] == "Bill Amount".localized{
                        valueLabel.text =  " : " + wrapAmountWithCommaDecimal(key: qrInfo[i])
                    }else{
                        valueLabel.text =  " : " + qrInfo[i]
                    }
                    
                    
                }
                
                //for  last index
                if i == qrTitle.count - 1{
                    let dateImage = UIImageView(frame: CGRect(x: 20, y:  valueLabel.frame.size.height + valueLabel.frame.origin.y + 20, width: 15.0, height: 15.0))
                    let timeImage = UIImageView(frame: CGRect(x: 265.0, y:  valueLabel.frame.size.height + valueLabel.frame.origin.y + 20, width: 15.0, height: 15.0))
                    let dateTime = UILabel(frame: CGRect(x: 55.0, y:  valueLabel.frame.size.height + valueLabel.frame.origin.y + 18, width: 160.0, height: 20))
                    let timeLabel = UILabel(frame: CGRect(x: 290.0, y:  valueLabel.frame.size.height + valueLabel.frame.origin.y + 17.5 , width: 150.0, height: 20))
                    
                    timeLabel.textAlignment = .center
                    dateImage.image = UIImage(named: "merchantCalendar")
                    timeImage.image = UIImage(named: "merchantClock")
                    
                //  dateTime.backgroundColor = .yellow
                    dateTime.font =  UIFont.init(name: appFont, size: 13)
                    timeLabel.font = UIFont.init(name: appFont, size: 13)
                    
                    if let isValidObj = taxModel{
                        if let isValidDate = isValidObj.responseDataTime{
                             dateTime.text = formattedDateAndTime(dateString: isValidDate).date
                             timeLabel.text = formattedDateAndTime(dateString: isValidDate).time
                        }
                    }

                    let imageViewQR = UIImageView(frame: CGRect(x: self.view.frame.size.width/3, y:  dateTime.frame.size.height + dateTime.frame.origin.y + 40.0, width: 100.0, height: 100.0))
                    if let value = qrImage{
                        imageViewQR.image = value
                    }
                    
                    self.newTusharPDf.addSubview(dateImage)
                    self.newTusharPDf.addSubview(timeImage)
                    self.newTusharPDf.addSubview(timeLabel)
                    self.newTusharPDf.addSubview(imageViewQR)
                    self.newTusharPDf.addSubview(dateTime)
                }
                self.newTusharPDf.addSubview(titleLabel)
                self.newTusharPDf.addSubview(valueLabel)
               // self.newTusharPDf.addSubview(borderView)
            }
            
            
            let formatingDate = self.getFormattedDate(date: Date(), format: "yyyy-MM-dd'T'HH:mm:ssZZZZZ")
             print(formatingDate)

             let strPdfName = "OK$-TAX_PAYMENT_RECEIPT" + "\(Date().toMillis() ?? 232333333323223)" + "-" + formatingDate


            // let pdfUrl = self.pdfDataWithTableView(tableView: self.newTusharPDf, saveToDocumentsWithFileName: strPdfName)

             let pdfUrl = self.createPdfFromView(aView: self.newTusharPDf, saveToDocumentsWithFileName: strPdfName)
             

             if let document = PDFDocument(url: pdfUrl!) {
                 document.delegate = self
                 pdfDocument = document
             }

             guard let data = pdfDocument!.dataRepresentation() else { return }

             let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

             let docURL = documentDirectory.appendingPathComponent(strPdfName + ".pdf")

             do{

                 try data.write(to: docURL)

             }catch(let error){
                 print("error is \(error.localizedDescription)")
             }

             pdfName =  documentDirectory.appendingPathComponent(strPdfName + ".pdf")
            
        }
    
    func createPdfFromView(aView: UIView, saveToDocumentsWithFileName pdfFileName: String) -> URL? {
        let render = UIPrintPageRenderer()
        let page = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 1200) // A4, 72 dpi
        let printable = page.insetBy(dx: 0, dy: 0)
        render.setValue(NSValue(cgRect: page), forKey: "paperRect")
        render.setValue(NSValue(cgRect: printable), forKey: "printableRect")
        let priorBounds = aView.bounds
        let fittedSize = aView.sizeThatFits(CGSize(width:priorBounds.size.width, height:1200))
        aView.bounds = CGRect(x:0, y:0, width:fittedSize.width, height:1200)
        let pdfPageBounds = CGRect(x:0, y:0, width:aView.frame.width, height:1200)
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds,nil)
        let pageOriginY: CGFloat = 0
        UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
        UIGraphicsGetCurrentContext()!.saveGState()
        UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: -pageOriginY)
        aView.layer.render(in: UIGraphicsGetCurrentContext()!)
        UIGraphicsGetCurrentContext()!.restoreGState()
        UIGraphicsEndPDFContext()
        aView.bounds = priorBounds
        guard let documentDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else  { return nil}
        let properPDFFileName = pdfFileName.replacingOccurrences(of: ".pdf", with: "") + ".pdf"
        let pdfUrl = documentDirectories + "/" + properPDFFileName
        //Delete file if alreay exists
        do {
            try FileManager.default.removeItem(atPath: pdfUrl)
        } catch let error as NSError {
            println_debug("Error: \(error.domain)")
        }
        //Write file
        if pdfData.write(toFile: pdfUrl, atomically: true) {
            println_debug("Successfully wrote to \(pdfUrl)")
            let pathUrl = URL(fileURLWithPath: documentDirectories)
            let fileUrl = pathUrl.appendingPathComponent(properPDFFileName)
            return fileUrl
        }
        return nil
    }
    
    func openPDFToShare(){
        self.PDFViewContainer?.autoScales = true
        self.PDFViewContainer?.backgroundColor = UIColor.lightGray
        
        if let url = self.pdfName{
            let message = MessageWithSubject(subject: "OK$ Tax Receipt", message: "This is OK$ Tax Receipt.")
            var activityItems = [Any]()
            activityItems.append(message)
            activityItems.append(url)
           
            let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
            activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.copyToPasteboard,UIActivity.ActivityType.assignToContact, UIActivity.ActivityType.addToReadingList,UIActivity.ActivityType.postToFlickr, UIActivity.ActivityType.postToVimeo]
            activityViewController.popoverPresentationController?.sourceView = self.view
            DispatchQueue.main.async {
                self.present(activityViewController, animated: true, completion: {
                })
            }
            
            //this code will work when i will press the cross button of activity view controller
//            activityViewController.completionWithItemsHandler = { (activityType, completed:Bool, returnedItems:[Any]?, error: Error?) in
//                if !completed {
//                    DispatchQueue.main.async {
//                        self.navigationController?.popViewController(animated: false)
//                    }
//                }
            }
        }
        
  //  }
    
    @IBAction func onClickHomeButton(_ sender: Any) {
        if isComingFromRecent{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MoveToDashboardFromTax"), object: nil)
        }else{
            self.navigationController?.popToRootViewController(animated: true)
        }
       
    }
    
    @IBAction func onClickShareButton(_ sender: Any) {
        showShareView(hide: false)
        
        
        
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        if self.isMovingFromParent {
//            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
//             for aViewController in viewControllers {
//                 if aViewController is NearByController {
//                     self.navigationController!.popToViewController(aViewController, animated: false)
//                 }
//             }
//        }
//    }
//
    
    @IBAction func onClickRepeatPayment(_ sender: Any) {
        showShareView(hide: true)
       let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is NearByController {
                self.navigationController!.popToViewController(aViewController, animated: false)
            }
        }
    }
    
    @IBAction func onClickInvoice(_ sender: Any) {
        showShareView(hide: true)
        
        if #available(iOS 13.0, *) {
                       let obj = self.storyboard?.instantiateViewController(identifier: NearByConstant.kTaxReceiptFinalController) as? TaxReceiptFinalController
                       if let isValid = obj{
                           isValid.qrTitle = self.qrTitle
                           isValid.qrInfo = self.qrInfo
                        isValid.iscomingFromShare  = false
                        if let isValidObj = taxModel{
                            if let isValidDate = isValidObj.responseDataTime{
                               // isValid.dateTimeStr = formattedDateAndTime(dateString: isValidDate).date + " " + formattedDateAndTime(dateString: isValidDate).time
                                isValid.transactionDate = formattedDateAndTime(dateString: isValidDate).date
                                isValid.transactionTime = formattedDateAndTime(dateString: isValidDate).time
                            }
                        }
                           isValid.qrImage = UitilityClass.getQRImage(stringQR:qrInfoString ?? "",withSize: 5,logoName: "tax_Logo")
                         //  isValid.qrCodeString = qrInfoString
                           self.navigationController?.pushViewController(isValid, animated: true)
                       }
               } else {
                   if let obj = self.storyboard?.instantiateViewController(withIdentifier: NearByConstant.kTaxReceiptFinalController) as? TaxReceiptFinalController{
                       obj.qrTitle = self.qrTitle
                       obj.qrInfo = self.qrInfo
                      obj.iscomingFromShare  = false
                    
                    if let isValidObj = taxModel{
                        if let isValidDate = isValidObj.responseDataTime{
//                            obj.dateTimeStr = formattedDateAndTime(dateString: isValidDate).date + " " + formattedDateAndTime(dateString: isValidDate).time
                            obj.transactionDate = formattedDateAndTime(dateString: isValidDate).date
                            obj.transactionTime = formattedDateAndTime(dateString: isValidDate).time
                        }
                    }
                   
                    //obj.dateTimeStr = taxModel?.responseDataTime ?? "02-Jun-2020 17:30:18"
                       obj.qrImage = UitilityClass.getQRImage(stringQR:qrInfoString ?? "",withSize: 5,logoName: "tax_Logo")
                      // obj.qrCodeString = qrInfoString
                       self.navigationController?.pushViewController(obj, animated: true)
                   }
               }
        
    }
    
    
    @IBAction func onClickShareButtonWithPDF(_ sender: Any) {
      showShareView(hide: true)
       // makePDF()
        openPDFToShare()
//        if #available(iOS 13.0, *) {
//                let obj = self.storyboard?.instantiateViewController(identifier: NearByConstant.kTaxReceiptFinalController) as? TaxReceiptFinalController
//                if let isValid = obj{
//                    isValid.qrTitle = self.qrTitle
//                    isValid.qrInfo = self.qrInfo
//                    isValid.iscomingFromShare  = true
//
//                    if let isValidObj = taxModel{
//                        if let isValidDate = isValidObj.responseDataTime{
//                            // isValid.dateTimeStr = formattedDateAndTime(dateString: isValidDate).date + " " + formattedDateAndTime(dateString: isValidDate).time
//                            isValid.transactionDate = formattedDateAndTime(dateString: isValidDate).date
//                            isValid.transactionTime = formattedDateAndTime(dateString: isValidDate).time
//                        }
//                    }
//
//
//                   // isValid.dateTimeStr = taxModel?.responseDataTime ?? "02-Jun-2020 17:30:18"
//                    isValid.qrImage = UitilityClass.getQRImage(stringQR:qrInfoString ?? "",withSize: 5,logoName: "tax_Logo")
//                  //  isValid.qrCodeString = qrInfoString
//                    self.navigationController?.pushViewController(isValid, animated: true)
//                }
//        } else {
//            if let obj = self.storyboard?.instantiateViewController(withIdentifier: NearByConstant.kTaxReceiptFinalController) as? TaxReceiptFinalController{
//                obj.qrTitle = self.qrTitle
//                obj.qrInfo = self.qrInfo
//                obj.iscomingFromShare  = true
//               // obj.dateTimeStr = taxModel?.responseDataTime ?? "02-Jun-2020 17:30:18"
//
//                if let isValidObj = taxModel{
//                    if let isValidDate = isValidObj.responseDataTime{
//                        obj.transactionDate = formattedDateAndTime(dateString: isValidDate).date
//                        obj.transactionTime = formattedDateAndTime(dateString: isValidDate).time
//                    }
//                }
//                obj.qrImage = UitilityClass.getQRImage(stringQR:qrInfoString ?? "",withSize: 5,logoName: "tax_Logo")
//               // obj.qrCodeString = qrInfoString
//                self.navigationController?.pushViewController(obj, animated: true)
//            }
//        }
    }
    
    fileprivate  func showShareView(hide: Bool){
        if hide{
            self.view.willRemoveSubview(cardView)
            self.view.willRemoveSubview(shareMainView)
        }else{
            //self.view.addSubview(cardView)
            //self.view.addSubview(shareMainView)
            
            let window = UIApplication.shared.keyWindow!
            window.addSubview(shareMainView)
            window.addSubview(cardView)
            cardView.center = window.center
            
            cardView.translatesAutoresizingMaskIntoConstraints = false
            cardView.centerXAnchor.constraint(equalTo: window.centerXAnchor).isActive = true
            cardView.centerYAnchor.constraint(equalTo: window.centerYAnchor).isActive = true
            
            
            self.view.bringSubviewToFront(shareMainView)
            self.view.bringSubviewToFront(cardView)
        }
        self.cardView.isHidden = hide
        self.shareMainView.isHidden = hide
        
    }
    
    fileprivate  func formattedDateAndTime(dateString: String)-> (date: String, time: String)  {
      
    let formatter = DateFormatter()
    formatter.dateFormat = "dd-MMM-yyyy HH:mm:ss"
    formatter.calendar = Calendar(identifier: .gregorian)

    let yourDate = formatter.date(from: dateString)
    formatter.dateFormat = "EEE, dd-MMM-yyyy"
    
    if let isValidDate = yourDate{
        let date = formatter.string(from: isValidDate)
        let timeFormatterPrint = DateFormatter()
        timeFormatterPrint.dateFormat = "HH:mm:ss"
        let time = timeFormatterPrint.string(from: isValidDate)
        print(date)
        print(time)
        return (date,time)
    }else{
        let formatter = DateFormatter()
            formatter.dateFormat = "dd-MMM-yyyy hh:mm:ss"
            formatter.calendar = Calendar(identifier: .gregorian)

            let yourDate = formatter.date(from: dateString)
            formatter.dateFormat = "EEE, dd-MMM-yyyy"
            
            if let isValidDate = yourDate{
                let date = formatter.string(from: isValidDate)
                let timeFormatterPrint = DateFormatter()
                timeFormatterPrint.dateFormat = "HH:mm:ss"
                let time = timeFormatterPrint.string(from: isValidDate)
                print(date)
                print(time)
                return (date,time)
        
    }
    }
        return("","")
}
}

extension NearByTaxReceiptController{
    
    fileprivate func setUI(){
        navigationItem.hidesBackButton = true
        taxPaymentLabel.text = "Tax Payment Successful".localized
        if appDel.currentLanguage == "uni" || appDel.currentLanguage == "my"{
             taxPaymentLabel.font = UitilityClass.getZwagiFontWithSize(size: 16.0)
        }else{
             taxPaymentLabel.font = UitilityClass.getZwagiFontWithSize(size: 20.0)
        }
        govtTextLabel.font = UitilityClass.getZwagiFontWithSize(size: 15)
        govtTextLabel.text = "The Republic of the Union of Myanmar Ministry of Planning,Finance and Industry. Internal Revenue Department Tax Reference".localized
       
        qrTitle.insert("OK$ Transaction ID".localized, at: 2)
        qrInfo.insert(taxModel?.transactionId ?? "", at: 2)
        
        generateQRString()
        makePDF()
        print(qrTitle)
        print(qrInfo)
        
        
        shareMainView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.height)
        taxReceiptTableView.layer.borderColor = UIColor(red: 221.0/255.0, green: 221.0/255.0, blue: 221.0/255.0, alpha: 1.0).cgColor
        taxReceiptTableView.layer.borderWidth = 1.0
        self.title = "Receipt".localized
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
         showShareView(hide: true)
        let gestureOnMainView = UITapGestureRecognizer(target: self, action: #selector(handleTapOnShareMain(sender:)))
               shareMainView.addGestureRecognizer(gestureOnMainView)
     
        // dateLabel.text = taxModel?.responseDataTime?.components(separatedBy: " ")[0] //formattedDateAndTime().date
       // timeLabel.text = taxModel?.responseDataTime?.components(separatedBy: " ")[1] //formattedDateAndTime().time
        
        if let value = taxModel{
            if let isValid = value.responseDataTime{
                dateLabel.text = formattedDateAndTime(dateString: isValid).date
                timeLabel.text = formattedDateAndTime(dateString: isValid).time
            }
        }
        
        shareButton.titleLabel?.font = UitilityClass.getZwagiFontWithSize(size: 14.0)
        shareButton.setTitle("Share".localized, for: .normal)
        inVoiceButton.titleLabel?.font = UitilityClass.getZwagiFontWithSize(size: 14.0)
        inVoiceButton.setTitle("Invoice".localized, for: .normal)
        
        repeatPaymentButton.titleLabel?.font = UitilityClass.getZwagiFontWithSize(size: 14.0)
        repeatPaymentButton.setTitle("More Payment".localized, for: .normal)
        
        self.taxReceiptTableView.backgroundColor = .white
        self.taxReceiptTableView.register(UINib(nibName: NearByConstant.kNearByPaymentCell, bundle: nil), forCellReuseIdentifier: NearByConstant.kNearByPaymentCell)
       // self.taxReceiptTableView.separatorColor = .clear
        self.taxReceiptTableView.delegate = self
        self.taxReceiptTableView.dataSource  = self
        self.taxReceiptTableView.reloadData()
        
    }
    
    fileprivate func generateQRString(){
        if let value = taxModel{
            if let isValid = value.responseDataTime{
                qrInfoString = UitilityClass.makeStringForQR(firstName: "OK$ Number".localized,secondName: UserModel.shared.mobileNo) + UitilityClass.makeStringForQR(firstName: "OK$ Name".localized,secondName: UserModel.shared.name) + UitilityClass.makeStringForQR(firstName: "OK$ Transaction ID".localized,secondName: taxModel?.transactionId ?? "") + UitilityClass.makeStringForQR(firstName: "Bill Amount".localized,secondName: qrInfo[qrInfo.count - 2]) +   UitilityClass.makeStringForQR(firstName: "Transaction Time".localized,secondName:formattedDateAndTime(dateString: isValid).date + " " + formattedDateAndTime(dateString: isValid).time) + UitilityClass.makeStringForQR(firstName: "Taxpayer Identification Number".localized,secondName: qrInfo[4])
               qrImage =  UitilityClass.getQRImage(stringQR:qrInfoString ?? "",withSize: 5,logoName: "tax_Logo")
            }
        }
    }
    
    @objc func handleTapOnShareMain(sender: UITapGestureRecognizer) {
       showShareView(hide: true)
    }
    
}


extension NearByTaxReceiptController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return qrTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NearByConstant.kNearByPaymentCell) as? NearByPaymentCell else{
            return UITableViewCell()
        }
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor(red: 221.0/255.0, green: 221.0/255.0, blue: 221.0/255.0, alpha: 1.0)
        
        if qrInfo.indices.contains(indexPath.row){
            let height =  heightForView(text:qrInfo[indexPath.row] , font: UitilityClass.getZwagiFontWithSize(size: 13.0), width: screenWidth)
            
            if qrTitle.indices.contains(indexPath.row){
                let titleHeight = heightForView(text:qrTitle[indexPath.row] , font: UitilityClass.getZwagiFontWithSize(size: 13.0), width: screenWidth)
                
                if height>titleHeight{
                    if height<=19.0{
                        cell.merchantLabelHeight.constant = 20.0
                    }else{
                        cell.merchantLabelHeight.constant = height + 10
                    }
                }else{
                    if titleHeight<=20.0{
                        cell.merchantLabelHeight.constant = 20.0
                    }else{
                        cell.merchantLabelHeight.constant = titleHeight
                    }
                }
                cell.titleLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
                cell.titleLabel.text = qrTitle[indexPath.row]
            }
            cell.merchantValueLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
            cell.merchantValueLabel.text = qrInfo[indexPath.row]
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if qrInfo.indices.contains(indexPath.row){
          let height =  heightForView(text:qrInfo[indexPath.row] , font: UitilityClass.getZwagiFontWithSize(size: 16.0), width: screenWidth)
            
            if qrTitle.indices.contains(indexPath.row){
                let infoHeight = heightForView(text:qrTitle[indexPath.row] , font: UitilityClass.getZwagiFontWithSize(size: 16.0), width: screenWidth)
                
                if infoHeight>height{
                    if infoHeight<=19.0{
                        return 40.0
                    }else{
                        let newHeight = infoHeight - 10.0
                        return 40.0 + newHeight
                    }
                }else{
                    if height<=19.0{
                        return 40.0
                    }else{
                        let newHeight = height - 10.0
                        return 40.0 + newHeight
                    }
                }
                
            }
            
        }else{
            return 0
        }
        return 0.0
    }
}


@available(iOS 11.0, *)
extension NearByTaxReceiptController : PDFDocumentDelegate {
    
}

