//
//  TaxInvoiceViewController.swift
//  OK
//
//  Created by Tushar Lama on 04/06/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class TaxInvoiceViewController: OKBaseController {
    @IBOutlet var inVoiceTableView: UITableView!
    
    @IBOutlet var titleLabel: UILabel!
    var taxViewModel = TaxViewModel()
    var trasactionMainHistory = [TaxInvoiceDetail]()
    var trasactionFilterHistory = [TaxInvoiceDetail]()
    var transactionHistory = [TaxInvoiceDetail]()
    var qrTitle = [String]()
    var qrInfo = [String]()
    var isComingFromRecent = false
    private lazy var searchBar = UISearchBar()
    private let refreshControl = UIRefreshControl()
    var filterSelected = false
    private var isSearchNeed = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigation()
        searchBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40)
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        searchBar.tintColor = UIColor.white
        searchBar.placeholder = "Search".localized
        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
                searchTextField.keyboardType = .asciiCapable
            }
        }
        searchBar.tintColor = UIColor.blue
        
        if let cancelButton : UIButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            cancelButton.setTitle("Cancel".localized, for:.normal)
            switch appDelegate.currentLanguage {
            case "en":
                if let navFont = UIFont(name: appFont, size: appButtonSize) {
                    cancelButton.titleLabel?.font = navFont
                }
            default:
                if let navFont = UIFont(name: appFont, size: appButtonSize) {
                    cancelButton.titleLabel?.font = navFont
                }
            }
        }
        
        let view = self.searchBar.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                //subView.tintColor = UIColor.gray
            }
        }
        
        titleLabel.font = UitilityClass.getZwagiFontWithSize(size: 16.0)
        titleLabel.text = "The Republic of the Union of Myanmar Ministry of Planning,Finance and Industry. Internal Revenue Department Tax Reference".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
      //  self.navigationItem.title = "Tax History".localized
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.getTransactionHistory()
        self.inVoiceTableView.register(UINib(nibName: NearByConstant.kTaxPaymentInvoiceCell, bundle: nil), forCellReuseIdentifier: NearByConstant.kTaxPaymentInvoiceCell)
    }
    
    func setUpNavigation() {
           self.navigationController?.navigationBar.barTintColor        = kYellowColor
           self.navigationController?.navigationBar.tintColor           = UIColor.white
           self.navigationItem.setHidesBackButton(true, animated:true)
           let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
           let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
           let ypos = (titleViewSize.height - 30 ) / 2
           
         //  if isSearchNeed {
               let searchButton = UIButton(type: .custom)
               searchButton.setImage(#imageLiteral(resourceName: "reportSearch").withRenderingMode(.alwaysOriginal), for: .normal)
               searchButton.addTarget(self, action: #selector(showSearchOption), for: .touchUpInside)
               searchButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
               navTitleView.addSubview(searchButton)
           
           let backButton = UIButton(type: .custom)
           backButton.setImage(#imageLiteral(resourceName: "tabBarBack").withRenderingMode(.alwaysOriginal), for: .normal)
           backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
           backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
           
           let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
           label.type = .continuous
           label.speed = .duration(4)
           label.text =  "Tax History".localized
           label.textColor = UIColor.white
           if let navFont = UIFont(name: appFont, size: 21) {
               label.font = navFont
           }
           label.center = navTitleView.center
           label.textAlignment = .center
           
           navTitleView.addSubview(backButton)
           navTitleView.addSubview(label)
           self.navigationItem.titleView = navTitleView
       }
    
    @objc private func showSearchOption() {
           searchBar.placeholder = "Search".localized
           searchBar.text = ""
           self.navigationItem.titleView = searchBar
           searchBar.becomeFirstResponder()
       }
    
    
    private func applySearch(with searchText: String) {
        var tempRecords = transactionHistory
        if searchText.count > 0 {
            tempRecords = tempRecords.filter({($0.transactionCode.lowercased().contains(find: searchText.lowercased())) || (String($0.taxAmount).lowercased().contains(find: searchText.lowercased()))})
        }
        trasactionMainHistory = tempRecords.sorted(by: { $0.transactionTime ?? "" > $1.transactionTime ?? ""})
        self.inVoiceTableView.reloadData()
    }
    
    fileprivate func getTransactionHistory(){
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            taxViewModel.getTaxTransactionhistory(finished:{
                progressViewObj.removeProgressView()
                //checking if data is available
                if let isValidObj = self.taxViewModel.trasactionHistory{
                    if isValidObj.isTransactionSuccess{
                        if isValidObj.taxHistoryDetails.count>0{
                            self.trasactionMainHistory.removeAll()
                            self.trasactionMainHistory = isValidObj.taxHistoryDetails
                            let value =  self.trasactionMainHistory
                            self.trasactionMainHistory.removeAll()
                            self.trasactionMainHistory =  value.sorted(by: { $0.transactionTime ?? "" > $1.transactionTime ?? ""})
                            self.transactionHistory = self.trasactionMainHistory
                            DispatchQueue.main.async {
                                self.inVoiceTableView.separatorColor = .clear
                                self.inVoiceTableView.delegate = self
                                self.inVoiceTableView.dataSource  = self
                                self.inVoiceTableView.reloadData()
                            }
                        }else{
                            DispatchQueue.main.async {
                                //show no data avaialble
                                alertViewObj.wrapAlert(title: "", body: "No Records Found".localized, img: nil)
                                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                    self.navigationController?.popViewController(animated: true)
                                })
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                    }else{
                        //failed
                        DispatchQueue.main.async {
                            self.showErrorAlert(errMessage: isValidObj.msg ?? "")
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                      self.showErrorAlert(errMessage: "Some error happen please try again".localized)
                    }
                }
            })
        } else {
            self.noInternetAlert()
            progressViewObj.removeProgressView()
        }
    }
  
    
    fileprivate func convertDateFormatter(date: String) -> String {
        let localDate = date
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"//this your string date format
        let date = dateFormatter.date(from: localDate)
        //dateFormatter.dateFormat = "EEE, dd-MM-yyyy HH:mm:ss"///this is what you want to convert format
        dateFormatter.dateFormat = "dd-MMM-yyyy HH:mm:ss"
        if let data = date{
            let timeStamp = dateFormatter.string(from: data)
            return timeStamp
        }else{
            let dateNEWFormatter = DateFormatter()
            dateNEWFormatter.calendar = Calendar(identifier: .gregorian)
            dateNEWFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:SS"
            let value = dateNEWFormatter.date(from: localDate)
           // dateNEWFormatter.dateFormat = "EEE, dd-MM-yyyy HH:mm:ss"
            dateNEWFormatter.dateFormat = "dd-MMM-yyyy HH:mm:ss"
            
            if let newData = value{
                let timeStamp = dateNEWFormatter.string(from: newData)
                if timeStamp.contains("PM") || timeStamp.contains("AM"){
                    let newTimeValue = timeStamp.components(separatedBy: " ")
                    dateNEWFormatter.dateFormat = "dd-MMM-yyyy hh:mm:SS"
                    let newTime = dateNEWFormatter.string(from: newData)
                    
                    if newTimeValue.indices.contains(2){
                         return newTime + " " + newTimeValue[2]
                    }else{
                         return newTime
                    }
                    
                   
                }else{
                    //return 24 hr format
                   return timeStamp
                }
                
                
            }
            return ""
        }
    }
    
}


extension TaxInvoiceViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if trasactionMainHistory.count>0{
            return trasactionMainHistory.count
        }else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NearByConstant.kTaxPaymentInvoiceCell) as? TaxPaymentInvoiceCell else{
            return UITableViewCell()
        }
        cell.layer.cornerRadius = 20
        cell.layer.masksToBounds = true
        cell.invoiceButton.tag = indexPath.row
        cell.delegate = self
        cell.selectionStyle = .default
        if trasactionMainHistory.indices.contains(indexPath.row){
            if convertDateFormatter(date: trasactionMainHistory[indexPath.row].transactionTime ?? "") != "" {
                let data = convertDateFormatter(date: trasactionMainHistory[indexPath.row].transactionTime ?? "")
                if data != ""{
                   // let value = data.replacingOccurrences(of: ",", with: "").components(separatedBy: " ")
                    let value = data.components(separatedBy: " ")
                    if value.count>0{
                        if value.indices.contains(0){
                            cell.headerDate.text = value[0]
                        }
                        if value.indices.contains(1){
                            cell.headerDate.text = value[0] //+ " " + value[1]
                        }
                    }
                }
            }else{
                cell.headerDate.text = ""
            }
            cell.invoiceButton.titleLabel?.font = UitilityClass.getZwagiFontWithSize(size: appButtonSize)
            cell.invoiceButton.setTitle("View Invoice".localized, for: .normal)
            cell.setDataOnCell(data :trasactionMainHistory[indexPath.row])
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 285.0
        
    }
    
}


extension TaxInvoiceViewController: TaxInvoiceDelegate{
    func sendDataOfParticularCell(index: Int){
        //send data from to the pdf screeen
        let data = convertDateFormatter(date: trasactionMainHistory[index].transactionTime ?? "").components(separatedBy: " ")
        var dicName = ""
        
//        var dic  = Dictionary[String: String]()
//        dic["Bill Amount"] = String(trasactionMainHistory[index].payAmount ?? 0.0) + "MMK"
//        dic["Transaction ID"] = trasactionMainHistory[index].transactionID ?? ""
        
        
        if data.count>0{
            if data.indices.contains(1) {
               // dic["Transaction Time"] = data[0] + " " + data[1]
                dicName = UitilityClass.makeStringForQR(firstName: "OK$ Number".localized,secondName: UserModel.shared.mobileNo) + UitilityClass.makeStringForQR(firstName: "OK$ Name".localized,secondName: UserModel.shared.name) + UitilityClass.makeStringForQR(firstName: "OK$ Transaction ID".localized,secondName: trasactionMainHistory[index].transactionID ?? "") + UitilityClass.makeStringForQR(firstName: "Bill Amount".localized,secondName: wrapAmountWithCommaDecimal(key: String(trasactionMainHistory[index].payAmount)) + " MMK") + UitilityClass.makeStringForQR(firstName: "Transaction Time".localized,secondName: data[0] + " " + data[1]) + UitilityClass.makeStringForQR(firstName: "Taxpayer Identification Number".localized,secondName: trasactionMainHistory[index].payerRegNumber ?? "")
            }
            if data.indices.contains(2){
                //dic["Transaction Time"] = data[0] + " " + data[1] + " " + data[2]
                dicName = UitilityClass.makeStringForQR(firstName: "OK$ Number".localized,secondName: UserModel.shared.mobileNo) + UitilityClass.makeStringForQR(firstName: "OK$ Name".localized,secondName: UserModel.shared.name)
                    + UitilityClass.makeStringForQR(firstName: "OK$ Transaction ID".localized,secondName: trasactionMainHistory[index].transactionID ?? "")  + UitilityClass.makeStringForQR(firstName: "Bill Amount".localized,secondName: wrapAmountWithCommaDecimal(key: String(trasactionMainHistory[index].payAmount)) + " MMK") + UitilityClass.makeStringForQR(firstName: "Transaction Time".localized,secondName: data[0] + " " + data[1] + " " + data[2]) + UitilityClass.makeStringForQR(firstName: "Taxpayer Identification Number".localized,secondName: trasactionMainHistory[index].payerRegNumber ?? "")
            }
            
            if data.indices.contains(3){
                //dic["Transaction Time"] = data[0] + " " + data[1] + " " + data[2]
                dicName = UitilityClass.makeStringForQR(firstName: "OK$ Number".localized,secondName: UserModel.shared.mobileNo) + UitilityClass.makeStringForQR(firstName: "OK$ Name".localized,secondName: UserModel.shared.name)
                    + UitilityClass.makeStringForQR(firstName: "OK$ Transaction ID".localized,secondName: trasactionMainHistory[index].transactionID ?? "")  + UitilityClass.makeStringForQR(firstName: "Bill Amount".localized,secondName: wrapAmountWithCommaDecimal(key: String(trasactionMainHistory[index].payAmount)) + " MMK") + UitilityClass.makeStringForQR(firstName: "Transaction Time".localized,secondName: data[0] + " " + data[1] + " " + data[2]) + UitilityClass.makeStringForQR(firstName: "Taxpayer Identification Number".localized,secondName: trasactionMainHistory[index].payerRegNumber ?? "")
            }

            
        }else{
            dicName = UitilityClass.makeStringForQR(firstName: "OK$ Number".localized,secondName: UserModel.shared.mobileNo) + UitilityClass.makeStringForQR(firstName: "OK$ Name".localized,secondName: UserModel.shared.name) + UitilityClass.makeStringForQR(firstName: "OK$ Transaction ID".localized,secondName: trasactionMainHistory[index].transactionID ?? "") + UitilityClass.makeStringForQR(firstName: "Bill Amount".localized,secondName: wrapAmountWithCommaDecimal(key: String(trasactionMainHistory[index].payAmount)) + " MMK") +   UitilityClass.makeStringForQR(firstName: "Transaction Time".localized,secondName:trasactionMainHistory[index].transactionTime ?? "") +
                UitilityClass.makeStringForQR(firstName: "Taxpayer Identification Number".localized,secondName: trasactionMainHistory[index].payerRegNumber ?? "")
        }
        
        
        
        let titleString = ["OK$ Name".localized,
                           "OK$ Number".localized,
                           "OK$ Transaction ID".localized,
                           "Tax Payer".localized,
                           "Taxpayer Identification Number".localized,
                           "NRC".localized,
                           "Tax Office".localized,
                           "Tax Type".localized,
                           "Income Year".localized,
                           "Bank Acc Number".localized,
                           "Address".localized,
                           "Supplier".localized,
                           "Pay Amount".localized,
                           "Transaction ID".localized]
        
        //made this two seperate because compiler was not able to compile
        let firstSet = [trasactionMainHistory[index].transactionID ?? "",
                        trasactionMainHistory[index].payerName ?? "",
                   trasactionMainHistory[index].payerRegNumber ?? "",
        trasactionMainHistory[index].companyOrCitizenNumber ?? "",
        trasactionMainHistory[index].taxOffice ?? "",
        trasactionMainHistory[index].taxType ?? ""]
        let infoString = [trasactionMainHistory[index].incomeYear ?? "",
                          trasactionMainHistory[index].bankAcNumber ?? "",
                          trasactionMainHistory[index].address ?? "",
                          trasactionMainHistory[index].supplier ?? "",
                          String(trasactionMainHistory[index].payAmount),
                          trasactionMainHistory[index].transactionCode]
        
        qrInfo.removeAll()
        qrTitle.removeAll()
        
        qrTitle = titleString
        qrInfo.append(UserModel.shared.name)
        qrInfo.append(UserModel.shared.mobileNo.replacingOccurrences(of: "0095", with: "(+95) 0"))

        for i in 0..<firstSet.count{
            qrInfo.append(firstSet[i])
        }
        for j in 0..<infoString.count{
            qrInfo.append(infoString[j])
        }
        
        
        //qrInfo.ap = name + num + firstSet + infoString

            if #available(iOS 13.0, *) {
                let obj = self.storyboard?.instantiateViewController(identifier: NearByConstant.kTaxReceiptFinalController) as? TaxReceiptFinalController
                if let isValid = obj{
                    isValid.qrCodeString = dicName
                    isValid.qrTitle = qrTitle
                    isValid.qrInfo = qrInfo
                   // isValid.dateTimeStr = convertDateFormatter(date: trasactionMainHistory[index].transactionTime ?? "")
                        //.replacingOccurrences(of: ",", with: "")
                    
                   
                    if data.count>0{
                        if data.indices.contains(0){
                            isValid.transactionDate = data[0]
                        }
                        if data.indices.contains(1){
                            isValid.transactionTime = data[1]
                        }
                        if data.indices.contains(2){
                            isValid.transactionTime = data[1] + " " + data[2]
                        }
                    }
                    
                    isValid.qrImage = UitilityClass.getQRImage(stringQR:dicName,withSize: 5,logoName: "tax_Logo")
                    isValid.trasactionHistory = trasactionMainHistory[index]
                    self.navigationController?.pushViewController(isValid, animated: true)
                }
            } else {
                if let obj = self.storyboard?.instantiateViewController(withIdentifier: NearByConstant.kTaxReceiptFinalController) as? TaxReceiptFinalController{
                    obj.qrCodeString = dicName
                    obj.qrInfo = qrInfo
                    obj.qrTitle = qrTitle
                  //  obj.dateTimeStr = convertDateFormatter(date: trasactionMainHistory[index].transactionTime ?? "")
                    
                    if data.count>0{
                        if data.indices.contains(0){
                            obj.transactionDate = data[0]
                        }
                        if data.indices.contains(1){
                            obj.transactionTime = data[1]
                        }
                        if data.indices.contains(2){
                            obj.transactionTime = data[1] + " " + data[2]
                        }
                    }
                    
                        //.replacingOccurrences(of: ",", with: "")
                    obj.qrImage = UitilityClass.getQRImage(stringQR:dicName,withSize: 5,logoName: "tax_Logo")
                    obj.trasactionHistory = trasactionMainHistory[index]
                    self.navigationController?.pushViewController(obj, animated: true)
                }
            }
            
       // }
    }
}


// MARK: -UISearchBarDelegate
extension TaxInvoiceViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
//            uiButton.setTitle("Cancel".localized, for:.normal)
//        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if range.location == 0 && text == " " { return false }
        if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
            let updatedText = searchText.replacingCharacters(in: textRange, with: text)
            if updatedText.containsEmoji {
                return false
            }
            if searchBar.text?.last == " " && text == " " {
                return false
            }
            if updatedText.count > 50 {
                return false
            }
            
            applySearch(with: updatedText)
        }
        return true
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            applySearch(with: "")
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearchNeed = true
        setUpNavigation()
        //setting it to initial
        trasactionMainHistory = transactionHistory
        self.inVoiceTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.applySearch(with: searchBar.text ?? "")
        searchBar.endEditing(true)
    }
}
