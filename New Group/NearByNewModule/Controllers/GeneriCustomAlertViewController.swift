//
//  GeneriCustomAlertViewController.swift

import UIKit


protocol GeneriCustomAlertViewDelegate {
    func closeClick()
}


class GeneriCustomAlertViewController: UIViewController {
    
    var responseCode:String?
    var delegate: GeneriCustomAlertViewDelegate?
    var recordArray: [Dictionary<String, String>] = [Dictionary<String, String>]()
     var isComingFromRecent = false
    @IBOutlet weak var parentViewView: UIView!
    
    @IBOutlet weak var tblTableView: UITableView!
    
    
    
    @IBOutlet weak var customShareBtn: UIButton!{
        didSet{
            customShareBtn.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
            customShareBtn.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
            customShareBtn.layer.shadowOpacity = 1.0
            customShareBtn.layer.shadowRadius = 0.0
            customShareBtn.layer.masksToBounds = false
            //            customShareBtn.layer.cornerRadius = 25.0
            customShareBtn.roundedButtonOneSide()
            customShareBtn.setTitle(customShareBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    var qrInfoLocalTitle = [String]()
    var qrInfoLocalValue = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationItem.title = "Receipt"
        if let hasValue = responseCode{
            
            let values = hasValue.components(separatedBy: ";")
            qrInfoLocalTitle.removeAll()
            qrInfoLocalValue.removeAll()
            
            for i in 0..<values.count-1{
                let data = values[i].components(separatedBy: "=")
                            
                print(data)
                qrInfoLocalTitle.append(data[0])
                qrInfoLocalValue.append(data[1])
            }
            
            if qrInfoLocalTitle.count>0{
                DispatchQueue.main.async {
                    self.tblTableView.delegate = self
                    self.tblTableView.dataSource = self
                    self.showAnimate()
                    self.tblTableView.tableFooterView = UIView()
                    self.tblTableView.reloadData()
                }
            }
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print(recordArray)
        print(recordArray.count)
        self.navigationItem.hidesBackButton = true
        //         DispatchQueue.main.async {
        //             self.tblTableView.delegate = self
        //             self.tblTableView.dataSource = self
        //             self.tblTableView.reloadData()
        //         }
    }
    
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate(completionHandler: @escaping (_ isCompleted: Bool) -> Void) {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished) {
                self.dismiss(animated: true, completion: nil)
                completionHandler(true)
            }
        })
    }
    
    
    
    
    
    @IBAction func dismissViewAction(_ sender: UITapGestureRecognizer) {
        //        self.removeAnimate() { isCompleted in
        //
        //        }
    }
    
    
    @IBAction func crossAction(_ sender: UIButton) {
        self.removeAnimate() { isCompleted in
            self.delegate?.closeClick()
        }
        
    }
    
    @IBAction func btnShareClick(_ sender: UIButton){
        guard let pdfUrl = CodeSnippets.generatePDF(viewForPDF: self.parentViewView, pdfFile: "Near Me Qr Payment Receipt") else { return }
        let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
        
    }
    
}


extension GeneriCustomAlertViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return qrInfoLocalTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tblTableView.dequeueReusableCell(withIdentifier: "customTableViewCell", for: indexPath) as? customTableViewCell{
            
            if qrInfoLocalTitle.indices.contains(indexPath.row){
               cell.NameLbl?.text = qrInfoLocalTitle[indexPath.row]
            }
            
            if qrInfoLocalValue.indices.contains(indexPath.row){
                if indexPath.row == 0{
                    cell.NameValueLbl?.text = qrInfoLocalValue[indexPath.row].replacingOccurrences(of: "0095", with: "(+95) 0")
                }else{
                  cell.NameValueLbl?.text = qrInfoLocalValue[indexPath.row]
                }
                
                
            }
            
            
            return cell
        }
        return customTableViewCell()
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

 
// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
