//
//  NearMeQrPaymentViewController.swift
//  OK
//
//  Created by iMac on 27/07/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

class NearMeQrPaymentViewController: UIViewController {
    
    var nearByQRPayModelObj:NearByQRPayModel?
    var recordArray: [Dictionary<String, String>] = [Dictionary<String, String>]()
     var isComingFromRecent = false
    @IBOutlet var transactionLabel: UILabel!
    @IBOutlet weak var parentViewView: UIView?
    @IBOutlet weak var tblTableView: UITableView?
    @IBOutlet weak var customShareBtn: UIButton?{
        didSet{
            customShareBtn?.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
            customShareBtn?.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
            customShareBtn?.layer.shadowOpacity = 1.0
            customShareBtn?.layer.shadowRadius = 0.0
            customShareBtn?.layer.masksToBounds = false
            customShareBtn?.layer.cornerRadius = 4.0
            customShareBtn?.setTitle(customShareBtn?.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
       self.navigationItem.title = "Receipt"
        transactionLabel.text = "Tax Payment Successful".localized
        recordArray.append(["Payment Transaction ID": nearByQRPayModelObj?.paymentTransId ?? ""])
        recordArray.append(["Merchant Name": nearByQRPayModelObj?.merchantName ?? "-"])
        
        recordArray.append(["Near Me Reference Number": nearByQRPayModelObj?.merRefNumber ?? ""])
        recordArray.append(["Amount": "\(nearByQRPayModelObj?.amount ?? "0") MMK"])
        
        //recordArray.append(["Transaction Date and Time": nearByQRPayModelObj?.transactionTime ?? ""])
        recordArray.append(["Transaction Type": nearByQRPayModelObj?.PaymentType ?? ""])
        
        recordArray.append(["Merchant ID": nearByQRPayModelObj?.paymentTransId ?? ""])
        
        print(recordArray)
        
        DispatchQueue.main.async {
            self.tblTableView?.delegate = self
            self.tblTableView?.dataSource = self
            self.showAnimate()
            self.tblTableView?.tableFooterView = UIView()
            self.tblTableView?.reloadData()
        }
        
        self.saveToPrint()
        
        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print(recordArray)
        print(recordArray.count)
        self.navigationItem.hidesBackButton = true
        //         DispatchQueue.main.async {
        //             self.tblTableView.delegate = self
        //             self.tblTableView.dataSource = self
        //             self.tblTableView.reloadData()
        //         }
    }
    
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate(completionHandler: @escaping (_ isCompleted: Bool) -> Void) {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished) {
                self.dismiss(animated: true, completion: nil)
                completionHandler(true)
            }
        })
    }
    
    
    
    
    
    @IBAction func dismissViewAction(_ sender: UITapGestureRecognizer) {
        //        self.removeAnimate() { isCompleted in
        //
        //        }
    }
    
    
    @IBAction func crossAction(_ sender: UIButton) {
        //        self.removeAnimate() { isCompleted in
        //
        //        }
        
        NotificationCenter.default.post(name: .goDashboardScreen, object: nil)
        self.performSegue(withIdentifier: "unwindSegueToVC1", sender: self)
        
        
    }
    
    
    private func saveToPrint() {
        
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == PHAuthorizationStatus.authorized) {
            println_debug(".authorized")
            DispatchQueue.main.async {
                self.saveToGallary()
            }
        }else if (status == PHAuthorizationStatus.denied) {
            println_debug(".denied")
            self.showPhotoAlert()
        }else if (status == PHAuthorizationStatus.notDetermined) {
            println_debug(".notDetermined")
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                if (newStatus == PHAuthorizationStatus.authorized) {
                    println_debug(".authorized 2")
                    DispatchQueue.main.async {
                        self.saveToGallary()
                    }
                }else {
                    self.showPhotoAlert()
                }
            })
        }else if (status == PHAuthorizationStatus.restricted) {
            // Restricted access - normally won't happen.
        }
    }
    
    private func saveToGallary() {
        guard let newImage = self.parentViewView?.toImage() else { return }
        UIImageWriteToSavedPhotosAlbum(newImage, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        var strMessage: String?
        if let error = error {
            strMessage = error as? String
        }else {
            strMessage = "Successfully saved in gallery".localized
        }
        alertViewObj.wrapAlert(title: "", body: strMessage ?? "", img: #imageLiteral(resourceName: "info_success"))
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            
        })
        alertViewObj.showAlert(controller: self)
    }
    
    
    private func showPhotoAlert(){
        alertViewObj.wrapAlert(title:"", body:"Please Allow Camera Access".localized, img: UIImage(named: "alert-icon"))
        alertViewObj.addAction(title: "Setting".localized, style: .target , action: {
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString) as! URL , options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString) as! URL)
            }
        })
        alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
        }
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    @IBAction func btnShareClick(_ sender: UIButton){
        guard let pdfUrl = CodeSnippets.generatePDF(viewForPDF: self.parentViewView ?? UIView() , pdfFile: "Near Me Qr Payment Receipt") else { return }
        let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
        
    }
    
}


extension NearMeQrPaymentViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{return 1}else{return (recordArray.count)}
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            
            if let cell = tblTableView?.dequeueReusableCell(withIdentifier: "customTableViewTimeCell", for: indexPath) as? customTableViewCell{
                cell.wrapRecordTime(dateAndTimeStr: nearByQRPayModelObj?.transactionTime ?? "")
                return cell
            }
        }else{
            if let cell = tblTableView?.dequeueReusableCell(withIdentifier: "customTableViewCell", for: indexPath) as? customTableViewCell{
                cell.wrapRecord(obj: recordArray[indexPath.row])
                return cell
            }
        }
        return customTableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}




class customTableViewCell: UITableViewCell {
    @IBOutlet var  NameLbl: UILabel?{
        didSet {
            self.NameLbl?.font = UIFont(name: appFont, size: 15.0)
            NameLbl?.text = NameLbl?.text?.localized
            
        }
    }
    
    
    @IBOutlet var  NameValueLbl: UILabel?{
        didSet {
            self.NameValueLbl?.font = UIFont(name: appFont, size: 15.0)
            NameValueLbl?.text = NameValueLbl?.text?.localized
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    func wrapRecordTime(dateAndTimeStr:String){
        //print(dateAndTimeStr)
        
        if let dateAndTime = dateAndTimeStr.characters.split(whereSeparator: {$0 == " "}).map(String.init) as? [String]{
            
            self.NameLbl?.text =  self.convertDateAndTime(format1: "MM/dd/yyyy", format2: "E, d MMM yyyy", value: dateAndTime[0])
            self.NameValueLbl?.text = self.convertDateAndTime(format1: "HH:mm:ss", format2: "h:mm a", value: dateAndTime[1]) //+ " \(dateAndTime[2])"
        }
        
    }
    
    
    func convertDateAndTime(format1: String, format2: String,value: String) -> String{
        
        let inputDateFormatter = DateFormatter()
        inputDateFormatter.dateFormat = format1
        let date1: Date = inputDateFormatter.date(from: value) ?? Date()
        
        let outputDateFormatter = DateFormatter()
        outputDateFormatter.dateFormat = format2
         
        
        return (outputDateFormatter.string(from: date1))
    }
    
    
    func wrapRecord(obj:Dictionary<String, String>){
        if obj.count>0{
            println_debug(obj)
            for data in obj {
                println_debug(data)
                let key = data.key //key value which is colour or fabric
                let value = data.value //value which would be moss green or cotton
                
                self.NameLbl?.text = key.localized
                
                if value != ""{
                    self.NameValueLbl?.text = value
                }else{
                    self.NameValueLbl?.text = "-"
                }
            }
        }
    }
}



// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
