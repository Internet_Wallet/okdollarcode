//
//  NearByConnection.swift
//  OK
//
//  Created by Tushar Lama on 18/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation

class NearByConnection {
    var networkObject = NearByNetwork()
    
    //get the QR Code
    func requestNearByQR(completion:@escaping(_ jsonData : NearByQRModel?,_ hasError: Bool?)->Void) {
        networkObject.sendHttpRequest(requestUrl: URL(string:NearByConstant.getURLForNearByGetDataForQR())!, requestData: NearByParam.nearByGetParamForQR(), httpMethodName: NearByConstant.nearByServiceTypePost, completionHandler: { (response,error) in
            //if there is no error
            if !error{
                completion(NearByQRModel(response: response as? NSDictionary), error)
            }else{
                completion(nil, error)
            }
        })
    }
    
    //Get the latest transaction receipt
    func requestNearByCurrentTransaction(qrID: String,completion:@escaping(_ jsonData : NearByQRPayModel?,_ hasError: Bool?)->Void) {
        networkObject.sendHttpRequest(requestUrl: URL(string:NearByConstant.getURLForNearByGetQR())!, requestData: NearByParam.nearByGetParamForTransactionQR(qrID: qrID,spid: "",destinationNumber: ""), httpMethodName: NearByConstant.nearByServiceTypePost, completionHandler: { (response,error) in
          
            //if there is no error
            if !error{
                let data =  response as? NSDictionary
                if let isValidRes = data{
                    if isValidRes.count>0{
                      let dataNew = isValidRes.value(forKey: "Data") as? String
                        if let isValidStr = dataNew{
                            completion(NearByQRPayModel(data: self.convertToDictionary(text: isValidStr),code:isValidRes.value(forKey: "Code") as? Int,msg: isValidRes.value(forKey: "Msg") as? String), error)
                        }
                    }else{
                        completion(nil, error)
                    }
                }else{
                    completion(nil, error)
                }
            }else{
                completion(nil, error)
            }
        })
    }
    
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    //send the merchant information\
    func sendMerchantInfo(merchantInfo: String,completion:@escaping(_ jsonData : NearByQRMerchantInfo?,_ hasError: Bool?)->Void) {
        networkObject.sendHttpRequest(requestUrl: URL(string:NearByConstant.getURLForNearByScanQR())!, requestData: NearByParam.getParamForMerchantTransaction(merTransInfo: merchantInfo), httpMethodName: NearByConstant.nearByServiceTypePost, completionHandler: { (response,error) in
          
            //if there is no error
            if !error{
                completion(NearByQRMerchantInfo(response: response as? NSDictionary), error)
            }else{
                completion(nil, error)
            }
        })
    }
    
    //send tax payment info to the server
    func sendMerchantTaxInfo(merchantInfo: [String],completion:@escaping(_ jsonData : TaxFinalModel?,_ hasError: Bool?)->Void) {
        
        //for now sending only one mmk
        let validAmount = merchantInfo[merchantInfo.count - 2].components(separatedBy: " ")
        if validAmount.indices.contains(0){
            let amount = validAmount[0].components(separatedBy: "MMK")
            if amount.indices.contains(0){
                let amount = amount[0].replacingOccurrences(of: ",", with: "")
                print(amount)
                print(NearByConstant.getURLToPayTax())
                print(NearByParam.getParamToPayTax(transcationCode: merchantInfo[merchantInfo.count - 1], payerName: merchantInfo[2], payerRegNum: merchantInfo[3], companyCitizenNumber: merchantInfo[4], address: merchantInfo[5], taxType: merchantInfo[6], incomeYear: merchantInfo[7], bankAccountNum: merchantInfo[8], taxOffice: merchantInfo[9], supplier: merchantInfo[10], payAmount:amount, taxAmount: amount))
                networkObject.sendHttpRequest(requestUrl: URL(string:NearByConstant.getURLToPayTax())!, requestData: NearByParam.getParamToPayTax(transcationCode: merchantInfo[merchantInfo.count - 1], payerName: merchantInfo[2], payerRegNum: merchantInfo[3], companyCitizenNumber: merchantInfo[4], address: merchantInfo[5], taxType: merchantInfo[6], incomeYear: merchantInfo[7], bankAccountNum: merchantInfo[8], taxOffice: merchantInfo[9], supplier: merchantInfo[10], payAmount:amount, taxAmount: amount), httpMethodName: NearByConstant.nearByServiceTypePost, completionHandler: { (response,error) in
                    
                    //merchantInfo[merchantInfo.count - 2]
                    
                    //if there is no error
                    if !error{
                        completion(TaxFinalModel(data: response as? NSDictionary), error)
                    }else{
                        completion(nil, error)
                    }
                })
            }
        }
        
        
    }
    
    
    //get all the trasaction history of tax and parsing data here
    func getTaxTransactionhistory(completion:@escaping(_ jsonData : TaxInvoiceModel?,_ hasError: Bool?)->Void) {
        
        print(NearByConstant.getURLToSeeTaxHistory())
        print(NearByParam.getParamToGenerateInvoice())
        
        networkObject.sendHttpRequest(requestUrl: URL(string:NearByConstant.getURLToSeeTaxHistory())!, requestData: NearByParam.getParamToGenerateInvoice(), httpMethodName: NearByConstant.nearByServiceTypePost, completionHandler: { (response,error) in
          
            //if there is no error
            if !error{
                completion(TaxInvoiceModel(data: response as? NSDictionary), error)
            }else{
                completion(nil, error)
            }
        })
    }
    
    
    //get all the trasaction history of tax and parsing data here
    func getQRTimeMsg(completion:@escaping(_ jsonData : NearMeTimeStringModel?,_ hasError: Bool?)->Void) {
        networkObject.sendHttpRequest(requestUrl: URL(string:NearByConstant.getURLToFetchQRTimeAndMsg())!, requestData: NearByParam.nearByGetTimeAndInfoString(), httpMethodName: NearByConstant.nearByServiceTypePost, completionHandler: { (response,error) in
          
            //if there is no error
            if !error{
                let data =  response as? NSDictionary
                if let isValidRes = data{
                    if isValidRes.count>0{
                            completion(NearMeTimeStringModel(data: isValidRes), error)
                    }else{
                        completion(nil, error)
                    }
                }else{
                    completion(nil, error)
                }
            }else{
                completion(nil, error)
            }
        })
    }
    
    
    
    //get the data of the merchant based on QR scan
    
    func getDataFromQR(qrString: String,iVector: String,merchantId: String,completion:@escaping(_ jsonData : NewScanQRModel?,_ hasError: Bool?)->Void) {
           networkObject.sendHttpRequest(requestUrl: URL(string:NewScanToPayHelper.getUserInfoFromQR())!, requestData: NewScanPayParameter.newScanDataQRParam(qrString: qrString, iVector: iVector, merchantID: merchantId), httpMethodName: NearByConstant.nearByServiceTypePost, completionHandler: { (response,error) in
             
               //if there is no error
               if !error{
                   let data =  response as? NSDictionary
                   if let isValidRes = data{
                       if isValidRes.count>0{
                         let dataNew = isValidRes.value(forKey: "Data") as? String
                           if let isValidStr = dataNew{
                               completion(NewScanQRModel(data: self.convertToDictionary(text: isValidStr),code:isValidRes.value(forKey: "Code") as? Int,msg: isValidRes.value(forKey: "Msg") as? String), error)
                           }
                       }else{
                           completion(nil, error)
                       }
                   }else{
                       completion(nil, error)
                   }
               }else{
                   completion(nil, error)
               }
           })
       }
    
    
    func cancelQRData(refernceNumber: String,completion:@escaping(_ jsonData : NSDictionary?,_ hasError: Bool?)->Void) {
        networkObject.sendHttpRequest(requestUrl: URL(string:NewScanToPayHelper.UserCancelQR())!, requestData: NewScanPayParameter.newScanDataCancelSendQRInfo(refernceNumber: refernceNumber), httpMethodName: NearByConstant.nearByServiceTypePost, completionHandler: { (response,error) in
          
            //if there is no error
            if !error{
                let data =  response as? NSDictionary
                if let isValidRes = data{
                    if isValidRes.count>0{
                    //  let dataNew = isValidRes.value(forKey: "Data") as? String
                      //  if let isValidStr = dataNew{
                            completion(isValidRes, error)
                      //  }
                    }else{
                        completion(nil, error)
                    }
                }else{
                    completion(nil, error)
                }
            }else{
                completion(nil, error)
            }
        })
    }
    
    
    
    func sendQRData(refernceNumber: String,comments: String,completion:@escaping(_ jsonData : NewScanPayModel?,_ hasError: Bool?)->Void) {
        networkObject.sendHttpRequest(requestUrl: URL(string:NewScanToPayHelper.sendUserInfoFromQR())!, requestData: NewScanPayParameter.newScanDataSendQRInfo(refernceNumber: refernceNumber,comments: comments), httpMethodName: NearByConstant.nearByServiceTypePost, completionHandler: { (response,error) in
          
            //if there is no error
            if !error{
                let data =  response as? NSDictionary
                if let isValidRes = data{
                    if isValidRes.count>0{
                      let dataNew = isValidRes.value(forKey: "Data") as? String
                        if let isValidStr = dataNew{
                            completion(NewScanPayModel(data: self.convertToDictionary(text: isValidStr),code:isValidRes.value(forKey: "Code") as? Int,msg: isValidRes.value(forKey: "Msg") as? String), error)
                        }
                    }else{
                        completion(nil, error)
                    }
                }else{
                    completion(nil, error)
                }
            }else{
                completion(nil, error)
            }
        })
    }
    
    //failure block
    
    func sendfailureBlockToApi(request: Any,response: Any,type: String,completion:@escaping(_ hasError: Bool?)->Void) {
        networkObject.sendHttpRequest(requestUrl: URL(string:FailureHelper.failureAPI)!,requestData:FailureBlockParam.getFailureBlockParam(request: request, response: response, type: type), httpMethodName: NearByConstant.nearByServiceTypePost, completionHandler: { (response,error)  in
            
            completion(nil)
//            //if there is no error
//            if !error{
//                completion(error)
//            }else{
//                completion(error)
//            }
        })
    }

    
    
}
