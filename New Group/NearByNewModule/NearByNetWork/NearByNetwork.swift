//
//  NearByNetwork.swift
//  OK
//
//  Created by Tushar Lama on 18/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation

class NearByNetwork: NSObject {
    
    func sendHttpRequest(requestUrl:URL,requestData:Any,httpMethodName:String,completionHandler:@escaping(_ jsonData : Any?,_ hasError: Bool)->Void){
        var request = URLRequest(url: requestUrl , cachePolicy: .useProtocolCachePolicy, timeoutInterval: 50.0)
        let jsonData = try! JSONSerialization.data(withJSONObject: requestData, options: JSONSerialization.WritingOptions.prettyPrinted)
        let jsonString = String(data: jsonData, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as String
        let dataToSend = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        request.httpBody = dataToSend
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = httpMethodName
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest) {data,response,error in
            guard error == nil else
            {
                completionHandler(nil,true)
                return
            }
            
            do {
                if let isValidData =  data{
                    let result = try JSONSerialization.jsonObject(with: isValidData, options: [])
                    print("*******************************************")
                    print(result)
                    print("*******************************************")
                    completionHandler(result,false)
                }else{
                    completionHandler(nil,true)
                }
            }
            catch{
                completionHandler(nil,true)
            }
        }
        dataTask.resume()
    }
    
}
