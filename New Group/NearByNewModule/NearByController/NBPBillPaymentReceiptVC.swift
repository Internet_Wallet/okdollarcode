//
//  NBPBillPaymentReceiptVC.swift
//  OK
//
//  Created by OK$ on 31/08/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

class NBPBillPaymentReceiptVC: OKBaseController {

    var nearByQRPayModelObj:NearByQRPayModel?

       @IBOutlet var receiptTableVidw: UITableView!

       @IBOutlet var starButton: UIButton!
       var arrReceiptList = [NSMutableDictionary]()

       @IBOutlet weak var receiptView: UIView!
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        starButton.setImage(#imageLiteral(resourceName: "star1").withRenderingMode(.alwaysOriginal), for: .normal)
        
        self.receiptTableVidw.register(UINib(nibName: NearByConstant.kNearByPaymentCell, bundle: nil), forCellReuseIdentifier: NearByConstant.kNearByPaymentCell)
        
        arrReceiptList = [["Title": "Payer OK$","Value":"Maung Maung +95923548764"],["Title": "Merchant ID","Value":"45312515487628"],["Title": "Near Me Txn ID","Value":"36985234"],["Title": "OK$ Txn ID","Value":"4568742325"],["Title": "Amount","Value":"95,000.00 MMK"],["Title": "Remark","Value":"Remarks will come if present"]]
        
        DispatchQueue.main.async {
            self.receiptTableVidw?.delegate = self
            self.receiptTableVidw?.dataSource = self
            self.receiptTableVidw?.tableFooterView = UIView()
            self.receiptTableVidw.reloadData()
        }
        
    }
    
    @IBAction func onClickBackButton(_ sender: Any) {
              self.dismiss(animated: true, completion: nil)
          }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - Print & Share

extension NBPBillPaymentReceiptVC {
   
    private func saveToPrint() {
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == PHAuthorizationStatus.authorized) {
            println_debug(".authorized")
            DispatchQueue.main.async {
                self.saveToGallary()
            }
        }else if (status == PHAuthorizationStatus.denied) {
            println_debug(".denied")
            self.showPhotoAlert()
        }else if (status == PHAuthorizationStatus.notDetermined) {
            println_debug(".notDetermined")
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                if (newStatus == PHAuthorizationStatus.authorized) {
                    println_debug(".authorized 2")
                    DispatchQueue.main.async {
                        self.saveToGallary()
                    }
                }else {
                    self.showPhotoAlert()
                }
            })
        }else if (status == PHAuthorizationStatus.restricted) {
            // Restricted access - normally won't happen.
        }
    }
    
    private func saveToGallary() {
        guard let newImage = self.receiptView?.toImage() else { return }
        UIImageWriteToSavedPhotosAlbum(newImage, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        var strMessage: String?
        if let error = error {
            strMessage = error as? String
        }else {
            strMessage = "Successfully saved in gallery".localized
        }
        alertViewObj.wrapAlert(title: "", body: strMessage ?? "", img: #imageLiteral(resourceName: "info_success"))
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            
        })
        alertViewObj.showAlert(controller: self)
    }
    
    private func showPhotoAlert(){
        DispatchQueue.main.async {
        alertViewObj.wrapAlert(title:"", body:"Please Allow Camera Access".localized, img: UIImage(named: "alert-icon"))
        alertViewObj.addAction(title: "Setting".localized, style: .target , action: {
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString) as! URL , options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString) as! URL)
            }
        })
        alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
        }
            alertViewObj.showAlert(controller: self)
        }
    }
    
    @IBAction func btnShareClick(_ sender: UIButton){
        guard let pdfUrl = CodeSnippets.generatePDF(viewForPDF: self.receiptView ?? UIView() , pdfFile: "Near Me Qr Payment Receipt") else { return }
        let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
}

//MARK: Note Delegate
extension NBPBillPaymentReceiptVC : NearByPaymentNoteDelegate {
    
    @IBAction func enterNoteButtonAction(_ sender: Any) {
        println_debug("enterNoteButtonAction")
        guard  let vc = UIStoryboard.init(name: "NearMePaymentGeneric", bundle: nil).instantiateViewController(withIdentifier: "NearByPaymentNoteVC") as? NearByPaymentNoteVC else {return}
        vc.strStatusScreen = "Receipt"
        addChild(vc)
        vc.view.frame = self.view.bounds
        vc.didMove(toParent: self)
        vc.delegate = self
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.reveal
        vc.view.layer.add(transition, forKey: nil)
        view.addSubview(vc.view)
    }
    
    func returnNote(note: String){
        println_debug(note)
        if note == "mark" {
            starButton.setImage(#imageLiteral(resourceName: "star3").withRenderingMode(.alwaysOriginal), for: .normal)
        } else {
            starButton.setImage(#imageLiteral(resourceName: "star2").withRenderingMode(.alwaysOriginal), for: .normal)
        }
    }
}

//MARK: Table View Delegate
extension NBPBillPaymentReceiptVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrReceiptList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NearByConstant.kNearByPaymentCell) as? NearByPaymentCell else{
            return UITableViewCell()
        }
        cell.selectionStyle = .none
        cell.titleLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
        cell.titleLabel.text = arrReceiptList[indexPath.row]["Title"] as? String ?? ""
        cell.merchantValueLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
        cell.merchantValueLabel.text = arrReceiptList[indexPath.row]["Value"] as? String ?? ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return  40.0
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
