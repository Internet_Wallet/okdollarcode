//
//  NearByPaymentAutoPayQRVC.swift
//  OK
//
//  Created by OK$ on 25/08/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class NearByPaymentAutoPayQRVC: OKBaseController {
    
    @IBOutlet var historyButton: UIButton!
    @IBOutlet var remarkButton: UIButton!
    @IBOutlet var remarkEditButton: UIButton!
    
    @IBOutlet weak var viewScanComplete: UIView!
    //Timer Display
    @IBOutlet var timeLabel: UILabel!
    var timerQR: Timer?
    var totalTime = 120
    var timeAddedFromServer = 0
    var timeCountFromServer = 0
    var timeStart = false
    
    //QR View
    @IBOutlet var qrImageView: UIImageView!
    var nearByViewModel = NearByViewModel()
    var qrData:  String?
    var isHistoryStatus: Bool?

    @IBOutlet weak var viewBGRemark: UIView!
    @IBOutlet weak var viewBGQRCode: UIView!
    //MARK: View Life Cycle

     override func viewWillAppear(_ animated: Bool) {
        viewScanComplete.isHidden = true
        getDataForTimeMsg()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
           super.viewWillDisappear(animated)
        //if !(isHistoryStatus ?? false) {
            if let timer = self.timerQR {
                timer.invalidate()
            }
        //}
       }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.configureBackButton(withTitle: "Near Me Payment".localized)
        
        historyButton.isHidden = true
        remarkEditButton.isHidden = true
        viewScanComplete.isHidden = true
        isHistoryStatus = false
        
        viewBGRemark.layer.borderWidth = 1
        viewBGRemark.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        
        viewBGQRCode.layer.borderWidth = 1
        viewBGQRCode.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        //Time Label
        timeLabel.layer.cornerRadius = 8.0
        timeLabel.layer.masksToBounds = true
        timeLabel.layer.borderColor = kDarkBlueColor.cgColor
        timeLabel.layer.borderWidth = 3.0
        //Add History Icon Navigation Right Bar
        setHistoryButton()
    }
   
    func setHistoryButton() {
        let buttonIcon      = UIImage(named: "menuUPI")
        let rightBarButton   = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.done, target: self, action: #selector(onClickHistoryButton))
        rightBarButton.image = buttonIcon
        rightBarButton.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    //MARK: Action Method
    @IBAction func onClickBackButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickHistoryButton(_ sender: Any) {
        if historyButton.isHidden == true {
            historyButton.isHidden = false
        } else {
            historyButton.isHidden = true
        }
    }
    
    @IBAction func onClickHistoryButtonAction(_ sender: Any) {
        historyButton.isHidden = true
        isHistoryStatus = true
        if let objHistory = UIStoryboard(name: "NewNearMePayment", bundle: nil).instantiateViewController(withIdentifier: "NearByPaymentHistoryVC") as? NearByPaymentHistoryVC {
            objHistory.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(objHistory, animated: true)
        }
    }
    
    
    //setting timer to start from 5 min
    fileprivate func setTimer(){
        let value = totalTime/60
        
        if value<10{
            timeLabel.text = "\(0)\(value):00"
        }else{
            timeLabel.text = "\(value):00"
        }
                
        self.startOtpTimer(time: totalTime - 1)
    }
    
    private func startOtpTimer(time: Int) {
        //5 min 300 sec
        self.totalTime = time
        self.timerQR = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    //once 5 min is over updating the timer here
    @objc func updateTimer() {
        timeCountFromServer += 1
        self.timeLabel.text = self.timeFormatted(self.totalTime)
        
        if totalTime > 0 {
            totalTime -= 1
            if timeCountFromServer >= 30 {
                viewScanComplete.isHidden = false
            } else {
                viewScanComplete.isHidden = true
            }
        }else {
            if let timer = self.timerQR {
                timeCountFromServer = 0
                timer.invalidate()
                alertViewObj.wrapAlert(title: "", body: "QR Code Expired \n Do you wish to create New QR?".localized, img: #imageLiteral(resourceName: "scan_qrTabNew"))
                alertViewObj.addAction(title: "CREATE QR".localized, style: .target , action: {
                    self.totalTime = self.timeAddedFromServer
                    self.getQR()
                })
                alertViewObj.addAction(title: "CANCEL".localized, style: .target , action: {
                    //self.dismiss(animated: true, completion: nil)
                    self.navigationController?.popToRootViewController(animated: true)
                })
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%01d:%02d", minutes, seconds)
    }
    
    //get the msg and time to show QR
    //this function will post merchant info to the server
    fileprivate func getDataForTimeMsg(){
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            nearByViewModel.getQRTimeMsg(finished:{
                progressViewObj.removeProgressView()
                //checking if data is available
                if let isValidData = self.nearByViewModel.nearByQRTimeMSgModel{
                    if isValidData.code == 200{
                        if let timeValue = isValidData.time{
                            self.totalTime = Int(timeValue) ?? 0
                            self.timeAddedFromServer = Int(timeValue) ?? 0
                        }else{
                            self.totalTime = 120
                        }
                        
                        if appDel.currentLanguage == "my"{
                            self.showMsgAlert(msg: isValidData.showAlertCustomMsgMyZaw ?? "")
                        }else if appDel.currentLanguage == "uni"{
                            self.showMsgAlert(msg: isValidData.showAlertCustomMsgMyUni ?? "")
                        }else{
                             self.showMsgAlert(msg: isValidData.showAlertCustomMsgEng ?? "")
                        }
                    }else{
                        self.showErrorAlert(errMessage: isValidData.msg ?? "")
                    }
                }else{
                    self.showErrorAlert(errMessage: "Some error happen please try again".localized)
                }
            })
        } else {
            self.noInternetAlert()
            progressViewObj.removeProgressView()
        }
    }
    
    fileprivate func showMsgAlert(msg: String){
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: "", body: msg, img: #imageLiteral(resourceName: "NearMeNew"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                self.getQR()
            })
            alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
                self.navigationController?.popToRootViewController(animated: false)
            })
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

// MARK: - API Data
 // MARK: - QR Generate
extension NearByPaymentAutoPayQRVC {
    
    @IBAction func onClickGenerateReceipt(_ sender: Any) {
        self.getTransactionDetail(qrData: qrData ?? "")
    }
    
    //this function will give details of the user transaction
    fileprivate func getTransactionDetail(qrData: String) {
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            nearByViewModel.getNearByCurrentTransactionDetails(qrData: qrData ,finished:{
                progressViewObj.removeProgressView()
                //checking if data is available
                if let obj = self.nearByViewModel.nearByQRPayModelObj{
                    DispatchQueue.main.async { [weak self] in
                        //if let timer = self?.timerQR {
                           // timer.invalidate()
                           // self?.timeLabel.isHidden = true
                           // self?.minLabel.isHidden = true
                            if obj.code ?? 0 == 200{
                                DispatchQueue.main.async {
                                    self?.navigateToReceipt()
                                }
                            }else{
                                DispatchQueue.main.async {
                                     alertViewObj.wrapAlert(title: "", body: obj.msg ?? "", img: #imageLiteral(resourceName: "scan_qrTabNew"))
                                                   alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                                    self?.navigateToReceipt()
                                                   })
                                                   alertViewObj.showAlert(controller: self)
                                }
                               // self?.showErrorAlert(errMessage: obj.msg ?? "")
                            }
                       // }
                    }
                }else{
                    //some problem
                }
            })
        } else {
            self.noInternetAlert()
            progressViewObj.removeProgressView()
        }
    }
    
    func navigateToReceipt(){
        
        if let objReceipt = UIStoryboard(name: "NewNearMePayment", bundle: nil).instantiateViewController(withIdentifier: "NearByPaymentReceipt") as? NearByPaymentReceipt {
            //                    if let isValidObj = self.nearByViewModel.nearByQRPayModelObj{
            //                        objReceipt.nearByQRPayModelObj = isValidObj
            //                    }
            objReceipt.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(objReceipt, animated: true)
        }
    }
    
    //this function will be called to generate QRCode
      fileprivate func getQR(){
          if appDelegate.checkNetworkAvail() {
              progressViewObj.showProgressView()
              nearByViewModel.getDataForQRScan( finished:{
                  progressViewObj.removeProgressView()
                  //checking if data is available
                  if let isValidObj = self.nearByViewModel.nearByQRModelObj{
                      if let qrData = isValidObj.data{
                          DispatchQueue.main.async {
                              self.setTimer()
                              self.qrData = qrData
                              self.qrImageView.image = UitilityClass.getQRImageNew(stringQR:qrData,withSize: 10,logoName: "NearMeNew")
                          }
                      }else{
                          self.showErrorAlert(errMessage: isValidObj.msg ?? "")
                      }
                  }else{
                      self.showErrorAlert(errMessage: "Some error happen please try again".localized)
                  }
              })
          } else {
              self.noInternetAlert()
              progressViewObj.removeProgressView()
          }
      }
}

// MARK: - Remark Add & Update
extension NearByPaymentAutoPayQRVC : NearByPaymentRemarkDelegate {
    
    @IBAction func enterRemarkButtonAction(_ sender: Any) {
        println_debug("enterRemarkButtonAction")
        self.navigationController?.navigationBar.isUserInteractionEnabled = false
        guard  let vc = UIStoryboard.init(name: "NearMePaymentGeneric", bundle: nil).instantiateViewController(withIdentifier: "NearByPaymentRemarkVC") as? NearByPaymentRemarkVC else {return}
        addChild(vc)
        vc.editTextStr = remarkButton.titleLabel?.text
        vc.view.frame = self.view.bounds
        vc.didMove(toParent: self)
        vc.delegate = self
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.reveal
        vc.view.layer.add(transition, forKey: nil)
        view.addSubview(vc.view)
    }
       
    func returnRemark(remark: String){
        DispatchQueue.main.async {
            self.navigationController?.navigationBar.isUserInteractionEnabled = true
            self.remarkEditButton.isHidden = false
            self.remarkButton.titleLabel?.numberOfLines = 0
            self.remarkButton.titleLabel?.lineBreakMode = .byWordWrapping
            self.remarkButton.titleLabel?.textAlignment = .left
            self.remarkButton.setAttributedTitle(NSMutableAttributedString(string: String(remark)), for: .normal)
        }
    }

    @IBAction func editRemarks(_ sender: Any) {
        self.navigationController?.navigationBar.isUserInteractionEnabled = false
        guard  let vc = UIStoryboard.init(name: "NearMePaymentGeneric", bundle: nil).instantiateViewController(withIdentifier: "NearByPaymentRemarkVC") as? NearByPaymentRemarkVC else {return}
        addChild(vc)
        vc.editTextStr = remarkButton.titleLabel?.text
        vc.view.frame = self.view.bounds
        vc.didMove(toParent: self)
        vc.delegate = self
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.reveal
        vc.view.layer.add(transition, forKey: nil)
        view.addSubview(vc.view)
    }
    
}
