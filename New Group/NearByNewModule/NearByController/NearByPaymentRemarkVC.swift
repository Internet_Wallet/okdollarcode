//
//  NearByPaymentRemarkVC.swift
//  OK
//
//  Created by OK$ on 27/08/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

protocol NearByPaymentRemarkDelegate : class {
    func returnRemark(remark: String)
}

class NearByPaymentRemarkVC: OKBaseController {

    @IBOutlet var viewBG: UIView!
    @IBOutlet var okButton: UIButton!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var textViewRemark: UITextView!
    weak var delegate : NearByPaymentRemarkDelegate?
    var editTextStr : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if editTextStr?.contains(find: "Enter Remark") ?? false {
            textViewRemark.text = "Enter Remark"
            textViewRemark.textColor = UIColor.lightGray
        } else {
            textViewRemark.text = editTextStr
            textViewRemark.textColor = UIColor.black
        }
        
        // Do any additional setup after loading the view.
      let gester = UITapGestureRecognizer(target: self, action: #selector(hideView(_:)))
      viewBG?.addGestureRecognizer(gester)
    }
    
    @objc fileprivate func hideView(_ sender: UITapGestureRecognizer) {
        if textViewRemark.text?.contains(find: "Enter Remark") ?? false {
            self.delegate?.returnRemark(remark: "Enter Remark")
        } else {
            self.delegate?.returnRemark(remark: textViewRemark.text)
        }
        self.view.removeFromSuperview()
    }
       
    @IBAction func onClickOKButton(_ sender: Any) {
        if textViewRemark.text.count >   0 {
            self.delegate?.returnRemark(remark: textViewRemark.text)
            self.view.removeFromSuperview()
        } else {
            DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: "", body: "Please Enter Remark!".localized, img: #imageLiteral(resourceName: "error"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
            }
        }
       }
       
    @IBAction func onClickCancelButton(_ sender: Any) {
        textViewRemark.text = ""
        cancelButton.isHidden = true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NearByPaymentRemarkVC : UITextViewDelegate {
    
  func textViewDidBeginEditing(_ textView: UITextView) {
      if textView.textColor == UIColor.lightGray {
          textView.text = nil
          textView.textColor = UIColor.black
      }
  }
  
  func textViewDidChange(_ textView: UITextView) {
      if textView.text.length > 0 {
          self.cancelButton.isHidden = false
      } else {
          self.cancelButton.isHidden = true
      }
  }
  
  internal func textViewDidEndEditing(_ textView: UITextView) {
//      if textView.text.isEmpty {
//          self.textViewRemark.text = "Enter Remark".localized
//          textView.textColor = UIColor.lightGray
//      }
  }
  
  func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
      if (text == "\n") {
          textView.resignFirstResponder()
      }
      return true
  }
}
