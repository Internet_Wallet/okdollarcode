//
//  NearByPaymentInitialVC.swift
//  OK
//
//  Created by Tushar Lama on 24/08/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit
import WebKit

class NearByPaymentInitialVC: OKBaseController, BioMetricLoginDelegate {

    @IBOutlet weak var webViewData: WKWebView!
    
    var navigation: UINavigationController?

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureBackButton(withTitle: "Near Me Payment".localized)

        // Do any additional setup after loading the view.
        webViewData.loadHTMLString("<html><body><p>Hello!</p></body></html>", baseURL: nil)
    }
    
    //MARK:- Custom Actions
    @IBAction func onClickAgreeButton(_ sender: Any) {
           OKPayment.main.authenticate(screenName: "NearByAgree", delegate: self)
       }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        
        if isSuccessful{
            
            if screen == "NearByAgree" {
                if let objAutoPay = UIStoryboard(name: "NewNearMePayment", bundle: nil).instantiateViewController(withIdentifier: "NearByPaymentAutoPayQRVC") as? NearByPaymentAutoPayQRVC {
                    objAutoPay.modalPresentationStyle = .fullScreen
                    self.navigationController?.pushViewController(objAutoPay, animated: true)
                }
            }
        }
        
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
