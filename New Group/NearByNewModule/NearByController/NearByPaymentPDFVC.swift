//
//  NearByPaymentPDFVC.swift
//  OK
//
//  Created by OK$ on 04/09/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

extension NearByPaymentReceipt {
    
    
    func createPDF() {
        //Image Icon
        
        yAxis = 30
        let imgAppIcon = UIImageView(frame: CGRect(x: (self.view.frame.width/2) - 40, y: yAxis, width: 80, height: 80))
        imgAppIcon.image = UIImage(named: "appIcon_Ok")
        self.pdfview.addSubview(imgAppIcon)
        //Y= 120
        yAxis = 120
        let lblPaymentReceipt = UILabel(frame: CGRect(x: 0, y: yAxis, width: self.view.frame.width, height: 40))
        lblPaymentReceipt.text = "PAYMENT RECEIPT".localized
        lblPaymentReceipt.backgroundColor = #colorLiteral(red: 0.1096028909, green: 0.2039180398, blue: 0.5867625475, alpha: 1)
        lblPaymentReceipt.textAlignment = .center
        lblPaymentReceipt.textColor = .white
        lblPaymentReceipt.font = UIFont.systemFont(ofSize: 16)
        self.pdfview.addSubview(lblPaymentReceipt)
        //Y= 160
        yAxis = 160
        let dateView = UILabel(frame: CGRect(x: 0, y: yAxis, width: self.view.frame.width, height: 50))
        dateView.backgroundColor = .clear
        
        
        let imgDate = UIImageView(frame: CGRect(x: 10, y: 15, width: 20, height: 20))
        imgDate.image = UIImage(named: "merchantCalendar")
        dateView.addSubview(imgDate)
        
        let lblDate = UILabel()
        lblDate.text = self.getDate()
        lblDate.font = UIFont.systemFont(ofSize: 12)
        lblDate.textAlignment = .left
        lblDate.sizeToFit()
        lblDate.frame = CGRect(x: 35, y: 0, width: lblDate.frame.width, height: 50)
        dateView.addSubview(lblDate)
        
        let lblCountry = UILabel()
        lblCountry.text = "Myanmar"
        lblCountry.font = UIFont.systemFont(ofSize: 12)
        lblCountry.textAlignment = .center
        lblCountry.sizeToFit()
        lblCountry.frame = CGRect(x: (self.view.frame.width/2) - 25, y: 0, width: lblCountry.frame.width,height: 50)
        //dateView.addSubview(lblCountry)
        
      
        let lblTime = UILabel()
        lblTime.text = self.getTime()
        lblTime.font = UIFont.systemFont(ofSize: 12)
        lblTime.textAlignment = .left
        lblTime.sizeToFit()
        lblTime.frame = CGRect(x: self.view.frame.width - 130, y: 0, width: lblTime.frame.width, height: 50)
        dateView.addSubview(lblTime)
        
        let imgTime = UIImageView(frame: CGRect(x: lblTime.frame.origin.x - 30, y: 15, width: 20, height: 20))
        imgTime.image = UIImage(named: "merchantClock")
        dateView.addSubview(imgTime)
              
        
        //Y= 210
        yAxis = 210
        let upiView = UILabel(frame: CGRect(x: 10, y: yAxis, width: self.view.frame.width - 20, height: 60))
        upiView.backgroundColor = .white
        
        let lblUPI = UILabel()
        lblUPI.text = "Near Me Payment".localized
        lblUPI.font = UIFont.systemFont(ofSize: 13)
        lblUPI.textAlignment = .center
        lblUPI.sizeToFit()
        lblUPI.frame = CGRect(x: (self.view.frame.width/2) - (lblUPI.frame.width / 2), y: 0, width: lblUPI.frame.width, height: 60)
        upiView.addSubview(lblUPI)
        
        let imgUPI = UIImageView(frame: CGRect(x: lblUPI.frame.origin.x - 50, y: 10, width: 40, height: 40))
        imgUPI.image = UIImage(named: "nearMePayment")
        imgUPI.layer.cornerRadius = 20
        //imgUPI.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        imgUPI.layer.borderWidth = 1.0
        imgUPI.layer.masksToBounds = true
        upiView.addSubview(imgUPI)
        
        let lblUPISep = UILabel()
        lblUPISep.text = ""
        lblUPISep.frame = CGRect(x: 0, y: 59, width: upiView.frame.width, height: 1)
        lblUPISep.backgroundColor = #colorLiteral(red: 0.8273780942, green: 0.8274977207, blue: 0.8273518085, alpha: 1)
        upiView.addSubview(lblUPISep)
        //Y = 270
        yAxis = 270
        
        self.pdfview.addSubview(dateView)
        self.pdfview.addSubview(upiView)
  
        if let list = listTitleValue {
            for (index, _) in list.enumerated() {
                
                let containerView = UIView(frame: CGRect(x: 10, y: yAxis, width: self.view.frame.width - 20, height: 0))
                let dicTitle = list[index]
                let title = dicTitle["title"]
                let lblTitle = UILabel()
                lblTitle.text = title  ?? ""
                lblTitle.textAlignment = .left
                lblTitle.font = UIFont.systemFont(ofSize: 15)
                lblTitle.numberOfLines = 0
                lblTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
                lblTitle.frame = CGRect(x: 10, y: 10, width: 140, height: heightForViewUPI(text: title ?? "", font: UIFont.systemFont(ofSize: 15), width: 140))
                containerView.addSubview(lblTitle)
                
                let lblColon = UILabel()
                lblColon.text = ":"
                lblColon.textAlignment = .center
                lblColon.font = UIFont.systemFont(ofSize: 15)
                lblColon.numberOfLines = 0
                lblColon.frame = CGRect(x: 150, y: 8, width: 10, height: heightForViewUPI(text:  ":", font: UIFont.systemFont(ofSize: 15), width: 10))
                containerView.addSubview(lblColon)
                
                let dicValue = list[index]
                let value = dicValue["value"]
                let lblValue = UILabel()
                lblValue.text = value ?? ""
                lblValue.textAlignment = .left
                lblValue.font = UIFont.systemFont(ofSize: 15)
                lblValue.numberOfLines = 0
                lblValue.lineBreakMode = NSLineBreakMode.byWordWrapping
                lblValue.frame = CGRect(x: 160, y: 10, width: containerView.frame.width - 165, height: heightForViewUPI(text: value ?? "", font: UIFont.systemFont(ofSize: 15), width: containerView.frame.width - 160))
                containerView.addSubview(lblValue)
                
                if (lblTitle.frame.height == lblValue.frame.height) || (lblTitle.frame.height > lblValue.frame.height) {
                    containerView.frame = CGRect(x: 10, y: yAxis, width: self.view.frame.width - 20, height: lblTitle.frame.height + 20)
                }else {
                    containerView.frame = CGRect(x: 10, y: yAxis, width: self.view.frame.width - 20, height: lblValue.frame.height + 20)
                }
                
                let lblSap = UILabel()
                lblSap.text = ""
                lblSap.backgroundColor = #colorLiteral(red: 0.8273780942, green: 0.8274977207, blue: 0.8273518085, alpha: 1)
                lblSap.frame = CGRect(x: 0, y: containerView.frame.height - 1, width: containerView.frame.width, height: 1)
                containerView.addSubview(lblSap)
                
                containerView.backgroundColor = #colorLiteral(red: 0.9638236165, green: 0.9687631726, blue: 0.9728998542, alpha: 1)
                pdfview.addSubview(containerView)
                
                print("Height: \(yAxis)")
                yAxis = yAxis + containerView.frame.height
                if title == "Remark" {
                    let imgAppIcon = UIImageView(frame: CGRect(x: 0, y: yAxis + 30, width: self.view.frame.width, height: 30))
                    imgAppIcon.image = UIImage(named: "footerSMB")
                    self.pdfview.addSubview(imgAppIcon)
                    break
                }
            }
        }
        pdfview.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: yAxis + 90)
        self.bgView.addSubview(pdfview)
        
    }
    
    func heightForViewUPI(text:String, font:UIFont, width:CGFloat) -> CGFloat {
         let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
         label.numberOfLines = 0
         label.lineBreakMode = NSLineBreakMode.byWordWrapping
         label.font = font
         label.text = text
         label.sizeToFit()
         return label.frame.height
     }
    
}
