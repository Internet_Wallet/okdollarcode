//
//  NearByPaymentReceipt.swift
//  OK
//
//  Created by OK$ on 25/08/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class NearByPaymentReceipt: OKBaseController {
    
    @IBOutlet var tvReciept: UITableView!
    @IBOutlet var lblReceipt: UILabel!
    @IBOutlet var btnShare: UIButton!
    @IBOutlet var btnHome: UIButton!
    @IBOutlet var bgView:UIView!
    var pdfview = UIView()
    var listTitleValue : [Dictionary<String,String>]?
    var yAxis : CGFloat = 0.0
    
    //MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isUserInteractionEnabled  = true
       }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        println_debug(listTitleValue)
        self.navigationItem.hidesBackButton = true
        self.navigationItem.title = "Receipt"
        //Add History Icon Navigation Right Bar
        setNoteButton()
        
        pdfview.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30)
        self.pdfview.backgroundColor = #colorLiteral(red: 0.9285323024, green: 0.9334669709, blue: 0.9376094937, alpha: 1)
        self.bgView.backgroundColor = .white
        //static Data
        listTitleValue = [
            ["title" : "Payer OK$" ,"value" : "\(UserModel.shared.name) \(UserModel.shared.phoneNumber)"],
            ["title" : "Merchant" ,"value" : "Pizza Company Junction Square"],
            ["title" : "Merchant ID" ,"value" : "45312515487628"],
            ["title" : "Near Me Txn ID" ,"value" :  "242342408"], //self.values?[2] ??
            ["title" : "OK$ Txn ID" ,"value" : "6549393"],
            ["title" : "Amount" ,"value" : "95,000.00 MMK"],
            ["title" : "Remark" ,"value" : "Testing Value"],
            ["title" : "Ok$ Balance:" ,"value" : "\(self.walletAmount())"]
        ]
        //Create PDF
        self.createPDF()
         DispatchQueue.main.async {
            self.saveToGallery()
        }
    }
    
    func setNoteButton() {
           let rightBarButton   = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.done, target: self, action: #selector(enterNoteButtonAction))
        rightBarButton.image = #imageLiteral(resourceName: "star1").withRenderingMode(.alwaysOriginal)
           self.navigationItem.rightBarButtonItem = rightBarButton
       }
       
    @IBAction func onClickShareActin(_: UIButton) {
        DispatchQueue.main.async {

        alertViewObj.wrapAlert(title:"", body: "Do you want to share Receipt?".localized, img: UIImage(named: "save_gray"))
        
        alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
        })
        
        alertViewObj.addAction(title: "Share".localized, style: .target , action: {
            self.shareQRWithDetail()
        })
            alertViewObj.showAlert(controller: self)
        }
     
    }
    
    @IBAction func onClickHomeAction(_ sender: UIButton) {
        println_debug("enterRemarkButtonAction")
        self.navigationController?.navigationBar.isUserInteractionEnabled = false
        guard  let vc = UIStoryboard.init(name: "NearMePaymentGeneric", bundle: nil).instantiateViewController(withIdentifier: "NearByPaymentFeedbackVC") as? NearByPaymentFeedbackVC else {return}
        vc.delegate = self
        addChild(vc)
        vc.view.frame = self.view.bounds
        vc.didMove(toParent: self)
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.reveal
        vc.view.layer.add(transition, forKey: nil)
        view.addSubview(vc.view)
    }
    
    func saveToGallery() {
       let image = captureScreen(view: self.bgView)
       UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
           if let error = error {
            DispatchQueue.main.async {

               alertViewObj.wrapAlert(title: "Error".localized, body: error.localizedDescription, img: nil)
               alertViewObj.addAction(title: "OK".localized, style: .target , action: {
               })
               alertViewObj.showAlert(controller: self)
            }
           } else {
               println_debug("Your image has been saved to your photos.")
           }
       }
    
    func shareQRWithDetail() {
        let image = captureScreen(view: self.bgView)
        let imageToShare = [image]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        DispatchQueue.main.async {
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    func getDate() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateFormat = "EEE, dd-MMM-yyyy"
        return formatter.string(from: date)
    }
    
    func getTime() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateFormat = "HH:mm:ss"
        return formatter.string(from: date)
    }
    
}

// MARK: - Remark Add & Update
extension NearByPaymentReceipt : NearByPaymentFeedbackDelegate {
 
    func returnFeedback(feedback: String) {
        println_debug("returnFeedback submitted")
        self.navigationController?.navigationBar.isUserInteractionEnabled = true
        if UserModel.shared.agentType == .user {
            if NSString(string: UserLogin.shared.walletBal).floatValue < 200000.0{
                self.addMoney()
            }
        } else if UserModel.shared.agentType == .merchant {
            if NSString(string: UserLogin.shared.walletBal).floatValue < 500000.0{
                self.addMoney()
            }
        } else if UserModel.shared.agentType == .agent {
            if NSString(string: UserLogin.shared.walletBal).floatValue < 1000000.0{
                self.addMoney()
            }
        }
    }
    
     // wallet balance if zero
    func addMoney() {
        alertViewObj.wrapAlert(title: nil, body: "Your OK$ balance is low. Please recharge.".localized, img: #imageLiteral(resourceName: "error"))
        alertViewObj.addAction(title: "OK".localized, style: .target, action: {
            self.navigationController?.popToRootViewController(animated: true)
        })
        alertViewObj.addAction(title: "CASH IN".localized, style: .target, action: {
            
            if let objCashIn = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                objCashIn.modalPresentationStyle = .fullScreen
                self.navigationController?.present(objCashIn, animated: true, completion: nil)
            }
        })
        alertViewObj.showAlert()
    }
}

//MARK: Note Delegate
extension NearByPaymentReceipt : NearByPaymentNoteDelegate {
    
    @IBAction func enterNoteButtonAction(_ sender: Any) {
        println_debug("enterNoteButtonAction")
        self.navigationController?.navigationBar.isUserInteractionEnabled = false
        guard  let vc = UIStoryboard.init(name: "NearMePaymentGeneric", bundle: nil).instantiateViewController(withIdentifier: "NearByPaymentNoteVC") as? NearByPaymentNoteVC else {return}
        vc.strStatusScreen = "Receipt"
        vc.delegate = self
        addChild(vc)
        vc.view.frame = self.view.bounds
        vc.didMove(toParent: self)
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.reveal
        vc.view.layer.add(transition, forKey: nil)
        view.addSubview(vc.view)
    }
    
    func returnNote(note: String){
        println_debug(note)
        self.navigationController?.navigationBar.isUserInteractionEnabled = true
        if note == "mark" {
            let rightBarButton   = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.done, target: self, action: #selector(enterNoteButtonAction))
            rightBarButton.image = #imageLiteral(resourceName: "star3").withRenderingMode(.alwaysOriginal)
            self.navigationItem.rightBarButtonItem = rightBarButton
        } else if note == "unmark" {
            let rightBarButton   = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.done, target: self, action: #selector(enterNoteButtonAction))
            rightBarButton.image = #imageLiteral(resourceName: "star2").withRenderingMode(.alwaysOriginal)
            self.navigationItem.rightBarButtonItem = rightBarButton
        } else {
            let rightBarButton   = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.done, target: self, action: #selector(enterNoteButtonAction))
            rightBarButton.image = #imageLiteral(resourceName: "star1").withRenderingMode(.alwaysOriginal)
            self.navigationItem.rightBarButtonItem = rightBarButton
        }
    }
}


extension NearByPaymentReceipt: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        }else if section == 2 {
            return 1
        }else {
            return (listTitleValue?.count ?? 0) - 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NBPRecieptHeader", for: indexPath) as! NBPRecieptHeader
                cell.lblPaymentSuccess.text = "Payment Successful".localized
                cell.lblAmount.text = "905,000,00"
                cell.lblDate.text = self.getDate()
                cell.lblCountry.text = ""
                cell.lblTime.text = self.getTime()
                cell.selectionStyle = .none
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NBPPayementTitle", for: indexPath) as! NBPPayementTitle
                cell.lblNBP.text = "Near Me Payment"
                cell.selectionStyle = .none
                return cell
            }
        }else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NBPRemarkFooter", for: indexPath) as! NBPRemarkFooter
            let dic = self.listTitleValue?[(listTitleValue?.count ?? 0) - 1]
            cell.btnOKBalance.setTitle("OK$ Balance: \(dic?["value"] ?? "") MMK", for: .normal)
            cell.selectionStyle = .none
            return cell
        }else {
            if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NBPMerchant", for: indexPath) as! NBPMerchant
                let dic = self.listTitleValue?[indexPath.row]
                cell.lblTitle.text = dic?["title"]
                cell.lblValue.text = dic?["value"]
                cell.selectionStyle = .none
                return cell
            }else if indexPath.row == (listTitleValue?.count ?? 0) - 2 {
                // Remark
                let cell = tableView.dequeueReusableCell(withIdentifier: "NBPRemark", for: indexPath) as! NBPRemark
                let dic = self.listTitleValue?[indexPath.row]
                cell.lblTitle.text = dic?["title"]
                cell.lblValue.text = dic?["value"]
                cell.selectionStyle = .none
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NBPTitleValue", for: indexPath) as! NBPTitleValue
                let dic = self.listTitleValue?[indexPath.row]
                cell.lblTitle.text = dic?["title"]
                cell.lblValue.text = dic?["value"]
                cell.selectionStyle = .none
                return cell
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                return 120
            }else {
                return 60
            }
        }else if indexPath.section == 2 {
            return 50
        }else {
            return UITableView.automaticDimension
        }
    }
    
}

class NBPRecieptHeader: UITableViewCell {
    @IBOutlet var lblPaymentSuccess: UILabel!
    @IBOutlet var lblAmount: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lblCountry: UILabel!
    @IBOutlet var lblSep: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lblPaymentSuccess.font = UIFont(name: appFont, size: 18)
        lblAmount.font = UIFont(name: appFont, size: 16)
        lblDate.font = UIFont(name: appFont, size: 12)
        lblTime.font = UIFont(name: appFont, size: 12)
        lblCountry.font = UIFont(name: appFont, size: 14)
    }
    
}

class NBPPayementTitle: UITableViewCell {
    @IBOutlet var lblNBP: UILabel!
    @IBOutlet var lblSep: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lblNBP.font = UIFont(name: appFont, size: 16)
    }
}

class NBPTitleValue: UITableViewCell {
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblValue: UILabel!
    @IBOutlet var lblSep: UILabel!
    @IBOutlet var lblColon: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lblTitle.font = UIFont(name: appFont, size: 15)
        lblValue.font = UIFont(name: appFont, size: 15)
        lblColon.font = UIFont(name: appFont, size: 15)
    }
}

class NBPMerchant: UITableViewCell {
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblValue: UILabel!
    @IBOutlet var lblSep: UILabel!
    @IBOutlet var lblColon: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lblTitle.font = UIFont(name: appFont, size: 15)
        lblValue.font = UIFont(name: appFont, size: 15)
        lblColon.font = UIFont(name: appFont, size: 15)
    }
}

class NBPRemark: UITableViewCell {
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblValue: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lblTitle.font = UIFont(name: appFont, size: 15)
        lblValue.font = UIFont(name: appFont, size: 15)
    }
}

class NBPRemarkFooter: UITableViewCell {
    @IBOutlet var btnOKBalance: UIButton!
    @IBOutlet var lblSep: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        btnOKBalance.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
    }
}
