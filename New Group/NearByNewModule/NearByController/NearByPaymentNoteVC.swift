//
//  NearByPaymentNoteVC.swift
//  OK
//
//  Created by OK$ on 28/08/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

protocol NearByPaymentNoteDelegate : class {
    func returnNote(note: String)
    func returnHistoryID(note: String, historyID: String)
}

extension NearByPaymentNoteDelegate {
    func returnNote(note: String){
    }
    func returnHistoryID(note: String, historyID: String){
    }
}

class NearByPaymentNoteVC: OKBaseController {
    
    @IBOutlet var viewBG: UIView!
    @IBOutlet var unmarkButton: UIButton!
    @IBOutlet var markButton: UIButton!
    
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var textViewRemark: UITextView!
    weak var delegate : NearByPaymentNoteDelegate?
    var editTextStr : String?
    var strHistoryID : String?
    var strStatusScreen : String?

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if textViewRemark.text?.contains(find: "Add Note") ?? false {
            textViewRemark.textColor = UIColor.lightGray
                   textViewRemark.text = "Add Note"
               } else {
                   textViewRemark.text = editTextStr
                   textViewRemark.textColor = UIColor.black
               }
        let gester = UITapGestureRecognizer(target: self, action: #selector(hideView(_:)))
        viewBG?.addGestureRecognizer(gester)
    }
    
    @objc fileprivate func hideView(_ sender: UITapGestureRecognizer) {
           self.delegate?.returnNote(note: "")
           self.view.removeFromSuperview()
       }
    
    //MARK:- Custom Button Action
    @IBAction func onClickCancelButton(_ sender: Any) {
        textViewRemark.text = ""
        cancelButton.isHidden = true
        markButton.setImage(#imageLiteral(resourceName: "receiptStarUnfilled"), for: .normal)
    }
       
    @IBAction func unMarkButtonAction(_ sender: Any) {
        if strStatusScreen == "Receipt" {
        self.delegate?.returnNote(note: "unmark")
        } else {
            self.delegate?.returnHistoryID(note: "unmark", historyID: strHistoryID ?? "")
        }
        self.view.removeFromSuperview()
    }
    
    @IBAction func markButtonAction(_ sender: Any) {
        markButton.setImage(#imageLiteral(resourceName: "star6"), for: .normal)

        if strStatusScreen == "Receipt" {
        self.delegate?.returnNote(note: "mark")
        } else {
            self.delegate?.returnHistoryID(note: "mark", historyID: strHistoryID ?? "")
        }
        self.view.removeFromSuperview()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension NearByPaymentNoteVC : UITextViewDelegate {
    
  func textViewDidBeginEditing(_ textView: UITextView) {
      if textView.textColor == UIColor.lightGray {
          textView.text = nil
          textView.textColor = UIColor.black
      }
  }
  
  func textViewDidChange(_ textView: UITextView) {
      if textView.text.length > 0 {
          self.cancelButton.isHidden = false
            markButton.setImage(#imageLiteral(resourceName: "star6"), for: .normal)
      } else {
          self.cancelButton.isHidden = true
            markButton.setImage(#imageLiteral(resourceName: "receiptStarUnfilled"), for: .normal)
      }
  }
  
  func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
      if (text == "\n") {
          textView.resignFirstResponder()
      }
      return true
  }
}
