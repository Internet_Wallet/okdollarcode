//
//  NearByPaymentFeedbackVC.swift
//  OK
//
//  Created by OK$ on 31/08/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

protocol NearByPaymentFeedbackDelegate : class {
    func returnFeedback(feedback: String)
}

class NearByPaymentFeedbackVC: OKBaseController {

    @IBOutlet var viewBG: UIView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var okButton: UIButton!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var disappointedButton: UIButton!
    @IBOutlet var confusedButton: UIButton!
    @IBOutlet var smilyFaceButton: UIButton!
    @IBOutlet weak var okButtonConstraintHeight: NSLayoutConstraint!
    weak var delegate : NearByPaymentFeedbackDelegate?

    @IBOutlet var feedbackView: UIView!
    @IBOutlet var buttonsView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        feedbackView.layer.cornerRadius = 10.0
        
        okButtonConstraintHeight.constant = 0
        buttonsView.backgroundColor = UIColor.white
        let gester = UITapGestureRecognizer(target: self, action: #selector(hideView(_:)))
        viewBG?.addGestureRecognizer(gester)
    }
    
    @objc fileprivate func hideView(_ sender: UITapGestureRecognizer) {
        self.delegate?.returnFeedback(feedback: "Cancel")
        self.view.removeFromSuperview()
    }
    
    @IBAction func faceButtonAction(_ sender: Any) {
        okButtonConstraintHeight.constant = 175
        buttonsView.backgroundColor = UIColor.lightGray
        let tagButton = ((sender as AnyObject).tag ?? 0)
        
        if tagButton == 0 {
            println_debug("disappointed")
            disappointedButton.setImage(#imageLiteral(resourceName: "emoji6").withRenderingMode(.alwaysOriginal), for: .normal)
            confusedButton.setImage(#imageLiteral(resourceName: "emoji1").withRenderingMode(.alwaysOriginal), for: .normal)
            smilyFaceButton.setImage(#imageLiteral(resourceName: "emoji2").withRenderingMode(.alwaysOriginal), for: .normal)
        } else if tagButton == 1 {
            println_debug("angry face")
            disappointedButton.setImage(#imageLiteral(resourceName: "emoji3").withRenderingMode(.alwaysOriginal), for: .normal)
            confusedButton.setImage(#imageLiteral(resourceName: "emoji4").withRenderingMode(.alwaysOriginal), for: .normal)
            smilyFaceButton.setImage(#imageLiteral(resourceName: "emoji2").withRenderingMode(.alwaysOriginal), for: .normal)
        } else if tagButton == 2 {
            println_debug("smily face")
            disappointedButton.setImage(#imageLiteral(resourceName: "emoji3").withRenderingMode(.alwaysOriginal), for: .normal)
            confusedButton.setImage(#imageLiteral(resourceName: "emoji1").withRenderingMode(.alwaysOriginal), for: .normal)
            smilyFaceButton.setImage(#imageLiteral(resourceName: "emoji5").withRenderingMode(.alwaysOriginal), for: .normal)
        }
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.delegate?.returnFeedback(feedback: "Cancel")
        self.view.removeFromSuperview()
    }
    
    @IBAction func okButtonAction(_ sender: Any) {
        self.delegate?.returnFeedback(feedback: "OK")
        self.view.removeFromSuperview()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
