//
//  NearByPaymentHistoryVC.swift
//  OK
//
//  Created by OK$ on 26/08/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class NearByPaymentHistoryVC: OKBaseController {

    @IBOutlet weak var tblHistory : UITableView!
    var arrHistoryList : [NBPHistoryModel]?
    var arrMainHistoryList : [NBPHistoryModel]?

    @IBOutlet weak var viewNoRecord : UIView!

    @IBOutlet weak var btnSort: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var searchBarHistory: UISearchBar!
    @IBOutlet weak var lblTotalRecords: UILabel!
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        searchBarHistory.isHidden = true
        lblTotalRecords.isHidden = false
        lblTotalRecords.text = "Total Records - 2"
        self.configureBackButton(withTitle: "History".localized)
        
        //Search & Sort Button color
        btnSearch.layer.cornerRadius = 8.0
        btnSearch.layer.masksToBounds = true
        btnSearch.layer.borderColor = kDarkBlueColor.cgColor
        btnSearch.layer.borderWidth = 1.0
        btnSort.layer.cornerRadius = 8.0
        btnSort.layer.masksToBounds = true
        btnSort.layer.borderColor = kDarkBlueColor.cgColor
        btnSort.layer.borderWidth = 1.0
    }
    
    @IBAction func btnSearchAction(_ sender: Any) {
        
        if searchBarHistory.isHidden == true {
            searchBarHistory.isHidden = false
            lblTotalRecords.isHidden = true
            btnSearch.setTitle("Cancel", for: .normal)
            btnSearch.setImage(nil, for: .normal)

        } else {
            searchBarHistory.isHidden = true
            lblTotalRecords.isHidden = false
            btnSearch.setTitle("", for: .normal)
            btnSearch.setImage(#imageLiteral(resourceName: "search_blue"), for: .normal)
        }
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func navigateToFilter(_ sender: Any) {
           if let objReceipt = UIStoryboard(name: "NewNearMePayment", bundle: nil).instantiateViewController(withIdentifier: "NearByPaymentFilterVC") as? NearByPaymentFilterVC {
             
               objReceipt.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(objReceipt, animated: true)
           }
       }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NearByPaymentHistoryVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 108
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if arrMainHistoryList?.count == 0{
            tableView.backgroundView = viewNoRecord
        }else{
            tableView.backgroundView = nil
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2//arrHistoryList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: "NBPHistoryCell"), for: indexPath) as? NBPHistoryCell
        //cell?.wrapCellData(modelData: arrMainHistoryList[indexPath.row])
        cell?.btnMarkUnMark.tag = indexPath.row
        //cell?.btnMarkUnMark.addTarget(self, action: #selector(self.btnMarkUnMarkAction(_:)), for: .touchUpInside)
        cell?.selectionStyle = .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let objReceipt = UIStoryboard(name: "NewNearMePayment", bundle: nil).instantiateViewController(withIdentifier: "NBPBillPaymentReceiptVC") as? NBPBillPaymentReceiptVC {
            objReceipt.modalPresentationStyle = .fullScreen
            //self.navigationController?.pushViewController(objReceipt, animated: true)
        }
    }
}

extension NearByPaymentHistoryVC : NearByPaymentNoteDelegate {
    
    @IBAction func btnMarkUnMarkAction(_ sender: Any) {
        println_debug("btnMarkUnMarkAction")
        guard  let vc = UIStoryboard.init(name: "NearMePaymentGeneric", bundle: nil).instantiateViewController(withIdentifier: "NearByPaymentNoteVC") as? NearByPaymentNoteVC else {return}
        vc.strHistoryID = "\((sender as AnyObject).tag ?? 0)"
        vc.strStatusScreen = "History"
        addChild(vc)
        vc.view.frame = self.view.bounds
        vc.didMove(toParent: self)
        vc.delegate = self
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.reveal
        vc.view.layer.add(transition, forKey: nil)
        view.addSubview(vc.view)
    }
    
    func returnHistoryID(note: String, historyID: String){
        println_debug(note)
        if note == "mark" {
            //starButton.setImage(#imageLiteral(resourceName: "active").withRenderingMode(.alwaysOriginal), for: .normal)
        } else {
            //starButton.setImage(#imageLiteral(resourceName: "in_active").withRenderingMode(.alwaysOriginal), for: .normal)
        }
        println_debug("returnHistoryID = \(historyID) note = \(note)")
        //tblHistory.reloadData()
    }
}

// MARK: -UISearchBarDelegate
extension NearByPaymentHistoryVC: UISearchBarDelegate {
    
    private func applySearch(with searchText: String) {
        var tempRecords = arrHistoryList
//        if searchText.count > 0 {
//            tempRecords = tempRecords.filter({($0.transactionCode.lowercased().contains(find: searchText.lowercased())) || (String($0.taxAmount).lowercased().contains(find: searchText.lowercased()))})
//        }
//        arrHistoryList = tempRecords.sorted(by: { $0.transactionTime ?? "" > $1.transactionTime ?? ""})
        self.tblHistory.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
//            uiButton.setTitle("Cancel".localized, for:.normal)
//        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if range.location == 0 && text == " " { return false }
        if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
            let updatedText = searchText.replacingCharacters(in: textRange, with: text)
            if updatedText.containsEmoji {
                return false
            }
            if searchBar.text?.last == " " && text == " " {
                return false
            }
            if updatedText.count > 50 {
                return false
            }
            
            applySearch(with: updatedText)
        }
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            applySearch(with: "")
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        //setting it to initial
        arrMainHistoryList = arrHistoryList
        self.tblHistory.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.applySearch(with: searchBar.text ?? "")
        searchBar.endEditing(true)
    }
}
