//
//  NBPHistoryCell.swift
//  OK
//
//  Created by OK$ on 29/08/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class NBPHistoryCell: UITableViewCell {

    @IBOutlet var viewBorder: UIView!
    @IBOutlet var lblDDMMYY: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblCompanyName: UILabel!
    @IBOutlet var imgViewPaidUnPaid: UIImageView!
    @IBOutlet var btnMarkUnMark: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewBorder.layer.borderWidth = 1.0
        viewBorder.layer.cornerRadius = 10.0
        viewBorder.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func wrapCellData(modelData: NBPHistoryModel) {
     //print("wrapCellData-------")
        lblDDMMYY.text = ""
        lblPrice.text = ""
        lblCompanyName.text = ""
    }
    
}
