//
//  Extension.swift
//  OK
//
//  Created by iMac on 10/2/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//




import Foundation
import UIKit


extension UIView{
    func giveBorderShadowCornerRadiusToView(){
        self.layer.cornerRadius = 10.0
        self.clipsToBounds = true
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.yellow.cgColor
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 50.0
    }
}


extension UILabel {
    func startBlink() {
        UIView.animate(withDuration: 0.5,
              delay:0.0,
              options:[.allowUserInteraction, .curveEaseInOut, .autoreverse, .repeat],
              animations: { self.alpha = 0 },
              completion: nil)
    }

    func stopBlink() {
        layer.removeAllAnimations()
        alpha = 1
    }
}
