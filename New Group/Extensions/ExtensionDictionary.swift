//
//  ExtensionDictionary.swift
//  OK
//
//  Created by Ashish on 1/29/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

extension Dictionary {
    func safeValueForKey(_ key: Key) -> Value? {
        if self.contains(where: { $0.key == key }) {
            if let _ = self[key] as? NSNull {
                return nil
            }
            return self[key]
        } else {
            return nil
        }
    }
}



extension Dictionary {
    func jsonString() -> String {
        var tempDic = self as! Dictionary<String,Any>
        var keys = Array<String>()
        for key in tempDic.keys {
            keys.append(key)
        }
        keys.sort { $0 < $1 }
        var signString = "{"
        var arr: Array<String> = []
        for key in keys {
            let value = tempDic[key]
            if let value = value as? Dictionary<String,Any> {
                //arr.append("\"\(key)\":\"\(value.zx_sortJsonString())\"")不需要引号
                arr.append("\"\(key)\":\(value.jsonString())")
            }else if let value = value as? Array<Any> {
                //arr.append("\"\(key)\":\"\(value.zx_sortJsonString())\"")
                arr.append("\"\(key)\":\(value.jsonString())")
            }else{
                arr.append("\"\(key)\":\"\(tempDic[key]!)\"")
            }
        }
        signString += arr.joined(separator: ",")
        signString += "}"
        return signString
    }
}

extension Array {
    func  jsonString() -> String {
        let array = self
        var arr: Array<String> = []
        var signString = "["
        for value in array {
            if let value = value as? Dictionary<String,Any> {
                arr.append(value.jsonString())
            }else if let value = value as? Array<Any> {
                arr.append(value.jsonString())
            }else{
                arr.append("\"\(value)\"")
            }
        }
        arr.sort { $0 < $1 }
        signString += arr.joined(separator: ",")
        signString += "]"
        return signString
    }
    
    func unique<T:Hashable>(map: ((Element) -> (T)))  -> [Element] {
        var set = Set<T>() //the unique list kept in a Set for fast retrieval
        var arrayOrdered = [Element]() //keeping the unique list of elements but ordered
        for value in self {
            if !set.contains(map(value)) {
                set.insert(map(value))
                arrayOrdered.append(value)
            }
        }
        return arrayOrdered
    }
    
}
