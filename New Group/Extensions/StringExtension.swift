//
//  StringExtension.swift
//  OKSwiftMigration
//
//  Created by Uma Rajendran on 10/14/17.
//  Copyright © 2017 Uma Rajendran. All rights reserved.
//

import Foundation

fileprivate let allowedCharset = CharacterSet
    .decimalDigits
    .union(CharacterSet(charactersIn: "+"))

fileprivate let allowedCharsetDigits = CharacterSet
    .decimalDigits

fileprivate let allowedCharactersAmount = CharacterSet.decimalDigits.union(CharacterSet(charactersIn: ".").union(CharacterSet(charactersIn: ",")))
extension String {
    
    //Tushar
    //call to check the string is integer or number
    var isNumeric : Bool {
        return NumberFormatter().number(from: self) != nil
    }
    
    var digitsPhone: String {
        let str = String(self.unicodeScalars.filter(allowedCharset.contains))
        return str
    }
    
    var numbersOnly : String {
        let str = String(self.unicodeScalars.filter(allowedCharsetDigits.contains))
        return str
    }
    
    var amountOnly : String {
        let amount = String(self.unicodeScalars.filter(allowedCharactersAmount.contains))
        return amount
    }
    
    func deletingPrefix(_ prefix: String) -> String {
        guard self.hasPrefix(prefix) else { return self }
        return String(self.dropFirst(prefix.count))
    }
    
    func deletingZeroPrefixes() -> String {
        let arrayStr = Array(self)
        if arrayStr.count > 0 {
            var str = self
            for item in arrayStr {
                if item == "0" {
                    str = String(str.dropFirst())
                } else {
                    break
                }
            }
            return str
        } else {
            return self
        }
    }
    
    func capitalizedFirst() -> String {
        if self.count > 0 {
            let first = self[self.startIndex ..< self.index(startIndex, offsetBy: 1)]
            let rest = self[self.index(startIndex, offsetBy: 1) ..< self.endIndex]
            return first.uppercased() + rest.lowercased()
        } else {
            return ""
        }
    }
    
    func substring(from index: Int) -> String
    {
        if (index < 0 || index > self.count)
        {
            println_debug("index \(index) out of bounds")
            return ""
        }
        let fromIndex = self.index(self.startIndex, offsetBy: index)
        return String(self[fromIndex...])
    }
    
    func alignWithCurrencyFormat() -> String {
        if CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: self)){
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = NumberFormatter.Style.decimal
            if let formattedNumber = numberFormatter.number(from: self) {
                return numberFormatter.string(from: formattedNumber)!
            }else {
                return self
            }
            
        } else {
            return self
        }
    }
    
    func hmac_SHA1(key: String) -> String {
        let str = self.cString(using: String.Encoding.utf8)
        let strLen = Int(self.lengthOfBytes(using: String.Encoding.utf8))
        
        let digestLen = CC_SHA1_DIGEST_LENGTH
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: Int(digestLen))
        
        let keyStr = key.cString(using: String.Encoding.utf8)
        let keyLen = Int(key.lengthOfBytes(using: String.Encoding.utf8))
        
        CCHmac(CCHmacAlgorithm(kCCHmacAlgSHA1), keyStr!, keyLen, str!, strLen, result)
        
        let digest = stringFromResult(result: result, length: Int(digestLen))
        
        result.deallocate() //result.deallocate(capacity: Int(digestLen))
        
        return digest
    }
    
    private func stringFromResult(result: UnsafeMutablePointer<CUnsignedChar>, length: Int) -> String {
        let hash = NSMutableString()
        for i in 0..<length {
            hash.appendFormat("%02.2hhX", result[i]) // "%02x"
        }
        return String(hash)
    }
   
    func emojiToImage() -> UIImage? {
        let size = CGSize(width: 23, height: 25)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        UIColor.clear.set()
        let rect = CGRect(origin: CGPoint(), size: size)
        UIRectFill(CGRect(origin: CGPoint(), size: size))
        (self as NSString).draw(in: rect, withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20) ])
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    func validUrl() -> String {
        let escapedUrlStr = self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        return escapedUrlStr!
    }
    
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
    
//    func safelyWrapped(str: String?) -> String {
//        return (str == nil) ? "" : self
//    }

    
//??Ashish
}

extension Optional {
    func safelyWrappingString() -> String {
        switch self {
        case .some(let value):
            return String(describing: value)
        case _:
            return ""
        }
    }
    
    func unwrap<T>(_ any: T) -> Any {
        let mirror = Mirror(reflecting: any)
        guard mirror.displayStyle == .optional, let first = mirror.children.first else {
            return any
        }
        return unwrap(first.value)
    }


    
}




























