//
//  ExtensionUIViewController.swift
//  OK
//
//  Created by Uma Rajendran on 10/30/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import Foundation

extension UIViewController {
    
    func getPendingTopResultAPI( isRequestModule : Bool, isFromMyNumber : Bool) -> URL? {
        let urlStr   = Url.getPendingRequestTopResult
        let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
        var urlComponents = URLComponents.init(url: ur, resolvingAgainstBaseURL: false)
        let userData = UserModel.shared
        // add params
        
        //  RequestMoney = 0,BillSplitter = 1
        var requestNoduleValue : Int = 0
        if isRequestModule == false{
            requestNoduleValue = 1
        }
        
        let mobileNUmberQuery      = URLQueryItem(name:"MobileNumber", value:userData.mobileNo)
        let simIdQuery             = URLQueryItem(name:"Simid",value :userData.simID)
        let msIDQuery              = URLQueryItem(name:"MSID" , value:userData.msid)
        let osTypeQuery            = URLQueryItem(name:"OSType", value: userData.osType)
        let requestModuleQuery     = URLQueryItem(name:"RequestModuleType" , value :"\(requestNoduleValue)" )
        let isFromMyNumberQuery    = URLQueryItem(name:"IsMynumber" , value: "\(isFromMyNumber)")
        
        urlComponents?.queryItems  = [mobileNUmberQuery,simIdQuery,msIDQuery,osTypeQuery,requestModuleQuery,isFromMyNumberQuery]
        return  urlComponents?.url
    }
    
    func getNearByMerchantsWithPromotionsAPI(shopname: String, townshipcode: String, divisioncode: String, businesstype: String, businesscategory: String, neardistance: String, city: String, mobilenumber: String, lat: String,long: String) -> URL {
       
        let isAgent : String = (UserModel.shared.agentType == .agent) ? "true" : "false"
        let urlStr = String.init(format: Url.nearByMerchantsWithPromotionsAPI,shopname,townshipcode,divisioncode,msid,businesstype,businesscategory,neardistance,otp,cellid,simid,mobilenumber,lat,long,isAgent)
        let url = getUrl(urlStr: urlStr, serverType: .serverApp)
        return url
    }

    func getPhoneNumberByFormat(phoneNumber: String) -> String {
        var mobNumber = ""
        if phoneNumber.hasPrefix("0095") {
            mobNumber = phoneNumber.substring(from: 4)
        }else if phoneNumber.hasPrefix("95") {
            mobNumber = phoneNumber.substring(from: 2)
        }
        
        if mobNumber.count > 0 {
            if !mobNumber.hasPrefix("0") {
                mobNumber = "0\(mobNumber)"
            }
        }else {
            mobNumber = phoneNumber
        }
        return mobNumber
    }
    
    func getNearByMerchantsAPI(shopname: String, townshipcode: String, divisioncode: String, businesstype: String, businesscategory: String, neardistance: String, city: String, mobilenumber: String, lat: String,long: String) -> URL {
        let isAgent : String = (UserModel.shared.agentType == .agent) ? "true" : "false"
        let urlStr = String.init(format: Url.nearByMerchantsAPI,shopname, townshipcode, divisioncode, businesstype, businesscategory, neardistance, cellid, city, lat, long, mobilenumber, isAgent)
        let url = getUrl(urlStr: urlStr, serverType: .serverApp)
        return url
    }
    
    func getCashInAgentsUrl(lat: String, long: String, mobileNumber: String) -> URL {
        let urlStr = String.init(format: Url.cashInAgentsByLatLongAPI, lat, long, mobileNumber)
        let url = getUrl(urlStr: urlStr, serverType: .addwithdrawCashInCashOutUrl)
        return url
    }
    
    func getCashInOfficesUrl(lat: String, long: String, mobileNumber: String) -> URL {
        let urlStr = String.init(format: Url.cashInOfficesByLatLongAPI, lat, long, mobileNumber)
        let url = getUrl(urlStr: urlStr, serverType: .addwithdrawCashInCashOutUrl)
        return url
    }
    
    func getCashIn_AllList(by: CashInOutLocation, townshipname: String, phonenumber: String, lat: String, long: String) -> URL {
        if by == .byTownship {
            let urlStr = String.init(format: Url.cashIn_AllList_ByTownship, townshipname, phonenumber, lat, long)
            let url = getUrl(urlStr: urlStr, serverType: .addwithdrawCashInCashOutUrl)
            return url
        } else {
            let urlStr = String.init(format: Url.cashIn_AllList_ByCurrentLoc, lat, long, phonenumber)
            let url = getUrl(urlStr: urlStr, serverType: .addwithdrawCashInCashOutUrl)
            return url
        }
    }
    
    func getCashOut_AllList(by: CashInOutLocation, townshipname: String, phonenumber: String, lat: String, long: String) -> URL  {
        if by == .byTownship {
            let urlStr = String.init(format: Url.cashOut_AllList_ByTownship, townshipname, phonenumber, lat, long)
            let url = getUrl(urlStr: urlStr, serverType: .addwithdrawCashInCashOutUrl)
            return url
        } else {
            let urlStr = String.init(format: Url.cashOut_AllList_ByCurrentLoc, lat, long, phonenumber)
            let url = getUrl(urlStr: urlStr, serverType: .addwithdrawCashInCashOutUrl)
            return url
        }
    }
    
    func getCashIn_AllBanks(by: CashInOutLocation, townshipname: String, phonenumber: String, lat: String, long: String) -> URL  {
        if by == .byTownship {
            let urlStr = String.init(format: Url.cashIn_AllBanks_ByTownship, townshipname, phonenumber, lat, long)
            let url = getUrl(urlStr: urlStr, serverType: .addwithdrawCashInCashOutUrl)
            return url
        } else {
            let urlStr = String.init(format: Url.cashIn_AllBanks_ByCurrentLoc, lat, long, phonenumber)
            let url = getUrl(urlStr: urlStr, serverType: .addwithdrawCashInCashOutUrl)
            return url
        }
    }
    
    func getCashOut_AllBanks(by: CashInOutLocation, townshipname: String, phonenumber: String, lat: String, long: String) -> URL  {
        if by == .byTownship {
            let urlStr = String.init(format: Url.cashOut_AllBanks_ByTownship, townshipname, phonenumber, lat, long)
            let url = getUrl(urlStr: urlStr, serverType: .addwithdrawCashInCashOutUrl)
            return url
        } else {
            let urlStr = String.init(format: Url.cashOut_AllBanks_ByCurrentLoc, lat, long, phonenumber)
            let url = getUrl(urlStr: urlStr, serverType: .addwithdrawCashInCashOutUrl)
            return url
        }
    }
    
    func getCashIn_AllAgents(by: CashInOutLocation, townshipname: String, phonenumber: String, lat: String, long: String) -> URL  {
        if by == .byTownship {
            let urlStr = String.init(format: Url.cashIn_AllOkAgents_ByTownship, townshipname, phonenumber, lat, long)
            let url = getUrl(urlStr: urlStr, serverType: .addwithdrawCashInCashOutUrl)
            return url
        } else {
            let urlStr = String.init(format: Url.cashIn_AllOkAgents_ByCurrentLoc, lat, long, phonenumber)
            let url = getUrl(urlStr: urlStr, serverType: .addwithdrawCashInCashOutUrl)
            return url
        }
    }
    
    func getCashOut_AllAgents(by: CashInOutLocation, townshipname: String, phonenumber: String, lat: String, long: String) -> URL  {
        if by == .byTownship {
            let urlStr = String.init(format: Url.cashOut_AllOkAgents_ByTownship, townshipname, phonenumber, lat, long)
            let url = getUrl(urlStr: urlStr, serverType: .addwithdrawCashInCashOutUrl)
            return url
        } else {
            let urlStr = String.init(format: Url.cashOut_AllOkAgents_ByCurrentLoc, lat, long, phonenumber)
            let url = getUrl(urlStr: urlStr, serverType: .addwithdrawCashInCashOutUrl)
            return url
        }
    }
    
    func getCashIn_AllOffices(by: CashInOutLocation, townshipname: String, phonenumber: String, lat: String, long: String) -> URL  {
        if by == .byTownship {
            let urlStr = String.init(format: Url.cashIn_AllOkOffices_ByTownship, townshipname, phonenumber, lat, long)
            let url = getUrl(urlStr: urlStr, serverType: .addwithdrawCashInCashOutUrl)
            return url
        } else {
            let urlStr = String.init(format: Url.cashIn_AllOkOffices_ByCurrentLoc, lat, long, phonenumber)
            let url = getUrl(urlStr: urlStr, serverType: .addwithdrawCashInCashOutUrl)
            return url
        }
    }
    
    func getCashOut_AllOffices(by: CashInOutLocation, townshipname: String, phonenumber: String, lat: String, long: String) -> URL  {
        if by == .byTownship {
            let urlStr = String.init(format: Url.cashOut_AllOkOffices_ByTownship, townshipname, phonenumber, lat, long)
            let url = getUrl(urlStr: urlStr, serverType: .addwithdrawCashInCashOutUrl)
            return url
        } else {
            let urlStr = String.init(format: Url.cashOut_AllOkOffices_ByCurrentLoc, lat, long, phonenumber)
            let url = getUrl(urlStr: urlStr, serverType: .addwithdrawCashInCashOutUrl)
            return url
        }
    }
 
    func showErrorAlert(errMessage: String) {
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: nil, body: errMessage.localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func noratingPopup(errMessage: String) {
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: nil, body: errMessage.localized, img: #imageLiteral(resourceName: "norating.png"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func showMsgPopupAlert(msg: String , image: UIImage) {
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: nil, body: msg.localized, img: image)
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        }
    }
    
    @objc func dismissScreen() {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func popScreen() {
        self.navigationController?.popViewController(animated: true)
    }
}

extension UIViewController {
    private struct AssociatedKey {
        static var dHeight = "nsh_DescriptiveName"
        static var helpSupportEnum = "descriptive_enum_helpStructure"
    }
    
    var helpSupportNavigationEnum : HelpSupportNavigation? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKey.helpSupportEnum) as? HelpSupportNavigation
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(
                    self,
                    &AssociatedKey.helpSupportEnum,
                    newValue as HelpSupportNavigation?,
                    objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
    
     var dHeight: CGFloat? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKey.dHeight) as? CGFloat
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(
                    self,
                    &AssociatedKey.dHeight,
                    newValue as CGFloat?,
                    objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
}

enum HelpSupportNavigation {
  case Bank,
    Bonus_Point_Discount_Money,
    Bus,
    DTH,
    Electricity,
    Ferry,
    Flight,
    Gift_Cards,
    Hotel,
    Lucky_Draw,
    Merchant_Payment,
    Pay_Send,
    Postpaid_Mobile,
    Resale,
    Recharge,
    Request_Money,
    Bill_Splitter,
    Taxi,
    Landline_Bill,
    Toll,
    Train,
    Add_Withdraw,
    Reset_Password_Account_Blocked,
    dashboard,
    Others,
    InternationalTopUp
}
