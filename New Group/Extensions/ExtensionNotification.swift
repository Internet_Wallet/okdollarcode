//
//  ExtensionNotification.swift
//  OK
//
//  Created by isure on 7/20/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let goDashboardScreen = Notification.Name("goDashboardScreen")
    static let scanQRClose = Notification.Name("scanQRCloseAction")
    //This will be used to show hide tab bar in dashboard
    static let showHide = Notification.Name("showHide")
    static let animateTabbar = Notification.Name("animateTabbar")
    static let popToDashboard = Notification.Name("popToDashboardFromMap&MakePayment")
    static let changeUserType = Notification.Name("changeUserType")
    static let selectHome = Notification.Name("selectHome")
    static let comeFromCashBack = Notification.Name("comeFromCashBack")
}
