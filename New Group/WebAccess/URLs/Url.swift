//
//  Url.swift
//  OK
//
//  Created by Vinod's MacBookPro on 10/15/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

enum UrlTypes {
    case productionUrl, testUrl
}


let serverUrl: UrlTypes = .productionUrl

struct Url {
    
    static let advertisementUrl  = "AdService.svc/GetAdvertisements"
    
    static let nearbyMerchant = "https://www.okdollar.co/RestService.svc/FindNearByMerchants?BusinessCategory=&Township=&Long=%@&BusinessType=&NearDistance=%@&PhoneNumber=&Division=&CellID=%@&City=&Lat=%@&IsAgent=false"
    
    static let nearbyMerchantWithPromotions = "https://www.okdollar.co/RestService.svc/FindNearByMerchantsWithPromotions?Township=&MsId=%@&BusinessType=&NearDistance=%@&Otp=\(simid)&CellID=&City=&SimId=\(simid)&MobileNumber=\(UserModel.shared.mobileNo)&BusinessCategory=&OsType=1&Long=%@&PhoneNumber=&Division=%@&Lat=%@&IsAgent=false"
    
    static let preLogin   = "RestService.svc/GetAgentRegOtp" // new url our server for prelogin
    
    static let changePassword    = "WebServiceIpay/services/request;requesttype=CHANGEPIN;agentcode=%@;pin=%@;source=%@;newpin=%@;clienttype=GPRS;comments=CHANGEPIN;vendorcode=IPAY;securetoken=%@"
    
    static let feedbackPayment = "RestService.svc/AddCustomerFeedBack"
    
    static let favorite = "RestService.svc/GetFavourites?MobileNumber=%@&Simid=%@&MSID=%@&OSType=1&OTP=%@"
    
    static let deleteFavorite = "RestService.svc/RemoveFavourite?MobileNumber=%@&Simid=%@&MSID=%@&OSType=%@&OTP=%@&FavNum=%@&Type=%@"
    
    static let paymentGatewayOTP = "RestService.svc/Receive_otp?mobile_no=%@&SimID=%@&MSID=%@&OSType=%@&OTP=%@&is_admin=false&userName=%@&transType=PAYMENTGATEWAY"
    static let BlockUser = "WebServiceIpay/services/request;requesttype=BLOCK;agentcode=%@;vendorcode=IPAY;clienttype=GPRS;comments=%@;pin=%@;idprooftype=%@"
    
    static let generateAuthCode = "RestService.svc/SetAgentAuthCode?SimIDMobileNumber=%@&SimID=%@&MSID=%@&OSType=%@&OTP=%@"
    
    static let getOTP = "RestService.svc/Receive_otp?mobile_no=%@&SimID=%@&MSID=%@&OSType=%@&OTP=%@&is_admin=false&userName=%@&transType="
    
    static let getShopId = "RestService.svc/AddShops?MobileNumber=%@&Simid=%@&Lat=%@&Long=%@&CellID=%@&ShopName=%@&MSID=%@&OSType=%@&OTP=%@&Country=%@&State=%@&TownShip=%@&City=%@"
    
    static let promotionView = "RestService.svc/PromotionViews?MobileNumber=%@&Simid=%@&MSID=%@&OSType=%@&OTP=%@&PromotionId=%@"
    
    static let BroadcastGPS = "RestService.svc/BroadcastGPS?MobileNumber=%@&simid=%@&Lat=%@&Long=%@&CellID=%@&Active=%@&ShopId=%@&State=%@&TownShip=%@&City=%@"
    
    static let getPromotionId = "RestService.svc/AddPromotions?MobileNumber=%@&Simid=%@&MSID=%@&OSType=%@&OTP=%@&PromotionHeader=%@&Description=%@&WebUrl=%@&PhoneNumber=%@&FacebookId=%@&ShopId=%@&IsAdmin=%@&ContactPersonName=%@&EmailId=%@"
    
    static let addPromotion = "RestService.svc/AddPromotions?MobileNumber=%@&Simid=%@&MSID=%@&OSType=%@&OTP=%@&PromotionHeader=%@&Description=%@&WebUrl=%@&PhoneNumber=%@&FacebookId=%@&ShopId=%@&IsAdmin=%@&ContactPersonName=%@&EmailId=%@"
    
    static let addPromotionwithID = "RestService.svc/AddPromotions?MobileNumber=%@&Simid=%@&MSID=%@&OSType=%@&OTP=%@&PromotionHeader=%@&Description=%@&WebUrl=%@&PhoneNumber=%@&FacebookId=%@&ShopId=%@&IsAdmin=%@&Promotionid=%@&ContactPersonName=%@&EmailId=%@"
    
    static let ShopStatusApi = "RestService.svc/CheckShopStatus?MobileNumber=%@"
    
    static let Viewkickbackstatuschange_API = "WebServiceIpay/services/request;requesttype=KICKBACKSLABS;agentcode=%@;pin=%@;vendorcode=IPAY;clienttype=GPRS;minrange=%@;maxrange=%@;disbtype=%@;amount=%@;securetoken=%@;action=%@"
    
    static let CashBack = "WebServiceIpay/services/request;requesttype=VIEWSLABS;agentcode=%@;pin=%@;vendorcode=IPAY;clienttype=GPRS;securetoken=%@"
    
    static let Add_Kickback_Ratio_api = "WebServiceIpay/services/request;requesttype=KICKBACKSLABS;vendorcode=IPAY;amount=%@;agentcode=%@;clientos=iOS;peramt=%@;clienttype=GPRS;clientip=%@;pin=%@;disbtype=R;securetoken=%@;action=add;minrange=%@;maxrange=%@"
    
    static let addCashBack = "WebServiceIpay/services/request;requesttype=KICKBACKSLABS;vendorcode=IPAY;amount=%@;agentcode=%@;clientos=iOS;clienttype=GPRS;clientip=%@;pin=%@;disbtype=%@;securetoken=%@;action=add;minrange=%@;maxrange=%@"
    
    static let getReport = "WebServiceIpay/services/request;requesttype=TRANSINFO;agentcode=%@;pin=%@;vendorcode=IPAY;clienttype=GPRS;securetoken=%@;startindex=%@;lastindex=%@"
    
    static let getSendMoneyBankReport = "BankPayments.svc/GetBankCashInCounterTransReport"
    static let getResaleMainBalanceReport = "/products/phonenumber/%@/limit/%@"
    static let getSolarElectricityReport = "topup/phoneNumber/%@"
    
    static let getOfferSummaryReport = "RestService.svc/GetPromotionSummary"
    static let getOfferReport = "RestService.svc/GetPromotionFilterListForReport"
    static let getOfferFilterReport = "RestService.svc/GetPromotionDetailReportByFilter"
    static let getOfferListLocation = "RestService.svc/GetPromotionLocationFilterListForReport"
    static let getOfferAdvanceMerchantList = "RestService.svc/GetPromotionAdvanceMerchantFilterListForReport"
    static let getOfferDummyMerchantList = "RestService.svc/GetPromotionDummyMerchantFilterListForReport"
    static let CheckBalanceApi = "WebServiceIpay/services/request;requesttype=BALANCE;agentcode=%@;pin=%@;destination=%@;vendorcode=IPAY;clienttype=GPRS;securetoken=%@"
    static let getNonOfferReport = "RestService.svc/PromotionTransactionDetails"
    
    //Prabu NonOffer API
    static let getNonOfferPromotionTransactionSummary = "RestService.svc/GetPromotionTransactionSummary"
    static let getNonOfferPromotionAllTransactionRequestedType = "RestService.svc/GetPromotionTransactionRequestedType"
    static let getNonOfferPromotionLocation = "RestService.svc/GetPromotionLocation"
    static let getNonOfferPromotionAdvanceMerchant = "RestService.svc/GetPromotionAdvanceMerchant"
    static let getNonOfferPromotionDummyMerchant = "RestService.svc/GetPromotionDummyMerchant"
    
    static let getSafetyCashierTransctionSummary = "RestService.svc/GetSafetyCashierTransctionSummary"
    static let getSafetyCashierTransctionByTotalAvailableAmount = "RestService.svc/GetSafetyCashierTransctionByTotalAvailableAmount"
    static let getSafetyCashierTransctionByTotalReceivedAmount = "RestService.svc/GetSafetyCashierTransctionByTotalReceivedAmount"
    static let getSafetyCashierTransctionByTotalDiscountAmount = "RestService.svc/GetSafetyCashierTransctionByTotalDiscountAmount"
    

    
    static let saleNLock = "WebServiceIpay/services/request;requesttype=MERCHANTLIST;agentcode=%@;pin=%@;vendorcode=IPAY;clientip=%@;clientos=iOS;clienttype=GPRS;securetoken=%@"
    
    static let DummyMerchantListStatusUpdate_API = "WebServiceIpay/services/request;requesttype=MERCHANTSTATUS;agentcode=%@;destination=%@;status=%@;pin=%@;vendorcode=IPAY;clienttype=GPRS;securetoken=%@"
    
    static let deleteShop = "RestService.svc/DeleteShop?ShopId=%@"
    
    static let deleteCashback = "WebServiceIpay/services/request;requesttype=KICKBACKSLABS;agentcode=%@;pin=%@;vendorcode=IPAY;clienttype=GPRS;minrange=%@;maxrange=%@;disbtype=%@;amount=%@;securetoken=%@;action=delete"
    
    static let ApproveSaleNLock  = "/RestService.svc/RetrieveDummy?phonenumber=%@&simid=%@&ostype=1"
    
    static let SaleNLockList  = "RestService.svc/RetrieveDummy?&msid=%@&simid=%@&phonenumber=%@"

    static let SendForgotRequest = "/AdService.svc/SendOtp?dummymerchantnumber=%@"
    
    static let RequsteApprovalDummyMerchant_API  = "/RestService.svc/ApproveDummy?merchantphonenumber=%@&simid=%@&msid=&ostype=1&otp=%@&dummyid=%@&securetoken=%@"
    
    static let UpdateEmailDummyMerchant_API  = "RestService.svc/UpdateEmailIdByAccountNumber"
    
    // Subhash added urls for reg & cashinout
    static let URL_Security_Question  = "RestService.svc/SecurityQuestions"
    static let URL_Registration  = "RestService.svc/UpdateProfile"
    static let URL_FailureTrackInfo  = "RestService.svc/AddRegistraionFailureTrackInfo"
    static let URL_VillageList = "RestService.svc/GetVillageStreetByTownShipCode?TownshipCode="
    static let URL_ValidateReverifyForgotPassword = "RestService.svc/ValidateReverifyRequest?"
    static let URL_ViewSecurityQuestions = "RestService.Svc/ViewSecurityQuestions?"
    static let URL_AddForgotPasswordApproval = "RestService.svc/AddForgotPasswordApproval"
    static let URL_AddSecurityQuestionAnswer = "http://69.160.4.151:8002/AdService.svc/AddSecurityQuestionAnswer"
    static let URL_ReVerify = "RestService.svc/ReVerify"
    static let URL_CheckRegistrationStatus = "RestService.Svc/CheckRegistrationStatus?"
    static let URL_VerifyOkDollarNumber = "WebServiceIpay/services/request;requesttype=AUTH;agentcode=%@;vendorcode=IPAY;clienttype=GPRS"
    static let URL_ChangePassword = "WebServiceIpay/services/request;requesttype=CHANGEPIN;agentcode=%@;pin=%@;source=%@;newpin=%@;clienttype=GPRS;comments=CHANGEPIN;vendorcode=IPAY;securetoken=%@"
    static let URL_UpdateProfile = "RestService.svc/UpdateProfileDetails"
    static let URL_GetImageurls = "RestService.svc/GetMultiImageUrlByBase64String"
    static let URL_AddUnauthorisedAccessLog = "RestService.svc/AddUnauthorisedAccessLog"
    static let URL_GetMultiImageUrlByBase64String = "RestService.svc/GetMultiImageUrlByBase64String"

    
      // Subhash added urls for FaceIDPAY
    static let URL_GetCategoryApi = "OKDollar/GetCategoryApi"
    static let URL_SaveDeposit = "deposit/SaveDeposit"
    static let URL_FacePayDemoApi = "OKDollar/DemoApi"
    static let URL_FacePayAgentVerify = "OKDollar/AgentVerify" //SaveWithdraw
    static let URL_FacePaySMSVerification = "OKDollar/SMSVerification"
    static let URL_FacePayOtpVerification = "withdraw/OtpVerification"
    static let URL_FacePayResendOtp = "withdraw/ResendOtp"
    static let URL_FacePayVerifySenderNo = "withdraw/VerifySenderNo"
    static let URL_FacePayVerifyRefaranceNo = "withdraw/VerifyRefaranceNo"
    static let URL_FacePayPayment = "withdraw/Payment"
    static let URL_FacePaySaveWithdraw = "withdraw/SaveWithdraw"
    static let URL_FaceVerifyVideoUpload = "OKDollar/LiveFaceVerify"
    static let URL_FacePayFaceDetect = "OKDollar/FaceDetect"
    static let URL_FacePayFileUpload = "OKDollar/FileUpload"
    static let URL_FacePayGetCommisionsTaxi = "deposit/GetCommisionsTaxi"
    
    /////////---------------
    //Update Profile
    static let getUpdateProfileDetails    = "GetUpdateProfileMenuStatus"
    static let addUpdateProfileDetails    = "AddUpdateProfileMenuStatus"
    static let getNewUpStatus    = "RestService.svc/GetNewUpStatus"
    
    //Personal Details
    static let getPersonalDetails    = "GetUserProfilePersonalInfo"
    static let updatePersonalDetails    = "AddUserProfilePersonalInfo"
    static let getSecurityQuestions    = "RestService.svc/SecurityQuestions"
    
    //Business Details
    static let getBusinessDetails    = "GetMerchantBusinessInfoAndDocs"
    static let updateBusinessDetails    = "AddMerchantBusinessInfoAndDocs"
    
    //Merchant Details
    static let getMerchantDetails    = "RestService.svc/GetMerchantAuthorizationInfo"
    static let updateMerchantDetails    = "RestService.svc/UpdateMerchantAuthorizationInfo"
    
    // CashINCashOUT
    static let cashIn = "UpdateProfileMoneyInService"
    static let cashOUT = "UpdateProfileMoneyOutService"
    static let cashInCashOutGetService = "GetMoneyServiceByServiceType"
    //OK$Agent Details
    static let getOK$AgentDetails    = "GetAgentAuthorizationInfo"
    static let updateOK$AgentDetails    = "UpdateAgentAuthorizationInfo"
    
    //Other Services (Subhash Arya)
    static let getMerOtherServices = "GetMerOtherServices"
    static let updateProfileOtherServices = "UpdateProfileOtherServices"
    static let getTelcoCashBackPercentage = "RestService.svc/GetTelcoCashBackPercentage"
    
    // ALL Notes
    static let getAllItemInfoDetails = "GetAllItemInfoDetails"
    //Offers
    static let offersCategory    = "RestService.svc/GetAllPromotionCategory?"
    static let offersDetails = "RestService.svc/GetAllPromotionCodes?"
    static let offersConditionalMapping = "RestService.svc/GetAllConditionalMappingByPromotinId"
    static let offersPromotionByMerchant = "RestService.svc/GetAllPromotionByMerchant"

    //Bill Splitter
    static let billSplitter    = "RestService.svc/RequestMoney"
    static let billSplitterAllRequest = "RestService.svc/RequestMoneyViewBySender?"
    
    static let billSplitterReminderBill = "RestService.svc/RemindRequestMoney?"
    static let billSplitterCancelBill = "RestService.svc/RequestMoneyStatusUpdate?"
    static let billSplitterMobileValidation  = "RestService.svc/GetCategoryAndMercNameByMobileNumber?"
    
    //Bill Splitter Notification
    static let billSplitterNotification  = "RestService.svc/RequestMoneyViewByReciever?"
    static let billSplitterCancelNotification  = "RestService.svc/RequestMoneyStatusUpdate?"
    static let billSplitterPayNotification  = "https://www.okdollar.net/WebServiceIpay/services/request;"
    
    //Post Image Data
    
    static let businessDetailsImageURL    = "/RestService.svc/GetImageUrlByBase64String"
    
    static let AgentInfo_API = "WebServiceIpay/services/request;requesttype=AGENTINFO;agentcode=%@;vendorcode=IPAY;clienttype=GPRS"
    
    static let TransferToKickBack_APi = "WebServiceIpay/services/request;requesttype=PAYTOKICKBACK;agentcode=%@;destination=%@;amount=%@;comments=%@;pin=%@;vendorcode=IPAY;clientip=%@;clientos=%@;clienttype=GPRS;securetoken=%@"
    
    static let Transaction_History_details_API = "WebServiceIpay/services/request;requesttype=TRANSINFO;agentcode=%@;pin=%@;vendorcode=IPAY;clienttype=GPRS;securetoken=%@;startindex=%d;lastindex=100"
    
    static let CashInCashOut     = "RestService.svc/AddCashInCashOutSettings"
    
    static let addFiveNumber     = "RestService.svc/AdvMerAddOkAccNumbers"
    
    static let login             = "RestService.svc/Login"
    static let updateProfile     = "RestService.svc/RetrieveProfile?mobilenumber=%@&simid=%@"
    static let verifyOtp         = "RestService.svc/ValidateIosNumber?MobileNumber=%@&otp=%@"
    //static let giftCard          = "https://mobileview.okdollar.org/GiftCards/GiftCard/Index"
    
    //static let dth               = "https://mobileview.okdollar.org/Dth/DthBill/Index"
    //static let eCommerce = "http://69.160.4.149:1001/"
    static let merchantPayment   = "http://merchantpayment.okdollar.org/"//"https://mobileview.okdollar.org/MerchantPayments/Home/Index"
    // "http://test.mobileview.okdollar.org/OtherPayments/Home/GetInfo"
    static let bankDeposit       = "https://paymentgateway.okdollar.org/Utility/BankCashIn/pendingbankpayments.aspx"
    
    // Add money API
    static let reportAddMoney = "/netbanking/report/phoneNumber/%@"
    
    //Secret Keys
    static let cbBank_sKey = "9926d71687b6bc2c9699c3349fdc17fb3dfbba6224affb7676e1337926cdd602"
    static let cbBank_aKey = "422d1eb4b3d2af5b5f16fa6bb44b59a509834357256f0142f7d271f68be61f0fd0d3d12d1385f644327658948b3575a176ca0426c7e06f037b8434c01889592f9a0098b72f6aa69258fc2308514950c32b7384b3fddbf923dc8d014618ff66757003569395c918df1a1a456c5504db96668e59cce4e894c34a11956e72aa8c756bca2f252971648b40169a8803e1e2e522dc90299e1975a1a82038ae6c76d7e8"
    
    static let sKey_AddMoney = "ioGCWPBh1EhPHZ6OvYTQRLDec6/RjvtUTkhjeGsxZld5S29RMFRsR2hQSVZKUUZlc3VKRVROUVZyNkxwVUtSQjFLd211MzhWSEdjeDlRQzdDN0Ywc2FDazRTaTNINnZXOEdlOGVkdXp3eDdOdmNXYjRPblhGYVRhSUxyYlVFTkxVRnoyd0Rkak1RRTRjenliNkg5M1RZUmh4RDBTdlNlcmt2MnFuZG8wdURLeldaN1NYUWZORlUyNnhSRnROS3k0dldVcjZJeWRkSklQTmNLWEZoNWV4UW1RMjYyVlg0bmtQUURBRlFXT2NCdTVMbnNWSHJvcFBUR2V0RXlBUWNGaWdpOTdZSlN5THFDdmdxRVBOZ09wUTdVNTQ3ODU0YzgzLWQ2ZDAtNDRiMS1iMDVjLTgwY2Q2OWMyZjk5Ng=="
    static let aKey_AddMoney = "Sic1cKXLvQ5jL95olDqPiQ=="
    static let sKey_AddMoneyPayment = "kr5Zj3tCe3YhDmFLDw4AZ6AN0uGkl2wkA1Sdm+M/SaOrvVCIi8zOP8ZjE5PmLVORgV3s/RzvDpglPq10IMglDw===="
    
    //Get All Banks Deposit List
    static let sKey_AddMoneyBank = "VbxfRV/d1EgHbRBpJPV+RY2Ds6ByTpbIZlVQRXI2aU1DSXNRZVJTN003a055OXFTQ2pSWWRxQVVxWUs2d1lPT3ZGcGhIV3pCVFQwMGMxbWZvN0JIb0p0VklZa0dHV0x5c1ZjdFRJS3F1UUZKeksxenZvUnlXR2tsVlVadHZ0NDdJQ0hHWTNMZ3dVV1hFamY3Z3VVY0VXYm12MFg5c2E0QldJQnhEdmEyY3ZDQ3ROaHZ5Y05CWWMydmtmVmhmTXhWWlZBNWlzNHVVYjlTVVoyaVRsTFFOM20zUFBNaWEzWFE4cW1ZTWxOR2ljcjdkamFMbXdPNEFnSWdzSHFTZlZVYUVzVW93ZFdzWUZzdU1wYXF5QjVucGNQYjBlNjVhZjE0LTM2MDItNGNhYy04YWNkLTQ1ZWRmNjViNzVkOQ=="
    static let aKey_AddMoneyBank = "oU0y0bWekUEvaWsU+oPIdA=="
    
    //Bank Counter Deposit
    static let BankCounter_Deposit = "CounterDeposit"
    static let BankCounter_GetDeposit = "CounterDeposit/deposits/phonenumber/"
    static let BankCounter_DeleteDeposit = "CounterDeposit/deposits/"
    static let BankCounter_BankID = "banks/%@/parameters"
    
    static let AddMoney_GetBanksToken = "token"
    static let AddMoney_GetBanks = "banks"
    
    static let URLgetOKacDetails  = "RestService.svc/GetCategoryAndMercNameByMobileNumber?MobileNumber="
    
    static let nearByMerchantsWithPromotionsAPI = "RestService.svc/FindNearByMerchantsWithPromotions?ShopName=%@&Township=%@&Division=%@&MsId=%@&BusinessType=%@&BusinessCategory=%@&NearDistance=%@&Otp=%@&CellID=%@&City=&SimId=%@&MobileNumber=%@&startindex=&lastindex=&Lat=%@&Long=%@&OsType=1&PhoneNumber=&IsAgent=%@"
    
    static let nearByMerchantsAPI = "RestService.svc/FindNearByMerchants?ShopName=%@&Township=%@&Division=%@&BusinessType=%@&BusinessCategory=%@&NearDistance=%@&CellID=%@&City=%@&startindex=&lastindex=&Lat=%@&Long=%@&PhoneNumber=%@&IsAgent=%@"
    
    //    static let cashInAgentsByLatLongAPI = "https://cashincashout.okdollar.org/CashIn/agents/locations/Latitude/%@/Longitude/%@//phoneNumber/%@"
    static let cashInAgentsByLatLongAPI = "/CashIn/agents/locations/Latitude/%@/Longitude/%@//phoneNumber/%@"
    
    
    //    static let cashInOfficesByLatLongAPI = "https://cashincashout.okdollar.org/CashIn/offices/locations/Latitude/%@/Longitude/%@//phoneNumber/%@"
    static let cashInOfficesByLatLongAPI = "/CashIn/offices/locations/Latitude/%@/Longitude/%@//phoneNumber/%@"
    
    
    //    static let cashInOutTokenAPI = "https://cashincashout.okdollar.org/token"
    static let cashInOutTokenAPI = "/token"
    
    static let airtimeTokenApi = "/token"
    
    static let sKey_cash_in_out_prod = "bb8Vm0mj1EibEZ2MqjmnTr4pVKR+MtdeWTVoRW5FcnBoOHlKZjlHbVB4R2NIMm5xVEVSc0FmQkRSVjE5ZFlzOEJFdml0ZEtxb24wRkZTTWZTMHZpdzlwQzZ1MUpIR0kwRkliQ0hGNno0bEVYcDY4OEpWcW1uSk1CWnNsOGxBaWhlOTk3MUpZTjM1b0w0Ulg5dzJBdlpnTjU1Wm5ZZDhPM0NnMnA2QUhBN2NzcmZCR0lWbExzQXhIOHZFQ2JmQXU1cnVKWVlzQ3d2OXk2eWZaNFRmRDF3ZjJPWTQzMjhwZTdzeEF1MnN0bmpaN0I4TEtQb0hJQTYzSU0yNDlkeUt4RmYxT0FmcENrdmFTWGgxUU51bk8waGRBemJkNzYyOTVmLWEyZWEtNDZjZS1hZWZlLTc1NTI2M2Q2YzAxZg=="
    
    static let aKey_cash_in_out_prod = "ECAw65/v4oap26vqeIrNnw=="
    
    static let sKey_airtime = "VZ4Loe1h1EhUf79zUbX4QJoajeY2c2pkNXRLb3BTQzZ1bnNSQVVvMXB0bkRwaXhzRGV4WEFhZHNCd29HN0lDVnc4U1p4c0FtMlZyRFhsbVg2Y2xLZXA1VUs5UE5xaDNnclJSclBZNFVlTGZwc1pWYUtRS2ZnMjFIRDhQeGR3NlVjS1dGVDVIaWlScFRLUWx1b3pEWlhYOXlhS25SSUcwc09LTmlDUEdQSmw3UE1pNndXZUpvc2c3RUtocEpSelpiYTdFQUNEclpzeVF2WnVtMU4xNG83TTlyTUlYZmo3N1RMZnpQZDZ0VHh0NTRRTG9hNXg2YTV2ZU43WHg0cE16dTA3ajQ1V1lRSGwxTlFFYkFkN2p3QmhZYTY2MTc2NDcxLTkwNGItNDU3OS1iYmZhLTUyZGM1YzIwNTFmMA=="
    
    static let aKey_airtime = "ngKgfP6VzXBzmLBMxEUA1A=="
    
    static let aLoyalty_airtime = "aEfeJJrF+S7o0IxB5ymL9Q=="
    
    static let sLoyalty_airtime = "Bb/HiMMl1UgTwoyZhtcDSYJrD+jOAV6xd1p6MmpCVkJtbG1QUXZEUE5sUlRXSGM0RWFaQnlycVR5ekV6OGJUZE5iMzNLTHlBcHFRT2xVRXA2NXhKbVVONDRUYmFxT1F3U3lHNU5pdTJsWlZIYWJyZHZaMkNuSzEyTjg4MXVZVGJaZTV4T0piWUs0aE04RmRjbDhwUlZOSkk3TnJPajNJTkY0cjZmM09tOGUyc21vNEllZlB2c0NGTVZ4MEJjTXlSd1RIRVloNnN0RFMxVmFmVFRqdERvV2VDeWZsc0RNS3ZxazZUZ3p0N2poQnB0TzA0UmdzMmN1blp5RVEzNU9vaDNoTHVlTXdkendlSFZ6VGtvcmFnZVJUNjRiZjE1ZjNlLTg1MGItNDY4MS04YzQ1LTRmMGIwYTNlM2M4Yg=="
    
    static let AirtimeGet_PriceAPI = "http://airtimeapi.okdollar.org/sellingproducts/requestprice/telcos/%@/%@"
    
    static let Airtime_OrderPlaceAPI = "http://airtimeapi.okdollar.org/products/phonenumber/%@"
    
    static let AirtimeGetTelcoAPI = "http://airtimeapi.okdollar.org/telcos/%@"
    
    static let cashIn_AllList_ByTownship = "/CashIn/allLists/townships/%@/phoneNumber/%@/Latitude/%@/Longitude/%@"
    
    static let cashIn_AllList_ByCurrentLoc = "/CashIn/allLists/locations/Latitude/%@/Longitude/%@/phoneNumber/%@"
    
    static let cashIn_AllBanks_ByTownship = "/CashIn/banks/townships/%@/phoneNumber/%@/Latitude/%@/Longitude/%@"
    
    static let cashIn_AllBanks_ByCurrentLoc = "/CashIn/banks/locations/Latitude/%@/Longitude/%@/phoneNumber/%@"
    
    static let cashIn_AllOkAgents_ByTownship = "/CashIn/agents/townships/%@/phoneNumber/%@/Latitude/%@/Longitude/%@"
    
    static let cashIn_AllOkAgents_ByCurrentLoc = "/CashIn/agents/locations/Latitude/%@/Longitude/%@/phoneNumber/%@"
    
    static let cashIn_AllOkOffices_ByTownship = "/CashIn/offices/townships/%@/phoneNumber/%@/Latitude/%@/Longitude/%@"
    
    static let cashIn_AllOkOffices_ByCurrentLoc = "/CashIn/offices/locations/Latitude/%@/Longitude/%@/phoneNumber/%@"
    
    static let cashOut_AllList_ByTownship = "/CashOut/allLists/townships/%@/phoneNumber/%@/Latitude/%@/Longitude/%@"
    
    static let cashOut_AllList_ByCurrentLoc = "/CashOut/allLists/locations/Latitude/%@/Longitude/%@/phoneNumber/%@"
    
    static let cashOut_AllBanks_ByTownship = "/CashOut/banks/townships/%@/phoneNumber/%@/Latitude/%@/Longitude/%@"
    
    static let cashOut_AllBanks_ByCurrentLoc = "/CashOut/banks/locations/Latitude/%@/Longitude/%@/phoneNumber/%@"
    
    static let cashOut_AllOkAgents_ByTownship = "/CashOut/agents/townships/%@/phoneNumber/%@/phoneNumber/%@/Latitude/%@/Longitude/%@"
    
    static let cashOut_AllOkAgents_ByCurrentLoc = "/CashOut/agents/locations/Latitude/%@/Longitude/%@/phoneNumber/%@"
    
    static let cashOut_AllOkOffices_ByTownship = "/CashOut/offices/townships/%@/phoneNumber/%@/Latitude/%@/Longitude/%@"
    
    static let cashOut_AllOkOffices_ByCurrentLoc = "/CashOut/offices/locations/Latitude/%@/Longitude/%@/phoneNumber/%@"
    
    static let sKey_cash_in_out_test = "omgSZluj1EiFkVvKvdgNRINSYvCFeSvob0F1NnBHOTJqMWY2ZnVGU1FwQUt3NTUzOG9JdXRlVTluRDRIMVd5R2EwMDBFMWxjODhpN0hXUnNGMThCN1BKRWdmU2IwOGEzMDN0NUZFeUdDcXZ2NVgyUXdMQkFYeElMcXZUaWlLbDlocEdPbVZuQUMxOFJkWUlKbkNwcDdhZkR1aVBVdlFoMkV3TlFSbm5haUE3Z3kxc1NtSzRnZTZDR0pDZEVHU1B3VUdNcUsxMUFZT0c2ZDlQM3o2cmxWQVhJdEhIUEtqVVB2S0xyb0JJdVM0SVF6R25Kd2tQSmxvYVR0RTdDdW0weTQxTFN4aU94eTdTYjFIU25lOGNqVEtvRzQ4NTJiNGM2LTkwOWMtNDA3ZC05NzZiLWMyOWJhMTUzNzNkNQ=="
    
    static let aKey_cash_in_out_test = "ocHtW8nu1gz25ca1WV7nwA=="
    //Help & Support APIS
    static let submitQueryApi = "/OkHelperTicketService/BookOkHelperTicketWithMultipartData"
    static let myQueryUrlApi = "/OkHelperTicketService/ListAllCustomerTickets"
    static let closeQueryApi = "/OkHelperTicketService/ConfirmClosedTicket"
    static let bankListWithCountApi = "/AdService.svc/GetBankListWithCount"
    static let rateOkDollarHelpSupportApi = "OkHelperTicketService/RateOkDollarHelpTicket"
    static let getTicketDetailsApi = "/OkHelperTicketService/GetTicketDetails"
    static let ConfirmClosedTicket = "/OkHelperTicketService/ConfirmClosedTicket"
    
    //TbMore
    static let balanceCheckTimeUrl = "WebServiceIpay/services/request;requesttype=%@;agentcode=%@;destination=%@;pin=%@;vendorcode=%@;clientip=%@;clientos=%@;clienttype=%@;securetoken=%@"
    
    //Electricity
    static let Get_Electricity_appType = "https://www.okdollar.co/RestService.svc/GetElectricityAppType"
    static let Get_Ebill_Histrory = "https://www.okdollar.co/RestService.svc/GetEbBillHistoryByMobileNumber"
    static let  EB_Bill = "https://paymentgateway.okdollar.org/BillPayments/Electricity/ElectricityBill.aspx"
    
    //Solar Electricity
    static let SolarElectricityTokenAPIURL = "https://solarhometopupapi.okdollar.org/token" //Post
    static let Get_Solor_Bill_Details = "https://solarhometopupapi.okdollar.org/topup/device/"
    static let Solor_Service_Fee_Calculation = "https://solarhometopupapi.okdollar.org/topup/servicecharges"
    static let Solor_Bil_Pay = "https://solarhometopupapi.okdollar.org/topup/new"
    static let aKey_solor_bill = "PK5lsysopE5mSLowrcceow=="
    static let sKey_solor_bill = "wyDsGZEx1UgHTlIjryB4Q43YJlhMQtJnNVBLZlREeG9iTXl5VkFUV09aNm95aFljMGVPRGJET294aTFvcDhsVzRYN2psMlBjSXR1c1laaFkwc3B6enRNWDdLQXdKcHFReGt1Ymt6MDdPdzVzVGdQWUFjeDdTd2hLS2ptYjFjU2NDZTR5b0d5QjFybEN5ZUVhNGx3cTF0N0JEalFBTlpHdmRjcXptYnU0b3dGbEtlV3FlTGdQcW1rU2czdUV6eHljbDBqVnlQeWQxMUNGblcwak5rZkIzMEUxMlhTc3FwRjA3bjlVNEExUFdDWXpqZW9LREwxT1N3aXV2cmRhNkd4THdNSEFXZm9Nc1BwMWNQUlhzSHBleTBoTDhiNjQ0NDdmLTc3OWEtNGFmMS05OWYyLWUxNmUwMTQzYWJhMA=="
    
    //Tax Payment
    static let aKey_Tax_Payment = "Fm4WbeczPpMaey3JDRjPWw=="
    static let sKey_Tax_Payment = "qS37YZ6/1UiidgyusOi7SL7BySLIzCwgYmpZRzB3eTFiTGVZRUNvWVhlYmZ0ZENESUJyMG90NGN3aDJuNURtcmszVUt3bnhiQ0tYY25GNjdoVTh5RjhOc1JDbWhLQmNVMnU1MEkyUmVPUXhlV2J6cms0RmtFeGxtcG0ydEJQUUJyQ2syS0d5cERla3pLdFd6TjQ3UDUyUEVJU0tnOWhkODl2d1QxNmhkR1poa053YjZRQ2dHU0JGeXdmdDE2U3FmQW16WjZDOFJYZEpnVXAxVzFPUDZCV3A4cTdnVk1waHh0QmpsNkM5TkVPc3oxamVIb1hBckN0ZW42MXM4Z21FczVWd1RGUFp6UEIyd28wR0dkWXJ6cHp2QTg5MWZmMzkwLTMwN2YtNDgzYi1hZmU4LWZmYjg2YjQyNGU5Zg=="
    static let TaxPaymentTokenAPIURL = "token" //Post
    static let taxPaymentEnquiry = "inquiry/%@/%@"
    static let taxAccountTypes = "accountTypes"
    static let Tax_Payment_History = "payments/%@"
    static let IRD_Details = "irddetails/division/%@"
    static let paymentPay = "payments/pay"
    
    
    // "Add Vehicle Module"
    static let AllVehicleCategories  = "RestService.svc/GetAuctionGateCategories?"
    static let AddCarAPI  = "RestService.svc/AddAuctionGateCarType"
    static let GetAllVehicleAPI  = "RestService.svc/GetAuctionGateVehicalHistoryByMobileNumber?"
    static let GetVehicleBrands  = "RestService.svc/GetVehicleBrandNameByVehicleName?"
    static let GetVehicleSubBrands  = "RestService.svc/GetVehicleModelByBrandId?"
    
    //Search Locations url
    static let searchLocations = "BussinessLocationService/SearchForLocations"
    static let getAllFAQQuestions = "/AppSettingsService/GetAllQuestionTagsForPhone"
    static let getAllCallBack = "/AppSettingsService/ListAllIssueCategoriesForPhone"
    static let getQuestionAnswerByTagApi = "/AppSettingsService/GetQuestionAnswerByTagForPhone"
    
    //Loyalty API
    static let loyaltyTokenApiUrl = "token"
    static let CheckLoyaltyBalanceApi = "WebServiceIpay/services/request;requesttype=BALANCE;agentcode=%@;coreversion=2.0;pin=%@;destination=%@;vendorcode=IPAY;clientip=%@;clientos=%@;clienttype=SELFCARE;securetoken=%@"
    
    static let bonusConfigApiUrl = "WebServiceIpay/services/request;requesttype=LOYALTYLIST;agentcode=%@;vendorcode=IPAY;clientip=%@;clientos=%@;clienttype=SELFCARE;securetoken=%@"
    static let addNewBonusConfigAPiUrl = "WebServiceIpay/services/request;requesttype=LOYALTYADD;startts=%@;agentcode=%@;minrange=%@;peramt=%@;points=%@;status=%@;selfwallet=FALSE;pin=%@;mode=%@;endts=%@;refid=%@;maxrange=%@;vendorcode=IPAY;pointexpiry=%@;clientip=%@;clientos=%@;clienttype=SELFCARE;securetoken=%@"
    static let createSopNameApiUrl = "WebServiceIpay/services/request;requesttype=SHOPNAME;agentcode=%@;status=%@;shopname=%@;pin=%@;mode=ADD;image=;vendorcode=IPAY;clientip=%@;clientos=%@;clienttype=SELFCARE;securetoken=%@"
    static let editSopNameApiUrl = "WebServiceIpay/services/request;requesttype=SHOPNAME;agentcode=%@;status=%@;shopname=%@;pin=%@;mode=EDIT;image=;vendorcode=IPAY;clientip=%@;clientos=%@;clienttype=SELFCARE;securetoken=%@"
    static let updateShopInAppServerUrl = "RestService.svc/AddShops?MobileNumber=%@&Lat=%@&State=%@&TownShip=%@&Long=%@&OSType=%@&Country=%@&City=%@&MSID=%@&CellID=%@&ShopName=%@&Simid=%@"
    static let loyaltyViewApiUrl = "WebServiceIpay/services/request;requesttype=LOYALTYVIEW;agentcode=%@;pin=%@;vendorcode=IPAY;clientip=%@;clientos=%@;clienttype=SELFCARE;securetoken=%@"
    static let pointDistribution = "RestService.svc/PointDestibution" //"WebServiceIpay/services/request;requesttype=POINTDISTRIBUTION;agentcode=%@;destination=%@;amount=%@;pin=%@;image=%@;vendorcode=IPAY;pointexpiry=%@;clientip=%@;clientos=%@;clienttype=SELFCARE;securetoken=%@"
    static let loyaltyTopUpApiUrl = "WebServiceIpay/services/request;requesttype=LOYALTYTOPUP;agentcode=%@;amount=%@;pin=%@;vendorcode=IPAY;clientip=%@;clientos=%@;clienttype=SELFCARE;securetoken=%@"
    static let earnPointTrfApiUrl = "RestService.svc/EarnypointTransfer" //"WebServiceIpay/services/request;requesttype=LOYALTYPOINTTRF;agentcode=%@;destination=%@;points=%@;shopname=%@;pin=%@;vendorcode=IPAY;clientip=%@;clientos=%@;clienttype=SELFCARE;securetoken=%@"
    static let discountMoneyApiUrl = "WebServiceIpay/services/request;requesttype=DISCOUNTVIEW;agentcode=%@;vendorcode=IPAY;clientip=%@;clientos=%@;clienttype=GPRS;securetoken=%@"
    static let redeemInitialApiCall = "RestService.svc/GetLoyaltyMerchantTelco?MerchanatNumber=%@"
    static let submitRedeemNowApiUrl = "RestService.svc/LoyalityRedeemFromSource" //"WebServiceIpay/services/request;requesttype=LOYALTYREDEEM;agentcode=%@;amount=%@;merchant=%@;pin=%@;vendorcode=IPAY;clientip=%@;clientos=%@;clienttype=GPRS;securetoken=%@"
    static let loyaltyUpdateUserApiUrl = "UserService/UpdateUser"
    static let updateBusinessRunningStatus    = "RestService.svc/UpdateProfileBusinessRunningStatus"
    static let getMerchantListUrl = "bonuspoints/phonenumber/%@/name/%@/password/%@"
    static let bonusPointResaleStatusApi = "products/phonenumber/%@/name/%@/password/%@"
    static let resalePointApiUrl = "products/add"
    static let buyBonusPointUrl = "products/live/%@"
    static let buyBonusOrderNewUrl = "orders/new"
    static let getAdvanceMerchantListUrl = "RestService.svc/RetrieveDummy?msid=%@&simid=%@&phonenumber=%@"
    static let pulloutApiUrl = "WebServiceIpay/services/request;requesttype=LOYALTYPULLOUT;agentcode=%@;destination=%@;amount=%@;pin=%@;vendorcode=IPAY;clientip=%@;clientos=%@;clienttype=SELFCARE;securetoken=%@"
    static let advancePointTrfApiUrl = "WebServiceIpay/services/request;requesttype=LOYALTYTRF;agentcode=%@;destination=%@;amount=%@;pin=%@;vendorcode=IPAY;clientip=%@;clientos=%@;clienttype=SELFCARE;securetoken=%@"
    static let dummyWalConfigApiUrl = "WebServiceIpay/services/request;requesttype=DUMMYWALLETCONFIG;agentcode=%@;destination=%@;selfwallet=%@;mode=UPDATE;vendorcode=IPAY;clientip=%@;clientos=%@;clienttype=SELFCARE;securetoken=%@"
    static let redeemPointHistoryByCategoryApiUrl = "RestService.svc/GetRedeemPointsHistoryByCategory"
    
    //Request Money
    static let sendRequestForRequestMoney = "RestService.svc/RequestMoney"
    static let getPendingRequestTopResult = "RestService.svc/RequestMoneyPendingTopResult"
    static let getAllSendRequest = "RestService.svc/RequestMoneyViewBySender"
    static let getAllBlockContactList = "RestService.svc/GetAllListOfBlockNumbersFromReqMoney"
    static let blockOrUnblockParticularNumber = "RestService.svc/ReceiveMoneyBlockUserBySourceNum"
    static let getAllReceivedRequest = "RestService.svc/RequestMoneyViewByReciever"
    static let changeStatusOfRequest = "RestService.svc/RequestMoneyStatusUpdate"
    static let remindRequest =  "RestService.svc/RemindRequestMoney"
    
    
    //Cardless Cash API
    static let cardlessBankDetailsApi = "RestService.svc/GetBankListAndDenominations"
    
    //Offers API
    
    static let verifyCustomerByPromoID = "RestService.svc/VerifyCustomerByPromocodeId?Simid=%@&MobileNumber=%@&PromotionId=%@&balance=%@&MSID=%@&OSType=1&celltower=&OTP=%@&TransAmount=%@&lang=%@&lat=%@"
    static let directPromoRedeem = "RestService.svc/DirectPromoRedeem"
    static let offerOneTransactionPayment = "RestService.svc/PromotionPayment"
    
    //push notifications APi
    static let pushNotificationApi = "//RestService.svc//Update_GCMID"
    
    // push notifications APi Direct Estel API
    static let Estel_Notification_API = "/WebServiceIpay/services/request;requesttype=APPREG;agentcode=%@;vendorcode=IPAY;clienttype=GPRS;comments=%@;pin=%@;regid=%@;securetoken=%@;devicetype=iOS"
    
    //MARK:- LoyaltyTopup
    static let getSellingTopup       = "RestService.svc/RedeempointsSellingBySource"
    static let getHistoryByCategory  = "/RestService.svc/GetRedeemPointsHistoryByCategory"
    static let genericPayment        = "/RestService.svc//GenericPayment"
    static let bonusPointPay         = "/RestService.svc/RedeempointsSellingBySource"
    static let sellTopup             = "WebServiceIpay/services/request;requesttype=LOYALTYVIEW;agentcode=%@;vendorcode=IPAY;clientip=;clientos=ios;clienttype=SELFCARE;securetoken=%@"
    static let getAddedBanksID       = "/RestService.svc/GetBankDetails?MobileNumber=%@&Simid=%@&MSID=%@&OSType=1&OTP=%@"
    static let clearerAddedBanks      = "/BankPayments.svc/InactiveBankAccDetails"
    static let numberValidation     = "RestService.svc/ValidateAppVersion?OsVersion=%@&MobileNumber=%@&SimId=%@&Version=%@&MsId=%@&OsType=%@"
    
    //MARK:- Donation Apis
    static let donation = "/home/index"
    
    //MARK: - Voting
    static let votingToken = "https://votingapi.okdollar.org/token"
    
    //MARK: - New - Cashin and cashout apis
    static let authServiceLogin                 = "/AuthService/Login"
    static let cashInOutCommisionUrl            = "/Home/Info"
    static let cashInOutCommisionUrlMY          = "/Home/Info_burmese"
    static let cashInOutCommisionUrlUN          = "/Home/Info_burmese_unicode"
    static let agentsList                       = "/UserService/SearchAgents"
    static let updateUserLocation               = "/UserService/UpdateUserLocation"
    static let acceptedCashTransferList         = "/CashInOutService/ListAcceptedCashTransferMasters"
    static let pendingCashTransferList          = "/CashInOutService/ListPendingTransferRequests"
    static let successCashTransferList          = "/CashInOutService/ListTransferSuccessCashTransferMasters"
    static let allCashTransferList              = "/CashInOutService/ListAllCashTransferMasters"
    static let updateUser                       = "/UserService/UpdateUser"
    static let bookCashTransfer                 = "/CashInOutService/BookCashTransferRequest"
    static let cashTransferReqStatus            = "/CashInOutService/GetCashTransferRequestStatus"
    static let pushReceived                     = "/CashInOutService/PushReceived"
    static let getCashTransferRequestDetails    = "/CashInOutService/GetCashTransferRequestDetails"
    static let cashTrasferAccept                = "/CashInOutService/AcceptCashTransferRequest"
    static let cashTransferReject               = "/CashInOutService/RejectCashTrasnferRequest"
    static let getMasterDetail                  = "/CashInOutService/GetCashTransferMasterDetails"
    static let updateUserLocWhileTravel         = "/UserService/UpdateLocationWhileTravel"
    static let cancelCashTransfer               = "/CashInOutService/CancelCashTransfer"
    static let startTrip                        = "/CashInOutService/StartTrip"
    static let endTrip                          = "/CashInOutService/EndTripNew"
    static let genericPaymentCashinout          = "/RestService.svc/GenericPayment"
    static let rateCustomerOrAgent              = "/CashInOutService/RateCustomerOrAgent"
    static let completePayment                  = "/CashInOutService/CompletePaymentNew"
    static let createUser                       = "/UserService/CreateUser"
    static let reportCICOList                   = "/CashInOutService/ListCashTransferReport"
    
    //MARK: - InstaPay
    static let imagesByPhNumbers                = "//AdService.svc/GetProfilePicOrBusinessPicBymobileNumber"
    static let okWalletSizeURL                  = "RestService.svc/GetWalletsizeapiByMobilenumber"
   // static let mobileCategory                = "RestService.svc/GetCategoryAndMercNameByMobileNumber?MobileNumber=%@"
    
    //CBBank payments
    static let cbGenerateToken                  = "/v1/auth/OKToken"
    static let cbBankVerify                     = "/v1/VerifyBankAccount"
    static let cbBankTransfer                   = "/v1/WalletToBankTransfer"
    static let cbBankAssociation                = "/v1/BankAccountAssociation"
    //Secure Meter
    static let aKey_SecureMeter = "f85974c7076095282eb21e02b2889734bd5af86593984f6b6f449472c8844901fb9607493442503a11a22a1cd6fb2ff53caae95e20ac721fbae0ba94fdbccfa4904eb339e0fde989b7d20e01b60ecd1944cc4d4568e61580c7a199ae4e3782c880878185cc479c30f980db41bb3f32d8bcde85790135d40e30032819a685c534e87113a73cf6f2a048911aa8c99447cfb9a9c37fb47025ec4bc55665c0c2c663"
    
    static let sKey_SecureMeter = "9926d71687b6bc2c9699c3349fdc17fb3dfbba6224affb7676e1337926cdd602"
    
    //Loyalty Reports
    static let earnPointTrfReportUrl  = "WebServiceIpay/services/request;requesttype=LOYALITYPOINTREPORT;agentcode=%@;dateto=%@;subscriber=;record=50;vendorcode=IPAY;clientip=%@;clientos=iOS;clienttype=GPRS;securetoken=%@;datefrom=%@;reportname=leptreport"
    static let pointDistributionReportUrl  = "WebServiceIpay/services/request;requesttype=LOYALITYPOINTREPORT;agentcode=%@;dateto=%@;subscriber=;record=50;vendorcode=IPAY;clientip=%@;clientos=iOS;clienttype=GPRS;securetoken=%@;datefrom=%@;reportname=mlpdreport"
    static let redeempointsReportUrl  = "WebServiceIpay/services/request;requesttype=LOYALITYPOINTREPORT;agentcode=%@;dateto=%@;subscriber=;record=50;vendorcode=IPAY;clientip=%@;clientos=iOS;clienttype=GPRS;securetoken=%@;datefrom=%@;reportname=mlprreport"
    static let mitPayemntUrl = "http://69.160.4.151:8090/RestService.svc/DoPayment"
    
    static let bankdetailsUrl =  "okdollar/v1/Wallet/BankDetails"
   // static let bankVerifyWalletNumber = "okdollar/v1/Wallet/VerifyWalletNumber"
   // static let bankfeescheck = "okdollar/v1/Wallet/FeesCheck"
   // static let bankwalletTopup = "okdollar/v1/Wallet/BankWalletTopup"
    
    static let bankVerifyWalletNumber = "okdollar/v2/Wallet/VerifyWalletNumberV2"
    static let bankfeescheck = "okdollar/v2/Wallet/FeesCheckV2"
    static let bankwalletTopup = "okdollar/v2/Wallet/BankWalletTopupV2"
    
    static let upayIntGenerateQR = "/GenerateQr"
    static let upayIntEnrollment = "/CARD_ENROLLMENT"
    static let upayIntQRInfo = "/MPQRCINFO"
    static let upayIntExchangeRate = "/ExchangeRateInquiry"
    static let upayIntPayment = "/MPQRCPaymentEMV"

}

public enum ServerType {
    case bonusPointApi
    case serverApp
    case faceIDPay
    case updateProfile
    case serverEstel
    case promotionUrl
    case cashoutUrl
    case otherPayment
    case imageUpload
    case addwithdrawCashInCashOutUrl
    case helpandSupportUrl
    case airtimeUrl
    case businessLocationUrl
    case sendMoneyBankReportUrl
    case nearByServicesUrl
    case votingReportUrl
    case solarReportUrl
    case cashInCashOutCommisionUrl
    case cashInCashOutUrl
    case bankCounterDepositUrl
    case mediaUploadFilesUrl
    case donationUrl
    case addWithdrawUrl
    case taxation
    case cashInOutPayto
    case instaPay
    case eCommerce
    case giftCard
    case dth
    case pay123
    case AddMoney_PaymentMPU
    case AddMoney_PaymentCB
    case AddMoney_PaymentKBZ
    case AddMoney_PaymentVisaMaster
    case AddMoney_PaymentOneTwoThree
    case sendSaleNLockOTP
    case AddMoney_GetToken
    case AddMoney_GetCharges
    case SaleNLockServerEstel
    case ListUserContacts
    case CBBankPay
    case UPayInt
    case SMBBankList
    case UnionPayBankDetailsapi
    
}

public func getUrl(urlStr: String, serverType: ServerType) -> URL {
    var bonusPointUrl = ""
    var serverApp    = ""
    var faceIDPay    = ""
    var updateProfile = ""
    var serverEstel  = ""
    var promotionUrl = ""
    var cashoutUrl   = ""
    var otherPayment = ""
    var imageUploadUrl = ""
    var addWithdrawCashInCashOutUrl = ""
    var airtimeUrl = ""
    var businessLocationUrl = ""
    var sendMoneyUrl = ""
    var helpandSupportUrl = ""
    var nearByServicesUrl = ""
    var votingReportUrl = ""
    var solarReportUrl = ""
    var bankCounterDepositUrl = ""
    var cashInCashOutComissionUrl = ""
    var cashInCashOutUrl = ""
    var mediaUploadFilesUrl = ""
    var donationUrl = ""
    var addWithdrawUrl = ""
    var taxationUrl = ""
    var cashInOutPayto = ""
    var instaPayUrl = ""
    var giftCard = ""
    var dth = ""
    var pay123 = ""
    var eCommerce = ""
    var AddMoney_PaymentMPU = ""
    var AddMoney_PaymentCB = ""
    var AddMoney_PaymentKBZ = ""
    var AddMoney_PaymentVisaMaster = ""
    var AddMoney_PaymentOneTwoThree = ""
    var sendSaleNLockOTP = ""
    var AddMoney_GetToken = ""
    var AddMoney_GetCharges = ""
    var SaleNLockServerEstel = ""
    var ListUserContacts = ""
    var cbBankPay = ""
    var upayInt = ""
    var smbBankListUrl = ""
    // vamsi union Pay Url's
    var UnionPayBankDetailsapi = ""
   
    
    func enable(urlTo: UrlTypes) {
        switch urlTo {
        case .productionUrl:
            //            http://test.donation.okdollar.org
            sendSaleNLockOTP = "http://advertisement.api.okdollar.org"//"http://69.160.4.151:8002"
            AddMoney_GetToken = "https://addmoneyapi.okdollar.org/token"
            AddMoney_GetCharges = "https://addmoneyapi.okdollar.org/banks/charges/all"
            AddMoney_PaymentMPU = "https://mobileview.okdollar.org/netbanking/Mpu/Index"
            AddMoney_PaymentCB = "https://mobileview.okdollar.org/Netbanking/CbBank/Index"
            AddMoney_PaymentKBZ = "https://mobileview.okdollar.org/netbanking/KbzBank/Index"
            AddMoney_PaymentVisaMaster = "https://mobileview.okdollar.org/Netbanking/VisaMasterPay/Index"
            AddMoney_PaymentOneTwoThree = "https://mobileview.okdollar.org/AddMoney123/AddMoney123/Index"
            bonusPointUrl = "http://bonuspointapi.okdollar.org/"
            serverApp    = "https://www.okdollar.co/"
            faceIDPay    = "http://www.okdollarapp.com:3002/" //http://52.76.209.187:3002//"http://www.okdollarapp.com:3002/"
            updateProfile = "http://kycadminapi.okdollar.org/UpdateProfile.svc/"
            serverEstel  = "https://www.okdollar.net/"
            SaleNLockServerEstel = "http://www.okdollar.net/"
            ListUserContacts = "http://cashincashout.okdollar.org:1318/UserService/ListUserContacts"
            promotionUrl = "http://120.50.43.152:9090/"
            cashoutUrl   = "https://mobileview.okdollar.org/cashin/home/?/"
            otherPayment = "https://paymentgateway.okdollar.org/"
            imageUploadUrl = "http://192.168.1.13:8001"
            addWithdrawCashInCashOutUrl = "https://cashincashout.okdollar.org"
            airtimeUrl = "https://airtimeapi.okdollar.org"
            businessLocationUrl = "http://120.50.43.157:1318/"
            sendMoneyUrl = "https://www.okdollar.co/"
            helpandSupportUrl = "http://callcenter.okdollar.org:1318"
           // nearByServicesUrl = "http://120.50.43.157:1318/UserService/SearchAgentsForOkDollar"
            nearByServicesUrl =  "http://cashincashout.okdollar.org:1318/UserService/SearchUserContactsForOkDollar"
            votingReportUrl = "https://votingapi.okdollar.org/votingdetails/"
            solarReportUrl = "https://solarhometopupapi.okdollar.org/"
            bankCounterDepositUrl = "https://counterdepositapi.okdollar.org/"
            cashInCashOutComissionUrl = "http://120.50.43.157:5555"
            cashInCashOutUrl = "http://cashincashout.okdollar.org:1318" //http://120.50.43.157:1318"
            mediaUploadFilesUrl = "http://media.api.okdollar.org/"
            donationUrl = "http://donation.okdollar.org"
            addWithdrawUrl = "https://addmoneyapi.okdollar.org"
            taxationUrl = "http://tax-corporateapi.okdollar.org/" // "http://test.tax-corporateapi.okdollar.org/"
            cashInOutPayto = "http://cashincashout.okdollar.org:1318/CashInOutService/"
            instaPayUrl = "http://69.160.4.151:8002"
            giftCard = "https://mobileview.okdollar.org/GiftCards/GiftCard/Index"
            dth = "https://mobileview.okdollar.org/Dth/DthBill/Index"
            pay123 = "https://mobileview.okdollar.org/AddMoney123/AddMoney123/Index"
            eCommerce = "http://69.160.4.149:1001/"
            cbBankPay = "https://www.okdollarapp.com/okdollar"
            upayInt = "http://www.okcgm.com/UPI"
            smbBankListUrl = "http://advertisement.api.okdollar.org/"
            // vamsi unionpay
            UnionPayBankDetailsapi = "https://www.okdollarapp.com/"
            
            
        case .testUrl:
            sendSaleNLockOTP = "http://69.160.4.151:8002"
            AddMoney_GetToken = "http://test.addmoney.api.okdollar.org/token"
            AddMoney_GetCharges = "http://test.addmoney.api.okdollar.org/banks/charges/all"
           AddMoney_PaymentMPU = "http://test.mobileview.okdollar.org/netbanking/Mpu/Index"
            AddMoney_PaymentCB = "http://test.mobileview.okdollar.org/Netbanking/CbBank/Index"
           AddMoney_PaymentKBZ = "http://test.mobileview.okdollar.org/netbanking/KbzBank/Index"
            AddMoney_PaymentVisaMaster = "http://test.mobileview.okdollar.org/Netbanking/VisaMasterPay/Index"
            AddMoney_PaymentOneTwoThree = "http://test.mobileview.okdollar.org/AddMoney123/AddMoney123/Index"
            bonusPointUrl = "http://test.bonuspoint.api.okdollar.org/"
            updateProfile = "http://69.160.4.151:8085/UpdateProfile.svc/"
            serverApp    = "http://69.160.4.151:8001/"
            faceIDPay    = "http://18.141.12.199:3002/"//"http://52.76.209.187:3002/"
            serverEstel  = "http://120.50.43.150:8090/"//"http://120.50.43.150:8090/"
            SaleNLockServerEstel = "http://120.50.43.150:8090/"
            ListUserContacts = "http://cashincashout.okdollar.org:1313/UserService/ListUserContacts"
            promotionUrl = "http://122.248.120.221:8009/"
            cashoutUrl   = "http://test.mobileview.okdollar.org/Cashin/Home/?/"
            otherPayment = "http://69.160.4.151:8001/"
            imageUploadUrl = "http://69.160.4.151:8001" //http://192.168.1.13:8001 //103.242.99.234:8001
            addWithdrawCashInCashOutUrl = "http://test.cashincashout.okdollar.org"
            airtimeUrl = "https://airtimeapi.okdollar.org"
            businessLocationUrl = "http://120.50.43.157:1318/"
            sendMoneyUrl = "http://69.160.4.151:8001/"
            helpandSupportUrl = "http://callcenter.okdollar.org:1318"
            nearByServicesUrl =  "http://cashincashout.okdollar.org:1318/UserService/SearchUserContactsForOkDollar"
            votingReportUrl = "https://votingapi.okdollar.org/votingdetails/"
            solarReportUrl = "http://test.solarhometopup.api.okdollar.org/"
            bankCounterDepositUrl = "http://test.counterdeposit.api.okdollar.org/"
            cashInCashOutComissionUrl = "http://120.50.43.157:5555"
            cashInCashOutUrl = "http://120.50.43.157:1318"
            mediaUploadFilesUrl = "http://media.api.okdollar.org/"
            donationUrl = "http://test.donation.okdollar.org"
            addWithdrawUrl = "http://test.addmoney.api.okdollar.org"
            taxationUrl = "http://tax-corporateapi.okdollar.org/"
            cashInOutPayto = "http://cashincashout.okdollar.org:1313/CashInOutService/"
            instaPayUrl = "http://69.160.4.151:8002"
            giftCard = "http://test.mobileview.okdollar.org/GiftCards/GiftCard/Index"
            eCommerce = "http://69.160.4.149:1001/"
            dth = "http://test.mobileview.okdollar.org/Dth/DthBill/Index"
            pay123 = "http://69.160.4.151:5000/AddMoney123/AddMoney123/Index"
            cbBankPay = "https://www.okdollarapp.com/okdollar"
            upayInt = "http://www.okcgm.com/UPI"
            smbBankListUrl = "69.160.4.151:8002/"
            
            // vamsi unionpay
            UnionPayBankDetailsapi = "http://api.sandbox.okdollarapp.com/"
           
        }
    }
    
    #if DEBUG
    //Production Server
    enable(urlTo: serverUrl)
    #else
    //Test server
    //Earlier rest service host address with port  : 103.242.99.234:8001 or 192.168.1.13
    // New  rest service host address with port  :    69.160.4.151:8001
    enable(urlTo: .productionUrl)

    #endif
    
    var urlPrefix = ""
    
    switch serverType {
    case .serverApp:
        urlPrefix = serverApp
    case .faceIDPay:
        urlPrefix = faceIDPay
    case .serverEstel:
        urlPrefix = serverEstel
    case .updateProfile:
        urlPrefix = updateProfile
    case .promotionUrl:
        urlPrefix = promotionUrl
    case .cashoutUrl:
        urlPrefix = cashoutUrl
    case .otherPayment:
        urlPrefix = otherPayment
    case .imageUpload:
        urlPrefix = imageUploadUrl
    case .addwithdrawCashInCashOutUrl:
        urlPrefix = addWithdrawCashInCashOutUrl
    case.airtimeUrl:
        urlPrefix = airtimeUrl
    case .helpandSupportUrl:
        urlPrefix = helpandSupportUrl //Vinnu
    case .businessLocationUrl:
        urlPrefix = businessLocationUrl //kethan
    case .sendMoneyBankReportUrl:
        urlPrefix = sendMoneyUrl
    case .nearByServicesUrl:
        urlPrefix = nearByServicesUrl
    case .bonusPointApi:
        urlPrefix = bonusPointUrl
    case .votingReportUrl:
        urlPrefix = votingReportUrl
    case .solarReportUrl:
        urlPrefix = solarReportUrl
    case .cashInCashOutCommisionUrl:
        urlPrefix = cashInCashOutComissionUrl
    case .cashInCashOutUrl:
        urlPrefix = cashInCashOutUrl
    case .bankCounterDepositUrl:
        urlPrefix = bankCounterDepositUrl //Gauri
    case .mediaUploadFilesUrl:
        urlPrefix = mediaUploadFilesUrl //Gauri
    case .donationUrl:
        urlPrefix = donationUrl
    case .addWithdrawUrl:
        urlPrefix = addWithdrawUrl
    case .taxation:
        urlPrefix = taxationUrl
    case .cashInOutPayto:
        urlPrefix = cashInOutPayto
    case .instaPay:
        urlPrefix = instaPayUrl
    case .giftCard:
        urlPrefix = giftCard
    case .dth:
        urlPrefix = dth
    case .pay123:
        urlPrefix = pay123
    case .eCommerce:
        urlPrefix = eCommerce
    case .AddMoney_PaymentMPU:
        urlPrefix = AddMoney_PaymentMPU
    case .AddMoney_PaymentCB:
        urlPrefix = AddMoney_PaymentCB
    case .AddMoney_PaymentKBZ:
        urlPrefix = AddMoney_PaymentKBZ
    case .AddMoney_PaymentVisaMaster:
        urlPrefix = AddMoney_PaymentVisaMaster
    case .AddMoney_PaymentOneTwoThree:
        urlPrefix = AddMoney_PaymentOneTwoThree
    case .sendSaleNLockOTP:
        urlPrefix = sendSaleNLockOTP
    case .AddMoney_GetToken:
        urlPrefix = AddMoney_GetToken
    case .AddMoney_GetCharges:
        urlPrefix = AddMoney_GetCharges
    case .SaleNLockServerEstel:
        urlPrefix = SaleNLockServerEstel
    case .ListUserContacts:
        urlPrefix = ListUserContacts
    case .CBBankPay:
        urlPrefix = cbBankPay
    case .SMBBankList:
        urlPrefix = smbBankListUrl
    case .UPayInt :
        urlPrefix = upayInt
    case .UnionPayBankDetailsapi:
         urlPrefix = UnionPayBankDetailsapi
    }
    
    let urlStrData = urlStr.removingWhitespaces()
    let urlString = "\(urlPrefix)" + "\(urlStrData)"
    let url = URL.init(string: urlString)
    return url!
}
