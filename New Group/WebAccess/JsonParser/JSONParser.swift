//
//  JSONParser.swift
//  OfferScreen
//
//  Created by CGM on 5/10/17.
//  Copyright © 2017 CGM. All rights reserved.
//

import Foundation
import UIKit

// MARK : GLOBAL Functions
func println_debug <OK> (_ object: OK) {
    #if DEBUG
        print(object)
    #endif
}

class JSONParser: NSObject, URLSessionDelegate {
    
    static func PrepareJsonRequest(apiUrl: URL,methodType: String,contentType: String, inputVal: Any?, authStr: String?) -> URLRequest {
        
        var request = URLRequest.init(url: apiUrl, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60)
        request.httpMethod = methodType
        request.addValue(contentType, forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        if let authstr = authStr {
            request.addValue(authstr, forHTTPHeaderField: "Authorization")
        }
        
        if let inputstr = inputVal as? String {
            request.addValue(String(describing: inputstr.count), forHTTPHeaderField: "Content-Length")
            let data = inputstr.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue), allowLossyConversion: true)
            request.httpBody = data

        } else if let inputDic = inputVal as? NSDictionary {
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: inputDic, options: JSONSerialization.WritingOptions.prettyPrinted)
            }catch {
                println_debug("some error comes when prepare json request")
            }
        }
 
        return request
    }
    
    static func GetApiResponseWithRequest(apiUrlReq: URLRequest, _ completionHandler: @escaping (Bool, Any?) -> Void) {
        URLSession.shared.dataTask(with: apiUrlReq) { (data: Data?, response: URLResponse?, error: Error?) in
            do {
                guard data != nil || error == nil else {
                    completionHandler(false, nil)
                    return
                }
 
                let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
                completionHandler(true, json)
 
            }catch {
                completionHandler(false, nil)
            }
            
        }.resume()
    }
    
    static func GetResaleApiResponse(apiUrlReq: URLRequest, _ completionHandler: @escaping (Bool, Any?) -> Void) {
        URLSession.shared.dataTask(with: apiUrlReq) { (data: Data?, response: URLResponse?, error: Error?) in
            do {
                guard data != nil || error == nil else {
                    completionHandler(false, nil)
                    return
                }
                
                let httpResponse = response as! HTTPURLResponse
                guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                    completionHandler(false, nil)
                    return
                }
                
                let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
                completionHandler(true, json)
                
            }catch {
                completionHandler(false, nil)
            }
            
            }.resume()
    }

    
    static func GetApiResponse(apiUrl: URL, type : String, compilationHandler : @escaping  (Bool, Any?) -> Void){
        
        var request = URLRequest.init(url: apiUrl, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60)
        _ = URLSession.shared.configuration
        if type == "GET"
        {
            request.httpMethod = "GET"
        }
        
        URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            do {
                
                guard data != nil || error == nil else {
                    compilationHandler(false, nil)
                    return
                    
                }
                let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                
                print("json data-----\(json)")
                
                if (json["Msg"] as? String)?.lowercased() == "sucess" || (json["Msg"] as? String)?.lowercased() == "success" {
                    let resData: Data? = (json[kJSONData] as! String).data(using: String.Encoding.utf8)!
                    
                    guard resData != nil else {
                        println_debug("Hello")
                        compilationHandler(false, nil)
                        return
                    }
                    println_debug(resData)
                    // convert NSData to 'AnyObject'
                    let anyObj = try JSONSerialization.jsonObject(with: resData!, options: JSONSerialization.ReadingOptions.allowFragments)
                    println_debug(anyObj)
                    compilationHandler(true, anyObj)
                }else{
                   
                    compilationHandler(false, json["Msg"])
                }
                
            }catch {
                compilationHandler(false, nil)
            }
        }.resume()
    }
    
    static func GetCancelApiResponse(apiUrl: URL, type : String, compilationHandler : @escaping  (Bool, Any?) -> Void){
        
        var request = URLRequest.init(url: apiUrl, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60)
        _ = URLSession.shared.configuration
        if type == "GET" {
            request.httpMethod = "GET"
        }
        URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            do {
                
                guard data != nil || error == nil else {
                    compilationHandler(false, nil)
                    return }
                let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                
                if (json["Msg"] as? String) == "Success" || (json["Msg"] as? String) == "Sucess"{
                    compilationHandler(true, json)
                }else{
                    compilationHandler(false, json["Msg"])
                }
                
            }catch {
                compilationHandler(false, "")
            }
        }.resume()
    }
}

class OfferTableCell: UITableViewCell {
    
    @IBOutlet var offerTitle_lbl: UILabel?
    @IBOutlet var offerTC_lbl: UILabel?
    @IBOutlet var offerDesc_lbl: UILabel?
    @IBOutlet var offerDesc_img: UIImageView?
    @IBOutlet var offerDesc_btn: UIButton?
    @IBOutlet var offerContentView: UIView?
    
}

class OfferCategoryCell: UICollectionViewCell {
    @IBOutlet var lbl_categoryName: UILabel?
}
