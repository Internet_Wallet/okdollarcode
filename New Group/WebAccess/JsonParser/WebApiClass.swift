 //
 //  WebApiClass.swift
 //  OKPay
 //
 //  Created by Ashish Kr Singh on 26/07/17.
 //  Copyright © 2017 Ashish Kr Singh. All rights reserved.
 //
 
 import UIKit
 
 protocol WebServiceResponseDelegate : class {
    func webResponse(withJson json: AnyObject, screen: String)
    func webErrorResponse(withErrorCode: Int, screenName: String)
 }
 
 protocol WebServiceDataWithResponseDelegate: class {
    func webResponse(withJson json: Data?, response: URLResponse?, error: Error?, screen: String)
 }
 
 extension WebServiceResponseDelegate {
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        println_debug("Error Code: \(withErrorCode)")
    }
 }
 
 class WebApiClass: NSObject {
    
    weak var delegate : WebServiceResponseDelegate?
    weak var delegateWithAllResponse: WebServiceDataWithResponseDelegate?
    
    func genericClass(url: URL, param: AnyObject, httpMethod: String, mScreen: String, hbData: Data? = nil, authToken: String? = nil, authTokenKey: String? = nil, isUrlEncoded: Bool = false) {
        
        println_debug(param)
        
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 60
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        if !isUrlEncoded {
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            if httpMethod == "POST" {
                let theJSONData = try? JSONSerialization.data(withJSONObject: param , options: JSONSerialization.WritingOptions(rawValue: 0))
                let jsonString          = NSString(data: theJSONData!,encoding: String.Encoding.ascii.rawValue)
                let postLength = NSString(format:"%lu", jsonString!.length) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:true)
                if let httpBodyData = hbData {
                    request.httpBody = httpBodyData
                }
            }
        } else {
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            let postLength = "\(hbData?.count ?? 0)"
            request.setValue(postLength, forHTTPHeaderField:"Content-Length")
            if let httpBodyData = hbData {
                request.httpBody = httpBodyData
            }
        }
        request.httpMethod = httpMethod.capitalized
        if let authStr = authToken {
            if let authKey = authTokenKey {
                request.setValue(authStr, forHTTPHeaderField: authKey)
            } else {
                request.setValue(authStr, forHTTPHeaderField: "Authorization")
            }
        }
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            PTLoader.shared.hide()
            
            self.delegateWithAllResponse?.webResponse(withJson: data, response: response, error: error, screen: mScreen)
            if let errorResponse = error {
                println_debug(errorResponse.localizedDescription)
            } else {
                if let httpStatus = response as? HTTPURLResponse {
                  //  print("status code----\(httpStatus.statusCode)")
                   // print("response code----\(response)")
                   // print("httpStatus code----\(httpStatus)")

                    if httpStatus.statusCode == 200 {
                        guard (self.delegate?.webResponse(withJson: data as AnyObject, screen: mScreen) != nil) else { return }
                    } else if httpStatus.statusCode == 201 {
                        guard (self.delegate?.webResponse(withJson: data as AnyObject, screen: mScreen) != nil) else { return }
                    } else if httpStatus.statusCode == 302 && (mScreen == "Report Solar Electricity" || mScreen == "Report Voting List" || mScreen == "ScanBarCode") {
                        guard (self.delegate?.webResponse(withJson: data as AnyObject, screen: mScreen) != nil) else { return }
                    } else if httpStatus.statusCode == 404 && (mScreen == "ScanBarCode") {
                        guard (self.delegate?.webResponse(withJson: data as AnyObject, screen: mScreen) != nil) else { return }
                    } else {
                      //  println_debug(httpStatus.statusCode)
                        self.delegate?.webErrorResponse(withErrorCode: httpStatus.statusCode, screenName: mScreen)
                    }
                } else {
                    println_debug("No Response")
                }
            }
        }
        dataTask.resume()
    }
    
    func genericClassReg(url: URL, param: Dictionary<String , Any>, httpMethod: String, mScreen: String, hbData: Data? = nil, authToken: String? = nil) {
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 60
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = httpMethod.capitalized
        
        if httpMethod == "POST" {
            do {
                
                if let data = hbData {
                    request.httpBody = data
                } else {
                    let jsonData = try JSONSerialization.data(withJSONObject: param, options: [])
                    let strData = String(data: jsonData, encoding: .utf8)
                    // let strData = NSString(data: jsonData, encoding: String.Encoding.utf8)
                    // let postLength = NSString(format:"%lu",(strData?.length)!)
                    //  request.setValue(postLength as String , forHTTPHeaderField:"Content-Length")
                    // let data = strData?.data(using: String.Encoding.utf8)
                    
                    request.httpBody = strData?.data(using: .utf8)
                }
            } catch {
                PTLoader.shared.hide()
                print(error.localizedDescription)
            }
        }
        if let authStr = authToken {
            request.setValue(authStr, forHTTPHeaderField:"Authorization")
        }
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if((error) != nil) {
                PTLoader.shared.hide()
                println_debug(error!.localizedDescription)
            }else {
                PTLoader.shared.hide()
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                    guard (self.delegate?.webResponse(withJson: data as AnyObject, screen: mScreen) != nil) else { return }
                } else {
                    if let httpStatus = response as? HTTPURLResponse {
                        println_debug(httpStatus.statusCode)
                    }
                }
            }
        }
        dataTask.resume()
    }
    
    
    func genericClassUpdateProfile(url: URL, param: Dictionary<String , Any>, httpMethod: String, mScreen: String, hbData: Data? = nil, authToken: String? = nil) {
         let session             = URLSession.shared
         let request             = NSMutableURLRequest(url:url)
         request.timeoutInterval = 60
         request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
         request.addValue("application/json", forHTTPHeaderField: "Content-Type")
         request.setValue("application/json", forHTTPHeaderField: "Accept")
         request.httpMethod = httpMethod.capitalized
         
         if httpMethod == "POST" {
             do {
                 
                 if let data = hbData {
                     request.httpBody = data
                 } else {
                     let jsonData = try JSONSerialization.data(withJSONObject: param, options: [])
                     //let strData = String(data: jsonData, encoding: .nonLossyASCII)
                     request.httpBody = jsonData
                 }
             } catch {
                 PTLoader.shared.hide()
                 print(error.localizedDescription)
             }
         }
         if let authStr = authToken {
             request.setValue(authStr, forHTTPHeaderField:"Authorization")
         }
         let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
             if((error) != nil) {
                 PTLoader.shared.hide()
                 println_debug(error!.localizedDescription)
             }else {
                 PTLoader.shared.hide()
                 if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                     guard (self.delegate?.webResponse(withJson: data as AnyObject, screen: mScreen) != nil) else { return }
                 } else {
                     if let httpStatus = response as? HTTPURLResponse {
                         println_debug(httpStatus.statusCode)
                     }
                 }
             }
         }
         dataTask.resume()
     }
    
    func genericClassReg1(url: URL, param: AnyObject, httpMethod: String, mScreen: String, hbData: Data? = nil, authToken: String? = nil) {
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 60
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = httpMethod.capitalized
        
        if httpMethod == "POST" {
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: param, options:JSONSerialization.WritingOptions(rawValue: 0))
                let strData = String(data: jsonData, encoding: .utf8)
                let postLength = NSString(format:"%lu", strData!.length) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = strData?.data(using: .utf8, allowLossyConversion: false)
            } catch {
                PTLoader.shared.hide()
                print(error.localizedDescription)
            }
        }
        
        if let authStr = authToken {
            request.setValue(authStr, forHTTPHeaderField:"Authorization")
        }
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if((error) != nil) {
                PTLoader.shared.hide()
                println_debug(error!.localizedDescription)
            }else {
                PTLoader.shared.hide()
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                    guard (self.delegate?.webResponse(withJson: data as AnyObject, screen: mScreen) != nil) else { return }
                } else {
                    
                    if let httpStatus = response as? HTTPURLResponse {
                        println_debug(httpStatus.statusCode)
                        
                    }
                }
            }
        }
        dataTask.resume()
    }
    
    
    func genericClassRegLoyalty(url: URL, param: Dictionary<String , Any>, httpMethod: String, mScreen: String, hbData: Data? = nil, authToken: String? = nil) {
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 60
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = httpMethod.capitalized
        
        if httpMethod == "POST" {
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: param, options: [])
                let strData = String(data: jsonData, encoding: .utf8)
                
                // let strData = NSString(data: jsonData, encoding: String.Encoding.utf8)
                // let postLength = NSString(format:"%lu",(strData?.length)!)
                //  request.setValue(postLength as String , forHTTPHeaderField:"Content-Length")
                // let data = strData?.data(using: String.Encoding.utf8)
                
                request.httpBody = strData?.data(using: .utf8)
            } catch {
                PTLoader.shared.hide()
                print(error.localizedDescription)
            }
        }
        if let authStr = authToken {
            request.setValue(authStr, forHTTPHeaderField:"Authorization")
        }
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if((error) != nil) {
                PTLoader.shared.hide()
                println_debug(error!.localizedDescription)
            }else {
                PTLoader.shared.hide()
                guard (self.delegate?.webResponse(withJson: data as AnyObject, screen: mScreen) != nil) else { return }
            }
        }
        dataTask.resume()
    }
 }
