//
//  ParserAttributeClass.swift
//  Practice Project
//
//  Created by Sankalap Singh on 10/12/16.
//  Copyright © 2016 Sankalap Singh. All rights reserved.
//

import Foundation
import UIKit

class ParserAttributeClass: NSObject {
    
    class var sharedInstance: ParserAttributeClass {
        
        struct Static {
            static let instance = ParserAttributeClass()
        }
        return Static.instance
    }
   
    
    /*
     class func parseJSON(anyValue: AnyObject) -> [JSONModel]! {
     var mydata = [JSONModel]()
     var value: Int = 0
     while value < anyValue.count {
     let mydataValue = JSONModel.init(dictionary: anyValue.object(at: value) as! Dictionary)
     mydata.append(mydataValue)
     value = value + 1
     }
     return mydata
     }
     
     class func parseBillJSON(anyValue: AnyObject) -> [JSONBillModel]! {
     var mydata = [JSONBillModel]()
     var value: Int = 0
     while value < anyValue.count {
     let mydataValue = JSONBillModel.init(dictionary: anyValue.object(at: value) as! Dictionary)
     mydata.append(mydataValue)
     value = value + 1
     }
     return mydata
     }
     
     */
    
    static func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    class func parseNearbyServiceJSONNew(anyValue: AnyObject) -> [NearByServicesNewModel] {
        var mydata = [NearByServicesNewModel]()
        if let dictionary = anyValue["Content"] as? [Dictionary<String, AnyObject>] {
            for dic in dictionary {
                let cashinout = NearByServicesNewModel.init(dictionary: dic)
                mydata.append(cashinout)
            }
        }
        return mydata
    }
    
    class func parseprofilepicServiceJSONNew(anyValue: AnyObject) -> [ProfilePicModel] {
        var mydata = [ProfilePicModel]()
        
        if let dictionary = anyValue["Data"] as? String {
        
            if let list = self.convertToDictionary(text: dictionary) as? [AnyObject] {
                for dic in list {
                    let cashinout = ProfilePicModel.init(dictionary: dic as! Dictionary<String, AnyObject>)
                                   mydata.append(cashinout)
                }
              
            }
     
          
        }
        return mydata
    }
    
 class  func convertToDictionary(text: String) -> Any? {
        
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? Any
            } catch {
                print(error.localizedDescription)
            }
        }
        
        return nil
        
    }
    
    class func parseNearbyServiceJSON(anyValue: AnyObject) -> [NearByServicesModel] {
        var mydata = [NearByServicesModel]()
        if let dictionary = anyValue["Content"] as? [Dictionary<String, AnyObject>] {
            for dic in dictionary {
                let cashinout = NearByServicesModel.init(dictionary: dic)
                mydata.append(cashinout)
            }
        }
        return mydata
    }
    
    class func parseCashINJSON(anyValue: AnyObject, cashInType: Int) -> [CashInOutModel]! {
        var mydata = [CashInOutModel]()
        var value: Int = 0
        while value < anyValue.count {
            let cashinout = CashInOutModel.init(dictionary: anyValue.object(at: value) as! Dictionary)
            mydata.append(cashinout)
            value += 1
        }
      
        return mydata
    }
    
    class func parseCashInOutJSON(anyValue: AnyObject, cashInType: Int) -> [CashInOutModel]! {
        var mydata = [CashInOutModel]()
        var value: Int = 0
        
        while value < anyValue.count {
            let mydataValue = CashInOutModel.init(dictionary: anyValue.object(at: value) as! Dictionary)
            mydata.append(mydataValue)
            value += 1
        }
        return mydata
    }
    
    
    
    class func parseCashInCashOutAllJSON(anyValue: AnyObject) -> [CashInOutModel]! {
        var mydata = [CashInOutModel]()
        var value: Int = 0
        
        let keys = ["AgentList","BankList","OkOfficeList"]
      
        while value < anyValue.count {
            
            let objectValue: [AnyObject] = anyValue.value(forKey: keys[value]) as! Array
            if (objectValue.count > 0) {
                var innerValue: Int = 0
                while innerValue < objectValue.count {
                    let mydataValue = CashInOutModel.init(dictionary: objectValue[innerValue] as! Dictionary)
                    mydata.append(mydataValue)
                    innerValue += 1
                }
            }
            value += 1
        }
        return mydata
    }
    
    class func parsePromotionsJSON(anyValue: AnyObject) -> [PromotionsModel]  {
        var myData = [PromotionsModel]()
        var value: Int = 0
        var seen = Set<String>()
        while value < anyValue.count {
            let promotion = PromotionsModel.init(dictionary: anyValue.object(at: value) as! Dictionary)
            if !seen.contains(promotion.shop_Id!) {
                myData.append(promotion)
                seen.insert(promotion.shop_Id!)
            }
            value += 1
        }

        return myData
    }
    
    class func parseBankListJSON(anyValue: AnyObject) -> [BankModel] {
        var myData = [BankModel]()
        var value: Int = 0
        while value < anyValue.count {
            let bank = BankModel.init(dictionary: anyValue.object(at: value) as! Dictionary)
            myData.append(bank)
            value += 1
        }
        return myData
    }
    
    class func parseBankRoutingListJSON(anyValue: AnyObject) -> [BankRoutingModel] {
        var myData = [BankRoutingModel]()
        var value: Int = 0
        while value < anyValue.count {
            let bank = BankRoutingModel.init(dictionary: anyValue.object(at: value) as! Dictionary)
            myData.append(bank)
            value += 1
        }
        return myData
    }
    
}


