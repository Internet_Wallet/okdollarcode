//
//  ConfirmationVC.swift
//  OK
//
//  Created by Rahul Tyagi on 8/5/17.
//  Copyright © 2017 Cgm. All rights reserved.

import UIKit

var MeterBillAmountParam = ""
var AdjustAmountParam = ""
var EBCashBack = ""
var MeterBillServiceFee = ""

class ConfirmationVC: OKBaseController, UITableViewDataSource, UITableViewDelegate, popMyViewController {
        
    //@IBOutlet weak var navigationBarOutlet: UINavigationBar!
    @IBOutlet weak var payBtnOutlet: UIButton!{
        didSet{
            payBtnOutlet.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)

        }
    }
    
    var descriptionArray = [String]()
    var ValueArray = [Any]()
    var chainClass = GetElectricityBillVC()
    
    enum caseScenerio {
        case DataNotInServer, 
        EverythingElse , AdjustAction, DataInServer
    }
    
    var caseScenerioType : caseScenerio = .EverythingElse
    var isPresented  = true
    var delegate : popMyViewController? = nil
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.helpSupportNavigationEnum = .Electricity
        self.loadNavigationBarSettings()
        self.loadBackButton()
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.title = appDelegate.getlocaLizationLanguage(key: "Confirmation")
        
        payBtnOutlet?.setTitle( appDelegate.getlocaLizationLanguage(key: "PAY"), for: .normal)
        payBtnOutlet?.titleLabel?.font = UIFont.init(name: appFont, size: appButtonSize)
        
        if descriptionArray.contains("Contact Mobile Number") {
            
            let index = descriptionArray.index(of: "Contact Mobile Number")
            descriptionArray[index!] = "Contact Number"
        }
        
        if caseScenerioType != .AdjustAction {
            
            //Insert Comma and MMK in bill amount
            let formatter = NumberFormatter()
            formatter.numberStyle = NumberFormatter.Style.decimal
            let enteredAmount = (ValueArray[10] as? NSString)?.integerValue ?? 0
            //let formattedNum = formatter.string(from: NSNumber(value: enteredAmount))
            let serviceamount = (ValueArray[11] as? NSString)?.integerValue ?? 0
            //let formattedserviceAmount = formatter.string(from: NSNumber(value: serviceamount))
            
            let totalamount = enteredAmount  + serviceamount
            
            ValueArray[10] = "\(totalamount) MMK"
            ValueArray[11] = "\(serviceamount) MMK"
        }
        
    }
    
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    //MARK : -  Tableview Delegates
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : ConfirmationVCTableCell  = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! ConfirmationVCTableCell
        cell.descriptionLabelOutlet.text = " \(descriptionArray[indexPath.row])"
        cell.valueLabelOutlet?.text = "  \(ValueArray[indexPath.row] as! String)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return descriptionArray.count
    }
    
    @IBAction func PayBtnAction(_ sender: Any) {
        
        //Change Date Format
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let strDate = dateFormatter.date(from: ValueArray[5] as! String)
        println_debug(strDate!)
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.calendar = Calendar(identifier: .gregorian)
        dateFormatter1.dateFormat = "MM/dd/yyyy"
        let newDate = dateFormatter1.string(from: strDate!)
        
        self.ValueArray[5] = newDate
        PayAPIRequest()
    }
    
    func popNow() {
        
        self.navigationController?.popViewController(animated: false)
        if delegate != nil {
            
            delegate?.popNow()
        }
    }
    
    fileprivate func showConfirmAlert()
    {
        DispatchQueue.main.async {
            
            alertViewObj.wrapAlert(title: "", body: appDelegate.getlocaLizationLanguage(key: "Please Try Again Later."), img: UIImage(named : "AppIcon"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                
            })
            
            alertViewObj.showAlert(controller: self)
            progressViewObj.removeProgressView()
        }
    }
    
    fileprivate func showReceiptVC(dict : NSMutableDictionary, transDict : Dictionary<String, String>)
    {
        DispatchQueue.main.async {
            
            let ReceiptVCObj = self.storyboard?.instantiateViewController(withIdentifier: "ReceiptVCSID") as! ReceiptVC
            ReceiptVCObj.ReceiptTransDict = transDict
            ReceiptVCObj.GeneratePdfDict = dict
            ReceiptVCObj.chainClass = self.chainClass
            ReceiptVCObj.delegate = self
            if self.isPresented == false
            {
                ReceiptVCObj.isPresent = false
            }
            
            self.navigationController?.pushViewController(ReceiptVCObj, animated: true)
            
        }
        
    }
    
    func PayAPIRequest() {
        
        payBtnOutlet.isEnabled = false
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
        }
        
        let customURL = URL.init(string: "https://www.okdollar.co/RestService.svc/PayEbMeterBill")
        let agentNum = UserModel.shared.mobileNo
        let msid = UserModel.shared.msid
        let str = uuid
        let EBPassCode = ok_password
        
        // let EBSecureToken = UserDefaults.standard.value(forKey: secureToken)
        
        var mtrTypeStr : Bool?
        var parameters : [String:Any] = [:]
        
        if MeterOptionType == .myMeterOption {
            
            mtrTypeStr = true
            
        }else{
            mtrTypeStr = false
        }
        
        parameters  = [ "MeterBillRefNumForSearch":ValueArray[2],
                        "SearchType":"0",
                        "LoginDeviceInfo": [
                            "MobileNumber": agentNum,
                            "Msid": msid,
                            "Ostype": 1,
                            "Otp": "",
                            "Simid": str],
                        "EbBillPayInfo" : [ "DivisionOrStateName":ValueArray[0],
                                            "Township":ValueArray[1],
                                            "MeterNumber":ValueArray[2],
                                            "BillerName":ValueArray[3],
                                            "AccNumber":ValueArray[4],
                                            "MeterReadDate":ValueArray[5],
                                            "Tariff":ValueArray[6],
                                            "CustRefNum":ValueArray[7],
                                            "PayFor":ValueArray[8],
                                            "ContactMobileNum":ValueArray[9],
                                            "MeterBillAmount": MeterBillAmountParam,
                                            "ServiceFee": "0",
                                            "Remarks":ValueArray[ValueArray.count-1],
                                            "AdjustPaidAmoun":AdjustAmountParam,
                                            "CashBack":"",
                                            "DivisionId": EBDivisionID, 
                                            "InvoiceNumber":"",
                                            "LocalTransType":"ELECTRICITY",
                                            "MeterType":mtrTypeStr ?? "",
                                            "OkdollarAccNumber":agentNum,
                                            "OkdollarAccPwd": EBPassCode ?? "",
                                            "SecureToken" : "", //EBSecureToken!,
                            "TownshipId": EBTownshipID
                            ] as [String : Any],
                        ] as [String : Any]
        
        
        println_debug("Check here \(parameters)")
        
        // JSON all the things
        let auth = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: customURL!)
        
        do {
            //println_debug("Requesting Pay Bill API Now")
            
            // Set the request content type to JSON
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            // The magic...set the HTTP request method to POST
            request.httpMethod = "POST"
            
            // Add the JSON serialized login data to the body
            request.httpBody = auth
            
            // Create the task that will send our login request (asynchronously)
            let task = session.dataTask(with: request as URLRequest, completionHandler:
            { (data, response, error) -> Void in
                
                if(data == nil)
                {
                    DispatchQueue.main.async {
                        progressViewObj.removeProgressView()
                    }
                    self.showConfirmAlert()
                    self.payBtnOutlet.isEnabled = true
                    
                }
                else {
                    DispatchQueue.main.async {
                        self.payBtnOutlet.isEnabled = true
                    }
                    do {
                        let jsonResponse = try? JSONSerialization.jsonObject(with: data!, options:.allowFragments)
                        if let jsonResponse = jsonResponse as? [String: Any] {
                            // if let jsonResponse = jsonResponse as? [String: Any] {
                            println_debug("\n \n \n \(jsonResponse)")
                            
                            if jsonResponse["Code"] as! NSNumber ==  200
                            {
                                let DataStringToJSON : [String : Any] = self.convertStringToJson(ReceivedString: jsonResponse["Data"]! as! String) as! [String : Any]
                                println_debug(DataStringToJSON)
                                
                                let resultStringToJSON : AnyObject  = self.convertStringToJson(ReceivedString: DataStringToJSON["result"]! as! String) as AnyObject
                                println_debug(resultStringToJSON)
                                
                                if resultStringToJSON["responseCode"] as! String == "200"
                                {
                                    let transactionValue = (resultStringToJSON["transTime"] as! String)
                                    
                                    //vinnu added
                                    //let newStr = str.substring(to: index) // Swift 3
                                    //let newStr = String(str[..<index]) // Swift4
                                    // let newStr = str.substring(from: index) // Swift 3
                                    // let newStr = String(str[index...]) // Swift 4
                                    
                                    let index = transactionValue.index(str.startIndex, offsetBy: 11)
                                    //let transactionDate = transactionValue.substring(to: index)
                                    let transactionDate = String(transactionValue[..<index])
                                    
                                    let index2 = transactionValue.index(str.startIndex, offsetBy: 11)
                                    //let transactionTime = transactionValue.substring(from: index2)
                                    let transactionTime = String(transactionValue[index2...])
                                    
                                    
                                    let TransactionDictionary : Dictionary<String,String> = ["username" : self.ValueArray[3] as! String,
                                                                                             "transactionID" : resultStringToJSON["transId"] as! String,
                                                                                             "transactionAmount" : MeterBillAmountParam,
                                                                                             "transactionDate" : transactionDate,
                                                                                             "transactionTime" : transactionTime ]
                                    
                                    let agentNameValue = UserModel.shared.name
                                    
                                    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                                    let documentsDirectory: String = paths[0]
                                    let getImagePath: String = URL(fileURLWithPath: documentsDirectory).appendingPathComponent("RecentQRimage.png").absoluteString
                                    
                                    let pdfDict : NSMutableDictionary =  ["senderaccountname" : agentNameValue,
                                                                          "senderaccountnum" : UserModel.shared.formattedNumber,
                                                                          "NameOnTheBill" : self.ValueArray[3] as! String,
                                                                          "MeterBillAddress" : "\(self.ValueArray[0]),\(self.ValueArray[1])" ,
                                        "tansactionid" : resultStringToJSON["transId"] as! String,
                                        "type" : "ELECTRICITY",
                                        "DueDate"  : resultStringToJSON["transTime"] as! String,
                                        "PayFor" : self.ValueArray[8] as! String ,
                                        "datetime" : resultStringToJSON["transTime"] as! String,
                                        "remarks" : "#OKBILLPAYMENT-PAYMENTTYPE:Electricity-Meter Number: \(self.ValueArray[2]) -Meter Type: \(mtrTypeStr!) -Pay For: \(self.ValueArray[8]) -Division: \(self.ValueArray[0]) -Township: \(self.ValueArray[1]) -Name: \(self.ValueArray[3]) -Due Date: \(resultStringToJSON["transTime"] as! String) #",
                                        "paidamount" : MeterBillAmountParam,
                                        "QRimagePath" : getImagePath,
                                        "Remark" : "Electricity Bill",
                                        "receiveraccountname" : "",
                                        "receiveraccountnum" : "",
                                        "totamount" : "",
                                        "meterNumber" : self.ValueArray[2] as! String,
                                        "billMonth" : self.ValueArray[5] as! String,
                                        "ServiceFee": "0",
                                        "isFrom" : "EBBill"
                                    ]
                                    
                                    //Call show
                                    self.showReceiptVC(dict: pdfDict, transDict: TransactionDictionary)
                                } else {
                                    DispatchQueue.main.async {
                                        progressViewObj.removeProgressView()
                                    }
                                    self.showConfirmAlert()
                                }
                                
                            } else {
                                DispatchQueue.main.async {
                                    progressViewObj.removeProgressView()
                                }
                                self.showConfirmAlert()
                                println_debug("Condition failed")
                                //Show error
                            }
                        }
                        DispatchQueue.main.async {
                            progressViewObj.removeProgressView()
                        }
                    }
                }
            })
            
            // Start the task on a background thread
            task.resume()
            
        }
    }
    
    func convertStringToJson(ReceivedString : String) -> Any
    {
        let CustomStringData = ReceivedString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue), allowLossyConversion:true)
        do {
            let jsonData = try? JSONSerialization.jsonObject(with: CustomStringData!, options: .allowFragments ) as Any
            return jsonData!
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
