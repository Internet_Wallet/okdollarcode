//
//  ElectricityWebViewController.swift
//  OK
//
//  Created by Vinod's MacBookPro on 2/23/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ElectricityWebViewController: OKBaseController,UIWebViewDelegate {
    
    @IBOutlet weak var EbWebView: UIWebView!
    
    var currentLat = ""
    var CurrentLong = ""
    var passwordType = ""
    var aURLRequest: URLRequest?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        geoLocManager.startUpdateLocation()
        
        EbWebView.scrollView.bounces = false
        EbWebView.delegate = self
        
        if appDel.checkNetworkAvail() {
            progressViewObj.showProgressView()
            self.prepareJsonRequestForWeb()
        }
            
        else{
            
        }
        
//        if let statusbar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
//            statusbar.backgroundColor = kYellowColor
//        }
        
        self.loadNavigationBarSettings()
        self.leftBackButton()
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    func leftBackButton() {
//        let backBtn = UIBarButtonItem(image: UIImage(named: "tabBarBack"), style: .plain, target: self, action: #selector(backButtonAction))
//        self.navigationItem.leftBarButtonItem  = backBtn
    }
    
    @objc func backButtonAction() {
        if EbWebView.canGoBack {
            EbWebView.goBack()
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func prepareJsonRequestForWeb()  {
        
        let pass = ok_password
        let appId = UserModel.shared.appID
        let agentCode = UserModel.shared.mobileNo
        
        if (geoLocManager.currentLongitude != nil) {
            CurrentLong = geoLocManager.currentLongitude
        } else{
            CurrentLong = ""
        }
        
        if(geoLocManager.currentLatitude != nil){
            currentLat = geoLocManager.currentLatitude
        }else{
            currentLat = ""
        }
        
        if ok_password_type == true {
            passwordType = "1"
        }else{
            passwordType = "0"
        }
        
        let dateOfBirth = UserModel.shared.dob
        let gender = UserModel.shared.gender
        let balance = UserLogin.shared.walletBal
        let selectedLang = "en-GB"
        /*
        "TransactionsCellTower": {
            "Lac": "9221",
            "Mac": "02:00:00:00:00:00",
            "Mcc": "414",
            "Mnc": "01",
            "SignalStrength": "",
            "Ssid": "\"MPT\"",
            "ConnectedwifiName":"",
            "NetworkType":""  },
        
        "lGeoLocation": {
            "CellID": "134957334",
            "Latitude": "0.0",
            "Longitude": "0.0"
        },*/
        
        let childDicts = [
            "Pwd" : pass,
            "Appid" : appId,
            "MobileNumber" : agentCode,
            "Simid" : uuid,
            "Msid" : "",
            "Lat" : currentLat,
            "Long" : CurrentLong,
            "DeviceID" : uuid,
            "Language" : selectedLang,
            "Balance" : balance,
            "AppVersion" : buildVersion,
            "IsNativeApp" : "true",
            "Date of Birth" : dateOfBirth,
            "Gender" : gender
            ]
        let rootInputDict = [
            "UtilityJsonRequest" : childDicts
        ]
        println_debug("root input dic contains :: \(rootInputDict)")
        var _: HTTPCookie?
        let storage = HTTPCookieStorage.shared
        for cookie in storage.cookies! {
            storage.deleteCookie(cookie)
        }
        
        let inputRequest = composeURLRequest(toDownloadData: rootInputDict)
        EbWebView.loadRequest(inputRequest)
        
    }
    
    func composeURLRequest(toDownloadData requestDict: [AnyHashable: Any]) -> URLRequest {
        
        var _: Error?
        var url: URL?
        url = URL(string: Url.EB_Bill)
        let request = NSMutableURLRequest(url: url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        if !requestDict.isEmpty {
            let postData: Data? = try? JSONSerialization.data(withJSONObject: requestDict, options: [])
            request.httpBody = postData
        }
        aURLRequest = request as URLRequest
        return aURLRequest!
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        println_debug("start")
        progressViewObj.showProgressView()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        println_debug("finish")
        progressViewObj.removeProgressView()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
        progressViewObj.removeProgressView()
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        
        let requestPath: String? = request.url?.absoluteString
        println_debug("requestPath : \(String(describing: requestPath))")
        if requestPath?.contains(".pdf") ?? false {
            //open the PDF in seperate Browser
            if let aPath = requestPath, let aPath1 = URL(string: aPath) {
                UIApplication.shared.open(aPath1, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
        }
        return true
    }
    
    
    @IBAction func backBtnAction(_ sender: Any) {
        if EbWebView.canGoBack {
            EbWebView.goBack()
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    deinit {
        geoLocManager.stopUpdateLocation()
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
