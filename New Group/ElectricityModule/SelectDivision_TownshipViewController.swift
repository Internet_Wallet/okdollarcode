//
//  SelectDivision_TownshipViewController.swift
//  OK
//
//  Created by Rahul Tyagi on 29/07/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit

//Gloabl Variables
var DivisionFieldActive = true
var DivisionCode = ""
var SelectedDivision = ""
var SelectedTownship = ""
var EBTownshipID = ""
var EBDivisionID = ""


var TownshipImageArray : [String] = []
var  TownshipResultStringToJSON : AnyObject? = nil


protocol DivTownSelectedDelegate{
    func userSelectedData(DivisionString : String, TownshipString : String)
    func userSelectedDivision(DivisionString : String)
}


class SelectDivision_TownshipViewController: OKBaseController, UITableViewDelegate, UITableViewDataSource{
    var delegate : DivTownSelectedDelegate? = nil
    
    @IBOutlet weak var tableViewOutlet: UITableView!
    var divisionSelected = false
    //Arrays For Division and Township
    var DivisionList = [Dictionary<String, String>]()
    var TownshipList : [String] = []
        
  //  @IBOutlet weak var navigationBarOutlet: UINavigationBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tableViewOutlet.tableFooterView = UIView()
        self.helpSupportNavigationEnum = .Electricity
        self.loadNavigationBarSettings()
        self.leftBackButton()
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    func leftBackButton() {
        let backBtn = UIBarButtonItem(image: UIImage(named: "tabBarBack"), style: .plain, target: self, action: #selector(backButtonAction))
        self.navigationItem.leftBarButtonItem  = backBtn
    }
    
    @objc func backButtonAction() {
        self.view.endEditing(true)
        
        DivisionFieldActive = true
        self.tableViewOutlet.reloadData()
        if SelectedDivision != "" && divisionSelected {
            self.delegate?.userSelectedDivision(DivisionString: SelectedDivision)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
        divisionSelected = false
        self.navigationController?.navigationBar.isTranslucent = false
        UINavigationBar.appearance().barTintColor = UIColor.init(red: 246/255.0, green: 198/255.0, blue: 0, alpha: 1)
        
        DivisionList.removeAll()
        TownshipList.removeAll()
        
        if DivisionFieldActive == true
        {
            self.title =  appDel.getlocaLizationLanguage(key: "Select Division")
            GetDivisionListFromServer()
        }
        else
        {   self.title = appDel.getlocaLizationLanguage(key: "Select Township")
            GetTownshipListFromServer()
            
        }
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    func GetDivisionListFromServer()
    {
        
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
        }
        let customURL = URL.init(string: "http://www.okdollar.co/RestService.svc/GetDivisionListForEb")
        
        let agentNum = UserModel.shared.mobileNo
        
        let msid = UserModel.shared.msid
        let str = uuid
        
        let userDict = ["MobileNumber" : agentNum as Any,
                        "Msid" : msid as Any,
                        "Ostype" : 1,
                        "Otp" : "",
                        "Simid ": str] as [String : Any]
        
        let userData = ["LoginDeviceInfo" : userDict]
        
        // JSON all the things
        let auth = try? JSONSerialization.data(withJSONObject: userData, options: .prettyPrinted)
        
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: customURL!)
        
        do {
            
            //println_debug("Requesting Division API Now")
            
            // Set the request content type to JSON
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            // The magic...set the HTTP request method to POST
            request.httpMethod = "POST"
            
            // Add the JSON serialized login data to the body
            request.httpBody = auth
            
            // Create the task that will send our login request (asynchronously)
            let task = session.dataTask(with: request as URLRequest, completionHandler:
            { (data, response, error) -> Void in
                
                if(data == nil)
                {
                    //println_debug("")
                    DispatchQueue.main.async {
                        alertViewObj.wrapAlert(title: nil, body: appDel.getlocaLizationLanguage(key: "Please Try Again Later."), img: nil)
                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                            
                        })
                        alertViewObj.showAlert(controller: self)
                        progressViewObj.removeProgressView()
                    }
                    
                }
                else
                {
                    do {
                        let jsonResponse = try? JSONSerialization.jsonObject(with: data!, options:.allowFragments)
                        if let jsonResponse = jsonResponse as? [String: Any] {
                            // if let jsonResponse = jsonResponse as? [String: Any] {
                            //println_debug("\n \n \n \(jsonResponse)")
                            
                            if jsonResponse["Msg"] as! String == "Success"
                            {
                                //println_debug(jsonResponse["Data"] as Any)
                                
                                let DataStringToJSON : [String : Any] = self.convertStringToJson(ReceivedString: jsonResponse["Data"]! as! String) as! [String : Any]
                                //println_debug(DataStringToJSON)
                                
                                
                                let resultStringToJSON : AnyObject  = self.convertStringToJson(ReceivedString: DataStringToJSON["result"]! as! String) as AnyObject
                                
                                for element in resultStringToJSON as! Array<AnyObject> {
                                    //println_debug(element)
                                    
                                    //println_debug(element["name"])
                                    
                                    var DivName = ""
                                    
                                    if appDel.currentLanguage == "my" {
                                        DivName = element["bUnicode"]! as! String
                                    } else if appDel.currentLanguage == "uni" {
                                        DivName = element["bName"]! as! String
                                    } else {
                                        DivName = element["name"]! as! String
                                    }
                                    
                                    let DivCode = element["code"]! as! String
                                    let DivID = element["id"]! as! String
                                    
                                    let DivisionInfo : [String: String] = [
                                        "name" : DivName,
                                        "code" : DivCode,
                                        "id" : DivID
                                    ]
                                    self.DivisionList.append(DivisionInfo)
                                }
                                
                                DispatchQueue.main.async {
                                    progressViewObj.removeProgressView()
                                    self.tableViewOutlet.reloadData()
                                    
                                }
                            }  else {
                                //Show error
                                DispatchQueue.main.async {
                                    alertViewObj.wrapAlert(title: nil, body: appDel.getlocaLizationLanguage(key: "Please Try again in a while."), img: nil)
                                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                        
                                    })
                                    alertViewObj.showAlert(controller: self)
                                    progressViewObj.removeProgressView()
                                }
                            }
                        }
                    }
                }
                
            })
            
            // Start the task on a background thread
            task.resume()
        }
        
    }
    
    func GetTownshipListFromServer()
    {
        
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
        }
        
        let customURL = URL.init(string: "http://www.okdollar.co/RestService.svc/GetTownshipsForEB")
        
        let agentNum = UserModel.shared.mobileNo
        let msid = UserModel.shared.msid
        
        let str = uuid
        
        let parameters = [  "DivisionCode": DivisionCode ,
                            "LoginDeviceInfo": [
                                "MobileNumber": agentNum,
                                "Msid": msid,
                                "Ostype": 1,
                                "Otp": "",
                                "Simid": str
            ]
            ] as [String : Any]
        
        
        //println_debug(parameters)
        
        
        // JSON all the things
        let auth = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: customURL!)
        
        do {
            
            //println_debug("Requesting Township API Now")
            
            // Set the request content type to JSON
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            // The magic...set the HTTP request method to POST
            request.httpMethod = "POST"
            
            // Add the JSON serialized login data to the body
            request.httpBody = auth
            
            // Create the task that will send our login request (asynchronously)
            let task = session.dataTask(with: request as URLRequest, completionHandler:
            { (data, response, error) -> Void in
                
                if(data == nil)
                {
                    //println_debug("")
                    DispatchQueue.main.async {
                        alertViewObj.wrapAlert(title: nil, body: appDel.getlocaLizationLanguage(key: "Please Try Again Later."), img: nil)
                        
                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                            
                        })
                        
                        alertViewObj.showAlert(controller: self)
                        progressViewObj.removeProgressView()
                    }
                }
                else
                {
                    do {
                        let jsonResponse = try? JSONSerialization.jsonObject(with: data!, options:.allowFragments)
                        if let jsonResponse = jsonResponse as? [String: Any] {
                            // if let jsonResponse = jsonResponse as? [String: Any] {
                            println_debug("\n \n \n \(jsonResponse)")
                            
                            if jsonResponse["Msg"] as! String == "Success"
                            {
                                
                                let DataStringToJSON : [String : Any] = self.convertStringToJson(ReceivedString: jsonResponse["Data"]! as! String) as! [String : Any]
                                //println_debug(DataStringToJSON)
                                
                                TownshipResultStringToJSON  = self.convertStringToJson(ReceivedString: DataStringToJSON["result"]! as! String) as AnyObject
                                
                                for element in TownshipResultStringToJSON as! Array<AnyObject> {
                                    //println_debug(element)
                                    
                                    //println_debug(element["eName"])
                                    
                                    var TownshipName = ""
                                    //println_debug("language is:::::: \(self.appDel.getSelectedLanguage())")
                                    
                                    if appDel.currentLanguage == "my" {
                                        //println_debug("language is:::::: \(self.appDel.getSelectedLanguage())")
                                        TownshipName = element["bUnicode"]! as! String
                                    } else if appDel.currentLanguage == "uni" {
                                        //println_debug("language is:::::: \(self.appDel.getSelectedLanguage())")
                                        TownshipName = element["bName"]! as! String
                                    } else {
                                        TownshipName = element["eName"]! as! String
                                    }
                                    self.TownshipList.append(TownshipName)
                                }
                                
                                DivisionFieldActive = false
                                
                                
                                DispatchQueue.main.async {
                                    self.tableViewOutlet.isUserInteractionEnabled = true
                                    progressViewObj.removeProgressView()
                                    
                                    self.tableViewOutlet.reloadData()
                                    
                                    DispatchQueue.main.async {
                                        self.title = appDel.getlocaLizationLanguage(key: "Select Township")
                                    }
                                    
                                }
                            }
                            else
                            {
                                
                                DispatchQueue.main.async {
                                    alertViewObj.wrapAlert(title: nil, body: appDel.getlocaLizationLanguage(key: "Please Try Again Later."), img: nil)
                                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                        
                                    })
                                    alertViewObj.showAlert(controller: self)
                                    progressViewObj.removeProgressView()
                                }
                                
                            }
                        }
                    }
                }
                
            })
            
            // Start the task on a background thread
            task.resume()
            
        }
        
    }
    
    func convertStringToJson(ReceivedString : String) -> Any
    {
        let CustomStringData = ReceivedString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue), allowLossyConversion:true)
        do {
            let jsonData = try? JSONSerialization.jsonObject(with: CustomStringData!, options: .allowFragments ) as Any
            return jsonData!
        }
    }
    
    
    @IBAction func backBtnAction(_ sender: Any)
    {
        //println_debug("Btn was clicked")
        self.view.endEditing(true)
        
        DivisionFieldActive = true
        self.tableViewOutlet.reloadData()
        if SelectedDivision != "" && divisionSelected {
            self.delegate?.userSelectedDivision(DivisionString: SelectedDivision)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Table view Delegate & DataSource
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        
        // Configure the cell...
        
        
        if DivisionFieldActive == true && !DivisionList.isEmpty
        {
            cell.textLabel!.font = UIFont.init(name: appFont, size: 17)
            cell.textLabel!.text = " \(String(describing: (DivisionList[indexPath.row] as Dictionary)["name"]!))"
        }
        else if !TownshipList.isEmpty
        {
            cell.textLabel?.text = " \(TownshipList[indexPath.row])"
            cell.textLabel!.font = UIFont.init(name: appFont, size: 17)
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if DivisionFieldActive == true
        {
            return DivisionList.count
        }
        else
        {
            return TownshipList.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if DivisionFieldActive == true
        {
            divisionSelected = true
            SelectedDivision = (DivisionList[indexPath.row] as Dictionary)["name"]!
            
            DivisionCode = (DivisionList[indexPath.row] as Dictionary)["code"]!
            
            EBDivisionID = (DivisionList[indexPath.row] as Dictionary)["id"]!
            
            self.view.endEditing(true)
            
            if tableViewOutlet.isUserInteractionEnabled == true
            {
                GetTownshipListFromServer()
                self.tableViewOutlet.isUserInteractionEnabled = false
            }
        }
        else
        {
            if TownshipList.count > 0
            {
                SelectedTownship = TownshipList[indexPath.row]
                //                println_debug(SelectedDivision, SelectedTownship)
                if delegate != nil
                {
                    delegate?.userSelectedData(DivisionString: SelectedDivision, TownshipString: SelectedTownship)
                }
                DivisionFieldActive = true
                self.dismiss(animated: true, completion: nil)
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
}


