//
//  AdjustAndPayVC.swift
//  OK
//
//  Created by Rahul Tyagi on 8/17/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit

class AdjustAndPayVC: OKBaseController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, popMyViewController {
    
   // @IBOutlet weak var navigationBarOutlet: UINavigationBar!
    var jsonDataArray = Dictionary<String,Any>()
    var TableDescriptionArray : [String] = []
    var TableImageArray : [UIImage] = []
    var TableTextFieldArray : [Any] = []
    var activeField : UITextField? = nil
    
    @IBOutlet weak var submitBtnOutlet: UIButton!{
        didSet{
            submitBtnOutlet.titleLabel?.font = UIFont(name: appFont, size: appFontSize)

        }
    }
    
    enum AdjustPayOptions {
        
        case AdjustAction, PayAction , notSpecified
    }
    var AdjustAndPayActionType : AdjustPayOptions = .notSpecified
    
    @IBOutlet weak var tableviewOutlet: UITableView!
        
    var customCellRef : AdjustAndPayVCTableViewCell? = nil
    
    var delegate : popMyViewController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let TapGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.HandleTapGesture(_:)))
        
        TapGesture.cancelsTouchesInView = false
        
        self.view.addGestureRecognizer(TapGesture)
        self.helpSupportNavigationEnum = .Electricity
        self.loadNavigationBarSettings()
        self.leftBackButton()
        self.title = appDelegate.getlocaLizationLanguage(key: "Electricity Bill")
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    func leftBackButton() {
        let backBtn = UIBarButtonItem(image: UIImage(named: "tabBarBack"), style: .plain, target: self, action: #selector(backButtonAction))
        self.navigationItem.leftBarButtonItem  = backBtn
    }
    
    @objc func backButtonAction() {
        self.view.endEditing(true)
        MeterBillAmountParam = "" //Reset
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func HandleTapGesture(_:UIGestureRecognizer) {
        
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //println_debug(jsonDataArray)
        
        //Change Date Format
        let firstDateInstance = (jsonDataArray["meterReadDate"] ?? Date().stringValue(dateFormatIs: "dd-MM-yyyy")) as! String
        let inputFormatter = DateFormatter()
        inputFormatter.calendar = Calendar(identifier: .gregorian)
        inputFormatter.dateFormat = "dd/MM/yyyy"
        if inputFormatter.date(from: firstDateInstance) != nil
        {
            
        }
        else
        {
            let dateValue = (jsonDataArray["meterReadDate"] ?? Date().stringValue(dateFormatIs: "dd-MM-yyyy")) as! String
            let customIndex = dateValue.index(dateValue.startIndex, offsetBy: 11)
            //let shortDateStr = dateValue.substring(to: customIndex)
            let shortDateStr = String(dateValue[..<customIndex])
            
            jsonDataArray["meterReadDate"] = self.formattedDateFromString(dateString: shortDateStr, withFormat: "MMM d yyyy", toBeConvertedToFormat: "dd/MM/yyyy")
        }
        
        
        
        if AdjustAndPayActionType == .AdjustAction
        {
            TableDescriptionArray =             [appDelegate.getlocaLizationLanguage(key: "Division"),
                                                 appDelegate.getlocaLizationLanguage(key: "Township"),
                                                 appDelegate.getlocaLizationLanguage(key: "Meter Number"),
                                                 appDelegate.getlocaLizationLanguage(key: "Consumer Name (as on Bill)"),
                                                 appDelegate.getlocaLizationLanguage(key: "Ledger Number"),
                                                 appDelegate.getlocaLizationLanguage(key: "Meter Read Date"),
                                                 appDelegate.getlocaLizationLanguage(key: "Tariff"),
                                                 appDelegate.getlocaLizationLanguage(key: "Computer Code Number"),
                                                 appDelegate.getlocaLizationLanguage(key: "Pay For"),
                                                 appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number"),
                                                 appDelegate.getlocaLizationLanguage(key: "Current Paid Bill Amount"),
                                                 appDelegate.getlocaLizationLanguage(key: "Adjust Bill Amount") ,
                                                 appDelegate.getlocaLizationLanguage(key: "Service Fees"),
                                                 appDelegate.getlocaLizationLanguage(key: "Remark")]
            
            TableImageArray = [#imageLiteral(resourceName: "r_division"), #imageLiteral(resourceName: "township"), #imageLiteral(resourceName: "meternumber"), #imageLiteral(resourceName: "n_personal"), #imageLiteral(resourceName: "my_account.png"), #imageLiteral(resourceName: "Report_bill.png"), #imageLiteral(resourceName: "Tariff"),#imageLiteral(resourceName: "Computer Code Number"),#imageLiteral(resourceName: "Pay For"),#imageLiteral(resourceName: "Contact Mobile Number"),#imageLiteral(resourceName: "newpayamount.png"),#imageLiteral(resourceName: "newpayamount.png"),#imageLiteral(resourceName: "Service Fees"),#imageLiteral(resourceName: "newpayremark.png")]
            
            if jsonDataArray["serviceFee"] as! String == "0.00" || jsonDataArray["serviceFee"] as! String == ""{
                jsonDataArray["serviceFee"] = "0"
            }
            TableTextFieldArray = [jsonDataArray["divisionEName"]! ,
                                   jsonDataArray["townshipEName"]! ,
                                   jsonDataArray["meterNumber"]! ,
                                   jsonDataArray["billerName"]! ,
                                   jsonDataArray["accNumber"]! ,
                                   jsonDataArray["meterReadDate"] ?? Date().stringValue(dateFormatIs: "dd-MM-yyyy") ,
                                   jsonDataArray["tarrif"]! ,
                                   jsonDataArray["custRefNum"]! ,
                                   jsonDataArray["payFor"]! ,
                                   jsonDataArray["contactMobileNum"]! ,
                                   "\(jsonDataArray["totalPaidAmount"]!)",
                jsonDataArray["pendingAmount"]! ,
                jsonDataArray["serviceFee"]! ,
                "",
            ]
            
            AdjustAmountParam = jsonDataArray["pendingAmount"]! as! String
            MeterBillAmountParam = jsonDataArray["pendingAmount"]! as! String
            
            submitBtnOutlet.isHidden = false
            
        } else{
            
            TableDescriptionArray =             [appDelegate.getlocaLizationLanguage(key: "Division"),
                                                 appDelegate.getlocaLizationLanguage(key: "Township"),
                                                 appDelegate.getlocaLizationLanguage(key: "Meter Number"),
                                                 appDelegate.getlocaLizationLanguage(key: "Consumer Name (as on Bill)"),
                                                 appDelegate.getlocaLizationLanguage(key: "Ledger Number"),
                                                 appDelegate.getlocaLizationLanguage(key: "Meter Read Date"),
                                                 appDelegate.getlocaLizationLanguage(key: "Tariff"),
                                                 appDelegate.getlocaLizationLanguage(key: "Computer Code Number"),
                                                 appDelegate.getlocaLizationLanguage(key: "Pay For"),
                                                 appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number"),
                                                 appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount"),
                                                 appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount") ,
                                                 appDelegate.getlocaLizationLanguage(key: "Service Fees"),
                                                 appDelegate.getlocaLizationLanguage(key: "Remark")]
            
           TableImageArray = [#imageLiteral(resourceName: "r_division"), #imageLiteral(resourceName: "township"), #imageLiteral(resourceName: "meternumber"), #imageLiteral(resourceName: "n_personal"), #imageLiteral(resourceName: "my_account.png"), #imageLiteral(resourceName: "Report_bill.png"), #imageLiteral(resourceName: "Tariff"),#imageLiteral(resourceName: "Computer Code Number"),#imageLiteral(resourceName: "Pay For"),#imageLiteral(resourceName: "Contact Mobile Number"),#imageLiteral(resourceName: "newpayamount.png"),#imageLiteral(resourceName: "newpayamount.png"),#imageLiteral(resourceName: "Service Fees"),#imageLiteral(resourceName: "newpayremark.png")]
            
            if jsonDataArray["serviceFee"] as! String == "0.00" || jsonDataArray["serviceFee"] as! String == "" {
                jsonDataArray["serviceFee"] = "0"
            }
            
            TableTextFieldArray = [jsonDataArray["divisionEName"]! ,
                                   jsonDataArray["townshipEName"]! ,
                                   jsonDataArray["meterNumber"]! ,
                                   jsonDataArray["billerName"]! ,
                                   jsonDataArray["accNumber"]! ,
                                   jsonDataArray["meterReadDate"] ?? Date().stringValue(dateFormatIs: "dd-MM-yyyy") ,
                                   jsonDataArray["tarrif"]! ,
                                   jsonDataArray["custRefNum"]! ,
                                   jsonDataArray["payFor"]! ,
                                   jsonDataArray["contactMobileNum"]! ,
                                   "" ,
                                   "" ,
                                   jsonDataArray["serviceFee"]! ,
                                   "",
            ]
            
            if  jsonDataArray["actualBillAmountPreDef"]! as! String == "0.00" 
            {
                
                AdjustAmountParam = ""
                MeterBillAmountParam = ""
                
                submitBtnOutlet.isHidden = true
            }
            else //Available
            {
                self.TableDescriptionArray[10] = appDelegate.getlocaLizationLanguage(key: "Bill Amount")
                TableTextFieldArray[10] = jsonDataArray["actualBillAmountPreDef"]!
                
                TableDescriptionArray.remove(at: 11)
                TableTextFieldArray.remove(at: 11)
                
                AdjustAmountParam = ""
                MeterBillAmountParam = jsonDataArray["actualBillAmountPreDef"]! as! String
                
                submitBtnOutlet.isHidden = false
            }
            
            
        }
        
        tableviewOutlet.reloadData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden(notification:)) , name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillChangeHeight(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        self.title = appDelegate.getlocaLizationLanguage(key: "Electricity Bill")
        
        self.submitBtnOutlet.setTitle(appDelegate.getlocaLizationLanguage(key: "Submit"), for: .normal)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.view.endEditing(true)
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification,object : nil )
        
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        scrollToLastRow()
        
        let customIndex = NSIndexPath.init(row: 10, section: 0)
        self.customCellRef = self.tableviewOutlet.cellForRow(at: customIndex as IndexPath) as? AdjustAndPayVCTableViewCell
        //println_debug(self.customCellRef?.descriptionLabelOutlet.text as Any)
        self.customCellRef?.textFieldOutlet.becomeFirstResponder()
    }
    
    func scrollToLastRow()
    {
        let indexPath = IndexPath(row: self.TableDescriptionArray.count-1, section: 0)
        self.tableviewOutlet.scrollToRow(at: indexPath, at: .bottom, animated: false)
        
    }
    
    @objc func keyboardWasShown(notification: NSNotification)
    {
        //Need to calculate keyboard exact size due to Apple suggestions
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: keyboardSize!.height + 45, right: 0.0)
        
        self.tableviewOutlet.contentInset = contentInsets
        self.tableviewOutlet.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if activeField != nil
        {
            if (!aRect.contains(activeField!.frame.origin))
            {
                self.tableviewOutlet.scrollRectToVisible(activeField!.frame, animated: true)
            }
        }
        
        
    }
    
    @objc func keyboardWillChangeHeight(notification: NSNotification)
    {
        //Need to calculate keyboard exact size due to Apple suggestions
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        let KeyboardSize = (keyboardSize?.height)!
        submitBtnOutlet?.frame.origin.y = self.view.frame.height - KeyboardSize - 45
        
        self.view.layoutIfNeeded()
    }
    
    
    @objc func keyboardWillBeHidden(notification: NSNotification)
    {
        //Once keyboard disappears, restore original positions
        //let info : NSDictionary = notification.userInfo! as NSDictionary
        //println_debug(".....>>>>\(info)")
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        self.tableviewOutlet.contentInset = contentInsets
        self.tableviewOutlet.scrollIndicatorInsets = contentInsets
        submitBtnOutlet?.frame.origin.y = self.view.frame.height - 45
        // self.view.endEditing(true)
        
    }
    
    func formattedDateFromString(dateString: String, withFormat inputFormat: String, toBeConvertedToFormat outputFormat : String) -> String?
    {
        //Check if dateString complies with Input format
        
        let inputFormatter = DateFormatter()
        inputFormatter.calendar = Calendar(identifier: .gregorian)
        inputFormatter.dateFormat = inputFormat
        
        
        
        if let date = inputFormatter.date(from: dateString) {
            
            let outputFormatter = DateFormatter()
            outputFormatter.calendar = Calendar(identifier: .gregorian)
            outputFormatter.dateFormat = outputFormat
            
            return outputFormatter.string(from: date)
        }
        
        return nil
    }
    
    
    
    @IBAction func backBtnAction(_ sender: Any)
    {
        self.view.endEditing(true)
        MeterBillAmountParam = "" //Reset
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitBtnAction(_ sender: Any)
    {
        self.view.endEditing(true)
        
        //Check Balance
        let balanceAmount = (UserLogin.shared.walletBal as NSString).floatValue
        //println_debug(balanceAmount,MeterBillAmountParam)
        
        let billAmountIntValue = (MeterBillAmountParam as NSString).floatValue
        let myAttribute = [NSAttributedString.Key.font: UIFont(name: appFont, size: 17) ?? UIFont.systemFont(ofSize: 17)]
        
        if balanceAmount <= billAmountIntValue
        {
            //Show Alert
            var lowBalanceAlert = ""
            
            
            if appDelegate.getSelectedLanguage() == "en"
            {
                lowBalanceAlert = NSAttributedString(string: appDelegate.getlocaLizationLanguage(key: "Your meter bill details is submitted but you have insufficient balance."), attributes: myAttribute).string
                
            }
            else
            {
                lowBalanceAlert = NSAttributedString(string: appDelegate.getlocaLizationLanguage(key: "Your meter bill details is submitted but you have insufficient balance."), attributes: myAttribute).string
                
            }
                
                DispatchQueue.main.async(){
                    
                    alertViewObj.wrapAlert(title: "", body: lowBalanceAlert, img: nil)
                    
                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                        
                        MeterBillAmountParam = ""
                        self.navigationController?.popViewController(animated: true)
                        
                    })
                    
                    alertViewObj.showAlert(controller: self)
                    progressViewObj.removeProgressView()
                }
            
        }
        else
        {
            let ConfirmationVCObj = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationVCSID") as! ConfirmationVC
            
            if TableDescriptionArray.contains(appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount"))
            {
                TableDescriptionArray.remove(at: 11)
                TableTextFieldArray.remove(at: 11)
            }
            
            if TableDescriptionArray.contains(appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount"))
            {
                TableDescriptionArray[10] = appDelegate.getlocaLizationLanguage(key: "Bill Amount")
            }
            
            if AdjustAndPayActionType == .PayAction
            {
                let formatter = NumberFormatter()
                formatter.numberStyle = NumberFormatter.Style.decimal
                let textAfterRemovingComma = (TableTextFieldArray[10] as! String).replacingOccurrences(of: ",", with: "")
                TableTextFieldArray[10] = textAfterRemovingComma
            }
            else
            {
                ConfirmationVCObj.caseScenerioType = .AdjustAction
            }
            
            ConfirmationVCObj.descriptionArray = TableDescriptionArray
            
            ConfirmationVCObj.ValueArray = TableTextFieldArray
            
            ConfirmationVCObj.isPresented = false
            
            ConfirmationVCObj.delegate = self as popMyViewController
            
            self.navigationController?.pushViewController(ConfirmationVCObj, animated: true)
        }
    }
    
    func popNow() {
        
        MeterBillAmountParam = "" //Reset Meter Bill Amount Value
        
        self.navigationController?.popViewController(animated: false)
        if delegate != nil
        {
            delegate?.popNow()
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let customCell : AdjustAndPayVCTableViewCell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! AdjustAndPayVCTableViewCell
        customCell.descriptionLabelOutlet.text = TableDescriptionArray[indexPath.row]
        customCell.imageLabelOutlet.image = TableImageArray[indexPath.row]
        customCell.textFieldOutlet.text = TableTextFieldArray[indexPath.row] as? String
        customCell.textFieldOutlet.tag = setTag(LabelText: customCell.descriptionLabelOutlet.text!)
        
        if AdjustAndPayActionType == .AdjustAction
        {
            if customCell.textFieldOutlet.tag == 3013
            {
                customCell.textFieldOutlet.isUserInteractionEnabled = true
            }
            else
            {
                customCell.textFieldOutlet.isUserInteractionEnabled = false
            }
        }
        else
        {
            
            if customCell.textFieldOutlet.tag == 3013 || customCell.textFieldOutlet.tag == 3010 || customCell.textFieldOutlet.tag == 3011
            {
                customCell.textFieldOutlet.isUserInteractionEnabled = true
                
                
                if customCell.textFieldOutlet.tag == 3010 && !TableDescriptionArray.contains(appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount"))
                {
                    customCell.textFieldOutlet.isUserInteractionEnabled = false
                }
                else
                {
                    customCell.textFieldOutlet.isUserInteractionEnabled = true
                }
                
            }
            else
            {
                customCell.textFieldOutlet.isUserInteractionEnabled = false
            }
        }
        
        
        customCellRef = customCell
        
        
        return customCell
        
    }
    
    func setTag(LabelText : String) -> Int
    {
        switch LabelText
        {
            
            
        case appDelegate.getlocaLizationLanguage(key: "Division") : return 3000
            
        case appDelegate.getlocaLizationLanguage(key: "Township") : return 3001
            
        case appDelegate.getlocaLizationLanguage(key: "Meter Number") : return 3002
            
        case appDelegate.getlocaLizationLanguage(key: "Consumer Name (as on Bill)") : return 3003
            
        case appDelegate.getlocaLizationLanguage(key: "Ledger Number") : return 3004
            
        case appDelegate.getlocaLizationLanguage(key: "Meter Read Date") : return 3005
            
        case appDelegate.getlocaLizationLanguage(key: "Tariff") : return  3006
            
        case appDelegate.getlocaLizationLanguage(key: "Computer Code Number") : return 3007
            
        case appDelegate.getlocaLizationLanguage(key: "Pay For") : return 3008
            
        case appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number") : return 3009
            
        case appDelegate.getlocaLizationLanguage(key: "Current Paid Bill Amount") : return 3010
            
        case appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount") : return 3010
            
        case appDelegate.getlocaLizationLanguage(key: "Bill Amount") : return 3010
            
        case appDelegate.getlocaLizationLanguage(key: "Adjust Bill Amount") : return 3011
            
        case appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount") : return 3011
            
        case appDelegate.getlocaLizationLanguage(key: "Service Fees") : return 3012
            
        case appDelegate.getlocaLizationLanguage(key: "Remark") : return 3013
            
        default: return 3014
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return TableDescriptionArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func textFieldDidChange(_ sender: UITextField)
    {
        //Check if scenerio is Adjust or pay
        
        if AdjustAndPayActionType == .AdjustAction
        {
            
        }
        else if AdjustAndPayActionType == .PayAction
        {
            
            if sender.tag == 3010
            {
                
                sender.keyboardType = .numberPad
                if sender.text! != ""
                {
                    if (sender.text?.count)! >  10
                    {
                        // sender.text?.characters = (sender.text?.characters.dropLast())!
                        sender.text = String.init(sender.text!.remove(at: String.Index.init(encodedOffset: sender.text!.count - 1)))
                    }
                    else
                    {
                        let formatter = NumberFormatter()
                        formatter.numberStyle = NumberFormatter.Style.decimal
                        
                        //remove any existing commas
                        let textAfterRemovingComma = sender.text!.replacingOccurrences(of: ",", with: "")
                        
                        //let enteredAmount = Int(textAfterRemovingComma)!
                        //println_debug(".....>>>>\(enteredAmount)")
                        
                        //update the textField with commas
                        let formattedNum = formatter.string(from: NSNumber(value: Int(textAfterRemovingComma)!))
                        
                        sender.text = formattedNum!
                        TableTextFieldArray[10] = formattedNum!
                        
                        if TableTextFieldArray[10] as! String == TableTextFieldArray[11] as! String
                        {
                            submitBtnOutlet.isHidden = false
                        }
                        else
                        {
                            submitBtnOutlet.isHidden = true
                        }
                        
                    }
                    
                }
            }
            else if sender.tag == 3011
            {
                submitBtnOutlet?.isHidden = true
                sender.keyboardType = .numberPad
                if sender.text! != ""
                {
                    
                    let formatter = NumberFormatter()
                    formatter.numberStyle = NumberFormatter.Style.decimal
                    
                    //remove any existing commas
                    let currentTextWithoutComma = sender.text!.replacingOccurrences(of: ",", with: "")
                    
                    //Convert Entered Bill Amount to Without comma
                    let enteredBillAmount : NSString = self.TableTextFieldArray[10] as! NSString
                    let AlreadyEnteredAmountTextWithoutComma = enteredBillAmount.replacingOccurrences(of: ",", with: "")
                    
                    
                    if acceptableNumberString(inCharacter: AlreadyEnteredAmountTextWithoutComma , fromCharacter:currentTextWithoutComma) == true
                    {
                        println_debug("true")
                        //update the textField with commas
                        let formattedNum = formatter.string(from: NSNumber(value: Int(currentTextWithoutComma)!))
                        sender.text = formattedNum!
                        TableTextFieldArray[11] = formattedNum!
                        
                        if AlreadyEnteredAmountTextWithoutComma == currentTextWithoutComma && ( Int(currentTextWithoutComma)! >= 1000)
                        {
                            self.view.endEditing(true)
                            submitBtnOutlet?.isHidden = false
                            AdjustAmountParam = ""
                            MeterBillAmountParam = AlreadyEnteredAmountTextWithoutComma
                            
                        }
                        else
                        {
                            submitBtnOutlet?.isHidden = true
                        }
                        
                        
                    }
                    else
                    {
                        println_debug("False")
                        // sender.text?.characters = (sender.text?.characters.dropLast())!
                        sender.text = String.init(sender.text!.remove(at: String.Index.init(encodedOffset: sender.text!.count - 1)))
                        
                        // vinnu added
                        // sender.text = String(describing: sender.text?.dropLast())
                        
                    }
                }
                
            }
        }
        
        if sender.tag == 3013
        {
            TableTextFieldArray[TableTextFieldArray.count - 1] = sender.text!
            
        }
    }
    
    func acceptableNumberString(inCharacter chars: String, fromCharacter finalChar: String) -> Bool {
        if chars .hasPrefix(finalChar){
            return true
        } else {
            return false
        }
    }
    
    
}
