//
//  GetElectricityBillTableViewCell.swift
//  OK
//
//  Created by Rahul Tyagi on 28/07/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit

protocol ElectricityClearFieldDelegate : class {
    func clearButtonTapped(cell : GetElectricityBillTableViewCell)
}

class GetElectricityBillTableViewCell: UITableViewCell {
        
    @IBOutlet weak var DescriptionLabelOutlet: UILabel!{
        didSet{
            DescriptionLabelOutlet.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var imageViewOutlet: UIImageView!
    
    @IBOutlet weak var textFieldOutlet: customTextField! {
     didSet {
            self.textFieldOutlet.font = UIFont(name: appFont, size: appFontSize)
            self.textFieldOutlet.titleTextColour = UIColor.init(red: 28/255.0, green: 45/255.0, blue: 153/255.0, alpha: 1)
        }
    }
    
    @IBOutlet weak var operatorLabelOutlet: UILabel!{
        didSet{
            operatorLabelOutlet.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var dropDownBtnOutlet: UIButton!{
        didSet {
            dropDownBtnOutlet.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    @IBOutlet weak var blackDropDownBtnOutlet: UIButton!{
        didSet {
            blackDropDownBtnOutlet.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var selectContactsBtn: UIButton!{
        didSet {
            selectContactsBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var clearButton: UIButton!
    {
        didSet {
            self.clearButton.isHidden = true
        }
    }
    
    weak var delegate : ElectricityClearFieldDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func clearButtonAction(_ sender : UIButton)
    {
        self.delegate?.clearButtonTapped(cell: self)
    }
}
