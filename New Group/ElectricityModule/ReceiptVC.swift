//
//  ReceiptVC.swift
//  OK
//
//  Created by Rahul Tyagi on 8/10/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit

class ReceiptVC: OKBaseController  {
    //@IBOutlet weak var navigationBarOutlet: UINavigationBar!
    @IBOutlet weak var categoryDescriptionOutlet: UILabel!{
        didSet{
            categoryDescriptionOutlet.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var balanceDescriptionOutlet: UILabel!{
        didSet{
            balanceDescriptionOutlet.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var TransactionDescriptionOutlet: UILabel!{
        didSet{
            TransactionDescriptionOutlet.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var transactionTypeDescriptionOutlet: UILabel!{
        didSet{
            transactionTypeDescriptionOutlet.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var cashbackDescriptionOutlet: UILabel!{
        didSet{
            cashbackDescriptionOutlet.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var categoryValueOutlet: UILabel!{
        didSet{
            categoryValueOutlet.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var backToHomeBtnOutlet: UIButton!{
        didSet{
            backToHomeBtnOutlet.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var shareBtnOutlet: UIButton!
    @IBOutlet weak var receiptViewOutlet: UIView!
    @IBOutlet weak var payToValueOutlet: UILabel!{
        didSet{
            payToValueOutlet.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var usernameOutlet: UILabel!{
        didSet{
            usernameOutlet.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var transactionAmountOutlet: UILabel!{
        didSet{
            transactionAmountOutlet.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var transactionDateOutlet: UILabel!{
        didSet{
            transactionDateOutlet.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var transactionTimeOutlet: UILabel!{
        didSet{
            transactionTimeOutlet.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var transactionIDValueOutlet: UILabel!{
        didSet{
            transactionIDValueOutlet.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var balanceValueOutlet: UILabel!{
        didSet{
            balanceValueOutlet.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var cashbackValueOutlet: UILabel!{
        didSet{
            cashbackValueOutlet.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var homeLabelOutlet: UILabel!{
        didSet{
            homeLabelOutlet.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var shareLabelOutlet: UILabel!{
        didSet{
            shareLabelOutlet.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var nameOnTheBill: UILabel!{
        didSet{
            nameOnTheBill.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var monthOnTheBill: UILabel!{
        didSet{
            monthOnTheBill.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var serviceFees: UILabel!{
        didSet{
            serviceFees.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var totalPaidAmount: UILabel!{
        didSet{
            totalPaidAmount.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var nameonBillLabel: UILabel!{
        didSet{
            nameonBillLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var billonMonthLabel: UILabel!{
        didSet{
            billonMonthLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var totalpaidAmountLabeltitle: UILabel!{
        didSet{
            totalpaidAmountLabeltitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var menuimage: UIImageView!
    
    var isPresent = true
    var chainClass = GetElectricityBillVC()
    var GeneratePdfDict : NSMutableDictionary = ["":""]
    var ReceiptTransDict : Dictionary<String,String> = [:]
    var delegate : popMyViewController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        //receiptViewOutlet.layer.masksToBounds = false
        receiptViewOutlet.layer.shadowColor = UIColor.black.cgColor
        receiptViewOutlet.layer.shadowOffset = CGSize(width: 0.3, height: 3.0)
        receiptViewOutlet.layer.shadowOpacity = 0.45;
        self.helpSupportNavigationEnum = .Electricity
        self.loadNavigationBarSettings()
        self.leftBackButton()
        self.title = appDel.getlocaLizationLanguage(key: "Receipt")
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    func leftBackButton() {
        let backBtn = UIBarButtonItem(image: UIImage(named: "tabBarBack"), style: .plain, target: self, action: #selector(backButtonAction))
        self.navigationItem.leftBarButtonItem  = backBtn
    }
    
    @objc func backButtonAction() {
        if isPresent {
            self.navigationController?.popViewController(animated: false)
            chainClass.dismiss(animated: true, completion: nil)
            
            if MeterOptionType == .myMeterOption
            {
                chainClass.parentVC_meter.dismissNow()
            }
            else
            {
                chainClass.parentVC_other.dismissNow()
            }
        } else
        {
            //self.navigationController?.popToRootViewController(animated: true)
            self.navigationController?.popViewController(animated: false)
            if delegate != nil
            {
                delegate?.popNow()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let balanceAmount = (UserLogin.shared.walletBal as NSString).floatValue
        //let UtilObj = Utilities()
        //balanceAmount = UtilObj.roundOffMoney(balanceAmount as String!)
        
        usernameOutlet.text = ReceiptTransDict["username"]
        
        //Amount String Formatting
        
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.decimal
        
        //remove any existing commas
        let serviceFeesAmount = (GeneratePdfDict["ServiceFee"] as! NSString).floatValue
        let transAmount = (GeneratePdfDict["paidamount"] as! NSString).floatValue
        
        let formattedTransAmount = formatter.string(from: NSNumber(value: Int(transAmount)))
        
        let customAttribute1 = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 21) ?? UIFont.systemFont(ofSize: 21),
                                 NSAttributedString.Key.foregroundColor: UIColor.red ]
        let attributedAmountString = NSMutableAttributedString.init(string: formattedTransAmount!, attributes: customAttribute1)
        
        let customAttribute2 = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 16) ?? UIFont.systemFont(ofSize: 16),
                                 NSAttributedString.Key.foregroundColor: UIColor.red ]
        let mmkString = NSMutableAttributedString.init(string: " MMK", attributes: customAttribute2)
        
        attributedAmountString.append(mmkString)
        
        let formattedTransAmount1 = formatter.string(from: NSNumber(value: Int(serviceFeesAmount)))
        let attributedserviceString = NSMutableAttributedString.init(string: formattedTransAmount1!, attributes: customAttribute1)
        
        attributedserviceString.append(mmkString)
        
        transactionAmountOutlet.attributedText = attributedAmountString
        //End of Amount String Formatting
        
        transactionDateOutlet.text = ReceiptTransDict["transactionDate"]
        transactionTimeOutlet.text = ReceiptTransDict["transactionTime"]
        transactionIDValueOutlet.text = ReceiptTransDict["transactionID"]
        balanceValueOutlet.text = "\((balanceAmount) - transAmount)"
        cashbackValueOutlet.attributedText = attributedserviceString
        nameOnTheBill.text = GeneratePdfDict["senderaccountname"] as? String ?? ""
        monthOnTheBill.text = GeneratePdfDict["billMonth"] as? String ?? ""
        serviceFees.attributedText = attributedserviceString
        totalPaidAmount.attributedText = attributedAmountString
        setLang()
    }
    
    func setLang()
    {
        shareLabelOutlet.text = appDel.getlocaLizationLanguage(key: "Share")
        homeLabelOutlet.text = appDel.getlocaLizationLanguage(key: "Home Page")
        categoryDescriptionOutlet.text = appDel.getlocaLizationLanguage(key: "Categories")
        balanceDescriptionOutlet.text = appDel.getlocaLizationLanguage(key: "Balance")
        TransactionDescriptionOutlet.text = appDel.getlocaLizationLanguage(key: "Transaction ID")
        transactionTypeDescriptionOutlet.text = appDel.getlocaLizationLanguage(key: "Transaction Type")
        cashbackDescriptionOutlet.text = appDel.getlocaLizationLanguage(key: "Service Fees")
        categoryValueOutlet.text = appDel.getlocaLizationLanguage(key: "Electricity")
        payToValueOutlet.text = appDel.getlocaLizationLanguage(key: "Bill Payments")
        self.title = appDel.getlocaLizationLanguage(key: "Receipt")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BackBtnAction(_ sender: Any)
    {
        if isPresent {
            self.navigationController?.popViewController(animated: false)
            chainClass.dismiss(animated: true, completion: nil)
            
            if MeterOptionType == .myMeterOption
            {
                chainClass.parentVC_meter.dismissNow()
            }
            else
            {
                chainClass.parentVC_other.dismissNow()
            }
        } else
        {
            self.navigationController?.popViewController(animated: false)
            if delegate != nil
            {
                delegate?.popNow()
            }
        }
    }
    
    func generateQRCodeAct()  {
        let agentNameValue = UserModel.shared.name
        let agentNum = UserModel.shared.formattedNumber

        let part1 = "\(agentNameValue)" + "," + (agentNum) + ", " + transactionIDValueOutlet.text!
        let part2 =  "," + "\(ReceiptTransDict["transactionDate"]!)" + "," + "\(GeneratePdfDict["paidamount"]!)"

        let finalEncyptFormation = "\(part1)---\(part2)"
        let qrImageObject = PTQRGenerator()
        guard let qrImage =  qrImageObject.getQRImage(stringQR: finalEncyptFormation, withSize: 10) else { return  }
        saveImageDocumentDirectory(QRImage: qrImage, name: "\(ReceiptTransDict["transactionDate"]!)\(ReceiptTransDict["transactionTime"]!)")
    }
     
     func saveImageDocumentDirectory(QRImage : UIImage, name: String)
     {
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(name)
        println_debug(paths)
        let imageData = QRImage.jpegData(compressionQuality: 0.1)
        fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
        
        GeneratePdfDict["QRimagePath"] = paths
        generateAndSharePDF(qrImage : QRImage)
    }
     
    func generateAndSharePDF(qrImage : UIImage) {
        guard let safeDict = GeneratePdfDict as? Dictionary<String, Any> else { return }
        var fileName = "Electricity Bill Receipt"
        if let receiptDate = transactionDateOutlet.text {
            fileName = fileName + ", \(receiptDate)"
        }
        self.generateViewForBill(dictionay: safeDict, qrImage: qrImage, fileName: fileName)
     }
     
 
    fileprivate func saveSnapToGallery() {
        let img = self.receiptViewOutlet.snapshotOfCustomeView
        img.saveImageToGallery()
    }
    
    fileprivate func generatePdfandShare(_ completionHandler: @escaping (_ accessGranted: Bool, _ pdfUrl : URL) -> Void) {
        guard let pdfUrl = CodeSnippets.generatePDF(viewForPDF: receiptViewOutlet, pdfFile: "OK$ Electricity Bill Receipt, \(String(describing: transactionDateOutlet.text))") else {
            println_debug("Error - pdf not generated")
            return
        }
        completionHandler(true, pdfUrl)
    }
    
    @IBAction func shareBtnAction(_ sender: Any) {
        self.saveSnapToGallery()
        generateQRCodeAct()
    }
}

protocol popMyViewController
{
    func popNow()
}

protocol DismissMyViewController
{
    func DismissNow()
}
