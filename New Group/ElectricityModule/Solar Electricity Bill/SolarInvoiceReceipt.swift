//
//  SolarInvoiceReceipt.swift
//  OK
//
//  Created by Shobhit Singhal on 7/13/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class SolarInvoiceReceipt: UIViewController {

    @IBOutlet weak var transactionReceiptView: UIView!
    @IBOutlet weak var transactionReceiptView2: UIView!
    @IBOutlet weak var amountPaidView: UIView!
    @IBOutlet weak var amountPaidView2: UIView!
    @IBOutlet weak var businessNameView: UIView!
    @IBOutlet var businessNameLabel: UILabel!{
        didSet
        {
            businessNameLabel.font = UIFont(name: appFont, size: appFontSize)
            businessNameLabel.text = businessNameLabel.text?.localized
        }
    }
    @IBOutlet var senderAccNameLabel: UILabel!{
        didSet
        {
            senderAccNameLabel.font = UIFont(name: appFont, size: appFontSize)
            senderAccNameLabel.text = senderAccNameLabel.text?.localized
        }
    }
    @IBOutlet var senderAccNumberLabel: UILabel!{
        didSet
        {
            senderAccNumberLabel.font = UIFont(name: appFont, size: appFontSize)
            senderAccNumberLabel.text = senderAccNumberLabel.text?.localized
        }
    }
    @IBOutlet var solarMeterNumberLabel: UILabel!{
        didSet
        {
            solarMeterNumberLabel.font = UIFont(name: appFont, size: appFontSize)
            solarMeterNumberLabel.text = solarMeterNumberLabel.text?.localized
        }
    }
    @IBOutlet var transactionIdLabel: UILabel!{
        didSet
        {
            transactionIdLabel.font = UIFont(name: appFont, size: appFontSize)
            transactionIdLabel.text = transactionIdLabel.text?.localized
        }
    }
    @IBOutlet var transactionTypeLabel: UILabel!{
        didSet
        {
            transactionTypeLabel.font = UIFont(name: appFont, size: appFontSize)
            transactionTypeLabel.text = transactionTypeLabel.text?.localized
        }
    }
    @IBOutlet var serviceFeesLabel: UILabel!{
        didSet
        {
            serviceFeesLabel.font = UIFont(name: appFont, size: appFontSize)
            serviceFeesLabel.text = serviceFeesLabel.text?.localized
        }
    }
    @IBOutlet var dateTimeLabel: UILabel!{
        didSet
        {
            dateTimeLabel.font = UIFont(name: appFont, size: appFontSize)
            dateTimeLabel.text = dateTimeLabel.text?.localized
        }
    }
    @IBOutlet var totalMeterBillLabel: UILabel!{
        didSet
        {
            totalMeterBillLabel.font = UIFont(name: appFont, size: appFontSize)
            totalMeterBillLabel.text = totalMeterBillLabel.text?.localized
        }
    }

    @IBOutlet var senderAccNameLabel2: UILabel!{
        didSet
        {
            senderAccNameLabel2.font = UIFont(name: appFont, size: appFontSize)
            senderAccNameLabel2.text = senderAccNameLabel2.text?.localized
        }
    }
    @IBOutlet var senderAccNumberLabel2: UILabel!{
        didSet
        {
            senderAccNumberLabel2.font = UIFont(name: appFont, size: appFontSize)
            senderAccNumberLabel2.text = senderAccNumberLabel2.text?.localized
        }
    }
    @IBOutlet var solarMeterNumberLabel2: UILabel!{
        didSet
        {
            solarMeterNumberLabel2.font = UIFont(name: appFont, size: appFontSize)
            solarMeterNumberLabel2.text = solarMeterNumberLabel2.text?.localized
        }
    }
    @IBOutlet var transactionIdLabel2: UILabel!{
        didSet
        {
            transactionIdLabel2.font = UIFont(name: appFont, size: appFontSize)
            transactionIdLabel2.text = transactionIdLabel2.text?.localized
        }
    }
    @IBOutlet var transactionTypeLabel2: UILabel!{
        didSet
        {
            transactionTypeLabel2.font = UIFont(name: appFont, size: appFontSize)
            transactionTypeLabel2.text = transactionTypeLabel2.text?.localized
        }
    }
    @IBOutlet var serviceFeesLabel2: UILabel!{
        didSet
        {
            serviceFeesLabel2.font = UIFont(name: appFont, size: appFontSize)
            serviceFeesLabel2.text = serviceFeesLabel2.text?.localized
        }
    }
    @IBOutlet var dateTimeLabel2: UILabel!{
        didSet
        {
            dateTimeLabel2.font = UIFont(name: appFont, size: appFontSize)
            dateTimeLabel2.text = dateTimeLabel2.text?.localized
        }
    }
    @IBOutlet var totalMeterBillLabel2: UILabel!{
        didSet
        {
            totalMeterBillLabel2.font = UIFont(name: appFont, size: appFontSize)
            totalMeterBillLabel2.text = totalMeterBillLabel2.text?.localized
        }
    }
    @IBOutlet var meterBillAmountLabel2: UILabel!{
        didSet
        {
            meterBillAmountLabel2.font = UIFont(name: appFont, size: appFontSize)
            meterBillAmountLabel2.text = meterBillAmountLabel2.text?.localized
        }
    }

    @IBOutlet var businessNameLabelValue: UILabel!{
        didSet
        {
            businessNameLabelValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var senderAccNameLabelValue: UILabel!{
        didSet
        {
            senderAccNameLabelValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var senderAccNumberLabelValue: UILabel!{
        didSet
        {
            senderAccNumberLabelValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var solarMeterNumberLabelValue: UILabel!{
        didSet
        {
            solarMeterNumberLabelValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var transactionIdLabelValue: UILabel!{
        didSet
        {
            transactionIdLabelValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var transactionTypeLabelValue: UILabel!{
        didSet
        {
            transactionTypeLabelValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var serviceFeesLabelValue: UILabel!{
        didSet
        {
            serviceFeesLabelValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var dateTimeLabelValue: UILabel!{
        didSet
        {
            dateTimeLabelValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var totalMeterBillAmount: UILabel!{
        didSet
        {
            totalMeterBillAmount.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet var senderAccNameLabelValue2: UILabel!{
        didSet
        {
            senderAccNameLabelValue2.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var senderAccNumberLabelValue2: UILabel!{
        didSet
        {
            senderAccNumberLabelValue2.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var solarMeterNumberLabelValue2: UILabel!{
        didSet
        {
            solarMeterNumberLabelValue2.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var transactionIdLabelValue2: UILabel!{
        didSet
        {
            transactionIdLabelValue2.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var transactionTypeLabelValue2: UILabel!{
        didSet
        {
            transactionTypeLabelValue2.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var serviceFeesLabelValue2: UILabel!{
        didSet
        {
            serviceFeesLabelValue2.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var dateTimeLabelValue2: UILabel!{
        didSet
        {
            dateTimeLabelValue2.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var totalMeterBillAmount2: UILabel!{
        didSet
        {
            totalMeterBillAmount2.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var meterBillAmount2: UILabel!{
        didSet
        {
            meterBillAmount2.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var shareBtn: UIButton!{
        didSet{
            shareBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            shareBtn.setTitle(shareBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var businessViewHeightConstraint: NSLayoutConstraint!

    var paymentResultDict = [String: String]()
    let validObj = PayToValidations()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.title = "Invoice Receipt".localized;
        amountPaidView.layer.borderWidth = 1
        amountPaidView.layer.borderColor = UIColor.colorWithRedValue(redValue: 60, greenValue: 60, blueValue: 60, alpha: 1).cgColor
        amountPaidView2.layer.borderWidth = 1
        amountPaidView2.layer.borderColor = UIColor.colorWithRedValue(redValue: 60, greenValue: 60, blueValue: 60, alpha: 1).cgColor
        self.initLoad()
    }
    
    func initLoad() {
        if UserModel.shared.agentType == .user {
            businessNameView.isHidden = true
            businessViewHeightConstraint.constant = 0
        } else {
            businessNameView.isHidden = false
            businessViewHeightConstraint.constant = 45
        }
        businessNameLabelValue.text = UserModel.shared.businessName
        senderAccNameLabelValue.text = UserModel.shared.name
        senderAccNumberLabelValue.text = paymentResultDict["mobile_Number"]
        solarMeterNumberLabelValue.text = paymentResultDict["solarAccountNumber"]
        transactionIdLabelValue.text = paymentResultDict["transaction_Id"]
        transactionTypeLabelValue.text = paymentResultDict["transaction_Type"]
        serviceFeesLabelValue.text = paymentResultDict["service_Fees"]
        dateTimeLabelValue.text = "\(paymentResultDict["date"]!) \(paymentResultDict["time"]!)"
        
        totalMeterBillAmount.text = paymentResultDict["total_amount"]

        senderAccNameLabelValue2.text = (UserModel.shared.name.replacingOccurrences(of: ",", with: ". "))
        let num = "0\(UserModel.shared.mobileNo.dropFirst(4))"
        senderAccNumberLabelValue2.text = "0\(UserModel.shared.mobileNo.dropFirst(4)) \(validObj.getNumberRangeValidation(num).operator)"
        solarMeterNumberLabelValue2.text = paymentResultDict["solarAccountNumber"]
        transactionIdLabelValue2.text = paymentResultDict["transaction_Id"]
        transactionTypeLabelValue2.text = paymentResultDict["transaction_Type"]
        serviceFeesLabelValue2.text = paymentResultDict["service_Fees"]
        dateTimeLabelValue2.text = "\(paymentResultDict["date"]!) \(paymentResultDict["time"]!)"
        totalMeterBillAmount2.text = paymentResultDict["total_amount"]
        meterBillAmount2.text = paymentResultDict["amount_Solar_Pay"]
    }

    @IBAction func dismissControllerAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func sharePdfAction(_ sender: UIButton) {
        let pdfUrl = self.createPdfFromView(aView: transactionReceiptView2, saveToDocumentsWithFileName: "Transaction Receipt".localized)
        let activityViewController = UIActivityViewController(activityItems: [pdfUrl!], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }

    func createPdfFromView(aView: UIView, saveToDocumentsWithFileName pdfFileName: String) -> URL? {
        let render = UIPrintPageRenderer()
        let page = CGRect(x: 0, y: 0, width: 595.2, height: 841)
        let printable = page.insetBy(dx: 0, dy: 0)
        render.setValue(NSValue(cgRect: page), forKey: "paperRect")
        render.setValue(NSValue(cgRect: printable), forKey: "printableRect")
        let priorBounds = aView.bounds
        let fittedSize = aView.sizeThatFits(CGSize(width:priorBounds.size.width, height:aView.bounds.size.height))
        aView.bounds = CGRect(x:0, y:0, width:fittedSize.width, height:fittedSize.height)
        let pdfPageBounds = CGRect(x:0, y:0, width:aView.frame.width, height:self.view.frame.height)
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds,nil)
        let pageOriginY: CGFloat = 0
        UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
        UIGraphicsGetCurrentContext()!.saveGState()
        UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: -pageOriginY)
        aView.layer.render(in: UIGraphicsGetCurrentContext()!)
        UIGraphicsGetCurrentContext()!.restoreGState()
        UIGraphicsEndPDFContext()
        aView.bounds = priorBounds
        
        guard let documentDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else  { return nil}
        let properPDFFileName = pdfFileName.replacingOccurrences(of: ".pdf", with: "") + ".pdf"
        let pdfUrl = documentDirectories + "/" + properPDFFileName
        //Delete file if alreay exists
        do {
            try FileManager.default.removeItem(atPath: pdfUrl)
        } catch let error as NSError {
            println_debug("Error: \(error.domain)")
        }
        //Write file
        if pdfData.write(toFile: pdfUrl, atomically: true) {
            println_debug("Successfully wrote to \(pdfUrl)")
            
            let pathUrl = URL(fileURLWithPath: documentDirectories)
            let fileUrl = pathUrl.appendingPathComponent(properPDFFileName)
            return fileUrl
        }
        return nil
    }
}
