//
//  ReceiptSolarBill.swift
//  OK
//
//  Created by Shobhit Singhal on 7/12/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ReceiptSolarBill: OKBaseController {
    @IBOutlet weak var receiptView: UIView!
    @IBOutlet weak var receiptShareView: UIView!
    @IBOutlet var home: UITabBarItem!
    @IBOutlet var more: UITabBarItem!
    @IBOutlet weak var dateLabel: UILabel!{
        didSet{
            dateLabel.font = UIFont(name : appFont, size :appFontSize)
        }
    }
    @IBOutlet weak var timeLabel: UILabel!{
        didSet{
            timeLabel.font = UIFont(name : appFont, size :appFontSize)
        }
    }
    @IBOutlet weak var dateLabel2: UILabel!{
        didSet{
            dateLabel2.font = UIFont(name : appFont, size :appFontSize)
        }
    }
    @IBOutlet weak var timeLabel2: UILabel!{
        didSet{
            timeLabel2.font = UIFont(name : appFont, size :appFontSize)
        }
    }
    @IBOutlet weak var categoriesLabelValue: UILabel!{
        didSet{
            categoriesLabelValue.font = UIFont(name : appFont, size :appFontSize)
        }
    }
    @IBOutlet weak var transactionIdLabelValue: UILabel!{
        didSet{
            transactionIdLabelValue.font = UIFont(name : appFont, size :appFontSize)
        }
    }
    @IBOutlet weak var transactionTypeLabelValue: UILabel!{
        didSet{
            transactionTypeLabelValue.font = UIFont(name : appFont, size :appFontSize)
        }
    }
    @IBOutlet weak var solarAccountNumberLabelValue: UILabel!{
        didSet{
            solarAccountNumberLabelValue.font = UIFont(name : appFont, size :appFontSize)
        }
    }
    @IBOutlet weak var transactionIdLabelValue2: UILabel!{
        didSet{
            transactionIdLabelValue2.font = UIFont(name : appFont, size :appFontSize)
        }
    }
    @IBOutlet weak var transactionTypeLabelValue2: UILabel!{
        didSet{
            transactionTypeLabelValue2.font = UIFont(name : appFont, size :appFontSize)
        }
    }
    @IBOutlet weak var solarAccountNumberLabelValue2: UILabel!{
        didSet{
            solarAccountNumberLabelValue2.font = UIFont(name : appFont, size :appFontSize)
        }
    }
    @IBOutlet weak var balanceLabelValue: UILabel!{
        didSet{
            balanceLabelValue.font = UIFont(name : appFont, size :appFontSize)
        }
    }
    @IBOutlet weak var serviceFeesLabelValue: UILabel!{
        didSet{
            serviceFeesLabelValue.font = UIFont(name : appFont, size :appFontSize)
        }
    }
    @IBOutlet weak var serviceFeesLabelValue2: UILabel!{
        didSet{
            serviceFeesLabelValue2.font = UIFont(name : appFont, size :appFontSize)
        }
    }
    @IBOutlet weak var userNameLabel: UILabel!{
        didSet{
            userNameLabel.font = UIFont(name : appFont, size :appFontSize)
        }
    }
    @IBOutlet weak var amountPayLabel: UILabel!{
        didSet{
            amountPayLabel.font = UIFont(name : appFont, size :appFontSize)
        }
    }
    @IBOutlet weak var userNameLabel2: UILabel!{
        didSet{
            userNameLabel2.font = UIFont(name : appFont, size :appFontSize)
        }
    }
    @IBOutlet weak var amountPayLabel2: UILabel!{
        didSet{
            amountPayLabel2.font = UIFont(name : appFont, size :appFontSize)
        }
    }

    @IBOutlet weak var categoriesLabel: UILabel!{
        didSet {
            categoriesLabel.font = UIFont(name : appFont, size :appFontSize)
            categoriesLabel.text = categoriesLabel.text?.localized
        }
    }
    @IBOutlet weak var transactionIdLabel: UILabel!{
        didSet {
            transactionIdLabel.font = UIFont(name : appFont, size :appFontSize)
            transactionIdLabel.text = transactionIdLabel.text?.localized
        }
    }
    @IBOutlet weak var transactionIdLabel2: UILabel!{
        didSet {
            transactionIdLabel2.font = UIFont(name : appFont, size :appFontSize)
            transactionIdLabel2.text = transactionIdLabel2.text?.localized
        }
    }
    @IBOutlet weak var transactionTypeLabel: UILabel!{
        didSet {
            transactionTypeLabel.font = UIFont(name : appFont, size :appFontSize)
            transactionTypeLabel.text = transactionTypeLabel.text?.localized
        }
    }
    @IBOutlet weak var transactionTypeLabel2: UILabel!{
        didSet {
            transactionTypeLabel2.font = UIFont(name : appFont, size :appFontSize)
            transactionTypeLabel2.text = transactionTypeLabel2.text?.localized
        }
    }
    @IBOutlet weak var solarAccountNumberLabel: UILabel!{
        didSet {
            solarAccountNumberLabel.font = UIFont(name : appFont, size :appFontSize)
            solarAccountNumberLabel.text = solarAccountNumberLabel.text?.localized
        }
    }
    @IBOutlet weak var solarAccountNumberLabel2: UILabel!{
        didSet {
            solarAccountNumberLabel2.font = UIFont(name : appFont, size :appFontSize)
            solarAccountNumberLabel2.text = solarAccountNumberLabel2.text?.localized
        }
    }
    @IBOutlet weak var balanceLabel: UILabel!{
        didSet {
            balanceLabel.font = UIFont(name : appFont, size :appFontSize)
            balanceLabel.text = balanceLabel.text?.localized
        }
    }
    @IBOutlet weak var serviceFeesLabel: UILabel!{
        didSet {
            serviceFeesLabel.font = UIFont(name : appFont, size :appFontSize)
            serviceFeesLabel.text = serviceFeesLabel.text?.localized
        }
    }
    @IBOutlet weak var serviceFeesLabel2: UILabel!{
        didSet {
            serviceFeesLabel2.font = UIFont(name : appFont, size :appFontSize)
            serviceFeesLabel2.text = serviceFeesLabel2.text?.localized
        }
    }
    var paymentResultDict = [String: String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.title = "Receipt".localized
        self.initLoad()
        self.receivedDataAfterSolarPay()
        self.setDataForScreenShot()
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont(name: appFont, size: 11) ?? UIFont.systemFont(ofSize: 11)], for: .selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont(name: appFont, size: 11) ?? UIFont.systemFont(ofSize: 11)], for: .normal)
        UITabBar.appearance().unselectedItemTintColor = .lightGray
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let color = UIColor.lightGray
        let attrFont = [NSAttributedString.Key.font: UIFont(name: appFont, size: 11.0) ?? UIFont.systemFont(ofSize: 11.0), NSAttributedString.Key.foregroundColor: color]
        
        self.home.setTitleTextAttributes(attrFont, for: .normal)
        self.more.setTitleTextAttributes(attrFont, for: .normal)
        self.home.title = "Home".localized
        
        self.more.title = "More".localized
        
    }

    func initLoad() {
        self.receiptView.layer.cornerRadius = 2
        self.receiptView.layer.shadowOpacity = 1
        self.receiptView.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.receiptView.layer.shadowRadius = 2
        self.receiptView.layer.shadowColor = UIColor.black.cgColor
        self.receiptView.layer.masksToBounds = false
    }
    
    func receivedDataAfterSolarPay() {
        userNameLabel.text = UserModel.shared.name
        amountPayLabel.text = paymentResultDict["amount_Solar_Pay"]
        categoriesLabelValue.text = "Solar home".localized
        transactionIdLabelValue.text = paymentResultDict["transaction_Id"]
        transactionTypeLabelValue.text = paymentResultDict["transaction_Type"]
        solarAccountNumberLabelValue.text = paymentResultDict["solarAccountNumber"]
        balanceLabelValue.text = paymentResultDict["balance"]
        serviceFeesLabelValue.text = paymentResultDict["service_Fees"]
        dateLabel.text = paymentResultDict["date"]
        timeLabel.text = paymentResultDict["time"]
    }
    
    func setDataForScreenShot() {
        userNameLabel2.text = UserModel.shared.name
        amountPayLabel2.text = paymentResultDict["amount_Solar_Pay"]
        transactionIdLabelValue2.text = paymentResultDict["transaction_Id"]
        transactionTypeLabelValue2.text = paymentResultDict["transaction_Type"]
        solarAccountNumberLabelValue2.text = paymentResultDict["solarAccountNumber"]
        serviceFeesLabelValue2.text = paymentResultDict["service_Fees"]
        dateLabel2.text = paymentResultDict["date"]
        timeLabel2.text = paymentResultDict["time"]
    }
}

extension ReceiptSolarBill : UITabBarDelegate {
    
    //MARK:- Tabbar Delegate
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch(item) {
        case home : self.btnHomeClick(item)
        case more : self.btnMoreClick(item)
        default: break
        }
    }
    
    @IBAction func btnMoreClick(_ sender: UITabBarItem){
        self.performSegue(withIdentifier: "receiptToMoreOptionsScreen", sender: self)
    }
    
    @IBAction func btnHomeClick(_ sender: UITabBarItem){
        performSegue(withIdentifier: "unwindSegueToVC3", sender: self)
    }
}

extension ReceiptSolarBill : InvoiceActionPerformDelegate {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "receiptToMoreOptionsScreen" {
            let controller = segue.destination as! MoreOptionsScreen
            controller.delegate = self
        }
        else if segue.identifier == "moreOptionsToSolarInvoiceReceipt" {
            let vc = segue.destination as! UINavigationController
            let rootViewController = vc.viewControllers.first as! SolarInvoiceReceipt
            rootViewController.paymentResultDict = self.paymentResultDict
        }
    }
    
    func dismissMoreOptionsScreenOnClickMorePayment() {
        self.addMoney()
    }
    
    func dismissMoreOptionsScreenOnClickInvoice() {
        self.performSegue(withIdentifier: "moreOptionsToSolarInvoiceReceipt", sender: self)
    }
    
    func dismissMoreOptionsScreenOnClickShare() {
        let image = self.captureScreen()
        let imageToShare = [image!]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
    
    func addMoney() {
        alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
        alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
            if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                addWithdrawView.modalPresentationStyle = .fullScreen
                self.present(addWithdrawView, animated: true, completion: nil)
            }
        })
        alertViewObj.showAlert()
    }
    
    func captureScreen() -> UIImage? {
        let bounds = receiptShareView.bounds
        UIGraphicsBeginImageContextWithOptions(bounds.size, true, 0.0)
        receiptShareView.drawHierarchy(in: bounds, afterScreenUpdates: false)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}


