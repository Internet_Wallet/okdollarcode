//
//  MoreOptionsScreen.swift
//  OK
//
//  Created by Shobhit Singhal on 7/12/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol InvoiceActionPerformDelegate:class {
    func dismissMoreOptionsScreenOnClickInvoice()
    func dismissMoreOptionsScreenOnClickShare()
    func dismissMoreOptionsScreenOnClickMorePayment()
}

class MoreOptionsScreen: UIViewController {
    weak var delegate: InvoiceActionPerformDelegate?

    @IBOutlet var repeatPaymentBtn: UIButton!{
        didSet{
            repeatPaymentBtn.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
            repeatPaymentBtn.setTitle(repeatPaymentBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet var invoiceBtn: UIButton!{
        didSet{
            invoiceBtn.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
            invoiceBtn.setTitle(invoiceBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet var shareBtn: UIButton!{
        didSet{
            shareBtn.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
            shareBtn.setTitle(shareBtn.titleLabel?.text?.localized, for: .normal)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func dismissViewAction(_ sender: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.20, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.dismiss(animated: true, completion: nil)
            }
        });
    }
    
    @IBAction func repaymentAction(_ sender: UIButton) {
        if (UserLogin.shared.walletBal as NSString).floatValue < 500.00 {
            self.dismiss(animated: false, completion: nil)
            self.delegate?.dismissMoreOptionsScreenOnClickMorePayment()
            return
        }
        self.performSegue(withIdentifier: "repeatPaymentToSolarBill", sender: self)
    }

    @IBAction func invoiceAction(_ sender: UIButton) {
        UIView.animate(withDuration: 0.15, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished){
                self.dismiss(animated: true, completion: nil)
                self.delegate?.dismissMoreOptionsScreenOnClickInvoice()
            }
        });
    }

    @IBAction func sharePdfAction(_ sender: UIButton) {
        UIView.animate(withDuration: 0.15, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished){
                self.dismiss(animated: true, completion: nil)
                self.delegate?.dismissMoreOptionsScreenOnClickShare()
            }
        });
    }
    
    func addMoney(_ vc: UIViewController) {
        alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
        alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
            if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                addWithdrawView.modalPresentationStyle = .fullScreen
                self.present(addWithdrawView, animated: true, completion: nil)
            }
        })
        alertViewObj.showAlert()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "repeatPaymentToSolarBill" {
            let vc = segue.destination as! UINavigationController
            let rootViewController = vc.viewControllers.first as! ElectricityBillSolar
            rootViewController.isRepeatPayment = true
        }
    }
}
