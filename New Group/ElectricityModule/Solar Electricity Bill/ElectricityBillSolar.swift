//
//  ElectricityBillSolar.swift
//  OK
//
//  Created by Shobhit Singhal on 7/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import IQKeyboardManagerSwift

class ElectricityBillSolar: OKBaseController {
    @IBOutlet weak var backView : UIView!
    @IBOutlet weak var firstView : UIView!
    @IBOutlet weak var secondView : UIView!
    @IBOutlet weak var remarksView : UIView!
    @IBOutlet weak var nameTF: SkyFloatingLabelTextField!{
        didSet{
            nameTF.font = UIFont(name: appFont, size: appFontSize)
            nameTF.titleFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
            nameTF.placeholderFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
        }
    }
    @IBOutlet weak var mobileNumberTF: SkyFloatingLabelTextField!{
        didSet{
            mobileNumberTF.font = UIFont(name: appFont, size: appFontSize)
            mobileNumberTF.titleFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
            mobileNumberTF.placeholderFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
        }
    }
    @IBOutlet weak var solarAccountNumberTF: SkyFloatingLabelTextField!{
        didSet{
            solarAccountNumberTF.font = UIFont(name: appFont, size: appFontSize)
            solarAccountNumberTF.titleFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
            solarAccountNumberTF.placeholderFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
        }
    }
    @IBOutlet weak var billAmountTF: SkyFloatingLabelTextField!{
        didSet{
            billAmountTF.font = UIFont(name: appFont, size: appFontSize)
            billAmountTF.titleFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
            billAmountTF.placeholderFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
        }
    }
    @IBOutlet weak var remarksTF: SkyFloatingLabelTextField!{
        didSet{
            remarksTF.font = UIFont(name: appFont, size: appFontSize)
            remarksTF.titleFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
            remarksTF.placeholderFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
        }
    }
    @IBOutlet var submitBtn: UIButton!{
        didSet{
            submitBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            submitBtn.setTitle(submitBtn.titleLabel?.text?.localized, for: .normal)
        }
    }

    @IBOutlet weak var backViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomBtnConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightMainViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnSolarAccountNumber: UIButton!
    
    var whichApiCall:String = ""
    var tokenFullString = ""
    var dataDict = [String: String]()
    let web = WebApiClass()
    var isRepeatPayment = false
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.title = "Electricity Bill Solar".localized
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
        IQKeyboardManager.sharedManager().keyboardDistanceFromTextField = 50
        self.initLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if isRepeatPayment {
            solarAccountNumberTF.becomeFirstResponder()
        } else {
            solarAccountNumberTF.becomeFirstResponder()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.sharedManager().keyboardDistanceFromTextField = 10
        IQKeyboardManager.sharedManager().enable = false
    }
    
    func initLoad() {
        
//        nameTF.font = UIFont(name : appFont, size :appFontSize)
//        mobileNumberTF.font = UIFont(name : appFont, size :appFontSize)
//        solarAccountNumberTF.font = UIFont(name : appFont, size :appFontSize)
//        billAmountTF.font = UIFont(name : appFont, size :appFontSize)
//        remarksTF.font = UIFont(name : appFont, size :appFontSize)
        
        nameTF.placeholder = "Name".localized
        mobileNumberTF.placeholder = "Mobile Number".localized
        solarAccountNumberTF.placeholder = "Enter Solar Account Number".localized
        billAmountTF.placeholder = "Enter Bill Amount".localized
        remarksTF.placeholder = "Enter Remarks".localized
        self.backView.layer.cornerRadius = 6
        self.backView.layer.shadowOpacity = 0.35
        self.backView.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.backView.layer.shadowRadius = 6
        self.backView.layer.shadowColor = UIColor.black.cgColor
        self.backView.layer.masksToBounds = false
        self.submitBtn.isHidden = true
        nameTF.isUserInteractionEnabled = false
        mobileNumberTF.isUserInteractionEnabled = false
        backViewHeightConstraint.constant = 255
        heightMainViewConstraint.constant = 290
        secondView.isHidden = true
        nameTF.text = UserModel.shared.name
        mobileNumberTF.text = "(+95)0\(UserModel.shared.mobileNo.dropFirst(4))"
        subscribeToShowKeyboardNotifications()
        self.remarksView.isHidden = true
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.height
        UIView.animate(withDuration: 1, animations: { () -> Void in
            self.bottomBtnConstraint.constant = keyboardHeight
        })
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        bottomBtnConstraint.constant = 0
    }
    
    func subscribeToShowKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @IBAction func onClickSolarAccountNoAction(_ sender: Any) {
        self.solarAccountNumberTF.text = ""
        self.submitBtn.isHidden = true
        self.btnSolarAccountNumber.isHidden = true
    }
    

    @IBAction func dismissControllerAction(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "unwindSegueToVC4", sender: self)
    }

    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }

    @IBAction func submitAction(_ sender: UIButton) {
        let solarNumberStr = solarAccountNumberTF.text
        let testNumber = "^0*$"
        let mobileNumberPred = NSPredicate(format: "SELF MATCHES %@", testNumber)
        let matched: Bool = mobileNumberPred.evaluate(with: solarNumberStr)
        if matched {
            //contains all zero
            alertViewObj.wrapAlert(title: nil, body: "Please enter valid solar account number".localized, img: nil)
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        }
        else {
            if self.backViewHeightConstraint.constant == 255 {
                if appDelegate.checkNetworkAvail() {
                    self.view.endEditing(true)
                    getTokenFromServer(completionHandler: { (isSuccess, tokenString) in
                        guard isSuccess, let _ = tokenString else {
                            self.showErrorAlert(errMessage: "Try Again".localized)
                            return
                        }
                        DispatchQueue.main.async(execute: {
                            self.whichApiCall = "solarBillDetails"
                            self.callingHttp()
                        })
                    })
                }
                else {
                    alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
                    alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                    }
                    alertViewObj.showAlert(controller: UIViewController.init())
                }
            }
            else {
                if self.billAmountTF.text == "" {
                    alertViewObj.wrapAlert(title: nil, body: "Please enter amount".localized, img: nil)
                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                    })
                    alertViewObj.showAlert(controller: self)
                }
                else if NSString(string: (self.billAmountTF.text?.replacingOccurrences(of: ",", with: ""))!).floatValue < 500.00 {
                    alertViewObj.wrapAlert(title: nil, body: "Please enter minimum amount 500 MMK.".localized, img: nil)
                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                    })
                    alertViewObj.showAlert(controller: self)
                }
                else {
                    if appDelegate.checkNetworkAvail() {
                        self.view.endEditing(true)
                        getTokenFromServer(completionHandler: { (isSuccess, tokenString) in
                            guard isSuccess, let _ = tokenString else {
                                self.showErrorAlert(errMessage: "Try Again".localized)
                                return
                            }
                            DispatchQueue.main.async(execute: {
                                self.whichApiCall = "solarServiceFee"
                                self.callingHttp()
                            })
                        })
                    }
                    else {
                        alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
                        alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                        }
                        alertViewObj.showAlert(controller: UIViewController.init())
                    }
                }
            }
        }
    }

    func getTokenFromServer(completionHandler: @escaping (Bool,Any?) -> Void) {
        let urlStr = String.init(format: Url.SolarElectricityTokenAPIURL)
        let url: URL? = URL.init(string: urlStr)

        guard let tokenUrl = url else {
            showErrorAlert(errMessage: "Something went wrong : Dev need to check")
            return
        }
        let hashValue = Url.aKey_solor_bill.hmac_SHA1(key: Url.sKey_solor_bill)
        let inputVal = "password=\(hashValue)&grant_type=password"
        
        progressViewObj.showProgressView()
        println_debug("get token from server called........")
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: kMethod_Post, contentType: kContentType_urlencoded, inputVal: inputVal, authStr: nil)
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    println_debug("remove progress view called inside get token from server call")
                }
                completionHandler(false,nil)
                return
            }
            println_debug("response dict for get token from server :: \(String(describing: response))")
            let responseDic = response as! NSDictionary
            guard let tokentype = responseDic.value(forKey: "token_type"), let accesstoken = responseDic.value(forKey: "access_token") else {
                completionHandler(false,nil)
                return
            }
            let authorizationString =  "\(tokentype) \(accesstoken)"
            self.tokenFullString = authorizationString
            completionHandler(true,authorizationString)
        })
    }
    
    @IBAction func solarNumberEditingBegin(_ sender: SkyFloatingLabelTextField) {
        if self.backViewHeightConstraint.constant == 335 {
            remarksTF.resignFirstResponder()
        }
        solarAccountNumberTF.keyboardType = .numberPad
        solarAccountNumberTF.becomeFirstResponder()
    }
    
    @IBAction func solarNumberEditingChanged(_ sender: SkyFloatingLabelTextField) {
        backViewHeightConstraint.constant = 255
        heightMainViewConstraint.constant = 290
        secondView.isHidden = true
        if sender.text?.count == 0 {
            solarAccountNumberTF.placeholder = "Enter Solar Account Number".localized
            self.submitBtn.isHidden = true
            self.btnSolarAccountNumber.isHidden = true
        }
        else {
            solarAccountNumberTF.placeholder = "Solar Account Number".localized
            self.submitBtn.isHidden = false
            self.btnSolarAccountNumber.isHidden = false
        }
    }
    
    @IBAction func billAmountEditingChanged(_ sender: SkyFloatingLabelTextField) {
        if sender.text?.count == 0 {
            billAmountTF.placeholder = "Enter Bill Amount".localized
        }
        else {
            billAmountTF.placeholder = "Bill Amount".localized
        }
        if let text = sender.text, text.count > 0 {
            let formatter = NumberFormatter()
            formatter.numberStyle = NumberFormatter.Style.decimal
            
            //remove any existing commas
            let textAfterRemovingComma = text.replacingOccurrences(of: ",", with: "")
            let formattedNum = formatter.string(from: NSNumber(value: Int(textAfterRemovingComma)!))
            sender.text = formattedNum!
            if NSString(string: UserLogin.shared.walletBal).floatValue < NSString(string: text.replacingOccurrences(of: ",", with: "")).floatValue {
                self.billAmountTF.text = ""
                billAmountTF.placeholder = "Enter Bill Amount".localized
                alertViewObj.wrapAlert(title: "", body: "Insufficent Balance".localized, img: nil)
                alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {
                })
                alertViewObj.showAlert(controller: self)
            }
            if textAfterRemovingComma.count > 9 {
                sender.text = String((sender.text?.dropLast())!)
            }
        }
    }
    
    @IBAction func remarksEditingChanged(_ sender: SkyFloatingLabelTextField) {
        if sender.text?.count == 0 {
            remarksTF.placeholder = "Enter Remarks".localized
        }
        else {
            remarksTF.placeholder = "Remarks".localized
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "solarElectricityToConfirmationBillSolar" {
            let vc = segue.destination as! UINavigationController
            let rootViewController = vc.viewControllers.first as! ConfirmationBillSolarScreen
            rootViewController.dataDict = self.dataDict
        }
    }
}

extension ElectricityBillSolar : WebServiceResponseDelegate {
    func webResponse(withJson json: AnyObject, screen: String) {
    }
    
    func callingHttp() {
        if self.whichApiCall == "solarBillDetails" {
            if appDelegate.checkNetworkAvail() {
                let urlString =  Url.Get_Solor_Bill_Details + solarAccountNumberTF.text!
                guard let urlStr = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else { return }
                let apiUrl = URL.init(string: urlStr)
                println_debug(urlStr)
                let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: apiUrl!, methodType: kMethod_Get, contentType: kContentType_Json, inputVal: nil, authStr: self.tokenFullString)
                DispatchQueue.main.async {
//                    progressViewObj.showProgressView()
                    println_debug("progress view started")
                }

                JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
                    guard isSuccess else {
                        DispatchQueue.main.async {
                            println_debug("remove progress view called inside cashin main api call with error")
                            progressViewObj.removeProgressView()
                        }
                        self.showErrorAlert(errMessage: "Try Again".localized)
                        return
                    }
                    DispatchQueue.main.async(execute: {
                        if let responseDic = response as? Dictionary<String,Any> {
                            let jsonData = JSON(responseDic)
                            println_debug(jsonData)
                            if jsonData["StatusCode"].stringValue == "302" && jsonData["Status"].boolValue == true {
                                self.backViewHeightConstraint.constant = 335
                                self.heightMainViewConstraint.constant = 390
                                self.secondView.isHidden = false
                                self.billAmountTF.text = ""
                                self.remarksTF.text = ""
                                self.billAmountTF.becomeFirstResponder()
                            }
                            else {
                               
                                alertViewObj.wrapAlert(title: "".localized, body: jsonData["Message"].stringValue , img: #imageLiteral(resourceName: "alert-icon"))
                                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                    self.solarAccountNumberTF.text = ""
                                    self.solarAccountNumberTF.becomeFirstResponder()
                                    self.submitBtn.isHidden = true
                                    self.btnSolarAccountNumber.isHidden = true
                                }
                                alertViewObj.showAlert(controller: UIViewController.init())
                            }
                        }
                    })
                    progressViewObj.removeProgressView()
                })
            }
            else {
                alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                }
                alertViewObj.showAlert(controller: UIViewController.init())
            }
        }
        else if self.whichApiCall == "solarServiceFee" {
            if appDelegate.checkNetworkAvail() {
                let urlString =  Url.Solor_Service_Fee_Calculation
                guard let urlStr = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else { return }
                let apiUrl = URL.init(string: urlStr)
                println_debug(urlStr)
                let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: apiUrl!, methodType: kMethod_Get, contentType: kContentType_Json, inputVal: nil, authStr: self.tokenFullString)
                DispatchQueue.main.async {
//                    progressViewObj.showProgressView()
                    println_debug("progress view started")
                }
                
                JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
                    guard isSuccess else {
                        DispatchQueue.main.async {
                            println_debug("remove progress view called inside cashin main api call with error")
                            progressViewObj.removeProgressView()
                        }
                        self.showErrorAlert(errMessage: "Try Again".localized)
                        return
                    }
                    DispatchQueue.main.async(execute: {
                        if let responseDic = response as? Dictionary<String,Any> {
                            let jsonData = JSON(responseDic)
                            println_debug(jsonData)
                            if jsonData["StatusCode"].stringValue == "302" && jsonData["Status"].boolValue == true {
                                self.dataDict["name"] = self.nameTF.text
                                self.dataDict["mobile_Number"] = self.mobileNumberTF.text
                                self.dataDict["solar_Account_Number"] = self.solarAccountNumberTF.text
                                self.dataDict["bill_Amount"] = (self.billAmountTF.text?.replacingOccurrences(of: ",", with: ""))
                                self.dataDict["service_Fees"] = (jsonData["Result"]["ServiceCharges"].stringValue.replacingOccurrences(of: ",", with: ""))
//                                self.dataDict["service_Fees"] = jsonData["Result"]["ServiceCharges"].stringValue
                                self.dataDict["remarks"] = self.remarksTF.text
                                self.performSegue(withIdentifier: "solarElectricityToConfirmationBillSolar", sender: self)
                            }
                            else {
                                DispatchQueue.main.async {
                                    self.showAlert(alertTitle: "", alertBody: jsonData["Message"].stringValue , alertImage: #imageLiteral(resourceName: "payment_gateway_st"))
                                }
                                
                            }
                        }
                    })
                    progressViewObj.removeProgressView()
                })
            }
            else {
                alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                }
                alertViewObj.showAlert(controller: UIViewController.init())
            }
        }
    }
}

extension ElectricityBillSolar : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let chars = textField.text! + string;
        if textField == billAmountTF {
            if chars == "0" {
                return false
            }
            else {
                if string.isEmpty {
                    return true
                }
                let regex = "[0-9]{1,}"
                return NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: string)
            }
        }
        else if textField == remarksTF {
            if chars.trimmingCharacters(in: .whitespaces).isEmpty {
                return false
            }
            if textField.text?.last == " "  && string == " "{
                
                // If consecutive spaces entered by user
                return false
            }
        }
        else if textField == solarAccountNumberTF {
            if string.isEmpty {
                return true
            }
            let regex = "[0-9]{1,}"
            return NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: string)

//            if !chars.isNumber {
//                return false
//            }
        }
        return true
    }
}


