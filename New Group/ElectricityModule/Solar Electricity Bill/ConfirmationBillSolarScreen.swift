//
//  ConfirmationBillSolarScreen.swift
//  OK
//
//  Created by Shobhit Singhal on 7/11/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class ConfirmationBillSolarScreen: OKBaseController, BioMetricLoginDelegate {
    
    @IBOutlet weak var nameLabelValue : UILabel!{
        didSet{
            nameLabelValue.font = UIFont(name : appFont, size :appFontSize)
        }
    }
    @IBOutlet weak var mobileNumberLabelValue : UILabel!{
        didSet{
            mobileNumberLabelValue.font = UIFont(name : appFont, size :appFontSize)
        }
    }
    @IBOutlet weak var solarAccountNumberLabelValue : UILabel!{
        didSet{
            solarAccountNumberLabelValue.font = UIFont(name : appFont, size :appFontSize)
        }
    }
    @IBOutlet weak var billAmountLabelValue : UILabel!{
        didSet{
            billAmountLabelValue.font = UIFont(name : appFont, size :appFontSize)
        }
    }
    @IBOutlet weak var serviceFeesLabelValue : UILabel!{
        didSet{
            serviceFeesLabelValue.font = UIFont(name : appFont, size :appFontSize)
        }
    }
    @IBOutlet weak var remarksLabelValue : UILabel!{
        didSet{
            remarksLabelValue.font = UIFont(name : appFont, size :appFontSize)
        }
    }
    @IBOutlet weak var remarksLabelColon : UILabel!{
        didSet{
            remarksLabelColon.font = UIFont(name : appFont, size :appFontSize)
        }
    }
    @IBOutlet var confirmViewHeightConstraint: NSLayoutConstraint!
   
    var dataDict = [String: String]()
    var paymentResultDict = [String: String]()
    var tokenFullString = ""
    var totalAmountToPayString = ""
    let web = WebApiClass()

    @IBOutlet weak var nameLabel: UILabel!{
        didSet {
            nameLabel.font = UIFont(name : appFont, size :appFontSize)
            nameLabel.text = nameLabel.text?.localized
        }
    }
    @IBOutlet weak var mobileNumberLabel: UILabel!{
        didSet {
            mobileNumberLabel.font = UIFont(name : appFont, size :appFontSize)
            mobileNumberLabel.text = mobileNumberLabel.text?.localized
        }
    }
    @IBOutlet weak var solarAccountNumberLabel: UILabel!{
        didSet {
            solarAccountNumberLabel.font = UIFont(name : appFont, size :appFontSize)
            solarAccountNumberLabel.text = solarAccountNumberLabel.text?.localized
        }
    }
    @IBOutlet weak var billAmountLabel: UILabel!{
        didSet {
            billAmountLabel.font = UIFont(name : appFont, size :appFontSize)
            billAmountLabel.text = billAmountLabel.text?.localized
        }
    }
    @IBOutlet weak var serviceFeesLabel: UILabel!{
        didSet {
            serviceFeesLabel.font = UIFont(name : appFont, size :appFontSize)
            serviceFeesLabel.text = serviceFeesLabel.text?.localized
        }
    }
    @IBOutlet weak var remarksLabel: UILabel!{
        didSet {
            remarksLabel.font = UIFont(name : appFont, size :appFontSize)
            remarksLabel.text = remarksLabel.text?.localized
        }
    }
    @IBOutlet var payBtn: UIButton!{
        didSet{
            payBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            payBtn.setTitle(payBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.title = "Confirmation".localized
        self.getDataFromPreviousScreen()
        self.initLoad()
//        web.delegate = self
    }
    
    func initLoad() {
        if dataDict["remarks"] == "" {
            if screenWidth == 320 {
                confirmViewHeightConstraint.constant = 260
            }else {
                confirmViewHeightConstraint.constant = 260
            }
            remarksLabel.isHidden = true
            remarksLabelValue.isHidden = true
            remarksLabelColon.isHidden = true
        }
        else {
            if screenWidth == 320 {
                confirmViewHeightConstraint.constant = 300
            }else {
                confirmViewHeightConstraint.constant = 260
            }
            let font = UIFont.init(name: appFont, size: 15)
            let remarkLabelHeight = heightForView(text: self.remarksLabelValue.text!, font: font!, width: (screenWidth/2) - 42)
            if remarkLabelHeight > 30 {
                if screenWidth == 320 {
                    confirmViewHeightConstraint.constant = remarkLabelHeight + 300
                }else {
                    confirmViewHeightConstraint.constant = remarkLabelHeight + 260
                }
            }
        }
    }
    
    func getDataFromPreviousScreen() {
        nameLabelValue.text = dataDict["name"]
        mobileNumberLabelValue.text = dataDict["mobile_Number"]
        solarAccountNumberLabelValue.text = dataDict["solar_Account_Number"]
       
        let totalAmount : Double = (NSString(string: dataDict["bill_Amount"]!).doubleValue + NSString(string: dataDict["service_Fees"]!).doubleValue)
        let doubleStrBillAmount = String(format: "%.1f", totalAmount)
        totalAmountToPayString = doubleStrBillAmount
        
        if let billAmount = dataDict["bill_Amount"] {
            let myDouble = Double(billAmount)
            let doubleStrBillAmount = String(format: "%.2f", myDouble!)
            let billAmountStr = wrapAmountWithCommaDecimal(key: doubleStrBillAmount)
            billAmountLabelValue.text = billAmountStr
        }
        if let serviceFees = dataDict["service_Fees"] {
            let myDouble = Double(serviceFees)
            let doubleStrServiceFee = String(format: "%.2f", myDouble!)
            let serviceAmountStr = wrapAmountWithCommaDecimal(key: doubleStrServiceFee)
            serviceFeesLabelValue.text = serviceAmountStr
        }
        remarksLabelValue.text = dataDict["remarks"]
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }

    @IBAction func dismissControllerAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func payAction(_ sender: UIButton) {
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "SolarElectricity", delegate: self)
        }
        else {
            if appDelegate.checkNetworkAvail() {
                self.view.endEditing(true)
                getTokenFromServer(completionHandler: { (isSuccess, tokenString) in
                    guard isSuccess, let _ = tokenString else {
                        self.showErrorAlert(errMessage: "Try Again".localized)
                        return
                    }
                    DispatchQueue.main.async(execute: {
                        self.callingHttp()
                    })
                })
            }
            else {
                alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                }
                alertViewObj.showAlert(controller: UIViewController.init())
            }
        }
    }
    
    func getTokenFromServer(completionHandler: @escaping (Bool,Any?) -> Void) {
        let urlStr = String.init(format: Url.SolarElectricityTokenAPIURL)
        let url: URL? = URL.init(string: urlStr)
        guard let tokenUrl = url else {
            showErrorAlert(errMessage: "Something went wrong : Dev need to check")
            return
        }
        let hashValue = Url.aKey_solor_bill.hmac_SHA1(key: Url.sKey_solor_bill)
        let inputVal = "password=\(hashValue)&grant_type=password"
        
//        progressViewObj.showProgressView()
        println_debug("get token from server called........")
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: kMethod_Post, contentType: kContentType_urlencoded, inputVal: inputVal, authStr: nil)
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    println_debug("remove progress view called inside get token from server call")
                }
                completionHandler(false,nil)
                return
            }
            println_debug("response dict for get token from server :: \(String(describing: response))")
            let responseDic = response as! NSDictionary
            guard let tokentype = responseDic.value(forKey: "token_type"), let accesstoken = responseDic.value(forKey: "access_token") else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                }
                completionHandler(false,nil)
                return
            }
            let authorizationString =  "\(tokentype) \(accesstoken)"
            self.tokenFullString = authorizationString
            completionHandler(true,authorizationString)
        })
    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if screen == "SolarElectricity" {
            if isSuccessful{
                self.callingHttp()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "solarConfirmationToReceipt" {
            let vc = segue.destination as! UINavigationController
            let rootViewController = vc.viewControllers.first as! ReceiptSolarBill
            rootViewController.paymentResultDict = self.paymentResultDict
        }
    }
}

extension ConfirmationBillSolarScreen {
    func callingHttp() {
        if appDelegate.checkNetworkAvail() {
            let okAccountNumber = UserModel.shared.mobileNo
            let nameUser = nameLabelValue.text!
            let password : String = ok_password!
            
            let pipeLineStr = "OkAccountNumber=\(okAccountNumber)|Name=\(nameUser)|Password=\(password)|Amount=\(totalAmountToPayString)|SolarHomeAccountNumber=\(solarAccountNumberLabelValue.text!)"
            let hashValue: String = pipeLineStr.hmac_SHA1(key: Url.sKey_solor_bill)
            println_debug("revised hash value :: \(hashValue)")
            
            let keys : [String] = ["OkAccountNumber","Name","Password","Amount","SolarHomeAccountNumber","HashValue"]
            let values : [String] = [okAccountNumber,nameUser,password,totalAmountToPayString,solarAccountNumberLabelValue.text!,hashValue]
            let jsonObj = JSONStringWriter()
            let jsonText = jsonObj.writeJsonIntoString(keys, values)
            let urlString = Url.Solor_Bil_Pay
            
            guard let urlStr = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else { return }
            let apiUrl = URL.init(string: urlStr)
            let params = [String: AnyObject]() as AnyObject
            let httpData = jsonText.data(using: .utf8)!
            let header = String.init(format: "%@", tokenFullString)
            
            TopupWeb.genericApiWithHeader(url: apiUrl!, param: params as AnyObject, httpMethod: "POST", header: header, data: httpData) { (response, success) in
                DispatchQueue.main.async {
                    if success {
                        let jsonData = JSON(response)
                        println_debug(jsonData)
                        if jsonData["StatusCode"].stringValue == "201" && jsonData["Status"].boolValue == true {
                            DispatchQueue.main.async {
                                self.passDataToReceipt(jsonData: jsonData)
                                self.performSegue(withIdentifier: "solarConfirmationToReceipt", sender: self)
                            }
                        }
                        else {
                            alertViewObj.wrapAlert(title: nil, body: jsonData["Message"].stringValue, img: nil)
                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                            })
                            alertViewObj.showAlert(controller: self)
                        }
                    } else {
                        alertViewObj.wrapAlert(title: nil, body:"Network error and try again".localized, img: #imageLiteral(resourceName: "alert-icon"))
                        alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                        }
                        alertViewObj.showAlert(controller: UIViewController.init())
                    }
                }
            }
        }
        else {
            alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            }
            alertViewObj.showAlert(controller: UIViewController.init())
        }
    }

    func passDataToReceipt(jsonData: JSON) {
        paymentResultDict["categories"] = "Solar"
        paymentResultDict["transaction_Id"] = jsonData["Result"]["CustomerOkPaymentId"].stringValue
        paymentResultDict["transaction_Type"] = "Bill Payment"
        paymentResultDict["solarAccountNumber"] = solarAccountNumberLabelValue.text
        
        let balance : Double = (UserLogin.shared.walletBal as NSString).doubleValue - (NSString(string: dataDict["bill_Amount"]!).doubleValue + NSString(string: dataDict["service_Fees"]!).doubleValue)
        let doubleStrBillAmount = String(format: "%.2f", balance)
        paymentResultDict["balance"] = wrapAmountWithCommaDecimal(key: doubleStrBillAmount)
        UserLogin.shared.walletBal = doubleStrBillAmount

        paymentResultDict["service_Fees"] = serviceFeesLabelValue.text
        paymentResultDict["amount_Solar_Pay"] = billAmountLabelValue.text
        paymentResultDict["date"] = getDate(timeStr: jsonData["Result"]["CreatedDate"].stringValue)
        paymentResultDict["time"] = getTime(timeStr: jsonData["Result"]["CreatedDate"].stringValue)
        paymentResultDict["mobile_Number"] = dataDict["mobile_Number"]
        paymentResultDict["total_amount"] = totalAmountToPayString
    }
    
    func getDate(timeStr: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        //dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let time = dateFormatter.date(from: timeStr)//timeStr
        dateFormatter.dateFormat = "dd-MMM-yyyy"//hh:mm:ss”
        let date = dateFormatter.string(from: time!)
        return date
    }

    func getTime(timeStr: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        //dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let time = dateFormatter.date(from: timeStr)//timeStr
        dateFormatter.dateFormat = "HH:mm:ss"
        let hour = dateFormatter.string(from: time!)
        return hour
    }

}


