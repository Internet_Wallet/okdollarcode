//
//  PayBillTableViewCell.swift
//  Electricity Bill Module
//
//  Created by Rahul Tyagi on 22/07/17.
//  Copyright © 2017 com.Create. All rights reserved.
//

import UIKit

protocol HeightDelegateForCell {
    func heightForCell(tag: IndexPath, btn: UIButton)
}


class PayBillTableViewCell: UITableViewCell {    
    
    @IBOutlet weak var divisionLabelValue: UILabel!{
        didSet{
            divisionLabelValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    @IBOutlet weak var townshipLabelValue: UILabel!{
        didSet{
            townshipLabelValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    @IBOutlet weak var meterNumberLabelValue: UILabel!{
        didSet{
            meterNumberLabelValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    @IBOutlet weak var payForLabelValue: UILabel!{
        didSet{
            payForLabelValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    @IBOutlet weak var contactPhoneNoLabelValue: UILabel!{
        didSet{
            contactPhoneNoLabelValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    @IBOutlet var ledgerNumberLabelValue: UILabel!{
        didSet{
            ledgerNumberLabelValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    @IBOutlet var nameOnTheBillLabelValue: UILabel!{
        didSet{
            nameOnTheBillLabelValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    @IBOutlet var meterReadDateLabelValue: UILabel!{
        didSet{
            meterReadDateLabelValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    @IBOutlet weak var computerCodeValueOutlet: UILabel!{
        didSet{
            computerCodeValueOutlet.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    @IBOutlet var tariffLabelValue: UILabel!{
        didSet{
            tariffLabelValue.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet var heightConstraintAddV: NSLayoutConstraint!
    @IBOutlet var upDownBtn: UIButton!
    
    
    @IBOutlet weak var adjustBtnOutlet: UIButton!{
        didSet{
            adjustBtnOutlet.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var deleteBtnOutlet: UIButton!{
        didSet{
            deleteBtnOutlet.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var payBtnOutlet: UIButton!{
        didSet{
            payBtnOutlet.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    
    @IBOutlet weak var lblDivision: UILabel!{
        didSet{
            lblDivision.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblTownship: UILabel!{
        didSet{
            lblTownship.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblMeterNo: UILabel!{
        didSet{
            lblMeterNo.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblPayFor: UILabel!{
        didSet{
            lblPayFor.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblContactPhone: UILabel!{
        didSet{
            lblContactPhone.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    @IBOutlet weak var lblLedgerNumber: UILabel!{
        didSet{
            lblLedgerNumber.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lblNameOnTheBill: UILabel!{
        didSet{
            lblNameOnTheBill.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    @IBOutlet weak var lblMeterReadDate: UILabel!{
        didSet{
            lblMeterReadDate.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    @IBOutlet weak var lbltariff: UILabel!{
        didSet{
            lbltariff.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    @IBOutlet weak var lblComputerCode: UILabel!{
        didSet{
            lblComputerCode.font = UIFont(name: appFont, size: appFontSizeReport)
        }
    }
    
    
    @IBOutlet var collectionHorizontalConstraints: [NSLayoutConstraint]!
    
    
    
    
    var delegate : HeightDelegateForCell?
    
    var myTagAccess : IndexPath?
    var isHeighted = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        upDownBtn.setImage(#imageLiteral(resourceName: "Yello_Down.png"), for: .selected)
        
        setLng()
        
        updateConst()
        
    }
    
    func updateConst() {
        
        var const = CGFloat(0)
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 480:
                println_debug("iPhone Classic")
                const = 8.00
            case 960:
                println_debug("iPhone 4 or 4S")
                const = 8.00
            case 1136:
                println_debug("iPhone 5 or 5S or 5C")
                const = 8.00
            case 1334:
                println_debug("iPhone 6/6S/7/7S")
                const = 35.00
            case 2208:
                println_debug("iPhone 6+/6S+/7+")
                const = 45.00
            default:
                println_debug("unknown")
                const = 40.00
            }
        }
        
        for item in collectionHorizontalConstraints {
            item.constant = const
        }
    }
    
    func setupBtnImage() {
        if isHeighted {
            upDownBtn.setImage(#imageLiteral(resourceName: "Yello_Down.png"), for: .normal)
            
            
        } else {
            upDownBtn.setImage(#imageLiteral(resourceName: "Yello_UP"), for: .normal)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setLng() -> Void {
        
        self.lblDivision.text = appDel.getlocaLizationLanguage(key: "Division")
        self.lblTownship.text = appDel.getlocaLizationLanguage(key: "Township")
        self.lblMeterNo.text = appDel.getlocaLizationLanguage(key: "Meter Number")
        self.lblPayFor.text = appDel.getlocaLizationLanguage(key: "Pay For")
        self.lblContactPhone.text = appDel.getlocaLizationLanguage(key: "Contact Number")
        
        self.lblLedgerNumber.text = appDel.getlocaLizationLanguage(key: "Ledger Number")
        self.lblNameOnTheBill.text = appDel.getlocaLizationLanguage(key: "Consumer Name (as on Bill)")
        self.lbltariff.text = appDel.getlocaLizationLanguage(key: "Tariff")
        self.lblComputerCode.text = appDel.getlocaLizationLanguage(key: "Computer Code Number")
        self.lblMeterReadDate.text = appDel.getlocaLizationLanguage(key: "Meter Read Date")
        
        self.lblDivision.font = UIFont.init(name: appFont, size: appFontSizeReport)
        
        self.lblTownship.font = UIFont.init(name: appFont, size: appFontSizeReport)
        
        self.lblMeterNo.font = UIFont.init(name: appFont, size: appFontSizeReport)
        
        self.lblPayFor.font = UIFont.init(name: appFont, size: appFontSizeReport)
        
        self.lblContactPhone.font = UIFont.init(name: appFont, size: appFontSizeReport)
        
        self.lblLedgerNumber.font = UIFont.init(name: appFont, size: appFontSizeReport)
        
        self.lblNameOnTheBill.font = UIFont.init(name: appFont, size: appFontSizeReport)
        
        self.lbltariff.font = UIFont.init(name: appFont, size: appFontSizeReport)
        
        self.lblComputerCode.font = UIFont.init(name: appFont, size: appFontSizeReport)
        
        self.lblMeterReadDate.font = UIFont.init(name: appFont, size: appFontSizeReport)
        
        // button.setTitle("Button Title",for: .normal)
        self.deleteBtnOutlet.setTitle(appDel.getlocaLizationLanguage(key: "Delete"),for: .normal)
        self.payBtnOutlet.setTitle(appDel.getlocaLizationLanguage(key: "Pay"),for: .normal)
        self.adjustBtnOutlet.setTitle(appDel.getlocaLizationLanguage(key: "Adjust"),for: .normal)
    }
    
    
    @IBAction func actionUpDwn(_ sender: UIButton) {
        guard self.delegate?.heightForCell(tag: myTagAccess!, btn: upDownBtn) != nil else {
            return
        }
    }
    
}
