//
//  PayBillChildVC_MyMeter.swift
//  OK
//
//  Created by Rahul Tyagi on 9/4/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit

protocol addMoneyDelagate: class {
    func callAddMoneyClass()
}


class PayBillChildVC_MyMeter: OKBaseController ,UITableViewDataSource , UITableViewDelegate, HeightDelegateForCell,popMyViewController, NotifyMeterViewsDelegate  {
    
    var myMeterList = [Dictionary<String, String>]()
    
    var delegate : DismissMyViewController? = nil
    
    var EmptyDataContainerView : UIView? = nil
    
    var PayBillTableCellRef : PayBillTableViewCell? = nil
    
    var meterStrForDeletion : String? = nil
    
    var EBPayBillresultStringToJSON : AnyObject? = nil
    
    var searchResults = [Dictionary<String, String>]()
    
    var NoResultsLabel : UILabel? = nil
    
    var AddBtn : UIButton?
    
    var expandedCells = [IndexPath]()
    
//    var ActivityIndicator : UIActivityIndicatorView?
    
    var parentSearchBar : UISearchBar? = nil
    
    var parentNavBar : UINavigationBar?
    var parentSearchBarButtonOutlet = UIBarButtonItem()
    
    @IBOutlet weak var myMeterTableViewOutlet: UITableView!
    weak var delegateAddMoney: addMoneyDelagate?

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        parentNavBar = UINavigationBar()
        // Do any additional setup after loading the view.
        setupEmptyView()
        parentSearchBar = UISearchBar()
        parentSearchBar?.isHidden = true
        myMeterTableViewOutlet.tableFooterView = UIView()
        self.helpSupportNavigationEnum = .Electricity
        parentSearchBar?.isHidden = true
//        if let statusbar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
//            statusbar.backgroundColor = kYellowColor
//        }
        MeterOptionType = .myMeterOption
        self.searchResults.removeAll()
        fetchDataFromServer()
        myMeterTableViewOutlet.reloadData()
    }
    
    func setupEmptyView()
    {
        EmptyDataContainerView = UIView.init(frame: CGRect(x: 0, y: 10, width: self.view.frame.width, height: 150))
        EmptyDataContainerView?.backgroundColor = .white
        self.view.addSubview(EmptyDataContainerView!)
        EmptyDataContainerView?.isHidden = true
        
        let DivImageview = UIImageView.init(frame: CGRect(x: 10, y: 15, width: 35, height: 35))
        DivImageview.image = #imageLiteral(resourceName: "r_division")
        DivImageview.contentMode = .scaleAspectFit
        EmptyDataContainerView?.addSubview(DivImageview)
        
        let SelectDivisionButton = UIButton.init(frame: CGRect(x: 55 , y: 18, width: self.view.frame.width - 55   , height: 30))
        SelectDivisionButton.setTitle(appDelegate.getlocaLizationLanguage(key: "Select Division"), for: .normal)
        SelectDivisionButton.titleLabel?.font = UIFont.init(name: appFont, size: appButtonSize)
        SelectDivisionButton.contentHorizontalAlignment = .left
        SelectDivisionButton.setTitleColor(UIColor.init(red: 28/255.0, green: 45/255.0, blue: 153/255.0, alpha: 1), for: .normal)
        SelectDivisionButton.addTarget(self, action: #selector(presentDivisionController), for: .touchUpInside)
        
        EmptyDataContainerView?.addSubview(SelectDivisionButton)
        
        let arrowImageView = UIImageView.init(frame: CGRect(x: self.view.frame.width - 40, y: 18, width: 20, height: 20))
        arrowImageView.image = #imageLiteral(resourceName: "YellowRightArrow")
        arrowImageView.contentMode = .scaleAspectFit
        EmptyDataContainerView?.addSubview(arrowImageView)
        
        let grayView = UIView.init(frame: CGRect(x: 0, y: 65, width: self.view.frame.width, height: self.view.frame.height - 70))
        grayView.backgroundColor = .lightGray
        EmptyDataContainerView?.addSubview(grayView)
        
        NoResultsLabel = UILabel.init(frame: CGRect(x: 0, y: self.view.frame.height/2 - 150, width: self.view.frame.width, height: 30))
        NoResultsLabel?.text = appDelegate.getlocaLizationLanguage(key: "No Results Found".localized)
        NoResultsLabel?.textColor = .darkGray
        NoResultsLabel?.textAlignment = .center
        NoResultsLabel?.numberOfLines = 2
        self.view.addSubview(NoResultsLabel!)
        NoResultsLabel?.textAlignment = .center
        NoResultsLabel?.font = UIFont.init(name: appFont, size: 22)
        NoResultsLabel?.isHidden = true
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func popNow()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func presentDivisionController()
    {
        
        let GetBillVCObj = self.storyboard?.instantiateViewController(withIdentifier: "GetBillVCSID") as! GetElectricityBillVC
        GetBillVCObj.parentVC_meter = self
        GetBillVCObj.delegateBillVC = self
        GetBillVCObj.autoPresentDivController = true
        let navController = UINavigationController.init(rootViewController: GetBillVCObj)
        navController.isNavigationBarHidden = true
        GetBillVCObj.checkCount = myMeterList.count
        navController.modalPresentationStyle = .overFullScreen
        navController.definesPresentationContext = true
        
        self.present(navController, animated: false, completion: nil)
        
    }
    
    
    
    override func viewDidLayoutSubviews() {
        //Add Button
        if AddBtn == nil {
            AddBtn = UIButton.init(frame: CGRect(x: self.view.frame.width * 0.8 , y: self.view.frame.height * 0.8 , width: 60.0, height: 60.0))
            AddBtn?.setImage(#imageLiteral(resourceName: "addicon"), for: .normal)
            AddBtn?.addTarget(self, action: #selector (self.AddbtnAction), for: .touchUpInside)
            AddBtn?.backgroundColor = UIColor.clear
//            AddBtn?.layer.cornerRadius =  (AddBtn!.bounds.size.width) * 0.5
//            AddBtn?.clipsToBounds = true
            self.view.addSubview(AddBtn!)
            AddBtn?.isHidden = true
//            AddBtn?.layer.masksToBounds = false
//            AddBtn?.layer.shadowColor = UIColor.black.cgColor
//            AddBtn?.layer.shadowOffset = CGSize(width: 0.2, height: 3.0)
//            AddBtn?.layer.shadowOpacity = 0.25;
            self.view.bringSubviewToFront(AddBtn!)
            
        }
        
    }
    
    @objc func AddbtnAction()
    {
        
        
        let GetBillVCObj = self.storyboard?.instantiateViewController(withIdentifier: "GetBillVCSID") as! GetElectricityBillVC
        GetBillVCObj.parentVC_meter = self
        GetBillVCObj.delegateBillVC = self
        let navController = UINavigationController.init(rootViewController: GetBillVCObj)
        navController.isNavigationBarHidden = true
        GetBillVCObj.checkCount = myMeterList.count
        navController.modalPresentationStyle = .overFullScreen
        navController.definesPresentationContext = true
        
        self.present(navController, animated: true, completion: nil)
        
    }
    
    func refreshSetup() {
        println_debug("Done Refresh Setup")
        MeterOptionType = .myMeterOption
        fetchDataFromServer()
        //myMeterTableViewOutlet.reloadData()
    }
    
    func dismissNow()
    {
        println_debug("Call was in Dismiss method")
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
//        MeterOptionType = .myMeterOption
//
//        fetchDataFromServer()
//        myMeterTableViewOutlet.reloadData()

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        
        //Reset search bar
        parentSearchBar?.text = ""
//        MeterOptionType = .myMeterOption
//        self.searchResults.removeAll()
//        self.myMeterTableViewOutlet.reloadData()
        
        parentNavBar?.topItem?.title = appDelegate.getlocaLizationLanguage(key: "Electricity Bill")
        parentNavBar?.topItem?.rightBarButtonItem?.isEnabled = true
        parentNavBar?.topItem?.rightBarButtonItem?.tintColor = UIColor.white;
        
        //End of reset search bar
        if self.myMeterList.isEmpty {
            self.AddBtn?.isHidden = true
        } else {
            self.AddBtn?.isHidden = false
        }
        if self.myMeterList.count > 1 {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowRightBarButton"), object: nil)
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "HideRightBarButton"), object: nil)
        }
        MeterOptionType = .myMeterOption
        
    }
    
    func fetchDataFromServer()
    {
        println_debug("Subview added")
        
        
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
        }
        
        let customURL = URL.init(string: "https://www.okdollar.co/RestService.svc/GetEbBillHistoryByMobileNumber")
        
        let agentNum = UserModel.shared.mobileNo
        let msid = UserModel.shared.msid
        
        let str = uuid
        
        let parameters : [String : Any] = [ "SearchDate":"07-04-2018",
                                            "SearchType":"0",
                                            "LoginDeviceInfo":
                                                [ "MobileNumber": agentNum,
                                                  "Otp": uuid,
                                                  "Simid": str,
                                                  "Msid": msid,
                                                  "Ostype": 1
            ]
        ]
        println_debug(parameters)
        
        
        // JSON all the things
        let auth = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        
        
        
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: customURL!)
        
        do {
            
            println_debug("Requesting Get Bill API Now")
            
            // Set the request content type to JSON
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            // The magic...set the HTTP request method to POST
            request.httpMethod = "POST"
            
            // Add the JSON serialized login data to the body
            request.httpBody = auth
            
            // Create the task that will send our login request (asynchronously)
            let task = session.dataTask(with: request as URLRequest, completionHandler:
            { (data, response, error) -> Void in
                
                if(data == nil)
                {
                    println_debug("Data is nil")
                    DispatchQueue.main.async {
                        progressViewObj.removeProgressView()
                        
                        alertViewObj.wrapAlert(title: nil, body: "Please Try Again Later.".localized, img: nil)
                        
                        
                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                            
                        })
                        
                        alertViewObj.showAlert(controller: self)
                        
                    }
                    
                }
                else
                {
                    do {
                        DispatchQueue.main.async {
                            progressViewObj.removeProgressView()
                        }
                        
                        println_debug("Call was in do block")
                        let jsonResponse = try? JSONSerialization.jsonObject(with: data!, options:.allowFragments)
                        if let jsonResponse = jsonResponse as? [String: Any] {
                            // if let jsonResponse = jsonResponse as? [String: Any] {
                            println_debug("\n \n \n \(jsonResponse)")
                            
                            
                            if jsonResponse["Code"] as! NSNumber ==  200
                            {
                                
                                
                                let DataStringToJSON : [String : Any] = self.convertStringToJson(ReceivedString: jsonResponse["Data"]! as! String) as! [String : Any]
                                // println_debug(DataStringToJSON)
                                
                                
                                self.EBPayBillresultStringToJSON  = self.convertStringToJson(ReceivedString: DataStringToJSON["result"]! as! String) as AnyObject
                                //println_debug(resultStringToJSON)
                                
                                self.myMeterList.removeAll()
                                
                                
                                for element in self.EBPayBillresultStringToJSON as! Array<AnyObject> {
                                    
                                    
                                    
                                    //println_debug("\n \(element.index).  \(element)")
                                    
                                    
                                    if let numberVal = element["meterType"] as? Int, numberVal == 1 {
                                        
                                        let DivName = element["stateOrDivisionName"]! as! String
                                        let Township = element["township"]! as! String
                                        let MeterNumber = element["meterNumber"]! as! String
                                        let PayFor = element["payFor"]! as! String
                                        let ContactPhoneNumber = element["contactMobileNum"]! as! String
                                        let computerCodeNo = element["custRefNum"]! as! String
                                        let isAdjust = element["isAdjust"]! as! String
                                        let ledgerNumber = element["accNumber"]! as! String
                                        let billerName = element["billerName"]! as! String
                                        
                                        let dateValue = element["meterReadDate"]! as! String
                                        let index = dateValue.index(dateValue.startIndex, offsetBy: 11)
                                        
                                        // Reference
                                        //let newStr = str.substring(to: index) // Swift 3
                                        //let newStr = String(str[..<index]) // Swift 4
                                        
                                        //old
                                        //let shortDateStr = dateValue.substring(to: index)
                                        
                                        // Vinnu Added
                                        let shortDateStr = String(dateValue[..<index])
                                        let meterReadDate = self.formattedDateFromString(dateString: shortDateStr, withFormat: "dd-MM-yyyy")
                                        
                                        let tariff = element["tarrif"]! as! String
                                        
                                        let currentBillCyclePaidStatus = String(describing: element["currentBillCyclePaidStatus"] as! NSNumber)
                                        
                                        let DivisionID = element["divisionId"]! as! String
                                        let TownshipID = element["townshipId"]! as! String
                                        
                                        let myMeterInfo : [String: String] = [
                                            "DivName" : DivName,
                                            
                                            "Township" : Township,
                                            "MeterNumber" : MeterNumber,
                                            "PayFor" : PayFor,
                                            "ContactPhoneNumber" : ContactPhoneNumber,
                                            "isAdjust" : isAdjust,
                                            "ledgerNumber" : ledgerNumber,
                                            "computerCodeNo" : computerCodeNo,
                                            "billerName":billerName,
                                            "meterReadDate":meterReadDate ?? Date().stringValue(dateFormatIs: "dd-MM-yyyy"),
                                            "tariff":tariff,
                                            "currentBillCyclePaidStatus":currentBillCyclePaidStatus,
                                            "divisionId" : DivisionID,
                                            "townshipId" : TownshipID,
                                            ]
                                        
                                        self.myMeterList.append(myMeterInfo)
                                        
                                        println_debug("Meter List not empty")
                                        
                                        
                                    }
                                    
                                }
                                
                                DispatchQueue.main.async {
//                                    self.ActivityIndicator?.stopAnimating()
                                     progressViewObj.removeProgressView()
                                    self.myMeterTableViewOutlet.reloadData()
                                    
                                    if self.myMeterList.isEmpty
                                    {
                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "HideRightBarButton"), object: nil)
                                        self.EmptyDataContainerView?.isHidden = false
                                        self.AddBtn?.isHidden = true
                                        self.myMeterTableViewOutlet.isHidden = true
                                    }
                                    else
                                    {
                                        if self.myMeterList.count > 1 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowRightBarButton"), object: nil)
                                        }
                                        self.EmptyDataContainerView?.isHidden = true
                                        self.AddBtn?.isHidden = false
                                        self.myMeterTableViewOutlet.isHidden = false
                                    }
                                    
                                }
                                
                                
                            }
                            else
                            {
                                
                                DispatchQueue.main.async {
//                                    self.ActivityIndicator?.stopAnimating()
                                    progressViewObj.removeProgressView()
                                }
                                
                                println_debug("Condition failed")
                                
                                //Show error
                                
                                
                                
                            }
                            
                            
                            
                            //}
                        }
                    }
                }
                
            })
            
            // Start the task on a background thread
            task.resume()
            
        }
        
        
        
    }
    
    func convertStringToJson(ReceivedString : String) -> Any
    {
        let CustomStringData = ReceivedString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue), allowLossyConversion:true)
        
        do {
            let jsonData = try? JSONSerialization.jsonObject(with: CustomStringData!, options: .allowFragments ) as Any
            return jsonData!
        }
        
    }
    
    
    func formattedDateFromString(dateString: String, withFormat format: String) -> String?
    {
        let inputFormatter = DateFormatter()
        inputFormatter.calendar = Calendar(identifier: .gregorian)
        inputFormatter.dateFormat = "MMM-dd-yyyy"
        inputFormatter.locale =  NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        let dateStr = dateString.replacingOccurrences(of: "  ", with: " ")
        
        if let date = inputFormatter.date(from: dateStr) {
            
            let outputFormatter = DateFormatter()
            outputFormatter.calendar = Calendar(identifier: .gregorian)
            outputFormatter.dateFormat = format
            outputFormatter.locale =  NSLocale(localeIdentifier: "en_US_POSIX") as Locale
            return outputFormatter.string(from: date)
        }
        return nil
    }
    
    
    @IBAction func DeleteBtnAction(_ sender: Any)
    {
        println_debug("My Meter - Delete Btn")
        
        println_debug((sender as AnyObject).superview!!.superview!.superview!.superview!)
        
        let customCell : PayBillTableViewCell = (sender as AnyObject).superview!!.superview!.superview!.superview! as! PayBillTableViewCell
        println_debug(customCell.meterNumberLabelValue.text!)
        
        let MeterNoToMatch = customCell.meterNumberLabelValue.text!
        
        let index = MeterNoToMatch.index(MeterNoToMatch.startIndex, offsetBy: 2)
        
        // vinnu added
        meterStrForDeletion = String(MeterNoToMatch[index...])
        
        
        DispatchQueue.main.async(){
            
            alertViewObj.wrapAlert(title: "", body: "Do you want to Delete Meter Bill Details?".localized, img: nil)
            
            alertViewObj.addAction(title: "YES".localized, style: .target , action: {
                
                self.deleteAPIRequest(meterString: self.meterStrForDeletion!)
                
            })
            
            alertViewObj.addAction(title: "NO".localized, style: .target , action: {
                
                
            })
            
            
            
            alertViewObj.showAlert(controller: self)
            progressViewObj.removeProgressView()
        }
        
        
        
        
        
    }
    
    @IBAction func PayBtnAction(_ sender: Any)
    {
        //Check Balance
        let balanceAmount = (UserLogin.shared.walletBal as NSString).floatValue
        
        
        
        if balanceAmount < 1
        {
            //Show Alert
            var lowBalanceAlert = ""
            let myAttribute = [NSAttributedString.Key.font: UIFont(name: appFont, size: 17) ?? UIFont.systemFont(ofSize: 17)]
            
            if appDelegate.getSelectedLanguage() == "en"
            {
                lowBalanceAlert = NSAttributedString(string: "Insufficient Balance".localized, attributes: myAttribute).string
                
            }
            else
            {
                lowBalanceAlert = NSAttributedString(string: "Insufficient Balance".localized, attributes: myAttribute).string
                
            }
            
            DispatchQueue.main.async {
                
                alertViewObj.wrapAlert(title:"", body:lowBalanceAlert, img: nil)
                alertViewObj.addAction(title: "Cancel".localized, style: .cancel , action: {
                })
                alertViewObj.addAction(title: "ADD MONEY".localized, style: .target , action: {
                    TBHomeNavigations.main.navigate(.addWithdraw)
                    println_debug("vinnu call ----> add money")
                })
                alertViewObj.showAlert(controller: self)
                progressViewObj.removeProgressView()
            }
            return
        }
        
        println_debug("My Meter - Pay Btn")
        println_debug((sender as AnyObject).superview!!.superview!.superview!.superview!)
        
        let customCell : PayBillTableViewCell = (sender as AnyObject).superview!!.superview!.superview!.superview! as! PayBillTableViewCell
        println_debug(customCell.meterNumberLabelValue.text!)
        
        let ViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "AdjustAndPayVCSID") as! AdjustAndPayVC
        ViewControllerObj.AdjustAndPayActionType = .PayAction
        
        for element in self.EBPayBillresultStringToJSON as! Array<AnyObject> {
            
            let MeterNoToMatch = customCell.meterNumberLabelValue.text!
            
            let index = MeterNoToMatch.index(MeterNoToMatch.startIndex, offsetBy: 2)
            
            
            if element["meterNumber"]! as! String == String(MeterNoToMatch[index...]) //MeterNoToMatch.substring(from: index)
            {
                println_debug(element)
                
                let myAttribute = [NSAttributedString.Key.font: UIFont(name: appFont, size: 17) ?? UIFont.systemFont(ofSize: 17)]
                
                
                if element["okDollarMeterReadDateStatus"]! as! Bool == false
                {
                    var OkDollarMeterReadDateErrorMsg = ""
                    
                    
                    if appDelegate.getSelectedLanguage() == "en"
                    {
                        OkDollarMeterReadDateErrorMsg = NSAttributedString(string: (element["okDollarmeterReadDateStatusMessageEName"]! as! String), attributes: myAttribute).string
                        
                    }
                    else
                    {
                        OkDollarMeterReadDateErrorMsg = NSAttributedString(string: (element["okDollarmeterReadDateStatusMessageUnicode"]! as! String), attributes: myAttribute).string
                        
                    }
                    
                    DispatchQueue.main.async {
                        
                        alertViewObj.wrapAlert(title: "", body: OkDollarMeterReadDateErrorMsg.localized, img: nil)
                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                            
                        })
                        
                        alertViewObj.showAlert(controller: self)
                        progressViewObj.removeProgressView()
                        
                    }
                }
                else
                {
                    if element["meterReadDateStatus"]! as! Bool == true
                    {
                        if element["isPayCurrentMonth"]! as! Bool == true
                        {
                            
                            println_debug("Match Found")
                            if element is Dictionary<String,Any> {
                                println_debug(element)
                                
                                ViewControllerObj.jsonDataArray = element as! Dictionary<String, Any>
                                EBDivisionID = element["divisionId"]! as! String
                                EBTownshipID = element["townshipId"]! as! String
                                ViewControllerObj.delegate = self as? popMyViewController
                                
                                self.navigationController?.pushViewController(ViewControllerObj, animated: true)
                                
                            }
                            
                            break
                            
                        }
                        else
                        {
                            
                            var alreadyPaidMessage = ""
                            
                            if appDelegate.getSelectedLanguage() == "en"
                            {
                                alreadyPaidMessage = NSAttributedString(string: (element["alreadyPaidMessageUnicode"]! as! String), attributes: myAttribute).string
                                
                            }
                            else
                            {
                                alreadyPaidMessage = "You have Already Paid Electricity Bill for the Current Month".localized
                            }
                            
                            DispatchQueue.main.async {
                                
                                alertViewObj.wrapAlert(title: nil, body: alreadyPaidMessage, img: nil)
                                
                                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                    
                                })
                                
                                alertViewObj.showAlert(controller: self)
                                progressViewObj.removeProgressView()
                            }
                        }
                        
                    }
                    else
                    {
                        var meterReadDateStatusMessage = ""
                        
                        if appDelegate.getSelectedLanguage() == "en"
                        {
                            meterReadDateStatusMessage = NSAttributedString(string: (element["meterReadDateStatusMessageEName"]! as! String), attributes: myAttribute).string
                            
                            
                        }
                        else
                        {
                            meterReadDateStatusMessage = NSAttributedString(string: (element["meterReadDateStatusMessageUnicode"]! as! String), attributes: myAttribute).string
                            
                        }
                        DispatchQueue.main.async {
                            alertViewObj.wrapAlert(title: nil, body: meterReadDateStatusMessage, img: nil)
                            
                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                
                            })
                            
                            alertViewObj.showAlert(controller: self)
                            progressViewObj.removeProgressView()
                            
                            
                        }
                        
                    }
                    
                }
            }
        }
        
    }
    
    
    
    @IBAction func AdjustBtnAction(_ sender: Any)
    {
        println_debug("My Meter - Adjust Btn")
        println_debug((sender as AnyObject).superview!!.superview!.superview!.superview!)
        
        let customCell : PayBillTableViewCell = (sender as AnyObject).superview!!.superview!.superview!.superview! as! PayBillTableViewCell
        println_debug(customCell.meterNumberLabelValue.text!)
        
        let ViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "AdjustAndPayVCSID") as! AdjustAndPayVC
        ViewControllerObj.AdjustAndPayActionType = .AdjustAction
        
        
        
        for element in self.EBPayBillresultStringToJSON as! Array<AnyObject> {
            let MeterNoToMatch = customCell.meterNumberLabelValue.text!
            
            let index = MeterNoToMatch.index(MeterNoToMatch.startIndex, offsetBy: 2)
            
            if element["meterNumber"]! as! String == String(MeterNoToMatch[index...]) //MeterNoToMatch.substring(from: index)
            {
                let myAttribute = [NSAttributedString.Key.font: UIFont(name: appFont, size: 17) ?? UIFont.systemFont(ofSize: 17)]
                
                if element["okDollarMeterReadDateStatus"]! as! Bool == false
                {
                    var OkDollarMeterReadDateErrorMsg = ""
                    
                    if appDelegate.getSelectedLanguage() == "en"
                    {
                        OkDollarMeterReadDateErrorMsg = NSAttributedString(string: (element["okDollarmeterReadDateStatusMessageEName"]! as! String), attributes: myAttribute).string
                    }
                    else
                    {
                        OkDollarMeterReadDateErrorMsg = NSAttributedString(string: (element["okDollarmeterReadDateStatusMessageUnicode"]! as! String), attributes: myAttribute).string
                    }
                    
                    DispatchQueue.main.async {
                        alertViewObj.wrapAlert(title: nil, body: OkDollarMeterReadDateErrorMsg, img: nil)
                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                            
                        })
                        
                        alertViewObj.showAlert(controller: self)
                        progressViewObj.removeProgressView()
                        
                    }
                    
                }
                else
                {
                    if element["meterReadDateStatus"]! as! Bool == true
                    {
                        
                        println_debug("Match Found")
                        if element is Dictionary<String,Any> {
                            println_debug(element)
                            EBDivisionID = element["divisionId"]! as! String
                            EBTownshipID = element["townshipId"]! as! String
                            ViewControllerObj.jsonDataArray = element as! Dictionary<String, Any>
                            ViewControllerObj.delegate = self as? popMyViewController 
                            self.navigationController?.pushViewController(ViewControllerObj, animated: true)
                            
                        }
                        break
                    }
                    else
                    {
                        var meterReadDateStatusMessage = ""
                        
                        if appDelegate.getSelectedLanguage() == "en"
                        {
                            meterReadDateStatusMessage = NSAttributedString(string: (element["meterReadDateStatusMessageEName"]! as! String), attributes: myAttribute).string
                            
                        }
                        else
                        {
                            meterReadDateStatusMessage = NSAttributedString(string: (element["meterReadDateStatusMessageUnicode"]! as! String), attributes: myAttribute).string
                            
                        }
                        DispatchQueue.main.async {
                            
                            
                            alertViewObj.wrapAlert(title: nil, body: meterReadDateStatusMessage, img: nil)
                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                
                            })
                            
                            alertViewObj.showAlert(controller: self)
                            progressViewObj.removeProgressView()
                            
                        }
                    }
                }
            }
        }
    }
    
    func deleteAPIRequest(meterString : String)
    {
        
        println_debug("Subview added")
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
            
        }
        
        let customURL = URL.init(string: "http://www.okdollar.co/RestService.svc/InActiveMeterBill")
        
        let agentNum = UserModel.shared.mobileNo
        
        let msid = UserModel.shared.msid
        
        let str = uuid
        
        let parameters : [String : Any] = [ "MeterBillNum": meterString,
                                            "LoginDeviceInfo":
                                                [ "MobileNumber":   agentNum,
                                                  "Otp": "",
                                                  "Simid": str,
                                                  "Msid": msid,
                                                  "Ostype": 1
            ]
        ]
        println_debug(parameters)
        
        
        // JSON all the things
        let auth = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        
        
        
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: customURL!)
        
        do {
            
            println_debug("Requesting Delete API Now")
            
            // Set the request content type to JSON
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            // The magic...set the HTTP request method to POST
            request.httpMethod = "POST"
            
            // Add the JSON serialized login data to the body
            request.httpBody = auth
            
            // Create the task that will send our login request (asynchronously)
            let task = session.dataTask(with: request as URLRequest, completionHandler:
            { (data, response, error) -> Void in
                
                if(data == nil)
                {
                    println_debug("Data is nil")
                    DispatchQueue.main.async {
//                        ActivityIndicator.stopAnimating()
                        progressViewObj.removeProgressView()
                    }
                    
                }
                else
                {
                    do {
                        
                        println_debug("Call was in do block")
                        let jsonResponse = try? JSONSerialization.jsonObject(with: data!, options:.allowFragments)
                        if let jsonResponse = jsonResponse as? [String: Any] {
                            // if let jsonResponse = jsonResponse as? [String: Any] {
                            println_debug("\n \n \n \(jsonResponse)")
                            
                            DispatchQueue.main.async {
                                
                            }
                            
                            if jsonResponse["Code"] as! NSNumber ==  200
                            {
                                
                                DispatchQueue.main.async {
                                    
                                    var i = 0
                                    
                                    if !self.searchResults.isEmpty
                                    {
                                        while i < self.searchResults.count
                                        {
                                            if (self.searchResults[i] as Dictionary)["MeterNumber"] == self.meterStrForDeletion
                                            {
                                                self.searchResults.remove(at: i)
                                                if self.searchResults.count == 0
                                                {
                                                    self.NoResultsLabel?.isHidden = false
                                                    self.myMeterTableViewOutlet.isHidden = true
                                                }
                                                
                                            }
                                            i = i + 1
                                        }
                                        self.myMeterTableViewOutlet.reloadData()
                                        
                                        
                                    }
                                    
                                    var  j = 0
                                    while j < self.myMeterList.count
                                    {
                                        if (self.self.myMeterList[j] as Dictionary)["MeterNumber"] == self.meterStrForDeletion
                                        {
                                            self.myMeterList.remove(at: j)
                                            if self.myMeterList.count == 0
                                            {
                                                self.EmptyDataContainerView?.isHidden = false
                                                self.AddBtn?.isHidden = true
                                                self.myMeterTableViewOutlet.isHidden = true
                                            }
                                        }
                                        j = j + 1
                                    }
                                    self.myMeterTableViewOutlet.reloadData()
                                    
//                                    ActivityIndicator.stopAnimating()
                                    progressViewObj.removeProgressView()
                                    
                                    alertViewObj.wrapAlert(title: nil, body: appDelegate.getlocaLizationLanguage(key: "Electricity Bill Details Deleted Successfully."), img: nil)
                                    
                                    
                                    alertViewObj.addAction(title: "Done".localized, style: .target , action: {
                                        
                                    })
                                    
                                    alertViewObj.showAlert(controller: self)
                                    progressViewObj.removeProgressView()
                                    
                                }
                                
                            }
                            else
                            {
                                
                                DispatchQueue.main.async {
//                                    ActivityIndicator.stopAnimating()
                                    progressViewObj.removeProgressView()
                                }
                                
                                println_debug("Condition failed")
                                
                            }
                        }
                    }
                }
                
            })
            
            // Start the task on a background thread
            task.resume()
            
        }
    }
    
    func FilterContentForSearchText(searchText : String )
    {
        println_debug(searchText)
        if searchText.isEmpty
        {
            myMeterTableViewOutlet.isHidden = false
            NoResultsLabel?.isHidden = true
            
            searchResults.removeAll()
            myMeterTableViewOutlet.reloadData()
            
            AddBtn?.isHidden = false
        }
        else
        {
            AddBtn?.isHidden = true
            
            myMeterTableViewOutlet.isHidden = false
            NoResultsLabel?.isHidden = true
            
            searchResults .removeAll()
            
            var i = 0
            
            while i < myMeterList.count
            {
                if (((myMeterList[i] as Dictionary)["MeterNumber"])?.localizedCaseInsensitiveContains(searchText))! || (((myMeterList[i] as Dictionary)["computerCodeNo"])?.localizedCaseInsensitiveContains(searchText))! || (((myMeterList[i] as Dictionary)["ledgerNumber"])?.localizedCaseInsensitiveContains(searchText))!
                {
                    searchResults.append(myMeterList[i])
                    println_debug(searchResults)
                    myMeterTableViewOutlet.reloadData()
                }
                i = i + 1
            }
        }
        
        if searchResults.isEmpty && !searchText.isEmpty
        {
            println_debug("Search Results empty & search text also empty")
            myMeterTableViewOutlet.isHidden = true
            NoResultsLabel?.isHidden = false
        }
    }
    
    //MARK:- TableView Methods
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let customCell : PayBillTableViewCell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! PayBillTableViewCell
        
        
        if searchResults.isEmpty
        {
            if !myMeterList.isEmpty
            {
                
                customCell.divisionLabelValue.text = ": \(String(describing: (myMeterList[indexPath.row] as Dictionary)["DivName"]!))"
                
                customCell.townshipLabelValue.text = ": \((myMeterList[indexPath.row] as Dictionary)["Township"]!)"
                
                customCell.meterNumberLabelValue.text = ": \(String(describing: (myMeterList[indexPath.row] as Dictionary)["MeterNumber"]!))"
                
                customCell.payForLabelValue.text =  ": \(String(describing: (myMeterList[indexPath.row] as Dictionary)["PayFor"]!))"
                
                customCell.contactPhoneNoLabelValue.text = ": \(String(describing: (myMeterList[indexPath.row] as Dictionary)["ContactPhoneNumber"]!))"
                
                if (myMeterList[indexPath.row] as Dictionary)["isAdjust"]! as String == "True"
                {
                    customCell.adjustBtnOutlet.isHidden = false
                }
                else
                {
                    customCell.adjustBtnOutlet.isHidden = true
                }
                
                customCell.ledgerNumberLabelValue.text = ": \(String(describing: (myMeterList[indexPath.row] as [String:String])["ledgerNumber"]!))"
                
                customCell.nameOnTheBillLabelValue.text = ": \(String(describing: (myMeterList[indexPath.row] as [String:String])["billerName"]!))"
                
                customCell.meterReadDateLabelValue.text = ": \(String(describing: (myMeterList[indexPath.row] as [String:String])["meterReadDate"]!))"
                
                customCell.tariffLabelValue.text = ": \(String(describing: (myMeterList[indexPath.row] as [String:String])["tariff"]!))"
                
                customCell.computerCodeValueOutlet.text = ": \(String(describing: (myMeterList[indexPath.row] as [String:String])["computerCodeNo"]!))"
                
//                if (myMeterList[indexPath.row] )["currentBillCyclePaidStatus"]!  == "300"
//                {
                    customCell.upDownBtn.isHidden = false
//                }
//                else
//                {
//                    customCell.upDownBtn.isHidden = true
//                }
            }
            
        }
        else //Search is not Empty
        {
            customCell.divisionLabelValue.text = ": \(String(describing: (searchResults[indexPath.row] as Dictionary)["DivName"]!))"
            
            customCell.townshipLabelValue.text = ": \(String(describing: (searchResults[indexPath.row] as Dictionary)["Township"]!))"
            
            customCell.meterNumberLabelValue.text = ": \(String(describing: (searchResults[indexPath.row] as Dictionary)["MeterNumber"]!))"
            
            customCell.payForLabelValue.text = ": \(String(describing: (searchResults[indexPath.row] as Dictionary)["PayFor"]!))"
            
            customCell.contactPhoneNoLabelValue.text = ": \(String(describing: (searchResults[indexPath.row] as Dictionary)["ContactPhoneNumber"]!))"
            
            
            if (searchResults[indexPath.row] as Dictionary)["isAdjust"]! as String == "True"
            {
                customCell.adjustBtnOutlet.isHidden = false
            }
            else
            {
                customCell.adjustBtnOutlet.isHidden = true
            }
            
            customCell.ledgerNumberLabelValue.text = ": \(String(describing: (searchResults[indexPath.row] as [String:String])["ledgerNumber"]!))"
            
            customCell.nameOnTheBillLabelValue.text = ": \(String(describing: (searchResults[indexPath.row] as [String:String])["billerName"]!))"
            
            customCell.meterReadDateLabelValue.text = ": \(String(describing: (searchResults[indexPath.row] as [String:String])["meterReadDate"]!))"
            
            customCell.tariffLabelValue.text = ": \(String(describing: (searchResults[indexPath.row] as [String:String])["tariff"]!))"
            
            customCell.computerCodeValueOutlet.text = ": \(String(describing: (searchResults[indexPath.row] as [String:String])["computerCodeNo"]!))"
            
            
//            if (searchResults[indexPath.row] )["currentBillCyclePaidStatus"]!  == "300"
//            {
                customCell.upDownBtn.isHidden = false
//            }
//            else
//            {
//                customCell.upDownBtn.isHidden = true
//            }
            
        }
        PayBillTableCellRef = customCell
        
        customCell.myTagAccess = indexPath
        customCell.delegate = self
        
        
        if expandedCells.contains(indexPath) {
            customCell.upDownBtn.isSelected = true
        } else {
            customCell.upDownBtn.isSelected = false
        }
        return customCell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if searchResults.isEmpty
        {
            
            if !myMeterList.isEmpty
            {
                if myMeterList.count == 1
                {
                    parentNavBar?.topItem?.rightBarButtonItem?.isEnabled = false
                    parentNavBar?.topItem?.rightBarButtonItem?.tintColor = UIColor.clear;
                }
                else
                {
                    if parentSearchBar?.isHidden == true
                    {
                        parentNavBar?.topItem?.rightBarButtonItem?.isEnabled = true
                        parentNavBar?.topItem?.rightBarButtonItem?.tintColor = UIColor.white;
                        
                    }
                }
                return myMeterList.count
            }
            else
            {
                parentNavBar?.topItem?.rightBarButtonItem?.isEnabled = false
                parentNavBar?.topItem?.rightBarButtonItem?.tintColor = UIColor.clear;
                return 0
            }
        }
        else
        {
            return searchResults.count;
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if expandedCells.contains(indexPath) {
            return 335.00
        } else {
            return 196.00
        }
    }
    
    func heightForCell(tag: IndexPath, btn: UIButton) {
        
        if expandedCells.contains(tag) {
            expandedCells = expandedCells.filter({ $0 != tag})
        }
        else {
            expandedCells.append(tag)
        }
        myMeterTableViewOutlet.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        parentSearchBar?.endEditing(true)
        self.view.endEditing(true)
        NoResultsLabel?.isHidden = true
//        ActivityIndicator?.removeFromSuperview()
        super.viewWillDisappear(animated)
    }
    
    func reloadSubmitData() {
        DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 1.0) {
            self.refreshSetup()
        }
    }
}


