//
//  ElectricityShareViewController.swift
//  OK
//  It is an extension of ReceiptVC
//  Created by Kethan on 5/31/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

extension ReceiptVC {
    func generateViewForBill(dictionay : Dictionary<String, Any>, qrImage : UIImage, fileName: String) {
        let pageWidth: CGFloat = 842
        let pageHeight: CGFloat = 1161
        let marginSpace: CGFloat = 25
        
        func getHeaderLabel(yPosition: CGFloat = 0) -> UILabel {
            let headerLabel = UILabel(frame: CGRect(x: 25, y: yPosition, width: pageWidth - 50, height: 30))
            headerLabel.numberOfLines = 0
            headerLabel.textAlignment = .center
            headerLabel.textColor = UIColor(red: 71.0/255.0, green: 53.0/255.0, blue: 188.0/255.0, alpha: 1.0)
            if let myFont = UIFont(name: appFont, size: 14)  {
                headerLabel.font = myFont
            }
            headerLabel.text = "Electricity Meter Bill".localized
            return headerLabel
        }
        
        func getTitleFrame(yPosition: CGFloat = 0) -> CGRect {
            let titleTrailingSpace: CGFloat = 10
            let titleWidth: CGFloat = (pageWidth / 2) - marginSpace - titleTrailingSpace
            let titleHeight: CGFloat = 25
            return CGRect(x: marginSpace, y: yPosition, width: titleWidth, height: titleHeight)
        }
        
        func getValueFrame(yPosition: CGFloat = 0) -> CGRect {
            let valueLeadingSpace: CGFloat = (pageWidth / 2) + 10
            let valueWidth: CGFloat = (pageWidth / 2) - 10 - marginSpace
            let valueHeight: CGFloat = 25
            return CGRect(x: valueLeadingSpace, y: yPosition, width: valueWidth, height: valueHeight)
        }
        
        func getTitleLabel(yPosition: CGFloat = 0) -> UILabel {
            let titleLabel = UILabel(frame: getTitleFrame(yPosition: yPosition))
            titleLabel.numberOfLines = 0
            titleLabel.textAlignment = .left
            titleLabel.textColor = UIColor.black
            if let myFont = UIFont(name: appFont, size: 14)  {
                titleLabel.font = myFont
            }
            return titleLabel
        }
        
        func getVerticalSeparator(yPosition: CGFloat = 0) -> UILabel {
            let colonLabel = UILabel(frame: CGRect(x: (pageWidth / 2) - 4, y: yPosition, width: 8, height: 30))
            colonLabel.textAlignment = .center
            colonLabel.textColor = UIColor.black
            if let myFont = UIFont(name: appFont, size: 14)  {
                colonLabel.font = myFont
            }
            colonLabel.text = ":"
            return colonLabel
        }
        
        func getValueLabel(yPosition: CGFloat = 0) -> UILabel {
            let valueLabel = UILabel(frame: getValueFrame(yPosition: yPosition))
            valueLabel.numberOfLines = 0
            valueLabel.textAlignment = .left
            valueLabel.textColor = UIColor.black
            if let myFont = UIFont(name: appFont, size: 14)  {
                valueLabel.font = myFont
            }
            return valueLabel
        }
        
        func getFinalBillView(yPosition: CGFloat = 0, billAmount: String) -> UIView {
            var yPosInBill: CGFloat = 0
            let finalBillViewWidth = pageWidth - (2 * marginSpace)
            let finalBillView = UIView(frame: CGRect(x: marginSpace, y: yPosition, width: finalBillViewWidth, height: 100))
            let meterBillAmountTitle = UILabel(frame: CGRect(x: 5, y: 2, width: (finalBillViewWidth / 2) - 10, height: 25))
            meterBillAmountTitle.textAlignment = .left
            meterBillAmountTitle.textColor = UIColor.black
            if let myFont = UIFont(name: appFont, size: 14)  {
                meterBillAmountTitle.font = myFont
            }
            meterBillAmountTitle.text = "Meter Bill Amount".localized
            finalBillView.addSubview(meterBillAmountTitle)
            let meterBillAmountValue = UILabel(frame: CGRect(x: (finalBillViewWidth / 2) + 5, y: 2, width: (finalBillViewWidth / 2) - 10, height: 25))
            meterBillAmountValue.textColor = UIColor.black
            meterBillAmountValue.textAlignment = .right
            if let myFont = UIFont(name: appFont, size: 14)  {
                meterBillAmountValue.font = myFont
            }
            let doubleAmount: Double = Double(billAmount) ?? 0.0
            meterBillAmountValue.text = "\(doubleAmount)" + " " + "MMK".localized
            finalBillView.addSubview(meterBillAmountValue)
            var totalAmount: Double = 0
            if let billAmountInDouble = Double(billAmount) {
                totalAmount += billAmountInDouble
            }
            yPosInBill += meterBillAmountValue.frame.maxY + 2
            
            let serviceFeesTitle = UILabel(frame: CGRect(x: 5, y: yPosInBill, width: (finalBillViewWidth / 2) - 10, height: 25))
            serviceFeesTitle.textAlignment = .left
            serviceFeesTitle.textColor = UIColor.black
            if let myFont = UIFont(name: appFont, size: 14)  {
                serviceFeesTitle.font = myFont
            }
            serviceFeesTitle.text = "Service Fees".localized
            finalBillView.addSubview(serviceFeesTitle)
            let serviceFeesValue = UILabel(frame: CGRect(x: (finalBillViewWidth / 2) + 5, y: yPosInBill, width: (finalBillViewWidth / 2) - 10, height: 25))
            serviceFeesValue.textColor = UIColor.black
            serviceFeesValue.textAlignment = .right
            if let myFont = UIFont(name: appFont, size: 14)  {
                serviceFeesValue.font = myFont
            }
            var serviceFees: Double = 0
            if let sFees = dictionay["ServiceFee"] as? String, let sFeesInDouble = Double(sFees) {
                serviceFees = sFeesInDouble
            }
            serviceFeesValue.text = "\(serviceFees)" + " " + "MMK".localized
            finalBillView.addSubview(serviceFeesValue)
            totalAmount += serviceFees
            yPosInBill = serviceFeesValue.frame.maxY + 2
            let separatorView = UILabel(frame: CGRect(x:0, y: yPosInBill, width: finalBillViewWidth, height: 1))
            separatorView.backgroundColor = UIColor.black
            yPosInBill += 2
            finalBillView.addSubview(separatorView)
            
            let finalBillTitle = UILabel(frame: CGRect(x: 5, y: yPosInBill, width: (finalBillViewWidth / 2) - 10, height: 25))
            finalBillTitle.textAlignment = .left
            finalBillTitle.textColor = UIColor.black
            if let myFont = UIFont(name: appFont, size: 14)  {
                finalBillTitle.font = myFont
            }
            finalBillTitle.text = "Total Meter Bill Paid Amount".localized
            finalBillView.addSubview(finalBillTitle)
            let finalBillValue = UILabel(frame: CGRect(x: (finalBillViewWidth / 2) + 5, y: yPosInBill, width: (finalBillViewWidth / 2) - 10, height: 25))
            finalBillValue.textColor = UIColor.black
            finalBillValue.textAlignment = .right
            if let myFont = UIFont(name: appFont, size: 14)  {
                finalBillValue.font = myFont
            }
            finalBillValue.text = "\(totalAmount)" + " " + "MMK".localized
            finalBillView.addSubview(finalBillValue)
            yPosInBill += finalBillValue.frame.maxY + 2
            
            finalBillView.layer.borderWidth = 1
            finalBillView.layer.borderColor = UIColor.black.cgColor
            return finalBillView
        }
        
        let pdfView = UIView(frame: CGRect(x: 0, y: 0, width: pageWidth, height: pageHeight))
        var yPosition: CGFloat = 0
        //MARK: Logo
        let logoImageView = UIImageView(frame: CGRect(x: (pageWidth / 2) - 30, y: 20, width: 60, height: 60))
        logoImageView.image = #imageLiteral(resourceName: "appIcon_Ok")
        pdfView.addSubview(logoImageView)
        yPosition = logoImageView.frame.maxY + 30
        //MARK: Header Label
        let headerLabel = getHeaderLabel(yPosition: yPosition)
        pdfView.addSubview(headerLabel)
        yPosition = headerLabel.frame.maxY + 30
        if let senderName = dictionay["senderaccountname"] as? String {
            //Account Name
            let accNameTitle = getTitleLabel(yPosition: yPosition)
            accNameTitle.text = "Sender Account Name".localized
            let colonLabel = getVerticalSeparator(yPosition: yPosition)
            let accNameValue = getValueLabel(yPosition: yPosition)
            accNameValue.text = senderName
            pdfView.addSubview(accNameTitle)
            pdfView.addSubview(colonLabel)
            pdfView.addSubview(accNameValue)
            yPosition = max(accNameTitle.frame.maxY, accNameValue.frame.maxY) + 12
        }
        if let senderNumber = dictionay["senderaccountnum"] as? String {
            //Account Number
            let accNumberTitle = getTitleLabel(yPosition: yPosition)
            accNumberTitle.text = "Sender Account Number".localized
            let colonLabel = getVerticalSeparator(yPosition: yPosition)
            let accNumberValue = getValueLabel(yPosition: yPosition)
            accNumberValue.text = senderNumber
            pdfView.addSubview(accNumberTitle)
            pdfView.addSubview(colonLabel)
            pdfView.addSubview(accNumberValue)
            yPosition = max(accNumberTitle.frame.maxY, accNumberValue.frame.maxY) + 12
        }
        
        if let nameOnBill = dictionay["NameOnTheBill"] as? String {
            //Name on Bill
            let nameOnBillTitle = getTitleLabel(yPosition: yPosition)
            nameOnBillTitle.text = "Consumer Name (as on Bill)".localized
            let colonLabel = getVerticalSeparator(yPosition: yPosition)
            let nameOnBillValue = getValueLabel(yPosition: yPosition)
            nameOnBillValue.text = nameOnBill
            pdfView.addSubview(nameOnBillTitle)
            pdfView.addSubview(colonLabel)
            pdfView.addSubview(nameOnBillValue)
            yPosition = max(nameOnBillTitle.frame.maxY, nameOnBillValue.frame.maxY) + 12
        }
        
        if let meterBillAddress = dictionay["MeterBillAddress"] as? String {
            //Meter Bil lAddress
            let meterBillAddressTitle = getTitleLabel(yPosition: yPosition)
            meterBillAddressTitle.text = "Meter Bill Address".localized
            let colonLabel = getVerticalSeparator(yPosition: yPosition)
            let meterBillAddressValue = getValueLabel(yPosition: yPosition)
            meterBillAddressValue.text = meterBillAddress
            pdfView.addSubview(meterBillAddressTitle)
            pdfView.addSubview(colonLabel)
            pdfView.addSubview(meterBillAddressValue)
            yPosition = max(meterBillAddressTitle.frame.maxY, meterBillAddressValue.frame.maxY) + 12
        }
        if let meterNumber = dictionay["meterNumber"] as? String {
            //Meter Bil lAddress
            let meterNumberTitle = getTitleLabel(yPosition: yPosition)
            meterNumberTitle.text = "Meter Number".localized
            let colonLabel = getVerticalSeparator(yPosition: yPosition)
            let meterNumberValue = getValueLabel(yPosition: yPosition)
            meterNumberValue.text = meterNumber
            pdfView.addSubview(meterNumberTitle)
            pdfView.addSubview(colonLabel)
            pdfView.addSubview(meterNumberValue)
            yPosition = max(meterNumberTitle.frame.maxY, meterNumberValue.frame.maxY) + 12
        }
        if let billDate = dictionay["billMonth"] as? String {
            //BillMonth
            if let dateBill = billDate.dateValue(dateFormatIs: "MM/dd/yyyy") {
                let billMonthTitle = getTitleLabel(yPosition: yPosition)
                billMonthTitle.text = "Bill on the Month".localized
                let colonLabel = getVerticalSeparator(yPosition: yPosition)
                let billMonthValue = getValueLabel(yPosition: yPosition)
                billMonthValue.text = dateBill.stringValue(dateFormatIs: "MMM yyyy")
                pdfView.addSubview(billMonthTitle)
                pdfView.addSubview(colonLabel)
                pdfView.addSubview(billMonthValue)
                yPosition = max(billMonthTitle.frame.maxY, billMonthValue.frame.maxY) + 12
            }
        }
        if let transID = dictionay["tansactionid"] as? String {
            //Transaction ID
            let transactionIDTitle = getTitleLabel(yPosition: yPosition)
            transactionIDTitle.text = "Transaction ID".localized
            let colonLabel = getVerticalSeparator(yPosition: yPosition)
            let transactionIDValue = getValueLabel(yPosition: yPosition)
            transactionIDValue.text = transID
            pdfView.addSubview(transactionIDTitle)
            pdfView.addSubview(colonLabel)
            pdfView.addSubview(transactionIDValue)
            yPosition = max(transactionIDTitle.frame.maxY, transactionIDValue.frame.maxY) + 12
        }
        if let transType = dictionay["type"] as? String {
            //Transaction Type
            let transTypeTitle = getTitleLabel(yPosition: yPosition)
            transTypeTitle.text = "Transaction Type".localized
            let colonLabel = getVerticalSeparator(yPosition: yPosition)
            let transTypeValue = getValueLabel(yPosition: yPosition)
            transTypeValue.text = transType
            pdfView.addSubview(transTypeTitle)
            pdfView.addSubview(colonLabel)
            pdfView.addSubview(transTypeValue)
            yPosition = max(transTypeTitle.frame.maxY, transTypeValue.frame.maxY) + 12
        }
        if let payFor = dictionay["PayFor"] as? String {
            //Pay For
            let payForTitle = getTitleLabel(yPosition: yPosition)
            payForTitle.text = "Pay For".localized
            let colonLabel = getVerticalSeparator(yPosition: yPosition)
            let payForValue = getValueLabel(yPosition: yPosition)
            payForValue.text = payFor
            pdfView.addSubview(payForTitle)
            pdfView.addSubview(colonLabel)
            pdfView.addSubview(payForValue)
            yPosition = max(payForTitle.frame.maxY, payForValue.frame.maxY) + 12
        }
        if let dateTimeStr = dictionay["datetime"] as? String {
            //Date Time
            if let dateTime = dateTimeStr.dateValue(dateFormatIs: "dd-MMM-yyyy HH:mm:ss") {
                let dateTimeTitle = getTitleLabel(yPosition: yPosition)
                dateTimeTitle.text = "Date & Time".localized
                let colonLabel = getVerticalSeparator(yPosition: yPosition)
                let dateTimeValue = getValueLabel(yPosition: yPosition)
                dateTimeValue.text = dateTime.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss")
                pdfView.addSubview(dateTimeTitle)
                pdfView.addSubview(colonLabel)
                pdfView.addSubview(dateTimeValue)
                yPosition = max(dateTimeTitle.frame.maxY, dateTimeValue.frame.maxY) + 12
            }
        }
        if let paidAmount = dictionay["paidamount"] as? String {
            //Total Amount
            let totalAmountView = getFinalBillView(yPosition: yPosition, billAmount: paidAmount)
            pdfView.addSubview(totalAmountView)
            yPosition = totalAmountView.frame.maxY + 15
        }
        let qrImageView = UIImageView(frame: CGRect(x: (pageWidth / 2) - 50, y: yPosition, width: 100, height: 100))
        qrImageView.image = qrImage
        pdfView.addSubview(qrImageView)
        yPosition = logoImageView.frame.maxY + 30
        
        guard let pdfUrl = CodeSnippets.generatePDF(viewForPDF: pdfView, pdfFile: fileName) else {
            println_debug("Error - pdf not generated")
            return
        }
        let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
}
