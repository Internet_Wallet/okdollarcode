//
//  AdjustAndPayVCTableViewCell.swift
//  OK
//
//  Created by Rahul Tyagi on 8/17/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit

class AdjustAndPayVCTableViewCell: UITableViewCell {
    @IBOutlet weak var descriptionLabelOutlet: UILabel!{
        didSet{
            descriptionLabelOutlet.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var imageLabelOutlet: UIImageView!
    
    @IBOutlet weak var textFieldOutlet: customTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
