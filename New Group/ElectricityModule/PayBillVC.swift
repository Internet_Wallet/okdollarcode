//
//  PayBillVC.swift
//  Electricity Bill Module
//
//  Created by Rahul Tyagi on 22/07/17.
//  Copyright © 2017 com.Create. All rights reserved.
//

import UIKit


enum MeterOptions {
    case myMeterOption, otherMeterOption
}

var MeterOptionType : MeterOptions = .myMeterOption

let appDel = UIApplication.shared.delegate as! AppDelegate


class PayBillVC: OKBaseController, SMSwipeableTabViewControllerDelegate, UISearchBarDelegate , UIScrollViewDelegate, addMoneyDelagate   {
    
    var PayBillChildVCMyMeterObj : PayBillChildVC_MyMeter?
    var PayBillChildVCOtherMeterObj : PayBillChildVC_OtherMeter?
    
    let titleBarDataSource = ["My Meter", "Other Meter"]
    
    let swipeableView = SMSwipeableTabViewController()

    
    @IBOutlet weak var searchBarButtonOutlet: UIBarButtonItem!
    //@IBOutlet weak var navigationBarOutlet: UINavigationBar!
     @IBOutlet var searchBarOutlet: UISearchBar!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.helpSupportNavigationEnum = .Electricity
        self.loadNavigationBarSettings()
        self.leftBackButton()
        self.title = "Electricity Bill".localized
        // Do any additional setup after loading the view.
        searchBarOutlet.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40)
        searchBarOutlet.showsCancelButton = true
        searchBarOutlet.delegate = self
        searchBarOutlet.tintColor = UIColor.white
        searchBarOutlet.placeholder = "Search".localized
        if let searchTextField = searchBarOutlet.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
                //searchTextField.textColor = UIColor.black
            }
        }
        let view = self.searchBarOutlet.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                subView.tintColor = ConstantsColor.navigationHeaderTransaction
            }
        }
        navigationItem.titleView = nil

        
        //self.navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: backButton), searchBarButton]
        PayBillChildVCMyMeterObj = self.storyboard?.instantiateViewController(withIdentifier: "PayBillChildVC_MyMeterSID") as? PayBillChildVC_MyMeter
        PayBillChildVCMyMeterObj?.parentSearchBar = searchBarOutlet
        //PayBillChildVCMyMeterObj?.parentNavBar = self.navigationController?.navigationBar
        PayBillChildVCMyMeterObj?.parentSearchBarButtonOutlet = searchBarButtonOutlet
        
        PayBillChildVCOtherMeterObj = self.storyboard?.instantiateViewController(withIdentifier: "PayBillChildVC_OtherMeterSID") as? PayBillChildVC_OtherMeter
        PayBillChildVCOtherMeterObj?.parentSearchBar = searchBarOutlet
        //PayBillChildVCOtherMeterObj?.parentNavBar = self.navigationController?.navigationBar
        PayBillChildVCOtherMeterObj?.parentSearchBarButtonOutlet = searchBarButtonOutlet
        
        //Add the title bar elements as an Array of String
        swipeableView.titleBarDataSource = titleBarDataSource
        //Assign your viewcontroller as delegate to load the Viewcontroller
        swipeableView.delegate = self
        
        //Set the View Frame (64.0 is 44.0(NavigationBar Height) + 20.0(StatusBar Height))
        swipeableView.viewFrame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: self.view.frame.size.height - 64)

        swipeableView.segmentBarAttributes = [SMBackgroundColorAttribute : UIColor.lightText]
        
        swipeableView.selectionBarAttributes = [
            SMBackgroundColorAttribute : UIColor.init(red: 0/255.0, green: 0/255.0, blue: 0, alpha: 1)]
        
        swipeableView.buttonWidth = self.view.frame.width/2 //- 16
        swipeableView.selectionBarHeight = 2
        swipeableView.segementBarHeight = 50 //Default is 44.0
        
        swipeableView.buttonAttributes = [
            SMBackgroundColorAttribute : UIColor.clear,
            
            SMFontAttribute : UIFont.systemFont(ofSize: 14),
            SMForegroundColorAttribute : UIColor.orange
        ]
        
        //Then add the view controller on the current view.
        self.addChild(swipeableView)
        self.view.addSubview(swipeableView.view)
        swipeableView.didMove(toParent: self)
        
        NotificationCenter.default.addObserver(self, selector: #selector(PayBillVC.showRightBarButton), name: NSNotification.Name(rawValue: "ShowRightBarButton"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PayBillVC.hideRightBarButton), name: NSNotification.Name(rawValue: "HideRightBarButton"), object: nil)
    }
    
    func leftBackButton() {
        let backBtn = UIBarButtonItem(image: UIImage(named: "tabBarBack"), style: .plain, target: self, action: #selector(backButtonAction))
        self.navigationItem.leftBarButtonItem  = backBtn
    }
    
    @objc func backButtonAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func showRightBarButton() {
        if let button = self.navigationItem.rightBarButtonItem {
            button.isEnabled = true
            button.tintColor = UIColor.white
        }
    }
    
    @objc func hideRightBarButton() {
        if let button = self.navigationItem.rightBarButtonItem {
            button.isEnabled = false
            button.tintColor = UIColor.clear
        }
    }
    
    func callAddMoneyClass() {
        println_debug("kskskskks")
        TBHomeNavigations.main.navigate(.addWithdraw)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        println_debug("Scrolled")
        
        //scrollView.isScrollEnabled = false
    }
    
    func didLoadViewControllerAtIndex(_ index: Int) -> UIViewController {
        //We can implement switch case with the index Parameter and load new controller at every new index. Or we can load the same list view with different datasource.
        println_debug("Did Load VC Called")
        
        switch index
        {
        case 0:
            
            return PayBillChildVCMyMeterObj!
            
        case 1:
            return PayBillChildVCOtherMeterObj!
            
        default:
            
            println_debug("default called")
            return PayBillChildVCMyMeterObj!
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.view.endEditing(true)
        
        self.searchBarOutlet.endEditing(true)
        
        self.navigationController?.navigationBar.isTranslucent = false
        UINavigationBar.appearance().barTintColor = UIColor.init(red: 246/255.0, green: 198/255.0, blue: 0, alpha: 1)
        
        customizeSearchBar()
        
        setLng()
    }
    
    func customizeSearchBar()
    {
        searchBarOutlet.isHidden = true
        DispatchQueue.main.async {
            self.navigationItem.titleView = nil
        }
        self.title = "Electricity Bill".localized
        self.searchBarOutlet.returnKeyType = .done

        
        let TapGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.HandleTapGesture(_:)))
        
        TapGesture.cancelsTouchesInView = false
        
        self.view.addGestureRecognizer(TapGesture)
        
    }
    
    @objc func HandleTapGesture(_:UIGestureRecognizer)
    {
        self.view.endEditing(true)
        
        if searchBarOutlet.text == ""
        {
            searchBarOutlet.isHidden = true
            DispatchQueue.main.async {
                self.navigationItem.titleView = nil
            }
            self.title = "Electricity Bill".localized
            self.title = appDel.getlocaLizationLanguage(key: "Electricity Bill")
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white;
        }
    }
    
    @IBAction func backBtnAction(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK: - Search methods
    
    @IBAction func searchBtnAction(_ sender: Any)
    {
        println_debug("Search Btn was clicked")
        
        self.title = ""
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.clear;
        
        searchBarOutlet.isHidden = false
        DispatchQueue.main.async {
            self.navigationItem.titleView = self.searchBarOutlet
        }
        searchBarOutlet.becomeFirstResponder()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        println_debug("call in search bar text did began editing")
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        println_debug("text did changed called")
        if (MeterOptionType == .myMeterOption)
        {
            println_debug("My Meter is active");
            PayBillChildVCMyMeterObj?.FilterContentForSearchText(searchText: searchBarOutlet.text!)
        }
        else
        {
            println_debug("Other Meter is active");
            PayBillChildVCOtherMeterObj?.FilterContentForSearchText(searchText: searchBarOutlet.text!)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        self.view.endEditing(true)
        self.searchBarOutlet.text = ""
        
        self.title = appDel.getlocaLizationLanguage(key: "Electricity Bill")
        
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white;
        
        searchBarOutlet.isHidden = true
        DispatchQueue.main.async {
            self.navigationItem.titleView = nil
        }
        self.title = "Electricity Bill".localized
        if (MeterOptionType == .myMeterOption)
        {
            println_debug("My Meter is active");
            PayBillChildVCMyMeterObj?.FilterContentForSearchText(searchText: searchBarOutlet.text!)
        }
        else
        {
            println_debug("Other Meter is active");
            PayBillChildVCOtherMeterObj?.FilterContentForSearchText(searchText: searchBarOutlet.text!)
        }
    }
    
    func setLng() -> Void {
        
        self.title = appDel.getlocaLizationLanguage(key: "Electricity Bill")
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20),
                                                         NSAttributedString.Key.foregroundColor: UIColor.white ]
        let titleBarDataSource1 = [appDel.getlocaLizationLanguage(key: "My Meter"), appDel.getlocaLizationLanguage(key: "Other Meter")]
        swipeableView.titleBarDataSource = titleBarDataSource1
        
        for view in (searchBarOutlet.subviews[0]).subviews{
            if let button = view as? UIButton{
                button.setTitle(appDel.getlocaLizationLanguage(key: "Cancel"), for:.normal)
                button.titleLabel?.font = UIFont.init(name: appFont, size: appButtonSize)
            }
        }
        
        for subView in searchBarOutlet.subviews  {
            for subsubView in subView.subviews  {
                if let textField = subsubView as? UITextField {
                    textField.attributedPlaceholder =  NSAttributedString(string:NSLocalizedString(appDel.getlocaLizationLanguage(key: "Search"), comment:""),
                                                                          attributes:[NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: appFont, size: 15) ?? UIFont.systemFont(ofSize: 15)])
                }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ShowRightBarButton"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "HideRightBarButton"), object: nil)
    }
}

public extension UISearchBar {
    
    public func setTextColor(color: UIColor) {
        let svs = subviews.flatMap { $0.subviews }
        guard let tf = (svs.filter { $0 is UITextField }).first as? UITextField else { return }
        tf.textColor = color
    }
}
