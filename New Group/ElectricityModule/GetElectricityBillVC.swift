//
//  GetElectricityBillVC.swift
//  Electricity Bill Module
//
//  Created by Rahul Tyagi on 20/07/17.
//  Copyright © 2017 com.Create. All rights reserved.

import UIKit
import SDWebImage

protocol NotifyMeterViewsDelegate : class {
    func reloadSubmitData()
}

class GetElectricityBillVC: OKBaseController, UIScrollViewDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, UIPickerViewDataSource, UIPickerViewDelegate , DivTownSelectedDelegate, DeleteDelegate, UIGestureRecognizerDelegate {
    
    //Outlets
    @IBOutlet weak var TopImageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var TopImageViewOutlet: UIImageView!
    @IBOutlet weak var tableViewOutlet: UITableView!
    
    @IBOutlet weak var topBtnHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var toplayoutTableConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var ViewSampleMeterImageBtnOutlet: UIButton!{
        didSet {
            ViewSampleMeterImageBtnOutlet.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    //Data Source Array, Enum and Variables.
    var getBillDataSourceArray : [String] = []
    var DataNotInServerDescriptionArray : [String] = []
    var DataNotInServerImageArray : [UIImage] = []
    var DataNotInServerPlaceholderArray : [String] = []
    
    var DataInServerDescriptionArray : [String] = []
    var DataInServerImageArray: [UIImage] = []
    var DataInServerPlaceholderArray : [String] = []
    
    var parentVC_meter = PayBillChildVC_MyMeter()
    var parentVC_other = PayBillChildVC_OtherMeter()
    
    enum DataSourceOptions {
        case GetBillDataSource,  DataNotInServerDataSource, DataInServerDataSource  }
    
    var DataSourceType : DataSourceOptions = .GetBillDataSource
    
    var sectionUpdate : Int = 1
    
    enum GetBillVsSubmit {
        case GetBillBtnAction,SubmitBillBtnAction
    }
    var GetBillVsSubmitType : GetBillVsSubmit = .GetBillBtnAction
    
    enum MeterVsComputerCodeOptions
    {
        case MeterNo , ComputerCodeNo
    }
    var MeterVsComputerCodeType : MeterVsComputerCodeOptions = .MeterNo
    
    var DataInServerTextFieldDict : [String : String] = [:]
    var DataNotInServerTextFieldDict : [String : String] = [:]
    
    //Row and Section Count Array
    var rowCountArray : [String] = [""]
    var sectionCountArray : [String] = [""]
    
    //PickerView Array
    var PayForPickerArray  : [String] = [""]
    
    //Get Biland Submit Button
    var GetBillButton : UIButton? = nil
    var SubmitButton : UIButton? = nil
    
     let validObj  = PayToValidations()
    //Other Variables
    var KeyboardSize : CGFloat = 0.0
    var activeField : UITextField? = nil
    var electricityCellRef : GetElectricityBillTableViewCell? = nil
    var MeterNoString  = ""
    var computerCodeString = ""
    var computerCodeLengthForSelectedTownship = 0
    var showHideComputerCodeField = 0
    var customDatePicker : UIDatePicker? = nil
    var customPayForPicker : UIPickerView? = nil
    
    //Varaibles for Date Picker
    var DarkViewLayer : UIView? = nil;
    var NameViewTransparentLayer : UIView? = nil;
    var datePicker: UIDatePicker? = nil
    var CustomToolbar: UIToolbar? = nil
    var PayForPickerView : UIPickerView? = nil
    var FooterViewContainer : UIView? = nil
    var FooterText :String? = nil
    
    var nameClear = false
    var spaceIsAvail = false
    var spaceIsAvailMid = false
    var pos = 0
    
    enum PickerTypeOptions {
        case DatePicker,PayForPicker
    }
    var PickerType : PickerTypeOptions = .DatePicker
    
    var checkCount = 0
    
    var NameValidationShownOnce = false
    
    var CellYPos : CGFloat = 0
    var nameFHeight : CGFloat = 0
    
    //NameFieldValidation Var
    var NameValidationContainerView : UIView? = nil
    
    var BtnShownOnce : Bool = false
        
    var autoPresentDivController = false
    var computerFieldEnable = false
    
    //protocol delegate
    weak var delegateBillVC : NotifyMeterViewsDelegate?
    //MARK: - View Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        PayForPickerArray = [appDelegate.getlocaLizationLanguage(key: "HomeMeter"),
                             appDelegate.getlocaLizationLanguage(key: "Office"),
                             appDelegate.getlocaLizationLanguage(key: "Business"),
                             appDelegate.getlocaLizationLanguage(key: "Shop")]
        
        TopImageViewHeightConstraint.constant = 0
        
        setupTableDataSource()
        tableViewOutlet.tableFooterView = UIView()
        DataSourceType = .GetBillDataSource
        
        setupCustomisedView()
        showHideComputerCodeField = 0
        self.helpSupportNavigationEnum = .Electricity
        self.loadNavigationBarSettings()
        self.leftBackButton()
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    func leftBackButton() {
        let backBtn = UIBarButtonItem(image: UIImage(named: "tabBarBack"), style: .plain, target: self, action: #selector(backButtonAction))
        self.navigationItem.leftBarButtonItem  = backBtn
    }
    
    @objc func backButtonAction() {
        NameValidationContainerView?.subviews.forEach { $0.removeFromSuperview() }
        NameValidationContainerView?.removeFromSuperview()
        NameViewTransparentLayer?.removeFromSuperview()
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.view.endEditing(true)
        
        if autoPresentDivController == true
        {
            let DivisonVCObj = self.storyboard?.instantiateViewController(withIdentifier: "SelectDivision_TownshipViewControllerSID") as! SelectDivision_TownshipViewController
            DivisonVCObj.delegate = self
            let navController = UINavigationController(rootViewController: DivisonVCObj)
            navController.isNavigationBarHidden = true
            navController.modalPresentationStyle = .fullScreen
            self.present(navController, animated: true, completion: nil)
            
        } else {
             self.tableViewOutlet.reloadData()
        }
        
        SubmitButton?.isEnabled = true
        
        self.navigationController?.navigationBar.isTranslucent = false
        UINavigationBar.appearance().barTintColor = UIColor.init(red: 246/255.0, green: 198/255.0, blue: 0, alpha: 1)
        
        //self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.systemFont(ofSize: 17)]
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden(notification:)) , name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillChangeHeight(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        if MeterOptionType == .myMeterOption
        {
            self.title =  appDelegate.getlocaLizationLanguage(key: "My Meter")
        }
        else
        {
            self.title =  appDelegate.getlocaLizationLanguage(key: "Other Meter")
        }
        
        self.ViewSampleMeterImageBtnOutlet.setTitle(appDelegate.getlocaLizationLanguage(key: "View Sample Meter Bill"), for: .normal)
        self.ViewSampleMeterImageBtnOutlet.titleLabel?.font = UIFont.init(name: appFont, size: appButtonSize)
        
        
        if DataSourceType == .DataNotInServerDataSource && rowCountArray.count == 14
        {
            self.SubmitButton?.isHidden = false
        }
        else if DataSourceType == .DataInServerDataSource && rowCountArray.count == 9
        {
            self.SubmitButton?.isHidden = false
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        autoPresentDivController = false
    }
    
    func btnOrImage(showImage:Bool)
    {
        if checkCount == 0 //Hide Button Always
        {
            self.topBtnHeightConstraint.constant = 0
            
            if showImage == true
            {
                self.TopImageViewHeightConstraint.constant = 180
                self.toplayoutTableConstraint.constant = 8
            }
            else
            {
                self.TopImageViewHeightConstraint.constant = 0
                self.toplayoutTableConstraint.constant = 0
                
            }
            
            
        }
        else //User coming for the second time
        {
            if BtnShownOnce == false
            {
                self.topBtnHeightConstraint.constant = 50
                self.TopImageViewHeightConstraint.constant = 0
                self.toplayoutTableConstraint.constant = 58
                
            }
            else //True
            {
                self.topBtnHeightConstraint.constant = 0
                
                if showImage == true
                {
                    self.TopImageViewHeightConstraint.constant = 180
                    self.toplayoutTableConstraint.constant = 8
                }
                else
                {
                    self.TopImageViewHeightConstraint.constant = 0
                    self.toplayoutTableConstraint.constant = 0
                    
                }
                
                
            }
            
        }
        self.view.layoutIfNeeded()
        
    }
    
    @IBAction func reloadSampleImage(_ sender: Any) {
        self.topBtnHeightConstraint.constant = 0
        self.TopImageViewHeightConstraint.constant = 180
        self.toplayoutTableConstraint.constant = 8
        self.view.layoutIfNeeded()
        BtnShownOnce = true
        ViewSampleMeterImageBtnOutlet.isHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification,object : nil )
        
        self.view.endEditing(true)
        
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
        
    }
    
    @IBAction func BackBtnAction(_ sender: Any)
    {
        NameValidationContainerView?.subviews.forEach { $0.removeFromSuperview() }
        NameValidationContainerView?.removeFromSuperview()
        NameViewTransparentLayer?.removeFromSuperview()
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification)
    {
        //Need to calculate keyboard exact size due to Apple suggestions
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        self.KeyboardSize = (keyboardSize?.height)!
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: keyboardSize!.height + 45, right: 0.0)
        
        self.tableViewOutlet.contentInset = contentInsets
        self.tableViewOutlet.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if activeField != nil
        {
            if (!aRect.contains(activeField!.frame.origin))
            {
                self.tableViewOutlet.scrollRectToVisible(activeField!.frame, animated: true)
            }
        }
        
        
    }
    
    
    @objc func keyboardWillChangeHeight(notification: NSNotification)
    {
        //Need to calculate keyboard exact size due to Apple suggestions
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size  
        self.KeyboardSize = (keyboardSize?.height)!
        println_debug("Keyboard changed \((KeyboardSize,keyboardSize?.height)))")
        GetBillButton?.frame.origin.y = self.view.frame.height - KeyboardSize - 54
        SubmitButton?.frame.origin.y = self.view.frame.height - KeyboardSize - 54
        
        self.view.layoutIfNeeded()
    }
    
    
    @objc func keyboardWillBeHidden(notification: NSNotification)
    {
        //Once keyboard disappears, restore original positions
        let info : NSDictionary = notification.userInfo! as NSDictionary
        _ = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        self.tableViewOutlet.contentInset = contentInsets
        self.tableViewOutlet.scrollIndicatorInsets = contentInsets
        // self.view.endEditing(true)
        GetBillButton?.frame.origin.y = self.view.frame.height - 54
        SubmitButton?.frame.origin.y = self.view.frame.height - 54
    }
    
    
    //MARK: - Buttons & View Initialization
    
    func setupCustomisedView()
    {
        
        //Get Bill Button
        GetBillButton = UIButton.init(frame: CGRect(x: 0, y: self.view.frame.height - KeyboardSize - 60 , width: self.view.frame.width, height: 54))
        GetBillButton?.setTitle( appDelegate.getlocaLizationLanguage(key: "Get Bill"), for: .normal)
        GetBillButton?.titleLabel?.font = UIFont.init(name: appFont, size: appButtonSize)
        GetBillButton?.backgroundColor = UIColor.init(red: 246/255.0, green: 198/255.0, blue: 0, alpha: 1)
        GetBillButton?.addTarget(self, action: #selector(self.GetBillBtnAction), for: .touchUpInside)
        self.view.addSubview(GetBillButton!)
        self.view.bringSubviewToFront(GetBillButton!)
        GetBillButton?.isHidden = true
        
        
        //Submit Bill Button
        SubmitButton = UIButton.init(frame: CGRect(x: 0, y: self.view.frame.height - KeyboardSize - 60 , width: self.view.frame.width, height: 54))
        SubmitButton?.setTitle( appDelegate.getlocaLizationLanguage(key: "Submit"), for: .normal)
        SubmitButton?.titleLabel?.font = UIFont.init(name: appFont, size: appButtonSize)
        SubmitButton?.backgroundColor = UIColor.init(red: 246/255.0, green: 198/255.0, blue: 0, alpha: 1)
        SubmitButton?.addTarget(self, action: #selector(self.submitBtnAction), for: .touchUpInside)
        self.view.addSubview(SubmitButton!)
        self.view.bringSubviewToFront(SubmitButton!)
        SubmitButton?.isHidden = true
        
        FooterViewContainer = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100))
        
    }
    
    
    @objc func GetBillBtnAction()
    {
        GetBillVsSubmitType = .GetBillBtnAction
        GetBillAPIRequest()
    }
    
    @objc func submitBtnAction()
    {
        SubmitButton?.isEnabled = false
        
        self.view.endEditing(true)
        self.SubmitButton?.frame.origin.y = self.view.frame.height - 50
        
        GetBillVsSubmitType = .SubmitBillBtnAction
        GetBillAPIRequest()
    }
    
    //MARK:- Date PickerView Methods
    func showCustomPickerView()
    {
        
        
        DarkViewLayer = UIView.init(frame: CGRect(x: 0, y: 0, width: (UIApplication.shared.keyWindow?.frame.width)!, height: (UIApplication.shared.keyWindow?.frame.height)!))
        DarkViewLayer?.backgroundColor = .lightGray
        datePicker?.maximumDate = Date()
        DarkViewLayer?.alpha = 0.9
        UIApplication.shared.keyWindow?.addSubview(DarkViewLayer!)
        
        
        CustomToolbar = UIToolbar.init(frame: CGRect(x: 0, y: ((UIApplication.shared.keyWindow?.frame.height)! - 200) - 40, width: self.view.frame.size.width , height: 30))
        
        CustomToolbar?.barStyle = UIBarStyle.default
        CustomToolbar?.isTranslucent = false
        CustomToolbar?.tintColor = .white
        CustomToolbar?.barTintColor = UIColor.init(red: 246/255.0, green: 198/255.0, blue: 0, alpha: 1)
        
        
        CustomToolbar?.sizeToFit()
        UIApplication.shared.keyWindow?.addSubview(CustomToolbar!)
        
        
        
        if PickerType == .DatePicker
        {
            
            datePicker = UIDatePicker.init(frame: CGRect(x: 0, y: (UIApplication.shared.keyWindow?.frame.height)! - 200, width: self.view.frame.size.width, height: 200))
            datePicker?.datePickerMode = .date
            //datePicker?.backgroundColor = UIColor.white
            
            let components:NSDateComponents = Calendar.current.dateComponents([.year, .month], from: NSDate() as Date) as NSDateComponents
            let minDate = Calendar.current.date(from: components as DateComponents)!
            
            
            let maxDate = Calendar.current.date(byAdding: .day, value: 4, to: minDate)!
            
            //println_debug(minDate,maxDate)
            datePicker?.backgroundColor = .gray
            datePicker?.minimumDate = minDate
            datePicker?.maximumDate = maxDate
            if #available(iOS 13.4, *) {
                datePicker?.preferredDatePickerStyle = .wheels
            }
            datePicker?.setDate(minDate, animated: true)
            
            UIApplication.shared.keyWindow?.addSubview(datePicker!)
              
            let doneButton = UIBarButtonItem(title:  appDelegate.getlocaLizationLanguage(key: "Select Date"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.selectDateFromPicker))
            doneButton.setTitleTextAttributes([ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20),
                                                NSAttributedString.Key.foregroundColor: UIColor.white ], for: .normal)
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: appDelegate.getlocaLizationLanguage(key: "Cancel"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.hidePicker))
            cancelButton.setTitleTextAttributes([ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20),
                                                  NSAttributedString.Key.foregroundColor: UIColor.white ], for: .normal)
            
            
            CustomToolbar?.setItems([spaceButton, cancelButton, spaceButton,spaceButton,spaceButton, doneButton, spaceButton], animated: true)
        }
        else
        {
            PayForPickerView = UIPickerView.init(frame: CGRect(x: 0, y: (UIApplication.shared.keyWindow?.frame.height)! - 200, width: self.view.frame.size.width, height: 200))
            PayForPickerView?.dataSource = self
            PayForPickerView?.delegate = self
            PayForPickerView?.backgroundColor = UIColor.white
            UIApplication.shared.keyWindow?.addSubview(PayForPickerView!)
            
            
            let doneButton = UIBarButtonItem(title: appDelegate.getlocaLizationLanguage(key: "Select"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.selectValueFromPayForPicker))
            doneButton.setTitleTextAttributes([ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20),
                                                NSAttributedString.Key.foregroundColor: UIColor.white ], for: .normal)
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: appDelegate.getlocaLizationLanguage(key: "Cancel"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.hidePicker))
            cancelButton.setTitleTextAttributes([ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20),
                                                  NSAttributedString.Key.foregroundColor: UIColor.white ], for: .normal)
            
            CustomToolbar?.setItems([spaceButton, cancelButton, spaceButton,spaceButton,spaceButton, doneButton, spaceButton], animated: true)
            
        }
        
        CustomToolbar?.isUserInteractionEnabled = true
    }
    
    
    @objc func selectValueFromPayForPicker()
    {
        
        let valueFromPayPicker = PayForPickerArray[(PayForPickerView?.selectedRow(inComponent: 0))!]
        
        if DataSourceType == . DataNotInServerDataSource
        {
            DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[8]] = valueFromPayPicker
            let eighthIndex = NSIndexPath.init(row: 8, section: 0)
            electricityCellRef = tableViewOutlet.cellForRow(at: eighthIndex as IndexPath) as? GetElectricityBillTableViewCell
            electricityCellRef?.textFieldOutlet.text =  appDelegate.getlocaLizationLanguage(key: valueFromPayPicker)
            
            DispatchQueue.main.async {
                
                if self.rowCountArray.count == 9
                {
                    self.showNextRow()
                }
                
                if self.view.frame.height > 600
                {
                    //self.scrollToLastRow()
                }
                
                let ninthIndex = NSIndexPath.init(row: 9, section: 0)
                self.electricityCellRef = self.tableViewOutlet.cellForRow(at: ninthIndex as IndexPath) as? GetElectricityBillTableViewCell
                
                self.electricityCellRef?.textFieldOutlet.becomeFirstResponder()
                self.DarkViewLayer?.removeFromSuperview()
                self.datePicker?.removeFromSuperview()
                self.CustomToolbar?.removeFromSuperview()
                self.PayForPickerView?.removeFromSuperview()
            }
            
        }
        else if DataSourceType == .DataInServerDataSource
        {
            DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Pay For")] = valueFromPayPicker
            let thirdIndex = NSIndexPath.init(row: 3, section: 0)
            electricityCellRef = tableViewOutlet.cellForRow(at: thirdIndex as IndexPath) as? GetElectricityBillTableViewCell
            electricityCellRef?.textFieldOutlet.text = valueFromPayPicker
            
            DispatchQueue.main.async {
                
                if self.rowCountArray.count == 4
                {
                    self.showNextRow()
                }
                
                if self.view.frame.height > 600
                {
                    //self.scrollToLastRow()
                }
                
                let conntactNoIndexPath = NSIndexPath.init(row: 4, section: 0)
                self.electricityCellRef = self.tableViewOutlet.cellForRow(at: conntactNoIndexPath as IndexPath) as? GetElectricityBillTableViewCell
                self.electricityCellRef?.textFieldOutlet.becomeFirstResponder()
                
                self.DarkViewLayer?.removeFromSuperview()
                self.datePicker?.removeFromSuperview()
                self.CustomToolbar?.removeFromSuperview()
                self.PayForPickerView?.removeFromSuperview()
            }
        }
        
    }
    
    
    @objc func selectDateFromPicker()
    {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        let strDate = dateFormatter.string(from: (datePicker?.date)!)
        //println_debug(strDate)
        
        if DataSourceType == . DataNotInServerDataSource
        {
            DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[5]] = strDate
        }
        
        
        let FithIndexPath = NSIndexPath.init(row: 5, section: 0)
        electricityCellRef = tableViewOutlet.cellForRow(at: FithIndexPath as IndexPath) as? GetElectricityBillTableViewCell
        electricityCellRef?.textFieldOutlet.text = strDate
        
        DispatchQueue.main.async {
            
            if self.rowCountArray.count == 6
            {
                self.showNextRow()
            }
            
            if self.view.frame.height > 600
            {
                //self.scrollToLastRow()
            }
            
            
            let NextIndexPath = NSIndexPath.init(row: 6, section: 0)
            self.electricityCellRef = self.tableViewOutlet.cellForRow(at: NextIndexPath as IndexPath) as? GetElectricityBillTableViewCell
            self.electricityCellRef?.textFieldOutlet.becomeFirstResponder()
            
            self.DarkViewLayer?.removeFromSuperview()
            self.datePicker?.removeFromSuperview()
            self.CustomToolbar?.removeFromSuperview()
            self.PayForPickerView?.removeFromSuperview()
        }
        
    }
    
    @objc func hidePicker()
    {
        DarkViewLayer?.removeFromSuperview()
        datePicker?.removeFromSuperview()
        CustomToolbar?.removeFromSuperview()
        PayForPickerView?.removeFromSuperview()
        self.view.endEditing(true)
        //scrollToLastRow()
        SubmitButton?.frame.origin.y = self.view.frame.height - 50
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return PayForPickerArray.count
    }

    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel = view as? UILabel;
        
        if (pickerLabel == nil)
        {
            pickerLabel = UILabel()
            
            pickerLabel?.font = UIFont(name: appFont, size: 23)
            pickerLabel?.textAlignment = NSTextAlignment.center
        }
        
        pickerLabel?.text = PayForPickerArray[row]
        
        return pickerLabel!;
    }
    
    func formattedDateFromString(dateString: String, withFormat inputFormat: String, toBeConvertedToFormat outputFormat : String) -> String?
    {
        //Check if dateString complies with Input format
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = inputFormat
        inputFormatter.calendar = Calendar(identifier: .gregorian)

        if let date = inputFormatter.date(from: dateString) {
            
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = outputFormat
            outputFormatter.calendar = Calendar(identifier: .gregorian)

            return outputFormatter.string(from: date)
        }
        
        return nil
    }
    
    
    
    fileprivate func displayAllStatusAlert(alertMessage : String)
    {
        self.view.endEditing(true)
        alertViewObj.wrapAlert(title: nil, body: appDelegate.getlocaLizationLanguage(key: alertMessage), img: #imageLiteral(resourceName: "dashboard_electricity"))
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        
        alertViewObj.showAlert(controller: self)
        progressViewObj.removeProgressView()
    }
    
    
    fileprivate func displayReadBillStatus(resultStringToJSON : AnyObject)
    {
        
        DispatchQueue.main.async {
            self.DataSourceType = .DataInServerDataSource
            
            if self.MeterVsComputerCodeType == .MeterNo
            {
                self.DataInServerDescriptionArray[2] = appDelegate.getlocaLizationLanguage(key: "Meter Number")
                self.DataInServerPlaceholderArray[2] = appDelegate.getlocaLizationLanguage(key: "Enter Meter Number")
                self.DataInServerImageArray[2] = #imageLiteral(resourceName: "meternumber")
            }
            else
            {
                self.DataInServerDescriptionArray[2] = appDelegate.getlocaLizationLanguage(key: "Computer Code Number")
                self.DataInServerPlaceholderArray[2] = appDelegate.getlocaLizationLanguage(key: "Enter Computer Code Number")
                self.DataInServerImageArray[2] = #imageLiteral(resourceName: "Computer Code Number")
            }
            
            self.MeterNoString = resultStringToJSON["meterNumber"] as! String
            self.computerCodeString = resultStringToJSON["custRefNum"] as! String
            
            EBCashBack = (resultStringToJSON["cashBack"] as? String)!
            
            //Set Data From Server in TextField
            
            if resultStringToJSON["cashBack"] as? String == "" || resultStringToJSON["cashBack"] as? String == "0.00"
            {
                self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Cash Back")] = "0"                                             }
            else
            {
                self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Cash Back")] = resultStringToJSON["cashBack"] as? String
            }
            
            if resultStringToJSON["serviceFee"] as? String == "" || resultStringToJSON["serviceFee"] as? String == "0.00"
            {
                self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Service Fees")] = "0"
                MeterBillServiceFee = "0"
            }
            else
            {
                self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Service Fees")] = resultStringToJSON["serviceFee"] as? String
                MeterBillServiceFee = resultStringToJSON["serviceFee"] as? String ?? "0"
            }
            
            self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Number")] = resultStringToJSON["meterNumber"] as? String
            self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Consumer Name (as on Bill)")] = resultStringToJSON["billerName"] as? String
            if let val = resultStringToJSON["tarifNumber"] as? String
            {
                self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Tarif Number")] = val
            }
            else
            {
                self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Tarif Number")] = ""
            }
            self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Computer Code Number")] = resultStringToJSON["custRefNum"] as? String
            
            
            //Change Date Format
            let delimiter = " "
            let dateValue = resultStringToJSON["meterReadDate"] as? String
            let shortDateStr = dateValue?.components(separatedBy: delimiter)
            let CustomMeterReadDate = self.formattedDateFromString(dateString: shortDateStr![0], withFormat: "MM/dd/yyyy", toBeConvertedToFormat: "dd/MM/yyyy")
            
            self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Read Date")] = CustomMeterReadDate
            self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "LedgerNumber")] = resultStringToJSON["accountNumber"] as? String
            
            
            if resultStringToJSON["actualBillAmountPreDef"]  as! String == "0.00"
            {
                
            }
            else
            {
                
                MeterBillAmountParam = resultStringToJSON["actualBillAmountPreDef"]  as! String
                AdjustAmountParam = ""
                
                let IntValue = (resultStringToJSON["actualBillAmountPreDef"] as! NSString).integerValue
                
                self.DataInServerDescriptionArray[5] = appDelegate.getlocaLizationLanguage(key: "Bill Amount")
                
                self.DataInServerDescriptionArray.remove(at: 6)
                self.DataInServerImageArray.remove(at: 6)
                self.DataInServerPlaceholderArray.remove(at: 6)
                
                //Change
                let formatter = NumberFormatter()
                formatter.numberStyle = NumberFormatter.Style.decimal
                let formattedNum = formatter.string(from: NSNumber(value: IntValue))
                
                self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Bill Amount")] = formattedNum
                
            }
            
            self.tableViewOutlet.reloadData()
            
            self.rowCountArray.append("")
            self.tableViewOutlet.beginUpdates()
            self.tableViewOutlet.insertRows(at: [IndexPath(row: self.rowCountArray.count-1, section: 0)], with: .none)
            self.tableViewOutlet.endUpdates()
            
            if self.view.frame.height > 600
            {
                //self.scrollToLastRow()
            }
            
        }
    }
    
    
    
    fileprivate func responseToSubmitButton(resultStringToJSON : AnyObject)
    {
        let myAttribute = [NSAttributedString.Key.font: UIFont(name: appFont, size: 17) ?? UIFont.systemFont(ofSize: 17)]
        
        if (resultStringToJSON["okDollarmeterReadDateStatus"] as! Bool == false || resultStringToJSON["meterReadDateStatus"] as! Bool == false) && resultStringToJSON["allowToAddMeterBillData"] as! String == "300"
        {
            
            var OkDollarMeterReadDateErrorMsg = ""
            
            if appDelegate.getSelectedLanguage() == "en"
            {
                OkDollarMeterReadDateErrorMsg = NSAttributedString(string: (resultStringToJSON["okDollarmeterReadDateStatusMessageEName"]! as! String), attributes: myAttribute).string
            }
            else
            {
                OkDollarMeterReadDateErrorMsg = NSAttributedString(string: (resultStringToJSON["okDollarmeterReadDateStatusMessageUnicode"]! as! String), attributes: myAttribute).string
            }
            
            DispatchQueue.main.async(){
                alertViewObj.wrapAlert(title: nil, body: OkDollarMeterReadDateErrorMsg.localized, img: UIImage(named : "AppIcon"))
                alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                    self.SubmitAPIRequest()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                        self.delegateBillVC?.reloadSubmitData()
                        self.dismiss(animated: true, completion: nil)
                    })
                    
                })
                alertViewObj.showAlert(controller: self)
            }
        }
        else if resultStringToJSON["okDollarmeterReadDateStatus"] as! Bool == true && resultStringToJSON["meterReadDateStatus"] as! Bool == true
        {
            //Check Balance
            let balanceAmount = (UserLogin.shared.walletBal as NSString).floatValue
            
            //println_debug(balanceAmount,MeterBillAmountParam)
            
            let billAmountIntValue = (MeterBillAmountParam as NSString).floatValue
            
            if balanceAmount <= billAmountIntValue
            {
                //Show Alert
                var lowBalanceAlert = ""
                
                if appDelegate.getSelectedLanguage() == "en"
                {
                    lowBalanceAlert = NSAttributedString(string: appDelegate.getlocaLizationLanguage(key: "Your meter bill details is submitted but you have insufficient balance."), attributes: myAttribute).string
                }
                else
                {
                    lowBalanceAlert = NSAttributedString(string: appDelegate.getlocaLizationLanguage(key: "Your meter bill details is submitted but you have insufficient balance."), attributes: myAttribute).string
                }
                
                
                
                DispatchQueue.main.async(){
                    alertViewObj.wrapAlert(title: nil, body: lowBalanceAlert.localized, img: UIImage(named : "AppIcon"))
                    alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                        self.SubmitAPIRequest()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                            self.delegateBillVC?.reloadSubmitData()
                            self.dismiss(animated: true, completion: nil)
                        })
                    })
                    alertViewObj.showAlert(controller: self)
                }
                
                
            }
            else
            {
                //Go for Pay
                var DataToBePassedArray : [String] =  []
                DataToBePassedArray.removeAll()
                let ConfirmationVCObj = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationVCSID") as! ConfirmationVC
                
                let descriptionArrayToBePassed = [appDelegate.getlocaLizationLanguage(key: "Division"),
                                                  appDelegate.getlocaLizationLanguage(key: "Township"),
                                                  appDelegate.getlocaLizationLanguage(key: "Meter Number"),
                                                  appDelegate.getlocaLizationLanguage(key: "Consumer Name (as on Bill)"),
                                                  appDelegate.getlocaLizationLanguage(key: "Ledger Number"),
                                                  appDelegate.getlocaLizationLanguage(key: "Meter Read Date"),
                                                  appDelegate.getlocaLizationLanguage(key: "Tariff"),
                                                  appDelegate.getlocaLizationLanguage(key: "Computer Code Number"),
                                                  appDelegate.getlocaLizationLanguage(key: "Pay For"),
                                                  appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number"),
                                                  appDelegate.getlocaLizationLanguage(key: "Bill Amount"),
                                                  appDelegate.getlocaLizationLanguage(key: "Service Fee"),
                                                  appDelegate.getlocaLizationLanguage(key: "Remark")]
                
                
                if self.DataSourceType == .DataNotInServerDataSource
                {
                    self.DataNotInServerTextFieldDict["Bill Amount"] = MeterBillAmountParam
                    self.DataNotInServerTextFieldDict["Service Fees"] = MeterBillServiceFee

                    DataToBePassedArray = [self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Division")]!,
                                           self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Township")]!,
                                           self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Number")]!,
                                           self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Consumer Name (as on Bill)")]!,
                                           self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Ledger Number")]!,
                                           self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Read Date")]!,
                                           self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Tariff")]!,
                                           self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Computer Code Number")]!,
                                           self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Pay For")]!,
                                           self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number")]!,
                                           MeterBillAmountParam,
                                           MeterBillServiceFee,
                                           self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")]!]
                    
                    ConfirmationVCObj.caseScenerioType = .DataNotInServer
                    
                }
                else if self.DataSourceType == .DataInServerDataSource
                {
//                    let formatter = NumberFormatter()
//                    formatter.numberStyle = NumberFormatter.Style.decimal
//                    let textAfterRemovingComma = (self.DataInServerTextFieldDict["Enter Bill Amount"]!).replacingOccurrences(of: ",", with: "")
                    self.DataInServerTextFieldDict["Bill Amount"] = MeterBillAmountParam
                    self.DataInServerTextFieldDict["Service Fees"] = MeterBillServiceFee
                    
                    DataToBePassedArray =   [self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Division")]!,
                                             self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Township")]!,
                                             self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Number")]!,
                                             self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Consumer Name (as on Bill)")]!,
                                             self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "LedgerNumber")]!,
                                             self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Read Date")]!,
                                             self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Tarif Number")]!,
                                             self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Computer Code Number")]!,
                                             self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Pay For")]!,
                                             self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number")]!,
                                             MeterBillAmountParam,
                                             MeterBillServiceFee,
                                             self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")]!]
                    
                    
                    ConfirmationVCObj.caseScenerioType = .DataInServer
                    
                }
                DispatchQueue.main.async {
                    
                    println_debug("Transitioning to confirmation Page")
                    ConfirmationVCObj.descriptionArray = descriptionArrayToBePassed
                    ConfirmationVCObj.ValueArray = DataToBePassedArray
                    
                    ConfirmationVCObj.chainClass = self
                    self.navigationController?.pushViewController(ConfirmationVCObj, animated: true)
                    
                }
            }
        } else {
             if let message =  resultStringToJSON["okDollarmeterReadDateStatusMessageEName"] as? String
             {
                DispatchQueue.main.async(){
                    alertViewObj.wrapAlert(title: nil, body: message.localized, img: UIImage(named : "AppIcon"))
                    alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                        self.delegateBillVC?.reloadSubmitData()
                        self.dismiss(animated: true, completion: nil)
                    })
                    alertViewObj.showAlert(controller: self)
                }
             }
             else if let message =  resultStringToJSON["meterReadDateStatusMessageEName"] as? String
             {
                DispatchQueue.main.async(){
                    alertViewObj.wrapAlert(title: nil, body: message.localized, img: UIImage(named : "AppIcon"))
                    alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                        self.delegateBillVC?.reloadSubmitData()
                        self.dismiss(animated: true, completion: nil)
                    })
                    alertViewObj.showAlert(controller: self)
                }
             } else {
                DispatchQueue.main.async(){
                    self.displayAllStatusAlert(alertMessage: "Try Again".localized)
                }
            }
        }
        
    }
    
    
    fileprivate func readStatusFalse(resultStringToJSON : AnyObject)
    {
        self.MeterVsComputerCodeType = .MeterNo
        
        let myAttribute = [NSAttributedString.Key.font: UIFont(name: appFont, size: 17) ?? UIFont.systemFont(ofSize: 17)]
        
        if resultStringToJSON["allowToAddMeterBillData"] as! String == "200"
        {
            if resultStringToJSON["okDollarmeterReadDateStatus"] as! Bool == false
            {
                var OkDollarMeterReadDateErrorMsg = ""
                
                
                if appDelegate.getSelectedLanguage() == "en"
                {
                    OkDollarMeterReadDateErrorMsg = NSAttributedString(string: (resultStringToJSON["okDollarmeterReadDateStatusMessageEName"]! as! String), attributes: myAttribute).string
                }
                else
                {
                    OkDollarMeterReadDateErrorMsg = NSAttributedString(string: (resultStringToJSON["okDollarmeterReadDateStatusMessageUnicode"]! as! String), attributes: myAttribute).string
                }
                
                DispatchQueue.main.async(){
                    self.displayAllStatusAlert(alertMessage: OkDollarMeterReadDateErrorMsg)
                }
                
            }
            else
            {
                if resultStringToJSON["meterReadDateStatus"] as! Bool == false
                {
                    var meterReadDateStatusMessage = ""
                    
                    if appDelegate.getSelectedLanguage() == "en"
                    {
                        meterReadDateStatusMessage = NSAttributedString(string: (resultStringToJSON["meterReadDateStatusMessageEName"]! as! String), attributes: myAttribute).string
                    }
                    else
                    {
                        meterReadDateStatusMessage = NSAttributedString(string: (resultStringToJSON["meterReadDateStatusMessageUnicode"]! as! String), attributes: myAttribute).string
                        
                    }
                    
                    DispatchQueue.main.async(){
                        self.displayAllStatusAlert(alertMessage: meterReadDateStatusMessage)
                    }
                }
                
            }
        }
    }
    
    
    fileprivate func readStatusTrue(resultStringToJSON : AnyObject)
    {
        
        DispatchQueue.main.async {
            self.DataSourceType = .DataNotInServerDataSource
            
            if self.sectionCountArray.count == 2 {
                self.sectionCountArray.removeLast()
                self.sectionUpdate = 0
            }
            
            self.tableViewOutlet.reloadData()
            
            if self.MeterVsComputerCodeType == .MeterNo
            {
                self.rowCountArray.append("")
                self.tableViewOutlet.beginUpdates()
                self.tableViewOutlet.insertRows(at: [IndexPath(row: self.rowCountArray.count-1, section: 0)], with: .none)
                self.tableViewOutlet.endUpdates()
                
            }
            else
            {
                
            }
            EBCashBack = (resultStringToJSON["cashBack"] as? String)!
            
            if resultStringToJSON["cashBack"] as? String == "" || resultStringToJSON["cashBack"] as? String == "0.00"
            {
                self.DataNotInServerTextFieldDict["Cash Back"] = "0"
                
            }
            else
            {
                self.DataNotInServerTextFieldDict["Cash Back"] = resultStringToJSON["cashBack"] as? String
            }
            
            if resultStringToJSON["serviceFee"] as? String == "" || resultStringToJSON["serviceFee"] as? String == "0.00"
            {
                self.DataNotInServerTextFieldDict["Service Fees"] = "0"
                MeterBillServiceFee = "0"
                
            }
            else
            {
                self.DataNotInServerTextFieldDict["Service Fees"] = resultStringToJSON["serviceFee"] as? String
                MeterBillServiceFee = resultStringToJSON["serviceFee"] as? String ?? "0"
            }
            
            if self.view.frame.height > 600
            {
                //self.scrollToLastRow()
            }
            
            
        }
    }
    //MARK: - API Calls
    
    
    func GetBillAPIRequest()
    {
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
        }
        
        let customURL = URL.init(string: "http://www.okdollar.co/RestService.svc/GetDataByVerifyMeterBillOrRefNum")
        let agentNum = UserModel.shared.mobileNo
        let msid = UserModel.shared.msid
        let str = uuid
        println_debug("Meter : \(MeterNoString) Computer Code : \(computerCodeString)")
        
        var parameters : [String : Any] = [:]
        
        if GetBillVsSubmitType == .GetBillBtnAction  //Requesting Get Bill 1st time
        {
            var paramString = ""
            if MeterNoString == ""
            {
                paramString = computerCodeString
                MeterVsComputerCodeType = .ComputerCodeNo
            }
            else if computerCodeString == ""
            {
                paramString = MeterNoString
                MeterVsComputerCodeType = .MeterNo
            }
            
            parameters = [  "MeterBillRefNumForSearch":paramString,
                            "SearchType":"0",
                            "TownshipId": EBTownshipID ,
                            "LoginDeviceInfo": [
                                "MobileNumber": agentNum,
                                "Msid": msid,
                                "Ostype": 1,
                                "Otp": "",
                                "Simid": str
                ]
                ] as [String : Any]
            
            println_debug(parameters)
        }
        else  ////Requesting Get Bill 2nd time on submit btn click
        {
            var paramString = ""
            
            if MeterVsComputerCodeType == .MeterNo
            {
                paramString = computerCodeString
            }
            else
            {
                paramString = MeterNoString
            }
            
            parameters = [  "MeterBillRefNumForSearch": paramString,
                            "SearchType":"1",
                            "TownshipId": EBTownshipID ,
                            "LoginDeviceInfo": [
                                "MobileNumber": agentNum,
                                "Msid": msid,
                                "Ostype": 1,
                                "Otp": "",
                                "Simid": str
                ]
                ] as [String : Any]
        }
        
        println_debug(parameters)
        
        // JSON all the things
        let auth = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: customURL!)
        
        do {
            println_debug("Requesting Get Bill API Action")
            
            // Set the request content type to JSON
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            // The magic...set the HTTP request method to POST
            request.httpMethod = "POST"
            
            // Add the JSON serialized login data to the body
            request.httpBody = auth
            
            // Create the task that will send our login request (asynchronously)
            let task = session.dataTask(with: request as URLRequest, completionHandler:
            { (data, response, error) -> Void in
                
                if(data == nil)
                {
                    println_debug("Data is nil")
                    DispatchQueue.main.async(){
                        self.SubmitButton?.isEnabled = true
                        alertViewObj.wrapAlert(title: nil, body: appDelegate.getlocaLizationLanguage(key: "Please Try Again Later."), img: #imageLiteral(resourceName: "call"))
                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                        })
                        alertViewObj.showAlert(controller: self)
                        progressViewObj.removeProgressView()
                    }
                }
                else
                {
                    do {
                        println_debug("Call was in do block")
                        let jsonResponse = try? JSONSerialization.jsonObject(with: data!, options:.allowFragments)
                        if let jsonResponse = jsonResponse as? [String: Any] {
                            //if let jsonResponse = jsonResponse as? [String: Any]  {
                            //println_debug("\n \n \n \(jsonResponse)")
                            
                            DispatchQueue.main.async {
                                self.SubmitButton?.isEnabled = true
                                self.GetBillButton?.isHidden = true
                            }
                            
                            if jsonResponse["Code"] as! NSNumber ==  200
                            {
                                let DataStringToJSON : [String : Any] = self.convertStringToJson(ReceivedString: jsonResponse["Data"]! as! String) as! [String : Any]
                                //println_debug(DataStringToJSON)
                                
                                let resultStringToJSON : AnyObject  = self.convertStringToJson(ReceivedString: DataStringToJSON["result"]! as! String) as AnyObject
                                //println_debug(resultStringToJSON)
                                let jsonData = JSON(resultStringToJSON)
                                println_debug(jsonData)

                                if appDelegate.getSelectedLanguage() == "en"
                                {
                                    self.FooterText = resultStringToJSON["bottom1TextEName"] as? String
                                }
                                else
                                {
                                    self.FooterText = resultStringToJSON["bottom1TextUnicode"] as? String
                                }
                                
                                DispatchQueue.main.async {
                                    progressViewObj.removeProgressView()
                                }
//                                self.readStatusTrue(resultStringToJSON: resultStringToJSON)
 
                                if self.GetBillVsSubmitType == .GetBillBtnAction
                                {
                                    if resultStringToJSON["exceptionCode"] as! NSNumber == 500 || resultStringToJSON["resultCode"] as! NSNumber == 500
                                    {
                                        println_debug("Exception Occurred")
                                        
                                        DispatchQueue.main.async(){
                                            self.view.endEditing(true)
                                            self.MeterVsComputerCodeType = .MeterNo
                                            self.displayAllStatusAlert(alertMessage: "Please Try Again Later.")
                                        }
                                    }
                                    else
                                    {
                                        switch (resultStringToJSON["resultCode"] as! NSNumber)
                                        {
                                        case 200 : println_debug("Result Code  is 200 ")
                                        self.readStatusTrue(resultStringToJSON: resultStringToJSON)
                                            
                                        case 201 :
                                            println_debug("Result Code is 201")
                                            
                                            if resultStringToJSON["meterReadDateStatus"] as! Bool == true || resultStringToJSON["allowToAddMeterBillData"] as! String == "300"
                                            {
                                                //Reload Table
                                                self.displayReadBillStatus(resultStringToJSON: resultStringToJSON)
                                            }
                                            else  //Meter Read Date status is false
                                            {
                                                self.readStatusFalse(resultStringToJSON: resultStringToJSON)
                                            }
                                            
                                        case 202 :  println_debug("Result Code is 202")
                                        
                                        DispatchQueue.main.async(){
                                            self.view.endEditing(true)
                                            self.MeterVsComputerCodeType = .MeterNo
                                            self.displayAllStatusAlert(alertMessage: "You have Already Paid Electricity Bill for the Current Month")
                                            }
                                            
                                        default : println_debug("something went wrong")
                                            
                                        }
                                    }
                                }
                                else //Submit Btn Action
                                {
                                    self.responseToSubmitButton(resultStringToJSON: resultStringToJSON)
                                }
                            }
                            else
                            {
                                DispatchQueue.main.async {
                                    progressViewObj.removeProgressView()
                                }
                                println_debug("Condition failed")
                            }
                        }
                    }
                }
            })
            // Start the task on a background thread
            task.resume()
        }
    }
    
    
    func SubmitAPIRequest()
    {
        
        
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
        }
        
        let customURL = URL.init(string: "https://www.okdollar.co/RestService.svc/AddEBillMetrInfo")
        
        let agentNum = UserModel.shared.mobileNo
        
        let msid = UserModel.shared.msid
        
        let str = uuid
        
        let pwd = ok_password
        
        var parameters : [String : Any] = [:]
        
        var paramString  = ""
        
        if MeterVsComputerCodeType == .MeterNo
        {
            paramString = computerCodeString
        }
        else
        {
            paramString = MeterNoString
        }
        
        var meterTypeParam = true
        if MeterOptionType == .myMeterOption
        {
            meterTypeParam = true
        }
        else
        {
            meterTypeParam = false
        }
        
        if DataSourceType == .DataNotInServerDataSource
        {
            //Change Date Format First
            let dateFormatter = DateFormatter()
            dateFormatter.calendar = Calendar(identifier: .gregorian)
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let strDate = dateFormatter.date(from: self.DataNotInServerTextFieldDict[self.DataNotInServerDescriptionArray[5]]!)
            println_debug(strDate!)
            let dateFormatter1 = DateFormatter()
            dateFormatter1.calendar = Calendar(identifier: .gregorian)
            dateFormatter1.dateFormat = "MM/dd/yyyy"
            let newDate = dateFormatter1.string(from: strDate!)
            self.DataNotInServerTextFieldDict[self.DataNotInServerDescriptionArray[5]] = newDate
            
            parameters  = [ "MeterBillRefNumForSearch":paramString,
                            "SearchType":"0",
                            "LoginDeviceInfo": [
                                "MobileNumber": agentNum,
                                "Msid": msid,
                                "Ostype": 1,
                                "Otp": "",
                                "Simid": str],
                            "EbBillPayInfo" :       [ "DivisionOrStateName":self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Division")]!,
                                                      "Township":self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Township")]!,
                                                      "MeterNumber":self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Number")]!,
                                                      "BillerName":self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Consumer Name (as on Bill)")]!,
                                                      "AccNumber":self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Ledger Number")]!,
                                                      "MeterReadDate":self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Read Date")]!,
                                                      "Tariff":self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Tariff")]!,
                                                      "CustRefNum":self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Computer Code Number")]!,
                                                      "PayFor":self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Pay For")]!,
                                                      "ContactMobileNum":self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number")]!,
                                                      "MeterBillAmount":self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Bill Amount")]!,
                                                      "ServiceFee":self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Service Fees")]!,
                                                      "Remark":self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")]!,
                                                      
                                                      "AdjustPaidAmoun": "",
                                                      "CashBack":"",
                                                      "DivisionId": EBDivisionID,
                                                      "InvoiceNumber":"",
                                                      "LocalTransType":"ELECTRICITY",
                                                      "MeterType":meterTypeParam,
                                                      "OkdollarAccNumber":agentNum,
                                                      "OkdollarAccPwd": pwd ?? "",
                                                      "TownshipId":EBTownshipID
                                ] as [String : Any],
                            ] as [String : Any]
            
        }
        else if DataSourceType == .DataInServerDataSource
        {
            let dateFormatter = DateFormatter()
            dateFormatter.calendar = Calendar(identifier: .gregorian)
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let strDate = dateFormatter.date(from: self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Read Date")]!)
            println_debug(strDate!)
            let dateFormatter1 = DateFormatter()
            dateFormatter1.calendar = Calendar(identifier: .gregorian)
            dateFormatter1.dateFormat = "MM/dd/yyyy"
            let newDate = dateFormatter1.string(from: strDate!)
            self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Read Date")] = newDate
            
            
            parameters  = [ "MeterBillRefNumForSearch":paramString,
                            "SearchType":"0",
                            "LoginDeviceInfo": [
                                "MobileNumber": agentNum,
                                "Msid": msid,
                                "Ostype": 1,
                                "Otp": "",
                                "Simid": str],
                            "EbBillPayInfo" :       [ "DivisionOrStateName":self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Division")]!,
                                                      "Township":self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Township")]!,
                                                      "MeterNumber":self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Number")]!,
                                                      "BillerName":self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Consumer Name (as on Bill)")]!,
                                                      "AccNumber":self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "LedgerNumber")]!,
                                                      "MeterReadDate":self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Read Date")]!,
                                                      "Tariff":self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Tarif Number")]!,
                                                      "CustRefNum":self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Computer Code Number")]!,
                                                      "PayFor":self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Pay For")]!,
                                                      "ContactMobileNum":self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number")]!,
                                                      "MeterBillAmount":self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Bill Amount")]!,
                                                      "ServiceFee":self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Service Fees")]!,
                                                      "Remark":self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")]!,
                                                      
                                                      "AdjustPaidAmoun":"",
                                                      "CashBack":self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Cash Back")]!,
                                                      "DivisionId": EBDivisionID,
                                                      "InvoiceNumber":"",
                                                      "LocalTransType":"ELECTRICITY",
                                                      "MeterType":meterTypeParam,
                                                      "OkdollarAccNumber":agentNum,
                                                      "OkdollarAccPwd": pwd ?? "",
                                                      "TownshipId":EBTownshipID
                                ] as [String : Any],
                            ] as [String : Any]
            
        }
        
        
        println_debug(parameters)
        
        
        // JSON all the things
        let auth = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: customURL!)
        
        do {
            
            println_debug("Requesting Get Bill API Now")
            
            // Set the request content type to JSON
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            // The magic...set the HTTP request method to POST
            request.httpMethod = "POST"
            
            // Add the JSON serialized login data to the body
            request.httpBody = auth
            
            // Create the task that will send our login request (asynchronously)
            let task = session.dataTask(with: request as URLRequest, completionHandler:
            { (data, response, error) -> Void in
                
                if(data == nil)
                {
                    println_debug("Data is nil")
                    DispatchQueue.main.async {
                        progressViewObj.removeProgressView()
                    }
                    
                }
                else
                {
                    do {
                        
                        //println_debug("Call was in do block")
                        let jsonResponse = try? JSONSerialization.jsonObject(with: data!, options:.allowFragments)
                        if let jsonResponse = jsonResponse as? [String: Any] {
                            // if let jsonResponse = jsonResponse as? [String: Any] {
                            //println_debug("\n \n \n \(jsonResponse)")
                            
                            DispatchQueue.main.async {
                                self.GetBillButton?.isHidden = true
                            }
                            
                            if jsonResponse["Code"] as! NSNumber ==  200
                            {
                                
                                let _ : [String : Any] = self.convertStringToJson(ReceivedString: jsonResponse["Data"]! as! String) as! [String : Any]
                                //println_debug(DataStringToJSON)
                                
                                DispatchQueue.main.async {
                                    progressViewObj.removeProgressView()
                                    
                                    self.dismiss(animated: false, completion: nil)
                                    
                                    if MeterOptionType == .myMeterOption
                                    {
                                        self.parentVC_meter.refreshSetup()
                                    }
                                    else
                                    {
                                        self.parentVC_other.refreshSetup()
                                    }
                                }
                            }
                            else
                            {
                                DispatchQueue.main.async {
                                    progressViewObj.removeProgressView()
                                }
                                
                                println_debug("Condition failed")
                                
                            }
                        }
                    }
                }
                
            })
            
            // Start the task on a background thread
            task.resume()
            
        }
    }
    
    func convertStringToJson(ReceivedString : String) -> Any
    {
        let CustomStringData = ReceivedString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue), allowLossyConversion:true)
        
        do {
            let jsonData = try? JSONSerialization.jsonObject(with: CustomStringData!, options: .allowFragments ) as Any
            return jsonData!
        }
    }
    
    
    //MARK: - Table view Delegate & DataSource
    
    func setupTableDataSource()
    {
        
        getBillDataSourceArray = [appDelegate.getlocaLizationLanguage(key: "Division"),
                                  appDelegate.getlocaLizationLanguage(key: "Township"),
                                  appDelegate.getlocaLizationLanguage(key: "Meter Number")
        ]
        
        DataNotInServerDescriptionArray = [ appDelegate.getlocaLizationLanguage(key: "Division"),
                                            appDelegate.getlocaLizationLanguage(key: "Township"),
                                            appDelegate.getlocaLizationLanguage(key: "Meter Number"),
                                            appDelegate.getlocaLizationLanguage(key: "Consumer Name (as on Bill)"),
                                            appDelegate.getlocaLizationLanguage(key: "Ledger Number"),
                                            appDelegate.getlocaLizationLanguage(key: "Meter Read Date"),
                                            appDelegate.getlocaLizationLanguage(key: "Tariff"),
                                            appDelegate.getlocaLizationLanguage(key: "Computer Code Number"),
                                            appDelegate.getlocaLizationLanguage(key: "Pay For"),
                                            appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number"),
                                            appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount"),
                                            appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount") ,
                                            appDelegate.getlocaLizationLanguage(key: "Service Fees"),
                                            appDelegate.getlocaLizationLanguage(key: "Remark")
        ]
        
        DataNotInServerPlaceholderArray = [ appDelegate.getlocaLizationLanguage(key: "Select Division"),
                                            appDelegate.getlocaLizationLanguage(key: "Township"),
                                            appDelegate.getlocaLizationLanguage(key: "Enter Meter Number"),
                                            appDelegate.getlocaLizationLanguage(key: "Enter Consumer Name (as on Bill)"),
                                            appDelegate.getlocaLizationLanguage(key: "Enter Ledger Number"),
                                            appDelegate.getlocaLizationLanguage(key: "Select Meter Read Date"),
                                            appDelegate.getlocaLizationLanguage(key: "Enter Tariff"),
                                            appDelegate.getlocaLizationLanguage(key: "Enter Computer Code Number"),
                                            appDelegate.getlocaLizationLanguage(key: "Select name for this meter"),
                                            appDelegate.getlocaLizationLanguage(key: "Enter Contact Mobile Number"),
                                            appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount"),
                                            appDelegate.getlocaLizationLanguage(key: "Re-enter Bill Amount") ,
                                            appDelegate.getlocaLizationLanguage(key: "Service Fees"),
                                            appDelegate.getlocaLizationLanguage(key: "Remark")
        ]
        
        DataInServerDescriptionArray = [ appDelegate.getlocaLizationLanguage(key: "Division"),
                                         appDelegate.getlocaLizationLanguage(key: "Township"),
                                         appDelegate.getlocaLizationLanguage(key: "Meter Number"),
                                         appDelegate.getlocaLizationLanguage(key: "Pay For"),
                                         appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number"),
                                         appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount"),
                                         appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount"),
                                         appDelegate.getlocaLizationLanguage(key: "Service Fees"),
                                         appDelegate.getlocaLizationLanguage(key: "Remark")
            
        ]
        
        DataInServerPlaceholderArray = [ appDelegate.getlocaLizationLanguage(key: "Select Division"),
                                         appDelegate.getlocaLizationLanguage(key: "Township"),
                                         appDelegate.getlocaLizationLanguage(key: "Enter Meter Number"),
                                         appDelegate.getlocaLizationLanguage(key: "Select name for this meter"),
                                         appDelegate.getlocaLizationLanguage(key: "Enter Contact Mobile Number"),
                                         appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount"),
                                         appDelegate.getlocaLizationLanguage(key: "Re-enter Bill Amount"),
                                         appDelegate.getlocaLizationLanguage(key: "Service Fees"),
                                         appDelegate.getlocaLizationLanguage(key: "Remark")
            
        ]
        
        DataNotInServerImageArray = [#imageLiteral(resourceName: "r_division"), #imageLiteral(resourceName: "township"), #imageLiteral(resourceName: "meternumber"),#imageLiteral(resourceName: "EBNameonTheBill"), #imageLiteral(resourceName: "my_account"), #imageLiteral(resourceName: "meterreaddate"), #imageLiteral(resourceName: "Tariff"),#imageLiteral(resourceName: "Computer Code Number"),#imageLiteral(resourceName: "Pay For"),#imageLiteral(resourceName: "Contact Mobile Number"),#imageLiteral(resourceName: "newpayamount.png"),#imageLiteral(resourceName: "newpayamount"),#imageLiteral(resourceName: "Service Fees"),#imageLiteral(resourceName: "newpayremark.png")]
        
        DataInServerImageArray = [#imageLiteral(resourceName: "r_division"), #imageLiteral(resourceName: "township"), #imageLiteral(resourceName: "meternumber"),#imageLiteral(resourceName: "Pay For"),#imageLiteral(resourceName: "Contact Mobile Number"),#imageLiteral(resourceName: "newpayamount.png"),#imageLiteral(resourceName: "newpayamount.png"),#imageLiteral(resourceName: "Service Fees"),#imageLiteral(resourceName: "newpayremark.png")]
        
        DataNotInServerTextFieldDict  = [ appDelegate.getlocaLizationLanguage(key: "Division") : "",
                                          appDelegate.getlocaLizationLanguage(key: "Township") : "",
                                          appDelegate.getlocaLizationLanguage(key: "Meter Number") : "",
                                          appDelegate.getlocaLizationLanguage(key: "Consumer Name (as on Bill)") : "",
                                          appDelegate.getlocaLizationLanguage(key: "Ledger Number") : "",
                                          appDelegate.getlocaLizationLanguage(key: "Meter Read Date") : "",
                                          appDelegate.getlocaLizationLanguage(key: "Tariff" ): "",
                                          appDelegate.getlocaLizationLanguage(key: "Computer Code Number") : "",
                                          appDelegate.getlocaLizationLanguage(key: "Pay For") : "",
                                          appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number") : "09",
                                          appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount") : "",
                                          appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount") : "",
                                          appDelegate.getlocaLizationLanguage(key: "Service Fees") : "0",
                                          appDelegate.getlocaLizationLanguage(key: "Remark") : "",
                                          appDelegate.getlocaLizationLanguage(key: "Bill Amount") : "",
                                          appDelegate.getlocaLizationLanguage(key: "Cash Back") : ""
        ]
        
        DataInServerTextFieldDict  =       [  appDelegate.getlocaLizationLanguage(key: "Division") : "",
                                              appDelegate.getlocaLizationLanguage(key: "Township") : "",
                                              appDelegate.getlocaLizationLanguage(key: "Meter Number") : "",
                                              appDelegate.getlocaLizationLanguage(key: "Pay For") : "",
                                              appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number") : "09",
                                              appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount") : "",
                                              appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount") : "",
                                              appDelegate.getlocaLizationLanguage(key: "Service Fees") : "0",
                                              appDelegate.getlocaLizationLanguage(key: "Remark") : "",
                                              appDelegate.getlocaLizationLanguage(key: "Computer Code Number") : "",
                                              appDelegate.getlocaLizationLanguage(key: "Bill Amount") : "",
                                              appDelegate.getlocaLizationLanguage(key: "Cash Back") : "",
                                              appDelegate.getlocaLizationLanguage(key: "Consumer Name (as on Bill)") : "",
                                              appDelegate.getlocaLizationLanguage(key: "Tarif Number") : "",
                                              appDelegate.getlocaLizationLanguage(key: "LedgerNumber") : "",
                                              appDelegate.getlocaLizationLanguage(key: "Meter Read Date") : ""
        ]
    }
    
    fileprivate func setPlaceholderForCell(cell : GetElectricityBillTableViewCell, index : Int) {
        if cell.textFieldOutlet.text != "" || cell.textFieldOutlet.text != "09" {
            let placeHolder = self.DataNotInServerPlaceholderArray[index]
            if placeHolder.contains(find: "Enter") || placeHolder.contains(find: "Select") {
                if let index = placeHolder.range(of: " ")?.lowerBound {
                    let substring = placeHolder[index...]
                    let string = String(substring)
                    if string == "Remark" {
                        cell.textFieldOutlet.placeholder = string + "s"
                    }
                  else if string == " name for this meter" {
                      if cell.textFieldOutlet.text?.count == 0
                      {
                        cell.textFieldOutlet.placeholder = "Select name for this meter".localized
                      }
                      else
                      {
                        cell.textFieldOutlet.placeholder = "Name for this meter".localized
                      }
                  }
                    else if string == " Consumer Name (as on Bill)" {
                      if cell.textFieldOutlet.text?.count == 0
                      {
                        cell.textFieldOutlet.placeholder = "Enter Consumer Name (as on Bill)".localized
                      }
                      else
                      {
                        cell.textFieldOutlet.placeholder = string
                      }
                    }
                    else if string == " Computer Code Number" {
                      if cell.textFieldOutlet.text?.count == 0
                      {
                        cell.textFieldOutlet.placeholder = "Enter Computer Code Number".localized
                      }
                      else
                      {
                        cell.textFieldOutlet.placeholder = string
                      }
                    }
                    else if string == "Re-enter Bill Amount" {
                        cell.textFieldOutlet.placeholder = "Confirm Bill Amount"
                    }  else {
                        cell.textFieldOutlet.placeholder = string
                    }
                } else {
                    cell.textFieldOutlet.placeholder = placeHolder.localized
                }
                
            } else {
                cell.textFieldOutlet.placeholder = placeHolder.localized
            }
        } else {
            cell.textFieldOutlet.placeholder = self.DataNotInServerPlaceholderArray[index]
        }
        if self.DataNotInServerPlaceholderArray[index] == "Service Fees" {
            cell.textFieldOutlet.textColor = UIColor.gray
        } else {
            cell.textFieldOutlet.textColor = UIColor.black
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
        let cell : GetElectricityBillTableViewCell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! GetElectricityBillTableViewCell
        cell.delegate = self
        cell.operatorLabelOutlet.isHidden = true
        cell.selectContactsBtn.isHidden = true
        cell.textFieldOutlet.myDelegate = self
        
        //Check Data Source 
        
        switch self.DataSourceType
        {
            
        case .GetBillDataSource :
            
            if indexPath.section == 0
            {
                if self.getBillDataSourceArray.count > indexPath.row {
                    cell.DescriptionLabelOutlet?.text = self.getBillDataSourceArray[indexPath.row]
                }
                cell.imageViewOutlet.image = self.DataNotInServerImageArray[indexPath.row]
                if indexPath.row == 0
                {
                    cell.textFieldOutlet.isUserInteractionEnabled = false
                    if self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Division")] == "" {
                        cell.textFieldOutlet.placeholder = appDelegate.getlocaLizationLanguage(key: "Select Division")
                    } else {
                        cell.textFieldOutlet.placeholder = appDelegate.getlocaLizationLanguage(key: "Division")
                    }
                    cell.textFieldOutlet.text = self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Division")]
                }
                if indexPath.row == 1
                {
                    cell.textFieldOutlet.isUserInteractionEnabled = false
                    if self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Township")] == "" {
                        cell.textFieldOutlet.placeholder = appDelegate.getlocaLizationLanguage(key: "Select Township")
                    } else {
                        cell.textFieldOutlet.placeholder = appDelegate.getlocaLizationLanguage(key: "Township")
                    }
                    cell.textFieldOutlet.text = self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Township")]
                }
                
                if indexPath.row == 2
                {
                    cell.textFieldOutlet.placeholder = appDelegate.getlocaLizationLanguage(key: "Enter Meter Number")
                    cell.clearButton.isHidden = false
                    cell.textFieldOutlet.isUserInteractionEnabled = true
                    cell.textFieldOutlet.tintColor = UIColor.init(hex: "087023")
                    if !computerFieldEnable {
                        cell.textFieldOutlet.becomeFirstResponder()
                    }
                    cell.textFieldOutlet.text = self.DataNotInServerTextFieldDict["Meter Number"]
                }
                
            }
            else
            {
                cell.DescriptionLabelOutlet.text = appDelegate.getlocaLizationLanguage(key: "Computer Code Number")
                cell.textFieldOutlet.placeholder = appDelegate.getlocaLizationLanguage(key: "Enter Computer Code Number")
                cell.textFieldOutlet.text = ""
                cell.imageViewOutlet.image = #imageLiteral(resourceName: "Computer Code Number")
                cell.dropDownBtnOutlet.isHidden = true
                cell.textFieldOutlet.tag = 9080
                cell.textFieldOutlet.isUserInteractionEnabled = true
                cell.textFieldOutlet.tintColor = UIColor.init(hex: "087023")
            }
            
        case .DataNotInServerDataSource :
            if indexPath.section == 0
            {
                cell.DescriptionLabelOutlet?.text = self.DataNotInServerDescriptionArray[indexPath.row]
                cell.imageViewOutlet.image = self.DataNotInServerImageArray[indexPath.row]
                cell.textFieldOutlet.text = setTextFieldString(LabelText: cell.DescriptionLabelOutlet.text!)
                self.setPlaceholderForCell(cell: cell, index: indexPath.row)
                cell.textFieldOutlet.isUserInteractionEnabled = true
                cell.textFieldOutlet.tintColor = UIColor.init(hex: "087023")
            } else {
                cell.DescriptionLabelOutlet.text = appDelegate.getlocaLizationLanguage(key: "Computer Code Number")
                cell.textFieldOutlet.placeholder = appDelegate.getlocaLizationLanguage(key: "Enter Computer Code Number")
                cell.textFieldOutlet.text = ""
                cell.imageViewOutlet.image = #imageLiteral(resourceName: "Computer Code Number")
                cell.dropDownBtnOutlet.isHidden = true
                cell.textFieldOutlet.tag = 9080
                cell.textFieldOutlet.isUserInteractionEnabled = true
                cell.textFieldOutlet.tintColor = UIColor.init(hex: "087023")
            }
            
        case .DataInServerDataSource :
            cell.DescriptionLabelOutlet?.text = self.DataInServerDescriptionArray[indexPath.row]
            cell.imageViewOutlet.image = self.DataInServerImageArray[indexPath.row]
            cell.textFieldOutlet.text = setTextFieldString(LabelText: cell.DescriptionLabelOutlet.text!)
            cell.textFieldOutlet.placeholder = self.DataInServerPlaceholderArray[indexPath.row]
            cell.textFieldOutlet.isUserInteractionEnabled = true
            cell.textFieldOutlet.tintColor = UIColor.init(hex: "087023")
        }
        
        
        //Set Tags
        if indexPath.section == 0
        {
            cell.textFieldOutlet.tag = setTag(LabelText: cell.DescriptionLabelOutlet.text!)
            
            if cell.textFieldOutlet.tag == 1002 || cell.textFieldOutlet.tag == 1003 || cell.textFieldOutlet.tag == 1009 || cell.textFieldOutlet.tag == 2002 || cell.textFieldOutlet.tag == 2004
            {
                if (cell.textFieldOutlet.text?.count)! > 0
                {
                    cell.clearButton.isHidden = false
                }
                else
                {
                    cell.clearButton.isHidden = true
                }
            } else
            {
                cell.clearButton.isHidden = true
            }
            
            if DataSourceType == .DataInServerDataSource {
                if cell.textFieldOutlet.tag == 2002 {
                    cell.clearButton.isHidden = true
                    cell.textFieldOutlet.isUserInteractionEnabled = false
                }
            }
            
            if cell.textFieldOutlet.tag == 1009 || cell.textFieldOutlet.tag == 2004 //Data Not In Server
            {
                if (cell.textFieldOutlet.text?.count)! > 2 {
                    cell.clearButton.isHidden = false 
                } else {
                    cell.clearButton.isHidden = true
                }
                cell.operatorLabelOutlet.isHidden = false
                cell.selectContactsBtn.isHidden = false
            }
            else
            {
                cell.operatorLabelOutlet.isHidden = true
                cell.selectContactsBtn.isHidden = true
            }
            if cell.textFieldOutlet.tag == 1003
            {
                cell.textFieldOutlet.leftViewMode = .always
            }
            else
            {
                cell.textFieldOutlet.leftViewMode = .never
            }
        }
        
        //Show Drop Down Button on Only First two rows of first section
        if (indexPath.section == 0) && (indexPath.row == 0 || indexPath.row == 1)
        {
            cell.dropDownBtnOutlet.isHidden = false
        }
        else
        {
            cell.dropDownBtnOutlet.isHidden = true
        }
        
        if cell.textFieldOutlet.tag == 1005 || cell.textFieldOutlet.tag == 1008 || cell.textFieldOutlet.tag == 2003
        {
            cell.blackDropDownBtnOutlet.isHidden = false
            
        }
        else
        {
            cell.blackDropDownBtnOutlet.isHidden = true
        }
        electricityCellRef = cell
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch DataSourceType
        {
            
        case .GetBillDataSource :   if section == 0
        {
            return  rowCountArray.count
        }
        else
        {
            return sectionUpdate
            }
            
        case .DataNotInServerDataSource :    if section == 0
        {
            return  rowCountArray.count
        }
        else
        {
            return sectionUpdate
            }
            
        case .DataInServerDataSource :      return  rowCountArray.count
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        switch DataSourceType
        {
            
        case .GetBillDataSource :   if rowCountArray.count == 3
        {
            return sectionCountArray.count
        }
        else
        {
            return sectionCountArray.count
            }
            
        case .DataNotInServerDataSource :   if rowCountArray.count == 3
        {
            return sectionCountArray.count
        }
        else
        {
            return sectionCountArray.count
            }
        case .DataInServerDataSource :      return  1
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1
        {
            return appDelegate.getlocaLizationLanguage(key: "(OR)")
        }
        else
        {
            return ""
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.textLabel?.textAlignment = .center
            if section == 1 {
                headerView.textLabel?.textColor = UIColor.gray
            } else {
                headerView.textLabel?.textColor = UIColor.init(red: 28/255.0, green: 45/255.0, blue: 153/255.0, alpha: 1)
            }
            headerView.textLabel?.font = UIFont(name: appFont, size: 17.0)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == 1
        {
            return 45
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 && indexPath.section == 0
        {
            self.removeNameFiledOptionsView()
            DispatchQueue.main.async {
                self.view.endEditing(true)
                self.GetBillButton?.isHidden = true
                self.SubmitButton?.isHidden = true
                self.btnOrImage(showImage: false)
                //self.topBtnHeightConstraint.constant = 0
                self.toplayoutTableConstraint.constant = 0
            }
            DivisionFieldActive = true
            
            let DivisonVCObj = self.storyboard?.instantiateViewController(withIdentifier: "SelectDivision_TownshipViewControllerSID") as! SelectDivision_TownshipViewController
            DivisonVCObj.delegate = self
            let navController = UINavigationController(rootViewController: DivisonVCObj)
            navController.isNavigationBarHidden = true
            navController.modalPresentationStyle = .fullScreen
            self.present(navController, animated: true, completion: nil)
            
        }
        else if indexPath.row == 1 && indexPath.section == 0
        {
            self.removeNameFiledOptionsView()
            DispatchQueue.main.async {
                self.view.endEditing(true)
                self.GetBillButton?.isHidden = true
                self.SubmitButton?.isHidden = true
                self.btnOrImage(showImage: false)
            }
            
            DivisionFieldActive = false
            
            let TownshipVCObj = self.storyboard?.instantiateViewController(withIdentifier: "SelectDivision_TownshipViewControllerSID") as! SelectDivision_TownshipViewController
            TownshipVCObj.delegate = self
            let navController = UINavigationController(rootViewController: TownshipVCObj)
            navController.isNavigationBarHidden = true
            navController.modalPresentationStyle = .fullScreen
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    func showNextRow()
    {
        self.rowCountArray.append("")
        self.tableViewOutlet.beginUpdates()
        self.tableViewOutlet.insertRows(at: [IndexPath(row: self.rowCountArray.count-1, section: 0)], with: .automatic)
        self.tableViewOutlet.endUpdates()
    }
    
    func RemoveLastRow()
    {
        self.rowCountArray.removeLast()
        self.tableViewOutlet.beginUpdates()
        self.tableViewOutlet.deleteRows(at: [IndexPath(row: self.rowCountArray.count, section: 0)], with: .automatic)
        self.tableViewOutlet.endUpdates()
    }
    
    func scrollToLastRow()
    {
        let indexPath = IndexPath(row: self.rowCountArray.count-2, section: 0)
        self.tableViewOutlet.scrollToRow(at: indexPath, at: .bottom, animated: false)
    }
    
    //MARK: - Protocol Delegate Method
    
    func userSelectedDivision(DivisionString: String) {
        DataSourceType = .GetBillDataSource
        self.removeNameFiledOptionsView()
        extractTownshipData()
        
        //println_debug(DivisionString, TownshipString)
        
        DataNotInServerTextFieldDict.removeAll()
        DataNotInServerImageArray.removeAll()
        DataNotInServerDescriptionArray.removeAll()
        
        DataInServerTextFieldDict.removeAll()
        DataInServerDescriptionArray.removeAll()
        DataInServerImageArray.removeAll()
        
        setupTableDataSource()
        
        self.rowCountArray.removeAll()
        self.sectionCountArray.removeAll()
        
        self.sectionCountArray.append("")
        
        self.rowCountArray.append("")
        self.rowCountArray.append("")
        
        self.tableViewOutlet.tableFooterView = UIView()
        
        self.tableViewOutlet.reloadData()
        //println_debug("Show Hide value here \(showHideComputerCodeField ) ")
        
        let DivIndexPath = NSIndexPath.init(row: 0, section: 0)
        let TownshipIndexPath = NSIndexPath.init(row: 1, section: 0)
        
        electricityCellRef  = tableViewOutlet.cellForRow(at: DivIndexPath as IndexPath) as? GetElectricityBillTableViewCell
        electricityCellRef?.textFieldOutlet.text = DivisionString
        
        electricityCellRef  = tableViewOutlet.cellForRow(at: TownshipIndexPath as IndexPath) as? GetElectricityBillTableViewCell
        
        DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[0]] = DivisionString
        
        DataInServerTextFieldDict[DataInServerDescriptionArray[0]] = DivisionString
        
        DispatchQueue.main.async {
            self.tableViewOutlet.reloadData()
        }
    }
    
    func userSelectedData(DivisionString: String, TownshipString: String)
    {
        DataSourceType = .GetBillDataSource
        self.removeNameFiledOptionsView()

        extractTownshipData()
        
        //println_debug(DivisionString, TownshipString)
        
        DataNotInServerTextFieldDict.removeAll()
        DataNotInServerImageArray.removeAll()
        DataNotInServerDescriptionArray.removeAll()
        
        DataInServerTextFieldDict.removeAll()
        DataInServerDescriptionArray.removeAll()
        DataInServerImageArray.removeAll()
        
        setupTableDataSource()
        
        self.rowCountArray.removeAll()
        self.sectionCountArray.removeAll()
        
        self.sectionCountArray.append("")
        
        self.rowCountArray.append("")
        self.rowCountArray.append("")
        self.rowCountArray.append("")
        
        self.tableViewOutlet.tableFooterView = UIView()
        
        self.tableViewOutlet.reloadData()
        //println_debug("Show Hide value here \(showHideComputerCodeField ) ")
        
        if self.showHideComputerCodeField == 1
        {
            self.sectionCountArray.append("")
            
            let indexPath = IndexPath(row: 0, section: 1)
            
            sectionUpdate = 1
            if self.sectionCountArray.count == 1{
                self.tableViewOutlet.beginUpdates()
                self.tableViewOutlet.insertSections(IndexSet.init(integer: .init(1)), with: .automatic)
                self.tableViewOutlet.insertRows(at: [indexPath], with: .automatic)
                self.tableViewOutlet.endUpdates()
            }
            
        }
        
        let DivIndexPath = NSIndexPath.init(row: 0, section: 0)
        let TownshipIndexPath = NSIndexPath.init(row: 1, section: 0)
        let MeterrNoIndexPath = NSIndexPath.init(row: 2, section: 0)
        
        electricityCellRef  = tableViewOutlet.cellForRow(at: DivIndexPath as IndexPath) as? GetElectricityBillTableViewCell
        electricityCellRef?.textFieldOutlet.text = DivisionString
        
        electricityCellRef  = tableViewOutlet.cellForRow(at: TownshipIndexPath as IndexPath) as? GetElectricityBillTableViewCell
        electricityCellRef?.textFieldOutlet.text = TownshipString
        
        electricityCellRef  = tableViewOutlet.cellForRow(at: MeterrNoIndexPath as IndexPath) as? GetElectricityBillTableViewCell
        electricityCellRef?.textFieldOutlet.text = ""
        electricityCellRef?.textFieldOutlet.becomeFirstResponder()
        
        DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[0]] = DivisionString
        DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[1]] = TownshipString
        
        DataInServerTextFieldDict[DataInServerDescriptionArray[0]] = DivisionString
        DataInServerTextFieldDict[DataInServerDescriptionArray[1]] = TownshipString
        
        DispatchQueue.main.async {
            self.tableViewOutlet.reloadData()
        }
    }
    
    
    func extractTownshipData()
    {
        TownshipImageArray.removeAll()
        
        for element in TownshipResultStringToJSON as! Array<AnyObject> {
            println_debug(element)
            
            var TownshipName = ""
            
            if appDelegate.currentLanguage == "my"
            {
                TownshipName = element["bUnicode"]! as! String
            } else if appDelegate.currentLanguage == "uni"
            {
                TownshipName = element["bName"]! as? String ?? ""
            }
            else
            {
                TownshipName = element["eName"]! as? String ?? ""
            }
            
            if TownshipName == SelectedTownship
            {
                //println_debug("Match Found")
                TownshipImageArray.append(element["meterNumberImg"]! as! String)
                TownshipImageArray.append(element["consumerNameImg"]! as! String)
                TownshipImageArray.append(element["ledgerNumberImg"]! as! String)
                TownshipImageArray.append(element["meterReadDateImg"]! as! String)
                TownshipImageArray.append(element["tariffImg"]! as! String)
                TownshipImageArray.append(element["computerCodeImg"]! as! String)
                TownshipImageArray.append(element["totalAmountImg"]! as! String)
                
                EBTownshipID = element["id"]! as! String
                
                computerCodeLengthForSelectedTownship = element["computerCodeLength"]! as! Int
                
                showHideComputerCodeField = element["isRefNum"]! as! Int
                
                
                return
                
            }
            
            
        }
    }
    
    //MARK: - Text Field Delegeate methods
    
    
    func setTag(LabelText : String) -> Int
    {
        println_debug(LabelText)
        
        
        if DataSourceType == .DataNotInServerDataSource || DataSourceType == .GetBillDataSource
        {
            switch LabelText
            {
            case appDelegate.getlocaLizationLanguage(key: "Division") : return 1000
                
            case appDelegate.getlocaLizationLanguage(key: "Township") : return 1001
                
            case appDelegate.getlocaLizationLanguage(key: "Meter Number") : return 1002
                
            case appDelegate.getlocaLizationLanguage(key: "Consumer Name (as on Bill)") : return 1003
                
            case appDelegate.getlocaLizationLanguage(key: "Ledger Number") : return 1004
                
            case appDelegate.getlocaLizationLanguage(key: "Meter Read Date") : return 1005
                
            case appDelegate.getlocaLizationLanguage(key: "Tariff") : return 1006
                
            case appDelegate.getlocaLizationLanguage(key: "Computer Code Number") : return 1007
                
            case appDelegate.getlocaLizationLanguage(key: "Pay For") : return 1008
                
            case appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number") : return 1009
                
            case appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount") : return 1010
                
            case appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount") : return 1011
                
            case appDelegate.getlocaLizationLanguage(key: "Service Fees") : return 1012
                
            case appDelegate.getlocaLizationLanguage(key: "Remark") : return 1013
                
            default: return 945454
            }
        }
        else if DataSourceType == .DataInServerDataSource
        {
            switch LabelText
            {
                
            case appDelegate.getlocaLizationLanguage(key: "Division") : return 2000
                
            case appDelegate.getlocaLizationLanguage(key: "Township") : return 2001
                
            case appDelegate.getlocaLizationLanguage(key: "Meter Number") : return 2002
                
            case appDelegate.getlocaLizationLanguage(key: "Computer Code Number") : return 2002
                
            case appDelegate.getlocaLizationLanguage(key: "Pay For") : return 2003
                
            case appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number") : return 2004
                
            case appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount") : return 2005
                
            case appDelegate.getlocaLizationLanguage(key: "Bill Amount") : return 2005
                
            case appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount") : return 2006
                
            case appDelegate.getlocaLizationLanguage(key: "Service Fees") : return 2007
                
            case appDelegate.getlocaLizationLanguage(key: "Remark") : return 2008
                
            default: return 945454
            }
        }
        
        return 0
        
        
    }
    
    
    func setTextFieldString(LabelText : String) -> String
    {
        println_debug(LabelText)
        
        if DataSourceType == .DataNotInServerDataSource
        {
            switch LabelText
            {
                
                
            case appDelegate.getlocaLizationLanguage(key: "Division") : return DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[0]]!
                
            case appDelegate.getlocaLizationLanguage(key: "Township") : return DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[1]]!
                
            case appDelegate.getlocaLizationLanguage(key: "Meter Number") : return DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[2]]!
                
            case appDelegate.getlocaLizationLanguage(key: "Consumer Name (as on Bill)") : return DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[3]]!
                
            case appDelegate.getlocaLizationLanguage(key: "Ledger Number") : return DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[4]]!
                
            case appDelegate.getlocaLizationLanguage(key: "Meter Read Date") : return DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[5]]!
                
            case appDelegate.getlocaLizationLanguage(key: "Tariff") : return  DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[6]]!
                
            case appDelegate.getlocaLizationLanguage(key: "Computer Code Number") : return DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[7]]!
                
            case appDelegate.getlocaLizationLanguage(key: "Pay For") : return DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[8]]!
                
            case appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number") : return DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[9]]!
                
            case appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount") : return DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[10]]!
                
            case appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount") : return DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[11]]!
                
            case appDelegate.getlocaLizationLanguage(key: "Service Fees") : return DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[12]]!
                
            case appDelegate.getlocaLizationLanguage(key: "Remark") : return DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[13]]!
                
            default: return ""
            }
            
        }
        else if DataSourceType == .DataInServerDataSource
        {
            switch LabelText
            {
                
                
            case appDelegate.getlocaLizationLanguage(key: "Division") : return DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Division")]!
                
            case appDelegate.getlocaLizationLanguage(key: "Township") : return DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Township")]!
                
            case appDelegate.getlocaLizationLanguage(key: "Meter Number") :  return DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Number")]!
                
            case appDelegate.getlocaLizationLanguage(key: "Computer Code Number") : return DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Computer Code Number")]!
                
            case appDelegate.getlocaLizationLanguage(key: "Pay For") : return DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Pay For")]!
                
            case appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number") : return DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number")]!
                
            case appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount") : return DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount")]!
                
            case appDelegate.getlocaLizationLanguage(key: "Bill Amount"): return DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Bill Amount")]!
                
            case appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount") : return DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount")]!
                
            case appDelegate.getlocaLizationLanguage(key: "Service Fees") : return DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Service Fees")]!
                
            case appDelegate.getlocaLizationLanguage(key: "Remark") : return DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")]!
                
            default: return ""
            }
            
        }
        
        return ""
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 1002 {
            if range.location == 0
            {
                if string >= "A" && string <= "Z" || string >= "a" && string <= "z"
                {
                    return true
                }
//                else if string >= "a" && string <= "z" {
//                    self.showErrorAlert(errMessage: "Please enter Meter Number in Capital Letters".localized)
//                    return false
//                }
                else {
                    if string == ""
                    {
                        if range.length > 1
                        {
                            if let index1 = textField.text?.index((textField.text?.startIndex)!, offsetBy: range.length)
                            {//will call succ 2 times
                                if let lastChar: Character = (textField.text?[index1]) //now we can index!
                                {
                                    println_debug(lastChar)
                                    if lastChar == " "
                                    {
                                        spaceIsAvail = true
                                    }
                                    
                                    return true
                                }
                            }
                        }
                        else {
                            return true
                        }
                    }
                    return true
                }
            }
//            else if string >= "a" && string <= "z" {
//                self.showErrorAlert(errMessage: "Please enter Meter Number in Capital Letters".localized)
//                return false
//            }
            else
            {
                //textField.text = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
                
                println_debug(range)
                if string != "" {
                    if range.location > 1 {
                        if let index1 = textField.text?.index((textField.text?.startIndex)!, offsetBy: range.location - 1)
                        {//will call succ 2 times
                            if let lastChar: Character = (textField.text?[index1]) //now we can index!
                            {
                                if lastChar == " " && string == " "
                                {
                                    return false
                                }
                            }
                        }
                        
                        if (textField.text?.count)! > range.location {
                            if let index1 = textField.text?.index((textField.text?.startIndex)!, offsetBy: range.location)
                            {//will call succ 2 times
                                if let lastChar: Character = (textField.text?[index1]) //now we can index!
                                {
                                    if lastChar == " " && string == " "
                                    {
                                        return false
                                    }
                                }
                            }
                        }
                    } else {
                        textField.text = textField.text?.filter({ (char) -> Bool in
                            return char != " "
                        })
                        textField.text = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
                    }
                }
                else
                {
                    if let index1 = textField.text?.index((textField.text?.startIndex)!, offsetBy: range.location - 1)
                    {//will call succ 2 times
                        if let lastChar: Character = (textField.text?[index1]) //now we can index!
                        {
                            if lastChar == " "
                            {
                                spaceIsAvailMid = true
                                pos = range.location - 1
                            }
                            
                            return true
                        }
                    }
                }
                
                
            }
            return true
        } else {
            if range.location == 0 {
                if string == " " {
                    return false
                } else if string.containsEmoji == true {
                    return false
                }
            }
        }
        
        if textField.tag == 1007 || textField.tag == 2002 || textField.tag == 9080 {
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            
            let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: InternationalTopupConstants.acceptableCharacters.mobileNumber).inverted
            let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
            
            if (string == filteredSet || isBackSpace == -92) {
                return true
            } else {
                return false
            }
        }
        
        
        if textField.tag == 1009
        {

            
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            
            let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: InternationalTopupConstants.acceptableCharacters.mobileNumber).inverted
            let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
            
            
            if (isBackSpace == -92) {
                let textString = String((textField.text?.dropLast())!)
                if textString == "09" {
                    textField.text = commonPrefixMobileNumber
                    self.deleteBottomRows(AfterRowIndex: 9)
                    return false
                }
            }
            
            if (string != filteredSet) {
                return false
            }
            
            let textString = textField.text! + string
            if (textString.count) > 1 {
                textField.placeholder = "Contact Mobile Number".localized
                if let textField = textField as? FloatLabelTextField
                {
                    textField.titleActiveTextColour = UIColor.black
                }
            } else {
                textField.placeholder = "Enter Contact Mobile Number".localized
            }
            
            if range.location == 0 || range.location == 1 {
                let newPosition = textField.endOfDocument
                textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
                return false
            }
            
        }
        
        if textField.tag == 1010 || textField.tag == 1011 {
            let newPosition = textField.endOfDocument
            textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
        }
        
        return true
        
    }
    
    
    
    func meterNoFieldModified()
    {
        //println_debug("Meter No Modified")
        
        var arrIndexPath = [IndexPath]()
        
        if self.rowCountArray.count > 3 {
            for count in 3 ... self.rowCountArray.count - 1 {
                let indexPath = IndexPath(row: count, section: 0)
                arrIndexPath.append(indexPath)
            }
        }
        
        if self.sectionCountArray.count == 1 {
           self.sectionCountArray.append("")
        }
        self.rowCountArray.removeAll()
        self.rowCountArray.append("")
        self.rowCountArray.append("")
        self.rowCountArray.append("")
        
        tableViewOutlet.beginUpdates()
        tableViewOutlet.deleteRows(at: arrIndexPath, with: .automatic)
        tableViewOutlet.endUpdates()
        
        DataSourceType = DataSourceOptions.GetBillDataSource
        
        self.SubmitButton?.frame.origin.y = self.view.frame.height - 50
        self.view.endEditing(true)
        
        SubmitButton?.isHidden = true
        self.tableViewOutlet.tableFooterView = nil
        
        
        self.DataNotInServerTextFieldDict.removeAll()
        self.DataInServerTextFieldDict.removeAll()
        
        //        self.tableViewOutlet.reloadData()
        
        setupTableDataSource()
        
        DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Division")] = SelectedDivision
        DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Township")] = SelectedTownship
        
        DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Division")] = SelectedDivision
        DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Township")] = SelectedTownship
        
        
        DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Number")] = ""
        DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Consumer Name (as on Bill)")] = ""
        DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Ledger Number")] = ""
        DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Read Date")] = ""
        DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Tariff")] = ""
        DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Pay For")] = ""
        DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number")] = commonPrefixMobileNumber
        DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount")] = ""
        DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount")] = ""
        DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")] = ""
        DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Bill Amount")] = ""
        
        MeterBillAmountParam = ""
        AdjustAmountParam = ""
        
        MeterNoString = ""
        computerCodeString = ""
        
        tableViewOutlet.tableFooterView = UIView()
        
        self.tableViewOutlet.reloadData()
        
        let indexPath = IndexPath(row: 2, section: 0)
        let cell = self.tableViewOutlet.cellForRow(at: indexPath) as? GetElectricityBillTableViewCell
        cell?.textFieldOutlet.becomeFirstResponder()
        
        
    }
    
    func validateNubersAndCharacterforElectricityMeterNumber(_ str: String) -> Bool
    {
        let allowed_CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789()-/_. "
        let cs = CharacterSet(charactersIn: allowed_CHARACTERS).inverted
        let filtered: String = (str.components(separatedBy: cs) as NSArray).componentsJoined(by: "")
        return (str == filtered)
    }
    
    func validateNubersAndCharacterforNameOnTheBill(_ str: String) -> Bool
    {
        let allowed_CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ်   ိ  ္ ့  ံ ျ     ု  ဳ   ူ  ဴ  း  ၚ ွ  ီ ြ  ၤ   ဲ    ွ်   ၽႊ  ႏ  ၽြ    ႊ  ႈ  ဥ  ဧ ၿ   ၾ      ၌ ဋ ႑ ဍ   ၨ    ၳ  ၡ    ႅ   ၻ  ဉ    ဎ  ၺ  ႎ   ႍ `ါ  ႄ      ၶ    ၦ    ၱ   ၷ  ၼ  ဤ   ၸ    ၠ ၍ ႆ  ၥ ၮ   ၎ ဩ   ႀ      ဦ   ၢ႐ ဪ  ႁ    ႂ     ၯ  ၩ ၏ ာ"
        let cs = CharacterSet(charactersIn: allowed_CHARACTERS).inverted
        let filtered: String = (str.components(separatedBy: cs) as NSArray).componentsJoined(by: "")
        return (str == filtered)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    @IBAction func textFieldDidChange(_ sender: UITextField)
    {
        activeField = sender
        
        if DataSourceType == .GetBillDataSource || DataSourceType == .DataNotInServerDataSource
        {
            
            switch sender.tag
            {
                
            case 1000:          println_debug("call was in text Field did change")
            
            DispatchQueue.main.async {
                self.view.endEditing(true)
                self.GetBillButton?.isHidden = true
                self.SubmitButton?.isHidden = true
            }
            
            self.btnOrImage(showImage: false)
            
                
            case 1001:          println_debug("call was in text Field did change")
            
            DispatchQueue.main.async {
                self.view.endEditing(true)
                self.btnOrImage(showImage: false)
                self.GetBillButton?.isHidden = true
                self.SubmitButton?.isHidden = true
                
            }
            
            DivisionFieldActive = false
                
            case 1002   :   sender.keyboardType = .default
            sender.autocapitalizationType = .words
            sender.autocapitalizationType = .allCharacters
            
            if spaceIsAvail
            {
                sender.text?.removeFirst()
                spaceIsAvail = false
            }
            
            if spaceIsAvailMid
            {
                spaceIsAvailMid = false
                let index = sender.text?.index((sender.text?.startIndex)!, offsetBy: pos)
                sender.text?.remove(at: index!)
            }
                
            
            if (sender.text?.count)! > 0 {
                sender.placeholder = "Meter Number".localized
                if let textField = sender as? FloatLabelTextField
                {
                    textField.titleActiveTextColour = UIColor.black
                }
            } else {
                sender.placeholder = "Enter Meter Number".localized
            }
            
            let url = URL(string: TownshipImageArray[0])!
            self.downLoadImage(imageview1: TopImageViewOutlet, url: url)
            
            self.btnOrImage(showImage: true)
            
            let customIndex = NSIndexPath.init(row: 2, section: 0)
            let electricityCellRef1  = self.tableViewOutlet.cellForRow(at: customIndex as IndexPath) as? GetElectricityBillTableViewCell
            electricityCellRef1?.clearButton.isHidden = false
            let str = sender.text!
            if str.count > 20 {
                sender.text = String(sender.text!.dropLast())
                return
            }
            
            if !validateNubersAndCharacterforElectricityMeterNumber(str)
            {
            
                
                GetBillButton?.isHidden = true
                if sender.text!.count > 0 {
                    let str: String = sender.text!
                    sender.text = String(str.dropLast())
                }
                
                return
                
            }
            
            //Get Bill API has been requested once.
            if MeterVsComputerCodeType == .ComputerCodeNo
            {
                if  self.rowCountArray.count == 3
                {
                    if (sender.text?.count)! >= 3
                    {
                        //GetBillButton?.isHidden = false
                        
                        if showHideComputerCodeField == 1{
                            
                            sectionUpdate = 0
                            let indexPath = IndexPath(row: 0, section: 1)
                            if sectionCountArray.count == 2 {
                                sectionCountArray.removeLast()
                                self.tableViewOutlet.beginUpdates()
                                self.tableViewOutlet.deleteRows(at: [indexPath], with: .automatic)
                                self.tableViewOutlet.deleteSections(IndexSet.init(integer: .init(1)), with: .automatic)
                                self.tableViewOutlet.endUpdates()
                                
                            }
                            
                        }
                        showNextRow()
                        
                    }
                    
                    MeterNoString = sender.text!
                    DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[2]] = sender.text!
                    
                }
                else if  self.rowCountArray.count == 4
                {
                    if (sender.text?.count)! < 3
                    {
                        GetBillButton?.isHidden = true
                        electricityCellRef1?.clearButton.isHidden = false
                        
                        if showHideComputerCodeField == 1 {
                            
                            
                            let indexPath = IndexPath(row: 0, section: 1)
                            
                            sectionUpdate = 1
                            if self.sectionCountArray.count == 1 //, (sender.text?.characters.count)! == 2
                            {
                                self.sectionCountArray.append("")
                                
                                self.tableViewOutlet.beginUpdates()
                                self.tableViewOutlet.insertSections(IndexSet.init(integer: .init(1)), with: .automatic)
                                self.tableViewOutlet.insertRows(at: [indexPath], with: .automatic)
                                self.tableViewOutlet.endUpdates()
                            }
                        }
                        RemoveLastRow()
                    }
                    MeterNoString = sender.text!
                    DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[2]] = sender.text!
                    
                }
                else if self.rowCountArray.count > 4
                {
                    if (sender.text?.count)! < 3
                    {
                        deleteBottomRows(AfterRowIndex: 2)
                        
                        DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Number")] = ""
                        DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Consumer Name (as on Bill)")] = ""
                        DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Ledger Number")] = ""
                        DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Read Date")] = ""
                        DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Tariff")] = ""
                        DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Pay For")] = ""
                        DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number")] = commonPrefixMobileNumber
                        DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount")] = ""
                        DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount")] = ""
                        DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")] = ""
                        DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Bill Amount")] = ""
                    }
                    
                }
                
            }
            else //Get Bill API hasn't been requested.
            {
                if (self.rowCountArray.count > 3)   //Meter no is being modified
                {
                    if (sender.text?.count)! < 3
                    {
                        meterNoFieldModified()
                    }
                    
                }
                else //Validating Meter No.
                {
                    //println_debug("Meter Number Field")
                    if (sender.text?.count)! >= 3
                    {
                        //GetBillButton?.frame.origin.y = self.view.frame.height - KeyboardSize - 54
                        GetBillButton?.isHidden = false
                        
                        if showHideComputerCodeField == 1{
                            
                            sectionUpdate = 0
                            let indexPath = IndexPath(row: 0, section: 1)
                            if sectionCountArray.count == 2 {
                                sectionCountArray.removeLast()
                                self.tableViewOutlet.beginUpdates()
                                self.tableViewOutlet.deleteRows(at: [indexPath], with: .automatic)
                                self.tableViewOutlet.deleteSections(IndexSet.init(integer: .init(1)), with: .automatic)
                                self.tableViewOutlet.endUpdates()
                                
                            }
                            
                        }
                    }
                    else if (sender.text?.count)! < 3
                    {
                        GetBillButton?.isHidden = true
                        electricityCellRef1?.clearButton.isHidden = false
                        
                        if showHideComputerCodeField == 1 {
                            
                            
                            let indexPath = IndexPath(row: 0, section: 1)
                            
                            sectionUpdate = 1
                            if self.sectionCountArray.count == 1 //, (sender.text?.characters.count)! == 2
                            {
                                self.sectionCountArray.append("")
                                
                                self.tableViewOutlet.beginUpdates()
                                self.tableViewOutlet.insertSections(IndexSet.init(integer: .init(1)), with: .automatic)
                                self.tableViewOutlet.insertRows(at: [indexPath], with: .automatic)
                                self.tableViewOutlet.endUpdates()
                            }
                        }
                    }
                    if (sender.text?.count)! < 1
                    {
                        electricityCellRef1?.clearButton.isHidden = true
                    }
                    if (sender.text?.count)! > 0 {
                        let ComputerCodeIndexPath = NSIndexPath.init(row: 0, section: 1)
                        
                        electricityCellRef  = tableViewOutlet.cellForRow(at: ComputerCodeIndexPath as IndexPath) as? GetElectricityBillTableViewCell
                        electricityCellRef?.textFieldOutlet.text = ""
                        //electricityCellRef?.textFieldOutlet.placeholder =  appDelegate.getlocaLizationLanguage(key: "Enter Computer Code Number")
                        electricityCellRef?.clearButton.isHidden = true
                        MeterNoString = sender.text!
                        computerCodeString = ""
                        DataNotInServerTextFieldDict["Computer Code Number"] = ""
                    }
                    DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[2]] = sender.text!
                    DataInServerTextFieldDict[DataInServerDescriptionArray[2]] = sender.text!
                    
                }
                
                }
                
            //Section 1
            case 9080 :     println_debug("/Computer Field")
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Number")] = ""
            computerFieldEnable = true
            let url = URL(string: TownshipImageArray[5])!
            self.downLoadImage(imageview1: TopImageViewOutlet, url: url)

            self.btnOrImage(showImage: true)
            
            sender.keyboardType = .numberPad
            
            if (sender.text?.count)! > 0 {
                sender.placeholder = "Computer Code Number".localized
                if let textField = sender as? FloatLabelTextField
                {
                    textField.titleActiveTextColour = UIColor.black
                }
            } else {
                sender.placeholder = "Enter Computer Code Number".localized
            }
            
            if (sender.text?.count)! == computerCodeLengthForSelectedTownship && computerCodeLengthForSelectedTownship != 0
            {
                GetBillButton?.frame.origin.y = self.view.frame.height - KeyboardSize - 54
                GetBillButton?.isHidden = false
            }
            else if (sender.text?.count)! !=  computerCodeLengthForSelectedTownship
            {
                GetBillButton?.isHidden = true
                
                if (sender.text?.count)! >  computerCodeLengthForSelectedTownship
                {
                    // sender.text?.characters = (sender.text?.characters.dropLast())!
                    if sender.text != nil {
                        sender.text = String(sender.text!.dropLast())
                    }
                    //sender.text = String.init(sender.text!.remove(at: String.Index.init(encodedOffset: sender.text!.count - 1)))
                    
                    
                    if  computerCodeLengthForSelectedTownship == 0
                    {
                        GetBillButton?.isHidden = true
                    }
                    else
                    {
                        GetBillButton?.isHidden = false
                    }
                    
                }
                
            }
            
            if (sender.text?.count)! > 0 {
                let MeterIndexPath = NSIndexPath.init(row: 2, section: 0)
                
                electricityCellRef  = tableViewOutlet.cellForRow(at: MeterIndexPath as IndexPath) as? GetElectricityBillTableViewCell
                electricityCellRef?.textFieldOutlet.text = ""
                //electricityCellRef?.textFieldOutlet.placeholder =  appDelegate.getlocaLizationLanguage(key: "Enter Meter Number")
                electricityCellRef?.clearButton.isHidden = true
                computerCodeString = sender.text!
                MeterNoString = ""
            }
            
            DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[7]] = sender.text!
            DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Computer Code Number")] = sender.text!
                
            case 1003       : println_debug("Consumer Name (as on Bill)")
            let url = URL(string: TownshipImageArray[1])!
            self.downLoadImage(imageview1: TopImageViewOutlet, url: url)
            
            self.btnOrImage(showImage: true)
            sender.keyboardType = .default
            sender.autocapitalizationType = .allCharacters
            let customIndex = NSIndexPath.init(row: 3, section: 0)
            self.electricityCellRef  = self.tableViewOutlet.cellForRow(at: customIndex as IndexPath) as? GetElectricityBillTableViewCell
            self.electricityCellRef?.isHidden = false
            
            if (sender.text?.count)! > 50 {
                sender.text = String(sender.text!.dropLast())
                return
            }
            
            if (sender.text?.count)! > 0 {
                sender.placeholder = "Consumer Name (as on Bill)".localized
                if let textField = sender as? FloatLabelTextField
                {
                    textField.titleActiveTextColour = UIColor.black
                }
            } else {
                sender.placeholder = "Enter Consumer Name (as on Bill)".localized
            }
            
            if sender.leftView == nil //&& sender.text! == ""
            {
                if MeterVsComputerCodeType == .MeterNo
                {
                    sender.resignFirstResponder()
                    self.showNameFieldValidationView()
                    return
                }
                else //Computer Code
                {
                    DispatchQueue.main.async {
                        
                        sender.resignFirstResponder()
                        self.showNameFieldValidationView()
                        return
                    }
                    
                }
            }
            else
            {
                let str = sender.text!
                if !validateNubersAndCharacterforNameOnTheBill(str)
                {
                    
                    GetBillButton?.isHidden = true
                    sender.text = String(sender.text!.dropLast())
                    //sender.text?.characters = (sender.text?.characters.dropLast())!
                    //sender.text = String.init(sender.text!.remove(at: String.Index.init(encodedOffset: sender.text!.count - 1)))
                    
                    return
                    
                }
                
                if (sender.text?.count)! >= 2
                {
                    if (self.rowCountArray.count == 4)
                    {
                        showNextRow()
                        
                        if self.view.frame.height > 650
                        {
                            //scrollToLastRow()
                        }
                    }
                    
                    
                    DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[3]] = sender.text!
                    
                }
                else if (sender.text?.count)! < 2 && self.rowCountArray.count == 5
                {
                    self.electricityCellRef?.clearButton.isHidden = true
                    RemoveLastRow()
                    SubmitButton?.isHidden = true
                }
                }
                
            if self.electricityCellRef?.textFieldOutlet.leftView == nil {
                self.electricityCellRef?.clearButton.isHidden = true

            } else {
                self.electricityCellRef?.clearButton.isHidden = false
                }
                
                
            case 1004       :   sender.keyboardType = .default
            sender.autocapitalizationType = .sentences
            
            if (sender.text?.count)! > 25 {
                sender.text = String(sender.text!.dropLast())
                return
            }
            
            if (sender.text?.count)! > 0 {
                sender.placeholder = "Ledger Number".localized
                if let textField = sender as? FloatLabelTextField
                {
                    textField.titleActiveTextColour = UIColor.black
                }
            } else {
                sender.placeholder = "Enter Ledger Number".localized
            }
            
            let url = URL(string: TownshipImageArray[2])!
            self.downLoadImage(imageview1: TopImageViewOutlet, url: url)
            self.btnOrImage(showImage: true)
            if (sender.text?.count)! >= 4
            {
                
                if self.rowCountArray.count == 5
                {
                    showNextRow()
                    
                    if self.view.frame.height > 600
                    {
                        //scrollToLastRow()
                    }
                }
                
                DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[4]] = sender.text!
                
            }
            else if(sender.text?.count)! < 4 && self.rowCountArray.count == 6
            {
                RemoveLastRow()
                SubmitButton?.isHidden = true
                }
                
            case 1005:
                let url = URL(string: TownshipImageArray[3])!
                self.downLoadImage(imageview1: TopImageViewOutlet, url: url)
                self.btnOrImage(showImage: true)
                
                DispatchQueue.main.async {
                    self.PickerType = .DatePicker
                    self.showCustomPickerView()
                    self.view.endEditing(true)
                }
                
                if (sender.text?.count)! > 0 {
                    if let textField = sender as? FloatLabelTextField
                    {
                        textField.titleActiveTextColour = UIColor.black
                    }
                }
                
            case 1006:      sender.keyboardType = .default
            sender.autocapitalizationType = .sentences
            let url = URL(string: TownshipImageArray[4])!
            self.downLoadImage(imageview1: TopImageViewOutlet, url: url)
            
            self.btnOrImage(showImage: true)
            DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[6]] = sender.text!
            
            if (sender.text?.count)! > 25 {
                sender.text = String(sender.text!.dropLast())
                return
            }
            
            if (sender.text?.count)! > 0 {
                sender.placeholder = "Tariff".localized
                if let textField = sender as? FloatLabelTextField
                {
                    textField.titleActiveTextColour = UIColor.black
                }
            } else {
                sender.placeholder = "Enter Tariff".localized
            }
            if (sender.text?.count)! > 0 && self.rowCountArray.count == 7
            {
                showNextRow()
                
                if MeterVsComputerCodeType == .ComputerCodeNo {
                    showNextRow()
                }
                else
                {
                    if self.view.frame.height > 600
                    {
                        //scrollToLastRow()
                    }
                }
                
                SubmitButton?.isHidden = true
            }
            else if(sender.text?.count)! < 1 && self.rowCountArray.count == 8
            {
                RemoveLastRow()
                SubmitButton?.isHidden = true
                }
                
            case 1007:      sender.keyboardType = .numberPad
            let url = URL(string: TownshipImageArray[5])!
            self.downLoadImage(imageview1: TopImageViewOutlet, url: url)
            self.btnOrImage(showImage: true)
            
            if (sender.text?.count)! > 0 {
                sender.placeholder = "Computer Code Number".localized
                if let textField = sender as? FloatLabelTextField
                {
                    textField.titleActiveTextColour = UIColor.black
                }
            } else {
                sender.placeholder = "Enter Computer Code Number".localized
            }
            
            if (sender.text?.count)! >  computerCodeLengthForSelectedTownship
            {
                //sender.text?.characters = (sender.text?.characters.dropLast())!
                let str: String = sender.text!
                sender.text = String(str.dropLast())
                if self.rowCountArray.count == 14
                {
                    SubmitButton?.isHidden = false
                }
                    //sender.text = String.init(sender.text!.remove(at: String.Index.init(encodedOffset: sender.text!.count - 1)))
                //sender.text = String.init(sender.text!.remove(at: String.Index.init(encodedOffset: sender.text!.count - 1)))
            } else if (sender.text?.count)! ==  computerCodeLengthForSelectedTownship {
                if self.rowCountArray.count == 14
                {
                    SubmitButton?.isHidden = false
                }
            } else {
                SubmitButton?.isHidden = true
            }
            
            
            if (sender.text?.count)! == computerCodeLengthForSelectedTownship && computerCodeLengthForSelectedTownship != 0
            {
                if self.rowCountArray.count == 8
                {
                    showNextRow()
                    if self.view.frame.height > 600
                    {
                        //scrollToLastRow()
                    }
                }
                
                DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[7]] = sender.text!
                computerCodeString = sender.text!
                
            }
            else if(sender.text?.count)! != computerCodeLengthForSelectedTownship && self.rowCountArray.count == 9
            {
                
                SubmitButton?.isHidden = true
                RemoveLastRow()
                
                }
            case 1008:
                DispatchQueue.main.async {
                    self.PickerType = .PayForPicker
                    self.showCustomPickerView()
                    self.view.endEditing(true)
                }
                
                TopImageViewOutlet.image = nil
                self.btnOrImage(showImage: false)
                if self.rowCountArray.count == 14
                {
                    SubmitButton?.isHidden = false
                }
                if (sender.text?.count)! > 0 {
                    if let textField = sender as? FloatLabelTextField
                    {
                        textField.titleActiveTextColour = UIColor.black
                    }
                }
                
            case 1009:      sender.keyboardType = .numberPad
            TopImageViewOutlet.image = nil
            self.btnOrImage(showImage: false)
            
            let customIndex = NSIndexPath.init(row: 9, section: 0)
            self.electricityCellRef  = self.tableViewOutlet.cellForRow(at: customIndex as IndexPath) as? GetElectricityBillTableViewCell
            //self.electricityCellRef?.clearButton.isHidden = true
            
            //println_debug(getMobileNoLength(text: sender.text!))
            
            if  sender.text == commonPrefixMobileNumber {
                self.electricityCellRef?.clearButton.isHidden = true
                self.electricityCellRef?.operatorLabelOutlet.text = ""
            } else {
                electricityCellRef?.clearButton.isHidden = false
            }
            
            if (sender.text?.count)! > 0 {
                if let textField = sender as? FloatLabelTextField
                {
                    textField.titleActiveTextColour = UIColor.black
                }
            }
            
            guard validObj.getNumberRangeValidation(sender.text!).isRejected == false else {
                alertViewObj.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: UIImage(named : "AppIcon"))
                alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                    self.electricityCellRef?.clearButton.isHidden = true
                    self.view.endEditing(true)
                    sender.text = commonPrefixMobileNumber
                    self.electricityCellRef?.operatorLabelOutlet.text = ""
                })
                alertViewObj.showAlert(controller: self)
                return
            }
            
            let minLength = validObj.getNumberRangeValidation(sender.text!).min
            let maxLength = validObj.getNumberRangeValidation(sender.text!).max
            
            
                if minLength == (sender.text?.count)! && self.rowCountArray.count == 10 {
                    showNextRow()
                } else if (sender.text?.count)! < minLength && self.rowCountArray.count == 11 {
                    RemoveLastRow()
                    DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[10]] = ""
                } else if sender.text?.count == maxLength {
                    
                } else {
                    
                }
            
            
            if  (sender.text?.count)! > maxLength {
                sender.text = String(sender.text!.dropLast())
                let customIndexPath = NSIndexPath.init(row: 10, section: 0)
                electricityCellRef = tableViewOutlet.cellForRow(at: customIndexPath as IndexPath) as? GetElectricityBillTableViewCell
                electricityCellRef?.textFieldOutlet.becomeFirstResponder()
            }
            
            
                
                
//            else if (sender.text?.hasPrefix("0986"))!
//            {
//                if (sender.text?.count)! == 9
//                {
//                    if self.rowCountArray.count == 10 {
//                        showNextRow()
//                    }
//
//                }
//                else if (sender.text?.count)! < 9 &&  self.rowCountArray.count == 11 {
//                    RemoveLastRow()
//                    DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[10]] = ""
//                    SubmitButton?.isHidden = true
//                }
//
//            }
//            else if (sender.text?.count)! == getMobileNoLength(text: sender.text!)  && self.rowCountArray.count == 10
//            {
//
//                showNextRow()
//                let customIndexPath = NSIndexPath.init(row: 10, section: 0)
//                electricityCellRef = tableViewOutlet.cellForRow(at: customIndexPath as IndexPath) as? GetElectricityBillTableViewCell
//                electricityCellRef?.textFieldOutlet.becomeFirstResponder()
//
//                if self.view.frame.height > 600
//                {
//                    //scrollToLastRow()
//                }
//
//            }
//            else if(sender.text?.count)! < getMobileNoLength(text: sender.text!) && self.rowCountArray.count == 11
//            {
//                RemoveLastRow()
//                DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[10]] = ""
//                SubmitButton?.isHidden = true
//            }
//            if (sender.text?.count)! >  2 && (sender.text?.count)! > getMobileNoLength(text: sender.text!)
//            {
//                //                            sender.text?.characters = (sender.text?.characters.dropLast())!
//                sender.text = String(sender.text!.dropLast())
//                //sender.text = String.init(sender.text!.remove(at: String.Index.init(encodedOffset: sender.text!.count - 1)))
//
//            }
            
            if (sender.text?.count)! >  3
            {
                electricityCellRef?.operatorLabelOutlet.text = validObj.getNumberRangeValidation(sender.text!).operator //getOperatorName(sender.text!)
            }
            
            DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[9]] = sender.text!
                
                
            case 1010:      sender.keyboardType = .numberPad
            let url = URL(string: TownshipImageArray[6])!
            self.downLoadImage(imageview1: TopImageViewOutlet, url: url)
            self.btnOrImage(showImage: true)
            
            if (sender.text?.count)! > 0 {
                if let textField = sender as? FloatLabelTextField
                {
                    textField.titleActiveTextColour = UIColor.black
                }
                sender.placeholder = "Bill Amount".localized
            } else {
                sender.placeholder = "Enter Bill Amount".localized
            }
            if sender.text! != ""
            {
                
                if (sender.text?.count)! >  10
                {
                    //sender.text?.characters = (sender.text?.characters.dropLast())!
                    sender.text = String(sender.text!.dropLast())
                    //sender.text = String.init(sender.text!.remove(at: String.Index.init(encodedOffset: sender.text!.count - 1)))
                    
                }
                else
                {
                    let formatter = NumberFormatter()
                    formatter.numberStyle = NumberFormatter.Style.decimal
                  var enteredAmount = 0

                    //remove any existing commas
                   let textAfterRemovingComma = sender.text!.replacingOccurrences(of: ",", with: "")
                  enteredAmount = Int(textAfterRemovingComma)  ?? 0
                  
                    if enteredAmount > 999  && self.rowCountArray.count == 11
                    {
                        showNextRow()
                        DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[11]] = ""
                        if self.view.frame.height > 600
                        {
                            //scrollToLastRow()
                        }
                        
                    }
                    else
                    {
                        //Check if Reenter field has amount
                        let formatter1 = NumberFormatter()
                        formatter1.numberStyle = NumberFormatter.Style.decimal
                        
                        //Convert Entered Bill Amount to Without comma
                        let enteredBillAmount : NSString = self.DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[11]]! as NSString
                        if enteredBillAmount != ""
                        {
                            let AlreadyEnteredAmountTextWithoutComma = enteredBillAmount.replacingOccurrences(of: ",", with: "")
                            
                            if enteredAmount != Int(AlreadyEnteredAmountTextWithoutComma)!
                            {
                                if (self.rowCountArray.count == 12)
                                {
                                    RemoveLastRow()
                                    SubmitButton?.isHidden = true
                                    DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[11]] = ""
                                    
                                }
                                else if self.rowCountArray.count == 14
                                {
                                    RemoveLastRow()
                                    RemoveLastRow()
                                    RemoveLastRow()
                                    SubmitButton?.isHidden = true
                                    DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[11]] = ""
                                    FooterViewContainer?.removeFromSuperview()
                                    self.tableViewOutlet.tableFooterView = nil
                                }
                                
                                if enteredAmount > 999  && self.rowCountArray.count == 11
                                {
                                    showNextRow()
                                }
                            }
                        }
                        
                    }
                    
                    //update the textField with commas
                  let formattedNum = formatter.string(from: NSNumber(value: Int(textAfterRemovingComma) ?? 0))
                    sender.text = formattedNum!
                    DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[10]] = sender.text!
                }
                }
                
                
            case 1011:      sender.keyboardType = .numberPad
            
            TopImageViewOutlet.image = nil
            self.btnOrImage(showImage: false)
            
            if (sender.text?.count)! > 0 {
                if let textField = sender as? FloatLabelTextField
                {
                    textField.titleActiveTextColour = UIColor.black
                }
                sender.placeholder = "Confirm Bill Amount".localized
            } else {
                sender.placeholder = "Re-enter Bill Amount".localized
            }
            if sender.text! != ""
            {
                DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[11]] = sender.text!
                
                let formatter = NumberFormatter()
                formatter.numberStyle = NumberFormatter.Style.decimal
                
                //remove any existing commas
                let currentTextWithoutComma = sender.text!.replacingOccurrences(of: ",", with: "")
                
                //Convert Entered Bill Amount to Without comma
                let enteredBillAmount : NSString = self.DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[10]]! as NSString
                let AlreadyEnteredAmountTextWithoutComma = enteredBillAmount.replacingOccurrences(of: ",", with: "")
                
                
                if acceptableNumberString(inCharacter: AlreadyEnteredAmountTextWithoutComma , fromCharacter:currentTextWithoutComma) == true
                {
                    println_debug("true")
                    //update the textField with commas
                    let formattedNum = formatter.string(from: NSNumber(value: Int(currentTextWithoutComma)!))
                    sender.text = formattedNum!
                    
                    
                    if AlreadyEnteredAmountTextWithoutComma == currentTextWithoutComma
                    {
                        if self.rowCountArray.count == 12
                        {
                            showNextRow()
                            showNextRow()
                            
                            if self.view.frame.height > 600
                            {
                                //scrollToLastRow()
                            }
                            let lastIndex = NSIndexPath.init(row: self.rowCountArray.count - 1, section: 0)
                            self.electricityCellRef = self.tableViewOutlet.cellForRow(at: lastIndex as IndexPath) as? GetElectricityBillTableViewCell
                            electricityCellRef?.textFieldOutlet.becomeFirstResponder()
                        }
                        
                        SubmitButton?.isHidden = false
                        
                        AdjustAmountParam = ""
                        MeterBillAmountParam = AlreadyEnteredAmountTextWithoutComma
                        self.DataNotInServerTextFieldDict["Bill Amount"] = AlreadyEnteredAmountTextWithoutComma
                        DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[11]] = sender.text!
                        
                        self.tableViewOutlet.tableFooterView = nil
                        let footerViewLabel = UILabel.init(frame: CGRect(x: 15, y: 5, width: self.view.frame.width - 30, height: 90))
                        footerViewLabel.textAlignment = .center
                        footerViewLabel.textColor = .red
                        footerViewLabel.numberOfLines = 4
                        footerViewLabel.font = UIFont(name: appFont, size: appFontSize)
                        
                        footerViewLabel.text = FooterText
                        DispatchQueue.main.async {
                            self.FooterViewContainer?.addSubview(footerViewLabel)
                            self.tableViewOutlet.beginUpdates()
                            self.tableViewOutlet.tableFooterView  = self.FooterViewContainer
                            self.tableViewOutlet.endUpdates()
                            self.toplayoutTableConstraint.constant = 0
                            self.topBtnHeightConstraint.constant = 0
                        }
                        
                    }
                    else if self.rowCountArray.count == 14
                    {
                        RemoveLastRow()
                        RemoveLastRow()
                        SubmitButton?.isHidden = true
                        FooterViewContainer?.removeFromSuperview()
                        self.tableViewOutlet.tableFooterView = nil
                    }
                    
                }
                else
                {
                    println_debug("False")
                    // sender.text?.characters = (sender.text?.characters.dropLast())!
                    sender.text = String(sender.text!.dropLast())
                   // sender.text = String.init(sender.text!.remove(at: String.Index.init(encodedOffset: sender.text!.count - 1)))
                    
                }
                
                }
                
            case 1012:      DispatchQueue.main.async {
                self.view.endEditing(true)
                self.btnOrImage(showImage: false)
                self.SubmitButton?.frame.origin.y = self.view.frame.height - 50
                
                self.DataNotInServerTextFieldDict[self.DataNotInServerDescriptionArray[12]] = sender.text!
                
                if self.view.frame.height > 600
                {
                    //self.scrollToLastRow()
                }
                
                }
                
            case 1013:      sender.keyboardType = .default
            sender.autocapitalizationType = .sentences
            DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[13]] = sender.text!
            SubmitButton?.frame.origin.y = self.view.frame.height - KeyboardSize - 50
            if (sender.text?.count)! > 0 {
                if let textField = sender as? FloatLabelTextField
                {
                    textField.titleActiveTextColour = UIColor.black
                }
                sender.placeholder = "Remarks".localized
            } else {
                sender.placeholder = "Enter Remarks".localized
            }
                
            default:
                println_debug("none")
            }
            
        }
        else if DataSourceType == .DataInServerDataSource
        {
            switch sender.tag
            {
                
            case 2000:      println_debug("call was in text Field did change")
            
            DispatchQueue.main.async {
                self.view.endEditing(true)
                self.GetBillButton?.isHidden = true
                self.SubmitButton?.isHidden = true
                self.btnOrImage(showImage: false)
                
            }
            if (sender.text?.count)! > 0 {
                if let textField = sender as? FloatLabelTextField
                {
                    textField.titleActiveTextColour = UIColor.black
                }
            }
            println_debug("Division Filed Clicked")
            let DivisonVCObj = self.storyboard?.instantiateViewController(withIdentifier: "SelectDivision_TownshipViewControllerSID") as! SelectDivision_TownshipViewController
            DivisonVCObj.delegate = self
            let navController = UINavigationController(rootViewController: DivisonVCObj)
            navController.isNavigationBarHidden = true
            navController.modalPresentationStyle = .fullScreen
            self.present(navController, animated: true, completion: nil)
                
                
            case 2001:        println_debug("call was in text Field did change")
            
            DispatchQueue.main.async {
                self.view.endEditing(true)
                self.btnOrImage(showImage: false)
                self.GetBillButton?.isHidden = true
                self.SubmitButton?.isHidden = true
                
            }
            if (sender.text?.count)! > 0 {
                if let textField = sender as? FloatLabelTextField
                {
                    textField.titleActiveTextColour = UIColor.black
                }
            }
            DivisionFieldActive = false
            println_debug("Div Code \(DivisionCode)")
            
            let TownshipVCObj = self.storyboard?.instantiateViewController(withIdentifier: "SelectDivision_TownshipViewControllerSID") as! SelectDivision_TownshipViewController
            TownshipVCObj.delegate = self
            let navController = UINavigationController(rootViewController: TownshipVCObj)
            navController.isNavigationBarHidden = true
            navController.modalPresentationStyle = .fullScreen
            self.present(navController, animated: true, completion: nil)
                
            case 2002   :
                if (sender.text?.count)! > 0 {
                    if let textField = sender as? FloatLabelTextField
                    {
                        textField.titleActiveTextColour = UIColor.black
                    }
                }
            if DataInServerDescriptionArray[2] == appDelegate.getlocaLizationLanguage(key: "Meter Number")
            {
                let customIndex = NSIndexPath.init(row: 2, section: 0)
                self.electricityCellRef  = self.tableViewOutlet.cellForRow(at: customIndex as IndexPath) as? GetElectricityBillTableViewCell
                self.electricityCellRef?.clearButton.isHidden = false
                //Show Meter No Image
                guard let url = URL(string: TownshipImageArray[0]) else { return }
                self.downLoadImage(imageview1: TopImageViewOutlet, url: url)
                self.btnOrImage(showImage: true)
                
                sender.keyboardType = .default
                sender.autocapitalizationType = .sentences
                
                //If value of this field is changed then change data source and do get bill again
                if sender.text != DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Number")]
                {
                    println_debug("Value Change Detected")
                    MeterVsComputerCodeType = .MeterNo
                    meterNoFieldModified()
                }
                
            }
            else
            {
                //Show Computer Code Image
                let url = URL(string: TownshipImageArray[5])!
                self.downLoadImage(imageview1: TopImageViewOutlet, url: url)
                self.btnOrImage(showImage: true)
                
                
                sender.keyboardType = .numberPad
                
                //If value of this field is changed then change data source and do get bill again
                if sender.text != DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Computer Code Number")]
                {
                    println_debug("Value Change Detected")
                    sender.text = ""
                    MeterVsComputerCodeType = .MeterNo
                    meterNoFieldModified()
                }
                }
                
            case 2003 : DispatchQueue.main.async {
                self.PickerType = .PayForPicker
                self.showCustomPickerView()
                self.view.endEditing(true)
                
            }
            if (sender.text?.count)! > 0 {
                if let textField = sender as? FloatLabelTextField
                {
                    textField.titleActiveTextColour = UIColor.black
                }
            }
            TopImageViewOutlet.image = nil
            self.btnOrImage(showImage: false)
                
            case 2004 : sender.keyboardType = .numberPad
            TopImageViewOutlet.image = nil
            self.btnOrImage(showImage: false)
            
            if (sender.text?.count)! > 0 {
                if let textField = sender as? FloatLabelTextField
                {
                    textField.titleActiveTextColour = UIColor.black
                }
            }
            //Get Cell Reference
            let customIndex = NSIndexPath.init(row: 4, section: 0)
            self.electricityCellRef  = self.tableViewOutlet.cellForRow(at: customIndex as IndexPath) as? GetElectricityBillTableViewCell
            self.electricityCellRef?.clearButton.isHidden = false
            
            guard validObj.getNumberRangeValidation(sender.text!).isRejected == false else {
                alertViewObj.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: UIImage(named : "AppIcon"))
                alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                    self.electricityCellRef?.clearButton.isHidden = true
                    self.view.endEditing(true)
                    sender.text = commonPrefixMobileNumber
                    self.electricityCellRef?.operatorLabelOutlet.text = ""
                })
                alertViewObj.showAlert(controller: self)
                return
            }
            
            let maxLength = validObj.getNumberRangeValidation(sender.text!).max
            if (sender.text?.count)! > maxLength {
                sender.text = String(sender.text!.dropLast())
                return
            }
            
            if (sender.text?.hasPrefix("0986"))!
            {
                if (sender.text?.count)! == 9
                {
                    if self.rowCountArray.count == 5 {
                        showNextRow()
                    }
                    
                    if DataInServerDescriptionArray[5] == appDelegate.getlocaLizationLanguage(key: "Bill Amount") && rowCountArray.count == 6
                    {
                        showNextRow()
                        showNextRow()
                        
                        SubmitButton?.isHidden = false
                        
                    }
                    DataInServerTextFieldDict[DataInServerDescriptionArray[4]] = sender.text!
                    
                }
                else if (sender.text?.count)! < 9 &&  self.rowCountArray.count == 6 {
                    RemoveLastRow()
                    
                }
                
            }
            else if (sender.text?.count)! == validObj.getNumberRangeValidation(sender.text!).min
            {
                
                if self.rowCountArray.count == 5 {
                    //If Charachter count is greater than 10 show next row
                    showNextRow()
                    
                    //If Next field is Bill Amount and row count is 6 then show the next two fields as well
                    if DataInServerDescriptionArray[5] == appDelegate.getlocaLizationLanguage(key: "Bill Amount") && rowCountArray.count == 6
                    {
                        showNextRow()
                        showNextRow()
                        
                        SubmitButton?.isHidden = false
                    }
                    
                    
                    if self.view.frame.height > 600
                    {
                        //scrollToLastRow()
                    }
                }
                
                DataInServerTextFieldDict[DataInServerDescriptionArray[4]] = sender.text!
                
            } else if (sender.text?.count)! == validObj.getNumberRangeValidation(sender.text!).max {
                if self.rowCountArray.count == 5 {
                    let nextIndex = NSIndexPath.init(row: 5, section: 0)
                    self.electricityCellRef = self.tableViewOutlet.cellForRow(at: nextIndex as IndexPath) as? GetElectricityBillTableViewCell
                    electricityCellRef?.textFieldOutlet.becomeFirstResponder()
                }
                DataInServerTextFieldDict[DataInServerDescriptionArray[4]] = sender.text!
            }
            
            //Set-Remove Label
            if (sender.text?.count)! >  3
            {
                electricityCellRef?.operatorLabelOutlet.text = validObj.getNumberRangeValidation(sender.text!).operator //getOperatorName(sender.text!)
            }
            else
            {
                electricityCellRef?.operatorLabelOutlet.text = ""
            }
            
            
            //Drop Charachter if count is more than 11
            if (sender.text?.count)! >  2 && (sender.text?.count)! > validObj.getNumberRangeValidation(sender.text!).max
            {
                // sender.text?.characters = (sender.text?.characters.dropLast())!
                sender.text = String.init(sender.text!.remove(at: String.Index.init(encodedOffset: sender.text!.count - 1)))
                }
                
            case 2005 : let url = URL(string: TownshipImageArray[6])!
            self.downLoadImage(imageview1: TopImageViewOutlet, url: url)
            self.btnOrImage(showImage: true)
            sender.keyboardType = .numberPad
            
            if (sender.text?.count)! > 0 {
                if let textField = sender as? FloatLabelTextField
                {
                    textField.titleActiveTextColour = UIColor.black
                }
            }
            //Get Cell Reference
            let currentIndexPath = NSIndexPath.init(row: 5, section: 0)
            electricityCellRef = tableViewOutlet.cellForRow(at: currentIndexPath as IndexPath) as? GetElectricityBillTableViewCell
            
            //Check if the field is Enter Bill amount
            if electricityCellRef?.DescriptionLabelOutlet.text! == appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount")
            {
                if sender.text! != ""
                {
                    if (sender.text?.count)! >  10
                    {
                        // sender.text?.characters = (sender.text?.characters.dropLast())!
                        sender.text = String(sender.text!.dropLast())
                        //sender.text = String.init(sender.text!.remove(at: String.Index.init(encodedOffset: sender.text!.count - 1)))
                        
                    }
                    else
                    {
                        let formatter = NumberFormatter()
                        formatter.numberStyle = NumberFormatter.Style.decimal
                        
                        //remove any existing commas
                        let textAfterRemovingComma = sender.text!.replacingOccurrences(of: ",", with: "")
                        
                        let enteredAmount = Int(textAfterRemovingComma)!
                        
                        if enteredAmount > 999  && self.rowCountArray.count == 6
                        {
                            showNextRow()
                            DataInServerTextFieldDict[DataInServerDescriptionArray[6]] = ""
                            
                            if self.view.frame.height > 600
                            {
                                //scrollToLastRow()
                            }
                            
                        }
                        else
                        {
                            
                            //Check if Reenter field has amount
                            let formatter1 = NumberFormatter()
                            formatter1.numberStyle = NumberFormatter.Style.decimal
                            
                            //Convert Entered Bill Amount to Without comma
                            let enteredBillAmount : NSString = DataInServerTextFieldDict[DataInServerDescriptionArray[6]]! as NSString
                            if enteredBillAmount != ""
                            {
                                let AlreadyEnteredAmountTextWithoutComma = enteredBillAmount.replacingOccurrences(of: ",", with: "")
                                
                                if enteredAmount != Int(AlreadyEnteredAmountTextWithoutComma)!
                                {
                                    if self.rowCountArray.count == 7
                                    {
                                        RemoveLastRow()
                                        SubmitButton?.isHidden = true
                                        DataInServerTextFieldDict[DataInServerDescriptionArray[6]] = ""
                                    }
                                    else if self.rowCountArray.count == 9
                                    {
                                        RemoveLastRow()
                                        RemoveLastRow()
                                        RemoveLastRow()
                                        SubmitButton?.isHidden = true
                                        DataInServerTextFieldDict[DataInServerDescriptionArray[6]] = ""
                                        FooterViewContainer?.removeFromSuperview()
                                        self.tableViewOutlet.tableFooterView = nil
                                        self.tableViewOutlet.tableFooterView = UIView()
                                        
                                    }
                                    
                                    if enteredAmount > 999  && self.rowCountArray.count == 6
                                    {
                                        showNextRow()
                                    }
                                    
                                }
                            }
                            
                        }
                        
                        //update the textField with commas
                        let formattedNum = formatter.string(from: NSNumber(value: Int(textAfterRemovingComma)!))
                        sender.text = formattedNum!
                        DataInServerTextFieldDict[DataInServerDescriptionArray[5]] = sender.text!
                        DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Bill Amount")] = sender.text!
                    }
                    
                }
                
            }
            else if electricityCellRef?.DescriptionLabelOutlet.text! == appDelegate.getlocaLizationLanguage(key: "Bill Amount") {
                      self.view.endEditing(true)
                      self.SubmitButton?.frame.origin.y = self.view.frame.height - 50
                }
                
            case 2006 : TopImageViewOutlet.image = nil
            self.btnOrImage(showImage: false)
            sender.keyboardType = .numberPad
            
            if (sender.text?.count)! > 0 {
                if let textField = sender as? FloatLabelTextField
                {
                    textField.titleActiveTextColour = UIColor.black
                }
            }
            
            if let text = sender.text, text.count > 0
            {
                if DataInServerDescriptionArray.count >= 6 {
                    DataInServerTextFieldDict[DataInServerDescriptionArray[6]] = ""
                }
                
                let formatter = NumberFormatter()
                formatter.numberStyle = NumberFormatter.Style.decimal
                
                //remove any existing commas
                let currentTextWithoutComma = sender.text!.replacingOccurrences(of: ",", with: "")
                
                //Convert Entered Bill Amount to Without comma
                let enteredBillAmount : NSString = self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount")]! as NSString
                let AlreadyEnteredAmountTextWithoutComma = enteredBillAmount.replacingOccurrences(of: ",", with: "")
                
                
                if acceptableNumberString(inCharacter: AlreadyEnteredAmountTextWithoutComma , fromCharacter:currentTextWithoutComma) == true
                {
                    println_debug("true")
                    //update the textField with commas
                    let formattedNum = formatter.string(from: NSNumber(value: Int(currentTextWithoutComma)!))
                    sender.text = formattedNum!
                    
                    
                    if AlreadyEnteredAmountTextWithoutComma == currentTextWithoutComma
                    {
                        if self.rowCountArray.count == 7
                        {
                            showNextRow()
                            showNextRow()
                            if self.view.frame.height > 600
                            {
                                //scrollToLastRow()
                            }
                            
                            let lastIndex = NSIndexPath.init(row: self.rowCountArray.count - 1, section: 0)
                            self.electricityCellRef = self.tableViewOutlet.cellForRow(at: lastIndex as IndexPath) as? GetElectricityBillTableViewCell
                            electricityCellRef?.textFieldOutlet.becomeFirstResponder()
                            
                            
                        }
                        
                        SubmitButton?.isHidden = false
                        
                        AdjustAmountParam = ""
                        MeterBillAmountParam = AlreadyEnteredAmountTextWithoutComma
                        DataInServerTextFieldDict[DataInServerDescriptionArray[6]] = AlreadyEnteredAmountTextWithoutComma
                        
                        self.tableViewOutlet.tableFooterView = nil
                        let footerViewLabel = UILabel.init(frame: CGRect(x: 15, y: 5, width: self.view.frame.width - 30, height: 90))
                        footerViewLabel.font = UIFont(name: appFont, size: appFontSize)
                        footerViewLabel.textAlignment = .center
                        footerViewLabel.textColor = .red
                        footerViewLabel.numberOfLines = 4
                        //footerViewLabel.font = UIFont(name: appFont, size: 14.0)
                        if let footerTxt = FooterText {
                            footerViewLabel.text = footerTxt
                        }
                        DispatchQueue.main.async {
                            self.FooterViewContainer?.addSubview(footerViewLabel)
                            self.tableViewOutlet.beginUpdates()
                            self.tableViewOutlet.tableFooterView  = self.FooterViewContainer
                            self.tableViewOutlet.endUpdates()
                            self.toplayoutTableConstraint.constant = 0
                            self.topBtnHeightConstraint.constant = 0
                            
                        }
                    }
                    else if self.rowCountArray.count == 9
                    {
                        RemoveLastRow()
                        RemoveLastRow()
                        SubmitButton?.isHidden = true
                        FooterViewContainer?.removeFromSuperview()
                        self.tableViewOutlet.tableFooterView = nil
                        self.tableViewOutlet.tableFooterView = UIView()
                    }
                }
                else
                {
                    println_debug("False")
                    //sender.text?.characters = (sender.text?.characters.dropLast())!
                    sender.text = String.init(sender.text!.remove(at: String.Index.init(encodedOffset: sender.text!.count - 1)))
                }
                }
                
            case 2007 : DispatchQueue.main.async {
                self.view.endEditing(true)
                self.btnOrImage(showImage: false)
                self.SubmitButton?.frame.origin.y = self.view.frame.height - 50
                
                self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Service Fees")] = sender.text!
                if (sender.text?.count)! > 0 {
                    if let textField = sender as? FloatLabelTextField
                    {
                        textField.titleActiveTextColour = UIColor.black
                    }
                }
                
                if self.view.frame.height > 600
                {
                    //self.scrollToLastRow()
                }
                }
                
            case 2008 :     sender.keyboardType = .default
            sender.autocapitalizationType = .sentences
            DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")] = sender.text!
            if (sender.text?.count)! > 0 {
                if let textField = sender as? FloatLabelTextField
                {
                    textField.titleActiveTextColour = UIColor.black
                }
                sender.placeholder = "Remarks".localized
            } else {
                sender.placeholder = "Enter Remarks".localized
            }
            SubmitButton?.frame.origin.y = self.view.frame.height - KeyboardSize - 50
                
            default:
                println_debug("none")
            }
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        switch textField.tag {
            
        case 1002 :  if MeterVsComputerCodeType == .ComputerCodeNo
        {
            
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Number")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Consumer Name (as on Bill)")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Ledger Number")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Read Date")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Tariff")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Pay For")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number")] = commonPrefixMobileNumber
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Bill Amount")] = ""
            
            deleteBottomRows(AfterRowIndex: 2)
            
            return false
            
        }
        else
        {
            if self.rowCountArray.count == 3
            {
                if showHideComputerCodeField == 1 {
                    
                    let indexPath = IndexPath(row: 0, section: 1)
                    
                    sectionUpdate = 1
                    if self.sectionCountArray.count == 1 {
                        self.sectionCountArray.append("")
                        
                        self.tableViewOutlet.beginUpdates()
                        self.tableViewOutlet.insertSections(IndexSet.init(integer: .init(1)), with: .automatic)
                        self.tableViewOutlet.insertRows(at: [indexPath], with: .automatic)
                        self.tableViewOutlet.endUpdates()
                    }
                }
            }
            else
            {
                meterNoFieldModified()
                if showHideComputerCodeField == 1 {
                    let indexPath = IndexPath(row: 0, section: 1)
                    
                    sectionUpdate = 1
                    if self.sectionCountArray.count == 1 {
                        self.sectionCountArray.append("")
                        
                        self.tableViewOutlet.beginUpdates()
                        self.tableViewOutlet.insertSections(IndexSet.init(integer: .init(1)), with: .automatic)
                        self.tableViewOutlet.insertRows(at: [indexPath], with: .automatic)
                        self.tableViewOutlet.endUpdates()
                    }
                }
            }
            
            }
            
            
        case 1003 :  DispatchQueue.main.async {
            self.view.endEditing(true)
            textField.text = ""
            textField.leftView = nil
            
            self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Consumer Name (as on Bill)")] = ""
            self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Ledger Number")] = ""
            self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Read Date")] = ""
            self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Tariff")] = ""
            if self.MeterVsComputerCodeType == .MeterNo
            {
                self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Computer Code Number")] = ""
            }
            self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Pay For")] = ""
            self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number")] = commonPrefixMobileNumber
            self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount")] = ""
            self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount")] = ""
            self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")] = ""
            self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Bill Amount")] = ""
            
            self.deleteBottomRows(AfterRowIndex: 3)
            
        }
        return false
            
        case 1004 :
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Ledger Number")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Read Date")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Tariff")] = ""
            if self.MeterVsComputerCodeType == .MeterNo
            {
                self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Computer Code Number")] = ""
            }
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Pay For")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number")] = commonPrefixMobileNumber
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Bill Amount")] = ""
            
            deleteBottomRows(AfterRowIndex: 4)
            
            return false
            
        case 1006 :
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Tariff")] = ""
            if self.MeterVsComputerCodeType == .MeterNo
            {
                self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Computer Code Number")] = ""
            }
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Pay For")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number")] = commonPrefixMobileNumber
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Bill Amount")] = ""
            
            if self.MeterVsComputerCodeType == .ComputerCodeNo
            {
                let tempVar = self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Computer Code Number")]
                deleteBottomRows(AfterRowIndex: 6)
                self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Computer Code Number")] = tempVar
                
            }
            else
            {
                deleteBottomRows(AfterRowIndex: 6)
            }
            
            return false
            
        case 1007 :
            if self.MeterVsComputerCodeType == .MeterNo
            {
                self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Computer Code Number")] = ""
            }
            else
            {
                self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Computer Code Number")] = ""
                //Confirm this case scenerio with nanaji
            }
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Pay For")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number")] = commonPrefixMobileNumber
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Bill Amount")] = ""
            
            deleteBottomRows(AfterRowIndex: 7)
            
            return false
            
        case 1009 : if (textField.text?.count)! > 2
        {
            textField.text = commonPrefixMobileNumber
            let customIndex = NSIndexPath.init(row: 9, section: 0)
            self.electricityCellRef  = self.tableViewOutlet.cellForRow(at: customIndex as IndexPath) as? GetElectricityBillTableViewCell
            electricityCellRef?.operatorLabelOutlet.text = ""
            
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number")] = commonPrefixMobileNumber
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Bill Amount")] = ""
            
            deleteBottomRows(AfterRowIndex: 9)
            
        }
        return false
            
        case 1010 : let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.decimal
        
        //remove any existing commas
        let textRemovedCommma = textField.text!.replacingOccurrences(of: ",", with: "")
        
        textField.text = textRemovedCommma
        if self.rowCountArray.count == 12
        {
            RemoveLastRow()
            SubmitButton?.isHidden = true
            DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[11]] = ""
            FooterViewContainer?.removeFromSuperview()
            self.tableViewOutlet.tableFooterView = nil
        }
        else if self.rowCountArray.count == 14
        {
            RemoveLastRow()
            RemoveLastRow()
            RemoveLastRow()
            SubmitButton?.isHidden = true
            DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[11]] = ""
            FooterViewContainer?.removeFromSuperview()
            self.tableViewOutlet.tableFooterView = nil
            }
            
        case 1011 :  let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.decimal
        
        //remove any existing commas
        let textRemovedCommma = textField.text!.replacingOccurrences(of: ",", with: "")
        
        textField.text = textRemovedCommma
        
        if self.rowCountArray.count == 14
        {
            
            RemoveLastRow()
            RemoveLastRow()
            SubmitButton?.isHidden = true
            self.tableViewOutlet.tableFooterView = nil
            
            }
            
        case 2004 : if (textField.text?.count)! > 2
        {
            textField.text = commonPrefixMobileNumber
            let customIndex = NSIndexPath.init(row: 4, section: 0)
            self.electricityCellRef  = self.tableViewOutlet.cellForRow(at: customIndex as IndexPath) as? GetElectricityBillTableViewCell
            electricityCellRef?.operatorLabelOutlet.text = ""
            
            DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number")] = commonPrefixMobileNumber
            DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount")] = ""
            DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount")] = ""
            DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")] = ""
            deleteBottomRows(AfterRowIndex: 4)
            
        }
        return false
            
        case 2005 : DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount")] = ""
        DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount")] = ""
        DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")] = ""
        
        deleteBottomRows(AfterRowIndex: 5)
        
        return false
            
        case 2006 : DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount")] = ""
        DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")] = ""
        
        deleteBottomRows(AfterRowIndex: 6)
        
        return false
            
        default  : print ("")
            
        }
        
        return true
    }
    
    func didDelete(sender: UITextField) {
        
        switch sender.tag {
            
        case 1003 : if (sender.text?.count)! < 1
        {
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Consumer Name (as on Bill)")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Ledger Number")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Read Date")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Tariff")] = ""
            if self.MeterVsComputerCodeType == .MeterNo
            {
                self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Computer Code Number")] = ""
            }
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Pay For")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number")] = commonPrefixMobileNumber
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Bill Amount")] = ""
            
            deleteBottomRows(AfterRowIndex: 3)
            
            }
            
        case 1004 : if (sender.text?.count)! < 1
        {
            
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Ledger Number")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Read Date")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Tariff")] = ""
            if self.MeterVsComputerCodeType == .MeterNo
            {
                self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Computer Code Number")] = ""
            }
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Pay For")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number")] = commonPrefixMobileNumber
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Bill Amount")] = ""
            
            deleteBottomRows(AfterRowIndex: 4)
            
            }
        case 1006 : if (sender.text?.count)! < 1
        {
            
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Tariff")] = ""
            if self.MeterVsComputerCodeType == .MeterNo
            {
                self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Computer Code Number")] = ""
            }
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Pay For")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number")] = commonPrefixMobileNumber
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Bill Amount")] = ""
            
            if self.MeterVsComputerCodeType == .ComputerCodeNo
            {
                let tempVar = self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Computer Code Number")]
                deleteBottomRows(AfterRowIndex: 6)
                self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Computer Code Number")] = tempVar
            }
            else
            {
                deleteBottomRows(AfterRowIndex: 6)
            }
            
            }
        case 1007 : if (sender.text?.count)! < 1
        {
            
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Computer Code Number")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Pay For")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number")] = commonPrefixMobileNumber
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Bill Amount")] = ""
            
            deleteBottomRows(AfterRowIndex: 7)
            }
            
            
        case 1009 : if (sender.text?.count)! < 3
        {
            
            //DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number")] = "09"
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Bill Amount")] = ""
            
            deleteBottomRows(AfterRowIndex: 9)
            }
            
        case 1010 : if (sender.text?.count)! == 3
        {
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Bill Amount")] = ""
            
            if (self.rowCountArray.count == 12)
            {
                RemoveLastRow()
                SubmitButton?.isHidden = true
                DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[11]] = ""
                
            }
            else if self.rowCountArray.count == 14
            {
                RemoveLastRow()
                RemoveLastRow()
                RemoveLastRow()
                SubmitButton?.isHidden = true
                DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[11]] = ""
                FooterViewContainer?.removeFromSuperview()
                self.tableViewOutlet.tableFooterView = nil
            }
            }
            
        case 2004 : if (sender.text?.count)! < 3
        {
            DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number")] = commonPrefixMobileNumber
            DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount")] = ""
            DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount")] = ""
            DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")] = ""
            
            deleteBottomRows(AfterRowIndex: 4)
            }
            
        case 2005 : if (sender.text?.count)! == 3
        {
            
            DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount")] = ""
            DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")] = ""
            
            deleteBottomRows(AfterRowIndex: 5)
            }
            
        default : break
            
            
        }
    }
    
    func deleteBottomRows(AfterRowIndex: Int) {
        
        SubmitButton?.isHidden = true
        FooterViewContainer?.removeFromSuperview()
        self.tableViewOutlet.tableFooterView = nil
        self.tableViewOutlet.tableFooterView = UIView()
        
        self.rowCountArray.removeAll()
        
        for _ in  0 ... AfterRowIndex {
            self.rowCountArray.append("")
        }
        if AfterRowIndex == 9 {
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number")] = commonPrefixMobileNumber
        }
        
        self.tableViewOutlet.reloadData()
        
        let customIndexPath = NSIndexPath.init(row: self.rowCountArray.count - 1, section: 0)
        electricityCellRef = tableViewOutlet.cellForRow(at: customIndexPath as IndexPath) as? GetElectricityBillTableViewCell
        if self.rowCountArray.count - 1 == 9 {
            electricityCellRef?.textFieldOutlet.text = commonPrefixMobileNumber
            electricityCellRef?.clearButton.isHidden = true
        } else {
            electricityCellRef?.textFieldOutlet.text = ""
        }
        if nameClear {
            nameClear = false
        } else {
            electricityCellRef?.textFieldOutlet.becomeFirstResponder()
        }
        
    }
    
    func acceptableNumberString(inCharacter chars: String, fromCharacter finalChar: String) -> Bool {
        if chars .hasPrefix(finalChar){
            return true
        } else {
            return false
        }
    }
    
    @IBAction func textFieldDidEndEditing(_ sender: UITextField)
    {
        if let textField = sender as? FloatLabelTextField {
            textField.titleTextColour = UIColor.init(red: 28/255.0, green: 45/255.0, blue: 153/255.0, alpha: 1)
        }
        computerFieldEnable = false
        activeField = sender
        if DataSourceType == .DataNotInServerDataSource
        {
            switch sender.tag{
                
            case 1002 : GetBillButton?.isHidden = true
            sender.resignFirstResponder()
                
                
            default : println_debug("Exception")
                
            }
        }
        
        if sender.tag == 1003 {
            let contactIndexPath = NSIndexPath.init(row: 3, section: 0)
            if let cell = tableViewOutlet.cellForRow(at: contactIndexPath as IndexPath) as? GetElectricityBillTableViewCell {
                if (cell.textFieldOutlet.text?.count)! > 0 {
                    cell.textFieldOutlet.placeholder = "Consumer Name (as on Bill)".localized
                } else {
                    cell.textFieldOutlet.placeholder = "Enter Consumer Name (as on Bill)".localized
                }
            }
        }
    }
    
    
    @IBAction func selectContactAction(_ sender: Any)
    {
        self.view.endEditing(true)
        let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
        nav.modalPresentationStyle = .fullScreen
        self.present(nav, animated: true, completion: nil)
        // vinnu commented
        //        let contactView = ContactViewController()
        //        contactView.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        //        contactView.isfrom = "electricityBill"
        //        contactView.electricityBillDelegateContacts = self
        //        self.present(contactView, animated: true, completion: nil)
        
        
    }
    
    func getContact(_ contactName: String!, contactNumber phoneNumber: String!)
    {
        println_debug(phoneNumber)
        var phoneNumberToPass = phoneNumber
        
        if phoneNumber.hasPrefix("+91")
        {
            return
        }
        else if phoneNumber.hasPrefix("+95")
        {
            phoneNumberToPass = phoneNumber.replacingOccurrences(of: "+95", with: "0")
        }
        
        if DataSourceType == .DataNotInServerDataSource
        {
            let contactIndexPath = NSIndexPath.init(row: 9, section: 0)
            electricityCellRef = tableViewOutlet.cellForRow(at: contactIndexPath as IndexPath) as? GetElectricityBillTableViewCell
            electricityCellRef?.textFieldOutlet.text = phoneNumberToPass
            electricityCellRef?.operatorLabelOutlet.text = validObj.getNumberRangeValidation(phoneNumberToPass!).operator //getOperatorName(phoneNumberToPass!)
            DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[9]] = phoneNumberToPass
            if rowCountArray.count == 10
            {
                showNextRow()
            }
            
        }
        else
        {
            let contactIndexPath = NSIndexPath.init(row: 4, section: 0)
            electricityCellRef = tableViewOutlet.cellForRow(at: contactIndexPath as IndexPath) as? GetElectricityBillTableViewCell
            electricityCellRef?.textFieldOutlet.text = phoneNumberToPass
            electricityCellRef?.operatorLabelOutlet.text = validObj.getNumberRangeValidation(phoneNumberToPass!).operator //getOperatorName(phoneNumberToPass!)
            DataInServerTextFieldDict[DataInServerDescriptionArray[4]] = phoneNumberToPass
            if rowCountArray.count == 5
            {
                showNextRow()
            }
            
        }
        
    }
    
    @objc func hideNameValidationView()
    {
        //        NameViewTransparentLayer?.removeFromSuperview()
        //        NameValidationContainerView?.removeFromSuperview()
        //        NameValidationShownOnce = false
    }
    
    
    func showNameFieldValidationView()
    {
        self.tableViewOutlet.isScrollEnabled = false
        
        if MeterVsComputerCodeType == .ComputerCodeNo
        {
            if TopImageViewHeightConstraint.constant > 0
            {
                println_debug("Frame will mess up")
                
                let customIndexPath = NSIndexPath.init(row: 0, section: 0)
                self.tableViewOutlet.scrollToRow(at: customIndexPath as IndexPath, at: .top, animated: true)
            }
        }
        
        self.tableViewOutlet.reloadData()
        println_debug("Adding Name view")
        
        NameViewTransparentLayer = UIView.init(frame: CGRect(x: 0, y: 0, width: (UIApplication.shared.keyWindow?.frame.width)!, height: (UIApplication.shared.keyWindow?.frame.height)!))
        NameViewTransparentLayer?.backgroundColor = .black
        NameViewTransparentLayer?.alpha = 0.09
        //UIApplication.shared.keyWindow?.addSubview(NameViewTransparentLayer!)
        
//        let visibleCellsList: [GetElectricityBillTableViewCell] = tableViewOutlet.visibleCells as! [GetElectricityBillTableViewCell]
//        for currentCell: GetElectricityBillTableViewCell in visibleCellsList {
//            println_debug("cell : \(String(describing: currentCell.DescriptionLabelOutlet?.text)) \n\n")
//
//            if currentCell.DescriptionLabelOutlet?.text! ==  appDelegate.getlocaLizationLanguage(key: "Consumer Name (as on Bill)")
//            {
//                println_debug("reference mil gya")
//
//                let customIndexPath = NSIndexPath.init(row: 3, section: 0)
//                let rectOfCellInTableView = tableViewOutlet.rectForRow(at: customIndexPath as IndexPath)
//                let rectOfCellInSuperview = tableViewOutlet.convert(rectOfCellInTableView, to: tableViewOutlet.superview)
//
//                println_debug("Y of Cell is: \(rectOfCellInSuperview.origin.y)")
//
//                CellYPos = rectOfCellInSuperview.origin.y + rectOfCellInSuperview.height
//                nameFHeight = rectOfCellInSuperview.height
//            }
//
//        }
        
        if let cell = tableViewOutlet.cellForRow(at: IndexPath(row: 3, section: 0)) as? GetElectricityBillTableViewCell {
            
            NameValidationContainerView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: cell.frame.size.height))
            NameValidationContainerView?.backgroundColor = .white
            NameValidationContainerView?.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
            NameValidationContainerView?.layer.borderWidth = 1.0
            cell.addSubview(NameValidationContainerView!)
            //UIApplication.shared.keyWindow?.addSubview(NameValidationContainerView!)
            
            let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(hideNameValidationView))
            tapGesture.delegate = self
            NameViewTransparentLayer?.addGestureRecognizer(tapGesture)
            
            
            let MaleImageView = UIImageView.init(frame: CGRect(x: 25, y: 23, width: 30, height: 30))
            MaleImageView.image = #imageLiteral(resourceName: "father")
            MaleImageView.contentMode = .scaleAspectFit
            NameValidationContainerView?.addSubview(MaleImageView)
            
            let MaleBtn = UIButton.init(frame: CGRect(x: 60, y: 13, width: self.view.frame.width / 2 - 60, height: 50))
            MaleBtn.setTitle(appDelegate.getlocaLizationLanguage(key: "Male"), for: .normal)
            MaleBtn.titleLabel?.font = UIFont.init(name: appFont, size: appButtonSize)
            MaleBtn.setTitleColor(.gray, for: .normal)
            MaleBtn.addTarget(self, action: #selector(self.MaleBtnClicked), for: .touchUpInside)
            NameValidationContainerView?.addSubview(MaleBtn)
            
            let femaleImageView = UIImageView.init(frame: CGRect(x: self.view.frame.width/2 + 10, y: 23, width: 30, height: 30))
            femaleImageView.image = #imageLiteral(resourceName: "r_female")
            femaleImageView.contentMode = .scaleAspectFit
            NameValidationContainerView?.addSubview(femaleImageView)
            
            let FemaleBtn = UIButton.init(frame: CGRect(x: self.view.frame.width/2 + 50, y: 13, width: self.view.frame.width / 2 - 60, height: 50))
            FemaleBtn.setTitle(appDelegate.getlocaLizationLanguage(key: "Female"), for: .normal)
            FemaleBtn.titleLabel?.font = UIFont.init(name: appFont, size: appButtonSize)
            FemaleBtn.addTarget(self, action: #selector(self.FemaleBtnClicked), for: .touchUpInside)
            FemaleBtn.setTitleColor(.gray, for: .normal)
            NameValidationContainerView?.addSubview(FemaleBtn)
        }
    }
    
    @objc func MaleBtnClicked()
    {
        
        NameValidationContainerView?.subviews.forEach { $0.removeFromSuperview() }
        
        var BtnNameArray = [""]
        
        if appDelegate.getSelectedLanguage() == "en"
        {
            BtnNameArray = [appDelegate.getlocaLizationLanguage(key: "U"),appDelegate.getlocaLizationLanguage(key: "Mg"), appDelegate.getlocaLizationLanguage(key: "Mr") ,appDelegate.getlocaLizationLanguage(key: "Dr")]
        }
        else
        {
            BtnNameArray =  [appDelegate.getlocaLizationLanguage(key: "U"),appDelegate.getlocaLizationLanguage(key: "Mg"),appDelegate.getlocaLizationLanguage(key: "Dr")]
        }
        
        let MaleImageView = UIImageView.init(frame: CGRect(x: 10, y: 25, width: 30, height: 30))
        MaleImageView.image = #imageLiteral(resourceName: "r_male")
        MaleImageView.contentMode = .scaleAspectFit
        NameValidationContainerView?.addSubview(MaleImageView)
        
        var i = 0
        var xPos = 45
        let Btnwidth = ((self.view.frame.width - 60) - 15) / CGFloat(BtnNameArray.count)
        
        while i < BtnNameArray.count
        {
            let Button = UIButton.init(frame: CGRect(x: xPos, y: 15, width: Int(Btnwidth), height: 60))
            Button.setTitle(BtnNameArray[i], for: .normal)
            Button.titleLabel?.font = UIFont.init(name: appFont, size: appButtonSize)
            Button.setTitleColor(.gray, for: .normal)
            Button.addTarget(self, action: #selector(MaleOptionsBtnClicked(sender:)), for: .touchUpInside)
            Button.tag = i
            xPos = xPos + Int(Btnwidth) + 5
            DispatchQueue.main.async {
                
                self.NameValidationContainerView?.addSubview(Button)
                
            }
            i =  i + 1
        }
    }
    
    @objc func MaleOptionsBtnClicked(sender:UIButton)
    {
        NameValidationContainerView?.subviews.forEach { $0.removeFromSuperview() }
        
        var BtnNameArray = [""]
        
        if appDelegate.getSelectedLanguage() == "en"
        {
            BtnNameArray = [appDelegate.getlocaLizationLanguage(key: "U"),appDelegate.getlocaLizationLanguage(key: "Mg"), appDelegate.getlocaLizationLanguage(key: "Mr") ,appDelegate.getlocaLizationLanguage(key: "Dr")]
        }
        else
        {
            BtnNameArray =  [appDelegate.getlocaLizationLanguage(key: "U"),appDelegate.getlocaLizationLanguage(key: "Mg"),appDelegate.getlocaLizationLanguage(key: "Dr")]
        }
        
        println_debug(BtnNameArray[sender.tag])
        
        
        let customLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 40, height: 30))
        customLabel.font = UIFont.init(name: appFont, size: 15)
        customLabel.text = BtnNameArray[sender.tag]
        
        DataNotInServerImageArray.remove(at: 3)
        DataNotInServerImageArray.insert(#imageLiteral(resourceName: "EBNameonTheBill"), at: 3)
        
        tableViewOutlet.reloadData()
        
        let customIndexPath = NSIndexPath.init(row: 3, section: 0)
        electricityCellRef = tableViewOutlet.cellForRow(at: customIndexPath as IndexPath) as? GetElectricityBillTableViewCell
        println_debug("cell : \(electricityCellRef?.DescriptionLabelOutlet?.text, electricityCellRef?.textFieldOutlet?.tag) \n\n")
        electricityCellRef?.textFieldOutlet.leftView = customLabel
        electricityCellRef?.textFieldOutlet.leftViewMode = .always
        electricityCellRef?.clearButton.isHidden = false
        electricityCellRef?.textFieldOutlet.becomeFirstResponder()
        
        NameValidationContainerView?.removeFromSuperview()
        NameViewTransparentLayer?.removeFromSuperview()
        
        println_debug("Removing Name ValidationContainer")
        NameValidationShownOnce = false
        self.tableViewOutlet.isScrollEnabled = true
    }
    
    @objc func FemaleBtnClicked()
    {
        NameValidationContainerView?.subviews.forEach { $0.removeFromSuperview() }
        
        var BtnNameArray = [""]
        
        if appDelegate.getSelectedLanguage() == "en"
        {
            BtnNameArray = [appDelegate.getlocaLizationLanguage(key: "Daw"),appDelegate.getlocaLizationLanguage(key: "Ma"), appDelegate.getlocaLizationLanguage(key: "Ms") ,appDelegate.getlocaLizationLanguage(key: "Mrs"), appDelegate.getlocaLizationLanguage(key: "Dr")]
        }
        else
        {
            BtnNameArray =  [appDelegate.getlocaLizationLanguage(key: "Daw"),appDelegate.getlocaLizationLanguage(key: "Ma"), appDelegate.getlocaLizationLanguage(key: "Dr")]
        }
        
        let FemaleImageView = UIImageView.init(frame: CGRect(x: 10, y: 25, width: 30, height: 30))
        FemaleImageView.image = #imageLiteral(resourceName: "r_female")
        FemaleImageView.contentMode = .scaleAspectFit
        NameValidationContainerView?.addSubview(FemaleImageView)
        
        var i = 0
        var xPos = 45
        let Btnwidth = ((self.view.frame.width - 60) - 15) / CGFloat(BtnNameArray.count)
        
        while i < BtnNameArray.count
        {
            let Button = UIButton.init(frame: CGRect(x: xPos, y: 15, width: Int(Btnwidth), height: 60))
            Button.setTitle(BtnNameArray[i], for: .normal)
            Button.titleLabel?.font = UIFont.init(name: appFont, size: appButtonSize)
            Button.setTitleColor(.gray, for: .normal)
            Button.addTarget(self, action: #selector(FemaleOptionsBtnClicked(sender:)), for: .touchUpInside)
            Button.tag = i
            xPos = xPos + Int(Btnwidth) + 5
            DispatchQueue.main.async {
                
                self.NameValidationContainerView?.addSubview(Button)
                
            }
            i =  i + 1
        }
        
    }
    
    @objc func FemaleOptionsBtnClicked(sender:UIButton)
    {
        NameValidationContainerView?.subviews.forEach { $0.removeFromSuperview() }
        var BtnNameArray = [""]
        
        if appDelegate.getSelectedLanguage() == "en"
        {
            BtnNameArray = [appDelegate.getlocaLizationLanguage(key: "Daw"),appDelegate.getlocaLizationLanguage(key: "Ma"), appDelegate.getlocaLizationLanguage(key: "Ms") ,appDelegate.getlocaLizationLanguage(key: "Mrs"), appDelegate.getlocaLizationLanguage(key: "Dr")]
        }
        else
        {
            BtnNameArray =  [appDelegate.getlocaLizationLanguage(key: "Daw"),appDelegate.getlocaLizationLanguage(key: "Ma"), appDelegate.getlocaLizationLanguage(key: "Dr")]
        }
        println_debug(BtnNameArray[sender.tag])
        
        let customLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        customLabel.font = UIFont.init(name: appFont, size: 15)
        customLabel.text = BtnNameArray[sender.tag]
        
        DataNotInServerImageArray.remove(at: 3)
        DataNotInServerImageArray.insert(#imageLiteral(resourceName: "r_female"), at: 3)
        
        tableViewOutlet.reloadData()
        
        let customIndexPath = NSIndexPath.init(row: 3, section: 0)
        electricityCellRef = tableViewOutlet.cellForRow(at: customIndexPath as IndexPath) as? GetElectricityBillTableViewCell
        electricityCellRef?.textFieldOutlet.leftView = customLabel
        electricityCellRef?.textFieldOutlet.leftViewMode = .always
        electricityCellRef?.clearButton.isHidden = false
        electricityCellRef?.textFieldOutlet.becomeFirstResponder()
        
        NameValidationContainerView?.removeFromSuperview()
        NameViewTransparentLayer?.removeFromSuperview()
        
        println_debug("Removing Name ValidationContainer")
        NameValidationShownOnce = false
        self.tableViewOutlet.isScrollEnabled = true
    }
    
    func removeNameFiledOptionsView() {
        NameValidationContainerView?.subviews.forEach { $0.removeFromSuperview() }
        NameValidationContainerView?.removeFromSuperview()
        NameViewTransparentLayer?.removeFromSuperview()
    }

    
    private func downLoadImage(imageview1 : UIImageView?, url : URL?) {
        if let imageView = imageview1,  let url = url {
            imageView.contentMode = UIView.ContentMode.scaleAspectFit
            LazyImageLoader.showImage(On: imageView, fromURL: url, showActivityIndicator: false, indicatorStyle: .gray)
        }
    }
    
}

protocol DeleteDelegate {
    func didDelete(sender: UITextField)
}

class customTextField : FloatLabelTextField {
    
    var myDelegate : DeleteDelegate?
    
    override func deleteBackward() {
        super.deleteBackward()
        guard (self.myDelegate?.didDelete(sender: self) != nil ) else {
            return
        }
    }
}


extension GetElectricityBillVC : ElectricityClearFieldDelegate
{
    
    func clearButtonTapped(cell: GetElectricityBillTableViewCell) {
        let customIndexPath = NSIndexPath.init(row: 3, section: 0)
        let cell123 = tableViewOutlet.cellForRow(at: customIndexPath as IndexPath) as? GetElectricityBillTableViewCell
        if cell123?.textFieldOutlet.leftView == nil {
            NameValidationContainerView?.subviews.forEach { $0.removeFromSuperview() }
            NameValidationContainerView?.removeFromSuperview()
            NameViewTransparentLayer?.removeFromSuperview()
            NameValidationShownOnce = false
        } else {
            NameValidationContainerView?.subviews.forEach { $0.removeFromSuperview() }
            NameValidationContainerView?.removeFromSuperview()
            NameViewTransparentLayer?.removeFromSuperview()
        }
        self.tableViewOutlet.isScrollEnabled = true
        let indexPath = tableViewOutlet.indexPath(for: cell)
        cell.textFieldOutlet.text = ""
        if indexPath?.row == 2
        {
            if sectionCountArray.count == 1 {
                DispatchQueue.main.async {
                    var arrIndexPath = [IndexPath]()
                    
                    if self.rowCountArray.count > 3 {
                        for count in 3 ... self.rowCountArray.count - 1 {
                            let indexPath = IndexPath(row: count, section: 0)
                            arrIndexPath.append(indexPath)
                        }
                    }
                    
                    self.rowCountArray.removeAll()
                    self.rowCountArray.append("")
                    self.rowCountArray.append("")
                    self.rowCountArray.append("")
                    
                    self.tableViewOutlet.beginUpdates()
                    self.tableViewOutlet.deleteRows(at: arrIndexPath, with: .automatic)
                    self.tableViewOutlet.endUpdates()
                    
                    self.DataSourceType = DataSourceOptions.DataNotInServerDataSource
                    
                    self.SubmitButton?.frame.origin.y = self.view.frame.height - 50
                    self.view.endEditing(true)
                    
                    self.SubmitButton?.isHidden = true
                    self.tableViewOutlet.tableFooterView = nil
                    
                    var computerCode = ""
                    if self.MeterVsComputerCodeType == .ComputerCodeNo {
                        computerCode = self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Computer Code Number")] ?? ""
                    }
                    
                    self.DataNotInServerTextFieldDict.removeAll()
                    self.DataInServerTextFieldDict.removeAll()
                    
                    //        self.tableViewOutlet.reloadData()
                    
                    
                    self.setupTableDataSource()
                    
                    self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Division")] = SelectedDivision
                    self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Township")] = SelectedTownship
                    
                    self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Division")] = SelectedDivision
                    self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Township")] = SelectedTownship
                    
                    if self.MeterVsComputerCodeType == .ComputerCodeNo {
                        self.DataNotInServerTextFieldDict[self.DataNotInServerDescriptionArray[7]] = computerCode
                        self.DataInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Computer Code Number")] = computerCode
                    }
                    
                    
                    MeterBillAmountParam = ""
                    AdjustAmountParam = ""
                    
                    self.MeterNoString = ""
                    self.computerCodeString = ""
                    
                    self.tableViewOutlet.tableFooterView = UIView()
                    
                    if self.showHideComputerCodeField == 1
                    {
                        let indexPath = IndexPath(row: 0, section: 1)
                        
                        self.sectionUpdate = 1
                        if self.sectionCountArray.count == 1{
                            self.sectionCountArray.append("")
                            self.tableViewOutlet.beginUpdates()
                            self.tableViewOutlet.insertSections(IndexSet.init(integer: .init(1)), with: .automatic)
                            self.tableViewOutlet.insertRows(at: [indexPath], with: .automatic)
                            self.tableViewOutlet.endUpdates()
                        }
                        
                    }
                    self.tableViewOutlet.reloadData()
                    
                    let indexPath = IndexPath(row: 2, section: 0)
                    let cell = self.tableViewOutlet.cellForRow(at: indexPath) as? GetElectricityBillTableViewCell
                    cell?.clearButton.isHidden = true
                    cell?.textFieldOutlet.becomeFirstResponder()
                    
                }
            } else {
                cell.textFieldOutlet.text = ""
                cell.clearButton.isHidden = true
            }
            
            
        } else if indexPath?.row == 3 {
            nameClear = true
            NameValidationContainerView?.subviews.forEach { $0.removeFromSuperview() }
            NameValidationContainerView?.removeFromSuperview()
            NameViewTransparentLayer?.removeFromSuperview()
            NameValidationShownOnce = false
            cell.textFieldOutlet.text = ""
            cell.textFieldOutlet.leftView = nil
            cell.clearButton.isHidden = true
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Consumer Name (as on Bill)")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Ledger Number")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Meter Read Date")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Tariff")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Pay For")] = ""
            self.DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Computer Code Number")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number")] = commonPrefixMobileNumber
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")] = ""
            self.deleteBottomRows(AfterRowIndex: (indexPath?.row)!)
        } else if indexPath?.row == 9 {
            cell.clearButton.isHidden = true
            cell.operatorLabelOutlet.text = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Contact Mobile Number")] = commonPrefixMobileNumber
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Enter Bill Amount")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Confirm Bill Amount")] = ""
            DataNotInServerTextFieldDict[appDelegate.getlocaLizationLanguage(key: "Remark")] = ""
            self.deleteBottomRows(AfterRowIndex: (indexPath?.row)!)
        } else {
            self.deleteBottomRows(AfterRowIndex: (indexPath?.row)!)
        }
    }
}

extension String {
    
    var containsEmoji: Bool {
        for scalar in unicodeScalars {
            switch scalar.value {
            case 0x1F600...0x1F64F, // Emoticons
            0x1F300...0x1F5FF, // Misc Symbols and Pictographs
            0x1F680...0x1F6FF, // Transport and Map
            0x2600...0x26FF,   // Misc symbols
            0x2700...0x27BF,   // Dingbats
            0xFE00...0xFE0F,   // Variation Selectors
            0x1F900...0x1F9FF, // Supplemental Symbols and Pictographs
            0x1F1E6...0x1F1FF: // Flags
                return true
            default:
                continue
            }
        }
        return false
    }
    
}



extension GetElectricityBillVC : ContactPickerDelegate{
    
    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]) {
        for contact in contacts {
            decodeContact(contact: contact, isMulti: true)
        }
    }
    
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) {
        decodeContact(contact: contact, isMulti: false)
    }
    
    func decodeContact(contact: ContactPicker, isMulti: Bool) {
        
        let phoneNumberCount = contact.phoneNumbers.count
        
        if phoneNumberCount >= 1 {
            _ = "\(contact.phoneNumbers[0].phoneNumber)"
            
            if(isMyanmarNumber(contact.phoneNumbers[0].phoneNumber,withCountryCode: "+95"))
            {
                var strMobNo = contact.phoneNumbers[0].phoneNumber
                
                let cDetails = identifyCountry(withPhoneNumber: strMobNo) // 1 -> flag, 0 -> code
                
                if strMobNo.hasPrefix(cDetails.countryCode) {
                    strMobNo = strMobNo.replacingOccurrences(of: cDetails.countryCode, with: "")
                }
                if let charEncode = String(strMobNo.first!).addingPercentEncoding(withAllowedCharacters: .decimalDigits) {
                    if charEncode == "%C2%A0" {
                        _ = strMobNo.remove(at: String.Index.init(encodedOffset: 0))
                    }
                }
                
                let strMobileNoNew = "0" + strMobNo
                let customIndex = NSIndexPath.init(row: 9, section: 0)
                self.electricityCellRef  = self.tableViewOutlet.cellForRow(at: customIndex as IndexPath) as? GetElectricityBillTableViewCell
                self.electricityCellRef?.textFieldOutlet.text = strMobileNoNew
                self.electricityCellRef?.clearButton.isHidden = false
                DataNotInServerTextFieldDict[DataNotInServerDescriptionArray[9]] = strMobileNoNew
                self.electricityCellRef?.operatorLabelOutlet.isHidden = false
                self.electricityCellRef?.operatorLabelOutlet.text = validObj.getNumberRangeValidation(strMobileNoNew).operator
                showNextRow()
                let customIndexPath = NSIndexPath.init(row: 10, section: 0)
                electricityCellRef = tableViewOutlet.cellForRow(at: customIndexPath as IndexPath) as? GetElectricityBillTableViewCell
                electricityCellRef?.textFieldOutlet.becomeFirstResponder()
                
            }
            else
            {
                alertViewObj.wrapAlert(title: nil, body:"Please select Myanmar number.".localized, img: #imageLiteral(resourceName: "bill_splitter"))
                
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                    
                }
                alertViewObj.showAlert(controller: self)
            }
        }
        else {
            _ = ContactGlobalConstants.Strings.phoneNumberNotAvaialable
        }
        
     
    }
    
}
