//
//  PromotionsModel.swift
//  OK
//
//  Created by Uma Rajendran on 11/7/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PromotionsModel: NSObject, NSCoding {
    var shop_Id                             : String? = ""
    var shop_Name                           : String? = ""
    var shop_City                           : String? = ""
    var shop_Township                       : String? = ""
    var shop_Division                       : String? = ""
    var shop_isPromotion                    : Bool = false
    var shop_InMeter                        : Double?
    var shop_OpenTime                       : String? = ""
    var shop_CloseTime                      : String? = ""
    var shop_Holidays                       : [String]?
    var shop_AgentSettingCashInOut          : Any? = nil
    var shop_AgentId                        : String? = ""
    var shop_AgentName                      : String? = ""
    var shop_Phonenumber                    : String? = ""
    var shop_AgentType                      : String? = ""
    var shop_Address                        : ShopAddress?
    var shop_GeoLocation                    : ShopGeoLocation?
    var shop_Category                       : ShopBusinessCategory?
    var shop_Templates                      = [ShopTemplate]()
    var shop_IsActive                       : Bool = false
    var isAgent                             : Bool = false
    var shop_Email                      : String? = ""
    var shop_Latitude                      : Double? = 0.0
    var shop_Longtitude                      : Double? = 0.0
    var shop_NewAddress                     : String? = ""

    init(model: NearByServicesModel) {
        super.init()
        
        self.shop_OpenTime = model.openingTime
        self.shop_CloseTime = model.closingTime
        self.shop_Phonenumber = model.phoneNumber
        self.shop_Id = model.id
        self.shop_GeoLocation = ShopGeoLocation.init(ordinate: model.nearBy_GeoLocation)
        
        self.shop_Name = model.businessName
        if(model.businessName?.count == 0){
          self.shop_Name = model.firstName
        }
        self.shop_AgentName = model.firstName
        self.shop_InMeter = 0.0
        
        //Category
        self.shop_Category = ShopBusinessCategory.init(category: model.category!,subCategory: model.subCategory!)
        self.shop_Address = ShopAddress.init(addLine1: model.addressLine1!,addLine2: model.addressLine2!)
        self.shop_Email = model.email

    }
    
    
    init(model: NearByServicesNewModel) {
        super.init()
        
        self.shop_OpenTime = model.UserContactData?.OfficeHourFrom
        self.shop_CloseTime = model.UserContactData?.OfficeHourTo
        self.shop_Phonenumber = model.UserContactData?.PhoneNumber
        self.shop_Id = model.UserContactData?.Id
        self.shop_Latitude = model.UserContactData?.LocationNewData?.Latitude
        self.shop_Longtitude = model.UserContactData?.LocationNewData?.Longitude

        self.shop_Name = model.UserContactData?.BusinessName
        if(model.UserContactData?.BusinessName!.count == 0)
        {
            self.shop_Name = model.UserContactData?.FirstName!
        }
        self.shop_AgentName = model.UserContactData?.FirstName!
        self.shop_InMeter = 0.0
        
        //Category
        self.shop_Category = ShopBusinessCategory.init(category: "",subCategory: "")
        self.shop_NewAddress = model.UserContactData?.Address
        self.shop_Address = ShopAddress.init(addLine1: model.UserContactData!.AddressLine1!,addLine2: model.UserContactData!.AddressLine2!)

        
        self.shop_Email = ""
        
    }
    
    init(dictionary: Dictionary<String, Any>) {
        super.init()
        if let title = dictionary["shopId"] as? String {
            self.shop_Id = title
        }
        if let title = dictionary["shopName"] as? String {
            self.shop_Name = title
        }
        if let title = dictionary["Email"] as? String {
            self.shop_Email = title
        }
        if let title = dictionary["shopCity"] as? String {
            self.shop_City = title
        }
        if let title = dictionary["shopTownShip"] as? String {
            self.shop_Township = title
        }
        if let title = dictionary["shopDivision"] as? String {
            self.shop_Division = title
        }
        if let promotions = dictionary["promotion"] as? [Any] {
            for eachPromo in promotions {
                if let dics = eachPromo as? Dictionary<String, Any> {
                    let promotionTemplate = ShopTemplate.init(dictionary: dics)
                    shop_Templates.append(promotionTemplate)
                }
            }
        }
        
        if let title = dictionary["isPromotion"] as? Bool {
            self.shop_isPromotion = title
        }
        if let title = dictionary["shopInMeter"] as? Double {
            self.shop_InMeter = title / 1000.0
        }
        if let title = dictionary["shopOpenTime"] as? String {
            self.shop_OpenTime = title
        }
        if let title = dictionary["shopCloseTime"] as? String {
            self.shop_CloseTime = title
        }
        if let title = dictionary["shopHolidays"] as? [String] {
            self.shop_Holidays = title
        }
        if let title = dictionary["agentSettingsCashInCashOut"]  {
            self.shop_AgentSettingCashInOut = title
        }
        if let title = dictionary["agentId"] as? String {
            self.shop_AgentId = title
        }
        if let title = dictionary["agentName"] as? String {
            self.shop_AgentName = title
        }
        if let title = dictionary["phoneNumber"] as? String {
            self.shop_Phonenumber = title
        }
        if let title = dictionary["agentType"] as? String {
            self.shop_AgentType = title
            self.checkAgentORNot(agenttype: title)
        }
        if let title = dictionary["address"] as? Dictionary<String,Any> {
            self.shop_Address = ShopAddress.init(dictionary: title)
        }
        if let title = dictionary["geoLocationShop"] as? Dictionary<String,Any> {
            self.shop_GeoLocation = ShopGeoLocation.init(dictionary: title)
        }
        if let title = dictionary["businessCategory"] as? Dictionary<String,Any> {
            self.shop_Category = ShopBusinessCategory.init(dictionary: title)
        }
        if let title = dictionary["isActive"] as? Bool {
            self.shop_IsActive = title
        }
        
       
    }
    
    func checkAgentORNot(agenttype: String) {
        if agenttype == "AGENT" {
            self.isAgent = true
        }else {
            self.isAgent = false
        }
    }
  
    required init(coder aDecoder: NSCoder) {
        println_debug("main promotion model class init with coder calling")
        shop_Id = aDecoder.decodeObject(forKey: "shop_Id") as? String
        shop_Name = aDecoder.decodeObject(forKey: "shop_Name") as? String
        shop_City = aDecoder.decodeObject(forKey: "shop_City") as? String
        shop_Township = aDecoder.decodeObject(forKey: "shop_Township") as? String
        shop_Division = aDecoder.decodeObject(forKey: "shop_Division") as? String
//        shop_isPromotion = aDecoder.decodeObject(forKey: "shop_isPromotion") as! Bool
        shop_isPromotion = aDecoder.decodeBool(forKey: "shop_isPromotion")
        shop_InMeter = aDecoder.decodeObject(forKey: "shop_InMeter") as? Double
        shop_OpenTime = aDecoder.decodeObject(forKey: "shop_OpenTime") as? String
        shop_CloseTime = aDecoder.decodeObject(forKey: "shop_CloseTime") as? String
        shop_Holidays = aDecoder.decodeObject(forKey: "shop_Holidays") as? [String]
        shop_AgentSettingCashInOut = aDecoder.decodeObject(forKey: "shop_AgentSettingCashInOut")
        shop_AgentId = aDecoder.decodeObject(forKey: "shop_AgentId") as? String
        shop_AgentName = aDecoder.decodeObject(forKey: "shop_AgentName") as? String
        shop_Phonenumber = aDecoder.decodeObject(forKey: "shop_Phonenumber") as? String
        shop_AgentType = aDecoder.decodeObject(forKey: "shop_AgentType") as? String
        shop_Address = aDecoder.decodeObject(forKey: "shop_Address") as? ShopAddress
        shop_GeoLocation = aDecoder.decodeObject(forKey: "shop_GeoLocation") as? ShopGeoLocation
        shop_Category = aDecoder.decodeObject(forKey: "shop_Category") as? ShopBusinessCategory
        shop_Templates = (aDecoder.decodeObject(forKey: "shop_Templates") as? [ShopTemplate])!
        shop_IsActive = aDecoder.decodeBool(forKey: "shop_IsActive")
        isAgent = aDecoder.decodeBool(forKey: "isAgent")
      
    }
    
    func encode(with aCoder: NSCoder) {
        println_debug("main promotion model class encode with coder calling")
        aCoder.encode(shop_Id, forKey: "shop_Id")
        aCoder.encode(shop_Name, forKey: "shop_Name")
        aCoder.encode(shop_City, forKey: "shop_City")
        aCoder.encode(shop_Township, forKey: "shop_Township")
        aCoder.encode(shop_Division, forKey: "shop_Division")
        aCoder.encode(shop_isPromotion, forKey: "shop_isPromotion")
        aCoder.encode(shop_InMeter, forKey: "shop_InMeter")
        aCoder.encode(shop_OpenTime, forKey: "shop_OpenTime")
        aCoder.encode(shop_CloseTime, forKey: "shop_CloseTime")
        aCoder.encode(shop_Holidays, forKey: "shop_Holidays")
        aCoder.encode(shop_AgentSettingCashInOut, forKey: "shop_AgentSettingCashInOut")
        aCoder.encode(shop_AgentId, forKey: "shop_AgentId")
        aCoder.encode(shop_AgentName, forKey: "shop_AgentName")
        aCoder.encode(shop_Phonenumber, forKey: "shop_Phonenumber")
        aCoder.encode(shop_AgentType, forKey: "shop_AgentType")
        aCoder.encode(shop_Address, forKey: "shop_Address")
        aCoder.encode(shop_GeoLocation, forKey: "shop_GeoLocation")
        aCoder.encode(shop_Category, forKey: "shop_Category")
        aCoder.encode(shop_Templates, forKey: "shop_Templates")
        aCoder.encode(shop_IsActive, forKey: "shop_IsActive")
        aCoder.encode(isAgent, forKey: "isAgent")
    }
    
}

class ShopGeoLocation: NSObject, NSCoding {
    var lattitude     : Double?
    var longitude     : Double?
    init(dictionary: Dictionary<String, Any>) {
        super.init()
        if let title = dictionary["latitude"] as? Double {
            self.lattitude = title
        }
        if let title = dictionary["longitude"] as? Double {
            self.longitude = title
        }
    }
    
    init(ordinate: NearByGeoLocation?) {
        super.init()
        self.lattitude = ordinate?.lattitude
        self.longitude = ordinate?.longitude
    }
    
    required init(coder aDecoder: NSCoder) {
        println_debug("ShopGeoLocation class init with coder calling")

        lattitude = aDecoder.decodeObject(forKey: "lattitude") as? Double
        longitude = aDecoder.decodeObject(forKey: "longitude") as? Double
    }
    
    func encode(with aCoder: NSCoder) {
        println_debug("ShopGeoLocation class encode with coder calling")

        aCoder.encode(lattitude, forKey: "lattitude")
        aCoder.encode(longitude, forKey: "longitude")
    }
}

class ShopTemplate: NSObject, NSCoding {
    var promotion_Id : String? = ""
    var template_Id  : String? = ""
    var template_Header : String? = ""
    var template_Name : String? = ""
    init(dictionary: Dictionary<String, Any>) {
        super.init()
        if let title = dictionary["id"] as? String {
            self.promotion_Id = title
        }
        if let title = dictionary["templateId"] as? String {
            self.template_Id = title
        }
        if let title = dictionary["header"] as? String {
            self.template_Header = title
        }
        if let title = dictionary["templateName"] as? String {
            self.template_Name = title
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        println_debug("ShopTemplate class init with coder calling")

        promotion_Id = aDecoder.decodeObject(forKey: "promotion_Id") as? String
        template_Id = aDecoder.decodeObject(forKey: "template_Id") as? String
        template_Header = aDecoder.decodeObject(forKey: "template_Header") as? String
        template_Name = aDecoder.decodeObject(forKey: "template_Name") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        println_debug("ShopTemplate class encode with coder calling")

        aCoder.encode(promotion_Id, forKey: "promotion_Id")
        aCoder.encode(template_Id, forKey: "template_Id")
        aCoder.encode(template_Header, forKey: "template_Header")
        aCoder.encode(template_Name, forKey: "template_Name")
    }
}

class ShopAddress: NSObject, NSCoding {
    var addressLine1        : String? = ""
    var addressLine2        : String? = ""
    var townShip            : ShopAddressTownShip?
    var state               : ShopAddressState?
    var country             : String? = ""
    
    
    init(dictionary: Dictionary<String,Any>) {
        super.init()
        if let title = dictionary["addressLine1"] as? String {
            self.addressLine1 = title
        }
        if let title = dictionary["addressLine2"] as? String {
            self.addressLine2 = title
        }
        if let title = dictionary["townShip"] as? Dictionary<String, Any> {
            self.townShip = ShopAddressTownShip.init(dictionary: title)
        }
        if let title = dictionary["state"] as? Dictionary<String, Any> {
            self.state = ShopAddressState.init(dictionary: title)
        }
        if let title = dictionary["country"] as? String {
            self.country = title
        }
    }
    
    init(addLine1:String, addLine2:String){
        super.init()
        
        self.addressLine1 = addLine1
        self.addressLine2 = addLine2
    }
    
    required init(coder aDecoder: NSCoder) {
        println_debug("ShopAddress class init with coder calling")

        addressLine1 = aDecoder.decodeObject(forKey: "addressLine1") as? String
        addressLine2 = aDecoder.decodeObject(forKey: "addressLine2") as? String
        townShip = aDecoder.decodeObject(forKey: "townShip") as? ShopAddressTownShip
        state = aDecoder.decodeObject(forKey: "state") as? ShopAddressState
        country = aDecoder.decodeObject(forKey: "country") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        println_debug("ShopAddress class encode with coder calling")

        aCoder.encode(addressLine1, forKey: "addressLine1")
        aCoder.encode(addressLine2, forKey: "addressLine2")
        aCoder.encode(townShip, forKey: "townShip")
        aCoder.encode(state, forKey: "state")
        aCoder.encode(country, forKey: "country")
    }
}

class ShopAddressTownShip: NSObject, NSCoding {
    var townshipCode        : String? = ""
    var townshipNameEN      : String? = ""
    var townshipNameMY      : String? = ""
    
    init(dictionary: Dictionary<String, Any>) {
        super.init()
        if let title = dictionary["code"] as? String {
            self.townshipCode = title
        }
        if let title = dictionary["name"] as? String {
            self.townshipNameEN = title
        }
        if let title = dictionary["burmeseName"] as? String {
            self.townshipNameMY = title
        }
    }
    
    
    required init(coder aDecoder: NSCoder) {
        println_debug("ShopAddressTownShip class init with coder calling")

        townshipCode = aDecoder.decodeObject(forKey: "townshipCode") as? String
        townshipNameEN = aDecoder.decodeObject(forKey: "townshipNameEN") as? String
        townshipNameMY = aDecoder.decodeObject(forKey: "townshipNameMY") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        println_debug("ShopAddressTownShip class encode with coder calling")

        aCoder.encode(townshipCode, forKey: "townshipCode")
        aCoder.encode(townshipNameEN, forKey: "townshipNameEN")
        aCoder.encode(townshipNameMY, forKey: "townshipNameMY")
    }
}

class ShopAddressState: NSObject, NSCoding {
    var isDivision      : Bool = false
    var stateCode       : String? = ""
    var stateNameEN     : String? = ""
    var stateNameMY     : String? = ""
    
    init(dictionary: Dictionary<String, Any>) {
        super.init()
        if let title = dictionary["isDivision"] as? Bool {
            self.isDivision = title
        }
        if let title = dictionary["code"] as? String {
            self.stateCode = title
        }
        if let title = dictionary["name"] as? String {
            self.stateNameEN = title
        }
        if let title = dictionary["burmeseName"] as? String {
            self.stateNameMY = title
        }
    }
    
   
    required init(coder aDecoder: NSCoder) {
        println_debug("ShopAddressState class init with coder calling")

        isDivision = aDecoder.decodeBool(forKey: "isDivision")
        stateCode = aDecoder.decodeObject(forKey: "stateCode") as? String
        stateNameEN = aDecoder.decodeObject(forKey: "stateNameEN") as? String
        stateNameMY = aDecoder.decodeObject(forKey: "stateNameMY") as? String

    }
    
    func encode(with aCoder: NSCoder) {
        println_debug("ShopAddressState class encode with coder calling")

        aCoder.encode(isDivision, forKey: "isDivision")
        aCoder.encode(stateCode, forKey: "stateCode")
        aCoder.encode(stateNameEN, forKey: "stateNameEN")
        aCoder.encode(stateNameMY, forKey: "stateNameMY")
    }
}

class ShopBusinessCategory: NSObject, NSCoding {
    var categoryCode        : String? = ""
    var categoryNameEN      : String? = ""
    var categoryNameMY      : String? = ""
    var categoryLogo        : String? = ""
    var businessType        : ShopBusinessType?
    
    init(dictionary: Dictionary<String, Any>) {
        super.init()
        if let title = dictionary["code"] as? String {
            self.categoryCode = title
        }
        if let title = dictionary["name"] as? String {
            self.categoryNameEN = title
        }
        if let title = dictionary["burmeseName"] as? String {
            self.categoryNameMY = title
        }
        if let title = dictionary["logo"] as? String {
            self.categoryLogo = title
        }
        if let title = dictionary["businessType"] as? Dictionary<String, Any> {
            self.businessType = ShopBusinessType.init(dictionary: title)
        }
    }
    
    init(category:String, subCategory:String){
        super.init()
       
        self.categoryNameEN = category
        self.categoryNameMY = category
        self.businessType = ShopBusinessType.init(subCategory: subCategory)
    }
    
    required init(coder aDecoder: NSCoder) {
        println_debug("ShopBusinessCategory class init with coder calling")

        categoryCode = aDecoder.decodeObject(forKey: "categoryCode") as? String
        categoryNameEN = aDecoder.decodeObject(forKey: "categoryNameEN") as? String
        categoryNameMY = aDecoder.decodeObject(forKey: "categoryNameMY") as? String
        categoryLogo = aDecoder.decodeObject(forKey: "categoryLogo") as? String
        businessType = aDecoder.decodeObject(forKey: "businessType") as? ShopBusinessType
     }
    
    func encode(with aCoder: NSCoder) {
        println_debug("ShopBusinessCategory class encode with coder calling")

        aCoder.encode(categoryCode, forKey: "categoryCode")
        aCoder.encode(categoryNameEN, forKey: "categoryNameEN")
        aCoder.encode(categoryNameMY, forKey: "categoryNameMY")
        aCoder.encode(categoryLogo, forKey: "categoryLogo")
        aCoder.encode(businessType, forKey: "businessType")
     }
}

class ShopBusinessType: NSObject, NSCoding {
    var businessType_Code       : String? = ""
    var businessType_NameEN     : String? = ""
    var businessType_NameMY     : String? = ""
    var businessType_Logo       : String? = ""
    init(dictionary: Dictionary<String, Any>) {
        super.init()
        if let title = dictionary["code"] as? String {
            self.businessType_Code = title
        }
        if let title = dictionary["name"] as? String {
            self.businessType_NameEN = title
        }
        if let title = dictionary["burmeseName"] as? String {
            self.businessType_NameMY = title
        }
        if let title = dictionary["logo"] as? String {
            self.businessType_Logo = title
        }
    }
    
    init(subCategory: String) {
        super.init()
        
        self.businessType_NameEN = subCategory
      
        self.businessType_NameMY = subCategory
        
    }

    required init(coder aDecoder: NSCoder) {
        println_debug("ShopBusinessType class init with coder calling")

        businessType_Code = aDecoder.decodeObject(forKey: "businessType_Code") as? String
        businessType_NameEN = aDecoder.decodeObject(forKey: "businessType_NameEN") as? String
        businessType_NameMY = aDecoder.decodeObject(forKey: "businessType_NameMY") as? String
        businessType_Logo = aDecoder.decodeObject(forKey: "businessType_Logo") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        println_debug("ShopBusinessType class encode with coder calling")

        aCoder.encode(businessType_Code, forKey: "businessType_Code")
        aCoder.encode(businessType_NameEN, forKey: "businessType_NameEN")
        aCoder.encode(businessType_NameMY, forKey: "businessType_NameMY")
        aCoder.encode(businessType_Logo, forKey: "businessType_Logo")
    }
}

