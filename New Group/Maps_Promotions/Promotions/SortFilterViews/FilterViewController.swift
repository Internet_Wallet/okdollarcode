//
//  FilterViewController.swift
//  Promotions
//
//  Created by Uma Rajendran on 10/28/17.
//  Copyright © 2017 Uma Rajendran. All rights reserved.
//

import UIKit

protocol FilterViewDelegate: class  {
    func didSelectFilterOption(filteredList: [Any], selectedFilter: Any?)
}

class FilterViewController: UIViewController {

    @IBOutlet weak var headerLabel      : UILabel!
    {
        didSet
        {
            headerLabel.text = headerLabel.text?.localized
        }
    }
    @IBOutlet weak var filterTable      : UITableView!
    @IBOutlet weak var resigningView    : UIView!
    weak var delegate                   : FilterViewDelegate?
   
    var isExpandCategories: Bool = true
    var previousSelectedFilterData: Any?
    var filterDataList: [Any]?
    var filterShowList: [Any]?
    var viewFrom: FromView?
    var currentNearByView: NearByView?
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadUI()
        loadInitialize()
        updateLocalizations()
    }
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
            isExpandCategories = true
            removeDuplicatePromotions()
            loadTableView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadUI() {
        headerHeightConstraint.constant = kHeaderHeight
        self.view.backgroundColor = kGradientGreyColor
        resigningView.backgroundColor = UIColor.clear
    }
    
    func loadInitialize() {
      
    }
    
    func updateLocalizations() {
        headerLabel.font = UIFont.init(name: appFont, size: 17)
    }
    
    func loadTableView() {
//        filterTable.register(UITableViewCell.self, forCellReuseIdentifier: "DefaultCell")
        filterTable.register(UINib(nibName: "SortCell", bundle: nil), forCellReuseIdentifier: "sortcellidentifier")

        filterTable.register(UINib(nibName: "DivisionCell", bundle: nil), forCellReuseIdentifier: "divisioncellidentifier")
        filterTable.tableFooterView = UIView(frame: CGRect.zero)
        filterTable.reloadData()
    }
    
    func removeDuplicatePromotions() {
        guard viewFrom == .promotion else {
            return
        }
        var newFilteredData: [PromotionsModel] = []
        var checkSet = Set<String>()
        if let filterlist = filterDataList as? [PromotionsModel] {
            for promotion in filterlist {
                if !checkSet.contains((promotion.shop_Category?.businessType?.businessType_Code!)!) {
                    newFilteredData.append(promotion)
                    checkSet.insert((promotion.shop_Category?.businessType?.businessType_Code!)!)
                }
            }
            filterShowList?.removeAll()
            filterShowList = newFilteredData
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if touch.view == self.resigningView {
              resignTheFilterView()
            }
         
        }
        super.touchesBegan(touches, with: event)
    }
    
    func resignTheFilterView() {
        if let presentingView = self.presentingViewController {
            presentingView.dismiss(animated: true, completion: nil)
        }
    }

}

extension FilterViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowCount = 1
        if viewFrom == .promotion {
            if section == 1 && isExpandCategories{
                rowCount = (filterShowList?.count)! + 1
            }
        }else {
            if section == 1 && isExpandCategories{
                rowCount = 2
            }
        }
        
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 0) {
            
            let defaultCell = tableView.dequeueReusableCell(withIdentifier: "sortcellidentifier", for: indexPath) as! SortCell
            defaultCell.wrap_Data(title: "Default".localized, imgName: "")
            return defaultCell
            
        }else {
            if indexPath.row == 0 {
                
                let categoriesHeaderCell = tableView.dequeueReusableCell(withIdentifier: "divisioncellidentifier", for: indexPath) as! DivisionCell  // just reusing the division cell design here
                categoriesHeaderCell.expandImgView.isHidden =  false
                let image = (isExpandCategories == true) ? UIImage.init(named: "up_arrow.png") : UIImage.init(named: "down_arrow.png")
                categoriesHeaderCell.wrapData(text: "Categories".localized, img: image!)
                return categoriesHeaderCell
                
            }else {
                
                
                let categoriesListCell = tableView.dequeueReusableCell(withIdentifier: "sortcellidentifier", for: indexPath) as! SortCell
                if viewFrom == .promotion {
                    if let filterList = filterShowList![indexPath.row - 1] as? PromotionsModel {
                        let promotionData = filterList //as! PromotionsModel
                        let tab = "    "
                        
                        if(appDel.currentLanguage == "my")
                        {
                            categoriesListCell.contentLbl.text = "\(tab)\((promotionData.shop_Category?.businessType?.businessType_NameMY)!)"
                            let name = "\(tab)\((promotionData.shop_Category?.businessType?.businessType_NameMY)!)"
                            var imageName = ""
                            if let prevFilterData = previousSelectedFilterData as? PromotionsModel {
                                if prevFilterData.shop_Id! == promotionData.shop_Id {
                                    // here show the selected image
                                    imageName =  "success"
                                }
                            }
                            categoriesListCell.wrap_Data(title: name, imgName: imageName)
                        }
                        else
                        {
                            categoriesListCell.contentLbl.text = "\(tab)\((promotionData.shop_Category?.businessType?.businessType_NameEN)!)"
                            let name = "\(tab)\((promotionData.shop_Category?.businessType?.businessType_NameEN)!)"
                            var imageName = ""
                            if let prevFilterData = previousSelectedFilterData as? PromotionsModel {
                                if prevFilterData.shop_Id! == promotionData.shop_Id {
                                    // here show the selected image
                                    imageName =  "success"
                                }
                            }
                            categoriesListCell.wrap_Data(title: name, imgName: imageName)
                        }
                        
                        
                    }
                }else {
                    if currentNearByView == .okAgentsView {
                        let tab = "    "
                        categoriesListCell.contentLbl.text = "\(tab)" + "OK$ Agents".localized
                        categoriesListCell.imgV.image = UIImage.init(named: "")
                        
                    }else if currentNearByView == .okOfficesView  {
                        let tab = "    "
                        categoriesListCell.contentLbl.text = "\(tab)" + "OK$ Office".localized
                        categoriesListCell.imgV.image = UIImage.init(named: "")
                        
                    }
                }
                
                return categoriesListCell
                
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            
            self.delegate?.didSelectFilterOption(filteredList: filterDataList!,selectedFilter: nil)
            resignTheFilterView()
        }else if (indexPath.section == 1 && indexPath.row == 0) {
            
            isExpandCategories = !isExpandCategories
            filterTable.reloadSections(NSIndexSet(index: indexPath.section) as IndexSet, with: .fade)
        }else {
            
            if viewFrom == .promotion {
                
                if let filtershowlistData = filterShowList![indexPath.row - 1] as? PromotionsModel {
                    var newFilteredData: [PromotionsModel] = []
                    let selectedPromotion = filtershowlistData //as! PromotionsModel
                    if let filterlist = filterDataList as? [PromotionsModel] {
                        for promotion in filterlist {
                            if promotion.shop_Category?.businessType?.businessType_NameEN == selectedPromotion.shop_Category?.businessType?.businessType_NameEN {
                                newFilteredData.append(promotion)
                            }
                        }
                    }
                    self.delegate?.didSelectFilterOption(filteredList: newFilteredData ,selectedFilter: selectedPromotion)
                }
                
            }else {
                // from cashincashout
                self.delegate?.didSelectFilterOption(filteredList: filterDataList! ,selectedFilter: nil)
            }
           
            resignTheFilterView()
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}


