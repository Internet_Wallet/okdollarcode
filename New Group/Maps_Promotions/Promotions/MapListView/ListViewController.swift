//
//  ListViewController.swift
//  Promotions
//
//  Created by Uma Rajendran on 10/24/17.
//  Copyright © 2017 Uma Rajendran. All rights reserved.
//

import UIKit

class ListViewController: MapBaseViewController {

    // maplistcell have to use in this tableview
    
    @IBOutlet weak var listTableView: UITableView!
    var promotionsList = [PromotionsModel]()

    var nav : UINavigationController?
    @IBOutlet weak var infoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadUI()
        loadInitialize()
        updateLocalizations()
    }
    
   
    
    func loadUI() {
        println_debug("list view frame :::: \(self.view.frame.origin.x) ,, \(self.view.frame.origin.y) ,, \(self.view.frame.size.width) ,, \(self.view.frame.size.height)")
    }

    func loadInitialize() {
        let nib = UINib.init(nibName: "MapListCell", bundle: nil)
        listTableView.register(nib, forCellReuseIdentifier: "maplistcellidentifier")
        listTableView.tableFooterView = UIView(frame: CGRect.zero)
        listTableView.rowHeight = UITableView.automaticDimension
        listTableView.estimatedRowHeight = 250
        listTableView.reloadData()
    }
    
    func updateLocalizations() {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showHideInfoLabel(isShow: Bool, content: String) {
        DispatchQueue.main.async {
            if isShow {
                self.infoLabel.isHidden = false
                self.infoLabel.text = content
            }else {
                self.infoLabel.isHidden = true
            }

        }

    }
    
    func getTime(timeStr: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "HH:mm:ss"
        //dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        var hour = ""
        if let time = dateFormatter.date(from: timeStr) {
            dateFormatter.dateFormat = "hh:mm a"
            hour = dateFormatter.string(from: time)
        }
        return hour
    }

    @objc func callBtnAction(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to: self.listTableView)
        if let indexPath = self.listTableView.indexPathForRow(at: buttonPosition), promotionsList.count > 0 {
            let selectedpromotion = promotionsList[indexPath.row]
            let promotion = selectedpromotion
            if let phonenumber = promotion.shop_Phonenumber {
                if phonenumber.count > 0 {
                    var mobNumber = getPhoneNumberByFormat(phoneNumber: phonenumber)
                    mobNumber = mobNumber.removingWhitespaces()
                    let phone = "tel://\(mobNumber)"
                    if let url = URL.init(string: phone) {
                        UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                    }
                }
            }
        }else {
            alertViewObj.wrapAlert(title: nil, body: "No Phonenumber Exists".localized, img: nil)
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                
            })
            alertViewObj.showAlert(controller: self)
        }
    }
    
}

extension ListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("Listviewpage page------\(promotionsList.count)")
        return promotionsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let listCell = tableView.dequeueReusableCell(withIdentifier: "maplistcellidentifier", for: indexPath) as! MapListCell
        let promotion = promotionsList[indexPath.row]
        listCell.callBtn.addTarget(self, action: #selector(callBtnAction(_:)), for: UIControl.Event.touchUpInside)
        listCell.photoImgView.image = UIImage.init(named: "btc")
        listCell.nameLbl.text = promotion.shop_Name
        print("promotion table\(String(describing: promotion.shop_Name))")
//        listCell.rateImgView.image = UIImage.init(named: "star_five")
        
//        for img in listCell.rateImgViewList {
//            img.isHighlighted = false
//        }
        
        let agentCodenew  = getPlusWithDestinationNum(promotion.shop_Phonenumber!, withCountryCode: "+95")
        listCell.phonenumberLbl.text = agentCodenew
        
        if let openTime = promotion.shop_OpenTime , let closeTime = promotion.shop_CloseTime {
            if openTime.count > 0 && closeTime.count > 0 {
                listCell.timeImgView.isHidden = false
                listCell.timeLbl.isHidden = false
                listCell.timeLblHeightConstraint.constant = 20
                //listCell.timeImgView.image = UIImage.init(named: "time")
                listCell.timeLbl.text = "\(self.getTime(timeStr: openTime)) - \(self.getTime(timeStr: closeTime))"
            }else {
                listCell.timeImgView.isHidden = true
                listCell.timeLbl.isHidden = true
                listCell.timeLblHeightConstraint.constant = 0
            }
        }
        listCell.locationImgView.image = UIImage.init(named: "location.png")
//        if let townshipName = promotion.shop_Address?.townShip?.townshipNameEN {
//            if townshipName.count > 0 {
//                listCell.locationLbl.text = townshipName
//            }else {
//                listCell.locationLbl.text = "NA"
//            }
//        }
//        listCell.remarksLbl.text = "NA"
//        if promotion.shop_Templates.count > 0 {
//            let promoOffer = promotion.shop_Templates[0]
//            listCell.remarksLbl.text = promoOffer.template_Header
//        }
        
        if let addressnew = promotion.shop_Address?.addressLine1 {
            if addressnew.count > 0 {
                
                listCell.locationLbl.text = "\(addressnew)"
                
                listCell.locationLbl.text = "\(promotion.shop_Address!.addressLine1!),\( promotion.shop_Address!.addressLine2!),\( promotion.shop_Address!.townShip!.townshipNameEN!),\( promotion.shop_Address!.state!.stateNameEN!)"
            }else {
                listCell.locationLbl.text = "\( promotion.shop_Address!.townShip!.townshipNameEN!),\( promotion.shop_Address!.state!.stateNameEN!)"
            }
        }
        
        listCell.cashOutLblHeightConstraint.constant = 0
        listCell.cashInLblHeightConstraint.constant = 0


        listCell.callBtn.setImage(UIImage.init(named: "call.png"), for: UIControl.State.normal)
        print("promotion meter\(String(describing: promotion.shop_InMeter))")

        listCell.distanceLblKm.text = "0 Km"
        if let shopDistance = promotion.shop_InMeter {
            listCell.distanceLblKm.text = String(format: "%.2f Km", shopDistance)
        }
       // listCell.distanceLblMile.text = "0 ml"
        
        listCell.durationLbl.text = self.durationFromLatLong(lat: promotion.shop_GeoLocation?.lattitude, long:promotion.shop_GeoLocation?.longitude)
        return listCell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let verticalIndicator = scrollView.subviews.last as? UIImageView
        verticalIndicator?.backgroundColor = UIColor.yellow
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let promotion = promotionsList[indexPath.row]
        let storyboard: UIStoryboard = UIStoryboard(name: "Promotion", bundle: nil)
        let protionDetailsView = storyboard.instantiateViewController(withIdentifier: "PromotionDetailView_ID") as! PromotionDetailViewController
        protionDetailsView.selectedPromotion = promotion
        protionDetailsView.strNearByPromotion = "Promotion"
        nav?.pushViewController(protionDetailsView, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         tableView.estimatedRowHeight = 250
         tableView.rowHeight = UITableView.automaticDimension
         return tableView.rowHeight
    }
  
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
