//
//  PromotionSubViewController.swift
//  Promotions
//
//  Created by Uma Rajendran on 10/24/17.
//  Copyright © 2017 Uma Rajendran. All rights reserved.
//

import UIKit
import Rabbit_Swift

class PromotionSubViewController: MapBaseViewController, SortViewDelegate, FilterViewDelegate, MainLocationViewDelegate, CategoriesSelectionDelegate, SearchPromotionDelegate ,NearBySortViewDelegate,NearByFilterViewDelegate,MainNearByLocationViewDelegate,MainwheretoLocationViewDelegate {
    
    let appDel = UIApplication.shared.delegate as! AppDelegate

    @IBOutlet weak var currentLocationView      : UIView!
    
    @IBOutlet weak var locationContentLbl       : UILabel!
        {
        didSet
        {
            locationContentLbl.text = locationContentLbl.text?.localized
            locationContentLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var collectionView           : UICollectionView!
    @IBOutlet weak var baseView                 : UIView!

    private var collectItems     = [String]()
    private var collectImages    = [String]()
    private var highLightImages  = [Bool]()
    
    var mapClusterView          : MapClusterViewController?
    var listPromotionsView      : ListViewController?
    
    var mapUiType: UIType?
    var navController : UINavigationController?
    
    var selectedSortItem        : Int = 0
    var selectedFilterData      : PromotionsModel?
    
    let sortView        = SortViewController()
    let filterView      = FilterViewController()
    
    let margin: CGFloat = 2
    var cellsPerRow: Int = 0
    
    var selectedLocationType: Location?
    let nearbydistance = kNearByDistance
    
    var selectedLocationDetail          : LocationDetail?
    var selectedTownshipDetail          : TownShipDetail?
    
    var promotionsListBackUPByCurLoc   : [PromotionsModel]?
    var promotionsListRecentFromServer : [PromotionsModel]?
    
    var viewFrom: FromView?
    var baseViewHeight: CGFloat {
        if viewFrom == .cashInOut {
            return screenArea.size.height - 250
        }
        return screenArea.size.height
    }
    
    //prabu
    var promotionmainviewDelegate:PromotionsMainViewControllerDelegate?
    let defaults = UserDefaults.standard

    @IBOutlet weak var mapView: UIView! {
        
        didSet {
            
            self.mapView.clipsToBounds = true
            self.mapView.layer.borderWidth = 0.2
            self.mapView.layer.borderColor = UIColor.lightGray.cgColor
            self.mapView.layer.cornerRadius = 20.0
        }
    }
    
    @IBOutlet weak var filterLabel: UILabel! {
        didSet {
            self.filterLabel.text = self.filterLabel.text?.localized
            self.filterLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var mapLabel: UILabel! {
        didSet {
            self.mapLabel.text = self.mapLabel.text?.localized
            self.mapLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    
    @IBOutlet weak var listView: UIView! {
        
        didSet {
            
            self.listView.clipsToBounds = true
            self.listView.layer.borderWidth = 0.2
            self.listView.layer.borderColor = UIColor.lightGray.cgColor
            self.listView.layer.cornerRadius = 20.0
        }
    }
    
    @IBOutlet weak var filterListLabel: UILabel! {
        didSet {
            self.filterListLabel.text = self.filterListLabel.text?.localized
            self.filterListLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var maplistLabel: UILabel! {
        didSet {
            self.maplistLabel.text = self.maplistLabel.text?.localized
            self.maplistLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.title?.localized

        //prabu
        self.listView.isHidden = true
        
        // Do any additional setup after loading the view.
        loadUI()
        //loadInitialize()
        //updateLocalizations()
       // collectionViewSettings()
        //checkAndLoadCollectionView()
        setSelectedLocationType(locationtype: .byCurrentLocation)
        didSelectLocationByCurrentLocation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        loadListViewAtFirstNew()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        geoLocManager.startUpdateLocation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        geoLocManager.stopUpdateLocation()
    }
    
    func loadUI() {
        self.view.backgroundColor = kBackGroundGreyColor
        //currentLocationView.backgroundColor = UIColor.white
       // collectionView.backgroundColor = UIColor.white
        
//        baseView.layer.shadowColor = kBackGroundGreyColor.cgColor
//        baseView.layer.shadowOpacity = 1
//        baseView.layer.shadowOffset = CGSize.zero
//        baseView.layer.shadowRadius = 4
        
//        collectionView.layer.shadowColor = kBackGroundGreyColor.cgColor
//        collectionView.layer.shadowOpacity = 1
//        collectionView.layer.shadowOffset = CGSize.zero
//        collectionView.layer.shadowRadius = 4
    }
    
    
    
    func loadInitialize() {
        setTapGesture()
        
    }
    
    
    func updateCurrentLocationToUI() {
        selectedLocationDetail = nil
        selectedTownshipDetail = nil
       // locationContentLbl.text = "Loading location.....".localized
        //self.promotionmainviewDelegate!.titileChange(name: "Loading location.....".localized)
        guard appDelegate.checkNetworkAvail() else {
            return
        }
        if let lat = geoLocManager.currentLatitude, let long = geoLocManager.currentLongitude {
            geoLocManager.getLocationNameByLatLong(lattitude: lat, longitude: long) { (isSuccess, currentAddress) in
                guard isSuccess else {
                    println_debug("something went wrong when get current address from google api")
                    return
                }
                DispatchQueue.main.async {
                    if let curAddress = currentAddress as? String {
                       // self.locationContentLbl.text = Rabbit.uni2zg(curAddress) //curAddress
                        //self.promotionmainviewDelegate!.titileChange(name: Rabbit.uni2zg(curAddress))
                        
                        self.defaults.set(Rabbit.uni2zg(curAddress), forKey: "NBSCurrentLocation")

                    }else {
                       // self.locationContentLbl.text = "Loading location.....".localized
                        self.promotionmainviewDelegate!.titileChange(name: "Loading location.....".localized)
                        
                        self.defaults.set("Loading location.....".localized, forKey: "NBSCurrentLocation")
                    }
                }
            }

        }
    }
    
    func updateOtherLocationToUI(location: LocationDetail, township: TownShipDetail) {
        selectedLocationDetail = location
        selectedTownshipDetail = township
        DispatchQueue.main.async {
//            self.locationContentLbl.text = "\(township.townShipNameEN), \(location.stateOrDivitionNameEn)" // later change to localizations
            if self.appDel.currentLanguage == "my" {
               // self.locationContentLbl.text = "\(township.townShipNameMY), \(location.stateOrDivitionNameMy)"
                self.promotionmainviewDelegate!.titileChange(name: "\(township.townShipNameMY), \(location.stateOrDivitionNameMy)")
            }
            else
            {
               // self.locationContentLbl.text = "\(township.townShipNameEN), \(location.stateOrDivitionNameEn)"
                self.promotionmainviewDelegate!.titileChange(name: "\(township.townShipNameEN), \(location.stateOrDivitionNameEn)")

            }
        }
    }
    
    func collectionViewSettings() {
        guard let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        flowLayout.minimumInteritemSpacing = margin
        flowLayout.minimumLineSpacing = margin
    }
    
    func setSelectedLocationType(locationtype: Location) {
        selectedLocationType = locationtype
    }
    
    func setTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnCurrentLocationView(_:)))
        currentLocationView.addGestureRecognizer(tap)
        currentLocationView.isUserInteractionEnabled = true
    }
    
    @objc func tapOnCurrentLocationView(_ sender: UITapGestureRecognizer) {
        println_debug("tap call on current location view")
        let storyBoardName = UIStoryboard(name: "Promotion", bundle: nil)
        let locationSelectionView = storyBoardName.instantiateViewController(withIdentifier: "SelecteMainLocationView_ID") as! SelectMainLocationViewController
        locationSelectionView.delegate = self
        //locationSelectionView.navController = self.navController
        self.navController?.pushViewController(locationSelectionView, animated: true)
    }
    
    func movetoPromotion() {
        
        println_debug("tap call on current location view")
        let storyBoardName = UIStoryboard(name: "Promotion", bundle: nil)
        let locationSelectionView = storyBoardName.instantiateViewController(withIdentifier: "SelecteMainLocationView_ID") as! SelectMainLocationViewController
        locationSelectionView.delegate = self
        locationSelectionView.delegatenearby = self
        locationSelectionView.delegatewhereto = self
        self.navController?.pushViewController(locationSelectionView, animated: true)
    }

    func checkAndLoadCollectionView() {
        if mapUiType == .listView {
            collectItems     = listViewDetails.collectionItems
            collectImages    = listViewDetails.collectionImages
            highLightImages  = listViewDetails.highLightImages
        }else {
            collectItems     = mapViewDetails.collectionItems
            collectImages    = mapViewDetails.collectionImages
            highLightImages  = mapViewDetails.highLightImages
        }
        self.refreshSortFilter()
        cellsPerRow = collectItems.count
        collectionView.reloadData()
    }
    
    func refreshSortFilter() {
         selectedSortItem       = 0
         selectedFilterData     = nil
    }
    
    func loadListViewAtFirst() {
        mapUiType = .listView
        listPromotionsView             = ListViewController()
//        listPromotionsView!.view.frame = baseView.bounds
//        listPromotionsView!.view.frame = CGRect(x: 0.0, y: 0.0, width: screenArea.size.width, height: screenArea.size.height - 190)//260)
        listPromotionsView!.view.frame = CGRect(x: 0.0, y: 0.0, width: screenArea.size.width, height: baseView.frame.size.height)//260)

        listPromotionsView?.view.backgroundColor = UIColor.white

        if let nav = self.navController {
            listPromotionsView?.nav = nav
        }
        DispatchQueue.main.async {
            self.listPromotionsView?.showHideInfoLabel(isShow: true, content: "Loading Data...".localized)
            self.baseView.addSubview(self.listPromotionsView!.view)
        }
    }
    
    func loadListViewAtFirstNew() {
        mapUiType = .listView
        listPromotionsView             = ListViewController()
        //        listPromotionsView!.view.frame = baseView.bounds
        //        listPromotionsView!.view.frame = CGRect(x: 0.0, y: 0.0, width: screenArea.size.width, height: screenArea.size.height - 190)//260)
        listPromotionsView!.view.frame = CGRect(x: 0.0, y: 0.0, width: screenArea.size.width, height: baseView.frame.size.height)//260)
        
        listPromotionsView?.view.backgroundColor = UIColor.white
        
        if let nav = self.navController {
            listPromotionsView?.nav = nav
        }
        DispatchQueue.main.async {
            self.listPromotionsView?.showHideInfoLabel(isShow: false, content: "".localized)
            self.baseView.addSubview(self.listPromotionsView!.view)
        }
    }
    
    func switchBaseView() {
        if mapUiType == .listView {
            if let map = mapClusterView {
                map.view.removeFromSuperview()
            }
            listPromotionsView             = ListViewController()
//            listPromotionsView!.view.frame = baseView.bounds
//            listPromotionsView!.view.frame = CGRect(x: 0.0, y: 0.0, width: screenArea.size.width, height: screenArea.size.height - 190)// 260)
            listPromotionsView!.view.frame = CGRect(x: 0.0, y: 0.0, width: screenArea.size.width, height: baseViewHeight)// 260)

            listPromotionsView?.view.backgroundColor = UIColor.white
            if let nav = self.navController {
                listPromotionsView?.nav = nav
            }
            if let promotionlistbackup = promotionsListBackUPByCurLoc {
                DispatchQueue.main.async {
                    self.listPromotionsView?.showHideInfoLabel(isShow: false, content: "")
                    self.listPromotionsView?.promotionsList = promotionlistbackup
                    self.listPromotionsView?.listTableView.reloadData()
                }
                
            }else {
                DispatchQueue.main.async {
                    self.listPromotionsView?.showHideInfoLabel(isShow: true, content: "Loading Data...".localized)
                }
                didSelectLocationByCurrentLocation()
            }
            DispatchQueue.main.async {
                self.baseView.addSubview(self.listPromotionsView!.view)
            }
        }else {
            if let list = listPromotionsView {
                list.view.removeFromSuperview()
            }
            mapClusterView             = MapClusterViewController()
//            mapClusterView?.view.frame = baseView.bounds
            mapClusterView?.view.frame = CGRect(x: 0.0, y: 0.0, width: screenArea.size.width, height: baseViewHeight)
            if let nav = self.navController {
                mapClusterView?.nav = nav
            }
            
            baseView.addSubview(mapClusterView!.view)
            if let promotionlistbackup = promotionsListBackUPByCurLoc {
                updateCurrentLocationToUI()
                mapClusterView?.addAnnotationsInMap(list: promotionlistbackup)
            }else {
                didSelectLocationByCurrentLocation()
            }
        }
    }
    
    func switchMapListView() -> UIType {
        if mapUiType == .listView {
            mapUiType = .mapView
        }else {
            mapUiType = .listView
        }
        DispatchQueue.main.async {
            //self.checkAndLoadCollectionView()
            self.switchBaseView()
        }
        return mapUiType!
    }
    
    @objc func collectionBtnAction(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to: self.collectionView)
        let indexPath = self.collectionView.indexPathForItem(at: buttonPosition)
        println_debug("indexpath row is \(indexPath!.row)")
        println_debug("collection view did select called.....")
        
        if mapUiType == .listView {
            if indexPath?.row == 0 {
                // this is to show categories list view
                showCategoriesView()
            }else if indexPath?.row == 1 {
                // this is to show sort list view
                showSortListView()
            }else if indexPath?.row == 2 {
                // this to show filter view
                showFilterListView()
            }
        }else {
            if indexPath?.row == 0 {
                // this is to show categories list view
                showCategoriesView()
            }else if indexPath?.row == 1 {
                // this to show filter view
                showFilterListView()
            }
        }
        
    }
    
    //List
    @IBAction func listfilterButtonAction(_ sender: UIButton) {

        if listPromotionsView?.promotionsList.count == 0  {
            self.showToast(message: "No Results Found".localized, align: .top)
            return
        }
        guard let sortViewController = UIStoryboard(name: "NBSMain", bundle: nil).instantiateViewController(withIdentifier: "FilterNearByServiceViewController") as? FilterNearByServiceViewController else { return }
        sortViewController.delegate = self
        sortViewController.mainlocationdelegate = self
        sortViewController.delegateFilter = self
        sortViewController.categoryDelegate = self
        // sortViewController.findviewStr = "NearByServices"
        sortViewController.findviewStr = "Promotions"
        
        if (defaults.value(forKey: "NBSsortBy") != nil) {
            
            sortViewController.previousSelectedSortOptionnew = defaults.value(forKey: "NBSsortBy") as? String
            
        }else {
            sortViewController.previousSelectedSortOptionnew = nil
        }
        
        sortViewController.checkMap = false
        sortViewController.previousSelectedSortOption = selectedSortItem
        sortViewController.previousSelectedFilterData = self.selectedFilterData
        sortViewController.dataList = self.promotionsListBackUPByCurLoc // this is used to sort all the promotions
        sortViewController.filterDataList = self.promotionsListBackUPByCurLoc // this is used to sort all the promotions
        sortViewController.modalPresentationStyle = .overCurrentContext
        // self.navigationController?.present(sortViewController, animated: true, completion: nil)
        self.present(sortViewController, animated: true, completion: nil)
        
    }
    
    @IBAction func listBtnAction(_ sender: Any) {

        self.listView.isHidden = true
        self.mapView.isHidden = false
        mapUiType = .listView

        self.switchBaseView()
    }
    
    
    //MAP
    @IBAction func filterButtonAction(_ sender: UIButton) {
        
        
        if listPromotionsView?.promotionsList.count == 0  {
            self.showToast(message: "No Results Found".localized, align: .top)
            return
        }
        guard let sortViewController = UIStoryboard(name: "NBSMain", bundle: nil).instantiateViewController(withIdentifier: "FilterNearByServiceViewController") as? FilterNearByServiceViewController else { return }
        sortViewController.delegate = self
        sortViewController.mainlocationdelegate = self
        sortViewController.delegateFilter = self
        sortViewController.categoryDelegate = self
       // sortViewController.findviewStr = "NearByServices"
        sortViewController.findviewStr = "Promotions"

        if (defaults.value(forKey: "NBSsortBy") != nil) {
            
            sortViewController.previousSelectedSortOptionnew = defaults.value(forKey: "NBSsortBy") as? String
            
        }else {
            sortViewController.previousSelectedSortOptionnew = nil
        }
        
        sortViewController.checkMap = false
        sortViewController.previousSelectedSortOption = selectedSortItem
        sortViewController.previousSelectedFilterData = self.selectedFilterData
        sortViewController.dataList = self.promotionsListBackUPByCurLoc // this is used to sort all the promotions
        sortViewController.filterDataList = self.promotionsListBackUPByCurLoc // this is used to sort all the promotions
        sortViewController.modalPresentationStyle = .overCurrentContext
       // self.navigationController?.present(sortViewController, animated: true, completion: nil)
        self.present(sortViewController, animated: true, completion: nil)

    }
    
    @IBAction func mapBtnAction(_ sender: Any) {
        
        self.listView.isHidden = false
        self.mapView.isHidden = true
        mapUiType = .mapView

        self.switchBaseView()
        
    }
    
    func showSortListView() {
       
        guard let listSpace = self.promotionsListBackUPByCurLoc, listSpace.count > 0 else {
            self.showToast(message: "No Results Found".localized, align: .top)
            return
        }

        sortView.delegate = self
        sortView.viewFrom = .promotion
        sortView.previousSelectedSortOption = selectedSortItem
        sortView.dataList = listPromotionsView?.promotionsList // this is used to sort all the promotions
        sortView.modalPresentationStyle = .overCurrentContext
        self.present(sortView, animated: true, completion: nil)
    }
    
    func showFilterListView() {
        guard let listSpace = self.promotionsListBackUPByCurLoc, listSpace.count > 0 else {
            self.showToast(message: "No Results Found".localized, align: .top)
            return
        }
        filterView.delegate = self
        filterView.viewFrom = .promotion
        filterView.previousSelectedFilterData = self.selectedFilterData
        filterView.filterDataList = self.promotionsListBackUPByCurLoc
        filterView.modalPresentationStyle = .overCurrentContext
        self.present(filterView, animated: true, completion: nil)
    }
    
    func showCategoriesView() {
        
        let storyBoardName = UIStoryboard(name: "Promotion", bundle: nil)
        let categoriesView = storyBoardName.instantiateViewController(withIdentifier: "CategoriesListView_ID") as! CategoriesListViewController
        categoriesView.delegate = self
        categoriesView.modalPresentationStyle = .overCurrentContext
        self.present(categoriesView, animated: true, completion: nil)
        
    }
    
    // MARK: - Custom Delegate Methods
    
    func didSelectLocationByCurrentLocation() {
        
        updateCurrentLocationToUI()
        setSelectedLocationType(locationtype: .byCurrentLocation) // division , state both are similar oly
        guard let lat = geoLocManager.currentLatitude, let long = geoLocManager.currentLongitude else {
            // throw the error to the user
            return
        }
        
//        var lat = "16.81668401110357"
//        var long = "96.13187085779862"

        let mobilenumber  = UserModel.shared.mobileNo
        let apiUrlStr = getNearByMerchantsWithPromotionsAPI(shopname: "", townshipcode: "", divisioncode: "", businesstype: "", businesscategory: "", neardistance: nearbydistance, city: "", mobilenumber: mobilenumber, lat: lat,long: long)
        
       // let apiUrlStr = self.getNearByMerchantsWithPromotionsAPI(shopname: "", townshipcode: "BAGOBAGODIVI", divisioncode: "BAGODIVI", businesstype: "", businesscategory: "", neardistance: self.nearbydistance, city: "", mobilenumber: mobilenumber, lat: lat,long: long)
    
        println_debug("nearbymerchant bycurrentlocation api:::: \(apiUrlStr)")
        
        callNearByMerchantsWithPromotionsAPI(api: apiUrlStr)
        
        
    }
    
    func didSelectNearByLocation() {
        
        updateCurrentLocationToUI()
        setSelectedLocationType(locationtype: .byCurrentLocation) // division , state both are similar oly
        guard let lat = geoLocManager.currentLatitude, let long = geoLocManager.currentLongitude else {
            // throw the error to the user
            return
        }
        let mobilenumber  = UserModel.shared.mobileNo
        let apiUrlStr = getNearByMerchantsWithPromotionsAPI(shopname: "", townshipcode: "", divisioncode: "", businesstype: "", businesscategory: "", neardistance: nearbydistance, city: "", mobilenumber: mobilenumber, lat: lat,long: long)
        
        println_debug("nearbymerchant bycurrentlocation api:::: \(apiUrlStr)")
        
        callNearByMerchantsWithPromotionsAPI(api: apiUrlStr)
        
        self.promotionmainviewDelegate!.titileChange(name: "NearBy")


    }
    
    func didSelectwheretoLocation(streetName:String)
    {
        self.promotionmainviewDelegate!.titileChange(name: streetName)

    }

    func didSelectLocationByTownship(location: LocationDetail, township: TownShipDetail) {
        updateOtherLocationToUI(location: location, township: township)
        setSelectedLocationType(locationtype: .byDivision) // division , state both are similar oly
        
        geoLocManager.getLatLongByName(cityname: township.townShipNameEN) { (isSuccess, lat, long) in
            guard isSuccess, let latti = lat, let longi = long  else {
                self.showErrorAlert(errMessage: "Try Again".localized)
                return
            }
            
            DispatchQueue.main.async {
                // need to check how to get this data
                let mobilenumber  = UserModel.shared.mobileNo
                let apiUrlStr = self.getNearByMerchantsWithPromotionsAPI(shopname: "", townshipcode: township.townShipCode, divisioncode: location.stateOrDivitionCode, businesstype: "", businesscategory: "", neardistance: self.nearbydistance, city: "", mobilenumber: mobilenumber, lat: latti,long: longi)
                println_debug("nearbymerchant bytownship api:::: \(apiUrlStr)")
                self.callNearByMerchantsWithPromotionsAPI(api: apiUrlStr)
            }
        }
        
        //Old Code
        /*
         // need to check how to get this data
        let mobilenumber  = UserModel.shared.mobileNo
        let apiUrlStr = getNearByMerchantsWithPromotionsAPI(shopname: "", townshipcode: township.townShipCode, divisioncode: location.stateOrDivitionCode, businesstype: "", businesscategory: "", neardistance: nearbydistance, city: "", mobilenumber: mobilenumber, lat: geoLocManager.currentLatitude,long: geoLocManager.currentLongitude)
        println_debug("nearbymerchant bytownship api:::: \(apiUrlStr)")
        callNearByMerchantsWithPromotionsAPI(api: apiUrlStr)
        */
    }
    
    func didSelectLocationBySearch(searchKey: String, isCurLocSearch: Bool) { //SearchPromotionDelegate
        setSelectedLocationType(locationtype: .bySearch)
        if isCurLocSearch {
            updateCurrentLocationToUI()
        }
        let mobilenumber  = UserModel.shared.mobileNo
        var divisionCode = ""
        var townshipCode = ""
        if let loc = selectedLocationDetail {
            if let township = selectedTownshipDetail {
                divisionCode = loc.stateOrDivitionCode
                townshipCode = township.townShipCode
            }
        }
        let apiUrlStr = getNearByMerchantsWithPromotionsAPI(shopname: searchKey, townshipcode: townshipCode, divisioncode: divisionCode, businesstype: "", businesscategory: "", neardistance: nearbydistance, city: "", mobilenumber: mobilenumber, lat: geoLocManager.currentLatitude,long: geoLocManager.currentLongitude)
        println_debug("nearbymerchant bysearch api:::: \(apiUrlStr)")
        callNearByMerchantsWithPromotionsAPI(api: apiUrlStr)
    }
    
    func didSelectLocationByCategory(category: SubCategoryDetail?) {
        setSelectedLocationType(locationtype: .byCategory)
        let mobilenumber  = UserModel.shared.mobileNo
        var mainCategorycode = ""
        var subCategoryCode = ""
        if let categ = category {
            mainCategorycode = categ.mainCategoryCode
            if mainCategorycode.length == 1 {
                mainCategorycode = "0\(categ.mainCategoryCode)"
            }
            subCategoryCode = categ.subCategoryCode
            if subCategoryCode.length == 1 {
                subCategoryCode = "0\(categ.subCategoryCode)"
            }
            
            self.promotionmainviewDelegate!.titileChange(name: "\(categ.mainCategoryName)"+"  \(categ.subCategoryName)  ")
             defaults.set(categ.mainCategoryName + "," + categ.subCategoryName, forKey: "NBScategoryBy")

        }
      
        var divisionCode = ""
        var townshipCode = ""
        if let loc = selectedLocationDetail {
            if let township = selectedTownshipDetail {
                divisionCode = loc.stateOrDivitionCode
                townshipCode = township.townShipCode
            }
        }
        
        if let _ = category {
            self.hightLightNeeded(isHighLight: true, from: "category")
        }else {
            self.hightLightNeeded(isHighLight: false, from: "category")
        }
        
        let apiUrlStr = getNearByMerchantsWithPromotionsAPI(shopname: "", townshipcode: townshipCode, divisioncode: divisionCode, businesstype: subCategoryCode, businesscategory: mainCategorycode, neardistance: nearbydistance, city: "", mobilenumber: mobilenumber, lat: geoLocManager.currentLatitude,long: geoLocManager.currentLongitude)
        
        
        println_debug("nearbymerchant bycategory api:::: \(apiUrlStr)")
        callNearByMerchantsWithPromotionsAPI(api: apiUrlStr)
        
    }
   
    func callNearByMerchantsWithPromotionsAPI(api: URL?) {
        
        guard appDelegate.checkNetworkAvail() else {
            return
        }
        guard let url = api else {
            // here need to show the error alert for the user
            self.showErrorAlert(errMessage: "Error on reading promotions.. Dev need to check".localized)
            return
        }
        progressViewObj.showProgressView()
        JSONParser.GetApiResponse(apiUrl: url, type: "POST") { (isSuccess, response) in
            progressViewObj.removeProgressView()
            guard isSuccess else {
                // here need to show the error alert to the user
                println_debug("error in api call dev need to check")
                self.listPromotionsView?.showHideInfoLabel(isShow: true, content: "No Promotions Found".localized)
                return
            }
            println_debug("response dict for nearby merchants list :: \(String(describing: response))")
            let promotionList = ParserAttributeClass.parsePromotionsJSON(anyValue: response as AnyObject) as [PromotionsModel]
            self.promotionsListRecentFromServer = promotionList
            print("Promotion Api------\(self.promotionsListRecentFromServer!.count)")

            if self.mapUiType == .listView {
                
                if promotionList.count > 0 {
                    if self.selectedLocationType == .byCurrentLocation {
                        self.promotionsListBackUPByCurLoc = promotionList  // this is too just save the list in backup
                    }
                    
                    DispatchQueue.main.async {

                    //self.baseView.willRemoveSubview((self.listPromotionsView?.view)!)
                       // self.listPromotionsView?.removeFromParent()
                      //  self.listPromotionsView = nil
                      //  self.loadListViewAtFirstNew()
                        self.listPromotionsView?.showHideInfoLabel(isShow: false, content: "")
                        self.listPromotionsView?.promotionsList = promotionList
                        //print("Promotion Api inside------\(self.listPromotionsView?.promotionsList.count)")
                    
                        self.listPromotionsView?.listTableView.reloadData()
                    }
                    
                }else {
                    DispatchQueue.main.async {
                        self.showErrorAlert(errMessage: "No Promotions Found".localized)
                        //self.listPromotionsView?.showHideInfoLabel(isShow: true, content: "No Promotions Found".localized)
                        self.listPromotionsView?.showHideInfoLabel(isShow: false, content: "")
                        self.listPromotionsView?.promotionsList = self.promotionsListBackUPByCurLoc ?? []
                        
                       //print("Promotion Api inside------\(self.listPromotionsView?.promotionsList.count)")
                     self.listPromotionsView?.listTableView.reloadData()
                    }
                }
            } else {
                DispatchQueue.main.async {
                    if promotionList.count > 0 {
                        self.mapClusterView?.addAnnotationsInMap(list: promotionList)
                    }else {
                        self.mapClusterView?.removeAllAnnotationsInMap()
                        self.showErrorAlert(errMessage: "No Promotions Found".localized)
                    }
                }
            }
        }
    }
    
    func didSelectSortOption(sortedList: [Any], selectedSortOption: Int)  {
        if selectedSortOption == 0 {
            // default selected so load the data get from server
            self.selectedSortItem = selectedSortOption
            listPromotionsView?.promotionsList = promotionsListBackUPByCurLoc!
            
            self.hightLightNeeded(isHighLight: false, from: "sort")
            
        }else {
            if let list = sortedList as? [PromotionsModel] {
                listPromotionsView?.promotionsList = list
                self.hightLightNeeded(isHighLight: true, from: "sort")
            }
            self.selectedSortItem = selectedSortOption

        }
        DispatchQueue.main.async {
            self.listPromotionsView?.listTableView.reloadData()
        }
       
    }
    
    func didSelectFilterOption(filteredList: [Any], selectedFilter: Any?) {
        if self.mapUiType == .listView  {
            if let selectedfilter = selectedFilter as? PromotionsModel {
                self.selectedFilterData = selectedfilter
                listPromotionsView?.promotionsList = filteredList as! [PromotionsModel]
            }else{
                // here is the default filter selection coming
                self.selectedFilterData = nil
                listPromotionsView?.promotionsList = promotionsListBackUPByCurLoc!
            }
            DispatchQueue.main.async {
                self.listPromotionsView?.listTableView.reloadData()
            }
        }else {
            if let selectedfilter = selectedFilter as? PromotionsModel  {
                self.selectedFilterData = selectedfilter
                DispatchQueue.main.async {
                    self.mapClusterView?.addAnnotationsInMap(list: filteredList as! [PromotionsModel])
                }
            }else{
                // here is the default filter selection coming
                self.selectedFilterData = nil
                DispatchQueue.main.async {
                    self.mapClusterView?.addAnnotationsInMap(list: self.promotionsListBackUPByCurLoc!)
                }
            }
           
        }
       
        if let _ = selectedFilterData {
            self.hightLightNeeded(isHighLight: true, from: "filter")
        }else {
            self.hightLightNeeded(isHighLight: false, from: "filter")
        }
    }
 
    func hightLightNeeded(isHighLight : Bool, from: String) {
        if mapUiType == .listView {
            if isHighLight {
                if from == "category" { // categories
                    highLightImages = [true, false, false]
                }else if from == "sort" { // sort
                    highLightImages = [false, true, false]
                    
                }else { // filter
                    highLightImages = [false, false, true]
                    
                }
            }else {
                highLightImages = [false, false, false]
            }
        }else {
            if isHighLight {
                if from == "category" { // categories
                    highLightImages = [true, false ]
                }else { // filter
                    highLightImages = [false, true]
                    
                }
            }else {
                highLightImages = [false, false ]
            }
        }
       
        
        self.collectionView?.reloadData()
    }
    
}

extension PromotionSubViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return collectItems.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mapcollectioncellidentifier", for: indexPath) as! PromotionsCollectionCell
        cell.wrapData(title: collectItems[indexPath.row], imgName: collectImages[indexPath.row], hightLights: highLightImages[indexPath.row])
//        cell.listBtn.addTarget(self, action: #selector(collectionBtnAction(_:)), for: .touchUpInside)
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if mapUiType == .listView {
            if indexPath.row == 0 {
                // this is to show categories list view
                showCategoriesView()
            }else if indexPath.row == 1 {
                // this is to show sort list view
                showSortListView()
            }else if indexPath.row == 2 {
                // this to show filter view
                showFilterListView()
            }
        }else {
            if indexPath.row == 0 {
                // this is to show categories list view
                showCategoriesView()
            }else if indexPath.row == 1 {
                // this to show filter view
                showFilterListView()
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellHeight = self.collectionView.frame.height
        let flowLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let marginsAndInsets = flowLayout.sectionInset.left + flowLayout.sectionInset.right + flowLayout.minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
        let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
        return CGSize(width: itemWidth, height: cellHeight)
    }
    
   
}


class PromotionsCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var midSeparatorLbl  : UILabel!
//    @IBOutlet weak var listBtn          : UIButton!
    @IBOutlet weak var highlightImgV    : UIImageView!
    @IBOutlet weak var itemImgV         : UIImageView!
    @IBOutlet weak var itemNameLbl      : UILabel!
    
    func wrapData(title: String, imgName: String, hightLights: Bool) {
        
//        self.listBtn.setTitle(title, for: UIControlState.normal)
//        self.listBtn.setImage(UIImage.init(named: imgName), for: UIControlState.normal)
        
        self.itemNameLbl.text = title
        self.itemImgV.image = UIImage.init(named: imgName)
        
        if hightLights {
            self.highlightImgV.isHidden = false
        }else {
            self.highlightImgV.isHidden = true
        }
        
//        self.highlightImgV.isHidden =   (hightLights == true) ? false : true
        self.highlightImgV.image    =   UIImage.init(named: "bank_success")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.listBtn.setTitleColor(UIColor.black, for: UIControlState.normal)
//        self.listBtn.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
//        self.listBtn.titleLabel?.font = UIFont.init(name: appFont, size: 13.0)
        
        self.itemNameLbl.font = UIFont.init(name: appFont, size: 13.0)
        self.itemNameLbl.textColor = UIColor.black
     }
}
