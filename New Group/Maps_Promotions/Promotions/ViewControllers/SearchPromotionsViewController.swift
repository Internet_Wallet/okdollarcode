//
//  SearchPromotionsViewController.swift
//  OK
//
//  Created by Uma Rajendran on 11/25/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreData

protocol SearchPromotionDelegate: class {
    func didSelectLocationBySearch(searchKey: String, isCurLocSearch: Bool)
}

class SearchPromotionsViewController: UIViewController {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
        {
        didSet
        {
            searchTextField.placeholder = searchTextField.placeholder? .localized
        }
    }
    @IBOutlet weak var currentLocView: UIView!
    @IBOutlet weak var curLocLabel: UILabel!
    {
        didSet
        {
            curLocLabel.text = curLocLabel.text?.localized
        }
    }
    @IBOutlet weak var favoriteView: UIView!
    @IBOutlet weak var favLabel: UILabel!
        {
        didSet
        {
            favLabel.text = favLabel.text?.localized
        }
    }
    @IBOutlet weak var recentsView: UIView!
    @IBOutlet weak var recentsLabel: UILabel!
        {
        didSet
        {
            recentsLabel.text = recentsLabel.text?.localized
        }
    }
    @IBOutlet weak var recentsTable: UITableView!
    var recentsList = [Any]()
    var recentsMoreList = [Any]()
    
    var delegate: SearchPromotionDelegate?
    @IBOutlet weak var noRecentsLabel: UILabel!
        {
        didSet
        {
            noRecentsLabel.text = "No Record Found!".localized
        }
    }
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    
    //Show Recents More and less
    
    var isViewMoreLess:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = self.title?.localized

        loadUI()
        loadInitializations()
        updateLocalizations()
        textfieldSettings()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadRecentsList()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        searchTextField.resignFirstResponder()
    }
    
    func loadUI() {
//        headerHeightConstraint.constant = kHeaderHeight
        headerView.backgroundColor = kYellowColor
        self.view.backgroundColor = kYellowColor
        self.view.setShadowToView(view: currentLocView)
        self.view.setShadowToView(view: favoriteView)
        self.view.setShadowToView(view: recentsView)
        noRecentsLabel.backgroundColor = UIColor.white
    }

    func loadInitializations() {
        recentsTable.register(UINib(nibName: "RecentsCell", bundle: nil), forCellReuseIdentifier: "recentscellidentifier")
        recentsTable.tableFooterView = UIView(frame: CGRect.zero)
        loadTapGestures()
    }
    
    func updateLocalizations() {
        curLocLabel.font = UIFont.init(name: appFont, size: 15)
        favLabel.font = UIFont.init(name: appFont, size: 15)
        recentsLabel.font = UIFont.init(name: appFont, size: 15)
    }
        
    func loadTapGestures() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnCurrentLocationView(_:)))
        currentLocView.addGestureRecognizer(tap)
        currentLocView.isUserInteractionEnabled = true
        
        let favtap = UITapGestureRecognizer(target: self, action: #selector(tapOnFavoriteView(_:)))
        favoriteView.addGestureRecognizer(favtap)
        favoriteView.isUserInteractionEnabled = true
    }
    
    @objc func tapOnCurrentLocationView(_ sender: UITapGestureRecognizer) {
        println_debug("tap call on current location view")
        self.delegate?.didSelectLocationBySearch(searchKey: self.searchTextField.text!, isCurLocSearch: true)
        if let nav =  self.navigationController {
            nav.popViewController(animated: true)
        }
     }

    @objc func tapOnFavoriteView(_ sender: UITapGestureRecognizer) {
        println_debug("tap call on favorite view")
        let story = UIStoryboard(name: "Promotion", bundle: nil)
        let favoritesView = story.instantiateViewController(withIdentifier: "FavoritePromotionView_ID") as! FavoritePromotionsViewController
        self.navigationController?.pushViewController(favoritesView, animated: true)
    }
    
    func textFieldLeftView()  {
        let leftView = UILabel(frame: CGRect(x: 0, y: 0, width: 7, height: searchTextField.frame.height))
        leftView.backgroundColor = .clear
        
        searchTextField.leftView = leftView
        searchTextField.leftViewMode = .always
        searchTextField.contentVerticalAlignment = .center
    }
    
    func textfieldSettings() {
        searchTextField.delegate = self //as? UITextFieldDelegate
        searchTextField.backgroundColor = UIColor.white
        searchTextField.borderStyle = UITextField.BorderStyle.none
        searchTextField.layer.borderColor = UIColor.lightGray.cgColor
//        searchTextField.layer.cornerRadius = 5.0
        searchTextField.layer.borderWidth = 0.5
        searchTextField.textColor = UIColor.black
        searchTextField.autocorrectionType = UITextAutocorrectionType.no
        searchTextField.font = UIFont.init(name: appFont, size: 16)
        let placeHolderLbl = searchTextField.value(forKey: "_placeholderLabel") as! UILabel
        placeHolderLbl.textColor = UIColor.black
        placeHolderLbl.font = UIFont.init(name: appFont, size: 16)
        placeHolderLbl.text = "Search".localized
        searchTextField.enablesReturnKeyAutomatically = true
        searchTextField.keyboardType = UIKeyboardType.default;
        self.textFieldLeftView()
    }
 
    @IBAction func backAction(_ sender: Any) {
        if let nav =  self.navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    @IBAction func searchAction(_ sender: Any) {
        if searchTextField.text?.count == 0 {
            searchTextField.becomeFirstResponder()
            return
        }
       searchPromotionsByOtherLoc()
    }
    
    func searchPromotionsByOtherLoc() {
        self.delegate?.didSelectLocationBySearch(searchKey: self.searchTextField.text!, isCurLocSearch: false)
        if let nav =  self.navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    func loadRecentsList() {
        self.recentsList.removeAll()
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "RecentPromotion")
        do {
            let sectionSortDescriptor = NSSortDescriptor(key: "addedDateTime", ascending: false)
            let sortDescriptors = [sectionSortDescriptor]
            fetchRequest.sortDescriptors = sortDescriptors
            let fetchedResults = try managedContext.fetch(fetchRequest) as! [RecentPromotion]
            guard fetchedResults.count > 0 else{
                showHideRecentsLabel(isShow: true)
                return
            }
            for aPromotion in fetchedResults {
                if let promotionData = aPromotion.value(forKey: "promotionData") as? Data {
                    let decodeData = NSKeyedUnarchiver.unarchiveObject(with: promotionData)
                    if let promotion = decodeData as? PromotionsModel {
                        
                        if(recentsList.count < 5)
                        {
                            recentsList.append(promotion)
                        }
                        else
                        {
                            if(isViewMoreLess)
                            {
                                recentsList.append(promotion)
                            }
                            else
                            {
                                println_debug("less data display")
                            }
                        }
                        println_debug("recentsList ::: \(recentsList)")
                        println_debug("promotion ::: \(promotion)")
                        println_debug("shop id ::: \(promotion.shop_Id ?? "")")
                        println_debug("shop name ::: \(promotion.shop_Name ?? "")")
                    }else {
                        println_debug("error on get promotion model")
                    }
                }else  {
                    println_debug("error on get promotion data")
                }
                //println_debug("db promotion id ::: \(aPromotion.value(forKey: "promotionId"))")
                //println_debug("db promotion added datetime ::: \(aPromotion.value(forKey: "addedDateTime"))")
             }
            self.recentsTable.reloadData()
            showHideRecentsLabel(isShow: false)
         }
        catch {
            print ("fetch task failed", error)
        }
    }
    
    func showHideRecentsLabel(isShow: Bool) {
        noRecentsLabel.isHidden = isShow ? false : true
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.searchTextField.resignFirstResponder()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
}



extension SearchPromotionsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(recentsList.count < 5)
        {
            return recentsList.count
        }
        else
        {
            if(isViewMoreLess)
            {
               return recentsList.count + 1
            }
            
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(recentsList.count < 5)
        {
            let recentCell = tableView.dequeueReusableCell(withIdentifier: "recentscellidentifier", for: indexPath) as! RecentsCell
            
            let promotion: PromotionsModel = recentsList[indexPath.row] as! PromotionsModel
            recentCell.shopName.text = promotion.shop_Name
            recentCell.merchantName.text = promotion.shop_AgentName
            recentCell.shopDistance.text = "0 Km".localized
            
            var isfavoritePromotion: Bool = false
            isfavoritePromotion = favoriteExist(shopId: promotion.shop_Id!)
            
            if(isfavoritePromotion)
            {
                recentCell.constraintWidthImgFav.constant = 30
            }
            else{
                recentCell.constraintWidthImgFav.constant = 0
            }
            
            if let shopDistance = promotion.shop_InMeter {
                recentCell.shopDistance.text = String(format: "%.2f Km", shopDistance)
            }
            
            return recentCell
        }
        else
        {
                if(isViewMoreLess)
                {
                    let identifier = "RecentsFooterCell"
                    
                    var cellFile: RecentsFooterCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? RecentsFooterCell
                    
                    if(indexPath.row == recentsList.count)
                    {
                        if cellFile == nil {
                            tableView.register(UINib(nibName: "RecentsFooterCell", bundle: nil), forCellReuseIdentifier: identifier)
                            cellFile = tableView.dequeueReusableCell(withIdentifier: identifier) as? RecentsFooterCell
                        }
                        cellFile.btnAllRecents.addTarget(self, action:#selector(self.allRecentsButtonClicked(sender:)), for: .touchUpInside)
                        cellFile.btnAllRecents.setTitle("VIEW LESS RECENTS SEARCHES".localized, for: .normal)
                        
                        return cellFile
                    }
                    else
                    {
                    let recentCell = tableView.dequeueReusableCell(withIdentifier: "recentscellidentifier", for: indexPath) as! RecentsCell
                    
                    let promotion: PromotionsModel = recentsList[indexPath.row] as! PromotionsModel
                    recentCell.shopName.text = promotion.shop_Name
                    recentCell.merchantName.text = promotion.shop_AgentName
                    recentCell.shopDistance.text = "0 Km".localized
                    
                    var isfavoritePromotion: Bool = false
                    isfavoritePromotion = favoriteExist(shopId: promotion.shop_Id!)
                    
                    if(isfavoritePromotion)
                    {
                        recentCell.constraintWidthImgFav.constant = 30
                    }
                    else{
                        recentCell.constraintWidthImgFav.constant = 0
                    }
                    
                    if let shopDistance = promotion.shop_InMeter {
                        recentCell.shopDistance.text = String(format: "%.2f Km", shopDistance)
                    }
                    
                    return recentCell
                    }
                }
                else
                {
                    let identifier = "RecentsFooterCell"
                    
                    var cellFile: RecentsFooterCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? RecentsFooterCell
                    
                    if(indexPath.row == 4)
                    {
                        if cellFile == nil {
                            tableView.register(UINib(nibName: "RecentsFooterCell", bundle: nil), forCellReuseIdentifier: identifier)
                            cellFile = tableView.dequeueReusableCell(withIdentifier: identifier) as? RecentsFooterCell
                        }
                        cellFile.btnAllRecents.addTarget(self, action:#selector(self.allRecentsButtonClicked(sender:)), for: .touchUpInside)
                        cellFile.btnAllRecents.setTitle("VIEW ALL RECENTS SEARCHES".localized, for: .normal)
                        
                        return cellFile
                    }
                    else
                    {
                    let recentCell = tableView.dequeueReusableCell(withIdentifier: "recentscellidentifier", for: indexPath) as! RecentsCell
                    
                    let promotion: PromotionsModel = recentsList[indexPath.row] as! PromotionsModel
                    recentCell.shopName.text = promotion.shop_Name
                    recentCell.merchantName.text = promotion.shop_AgentName
                    recentCell.shopDistance.text = "0 Km".localized
                    
                    var isfavoritePromotion: Bool = false
                    isfavoritePromotion = favoriteExist(shopId: promotion.shop_Id!)
                    
                    if(isfavoritePromotion)
                    {
                        recentCell.constraintWidthImgFav.constant = 30
                    }
                    else{
                        recentCell.constraintWidthImgFav.constant = 0
                    }
                    
                    if let shopDistance = promotion.shop_InMeter {
                        recentCell.shopDistance.text = String(format: "%.2f Km", shopDistance)
                    }
                    
                    return recentCell
                    }
                }
            
      
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(recentsList.count < 5)
        {
            let story = UIStoryboard(name: "Promotion", bundle: nil)
            let detailView = story.instantiateViewController(withIdentifier: "PromotionDetailView_ID") as! PromotionDetailViewController
            let promotion: PromotionsModel = recentsList[indexPath.row] as! PromotionsModel
            detailView.selectedPromotion = promotion
            self.navigationController?.pushViewController(detailView, animated: true)
        }
        else
        {
        if(indexPath.row != recentsList.count)
        {
        let story = UIStoryboard(name: "Promotion", bundle: nil)
        let detailView = story.instantiateViewController(withIdentifier: "PromotionDetailView_ID") as! PromotionDetailViewController
        let promotion: PromotionsModel = recentsList[indexPath.row] as! PromotionsModel
        detailView.selectedPromotion = promotion
        self.navigationController?.pushViewController(detailView, animated: true)
        }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
   
    @IBAction func allRecentsButtonClicked(sender: UIButton) {
        println_debug("allRecentsButtonClicked")
        
        if(isViewMoreLess)
        {
           isViewMoreLess = false
        }
        else
        {
            isViewMoreLess = true
        }
        
        loadRecentsList()

        self.recentsTable.reloadData()
    }
    
    func favoriteExist(shopId : String) -> Bool {
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FavoritePromotion")
        do {
            fetchRequest.predicate = NSPredicate(format: "promotionId == %@", shopId)
            let fetchedResults = try managedContext.fetch(fetchRequest) as! [FavoritePromotion]
            if let _ = fetchedResults.first {
                return true
            }else {
                return false
            }
        }
        catch {
            print ("fetch task failed", error)
        }
        return false
        
    }
}

extension SearchPromotionsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        searchPromotionsByOtherLoc()
        return true
    }
    
}
