//
//  SelectMainLocationExtension.swift
//  OK
//
//  Created by iMac on 4/9/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit


extension SelectMainLocationViewController: UITextFieldDelegate,StreetListViewDelegate {
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 500 {
//            PersonalProfileModelUP.shared.Street = textField.text ?? ""
//            let dic = AddressInfo[5]
//            dic["title"] = textField.text ?? ""
            self.removeStreetList()
        }
    }
    
    //tvStreetList
    func setUpStreetList() {
        if !self.view.contains(tvStreetList) {
            tvStreetList.frame = CGRect(x: 10, y: 150, width: self.view.frame.width - 20, height: 200)
            tvStreetList.backgroundColor = UIColor.clear
            tvStreetList.delegate = self
            tvStreetList.tvList.isHidden = true
            tvStreetList.isUserInteractionEnabled = true
            self.view.addSubview(tvStreetList)
            self.view.bringSubviewToFront(tvStreetList)
        }
    }
    
    func removeStreetList() {
        if self.view.contains(tvStreetList) {
            tvStreetList.removeFromSuperview()
        }
    }
    
    @objc func streetTextFieldEditing(_ textField: UITextField) {
        if textField.tag == 500 {
            
            if textField.text?.count ?? 0 > 30 {
               textField.text?.removeLast()
                return
            }
            
            if textField.text?.count ?? 0 > 0 {
                self.setUpStreetList()
                tvStreetList.tvList.isHidden = false
                tvStreetList.isComingFromWhereTo = true
                
                if UIScreen.main.bounds.size.height >= 812{
                    tvStreetList.getAllAddressDetails(searchTextStr: textField.text ?? "", yAsix: 175)
                }else{
                    tvStreetList.getAllAddressDetails(searchTextStr: textField.text ?? "", yAsix: 150)
                }
                
                
            }else {
                if self.view.contains(tvStreetList) {
                    self.removeStreetList()
                }
            }
        }
    }
    
    func streetNameSelected(street_Name: addressModel) {
        wheretoTF.text = street_Name.address
        self.removeStreetList()
        self.delegatewhereto?.didSelectwheretoLocation(streetName: street_Name.address ?? "")
        
        //self.recentcurrentlocationTitleLbl.text = street_Name
        if let nav =  self.navigationController {
            if isComingFromAgentList{
                self.dismiss(animated: true, completion: nil)
            }else{
                nav.popViewController(animated: true)
            }
            
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
     
        if textField == wheretoTF{
            
            
            
            let updatedText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            if !(updatedText == updatedText.components(separatedBy: NSCharacterSet(charactersIn: SEARCHCHARSETOffer).inverted).joined(separator: "")) { return false }
            
            if updatedText == " " || updatedText == "  "{
                return false
            }
            if (updatedText.contains("  ")){
                return false
            }
            if updatedText.count > 50 {
                return false
            }
            
          
//            let value = (textField.text! as NSString).replacingCharacters(in: range, with: string)
//
//            if !(value == value.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) {
//                return false
//            }
//
//            if range.location == 0 && value == " " {
//                return false
//            }
            
            if updatedText.count > 0 {
                crossButton.isHidden = false
            }else{
                crossButton.isHidden = true
            }

         //   return restrictMultipleSpaces(str: value, textField: wheretoTF)
            
        }
        return true
    }
    
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        
////        if textField == wheretoTF{
////            let value = (textField.text! as NSString).replacingCharacters(in: range, with: string)
////            if value.count>0{
////                crossButton.isHidden = false
////            }else{
////                crossButton.isHidden = true
////            }
////
////        }
//        
//    }
}


