//
//  SelectDivisionStateViewController.swift
//  OK
//
//  Created by Uma Rajendran on 12/4/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class SelectDivisionStateViewController: UIViewController {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var divisionStateTable: UITableView!
    var allLocationsList = [LocationDetail]()
    var allDivisionList = [LocationDetail]()
    var allStateList = [LocationDetail]()
     var nav: UINavigationController?
    weak var delegate: TownshipSelectionDelegate?

    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
 
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = self.title?.localized
        
       // UINavigationBar.appearance().barTintColor = UIColor(red: 246.0/255.0, green: 198.0/255.0, blue: 0.0/255.0, alpha: 1.0)
       // UINavigationBar.appearance().tintColor = UIColor(red: 246.0/255.0, green: 198.0/255.0, blue: 0.0/255.0, alpha: 1.0)

        //UINavigationBar.appearance().tintColor = UIColor.whiteColor()
       // nav?.navigationBar.appearance().barTintColor = UIColor(red: 246.0/255.0, green: 198.0/255.0, blue: 0.0/255.0, alpha: 1.0)


        loadUI()
        loadInitialize()
        updateLocalizations()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        allLocationsList.removeAll()
        allDivisionList.removeAll()
        allStateList.removeAll()

        TownshipManager.collapseAllLocation()
    }
    
    func loadUI() {
//        headerHeightConstraint.constant = kHeaderHeight
        headerView.backgroundColor = kYellowColor
        //self.view.backgroundColor = kBackGroundGreyColor
        self.view.backgroundColor = kYellowColor
    }
    
    func loadInitialize() {
        divisionStateTable.register(UINib(nibName: "DivisionStateCell", bundle: nil), forCellReuseIdentifier: "divisionstatecellidentifier")
        divisionStateTable.tableFooterView = UIView(frame: CGRect.zero)
       // allLocationsList = TownshipManager.allLocationsList
        
         allLocationsList = TownshipManager.allLocationsList.sorted(by: { $0.stateOrDivitionNameEn < $1.stateOrDivitionNameEn} )
        allDivisionList  = TownshipManager.allDivisionList
        allStateList     = TownshipManager.allStateList
        divisionStateTable.reloadData()
        
       
    }
    
    func updateLocalizations() {
        headerLbl.font = UIFont.init(name: appFont, size: 18)
        headerLbl.text = "Select Division/State".localized
        headerLbl.textColor = UIColor.white
    }
    
    @IBAction func closeAction(_ sender: Any) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
//        if let presentingView = self.presentingViewController {
//            presentingView.dismiss(animated: true, completion: nil)
//        }
    }
    
}

extension SelectDivisionStateViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowCount = 0
        rowCount = allLocationsList.count
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        let locationDetails: LocationDetail! = allLocationsList[indexPath.row]
        let divisionCell = tableView.dequeueReusableCell(withIdentifier: "divisionstatecellidentifier", for: indexPath) as! DivisionStateCell
        
        if appDel.currentLanguage == "my" {
            divisionCell.wrapData(title: locationDetails.stateOrDivitionNameMy, count: locationDetails.townShipArray.count)
        }
        else
        {
            divisionCell.wrapData(title: locationDetails.stateOrDivitionNameEn, count: locationDetails.townShipArray.count)
        }
        
        return divisionCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let locationDetails: LocationDetail = (isLoadDivision == true) ? allDivisionList[indexPath.section] : allStateList[indexPath.section]
        //        if indexPath.row == 0{
        //            locationDetails.isExpand = !locationDetails.isExpand
        //            townshipTable.reloadSections(NSIndexSet(index: indexPath.section) as IndexSet, with: .fade)
        //            townshipTable.scrollToRow(at: IndexPath(row: 0, section: indexPath.section), at: .top, animated: true)
        //        }else {
        //            delegate?.didSelectTownship(location: locationDetails, township: locationDetails.townShipArray[indexPath.row - 1])
        //            if let presentingView = self.presentingViewController {
        //                presentingView.dismiss(animated: false, completion: nil)
        //            }
        //            if let thisNav = self.nav {
        //                thisNav.popViewController(animated: true)
        //            }
        //        }
        let locationDetails: LocationDetail! = allLocationsList[indexPath.row]

        let storyboard: UIStoryboard = UIStoryboard(name: "Promotion", bundle: nil)
        let townshipListView = storyboard.instantiateViewController(withIdentifier: "SelectTownshipView_ID") as! SelectTownshipViewController
//        townshipListView.nav = self.nav
        townshipListView.indexpathrow = indexPath.row
        townshipListView.selectedDivState = locationDetails
        townshipListView.delegate = self.delegate
//        townshipListView.isLoadDivision = (indexPath.row == 1) ? true : false
//        self.present(townshipListView, animated: true, completion: nil)
        self.navigationController?.pushViewController(townshipListView, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}





