//
//  DirectionMapVC.swift
//  OK
//
//  Created by iMac on 8/21/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import GoogleMaps



class DirectionMapVC: OKBaseController {

    @IBOutlet weak var crossButton: UIButton!
   
    var mapView = GMSMapView()
    var arrayPolyline = [GMSPolyline]()
    var selectedRought:String!
    var url = ""
    var source = CLLocationCoordinate2D()
    var destination = CLLocationCoordinate2D()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.frame = self.view.frame
        view.addSubview(mapView)
        mapView.isHidden = true
        view.bringSubviewToFront(crossButton)
        LoadMapRoute(urlStr: url, source: source, and: destination)
    }
    
    @IBAction func onClickCrossButton(_ sender: Any) {
        self.dismissDetail()
    }
    
    private func showMarkerImage() {
        DispatchQueue.main.async {
            let endPosition = CLLocationCoordinate2D(latitude: self.destination.latitude, longitude: self.destination.longitude)
            let startPosition = CLLocationCoordinate2D(latitude: self.source.latitude, longitude: self.source.longitude)
            let startPos = GMSMarker(position: startPosition)
            let endPos = GMSMarker(position: endPosition)
            startPos.icon = UIImage(named: "okDollarCurrentLocation")
            startPos.map = self.mapView
            endPos.icon = UIImage(named: "yellowMarker")
            endPos.map = self.mapView
        }
    }
    
        
    func LoadMapRoute(urlStr: String ,source:CLLocationCoordinate2D ,and destination :CLLocationCoordinate2D)
        {
            
            let url = URL(string: urlStr)
            URLSession.shared.dataTask(with: url!, completionHandler:
                {
                    (data, response, error) in
                    if(error != nil)
                    {
                        print("error")
                    }
                    else
                    {
                        do{
                            let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                            let arrRouts = json["routes"] as! NSArray
                            
                            for  polyline in self.arrayPolyline
                            {
                                polyline.map = nil;
                            }
                            
                            self.arrayPolyline.removeAll()
                            
                            let pathForRought:GMSMutablePath = GMSMutablePath()
                            
                            if (arrRouts.count == 0)
                            {
                                let distance:CLLocationDistance = CLLocation.init(latitude: source.latitude, longitude: source.longitude).distance(from: CLLocation.init(latitude: destination.latitude, longitude: destination.longitude))
                                
                                pathForRought.add(source)
                                pathForRought.add(destination)
                                
                                let polyline = GMSPolyline.init(path: pathForRought)
                                self.selectedRought = pathForRought.encodedPath()
                                polyline.strokeWidth = 5
                                polyline.strokeColor = UIColor.blue
                                polyline.isTappable = true
                                
                                self.arrayPolyline.append(polyline)
                                
                                if (distance > 8000000)
                                {
                                    polyline.geodesic = false
                                }
                                else
                                {
                                    polyline.geodesic = true
                                }
                                
                                polyline.map = self.mapView;
                            }
                            else
                            {
                                for (index, element) in arrRouts.enumerated()
                                {
                                    let dicData:NSDictionary = element as! NSDictionary
                                    
                                    let routeOverviewPolyline = dicData["overview_polyline"] as! NSDictionary
                                    
                                    let path =  GMSPath.init(fromEncodedPath: routeOverviewPolyline["points"] as! String)
                                    
                                    let polyline = GMSPolyline.init(path: path)
                                    
                                    polyline.isTappable = true
                                    
                                    self.arrayPolyline.append(polyline)
                                    
                                    polyline.strokeWidth = 5
                                    
                                    if index == 0
                                    {
                                        self.selectedRought = routeOverviewPolyline["points"] as? String
                                        
                                        polyline.strokeColor = UIColor.blue;
                                    }
                                    else
                                    {
                                        polyline.strokeColor = UIColor.darkGray;
                                    }
                                    
                                    polyline.geodesic = true;
                                }
                                
                                for po in self.arrayPolyline.reversed()
                                {
                                    po.map = self.mapView;
                                }
                            }
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5)
                            {
                                let bounds:GMSCoordinateBounds = GMSCoordinateBounds.init(path: GMSPath.init(fromEncodedPath: self.selectedRought)!)
                                self.mapView.animate(with: GMSCameraUpdate.fit(bounds))
                                self.mapView.isHidden = false
                                self.showMarkerImage()
                            }
                        }
                        catch let error as NSError
                        {
                            print("error:\(error)")
                        }
                    }
            }).resume()
        }
        
        
        
        
    
    
   

}


