//
//  MapDetailViewController.swift
//  Promotions
//
//  Created by Uma Rajendran on 10/24/17.
//  Copyright © 2017 Uma Rajendran. All rights reserved.
//

import UIKit
import MapKit

class MapDetailViewController: MapBaseViewController, UIGestureRecognizerDelegate {

//    var locationManager: CLLocationManager!
//    let geoLocManager    = GeoLocationManager.shared
    
    //prabu
    var nearbyLatitude : Double?
    var nearbyLongtitude : Double?
    var screencheck : String?
    var infoViewTimer: Timer!


    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerLbl: UILabel!
    {
        didSet
        {
            headerLbl.text = headerLbl.text?.localized
        }
    }
    @IBOutlet var locationMapView: MKMapView!

    @IBOutlet weak var locationDetailsView: UIView!
    @IBOutlet weak var locationImageView: UIImageView!
    @IBOutlet weak var promotionNameLbl: MarqueeLabel!
        {
        didSet
        {
            promotionNameLbl.text = promotionNameLbl.text?.localized
        }
    }
    @IBOutlet weak var addFavoriteBtn: UIButton!
//    @IBOutlet weak var ratingImgView: UIImageView!
    @IBOutlet weak var remarksLbl: MarqueeLabel!
        {
        didSet
        {
            remarksLbl.text = remarksLbl.text?.localized
        }
    }
     @IBOutlet weak var contactNoLbl: UILabel!
        {
        didSet
        {
            contactNoLbl.text = contactNoLbl.text?.localized
        }
    }
    var selectedPromotion: PromotionsModel?
    let regionRadius: CLLocationDistance = 1000
    let clusteringManager = FBClusteringManager()
     @IBOutlet weak var directionIcon: UIButton!
    var isFavorite : Bool = false
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var rateImgViewList: [UIImageView]!
    
    //MARK: View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.locationMapView.showsUserLocation = false
        
        
        loadUI()
        loadInitialize()
        updateLocalizations()
 
        if (screencheck == "NearBy") {
            
           // print("Nearby Map Detail viewcontroller======")

            let initialLocation = CLLocation(latitude: Double(selectedPromotion?.shop_GeoLocation?.lattitude ?? 0.0), longitude: Double(selectedPromotion?.shop_GeoLocation?.longitude ?? 0.0))
                centerMapOnLocation(location: initialLocation)
                addAnnotationsInMap()
                loadInfoView()
            
        } else {
        if let promotion = selectedPromotion {
            
//            print("Other Map Detail viewcontroller======\(selectedPromotion)")
//            print("Other Map Detail viewcontroller======\(promotion.shop_Latitude)***\(promotion.shop_Longtitude)")

            let initialLocation = CLLocation(latitude: Double(promotion.shop_Latitude ?? 0.0), longitude: Double(promotion.shop_Longtitude ?? 0.0))
            centerMapOnLocation(location: initialLocation)
            addAnnotationsInMap()
            loadInfoView()
        }
        }
        
    }

    func loadUI() {
//        headerHeightConstraint.constant = kHeaderHeight
        //headerView.backgroundColor = kYellowColor
        headerLbl.font = UIFont.init(name: appFont, size: 18)
        
        self.promotionNameLbl.font = UIFont.init(name: appFont, size: 14)
        self.promotionNameLbl.textColor = UIColor.black
        self.contactNoLbl.font = UIFont.init(name: appFont, size: 13)
        self.contactNoLbl.textColor = UIColor.black
        self.remarksLbl.font = UIFont.init(name: appFont, size: 13)
        self.remarksLbl.textColor = kBlueColor
        self.setUpMarqueeLabel(label: self.remarksLbl)
//        ratingImgView.image = UIImage.init(named: "star_five")
       // locationImageView.image = UIImage.init(named: "btc")
         locationImageView.image = UIImage.init(named: "newBetterthan")
        directionIcon.layer.shadowColor = UIColor.darkGray.cgColor
        directionIcon.layer.shadowOpacity = 1
        directionIcon.layer.shadowOffset = CGSize.zero
        directionIcon.layer.shadowRadius = 4
        locationImageView.layer.shadowColor = kBackGroundGreyColor.cgColor
        locationImageView.layer.shadowOpacity = 1
        locationImageView.layer.shadowOffset = CGSize.zero
        locationImageView.layer.shadowRadius = 2
        if isFavorite {
            addFavoriteBtn.setImage(UIImage.init(named: "act_favorite"), for: UIControl.State.normal)
        }else {
            addFavoriteBtn.setImage(UIImage.init(named: "favorite"), for: UIControl.State.normal)
        }
    }
    
    func setUpMarqueeLabel(label: MarqueeLabel) {
        label.tag = 501
        label.type = .continuous
        label.speed = .duration(20)
        label.fadeLength = 10.0
        label.trailingBuffer = 30.0
        label.isUserInteractionEnabled = true // Don't forget this, otherwise the gesture recognizer will fail (UILabel has this as NO by default)
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(pauseTap))
        tapRecognizer.numberOfTapsRequired = 1
        tapRecognizer.numberOfTouchesRequired = 1
        label.addGestureRecognizer(tapRecognizer)
    }
    
    @objc func pauseTap(_ recognizer: UIGestureRecognizer) {
        let continuousLabel2 = recognizer.view as! MarqueeLabel
        if recognizer.state == .ended {
            continuousLabel2.isPaused ? continuousLabel2.unpauseLabel() : continuousLabel2.pauseLabel()
        }
    }
    
    func loadInitialize() {
        locationMapView.delegate = self
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(gestureRecognizer:)))
        gestureRecognizer.delegate = self
        locationDetailsView.addGestureRecognizer(gestureRecognizer)
    }
    
    func updateLocalizations() {
        
    }
    
    @objc func handleTap(gestureRecognizer: UIGestureRecognizer) {
       
        
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion.init(center: location.coordinate,
                                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        locationMapView.setRegion(coordinateRegion, animated: true)
    }
    
    func addAnnotationsInMap() {
        
        if (screencheck == "NearBy") {
            let promotion = selectedPromotion

            if let lat = promotion?.shop_Latitude , let long = promotion?.shop_Longtitude {
                let annotation = FBAnnotation()
                annotation.coordinate = CLLocationCoordinate2DMake(lat, long)
                annotation.userData = promotion
                clusteringManager.removeAll()
                clusteringManager.add(annotations: [annotation])
            }
        
            
        } else {
            
            if let promotion = selectedPromotion {
                if let lat = promotion.shop_Latitude , let long = promotion.shop_Longtitude {
                    let annotation = FBAnnotation()
                    annotation.coordinate = CLLocationCoordinate2DMake(lat, long)
                    annotation.userData = promotion
                  //  print("Annotationcoordinate-----\(annotation.coordinate)")
                    clusteringManager.removeAll()
                    clusteringManager.add(annotations: [annotation])
                }
            }
            
        }
        
    }
    
    func loadInfoView() {
        if let promotion = selectedPromotion {
            headerLbl.text = "\(promotion.shop_Name!) Directions"
            promotionNameLbl.text = promotion.shop_Name
            var mobileNumber = "NA".localized
            if promotion.shop_Phonenumber!.count > 0 {
                mobileNumber = getPhoneNumberByFormat(phoneNumber: promotion.shop_Phonenumber!)
            }
            contactNoLbl.text = mobileNumber
            if promotion.shop_Templates.count > 0 {
                let promoOffer = promotion.shop_Templates[0]
                remarksLbl.text = promoOffer.template_Header
            }else {
                remarksLbl.text = "NA".localized
            }
            
            for img in rateImgViewList {
                img.isHighlighted = false
            }
            
            
//            if let shopDistance = info.distanceInKm {
//                distanceLbl.text = String(format: "(%.2f Km)", shopDistance).replacingOccurrences(of: ".00", with: "")
//            }
            
          //  locationImageView.image = UIImage.init(named: "btc")
            locationImageView.image = UIImage.init(named: "newBetterthan")
            
        }
    }
    
    @objc func update() {
        infoViewTimer.invalidate()
        removeTheInfoView()
    }
    
    func removeTheInfoView() {
        UIView.animate(withDuration: 0.1,
                       delay: 0.1,
                       options: UIView.AnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.locationDetailsView.center.x -= self.view.bounds.width
        }, completion: { (finished) -> Void in
            self.locationDetailsView.isHidden = true
            self.locationDetailsView.center.x += self.view.bounds.width
        })
    }
    
    func showPromotionInfoView(promotion: PromotionsModel) {
        self.locationDetailsView.isHidden = false
        self.locationDetailsView.center.x -= self.view.bounds.width
        if let timer = infoViewTimer {
            timer.invalidate()
        }
        //loadInfoViewData(info: promotion)
        UIView.animate(withDuration: 1.5, delay: 0.5,
                       usingSpringWithDamping: 0.3,
                       initialSpringVelocity: 0.5,
                       options: [], animations: {
                        self.locationDetailsView.center.x += self.view.bounds.width
                        
        }, completion:{ _ in
            //self.infoViewTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.update), userInfo: nil, repeats: false)
        })
    }
    
    
    @IBAction func makePaymentBtnAction(_ sender: Any) {
        self.isComingFromMapMakePayment = true
        self.navigateToPayto(phone: self.selectedPromotion?.shop_Phonenumber ?? "" , amount: "", name: selectedPromotion?.shop_AgentName ?? "", fromMerchant: true)
    }
   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    
    @IBAction func addFavoriteBtnAction(_ sender: Any) {
        
    }
    
    @IBAction func currentLocBtnAction(_ sender: Any) {
        
        let initialLocation = CLLocation(latitude: Double((selectedPromotion?.shop_GeoLocation?.lattitude)!), longitude: Double((selectedPromotion?.shop_GeoLocation?.longitude)!))

        centerMapOnLocation(location: initialLocation)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension MapDetailViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        // DispatchQueue.global(qos: .userInitiated).async {
        DispatchQueue.main.async {
            let mapBoundsWidth = Double(self.locationMapView.bounds.size.width)
            let mapRectWidth = self.locationMapView.visibleMapRect.size.width
            let scale = mapBoundsWidth / mapRectWidth
            
            let annotationArray = self.clusteringManager.clusteredAnnotations(withinMapRect: self.locationMapView.visibleMapRect, zoomScale:scale)
            
            //            DispatchQueue.main.async {
            self.clusteringManager.display(annotations: annotationArray, onMapView:self.locationMapView)
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var reuseId = ""
        if annotation is FBAnnotationCluster {
            reuseId = "Cluster"
            var clusterView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
            if clusterView == nil {
                clusterView = FBAnnotationClusterView(annotation: annotation, reuseIdentifier: reuseId, configuration: FBAnnotationClusterViewConfiguration.default())
                
            } else {
                clusterView?.annotation = annotation
            }
            return clusterView
        } else {
            
            guard (annotation is MKUserLocation) else {
            
            //let coordinate : CLLocationCoordinate2D = annotation.coordinate
            let annotationIdentifier = "SomeCustomIdentifier"
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
            if annotationView == nil {
                
              //  print("annotationView nil called--------")
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
                annotationView?.canShowCallout = false
                let pinImage = UIImage(named: "ok_logo.png")
                annotationView?.image = pinImage
                /*
                if (screencheck == "NearBy") {

                let pinImage = UIImage(named: "ok_logo.png")
                annotationView?.image = pinImage
                    
                } else {
                    
                if let promotion = selectedPromotion {
                    if promotion.isAgent {
                        let pinImage = UIImage(named: "ok_logo.png")
                        annotationView?.image = pinImage
                    }else {
                        let emoji1 = (promotion.shop_Category?.categoryLogo!)!
                        let emoji2 = (promotion.shop_Category?.businessType?.businessType_Logo!)!
                        let topImage = emoji1.emojiToImage()
                        let bottomImage = emoji2.emojiToImage()
                        annotationView?.image = topImage?.combineWith(image: bottomImage!)
                    }
                }
                    
                }*/
            } else {
                
               // print("annotationView not nill called--------")
                annotationView?.annotation = annotation
            }
            return annotationView
        }
           return nil
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annotation = view.annotation else { return }
        
        if let cluster = annotation as? FBAnnotationCluster {
            var zoomRect = MKMapRect.null
            for annotation in cluster.annotations {
                if let fbAnnot = annotation as? FBAnnotation {
                    if let _ = fbAnnot.userData as? PromotionsModel {
                        //println_debug("my data details :::: \(userdata.shop_Name)  ,, \(userdata.shop_Address?.addressLine1) ,, \(userdata.shop_InMeter)")
                    }
                }
                let annotationPoint = MKMapPoint.init(annotation.coordinate)
                let pointRect = MKMapRect.init(x: annotationPoint.x, y: annotationPoint.y, width: 0, height: 0)
                if zoomRect.isNull {
                    zoomRect = pointRect
                } else {
                    zoomRect = zoomRect.union(pointRect)
                }
            }
            mapView.setVisibleMapRect(zoomRect, animated: true)
        }else {
            if let eachAnnotation = annotation as? FBAnnotation {
                if let _ = eachAnnotation.userData as? PromotionsModel {
                    //println_debug("user each data details :::: \(userdata.shop_Name)  ,, \(userdata.shop_Address?.addressLine1) ,, \(userdata.shop_InMeter)")
                    self.showPromotionInfoView(promotion: selectedPromotion!)

                }
                
            }
        }
    }
    
}
