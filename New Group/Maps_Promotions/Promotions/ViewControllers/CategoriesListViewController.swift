//
//  CategoriesListViewController.swift
//  Promotions
//
//  Created by Uma Rajendran on 10/24/17.
//  Copyright © 2017 Uma Rajendran. All rights reserved.
//

import UIKit

protocol CategoriesSelectionDelegate: class {
    func didSelectLocationByCategory(category: SubCategoryDetail?)
}


class CategoriesListViewController: UIViewController {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerLbl: UILabel!
    {
        didSet
        {
            headerLbl.text = headerLbl.text?.localized
            headerLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var categoriesTable: UITableView!
    var categoriesList = CategoriesManager.categoriesList
    weak var delegate: CategoriesSelectionDelegate?
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadUI()
        loadInitialize()
        updateLocalization()
        
        //let btn = UIButton.init()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CategoriesManager.collapseCategories()
    }
    
    func loadUI() {
//        headerHeightConstraint.constant = kHeaderHeight
        headerView.backgroundColor = kYellowColor
        headerLbl.font = UIFont.init(name: appFont, size: 18)
    }
    
    func loadInitialize() {
        categoriesTable.register(UINib(nibName: "CategoriesMainCell", bundle: nil), forCellReuseIdentifier: "categoriesMainCellIdentifier")
        categoriesTable.register(UINib(nibName: "CategoriesSubCell", bundle: nil), forCellReuseIdentifier: "categoriesSubCellIdentifier")
        categoriesTable.tableFooterView = UIView(frame: CGRect.zero)
        categoriesTable.reloadData()
    }
    
    func updateLocalization() {
        
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        dismissView()
    }
    
    @IBAction func refreshCategoriesAction(_ sender: Any) {
        self.delegate?.didSelectLocationByCategory(category: nil)
        dismissView()
    }
    
    func dismissView() {
        if let presentingView = self.presentingViewController {
            presentingView.dismiss(animated: true, completion: nil)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}



extension CategoriesListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return categoriesList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowCount = 1
        let category = categoriesList[section]
        if category.isExpand {
            rowCount = category.subCategoryList.count + 1
        }
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let category = categoriesList[indexPath.section]
        if indexPath.row == 0 {
            let mainCategoryCell = tableView.dequeueReusableCell(withIdentifier: "categoriesMainCellIdentifier", for: indexPath) as! CategoriesMainCell
            
            if appDel.currentLanguage == "my" {
                mainCategoryCell.categoryNameLbl.text = "\(category.categoryEmoji) \(category.categoryBurmeseName)"
            }
            else
            {
                mainCategoryCell.categoryNameLbl.text = "\(category.categoryEmoji) \(category.categoryName)"
            }
            
            mainCategoryCell.expandImgV.image = (category.isExpand == true) ? UIImage.init(named: "up_arrow.png") : UIImage.init(named: "down_arrow.png")
            return mainCategoryCell
        }else {
            let subCategoryCell = tableView.dequeueReusableCell(withIdentifier: "categoriesSubCellIdentifier", for: indexPath) as! CategoriesSubCell
            let subCategory = category.subCategoryList[indexPath.row - 1]
            
            if appDel.currentLanguage == "my" {
                subCategoryCell.subCategoryNameLbl.text = "\(subCategory.subCategoryEmoji) \(subCategory.subCategoryBurmeseName)"
            }
            else
            {
                subCategoryCell.subCategoryNameLbl.text = "\(subCategory.subCategoryEmoji) \(subCategory.subCategoryName)"
            }
            
            return subCategoryCell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let category = categoriesList[indexPath.section]
        if indexPath.row == 0{
            category.isExpand = !category.isExpand
            categoriesTable.reloadSections(NSIndexSet(index: indexPath.section) as IndexSet, with: .fade)
            categoriesTable.scrollToRow(at: IndexPath(row: 0, section: indexPath.section), at: .top, animated: true)

        }else {
            let subCategory = category.subCategoryList[indexPath.row - 1]
            self.delegate?.didSelectLocationByCategory(category: subCategory)
            dismissView()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
