//
//  PromotionsMainViewController.swift
//  Promotions
//
//  Created by Uma Rajendran on 10/24/17.
//  Copyright © 2017 Uma Rajendran. All rights reserved.
//

import UIKit

protocol PromotionsMainViewControllerDelegate : class {
    
    func titileChange(name:String)
    
}
class PromotionsMainViewController: MapBaseViewController,PromotionsMainViewControllerDelegate {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerLabel: MarqueeLabel!
        {
        didSet
        {
            headerLabel.font = UIFont(name: appFont, size: appFontSize)
            headerLabel.text = headerLabel.text?.localized
        }
    }

    @IBOutlet weak var baseView: UIView!
    
    var allSourceViews = [UIViewController]()
    var pageViewController: UIPageViewController?
    
    var mapUiType: UIType?
    var promotionView: PromotionSubViewController?
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var switchMapListBtn: UIButton!
    @IBOutlet weak var searchMapListBtn: UIButton!
    
    //prabu
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var TopLocationBtn: UIButton!
        {
        didSet
        {
            self.TopLocationBtn.layer.masksToBounds = true
            self.TopLocationBtn.layer.borderWidth = 1.0
            self.TopLocationBtn.layer.borderColor = UIColor.white.cgColor
            self.TopLocationBtn.layer.cornerRadius = 20.0
            
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //headerHeightConstraint.constant = kHeaderHeight
        geoLocManager.startUpdateLocation()
        loadUI()
        updateLocalizations()
        loadInitialize()
        
        //prabu
        defaults.set(nil, forKey: "NBSsortBy")
        defaults.set(nil, forKey: "NBSfilterBy")
        defaults.set(nil, forKey: "NBScategoryBy")
        defaults.set(nil, forKey: "NBSRecentLocationAddress1")
        defaults.set(nil, forKey: "NBSRecentLocationAddress2")
        defaults.set(nil, forKey: "NBSCurrentLocation")
      
//        loadPageViewController()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        geoLocManager.startUpdateLocation()
        mapUiType = .listView

        //Load List again
            appDel.floatingButtonControl?.window.closeButton?.isHidden = true
            appDel.floatingButtonControl?.window.isHidden = true
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        loadPromotionSubView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        geoLocManager.stopUpdateLocation()
    }
    
    // MARK: - Load UI and Localizations
    
    func loadUI() {
        headerView.backgroundColor = kYellowColor
        baseView.layer.shadowColor = kBackGroundGreyColor.cgColor
        baseView.layer.shadowOpacity = 1
        baseView.layer.shadowOffset = CGSize.zero
        baseView.layer.shadowRadius = 4
//        self.view.setShadowToLabel(thisView: promotionSeparatorLbl)
//        self.view.setShadowToLabel(thisView: offerSeparatorLbl)
    }
    
    func updateLocalizations() {
        headerLabel.font = UIFont.init(name: appFont, size: 18)
        headerLabel.text = userDef.value(forKey: "lastLoginKey") as? String ?? ""
    }
    
    func loadInitialize() {
       
        
    }
    
    func titileChange(name:String){
        
        self.headerLabel.text = name
    }
    
    func loadPromotionSubView() {
        let storyBoardName = UIStoryboard(name: "Promotion", bundle: nil)
        promotionView = storyBoardName.instantiateViewController(withIdentifier: "PromotionSubView_ID") as? PromotionSubViewController
        promotionView?.mapUiType = .listView
        promotionView?.viewFrom = .promotion
        promotionView!.promotionmainviewDelegate = self
        promotionView?.navController = self.navigationController
        //promotionView?.view.frame = CGRect(x: 0.0, y: 0.0, width: screenArea.size.width, height: screenArea.size.height - 190)//260)
        baseView.addSubview(promotionView!.view)
    }
    
    @IBAction func searchBtnAction(_ sender: Any) {
        
        if mapUiType == .listView {
            println_debug("listView")
            let story = UIStoryboard(name: "Promotion", bundle: nil)
            let searchView = story.instantiateViewController(withIdentifier: "SearchPromotionsView_ID") as! SearchPromotionsViewController
            searchView.delegate = promotionView
            self.navigationController?.pushViewController(searchView, animated: true)
        }
        else
        {
            println_debug("switchMapListView")
        }
    }
    
    @IBAction func promotionMainBackAction(_ sender: Any) {
        if let presentingView = self.presentingViewController {
            presentingView.dismiss(animated: true, completion: nil)
        }
    }
    
    
    @IBAction func mapBtnAction(_ sender: Any) {
        let maptype: UIType = (promotionView?.switchMapListView())!
        if maptype == .listView {
            switchMapListBtn.setImage(UIImage.init(named: "map_white"), for: .normal)
            searchMapListBtn.setImage(UIImage.init(named: "search_white"), for: .normal)
            mapUiType = .listView
        }else {
            switchMapListBtn.setImage(UIImage.init(named: "promotion_list"), for: .normal)
            searchMapListBtn.setImage(UIImage.init(named: ""), for: .normal)
            mapUiType = .mapView
        }
        
    }
    
    @IBAction func topheaderLocationAction(_ sender: UIButton) {
        
        promotionView?.movetoPromotion()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

