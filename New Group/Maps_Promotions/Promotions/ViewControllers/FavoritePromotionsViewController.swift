//
//  FavoritePromotionsViewController.swift
//  OK
//
//  Created by Uma Rajendran on 11/25/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreData

class FavoritePromotionsViewController: UIViewController {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
        {
        didSet
        {
            headerLabel.text = "Favorite".localized
        }
    }
    @IBOutlet weak var favoritesTable: UITableView!
    var favoritesList = [Any]()
    @IBOutlet weak var noResultsView: UIView!
    @IBOutlet weak var noResultsLabel: UILabel!
        {
        didSet
        {
            noResultsLabel.text = noResultsLabel.text?.localized
        }
    }
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadUI()
        loadInitialization()
        updateLocalization()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadFavoritesList()
    }
    
    func loadUI() {
//        headerHeightConstraint.constant = kHeaderHeight
        headerView.backgroundColor = kYellowColor
        self.view.backgroundColor = kBackGroundGreyColor
    }
    
    func loadInitialization() {
        favoritesTable.register(UINib(nibName: "RecentsCell", bundle: nil), forCellReuseIdentifier: "recentscellidentifier")
        favoritesTable.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    func updateLocalization() {
        headerLabel.font = UIFont.init(name: appFont, size: 18.0)
        noResultsLabel.font = UIFont.init(name: appFont, size: 16.0)
    }
    
    func loadFavoritesList() {
        self.favoritesList.removeAll()
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FavoritePromotion")
        do {
            let sectionSortDescriptor = NSSortDescriptor(key: "addedDateTime", ascending: false)
            let sortDescriptors = [sectionSortDescriptor]
            fetchRequest.sortDescriptors = sortDescriptors
            let fetchedResults = try managedContext.fetch(fetchRequest) as! [FavoritePromotion]
            if fetchedResults.count > 0 {
                for aPromotion in fetchedResults {
                    if let promotionData = aPromotion.value(forKey: "promotionData") as? Data {
                        let decodeData = NSKeyedUnarchiver.unarchiveObject(with: promotionData)
                        if let promotion = decodeData as? PromotionsModel {
                            self.favoritesList.append(promotion)
                            //println_debug("shop id ::: \(promotion.shop_Id)")
                            //println_debug("shop name ::: \(promotion.shop_Name)")
                        }else {
                            println_debug("error on get promotion model")
                        }
                    }else  {
                        println_debug("error on get promotion data")
                    }
                }
                self.favoritesTable.reloadData()
                showHideNoResultsView(isShow: false)
            }else {
                showHideNoResultsView(isShow: true)
            }
        }
        catch {
            print ("fetch task failed", error)
        }
    }
    
    func showHideNoResultsView(isShow: Bool) {
        noResultsView.isHidden = isShow ? false : true
    }
    
    @IBAction func backAction(_ sender: Any) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension FavoritePromotionsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favoritesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let favCell = tableView.dequeueReusableCell(withIdentifier: "recentscellidentifier", for: indexPath) as! RecentsCell
        let promotion: PromotionsModel = favoritesList[indexPath.row] as! PromotionsModel
        favCell.recentsImgV.image = nil
        favCell.constraintWidthImage.constant = 0
        favCell.shopName.text = promotion.shop_Name
        favCell.merchantName.text = promotion.shop_AgentName
        favCell.shopDistance.text = "0 Km".localized
        if let shopDistance = promotion.shop_InMeter {
            favCell.shopDistance.text = String(format: "%.2f Km", shopDistance)
        }
        return favCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let story = UIStoryboard(name: "Promotion", bundle: nil)
        let detailView = story.instantiateViewController(withIdentifier: "PromotionDetailView_ID") as! PromotionDetailViewController
        let promotion: PromotionsModel = favoritesList[indexPath.row] as! PromotionsModel
        detailView.selectedPromotion = promotion
        self.navigationController?.pushViewController(detailView, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
