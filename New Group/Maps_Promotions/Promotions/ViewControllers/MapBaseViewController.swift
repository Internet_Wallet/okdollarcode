//
//  MapBaseViewController.swift
//  Promotions
//
//  Created by Uma Rajendran on 10/24/17.
//  Copyright © 2017 Uma Rajendran. All rights reserved.
//

import UIKit
import CoreLocation

class MapBaseViewController: OKBaseController, UIScrollViewDelegate {
     
    
//    struct ListViewContent {
//        let collectionItems = ["Map","Category","Sort","Filter"]
//        let collectionImages = ["map_small.png","categories_small.png","sort_small.png","filter_small.png"]
//    }
//
//    struct MapViewContent {
//        let collectionItems = ["List","Category","Filter"]
//        let collectionImages = ["list_small.png","categories_small.png","filter_small.png"]
//    }
    
    struct ListViewContent {
        let collectionItems  = ["Category".localized,"Sort".localized,"Filter".localized]
        let collectionImages = ["categories.png","promotion_sort.png","filter.png"]
        let highLightImages  = [false, false, false]
        
    }
    
    struct MapViewContent {
        let collectionItems = ["Category".localized,"Filter".localized]
        let collectionImages = ["categories.png","filter.png"]
        let highLightImages  = [false, false]
    }
    
    let listViewDetails = ListViewContent()
    let mapViewDetails = MapViewContent()
 
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
 
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
