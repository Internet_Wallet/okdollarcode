//
//  SelectTownshipViewController.swift
//  Promotions
//
//  Created by Uma Rajendran on 10/24/17.
//  Copyright © 2017 Uma Rajendran. All rights reserved.
//

import UIKit

protocol TownshipSelectionDelegate: class {
    func didSelectTownship(location: LocationDetail, township: TownShipDetail)
}

class SelectTownshipViewController: UIViewController {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerLbl: UILabel!
    {
        didSet
        {
        headerLbl.text = headerLbl.text?.localized
        }
    }
    @IBOutlet weak var townshipTable: UITableView!
    var allLocationsList = [LocationDetail]()
//    var allDivisionList = [LocationDetail]()
//    var allStateList = [LocationDetail]()
    var isLoadDivision: Bool!
    weak var delegate: TownshipSelectionDelegate?
    var nav: UINavigationController?
    var selectedDivState: LocationDetail?
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    var indexpathrow : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = self.title?.localized
      
        

        loadUI()
        loadInitialize()
        updateLocalizations()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        TownshipManager.collapseAllLocation()
    }
    
    func loadUI() {
//        headerHeightConstraint.constant = kHeaderHeight
        headerView.backgroundColor = kYellowColor
        //self.view.backgroundColor = kBackGroundGreyColor
        self.view.backgroundColor = kYellowColor

    }
    
    func loadInitialize() {
//        townshipTable.register(UINib(nibName: "DivisionCell", bundle: nil), forCellReuseIdentifier: "divisioncellidentifier")
        
//         allLocationsList = TownshipManager.allLocationsList
//         allDivisionList  = TownshipManager.allDivisionList
//         allStateList     = TownshipManager.allStateList

        allLocationsList.removeAll()
        selectedDivState = nil
        
        townshipTable.register(UINib(nibName: "TownshipCell", bundle: nil), forCellReuseIdentifier: "townshipcellidentifier")
       // townshipTable.tableFooterView = UIView(frame: CGRect.zero)
        
        TownshipManager.collapseAllLocation()
        
        //TownshipManager.init()
        
        allLocationsList = TownshipManager.allLocationsList.sorted(by: { $0.stateOrDivitionNameEn < $1.stateOrDivitionNameEn})
        
        selectedDivState =  allLocationsList[indexpathrow!]
        //print("selectedDivState----\(selectedDivState)")
        print("index----\(String(describing: indexpathrow))")
        
        selectedDivState?.townShipArray = (selectedDivState?.townShipArray.sorted(by: { $0.cityNameEN < $1.cityNameEN} ))!
        townshipTable.reloadData()
    }
    
    func updateLocalizations() {
        headerLbl.font = UIFont.init(name: appFont, size: 18)
        headerLbl.textColor = UIColor.white
        headerLbl.text = "Select Township / City".localized

    }

    @IBAction func closeAction(_ sender: Any) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
//        if let presentingView = self.presentingViewController {
//            presentingView.dismiss(animated: true, completion: nil)
//        }
        
    }
}

extension SelectTownshipViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
//        return (isLoadDivision == true) ? allDivisionList.count : allStateList.count
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowCount = 0
//        let locationDetails: LocationDetail = (isLoadDivision == true) ? allDivisionList[section] : allStateList[section]
//        if locationDetails.isExpand {
//            rowCount = locationDetails.townShipArray.count + 1
//        }else{
//            rowCount = 1
//        }
        rowCount = (selectedDivState?.townShipArray.count)!
        return rowCount
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

//        let locationDetails: LocationDetail! = (isLoadDivision == true) ? allDivisionList[indexPath.section] : allStateList[indexPath.section]
//        if indexPath.row == 0 {
//            let divisionCell = tableView.dequeueReusableCell(withIdentifier: "divisioncellidentifier", for: indexPath) as! DivisionCell
//            let image = (locationDetails.isExpand == true) ? UIImage.init(named: "up_arrow.png") : UIImage.init(named: "down_arrow.png")
//            divisionCell.wrapData(text: locationDetails.stateOrDivitionNameEn, img: image!)
//            return divisionCell
//        }else {
//            let townshipCell = tableView.dequeueReusableCell(withIdentifier: "townshipcellidentifier", for: indexPath) as! TownshipCell
//            let townshipDetails: TownShipDetail = locationDetails.townShipArray[indexPath.row - 1]
//            townshipCell.townshipNameLbl.text = townshipDetails.cityNameEN
//            return townshipCell
//        }
        
        let townshipCell = tableView.dequeueReusableCell(withIdentifier: "townshipcellidentifier", for: indexPath) as! TownshipCell
        let townshipDetails: TownShipDetail = selectedDivState!.townShipArray[indexPath.row]
        
        if appDel.currentLanguage == "my" {
            townshipCell.townshipNameLbl.text = townshipDetails.cityNameMY
        }
        else
        {
            townshipCell.townshipNameLbl.text = townshipDetails.cityNameEN
        }
        return townshipCell
     
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let locationDetails: LocationDetail = (isLoadDivision == true) ? allDivisionList[indexPath.section] : allStateList[indexPath.section]
//        if indexPath.row == 0{
//            locationDetails.isExpand = !locationDetails.isExpand
//            townshipTable.reloadSections(NSIndexSet(index: indexPath.section) as IndexSet, with: .fade)
//            townshipTable.scrollToRow(at: IndexPath(row: 0, section: indexPath.section), at: .top, animated: true)
//        }else {
//            delegate?.didSelectTownship(location: locationDetails, township: locationDetails.townShipArray[indexPath.row - 1])
//            if let presentingView = self.presentingViewController {
//                presentingView.dismiss(animated: false, completion: nil)
//            }
//            if let thisNav = self.nav {
//                thisNav.popViewController(animated: true)
//            }
//        }
        let townshipDetails: TownShipDetail = selectedDivState!.townShipArray[indexPath.row]
        delegate?.didSelectTownship(location: selectedDivState!, township: townshipDetails)
        if let thisNav = self.navigationController {
            thisNav.popViewController(animated: false)
            thisNav.popViewController(animated: false)
            thisNav.popViewController(animated: true)
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}




