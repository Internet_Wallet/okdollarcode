//
//  SelectMainLocationViewController.swift
//  Promotions
//
//  Created by Uma Rajendran on 10/24/17.
//  Copyright © 2017 Uma Rajendran. All rights reserved.
//

import UIKit
import CoreLocation


protocol MainLocationViewDelegate : class {
    
    func didSelectLocationByTownship(location: LocationDetail, township: TownShipDetail)
    func didSelectLocationByCurrentLocation()

}

protocol MainLocationViewDelegateRegistration : class {
    
    func didSelectLocationByTownshipRegistration(location: LocationDetail, township: TownShipDetailForAddress)
    func didSelectLocationByCurrentLocation()

}

protocol MainNearByLocationViewDelegate : class {
    
    func didSelectNearByLocation()

}



protocol MainwheretoLocationViewDelegate : class {
    
    func didSelectwheretoLocation(streetName:String)
    
}

class SelectMainLocationViewController: UIViewController, TownshipSelectionDelegate , MyTextFieldDelegate {
  
    func textFieldDidDelete() {
        if wheretoTF.text?.count ?? 0 > 0 {
            self.crossButton.isHidden = false
        }else {
              self.crossButton.isHidden = true
        }
    }
    
    var navController : UINavigationController?

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerLbl: UILabel!
    {
        didSet
        {
            headerLbl.text = headerLbl.text?.localized
            headerLbl.font = UIFont(name : appFont, size : 16.0)
        }
    }
    @IBOutlet weak var locationTable: UITableView!
    
    @IBOutlet weak var crossButton: UIButton!
    weak var delegate: MainLocationViewDelegate?
    weak var delegateRegistration : MainLocationViewDelegateRegistration?
    weak var delegatenearby: MainNearByLocationViewDelegate?
    weak var delegatewhereto: MainwheretoLocationViewDelegate?
    var sampledata = LocationDetail()
    var isComingFromAgentList = false

    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    
    //prabu
    @IBOutlet weak var wheretoView: UIView!
    @IBOutlet weak var wheretoTF: MyTextField!

    @IBOutlet weak var currentlocationView: UIView!
    @IBOutlet weak var currentlocationTitleLbl: UILabel!{
        didSet {
            self.currentlocationTitleLbl.text = "Current Location".localized
            self.currentlocationTitleLbl.font = UIFont(name : appFont, size : appFontSize)
        }
    }
    @IBOutlet weak var currentlocationTitleDataLbl: UILabel!{
        didSet {
            self.currentlocationTitleDataLbl.font = UIFont(name : appFont, size : appFontSize)
        }
    }
    @IBOutlet weak var currentLocationBtnOut: UIButton!

    @IBOutlet weak var nearbyView: UIView!
    @IBOutlet weak var nearbyTitleLbl: UILabel!{
        didSet {
            self.nearbyTitleLbl.text = "Near By".localized
            self.nearbyTitleLbl.font = UIFont(name : appFont, size : appFontSize)
        }
    }
    @IBOutlet weak var nearbyBtnOut: UIButton!

    @IBOutlet weak var statedivisionView: UIView!
    @IBOutlet weak var statedivisionTitleLbl: UILabel!{
        didSet {
            self.statedivisionTitleLbl.text = "Division/State".localized
            self.statedivisionTitleLbl.font = UIFont(name : appFont, size : appFontSize)
        }
    }
    @IBOutlet weak var statedivisionBtnOut: UIButton!
    @IBOutlet weak var recentLbl: UILabel!{
        didSet {
            self.recentLbl.text = "RECENT".localized
            self.recentLbl.font = UIFont(name : appFont, size : appFontSize)
        }
    }
    @IBOutlet weak var recentView: UIView!

    //prabu
    let defaults = UserDefaults.standard

    var tvStreetList = Bundle.main.loadNibNamed("StreetListView", owner: self, options: nil)?[0] as! StreetListView

    //MARK:- View Life Cycle
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        
        if ok_default_language == "en"{
            CHARSET =  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789/-,:() "
        }else if ok_default_language == "my" {
            CHARSET =  "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာ၁၂၃၄၅၆၇၈၉၀/-,:() "
        }
        
        if appDel.currentLanguage == "uni" {
            let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                              .font : UIFont.systemFont(ofSize: appFontSize)]
            wheretoTF.attributedPlaceholder = NSAttributedString(string: "Where to ?".localized, attributes:attributes)
            wheretoTF.font = UIFont(name: "Myanmar3", size: appFontSize)//UIFont.systemFont(ofSize: 18)
        } else  {
            wheretoTF.font = UIFont(name: appFont, size: appFontSize)
        }
        
        self.wheretoTF.placeholder = "Where to ?".localized
        
        wheretoTF.addTarget(self, action: #selector(streetTextFieldEditing(_:)), for: .editingChanged)
        wheretoTF.becomeFirstResponder()
    }
    
    
    var CHARSET = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        crossButton.isHidden = true
       
        
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.barTintColor  = kYellowColor
       
        self.currentlocationTitleDataLbl.text = userDef.value(forKey: "lastLoginKey") as? String ?? ""
        locationTable.reloadData()
    }

    @IBAction func onClickCrossButton(_ sender: Any) {
        crossButton.isHidden = true
        wheretoTF.text = ""
        tvStreetList.tvList.isHidden = true
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateLocalizations() {
        headerView.backgroundColor = kYellowColor
        headerLbl.font = UIFont.init(name: appFont, size: appFontSize)
    }
    
    @IBAction func backAction(_ sender: Any) {
        if isComingFromAgentList{
             self.dismiss(animated: true, completion: nil)
        }else{
            if let nav =  self.navigationController {
                nav.popViewController(animated: true)
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func didSelectTownship(location: LocationDetail, township: TownShipDetail) {
        println_debug("main location view delegate called")
        //self.delegate?.didSelectLocationByTownship(location: location, township: township)
    }


    @IBAction func currentlocationBtn(_ sender: Any) {
        
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined:
                println_debug("notDetermined")
                delegate?.didSelectLocationByCurrentLocation()
                delegateRegistration!.didSelectLocationByCurrentLocation()
                if let nav =  self.navigationController {
                    if isComingFromAgentList{
                        self.dismiss(animated: true, completion: nil)
                    }else{
                        nav.popViewController(animated: true)
                    }
                    
                }else{
                    self.dismiss(animated: true, completion: nil)
                }
                break
            case .denied :
                println_debug("denied")
                showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                break
            case .restricted :
                println_debug("restricted")
                showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                break
            case .authorizedAlways, .authorizedWhenInUse:
                delegate?.didSelectLocationByCurrentLocation()
                delegateRegistration!.didSelectLocationByCurrentLocation()
                if let nav =  self.navigationController {
                    if isComingFromAgentList{
                        self.dismiss(animated: true, completion: nil)
                    }else{
                         nav.popViewController(animated: true)
                    }
                   
                }else{
                    self.dismissDetail()
                }
                
                
                break
            }
        } else {
            alertViewObj.wrapAlert(title:"", body: "Please Enable Location Service".localized, img: #imageLiteral(resourceName: "location"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
        
       
       
    }
    
  
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:"OK$ would like to use your Current Location. Please turn on the Location Services for your device.".localized, img:#imageLiteral(resourceName: "map"))
        
        alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
            
        })
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
            }
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
    
    
    @IBAction func nearByBtn(_ sender: Any) {
        
        delegatenearby?.didSelectNearByLocation()
        if let nav =  self.navigationController {
            if isComingFromAgentList{
                //Added by Tushar because of navigation issue
                self.dismiss(animated: false, completion: nil)
            }else{
               nav.popViewController(animated: true)
            }
            
        }else{
            
            //Added by Tushar because of navigation issue
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    @IBAction func satedivisionBtn(_ sender: Any) {
        
        
        let sb = UIStoryboard(name: "Registration", bundle: nil)
        let stateDivisionVC  = sb.instantiateViewController(withIdentifier: "StateDivisionVC") as! StateDivisionVC
        stateDivisionVC.isComingFromAgent = true
        stateDivisionVC.delegateMap = self
        let navController = UINavigationController(rootViewController: stateDivisionVC)
        
        
        
        stateDivisionVC.stateDivisonVCDelegate = self
        if appDel.currentLanguage == "my"
        {
            ok_default_language = "my"
        } else if appDel.currentLanguage ==  "en"{
            ok_default_language = "en"
        }else{
           ok_default_language = "uni"
        }
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        //self.navigationController?.present(navController, animated: true, completion: nil)
       // self.present(stateDivisionVC, animated: true, completion: nil)
       self.presentDetail(navController)
     //   self.navigationController?.presentDetail(stateDivisionVC)
    //    self.navigationController?.pushViewController(stateDivisionVC, animated: true)

        
//        let storyboard: UIStoryboard = UIStoryboard(name: "Promotion", bundle: nil)
//        let divisionStateView = storyboard.instantiateViewController(withIdentifier: "SelectDivisionStateView_ID") as! SelectDivisionStateViewController
//        divisionStateView.delegate = self
//        self.navigationController?.pushViewController(divisionStateView, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SelectMainLocationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if locationArr.count == 0 {
            recentView.isHidden = true
            let noDataLabel = UILabel(frame: tableView.bounds)
            noDataLabel.text = "No Recent Search !!".localized
            noDataLabel.textAlignment = .center
            noDataLabel.font = UIFont(name: appFont, size: appFontSize)
            tableView.backgroundView = noDataLabel
        } else {
            recentView.isHidden = false
            tableView.backgroundView = nil
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locationArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let locationCell = tableView.dequeueReusableCell(withIdentifier: "MainLocationCell", for: indexPath) as! MainLocationCell
        let tempObj = locationArr[indexPath.row]
        locationCell.contentLbl.text = tempObj.shortName
        locationCell.contentSubLbl.text = tempObj.address
        return locationCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           // delegate?.didSelectLocationByCurrentLocation()
            if let nav =  self.navigationController {
                
                let tempObj = locationArr[indexPath.row]
                
                defaults.set(tempObj.shortName, forKey: "NBSRecentLocationAddress1")
                defaults.set(tempObj.address, forKey: "NBSRecentLocationAddress2")
                defaults.set(tempObj.shortName, forKey: "NBSCurrentLocation")
                
             self.delegatewhereto?.didSelectwheretoLocation(streetName: tempObj.address ?? "" )
                
                if isComingFromAgentList{
                  self.dismiss(animated: true, completion: nil)
                }else{
                    nav.popViewController(animated: true)
                }
                
            }else{
                self.dismiss(animated: true, completion: nil)
        }
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}

extension SelectMainLocationViewController: StateDivisionVCDelegate {
    func setDivisionStateName(SelectedDic: LocationDetail) {
        
       // self.sampledata.removeAll()
        self.sampledata = SelectedDic
        print("division-----\(self.sampledata)")
        //locationDetails = SelectedDic
//        let dic = AddressInfo[1]
//        if ok_default_language == "my" {
//            dic["title"] = SelectedDic.stateOrDivitionNameMy
//        }else {
//            dic["title"] = SelectedDic.stateOrDivitionNameEn
//        }
    }
    
    func setTownshipCityNameFromDivison(Dic: TownShipDetailForAddress) {
        //self.setTownShip(township: Dic)
        print("Township-----\(self.sampledata)")
       print("*********************\(isComingFromAgentList)*************")
        
            if isComingFromAgentList{
                self.dismiss(animated: true, completion: {
                    self.dismiss(animated: true, completion: nil)
                })
            }else{
                if let nav =  self.navigationController {
                    nav.popViewController(animated: true)
                }else{
                    self.dismiss(animated: true, completion: nil)
                }
            }
        
      self.delegateRegistration?.didSelectLocationByTownshipRegistration(location: sampledata, township: Dic)
        
       
    }
}

fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}

class MainLocationCell: UITableViewCell {
    
    @IBOutlet weak var contentLbl: UILabel!{
        didSet{
            contentLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var contentSubLbl: UILabel!{
        didSet{
            contentLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var locImgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        contentLbl.font = UIFont.init(name: appFont, size: 15.0)
        locImgView.image = locImgView.image?.withRenderingMode(.alwaysTemplate)
        locImgView.tintColor = .lightGray
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}


//This delegate will be only used when presenting view controller from agent because to manage the flow
extension SelectMainLocationViewController: MapDelegateToPop {
    func setVariable(value: Bool){
        isComingFromAgentList = value
    }
}













