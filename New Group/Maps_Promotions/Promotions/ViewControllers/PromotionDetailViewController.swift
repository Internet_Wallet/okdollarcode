//
//  PromotionDetailViewController.swift
//  Promotions
//
//  Created by Uma Rajendran on 10/23/17.
//  Copyright © 2017 Uma Rajendran. All rights reserved.
//

import UIKit
import ParallaxHeader
import CoreData
import CoreLocation
import GoogleMaps

class PromotionDetailViewController: OKBaseController, CustomHeaderDelegate,WebServiceResponseDelegate {
  
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
        {
        didSet
        {
            headerLabel.text = headerLabel.text?.localized
            headerLabel.font =  UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var makePaymentBtn: UIButton!
        {
        didSet
        {
            self.makePaymentBtn.setTitle(self.makePaymentBtn.titleLabel?.text?.localized, for: .normal)
            self.makePaymentBtn.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    private let sliderHeight: CGFloat = 80
    var selectedPromotion: PromotionsModel?
    //var selectedPromotion: NearByServicesNewModel?
    var selectedNearByServices: NearByServicesModel?
    var selectedNearByServicesNew: NearByServicesNewModel?

    var isfavoritePromotion: Bool = false
    var strNearByPromotion: String = "Promotion"
    var strTimeDuration: String = ""
    var distance: String = ""
    var mainCatName: String = ""
    var iscategoryAPICall: Bool = false
    var categorylistArr = [String:Any]()

    ///MAP_VIEW//
    var map = GMSMapView()
    var mapCrossButton = UIButton()
    var mapView = UIView()
    var arrayPolyline = [GMSPolyline]()
    var selectedRought:String!

    
    
    //MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        
       // createMapView()
        
        self.navigationController?.isNavigationBarHidden = true
        super.viewWillAppear(animated)
       
            appDel.floatingButtonControl?.window.closeButton?.isHidden = true
            appDel.floatingButtonControl?.window.isHidden = true
        
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if(strNearByPromotion == "NearByService")
        {
            if ((selectedNearByServicesNew?.UserContactData?.PhoneNumber?.count)!) < 20 {
                
                self.getCategoryList()
                self.iscategoryAPICall = true
                makePaymentBtn.isHidden = false

            } else {
                
                self.iscategoryAPICall = false
                makePaymentBtn.isHidden = true
            }
            
            if let modelNearBy = selectedNearByServicesNew {
                selectedPromotion = PromotionsModel.init(model: modelNearBy)
                
            } else {
                
                if let modelNearBy = selectedNearByServices {
                    selectedPromotion = PromotionsModel.init(model: modelNearBy)
                }
            }
            
            if self.iscategoryAPICall == false {
                
                
                loadUI()
                loadDistanceInMinute()
                self.tableviewcall()
                addBackButton()
            }
            
        } else if(strNearByPromotion == "NearBy") {
        
        if let modelNearBy = selectedNearByServicesNew {
            selectedPromotion = PromotionsModel.init(model: modelNearBy)
        }
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        loadUI()
        loadDistanceInMinute()
        self.tableviewcall()
        addBackButton()
        
        
    } else {
            self.tableView.dataSource = self
            self.tableView.delegate = self
            loadUI()
            loadDistanceInMinute()
            self.tableviewcall()
            addBackButton()

        }

        //Load GUI
        self.tableView.dataSource = self
        self.tableView.delegate = self
        loadUI()
        loadDistanceInMinute()
        //loadDistanceFromLocation()
        self.tableviewcall()
        addBackButton()

     }
    
    func tableviewcall() {
        
        let imagesArr = ["betterthan.png"]
        
        //check to display time or not
        
        tableView.parallaxHeader.view = CustomHeaderView.instanceFromNib(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 260), images: imagesArr, delegate: self)
        //    tableView.parallaxHeader.view = imageView
        tableView.parallaxHeader.height = 260 // parallaxHeight //400
        tableView.parallaxHeader.minimumHeight = 0
        tableView.parallaxHeader.mode = .centerFill //.topFill
        tableView.parallaxHeader.parallaxHeaderDidScrollHandler = { parallaxHeader in
            //            println_debug("just check header scrolling or not")
            //            println_debug(parallaxHeader.progress)
        }
        tableView.tableFooterView = UIView.init(frame: CGRect.zero)
        
        //        if(strNearByPromotion == "Promotion" || strNearByPromotion == "NearBy")
        //        {
        isfavoritePromotion = favoriteExist()
        //checkAndAddToRecents()
        //        }
        
    }
    
    private func addBackButton() {
        let backBtn = UIBarButtonItem(image: UIImage(named: "tabBarBack"), style: .plain, target: self, action: #selector(backButtonAction))
        self.navigationItem.leftBarButtonItem  = backBtn
    }
        
    @objc func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func loadDistanceFromLocation()
    {
        let coordinate0 = CLLocation(latitude: selectedPromotion?.shop_Latitude ?? 0.0, longitude: selectedPromotion?.shop_Longtitude ?? 0.0)
        let coordinate1 = CLLocation(latitude: Double(geoLocManager.currentLatitude ?? "0.0")!, longitude: Double(geoLocManager.currentLongitude ?? "0.0")!)
        let tmpDistance = "\(coordinate0.distance(from: coordinate1))" // result is in meters
        
            let roundValue = (tmpDistance as NSString).floatValue
            //if roundValue > 1000 {
                let stringDistance = String.init(format: "%.2f Km", roundValue/1000)
                distance = stringDistance.replacingOccurrences(of: ".00", with: "")
//            } else {
//                let stringDistance = String.init(format: "%.2f Mile", roundValue)
//                distance = stringDistance
//            }
        
        self.tableView.reloadData()
    }
    
    func loadDistanceInMinute()
    {
        //http://maps.googleapis.com/maps/api/directions/json?origin=16.8166773,96.131959&destination=16.8092851,96.1350552&mode=driving&sensor=false
        
        if let curLat = geoLocManager.currentLatitude, let curLong = geoLocManager.currentLongitude {
            geoLocManager.getDurationData(sourceLatitude: curLat, sourceLongitude: curLong, destinationLatitude: curLat, destinationLongitude: curLong) { (_, address) in
                DispatchQueue.main.async {
                    if let addressUnwrapped = address as? String {
                        println_debug(addressUnwrapped)
                        self.strTimeDuration = addressUnwrapped
                        
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    func loadUI() {
        headerLabel.text = ""
        makePaymentBtn.backgroundColor = kYellowColor
        //headerHeightConstraint.constant = kHeaderHeight
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 
    @IBAction func backAction(_ sender: Any) {
        closeActionFromCustomHeader()
    }
    // MARK: - CATEGORY API CALL

    func getCategoryList() {
        
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self as WebServiceResponseDelegate
            showProgressView()
            
            let buldApi = Url.billSplitterMobileValidation + "MobileNumber=\(selectedNearByServicesNew?.UserContactData?.PhoneNumber ?? "")"
            
            
            let url = getUrl(urlStr: buldApi, serverType: .serverApp)
            println_debug(url)
            
            let params = Dictionary<String,String>()
            
            web.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", mScreen: "PromotionDetail")
        }
        
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        self.removeProgressView()
        //if screen == "PromotionDetail" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    print("promotiondetail category----\(dict)")
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        if dic["Code"]! as! Int == 200 {
                            DispatchQueue.main.async {
                                
                                let dataDict = dic["Data"] as? String
                                self.categorylistArr = OKBaseController.convertToDictionary(text: dataDict!)!
                                //print("promotiondetail category111----\(self.categorylistArr.safeValueForKey("MerchantCatName"))")
                                //print("promotiondetail category222----\(self.categorylistArr.count)")
                                self.mainCatName = self.categorylistArr.safeValueForKey("MerchantCatName") as! String
                                
                               self.tableView.reloadData()
                            }
                        } else {

                            DispatchQueue.main.async {
//                                alertViewObj.wrapAlert(title: nil, body:"Okdollar Account Number Not Exist. Please check!".localized, img:  #imageLiteral(resourceName: "phone_alert"))
//                                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
//                                }
//                                alertViewObj.showAlert(controller: self)
                            }
                        }
                    }
                }
            } catch {
                
            }
        //}
        
    }
    
    // MARK: - Delegate Methods
    
    func closeActionFromCustomHeader() {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    @objc func callBtnAction() {
        if let promotion = selectedPromotion {
            if let phonenumber = promotion.shop_Phonenumber {
                if phonenumber.count > 0 {
                    var mobNumber = getPhoneNumberByFormat(phoneNumber: phonenumber)
                    mobNumber = mobNumber.removingWhitespaces()
                    let phone = "tel://\(mobNumber)"
                    if let url = URL.init(string: phone) {
                        UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                    }
                }else{
                    showNoPhoneNumberError()
                }
            }else{
                showNoPhoneNumberError()
            }
        }else {
           showNoPhoneNumberError()
        }
    }
    
    func showNoPhoneNumberError() {
        alertViewObj.wrapAlert(title: nil, body: "No Phonenumber Exists".localized, img: nil)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        alertViewObj.showAlert(controller: self)
    }
    
    func checkAndAddToRecents() {
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "RecentPromotion")
        do {
            let shopId = self.selectedPromotion?.shop_Id ?? ""

            fetchRequest.predicate = NSPredicate(format: "promotionId == %@",shopId)
            let fetchedObjects = try managedContext.fetch(fetchRequest) as! [RecentPromotion]
            if fetchedObjects.count == 0 {
                // here need to add this promotion as recent looking into db
                guard let promotion = self.selectedPromotion  else {
                    return
                }
                let saveData = NSKeyedArchiver.archivedData(withRootObject: promotion)
                let entity = NSEntityDescription.entity(forEntityName: "RecentPromotion", in: managedContext)!
                let recentPromotion = NSManagedObject(entity: entity, insertInto: managedContext)
                recentPromotion.setValue(saveData, forKeyPath: "promotionData")
                recentPromotion.setValue(Date(), forKey: "addedDateTime")
                recentPromotion.setValue(self.selectedPromotion?.shop_Id, forKey: "promotionId")
                do {
                    try managedContext.save()
                    
                }catch let error as NSError{
                    println_debug("Could not save. \(error) , \(error.userInfo)")
                }
            }
        } catch {
            print ("fetch task failed", error)
        }
    }
    
    func favoriteExist() -> Bool {
            let managedContext = appDelegate.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FavoritePromotion")
            do {
                let shopId = self.selectedPromotion?.shop_Id ?? ""
                fetchRequest.predicate = NSPredicate(format: "promotionId == %@", shopId)
                let fetchedResults = try managedContext.fetch(fetchRequest) as! [FavoritePromotion]
                if let _ = fetchedResults.first {
                    return true
                }else {
                    return false
                }
            }
            catch {
                print ("fetch task failed", error)
            }
            return false
        
    }
    
    @objc func favoriteBtnAction() {
//        if(strNearByPromotion == "Promotion" || strNearByPromotion == "NearBy")
//        {
            addDeletePromotionInFavorite()
//        }
    }
    
    func addDeletePromotionInFavorite() {
        guard (self.selectedPromotion != nil) else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FavoritePromotion")
        do {
            let shopId = self.selectedPromotion?.shop_Id ?? ""
            fetchRequest.predicate = NSPredicate(format: "promotionId == %@", shopId)
            let fetchedResults = try managedContext.fetch(fetchRequest) as! [FavoritePromotion]
            if let aFavorite = fetchedResults.first {
                alertViewObj.wrapAlert(title: "", body: "Do you want to remove from favorite ?".localized, img: #imageLiteral(resourceName: "promotion_star"))
                alertViewObj.addAction(title: "Yes,Remove".localized, style: .target , action: {
                    // here need to delete the favorite from db
                    managedContext.delete(aFavorite)
                    do {
                        try managedContext.save()
                        self.isfavoritePromotion = false
                        let indexPath = IndexPath(item: 0, section: 1)
                        self.tableView.reloadRows(at: [indexPath], with: .fade)
                        
                    } catch let error as NSError {
                        println_debug("Could not save. \(error), \(error.userInfo)")
                    }
                })
                alertViewObj.showAlert(controller: self)
            }else {
                // here need to add the favorite
                alertViewObj.wrapAlert(title: "", body: "Do you want to add as favorite ?".localized, img: #imageLiteral(resourceName: "promotion_star"))
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                    // here need to add the favorite from db
                    guard let promotion = self.selectedPromotion else {
                        return
                    }
                    let saveData = NSKeyedArchiver.archivedData(withRootObject: promotion)
                    let managedContext = appDelegate.persistentContainer.viewContext
                    let entity = NSEntityDescription.entity(forEntityName: "FavoritePromotion",
                                                            in: managedContext)!
                    let favPromotion = NSManagedObject(entity: entity,
                                                       insertInto: managedContext)
                    favPromotion.setValue(saveData, forKeyPath: "promotionData")
                    favPromotion.setValue(Date(), forKeyPath: "addedDateTime")
                    favPromotion.setValue(self.selectedPromotion?.shop_Id, forKeyPath: "promotionId")
                    do {
                        try managedContext.save()
                        self.isfavoritePromotion = true
                        let indexPath = IndexPath(item: 0, section: 1)
                        self.tableView.reloadRows(at: [indexPath], with: .fade)
                        
                    } catch let error as NSError {
                        println_debug("Could not save. \(error), \(error.userInfo)")
                    }
                })
               
                alertViewObj.showAlert(controller: self)
            }
            
        }
        catch {
            print ("fetch task failed", error)
        }
        
    }
    
    
    @objc func directionBtnAction() {
        
        
        guard let promotion = self.selectedPromotion else {
                return
            }
            if let curLat = geoLocManager.currentLatitude, let curLong = geoLocManager.currentLongitude {
                var urlStr = ""
                    if(strNearByPromotion == "NearBy") {
                         urlStr = "http://maps.google.com/maps?saddr=\(curLat),\(curLong)&daddr=\(promotion.shop_GeoLocation?.lattitude ?? 0.0),\(promotion.shop_GeoLocation?.longitude ?? 0.0)"
                    }else{
                      urlStr = "http://maps.google.com/maps?saddr=\(curLat),\(curLong)&daddr=\(promotion.shop_Latitude ?? 0.0),\(promotion.shop_Longtitude ?? 0.0)"
                    }
                        if let url = URL.init(string: urlStr) {
                            UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                        }
                    }
            }
      
   
    
    @objc func shareBtnAction() {
             guard let promotion = self.selectedPromotion else {
                return
            }
        
            var mobileNumber = ""
            if let phonenumber = promotion.shop_Phonenumber {
                if phonenumber.count > 0 {
                    mobileNumber = getPlusWithDestinationNum (phonenumber, withCountryCode: "+95")
                }
            }
        
            let textToShare: String = "OK$\n\(promotion.shop_Name!)\n\(promotion.shop_AgentName!)\n\(mobileNumber)\n"
        var link: String = "https://www.google.com/maps/search/?api=1&query=\(promotion.shop_GeoLocation?.lattitude ?? 0.0),\(promotion.shop_GeoLocation?.longitude ?? 0.0)"
        
          if(strNearByPromotion == "NearBy") {
            link = "https://www.google.com/maps/search/?api=1&query=\(promotion.shop_GeoLocation?.lattitude ?? 0.0),\(promotion.shop_GeoLocation?.longitude ?? 0.0)"
          } else {
            link = "https://www.google.com/maps/search/?api=1&query=\(promotion.shop_Latitude ?? 0.0),\(promotion.shop_Longtitude ?? 0.0)"
        }
            let url:NSURL = NSURL.init(string: link)!
            var activityItems = [Any]()
            activityItems.append(textToShare)
            activityItems.append(url)
            
            let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
            activityViewController.excludedActivityTypes = [.assignToContact, .print]
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
         
    }
    
    @objc func navigateToMapDetailViewAction() {
      
        let storyboard: UIStoryboard = UIStoryboard(name: "Promotion", bundle: nil)
        let mapDetailsView = storyboard.instantiateViewController(withIdentifier: "MapDetailView_ID") as! MapDetailViewController
        
        if(strNearByPromotion == "NearBy") {

            mapDetailsView.selectedPromotion = self.selectedPromotion
            mapDetailsView.isFavorite = self.isfavoritePromotion
            mapDetailsView.screencheck = "NearBy"
            mapDetailsView.nearbyLatitude = selectedNearByServicesNew?.UserContactData?.LocationNewData?.Latitude
            mapDetailsView.nearbyLongtitude = selectedNearByServicesNew?.UserContactData?.LocationNewData?.Longitude
        } else {
        
        mapDetailsView.selectedPromotion = self.selectedPromotion
        mapDetailsView.isFavorite = self.isfavoritePromotion
            mapDetailsView.screencheck = "Promotion"
             mapDetailsView.nearbyLatitude = selectedNearByServicesNew?.UserContactData?.LocationNewData?.Latitude
            mapDetailsView.nearbyLongtitude = selectedNearByServicesNew?.UserContactData?.LocationNewData?.Longitude
        }
        self.navigationController?.pushViewController(mapDetailsView, animated: true)

    }
    
    @IBAction func makePaymentBtnAction(_ sender: Any) {
        self.navigateToPayto(phone: self.selectedPromotion?.shop_Phonenumber ?? "" , amount: "", name: selectedPromotion?.shop_AgentName ?? "", fromMerchant: true)
    }

}

extension PromotionDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowCount: Int = 0
        if section == 0 {
            rowCount = 1
        }else {
                rowCount = 8
        }
        return rowCount
    }
    
    func getTime(timeStr: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "HH:mm:ss"
//        if(strNearByPromotion == "NearBy")
//        {
//            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
//        }
        //dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let time = dateFormatter.date(from: timeStr)
        dateFormatter.dateFormat = "hh:mm a"
        if let t = time  {
            let hour = dateFormatter.string(from: t)
            return hour
        } else {
            return ""
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let headerCell = tableView.dequeueReusableCell(withIdentifier: "listDetailHeaderCellIdentifier", for: indexPath) as! MapDetailViewHeaderCell
            headerCell.backgroundColor = kYellowColor
            headerCell.shopNameLabel.type = .rightLeft
            headerCell.showText(text: selectedPromotion?.shop_Name ?? "")
           // headerCell.shopNameLabel.text = "https://s3-ap-southeast-1.amazonaws.com/okdollar/ReqDocuments00959787190850/62b0e06d-bd8d-45df-8820-d2a0bf3bc797ReqDocumentsMarch_10_2020 11_02_42"//selectedPromotion?.shop_Name
            for img in headerCell.rateImgViewList {
                img.isHighlighted = false
            }
            
            headerCell.shopDistanceLabel.text = "0 Km"
            if let shopDistance = selectedNearByServicesNew?.distanceInKm {
                headerCell.shopDistanceLabel.text = String(format: "%.2f Km", shopDistance).replacingOccurrences(of: ".00", with: "")
            }
            
            headerCell.timeLabel.text = strTimeDuration

            return headerCell
        }else {
            
                switch indexPath.row {
                case 0:
                    // here load options cell
                    let optionsCell = tableView.dequeueReusableCell(withIdentifier: "listDetailOptionCellIdentifier", for: indexPath) as! MapDetailViewOptionsCell
                    if isfavoritePromotion {
                        optionsCell.favoriteImgV.image = UIImage.init(named: "act_favorite")
                    }else {
                        optionsCell.favoriteImgV.image = UIImage.init(named: "favorite")
                    }
                    
                    if selectedPromotion?.shop_Phonenumber?.count ?? 0 > 15 {
                        optionsCell.callBtn.isEnabled = false
                    } else {
                        optionsCell.callBtn.addTarget(self, action: #selector(callBtnAction), for: UIControl.Event.touchUpInside)
                        optionsCell.callBtn.isEnabled = true
                    }
                    
                    optionsCell.favoriteBtn.addTarget(self, action: #selector(favoriteBtnAction), for: UIControl.Event.touchUpInside)
                    optionsCell.directionBtn.addTarget(self, action: #selector(directionBtnAction), for: UIControl.Event.touchUpInside)
                    optionsCell.shareBtn.addTarget(self, action: #selector(shareBtnAction), for: UIControl.Event.touchUpInside)
                    return optionsCell
                    
                case 1:
                    let descriptionCell = tableView.dequeueReusableCell(withIdentifier: "listDetailDescCellIdentifier", for: indexPath) as! MapDetailViewDescriptionCell
                    descriptionCell.wrapData(title: (selectedPromotion?.shop_AgentName)!, imgName: "promotion_user.png", isLocationCell: false)
                    return descriptionCell
                    
                case 2:
                    
                    let categoryCell = tableView.dequeueReusableCell(withIdentifier: "listDetailCategoryCellIdentifier", for: indexPath) as! MapDetailViewCategoryCell
                    
                    if(appDelegate.currentLanguage == "my")
                    {
                        if (selectedPromotion?.shop_Category?.categoryNameMY) != "" {
                            categoryCell.wrapData(categorytitle: ((selectedPromotion?.shop_Category?.categoryNameMY)!))
                        } else {
                             if (categorylistArr.count > 0)  { categoryCell.wrapData(categorytitle:self.categorylistArr.safeValueForKey("MerchantCatName") as! String)
                            } else {
                                categoryCell.wrapDataNew(categorytitle:"Not available".localized)
                            }
                        }
                        
                    } else {
                        
                        if (selectedPromotion?.shop_Category?.categoryNameEN) != "" {
                            categoryCell.wrapData(categorytitle: ((selectedPromotion?.shop_Category?.categoryNameEN)!))
                        } else {
                            
                            if (categorylistArr.count > 0)  { categoryCell.wrapData(categorytitle:self.categorylistArr.safeValueForKey("MerchantCatName") as! String)
                            } else {
                                categoryCell.wrapData(categorytitle:"Not available".localized)
                            }
                        }
                    }
                    return categoryCell
                case 3:
                    let categoryCell = tableView.dequeueReusableCell(withIdentifier: "listDetailCategoryCellIdentifier", for: indexPath) as! MapDetailViewCategoryCell
                    if(appDelegate.currentLanguage == "my"){
                        if ((selectedPromotion?.shop_Category?.businessType?.businessType_NameMY) != "") {
                            categoryCell.wrapData(categorytitle: ((selectedPromotion?.shop_Category?.businessType?.businessType_NameMY)!))
                        } else {
                                categoryCell.wrapDataNew(categorytitle:"Not available".localized)
                        }
                    } else {
                        if (selectedPromotion?.shop_Category?.businessType?.businessType_NameEN) != "" {
                            categoryCell.wrapDataNew(categorytitle: ((selectedPromotion?.shop_Category?.businessType?.businessType_NameEN)!))
                        } else {
                                categoryCell.wrapDataNew(categorytitle:"Not available".localized)
                        }
                    }
                    
                    return categoryCell
                case 4:
                    let descriptionCellTime = tableView.dequeueReusableCell(withIdentifier: "listDetailDescCellIdentifier", for: indexPath) as! MapDetailViewDescriptionCell
                    if let openTime = selectedPromotion?.shop_OpenTime , let closeTime = selectedPromotion?.shop_CloseTime {
                        
                        if openTime.count > 0 && closeTime.count > 0 {
                          if openTime == "00:00:00" && closeTime == "00:00:00" {
                                descriptionCellTime.wrapData(title:  "Open 24/7", imgName: "working24", isLocationCell: false)
                            } else {
                                descriptionCellTime.wrapData(title:  "\(self.getTime(timeStr: openTime)) - \(self.getTime(timeStr: closeTime))", imgName: "working24", isLocationCell: false)
                            }
                        } else {
                            descriptionCellTime.wrapData(title:  "Open 24/7", imgName: "working24", isLocationCell: false)
                        }
                    }
                    descriptionCellTime.descBtn.isHidden = true

                    return descriptionCellTime
                case 5:
                    let descriptionCell = tableView.dequeueReusableCell(withIdentifier: "listDetailDescCellIdentifier", for: indexPath) as! MapDetailViewDescriptionCell
                    
                    var address = ""
                    
                    if(strNearByPromotion == "NearBy")
                    {
                        address = "\(selectedPromotion?.shop_Address?.addressLine2 ?? "") , \(selectedPromotion?.shop_Address?.addressLine1 ?? "")"
                    } else if(strNearByPromotion == "NearByService") {
                      address = "\(selectedPromotion?.shop_NewAddress ?? "")"
                    } else {
                        address = "\(selectedPromotion?.shop_Address?.addressLine2 ?? "") , \(selectedPromotion?.shop_Address?.addressLine1 ?? "")"
                    }

                    descriptionCell.wrapData(title: address, imgName: "pin.png", isLocationCell: true)
                    descriptionCell.mapBtn.addTarget(self, action: #selector(navigateToMapDetailViewAction), for: UIControl.Event.touchUpInside)
                    let pinImage = UIImage(named: "ok.png")
                    descriptionCell.mapCategoryIcon.image = pinImage
                    
                    UIView.animate(withDuration: 0.5, delay: 0.4, options: .curveEaseOut, animations: {
                        descriptionCell.mapCategoryIcon.alpha = 1
                    }, completion: nil)
                    return descriptionCell
                case 6:
                    let descriptionCell = tableView.dequeueReusableCell(withIdentifier: "listDetailDescCellIdentifier", for: indexPath) as! MapDetailViewDescriptionCell
                    let mobileNumber = getPlusWithDestinationNum( (selectedPromotion?.shop_Phonenumber)!, withCountryCode: "+95")
                    if mobileNumber.length > 15 {
                        descriptionCell.descBtn.isEnabled = false
                        descriptionCell.wrapData(title: "NA", imgName: "b_call.png", isLocationCell: false)
                    } else {
                        descriptionCell.descBtn.addTarget(self, action: #selector(callBtnAction), for: UIControl.Event.touchUpInside)
                        descriptionCell.descBtn.isEnabled = true
                        descriptionCell.wrapData(title: mobileNumber, imgName: "b_call.png", isLocationCell: false)
                    }
                    
                    return descriptionCell
                case 7:
                    let descriptionCell = tableView.dequeueReusableCell(withIdentifier: "listDetailDescCellIdentifier", for: indexPath) as! MapDetailViewDescriptionCell
                    let mobileNumber =  selectedPromotion?.shop_Email
                    
                    descriptionCell.wrapDataNew(title: mobileNumber!, imgName: "b_division", isLocationCell: false)
                   // descriptionCell.descBtn.addTarget(self, action: #selector(callBtnAction), for: UIControl.Event.touchUpInside)
                    
                    return descriptionCell
                default:
                    break
                }
        }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var rowHeight: CGFloat = 0.0
        if indexPath.section == 0 {
            rowHeight = 90
        }else if (indexPath.section == 1 && indexPath.row == 0) {
            rowHeight = 100
        }else if (indexPath.section == 1 && indexPath.row == 2){
            rowHeight = 70
        }else {
            rowHeight = 70
        }
        return rowHeight
    }
}

class MapDetailViewHeaderCell: UITableViewCell {
   
    @IBOutlet weak var shopNameLabel: MarqueeLabel!
//    @IBOutlet weak var shopRateImgView: UIImageView!
    @IBOutlet weak var shopDistanceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet var rateImgViewList: [UIImageView]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        shopNameLabel.font = UIFont.init(name: appFont, size: 20.0)
        shopDistanceLabel.font = UIFont.init(name: appFont, size: 18.0)
        timeLabel.font = UIFont.init(name: appFont, size: 16.0)
        self.backgroundColor = kYellowColor
    }
    
    func showText(text: String){
        shopNameLabel.type = .continuous
        shopNameLabel.text = text
    }

}

class MapDetailViewOptionsCell: UITableViewCell {
    
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var callLabel: UILabel!
    {
        didSet
        {
            callLabel.text = "Call".localized
        }
    }
    @IBOutlet weak var callImgView: UIImageView!
    @IBOutlet weak var favoriteBtn: UIButton!
    @IBOutlet weak var favoriteImgV: UIImageView!
    @IBOutlet weak var favoriteLabel: UILabel!
        {
        didSet
        {
            favoriteLabel.text = "Favorite".localized
        }
    }
    @IBOutlet weak var directionBtn: UIButton!
    @IBOutlet weak var directionLabel: UILabel!
        {
        didSet
        {
            directionLabel.text = "Direction".localized
        }
    }
    @IBOutlet weak var directionImgV: UIImageView!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var shareLabel: UILabel!
        {
        didSet
        {
            shareLabel.text = "Share Details".localized
        }
    }
    @IBOutlet weak var shareImgV: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        callLabel.font = UIFont.init(name: appFont, size: 15.0)
        favoriteLabel.font = UIFont.init(name: appFont, size: 15.0)
        directionLabel.font = UIFont.init(name: appFont, size: 15.0)
        shareLabel.font = UIFont.init(name: appFont, size: 15.0)

        callLabel.textColor = UIColor.black
        favoriteLabel.textColor = UIColor.black
        directionLabel.textColor = UIColor.black
        shareLabel.textColor = UIColor.black
        callImgView.image = UIImage.init(named: "phone.png")
       // favoriteImgV.image = UIImage.init(named: "favorite.png")
        directionImgV.image = UIImage.init(named: "direction.png")
        shareImgV.image = UIImage.init(named: "share_details.png")

    }
    
    
}

class MapDetailViewDescriptionCell: UITableViewCell {
    @IBOutlet weak var descIconView: UIImageView!
    @IBOutlet weak var descLabel: FRHyperLabel!
    @IBOutlet weak var descBtn: UIButton!
    @IBOutlet weak var mapBtn: UIButton!
    @IBOutlet weak var mapBtnWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapCategoryIcon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        descLabel.textColor = UIColor.darkGray
        descLabel.font = UIFont.init(name: appFont, size: 16.0)
    }
    
    func wrapData(title: String, imgName: String, isLocationCell: Bool) {
        self.descLabel.text = title
        self.descIconView.image = UIImage.init(named: imgName)
        self.mapBtn.isHidden = isLocationCell ? false : true
        self.mapCategoryIcon.isHidden = isLocationCell ? false : true
        self.mapBtnWidthConstraint.constant = isLocationCell ? 70 : 0
        self.descBtn.isHidden = false
        
        mapBtn.layer.shadowColor = kBackGroundGreyColor.cgColor
        mapBtn.layer.shadowOpacity = 1
        mapBtn.layer.shadowOffset = CGSize.zero
        mapBtn.layer.shadowRadius = 3
        mapCategoryIcon.layer.shadowColor = UIColor.lightGray.cgColor
        mapCategoryIcon.layer.shadowOpacity = 1
        mapCategoryIcon.layer.shadowOffset = CGSize.zero
        mapCategoryIcon.layer.shadowRadius = 4
    }
    
    func wrapDataNew(title: String, imgName: String, isLocationCell: Bool) {
        self.descLabel.text = title
        //self.descLabel.textColor = UIColor.blue
        self.descBtn.isHidden = true

        let handlerEmail = {
            (hyperLabel: FRHyperLabel?, substring: String?) -> Void in
            
            if let url = URL(string: "mailto:" + self.descLabel.text!) {
                UIApplication.shared.open(url)
            }
            else{
                alertViewObj.wrapAlert(title: "", body: "No Mail Configured".localized, img: nil)
                alertViewObj.addAction(title: "OK", style: .target , action: {
                    
                })
                
                self.descLabel.text = "Not available".localized

               // alertViewObj.showAlert(controller: self.superview)
                //println_debug("No Mail Configured")
            }
            
        }
        
        self.descLabel.setLinksForSubstrings(["\(self.descLabel.text!)"], withLinkHandler: handlerEmail)
        
        self.descIconView.image = UIImage.init(named: imgName)
        self.mapBtn.isHidden = isLocationCell ? false : true
        self.mapCategoryIcon.isHidden = isLocationCell ? false : true
        self.mapBtnWidthConstraint.constant = isLocationCell ? 70 : 0
        
        mapBtn.layer.shadowColor = kBackGroundGreyColor.cgColor
        mapBtn.layer.shadowOpacity = 1
        mapBtn.layer.shadowOffset = CGSize.zero
        mapBtn.layer.shadowRadius = 3
        mapCategoryIcon.layer.shadowColor = UIColor.lightGray.cgColor
        mapCategoryIcon.layer.shadowOpacity = 1
        mapCategoryIcon.layer.shadowOffset = CGSize.zero
        mapCategoryIcon.layer.shadowRadius = 4
    }
    
    
}

class MapDetailViewCategoryCell: UITableViewCell {
    @IBOutlet weak var categoryImgV: UIImageView!
    @IBOutlet weak var categoryNameLbl: UILabel!
    @IBOutlet weak var categoryContentLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        categoryNameLbl.textColor = UIColor.black
        categoryContentLbl.textColor = UIColor.darkGray
        categoryNameLbl.font = UIFont.init(name: appFont, size: 16.0)
        categoryContentLbl.font = UIFont.init(name: appFont, size: 16.0)
 
    }
    
    func wrapData(categorytitle: String) {
        categoryImgV.image = UIImage.init(named: "categories.png")
        categoryNameLbl.text = "Category".localized
        categoryContentLbl.text = categorytitle
    }
    
    func wrapDataNew(categorytitle: String) {
        categoryImgV.image = UIImage.init(named: "promotion_categories.png")
        categoryNameLbl.text = "Sub-Category".localized
        categoryContentLbl.text = categorytitle
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}


extension PromotionDetailViewController{
    
    
    func createMapView(){
        mapView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        map.frame = mapView.frame
        view.addSubview(mapView)
        mapView.backgroundColor =  UIColor.black.withAlphaComponent(0.5)
        mapCrossButton.frame = CGRect(x: mapView.frame.size.width - 70, y: 50, width: 50, height: 50)
        mapCrossButton.backgroundColor = .red
        mapCrossButton.setTitle("X", for: .normal)
        mapCrossButton.addTarget(self, action: #selector(mapCrossButtonAction), for: .touchUpInside)
        mapView.addSubview(map)
        mapView.addSubview(mapCrossButton)
        mapView.isHidden = true
        
    }
    
    func hide(hidden :Bool){
        
        UIView.transition(with: view, duration: 3, options: .transitionCrossDissolve,
                          animations: {
                            // Animations
                            self.mapView.isHidden = hidden
        },
                          completion: { finished in
                            // Compeleted
        })
    }
    
    
    @objc func mapCrossButtonAction(sender: UIButton!) {
       hide(hidden :true)
    }
    
    func LoadMapRoute(urlStr: String ,source:CLLocationCoordinate2D ,and destination :CLLocationCoordinate2D){
        
        let url = URL(string: urlStr)
        URLSession.shared.dataTask(with: url!, completionHandler:
            {
                (data, response, error) in
                if(error != nil)
                {
                    print("error")
                }
                else
                {
                    do{
                        let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                        let arrRouts = json["routes"] as! NSArray
                        
                        for  polyline in self.arrayPolyline
                        {
                            polyline.map = nil;
                        }
                        
                        self.arrayPolyline.removeAll()
                        
                        let pathForRought:GMSMutablePath = GMSMutablePath()
                        
                        if (arrRouts.count == 0)
                        {
                            let distance:CLLocationDistance = CLLocation.init(latitude: source.latitude, longitude: source.longitude).distance(from: CLLocation.init(latitude: destination.latitude, longitude: destination.longitude))
                            
                            pathForRought.add(source)
                            pathForRought.add(destination)
                            
                            let polyline = GMSPolyline.init(path: pathForRought)
                            self.selectedRought = pathForRought.encodedPath()
                            polyline.strokeWidth = 5
                            polyline.strokeColor = UIColor.blue
                            polyline.isTappable = true
                            
                            self.arrayPolyline.append(polyline)
                            
                            if (distance > 8000000)
                            {
                                polyline.geodesic = false
                            }
                            else
                            {
                                polyline.geodesic = true
                            }
                            
                            polyline.map = self.map;
                        }
                        else
                        {
                            for (index, element) in arrRouts.enumerated()
                            {
                                let dicData:NSDictionary = element as! NSDictionary
                                
                                let routeOverviewPolyline = dicData["overview_polyline"] as! NSDictionary
                                
                                let path =  GMSPath.init(fromEncodedPath: routeOverviewPolyline["points"] as! String)
                                
                                let polyline = GMSPolyline.init(path: path)
                                
                                polyline.isTappable = true
                                
                                self.arrayPolyline.append(polyline)
                                
                                polyline.strokeWidth = 5
                                
                                if index == 0
                                {
                                    self.selectedRought = routeOverviewPolyline["points"] as? String
                                    
                                    polyline.strokeColor = UIColor.blue;
                                }
                                else
                                {
                                    polyline.strokeColor = UIColor.darkGray;
                                }
                                
                                polyline.geodesic = true;
                            }
                            
                            for po in self.arrayPolyline.reversed()
                            {
                                po.map = self.map;
                            }
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5)
                        {
                            let bounds:GMSCoordinateBounds = GMSCoordinateBounds.init(path: GMSPath.init(fromEncodedPath: self.selectedRought)!)
                            self.map.animate(with: GMSCameraUpdate.fit(bounds))
                            self.hide(hidden: false)
                        }
                    }
                    catch let error as NSError
                    {
                        print("error:\(error)")
                    }
                }
        }).resume()
    }
    
    
    
}


extension UIViewController {
    
    func presentDetail(_ viewControllerToPresent: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.75
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromTop
        self.view.window!.layer.add(transition, forKey: kCATransition)
        viewControllerToPresent.modalPresentationStyle = .fullScreen
        present(viewControllerToPresent, animated: false)
        
    }
    
    func dismissDetail() {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromBottom
        self.view.window!.layer.add(transition, forKey: kCATransition)
        
        dismiss(animated: false)
    }
}
