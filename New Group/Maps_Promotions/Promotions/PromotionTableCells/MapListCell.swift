//
//  MapListCell.swift
//  Promotions
//
//  Created by Uma Rajendran on 10/20/17.
//  Copyright © 2017 Uma Rajendran. All rights reserved.
//

import UIKit
import CoreLocation

class MapListCell: UITableViewCell {
    
    @IBOutlet weak var photoImgView: UIImageView!
    
    @IBOutlet weak var nameLbl: UILabel!{
        didSet{
            nameLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var phonenumberLbl: UILabel!
    
    @IBOutlet weak var rateImgView: UIImageView!
    
    @IBOutlet weak var locationImgView: UIImageView!
    
    @IBOutlet weak var locationLbl: UILabel!{
        didSet{
            locationLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var timeImgView: UIImageView!
    
    @IBOutlet weak var timeLbl: UILabel!
    
    @IBOutlet weak var callBtn: UIButton!
    
    @IBOutlet weak var distanceLblKm: UILabel!
    @IBOutlet weak var distanceLblMile: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var timeLblHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var cashInLbl: UILabel!{
        didSet{
            cashInLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var cashOutLbl: UILabel!{
        didSet{
            cashOutLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var cashInLblHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cashOutLblHeightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet var rateImgViewList: [UIImageView]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //setUpMarqueeLabel(label: self.nameLbl)
        // setUpMarqueeLabel(label: self.remarksLbl)
        nameLbl.font = UIFont.init(name: appFont, size: 16)
        phonenumberLbl.font = UIFont.init(name: appFont, size: 16)
        
        // remarksLbl.font = UIFont.init(name: appFont, size: 15)
        locationLbl.font = UIFont.init(name: appFont, size: 15)
        timeLbl.font = UIFont.init(name: appFont, size: 15)
        distanceLblKm.font = UIFont.init(name: appFont, size: 12)
        //distanceLblMile.font = UIFont.init(name: appFont, size: 12)
        durationLbl.font = UIFont.init(name: appFont, size: 12)
        cashInLbl.font = UIFont.init(name: appFont, size: 14)
        cashOutLbl.font = UIFont.init(name: appFont, size: 14)
        
        nameLbl.textColor = UIColor.black
        phonenumberLbl.textColor = UIColor.black
        locationLbl.textColor = UIColor.darkGray
        timeLbl.textColor = UIColor.darkGray
        // remarksLbl.textColor = kBlueColor
        distanceLblKm.textColor = UIColor.darkGray
        //distanceLblMile.textColor = UIColor.darkGray
        durationLbl.textColor = UIColor.black
        cashInLbl.textColor = UIColor.darkGray
        cashOutLbl.textColor = UIColor.darkGray
        
       // photoImgView.layer.borderColor = kYellowColor.cgColor
       // photoImgView.layer.borderWidth = 0.5
        //        photoImgView.layer.shadowOffset = CGSize.zero
        //        photoImgView.layer.shadowRadius = 2
        
    }
    
    func setUpMarqueeLabel(label: MarqueeLabel) {
        label.tag = 501
        label.type = .continuous
        label.speed = .duration(20)
        label.fadeLength = 10.0
        label.trailingBuffer = 30.0
        label.font = UIFont.init(name: appFont, size: 16)
        //        label.text = "This text is long, and can be paused with a tap - handled via a UIGestureRecognizer!"
        //
        label.isUserInteractionEnabled = true // Don't forget this, otherwise the gesture recognizer will fail (UILabel has this as NO by default)
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(pauseTap))
        tapRecognizer.numberOfTapsRequired = 1
        tapRecognizer.numberOfTouchesRequired = 1
        label.addGestureRecognizer(tapRecognizer)
    }
    
    @objc func pauseTap(_ recognizer: UIGestureRecognizer) {
        let continuousLabel2 = recognizer.view as! MarqueeLabel
        if recognizer.state == .ended {
            continuousLabel2.isPaused ? continuousLabel2.unpauseLabel() : continuousLabel2.pauseLabel()
        }
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func getTime(timeStr: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        if let time = dateFormatter.date(from: timeStr) {
            dateFormatter.dateFormat = "hh:mm a"
            let hour = dateFormatter.string(from: time)
            return hour
        }
        return ""
    }
    
    
    private func getPlusWithDestinationNum(_ phoneNum: String, withCountryCode countryCode: String) -> String {
        var FormateStr: String
        if phoneNum.hasPrefix("0") {
            _ = (phoneNum).substring(from: 1)
//            println_debug(phoneNum)
        }
        let notAllowedChars = CharacterSet(charactersIn: "+()")
        let resultString: String = countryCode.components(separatedBy: notAllowedChars).joined(separator: "")
        if (countryCode == "+95") || (countryCode == "(+95)") {
            if phoneNum.hasPrefix("9") {
                FormateStr = "00\(resultString)\(phoneNum)"
            }
            else if phoneNum.hasPrefix("0") {
                FormateStr = "+\((phoneNum).substring(from: 2))"
            }
            else {
                FormateStr = "00\(resultString)9\(phoneNum)"
            }
        }
        else {
            FormateStr = "00\(resultString)\((phoneNum).substring(from: 1))"
        }
        return FormateStr.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    //0095 Formate
    private func getDestinationNum(_ phoneNum: String, withCountryCode countryCode: String) -> String {
        var FormateStr: String
        if phoneNum.hasPrefix("0") {
            _ = (phoneNum).substring(from: 1)
//            println_debug(phoneNum)
        }
        let notAllowedChars = CharacterSet(charactersIn: "+()")
        let resultString: String = countryCode.components(separatedBy: notAllowedChars).joined(separator: "")
        if (countryCode == "+95") || (countryCode == "(+95)") {
            if phoneNum.hasPrefix("9") {
                FormateStr = "00\(resultString)\(phoneNum)"
            }
            else if phoneNum.hasPrefix("0") {
                FormateStr = "00\(resultString)\((phoneNum).substring(from: 1))"
            }
            else {
                FormateStr = "00\(resultString)9\(phoneNum)"
            }
        }
        else {
            FormateStr = "00\(resultString)\((phoneNum))"
        }
        return FormateStr.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    private func getNumberFormat(_ str: String) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        //  let groupingSeparator = NSLocale.current[.groupingSeparator] as? String
        
        let groupingSeparator = NSLocale.current.groupingSeparator
        formatter.groupingSeparator = groupingSeparator ?? ""
        formatter.groupingSize = 3
        formatter.alwaysShowsDecimalSeparator = false
        formatter.usesGroupingSeparator = true
        if str.contains(".") {
            formatter.minimumFractionDigits = 2
        }
        let amount: String? = formatter.string(from: Double(str)! as NSNumber)
        return amount ?? ""
    }
    
    private func durationFromLatLong(lat:Double?,long:Double?) -> String {
        var strDuration = ""
        let coordinate0 = CLLocation(latitude: lat ?? 0.0, longitude: long ?? 0.0)
        let coordinate1 = CLLocation(latitude: Double(geoLocManager.currentLatitude ?? "0.0")!, longitude: Double(geoLocManager.currentLongitude ?? "0.0")!)
        let distance = coordinate0.distance(from: coordinate1) // result is in meters
        let time = (distance / 500).rounded()
        if time < 1 {
            strDuration = "1 Min"
        } else {
            if time > 60 {
                let tmpTime = Int(time) % 60
                strDuration = "\(Int(tmpTime)) Hr"
            } else {
                strDuration = "\(Int(time)) Mins"
            }
        }
        return strDuration
    }
    
    func durationFromDistance(distance:Double?) -> String {
        var strDuration = ""
        let intDist = Int(distance! * 1000.0)
        let time = (intDist / 500)
        if time < 1 {
            strDuration = "1 Min"
        } else {
            if time > 60 {
                let tmpTime = Int(time) / 60
                strDuration = "\(Int(tmpTime)) Hr"
            } else {
                strDuration = "\(Int(time)) Mins"
            }
        }
        return strDuration
    }
    
    func wrapData(promotion: NearByServicesNewModel) {
        self.photoImgView.image = UIImage.init(named: "cicoCamera")
//        self.photoImgView.layer.borderWidth = 1.0
//        self.photoImgView.layer.masksToBounds = false
//        self.photoImgView.layer.borderColor = UIColor(red: 246/255, green: 198/255, blue: 0/225, alpha: 1).cgColor
//        self.photoImgView.layer.cornerRadius = self.photoImgView.frame.size.width / 2
//        self.photoImgView.clipsToBounds = true
        self.nameLbl.text = promotion.UserContactData?.BusinessName
        let agentCodenew  = getPlusWithDestinationNum((promotion.UserContactData?.PhoneNumber ?? ""), withCountryCode: "+95")
        self.phonenumberLbl.text = agentCodenew
        if((promotion.UserContactData?.BusinessName?.count ?? 0) == 0) {
            self.nameLbl.text = promotion.UserContactData?.FirstName
        }
        if let openTime = promotion.UserContactData!.OfficeHourFrom , let closeTime = promotion.UserContactData!.OfficeHourTo {

            if openTime.count > 0 && closeTime.count > 0 {
                self.timeImgView.isHidden = false
                self.timeLbl.isHidden = false
                self.timeLblHeightConstraint.constant = 20
                //listCell.timeImgView.image = UIImage.init(named: "time")
                if openTime == "00:00:00" && closeTime == "00:00:00" {
                    self.timeLbl.text = "Open 24/7"
                } else {
                    self.timeLbl.text = "\(self.getTime(timeStr: openTime)) - \(self.getTime(timeStr: closeTime))"
                }
            }else {
                self.timeImgView.isHidden = true
                self.timeLbl.isHidden = true
                self.timeLblHeightConstraint.constant = 0
            }
        }
        self.locationImgView.image = UIImage.init(named: "location.png")
        if let addressnew = promotion.UserContactData?.Address {
            if addressnew.count > 0 {
                self.locationLbl.text = "\(addressnew)"
            } else {
                self.locationLbl.text = "\(promotion.UserContactData?.Township ?? ""),\(promotion.UserContactData?.Division ?? "")"
            }
        }
        //CashIn And CashOut
        if let cashinamount = promotion.UserContactData?.CashInLimit , let cashoutamount = promotion.UserContactData?.CashOutLimit {
            
            if cashinamount > 0 {
                let phone =  self.getNumberFormat("\(promotion.UserContactData!.CashInLimit!)").replacingOccurrences(of: ".00", with: "")
                self.cashInLbl.text = "Maximum Cash In Amount".localized + " : \(phone)" + " MMK"
                self.cashInLblHeightConstraint.constant = 20
            } else {
                self.cashInLblHeightConstraint.constant = 0
            }
            
            if cashoutamount > 0 {
                let phonenew =  self.getNumberFormat("\(promotion.UserContactData!.CashOutLimit!)").replacingOccurrences(of: ".00", with: "")
                self.cashOutLbl.text = "Maximum Cash Out Amount".localized + " : \(phonenew)" + " MMK"
                self.cashOutLblHeightConstraint.constant = 20
            } else{
                self.cashOutLblHeightConstraint.constant = 0
            }
        }
        self.callBtn.setImage(UIImage.init(named: "call.png"), for: UIControl.State.normal)
        self.distanceLblKm.text = "(0 Km)"
        if let shopDistance = promotion.distanceInKm {
            self.distanceLblKm.text = String(format: "(%.2f Km)", shopDistance).replacingOccurrences(of: ".00", with: "")
        }
        
        self.durationLbl.text = self.durationFromLatLong(lat: promotion.UserContactData?.LocationNewData?.Latitude, long:promotion.UserContactData?.LocationNewData?.Longitude)
    }
    
}
