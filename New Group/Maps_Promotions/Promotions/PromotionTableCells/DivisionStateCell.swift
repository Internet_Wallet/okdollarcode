//
//  DivisionStateCell.swift
//  OK
//
//  Created by Uma Rajendran on 12/4/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class DivisionStateCell: UITableViewCell {

    @IBOutlet weak var divisionStateNamelbl: UILabel!
    @IBOutlet weak var townshipCountLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        divisionStateNamelbl.font = UIFont.init(name: appFont, size: 18)
        townshipCountLbl.font = UIFont.init(name: appFont, size: 13)
        townshipCountLbl.layer.cornerRadius = 15
        townshipCountLbl.layer.masksToBounds = true
        townshipCountLbl.layer.borderWidth = 1.0
        townshipCountLbl.layer.borderColor = UIColor.white.cgColor 
        townshipCountLbl.backgroundColor = UIColor.red
        townshipCountLbl.textColor = UIColor.white
        self.townshipCountLbl.center.x += 30
        self.townshipCountLbl.isHidden = true
        UIView.animate(withDuration: 0.5, delay: 0.5,
                       usingSpringWithDamping: 0.3,
                       initialSpringVelocity: 0.5,
                       options: [], animations: {
                        self.townshipCountLbl.center.x = screenArea.size.width - 30
        }, completion:{ _ in })

    }
    
    func wrapData(title: String, count: Int, isFromPayto: Bool = false) {
        if isFromPayto {
            self.divisionStateNamelbl.text = title
            self.townshipCountLbl.text = ""
        } else {
            self.divisionStateNamelbl.text = title
            self.townshipCountLbl.text = String(count)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
