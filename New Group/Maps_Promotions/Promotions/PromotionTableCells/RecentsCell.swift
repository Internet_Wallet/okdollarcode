//
//  RecentsCell.swift
//  OK
//
//  Created by Uma Rajendran on 11/25/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class RecentsCell: UITableViewCell {
    
    @IBOutlet weak var recentsImgV: UIImageView!
    @IBOutlet weak var shopName: MarqueeLabel!{
        didSet{
            shopName.text = shopName.text?.localized
        }
    }
    @IBOutlet weak var merchantName: MarqueeLabel!{
        didSet{
            merchantName.text = merchantName.text?.localized
        }
    }
    @IBOutlet weak var shopDistance: UILabel!{
        didSet{
            shopDistance.text = shopDistance.text?.localized
        }
    }
    //    @IBOutlet weak var shopName: MarqueeLabel!
    //    @IBOutlet weak var merchantName: MarqueeLabel!
    //    @IBOutlet weak var shopDistance: UILabel!
    @IBOutlet weak var constraintWidthImage: NSLayoutConstraint!
    @IBOutlet weak var constraintWidthImgFav: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUpMarqueeLabel(label: self.shopName)
        setUpMarqueeLabel(label: self.merchantName)
        shopName.font = UIFont.init(name: appFont, size: 16)
        merchantName.font = UIFont.init(name: appFont, size: 14)
        shopDistance.font = UIFont.init(name: appFont, size: 12)
        shopName.textColor = UIColor.black
        merchantName.textColor = UIColor.darkGray
        shopDistance.textColor = UIColor.darkGray
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setUpMarqueeLabel(label: MarqueeLabel) {
        label.tag = 501
        label.type = .continuous
        label.speed = .duration(20)
        label.fadeLength = 10.0
        label.trailingBuffer = 30.0
        label.font = UIFont.init(name: appFont, size: 16)
        //        label.isUserInteractionEnabled = true
        //        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(pauseTap))
        //        tapRecognizer.numberOfTapsRequired = 1
        //        tapRecognizer.numberOfTouchesRequired = 1
        //        label.addGestureRecognizer(tapRecognizer)
    }
    
    @objc func pauseTap(_ recognizer: UIGestureRecognizer) {
        let continuousLabel2 = recognizer.view as! MarqueeLabel
        if recognizer.state == .ended {
            continuousLabel2.isPaused ? continuousLabel2.unpauseLabel() : continuousLabel2.pauseLabel()
        }
    }
}
