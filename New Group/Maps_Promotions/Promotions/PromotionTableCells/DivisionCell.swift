//
//  DivisionCell.swift
//  Promotions
//
//  Created by Uma Rajendran on 10/20/17.
//  Copyright © 2017 Uma Rajendran. All rights reserved.
//

import UIKit

class DivisionCell: UITableViewCell {

    @IBOutlet weak var divisionNameLbl: UILabel!
    @IBOutlet weak var expandImgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        divisionNameLbl.font = UIFont.init(name: appFont, size: 15)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func wrapData(text: String,img: UIImage) {
        self.divisionNameLbl.text = text
        self.expandImgView.image = img
    }
    
}
