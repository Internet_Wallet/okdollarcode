//
//  SortCell.swift
//  OK
//
//  Created by Uma Rajendran on 2/9/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class SortCell: UITableViewCell {

    @IBOutlet weak var contentLbl: UILabel!
    @IBOutlet weak var imgV: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        contentLbl.font = UIFont.init(name: appFont, size: 15)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func wrap_Data(title: String, imgName: String) {
        self.contentLbl.text = title
        imgV.image = (imgName.count > 0) ? UIImage.init(named: imgName) : UIImage.init()
    }
    
}
