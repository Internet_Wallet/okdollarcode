//
//  TownshipCell.swift
//  Promotions
//
//  Created by Uma Rajendran on 10/20/17.
//  Copyright © 2017 Uma Rajendran. All rights reserved.
//

import UIKit

class TownshipCell: UITableViewCell {

    @IBOutlet weak var townshipNameLbl: UILabel!{
        didSet{
            townshipNameLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        townshipNameLbl.font = UIFont.init(name: appFont, size: 18.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
