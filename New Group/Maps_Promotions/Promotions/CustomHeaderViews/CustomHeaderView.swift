//
//  CustomHeaderView.swift
//  Promotions
//
//  Created by Uma Rajendran on 10/23/17.
//  Copyright © 2017 Uma Rajendran. All rights reserved.
//

import UIKit

protocol CustomHeaderDelegate: class {
    func closeActionFromCustomHeader()
}


class CustomHeaderView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    var delegate : CustomHeaderDelegate?
    @IBOutlet weak var headerScrollView: UIScrollView!
    @IBOutlet weak var headerPageControl: UIPageControl!
    @IBOutlet weak var headerCloseBtn: UIButton!
    var imageNamesArr = [String]()
    var screenArea: CGRect = UIScreen.main.bounds
    @IBOutlet weak var closeBtn: UIButton!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    class func instanceFromNib(frame: CGRect, images: Array<String>, delegate: Any) -> UIView {
        let nib = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        nib.loadViewFromNib(images: images, delegate: delegate)
        return nib
    }
    
    func loadViewFromNib(images: Array<String>, delegate: Any) {
        self.imageNamesArr = images
        if let del = delegate as? CashInOutDetailsViewController {
            self.delegate = del
            self.closeBtn.isHidden = false
        }else if let del = delegate as? PromotionDetailViewController{
            self.closeBtn.isHidden = true
            self.delegate = del
        }
//        self.delegate = delegate  // now we are not using this delegate call so hide right now
        setUpScrollView()
        headerPageControl.subviews.forEach { _ in
            headerPageControl.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    func setUpScrollView() {
        headerScrollView.isPagingEnabled = true
        headerScrollView.contentSize = CGSize(width: screenArea.width * CGFloat(imageNamesArr.count), height: 260)
        headerScrollView.showsHorizontalScrollIndicator = false
        for index  in 0 ... imageNamesArr.count - 1 {
            let imageView = UIImageView(frame: CGRect(x: CGFloat(index) * screenArea.width, y: 0, width: screenArea.width, height: 400))
            imageView.contentMode = .scaleAspectFit // OR .scaleAspectFill
            imageView.image = UIImage.init(named: imageNamesArr[index])
            headerScrollView.addSubview(imageView)
        }
        self.headerScrollView.delegate = self as UIScrollViewDelegate
        self.headerPageControl.currentPage = 0
    }
    
    
    @IBAction func headerCloseAction(_ sender: Any) {
        self.delegate?.closeActionFromCustomHeader()
    }
    
}

extension CustomHeaderView : UIScrollViewDelegate {
    //MARK: UIScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        println_debug("headerView :::: scrollViewDidScroll")
        let scrollOffset = -scrollView.contentOffset.y;
        let yPos = scrollOffset - self.bounds.size.height;
        self.frame = CGRect(x:0, y:yPos, width:self.frame.width, height:self.frame.height)
        let alpha = 2.0 - (-yPos / self.frame.height)
        self.alpha = alpha
        
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        // Test the offset and calculate the current page after scrolling ends
        println_debug("headerView :::: scrollViewDidEndDecelerating")
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        // Change the indicator
        self.headerPageControl.currentPage = Int(currentPage);
        self.headerPageControl.isHighlighted = true
        
    }
}
