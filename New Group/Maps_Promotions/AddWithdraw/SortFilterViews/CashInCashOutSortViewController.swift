//
//  SortViewController.swift
//  Promotions
//
//  Created by Uma Rajendran on 10/28/17.
//  Copyright © 2017 Uma Rajendran. All rights reserved.
//

import UIKit

protocol CashInCashOutSortViewDelegate: class {
    func didSelectSortOption(sortedList: [Any], selectedSortOption: Int)
}

class CashInCashOutSortViewController: UIViewController {

    @IBOutlet weak var headerLabel: UILabel!
        {
        didSet
        {
            headerLabel.text = headerLabel.text?.localized
        }
    }
    @IBOutlet weak var sortTable: UITableView!
    @IBOutlet weak var resigningView: UIView!
    
    let sortList = ["Default".localized,"Amount High to Low".localized,"Amount Low to High".localized,"Name A to Z".localized,"Name Z to A".localized,"Fees High to Low".localized,"Fees Low to High".localized]
    weak var delegate: CashInCashOutSortViewDelegate?
    var previousSelectedSortOption: Int?
    var dataList: [Any]?
    var currentCashType: CashType?
    var currentCashInOutType: CashInOutType?
    
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadUI()
        loadInitialize()
        updateLocalizations()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sortTable.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func loadUI() {
        headerHeightConstraint.constant = kHeaderHeight
        self.view.backgroundColor = kGradientGreyColor
        resigningView.backgroundColor = UIColor.clear
    }
    
    func loadInitialize() {
        sortTable.register(UINib(nibName: "SortCell", bundle: nil), forCellReuseIdentifier: "sortcellidentifier")
        sortTable.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    func updateLocalizations() {
        headerLabel.font = UIFont.init(name: appFont, size: 17)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if touch.view == self.resigningView {
                resignTheSortView()
            }
        }
        super.touchesBegan(touches, with: event)
    }

    func resignTheSortView() {
        if let presentingView = self.presentingViewController {
            presentingView.dismiss(animated: true, completion: nil)
        }
    }
    
}

extension CashInCashOutSortViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sortCell = tableView.dequeueReusableCell(withIdentifier: "sortcellidentifier", for: indexPath) as! SortCell
        var imageName = ""
        if previousSelectedSortOption == indexPath.row {
            // show the image
            imageName = "success"
        }
        sortCell.wrap_Data(title: sortList[indexPath.row], imgName: imageName)
        return sortCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            println_debug("default case falling")
            self.delegate?.didSelectSortOption(sortedList: dataList!, selectedSortOption:indexPath.row)

        case 1:
            println_debug("Amount High to Low")
            if let list = dataList as? [CashInOutModel] {
                dataList = list.sorted(by: {($0.settings?.setting_MaxAmount!)! > ($1.settings?.setting_MaxAmount!)!})
                self.delegate?.didSelectSortOption(sortedList: dataList!, selectedSortOption:indexPath.row)
            }
        case 2:
            println_debug("Amount Low to High")
            if let list = dataList as? [CashInOutModel] {
                dataList = list.sorted(by: {($0.settings?.setting_MaxAmount!)! < ($1.settings?.setting_MaxAmount!)!})
                self.delegate?.didSelectSortOption(sortedList: dataList!, selectedSortOption:indexPath.row)
            }

        case 3:
            
            println_debug("Name A to Z")
            if currentCashInOutType == .cashin_alllist || currentCashInOutType == .cashout_alllist {
                if let list = dataList as? [CashInOutModel] {
                    dataList = list.sorted(by: {$0.cash_UIDisplayName! < $1.cash_UIDisplayName!})
                    self.delegate?.didSelectSortOption(sortedList: dataList!, selectedSortOption:indexPath.row)
                }
               
            }else if currentCashInOutType == .cashin_allbanks || currentCashInOutType == .cashout_allbanks  {
                if let list = dataList as? [CashInOutModel] {
                    dataList = list.sorted(by: {$0.cash_BusinessName! < $1.cash_BusinessName!})
                    self.delegate?.didSelectSortOption(sortedList: dataList!, selectedSortOption:indexPath.row)
                }

            }else if currentCashInOutType == .cashin_allagents || currentCashInOutType == .cashout_allagents {
                if let list = dataList as? [CashInOutModel] {
                    dataList = list.sorted(by: {$0.cash_ContactPersonName! < $1.cash_ContactPersonName!})
                    self.delegate?.didSelectSortOption(sortedList: dataList!, selectedSortOption:indexPath.row)
                }

            }else if currentCashInOutType == .cashin_alloffices || currentCashInOutType == .cashout_alloffices {
                if let list = dataList as? [CashInOutModel] {
                    dataList = list.sorted(by: {$0.cash_BusinessName! < $1.cash_BusinessName!})
                    self.delegate?.didSelectSortOption(sortedList: dataList!, selectedSortOption:indexPath.row)
                }

            }else if currentCashInOutType == .cashin_allmyanmarpost || currentCashInOutType == .cashout_allmyanmarpost  {
                 // it will come soon
            }

           
        case 4:
            println_debug("Name Z to A")
            if currentCashInOutType == .cashin_alllist || currentCashInOutType == .cashout_alllist {
                if let list = dataList as? [CashInOutModel] {
                    dataList = list.sorted(by: {$0.cash_UIDisplayName! > $1.cash_UIDisplayName!})
                    self.delegate?.didSelectSortOption(sortedList: dataList!, selectedSortOption:indexPath.row)
                }
            }else if currentCashInOutType == .cashin_allbanks || currentCashInOutType == .cashout_allbanks  {
                if let list = dataList as? [CashInOutModel] {
                    dataList = list.sorted(by: {$0.cash_BusinessName! > $1.cash_BusinessName!})
                    self.delegate?.didSelectSortOption(sortedList: dataList!, selectedSortOption:indexPath.row)
                }
            }else if currentCashInOutType == .cashin_allagents || currentCashInOutType == .cashout_allagents {
                if let list = dataList as? [CashInOutModel] {
                    dataList = list.sorted(by: {$0.cash_ContactPersonName! > $1.cash_ContactPersonName!})
                    self.delegate?.didSelectSortOption(sortedList: dataList!, selectedSortOption:indexPath.row)
                }
            }else if currentCashInOutType == .cashin_alloffices || currentCashInOutType == .cashout_alloffices {
                if let list = dataList as? [CashInOutModel] {
                    dataList = list.sorted(by: {$0.cash_BusinessName! > $1.cash_BusinessName!})
                    self.delegate?.didSelectSortOption(sortedList: dataList!, selectedSortOption:indexPath.row)
                }
            }else if currentCashInOutType == .cashin_allmyanmarpost || currentCashInOutType == .cashout_allmyanmarpost  {
                // it will come soon
            }
           
        case 5:
            println_debug("Fees High to Low")
            if let list = dataList as? [CashInOutModel] {
                dataList = list.sorted(by: { $0.cash_MinimumFee! > $1.cash_MinimumFee!})
                self.delegate?.didSelectSortOption(sortedList: dataList!, selectedSortOption:indexPath.row)
            }

        case 6:
            println_debug("Fees Low to High")
            if let list = dataList as? [CashInOutModel] {
                dataList = list.sorted(by: { $0.cash_MinimumFee! < $1.cash_MinimumFee!})
                self.delegate?.didSelectSortOption(sortedList: dataList!, selectedSortOption:indexPath.row)
            }
        default:
            break
        }
        resignTheSortView()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

