//
//  FilterViewController.swift
//  Promotions
//
//  Created by Uma Rajendran on 10/28/17.
//  Copyright © 2017 Uma Rajendran. All rights reserved.
//

import UIKit

protocol CashInCashOutFilterViewDelegate: class  {
    func didSelectFilterOption(filteredList: [Any], isDefaultFilter: Bool)
}

class CashInCashOutFilterViewController: UIViewController {

    @IBOutlet weak var headerLabel: UILabel!
    {
        didSet
        {
            headerLabel.text = headerLabel.text?.localized
        }
    }
    @IBOutlet weak var filterTable: UITableView!
    @IBOutlet weak var resigningView: UIView!
    weak var delegate: CashInCashOutFilterViewDelegate?
    var isExpandCategories: Bool = true
    var previousSelectedFilterData: Any?
    var filterDataList: [CashInOutModel]?
    var currentCashType: CashType?
    var currentCashInOutType: CashInOutType?
    var filterShowList = [String]()
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadUI()
        loadInitialize()
        updateLocalizations()
    }
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
            isExpandCategories = true
            loadFilterShowList()
            loadTableView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadUI() {
        headerHeightConstraint.constant = kHeaderHeight
        self.view.backgroundColor = kGradientGreyColor
        resigningView.backgroundColor = UIColor.clear
    }
    
    func loadInitialize() {

    }
    
    func updateLocalizations() {
        headerLabel.font = UIFont.init(name: appFont, size: 17)
    }
    
    func loadFilterShowList() {
        if currentCashInOutType == .cashin_alllist || currentCashInOutType == .cashout_alllist {
            filterShowList = ["OK$ Office".localized, "OK$ Agents".localized, "OK$ Service Shop".localized]
        }else if currentCashInOutType == .cashin_allbanks || currentCashInOutType == .cashout_allbanks  {
            filterShowList = ["OK$ Service Shop".localized]

        }else if currentCashInOutType == .cashin_allagents || currentCashInOutType == .cashout_allagents {
            filterShowList = ["OK$ Agents".localized]

        }else if currentCashInOutType == .cashin_alloffices || currentCashInOutType == .cashout_alloffices {
            filterShowList = ["OK$ Office".localized]

        }else if currentCashInOutType == .cashin_allmyanmarpost || currentCashInOutType == .cashout_allmyanmarpost  {
            // it will come soon
        }
    }
    
    func loadTableView() {
//        filterTable.register(UITableViewCell.self, forCellReuseIdentifier: "DefaultCell")
        filterTable.register(UINib(nibName: "SortCell", bundle: nil), forCellReuseIdentifier: "sortcellidentifier")
        filterTable.register(UINib(nibName: "DivisionCell", bundle: nil), forCellReuseIdentifier: "divisioncellidentifier")
        filterTable.tableFooterView = UIView(frame: CGRect.zero)
        filterTable.reloadData()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if touch.view == self.resigningView {
              resignTheFilterView()
            }
         
        }
        super.touchesBegan(touches, with: event)
    }
    
    func resignTheFilterView() {
        if let presentingView = self.presentingViewController {
            presentingView.dismiss(animated: true, completion: nil)
        }
    }

}

extension CashInCashOutFilterViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowCount = 1
        if section == 1 && isExpandCategories{
            rowCount = filterShowList.count + 1
        }
      
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 0) {

            let defaultCell = tableView.dequeueReusableCell(withIdentifier: "sortcellidentifier", for: indexPath) as! SortCell
            defaultCell.wrap_Data(title: "Default".localized, imgName: "")
            return defaultCell
        }else {
            if indexPath.row == 0 {
                let categoriesHeaderCell = tableView.dequeueReusableCell(withIdentifier: "divisioncellidentifier", for: indexPath) as! DivisionCell  // just reusing the division cell design here
                categoriesHeaderCell.expandImgView.isHidden =  false
                let image = (isExpandCategories == true) ? UIImage.init(named: "up_arrow.png") : UIImage.init(named: "down_arrow.png")
                categoriesHeaderCell.wrapData(text: "Categories".localized, img: image!)
                return categoriesHeaderCell
            }else {
 
                let categoriesListCell = tableView.dequeueReusableCell(withIdentifier: "sortcellidentifier", for: indexPath) as! SortCell
                let tab   = "    "
                let name  = "\(tab) \(filterShowList[indexPath.row - 1])"
                categoriesListCell.wrap_Data(title: name, imgName: "")
                return categoriesListCell
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            self.delegate?.didSelectFilterOption(filteredList: filterDataList!, isDefaultFilter: true)
            resignTheFilterView()
        }else if (indexPath.section == 1 && indexPath.row == 0) {
            isExpandCategories = !isExpandCategories
            filterTable.reloadSections(NSIndexSet(index: indexPath.section) as IndexSet, with: .fade)
        }else {
             var filteredArray = [CashInOutModel]()
             let selectedFilterVal = filterShowList[indexPath.row - 1]
                if selectedFilterVal == "OK$ Office".localized {
                    filteredArray = (filterDataList?.filter { ($0.cash_CashInCashOutType ?? "").range(of: "okoffice", options: [.diacriticInsensitive, .caseInsensitive]) != nil })!
                }else if selectedFilterVal == "OK$ Agents".localized  {
                    filteredArray = (filterDataList?.filter { ($0.cash_CashInCashOutType ?? "").range(of: "okagent", options: [.diacriticInsensitive, .caseInsensitive]) != nil })!
                }else if selectedFilterVal == "OK$ Service Shop" {
                    filteredArray = (filterDataList?.filter { ($0.cash_CashInCashOutType ?? "").range(of: "bank", options: [.diacriticInsensitive, .caseInsensitive]) != nil })!
                }
            self.delegate?.didSelectFilterOption(filteredList: filteredArray ,isDefaultFilter: false)
          
            
            resignTheFilterView()
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}


