//
//  OK$ServicesViewController.swift
//  OK
//
//  Created by SHUBH on 6/30/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Rabbit_Swift
import IQKeyboardManagerSwift

class OK_ServicesViewController: OKBaseController ,MainNearByLocationViewDelegate,MainwheretoLocationViewDelegate,MaincurrentLocationViewDelegate,MainLocationViewDelegateRegistration {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerLabel: MarqueeLabel!
        {
        didSet
        {
            //headerLabel.text = headerLabel.text?.localized
            headerLabel.text = "Location".localized
            headerLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }

    //prabu
    let defaults = UserDefaults.standard
    var checkurl:String = ""

    @IBOutlet weak var TopLocationBtn: UIButton!
        {
        didSet
        {
            self.TopLocationBtn.layer.masksToBounds = true
            self.TopLocationBtn.layer.borderWidth = 1.0
            self.TopLocationBtn.layer.borderColor = UIColor.white.cgColor
            self.TopLocationBtn.layer.cornerRadius = 20.0
            
        }
    }
    
    @IBOutlet weak var mapView: UIView! {
        
        didSet {
            
            self.mapView.clipsToBounds = true
            self.mapView.layer.borderWidth = 0.2
            self.mapView.layer.borderColor = UIColor.lightGray.cgColor
            self.mapView.layer.cornerRadius = 20.0
        }
    }
    
    @IBOutlet weak var filterLabel: UILabel! {
        didSet {
            self.filterLabel.text = self.filterLabel.text?.localized
            self.filterLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var mapLabel: UILabel! {
        didSet {
            self.mapLabel.text = self.mapLabel.text?.localized
            self.mapLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var topSearchView: UIView!
    @IBOutlet weak var topSearchBar: UISearchBar!
    @IBOutlet weak var searchViewBackBtn: UIButton!
    @IBOutlet weak var headerSearchBtn: UIButton!
    var menusortedindex : Int = 0

    
    var mapUiType: UIType?
    var cashInCashOutSubView: CashInCashOutSubViewController?
    
    var selectedCashType: CashType?
    var selectedCashInOutType: CashInOutType?
    
    var isHeaderSearchOpened: Bool = false
    
    @IBOutlet weak var headerSearchWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerSearchHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var switchMapListBtn: UIButton!
    
    
    @IBOutlet weak var currentLocationView      : UIView!
    @IBOutlet weak var locationNameLbl          : UILabel!
    @IBOutlet weak var locationContentLbl       : UILabel!
        {
        didSet
        {
            locationContentLbl.text = locationContentLbl.text?.localized
            locationContentLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }

    var selectedSortItem    : Int = 0
    var selectedFilterData  : Any?
    
    let sortView        = NearBySortViewController()
    let filterView      = NearByFilterViewController()
    
    let margin: CGFloat = 2
    var cellsPerRow: Int = 0
    
    var selectedLocationType: Location?
    let nearbydistance = kNearByDistance
    
    var selectedLocationDetail      : LocationDetail?
    var selectedTownshipDetail      : TownShipDetail?
    
    var cashInListBackUPByCurLoc    = [NearByServicesNewModel]()
    var cashInListRecentFromServer  = [NearByServicesNewModel]()
    var arrEmptyList                   = [NearByServicesNewModel]()

    var selectedLocationDetailStr         : String?
    var selectedTownshipDetailStr         : String?
    
    var baseViewHeight: CGFloat {
        return screenArea.size.height - 190
    }
    
    //Manage Table View
    @IBOutlet weak var listTableView: UITableView!
    var nearByServicesList = [NearByServicesNewModel]()
    var nearByServicesListBackup = [NearByServicesNewModel]()

    @IBOutlet weak var infoLabel: UILabel!{
        didSet {
            infoLabel.text = "No Record Found".localized
            infoLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }

    //Manage TYpe
    var intType : Int = 7
    
    //MARK: View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        
        
        if let searchTextField = topSearchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                //(appDel.currentLanguage == "uni") ? UIFont.systemFont(ofSize: 15) : myFont
                //searchTextField.font = (appDel.currentLanguage == "uni") ? UIFont.systemFont(ofSize: 15) : myFont
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                searchTextField.keyboardType = .asciiCapable
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
            }
        }
        let view = self.topSearchBar.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                subView.tintColor = ConstantsColor.navigationHeaderTransaction
            }
        }
        
        self.navigationController?.isNavigationBarHidden = true
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // print("typenew11-------\(self.intType)")
        self.showHideInfoLabel(isShow: false, content: "")

        // Do any additional setup after loading the view.
        geoLocManager.startUpdateLocation()
        
        //prabu
        defaults.set(nil, forKey: "NBSsortBy")
        defaults.set(nil, forKey: "NBSfilterBy")
        defaults.set(nil, forKey: "NBScategoryBy")
        defaults.set(nil, forKey: "NBSRecentLocationAddress1")
        defaults.set(nil, forKey: "NBSRecentLocationAddress2")
        defaults.set(nil, forKey: "NBSCurrentLocation")


        loadInitialize()
        loadUI()
        
        //Search Setup
        searchBarSettings()
        hideSearchWithoutAnimation()
        
        //Manage Keyboard
        IQKeyboardManager.sharedManager().enable            = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        
        //Location Manage
        didSelectLocationByCurrentLocation()
    }

    //MARK:- Initial Setup
    
    func loadInitialize() {
        let nib = UINib.init(nibName: "MapListCell", bundle: nil)
        listTableView.register(nib, forCellReuseIdentifier: "maplistcellidentifier")
        listTableView.tableFooterView = UIView(frame: CGRect.zero)
        listTableView.rowHeight = UITableView.automaticDimension
        listTableView.estimatedRowHeight = 250
        //listTableView.reloadData()
    }
    
    func updateCurrentLocationToUI() {
        
        headerLabel.text = "\(userDef.value(forKey: "lastLoginKey") as? String ?? "")     "

        //headerLabel.text =  "          \(userDef.value(forKey: "lastLoginKey") as! String)          " ?? ""
        
    }
    
    func loadUI() {
        //headerHeightConstraint.constant = kHeaderHeight
       // headerSearchHeightConstraint.constant = kHeaderHeight
        headerView.backgroundColor = kYellowColor
        
    }
    

    
    @IBAction func topheaderLocationAction(_ sender: UIButton) {
       // print("Action method called-----")
        let storyBoardName = UIStoryboard(name: "Promotion", bundle: nil)
        guard let locationSelectionView = storyBoardName.instantiateViewController(withIdentifier: "SelecteMainLocationView_ID") as? SelectMainLocationViewController else { return }
        locationSelectionView.delegateRegistration = self
        locationSelectionView.delegatenearby = self
        locationSelectionView.delegatewhereto = self
    self.navigationController?.pushViewController(locationSelectionView, animated: true)
        
    }
    
    @IBAction func filterButtonAction(_ sender: UIButton) {
        
        
        if nearByServicesList.count == 0  {
            self.showToast(message: "No Results Found".localized, align: .top)
            return
        }
        guard let sortViewController = UIStoryboard(name: "NBSMain", bundle: nil).instantiateViewController(withIdentifier: "FilterNearByServiceViewController") as? FilterNearByServiceViewController else { return }
        sortViewController.maincurrentlocationdelegate = self
        sortViewController.categoryDelegate = self
        sortViewController.applybuttondelegate = self
        sortViewController.findviewStr = "NearByServices"
        
        if (defaults.value(forKey: "NBSsortBy") != nil) {
            
            sortViewController.previousSelectedSortOptionnew = defaults.value(forKey: "NBSsortBy") as? String
            
        }else {
            sortViewController.previousSelectedSortOptionnew = nil
        }
        
        sortViewController.checkMap = false
        sortViewController.previousSelectedSortOption = selectedSortItem
        sortViewController.previousSelectedFilterData = self.selectedFilterData
        sortViewController.dataList = cashInListBackUPByCurLoc // this is used to sort all the promotions
        sortViewController.filterDataList = cashInListBackUPByCurLoc // this is used to sort all the promotions
        sortViewController.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(sortViewController, animated: true, completion: nil)
       
    }


    //MARK: Search Manage Delegate
    func searchBarSettings() {
        topSearchBar.tintColor = UIColor.darkGray
        topSearchBar.barTintColor = UIColor.white
        topSearchBar.searchBarStyle = UISearchBar.Style.minimal
        
        let searchTextField: UITextField? = topSearchBar.value(forKey: "searchField") as? UITextField
        if searchTextField!.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
            let attributeDict = [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
            searchTextField!.attributedPlaceholder = NSAttributedString(string: "Search".localized, attributes: attributeDict)
            searchTextField!.backgroundColor = UIColor.white
            searchTextField!.borderStyle = UITextField.BorderStyle.none
            searchTextField!.layer.borderColor = UIColor.clear.cgColor
                     searchTextField!.layer.borderWidth = 0.0
            searchTextField!.textColor = UIColor.black
            searchTextField!.autocorrectionType = UITextAutocorrectionType.no
            let myFont = UIFont.init(name: appFont, size: 15)
            if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                searchTextField!.font = myFont
            }else{
                //
            }
            searchTextField!.placeholder = "Search".localized
            searchTextField!.backgroundColor  = .white
            searchTextField!.leftViewMode = UITextField.ViewMode.never
        }
        
    }
    
    func resignSearchBarKeypad() {
        topSearchBar.resignFirstResponder()
    }
    
    func hideSearchWithoutAnimation() {
        isHeaderSearchOpened = false
        topSearchBar.text = ""
        topSearchBar.resignFirstResponder()
        topSearchView.isHidden = true
    }
    
   
    
    @IBAction func searchBtnAction(_ sender: Any) {
       
        self.topSearchView.frame = CGRect(x: screenArea.size.width - 30, y: 0, width: 0, height: 50)
        showCashSearch()

    }
    
    func showCashSearch() {
        isHeaderSearchOpened = true
        topSearchBar.text = ""
        topSearchView.layer.masksToBounds = true
        topSearchView.layer.cornerRadius = 20
        topSearchView.alpha = 0.0
        UIView.animate(withDuration: 0.3, animations: {
          var viewFrame: CGRect = self.topSearchView.frame
            viewFrame.origin.x = 0
            viewFrame.size.width = screenArea.size.width
            self.topSearchView.frame = viewFrame
            self.topSearchView.alpha = 1.0
            self.topSearchView.isHidden = false
            self.topSearchBar.becomeFirstResponder()
            
        }) { _ in
            self.topSearchView.layer.cornerRadius = 0
        }
    }
    
    func hideCashSearch() {
        isHeaderSearchOpened = false
        topSearchBar.text = ""
        topSearchBar.resignFirstResponder()
        topSearchView.layer.cornerRadius = 20
        topSearchView.alpha = 1.0
        UIView.animate(withDuration: 0.3, animations: {
            var viewFrame: CGRect = self.topSearchView.frame
            viewFrame.origin.x = screenArea.size.width - 30
            viewFrame.origin.y = self.topSearchView.frame.origin.y + 15
            viewFrame.size.width = 0
            viewFrame.size.height = self.topSearchView.frame.size.height / 2
            self.topSearchView.frame = viewFrame
            
        }) { _ in
            self.topSearchView.alpha = 0.0
        }
    }
    
    @IBAction func searchViewBackAction(_ sender: Any) {
        //Hide search view
        hideCashSearch()
        reloadListViewAfterSwitch()
    }
    
    
    
    //MARK: - Location Manager Delegate
    
    func didSelectLocationByCurrentLocation() {
        updateCurrentLocationToUI()
        
        self.checkurl = "CurrentLocation"

        guard let lat = geoLocManager.currentLatitude, let long = geoLocManager.currentLongitude else {
            // throw the error to the user
            return
        }
        
        self.getOfficeAPI(lat: lat , long: long )
        
    }
    
    func didSelectNearByLocation() {
        
        self.checkurl = "NearBy"
        headerLabel.text = "Near By".localized
        
        guard let lat = geoLocManager.currentLatitude, let long = geoLocManager.currentLongitude else {
            // throw the error to the user
            return
        }
        
        self.getOfficeAPI(lat: lat , long: long )
    }
    
    
    
    func didSelectwheretoLocation(streetName:String)
    {
        
        headerLabel.text = "     " + streetName + "     "
        
        geoLocManager.getLatLongByName(cityname: streetName) { (isSuccess, lat, long) in
            
            guard isSuccess, let latti = lat, let longi = long  else {
                //self.showErrorAlert(errMessage: "Try Again".localized)
                return
            }
            
            self.getOfficeAPI(lat:latti , long:longi)
            
        }
        
        
    }
    
    func didSelectLocationByTownshipRegistration(location: LocationDetail, township: TownShipDetailForAddress) {
        
        self.checkurl = "Township"
        
        if appDel.getSelectedLanguage() == "my" {
            headerLabel.text = location.stateOrDivitionNameMy  + " , " + township.cityNameMY + "    "
        } else if appDel.getSelectedLanguage() == "en"  {
            headerLabel.text = location.stateOrDivitionNameEn + " , " + township.cityNameEN + "    "
        }else{
            headerLabel.text = location.stateOrDivitionNameUni + " , " + township.cityNameUni + "    "
        }
        
        updateOtherLocationToUINew(location: location.stateOrDivitionNameEn, township: township.townShipNameEN)
        self.getOfficeAPI(lat: geoLocManager.currentLatitude , long: geoLocManager.currentLongitude)
        
    }
    
    
    func didSelectLocationByTownship(location: LocationDetail, township: TownShipDetail) {
        
        self.checkurl = "Township"

        updateOtherLocationToUI(location: location, township: township)
        self.getOfficeAPI(lat: geoLocManager.currentLatitude , long: geoLocManager.currentLongitude)
    }
    
    func updateOtherLocationToUI(location: LocationDetail, township: TownShipDetail) {
        selectedLocationDetail = location
        selectedTownshipDetail = township
        DispatchQueue.main.async {
            
            if appDel.currentLanguage == "my" {
                self.headerLabel.text = location.stateOrDivitionNameMy + " , " + township.townShipNameMY + "    "
                self.updateOtherLocationToUINew(location: location.stateOrDivitionNameMy, township: township.townShipNameMY)
                
            }
            else
            {
                self.headerLabel.text = location.stateOrDivitionNameEn + " , " + township.townShipNameEN + "    "
                self.updateOtherLocationToUINew(location: location.stateOrDivitionNameMy, township: township.townShipNameMY)

            }
        }
    }
    
    func updateOtherLocationToUINew(location: String, township: String) {
        
        selectedLocationDetailStr = location
        selectedTownshipDetailStr = township
        
    }
    
    
    //MARK:- Get API Data
    
    func getOfficeAPI(lat: String, long: String)
    {
      //  println_debug("call ok offices api ")
        
        let url = getUrl(urlStr: "", serverType: .nearByServicesUrl)
        
        //let jsonDic = ["Amount" : 0, "CashType" : 0, "CountryCode" : "+95", "DistanceLimit" : 0, "Latitude" : lat, "Longitude": long, "PhoneNumber" : UserModel.shared.mobileNo, "UserIdNotToConsider": ""] as [String : Any]
        
        let jsonDic:[String : Any]

       // var lat = "16.81668401110357"
       // var long = "96.13187085779862"
        
        
        if checkurl ==  "NearBy" {
            
             jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 300, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":0],"types":intType, "phoneNumberNotToConsider":""] as [String : Any]
            
            
        } else if checkurl ==  "Township"{
            
            
             jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 1000, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":selectedTownshipDetailStr,"cityName":"","businessName":"","country":"","divisionName":selectedLocationDetailStr,"line1":""],"paginationParams": ["offset":0,"limit":0],"types":intType, "phoneNumberNotToConsider":""] as [String : Any]
            
        } else {
        
            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 1000, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":0],"types":[intType], "phoneNumberNotToConsider":""] as [String : Any]
            
        }
        
        //print("OK$ServiceurlRequest========\(jsonDic)")

        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: url,methodType: kMethod_Post, contentType: kContentType_Json, inputVal: jsonDic, authStr: nil)
        print("OK$ServiceurlRequest OK$Service========\(urlRequest)")
        
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
        }
        
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    //println_debug("remove progress view called inside cashin offices api main call with error")
                }
                //self.showErrorAlert(errMessage: "Try Again".localized)
                return
            }
           // println_debug("response dict for get all offices list :: \(response ?? "")")
            progressViewObj.removeProgressView()
            
            var cashArray = [NearByServicesNewModel]()
            DispatchQueue.main.async {
                cashArray = ParserAttributeClass.parseNearbyServiceJSONNew(anyValue: response as AnyObject) as [NearByServicesNewModel]
              //  println_debug("response count for ok offices :::: \(cashArray.count)")
                progressViewObj.removeProgressView()
                self.cashInListRecentFromServer = cashArray
                
                if cashArray.count > 0 {
                    self.cashInListBackUPByCurLoc.removeAll()
                    self.nearByServicesList.removeAll()
                    self.nearByServicesListBackup.removeAll()

                    //Seperate mechant =2,agent=1,office=7
                    for indexVal in 0..<cashArray.count {
                        let currentModel = cashArray[indexVal]
                        
                        //print("type-------\(self.intType)---\(currentModel.UserContactData!.type)")
                        
                        if self.intType == currentModel.UserContactData!.type! {
                            //print("innertype-------\(currentModel.UserContactData!.type)")
                            self.nearByServicesList.append(currentModel)
                            self.nearByServicesListBackup.append(currentModel)
                            self.cashInListBackUPByCurLoc.append(currentModel)
                            
                        }
                    }
                  
                  if self.nearByServicesList.count > 0
                  {
                    self.showHideInfoLabel(isShow: false, content: "")

                    self.listTableView.reloadData()
                  }
                  else
                  {
                    self.reloadListEmptyViewAfterSwitch()

                    self.showErrorAlert(errMessage: "No Record Found".localized)
                    self.showHideInfoLabel(isShow: true, content: "No Record Found".localized)
                
                    }
                }
                else
                {
                    
                    self.cashInListBackUPByCurLoc.removeAll()
                    self.nearByServicesList.removeAll()
                    self.nearByServicesListBackup.removeAll()
                    
                    self.reloadListEmptyViewAfterSwitch()


                    self.showErrorAlert(errMessage: "No Record Found".localized)
                    self.showHideInfoLabel(isShow: true, content: "No Record Found".localized)
                }
            }
        })
    }
    
    //MARK: Custom Action
   
    @IBAction func mainViewBackAction(_ sender: Any) {
        if let presentingView = self.presentingViewController {
            presentingView.dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func mapBtnAction(_ sender: Any) {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "NBSMain", bundle: nil)
        let objMapVC = storyboard.instantiateViewController(withIdentifier: "NBSMapVC") as! NBSMapVC
        
        objMapVC.arrMainList = nearByServicesList
        objMapVC.intType = intType
        objMapVC.screenType = headerLabel.text ?? "Service"
        self.navigationController?.pushViewController(objMapVC, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension OK_ServicesViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nearByServicesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let listCell = tableView.dequeueReusableCell(withIdentifier: "maplistcellidentifier", for: indexPath) as! MapListCell
        
        let promotion = nearByServicesList[indexPath.row]
        listCell.callBtn.addTarget(self, action: #selector(callBtnAction(_:)), for: UIControl.Event.touchUpInside)
        //listCell.photoImgView.image = UIImage.init(named: "btc")
        
        listCell.photoImgView.image = nil
        if let imageStr = promotion.UserContactData?.ImageUrl, let imageUrl = URL(string: imageStr) {
            listCell.photoImgView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "btc"))
        }
        
        let agentCodenew  = getPlusWithDestinationNum((promotion.UserContactData!.PhoneNumber)!, withCountryCode: "+95")
        
        if agentCodenew.length > 15 {
             listCell.phonenumberLbl.text = "NA"
        } else {
            listCell.phonenumberLbl.text = agentCodenew
        }
        
        //listCell.phonenumberLbl.text = agentCodenew
        
        listCell.nameLbl.text = promotion.UserContactData?.BusinessName
        if(promotion.UserContactData?.BusinessName!.count == 0)
        {
            listCell.nameLbl.text = promotion.UserContactData?.FirstName
        }
  
        if let openTime = promotion.UserContactData?.OfficeHourFrom , let closeTime = promotion.UserContactData?.OfficeHourTo {
            
                if openTime.count > 0 && closeTime.count > 0 {
                    listCell.timeImgView.isHidden = false
                    listCell.timeLbl.isHidden = false
                    listCell.timeLblHeightConstraint.constant = 20
                    //listCell.timeImgView.image = UIImage.init(named: "time")
                    listCell.timeLbl.text = "\(self.getTime(timeStr: openTime)) - \(self.getTime(timeStr: closeTime))"
                }else {
                    listCell.timeImgView.isHidden = true
                    listCell.timeLbl.isHidden = true
                    listCell.timeLblHeightConstraint.constant = 0
                }
    
        }
        listCell.locationImgView.image = UIImage.init(named: "location.png")
        
        if let addressnew = promotion.UserContactData?.Address {
            if addressnew.count > 0 {
                
                listCell.locationLbl.text = "\(addressnew)"
                
            }else {
                listCell.locationLbl.text = "\(promotion.UserContactData!.Township!),\(promotion.UserContactData!.Division!)"
            }
        }
        
        
        //CashIn And CashOut
        
        if let cashinamount = promotion.UserContactData?.CashInLimit , let cashoutamount = promotion.UserContactData?.CashOutLimit {
            
            if cashinamount > 0 {
                let phone =  self.getNumberFormat("\(promotion.UserContactData!.CashInLimit!)").replacingOccurrences(of: ".00", with: "")
                listCell.cashInLbl.text = "Maximum Cash In Amount".localized + " : \(phone)" + " MMK"
                listCell.cashInLblHeightConstraint.constant = 20
            } else {
                listCell.cashInLblHeightConstraint.constant = 0
            }
            
            if cashoutamount > 0 {
                let phonenew =  self.getNumberFormat("\(promotion.UserContactData!.CashOutLimit!)").replacingOccurrences(of: ".00", with: "")
                listCell.cashOutLbl.text = "Maximum Cash Out Amount".localized + " : \(phonenew)" + " MMK"
                listCell.cashOutLblHeightConstraint.constant = 20
            } else{
                listCell.cashOutLblHeightConstraint.constant = 0
            }
        }
        
        listCell.callBtn.setImage(UIImage.init(named: "call.png"), for: UIControl.State.normal)
        
        listCell.distanceLblKm.text = "(0 Km)"
        if let shopDistance = promotion.distanceInKm {
            listCell.distanceLblKm.text = String(format: "(%.2f Km)", shopDistance).replacingOccurrences(of: ".00", with: "")
        }
        
        listCell.durationLbl.text = listCell.durationFromDistance(distance: promotion.distanceInKm) as String

        return listCell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let verticalIndicator = scrollView.subviews.last as? UIImageView
        verticalIndicator?.backgroundColor = UIColor.yellow
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let promotion = nearByServicesList[indexPath.row]
        print("Did select promotion----\(promotion)")
        let storyboard: UIStoryboard = UIStoryboard(name: "Promotion", bundle: nil)
        let protionDetailsView = storyboard.instantiateViewController(withIdentifier: "PromotionDetailView_ID") as! PromotionDetailViewController
        protionDetailsView.selectedNearByServicesNew = promotion
        protionDetailsView.strNearByPromotion = "NearByService"
        self.navigationController?.pushViewController(protionDetailsView, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight = 250
        tableView.rowHeight = UITableView.automaticDimension
        return tableView.rowHeight
    }
    
    func showHideInfoLabel(isShow: Bool, content: String) {
        DispatchQueue.main.async {
            if isShow {
                self.infoLabel.isHidden = false
                self.infoLabel.text = content
            }else {
                self.infoLabel.isHidden = true
            }
        }
    }
    
    func getTime(timeStr: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "HH:mm:ss"
        //dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        var hour = ""
        if let time = dateFormatter.date(from: timeStr) {
            dateFormatter.dateFormat = "hh:mm a"
            hour = dateFormatter.string(from: time)
        }
        return hour
    }
    
    @objc func callBtnAction(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to: self.listTableView)
        if let indexPath = self.listTableView.indexPathForRow(at: buttonPosition), nearByServicesList.count > 0 {
            let selectedpromotion = nearByServicesList[indexPath.row]
            let promotion = selectedpromotion
            if let phonenumber = promotion.UserContactData?.PhoneNumber {
                if phonenumber.count > 0 && phonenumber.count < 15 {
                    var mobNumber = getPhoneNumberByFormat(phoneNumber: phonenumber)
                    mobNumber = mobNumber.removingWhitespaces()
                    let phone = "tel://\(mobNumber)"
                    if let url = URL.init(string: phone) {
                        UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                    }
                }
            }
        }else {
            alertViewObj.wrapAlert(title: nil, body: "No Phonenumber Exists".localized, img: nil)
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                
            })
            alertViewObj.showAlert(controller: self)
        }
    }
}


extension OK_ServicesViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        didSelectSearchWithKey(key: searchBar.text!)
        
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        didSelectSearchWithKey(key: searchBar.text!)
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        
        if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
            if (text.contains("\n")){
                searchBar.resignFirstResponder()
                           return false
                       }
            let updatedText = searchText.replacingCharacters(in: textRange, with: text)
            
            
            if appDel.getSelectedLanguage() == "my" {
                 if !(updatedText == updatedText.components(separatedBy: NSCharacterSet(charactersIn: SearchEnglishZwagi).inverted).joined(separator: "")) { return false }
            } else if appDel.getSelectedLanguage() == "uni" {
                if !(updatedText == updatedText.components(separatedBy: NSCharacterSet(charactersIn: SearchEnglishUnicode).inverted).joined(separator: "")) { return false }
            }else{
                if !(updatedText == updatedText.components(separatedBy: NSCharacterSet(charactersIn: SEARCH_CHAR_SET_En).inverted).joined(separator: "")) { return false }
            }
            
            
        //    if !(updatedText == updatedText.components(separatedBy: NSCharacterSet(charactersIn: SEARCHCHARSET).inverted).joined(separator: "")) { return false }
            
            if updatedText.count > 25 {
                return false
            }
            if updatedText == " " || updatedText == "  "{
                return false
            }
            if (updatedText.contains("  ")){
                return false
            }
            
            let containsEmoji: Bool = updatedText.containsEmoji
            if (containsEmoji){
                return false
            }
            
            if updatedText != "" && text != "\n" {
                didSelectSearchWithKey(key: updatedText)
            }
        }
        return true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        print("searchBarTextDidEndEditing called-----")
                searchBar.resignFirstResponder()

    }
    
    
    func didSelectSearchWithKey(key: String) {
        
        if key.count > 0 {
            
            let filteredArray = nearByServicesListBackup.filter { ($0.UserContactData?.BusinessName)?.range(of: key, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
            
            if((filteredArray.count) > 0)
            {
                reloadTableData(arrayList: filteredArray)
            }
            else
            {
                let filteredArray = nearByServicesListBackup.filter { ($0.UserContactData?.FirstName)?.range(of: key, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
                
                reloadTableData(arrayList: filteredArray)
            }
        } else {
          //  println_debug("Text field is cleared")
            reloadListViewAfterSwitch()
        }
    }
    
    func reloadTableData(arrayList : [NearByServicesNewModel])
    {
        nearByServicesList = arrayList
        listTableView.reloadData()
    }
    
    func reloadListViewAfterSwitch()
    {
        nearByServicesList = cashInListBackUPByCurLoc
        listTableView.reloadData()
    }
    
    func reloadListEmptyViewAfterSwitch() {
        
        nearByServicesList = self.arrEmptyList
        listTableView.reloadData()
    }

}

extension OK_ServicesViewController: MainFilterApplyViewDelegate {
    
    func didSelectApplyFilter(category: SubCategoryDetail? , index:Int , str:Int){
        
       // print("Nearbyservice===\(category)---\(index)")
        
        self.menusortedindex = index
        
        if category != nil {
            
            var mainCategoryName = ""
            var subCategoryName = ""
            
            if let categ = category {
                mainCategoryName = categ.mainCategoryName
                subCategoryName = categ.subCategoryName
                
                headerLabel.text = "\(categ.mainCategoryName) , \(categ.subCategoryName)"
                
                defaults.set(categ.mainCategoryName + "," + categ.subCategoryName, forKey: "NBScategoryBy")
            }
            
            
            var arrTempList = [NearByServicesNewModel]()
            
            arrTempList = cashInListBackUPByCurLoc

            let filteredArray = arrTempList.filter { ($0.UserContactData?.BusinessName)?.range(of: mainCategoryName, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
            
            if((filteredArray.count) > 0)
            {
                var subCatArray = filteredArray.filter { ($0.UserContactData?.BusinessName)?.range(of: subCategoryName, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
                
                if((subCatArray.count) > 0)
                {
                    
                    if index == 1 {
                        
                        subCatArray = subCatArray.sorted(by: {$0.UserContactData!.BusinessName! < $1.UserContactData!.BusinessName!})
                        
                    } else if index == 2{
                        
                        subCatArray = subCatArray.sorted(by: {$1.UserContactData!.BusinessName! < $0.UserContactData!.BusinessName!})
                        
                    }
                    
                    self.mapView.isHidden = false
                    reloadTableData(arrayList : subCatArray)
                }
                else
                {
                    
                    
                    self.showErrorAlert(errMessage: "No Records Found".localized)
                    
                    self.mapView.isHidden = true
                    reloadListEmptyViewAfterSwitch()
                    
                    
                }
            }
            else
            {
                self.showErrorAlert(errMessage: "No Records Found".localized)
                
                
                self.mapView.isHidden = true
                reloadListEmptyViewAfterSwitch()
                
            }
        } else {
            
            defaults.set("Default", forKey: "NBScategoryBy")
            
            var arrTempList = [NearByServicesNewModel]()
            
            arrTempList = cashInListBackUPByCurLoc

            
            if menusortedindex > 0  {
                
                if menusortedindex == 1 {
                    
                    var name1 : String = ""
                    var name2 : String = ""
                    
                    arrTempList = arrTempList.sorted {
                        
                        if $0.UserContactData!.BusinessName != "" {
                            name1 = $0.UserContactData!.BusinessName!
                        } else {
                            name1 = $0.UserContactData!.FirstName!
                        }
                        
                        if $1.UserContactData!.BusinessName != "" {
                            name2 = $1.UserContactData!.BusinessName!
                        } else {
                            name2 = $1.UserContactData!.FirstName!
                        }
                        
                        return name1.compare(name2) == .orderedAscending
                    }
                    
                } else if menusortedindex == 2{
                    
                    var name1 : String = ""
                    var name2 : String = ""
                    
                    arrTempList = arrTempList.sorted {
                        
                        if $0.UserContactData!.BusinessName != "" {
                            name1 = $0.UserContactData!.BusinessName!
                        } else {
                            name1 = $0.UserContactData!.FirstName!
                        }
                        
                        if $1.UserContactData!.BusinessName != "" {
                            name2 = $1.UserContactData!.BusinessName!
                        } else {
                            name2 = $1.UserContactData!.FirstName!
                        }
                        return name1.compare(name2) == .orderedDescending
                    }
                    
                    /*  sortTempList = arrTempList.sorted(by: {$0.UserContactData!.BusinessName! > $1.UserContactData!.BusinessName!})
                     let promotion = sortTempList[0]
                     print("sortTempList ALL Data name called-------\(promotion.UserContactData?.BusinessName)")
                     reloadTableData(arrayList : sortTempList) */
                    
                }
                reloadTableData(arrayList : arrTempList)
                
            } else {
                self.mapView.isHidden = false
                reloadListViewAfterSwitch()
            }
            
        }
        
        
    }
    
}

extension OK_ServicesViewController: CategoriesSelectionDelegate {
    
    func didSelectLocationByCategory(category: SubCategoryDetail?) {
        
        var mainCategoryName = ""
        var subCategoryName = ""
        
        if let categ = category {
            mainCategoryName = categ.mainCategoryName
            subCategoryName = categ.subCategoryName
            
             defaults.set(categ.mainCategoryName + "," + categ.subCategoryName, forKey: "NBScategoryBy")
            self.headerLabel.text = " \(categ.mainCategoryName)  \(categ.subCategoryName)"
        }
        
        let filteredArray = cashInListBackUPByCurLoc.filter { ($0.UserContactData?.BusinessName)?.range(of: mainCategoryName, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
        
        if((filteredArray.count) > 0)
        {
            let subCatArray = cashInListBackUPByCurLoc.filter { ($0.UserContactData?.BusinessName)?.range(of: subCategoryName, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
            if((subCatArray.count) > 0)
            {
                reloadTableData(arrayList : subCatArray)
            }
            else
            {
                self.showErrorAlert(errMessage: "No Records Found".localized)
                reloadListViewAfterSwitch()
            }
        }
        else
        {
            self.showErrorAlert(errMessage: "No Records Found".localized)
            
            reloadListViewAfterSwitch()
        }
    }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
