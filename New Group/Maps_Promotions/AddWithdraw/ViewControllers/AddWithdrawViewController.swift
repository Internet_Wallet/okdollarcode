//
//  AddWithdrawViewController.swift
//  OK
//
//  Created by Uma Rajendran on 7/17/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit

class AddWithdrawViewController: UIViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    @IBOutlet var topOptionsView: UIView!
    @IBOutlet var addMoneyBtn: UIButton!
    @IBOutlet var sendMoneyBtn: UIButton!
    @IBOutlet var moneyInBtn: UIButton!
    @IBOutlet var separatorLabel: UILabel!
    @IBOutlet var baseView: UIView!
    @IBOutlet weak var constraintWidthSeparator: NSLayoutConstraint!
    @IBOutlet weak var constraintLeadingSeparator: NSLayoutConstraint!
    
    let appDel = UIApplication.shared.delegate as! AppDelegate
    var allSourceViews = [UIViewController]()
    var pageViewController: UIPageViewController?
    let hightLightColor = UIColor.black
    let normalColor = kYellowColor
    private let separatorWidth = UIScreen.main.bounds.width / 3
    var navigationStatus:Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateLocalizations()
        self.loadPageViewController()
        self.updateUI()
        self.setUpNavigation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func updateLocalizations() {
        sendMoneyBtn.setTitle("Send Money".localized, for: .normal)
        addMoneyBtn.setTitle("Add Money".localized, for: .normal)
        moneyInBtn.setTitle("OK$ Services".localized, for: .normal)
        addMoneyBtn.titleLabel?.textAlignment = .center
        sendMoneyBtn.titleLabel?.textAlignment = .center
        moneyInBtn.titleLabel?.textAlignment = .center
        addMoneyBtn.setTitleColor(UIColor.orange, for: .normal)
        sendMoneyBtn.setTitleColor(UIColor.black, for: .normal)
        moneyInBtn.setTitleColor(UIColor.black, for: .normal)
        addMoneyBtn.backgroundColor = UIColor.clear
        sendMoneyBtn.backgroundColor = UIColor.clear
        moneyInBtn.backgroundColor = UIColor.clear
        topOptionsView.backgroundColor = UIColor.white
    }
    
    func updateUI() {
        baseView.layer.shadowColor = kBackGroundGreyColor.cgColor
        baseView.layer.shadowOpacity = 1
        baseView.layer.shadowOffset = CGSize.zero
        baseView.layer.shadowRadius = 4
        self.view.setShadowToLabel(thisView: separatorLabel)  //addMoneySeparatorLbl //sendMoneySeparatorLbl //moneyInSeparatorLbl
        self.constraintWidthSeparator.constant = separatorWidth
    }
    
    func loadPageViewController() {
        let addMoneyStoryboard = UIStoryboard.init(name: "AddWithdraw", bundle: nil)
        let firstView_addMoneyVC = addMoneyStoryboard.instantiateViewController(withIdentifier: "AddWithdrawMoneyVC") as! AddWithdrawMoneyVC
        let secondView = self.storyboard?.instantiateViewController(withIdentifier: "AddWithdrawTableViewController_ID") as! AddWithdrawTableViewController
        let thirdView = self.storyboard?.instantiateViewController(withIdentifier: "AddWithdrawTableViewController_ID") as! AddWithdrawTableViewController
        
        pageViewController = UIPageViewController.init(transitionStyle: UIPageViewController.TransitionStyle.scroll, navigationOrientation: UIPageViewController.NavigationOrientation.horizontal, options: nil)
        pageViewController!.delegate = self
        pageViewController!.dataSource = self
        pageViewController!.view.frame = baseView.bounds
        pageViewController!.view.backgroundColor = UIColor.clear
        secondView.pageIndex = 0
        thirdView.pageIndex = 1
        allSourceViews = [firstView_addMoneyVC, secondView, thirdView]
        pageViewController!.setViewControllers([allSourceViews[0]], direction: .forward, animated: true, completion: { _ in })
        addChild(pageViewController!)
        baseView.addSubview(pageViewController!.view)
        pageViewController!.didMove(toParent: self)
        highLightAddMoney()
    }
    
    @objc func backButtonAction(_ sender: Any) {
        /*if let presentingView = self.presentingViewController {
            presentingView.dismiss(animated: true, completion: nil)
        }*/
        if(navigationStatus)
        {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            if let presentingView = self.presentingViewController {
                presentingView.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func addMoneyBtnAction(_ sender: Any) {
        pageViewController!.setViewControllers([allSourceViews[0]], direction: .forward, animated: false, completion: { _ in })
        highLightAddMoney()
    }
    
    @IBAction func sendMoneyBtnAction(_ sender: Any) {
        pageViewController?.setViewControllers([allSourceViews[1]], direction: .forward, animated: false, completion: { _ in })
        highLightSendMoney()
    }
    
    @IBAction func moneyInBtnAction(_ sender: Any) {
        pageViewController?.setViewControllers([allSourceViews[2]], direction: .forward, animated: false, completion: { _ in })
        highLightMoneyIn()
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index : Int = self.indexOf(viewController)
        if index == 0 {
            return nil
        }
        index -= 1
        return allSourceViews[index]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index : Int = self.indexOf(viewController)
        index += 1
        if index == 3 {
            return nil
        }
        return allSourceViews[index]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        self.setPageIndex()
    }
    
    func indexOf(_ viewController: UIViewController) -> Int {
        return (allSourceViews as NSArray).index(of: viewController)
    }
    
    func setPageIndex() {
        let currentViewController: UIViewController = pageViewController!.viewControllers![0]
        let currentIndex: Int = self.indexOf(currentViewController)
        if currentIndex == 0 {
            highLightAddMoney()
        } else if currentIndex == 1 {
            highLightSendMoney()
        } else if currentIndex == 2 {
            highLightMoneyIn()
        }
    }
    
    func highLightAddMoney() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5) {
                self.constraintLeadingSeparator.constant = 0
                self.view.layoutIfNeeded()
                self.addMoneyBtn.setTitleColor(UIColor.orange, for: .normal)
                self.sendMoneyBtn.setTitleColor(UIColor.black, for: .normal)
                self.moneyInBtn.setTitleColor(UIColor.black, for: .normal)
            }
        }
    }
    
    func highLightSendMoney() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5) {
                self.constraintLeadingSeparator.constant = self.separatorWidth * 1
                self.view.layoutIfNeeded()
                self.addMoneyBtn.setTitleColor(UIColor.black, for: .normal)
                self.sendMoneyBtn.setTitleColor(UIColor.orange, for: .normal)
                self.moneyInBtn.setTitleColor(UIColor.black, for: .normal)
            }
        }
    }
    
    func highLightMoneyIn() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5) {
                self.constraintLeadingSeparator.constant = self.separatorWidth * 2
                self.view.layoutIfNeeded()
                self.addMoneyBtn.setTitleColor(UIColor.black, for: .normal)
                self.sendMoneyBtn.setTitleColor(UIColor.black, for: .normal)
                self.moneyInBtn.setTitleColor(UIColor.orange, for: .normal)
            }
        }
    }
}

// MARK: - Additional Methods
extension AddWithdrawViewController: NavigationSetUpProtocol {
    /// It set up the navigation bar
    func setUpNavigation() {
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "tabBarBack").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        
        let ypos = (titleViewSize.height - 30 ) / 2
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        //label.text = "Add / Withdraw".localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 17) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
}
