//
//  AddWithdrawTableViewController.swift
//  OK
//
//  Created by Uma Rajendran on 7/21/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit
import CoreLocation

class AddWithdrawTableViewController: UIViewController, BioMetricLoginDelegate {
    
    @IBOutlet var addWithdrawTableView: UITableView!
    
    var pageIndex       : Int = -1
    var contentsArr     = [String]()
    var imagesArr       = [String]()
    var numberOfRows    : Int = -1
    var selectedCashType: CashType  = CashType(rawValue: 0)!
    var enumArr                     = [CashInOutType]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if pageIndex == 0 {
            numberOfRows        = 2
            imagesArr           = ["send_money","blue_wallet"]
            contentsArr         = ["Send Money To Bank".localized, "Other Mobile Money Wallet".localized]
            addWithdrawTableView.reloadData()
        } else if pageIndex == 1 {
            numberOfRows        = 5
            imagesArr           = ["ok_office","add_money_ok_agent","ok_service","post","categories_new"]
            contentsArr         = ["OK$ OfficeAW".localized,"OK$ AgentsAW".localized,"OK$ Service Shop".localized, "Myanmar Post".localized ,"All".localized]
            enumArr             = [.cashin_alloffices, .cashin_allagents, .cashin_allbanks, .cashin_allmyanmarpost, .cashin_alllist]
            addWithdrawTableView.reloadData()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        println_debug("AddWithdrawTableViewController")
        super.viewDidAppear(animated)
    }
    
    // MARK: - Navigation
    func showSendMoneyToBankView() {
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "SendMoney", delegate: self)
        } else {
            self.navigateToSendMoneyToBank()
        }
    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if screen == "SendMoney" {
            if isSuccessful
            {
                if self.isAdvanceMerchant() {
                    //self.callKinleyNumAPI()
                } else {
                    self.navigateToSendMoneyToBank()
                }
            }
        }
    }
    
    func navigateToSendMoneyToBank() {
        let story = UIStoryboard(name: "SendMoney", bundle: nil)
        let sendMoneyToBankView = story.instantiateViewController(withIdentifier: "SendMoneyToBankView_ID") as! SendMoneyToBankViewController
        self.navigationController?.pushViewController(sendMoneyToBankView, animated: true)
    }
    
    func showOKMoneyInViews(_ indexPath: IndexPath) {
        
        let story: UIStoryboard = UIStoryboard(name: "AddWithdrawMain", bundle: nil)
        let nearByOkServicesView = story.instantiateViewController(withIdentifier: "OK$ServicesViewController") as! OK_ServicesViewController
       
       if(indexPath.row == 0)
        {
            nearByOkServicesView.intType = 7//office
        }
        else if(indexPath.row == 1)
        {
            nearByOkServicesView.intType = 8//agent
        }
        else if(indexPath.row == 2)
        {
            nearByOkServicesView.intType = 2//merchant
        }
        let aObjNav = UINavigationController(rootViewController: nearByOkServicesView)
        aObjNav.isNavigationBarHidden = true
        self.present(aObjNav, animated: true, completion: nil)
    }
    
    func showOKMoneyOutView(_ indexPath: IndexPath) {
        let cashInCashOutView = self.storyboard?.instantiateViewController(withIdentifier: "AddwithdrawCashInCashOutMainView_ID") as! CashInCashOutViewController
        selectedCashType                        = CashType.cashout
        cashInCashOutView.selectedCashType      = .cashout
        cashInCashOutView.selectedCashInOutType = enumArr[indexPath.row]
        self.navigationController?.pushViewController(cashInCashOutView, animated: true)
    }
}

extension AddWithdrawTableViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contentsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let addWithdrawCell = tableView.dequeueReusableCell(withIdentifier: "addwithdrawcellidentifier", for: indexPath) as! AddWithdrawTableCell
        addWithdrawCell.selectionStyle = .none
        addWithdrawCell.contentImgV.image = UIImage.init(named: imagesArr[indexPath.row])
        addWithdrawCell.contentLabel.text = contentsArr[indexPath.row]
        addWithdrawCell.comingSoonLabel.isHidden = true
        if pageIndex == 0 && indexPath.row == 1 {
            addWithdrawCell.comingSoonLabel.isHidden = false
        } else if pageIndex == 1 && indexPath.row == 3 {
            addWithdrawCell.comingSoonLabel.isHidden = false
        }
        return addWithdrawCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        
        if pageIndex == 0 {
            if indexPath.row == 0 {
                showSendMoneyToBankView()
            } else {
                self.showErrorAlert(errMessage: "Coming Soon".localized)
            }
        }else if pageIndex == 1 {
            if indexPath.row == 3 {
                self.showErrorAlert(errMessage: "Coming Soon".localized)
            }
            else if indexPath.row == 4 {
                //Check location access
                
                if CLLocationManager.locationServicesEnabled() {
                    switch(CLLocationManager.authorizationStatus()) {
                    case .notDetermined:
                        println_debug("notDetermined")
                        loadNearByServicesView()
                        break
                    case .denied :
                        println_debug("denied")
                        showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                        break
                    case .restricted :
                        println_debug("restricted")
                        showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                        break
                    case .authorizedAlways, .authorizedWhenInUse:
                        loadNearByServicesView()
                        break
                    }
                } else {
                    alertViewObj.wrapAlert(title:"", body: "Please Enable Location Service".localized, img: #imageLiteral(resourceName: "location"))
                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                        
                    })
                    DispatchQueue.main.async {
                        alertViewObj.showAlert(controller: self)
                    }
                }
                
            }
            else {
                //Check location access
                
                if CLLocationManager.locationServicesEnabled() {
                    switch(CLLocationManager.authorizationStatus()) {
                    case .notDetermined:
                        println_debug("notDetermined")
                        showOKMoneyInViews(indexPath)
                        break
                    case .denied :
                        println_debug("denied")
                        showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                        break
                    case .restricted :
                        println_debug("restricted")
                        showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                        break
                    case .authorizedAlways, .authorizedWhenInUse:
                        showOKMoneyInViews(indexPath)
                        break
                    }
                } else {
                    alertViewObj.wrapAlert(title:"", body: "Please Enable Location Service".localized, img: #imageLiteral(resourceName: "location"))
                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                        
                    })
                    DispatchQueue.main.async {
                        alertViewObj.showAlert(controller: self)
                    }
                }
                
            }
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:"OK$ would like to use your Current Location. Please turn on the Location Services for your device.".localized, img:#imageLiteral(resourceName: "map"))
        
        alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
            
        })
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
            }
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func loadNearByServicesView()
    {
        let story: UIStoryboard = UIStoryboard(name: "NBSMain", bundle: nil)
        let nearByOkServicesView = story.instantiateViewController(withIdentifier: "NearByServicesMainVC") as! NearByServicesMainVC
        let aObjNav = UINavigationController(rootViewController: nearByOkServicesView)
        aObjNav.isNavigationBarHidden = true
        nearByOkServicesView.statusScreen = "Service"
        self.present(aObjNav, animated: true, completion: nil)
    }
}

// MARK :- Advance Merchant Validation for Send Money to Bank
extension SendMoneyToBankViewController {
    
    func callKinleyNumAPI() {
        guard appDelegate.checkNetworkAvail() else {
            return
        }
        
        progressViewObj.showProgressView()
        BankApiClient.kinleyNum_AdvanceMerchant {  (isSuccess, response) in
            progressViewObj.removeProgressView()
            UserModel.shared.paymentNumber_AdvanceMerchant = ""
            guard isSuccess else {
                DispatchQueue.main.async {
                    self.showErrorAlert(errMessage: "Error On getting Kinley Number.. Dev need to check")
                    self.view.endEditing(true)
                }
                return
            }
            
            guard let kinleynum = response as? String else {
                DispatchQueue.main.async {
                    self.showErrorAlert(errMessage: "Error On getting Kinley Number.. Dev need to check")
                    self.view.endEditing(true)
                }
                return
            }
            
            let serverKinleyNumber = kinleynum
            for obj in User.shared.msnidDetails {
                let msnidNumber = obj.mobileNumber
                if msnidNumber == serverKinleyNumber {
                    UserModel.shared.paymentNumber_AdvanceMerchant = msnidNumber
//                    DispatchQueue.main.async {
//                        self.navigateToSendMoneyToBank()
//                    }
                    return
                }
            }
            guard User.shared.msnidDetails.count < 5  else {
                DispatchQueue.main.async {
                    self.showErrorAlert(errMessage: "Already Five Numbers Available.. So Contact customer care to delete any number ")
                }
                return
            }
            // call the add five number api
            self.callAddOK$FiveNumberApi(serverKinleyNumber: serverKinleyNumber, addfiveNumberHandler: { (isSuccess, errMsg, response) in
                DispatchQueue.main.async {
                    guard isSuccess else {
                        var message = "Error On Adding Kinley Number.. Dev need to check"
                        if let msg = errMsg, msg.count > 0 {
                            message = msg
                        }
                        self.showErrorAlert(errMessage: message)
                        self.view.endEditing(true)
                        return
                    }
                   // self.navigateToSendMoneyToBank()
                }
            })
        }
    }
    
    func callAddOK$FiveNumberApi(serverKinleyNumber: String, addfiveNumberHandler: @escaping (Bool, String?, Any?) -> Void) {
        progressViewObj.showProgressView()
        var ok$_FiveNums = [Dictionary<String, Any>]()
        for obj in User.shared.msnidDetails {
            var dic = [String: Any]()
            dic["Countrycode"]   = obj.countryCode
            dic["Msisidn"]       = obj.mobileNumber
            ok$_FiveNums.append(dic)
        }
        var addNumDict = [String: Any]()
        addNumDict["Countrycode"]   = "+95"
        addNumDict["Msisidn"]       = serverKinleyNumber
        ok$_FiveNums.append(addNumDict)
        var dictResponse = [String: Any]()
        dictResponse["Address1"]                = UserModel.shared.address1
        dictResponse["BusinessCategory"]        = UserModel.shared.businessCate
        dictResponse["BusinessType"]            = UserModel.shared.businessType
        dictResponse["DateOfBirth"]             = UserModel.shared.dob
        dictResponse["EmailId"]                 = UserModel.shared.email
        dictResponse["Father"]                  = UserModel.shared.fatherName
        dictResponse["Gender"]                  = UserModel.shared.gender
        dictResponse["LType"]                   = ""
        dictResponse["Language"]                = UserModel.shared.language
        dictResponse["MerBenefNumberList"]      = ok$_FiveNums
        dictResponse["MobileNumber"]            = UserModel.shared.mobileNo
        dictResponse["NRC"]                     = UserModel.shared.nrc
        dictResponse["Name"]                    = UserModel.shared.name
        dictResponse["OSType"]                  = UserModel.shared.osType
        dictResponse["Password"]                = UserModel.shared.pass
        dictResponse["Phone"]                   = UserModel.shared.phoneNumber
        dictResponse["SecureToken"]             = UserLogin.shared.token
        dictResponse["State"]                   = UserModel.shared.state
        dictResponse["Township"]                = UserModel.shared.township
        BankApiClient.addOk$FiveNumbers(param: dictResponse) {  (isSuccess, errMsg, response) in
            progressViewObj.removeProgressView()
            guard isSuccess else {
                addfiveNumberHandler(false,errMsg, nil)
                return
            }
            UserModel.shared.paymentNumber_AdvanceMerchant = serverKinleyNumber
            addfiveNumberHandler(true,errMsg, response)
        }
    }
}

class AddWithdrawTableCell: UITableViewCell {
    @IBOutlet var contentImgV: UIImageView!
    @IBOutlet var contentLabel: UILabel!
    @IBOutlet var comingSoonLabel: UILabel!
    {
        didSet
        {
            comingSoonLabel.text = comingSoonLabel.text?.localized
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        contentLabel.font = UIFont.init(setLabelFont: 16)
        comingSoonLabel.font = UIFont.init(setLabelFont: 12)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
