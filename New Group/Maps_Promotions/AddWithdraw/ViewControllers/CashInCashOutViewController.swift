//
//  CashInCashOutViewController.swift
//  OK
//
//  Created by Uma Rajendran on 12/6/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class CashInCashOutViewController: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
        {
        didSet
        {
            headerLabel.text = headerLabel.text?.localized
        }
    }
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var topSearchView: UIView!
    @IBOutlet weak var topSearchBar: UISearchBar!
    @IBOutlet weak var searchViewBackBtn: UIButton!
    @IBOutlet weak var headerSearchBtn: UIButton!
    
    var mapUiType: UIType?
    var cashInCashOutSubView: CashInCashOutSubViewController?
    
    var selectedCashType: CashType?
    var selectedCashInOutType: CashInOutType?
    
    var isHeaderSearchOpened: Bool = false
    
    @IBOutlet weak var headerSearchWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerSearchHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var switchMapListBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        geoLocManager.startUpdateLocation()
        loadUI()
        updateLocalizations()
        loadInitialize()
        mapUiType = .listView
        searchBarSettings()
        loadCashinCashoutSubView()
        hideSearchWithoutAnimation()
        
        //Manage Keyboard
        IQKeyboardManager.sharedManager().enable            = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        geoLocManager.startUpdateLocation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        geoLocManager.stopUpdateLocation()
    }
    
    // MARK: - Load UI and Localizations
    
    func loadUI() {
        headerHeightConstraint.constant = kHeaderHeight
        headerSearchHeightConstraint.constant = kHeaderHeight
        headerView.backgroundColor = kYellowColor
        baseView.layer.shadowColor = kBackGroundGreyColor.cgColor
        baseView.layer.shadowOpacity = 1
        baseView.layer.shadowOffset = CGSize.zero
        baseView.layer.shadowRadius = 4
        switchMapListBtn.setImage(UIImage.init(named: "map_white"), for: .normal)
    }
    
    func updateLocalizations() {
        headerLabel.font = UIFont.init(name: appFont, size: 20)
        if selectedCashType == .cashin {
            headerLabel.text = "Cash-In"
        }else {
            headerLabel.text = "Cash-Out"
        }
        headerLabel.text = "OK$ Services".localized

    }
    
    func loadInitialize() {
        
        
    }
    
    
    @IBAction func searchBtnAction(_ sender: Any) {
        self.topSearchView.frame = CGRect(x: screenArea.size.width - 30, y: 0, width: 0, height: kHeaderHeight)
        showCashSearch()
        
    }
    
    func showCashSearch() {
        isHeaderSearchOpened = true
        topSearchBar.text = ""
        topSearchView.layer.masksToBounds = true
        topSearchView.layer.cornerRadius = 20
        topSearchView.alpha = 0.0
        UIView.animate(withDuration: 0.3, animations: {
            var viewFrame: CGRect = self.topSearchView.frame
            viewFrame.origin.x = 0
            viewFrame.size.width = screenArea.size.width
            self.topSearchView.frame = viewFrame
            self.topSearchView.alpha = 1.0
            self.topSearchView.isHidden = false
            self.topSearchBar.becomeFirstResponder()
            
        }) { _ in
            self.topSearchView.layer.cornerRadius = 0
        }
    }
    
    func hideSearchWithoutAnimation() {
        isHeaderSearchOpened = false
        topSearchBar.text = ""
        topSearchBar.resignFirstResponder()
        topSearchView.isHidden = true
    }
    
    func hideCashSearch() {
        isHeaderSearchOpened = false
        topSearchBar.text = ""
        topSearchBar.resignFirstResponder()
        topSearchView.layer.cornerRadius = 20
        topSearchView.alpha = 1.0
        UIView.animate(withDuration: 0.3, animations: {
            var viewFrame: CGRect = self.topSearchView.frame
            viewFrame.origin.x = screenArea.size.width - 30
            viewFrame.origin.y = self.topSearchView.frame.origin.y + 15
            viewFrame.size.width = 0
            viewFrame.size.height = self.topSearchView.frame.size.height / 2
            self.topSearchView.frame = viewFrame
            
        }) { _ in
            self.topSearchView.alpha = 0.0
        }
    }
    
    
    @IBAction func searchViewBackAction(_ sender: Any) {
        hideCashSearch()
        cashInCashOutSubView?.reloadListWithRecentsFromServer()

    }
    
    
    @IBAction func mainViewBackAction(_ sender: Any) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
      
    }
    
   
    
    func searchBarSettings() {
        topSearchBar.tintColor = UIColor.darkGray
        topSearchBar.barTintColor = UIColor.white
        topSearchBar.searchBarStyle = UISearchBar.Style.minimal
        print("Cashincshout Viewcontroller searchBarSettings")
        let searchTextField: UITextField? = topSearchBar.value(forKey: "searchField") as? UITextField
        if searchTextField!.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
            let attributeDict = [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
            searchTextField!.attributedPlaceholder = NSAttributedString(string: "Search".localized, attributes: attributeDict)
            searchTextField!.backgroundColor = UIColor.white
            searchTextField!.borderStyle = UITextField.BorderStyle.none
            searchTextField!.layer.borderColor = UIColor.clear.cgColor
            searchTextField!.layer.borderWidth = 0.0
            searchTextField!.textColor = UIColor.black
            searchTextField!.autocorrectionType = UITextAutocorrectionType.no
            let myFont = UIFont.init(name: appFont, size: 15)
            if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                searchTextField!.font = myFont
            }else{
                //
            }
            searchTextField!.leftViewMode = UITextField.ViewMode.never
        }
        
    }
    
    func resignSearchBarKeypad() {
        topSearchBar.resignFirstResponder()
    }
    
    func loadCashinCashoutSubView() {
        let storyBoardName = UIStoryboard(name: "AddWithdrawMain", bundle: nil)
        cashInCashOutSubView = storyBoardName.instantiateViewController(withIdentifier: "AddwithdrawCashInCashOutSubView_ID") as? CashInCashOutSubViewController
        cashInCashOutSubView?.mapUiType = .listView
        cashInCashOutSubView?.navController = self.navigationController
        cashInCashOutSubView?.currentCashType = selectedCashType
        cashInCashOutSubView?.currentCashInOutType = selectedCashInOutType
        cashInCashOutSubView?.parentView = self
        baseView.addSubview(cashInCashOutSubView!.view)
    }
    
    func hideAndShowHeaderSearchBtn(isShow: Bool) {
        headerSearchBtn.isHidden = isShow ? false : true
    }
    
    @IBAction func switchListMapView(_ sender: Any) {
        let uitype: UIType = (cashInCashOutSubView?.switchMapListView())!
        let imagename = (uitype == .listView) ? "map_white" : "list"
        switchMapListBtn.setImage(UIImage.init(named: imagename), for: .normal)
    }
    
    
}


extension CashInCashOutViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {

        cashInCashOutSubView?.didSelectSearchWithKey(key: searchBar.text!)
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        cashInCashOutSubView?.didSelectSearchWithKey(key: searchBar.text!)

    }
    
    
}
