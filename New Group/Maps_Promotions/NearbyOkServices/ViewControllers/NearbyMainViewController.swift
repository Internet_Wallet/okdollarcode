//
//  NearbyMainViewController.swift
//  OK
//
//  Created by Uma Rajendran on 11/28/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift


protocol CashListDelegate: class {
    func resignParentSearchKeyboard()
    func hideParentSearchView()
}

class NearbyMainViewController: UIViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
 
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    {
        didSet
        {
            headerLabel.text = "Nearby OK$ Services".localized
        }
    }
    @IBOutlet weak var tabsView: UIView!
    @IBOutlet weak var tabMerchantBtn: UIButton!
        {
        didSet
        {
            self.tabMerchantBtn.setTitle("Merchants".localized, for: .normal)
        }
    }
    @IBOutlet weak var tabOkAgentsBtn: UIButton!
        {
        didSet
        {
            self.tabOkAgentsBtn.setTitle("Agents".localized, for: .normal)
        }
    }
    @IBOutlet weak var tabOkOfficesBtn: UIButton!
        {
        didSet
        {
            self.tabOkOfficesBtn.setTitle("Branch Office".localized, for: .normal)
        }
    }
    @IBOutlet weak var merchantsSeparatorLbl: UILabel!
    @IBOutlet weak var okAgentsSeparatorLbl: UILabel!
    @IBOutlet weak var okOfficesSeparatorLbl: UILabel!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var headerSearchBtn: UIButton!
    @IBOutlet weak var switchMapListBtn: UIButton!
    
    var allSourceViews = [UIViewController]()
    var pageViewController: UIPageViewController?
    var mapUiType: UIType?
    var promotionView: PromotionSubViewController?
    var okAgentsView: OkAgentSubViewController?
    var okOfficesView: OkOfficeSubViewController?
    var currentNearbyView: NearByView = .merchantView
    var prevNearbyView: NearByView = .merchantView
    
    @IBOutlet weak var topSearchView: UIView!
    
    @IBOutlet weak var topSearchBar: UISearchBar!
    
    @IBOutlet weak var searchViewBackBtn: UIButton!
     
    var isHeaderSearchOpened: Bool = false
    
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var headerSearchHeightConstraint: NSLayoutConstraint!
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topSearchBar.delegate = self
        // Do any additional setup after loading the view.
        geoLocManager.startUpdateLocation()
        loadUI()
        updateLocalizations()
        loadInitialize()
        mapUiType = .listView
        highLightPromotionsView()
        loadPageViewController()
        searchBarSettings()
        //getMerchantListAPI()
        
        //Manage Keyboard
        IQKeyboardManager.sharedManager().enable            = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
     }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        geoLocManager.startUpdateLocation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        geoLocManager.stopUpdateLocation()
    }
    
    //MARK:- Get API Data
    func getMerchantListAPI()
    {
        //1-Agent 2-Merchant 7-office
        //http://120.50.43.157:1318/UserService/SearchAgentsForOkDollar
        
        //Request: {"Amount":0,"CashType":0,"CountryCode":"+95","DistanceLimit":0,"Latitude":16.8166545,"Longitude":96.1319432,"PhoneNumber":"00959766378289","UserIdNotToConsider":""}
        
        let url = getUrl(urlStr: "", serverType: .nearByServicesUrl)
        
        let jsonDic = ["Amount" : 0, "CashType" : 0, "CountryCode" : "+95", "DistanceLimit" : 0, "Latitude" : 16.8166545, "Longitude": 96.1319432, "PhoneNumber" : UserModel.shared.mobileNo, "UserIdNotToConsider": ""] as [String : Any]

        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: url,methodType: kMethod_Post, contentType: kContentType_Json, inputVal: jsonDic, authStr: nil)
        
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else
            {
                println_debug(" API Fail")
                //self.removeProgressView()
                return
            }
            
            let dictResponse = response as! Dictionary<String, Any>
            println_debug("\n\n\n Data : \(dictResponse)")
            
        })
    }
    
    // MARK: - Load UI and Localizations
    
    func loadUI() {
        headerHeightConstraint.constant = kHeaderHeight
        headerSearchHeightConstraint.constant = kHeaderHeight
        headerView.backgroundColor = kYellowColor
        baseView.layer.shadowColor = kBackGroundGreyColor.cgColor
        baseView.layer.shadowOpacity = 1
        baseView.layer.shadowOffset = CGSize.zero
        baseView.layer.shadowRadius = 4
        self.view.setShadowToLabel(thisView: merchantsSeparatorLbl)
        self.view.setShadowToLabel(thisView: okAgentsSeparatorLbl)
        self.view.setShadowToLabel(thisView: okOfficesSeparatorLbl)
//        tabsView.layer.shadowColor = kBackGroundGreyColor.cgColor
//        tabsView.layer.shadowOpacity = 1
//        tabsView.layer.shadowOffset = CGSize.zero
//        tabsView.layer.shadowRadius = 4
        
        tabsView.backgroundColor = UIColor.white
        tabMerchantBtn.backgroundColor = UIColor.clear
        tabOkAgentsBtn.backgroundColor = UIColor.clear
        tabOkOfficesBtn.backgroundColor = UIColor.clear

    }
    
    func updateLocalizations() {
        headerLabel.font = UIFont.init(name: appFont, size: 17)
        tabMerchantBtn.titleLabel?.font = UIFont.init(name: appFont, size: appButtonSize)
        tabOkAgentsBtn.titleLabel?.font = UIFont.init(name: appFont, size: appButtonSize)
        tabOkOfficesBtn.titleLabel?.font = UIFont.init(name: appFont, size: appButtonSize)
    }
    
    func loadInitialize() {
        
        
    }
    
    
    func loadPageViewController() {
        let promotionStory = UIStoryboard(name: "Promotion", bundle: nil)
        let nearByStory = UIStoryboard(name: "NearbyOkServices", bundle: nil)
        promotionView = promotionStory.instantiateViewController(withIdentifier: "PromotionSubView_ID") as? PromotionSubViewController
         okAgentsView = nearByStory.instantiateViewController(withIdentifier: "OKAgentsView_ID") as? OkAgentSubViewController
         okOfficesView = nearByStory.instantiateViewController(withIdentifier: "OkOfficesView_ID") as? OkOfficeSubViewController
        
        pageViewController = UIPageViewController.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageViewController!.delegate = self
        pageViewController!.dataSource = self
        pageViewController!.view.frame = baseView.bounds
        pageViewController!.view.backgroundColor = UIColor.clear
        allSourceViews = [promotionView!,okAgentsView!,okOfficesView!]
        
        promotionView?.mapUiType = .listView
        promotionView?.viewFrom = .cashInOut
        promotionView?.navController = self.navigationController
        okAgentsView?.mapUiType = .listView
        okAgentsView?.navController = self.navigationController
        okAgentsView?.parentView = self
        okOfficesView?.mapUiType = .listView
        okOfficesView?.navController = self.navigationController
        okOfficesView?.parentView = self
        pageViewController!.setViewControllers([allSourceViews[0]], direction: .forward, animated: true, completion: nil)
        addChild(pageViewController!)
        baseView.addSubview(pageViewController!.view)
        pageViewController!.didMove(toParent: self)
        
    }
     
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index : Int = self.indexOf(viewController)
        if index == 0 {
            return nil
        }
        index -= 1
        return allSourceViews[index]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index : Int = self.indexOf(viewController)
        index += 1
        if index == allSourceViews.count {
            return nil
        }
        return allSourceViews[index]
    }
     
     func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        self.setPageIndex()
     }
     
     func indexOf(_ viewController: UIViewController) -> Int {
        return (allSourceViews as NSArray).index(of: viewController)
     }
    
    func viewControllerAtIndex(index: Int ) -> UIViewController? {
        if allSourceViews.count == 0 || index >= allSourceViews.count {
            return nil
        }
        return allSourceViews[index]
    }
    
    func setPageIndex() {
        let currentViewController: UIViewController = pageViewController!.viewControllers![0]
        let currentIndex: Int = self.indexOf(currentViewController)
        prevNearbyView = currentNearbyView
        if currentIndex == 0 {
            highLightPromotionsView()
        }else if currentIndex == 1 {
            highLightOKAgentsView()
        }else if currentIndex == 2 {
            highLightOKOfficesView()
        }
    }
     
     func highLightPromotionsView() {
        currentNearbyView = .merchantView
         DispatchQueue.main.async {
            self.hideSearchWithoutAnimation()
            self.hideAndShowHeaderSearchBtn(isShow: true)
            self.switchMapListBtn.setImage(UIImage.init(named: "map_white"), for: .normal)

            UIView.animate(withDuration: 0.5) {
                
                self.tabMerchantBtn.titleLabel?.textColor = UIColor.orange
                self.tabOkAgentsBtn.titleLabel?.textColor = UIColor.black
                self.tabOkOfficesBtn.titleLabel?.textColor = UIColor.black
                self.merchantsSeparatorLbl.backgroundColor = UIColor.black
                self.okAgentsSeparatorLbl.backgroundColor = UIColor.clear
                self.okOfficesSeparatorLbl.backgroundColor = UIColor.clear
                
                }
            }
     
     }
     
     func highLightOKAgentsView() {
        currentNearbyView = .okAgentsView
          DispatchQueue.main.async {
            self.hideSearchWithoutAnimation()
            self.hideAndShowHeaderSearchBtn(isShow: true)
            self.switchMapListBtn.setImage(UIImage.init(named: "map_white"), for: .normal)

            UIView.animate(withDuration: 0.5) {
              
                self.tabMerchantBtn.titleLabel?.textColor = UIColor.black
                self.tabOkAgentsBtn.titleLabel?.textColor = UIColor.orange
                self.tabOkOfficesBtn.titleLabel?.textColor = UIColor.black
                self.merchantsSeparatorLbl.backgroundColor = UIColor.clear
                self.okAgentsSeparatorLbl.backgroundColor = UIColor.black
                self.okOfficesSeparatorLbl.backgroundColor = UIColor.clear
                }
            }
     }
    
    func highLightOKOfficesView() {
        currentNearbyView = .okOfficesView
            DispatchQueue.main.async {
            self.hideSearchWithoutAnimation()
            self.hideAndShowHeaderSearchBtn(isShow: true)
            self.switchMapListBtn.setImage(UIImage.init(named: "map_white"), for: .normal)

            UIView.animate(withDuration: 0.5) {
            
                self.tabMerchantBtn.titleLabel?.textColor = UIColor.black
                self.tabOkAgentsBtn.titleLabel?.textColor = UIColor.black
                self.tabOkOfficesBtn.titleLabel?.textColor = UIColor.orange
                self.merchantsSeparatorLbl.backgroundColor = UIColor.clear
                self.okAgentsSeparatorLbl.backgroundColor = UIColor.clear
                self.okOfficesSeparatorLbl.backgroundColor = UIColor.black
                
                }
            }
    }
    
    @IBAction func searchBtnAction(_ sender: Any) {
        switch currentNearbyView {
        case .okAllView:
            self.topSearchView.frame = CGRect(x: screenArea.size.width - 30, y: 0, width: 0, height: kHeaderHeight)
            showCashSearch()
        case .merchantView:
            let story = UIStoryboard(name: "Promotion", bundle: nil)
            let searchView = story.instantiateViewController(withIdentifier: "SearchPromotionsView_ID") as! SearchPromotionsViewController
            searchView.delegate = promotionView
            self.navigationController?.pushViewController(searchView, animated: true)
        case .okAgentsView:
            self.topSearchView.frame = CGRect(x: screenArea.size.width - 30, y: 0, width: 0, height: kHeaderHeight)
            showCashSearch()
        case .okOfficesView:
            self.topSearchView.frame = CGRect(x: screenArea.size.width - 30, y: 0, width: 0, height: kHeaderHeight)
            showCashSearch()
//        default:
//            break
            
        }
    }
    
    func showCashSearch() {
        isHeaderSearchOpened = true
        topSearchBar.text = ""
        topSearchView.layer.masksToBounds = true
        topSearchView.layer.cornerRadius = 20
        topSearchView.alpha = 0.0
        UIView.animate(withDuration: 0.3, animations: {
            var viewFrame: CGRect = self.topSearchView.frame
            viewFrame.origin.x = 0
            viewFrame.size.width = screenArea.size.width
            self.topSearchView.frame = viewFrame
            self.topSearchView.alpha = 1.0
            self.topSearchView.isHidden = false
            self.topSearchBar.becomeFirstResponder()
            
        }) { _ in
            self.topSearchView.layer.cornerRadius = 0
        }
    }
    
    func hideSearchWithoutAnimation() {
        isHeaderSearchOpened = false
        topSearchBar.text = ""
        topSearchBar.resignFirstResponder()
        topSearchView.isHidden = true
    }
    
    func hideCashSearch() {
        isHeaderSearchOpened = false
        topSearchBar.text = ""
        topSearchBar.resignFirstResponder()
        topSearchView.layer.cornerRadius = 20
        topSearchView.alpha = 1.0
        UIView.animate(withDuration: 0.3, animations: {
            var viewFrame: CGRect = self.topSearchView.frame
            viewFrame.origin.x = screenArea.size.width - 30
            viewFrame.origin.y = self.topSearchView.frame.origin.y + 15
            viewFrame.size.width = 0
            viewFrame.size.height = self.topSearchView.frame.size.height / 2
            self.topSearchView.frame = viewFrame
            
        }) { _ in
            self.topSearchView.alpha = 0.0
        }
    }
    
    
    @IBAction func searchViewBackAction(_ sender: Any) {
        hideCashSearch()
        if currentNearbyView == .okAgentsView {
            okAgentsView?.reloadListWithRecentsFromServer()
        }else if currentNearbyView == .okAgentsView {
            okOfficesView?.reloadListWithRecentsFromServer()
        }
    }
    
    
    @IBAction func promotionMainBackAction(_ sender: Any) {
        if let presentingView = self.presentingViewController {
            presentingView.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func didSelectTabs(_ sender: Any) {
        prevNearbyView = currentNearbyView
        let btn: UIButton = sender as! UIButton
        switch btn.tag {
        case 0:
            if let view = viewControllerAtIndex(index: 0) {
                DispatchQueue.main.async {
                    self.highLightPromotionsView()
                }
                pageViewController!.setViewControllers([view], direction: .forward, animated: false, completion: nil)
            }
        case 1:
            if let view = viewControllerAtIndex(index: 1) {
                DispatchQueue.main.async {
                    self.highLightOKAgentsView()
                }
                pageViewController!.setViewControllers([view], direction: .forward, animated: false, completion: nil)
            }
        case 2:
            if let view = viewControllerAtIndex(index: 2) {
                DispatchQueue.main.async {
                    self.highLightOKOfficesView()
                }
                pageViewController!.setViewControllers([view], direction: .forward, animated: false, completion: nil)
            }
        default:
            break
        }
    }
    
    func searchBarSettings() {
        topSearchBar.tintColor = UIColor.darkGray
        topSearchBar.barTintColor = UIColor.white
        topSearchBar.searchBarStyle = UISearchBar.Style.minimal

        let searchTextField: UITextField? = topSearchBar.value(forKey: "searchField") as? UITextField
        if searchTextField!.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
            let attributeDict = [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
            searchTextField!.attributedPlaceholder = NSAttributedString(string: "Search".localized, attributes: attributeDict)
            searchTextField!.backgroundColor = UIColor.white
            searchTextField!.placeholder = "Search".localized
            searchTextField!.borderStyle = UITextField.BorderStyle.none
            searchTextField!.layer.borderColor = UIColor.clear.cgColor
            searchTextField!.layer.borderWidth = 0.0
            searchTextField!.textColor = UIColor.black
            searchTextField!.autocorrectionType = UITextAutocorrectionType.no
            let myFont = UIFont.init(name: appFont, size: 15)
            if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                searchTextField!.font = myFont
            }else{
                //
            }
            searchTextField!.leftViewMode = UITextField.ViewMode.never
        }
    
    }
    
    func resignSearchBarKeypad() {
        topSearchBar.resignFirstResponder()
    }
    
    func hideAndShowHeaderSearchBtn(isShow: Bool) {
        headerSearchBtn.isHidden = isShow ? false : true
    }
    
    @IBAction func switchMapListBtnAction(_ sender: Any) {
        switch currentNearbyView {
        case .okAllView:
            let uitype: UIType = (promotionView?.switchMapListView())!
            let imagename = (uitype == .listView) ? "map_white" : "list"
            
            if(imagename == "list")
            {
                self.hideAndShowHeaderSearchBtn(isShow: false)
            }
            else
            {
                self.hideAndShowHeaderSearchBtn(isShow: true)
            }
            switchMapListBtn.setImage(UIImage.init(named: imagename), for: .normal)
        case .merchantView:
            let uitype: UIType = (promotionView?.switchMapListView())!
            let imagename = (uitype == .listView) ? "map_white" : "list"
            
            if(imagename == "list")
            {
                self.hideAndShowHeaderSearchBtn(isShow: false)
            }
            else
            {
                self.hideAndShowHeaderSearchBtn(isShow: true)
            }
            switchMapListBtn.setImage(UIImage.init(named: imagename), for: .normal)
        case .okAgentsView:
            let uitype: UIType = (okAgentsView?.switchMapListView())!
            let imagename = (uitype == .listView) ? "map_white" : "list"
            if(imagename == "list")
            {
                self.hideAndShowHeaderSearchBtn(isShow: false)
            }
            else
            {
                self.hideAndShowHeaderSearchBtn(isShow: true)
            }
            switchMapListBtn.setImage(UIImage.init(named: imagename), for: .normal)
        case .okOfficesView:
            let uitype: UIType =  (okOfficesView?.switchMapListView())!
            let imagename = (uitype == .listView) ? "map_white" : "list"
            if(imagename == "list")
            {
                self.hideAndShowHeaderSearchBtn(isShow: false)
            }
            else
            {
                self.hideAndShowHeaderSearchBtn(isShow: true)
            }
            switchMapListBtn.setImage(UIImage.init(named: imagename), for: .normal)
//        default:
//            break
            
        }
    }
}


extension NearbyMainViewController: UISearchBarDelegate {
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
       
        if currentNearbyView == .okAgentsView {
            okAgentsView?.didSelectSearchWithKey(key: searchBar.text!)
        }else if currentNearbyView == .okOfficesView {
            okOfficesView?.didSelectSearchWithKey(key: searchBar.text!)
        }
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
      
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if currentNearbyView == .okAgentsView {
            okAgentsView?.didSelectSearchWithKey(key: searchBar.text!)
        }else if currentNearbyView == .okOfficesView {
            okOfficesView?.didSelectSearchWithKey(key: searchBar.text!)
        }
    }
    
    
}
