//
//  OkAgentSubViewController.swift
//  OK
//
//  Created by Uma Rajendran on 12/1/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Rabbit_Swift

class OkAgentSubViewController: UIViewController, SortViewDelegate, FilterViewDelegate, MainLocationViewDelegate, CashListDelegate {
    
    struct ListViewCollectionContent {
        let collectionItems         = ["Sort".localized,"Filter".localized]
        let collectionImages        = ["sort.png","filter.png"]
        let highLightImages         = [false, false]

    }
    
    struct MapViewCollectionContent {
        let collectionItems         = ["Filter".localized]
        let collectionImages        = ["filter.png"]
        let highLightImages         = [false]
    }
    
    let listViewDetails     = ListViewCollectionContent()
    let mapViewDetails      = MapViewCollectionContent()
    
    @IBOutlet weak var currentLocationView      : UIView!
    @IBOutlet weak var locationNameLbl          : UILabel!
        {
        didSet
        {
            locationNameLbl.text = locationNameLbl.text?.localized
            locationNameLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var locationContentLbl       : UILabel!
        {
        didSet
        {
            locationContentLbl.text = locationContentLbl.text?.localized
            locationContentLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var collectionView           : UICollectionView!
    @IBOutlet weak var baseView                 : UIView!
    
    private var collectItems         = [String]()
    private var collectImages        = [String]()
    private var highlightsImages     = [Bool]()
    
    var mapClusterView          : CashInOutMapViewController?
    var listCashInCashOutView   : CashInOutListViewController?
    
    var mapUiType: UIType?
    var navController : UINavigationController?
    
    var selectedSortItem    : Int = 0
    var selectedFilterData  : Any?
    
    let sortView        = SortViewController()
    let filterView      = FilterViewController()
    
    let margin: CGFloat = 2
    var cellsPerRow: Int = 0
    
    var selectedLocationType: Location?
    let nearbydistance = kNearByDistance
    
    var selectedLocationDetail      : LocationDetail?
    var selectedTownshipDetail      : TownShipDetail?
    
    var cashInListBackUPByCurLoc    : [CashInOutModel]?
    var cashInListRecentFromServer  : [CashInOutModel]?
    
    var cashUIType: CashUIType?
    var viewAppearFromScroll: Bool = true
    
    var parentView: NearbyMainViewController?
    var listViewHeight: CGFloat {
        if device.type == .iPhoneX {
            return screenArea.size.height - 270
        }
        return screenArea.size.height - 250
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        loadUI()
        loadInitialize()
        updateLocalizations()
        collectionViewSettings()
        checkAndLoadCollectionView()
        loadListViewAtFirst()
        setSelectedLocationType(locationtype: .byCurrentLocation)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        geoLocManager.startUpdateLocation()
        checkAndShowSearchBtnOnHeader()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if viewAppearFromScroll {
            mapUiType = .listView
            DispatchQueue.main.async {
                self.checkAndLoadCollectionView()
                self.switchBaseView()
            }
        }else {
            viewAppearFromScroll = true // here we just refresh the value from beginning
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        geoLocManager.stopUpdateLocation()
        hideParentSearchView()
    }
    
    func loadUI() {
        self.view.backgroundColor = kBackGroundGreyColor
        currentLocationView.backgroundColor = UIColor.white
        collectionView.backgroundColor = UIColor.white
        baseView.layer.shadowColor = kBackGroundGreyColor.cgColor
        baseView.layer.shadowOpacity = 1
        baseView.layer.shadowOffset = CGSize.zero
        baseView.layer.shadowRadius = 4
        
        collectionView.layer.shadowColor = kBackGroundGreyColor.cgColor
        collectionView.layer.shadowOpacity = 1
        collectionView.layer.shadowOffset = CGSize.zero
        collectionView.layer.shadowRadius = 4
    }
    
    func updateLocalizations() {
        locationNameLbl.textColor = kBlueColor
        locationContentLbl.textColor = kBlueColor
        locationNameLbl.font = UIFont.init(name: appFont, size: 15)
        locationContentLbl.font = UIFont.init(name: appFont, size: 14)
        locationContentLbl.lineBreakMode = NSLineBreakMode.byWordWrapping
    }
    
    func loadInitialize() {
        mapUiType = .listView
        setTapGesture()
        viewAppearFromScroll = true
        
    }
    
    func loadLocationByCurrentLocation() {
        cashInAgentsByCurrentLocation()
    }
    
    func didSelectLocationByCurrentLocation() {
        viewAppearFromScroll = false
        cashInAgentsByCurrentLocation()

    }
    
    
    func updateCurrentLocationToUI() {
        selectedLocationDetail = nil
        selectedTownshipDetail = nil
        locationNameLbl.text = "Current Location".localized
        locationContentLbl.text = "Loading location.....".localized
        guard appDelegate.checkNetworkAvail() else {
            return
        }
        if let lat = geoLocManager.currentLatitude, let long = geoLocManager.currentLongitude {
            geoLocManager.getLocationNameByLatLong(lattitude: lat, longitude: long) { (isSuccess, currentAddress) in
                guard isSuccess else {
                    println_debug("something went wrong when get current address from google api")
                    return
                }
                DispatchQueue.main.async {
                    if let curAddress = currentAddress as? String {
                        self.locationContentLbl.text = Rabbit.uni2zg(curAddress)//curAddress
                    }else {
                        self.locationContentLbl.text = "Loading location.....".localized
                    }
                }
            }
            
        }
    }
    
    func updateOtherLocationToUI(location: LocationDetail, township: TownShipDetail) {
        selectedLocationDetail = location
        selectedTownshipDetail = township
        DispatchQueue.main.async {
            self.locationNameLbl.text = "Other Location".localized
            
            if appDel.currentLanguage == "my" {
                self.locationContentLbl.text = "\(township.townShipNameMY), \(location.stateOrDivitionNameMy)" // later change to localizations
            }
            else
            {
                self.locationContentLbl.text = "\(township.townShipNameEN), \(location.stateOrDivitionNameEn)" // later change to localizations
            }
        }
    }
    
    func collectionViewSettings() {
        guard let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        flowLayout.minimumInteritemSpacing = margin
        flowLayout.minimumLineSpacing = margin
    }
    
    func setSelectedLocationType(locationtype: Location) {
        selectedLocationType = locationtype
    }
    
    func setTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnCurrentLocationView(_:)))
        currentLocationView.addGestureRecognizer(tap)
        currentLocationView.isUserInteractionEnabled = true
    }
    
    @objc func tapOnCurrentLocationView(_ sender: UITapGestureRecognizer) {
        println_debug("tap call on current location view")
        viewAppearFromScroll = false
        let storyBoardName = UIStoryboard(name: "Promotion", bundle: nil)
        let locationSelectionView = storyBoardName.instantiateViewController(withIdentifier: "SelecteMainLocationView_ID") as! SelectMainLocationViewController
        locationSelectionView.delegate = self
        self.navController?.pushViewController(locationSelectionView, animated: true)
    }
    
    func checkAndLoadCollectionView() {
        if mapUiType == .listView {
            collectItems        = listViewDetails.collectionItems
            collectImages       = listViewDetails.collectionImages
            highlightsImages   = listViewDetails.highLightImages
        }else {
            collectItems        = mapViewDetails.collectionItems
            collectImages       = mapViewDetails.collectionImages
            highlightsImages   = mapViewDetails.highLightImages
        }
        refreshSortFilter()
        cellsPerRow = collectItems.count
        collectionView.reloadData()
    }
    
    func refreshSortFilter() {
        selectedSortItem       = 0
        selectedFilterData     = nil
    }
    
    func loadListViewAtFirst() {
        mapUiType = .listView
        listCashInCashOutView             = CashInOutListViewController()
//        listCashInCashOutView!.view.frame = baseView.bounds
        listCashInCashOutView?.currentNearbyView = .okAgentsView
        listCashInCashOutView?.listDelegate = self
        listCashInCashOutView!.view.frame = CGRect(x: 0.0, y: 0.0, width: screenArea.size.width, height: listViewHeight)//screenArea.size.height - 250
        listCashInCashOutView?.view.backgroundColor = UIColor.white
        
        if let nav = self.navController {
            listCashInCashOutView?.nav = nav
        }
        DispatchQueue.main.async {
            self.listCashInCashOutView?.showHideInfoLabel(isShow: true, content: "Loading Data...".localized)
            self.baseView.addSubview(self.listCashInCashOutView!.view)
        }
    }
    
    func switchBaseView() {
        checkAndShowSearchBtnOnHeader()
        if mapUiType == .listView {
            if let map = mapClusterView {
                map.view.removeFromSuperview()
            }
            listCashInCashOutView             = CashInOutListViewController()
//            listCashInCashOutView!.view.frame = baseView.bounds
            listCashInCashOutView?.currentNearbyView = .okAgentsView
            listCashInCashOutView?.listDelegate = self
            listCashInCashOutView!.view.frame = CGRect(x: 0.0, y: 0.0, width: screenArea.size.width, height: listViewHeight)// 260)
            listCashInCashOutView?.view.backgroundColor = UIColor.white
            if let nav = self.navController {
                listCashInCashOutView?.nav = nav
            }
            if let cashlistbackup = cashInListBackUPByCurLoc {
                self.cashInListRecentFromServer = cashlistbackup
                DispatchQueue.main.async {
                    self.updateCurrentLocationToUI()
                    self.listCashInCashOutView?.showHideInfoLabel(isShow: false, content: "")
                    self.listCashInCashOutView?.cashInCashOutList = cashlistbackup
                    self.listCashInCashOutView?.listTableView.reloadData()
                }
            }else {
                DispatchQueue.main.async {
                    self.listCashInCashOutView?.showHideInfoLabel(isShow: true, content: "Loading Data...".localized)
                }
                loadLocationByCurrentLocation()
            }
            DispatchQueue.main.async {
                self.baseView.addSubview(self.listCashInCashOutView!.view)
            }
        }else {
            if let list = listCashInCashOutView {
                list.view.removeFromSuperview()
            }
            mapClusterView             = CashInOutMapViewController()
//            mapClusterView?.view.frame = baseView.bounds
            mapClusterView?.view.frame = CGRect(x: 0.0, y: 0.0, width: screenArea.size.width, height: listViewHeight)
            baseView.addSubview(mapClusterView!.view)
            if let cashlistbackup = cashInListBackUPByCurLoc {
                self.cashInListRecentFromServer = cashlistbackup
                updateCurrentLocationToUI()
                mapClusterView?.addAnnotationsInMap(list: cashlistbackup)
            }else {
                loadLocationByCurrentLocation()
            }
        }
    }

    func switchMapListView() -> UIType {
        if mapUiType == .listView {
            mapUiType = .mapView
        }else {
            mapUiType = .listView
        }
        DispatchQueue.main.async {
            self.checkAndLoadCollectionView()
            self.switchBaseView()
        }
        return mapUiType!
    }
    
    @objc func collectionBtnAction(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to: self.collectionView)
        let indexPath = self.collectionView.indexPathForItem(at: buttonPosition)
        println_debug("indexpath row is \(indexPath!.row)")
        println_debug("collection view did select called.....")
        if mapUiType == .listView {
            if indexPath?.row == 0 {
                // this is to show sort list view
                showSortListView()
            }else if indexPath?.row == 1 {
                // this to show filter view
                showFilterListView()
            }
        }else {
            if indexPath?.row == 0 {
                // this to show filter view
                showFilterListView()
            }
        }
        
        hideParentSearchView()

    }
    
    func showSortListView() {
        
        guard let listSpace = self.cashInListRecentFromServer, listSpace.count > 0 else {
            self.showToast(message: "No Results Found".localized, align: .top)
            return
        }
        
        sortView.delegate = self
        sortView.viewFrom = .cashInOut
        sortView.currentNearByView = .okAgentsView
        sortView.previousSelectedSortOption = selectedSortItem
        sortView.dataList = listCashInCashOutView?.cashInCashOutList // this is used to sort all the promotions
        sortView.modalPresentationStyle = .overCurrentContext
        self.present(sortView, animated: true, completion: nil)
    }
    
    func showFilterListView() {
        guard let listSpace = self.cashInListRecentFromServer, listSpace.count > 0 else {
            self.showToast(message: "No Results Found".localized, align: .top)
            return
        }
        
        filterView.delegate = self
        filterView.viewFrom = .cashInOut
        filterView.currentNearByView = .okAgentsView
        filterView.filterDataList = self.cashInListRecentFromServer
        filterView.modalPresentationStyle = .overCurrentContext
        self.present(filterView, animated: true, completion: nil)
    }
    
    // MARK: - Custom Delegate Methods
    
    func cashInAgentsByCurrentLocation() {
        updateCurrentLocationToUI()
        setSelectedLocationType(locationtype: .byCurrentLocation) // division , state both are similar oly
        guard let lat = geoLocManager.currentLatitude, let long = geoLocManager.currentLongitude else {
            self.showErrorAlert(errMessage: "Error on getting current location".localized)
            return
        }
        let mobilenumber  = UserModel.shared.mobileNo
        guard appDelegate.checkNetworkAvail() else {
            return
        }
        getTokenFromServer(completionHandler: { (isSuccess, tokenString) in
            
            guard isSuccess, let tokenAuthStr = tokenString else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                }
                self.showErrorAlert(errMessage: "Try Again".localized)
                return
            }
            let apiUrl = self.getCashInAgentsUrl(lat: lat, long: long, mobileNumber: mobilenumber)
            self.getCashInAPI(apiUrl: apiUrl, authenticationStr: tokenAuthStr as! String)
        })
    }
    
    func cashInAgentsByOtherLocation(location: LocationDetail, township: TownShipDetail) {
        updateOtherLocationToUI(location: location, township: township)
        setSelectedLocationType(locationtype: .byDivision)
        let mobilenumber  = UserModel.shared.mobileNo
        guard appDelegate.checkNetworkAvail() else {
            return
        }
        geoLocManager.getLatLongByName(cityname: township.cityNameEN) { (isSuccess, lat, long) in
            
            guard isSuccess, let latti = lat, let longi = long  else {
                self.showErrorAlert(errMessage: "Try Again".localized)
                return
            }
            self.getTokenFromServer(completionHandler: { (isSuccess, tokenString) in
                
                guard isSuccess, let tokenAuthStr = tokenString else {
                    DispatchQueue.main.async {
                        progressViewObj.removeProgressView()
                    }
                    self.showErrorAlert(errMessage: "Try Again".localized)
                    return
                }
                let apiUrl = self.getCashInAgentsUrl(lat: latti, long: longi, mobileNumber: mobilenumber)
                self.getCashInAPI(apiUrl: apiUrl, authenticationStr: tokenAuthStr as! String)
            })
        }
    }
    
    
    func getTokenFromServer(completionHandler: @escaping (Bool,Any?) -> Void){
        
        let urlStr = String.init(format: Url.cashInOutTokenAPI)
        let url: URL? = getUrl(urlStr: urlStr, serverType: .addwithdrawCashInCashOutUrl)

        guard let tokenUrl = url else {

            showErrorAlert(errMessage: "Something went wrong : Dev need to check".localized)
            return
        }
        
        let hashValue = Url.aKey_cash_in_out_prod.hmac_SHA1(key: Url.sKey_cash_in_out_prod)
        let inputVal = "password=\(hashValue)&grant_type=password"
        
        progressViewObj.showProgressView()
        println_debug("get token from server called........")
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: kMethod_Post, contentType: kContentType_urlencoded, inputVal: inputVal, authStr: nil)
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    println_debug("remove progress view called inside get token from server call")
                }
                completionHandler(false,nil)
                return
            }
            //println_debug("response dict for get token from server :: \(response)")
            let responseDic = response as! NSDictionary
            guard let tokentype = responseDic.value(forKey: "token_type"), let accesstoken = responseDic.value(forKey: "access_token") else {
                
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                }
                completionHandler(false,nil)
                return
            }
            
            let authorizationString =  "\(tokentype) \(accesstoken)"
            completionHandler(true,authorizationString)
        })
    }
    
    func getCashInAPI(apiUrl: URL, authenticationStr: String) {
        println_debug("call ok agents api :::: \(apiUrl)")
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: apiUrl, methodType: kMethod_Get, contentType: kContentType_Json, inputVal: nil, authStr: authenticationStr)
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
            println_debug("progress view started")
        }
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    println_debug("remove progress view called inside cashin main api call with error")
                    progressViewObj.removeProgressView()
                }
                self.listCashInCashOutView?.showHideInfoLabel(isShow: true, content: "No Promotions Found".localized)
                self.showErrorAlert(errMessage: "Try Again".localized)
                return
            }
            //println_debug("response dict for get all cashin agents list :: \(response)")
            var cashArray = [CashInOutModel]()
            DispatchQueue.main.async {
                cashArray = ParserAttributeClass.parseCashINJSON(anyValue: response as AnyObject, cashInType: CashInOutType.cashin_allagents.rawValue) as [CashInOutModel]
                println_debug("response count for ok agents :::: \(cashArray.count)")
                progressViewObj.removeProgressView()
                self.cashInListRecentFromServer = cashArray
                if self.mapUiType == .listView {
                    if cashArray.count > 0 {
                        if self.selectedLocationType == .byCurrentLocation {
                            self.cashInListBackUPByCurLoc = cashArray  // this is too just save the list in backup
                        }
                        DispatchQueue.main.async {
                            self.listCashInCashOutView?.showHideInfoLabel(isShow: false, content: "")
                            self.listCashInCashOutView?.cashInCashOutList = cashArray
                            self.listCashInCashOutView?.listTableView.reloadData()
                        }
                    }else {
                        DispatchQueue.main.async {
                            self.listCashInCashOutView?.showHideInfoLabel(isShow: true, content: "No Results Found".localized)
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        if cashArray.count > 0 {
                            self.mapClusterView?.addAnnotationsInMap(list: cashArray)
                        }else {
                            self.mapClusterView?.removeAllAnnotationsInMap()
                            self.showErrorAlert(errMessage: "No Results Found".localized)
                        }
                    }
                }
            }
        })
    }
    
    
    func didSelectLocationByTownship(location: LocationDetail, township: TownShipDetail) {
        viewAppearFromScroll = false
        cashInAgentsByOtherLocation(location: location, township: township)

    }
    
    
    func didSelectSortOption(sortedList: [Any], selectedSortOption: Int)  {
        viewAppearFromScroll = false

        if selectedSortOption == 0 {
            // default selected so load the data get from server
            self.selectedSortItem = selectedSortOption
            listCashInCashOutView?.cashInCashOutList = cashInListRecentFromServer!
            self.hightLightNeeded(isHighLight: false, from: "sort")

        }else {
            if let list = sortedList as? [CashInOutModel] {
                self.selectedSortItem = selectedSortOption
                listCashInCashOutView?.cashInCashOutList = list
                self.hightLightNeeded(isHighLight: true, from: "sort")

            }
        }
        DispatchQueue.main.async {
            self.listCashInCashOutView?.listTableView.reloadData()
        }
        
    }
    
    func didSelectFilterOption(filteredList: [Any], selectedFilter: Any?) {
        viewAppearFromScroll = false

        if self.mapUiType == .listView  {
            if let selectedfilter = selectedFilter as? CashInOutModel {
                self.selectedFilterData = selectedfilter
                listCashInCashOutView?.cashInCashOutList = filteredList as! [CashInOutModel]
            }else{
                // here is the default filter selection coming
                self.selectedFilterData = nil
                listCashInCashOutView?.cashInCashOutList = cashInListRecentFromServer!
            }
            DispatchQueue.main.async {
                self.listCashInCashOutView?.listTableView.reloadData()
            }
        }else {
            if let selectedfilter = selectedFilter as? CashInOutModel {
                self.selectedFilterData = selectedfilter
                DispatchQueue.main.async {
                    self.mapClusterView?.addAnnotationsInMap(list: filteredList as! [CashInOutModel])
                }
            }else{
                // here is the default filter selection coming
                self.selectedFilterData = nil
                DispatchQueue.main.async {
                    self.mapClusterView?.addAnnotationsInMap(list: self.cashInListRecentFromServer!)
                }
            }
            
        }
        
        if let _ = selectedFilterData {
            self.hightLightNeeded(isHighLight: true, from: "filter")
        }else {
            self.hightLightNeeded(isHighLight: false, from: "filter")
        }
        
    }
    
    
    func didSelectSearchWithKey(key: String) {
       
        guard let listSpace = self.cashInListRecentFromServer, listSpace.count > 0 else {
            self.showErrorAlert(errMessage: "No Records found to search".localized)
            return
        }
        
        if key.count > 0 {
            let filteredArray = cashInListRecentFromServer?.filter { ($0.cash_ContactPersonName ?? "").range(of: key, options: [.diacriticInsensitive, .caseInsensitive]) != nil }

            if let filter = filteredArray  {
                if filter.count > 0 {
                    DispatchQueue.main.async {
                        self.listCashInCashOutView?.showHideInfoLabel(isShow: false, content: "")
                        self.listCashInCashOutView?.cashInCashOutList = filter
                        self.listCashInCashOutView?.listTableView.reloadData()
                    }
                }else {
                    DispatchQueue.main.async {
                        self.listCashInCashOutView?.showHideInfoLabel(isShow: true, content: "No Results Found".localized)
                    }
                }
            }
            
        }else{
           reloadListWithRecentsFromServer()
        }

     }
    
    func resignParentSearchKeyboard() {
        self.parentView?.resignSearchBarKeypad()
    }
    
    func hideParentSearchView() {
        if (self.parentView?.isHeaderSearchOpened)! {
            self.parentView?.hideSearchWithoutAnimation()
            reloadListWithRecentsFromServer()
        }
    }
    
    func reloadListWithRecentsFromServer() {
        if let filter = cashInListRecentFromServer {
            if filter.count > 0 {
                DispatchQueue.main.async {
                    self.listCashInCashOutView?.showHideInfoLabel(isShow: false, content: "")
                    self.listCashInCashOutView?.cashInCashOutList = filter
                    self.listCashInCashOutView?.listTableView.reloadData()
                }
            }else {
                DispatchQueue.main.async {
                    self.listCashInCashOutView?.showHideInfoLabel(isShow: true, content: "No Results Found".localized)
                }
            }
        }
    }
    
    func checkAndShowSearchBtnOnHeader() {
        if mapUiType == .listView {
            self.parentView?.hideAndShowHeaderSearchBtn(isShow: true)
        }else {
            self.parentView?.hideAndShowHeaderSearchBtn(isShow: false)
        }
    }
    
    func hightLightNeeded(isHighLight : Bool, from: String) {
       
        if mapUiType == .listView {
            if isHighLight {
                if from == "sort" { // sort
                    highlightsImages = [true, false]
                    
                }else { // filter

                    highlightsImages = [false, true]
                }
            }else {
                highlightsImages = [false, false]
            }
        }else {
            if isHighLight {
                highlightsImages = [true]
            }else {
                highlightsImages = [false]
            }
        }
        
        self.collectionView?.reloadData()
    }
    
}

extension OkAgentSubViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return collectItems.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mapcollectioncellidentifier", for: indexPath) as! AgentCollectionCell
        cell.wrapData(title: collectItems[indexPath.row], imgName: collectImages[indexPath.row], hightLights: highlightsImages[indexPath.row])
//        cell.listBtn.addTarget(self, action: #selector(collectionBtnAction(_:)), for: .touchUpInside)
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if mapUiType == .listView {
            if indexPath.row == 0 {
                // this is to show sort list view
                showSortListView()
            }else if indexPath.row == 1 {
                // this to show filter view
                showFilterListView()
            }
        }else {
            if indexPath.row == 0 {
                // this to show filter view
                showFilterListView()
            }
        }
        
        hideParentSearchView()

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellHeight = self.collectionView.frame.height
        let flowLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let marginsAndInsets = flowLayout.sectionInset.left + flowLayout.sectionInset.right + flowLayout.minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
        let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
        return CGSize(width: itemWidth, height: cellHeight)
    }
     
}


class AgentCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var midSeparatorLbl  : UILabel!
    //    @IBOutlet weak var listBtn          : UIButton!
    @IBOutlet weak var highlightImgV    : UIImageView!
    @IBOutlet weak var itemImgV         : UIImageView!
    @IBOutlet weak var itemNameLbl      : UILabel!
    
    func wrapData(title: String, imgName: String, hightLights: Bool) {
        
        //        self.listBtn.setTitle(title, for: UIControlState.normal)
        //        self.listBtn.setImage(UIImage.init(named: imgName), for: UIControlState.normal)
        
        self.itemNameLbl.text = title
        self.itemImgV.image = UIImage.init(named: imgName)
        
        if hightLights {
            self.highlightImgV.isHidden = false
        }else {
            self.highlightImgV.isHidden = true
        }
        
        //        self.highlightImgV.isHidden =   (hightLights == true) ? false : true
        self.highlightImgV.image    =   UIImage.init(named: "bank_success")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //        self.listBtn.setTitleColor(UIColor.black, for: UIControlState.normal)
        //        self.listBtn.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
        //        self.listBtn.titleLabel?.font = UIFont.init(name: appFont, size: 13.0)
        
        self.itemNameLbl.font = UIFont.init(name: appFont, size: 13.0)
        self.itemNameLbl.textColor = UIColor.black
    }
}
