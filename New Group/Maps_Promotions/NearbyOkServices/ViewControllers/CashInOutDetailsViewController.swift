//
//  CashInOutDetailsViewController.swift
//  OK
//
//  Created by Uma Rajendran on 11/30/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

import ParallaxHeader
import CoreData

class CashInOutDetailsViewController: UIViewController, CustomHeaderDelegate {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    private let sliderHeight: CGFloat = 80
    var selectedCashInOut: CashInOutModel?
    var isfavoritePromotion: Bool = false
    
    let maxHeaderHeight: CGFloat = 88;
    let minHeaderHeight: CGFloat = 20;
    var previousScrollOffset: CGFloat = 0;
    
    @IBOutlet weak var titleTopConstraint: NSLayoutConstraint!

    var currentCashInOutType: CashInOutType?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //        headerView.isHidden = true
        //        headerHeightConstraint.constant = 0
        loadUI()
        let imagesArr = ["betterthan.png","betterthan.png","betterthan.png","betterthan.png","betterthan.png","betterthan.png","betterthan.png"]
        tableView.parallaxHeader.view = CustomHeaderView.instanceFromNib(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 260), images: imagesArr, delegate: self)
        //    tableView.parallaxHeader.view = imageView
        tableView.parallaxHeader.height = 260 // parallaxHeight //400
        tableView.parallaxHeader.minimumHeight = 0
        tableView.parallaxHeader.mode = .centerFill //.topFill
        tableView.parallaxHeader.parallaxHeaderDidScrollHandler = { parallaxHeader in
            //            println_debug("just check header scrolling or not")
            //            println_debug(parallaxHeader.progress)
        }
        tableView.tableFooterView = UIView.init(frame: CGRect.zero)
        isfavoritePromotion = favoriteExist()
        
        self.headerHeightConstraint.constant = self.minHeaderHeight
        headerView.alpha = 0
        self.titleTopConstraint.constant = -100
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func loadUI() {
        headerView.backgroundColor = kYellowColor
        headerLabel.text = "Not Available".localized
        headerLabel.font = UIFont.init(name: appFont, size: 18.0)
    }
    
    @IBAction func backAction(_ sender: Any) {
        closeActionFromCustomHeader()
    }
    
    
    // MARK: - Delegate Methods
    
    func closeActionFromCustomHeader() {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    
    @objc func callBtnAction() {
        if let cashinout = selectedCashInOut {
            if let phonenumber = cashinout.cash_ContactNumber {
                if phonenumber.count > 0 {
                    var mobNumber = getPhoneNumberByFormat(phoneNumber: phonenumber)
                    mobNumber = mobNumber.removingWhitespaces()
                    let phone = "tel://\(mobNumber)"
                    if let url = URL.init(string: phone) {
                        UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                    }
                }else{
                    showNoPhoneNumberError()
                }
            }else {
                showNoPhoneNumberError()
            }
        }else {
          showNoPhoneNumberError()
        }
     
    }
    
    func showNoPhoneNumberError() {
        alertViewObj.wrapAlert(title: nil, body: "No Phonenumber Exists".localized, img: nil)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        alertViewObj.showAlert(controller: self)
    }
    
    func favoriteExist() -> Bool {
      
            let managedContext = appDelegate.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FavoriteCashIn")
            do {
                fetchRequest.predicate = NSPredicate(format: "cashId == %@", (self.selectedCashInOut?.cash_Id)!)
                let fetchedResults = try managedContext.fetch(fetchRequest) as! [FavoriteCashIn]
                if let _ = fetchedResults.first {
                    return true
                }else {
                    return false
                }
            }
            catch {
                print ("fetch task failed", error)
            }
            return false
        
    }
    
    @objc func favoriteBtnAction() {
        addDeleteCashInFavorite()
    }
    
    func addDeleteCashInFavorite() {
        guard (self.selectedCashInOut != nil) else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FavoriteCashIn")
        do {
            fetchRequest.predicate = NSPredicate(format: "cashId == %@", (self.selectedCashInOut?.cash_Id)!)
            let fetchedResults = try managedContext.fetch(fetchRequest) as! [FavoriteCashIn]
            if let aFavorite = fetchedResults.first {
                alertViewObj.wrapAlert(title: "Delete favorite?", body: "Do you want to delete from your favorite ?", img: nil)
                alertViewObj.addAction(title: "Yes", style: .target , action: {
                    // here need to delete the favorite from db
                    managedContext.delete(aFavorite)
                    do {
                        try managedContext.save()
                        self.isfavoritePromotion = false
                        let indexPath = IndexPath(item: 0, section: 1)
                        self.tableView.reloadRows(at: [indexPath], with: .fade)
                        
                    } catch let error as NSError {
                        println_debug("Could not save. \(error), \(error.userInfo)")
                    }
                    
                })
                alertViewObj.addAction(title: "No", style: .target , action: {
                })
                alertViewObj.showAlert(controller: self)
            }else {
                // here need to add the favorite
                alertViewObj.wrapAlert(title: "Add as Favorite?".localized, body: "Do you want to add in your favorite ?".localized, img: nil)
                alertViewObj.addAction(title: "Yes".localized, style: .target , action: {
                    // here need to add the favorite from db
                    guard let cashinout = self.selectedCashInOut else {
                        return
                    }
                    let saveData = NSKeyedArchiver.archivedData(withRootObject: cashinout)
                    let managedContext = appDelegate.persistentContainer.viewContext
                    let entity = NSEntityDescription.entity(forEntityName: "FavoriteCashIn",
                                                            in: managedContext)!
                    let favPromotion = NSManagedObject(entity: entity,
                                                       insertInto: managedContext)
                    favPromotion.setValue(saveData, forKeyPath: "cashInData")
                    favPromotion.setValue(Date(), forKeyPath: "addedDateTime")
                    favPromotion.setValue(cashinout.cash_Id, forKeyPath: "cashId")
                    do {
                        try managedContext.save()
                        self.isfavoritePromotion = true
                        let indexPath = IndexPath(item: 0, section: 1)
                        self.tableView.reloadRows(at: [indexPath], with: .fade)
                        
                    } catch let error as NSError {
                        println_debug("Could not save. \(error), \(error.userInfo)")
                    }
                })
                alertViewObj.addAction(title: "No", style: .target , action: {
                })
                alertViewObj.showAlert(controller: self)
            }
            
        }
        catch {
            print ("fetch task failed", error)
        }
        
    }
    
    @objc func directionBtnAction() {
      
            guard let cashinout = self.selectedCashInOut else {
                return
            }
            if let curLat = geoLocManager.currentLatitude, let curLong = geoLocManager.currentLongitude {
                let urlStr = "http://maps.google.com/maps?saddr=\(curLat),\(curLong)&daddr=\((cashinout.addressDetails?.address_Latitude)!),\((cashinout.addressDetails?.address_Longitude)!)"
                if let url = URL.init(string: urlStr) {
                    UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                }
            }
        
    }
    
    @objc func shareBtnAction() {
        
            guard let cashinout = self.selectedCashInOut else {
                return
            }
            var mobileNumber = ""
            if let phonenumber = cashinout.cash_ContactNumber {
                if phonenumber.count > 0 {
                    mobileNumber = getPhoneNumberByFormat(phoneNumber: phonenumber)
                }
            }
            let textToShare: String = "OK$\n\(cashinout.cash_BusinessName!)\n\(cashinout.cash_ContactPersonName!)\n\(mobileNumber)\n"
            let link: String = "https://www.google.com/maps/search/?api=1&query=\((cashinout.addressDetails?.address_Latitude)!),\((cashinout.addressDetails?.address_Longitude)!)"
            
            let url:NSURL = NSURL.init(string: link)!
            var activityItems = [Any]()
            activityItems.append(textToShare)
            activityItems.append(url)
            
            let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
            activityViewController.excludedActivityTypes = [.assignToContact, .print]
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
        
    }
    
    @objc func navigateToMapDetailViewAction() {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "NearbyOkServices", bundle: nil)
        let mapDetailsView = storyboard.instantiateViewController(withIdentifier: "CashInOutMapDetailsView_ID") as! CashInOutDetailsInMapViewController
        mapDetailsView.isFavorite = self.isfavoritePromotion
        mapDetailsView.selectedCashInOut = self.selectedCashInOut
        self.navigationController?.pushViewController(mapDetailsView, animated: true)
    }
    
}

extension CashInOutDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowCount: Int = 0
        if section == 0 {
            rowCount = 1
        }else {
            rowCount = 8
        }
        return rowCount
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let headerCell = tableView.dequeueReusableCell(withIdentifier: "listDetailHeaderCellIdentifier", for: indexPath) as! MapDetailViewHeaderCell
            headerCell.contentView.backgroundColor = kYellowColor
//            headerCell.shopNameLabel.text = "Not Available"
            var name = "Not Available"
            if currentCashInOutType == .cashin_alllist || currentCashInOutType == .cashout_alllist {
                if (selectedCashInOut?.cash_CashInCashOutType?.count)! > 0 {
                    if selectedCashInOut?.cash_CashInCashOutType?.lowercased() == "okagent" {
                        if (selectedCashInOut?.cash_ContactPersonName?.count)! > 0 {
                            name = (selectedCashInOut?.cash_ContactPersonName!)!
                        }
                    }else if selectedCashInOut?.cash_CashInCashOutType?.lowercased() == "okoffice" {
                        if (selectedCashInOut?.cash_BusinessName?.count)! > 0 {
                            name = (selectedCashInOut?.cash_BusinessName)!
                        }
                    }else if selectedCashInOut?.cash_CashInCashOutType?.lowercased() == "bank" {
                        if (selectedCashInOut?.cash_BusinessName?.count)! > 0 {
                            name = (selectedCashInOut?.cash_BusinessName)!
                        }
                    }
                }
            }else if currentCashInOutType == .cashin_allbanks || currentCashInOutType == .cashout_allbanks  {
                if (selectedCashInOut?.cash_BusinessName?.count)! > 0 {
                    name = (selectedCashInOut?.cash_BusinessName)!
                }
            }else if currentCashInOutType == .cashin_allagents || currentCashInOutType == .cashout_allagents {
                if (selectedCashInOut?.cash_ContactPersonName?.count)! > 0 {
                    name = (selectedCashInOut?.cash_ContactPersonName)!
                }
            }else if currentCashInOutType == .cashin_alloffices || currentCashInOutType == .cashout_alloffices {
                if (selectedCashInOut?.cash_BusinessName?.count)! > 0 {
                    name = (selectedCashInOut?.cash_BusinessName)!
                }
            }else if currentCashInOutType == .cashin_allmyanmarpost || currentCashInOutType == .cashout_allmyanmarpost  {
                name = "NA" // right now we just set not available.. it will come soon
            }
            
            headerCell.shopNameLabel.text = name

//            headerCell.shopRateImgView.image = UIImage.init(named: "star_fiveWhite")
            for index in 0 ... headerCell.rateImgViewList.count - 1 {
                if index <  (selectedCashInOut?.cash_Rating)! {
                    headerCell.rateImgViewList[index].isHighlighted = true
                }else {
                    headerCell.rateImgViewList[index].isHighlighted = false
                }
            }
            
            var distance = "0 Km"
            if let shopDistance = selectedCashInOut?.addressDetails?.address_DistanceInKilometer {
                distance = String(format: "%.2f Km", shopDistance)
            }
            headerCell.shopDistanceLabel.text = distance
            headerCell.timeLabel.text = "1 MIN"
            return headerCell
        }else {
            switch indexPath.row {
            case 0:
                // here load options cell
                let optionsCell = tableView.dequeueReusableCell(withIdentifier: "listDetailOptionCellIdentifier", for: indexPath) as! MapDetailViewOptionsCell
                if isfavoritePromotion {
                    optionsCell.favoriteImgV.image = UIImage.init(named: "act_favorite")
                }else {
                    optionsCell.favoriteImgV.image = UIImage.init(named: "favorite")
                }
                optionsCell.callBtn.addTarget(self, action: #selector(callBtnAction), for: UIControl.Event.touchUpInside)
                optionsCell.favoriteBtn.addTarget(self, action: #selector(favoriteBtnAction), for: UIControl.Event.touchUpInside)
                optionsCell.directionBtn.addTarget(self, action: #selector(directionBtnAction), for: UIControl.Event.touchUpInside)
                optionsCell.shareBtn.addTarget(self, action: #selector(shareBtnAction), for: UIControl.Event.touchUpInside)
                return optionsCell
                
            case 1:
                let descriptionCell = tableView.dequeueReusableCell(withIdentifier: "listDetailDescCellIdentifier", for: indexPath) as! MapDetailViewDescriptionCell
                var personName = "Not available"
                if let name = selectedCashInOut?.cash_ContactPersonName {
                    if name.count > 0 {
                        personName = name
                    }
                }
                descriptionCell.wrapData(title: personName, imgName: "user.png", isLocationCell: false)
                return descriptionCell
                
            case 2:
                let categoryCell = tableView.dequeueReusableCell(withIdentifier: "listDetailCategoryCellIdentifier", for: indexPath) as! MapDetailViewCategoryCell
                var categoryName = "Not available" , subCategoryName = "Not available"
//                if isBurmese() {
//                    categoryName = (cashDetail?.cash_BusinessCategoryBurmese!)!
//                    subCategoryName = (cashDetail?.cash_BusinessSubCategoryBurmese!)!
//                }else {
                if let categ = selectedCashInOut?.cash_BusinessCategory, let subCateg = selectedCashInOut?.cash_BusinessSubCategory {
                    if categ.count > 0 {
                        categoryName = categ
                    }
                    if subCateg.count > 0 {
                        subCategoryName = subCateg
                    }
                }
//                }
               
                //categoryCell.wrapData(categorytitle: categoryName, subCategorytitle: subCategoryName)
                return categoryCell
            case 3:
                let descriptionCell = tableView.dequeueReusableCell(withIdentifier: "listDetailDescCellIdentifier", for: indexPath) as! MapDetailViewDescriptionCell

                var workingHours = ""
//                if self.isBurmese() {
//                    workingHours = (cashDetail?.settings?.setting_WorkingDaysBurmese!.count)! > 0 ? (cashDetail?.settings?.setting_WorkingDaysBurmese!)! : "9AM to 10PM"
//                }else {
                    workingHours = (selectedCashInOut?.settings?.setting_WorkingDays!.count)! > 0 ? (selectedCashInOut?.settings?.setting_WorkingDays!)! : "Not available"
//                }
                descriptionCell.wrapData(title: workingHours, imgName: "working", isLocationCell: false)
                return descriptionCell
                
            case 4:
                let descriptionCell = tableView.dequeueReusableCell(withIdentifier: "listDetailDescCellIdentifier", for: indexPath) as! MapDetailViewDescriptionCell
//                if self.isBurmese() {
//                    location = "\(cashDetail!.addressDetails!.address_AddressBurmese1!),\(cashDetail!.addressDetails!.address_AddressBurmese2!),\(cashDetail!.addressDetails!.address_TownshipNameBurmese!),\(cashDetail!.addressDetails!.address_StateNameBurmese!)"
//                }else {
                  var location = "\((selectedCashInOut?.addressDetails!.address_Address2!)!), \((selectedCashInOut?.addressDetails!.address_Address1!)!), \((selectedCashInOut?.addressDetails!.address_TownshipName!)!), \((selectedCashInOut?.addressDetails!.address_StateName!)!)"
//                }
                
                location = (location.count > 0) ? location : "Not availabe"
                descriptionCell.mapCategoryIcon.alpha = 0
                
                descriptionCell.wrapData(title: location, imgName: "pin.png", isLocationCell: true)
                descriptionCell.mapBtn.addTarget(self, action: #selector(navigateToMapDetailViewAction), for: UIControl.Event.touchUpInside)
                    let pinImage = UIImage(named: "ok.png")
                    descriptionCell.mapCategoryIcon.image = pinImage
                
                UIView.animate(withDuration: 0.5, delay: 0.4, options: .curveEaseOut, animations: {
                    descriptionCell.mapCategoryIcon.alpha = 1
                }, completion: nil)
                return descriptionCell
            case 5:
                let descriptionCell = tableView.dequeueReusableCell(withIdentifier: "listDetailDescCellIdentifier", for: indexPath) as! MapDetailViewDescriptionCell
                var mobileNumber = getPhoneNumberByFormat(phoneNumber: (selectedCashInOut?.cash_ContactNumber)!)
                mobileNumber = mobileNumber.count > 0 ? mobileNumber : "Not available"
                descriptionCell.wrapData(title: mobileNumber, imgName: "b_call.png", isLocationCell: false)
                return descriptionCell
            case 6:
                let descriptionCell = tableView.dequeueReusableCell(withIdentifier: "listDetailDescCellIdentifier", for: indexPath) as! MapDetailViewDescriptionCell
                let emailAddress = (selectedCashInOut?.socialMedia?.media_EmailAddress?.count)! > 0 ? selectedCashInOut?.socialMedia?.media_EmailAddress! : "Not available"
                descriptionCell.wrapData(title: emailAddress!, imgName: "division", isLocationCell: false)
                return descriptionCell
            case 7:
                let descriptionCell = tableView.dequeueReusableCell(withIdentifier: "listDetailDescCellIdentifier", for: indexPath) as! MapDetailViewDescriptionCell
                let fbAddress = (selectedCashInOut?.socialMedia?.media_Facebook?.count)! > 0 ? selectedCashInOut?.socialMedia?.media_Facebook! : "Not available"
                descriptionCell.wrapData(title: fbAddress!, imgName: "facebook", isLocationCell: false)
                return descriptionCell
                
            default:
                break
            }
        }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var rowHeight: CGFloat = 0.0
        if indexPath.section == 0 {
            rowHeight = 90
        }else if (indexPath.section == 1 && indexPath.row == 0) {
            rowHeight = 100
        }else if (indexPath.section == 1 && indexPath.row == 2){
            rowHeight = 90
        }else if indexPath.section == 1 && indexPath.row == 4 {
                rowHeight = 90
        }else {
            rowHeight = 70
        }
        return rowHeight
        
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 && indexPath.row == 5 {
            callBtnAction()
        }
    }
    
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollDiff = scrollView.contentOffset.y - self.previousScrollOffset
        
        let absoluteTop: CGFloat = 0;
        let absoluteBottom: CGFloat = scrollView.contentSize.height - scrollView.frame.size.height;
        
        let isScrollingDown = scrollDiff > 0 && scrollView.contentOffset.y > absoluteTop
        let isScrollingUp = scrollDiff < 0 && scrollView.contentOffset.y < absoluteBottom
        
        if canAnimateHeader(scrollView) {
            
            // Calculate new header height
            var newHeight = self.headerHeightConstraint.constant
            
            if isScrollingDown {
                newHeight = max(self.maxHeaderHeight, self.headerHeightConstraint.constant - abs(scrollDiff))
            } else if isScrollingUp {
                newHeight = min(self.minHeaderHeight, self.headerHeightConstraint.constant + abs(scrollDiff))
            }
            
            // Header needs to animate
            if newHeight != self.headerHeightConstraint.constant {
                self.headerHeightConstraint.constant = newHeight
                self.updateHeader()
                self.setScrollPosition(self.previousScrollOffset)
            }
            
            self.previousScrollOffset = scrollView.contentOffset.y
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.scrollViewDidStopScrolling()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            //  self.scrollViewDidStopScrolling()
        }
    }
    
    func scrollViewDidStopScrolling() {
        let range = self.maxHeaderHeight - self.minHeaderHeight
        let midPoint = self.minHeaderHeight + (range / 2)
        
        if self.headerHeightConstraint.constant > midPoint {
            self.expandHeader()  // collapseHeader
            
        } else {
            self.collapseHeader()    // expandHeader
        }
    }
    
    func canAnimateHeader(_ scrollView: UIScrollView) -> Bool {
        // Calculate the size of the scrollView when header is collapsed
        let scrollViewMaxHeight = scrollView.frame.height + self.headerHeightConstraint.constant - maxHeaderHeight
        
        // Make sure that when header is collapsed, there is still room to scroll
        return scrollView.contentSize.height > scrollViewMaxHeight
    }
    
    func expandHeader() {  // collapseHeader
        println_debug("expandHeader called")
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.2, animations: {
            self.headerHeightConstraint.constant = self.maxHeaderHeight
            self.updateHeader()
            UIView.animate(withDuration: 0.1, delay: 0.1,
                           usingSpringWithDamping: 0.2,
                           initialSpringVelocity: 0.2,
                           options: [], animations: {
                            self.titleTopConstraint.constant = 15
                            
            }, completion: nil)
            self.view.layoutIfNeeded()
        })
    }
    
    func collapseHeader() {  // expandHeader
        println_debug("collapseHeader called")
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.2, animations: {
            self.headerHeightConstraint.constant = self.minHeaderHeight
            UIView.animate(withDuration: 0.1, delay: 0.1,
                           usingSpringWithDamping: 0.2,
                           initialSpringVelocity: 0.2,
                           options: [], animations: {
                            self.titleTopConstraint.constant = -100
                            
            }, completion: nil)
            self.updateHeader()
            self.view.layoutIfNeeded()
        })
    }
    
    func setScrollPosition(_ position: CGFloat) {
        self.tableView.contentOffset = CGPoint(x: self.tableView.contentOffset.x, y: position)
    }
    
    func updateHeader() {
        let range = self.maxHeaderHeight - self.minHeaderHeight
        let openAmount = self.headerHeightConstraint.constant - self.minHeaderHeight
        let percentage = openAmount / range
        UIView.animate(withDuration: 0.3, animations: {
            self.headerView.alpha = percentage
            println_debug("percentage value ::: \(percentage)")
            if percentage == 0 {
                UIView.animate(withDuration: 0.1, delay: 0.1,
                               usingSpringWithDamping: 0.2,
                               initialSpringVelocity: 0.2,
                               options: [], animations: {
                                self.titleTopConstraint.constant = -100
                                
                }, completion: nil)
            }else if percentage == 1 {
                UIView.animate(withDuration: 0.1, delay: 0.1,
                               usingSpringWithDamping: 0.2,
                               initialSpringVelocity: 0.2,
                               options: [], animations: {
                                self.titleTopConstraint.constant = 15
                                
                }, completion: nil)
            }
        })
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
