//
//  CashInOutDetailsInMapViewController.swift
//  OK
//
//  Created by Uma Rajendran on 11/30/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import MapKit
class CashInOutDetailsInMapViewController: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet var locationMapView: MKMapView!
    @IBOutlet weak var locationDetailsView: UIView!
    @IBOutlet weak var locationImageView: UIImageView!
    @IBOutlet weak var promotionNameLbl: MarqueeLabel!
    @IBOutlet weak var addFavoriteBtn: UIButton!
//    @IBOutlet weak var ratingImgView: UIImageView!
    @IBOutlet weak var contactNoLbl: UILabel!
    let regionRadius: CLLocationDistance = 1000
    let clusteringManager = FBClusteringManager()
    @IBOutlet weak var directionIcon: UIButton!
    var isFavorite : Bool = false
    var selectedCashInOut: CashInOutModel?
    
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var rateImgViewList: [UIImageView]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadUI()
        loadInitialize()
        updateLocalizations()
        
        if let cashdetail = selectedCashInOut {
            let initialLocation = CLLocation(latitude: (cashdetail.addressDetails?.address_Latitude)!, longitude: (cashdetail.addressDetails?.address_Longitude)!)
            centerMapOnLocation(location: initialLocation)
            addAnnotationsInMap()
            loadInfoView()
        }
    }
    
    func loadUI() {
        headerHeightConstraint.constant = kHeaderHeight
        headerView.backgroundColor = kYellowColor
        headerLbl.font = UIFont.init(name: appFont, size: 18)
        self.promotionNameLbl.font = UIFont.init(name: appFont, size: 14)
        self.promotionNameLbl.textColor = UIColor.black
        self.contactNoLbl.font = UIFont.init(name: appFont, size: 13)
        self.contactNoLbl.textColor = UIColor.black
//        ratingImgView.image = UIImage.init(named: "star_five")
        
        
        locationImageView.image = UIImage.init(named: "btc")
        directionIcon.layer.shadowColor = UIColor.darkGray.cgColor
        directionIcon.layer.shadowOpacity = 1
        directionIcon.layer.shadowOffset = CGSize.zero
        directionIcon.layer.shadowRadius = 4
        locationImageView.layer.shadowColor = kBackGroundGreyColor.cgColor
        locationImageView.layer.shadowOpacity = 1
        locationImageView.layer.shadowOffset = CGSize.zero
        locationImageView.layer.shadowRadius = 2
        if isFavorite {
            addFavoriteBtn.setImage(UIImage.init(named: "act_favorite"), for: UIControl.State.normal)
        }else {
            addFavoriteBtn.setImage(UIImage.init(named: "favorite"), for: UIControl.State.normal)
        }
    }
    
    
    func loadInitialize() {
        locationMapView.delegate = self
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(gestureRecognizer:)))
        gestureRecognizer.delegate = self
        locationDetailsView.addGestureRecognizer(gestureRecognizer)
    }
    
    func updateLocalizations() {
        
    }
    
    @objc func handleTap(gestureRecognizer: UIGestureRecognizer) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion.init(center: location.coordinate,
                                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        locationMapView.setRegion(coordinateRegion, animated: true)
    }
    
    func addAnnotationsInMap() {
        if let cashdetail = selectedCashInOut {
            if let lat =  cashdetail.addressDetails?.address_Latitude, let long = cashdetail.addressDetails?.address_Longitude {
                let annotation = FBAnnotation()
                annotation.coordinate = CLLocationCoordinate2DMake(lat, long)
                annotation.userData = cashdetail
                clusteringManager.removeAll()
                clusteringManager.add(annotations: [annotation])
            }
        }
        
    }
    
    func loadInfoView() {
        if let cashdetail = selectedCashInOut {
            headerLbl.text = "Ok Agent Directions".localized
            var personName = "Not available".localized
            if let name = cashdetail.cash_ContactPersonName {
                if name.count > 0 {
                    personName = name
                }
            }
            promotionNameLbl.text = personName
            var mobileNumber = "NA".localized
            if cashdetail.cash_ContactNumber!.count > 0 {
                mobileNumber = getPhoneNumberByFormat(phoneNumber: cashdetail.cash_ContactNumber!)
            }
            contactNoLbl.text = mobileNumber
            
            for index in 0 ... self.rateImgViewList.count - 1 {
                if index <  cashdetail.cash_Rating {
                    self.rateImgViewList[index].isHighlighted = true
                }else {
                    self.rateImgViewList[index].isHighlighted = false
                }
            }
            
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    
    @IBAction func addFavoriteBtnAction(_ sender: Any) {
        
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension CashInOutDetailsInMapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        DispatchQueue.main.async {
            let mapBoundsWidth = Double(self.locationMapView.bounds.size.width)
            let mapRectWidth = self.locationMapView.visibleMapRect.size.width
            let scale = mapBoundsWidth / mapRectWidth
            let annotationArray = self.clusteringManager.clusteredAnnotations(withinMapRect: self.locationMapView.visibleMapRect, zoomScale:scale)
            self.clusteringManager.display(annotations: annotationArray, onMapView:self.locationMapView)
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var reuseId = ""
        if annotation is FBAnnotationCluster {
            reuseId = "Cluster"
            var clusterView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
            if clusterView == nil {
                clusterView = FBAnnotationClusterView(annotation: annotation, reuseIdentifier: reuseId, configuration: FBAnnotationClusterViewConfiguration.default())
                
            } else {
                clusterView?.annotation = annotation
            }
            return clusterView
        } else {
            let annotationIdentifier = "SomeCustomIdentifier"
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
            if annotationView == nil {
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
                annotationView?.canShowCallout = false
                let pinImage = UIImage(named: "ok.png")
                annotationView?.image = pinImage
            }
            else {
                annotationView?.annotation = annotation
            }
            return annotationView
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annotation = view.annotation else { return }
        
        if let cluster = annotation as? FBAnnotationCluster {
            var zoomRect = MKMapRect.null
            for annotation in cluster.annotations {
               
                let annotationPoint = MKMapPoint.init(annotation.coordinate)
                let pointRect = MKMapRect.init(x: annotationPoint.x, y: annotationPoint.y, width: 0, height: 0)
                if zoomRect.isNull {
                    zoomRect = pointRect
                } else {
                    zoomRect = zoomRect.union(pointRect)
                }
            }
            mapView.setVisibleMapRect(zoomRect, animated: true)
        }else {
            if let eachAnnotation = annotation as? FBAnnotation {
                if let _ = eachAnnotation.userData as? CashInOutModel {
                     //println_debug("my data details :::: \(userdata.cash_ContactPersonName)  ,, \(userdata.addressDetails?.address_TownshipNameBurmese) ,, \(userdata.addressDetails?.address_DistanceInKilometer)")
                    //self.showPromotionInfoView(promotion: userdata)
                }
                
            }
        }
    }
    
}
