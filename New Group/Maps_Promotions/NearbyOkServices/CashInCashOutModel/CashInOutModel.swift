//
//  CashInOutModel.swift
//  OK
//
//  Created by Uma Rajendran on 11/29/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class CashInOutModel: NSObject, NSCoding {

    var cash_Id                             : String? = ""
    var cash_BusinessCategory               : String? = ""
    var cash_BusinessCategoryBurmese        : String? = ""
    var cash_BusinessImageUrls              : String? = ""
    var cash_BusinessName                   : String? = ""
    var cash_BusinessNameBurmese            : String? = ""
    var cash_BusinessSubCategory            : String? = ""
    var cash_BusinessSubCategoryBurmese     : String? = ""
    var cash_CashInCashOutType              : String? = ""
    var cash_ContactNumber                  : String? = ""
    var cash_ContactNumberBurmese           : String? = ""
    var cash_ContactPersonName              : String? = ""
    var cash_ContactPersonNameBurmese       : String? = ""
    var cash_FavouriteId                    : String? = ""
    var cash_LandLineNumber                 : String? = ""
    var cash_LandLineNumberBurmese          : String? = ""
    var cash_MinimumFee                     : String? = ""
    var cash_MobileNumber                   : String? = ""
    var cash_MobileNumberBurmese            : String? = ""
    var cash_Rating                         : Int = 0
    var cash_RatingPersonCount              : Int = 0
    var addressDetails : CashAddressModel?
    var settings : CashSettingsModel?
    var socialMedia : CashSocialMediaModel?
    var cash_UIDisplayName                  : String? = ""
    
    
    init(dictionary: Dictionary<String, AnyObject>) {
        super.init()
        
        if  let title : Dictionary<String,AnyObject> = dictionary["Address"] as? Dictionary<String,AnyObject> {
            if title.count  > 0 {
                addressDetails = CashAddressModel.init(dictionary: title)
            }
        }
        
        if let title = dictionary["Setting"] as? Dictionary<String, AnyObject> {
            if title.count > 0 {
                settings = CashSettingsModel.init(dictionary: title)
            }
        }
        if let title = dictionary["SocialMedia"] as? Dictionary<String, AnyObject> {
            if title.count > 0 {
                socialMedia = CashSocialMediaModel.init(dictionary: title)
            }
        }
        
        if let title = dictionary["Id"] {
            self.cash_Id = String(title.int64Value)
        }
        if let title = dictionary["BusinessCategory"] as? String {
            self.cash_BusinessCategory = title
        }
        if let title = dictionary["BusinessCategoryBurmese"] as? String {
            self.cash_BusinessCategoryBurmese = title
        }
        if let title = dictionary["BusinessImageUrls"] as? String {
            self.cash_BusinessImageUrls = title
        }
        if let title = dictionary["BusinessName"] as? String {
            self.cash_BusinessName = title
        }
        if let title = dictionary["BusinessNameBurmese"] as? String {
            self.cash_BusinessNameBurmese = title
        }
        if let title = dictionary["BusinessSubCategory"] as? String {
            self.cash_BusinessSubCategory = title
        }
        if let title = dictionary["BusinessSubCategoryBurmese"] as? String {
            self.cash_BusinessSubCategoryBurmese = title
        }
        if let title = dictionary["CashInCashOutType"] as? String {
            self.cash_CashInCashOutType = title
        }
        if let title = dictionary["ContactNumber"] as? String {
            self.cash_ContactNumber = title
        }
        if let title = dictionary["ContactNumberBurmese"] as? String {
            self.cash_ContactNumberBurmese = title
        }
        if let title = dictionary["ContactPersonName"] as? String {
            self.cash_ContactPersonName = title
        }
        if let title = dictionary["ContactPersonNameBurmese"] as? String {
            self.cash_ContactPersonNameBurmese = title
        }
        if let title = dictionary["FavouriteId"] as? String {
            self.cash_FavouriteId = title
        }
        if let title = dictionary["LandLineNumber"] as? String {
            self.cash_LandLineNumber = title
        }
        if let title = dictionary["LandLineNumberBurmese"] as? String {
            self.cash_LandLineNumberBurmese = title
        }
        if let title = dictionary["MinimumFee"]  {
            self.cash_MinimumFee =  String(format:"%d", (title as AnyObject).int64Value)
        }
        if let title = dictionary["MobileNumber"] as? String {
            self.cash_MobileNumber = title
        }
        if let title = dictionary["MobileNumberBurmese"] as? String {
            self.cash_MobileNumberBurmese = title
        }
        if let title = dictionary["Rating"] as? Int {
//            self.cash_Rating = String(format:"%d", (title as AnyObject).int64Value)
            self.cash_Rating = title
        }
        if let title = dictionary["RatingPersonCount"] as? Int   {
//            self.cash_RatingPersonCount = String(format:"%d", (title as AnyObject).int64Value)
            self.cash_RatingPersonCount = title

        }
        
        
        if (self.cash_CashInCashOutType?.count)! > 0 {
            if self.cash_CashInCashOutType?.lowercased() == "okagent" {
                if (self.cash_ContactPersonName?.count)! > 0 {
                    self.cash_UIDisplayName = self.cash_ContactPersonName!
                }
            }else if self.cash_CashInCashOutType?.lowercased() == "okoffice" {
                if (self.cash_BusinessName?.count)! > 0 {
                    self.cash_UIDisplayName = self.cash_BusinessName!
                }
            }else if self.cash_CashInCashOutType?.lowercased() == "bank" {
                if (self.cash_BusinessName?.count)! > 0 {
                    self.cash_UIDisplayName = self.cash_BusinessName!
                }
            }
        }
    }
 
    required init(coder aDecoder: NSCoder) {
        cash_Id = aDecoder.decodeObject(forKey: "cash_Id") as? String
        cash_BusinessCategory = aDecoder.decodeObject(forKey: "cash_BusinessCategory") as? String
        cash_BusinessCategoryBurmese = aDecoder.decodeObject(forKey: "cash_BusinessCategoryBurmese") as? String
        cash_BusinessImageUrls = aDecoder.decodeObject(forKey: "cash_BusinessImageUrls") as? String
        cash_BusinessName = aDecoder.decodeObject(forKey: "cash_BusinessName") as? String
        cash_BusinessNameBurmese = aDecoder.decodeObject(forKey: "cash_BusinessNameBurmese") as? String
        cash_BusinessSubCategory = aDecoder.decodeObject(forKey: "cash_BusinessSubCategory") as? String
        cash_BusinessSubCategoryBurmese = aDecoder.decodeObject(forKey: "cash_BusinessSubCategoryBurmese") as? String
        cash_CashInCashOutType = aDecoder.decodeObject(forKey: "cash_CashInCashOutType") as? String
        cash_ContactNumber = aDecoder.decodeObject(forKey: "cash_ContactNumber") as? String
        cash_ContactNumberBurmese = aDecoder.decodeObject(forKey: "cash_ContactNumberBurmese") as? String
        cash_ContactPersonName = aDecoder.decodeObject(forKey: "cash_ContactPersonName") as? String
        cash_ContactPersonNameBurmese = aDecoder.decodeObject(forKey: "cash_ContactPersonNameBurmese") as? String
        cash_FavouriteId = aDecoder.decodeObject(forKey: "cash_FavouriteId") as? String
        cash_LandLineNumber = aDecoder.decodeObject(forKey: "cash_LandLineNumber") as? String
        cash_LandLineNumberBurmese = aDecoder.decodeObject(forKey: "cash_LandLineNumberBurmese") as? String
        cash_MinimumFee = aDecoder.decodeObject(forKey: "cash_MinimumFee") as? String
        cash_MobileNumber = aDecoder.decodeObject(forKey: "cash_MobileNumber") as? String
        cash_MobileNumberBurmese =  aDecoder.decodeObject(forKey: "cash_MobileNumberBurmese") as? String
//        cash_Rating = (aDecoder.decodeInt64(forKey: "cash_Rating") as? Int)!
//        cash_RatingPersonCount = (aDecoder.decodeInt64(forKey: "cash_RatingPersonCount") as? Int)!

        cash_Rating = (aDecoder.decodeObject(forKey: "cash_Rating") as? Int)!
        cash_RatingPersonCount = (aDecoder.decodeObject(forKey: "cash_RatingPersonCount") as? Int)!
        addressDetails = aDecoder.decodeObject(forKey: "addressDetails") as? CashAddressModel
        settings = aDecoder.decodeObject(forKey: "settings") as? CashSettingsModel
        socialMedia = aDecoder.decodeObject(forKey: "socialMedia") as? CashSocialMediaModel
        cash_UIDisplayName = aDecoder.decodeObject(forKey: "cash_UIDisplayName") as? String

    }
    
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(cash_Id, forKey: "cash_Id")
        aCoder.encode(cash_BusinessCategory, forKey: "cash_BusinessCategory")
        aCoder.encode(cash_BusinessCategoryBurmese, forKey: "cash_BusinessCategoryBurmese")
        aCoder.encode(cash_BusinessImageUrls, forKey: "cash_BusinessImageUrls")
        aCoder.encode(cash_BusinessName, forKey: "cash_BusinessName")
        aCoder.encode(cash_BusinessNameBurmese, forKey: "cash_BusinessNameBurmese")
        aCoder.encode(cash_BusinessSubCategory, forKey: "cash_BusinessSubCategory")
        aCoder.encode(cash_BusinessSubCategoryBurmese, forKey: "cash_BusinessSubCategoryBurmese")
        aCoder.encode(cash_CashInCashOutType, forKey: "cash_CashInCashOutType")
        aCoder.encode(cash_ContactNumber, forKey: "cash_ContactNumber")
        aCoder.encode(cash_ContactNumberBurmese, forKey: "cash_ContactNumberBurmese")
        aCoder.encode(cash_ContactPersonName, forKey: "cash_ContactPersonName")
        aCoder.encode(cash_ContactPersonNameBurmese, forKey: "cash_ContactPersonNameBurmese")
        aCoder.encode(cash_FavouriteId, forKey: "cash_FavouriteId")
        aCoder.encode(cash_LandLineNumber, forKey: "cash_LandLineNumber")
        aCoder.encode(cash_LandLineNumberBurmese, forKey: "cash_LandLineNumberBurmese")
        aCoder.encode(cash_MinimumFee, forKey: "cash_MinimumFee")
        aCoder.encode(cash_MobileNumber, forKey: "cash_MobileNumber")
        aCoder.encode(cash_MobileNumberBurmese, forKey: "cash_MobileNumberBurmese")
        aCoder.encode(cash_Rating, forKey: "cash_Rating")
        aCoder.encode(cash_RatingPersonCount, forKey: "cash_RatingPersonCount")
        aCoder.encode(addressDetails, forKey: "addressDetails")
        aCoder.encode(settings, forKey: "settings")
        aCoder.encode(socialMedia, forKey: "socialMedia")
        aCoder.encode(cash_UIDisplayName, forKey: "cash_UIDisplayName")
    }
}

class CashAddressModel: NSObject, NSCoding {
    var address_Address1                     :String?    = "NA"
    var address_AddressBurmese1              :String?    = "NA"
    var address_Address2                     :String?    = "NA"
    var address_AddressBurmese2              :String?    = "NA"
    var address_CityName                    :String?    = ""
    var address_CityNameBurmese             :String?    = ""
    var address_DistanceInKilometer         :Double?
    var address_Latitude                    :Double?
    var address_Longitude                   :Double?
    var address_StateName                   :String?    = ""
    var address_StateNameBurmese            :String?    = ""
    var address_TownshipName                :String?    = ""
    var address_TownshipNameBurmese         :String?    = ""
    
    init(dictionary: Dictionary<String, AnyObject>) {
        super.init()
        if let title = dictionary["AddressLine1"] as? String {
            self.address_Address1 = title
        }
        if let title = dictionary["AddressLine1Burmese"] as? String {
            self.address_AddressBurmese1 = title
        }
        if let title = dictionary["AddressLine2"] as? String {
            self.address_Address2 = title
        }
        if let title = dictionary["AddressLine2Burmese"] as? String {
            self.address_AddressBurmese2 = title
        }
        if let title = dictionary["CityName"] as? String {
            self.address_CityName = title
        }
        if let title = dictionary["CityNameBurmese"] as? String {
            self.address_CityNameBurmese = title
        }
        if let title = dictionary["DistanceInKilometer"] as? Double {
            self.address_DistanceInKilometer = title
        }
        if let title = dictionary["Latitude"] as? Double{
            self.address_Latitude =  title
        }
        if let title = dictionary["Longitude"] as? Double{
            self.address_Longitude = title
        }
        if let title = dictionary["StateName"] as? String {
            self.address_StateName = title
        }
        if let title = dictionary["StateNameBurmese"] as? String {
            self.address_StateNameBurmese = title
        }
        if let title = dictionary["TownshipName"] as? String {
            self.address_TownshipName = title
        }
        if let title = dictionary["TownshipNameBurmese"] as? String {
            self.address_TownshipNameBurmese = title
        }
     }
    
    required init(coder aDecoder: NSCoder) {
        address_Address1 = aDecoder.decodeObject(forKey: "address_Address1") as? String
        address_AddressBurmese1 = aDecoder.decodeObject(forKey: "address_AddressBurmese1") as? String
        address_Address2 = aDecoder.decodeObject(forKey: "address_Address2") as? String
        address_AddressBurmese2 = aDecoder.decodeObject(forKey: "address_AddressBurmese2") as? String
        address_CityName = aDecoder.decodeObject(forKey: "address_CityName") as? String
        address_CityNameBurmese = aDecoder.decodeObject(forKey: "address_CityNameBurmese") as? String
        address_DistanceInKilometer = aDecoder.decodeObject(forKey: "address_DistanceInKilometer") as? Double
        address_Latitude = aDecoder.decodeObject(forKey: "address_Latitude") as? Double
        address_Longitude = aDecoder.decodeObject(forKey: "address_Longitude") as? Double
        address_StateName = aDecoder.decodeObject(forKey: "address_StateName") as? String
        address_StateNameBurmese = aDecoder.decodeObject(forKey: "address_StateNameBurmese") as? String
        address_TownshipName = aDecoder.decodeObject(forKey: "address_TownshipName") as? String
        address_TownshipNameBurmese = aDecoder.decodeObject(forKey: "address_TownshipNameBurmese") as? String
    }
 
    func encode(with aCoder: NSCoder) {
        aCoder.encode(address_Address1, forKey: "address_Address1")
        aCoder.encode(address_AddressBurmese1, forKey: "address_AddressBurmese1")
        aCoder.encode(address_Address2, forKey: "address_Address2")
        aCoder.encode(address_AddressBurmese2, forKey: "address_AddressBurmese2")
        aCoder.encode(address_CityName, forKey: "address_CityName")
        aCoder.encode(address_CityNameBurmese, forKey: "address_CityNameBurmese")
        aCoder.encode(address_DistanceInKilometer, forKey: "address_DistanceInKilometer")
        aCoder.encode(address_Latitude, forKey: "address_Latitude")
        aCoder.encode(address_Longitude, forKey: "address_Longitude")
        aCoder.encode(address_StateName, forKey: "address_StateName")
        aCoder.encode(address_StateNameBurmese, forKey: "address_StateNameBurmese")
        aCoder.encode(address_TownshipName, forKey: "address_TownshipName")
        aCoder.encode(address_TownshipNameBurmese, forKey: "address_TownshipNameBurmese")
    }
}

class CashSettingsModel: NSObject, NSCoding {
    var setting_CashInService               : String? = ""
    var setting_CashOutRate                 : String? = ""
    var setting_CashOutService              : String? = ""
    var setting_IsOpen                      : Bool = false
    var setting_MaxAmount                   : String? = ""
    var setting_MinAmount                   : String? = ""
    var setting_OkAccountPhoneNumber        : String? = ""
    var setting_Remark                      : String? = ""
    var setting_WorkingDays                 : String? = ""
    var setting_WorkingDaysBurmese          : String? = ""
    var setting_WorkingHour                 : String? = ""
    var setting_WorkingHourBurmese          : String? = ""
    
    init(dictionary: Dictionary<String, AnyObject>) {
        super.init()
        if let title = dictionary["CashInService"] as? String {
            self.setting_CashInService = title
        }
        if let title = dictionary["CashOutRate"] as? String {
            self.setting_CashOutRate = title
        }
        if let title = dictionary["CashOutService"] as? String {
            self.setting_CashOutService = title
        }
        if let title = dictionary["IsOpen"] as? Bool {
            self.setting_IsOpen = title
        }
        if let title = dictionary["MaxAmount"] as? String {
            self.setting_MaxAmount = title
        }
        if let title = dictionary["MinAmount"] as? String {
            self.setting_MinAmount = title
        }
        if let title = dictionary["OkAccountPhoneNumber"] as? String {
            self.setting_OkAccountPhoneNumber = title
        }
        if let title = dictionary["Remark"] as? String {
            self.setting_Remark = title
        }
        if let title = dictionary["WorkingDays"] as? String {
            self.setting_WorkingDays = title
        }
        if let title = dictionary["WorkingDaysBurmese"] as? String {
            self.setting_WorkingDaysBurmese = title
        }
        if let title = dictionary["WorkingHour"] as? String {
            self.setting_WorkingHour = title
        }
        if let title = dictionary["WorkingHourBurmese"] as? String {
            self.setting_WorkingHourBurmese = title
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        setting_CashInService = aDecoder.decodeObject(forKey: "setting_CashInService") as? String
        setting_CashOutRate = aDecoder.decodeObject(forKey: "setting_CashOutRate") as? String
        setting_CashOutService = aDecoder.decodeObject(forKey: "setting_CashOutService") as? String
        setting_IsOpen = aDecoder.decodeBool(forKey: "setting_IsOpen")
        setting_MaxAmount = aDecoder.decodeObject(forKey: "setting_MaxAmount") as? String
        setting_MinAmount = aDecoder.decodeObject(forKey: "setting_MinAmount") as? String
        setting_OkAccountPhoneNumber = aDecoder.decodeObject(forKey: "setting_OkAccountPhoneNumber") as? String
        setting_Remark = aDecoder.decodeObject(forKey: "setting_Remark") as? String
        setting_WorkingDays = aDecoder.decodeObject(forKey: "setting_WorkingDays") as? String
        setting_WorkingDaysBurmese = aDecoder.decodeObject(forKey: "setting_WorkingDaysBurmese") as? String
        setting_WorkingHour = aDecoder.decodeObject(forKey: "setting_WorkingHour") as? String
        setting_WorkingHourBurmese = aDecoder.decodeObject(forKey: "setting_WorkingHourBurmese") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(setting_CashInService, forKey: "setting_CashInService")
        aCoder.encode(setting_CashOutRate, forKey: "setting_CashOutRate")
        aCoder.encode(setting_IsOpen, forKey: "setting_IsOpen")
        aCoder.encode(setting_MaxAmount, forKey: "setting_MaxAmount")
        aCoder.encode(setting_MinAmount, forKey: "setting_MinAmount")
        aCoder.encode(setting_OkAccountPhoneNumber, forKey: "setting_OkAccountPhoneNumber")
        aCoder.encode(setting_Remark, forKey: "setting_Remark")
        aCoder.encode(setting_WorkingDays, forKey: "setting_WorkingDays")
        aCoder.encode(setting_WorkingDaysBurmese, forKey: "setting_WorkingDaysBurmese")
        aCoder.encode(setting_WorkingHour, forKey: "setting_WorkingHour")
        aCoder.encode(setting_WorkingHourBurmese, forKey: "setting_WorkingHourBurmese")
     }
    
}

class CashSocialMediaModel: NSObject, NSCoding {
    var media_EmailAddress                  : String? = ""
    var media_Facebook                      : String? = ""
    var media_Viber                         : String? = ""
    var media_WhatsApp                      : String? = ""
    
    init(dictionary: Dictionary<String, AnyObject>) {
        super.init()
        if let title = dictionary["EmailAddress"] as? String {
            self.media_EmailAddress = title
        }
        if let title = dictionary["Facebook"] as? String {
            self.media_Facebook = title
        }
        if let title = dictionary["Viber"] as? String {
            self.media_Viber = title
        }
        if let title = dictionary["WhatsApp"] as? String {
            self.media_WhatsApp = title
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        media_EmailAddress = aDecoder.decodeObject(forKey: "media_EmailAddress") as? String
        media_Facebook = aDecoder.decodeObject(forKey: "media_Facebook") as? String
        media_Viber = aDecoder.decodeObject(forKey: "media_Viber") as? String
        media_WhatsApp = aDecoder.decodeObject(forKey: "media_WhatsApp") as? String
     }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(media_EmailAddress, forKey: "media_EmailAddress")
        aCoder.encode(media_Facebook, forKey: "media_Facebook")
        aCoder.encode(media_Viber, forKey: "media_Viber")
        aCoder.encode(media_WhatsApp, forKey: "media_WhatsApp")
    }
}
