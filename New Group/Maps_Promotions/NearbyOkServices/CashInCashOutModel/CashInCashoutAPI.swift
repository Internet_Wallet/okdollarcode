//
//  CashInCashoutAPI.swift
//  OK
//
//  Created by Uma Rajendran on 8/3/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import Foundation


let kCash_In = "http://test.cashincashout.okdollar.org/CashIn/"

let kCash_Out = "http://test.cashincashout.okdollar.org/CashOut/"

let kCashIn_AllListByDivision = kCash_In + "allLists/divisions/"

let kCashIn_AllListByTownship = kCash_In + "allLists/townships/"

let kCashIn_AllListByLocation = kCash_In + "allLists/locations/"

let kCashIn_AllBanksListByDivision = kCash_In + "banks/divisions/"

let kCashIn_AllBanksListByTownship = kCash_In + "banks/townships/"

let kCashIn_AllBanksListByLocation = kCash_In + "banks/locations/"

let kCashIn_AllOkAgentsByDivision = kCash_In + "agents/divisions/"

let kCashIn_AllOkAgentsByTownship = kCash_In + "agents/townships/"

let kCashIn_AllOkAgentsByLocation = kCash_In + "agents/locations/"

let kCashIn_AllOkOfficeByDivision = kCash_In + "offices/divisions/"

let kCashIn_AllOkOfficeByTownship = kCash_In + "offices/townships/"

let kCashIn_AllOkOfficeByLocation = kCash_In + "offices/locations/"


let kCashOut_AllListByDivision = kCash_Out + "allLists/divisions/"

let kCashOut_AllListByTownship = kCash_Out + "allLists/townships/"

let kCashOut_AllListByLocation = kCash_Out + "allLists/locations/"

let kCashOut_AllBanksListByDivision = kCash_Out + "banks/divisions/"

let kCashOut_AllBanksListByTownship = kCash_Out + "banks/townships/"

let kCashOut_AllBanksListByLocation = kCash_Out + "banks/locations/"

let kCashOut_AllOkAgentsByDivision = kCash_Out + "agents/divisions/"

let kCashOut_AllOkAgentsByTownship = kCash_Out + "agents/townships/"

let kCashOut_AllOkAgentsByLocation = kCash_Out + "agents/locations/"

let kCashOut_AllOkOfficeByDivision = kCash_Out + "offices/divisions/"

let kCashOut_AllOkOfficeByTownship = kCash_Out + "offices/townships/"

let kCashOut_AllOkOfficeByLocation = kCash_Out + "offices/locations/"

let kCashInCashOut_TokenAPI = "http://test.cashincashout.okdollar.org/token"

let kCheckNewRegistration = "https://www.okdollar.co/RestService.Svc/CheckRegistrationStatus?"

let kSecurityQuestionAPI = "https://www.okdollar.co/RestService.Svc/ViewSecurityQuestions?"

let kReverifyAPI = "https://www.okdollar.co/RestService.svc/ReVerify"

class CashInCashoutAPI: NSObject {
    
    var phoneNumber: String?
    var divisionName: String?
    var divisionCode: String?
    var townshipName: String?
    var townshipCode: String?
    var curLatitude: String?
    var curLongitude: String?
    
    /*
     func getCashInApi(cashInType: CashIn, locationType: Location) -> String{
    
        var apiString = ""
        switch cashInType {
        case .okoffice:
            switch locationType {
            case .byDivision:
                apiString = kCashIn_AllOkOfficeByDivision + "\(divisionName!)/phoneNumber/\(phoneNumber!)"
            case .byTownship:
                apiString = kCashIn_AllOkOfficeByTownship + "\(townshipName!)/phoneNumber/\(phoneNumber!)"

            case .byCurrentLocation:
                apiString = kCashIn_AllOkOfficeByLocation + "Latitude/\(curLatitude!)/Longitude/\(curLongitude!)/phoneNumber/\(phoneNumber!)"

            case .bySearch:
                break
            case .byCategory:
                break
            }
        case .okagents:
            switch locationType {
            case .byDivision:
                apiString = kCashIn_AllOkAgentsByDivision + "\(divisionName!)/phoneNumber/\(phoneNumber!)"

            case .byTownship:
                apiString = kCashIn_AllOkAgentsByTownship + "\(townshipName!)/phoneNumber/\(phoneNumber!)"
            case .byCurrentLocation:
                apiString = kCashIn_AllOkAgentsByLocation + "Latitude/\(curLatitude!)/Longitude/\(curLongitude!)/phoneNumber/\(phoneNumber!)"
            case .bySearch:
                break
            case .byCategory:
                break
            }
        case .okservicecounter:
            switch locationType {
            case .byDivision:
                apiString = kCashIn_AllBanksListByDivision + "\(divisionName!)/phoneNumber/\(phoneNumber!)"

            case .byTownship:
                apiString = kCashIn_AllBanksListByTownship + "\(townshipName!)/phoneNumber/\(phoneNumber!)"
            case .byCurrentLocation:
                apiString = kCashIn_AllBanksListByLocation + "Latitude/\(curLatitude!)/Longitude/\(curLongitude!)/phoneNumber/\(phoneNumber!)"
            case .bySearch:
                break
            case .byCategory:
                break
            }
        case .allcashin:
            switch locationType {
            case .byDivision:
                apiString = kCashIn_AllListByDivision + "\(divisionName!)/phoneNumber/\(phoneNumber!)"

            case .byTownship:
                apiString = kCashIn_AllListByTownship + "\(townshipName!)/phoneNumber/\(phoneNumber!)"
            case .byCurrentLocation:
                apiString = kCashIn_AllListByLocation + "Latitude/\(curLatitude!)/Longitude/\(curLongitude!)/phoneNumber/\(phoneNumber!)"
            case .bySearch:
                break
            case .byCategory:
                break
            }
            
        }
        return apiString
    }
    
    
    
    func getCashOutApi(cashOutType: CashOut, locationType: Location) -> String {
        var apiString = ""
        switch cashOutType {
        case .okoffice:
            switch locationType {
            case .byDivision:
                apiString = kCashOut_AllOkOfficeByDivision + "\(divisionName!)/phoneNumber/\(phoneNumber!)"
            case .byTownship:
                apiString = kCashOut_AllOkOfficeByTownship + "\(townshipName!)/phoneNumber/\(phoneNumber!)"
                
            case .byCurrentLocation:
                apiString = kCashOut_AllOkOfficeByLocation + "Latitude/\(curLatitude!)/Longitude/\(curLongitude!)/phoneNumber/\(phoneNumber!)"
                
            case .bySearch:
                break
            case .byCategory:
                break
            }
        case .okagents:
            switch locationType {
            case .byDivision:
                apiString = kCashOut_AllOkAgentsByDivision + "\(divisionName!)/phoneNumber/\(phoneNumber!)"
                
            case .byTownship:
                apiString = kCashOut_AllOkAgentsByTownship + "\(townshipName!)/phoneNumber/\(phoneNumber!)"
            case .byCurrentLocation:
                apiString = kCashOut_AllOkAgentsByLocation + "Latitude/\(curLatitude!)/Longitude/\(curLongitude!)/phoneNumber/\(phoneNumber!)"
            case .bySearch:
                break
            case .byCategory:
                break
            }
        case .okservicecounter:
            switch locationType {
            case .byDivision:
                apiString = kCashOut_AllBanksListByDivision + "\(divisionName!)/phoneNumber/\(phoneNumber!)"
                
            case .byTownship:
                apiString = kCashOut_AllBanksListByTownship + "\(townshipName!)/phoneNumber/\(phoneNumber!)"
            case .byCurrentLocation:
                apiString = kCashOut_AllBanksListByLocation + "Latitude/\(curLatitude!)/Longitude/\(curLongitude!)/phoneNumber/\(phoneNumber!)"
            case .bySearch:
                break
            case .byCategory:
                break
            }
        case .allcashout:
            switch locationType {
            case .byDivision:
                apiString = kCashOut_AllListByDivision + "\(divisionName!)/phoneNumber/\(phoneNumber!)"
                
            case .byTownship:
                apiString = kCashOut_AllListByTownship + "\(townshipName!)/phoneNumber/\(phoneNumber!)"
            case .byCurrentLocation:
                apiString = kCashOut_AllListByLocation + "Latitude/\(curLatitude!)/Longitude/\(curLongitude!)/phoneNumber/\(phoneNumber!)"
            case .bySearch:
                break
            case .byCategory:
                break
            }
            
        }
        return apiString
        
    }
    */
    
}


class CashApiDetails : NSObject {
    
var phoneNumber: String?
var divisionName: String?
var divisionCode: String?
var townshipName: String?
var townshipCode: String?
var currentLatitude: String?
var currentLongitude: String?

}
 
