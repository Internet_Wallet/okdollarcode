//
//  CashInOutListViewController.swift
//  OK
//
//  Created by Uma Rajendran on 11/28/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class CashInOutListViewController: UIViewController {
    
    // maplistcell have to use in this tableview
    
    @IBOutlet weak var listTableView: UITableView!
    var cashInCashOutList = [CashInOutModel]()
    var nav : UINavigationController?
    @IBOutlet weak var infoLabel: UILabel!
    var currentNearbyView: NearByView?
    var listDelegate: CashListDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadUI()
        loadInitialize()
        updateLocalizations()
    }
    
    func loadUI() {
        println_debug("list view frame :::: \(self.view.frame.origin.x) ,, \(self.view.frame.origin.y) ,, \(self.view.frame.size.width) ,, \(self.view.frame.size.height)")
    }
    
    func loadInitialize() {
        let nib = UINib.init(nibName: "CashInOutTableViewCell", bundle: nil)
        listTableView.register(nib, forCellReuseIdentifier: "cashinlistcellidentifier")
        listTableView.tableFooterView = UIView(frame: CGRect.zero)
        listTableView.rowHeight = UITableView.automaticDimension
        listTableView.estimatedRowHeight = 250
        listTableView.reloadData()
    }
    
    func updateLocalizations() {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showHideInfoLabel(isShow: Bool, content: String) {
        DispatchQueue.main.async {
            if isShow {
                self.infoLabel.isHidden = false
                self.infoLabel.text = content
            }else {
                self.infoLabel.isHidden = true
            }
        }
    }
    
    func getTime(timeStr: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let time = dateFormatter.date(from: timeStr)
        dateFormatter.dateFormat = "hh:mm a"
        let hour = dateFormatter.string(from: time!)
        return hour
    }
    
    @objc func callBtnAction(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to: self.listTableView)
        if let indexPath = self.listTableView.indexPathForRow(at: buttonPosition), cashInCashOutList.count > 0 {
            let selectedcashdetail = cashInCashOutList[indexPath.row]
            let cashinout = selectedcashdetail
            if let phonenumber = cashinout.cash_ContactNumber {
                if phonenumber.count > 0 {
                    var mobNumber = getPhoneNumberByFormat(phoneNumber: phonenumber)
                    mobNumber = mobNumber.removingWhitespaces()
                    let phone = "tel://\(mobNumber)"
                    if let url = URL.init(string: phone) {
                        UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                    }
                }else {
                    showNoPhoneNumberError()
                }
            }else {
                showNoPhoneNumberError()
            }
        }else {
            showNoPhoneNumberError()
        }
    }
    
    func showNoPhoneNumberError() {
        alertViewObj.wrapAlert(title: nil, body: "No Phonenumber Exists".localized, img: nil)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        alertViewObj.showAlert(controller: self)
    }
    
  
    
}

extension CashInOutListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cashInCashOutList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let locationCell = tableView.dequeueReusableCell(withIdentifier: "cashinlistcellidentifier", for: indexPath) as! CashInOutTableViewCell
        let cashInOut = cashInCashOutList[indexPath.row] as CashInOutModel
        locationCell.callBtn.addTarget(self, action: #selector(callBtnAction(_:)), for: UIControl.Event.touchUpInside)
        locationCell.photoImgView.image = UIImage.init(named: "btc")
//        locationCell.agentNameLbl.text = self.isBurmese() ? cashInOut.cash_BusinessNameBurmese : cashInOut.cash_BusinessName
        if currentNearbyView == .okAgentsView {
            if appDel.currentLanguage == "my" {
                locationCell.agentNameLbl.text = cashInOut.cash_ContactPersonNameBurmese
            }
            else
            {
                locationCell.agentNameLbl.text = cashInOut.cash_ContactPersonName
            }
        }else {
            if appDel.currentLanguage == "my" {
            locationCell.agentNameLbl.text = cashInOut.cash_BusinessNameBurmese
            }
            else{
                locationCell.agentNameLbl.text = cashInOut.cash_BusinessName
            }
        }

        for index in 0 ... locationCell.ratingImgViewCollection.count - 1 {
             if index <  cashInOut.cash_Rating {
                locationCell.ratingImgViewCollection[index].isHighlighted = true
            }else {
                locationCell.ratingImgViewCollection[index].isHighlighted = false
                
            }
        }

        var distance = "0 Km".localized
        if let shopDistance = cashInOut.addressDetails?.address_DistanceInKilometer {
            distance = String(format: "%.2f Km", shopDistance)
        }
        locationCell.distanceLbl.text = distance
        
        var location = ""
//        if isBurmese() {
//            location = "\(String(describing: cashInOut.addressDetails!.address_TownshipNameBurmese)) (\(String(describing: cashInOut.addressDetails!.address_StateNameBurmese)))"
//        }else {
            //location = "\(cashInOut.addressDetails!.address_TownshipName!)"
//        }
        
        if appDel.currentLanguage == "my" {
            location = "\(cashInOut.addressDetails!.address_TownshipNameBurmese!)"
        }
        else
        {
           location = "\(cashInOut.addressDetails!.address_TownshipName!)"
        }
    
        locationCell.locationNameLbl.text = location.count > 0 ? location : "NA".localized
        locationCell.timeLbl.text = (cashInOut.settings?.setting_WorkingHour?.count)! > 0 ? cashInOut.settings?.setting_WorkingHour : "NA".localized
        locationCell.amountLbl.text = cashInOut.cash_MinimumFee!.count > 0 ? "\(String(describing: cashInOut.cash_MinimumFee!)) MMK" :  "0 MMK"
        locationCell.locationImgV.image = UIImage.init(named: "location.png")
        locationCell.timeImgV.image = UIImage.init(named: "time")
        return locationCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cashInOut = cashInCashOutList[indexPath.row] as CashInOutModel
        let storyboard: UIStoryboard = UIStoryboard(name: "NearbyOkServices", bundle: nil)
        let cashDetailsView = storyboard.instantiateViewController(withIdentifier: "CashInOutDetailView_ID") as! CashInOutDetailsViewController
        cashDetailsView.selectedCashInOut = cashInOut
        if currentNearbyView == .okAgentsView {
            cashDetailsView.currentCashInOutType = .cashin_allagents
        }else {
            cashDetailsView.currentCashInOutType = .cashin_alloffices
        }
        nav?.pushViewController(cashDetailsView, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight = 250
        tableView.rowHeight = UITableView.automaticDimension
        return tableView.rowHeight
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        println_debug("scrollview will begin dragging")
        listDelegate?.resignParentSearchKeyboard()
    }
   
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
