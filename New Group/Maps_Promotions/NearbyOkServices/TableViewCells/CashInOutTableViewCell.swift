//
//  CashInOutTableViewCell.swift
//  OK
//
//  Created by Uma Rajendran on 11/30/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class CashInOutTableViewCell: UITableViewCell {

    @IBOutlet weak var photoImgView: UIImageView!
    @IBOutlet var agentNameLbl: MarqueeLabel!
    @IBOutlet var ratingImgV: UIImageView!
    @IBOutlet var distanceLbl: UILabel!
    @IBOutlet var locationImgV: UIImageView!
    @IBOutlet var locationNameLbl: UILabel!
    @IBOutlet var callBtn: UIButton!
    @IBOutlet var amountLbl: UILabel!
    @IBOutlet var timeImgV: UIImageView!
    @IBOutlet var timeLbl: UILabel!
    @IBOutlet var ratingImgViewCollection: [UIImageView]!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        setUpMarqueeLabel(label: self.agentNameLbl)
        locationNameLbl.font = UIFont.init(name: appFont, size: 15)
        timeLbl.font = UIFont.init(name: appFont, size: 14)
        distanceLbl.font = UIFont.init(name: appFont, size: 12)
        amountLbl.font = UIFont.init(name: appFont, size: 13)
        
        agentNameLbl.textColor = UIColor.black
        locationNameLbl.textColor = UIColor.darkGray
        timeLbl.textColor = UIColor.darkGray
        amountLbl.textColor = UIColor.red
        distanceLbl.textColor = UIColor.darkGray
        
        photoImgView.layer.shadowColor = kBackGroundGreyColor.cgColor
        photoImgView.layer.shadowOpacity = 1
        photoImgView.layer.shadowOffset = CGSize.zero
        photoImgView.layer.shadowRadius = 2
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setUpMarqueeLabel(label: MarqueeLabel) {
        label.tag = 501
        label.type = .continuous
        label.speed = .duration(20)
        label.fadeLength = 10.0
        label.trailingBuffer = 30.0
        label.font = UIFont.init(name: appFont, size: 16)
        label.isUserInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(pauseTap))
        tapRecognizer.numberOfTapsRequired = 1
        tapRecognizer.numberOfTouchesRequired = 1
        label.addGestureRecognizer(tapRecognizer)
    }
    
    @objc func pauseTap(_ recognizer: UIGestureRecognizer) {
        let continuousLabel2 = recognizer.view as! MarqueeLabel
        if recognizer.state == .ended {
            continuousLabel2.isPaused ? continuousLabel2.unpauseLabel() : continuousLabel2.pauseLabel()
        }
    }

}
