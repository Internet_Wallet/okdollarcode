//
//  ContactPickersPicker.swift
//  ContactPickers
//
//  Created by Prabaharan Elangovan on 12/10/15.
//  Copyright © 2015 Prabaharan Elangovan. All rights reserved.
//

import UIKit
import Contacts

class ContactNavigationController : UINavigationController {}

public protocol ContactPickerDelegate: class {
	func contact(_: ContactPickersPicker, didContactFetchFailed error: NSError)
    func contact(_: ContactPickersPicker, didCancel error: NSError)
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker)
	func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker])
}

public extension ContactPickerDelegate {
	func contact(_: ContactPickersPicker, didContactFetchFailed error: NSError) { }
	func contact(_: ContactPickersPicker, didCancel error: NSError) { }
	func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) { }
	func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]) { }
}

typealias ContactsHandler = (_ contacts : [CNContact] , _ error : NSError?) -> Void

public enum SubtitleCellValue{
    
    case phoneNumber
    case email
    case birthday
    case organization
    
}

open class ContactPickersPicker: UITableViewController, UISearchResultsUpdating, UISearchBarDelegate {
    
    // MARK: - Properties
    open weak var contactDelegate: ContactPickerDelegate?
    var contactsStore: CNContactStore?
    var resultSearchController = UISearchController()
    var orderedContacts = [String: [CNContact]]() //Contacts ordered in dicitonary alphabetically
    var sortedContactKeys = [String]()
    
    var selectedContacts = [ContactPicker]()
    var filteredContacts = [CNContact]()
    
    var subtitleCellValue = SubtitleCellValue.phoneNumber
    var multiSelectEnabled: Bool = false //Default is single selection contact
    
    // MARK: - Lifecycle Methods
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        registerContactCell()
        setUpNavigation()
        //inititlizeBarButtons()
        initializeSearchBar()
        reloadContacts()
    }
    
    func setUpNavigation() {
        
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(onTouchCancelButton), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        if multiSelectEnabled {
            let doneButton = UIButton(type: .custom)
            doneButton.setTitle("Done".localized, for: .normal)
            //doneButton.setImage(#imageLiteral(resourceName: "reportSearch").withRenderingMode(.alwaysOriginal), for: .normal)
            doneButton.addTarget(self, action: #selector(onTouchDoneButton), for: .touchUpInside)
            doneButton.frame = CGRect(x: navTitleView.frame.size.width - 60, y: ypos, width: 30, height: 30)
            navTitleView.addSubview(doneButton)
        }
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 140, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  ContactGlobalConstants.Strings.contactsTitle + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: "Zawgyi-One", size: 21.0) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
    
    func initializeSearchBar() {
        
        self.resultSearchController = ( {
            
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.hidesNavigationBarDuringPresentation = false
            controller.searchBar.sizeToFit()
            controller.searchBar.delegate = self
            controller.searchBar.barTintColor = kYellowColor
            controller.searchBar.keyboardType = .default
            controller.searchBar.autocorrectionType = .default
            self.tableView.tableHeaderView = controller.searchBar
            return controller
            
        })()
        
    }
    
    func inititlizeBarButtons() {
        
        let buttonIcon      = UIImage(named: "tabBarBack")
        let leftBarButton   = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.done, target: self, action: #selector(onTouchCancelButton))
        leftBarButton.image = buttonIcon
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        if multiSelectEnabled {
            let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(onTouchDoneButton))
            self.navigationItem.rightBarButtonItem = doneButton
        }
        
    }
    
    fileprivate func registerContactCell() {
        
        let podBundle = Bundle(for: self.classForCoder)
        if let bundleURL = podBundle.url(forResource: ContactGlobalConstants.Strings.bundleIdentifier, withExtension: "bundle") {
            if let bundle = Bundle(url: bundleURL) {
                let cellNib = UINib(nibName: ContactGlobalConstants.Strings.cellNibIdentifier, bundle: bundle)
                tableView.register(cellNib, forCellReuseIdentifier: "Cell")
            }
            else {
                assertionFailure("Could not load bundle")
            }
        }
        else {
            // Cell loafing changes according to cell. if any crashes come then put bundle as nil.
            let cellNib = UINib(nibName: ContactGlobalConstants.Strings.cellNibIdentifier, bundle: Bundle.main)
            tableView.register(cellNib, forCellReuseIdentifier: "Cell")
        }
        
    }

    override open func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Initializers
    convenience public init(delegate: ContactPickerDelegate?) {
        
        self.init(delegate: delegate, multiSelection: false)
    }
    
    convenience public init(delegate: ContactPickerDelegate?, multiSelection : Bool) {
        
        self.init(style: .plain)
        self.multiSelectEnabled = multiSelection
        contactDelegate = delegate
    }

    convenience public init(delegate: ContactPickerDelegate?, multiSelection : Bool, subtitleCellType: SubtitleCellValue) {
        
        self.init(style: .plain)
        self.multiSelectEnabled = multiSelection
        contactDelegate = delegate
        subtitleCellValue = subtitleCellType
        
    }
    
    // MARK: - Contact Operations
      open func reloadContacts() {
        getContacts( {(contacts, error) in
            if (error == nil) {
                DispatchQueue.main.async(execute: {
                    self.tableView.reloadData()
                })
            }
        })
      }
  
    func getContacts(_ completion:  @escaping ContactsHandler) {
        if contactsStore == nil {
            //ContactStore is control for accessing the Contacts
            contactsStore = CNContactStore()
        }
        let error = NSError(domain: "ContactPickerPickerErrorDomain", code: 1, userInfo: [NSLocalizedDescriptionKey: "No Contacts Access"])
        
        switch CNContactStore.authorizationStatus(for: CNEntityType.contacts) {
            case CNAuthorizationStatus.denied, CNAuthorizationStatus.restricted:
                //User has denied the current app to access the contacts.
                
                let productName = Bundle.main.infoDictionary!["CFBundleName"]!
                
                let alert = UIAlertController(title: "Unable to access contacts", message: "\(productName) does not have access to contacts. Kindly enable it in privacy settings ", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK".localized, style: UIAlertActionStyle.default, handler: {  action in
                    completion([], error)
                    self.dismiss(animated: true, completion: {
                        self.contactDelegate?.contact(self, didContactFetchFailed: error)
                    })
                })
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            
            case CNAuthorizationStatus.notDetermined:
                //This case means the user is prompted for the first time for allowing contacts
                contactsStore?.requestAccess(for: CNEntityType.contacts, completionHandler: { (granted, error) -> Void in
                    //At this point an alert is provided to the user to provide access to contacts. This will get invoked if a user responds to the alert
                    if  (!granted ) {
                        DispatchQueue.main.async(execute: { () -> Void in
                            completion([], error! as NSError?)
                        })
                    }
                    else {
                        self.getContacts(completion)
                    }
                })
            case  CNAuthorizationStatus.authorized:
                //Authorization granted by user for this app.
                var contactsArray = [CNContact]()
                let contactFetchRequest = CNContactFetchRequest(keysToFetch: allowedContactKeys())
                do {
                    try contactsStore?.enumerateContacts(with: contactFetchRequest, usingBlock: { (contact, stop) -> Void in
                        //Ordering contacts based on alphabets in firstname
                        // changes to display all contacts according to user details
                        if contactsArray.count > 1 {
                            for phone in contact.phoneNumbers {
                                let contactMutable = CNMutableContact()
                                let stringPhone = phone.value.stringValue
                                let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue :stringPhone ))
                                contactMutable.givenName = contact.givenName
                                contactMutable.phoneNumbers = [homePhone]
                                contactsArray.append(contactMutable)
                                
                                var key: String = "#"
                                //If ordering has to be happening via family name change it here.
                                if let firstLetter = contactMutable.givenName[0..<1] , firstLetter.containsAlphabets() {
                                    key = firstLetter.uppercased()
                                }
                                var contacts = [CNContact]()
                                if let segregatedContact = self.orderedContacts[key] {
                                    contacts = segregatedContact
                                }
                                contacts.append(contactMutable)
                                self.orderedContacts[key] = contacts
                            }
                        } else {
                            contactsArray.append(contact)
                            
                            var key: String = "#"
                            //If ordering has to be happening via family name change it here.
                            if let firstLetter = contact.givenName[0..<1] , firstLetter.containsAlphabets() {
                                key = firstLetter.uppercased()
                            }
                            var contacts = [CNContact]()
                            if let segregatedContact = self.orderedContacts[key] {
                                contacts = segregatedContact
                            }
                            contacts.append(contact)
                            self.orderedContacts[key] = contacts
                        }
                    })
                    self.sortedContactKeys = Array(self.orderedContacts.keys).sorted(by: <)
                    if self.sortedContactKeys.first == "#" {
                        self.sortedContactKeys.removeFirst()
                        self.sortedContactKeys.append("#")
                    }
                    completion(contactsArray, nil)
                }
                //Catching exception as enumerateContactsWithFetchRequest can throw errors
                catch let error as NSError {
                    println_debug(error.localizedDescription)
                }
        }
    }
    
    func allowedContactKeys() -> [CNKeyDescriptor]{
        //We have to provide only the keys which we have to access. We should avoid unnecessary keys when fetching the contact. Reducing the keys means faster the access.
        return [CNContactNamePrefixKey as CNKeyDescriptor,
            CNContactGivenNameKey as CNKeyDescriptor,
            CNContactFamilyNameKey as CNKeyDescriptor,
            CNContactOrganizationNameKey as CNKeyDescriptor,
            CNContactBirthdayKey as CNKeyDescriptor,
            CNContactImageDataKey as CNKeyDescriptor,
            CNContactThumbnailImageDataKey as CNKeyDescriptor,
            CNContactImageDataAvailableKey as CNKeyDescriptor,
            CNContactPhoneNumbersKey as CNKeyDescriptor,
            CNContactEmailAddressesKey as CNKeyDescriptor,
        ]
    }
    
    // MARK: - Table View DataSource
    override open func numberOfSections(in tableView: UITableView) -> Int {
        tableView.tableFooterView = UIView()
        if resultSearchController.isActive, let text = resultSearchController.searchBar.text, text.count > 0 {
            if filteredContacts.count == 0 {
                let noDataLabel = UILabel(frame: tableView.bounds)
                noDataLabel.text = "No Contact Found!!!".localized
                noDataLabel.textAlignment = .center
                tableView.backgroundView = noDataLabel
            } else {
                tableView.backgroundView = nil
            }
            return 1
        }
        return sortedContactKeys.count
    }
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if resultSearchController.isActive, let text = resultSearchController.searchBar.text, text.count > 0
        { return filteredContacts.count }
        if let contactsForSection = orderedContacts[sortedContactKeys[section]] {
            return contactsForSection.count
        }
        return 0
    }

    // MARK: - Table View Delegates
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ContactPickerCell
        cell.accessoryType = UITableViewCellAccessoryType.none
        //Convert CNContact to ContactPicker
		let contact: ContactPicker
        
        if resultSearchController.isActive , let text = resultSearchController.searchBar.text, text.count > 0{
            contact = ContactPicker(contact: filteredContacts[(indexPath as NSIndexPath).row])
        } else {
			guard let contactsForSection = orderedContacts[sortedContactKeys[(indexPath as NSIndexPath).section]] else {
				assertionFailure()
				return UITableViewCell()
			}
			contact = ContactPicker(contact: contactsForSection[(indexPath as NSIndexPath).row])
        }
		
        if multiSelectEnabled  && selectedContacts.contains(where: { $0.contactId == contact.contactId }) {
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
        }
        cell.updateContactsinUI(contact, indexPath: indexPath, subtitleType: subtitleCellValue)
        return cell
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! ContactPickerCell
        let selectedContact = cell.contact!
        
        if multiSelectEnabled {
            //Keeps track of enable=ing and disabling contacts
            if cell.accessoryType == UITableViewCellAccessoryType.checkmark {
                cell.accessoryType = UITableViewCellAccessoryType.none
                cell.contactSelectedImageView.isHidden = true
                selectedContacts = selectedContacts.filter(){
                    return selectedContact.contactId != $0.contactId
                }
            }
            else {
                cell.contactSelectedImageView.isHidden = false
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
                selectedContacts.append(selectedContact)
            }
        }
        else {
            //Single selection code
            resultSearchController.isActive = false
            self.dismiss(animated: true, completion: {
                DispatchQueue.main.async {
                    self.contactDelegate?.contact(self, didSelectContact: selectedContact)
                }
            })
        }
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    override open func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        if resultSearchController.isActive, let text = resultSearchController.searchBar.text, text.count > 0 { return 0 }
        tableView.scrollToRow(at: IndexPath(row: 0, section: index), at: UITableViewScrollPosition.top , animated: false)        
        return sortedContactKeys.index(of: title)!
    }
    
    override  open func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if resultSearchController.isActive, let text = resultSearchController.searchBar.text, text.count > 0 { return nil }
        return sortedContactKeys
    }

    override open func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if resultSearchController.isActive , let text = resultSearchController.searchBar.text, text.count > 0{ return nil }
        return sortedContactKeys[section]
    }
    
    // MARK: - Button Actions
    @objc func onTouchCancelButton(){
        
        if resultSearchController.isActive {
           resultSearchController.isActive = false
        }
        
        self.dismiss(animated: true, completion: nil)
//        dismiss(animated: true, completion: {
//            self.contactDelegate?.contact(self, didCancel: NSError(domain: "ContactPickerPickerErrorDomain", code: 2, userInfo: [ NSLocalizedDescriptionKey: "User Canceled Selection"]))
//        })
    }
    
    @objc func onTouchDoneButton(){
//        dismiss(animated: true, completion: {
//            self.contactDelegate?.contact(self, didSelectMultipleContacts: self.selectedContacts)
//        })
    }
    
    // MARK: - Search Actions
    open func updateSearchResults(for searchController: UISearchController) {
        if let searchText = resultSearchController.searchBar.text , searchController.isActive, !searchText.isEmpty {
            let predicate: NSPredicate
            if searchText.count > 0 {
                predicate = CNContact.predicateForContacts(matchingName: searchText)
            } else {
                predicate = CNContact.predicateForContactsInContainer(withIdentifier: contactsStore!.defaultContainerIdentifier())
            }
            let contactFetchRequest = CNContactFetchRequest(keysToFetch: allowedContactKeys())
            contactFetchRequest.predicate = predicate
            filteredContacts.removeAll()
            do {
                try contactsStore?.enumerateContacts(with: contactFetchRequest, usingBlock: { (contact, stop) -> Void in
                    //Ordering contacts based on alphabets in firstname
                    // changes to display all contacts according to user details
                    for phone in contact.phoneNumbers {
                        let contactMutable = CNMutableContact()
                        let stringPhone = phone.value.stringValue
                        let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue :stringPhone ))
                        contactMutable.givenName = contact.givenName
                        contactMutable.phoneNumbers = [homePhone]
                        self.filteredContacts.append(contactMutable)
                    }
                })
                //  println_debug("\(filteredContacts.count) count")
                self.tableView.reloadData()
            }
            catch{
                println_debug("Error!")
            }
        } else {
            self.tableView.reloadData()
        }
    }
    
    open func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        DispatchQueue.main.async(execute: {
            self.tableView.reloadData()
        })
    }
}
